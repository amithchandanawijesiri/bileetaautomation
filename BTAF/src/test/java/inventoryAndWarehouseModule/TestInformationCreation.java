package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.genarateInventoryAndWarehouseInformation;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})
public class TestInformationCreation extends genarateInventoryAndWarehouseInformation{
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Create Information",
				"Test Information Creation");
	}
	
	/* Product Group Configuration */
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Product Group")
	public void productGroupCreationVerify1() throws Exception {
		productGroupConfig1();
	}
	
	@Test(dependsOnMethods = "productGroupCreationVerify1")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Product Group")
	public void productGroupCreationVerify2() throws Exception {
		productGroupConfig2();
	}
	
	/* Manufacturer */
	
//	@Test(dependsOnMethods = "productGroupCreationVerify2")
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("Menufaturer")
//	public void createMenfactururverify() throws Exception {
//		createMenfacturur();
//	}
//	
//	/* Transaction Book Configuration - Warehouse */
//	
//	@Test(dependsOnMethods = "createMenfactururverify")
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("Lot Book Configuration")
//	public void transactionBookConfigurationWarehouseCreationVerify() throws Exception {
//		transactionBookConfigurationWarehouseCreation();
//	}
//	
//	/* Default UOM Group */
//	@Test(dependsOnMethods = "transactionBookConfigurationWarehouseCreationVerify")
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("UOM Group")
//	public void uomGroupCreationVerify() throws Exception {
//		uomGroupCreation();
//	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}
}
