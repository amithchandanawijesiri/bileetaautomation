package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})
public class IW_TC_009 extends InventoryAndWarehouseModule {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Verify that User can be able to Do Stock Take for warehouse",
				"IW_TC_009");
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to New Stock Take")
	@Step("Login, Navigate to inventory and warehousing")
	public void verifyNavigateToNewStockTakePage() throws Exception {
		navigateToNewStockTakePage();
	}
	
	@Test(dependsOnMethods = "verifyNavigateToNewStockTakePage")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Stock Take Process")
	public void verifyStockTakeProcess() throws Exception {
		stockTakeProcess();
	}
	
	@Test(dependsOnMethods = "verifyStockTakeProcess")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Stock Take Draft and Release")
	public void verifyDraftReleeseStockTake() throws Exception {
		draftReleeseStockTake();
	}
	
	@Test(dependsOnMethods = "verifyDraftReleeseStockTake")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Convert Stock Adjustment")
	public void verifyConvertStockAdjustmentStockTake() throws Exception {
		convertStockAdjustmentStockTake();
	}
	
	@Test(dependsOnMethods = "verifyConvertStockAdjustmentStockTake")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft and Release Stock Adjustment")
	public void verifyDraftReleaseStockAdjustment() throws Exception {
		draftReleaseStockAdjustment();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}
}
