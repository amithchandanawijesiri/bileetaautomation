package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression3;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_ST_042 extends InventoryAndWarehouseModuleRegression3 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Stock Take",
				"Verify that user cannot reverse the stock take when stock adjustment is drafted",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Stock Take")
	@Step("Login to the Entution")
	public void verifyStockTake_IN_ST_042() throws Exception {
		stockTake_IN_ST_042();
	}

	@Test(dependsOnMethods = "verifyStockTake_IN_ST_042")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft Stock Adjsutment")
	public void verifyDrfatStockAdjustment_IN_ST_042() throws Exception {
		drfatStockAdjustment_IN_ST_042();
	}
	
	@Test(dependsOnMethods = "verifyDrfatStockAdjustment_IN_ST_042")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify reverse action disappearence")
	public void verifyAndCheckReverseActionDisappearance_IN_ST_042() throws Exception {
		checkReverseActionDisappearance_IN_ST_042();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}

}
