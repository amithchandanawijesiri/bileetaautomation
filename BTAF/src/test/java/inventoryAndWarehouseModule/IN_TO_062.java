package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_062 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order", "Verify the docflow is displaying correctly",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {
			"inventoryAndWarehouseModule.AddQuantityGBV_01.verifyAddQuantityThroughStockAdjustment"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order Journey")
	@Step("Login to the Entution")
	public void verifyAndCompleteTransferOrderJourneyWithMultipleInboundShipments01_IN_TO_062() throws Exception {
		completeTransferOrderJourneyWithMultipleInboundShipments01_IN_TO_062();
	}

	@Test(dependsOnMethods = "verifyAndCompleteTransferOrderJourneyWithMultipleInboundShipments01_IN_TO_062")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order journey")
	public void verifyAndCompleteTransferOrderJourneyWithMultipleInboundShipments02_IN_TO_062() throws Exception {
		completeTransferOrderJourneyWithMultipleInboundShipments02_IN_TO_062();
	}

	@Test(dependsOnMethods = "verifyAndCompleteTransferOrderJourneyWithMultipleInboundShipments02_IN_TO_062")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify Dockflow")
	public void verifyAndCheckDockflow_IN_TO_O62() throws Exception {
		checkDockflow_IN_TO_O62();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
