package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_098 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that reserved  qty getting removed when reverse the Trasnfer order which already manually reserved",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"inventoryAndWarehouseModule.IN_TO_096.verifyAndCheckAllocationManager_IN_TO_096"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Reverse Transfer Order")
	@Step("Login to the Entution")
	public void verifyReverseTransferOrder_IN_TO_098() throws Exception {
		reverseTransferOrder_IN_TO_098();
	}

	@Test(dependsOnMethods = "verifyReverseTransferOrder_IN_TO_098")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check allocation manager")
	public void verifyAndCheckWeatherReservationNotAvailable_IN_TO_098() throws Exception {
		checkWeatherReservationNotAvailable_IN_TO_098();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}

}
