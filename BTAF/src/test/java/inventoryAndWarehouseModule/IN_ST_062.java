package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression3;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_ST_062 extends InventoryAndWarehouseModuleRegression3 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Stock Take",
				"Verify that only warehouse code display in stock take and stock adjustment form",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Configure warehouse code only")
	@Step("Login to the Entution")
	public void verifyConfigureWarehouseCodeOnly_IN_ST_062() throws Exception {
		configureWarehouseCodeOnly_IN_ST_062();
	}

	@Test(dependsOnMethods = "verifyConfigureWarehouseCodeOnly_IN_ST_062")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check Stock Take with warehouse code only")
	public void verifyStockTake_IN_ST_062() throws Exception {
		stockTake_IN_ST_062();
	}
	
	@Test(dependsOnMethods = "verifyStockTake_IN_ST_062")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check Stock Adjustment with warehouse code only")
	public void verifyStockAdjustment_IN_ST_062() throws Exception {
		stockAdjustment_IN_ST_062();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}

}
