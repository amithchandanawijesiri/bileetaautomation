package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })
public class IW_TC_003 extends InventoryAndWarehouseModule {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Verify that user can successfully add a batch product",
				"IW_TC_003");
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify user can create batch specific product")
	@Step("Login, Navigate to Inventory And Warehouse Module")
	public void verifyCreateBatchSpecificProduct() throws Exception {
		createBatchSpecificProduct();
	}

	@Test(dependsOnMethods = "verifyCreateBatchSpecificProduct")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify user can create batch fifo product")
	@Step("Navigate to Inventory And Warehouse Module")
	public void verifyCreateBatchFifoProduct() throws Exception {
		createBatchFifoProduct();
	}

	@Test(dependsOnMethods = "verifyCreateBatchFifoProduct")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify user can create lot product")
	@Step("Navigate to Inventory And Warehouse Module")
	public void verifyCreateLotProduct() throws Exception {
		createLotProduct();
	}

	@Test(dependsOnMethods = "verifyCreateLotProduct")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify user can create Seriel Specific product")
	@Step("Navigate to Inventory And Warehouse Module")
	public void verifyCreateSerielSpecificProduct() throws Exception {
		createSerielSpecificProduct();
	}

	@Test(dependsOnMethods = "verifyCreateSerielSpecificProduct")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify user can create Seriel Batch Specific product")
	@Step("Navigate to Inventory And Warehouse Module")
	public void verifyCreateSerielBatchSpecificProduct() throws Exception {
		createSerielBatchSpecificProduct();
	}

	@Test(dependsOnMethods = "verifyCreateSerielBatchSpecificProduct")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify user can create Seriel Batch FIFO product")
	@Step("Navigate to Inventory And Warehouse Module")
	public void verifyCreateSerielBatchFifoProduct() throws Exception {
		createSerielBatchFifoProduct();
	}

	@Test(dependsOnMethods = "verifyCreateSerielBatchFifoProduct")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify user can create Seriel FIFO product")
	@Step("Navigate to Inventory And Warehouse Module")
	public void verifyCreateSerielFifoProduct() throws Exception {
		createSerielFifoProduct();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}
}
