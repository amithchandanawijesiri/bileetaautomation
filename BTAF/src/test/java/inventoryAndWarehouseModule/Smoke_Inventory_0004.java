package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleSmoke;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})
public class Smoke_Inventory_0004 extends InventoryAndWarehouseModuleSmoke {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Verify that user can be able to dispatch product quantites via Internal Order [Employee Request]", "Smoke_Inventory_0004");
	}
	
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Internel Order page")
	@Step("Login, Navigate to inventory and warehousing")
	public void verifynavigateToInternelOrderPage() throws Exception {
		navigateToInternelOrderPage();
	}
	
	@Test(dependsOnMethods = "verifynavigateToInternelOrderPage")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to new Internel Order form")
	public void VerifyNavigateToNewInternelOrderForm() throws Exception {
		navigateToNewInternelOrderForm();
	}
	@Test(dependsOnMethods = "VerifyNavigateToNewInternelOrderForm")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Internel Order form fill")
	public void verifyFillNewInternelOrderForm() throws Exception {
		fillNewInternelOrderForm();
	}
	
	@Test(dependsOnMethods = "verifyFillNewInternelOrderForm")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft relese Internel Order")
	public void verifyDraftReleseInternelOrder() throws Exception {
		draftReleseInternelOrder();
	}
	
	@Test(dependsOnMethods = "verifyDraftReleseInternelOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft relese Internel Dispatch Order")
	public void veifyDraftReleseInternelDispatchOrder() throws Exception {
		draftReleseInternelDispatchOrder();
	}
	
	@Test(dependsOnMethods = "veifyDraftReleseInternelDispatchOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check available product deduction from the warehouse")
	public void verifyCheckWarehouseForProdctAvailabilityInternelOrder() throws Exception {
		checkWarehouseForProdctAvailabilityInternelOrder();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}
}
