package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_079 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that only warehouse description display in Trasnfer order / Outbound shipment/ Inbound shipment forms",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create both from and two warehouses")
	@Step("Login to the Entution")
	public void verifyWarehouseCreation_IN_TO_079() throws Exception {
		warehouseCreation_IN_TO_079();
	}

	@Test(dependsOnMethods = "verifyWarehouseCreation_IN_TO_079")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Stock Adjustment for from warehouse")
	public void verifyStockAdjustment_IN_TO_079() throws Exception {
		stockAdjustment_IN_TO_079();
	}

	@Test(dependsOnMethods = "verifyStockAdjustment_IN_TO_079")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Configure name only for warehouse in balancing laval setting")
	public void verifyConfigureWarehouseNameOnly_IN_TO_079() throws Exception {
		configureWarehouseNameOnly_IN_TO_079();
	}

	@Test(dependsOnMethods = "verifyConfigureWarehouseNameOnly_IN_TO_079")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order journey")
	public void verifyCompleteTransferOrderJourney_IN_TO_079() throws Exception {
		completeTransferOrderJourney_IN_TO_079();
	}

	@Test(dependsOnMethods = "verifyCompleteTransferOrderJourney_IN_TO_079")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check whether name only applied")
	public void verifyAndCheckNameOnlyOfWarehouseFieldsTransferOrderJourney_IN_TO_079() throws Exception {
		checkNameOnlyOfWarehouseFieldsTransferOrderJourney_IN_TO_079();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		resetWarehouseCodeAndDescription();
		common.totalTime(startTime);
		common.driverClose();

	}

}
