package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_094 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that reserved qty cannot be used for any transactions",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Configure automatic stock reservation")
	@Step("Login to the Entution")
	public void verifyConfigureAutomaticStockReservatio_IN_TO_094() throws Exception {
		configureAutomaticStockReservatio_IN_TO_094();
	}

	@Test(dependsOnMethods = "verifyConfigureAutomaticStockReservatio_IN_TO_094")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create new warehouse")
	public void verifyCreateNewWarehouse_IN_TO_094() throws Exception {
		createNewWarehouse_IN_TO_094();
	}
	
	@Test(dependsOnMethods = "verifyCreateNewWarehouse_IN_TO_094")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Stock Adjustment")
	public void verifyStockAdjustment_IN_TO_094() throws Exception {
		stockAdjustment_IN_TO_094();
	}
	
	@Test(dependsOnMethods = "verifyStockAdjustment_IN_TO_094")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Transfer Order")
	public void verifyTransferOrder_IN_TO_094() throws Exception {
		transferOrder_IN_TO_094();
	}
	
	@Test(dependsOnMethods = "verifyTransferOrder_IN_TO_094")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Sales Order to Outbound Shipment")
	public void verifySalesOrderOutboundShipment_IN_IO_094() throws Exception {
		salesOrderOutboundShipment_IN_IO_094();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		setManuelStockReservationTransferOrder();
		common.totalTime(startTime);
		common.driverClose();
	}

}
