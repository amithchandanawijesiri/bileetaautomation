package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;

public class IN_SA_089 extends InventoryAndWarehouseModuleRegression{
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Stock Adjustment Joureny active",
				this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Configure shipment acceptance")
	@Step("Login")
	public void navigateStockAdjustmentPage() throws Exception {
		navigateToJourneyConfig();
		journeyChange("Inactivated");
//		navigateToStockAdjustmentFormJourneyConfigActiveToInactive();
		navigateToStockAdjustmentFormForJourneyActive();

		
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}

}
