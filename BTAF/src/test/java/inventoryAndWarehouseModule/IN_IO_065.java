package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_IO_065 extends InventoryAndWarehouseModuleRegression{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "",
				this.getClass().getSimpleName());
	}

	@Test /*
			 * (dependsOnMethods =
			 * {"inventoryAndWarehouseModule.AddQuantity_03.verifyAddQuantityThroughStockAdjustment"
			 * })
			 */
	@Severity(SeverityLevel.CRITICAL)
	@Description("First Stock Adjustment")
	@Step("Login to the Entution")
	public void verifyFirstLotStockAdjustment_IN_IO_065() throws Exception {
		firstLotStockAdjustment_IN_IO_065();
	}
	
	@Test(dependsOnMethods = "verifyFirstLotStockAdjustment_IN_IO_065")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Second Stock Adjustment")
	public void verifySecondLotStockAdjustment_IN_IO_065() throws Exception {
		secondLotStockAdjustment_IN_IO_065();
	}
	
	@Test(dependsOnMethods = "verifySecondLotStockAdjustment_IN_IO_065")
	@Severity(SeverityLevel.CRITICAL)
	@Description("First Internal Order")
	public void verifyFirstInternelOrder_IN_IO_065() throws Exception {
		firstInternelOrder_IN_IO_065();
	}
	
	@Test(dependsOnMethods = "verifyFirstInternelOrder_IN_IO_065")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Second Internal Order")
	public void verifySecondInternelOrder_IN_IO_065() throws Exception {
		secondInternelOrder_IN_IO_065();
	}
	
	@Test(dependsOnMethods = "verifySecondInternelOrder_IN_IO_065")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check remaining stock")
	public void verifyCheckStock_IN_IO_065() throws Exception {
		checkStock_IN_IO_065();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
