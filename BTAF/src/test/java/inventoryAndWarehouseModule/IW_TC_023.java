package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;

public class IW_TC_023 extends InventoryAndWarehouseModule{

		TestCommonMethods common = new TestCommonMethods();

		@BeforeClass
		public void setUp() throws Exception {
			common.setUp("Inventory and Warehousing", "", "Verify that User can do Shipment Acceptance partially or fully [Shipment Acceptance]",
					"IW_TC_023");
		}
		
		@Test
		@Severity(SeverityLevel.CRITICAL)
		@Description("Configure shipment acceptance")
		@Step("Login")
		public void verifyCofigureShipmentAcceptance() throws Exception {
			cofigureShipmentAcceptance();
		}
		
		@Test(dependsOnMethods ="verifyCofigureShipmentAcceptance" )
		@Severity(SeverityLevel.CRITICAL)
		@Description("Create Purchase Order")
		public void verifyPurchaseOrderCommonForShipmentAcceptance() throws Exception {
			purchaseOrderCommonForShipmentAcceptance();
		}
		
		@Test(dependsOnMethods ="verifyPurchaseOrderCommonForShipmentAcceptance" )
		@Severity(SeverityLevel.CRITICAL)
		@Description("Shipment Acceptance")
		public void verifyShipmentAcceptance() throws Exception {
			shipmentAcceptance();
		}
		
		@Test(dependsOnMethods ="verifyShipmentAcceptance" )
		@Severity(SeverityLevel.CRITICAL)
		@Description("Complte Purchase Invoice for Shipment Acceptance")
		public void verifyCompltePurchaseInvoiseShipmentAcceptance() throws Exception {
			compltePurchaseInvoiseShipmentAcceptance();
		}
		
		@AfterClass(alwaysRun = true)
		public void AfterClass() throws Exception {
			common.totalTime(startTime);
			common.driverClose();

		}
		

	
}
