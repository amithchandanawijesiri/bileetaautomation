package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_054 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that user can do backdate transactions if stock available for the particular post date",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {
			"inventoryAndWarehouseModule.AddQuantityGBV_02.verifyAddQuantityThroughStockAdjustment" })

	@Severity(SeverityLevel.CRITICAL)
	@Description("Stock Adjustment - back date")
	@Step("Login to the Entution")
	public void verifyStockAdjustment_IN_TO_054() throws Exception {
		stockAdjustment_IN_TO_054();
	}

	@Test(dependsOnMethods = { "verifyStockAdjustment_IN_TO_054" })
	@Severity(SeverityLevel.CRITICAL)
	@Description("Transfer Order")
	public void verifyTransferOrder_IN_TO_054() throws Exception {
		transferOrder_IN_TO_054();
	}

	@Test(dependsOnMethods = { "verifyTransferOrder_IN_TO_054" })
	@Severity(SeverityLevel.CRITICAL)
	@Description("Outbound Shipment")
	public void verifyOutboundShipment_IN_TO_054() throws Exception {
		outboundShipment_IN_TO_054();
	}

	@Test(dependsOnMethods = { "verifyOutboundShipment_IN_TO_054" })
	@Severity(SeverityLevel.CRITICAL)
	@Description("Inbound Shipment")
	public void verifyInboundShipment_IN_TO_054() throws Exception {
		inboundShipment_IN_TO_054();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
