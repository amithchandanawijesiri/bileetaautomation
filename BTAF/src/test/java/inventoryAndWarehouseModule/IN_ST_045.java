package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression3;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_ST_045 extends InventoryAndWarehouseModuleRegression3 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Stock Take",
				"Verify that user can reverse the released stock adjustment, if not use the stock for any transactions",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Stock Take")
	@Step("Login to the Entution")
	public void verifyStockTake_IN_ST_045() throws Exception {
		stockTake_IN_ST_045();
	}

	@Test(dependsOnMethods = "verifyStockTake_IN_ST_045")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Stock Adjustment")
	public void verifyReleaseStockAdjustment_IN_ST_045() throws Exception {
		releaseStockAdjustment_IN_ST_045();
	}
	
	@Test(dependsOnMethods = "verifyReleaseStockAdjustment_IN_ST_045")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Reverse Stock Adjustment")
	public void verifyReverseStockAdjustment_IN_ST_045() throws Exception {
		reverseStockAdjustment_IN_ST_045();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}

}
