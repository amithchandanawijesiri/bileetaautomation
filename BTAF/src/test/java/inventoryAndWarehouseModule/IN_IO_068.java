package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_IO_068 extends InventoryAndWarehouseModuleRegression{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"inventoryAndWarehouseModule.AddQuantity_03.verifyAddQuantityThroughStockAdjustment"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internel Order for return")
	@Step("Login to the Entution")
	public void verifyInternelOrderForRetutn_IN_IO_068() throws Exception {
		internelOrderForRetutn_IN_IO_068();
	}
	
	@Test(dependsOnMethods = "verifyInternelOrderForRetutn_IN_IO_068")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internel Dspatch Order for return")
	public void verifyInternelDispatchOrderForRetutn_IN_IO_068() throws Exception {
		internelDispatchOrderForRetutn_IN_IO_068();
	}
	
	@Test(dependsOnMethods = "verifyInternelDispatchOrderForRetutn_IN_IO_068")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internel Return Order")
	public void verifyInternelReturnOrder_IN_IO_068() throws Exception {
		internelReturnOrder_IN_IO_068();
	}
	
	@Test(dependsOnMethods = "verifyInternelReturnOrder_IN_IO_068")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internel Order with return quantity")
	public void verifyInternelOrderWithReturnQuantity_IN_IO_068() throws Exception {
		internelOrderWithReturnQuantity_IN_IO_068();
	}
	
	@Test(dependsOnMethods = "verifyInternelOrderWithReturnQuantity_IN_IO_068")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internel Dispatch Order with return quantity")
	public void verifyInternelDispacthOrderWithReturnQuantity_IN_IO_068() throws Exception {
		internelDispacthOrderWithReturnQuantity_IN_IO_068();
	}
	
	@Test(dependsOnMethods = "verifyInternelDispacthOrderWithReturnQuantity_IN_IO_068")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Vetify stock deduction")
	public void verifyComapreTheStockThatTranferedFromTheWarehouse_IN_IO_068() throws Exception {
		comapreTheStockThatTranferedFromTheWarehouse_IN_IO_068();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
