package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression4;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_PC_017_018 extends InventoryAndWarehouseModuleRegression4 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Product Conversion",
				"Verify that user cannot add decimal value as Qty for \"Allow decimal disabled product\" AND Verify that user allow to add decimal values if product change to Decimal allow product", this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create Lot Product")
	public void verifyCreateLotProduct_IN_PC_017_018() throws Exception {
		createLotProduct_IN_PC_017_018();
	}
	
	@Test(dependsOnMethods = {"verifyCreateLotProduct_IN_PC_017_018"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create Batch Specific Product")
	public void verifyCreateBatchSpecificProduct_IN_PC_017_018() throws Exception {
		createBatchSpecificProduct_IN_PC_017_018();
	}
	
	@Test(dependsOnMethods = {"verifyCreateBatchSpecificProduct_IN_PC_017_018"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check decimal quantity with decimal allow diabled product")
	public void verifyCheckDecimalQuantityWithDecimalAllowDisabledProduct_IN_PC_017_018() throws Exception {
		checkDecimalQuantityWithDecimalAllowDisabledProduct_IN_PC_017_018();
	}
	
	@Test(dependsOnMethods = {"verifyCheckDecimalQuantityWithDecimalAllowDisabledProduct_IN_PC_017_018"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Change products to allow decimal")
	public void verifyChangeReleventProductToAllowDecimal_IN_PC_017_018() throws Exception {
		changeReleventProductToAllowDecimal_IN_PC_017_018();
	}
	
	@Test(dependsOnMethods = {"verifyChangeReleventProductToAllowDecimal_IN_PC_017_018"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check decimal quantity with changed products")
	public void verifyAndCheckDecimalQuantityWithWhichEditAsDecimalAllowProduct_IN_PC_017_018() throws Exception {
		checkDecimalQuantityWithWhichEditAsDecimalAllowProduct_IN_PC_017_018();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
