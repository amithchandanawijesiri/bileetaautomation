package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class AV_IW_AssemblyOrder extends InventoryAndWarehouseModule{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Action Verification - Assembly Order",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to new Assembly Order")
	@Step("Login, Navigate to Inventory And Warehouse Module")
	public void verifyNavigateToAssemblyOrderActionVerification() throws Exception {

		navigateToAssemblyOrderActionVerification();

	}

	@Test(dependsOnMethods = "verifyNavigateToAssemblyOrderActionVerification")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill Assembly Order")
	public void verifyFillAssemblyOrderActionVerification() throws Exception {

		fillAssemblyOrderActionVerification();

	}
	
	@Test(dependsOnMethods = "verifyFillAssemblyOrderActionVerification")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Action Verification")
	public void verifyActionVerificationAssemblyOrder() throws Exception {

		actionVerificationAssemblyOrder();

	}
	
	@Test(dependsOnMethods = "verifyActionVerificationAssemblyOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Action Section Verification")
	public void verifyActionSectionVerificationAssemblyOrder() throws Exception {

		actionSectionVerificationAssemblyOrder();

	}
	
	@Test(dependsOnMethods = "verifyActionSectionVerificationAssemblyOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Other Action Section Verification")
	public void verifyOtherActionVerificationAssemblyOrder() throws Exception {

		otherActionVerificationAssemblyOrder();

	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
