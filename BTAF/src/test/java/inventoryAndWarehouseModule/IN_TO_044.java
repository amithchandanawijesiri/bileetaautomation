package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_044 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that user allow to complete the trasnfer order with multiple outbound shipments and multiple inbound shipments",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {
			"inventoryAndWarehouseModule.IN_TO_032_034.verifySetStockReservationManuelForTransferOrderJourney",
			"inventoryAndWarehouseModule.AddQuantityGBV_01.verifyAddQuantityThroughStockAdjustment" })
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order")
	@Step("Login to the Entution")
	public void verifyCompleteTransferOrder_IN_TO_044() throws Exception {
		completeTransferOrder_IN_TO_044();
	}

	@Test(dependsOnMethods = "verifyCompleteTransferOrder_IN_TO_044")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Outbound Shipment with six quantity")
	public void verifyOutboundShipment01QuantitySix_IN_TO_044() throws Exception {
		outboundShipment01QuantitySix_IN_TO_044();
	}

	@Test(dependsOnMethods = "verifyOutboundShipment01QuantitySix_IN_TO_044")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Inbound Shipment with five quntity")
	public void verifyInboundShipment01QuantityFive_IN_TO_044() throws Exception {
		inboundShipment01QuantityFive_IN_TO_044();
	}

	@Test(dependsOnMethods = "verifyInboundShipment01QuantityFive_IN_TO_044")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Inbound Shipment with one quantity")
	public void verifyInboundShipment02QuantityOne_IN_TO_044() throws Exception {
		inboundShipment02QuantityOne_IN_TO_044();
	}

	@Test(dependsOnMethods = "verifyInboundShipment02QuantityOne_IN_TO_044")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Outbound Shipment with four quantity")
	public void verifyOutboundShipment02QuantityFour_IN_TO_044() throws Exception {
		outboundShipment02QuantityFour_IN_TO_044();
	}

	@Test(dependsOnMethods = "verifyOutboundShipment02QuantityFour_IN_TO_044")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Inbound Shipment with two quantity")
	public void verifyInboundShipment01QuantityTwo_IN_IO_044() throws Exception {
		inboundShipment01QuantityTwo_IN_IO_044();
	}

	@Test(dependsOnMethods = "verifyInboundShipment01QuantityTwo_IN_IO_044")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Inbound Shipment with two quantity")
	public void verifyInboundShipment02QuantityTwo_IN_TO_044() throws Exception {
		inboundShipment02QuantityTwo_IN_TO_044();
	}

	@Test(dependsOnMethods = "verifyInboundShipment01QuantityTwo_IN_IO_044")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check stock updatance")
	public void verifyCheckStockUpdateAccordingly_IN_TO_044() throws Exception {
		checkStockUpdateAccordingly_IN_TO_044();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
