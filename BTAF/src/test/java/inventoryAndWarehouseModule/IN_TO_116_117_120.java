package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression3;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_116_117_120 extends InventoryAndWarehouseModuleRegression3 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that user allow to complete shipment acceptance onece for multiple products AND Verify that stock will be added to \"To warehouse\" after complete the shipment acceptance for all products  onece AND Verify that inbound shipment will release after complete the shipment acceptance",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order with Shipment Acceptance configuration")
	@Step("Login to the Entution")
	public void verifyTransferOrder_IN_TO_116_117_120() throws Exception {
		transferOrder_IN_TO_116_117_120();
	}

	@Test(dependsOnMethods = "verifyTransferOrder_IN_TO_116_117_120")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Outbound Shipment")
	public void verifyOutboundShipment_IN_TO_116_117_120() throws Exception {
		outboundShipment_IN_TO_116_117_120();
	}

	@Test(dependsOnMethods = "verifyOutboundShipment_IN_TO_116_117_120")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Inbound Shipment")
	public void verifyInboundShipment_IN_TO_116_117_120() throws Exception {
		inboundShipment_IN_TO_116_117_120();
	}

	@Test(dependsOnMethods = "verifyInboundShipment_IN_TO_116_117_120")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Release shipment acceptance for multiple products")
	public void verifyReleaseShipmentAcceptance_IN_TO_116_117_120() throws Exception {
		releaseShipmentAcceptance_IN_TO_116_117_120();
	}

	@Test(dependsOnMethods = "verifyReleaseShipmentAcceptance_IN_TO_116_117_120")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check stock updatance")
	public void verifyAndCheckStockUpdatance_IN_TO_116_117_120() throws Exception {
		checkStockUpdatance_IN_TO_116_117_120();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		removeShipmentAcceptanceTransferOrderToInboundShipment();
		common.totalTime(startTime);
		common.driverClose();
	}

}
