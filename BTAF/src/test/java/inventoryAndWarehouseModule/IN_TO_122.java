package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression3;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_122 extends InventoryAndWarehouseModuleRegression3 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that relevant set of products display in shipemnt acceptance when release the Outbound Shipment for picking",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order with Shipment Acceptance configuration")
	@Step("Login to the Entution")
	public void verifyTransferOrder_IN_TO_122() throws Exception {
		transferOrder_IN_TO_122();
	}

	@Test(dependsOnMethods = "verifyTransferOrder_IN_TO_122")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Outbound Shipment")
	public void verifyOutboundShipment_IN_TO_122() throws Exception {
		outboundShipment_IN_TO_122();
	}
	
	@Test(dependsOnMethods = "verifyOutboundShipment_IN_TO_122")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check shipment acceptance availability")
	public void verifyAndCheckShipmentAcceptanceAvailability_IN_TO_122() throws Exception {
		checkShipmentAcceptanceAvailability_IN_TO_122();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		removeShipmentAcceptanceTransferOrderToOutboundShipment();
		common.totalTime(startTime);
		common.driverClose();
	}

}
