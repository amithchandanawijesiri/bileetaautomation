package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})

public class IW_TC_001 extends InventoryAndWarehouseModule {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Verify that user can be able to create Product Group",
				"IW_TC_001");
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to the new product group page")
	@Step("Navigate to Inventory And Warehouse Module")
	public void verifyNavigateNewProductGroupPage() throws Exception {

		navigateNewProductGroupPage();

	}

	@Test(dependsOnMethods = "verifyNavigateNewProductGroupPage")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Configure new Product Group")
	public void verifyConfigureNewProductGroup() throws Exception {

		configureNewProductGroup();

	}

	@Test(dependsOnMethods = "verifyConfigureNewProductGroup")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Inventory, Purchase And Sales Information")
	public void verifyInventoryPurchaseAndSalesProductGroupConfig() throws Exception {

		inventoryPurchaseAndSalesProductGroupConfig();
		

	}

	@Test(dependsOnMethods = "verifyInventoryPurchaseAndSalesProductGroupConfig")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft and releese Product Group Configuration")
	public void verifyDraftReleeseProductGroupConfig() throws Exception {

		draftReleeseProductGroupConfig();

	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}


}
