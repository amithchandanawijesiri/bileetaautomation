package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class AV_IW_OutboundLoanOrder extends InventoryAndWarehouseModule{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Action Verification - Outbound Loan Order",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to the new Outbound Loan Order")
	@Step("Login, Navigate to Inventory And Warehouse Module")
	public void verifyNaviagteToOutboundLOanOrderFormActionVerification() throws Exception {
		naviagteToOutboundLOanOrderFormActionVerification();
	}
	
	@Test(dependsOnMethods = "verifyNaviagteToOutboundLOanOrderFormActionVerification")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill Outbound Loan Order")
	public void verifyFillOutboundLoanOrderActionVerification() throws Exception {
		 fillOutboundLoanOrderActionVerification(); 
	}
	
	@Test(dependsOnMethods = "verifyFillOutboundLoanOrderActionVerification")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Action Verification - Outbound Loan Order")
	public void verifyActionVerficationOutboundLoanOrder() throws Exception {
		actionVerficationOutboundLoanOrder();
	}
	
	@Test(dependsOnMethods = "verifyActionVerficationOutboundLoanOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Action Section Verification - Outbound Loan Order")
	public void verifyActionSectionVerifcationOutboundLOanOrder() throws Exception {
		actionSectionVerifcationOutboundLOanOrder();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
