package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_083 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that user can auto generate Outbound Shipment and stock update accordingly",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Configure auto genarate Outbound Shipment")
	@Step("Login to the Entution")
	public void verifyConfigureAutoGenarateOutboundShipment_IN_TO_083() throws Exception {
		configureAutoGenarateOutboundShipment_IN_TO_083();
	}

	@Test(dependsOnMethods = "verifyConfigureAutoGenarateOutboundShipment_IN_TO_083")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft Transfer Order")
	public void verifyDraftTransferOrder_IN_TO_083() throws Exception {
		draftTransferOrder_IN_TO_083();
	}
	
	@Test(dependsOnMethods = "verifyDraftTransferOrder_IN_TO_083")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check Seriel/Batch capture validation")
	public void verifyAndCheckTransferOrderValidation_IN_TO_083() throws Exception {
		checkTransferOrderValidation_IN_TO_083();
	}

	@Test(dependsOnMethods = "verifyAndCheckTransferOrderValidation_IN_TO_083")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Seriel/Batch capture and release Transfer Order")
	public void verifySerielBatchCaptureAndReleaseTransferOrder() throws Exception {
		serielBatchCaptureAndReleaseTransferOrder();
	}
	
	@Test(dependsOnMethods = "verifySerielBatchCaptureAndReleaseTransferOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Inbound Shipment")
	public void verifyInboundShipment_IN_TO_083() throws Exception {
		inboundShipment_IN_TO_083();
	}
	
	@Test(dependsOnMethods = "verifyInboundShipment_IN_TO_083")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check stock updatance")
	public void verifyAndCheckStockUpdateAccordingly_IN_TO_083() throws Exception {
		checkStockUpdateAccordingly_IN_TO_083();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		uncheckAutoGenarateOutboundShipment_IN_TO_083();
		common.totalTime(startTime);
		common.driverClose();

	}

}
