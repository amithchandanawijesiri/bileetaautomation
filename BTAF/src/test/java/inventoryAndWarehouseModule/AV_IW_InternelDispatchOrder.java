package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class AV_IW_InternelDispatchOrder extends InventoryAndWarehouseModule{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Action Verification - Internel Dispatch Order",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Login to the 'Entution'")
	public void verifyLoginToInternelDispatchOrder() throws Exception {

		loginInternelDispatchOrder();
	}
	
	@Test(dependsOnMethods = "verifyLoginToInternelDispatchOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Internel Dispatch Order")
	public void verifyNavigateToInternelDispatchOrder() throws Exception {

		navigateToInternelDispatchOrder();
	}
	
	@Test(dependsOnMethods = "verifyNavigateToInternelDispatchOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Action Verification")
	public void verifyactionVerificationInternelDispatchOrder() throws Exception {

		actionVerificationInternelDispatchOrder();
	}
	
	@Test(dependsOnMethods = "verifyactionVerificationInternelDispatchOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Action Section Verification")
	public void verifyActionSectionVerificationInternelDispatchOrder() throws Exception {

		actionSectionVerificationInternelDispatchOrder();
	}
	
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
//		common.driverClose();

	}

}
