package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})
public class IW_TC_014 extends InventoryAndWarehouseModule{
	
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Verify that converting products in to Inbound Shipment which outbound via Outbound Loan Order [ inbound shipment partial Qty]", "IW_TC_014");
	}
	
	
	@Test(dependsOnMethods = "inventoryAndWarehouseModule.IW_TC_013.verifyCheckWarehouseForProdctAvailabilityOutboundLoanOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Find previously created 'Outbound Loan Order'")
	@Step("Login, Navigate to inventory and warehousing")
	public void verifyFindReleventOLODocument() throws Exception {
		findReleventOLODocument();
	}
	
	@Test(dependsOnMethods = "verifyFindReleventOLODocument")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Checke before process Quantity Availability and set Planed Quantity")
	public void verifyCheckAvailabitiyAndSetPalanQuantity() throws Exception {
		checkAvailabitiyAndSetPalanQuantity();
	}
	
	@Test(dependsOnMethods = "verifyCheckAvailabitiyAndSetPalanQuantity")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Checkout, Draft And Releese")
	public void verifyCheckoutDraftReleseBatchCapture() throws Exception {
		checkoutDraftReleseBatchCapture();
	}
	
	@Test(dependsOnMethods = "verifyCheckoutDraftReleseBatchCapture")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check product availability after Inbound Shipment")
	public void verifyCheckWarehouseForProdctAvailabilityConvertToInboundShipmentOutboundLoanOrder() throws Exception {
		checkWarehouseForProdctAvailabilityConvertToInboundShipmentOutboundLoanOrder();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}
}
