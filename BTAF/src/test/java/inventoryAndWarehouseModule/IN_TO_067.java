package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_067 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that converted serials/batches via Product Conversion, can be transfer form one warehouse to another",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Product Conversion")
	@Step("Login to the Entution")
	public void verifyProductConversion_IN_TO_067() throws Exception {
		productConversion_IN_TO_067();
	}

	@Test(dependsOnMethods = "verifyProductConversion_IN_TO_067")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order")
	public void verifyTransferOrder_IN_TO_067() throws Exception {
		transferOrder_IN_TO_067();
	}

	@Test(dependsOnMethods = "verifyTransferOrder_IN_TO_067")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Outbound Shipment")
	public void verifyOutboundShipment_IN_TO_067() throws Exception {
		outboundShipment_IN_TO_067();
	}

	@Test(dependsOnMethods = "verifyOutboundShipment_IN_TO_067")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Inbound Shipment")
	public void verifyInboundShipment_IN_TO_067() throws Exception {
		inboundShipment_IN_TO_067();
	}

	@Test(dependsOnMethods = "verifyInboundShipment_IN_TO_067")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check stock updatance")
	public void verifyAndCheckStockUpdatance_IN_TO_067() throws Exception {
		checkStockUpdatance_IN_TO_067();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
