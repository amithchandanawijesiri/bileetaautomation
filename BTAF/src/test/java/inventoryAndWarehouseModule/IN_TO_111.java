package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression3;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_111 extends InventoryAndWarehouseModuleRegression3 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that user can use QC failed qty after transfer them for allocation warehouse",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Sales Order with transfered quantity")
	@Step("Login to the Entution")
	public void verifySalesOrderWithQCPassQuantity_IN_TO_111() throws Exception {
		salesOrderWithQCPassQuantity_IN_TO_111();
	}

	@Test(dependsOnMethods = { "verifySalesOrderWithQCPassQuantity_IN_TO_111" })
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Outbound Shipment")
	@Step("Login to the Entution")
	public void verifyOutboundShipment_IN_TO_111() throws Exception {
		outboundShipment_IN_TO_111();
	}

	@Test(dependsOnMethods = { "verifyOutboundShipment_IN_TO_111" })
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Inbound Shipment")
	@Step("Login to the Entution")
	public void verifyInboundShipemtn_IN_TO_111() throws Exception {
		inboundShipemtn_IN_TO_111();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}

}
