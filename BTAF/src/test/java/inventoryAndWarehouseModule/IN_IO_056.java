package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_IO_056 extends InventoryAndWarehouseModuleRegression{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"inventoryAndWarehouseModule.AddQuantity_02.verifyAddQuantityThroughStockAdjustment"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Journey Configuaration")
	@Step("Login to the Entution")
	public void verifyJourneyConfigureFor_IN_IO_056() throws Exception {
		journeyConfigureFor_IN_IO_056();
	}
	
	@Test(dependsOnMethods = "verifyJourneyConfigureFor_IN_IO_056")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internel Order")
	public void veirfyInternelOrder_IN_IO_056() throws Exception {
		internelOrder_IN_IO_056();
	}
	
	@Test(dependsOnMethods = "veirfyInternelOrder_IN_IO_056")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft / Release and Reverse Internel Order")
	public void verifyInternelOrderReleaseAndReverse_IN_IO_056() throws Exception {
		internelOrderReleaseAndReverse_IN_IO_056();
	}
	
	@Test(dependsOnMethods = "verifyInternelOrderReleaseAndReverse_IN_IO_056")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check stock reservation")
	public void verifyCheckStockReservationAfterRevesed() throws Exception {
		chaeckStockReservationAfterRevesed();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
