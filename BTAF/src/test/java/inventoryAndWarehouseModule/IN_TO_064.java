package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_064 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that multiple transactions between same warehouses will not affect product quantities and reservations",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create two warehouses for transfer order transactions")
	@Step("Login to the Entution")
	public void verifyCreateNewWarehouses_IN_TO_064() throws Exception {
		createNewWarehouses_IN_TO_064();
	}

	@Test(dependsOnMethods = "verifyCreateNewWarehouses_IN_TO_064")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order 01")
	public void verifyTransferOrder_01_IN_TO_064() throws Exception {
		transferOrder_01_IN_TO_064();
	}

	@Test(dependsOnMethods = "verifyTransferOrder_01_IN_TO_064")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Outbound Shipment 01")
	public void verifyOutboundShipment_01_IN_TO_064() throws Exception {
		outboundShipment_01_IN_TO_064();
	}

	@Test(dependsOnMethods = "verifyOutboundShipment_01_IN_TO_064")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Inbound Shipment 01")
	public void verifyInboundShipment_01_IN_TO_064() throws Exception {
		inboundShipment_01_IN_TO_064();
	}

	@Test(dependsOnMethods = "verifyInboundShipment_01_IN_TO_064")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check stock updatance 01")
	public void verifyAndCheckStockUpdatance_01_IN_TO_064() throws Exception {
		checkStockUpdatance_01_IN_TO_064();
	}

	@Test(dependsOnMethods = "verifyAndCheckStockUpdatance_01_IN_TO_064")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order 02")
	public void verifyTransferOrder_02_IN_TO_064() throws Exception {
		transferOrder_02_IN_TO_064();
	}

	@Test(dependsOnMethods = "verifyTransferOrder_02_IN_TO_064")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Outbound Shipment 02")
	public void verifyOutboundShipment_02_IN_TO_064() throws Exception {
		outboundShipment_02_IN_TO_064();
	}

	@Test(dependsOnMethods = "verifyOutboundShipment_02_IN_TO_064")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Inbound Shipment 02")
	public void verifyInboundShipment_02_IN_TO_064() throws Exception {
		inboundShipment_02_IN_TO_064();
	}

	@Test(dependsOnMethods = "verifyInboundShipment_02_IN_TO_064")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check stock updatance 02")
	public void verifyAndCheckStockUpdatance_02_IN_TO_064() throws Exception {
		checkStockUpdatance_02_IN_TO_064();
	}
	
	@Test(dependsOnMethods = "verifyAndCheckStockUpdatance_02_IN_TO_064")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order 03")
	public void verifyTransferOrder_03_IN_TO_064() throws Exception {
		transferOrder_03_IN_TO_064();
	}

	@Test(dependsOnMethods = "verifyTransferOrder_03_IN_TO_064")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Outbound Shipment 03")
	public void verifyOutboundShipment_03_IN_TO_064() throws Exception {
		outboundShipment_03_IN_TO_064();
	}

	@Test(dependsOnMethods = "verifyOutboundShipment_03_IN_TO_064")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Inbound Shipment 03")
	public void verifyInboundShipment_03_IN_TO_064() throws Exception {
		inboundShipment_03_IN_TO_064();
	}

	@Test(dependsOnMethods = "verifyInboundShipment_03_IN_TO_064")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check stock updatance 03")
	public void verifyAndCheckStockUpdatance_03_IN_TO_064() throws Exception {
		checkStockUpdatance_03_IN_TO_064();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
