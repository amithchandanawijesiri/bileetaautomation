package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_IO_054 extends InventoryAndWarehouseModuleRegression{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"inventoryAndWarehouseModule.AddQuantity_02.verifyAddQuantityThroughStockAdjustment"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Active Send Review")
	@Step("Login to the Entution")
	public void verifyActiveSendReveiwForIODraft() throws Exception {
		activeSendReveiwForIODraft();
	}
	
	@Test(dependsOnMethods = "verifyActiveSendReveiwForIODraft")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft and Approve Internel Order")
	public void veifyDraftAndSendRevievInternelOrderAndApprove() throws Exception {
		draftAndSendRevievInternelOrderAndApprove();
	}
	
	@Test(dependsOnMethods = "veifyDraftAndSendRevievInternelOrderAndApprove")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify Aprovel and Delete Internel Order")
	public void verifyCheckApprovedInternelOrderAndDelete() throws Exception {
		checkApprovedInternelOrderAndDelete();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		inactiveWorkflowInternlaOrderSendReview();
		common.totalTime(startTime);
		common.driverClose();
	}
}
