package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})
public class IW_TC_020 extends InventoryAndWarehouseModule{
	
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Verify that serials can be viewed in Serial Number viewer",
				"IW_TC_020");
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Serial Number Viewer page")
	@Step("Login, Navigate to inventory and warehousing")
	public void verifyNavigateToSerielNumberViewer() throws Exception {
		navigateToSerielNumberViewer();
	}
	
	@Test(dependsOnMethods = "verifyNavigateToSerielNumberViewer")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complte Serial Number Viewer form")
	@Step("Login, Navigate to inventory and warehousing")
	public void verifySearchForm() throws Exception {
		searchForm();
	}
	
	@Test(dependsOnMethods = "verifySearchForm")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Serial Number Viewer search result")
	@Step("Login, Navigate to inventory and warehousing")
	public void verifySearchResultAvailability() throws Exception {
		searchResultAvailability();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}
}
