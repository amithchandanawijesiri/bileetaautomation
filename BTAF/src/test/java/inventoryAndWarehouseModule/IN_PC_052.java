package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression4;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_PC_052 extends InventoryAndWarehouseModuleRegression4 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Product Conversion",
				"Verify that user can be able to convert the  Serial/Batches which transferred via Transfer Order", this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order")
	public void verifyTransferOrder_IN_PC_052() throws Exception {
		transferOrder_IN_PC_052();
	}
	
	@Test(dependsOnMethods = "verifyTransferOrder_IN_PC_052")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Outbound Shipment")
	public void verifyOutboundShipment_IN_PC_052() throws Exception {
		outboundShipment_IN_PC_052();
	}
	
	@Test(dependsOnMethods = "verifyOutboundShipment_IN_PC_052")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Inbound Shipment")
	public void verifyInboundShipment_IN_PC_052() throws Exception {
		inboundShipment_IN_PC_052();
	}
	
	@Test(dependsOnMethods = "verifyInboundShipment_IN_PC_052")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Product Conversion")
	public void verifyProductConversion_IN_PC_052() throws Exception {
		productConversion_IN_PC_052();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
