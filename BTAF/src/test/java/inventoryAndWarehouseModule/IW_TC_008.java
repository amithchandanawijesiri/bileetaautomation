package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})
public class IW_TC_008 extends InventoryAndWarehouseModule{
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Verify that User can be able to convert one product to another through Product Conversion",
				"IW_TC_008");
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Product Conversion Form")
	@Step("Login, Navigate to inventory and warehousing")
	public void verifyNaigateToProductConversionForm() throws Exception {
		naigateToProductConversionForm();
	}
	
	@Test(dependsOnMethods ="verifyNaigateToProductConversionForm" )
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill Product Conversion Form")
	public void verifyFillProductConversionForm() throws Exception {
		fillProductConversionForm();
	}
	
	@Test(dependsOnMethods ="verifyFillProductConversionForm" )
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft and Releese Product Conversion")
	public void verifyProductConversionDraftReelese() throws Exception {
		productConversionDraftReelese();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
