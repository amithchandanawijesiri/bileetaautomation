package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_IO_008 extends InventoryAndWarehouseModule{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Internel Order",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"inventoryAndWarehouseModule.AddQuantity_01.verifyAddQuantityThroughStockAdjustment"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check OH Quantity")
	public void verifyCheckAndStoreOHquantity() throws Exception {

		checkAndStoreOHquantity();

	}
	
	@Test(dependsOnMethods = "verifyCheckAndStoreOHquantity")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check Warehouse Quantity")
	public void verifyCheckWarehouseQuantity() throws Exception {

		checkWarehouseQuantity();

	}
	
	@Test(dependsOnMethods = "verifyCheckWarehouseQuantity")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Compare warehouse quantity with OH quantity")
	public void verifyCheckWarehouseQuantityWithDisplayedOhQuantity() throws Exception {

		checkWarehouseQuantityWithDisplayedOhQuantity();

	}


	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
