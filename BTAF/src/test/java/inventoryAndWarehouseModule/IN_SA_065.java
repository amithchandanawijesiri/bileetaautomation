package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression;
import bileeta.BTAF.PageObjects.ProcumentModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class IN_SA_065 extends InventoryAndWarehouseModuleRegression{
	
	TestCommonMethods common = new TestCommonMethods();
	ProcumentModule pro = new ProcumentModule();
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Procument", "", "\r\n" + "Verify that user can be able to do minus stock adjustment for  serial/Batches which inbound via Outbound Loan Order", this.getClass().getSimpleName());
	
	}
	
	@Test//(priority = 1)
	@Severity(SeverityLevel.CRITICAL)
	@Description("User login and Click on navigation menu")
	public void clickNav() throws Exception {

		pro.navigateToTheLoginPage();
		pro.verifyTheLogo();
		pro.userLogin();
		pro.clickNavigation();
	}
	
	@Test(dependsOnMethods = "clickNav")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can be able to do minus stock adjustment for  serial/Batches which inbound via Outbound Loan Order")
	public void VerThatUserCanBeAbleToDoMinusStockAdjustmentForSerialBatchesWhichInboundViaOutboundLoanOrder() throws Exception {
	
		VerifyThatUserCanBeAbleToDoMinusStockAdjustmentForSerialBatchesWhichInboundViaOutboundLoanOrder();
	}
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		  
		common.driverClose();
		 
	}
	
}

