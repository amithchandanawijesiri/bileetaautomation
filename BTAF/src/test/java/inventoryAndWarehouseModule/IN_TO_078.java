package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_078 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that only warehouse code display in Trasnfer order / Outbound shipment/ Inbound shipment forms",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Set warehouse code only")
	@Step("Login to the Entution")
	public void verifyConfigureWarehouseCodeOnly_IN_TO_078() throws Exception {
		configureWarehouseCodeOnly_IN_TO_078();
	}

	@Test(dependsOnMethods = "verifyConfigureWarehouseCodeOnly_IN_TO_078")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete transfer order journey")
	public void verifyCompleteTransferOrderJourney_IN_TO_078() throws Exception {
		completeTransferOrderJourney_IN_TO_078();
	}

	@Test(dependsOnMethods = "verifyCompleteTransferOrderJourney_IN_TO_078")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check code only setup with warehouse field")
	public void verifyAndCheckCodeCodeOnlyOfWarehouseFieldsTransferOrderJourney_IN_TO_078() throws Exception {
		checkCodeCodeOnlyOfWarehouseFieldsTransferOrderJourney_IN_TO_078();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		resetWarehouseCodeAndDescription();
		common.totalTime(startTime);
		common.driverClose();

	}

}
