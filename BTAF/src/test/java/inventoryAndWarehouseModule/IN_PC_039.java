package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression4;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_PC_039 extends InventoryAndWarehouseModuleRegression4 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Product Conversion",
				"Verify that User will be able to capture serials without entering the Lot number and Lot number will be auto-generated if the warehouse enable Auto generate Lot No", this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create lot auto genarate warehouse")
	public void verifyCreateLotAutoGenarateWarehouse_IN_PC_039() throws Exception {
		createLotAutoGenarateWarehouse_IN_PC_039();
	}
	
	@Test(dependsOnMethods = "verifyCreateLotAutoGenarateWarehouse_IN_PC_039")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check auto lot genarate function")
	public void verifyAndCheckAutoLotGenarateFunction_IN_PC_039() throws Exception {
		checkAutoLotGenarateFunction_IN_PC_039();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
