package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_032_034 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that stock will be deducted from \"From warehouse\" once release the outbound shipment",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"inventoryAndWarehouseModule.AddQuantityGBV_01.verifyAddQuantityThroughStockAdjustment"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Journey configuration for manuel stock reservation")
	@Step("Login to the Entution")
	public void verifySetStockReservationManuelForTransferOrderJourney() throws Exception {
		setStockReservationManuelForTransferOrderJourney();
		
	}

	@Test(dependsOnMethods = "verifySetStockReservationManuelForTransferOrderJourney")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Release Outbound Shipment and check stock availability")
	@Step("Login to the Entution")
	public void verifyAndCheckStockDeductionWhenReleaseOutboundShipment01() throws Exception {
		checkStockDeductionWhenReleaseOutboundShipment01();
	}

	@Test(dependsOnMethods = "verifyAndCheckStockDeductionWhenReleaseOutboundShipment01")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Release Outbound Shipment and check stock availability")
	@Step("Login to the Entution")
	public void verifyAndCheckStockDeductionWhenReleaseOutboundShipment02() throws Exception {
		checkStockDeductionWhenReleaseOutboundShipment02();
	}

	@Test(dependsOnMethods = "verifyAndCheckStockDeductionWhenReleaseOutboundShipment02")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Reverse Outbound Shipment and check stock availability")
	@Step("Login to the Entution")
	public void verifyAndCheckAddingStockWhenReverseOutboundShipment() throws Exception {
		checkAddingStockWhenReverseOutboundShipment();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
