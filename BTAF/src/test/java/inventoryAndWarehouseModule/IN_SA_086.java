package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_SA_086 extends InventoryAndWarehouseModuleRegression {


	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Warehouse Name Only",
				this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Configure shipment acceptance")
	@Step("Login")
	public void navigateStockAdjustmentPage() throws Exception {
		navigateInventoryAndWarehouseModule();
		balancingLevelConfigRegression05("Name Only");
		navigateToStockAdjustmentFormForBalancing();
		stockAdjustmentFormFillForBalancing_086();
		draftSerielBatchCaptureAndReleaseStockAdjustmentIN_SA_086();
		
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
