package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class AV_IW_AssemblyCostEstimation extends InventoryAndWarehouseModule{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Action Verification - Assembly Cost Estimation",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to the new Assembly Cost Estimation form and verify 'Copy From' button")
	@Step("Login, Navigate to Inventory And Warehouse Module")
	public void verifyNavigateToAssemblyCostEstimationAndVerifyCopyFromActionVerification() throws Exception {

		navigateToAssemblyCostEstimationAndVerifyCopyFromActionVerification();

	}

	@Test(dependsOnMethods = "verifyNavigateToAssemblyCostEstimationAndVerifyCopyFromActionVerification")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill Assembly Cost Estimation and Action Verification")
	public void verifyFillAssemblyCostEstimationActionVerfication() throws Exception {

		fillAssemblyCostEstimationActionVerfication();

	}
	
	@Test(dependsOnMethods = "verifyFillAssemblyCostEstimationActionVerfication")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Action Verification - Assembly Cost Estimation")
	public void verifyActionsAssemblyCostEstimation() throws Exception {

		actionsAssemblyCostEstimation();
	}
	
	@Test(dependsOnMethods = "verifyActionsAssemblyCostEstimation")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Action Section Verification - Assembly Cost Estimation")
	public void verifyActionSectionVerificationAssemblyCostEstimation() throws Exception {

		actionSectionVerificationAssemblyCostEstimation();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
