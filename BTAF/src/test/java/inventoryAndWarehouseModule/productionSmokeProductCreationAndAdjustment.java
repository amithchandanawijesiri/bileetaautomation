package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class productionSmokeProductCreationAndAdjustment extends InventoryAndWarehouseModule {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Bill Of Material", "Production Smoke",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Product Create - Production Type")
	public void verifyCreateProductionTypeProductSmokeProduction() throws Exception {

		createProductionTypeProductSmokeProduction();

	}

	@Test(dependsOnMethods = "verifyCreateProductionTypeProductSmokeProduction")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Product Create - Batch FIFO")
	public void verifyCreateBatchFifoProductOneSmokeProduction() throws Exception {

		createBatchFifoProductOneSmokeProduction();

	}

	@Test(dependsOnMethods = "verifyCreateBatchFifoProductOneSmokeProduction")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Product Create - Batch FIFO")
	public void verifyCreateBatchFifoProductTwoSmokeProduction() throws Exception {

		createBatchFifoProductTwoSmokeProduction();

	}
	
	@Test(dependsOnMethods = "verifyCreateBatchFifoProductTwoSmokeProduction")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Stock Adjustment for Production Smoke")
	public void verifyStockAdjustmentProductionSmoke() throws Exception {

		stockAdjustmentProductionSmoke();

	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
