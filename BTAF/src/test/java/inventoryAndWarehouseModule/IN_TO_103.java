package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression3;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_103 extends InventoryAndWarehouseModuleRegression3 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verifying whether user will have to complete QC Acceptance to use the products transferred via TO [ Partially pass or Fail Qty ]",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order")
	@Step("Login to the Entution")
	public void verifyTransferOrder_IN_TO_103() throws Exception {
		transferOrder_IN_TO_103();
	}

	@Test(dependsOnMethods = "verifyTransferOrder_IN_TO_103")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Outbound Shipment")
	public void verifyOutboundShipmen_IN_TO_103() throws Exception {
		outboundShipmen_IN_TO_103();
	}
	
	@Test(dependsOnMethods = "verifyOutboundShipmen_IN_TO_103")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Inbound Shipment")
	public void verifyInboundShipment_IN_TO_103() throws Exception {
		inboundShipment_IN_TO_103();
	}
	
	@Test(dependsOnMethods = "verifyInboundShipment_IN_TO_103")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check Bincard before QC Acceptance")
	public void verifyAndCheckBincardBeforeQCAcceptance_IN_TO_103() throws Exception {
		checkBincardBeforeQCAcceptance_IN_TO_103();
	}
	
	@Test(dependsOnMethods = "verifyAndCheckBincardBeforeQCAcceptance_IN_TO_103")
	@Severity(SeverityLevel.CRITICAL)
	@Description("QC Acceptance process")
	public void verifyQcFailForFullQuantity_IN_TO_103() throws Exception {
		qcFailForFullQuantity_IN_TO_103();
	}
	
	@Test(dependsOnMethods = "verifyQcFailForFullQuantity_IN_TO_103")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check Bincard after QC Acceptance")
	public void verifyAndCheckBincardAfterQCAcceptance_IN_TO_103() throws Exception {
		checkBincardAfterQCAcceptance_IN_TO_103();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		removeQCAcceptanceTransferOrderToInboundShipment();
		common.totalTime(startTime);
		common.driverClose();
	}

}
