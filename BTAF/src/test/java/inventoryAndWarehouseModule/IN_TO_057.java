package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_057 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that user cannot enter decimal value as Qty if product is disabled allow decimal",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {
			"inventoryAndWarehouseModule.AddQuantityGBV_01.verifyAddQuantityThroughStockAdjustment"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create decimal not allow product")
	@Step("Login to the Entution")
	public void verifyCreateDecimalNotAllowProduct_IN_TO_057() throws Exception {
		createDecimalNotAllowProduct_IN_TO_057();
	}

	@Test(dependsOnMethods = "verifyCreateDecimalNotAllowProduct_IN_TO_057")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check decimal not allow product with Transfer Order")
	public void verfyAndCheckUserDoesNotAbleAddDecimalValue_IN_TO_057() throws Exception {
		checkUserDoesNotAbleAddDecimalValue_IN_TO_057();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
