package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_IO_067 extends InventoryAndWarehouseModuleRegression{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"inventoryAndWarehouseModule.AddQuantity_03.verifyAddQuantityThroughStockAdjustment"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Sales Order")
	@Step("Login to the Entution")
	public void verifySalesOrderOutbountShipmentToSalesInvoice_IN_IO_067() throws Exception {
		salesOrderOutbountShipmentToSalesInvoice_IN_IO_067();
	}
	
	@Test(dependsOnMethods = "verifySalesOrderOutbountShipmentToSalesInvoice_IN_IO_067")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Sales Return Order")
	public void verifySalessReturnOrderInboundShipmentToSalesInvoice_IN_IO_067() throws Exception {
		salesReturnOrderInboundShipmentToSalesInvoice_IN_IO_067();
	}
	
	@Test(dependsOnMethods = "verifySalessReturnOrderInboundShipmentToSalesInvoice_IN_IO_067")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internel Order")
	public void verifyInternelOrderPart_IN_IO_067() throws Exception {
		internelOrderPart_IN_IO_067();
	}
	
	@Test(dependsOnMethods = "verifyInternelOrderPart_IN_IO_067")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internel Dispatch Order and verify")
	public void verifyInternelDispatchOrderPart_IN_IO_067() throws Exception {
		internelDispatchOrderPart_IN_IO_067();
	}
	
	@Test(dependsOnMethods = "verifyInternelDispatchOrderPart_IN_IO_067")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify stock deduction")
	public void verifyComapreTheStockThatTranferedFromTheWarehouse_IN_IO_067() throws Exception {
		comapreTheStockThatTranferedFromTheWarehouse_IN_IO_067();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
