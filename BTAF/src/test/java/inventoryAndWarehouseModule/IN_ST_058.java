package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression3;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_ST_058 extends InventoryAndWarehouseModuleRegression3 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Stock Take",
				"Verify that user can do stock take for product qty which transfer via Rack Transfer and stock update properly",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Warehouse Creation with racks")
	@Step("Login to the Entution")
	public void verifyWarehouseCreationWithTwoRacks_IN_ST_058() throws Exception {
		warehouseCreationWithTwoRacks_IN_ST_058();
	}

	@Test(dependsOnMethods = "verifyWarehouseCreationWithTwoRacks_IN_ST_058")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Stock Adjustment")
	public void verifyStockAdjustmentRack01_IN_ST_058() throws Exception {
		stockAdjustmentRack01_IN_ST_058();
	}
	
	@Test(dependsOnMethods = "verifyStockAdjustmentRack01_IN_ST_058")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Rack Transfer")
	public void verifyRackTransfer_IN_ST_058() throws Exception {
		rackTransfer_IN_ST_058();
	}

	@Test(dependsOnMethods = "verifyRackTransfer_IN_ST_058")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Stock Take")
	public void verifyStockTake_IN_ST_058() throws Exception {
		stockTake_IN_ST_058();
	}

	@Test(dependsOnMethods = "verifyStockTake_IN_ST_058")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Stock Adjustment")
	public void verifyReleaseStockAdjustment_IN_ST_058() throws Exception {
		releaseStockAdjustment_IN_ST_058();
	}
	
	@Test(dependsOnMethods = "verifyReleaseStockAdjustment_IN_ST_058")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify stock update")
	public void verifyAndCheckStockUpdate_IN_ST_058() throws Exception {
		checkStockUpdate_IN_ST_058();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}

}
