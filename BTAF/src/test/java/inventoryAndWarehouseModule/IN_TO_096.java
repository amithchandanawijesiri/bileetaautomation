package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_096 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that Allocation allowed products can be reserved manually",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Configure manual stock reservation")
	@Step("Login to the Entution")
	public void verifyConfigureManuelStockReservatio_IN_TO_096() throws Exception {
		configureManuelStockReservatio_IN_TO_096();
	}

	@Test(dependsOnMethods = "verifyConfigureManuelStockReservatio_IN_TO_096")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order")
	public void verifyTransferOrder_IN_TO_096() throws Exception {
		transferOrder_IN_TO_096();
	}
	
	@Test(dependsOnMethods = "verifyTransferOrder_IN_TO_096")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check allocation manager")
	public void verifyAndCheckAllocationManager_IN_TO_096() throws Exception {
		checkAllocationManager_IN_TO_096();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}

}
