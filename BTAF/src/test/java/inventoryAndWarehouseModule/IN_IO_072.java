package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_IO_072 extends InventoryAndWarehouseModuleRegression{

	TestCommonMethods common = new TestCommonMethods();
	InventoryAndWarehouseModule invObj = new InventoryAndWarehouseModule();
	
	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Genarate Barcode")
	@Step("Login to the Entution , Create dispatch only product, Complete purchase order")
	public void verifyGenarateBarcode_IN_IO_072() throws Exception {
		genarateBarcode_IN_IO_072();
	}
	
	@Test(dependsOnMethods = "verifyGenarateBarcode_IN_IO_072")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internel Order")
	public void verifyInternelOrder_IN_IO_072() throws Exception {
		internelOrder_IN_IO_072();
	}
	
	@Test(dependsOnMethods = "verifyInternelOrder_IN_IO_072")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internel Dispatch Order")
	public void verifyInternelDispatchOrder_072() throws Exception {
		internelDispatchOrder_072();
	}
	
	@Test(dependsOnMethods = "verifyInternelDispatchOrder_072")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify stock deduction")
	public void verifyAndComapreTheStockThatTranferedFromTheWarehouse_IN_IO_072() throws Exception {
		comapreTheStockThatTranferedFromTheWarehouse_IN_IO_072();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		invObj.removeShipmentAcceptance();
		common.totalTime(startTime);
		common.driverClose();
	}
}
