package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})
public class IW_TC_013 extends InventoryAndWarehouseModule {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Verify that user can be able to outbound products as a loan via Outbound Loan Order",
				"IW_TC_013");
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Outbound Loan Order")
	@Step("Login, Navigate to inventory and warehousing")
	public void verifyNavigateToOutboundLoanOrder() throws Exception {
		navigateToOutboundLoanOrder();
	}
	@Test(dependsOnMethods = "verifyNavigateToOutboundLoanOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify Outbound Loan Order form avilability")
	public void verifyOutboundLoanOrderFormAvailability() throws Exception {
		OutboundLoanOrderFormAvailability();
	}
	
	@Test(dependsOnMethods = "verifyOutboundLoanOrderFormAvailability")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Filling Outbound Loan Orger form")
	public void verifyOutboundLoanOrderFormFilling() throws Exception {
		OutboundLoanOrderFormFilling();
	}
	
	@Test(dependsOnMethods = "verifyOutboundLoanOrderFormFilling")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Relese Outbound Loan Order")
	public void verifyReleseOutboundLoanOrder() throws Exception {
		releseOutboundLoanOrder();
	}
	
	@Test(dependsOnMethods = "verifyReleseOutboundLoanOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft Outbound Shipment")
	public void verifyDraftOutboundShipmentILO() throws Exception {
		draftOutboundShipmentILO();
	}
	
	@Test(dependsOnMethods = "verifyDraftOutboundShipmentILO")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Relese Outbound Shipment")
	public void verifyReleseOutboundShipmentILO() throws Exception {
		releseOutboundShipmentILO();
	}
	
	@Test(dependsOnMethods = "verifyReleseOutboundShipmentILO")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check product availability deduction")
	public void verifyCheckWarehouseForProdctAvailabilityOutboundLoanOrder() throws Exception {
		checkWarehouseForProdctAvailabilityOutboundLoanOrder();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}
}
