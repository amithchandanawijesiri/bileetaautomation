package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class AV_IW_InboundShipent extends InventoryAndWarehouseModule{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Action Verification - Inbound Shiment",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Inbound Loan Order for Inbound Shipment - Action Verification")
	@Step("Login, Navigate to Inventory And Warehouse Module")
	public void verifyInboundLOanOrderForInboundShipments() throws Exception {

		inboundLOanOrderForInboundShipments();

	}
	
	@Test(dependsOnMethods = "verifyInboundLOanOrderForInboundShipments")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Action Verification")
	public void verifyActionVerificationInboundShipment() throws Exception {

		actionVerificationInboundShipment();

	}
	
	@Test(dependsOnMethods = "verifyActionVerificationInboundShipment")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Action Section Verification")
	public void verifyActionSectionVerificationInboundShipment() throws Exception {

		actionSectionVerificationInboundShipment();

	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
//		common.driverClose();

	}

}
