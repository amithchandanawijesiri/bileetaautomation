package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression3;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_ST_019 extends InventoryAndWarehouseModuleRegression3 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Stock Take",
				"Verify that user can navigated to New Stock Take while Drafting one Stock Take and complete the both stock takes",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill and Draft & New Stock Take")
	@Step("Login to the Entution")
	public void verifyFillAndClickDraftAndNewButtonFirstStockTake_IN_ST_019() throws Exception {
		fillAndClickDraftAndNewButtonFirstStockTake_IN_ST_019();
	}
	
	@Test(dependsOnMethods = "verifyFillAndClickDraftAndNewButtonFirstStockTake_IN_ST_019")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Find recently drafted Stock Take")
	public void verifyFilndAndStoreRrcentlyDraftedStockTake_IN_ST_019() throws Exception {
		filndAndStoreRecentlyDraftedStockTake_IN_ST_019();
	}
	
	@Test(dependsOnMethods = "verifyFilndAndStoreRrcentlyDraftedStockTake_IN_ST_019")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft & Release second Stock Take")
	public void verifyAndDrfatAndReleaseSecondStockTake_IN_ST_019() throws Exception {
		drfatAndReleaseSecondStockTake_IN_ST_019();
	}
	
	@Test(dependsOnMethods = "verifyAndDrfatAndReleaseSecondStockTake_IN_ST_019")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Find and Release first drafted Stock Take")
	public void verifyFindAndReleaseFirstDraftedStockTake_IN_ST_019() throws Exception {
		findAndReleaseFirstDraftedStockTake_IN_ST_019();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}

}
