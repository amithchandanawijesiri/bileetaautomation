package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression3;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_115 extends InventoryAndWarehouseModuleRegression3 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that user cannot do any transactions for serial/batches which not complete the Shipment Acceptance",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Configure shipment accesptance and copmplete Transfer Order")
	@Step("Login to the Entution")
	public void verifyTransferOrder_IN_TO_115() throws Exception {
		transferOrder_IN_TO_115();
	}

	@Test(dependsOnMethods = "verifyTransferOrder_IN_TO_115")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Outbound Shipment")
	public void verifyOutboundShipmen_IN_TO_115() throws Exception {
		outboundShipmen_IN_TO_115();
	}
	
	@Test(dependsOnMethods = "verifyOutboundShipmen_IN_TO_115")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Inbound Shipment")
	public void verifyInboundShipment_IN_TO_115() throws Exception {
		inboundShipment_IN_TO_115();
	}

	@Test(dependsOnMethods = "verifyInboundShipment_IN_TO_115")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Try to outbound quantity via Sales Order")
	public void verifySalesOrderOutboundShipment_IN_IO_115() throws Exception {
		salesOrderOutboundShipment_IN_IO_115();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		removeShipmentAcceptanceTransferOrderToInboundShipment();
		common.totalTime(startTime);
		common.driverClose();
	}

}
