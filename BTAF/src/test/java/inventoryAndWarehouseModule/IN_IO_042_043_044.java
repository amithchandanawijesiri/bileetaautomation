package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_IO_042_043_044 extends InventoryAndWarehouseModuleRegression{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"inventoryAndWarehouseModule.AddQuantity_02.verifyAddQuantityThroughStockAdjustment"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Journey Configuration")
	@Step("Login to the Entution")
	public void verifyJourneyConfigureFor_IN_IO_042_043_044() throws Exception {
		journeyConfigureFor_IN_IO_042_043_044();
	}
	
	@Test(dependsOnMethods = "verifyJourneyConfigureFor_IN_IO_042_043_044")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internel Order")
	public void verifyFillInternelOrder_IN_IO_042_043_044() throws Exception {
		fillInternelOrder_IN_IO_042_043_044();
	}
	
	@Test(dependsOnMethods = "verifyFillInternelOrder_IN_IO_042_043_044")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft and Release Internel Dispatch Order")
	public void verifyDraftReleseInternelDispatchOrder_IN_IO_042_043_044() throws Exception {
		draftReleseInternelDispatchOrder_IN_IO_042_043_044();
	}
	
	@Test(dependsOnMethods = "verifyDraftReleseInternelDispatchOrder_IN_IO_042_043_044")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft and Release Internel Dispatch Order")
	public void verifyDraftReleseInternelDispatchOrder_IN_IO_042_043_044_2() throws Exception {
		draftReleseInternelDispatchOrder_IN_IO_042_043_044_2();
	}
	
	@Test(dependsOnMethods = "verifyDraftReleseInternelDispatchOrder_IN_IO_042_043_044_2")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft and Release Internel Dispatch Order")
	public void verifyDraftReleseInternelDispatchOrder_IN_IO_042_043_044_3() throws Exception {
		draftReleseInternelDispatchOrder_IN_IO_042_043_044_3();
	}
	
	@Test(dependsOnMethods = "verifyDraftReleseInternelDispatchOrder_IN_IO_042_043_044_3")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft and Release Internel Dispatch Order")
	public void verifyAndComapreTheStockThatTranferedFromTheWarehouse_IN_IO_042_043_044() throws Exception {
		comapreTheStockThatTranferedFromTheWarehouse_IN_IO_042_043_044();
	}
	
	@Test(dependsOnMethods = "verifyAndComapreTheStockThatTranferedFromTheWarehouse_IN_IO_042_043_044")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft and Release Internel Dispatch Order")
	public void verifyAndComapreTheStockMAinProductThatTranferedFromTheWarehouse_IN_IO_042_043_044() throws Exception {
		comapreTheStockMAinProductThatTranferedFromTheWarehouse_IN_IO_042_043_044();
	}
	
	@Test(dependsOnMethods = "verifyAndComapreTheStockMAinProductThatTranferedFromTheWarehouse_IN_IO_042_043_044")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify costing for the alternative product")
	public void verifyAndValidateCosting_IN_IO_042_I_043_I_044() throws Exception {
		validateCosting_IN_IO_042_I_043_I_044();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
