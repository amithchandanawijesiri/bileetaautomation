package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression3;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_109 extends InventoryAndWarehouseModuleRegression3 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that user can trasnfer QC failed serial/batch via Transfer order",
				this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order with Qc fail quantity")
	@Step("Login to the Entution")
	public void verifyTransferOrder_IN_TO_109() throws Exception {
		transferOrder_IN_TO_109();
	}
	
	@Test(dependsOnMethods = {"verifyTransferOrder_IN_TO_109"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Outbound Shipment")
	@Step("Login to the Entution")
	public void verifyOutboundShipmen_IN_TO_109() throws Exception {
		outboundShipmen_IN_TO_109();
	}

	@Test(dependsOnMethods = {"verifyOutboundShipmen_IN_TO_109"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Inbound Shipment")
	@Step("Login to the Entution")
	public void verifyInboundShipment_IN_TO_109() throws Exception {
		inboundShipment_IN_TO_109();
	}
	
	@Test(dependsOnMethods = {"verifyInboundShipment_IN_TO_109"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check stock updatance")
	@Step("Login to the Entution")
	public void verifyAndCheckStockUpdateAccordingly_IN_TO_109() throws Exception {
		checkStockUpdateAccordingly_IN_TO_109();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}

}
