package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_IO_023_025 extends InventoryAndWarehouseModule {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "", this.getClass().getSimpleName());
	}

	@Test
	/*
	 * (dependsOnMethods =
	 * {"inventoryAndWarehouseModule.AddQuantity_01.verifyAddQuantityThroughStockAdjustment"
	 * })
	 */
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill Internel Order")
	@Step("Login to the Entution")
	public void verifyFillMandatoryFieldsWithMultipleProductsIN_IO_023() throws Exception {
		fillMandatoryFieldsWithMultipleProductsIN_IO_023();

	}

	@Test(dependsOnMethods = "verifyFillMandatoryFieldsWithMultipleProductsIN_IO_023")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft / Release Internel Order and Internel Dispatch Order")
	public void verifyDraftReleseInternelOrderAndInternelDispatchOrder_IN_IO_023() throws Exception {
		draftReleseInternelOrderAndInternelDispatchOrder_IN_IO_023();

	}

	@Test(dependsOnMethods = "verifyDraftReleseInternelOrderAndInternelDispatchOrder_IN_IO_023")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify stock deduction")
	public void verifyComapreTheStockThatTranferedFromTheWarehouse() throws Exception {
		comapreTheStockThatTranferedFromTheWarehouse();

	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
