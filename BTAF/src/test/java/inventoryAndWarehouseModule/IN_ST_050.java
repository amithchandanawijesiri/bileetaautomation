package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression3;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_ST_050 extends InventoryAndWarehouseModuleRegression3 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Stock Take",
				"Verify that history details will display correctly in Stock Take or Stock Adjustment",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Stock Take")
	@Step("Login to the Entution")
	public void verifyStockTake_IN_ST_050() throws Exception {
		stockTake_IN_ST_050();
	}

	@Test(dependsOnMethods = "verifyStockTake_IN_ST_050")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Stock Adjustment")
	public void verifyStockAdjustment_IN_ST_050() throws Exception {
		stockAdjustment_IN_ST_050();
	}

	@Test(dependsOnMethods = "verifyStockAdjustment_IN_ST_050")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify history")
	public void verifyAndCheckHistoryRecords_IN_ST_050() throws Exception {
		checkHistoryRecords_IN_ST_050();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}

}
