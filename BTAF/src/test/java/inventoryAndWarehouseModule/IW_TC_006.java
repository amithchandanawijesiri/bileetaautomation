package inventoryAndWarehouseModule;

import javax.annotation.Priority;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })
public class IW_TC_006 extends InventoryAndWarehouseModule {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "",
				"Verify that User can be able to transfer product From one warehouse to another warehouse through transfer order verify that User can be able to do stock adjustmen",
				"IW_TC_006");
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify user can navigate to new  transfer order form")
	@Step("Login, Navigate to inventory and warehousing")
	public void verifyNavigateToTransferOrderForm() throws Exception {
		navigateToTransferOrderform();
	}

	@Test(dependsOnMethods = "verifyNavigateToTransferOrderForm")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify user can complete transfer order form")
	public void verifyCompleteTransferOrderForm() throws Exception {
		completeTransferOrderForm();
	}

	@Test(dependsOnMethods = "verifyCompleteTransferOrderForm")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify user can relese")
	public void verifyReleseTransferOrder() throws Exception {
		releseTransferOrder();
	}

	@Test(dependsOnMethods = "verifyReleseTransferOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify outbound shipment")
	public void verifyOutboundShipment() throws Exception {
		outboundShipment();
	}

	@Test(dependsOnMethods = "verifyOutboundShipment")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify inbound shipment")
	public void verifyInboundShipment() throws Exception {
		inboundShipment();
	}

	@Test(dependsOnMethods = "verifyInboundShipment")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify stock trnsfered to the destination")
	public void verifyCheckWarehouseForProdctAvailabilityTransferOrder() throws Exception {
		checkWarehouseForProdctAvailabilityTransferOrder();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
