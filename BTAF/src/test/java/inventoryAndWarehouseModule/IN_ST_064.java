package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression3;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_ST_064 extends InventoryAndWarehouseModuleRegression3 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Stock Take",
				"Verify that warehouse code and description display in stock take and stock adjustment form",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Configure warehouse code and description")
	@Step("Login to the Entution")
	public void verifyConfigureWarehouseCodeAndDescription_IN_ST_064() throws Exception {
		configureWarehouseCodeAndDescription_IN_ST_064();
	}

	@Test(dependsOnMethods = "verifyConfigureWarehouseCodeAndDescription_IN_ST_064")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check Stock Take with warehouse code and description")
	public void verifyStockTake_IN_ST_064() throws Exception {
		stockTake_IN_ST_064();
	}
	
	@Test(dependsOnMethods = "verifyStockTake_IN_ST_064")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check Stock Adjustment with warehouse code and description")
	public void verifyStockAdjustment_IN_ST_064() throws Exception {
		stockAdjustment_IN_ST_064();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}

}
