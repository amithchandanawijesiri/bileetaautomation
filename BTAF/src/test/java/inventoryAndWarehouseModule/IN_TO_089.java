package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_089 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that user can release the trasnfer order if validate stock disabled for the Trasnfer order form",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Journey configuration")
	@Step("Login to the Entution")
	public void verifyConfigureManualReservationAndValidateStockDisabled_IN_TO_089() throws Exception {
		configureManualReservationAndValidateStockDisabled_IN_TO_089();
	}

	@Test(dependsOnMethods = "verifyConfigureManualReservationAndValidateStockDisabled_IN_TO_089")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create new warehouse")
	public void verifyCreateNewWarehouse_IN_TO_089() throws Exception {
		createNewWarehouse_IN_TO_089();
	}
	
	@Test(dependsOnMethods = "verifyCreateNewWarehouse_IN_TO_089")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Stock Adjustment")
	public void verifyStockAdjustment_IN_TO_089() throws Exception {
		stockAdjustment_IN_TO_089();
	}

	@Test(dependsOnMethods = "verifyStockAdjustment_IN_TO_089")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Transfer Order 01")
	public void verifyTransferOrder_01_IN_TO_089() throws Exception {
		transferOrder_01_IN_TO_089();
	}
	
	@Test(dependsOnMethods = "verifyTransferOrder_01_IN_TO_089")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Transfer Order 02")
	public void verifyTransferOrder_02_IN_TO_089() throws Exception {
		transferOrder_02_IN_TO_089();
	}
	
	@Test(dependsOnMethods = "verifyTransferOrder_02_IN_TO_089")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Outbound Shipment of first Tarnsfer Order")
	public void verifyOutboundShipmentOfFirstTransferOrder_IN_TO_089() throws Exception {
		outboundShipmentOfFirstTransferOrder_IN_TO_089();
	}
	
	@Test(dependsOnMethods = "verifyOutboundShipmentOfFirstTransferOrder_IN_TO_089")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Outbound Shipment of second Tarnsfer Order")
	public void verifyOutboundShipmentSecondTransferOrder_IN_TO_089() throws Exception {
		outboundShipmentSecondTransferOrder_IN_TO_089();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
