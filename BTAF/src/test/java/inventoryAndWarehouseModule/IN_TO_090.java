package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_090 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that Allocation not allowed products can only be reserved through capturing serial/batch",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create allocation not allow product")
	@Step("Login to the Entution")
	public void verifyCreateAllocationNotAllowedProduct_IN_TO_090() throws Exception {
		createAllocationNotAllowedProduct_IN_TO_090();
	}

	@Test(dependsOnMethods = "verifyCreateAllocationNotAllowedProduct_IN_TO_090")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete stock adjustment")
	public void verifyStockAdjustment_IN_TO_090() throws Exception {
		stockAdjustment_IN_TO_090();
	}

	@Test(dependsOnMethods = "verifyStockAdjustment_IN_TO_090")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Set automatic reservation")
	public void verifySetAutomaticReservation_IN_TO_090() throws Exception {
		setAutomaticReservation_IN_TO_090();
	}

	@Test(dependsOnMethods = "verifySetAutomaticReservation_IN_TO_090")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Transfer Order part")
	public void verifyTransferOrder_IN_TO_090() throws Exception {
		transferOrder_IN_TO_090();
	}

	@Test(dependsOnMethods = "verifyTransferOrder_IN_TO_090")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check allocation manager")
	public void verifyCheckAllocationManager_IN_TO_090() throws Exception {
		checkAllocationManager_IN_TO_090();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
