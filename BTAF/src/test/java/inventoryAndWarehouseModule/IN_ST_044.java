package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression3;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_ST_044 extends InventoryAndWarehouseModuleRegression3 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Stock Take",
				"Verify that user can delete the drafted stock adjustment",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Stock Take")
	@Step("Login to the Entution")
	public void verifyCompleteStockTake_IN_ST_044() throws Exception {
		stockTake_IN_ST_044();
	}

	@Test(dependsOnMethods = "verifyCompleteStockTake_IN_ST_044")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft Stock Adjustment")
	public void verifyDrfatStockAdjustment_IN_ST_044() throws Exception {
		drfatStockAdjustment_IN_ST_044();
	}
	
	@Test(dependsOnMethods = "verifyDrfatStockAdjustment_IN_ST_044")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Delete Stock Adjustment")
	public void verifyDeleteStockAdjustment_IN_ST_044() throws Exception {
		deleteStockAdjustment_IN_ST_044();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}

}
