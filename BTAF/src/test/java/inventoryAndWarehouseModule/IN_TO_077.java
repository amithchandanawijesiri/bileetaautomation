package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_077 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that product code and description display in product grid of Trasnfer order / Outbound shipment/ Inbound shipment",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Set product code and description")
	@Step("Login to the Entution")
	public void verifyConfigureProdctCodeAndDescription_IN_TO_077() throws Exception {
		configureProdctCodeAndDescription_IN_TO_077();
	}

	@Test(dependsOnMethods = "verifyConfigureProdctCodeAndDescription_IN_TO_077")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete transfer order journey")
	public void verifyCompleteTransferOrderJourney_IN_TO_077() throws Exception {
		completeTransferOrderJourney_IN_TO_077();
	}

	@Test(dependsOnMethods = "verifyCompleteTransferOrderJourney_IN_TO_077")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check code and description")
	public void verifyAndCheckCodeAndDescriptionTransferOrderJourney_IN_TO_077() throws Exception {
		checkCodeAndDescriptionTransferOrderJourney_IN_TO_077();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		resetToProductCodeAndDescription();
		common.totalTime(startTime);
		common.driverClose();

	}

}
