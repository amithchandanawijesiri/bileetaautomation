package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_018 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that user allow to create new transfer order while updating Transfer order",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = "inventoryAndWarehouseModule.AddQuantityGBV_01.verifyAddQuantityThroughStockAdjustment")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft, Edit and Update & New Transfer Order")
	@Step("Login to the Entution")
	public void verifyDraftEditAndUpdateAndNewTransferOrder01_IN_TO_018() throws Exception {
		draftEditAndUpdateAndNewTransferOrder01_IN_TO_018();
	}

	@Test(dependsOnMethods = "verifyDraftEditAndUpdateAndNewTransferOrder01_IN_TO_018")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft, Edit and Update Transfer Order")
	public void verifyDraftEditAndUpdateAndNewTransferOrder02_IN_TO_018() throws Exception {
		draftEditAndUpdateAndNewTransferOrder02_IN_TO_018();
	}

	@Test(dependsOnMethods = "verifyDraftEditAndUpdateAndNewTransferOrder02_IN_TO_018")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order journey with which comes from Update & New")
	public void verifyCompleteJourneyTransferOrderWhichComesFromUpdateAndNew_IN_TO_018() throws Exception {
		completeJourneyTransferOrderWhichComesFromUpdateAndNew_IN_TO_018();
	}

	@Test(dependsOnMethods = "verifyCompleteJourneyTransferOrderWhichComesFromUpdateAndNew_IN_TO_018")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order journey with updated Transfer Order")
	public void verifySearchUpdatedTransferOrderAndContinueJourney_IN_TO_018() throws Exception {
		searchUpdatedTransferOrderAndContinueJourney_IN_TO_018();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
