package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression3;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_ST_048 extends InventoryAndWarehouseModuleRegression3 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Stock Take",
				"Verify that user can navigated to New Stock Adjustment while drafting one Stock Adjustment and complete the both Stock Adjustments",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Stock Take")
	@Step("Login to the Entution")
	public void verifyStockTake_IN_ST_048() throws Exception {
		stockTake_IN_ST_048();
	}

	@Test(dependsOnMethods = "verifyStockTake_IN_ST_048")
	@Severity(SeverityLevel.CRITICAL)
	@Description("First Stock Adjustment - Part 01")
	public void verifyFirstStockAdjustmentPart01_IN_ST_048() throws Exception {
		firstStockAdjustmentPart01_IN_ST_048();
	}
	
	@Test(dependsOnMethods = "verifyFirstStockAdjustmentPart01_IN_ST_048")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Find recently drafted Stock Adjustment")
	public void verifyFindRecentlyDraftedStockAdjustment_IN_ST_048() throws Exception {
		findRecentlyDraftedStockAdjustment_IN_ST_048();
	}
	
	@Test(dependsOnMethods = "verifyFindRecentlyDraftedStockAdjustment_IN_ST_048")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete second Stock Adjustment")
	public void verifySecondStockAdjustment_IN_ST_048() throws Exception {
		secondStockAdjustment_IN_ST_048();
	}
	
	@Test(dependsOnMethods = "verifySecondStockAdjustment_IN_ST_048")
	@Severity(SeverityLevel.CRITICAL)
	@Description("First Stock Adjustment - Part 02")
	public void verifyFirstStockAdjustmentPart02_IN_ST_048() throws Exception {
		firstStockAdjustmentPart02_IN_ST_048();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}

}
