package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_IO_012 extends InventoryAndWarehouseModule{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Internel Order",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"inventoryAndWarehouseModule.AddQuantity_01.verifyAddQuantityThroughStockAdjustment"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to New Internel Order")
	@Step("Login to the Entution")
	public void verifyNavigateToNewInternelOrder() throws Exception {
		navigateToNewInternelOrder();

	}

	@Test(dependsOnMethods = "verifyNavigateToNewInternelOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Filling the New Internel Order")
	public void verifyFillInternelOrder() throws Exception {
		fillInternelOrder();

	}
	
	@Test(dependsOnMethods = "verifyFillInternelOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft Internel Order")
	public void verifyDraftInterelOrderRegression_IN_IO_012() throws Exception {
		draftInterelOrderRegression_IN_IO_012();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
