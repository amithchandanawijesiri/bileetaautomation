package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class AV_IW_RackTransfer extends InventoryAndWarehouseModule{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Action Verification - Rack Transfer",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Rack Transfer by page")
	@Step("Login, Navigate to Inventory And Warehouse Module")
	public void verifyNavigateToNewRackTransferActionVerification() throws Exception {
		navigateToNewRackTransferActionVerification();
	}

	@Test(dependsOnMethods = "verifyNavigateToNewRackTransferActionVerification")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill Rack Transfer Form")
	public void verifyFillRackTransferActionVerification() throws Exception {
		fillRackTransferActionVerification();
	}
	
	@Test(dependsOnMethods = "verifyFillRackTransferActionVerification")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Action Verification")
	public void verifyActionVerificationRackTransfer() throws Exception {
		actionVerificationRackTransfer();
	}
	
	@Test(dependsOnMethods = "verifyActionVerificationRackTransfer")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Action Section Verification")
	public void verifyActionSectionVerificationRackTransfer() throws Exception {
		actionSectionVerificationRackTransfer();
	}
	
	@Test(dependsOnMethods = "verifyActionSectionVerificationRackTransfer")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Other Action Verification")
	public void verifyOtherActionVerificationRackTransfer() throws Exception {
		otherActionVerificationRackTransfer();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
