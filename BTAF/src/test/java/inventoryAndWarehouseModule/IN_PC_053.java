package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression4;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_PC_053 extends InventoryAndWarehouseModuleRegression4 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Product Conversion",
				"Verify that user can be able to convert the serial/Batches which transferred via Interdepartmental Transfer Order", this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Interdepartmental Transfer Order")
	public void verifyInterdepartmentalTransferOrder_IN_PC_053() throws Exception {
		interdepartmentalTransferOrder_IN_PC_053();
	}
	
	@Test(dependsOnMethods = "verifyInterdepartmentalTransferOrder_IN_PC_053")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Outbound Shipment")
	public void verifyOutboundShipmentInterdepartmentalTransferOrder_IN_PC_053() throws Exception {
		outboundShipmentInterdepartmentalTransferOrder_IN_PC_053();
	}
	
	@Test(dependsOnMethods = "verifyOutboundShipmentInterdepartmentalTransferOrder_IN_PC_053")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Inbound Shipment")
	public void verifyInboundShipmentInterdepartmentalTransferOrder_IN_PC_053() throws Exception {
		inboundShipmentInterdepartmentalTransferOrder_IN_PC_053();
	}
	
	@Test(dependsOnMethods = "verifyInboundShipmentInterdepartmentalTransferOrder_IN_PC_053")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Product Conversion")
	public void verifyProductConversion_IN_PC_053() throws Exception {
		productConversion_IN_PC_053();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
