package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_IO_040 extends InventoryAndWarehouseModuleRegression {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "", this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"inventoryAndWarehouseModule.AddQuantity_02.verifyAddQuantityThroughStockAdjustment"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Journey Configuration")
	@Step("Login to the Entution")
	public void verifyJourneyConfigureFor_IN_IO_040() throws Exception {
		journeyConfigureFor_IN_IO_040();

	}

	@Test(dependsOnMethods = "verifyJourneyConfigureFor_IN_IO_040")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internel Order")
	public void verifyStockAdjustmentForNewWarehouse_IN_IO_040() throws Exception {
		stockAdjustmentForNewWarehouse_IN_IO_040();
	}

	@Test(dependsOnMethods = "verifyStockAdjustmentForNewWarehouse_IN_IO_040")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internel Order For Reservation")
	public void verifyInternelOrder_IN_IO_040() throws Exception {
		internelOrder_IN_IO_040();
	}

	@Test(dependsOnMethods = "verifyInternelOrder_IN_IO_040")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify Minus Stock Adjustment")
	public void verifyMinusStockAdjustment_IN_IO_040() throws Exception {
		minusStockAdjustment_IN_IO_040();
	}

	@Test(dependsOnMethods = "verifyMinusStockAdjustment_IN_IO_040")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify Outbound Shipment via Sales Order")
	public void verifySalesOrderOutboundShipment_IN_IO_040() throws Exception {
		salesOrderOutboundShipment_IN_IO_040();
	}

	@Test(dependsOnMethods = "verifySalesOrderOutboundShipment_IN_IO_040")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify Product Conversion")
	public void verifyProductConversion_IN_IO_040() throws Exception {
		productConversion_IN_IO_040();
	}

	@Test(dependsOnMethods = "verifyProductConversion_IN_IO_040")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify Internal Order with reserved quantity")
	public void verifyInternelOrderCheckWithReservedQuantity_IN_IO_040() throws Exception {
		internelOrderCheckWithReservedQuantity_IN_IO_040();
	}

	@Test(dependsOnMethods = "verifyInternelOrderCheckWithReservedQuantity_IN_IO_040")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify Internal Diaspatch Order with reserved quantity")
	public void verifyInternelDispatchOrderCheckWithReservedQuantity_IN_IO_040() throws Exception {
		internelDispatchOrderCheckWithReservedQuantity_IN_IO_040();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
