package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_081_082 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that user cannot do transfer order journey when inactivate the journey in journey configuration",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Inactive journey")
	@Step("Login to the Entution")
	public void verifyInactiveTransferOrderJouerney_IN_TO_081_082() throws Exception {
		inactiveTransferOrderJouerney_IN_TO_081_082();
	}

	@Test(dependsOnMethods = "verifyInactiveTransferOrderJouerney_IN_TO_081_082")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check journey inactivation")
	public void verifyAndCheckTransferOrderJourneyInactivation_IN_TO_081_082() throws Exception {
		checkTransferOrderJourneyInactivation_IN_TO_081_082();
	}

	@Test(dependsOnMethods = "verifyAndCheckTransferOrderJourneyInactivation_IN_TO_081_082")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Active transfer order journey")
	public void verifyActiveTransferOrderJouerney_IN_TO_081_082() throws Exception {
		activeTransferOrderJouerney_IN_TO_081_082();
	}

	@Test(dependsOnMethods = "verifyActiveTransferOrderJouerney_IN_TO_081_082")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check journey activation")
	public void verifyAndcheckTransferOrderJourneyActivation_IN_TO_081_082() throws Exception {
		checkTransferOrderJourneyActivation_IN_TO_081_082();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
