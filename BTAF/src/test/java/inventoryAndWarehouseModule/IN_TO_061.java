package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_061 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that Close function will be working accordingly", this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = { "inventoryAndWarehouseModule.AddQuantityGBV_01.verifyAddQuantityThroughStockAdjustment"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order journey for check close function")
	@Step("Login to the Entution")
	public void verifyCloseFunctionCheckWithTransferOrderJourney_IN_TO_061() throws Exception {
		closeFunctionCheckWithTransferOrderJourney_IN_TO_061();
	}

	@Test(dependsOnMethods = "verifyCloseFunctionCheckWithTransferOrderJourney_IN_TO_061")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify stock allocation availability")
	public void verifyThatAllocationAvailability_IN_TO_061() throws Exception {
		verifyAllocationAvailability_IN_TO_061();
	}

	@Test(dependsOnMethods = "verifyThatAllocationAvailability_IN_TO_061")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check Task/Events and Allocation Manager")
	public void verifyAndCheckTaskEventAndAllocationAfterColsedTheDocument_IN_TO_061() throws Exception {
		checkTaskEventAndAllocationAfterColsedTheDocument_IN_TO_061();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		setManuelStockReservationTransferOrder();
		common.totalTime(startTime);
		common.driverClose();

	}

}
