package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})

public class IW_TC_015 extends InventoryAndWarehouseModule{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Verify that converting products in to sales invoice which Outbound via Outbound Loan Order [partial Qty]",this.getClass().getSimpleName());
	}
	
	
	@Test(dependsOnMethods = "inventoryAndWarehouseModule.IW_TC_014.verifyCheckWarehouseForProdctAvailabilityConvertToInboundShipmentOutboundLoanOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Sales Invoice")
	@Step("Login, Navigate to inventory and warehousing, Find relevent Outbound Loan Order")
	public void verifynNavigateToSalesInvoiceAndConvertToSalesInvoice() throws Exception {
		navigateToSalesInvoiceAndConvertToSalesInvoice();
	}

	@Test(dependsOnMethods = "verifynNavigateToSalesInvoiceAndConvertToSalesInvoice")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Checkout, Draft and Release Sales Invoice")
	public void verifyCehckoutDraftReleaseSalesInvoice() throws Exception {
		cehckoutDraftReleaseSalesInvoice();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}
}
