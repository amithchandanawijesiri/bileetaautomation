package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_046 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that user allow to complete the trasnfer order with single outbound shipment and multiple inbound shipments",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"inventoryAndWarehouseModule.AddQuantityGBV_02.verifyAddQuantityThroughStockAdjustment"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create new warehouse")
	@Step("Login to the Entution")
	public void verifyCreateNewWarehouse_IN_TO_046() throws Exception {
		createNewWarehouse_IN_TO_046();
	}

	@Test(dependsOnMethods = "verifyCreateNewWarehouse_IN_TO_046")
	@Severity(SeverityLevel.CRITICAL)
	@Description("First Stock Adjustment")
	public void verifyStockAdjustment01_IN_TO_046() throws Exception {
		stockAdjustment01_IN_TO_046();
	}

	@Test(dependsOnMethods = "verifyStockAdjustment01_IN_TO_046")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Second Stock Adjustment")
	public void verifyStockAdjustment02_IN_TO_046() throws Exception {
		stockAdjustment02_IN_TO_046();
	}

	@Test(dependsOnMethods = "verifyStockAdjustment02_IN_TO_046")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order part")
	public void verifyTransferOrder_IN_TO_046() throws Exception {
		transferOrder_IN_TO_046();
	}

	@Test(dependsOnMethods = "verifyTransferOrder_IN_TO_046")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Outbound Shipment part")
	public void verifyOutboundShipment_IN_TO_046() throws Exception {
		outboundShipment_IN_TO_046();
	}

	@Test(dependsOnMethods = "verifyOutboundShipment_IN_TO_046")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Inbound Shipment part")
	public void verifyInboundShipment_IN_TO_046() throws Exception {
		inboundShipment_IN_TO_046();
	}

	@Test(dependsOnMethods = "verifyOutboundShipment_IN_TO_046")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check stock updatance")
	public void verifyAndCheckStockUpdateAccordingly_IN_TO_046() throws Exception {
		checkStockUpdateAccordingly_IN_TO_046();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
