package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_IO_075 extends InventoryAndWarehouseModuleRegression{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"inventoryAndWarehouseModule.AddQuantity_03.verifyAddQuantityThroughStockAdjustment"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Stock Take")
	@Step("Login to the Entution")
	public void verifyStockTake_IN_IO_075() throws Exception {
		stockTake_IN_IO_075();
	}
	
	@Test(dependsOnMethods = "verifyStockTake_IN_IO_075")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Plus Stock Adjustment")
	public void verifyStockAdjustment_IN_IO_075() throws Exception {
		stockAdjustment_IN_IO_075();
	}
	
	@Test(dependsOnMethods = "verifyStockAdjustment_IN_IO_075")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internel Order with Plus Stock Adjustment")
	public void verifyInternelOrder_IN_IO_075() throws Exception {
		internelOrder_IN_IO_075();
	}
	
	@Test(dependsOnMethods = "verifyInternelOrder_IN_IO_075")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internel Dispatch Order")
	public void verifyInternelDispatchOrder_075() throws Exception {
		internelDispatchOrder_075();
	}
	
	@Test(dependsOnMethods = "verifyInternelDispatchOrder_075")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify stock deduction")
	public void verifyAndComapreTheStockThatTranferedFromTheWarehouse_IN_IO_075() throws Exception {
		comapreTheStockThatTranferedFromTheWarehouse_IN_IO_075();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
//		common.driverClose();
	}
}
