package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_IO_014 extends InventoryAndWarehouseModule{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Internel Order",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"inventoryAndWarehouseModule.AddQuantity_01.verifyAddQuantityThroughStockAdjustment","inventoryAndWarehouseModule.IN_IO_012.verifyDraftInterelOrderRegression_IN_IO_012"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Copy from process")
	@Step("Login to the Entution")
	public void verifyCopyFromProcessInternelOrderReg1() throws Exception {
		verifyCopyFromProcessInternelOrder();

	}
	
	@Test(dependsOnMethods = "verifyCopyFromProcessInternelOrderReg1")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Do any changes and draft Internel Order")
	public void verifyChangeesAndDraftInternelOrder() throws Exception {
		changeesAndDraftInternelOrder();

	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
