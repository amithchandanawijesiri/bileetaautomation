package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_066 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that returned serials/Batches via Internal Return , can be transfer from one warehouse to another",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create new warehouse")
	@Step("Login to the Entution")
	public void verifyCreateNewWarehouse_IN_TO_066() throws Exception {
		createNewWarehouse_IN_TO_066();
	}

	@Test(dependsOnMethods = "verifyCreateNewWarehouse_IN_TO_066")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Stock Adjustment")
	public void verifyStockAdjustment_IN_TO_066() throws Exception {
		stockAdjustment_IN_TO_066();
	}

	@Test(dependsOnMethods = "verifyStockAdjustment_IN_TO_066")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Internal Order")
	public void verifyInternelOrder_IN_TO_066() throws Exception {
		internelOrder_IN_TO_066();
	}

	@Test(dependsOnMethods = "verifyInternelOrder_IN_TO_066")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Internal Dispatch Order")
	public void verifyInternelDispatchOrder_IN_TO_066() throws Exception {
		internelDispatchOrder_IN_TO_066();
	}

	@Test(dependsOnMethods = "verifyInternelDispatchOrder_IN_TO_066")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Internal Return Order")
	public void verifyInternalReturnOrder_IN_TO_066() throws Exception {
		internalReturnOrder_IN_TO_066();
	}

	@Test(dependsOnMethods = "verifyInternalReturnOrder_IN_TO_066")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Internal Recipt")
	public void verifyInternelRecipt_IN_TO_066() throws Exception {
		internelRecipt_IN_TO_066();
	}

	@Test(dependsOnMethods = "verifyInternelRecipt_IN_TO_066")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Transfer Order")
	public void verifyTransferOrder_IN_TO_066() throws Exception {
		transferOrder_IN_TO_066();
	}

	@Test(dependsOnMethods = "verifyTransferOrder_IN_TO_066")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Outbound Shipment")
	public void verifyOutboundShipmen_IN_TO_066() throws Exception {
		outboundShipmen_IN_TO_066();
	}

	@Test(dependsOnMethods = "verifyOutboundShipmen_IN_TO_066")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Inbound Shipment")
	public void verifInboundShipment_IN_TO_066() throws Exception {
		inboundShipment_IN_TO_066();
	}
	
	@Test(dependsOnMethods = "verifInboundShipment_IN_TO_066")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check stock updatance")
	public void verifyAndCheckStockUpdatance_066() throws Exception {
		checkStockUpdatance_066();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
