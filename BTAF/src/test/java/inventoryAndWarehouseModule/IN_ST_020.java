package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression3;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_ST_020 extends InventoryAndWarehouseModuleRegression3 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Stock Take",
				"Verify that user can use update and new",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft first Stock Take")
	@Step("Login to the Entution")
	public void verifyDraftStockTake_IN_ST_020() throws Exception {
		draftStockTake_IN_ST_020();
	}
	
	@Test(dependsOnMethods = "verifyDraftStockTake_IN_ST_020")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Update and navigate to New Stock Take")
	public void verifyUpdateAndNew_IN_ST_020() throws Exception {
		updateAndNew_IN_ST_020();
	}
	
	@Test(dependsOnMethods = "verifyUpdateAndNew_IN_ST_020")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify updatance of drafted Stock Take")
	public void verifyAndCheckUpdatanceOnFirstStockTake_IN_ST_020() throws Exception {
		checkUpdatanceOnFirstStockTake_IN_ST_020();
	}
	
	@Test(dependsOnMethods = "verifyAndCheckUpdatanceOnFirstStockTake_IN_ST_020")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft and Release second Stock Take")
	public void verifyDraftReleseSecondStockTake_IN_ST_020() throws Exception {
		draftReleseSecondStockTake_IN_ST_020();
	}
	
	@Test(dependsOnMethods = "verifyDraftReleseSecondStockTake_IN_ST_020")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navifgate to early updated Stock Take and Release")
	public void verifyNavigateSerchedStockTakeAndRelease_IN_ST_020() throws Exception {
		navigateSerchedStockTakeAndRelease_IN_ST_020();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}

}
