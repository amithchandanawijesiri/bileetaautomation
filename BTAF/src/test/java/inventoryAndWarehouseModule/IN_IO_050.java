package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_IO_050 extends InventoryAndWarehouseModuleRegression{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"inventoryAndWarehouseModule.AddQuantity_02.verifyAddQuantityThroughStockAdjustment"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create Batch Product")
	@Step("Login to the Entution")
	public void verifyCreateBatchFifoProduct_IN_IO_050() throws Exception {
		createBatchFifoProduct_IN_IO_050();
	}
	
	@Test(dependsOnMethods = "verifyCreateBatchFifoProduct_IN_IO_050")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internel Order")
	public void verifyFilInternelOrder_IN_IO_050() throws Exception {
		filInternelOrder_IN_IO_050();
	}
	
	@Test(dependsOnMethods = "verifyFilInternelOrder_IN_IO_050")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internel Order")
	public void verifyFilInternelOrder_IN_IO_050_2() throws Exception {
		filInternelOrder_IN_IO_050_2();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
