package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_085 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that user can auto generate Inbound Shipment and Outbound Shipment and stock update accordingly",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Configure auto genarate Outbound Shipment and Inbound Shipment")
	@Step("Login to the Entution")
	public void verifyAndConfigureAutoGenarateInboundShipmentAndOutboundShipment_IN_TO_085() throws Exception {
		configureAutoGenarateInboundShipmentAndOutboundShipment_IN_TO_085();
	}

	@Test(dependsOnMethods = "verifyAndConfigureAutoGenarateInboundShipmentAndOutboundShipment_IN_TO_085")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft Transfer Order")
	public void verifyDraftTransferOrder_IN_TO_085() throws Exception {
		draftTransferOrder_IN_TO_085();
	}
	
	@Test(dependsOnMethods = "verifyDraftTransferOrder_IN_TO_085")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Seriel batch capture and release Transfer Order")
	public void verifySerielBatchCaptureAndReleaseTransferOrder_IN_TO_085() throws Exception {
		serielBatchCaptureAndReleaseTransferOrder_IN_TO_085();
	}

	@Test(dependsOnMethods = "verifySerielBatchCaptureAndReleaseTransferOrder_IN_TO_085")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Serie/Batch capture and release Transefer Order")
	public void verifyAndCheckAutoGenaratedInbounsShipmentAndOutboundShipmentAvailability_IN_TO_085() throws Exception {
		checkAutoGenaratedInbounsShipmentAndOutboundShipmentAvailability_IN_TO_085();
	}
	
	@Test(dependsOnMethods = "verifyAndCheckAutoGenaratedInbounsShipmentAndOutboundShipmentAvailability_IN_TO_085")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check stock updatance")
	public void verifyAndCheckStockUpdateAccordingly_IN_TO_085() throws Exception {
		checkStockUpdateAccordingly_IN_TO_085();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		uncheckAutoGenarateOutboundShipmentAndInboundShipment_IN_TO_085();
		common.totalTime(startTime);
		common.driverClose();

	}

}
