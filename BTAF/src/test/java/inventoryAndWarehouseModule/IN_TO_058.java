package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_058 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that user allow to add decimal values if product change to Decimal allow product",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {
			"inventoryAndWarehouseModule.AddQuantityGBV_02.verifyAddQuantityThroughStockAdjustment",
			"inventoryAndWarehouseModule.IN_TO_057.verfyAndCheckUserDoesNotAbleAddDecimalValue_IN_TO_057" })
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create decimal not allow product")
	@Step("Login to the Entution")
	public void verheckUserDoesNotAbleAddDecimalValue_IN_TO_058() throws Exception {
		checkUserDoesNotAbleAddDecimalValue_IN_TO_058();
	}

	@Test(dependsOnMethods = "verheckUserDoesNotAbleAddDecimalValue_IN_TO_058")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Change to decimal allow and check with decimal quantity")
	public void verifyAndCheckUserCanAddDecimalValueAfterEnableDecimalAllow_IN_TO_058() throws Exception {
		checkUserCanAddDecimalValueAfterEnableDecimalAllow_IN_TO_058();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
