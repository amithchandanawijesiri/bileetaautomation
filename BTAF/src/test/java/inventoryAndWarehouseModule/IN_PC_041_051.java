package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression4;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_PC_041_051 extends InventoryAndWarehouseModuleRegression4 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Product Conversion",
				"Verify that user cannot reverse the released product conversion, if user user the stock for any transaction AND Verify that user can be able to convert Serial/Batches which converted via Product Conversion", this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Stock Adjusyment for new warehouse")
	public void verifyStockAdjustmentForNewWarehouse_IN_PC_041_051() throws Exception {
		stockAdjustmentForNewWarehouse_IN_PC_041_051();
	}
	
	@Test(dependsOnMethods = "verifyStockAdjustmentForNewWarehouse_IN_PC_041_051")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Product Conversion")
	public void verifyProductConversion_IN_PC_041_051() throws Exception {
		productConversion_IN_PC_041_051();
	}
	
	@Test(dependsOnMethods = "verifyProductConversion_IN_PC_041_051")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Product Conversion with converted serials")
	public void verifyProductConversionWithConvertedSeriels_IN_PC_041_051() throws Exception {
		productConversionWithConvertedSeriels_IN_PC_041_051();
	}

	@Test(dependsOnMethods = "verifyProductConversionWithConvertedSeriels_IN_PC_041_051")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Reverse Product Conversion")
	public void verifyReverseUsedProductConversion_IN_PC_041_051() throws Exception {
		reverseUsedProductConversion_IN_PC_041_051();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
