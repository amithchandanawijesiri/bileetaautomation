package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleSmoke;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})


public class Smoke_Inventory_0007 extends InventoryAndWarehouseModuleSmoke{
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Verify that user can be able to Create Assembly Order",
				this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Assembly Order Form")
	@Step("Login, Navigate to Inventory and Warehousing")
	public void verifyNavigateToAssemblyOrderForm() throws Exception {
		navigateToAssemblyOrderForm();
	}
	
	
	@Test(dependsOnMethods = "verifyNavigateToAssemblyOrderForm")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill Assembly Order")
	public void verifyfFillAsemblyOrderForm() throws Exception {
		fillAsemblyOrderForm();
	}
	
	@Test(dependsOnMethods = "verifyfFillAsemblyOrderForm")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill prduct tab and estimation details tab")
	public void verifyProductTabAndEstimation() throws Exception {
		productTabAndEstimation();
	}
	
	@Test(dependsOnMethods = "verifyProductTabAndEstimation")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft and Releese 'Assembly Order'")
	public void verifyDraftAndReleeseAO() throws Exception {
		draftAndReleeseAO();
	}
	
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}
}
