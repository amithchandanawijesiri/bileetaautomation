package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression4;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_PC_016 extends InventoryAndWarehouseModuleRegression4 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Product Conversion",
				"Verify that user can add decimal value as Qty for \"Allow Decimal enabled product\"", this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create decimal allow Lot Product")
	@Step("Login to the Entution")
	public void verifyCreateDecimalAllowLotProduct_IN_PC_016() throws Exception {
		createDecimalAllowLotProduct_IN_PC_016();
	}
	
	@Test(dependsOnMethods = {"verifyCreateDecimalAllowLotProduct_IN_PC_016"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create decimal allow Batch Specific Product")
	public void verifyCreateBatchSpecificProductAllowDecimal_IN_PC_016() throws Exception {
		createBatchSpecificProductAllowDecimal_IN_PC_016();
	}
	
	@Test(dependsOnMethods = {"verifyCreateBatchSpecificProductAllowDecimal_IN_PC_016"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check Product Converion products with decimal qty")
	public void verifyAndCheckDecimalQuantityWithDecimalAllowProduct_IN_PC_016() throws Exception {
		checkDecimalQuantityWithDecimalAllowProduct_IN_PC_016();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
