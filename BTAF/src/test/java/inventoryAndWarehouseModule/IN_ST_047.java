package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression3;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_ST_047 extends InventoryAndWarehouseModuleRegression3 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Stock Take",
				"Verify that user cannot reverse the released stock adjustment, if use the stock for any transactions",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Stock Take")
	@Step("Login to the Entution")
	public void verifyStockTake_IN_ST_47() throws Exception {
		stockTake_IN_ST_47();
	}

	@Test(dependsOnMethods = "verifyStockTake_IN_ST_47")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete converted Stock Adjustment")
	public void verifyStockAdjustment_IN_ST_047() throws Exception {
		stockAdjustment_IN_ST_047();
	}
	
	@Test(dependsOnMethods = "verifyStockAdjustment_IN_ST_047")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Sales Order")
	public void verifySalesOrder_IN_ST_047() throws Exception {
		salesOrder_IN_ST_047();
	}
	
	@Test(dependsOnMethods = "verifySalesOrder_IN_ST_047")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Outbound Shipment")
	public void verifyOutboundShipment_IN_ST_047() throws Exception {
		outboundShipment_IN_ST_047();
	}
	
	@Test(dependsOnMethods = "verifyOutboundShipment_IN_ST_047")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Purchase Invoice")
	public void verifyPurchaseInvoice_IN_ST_047() throws Exception {
		purchaseInvoice_IN_ST_047();
	}
	
	@Test(dependsOnMethods = "verifyPurchaseInvoice_IN_ST_047")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Try to reverse Stock Adjustement")
	public void verifyUserCanReverseStockAdjustment_IN_ST_047() throws Exception {
		tryToReverseStockAdjustment_IN_ST_047();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}

}
