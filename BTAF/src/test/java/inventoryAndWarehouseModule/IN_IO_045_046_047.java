package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_IO_045_046_047 extends InventoryAndWarehouseModuleRegression{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"inventoryAndWarehouseModule.AddQuantity_02.verifyAddQuantityThroughStockAdjustment"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Journey Configuration")
	@Step("Login to the Entution")
	public void verifyJourneyConfigureFor_IN_IO_045_046_047() throws Exception {
		journeyConfigureFor_IN_IO_045_046_047();
	}
	
	@Test(dependsOnMethods = "verifyJourneyConfigureFor_IN_IO_045_046_047")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internel Order")
	public void verifyFillInternelOrder_IN_IO_045_046_047() throws Exception {
		fillInternelOrder_IN_IO_045_046_047();
	}
	
	@Test(dependsOnMethods = "verifyFillInternelOrder_IN_IO_045_046_047")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft and Release Internel Dispatch Order")
	public void verifyDraftReleseInternelDispatchOrder_IN_IO_045_046_047() throws Exception {
		draftReleseInternelDispatchOrder_IN_IO_045_046_047();
	}
	
	@Test(dependsOnMethods = "verifyDraftReleseInternelDispatchOrder_IN_IO_045_046_047")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft and Release Internel Dispatch Order")
	public void verifyDraftReleseInternelDispatchOrder_IN_IO_045_046_047_2() throws Exception {
		draftReleseInternelDispatchOrder_IN_IO_045_046_047_2();
	}
	
	@Test(dependsOnMethods = "verifyDraftReleseInternelDispatchOrder_IN_IO_045_046_047_2")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft and Release Internel Dispatch Order")
	public void verifyDraftReleseInternelDispatchOrder_IN_IO_045_046_047_3() throws Exception {
		draftReleseInternelDispatchOrder_IN_IO_045_046_047_3();
	}
	
	@Test(dependsOnMethods = "verifyDraftReleseInternelDispatchOrder_IN_IO_045_046_047_3")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Compare alternative product stock")
	public void verifyAndcomapreTheStockAlternativeProductTranferedFromTheWarehouse_IN_IO_045_046_047() throws Exception {
		comapreTheStockAlternativeProductTranferedFromTheWarehouse_IN_IO_045_046_047();
	}
	
	@Test(dependsOnMethods = "verifyAndcomapreTheStockAlternativeProductTranferedFromTheWarehouse_IN_IO_045_046_047")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Compare main product stock")
	public void verifyAndComapreTheStockMAinProductThatTranferedFromTheWarehouse_IN_IO_045_046_047() throws Exception {
		comapreTheStockMainProductThatTranferedFromTheWarehouse_IN_IO_045_046_047();
	}
	
	@Test(dependsOnMethods = "verifyAndComapreTheStockMAinProductThatTranferedFromTheWarehouse_IN_IO_045_046_047")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify stock for all three products")
	public void verifyAndompareReleventProductOH_IN_IO_045_046_047() throws Exception {
		compareReleventProductOH_IN_IO_045_046_047();
	}
	
	
	@Test(dependsOnMethods = "verifyAndompareReleventProductOH_IN_IO_045_046_047")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify costing for the alternative product")
	public void verifyAndValidateCosting_IN_IO_045_I_046_I_047() throws Exception {
		validateCosting_IN_IO_045_I_046_I_047();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
