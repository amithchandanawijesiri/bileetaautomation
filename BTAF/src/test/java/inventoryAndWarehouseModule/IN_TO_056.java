package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_056 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that user can enter decimal value as Qty if product is enabled allow decimal",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {
			"inventoryAndWarehouseModule.AddQuantityGBV_02.verifyAddQuantityThroughStockAdjustment"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create decimal allow product")
	@Step("Login to the Entution")
	public void verifyCreateDecimalAllowProduct_IN_TO_056() throws Exception {
		createDecimalAllowProduct_IN_TO_056();
	}

	@Test(dependsOnMethods = "verifyCreateDecimalAllowProduct_IN_TO_056")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Stock Adjustment")
	public void verifyStockAdjustmentWithAllowDecimalProduct_IN_TO_056() throws Exception {
		stockAdjustmentWithAllowDecimalProduct_IN_TO_056();
	}

	@Test(dependsOnMethods = "verifyStockAdjustmentWithAllowDecimalProduct_IN_TO_056")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order journey")
	public void verifyTransferOrderJourneyWithDecimalAllowProduct_IN_TO_056() throws Exception {
		transferOrderJourneyWithDecimalAllowProduct_IN_TO_056();
	}

	@Test(dependsOnMethods = "verifyTransferOrderJourneyWithDecimalAllowProduct_IN_TO_056")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check stock updatance")
	public void verifyCheckStockUpdatance_IN_TO_056() throws Exception {
		checkStockUpdatance_IN_TO_056();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
