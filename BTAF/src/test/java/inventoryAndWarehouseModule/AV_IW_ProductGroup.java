package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class AV_IW_ProductGroup extends InventoryAndWarehouseModule {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Action Verification - Product Group Configuration",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Inventory And Warehouse Module")
	@Step("Login to the Entution")
	public void verifyNavigateToInventoryAndWarehouseModuleActionVerification() throws Exception {

		navigateToInventoryAndWarehouseModuleActionVerification();

	}

	@Test(dependsOnMethods = "verifyNavigateToInventoryAndWarehouseModuleActionVerification")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to the new product group page")
	public void verifyNavigateNewProductGroupPageActionVerification() throws Exception {

		navigateNewProductGroupPageActionVerification();

	}

	@Test(dependsOnMethods = "verifyNavigateNewProductGroupPageActionVerification")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Configure new Product Group")
	public void verifyConfigureNewProductGroupActionVerification() throws Exception {

		configureNewProductGroupActionVerification();

	}

	@Test(dependsOnMethods = "verifyConfigureNewProductGroupActionVerification")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Active Inventory")
	public void verifyInventoryPurchaseAndSalesProductGroupConfigActionVerfication() throws Exception {
		inventoryPurchaseAndSalesProductGroupConfigActionVerfication();

	}

	@Test(dependsOnMethods = "verifyInventoryPurchaseAndSalesProductGroupConfigActionVerfication")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Action Verification - Product Group")
	public void verifyActionsProductGoup() throws Exception {

		actionsProductGoup();

	}
	
	@Test(dependsOnMethods = "verifyActionsProductGoup")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Action Section Verification - Product Group")
	public void verifyActionSectionVerification() throws Exception {

		actionSectionVerification();

	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
			common.driverClose();

	}

}
