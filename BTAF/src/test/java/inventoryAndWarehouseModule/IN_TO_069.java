package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_069 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that user can be able to transfer serial/batches which transferred via Interdepartmental Transfer Order",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create new warehouse")
	@Step("Login to the Entution")
	public void verifyCreateNewWarehouse_IN_TO_069() throws Exception {
		createNewWarehouse_IN_TO_069();
	}

	@Test(dependsOnMethods = "verifyCreateNewWarehouse_IN_TO_069")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Interdepartmental Transfer Order")
	public void verifyInterdepartmentalTransferOrder_IN_TO_069() throws Exception {
		interdepartmentalTransferOrder_IN_TO_069();
	}

	@Test(dependsOnMethods = "verifyInterdepartmentalTransferOrder_IN_TO_069")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Outbound Shipment for Interdepartmental Transfer Order")
	public void verifyOutboundShipmentInterdepartmentalTransferOrder_IN_TO_069() throws Exception {
		outboundShipmentInterdepartmentalTransferOrder_IN_TO_069();
	}

	@Test(dependsOnMethods = "verifyOutboundShipmentInterdepartmentalTransferOrder_IN_TO_069")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Inbound Shipment for Interdepartmental Transfer Order")
	public void verifyInboundShipmentInterdepartmentalTransferOrder_IN_TO_069() throws Exception {
		inboundShipmentInterdepartmentalTransferOrder_IN_TO_069();
	}

	@Test(dependsOnMethods = "verifyInboundShipmentInterdepartmentalTransferOrder_IN_TO_069")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order")
	public void verifyTransferOrder_IN_TO_069() throws Exception {
		transferOrder_IN_TO_069();
	}

	@Test(dependsOnMethods = "verifyTransferOrder_IN_TO_069")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Outbound Shipment")
	public void verifyOutboundShipmen_IN_TO_069() throws Exception {
		outboundShipmen_IN_TO_069();
	}

	@Test(dependsOnMethods = "verifyOutboundShipmen_IN_TO_069")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Inbound Shipment")
	public void verifInboundShipment_IN_TO_069() throws Exception {
		inboundShipment_IN_TO_069();
	}
	
	@Test(dependsOnMethods = "verifInboundShipment_IN_TO_069")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check stock updatance")
	public void verifyAndCheckStockUpdatance_069() throws Exception {
		checkStockUpdatance_069();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
