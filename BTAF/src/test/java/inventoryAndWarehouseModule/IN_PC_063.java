package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression4;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_PC_063 extends InventoryAndWarehouseModuleRegression4 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Product Conversion",
				"Verify that user cannot do Product Conversion if the journey is inactivated", this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Inactive Product Conversion journey")
	public void verifyInactiveProductConversionJouerney_IN_PC_063() throws Exception {
		inactiveProductConversionJouerney_IN_PC_063();
	}
	
	@Test(dependsOnMethods = "verifyInactiveProductConversionJouerney_IN_PC_063")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check whether the user cannot do a Product Conversion")
	public void verifyAndCheckProductConversionJourneyInactivation_IN_PC_063() throws Exception {
		checkProductConversionJourneyInactivation_IN_PC_063();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
