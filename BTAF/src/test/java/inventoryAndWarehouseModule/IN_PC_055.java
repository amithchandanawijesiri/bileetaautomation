package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression4;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_PC_055 extends InventoryAndWarehouseModuleRegression4 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Product Conversion",
				"Verify that user can be able to convert the Serial/Batches which onbound via Outbound Loan Order", this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Outbound Loan Order")
	public void verifyOutboundLoanOrder_IN_PC_055() throws Exception {
		outboundLoanOrder_IN_PC_055();
	}
	
	@Test(dependsOnMethods = "verifyOutboundLoanOrder_IN_PC_055")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Outbound Shipment and Inbound Shipment")
	public void verifyOutboundShipmentAndInboundShipment_IN_PC_055() throws Exception {
		outboundShipmentAndInboundShipment_IN_PC_055();
	}
	
	@Test(dependsOnMethods = "verifyOutboundShipmentAndInboundShipment_IN_PC_055")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Product Conversion")
	public void verifyProductConversion_IN_PC_055() throws Exception {
		productConversion_IN_PC_055();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
