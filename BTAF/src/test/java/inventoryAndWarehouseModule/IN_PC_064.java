package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression4;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_PC_064 extends InventoryAndWarehouseModuleRegression4 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Product Conversion",
				"Verify that user can do Product Conversion if the journey is activated", this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Active Product Conversion journey")
	public void verifyActiveProductConversionJouerney_IN_PC_064() throws Exception {
		activeProductConversionJouerney_IN_PC_064();
	}
	
	@Test(dependsOnMethods = "verifyActiveProductConversionJouerney_IN_PC_064")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check whether the user can do a Product Conversion")
	public void verifyAndCheckProductConversionJourneyActivation_IN_PC_064() throws Exception {
		checkProductConversionJourneyActivation_IN_PC_064();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
