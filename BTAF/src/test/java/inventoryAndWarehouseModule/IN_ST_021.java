package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression3;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_ST_021 extends InventoryAndWarehouseModuleRegression3 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Stock Take",
				"Verify that user can do stock take by changing physical balance in to \"0\" ",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Stock Adjustment for new warehouse")
	@Step("Login to the Entution")
	public void verifyStockAdjustmentForNewWarehouse_IN_ST_021() throws Exception {
		stockAdjustmentForNewWarehouse_IN_ST_021();
	}
	
	@Test(dependsOnMethods = "verifyStockAdjustmentForNewWarehouse_IN_ST_021")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft Release stock take with zero quantity")
	public void verifyAndCheckStockTakeWithZeroPhysicleBalance_IN_ST_021() throws Exception {
		checkStockTakeWithZeroPhysicleBalance_IN_ST_021();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}

}
