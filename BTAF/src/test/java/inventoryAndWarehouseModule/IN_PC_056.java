package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression4;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_PC_056 extends InventoryAndWarehouseModuleRegression4 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Product Conversion",
				"Verify that user can be able to convert the  Serials/Batches which having in Racks", this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete warehouse creation")
	public void verifyWarehouseCreationWithRacks_IN_PC_056() throws Exception {
		warehouseCreationWithRacks_IN_PC_056();
	}
	
	@Test(dependsOnMethods = "verifyWarehouseCreationWithRacks_IN_PC_056")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Stock Adjustment for new warehouse")
	public void verifyStockAdjustmentForRack_IN_PC_056() throws Exception {
		stockAdjustmentForRack_IN_PC_056();
	}
	
	@Test(dependsOnMethods = "verifyStockAdjustmentForRack_IN_PC_056")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Rack Transfer")
	public void verifyRackTransfer_IN_PC_056() throws Exception {
		rackTransfer_IN_PC_056();
	}
	
	@Test(dependsOnMethods = "verifyRackTransfer_IN_PC_056")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Product Conversion")
	public void verifyProductConversion_IN_PC_056() throws Exception {
		productConversion_IN_PC_056();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
