package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression4;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_PC_050 extends InventoryAndWarehouseModuleRegression4 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Product Conversion",
				"Verify that user can be able to convert the Serial/Batches which return via Internal Return Order", this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internal Order and Internal Dispatch Order")
	public void verifyInternalOrderAndInternalDispatchOrder_IN_PC_050() throws Exception {
		internalOrderAndInternalDispatchOrder_IN_PC_050();
	}
	
	@Test(dependsOnMethods = "verifyInternalOrderAndInternalDispatchOrder_IN_PC_050")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internal Return Order and Internal Recipt")
	public void verifyInternalRetunOrderAndInternalRecipt_IN_PC_050() throws Exception {
		internalRetunOrderAndInternalRecipt_IN_PC_050();
	}
	
	@Test(dependsOnMethods = "verifyInternalRetunOrderAndInternalRecipt_IN_PC_050")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Product Conversion with return serials")
	public void verifyProductConversion_IN_PC_050() throws Exception {
		productConversion_IN_PC_050();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
