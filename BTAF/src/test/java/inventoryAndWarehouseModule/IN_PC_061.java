package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression4;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_PC_061 extends InventoryAndWarehouseModuleRegression4 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Product Conversion",
				"Verify that only warehouse description display in Product Conversion form", this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Configure warehouse name only")
	public void verifyConfigureWarehouseNameOnly_IN_PC_061() throws Exception {
		configureWarehouseNameOnly_IN_PC_061();
	}
	
	@Test(dependsOnMethods = "verifyConfigureWarehouseNameOnly_IN_PC_061")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Warehouse Creation")
	public void verifyWarehouseCreation_IN_PC_061() throws Exception {
		warehouseCreation_IN_PC_061();
	}
	
	@Test(dependsOnMethods = "verifyWarehouseCreation_IN_PC_061")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Stock Adjustment")
	public void verifyStockAdjustment_IN_PC_061() throws Exception {
		stockAdjustment_IN_PC_061();
	}
	
	@Test(dependsOnMethods = "verifyStockAdjustment_IN_PC_061")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check if the warehouse only shows the description")
	public void verifyReleaseProductConversionAndCheckWarehouseNameOnly_IN_PC_061() throws Exception {
		releaseProductConversionAndCheckWarehouseNameOnly_IN_PC_061();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
