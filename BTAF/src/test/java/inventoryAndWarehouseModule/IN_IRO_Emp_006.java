package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_IRO_Emp_006 extends InventoryAndWarehouseModuleRegression{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"inventoryAndWarehouseModule.AddQuantity_03.verifyAddQuantityThroughStockAdjustment"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internal Order for first employee")
	@Step("Login to the Entution")
	public void verifyInternelOrderEmp01_IN_IRO_Emp_006() throws Exception {
		internelOrderEmp01_IN_IRO_Emp_006();
	}
	
	@Test(dependsOnMethods = "verifyInternelOrderEmp01_IN_IRO_Emp_006")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internal Dispatch Order for first employee")
	public void verifyInternelDispatchOrderEmp01_IN_IRO_Emp_006() throws Exception {
		internelDispatchOrderEmp01_IN_IRO_Emp_006();
	}
	
	@Test(dependsOnMethods = "verifyInternelDispatchOrderEmp01_IN_IRO_Emp_006")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internal Order for second employee")
	public void verifyInternelOrderEmp02_IN_IRO_Emp_006() throws Exception {
		internelOrderEmp02_IN_IRO_Emp_006();
	}
	
	@Test(dependsOnMethods = "verifyInternelOrderEmp02_IN_IRO_Emp_006")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internal Dispatch Order for second employee")
	public void verifyInternelDispatchOrderEmp02_IN_IRO_Emp_006() throws Exception {
		internelDispatchOrderEmp02_IN_IRO_Emp_006();
	}
	
	@Test(dependsOnMethods = "verifyInternelDispatchOrderEmp02_IN_IRO_Emp_006")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify Internel Return Order part")
	public void verifyInternelReturnOrder_IN_IRO_Emp_006() throws Exception {
		internelReturnOrder_IN_IRO_Emp_006();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
	}
}
