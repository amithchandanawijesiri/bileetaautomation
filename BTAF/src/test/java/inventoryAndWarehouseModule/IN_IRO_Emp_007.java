package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_IRO_Emp_007 extends InventoryAndWarehouseModuleRegression{

	TestCommonMethods common = new TestCommonMethods();
	
	@BeforeClass
	public void setUpDependent() throws Exception {
		setUpForDependent("Inventory and Warehousing", "Internal Return Order", this.getClass().getSimpleName(),
				this.getClass().getSimpleName());
	}
	
	@Test(dependsOnMethods = {"inventoryAndWarehouseModule.AddQuantity_03.verifyAddQuantityThroughStockAdjustment","inventoryAndWarehouseModule.IN_IRO_Emp_006.verifyInternelReturnOrder_IN_IRO_Emp_006"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify document finding methods")
	public void verifyDocumentFindingMethods_IN_IRO_Emp_007_01() throws Exception {
		verifyDocumentFindingMethoda_IN_IRO_Emp_007_01();
	}

	@Test(dependsOnMethods = "verifyDocumentFindingMethods_IN_IRO_Emp_007_01")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify document finding methods")
	public void verifyDocumentFindingMethods_IN_IRO_Emp_007_02() throws Exception {
		verifyDocumentFindingMethoda_IN_IRO_Emp_007_02();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
	}
}
