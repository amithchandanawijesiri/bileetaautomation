package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class AV_IW_InboundLoanOrder extends InventoryAndWarehouseModule{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Action Verification - Inbound Loan Order",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Inbound Loan Order")
	@Step("Login, Navigate to Inventory And Warehouse Module")
	public void verifyNavigateToNewInboundLanOrderActionVerification() throws Exception {
		navigateToNewInboundLanOrderActionVerification();
		createCommonProducts();
	}
	
	@Test(dependsOnMethods = "verifyNavigateToNewInboundLanOrderActionVerification")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill Inbound Loan Order")
	public void verifyFillInboundLOanOrderActionVerification() throws Exception {
		fillInboundLOanOrderActionVerification();
	}
	
	
	@Test(dependsOnMethods = "verifyFillInboundLOanOrderActionVerification")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Action Verification")
	public void verifyActionVerificationInboundLoanOrder() throws Exception {
		actionVerificationInboundLoanOrder();
	}
	
	@Test(dependsOnMethods = "verifyActionVerificationInboundLoanOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Action Section Verification")
	public void verifyActionSectionVerificationInboundLoanOrder() throws Exception {
		actionSectionVerificationInboundLoanOrder();
	}
	
	@Test(dependsOnMethods = "verifyActionSectionVerificationInboundLoanOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Other Action Verification")
	public void verifyActionSectionOtherVerificationInboundLoanOrder() throws Exception {
		actionSectionOtherVerificationInboundLoanOrder();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
