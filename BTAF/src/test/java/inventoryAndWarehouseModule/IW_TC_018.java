package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IW_TC_018 extends InventoryAndWarehouseModule {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "",
				"Verify that user can be able to do Assembly process via Sales Order [Sales Order to Sales Invoice]",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to new Sales Order form")
	@Step("Login")
	public void verifyNavigateToNewSalesOrder() throws Exception {
		navigateToNewSalesOrder();
	}

	@Test(dependsOnMethods = "verifyNavigateToNewSalesOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill Sales Order form")
	public void verifyFillSalesOrderwithAssemblyProduct() throws Exception {
		fillSalesOrderwithAssemblyProduct();
	}

	@Test(dependsOnMethods = "verifyFillSalesOrderwithAssemblyProduct")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Assembly Cost Estimation")
	public void veerifyNavigateToCostEstimationSalesOrder() throws Exception {
		navigateToCostEstimationSalesOrder();
	}

	@Test(dependsOnMethods = "veerifyNavigateToCostEstimationSalesOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill to Assembly Cost Estimation")
	public void verifyFillAssemblyCostEstimationAssemblyProcess() throws Exception {
		fillAssemblyCostEstimationAssemblyProcess();
	}

	@Test(dependsOnMethods = "verifyFillAssemblyCostEstimationAssemblyProcess")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Checkout Assembly Cost Estimation")
	public void verifyCheckoutAssemblyCostEstimationApplyToMainGrid() throws Exception {
		checkoutAssemblyCostEstimationApplyToMainGrid();
	}

	@Test(dependsOnMethods = "verifyCheckoutAssemblyCostEstimationApplyToMainGrid")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Checkout,Draft and Release Sales Order ")
	public void verifyDraftAndReleaseSalesOrder() throws Exception {
		draftAndReleaseSalesOrder();
	}

	@Test(dependsOnMethods = "verifyDraftAndReleaseSalesOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Genarate Assembly Order")
	public void verifyGenarateAssemblyOrderAssemblyProcess() throws Exception {
		genarateAssemblyOrderAssemblyProcess();
	}

	@Test(dependsOnMethods = "verifyGenarateAssemblyOrderAssemblyProcess")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill Assembly Order")
	public void verifyFillAssemblyOrderAssemblyProcess() throws Exception {
		fillAssemblyOrderAssemblyProcess();
	}

	@Test(dependsOnMethods = "verifyFillAssemblyOrderAssemblyProcess")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Product tab and Estimation")
	public void verifyProductTabAndEstimationAssemblyProcess() throws Exception {
		productTabAndEstimationAssemblyProcess();
	}

	@Test(dependsOnMethods = "verifyProductTabAndEstimationAssemblyProcess")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft and Release Assembly Order")
	public void verifyDraftReleeseAssemblyOrderAssemblyProcess() throws Exception {
		draftReleeseAssemblyOrderAssemblyProcess();
	}

	@Test(dependsOnMethods = "verifyDraftReleeseAssemblyOrderAssemblyProcess")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Job start and Navigate to Internel Order")
	public void verifyJobStartAndNavigateToInternelOrder() throws Exception {
		jobStartAndNavigateToInternelOrder();
	}

	@Test(dependsOnMethods = "verifyJobStartAndNavigateToInternelOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill Internel Order")
	public void verifyFillInternelOrderAssemblyProcess() throws Exception {
		fillInternelOrderAssemblyProcess();
	}

	@Test(dependsOnMethods = "verifyFillInternelOrderAssemblyProcess")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Update Output Product")
	public void verifyUpdateOutputProductAssemblyProcess() throws Exception {
		updateOutputProductAssemblyProcess();
	}

	@Test(dependsOnMethods = "verifyUpdateOutputProductAssemblyProcess")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Job Complete")
	public void verifyJobCompleteAssemblyProcess() throws Exception {
		jobCompleteAssemblyProcess();
	}

	@Test(dependsOnMethods = "verifyJobCompleteAssemblyProcess")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Convert to Internel Recipt and Draft")
	public void verifyConverToInternelReciptAndDraft() throws Exception {
		converToInternelReciptAndDraft();
	}

	@Test(dependsOnMethods = "verifyConverToInternelReciptAndDraft")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Seriel/Batch capture ant relese Internel")
	public void verifySerielBatchCaptureAndReleseInternelReciptAssemblyProcess() throws Exception {
		serielBatchCaptureAndReleseInternelReciptAssemblyProcess();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}
}
