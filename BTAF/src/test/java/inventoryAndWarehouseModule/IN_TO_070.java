package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_070 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that user can be able to tranfer serial/batches which inbound via Outbound Loan Order",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Outbound Loan Order part")
	@Step("Login to the Entution")
	public void verifyOutboundLoanOrder_IN_TO_070() throws Exception {
		outboundLoanOrder_IN_TO_070();
	}

	@Test(dependsOnMethods = "verifyOutboundLoanOrder_IN_TO_070")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Outbound Shipment and Inbound Shipment")
	public void verifyOutboundShipmentAndInboundShipment_IN_TO_070() throws Exception {
		outboundShipmentAndInboundShipment_IN_TO_070();
	}

	@Test(dependsOnMethods = "verifyOutboundShipmentAndInboundShipment_IN_TO_070")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order")
	public void verifyTransferOrder_IN_TO_070() throws Exception {
		transferOrder_IN_TO_070();
	}

	@Test(dependsOnMethods = "verifyTransferOrder_IN_TO_070")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Outbound Shipment of Transfer Order")
	public void verifyOutboundShipmen_IN_TO_070() throws Exception {
		outboundShipmen_IN_TO_070();
	}

	@Test(dependsOnMethods = "verifyOutboundShipmen_IN_TO_070")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Inbound Shipment of Transfer Order")
	public void verifyInboundShipment_IN_TO_070() throws Exception {
		inboundShipment_IN_TO_070();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
