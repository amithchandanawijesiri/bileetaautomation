package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class AV_IW_InterdeaprtmentalTO extends InventoryAndWarehouseModule{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Action Verification - Interdepartmental Transfer Order",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Interdepartmental Transfer Order")
	@Step("Login, Navigate to Inventory And Warehouse Module")
	public void verifyNavigateToInterDepartmentalTaransferOrderActoionVerification() throws Exception {

		navigateToInterDepartmentalTaransferOrderActoionVerification();

	}
	
	@Test(dependsOnMethods = "verifyNavigateToInterDepartmentalTaransferOrderActoionVerification")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill Interdepartmental Transfer Order")
	public void verifyFillInterDepartmentalTransferOrderActionVerification() throws Exception {

		fillInterDepartmentalTransferOrderActionVerification();

	}
	
	@Test(dependsOnMethods = "verifyFillInterDepartmentalTransferOrderActionVerification")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Action Verification - Interdepartmental Transfer Order")
	public void verifyTheActionsInterdepartmentalTransferOrderActionVerification() throws Exception {

		verifyActionsInterdepartmentalTransferOrderActionVerification();

	}
	
	@Test(dependsOnMethods = "verifyTheActionsInterdepartmentalTransferOrderActionVerification")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Action Section Verification - Interdepartmental Transfer Order")
	public void verifyActionSectionSectionVerficationInterdepartmentalTransferOrder() throws Exception {

		actionSectionSectionVerfication();

	}
	

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
//		common.driverClose();

	}

}
