package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_033_035 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that stock will be added to \"To warehouse\" once release the inbound shipment",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {
			"inventoryAndWarehouseModule.IN_TO_032_034.verifySetStockReservationManuelForTransferOrderJourney",
			"inventoryAndWarehouseModule.AddQuantityGBV_01.verifyAddQuantityThroughStockAdjustment" })
	@Severity(SeverityLevel.CRITICAL)
	@Description("Release Inbound Shipment and check stock availability")
	@Step("Login to the Entution")
	public void verifyAndCheckStockAddingWhenReleaseInboundShipment01() throws Exception {
		checkStockAddingWhenReleaseInboundShipment01();
	}

	@Test(dependsOnMethods = "verifyAndCheckStockAddingWhenReleaseInboundShipment01")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Release Inbound Shipment and check stock availability")
	@Step("Login to the Entution")
	public void verifyAndCheckStockAddingWhenReleaseInboundShipment02() throws Exception {
		checkStockAddingWhenReleaseInboundShipment02();
	}

	@Test(dependsOnMethods = "verifyAndCheckStockAddingWhenReleaseInboundShipment02")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Reverse Inbound Shipment and check stock availability")
	@Step("Login to the Entution")
	public void verifyReverseInboundShipmentAndCheckStockDeductionToWarehouse() throws Exception {
		reverseInboundShipmentAndCheckStockDeductionToWarehouse();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
