package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_036_037_038_039 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that unit cost will be updated according to the stock when deducting \"From warehouse\" AND \"Verify that stock value updated accordingly in journal entry of outbound shipment\" AND Verify that unit cost will be updated according to the stock when Adding to \"To  warehouse\" AND \"Verify that stock value updated accordingly with transfer cost in journal entry of inbound shipment\"",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {
			"inventoryAndWarehouseModule.IN_TO_032_034.verifySetStockReservationManuelForTransferOrderJourney",
			"inventoryAndWarehouseModule.AddQuantityGBV_01.verifyAddQuantityThroughStockAdjustment" })

	@Severity(SeverityLevel.CRITICAL)
	@Description("Stock adjustment with two lots")
	@Step("Login to the Entution")
	public void verifyStockAdjustment01_IN_TO_036_038() throws Exception {
		stockAdjustment01_IN_TO_036_038();
	}

	@Test(dependsOnMethods = "verifyStockAdjustment01_IN_TO_036_038")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Stock adjustment with two lots")
	public void verifyStockAdjustment02_IN_TO_036_038() throws Exception {
		stockAdjustment02_IN_TO_036_038();
	}

	@Test(dependsOnMethods = "verifyStockAdjustment02_IN_TO_036_038")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order journey")
	@Step("Login to the Entution")
	public void verifyCompleteTransferOrderJourney_IN_TO_036_038() throws Exception {
		completeTransferOrderJourney_IN_TO_036_038();
	}

	@Test(dependsOnMethods = "verifyCompleteTransferOrderJourney_IN_TO_036_038")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify Bincard Report")
	@Step("Login to the Entution")
	public void verifyCheckBincardDeatailReport_IN_TO_036_037_038_039() throws Exception {
		checkBincardDeatailReport_IN_TO_036_037_038_039();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
