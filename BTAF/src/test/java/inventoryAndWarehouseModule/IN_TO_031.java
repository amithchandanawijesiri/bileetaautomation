package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_031 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that user cannot reverse the inbound shipment if user use the stock for another transaction",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = "inventoryAndWarehouseModule.AddQuantityGBV_01.verifyAddQuantityThroughStockAdjustment")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order journey")
	@Step("Login to the Entution")
	public void verifyCompleteTransferOrderJourney_IN_TO_031() throws Exception {
		completeTransferOrderJourney_IN_TO_031();
	}

	@Test(dependsOnMethods = "verifyCompleteTransferOrderJourney_IN_TO_031")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internal Order joueney with inbound quantity")
	@Step("Login to the Entution")
	public void verifyUseStockForInternelOrderJourney_IN_IO_031() throws Exception {
		useStockForInternelOrderJourney_IN_IO_031();
	}

	@Test(dependsOnMethods = "verifyUseStockForInternelOrderJourney_IN_IO_031")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Reverse Inbound Shipment")
	@Step("Login to the Entution")
	public void verifyReverseInboundShipmentThatAltreadyUsedForTransaction_IN_TO_031() throws Exception {
		reverseInboundShipmentThatAltreadyUsedForTransaction_IN_TO_031();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
