package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_IO_038 extends InventoryAndWarehouseModuleRegression{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"inventoryAndWarehouseModule.AddQuantity_02.verifyAddQuantityThroughStockAdjustment"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Journey Configuration")
	@Step("Login to the Entution")
	public void verifyJourneyConfigureFor_IN_IO_038() throws Exception {
		journeyConfigureFor_IN_IO_038();
	}
	
	@Test(dependsOnMethods = "verifyJourneyConfigureFor_IN_IO_038")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Purchase Order")
	public void verifyPurchaseOrderInboundQuantityForVerifyAutomaticAndInboundStockReservation() throws Exception {
		purchaseOrderInboundQuantityForVerifyAutomaticAndInboundStockReservation();
	}
	
	@Test(dependsOnMethods = "verifyPurchaseOrderInboundQuantityForVerifyAutomaticAndInboundStockReservation")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill Internel Order")
	public void verifyFillInternelOrder_IN_IO_038() throws Exception {
		fillInternelOrder_IN_IO_038();
	}
	
	@Test(dependsOnMethods = "verifyFillInternelOrder_IN_IO_038")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify stock reservation")
	public void verifyAutomaticAndOutboundReserveStockProcessAndVerifyStockReservation_IN_IO_038() throws Exception {
		automaticAndOutboundReserveStockProcessAndVerifyStockReservation_IN_IO_038();
	}
	
	@Test(dependsOnMethods = "verifyAutomaticAndOutboundReserveStockProcessAndVerifyStockReservation_IN_IO_038")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify stock reservation updated after Inbound Shipment")
	public void verifyAutomaticAndOutboundReserveStockProcessAndVerifyStockReservation_IN_IO_038_2Nd() throws Exception {
		automaticAndOutboundReserveStockProcessAndVerifyStockReservation_IN_IO_038_2Nd();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
