package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_IRO_Emp_012 extends InventoryAndWarehouseModuleRegression{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"inventoryAndWarehouseModule.AddQuantity_04.verifyAddQuantityThroughStockAdjustment","inventoryAndWarehouseModule.IN_IRO_Emp_006.verifyInternelReturnOrder_IN_IRO_Emp_006"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify all properties according to the product")
	public void verifyAndCheckDetailsAreLoadingAccordingToTheProduct01_IN_IRO_Emp_012() throws Exception {
		checkDetailsAreLoadingAccordingToTheProduct01_IN_IRO_Emp_012();
	}
	
	@Test(dependsOnMethods = "verifyAndCheckDetailsAreLoadingAccordingToTheProduct01_IN_IRO_Emp_012")
	@Severity(SeverityLevel.CRITICAL)
	@Step("Login to the Entution")
	@Description("Verify all properties according to the product")
	public void verifyAndCheckDetailsAreLoadingAccordingToTheProduct02_IN_IRO_Emp_012() throws Exception {
		checkDetailsAreLoadingAccordingToTheProduct02_IN_IRO_Emp_012();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
