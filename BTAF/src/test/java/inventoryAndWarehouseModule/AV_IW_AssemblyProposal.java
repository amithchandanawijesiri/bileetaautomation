package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class AV_IW_AssemblyProposal extends InventoryAndWarehouseModule{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Action Verification - Assembly Proposal",
				this.getClass().getSimpleName());
	}
	String tc_name = this.getClass().getSimpleName();
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to the new Assembly Proposal")
	@Step("Login, Navigate to Inventory And Warehouse Module")
	public void verifyNavigateToAssemblyProposalActionVerification() throws Exception {

		navigateToAssemblyProposalActionVerification();

	}
	
	@Test(dependsOnMethods = "verifyNavigateToAssemblyProposalActionVerification")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill Assembly Proposal")
	public void verifyFillAssemblyProposalActionVerification() throws Exception {
		fillAssemblyProposalActionVerification();

	}
	
	@Test(dependsOnMethods = "verifyFillAssemblyProposalActionVerification")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Action Verification - Product Group")
	public void verifyActionVerificationAssemblyProposal() throws Exception {
		actionVerificationAssemblyProposal();
	}
	
	@Test(dependsOnMethods = "verifyActionVerificationAssemblyProposal")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Action Section Verification - Product Group")
	public void verifyActionSectionVerificationAssemblyProposal() throws Exception {
		actionSectionVerificationAssemblyProposal();
	}
	

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
//		common.totalTime(startTime);
		common.driverClose();

	}

}
