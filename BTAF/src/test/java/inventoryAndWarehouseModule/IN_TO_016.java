package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_016 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that user allow to navigate to new transfer order while drafting particular trasnfer order",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = "inventoryAndWarehouseModule.AddQuantityGBV_01.verifyAddQuantityThroughStockAdjustment")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft first Transfer Order")
	@Step("Login to the Entution")
	public void verifyDraftFirstTransferOrder_IN_TO_016() throws Exception {
		draftFirstTransferOrder_IN_TO_016();
	}

	@Test(dependsOnMethods = "verifyDraftFirstTransferOrder_IN_TO_016")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order journey which come from Draft & New")
	public void verifyCompleteJourneyOfTransferOrderWhichComesFromDraftAndNew_IN_TO_016() throws Exception {
		completeJourneyOfTransferOrderWhichComesFromDraftAndNew_IN_TO_016();
	}

	@Test(dependsOnMethods = "verifyCompleteJourneyOfTransferOrderWhichComesFromDraftAndNew_IN_TO_016")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order journey with drafted in step 2")
	public void verifySearchFirstTransferOrderAndContinueJourney_IN_IO_016() throws Exception {
		searchFirstTransferOrderAndContinueJourney_IN_TO_016();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
