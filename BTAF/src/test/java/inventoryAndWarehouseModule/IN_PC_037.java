package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression4;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_PC_037 extends InventoryAndWarehouseModuleRegression4 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Product Conversion",
				"Verify that Batches are loading only relevant to the Post Date when doing  Product Conversion [ Before Drafting the Product Conversion ]", this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Back date Stock Adjustment")
	public void verifyBackDateStockAdjustment_IN_PC_037() throws Exception {
		backDateStockAdjustment_IN_PC_037();
	}
	
	@Test(dependsOnMethods = "verifyBackDateStockAdjustment_IN_PC_037")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Today's date Stock Adjustment")
	public void verifyToDateStockAdjustment_IN_PC_037() throws Exception {
		toDateStockAdjustment_IN_PC_037();
	}
	
	@Test(dependsOnMethods = "verifyToDateStockAdjustment_IN_PC_037")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check batches with different post dates")
	public void verifyThatBatchesWithDifferentPostDates_IN_PC_037() throws Exception {
		verifyBatchesWithDifferentPostDates_IN_PC_037();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
