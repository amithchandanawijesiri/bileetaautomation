package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression3;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_ST_033 extends InventoryAndWarehouseModuleRegression3 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Stock Take",
				"Verify that user cannot Draft new Stock Take if there is already drafted Stock Take for same warehouse",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Stock Adjustment for new warehouse")
	@Step("Login to the Entution")
	public void verifyStockAdjustmentForStockTakeWarehouse_IN_ST_033() throws Exception {
		stockAdjustmentForStockTakeWarehouse_IN_ST_033();
	}
	
	@Test(dependsOnMethods = "verifyStockAdjustmentForStockTakeWarehouse_IN_ST_033")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft Stock Take")
	public void verifyDraftStockTake_IN_ST_033() throws Exception {
		draftStockTake_IN_ST_033();
	}
	
	@Test(dependsOnMethods = "verifyDraftStockTake_IN_ST_033")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Try to draft second Stock Take")
	public void verifyDraftSecondStockTake_IN_ST_033() throws Exception {
		draftSecondStockTake_IN_ST_033();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}

}
