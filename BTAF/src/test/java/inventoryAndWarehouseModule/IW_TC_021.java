package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})
public class IW_TC_021 extends InventoryAndWarehouseModule{
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Verify that serials can be changed in Serial Number viewer and and changed serials can be used in Transactions",
				"IW_TC_021");
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Edit serail number")
	@Step("Login, Navigate to inventory and warehousing, Search product by Serial Number viewer")
	public void verifySerialEditWindowLoad() throws Exception {
		serialEditWindowLoad();
	}
	@Test(dependsOnMethods = "verifySerialEditWindowLoad")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Edit product serial")
	public void verifyChangeSerial() throws Exception {
		changeSerial();
	}
	
	@Test(dependsOnMethods = "verifyChangeSerial")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Outbound Loan Order with Changed Seriel")
	public void verifyCompleteIW_TC_013WithChangegSearial() throws Exception {
		completeIW_TC_013WithChangegSearial();
	}
	
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}
}
