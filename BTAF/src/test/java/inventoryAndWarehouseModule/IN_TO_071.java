package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_071 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that user can be able to transfer serial/batches which having Racks",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete warehouse creation part")
	@Step("Login to the Entution")
	public void verifyWarehouseCreationWithTwoRacks_IN_TO_071() throws Exception {
		warehouseCreationWithTwoRacks_IN_TO_071();
	}

	@Test(dependsOnMethods = "verifyWarehouseCreationWithTwoRacks_IN_TO_071")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Stock Adjustment")
	public void verifyStockAdjustmentRack01_IN_TO_071() throws Exception {
		stockAdjustmentRack01_IN_TO_071();
	}

	@Test(dependsOnMethods = "verifyStockAdjustmentRack01_IN_TO_071")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Rack Transfer")
	public void verifyRackTransfer_IN_TO_071() throws Exception {
		rackTransfer_IN_TO_071();
	}

	@Test(dependsOnMethods = "verifyRackTransfer_IN_TO_071")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order")
	public void verifyTransferOrder_IN_TO_071() throws Exception {
		transferOrder_IN_TO_071();
	}

	@Test(dependsOnMethods = "verifyTransferOrder_IN_TO_071")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Outbound Shipment")
	public void verifyOutboundShipmen_IN_TO_071() throws Exception {
		outboundShipmen_IN_TO_071();
	}
	
	@Test(dependsOnMethods = "verifyOutboundShipmen_IN_TO_071")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Inbound Shipment")
	public void verifyInboundShipment_IN_TO_071() throws Exception {
		inboundShipment_IN_TO_071();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
