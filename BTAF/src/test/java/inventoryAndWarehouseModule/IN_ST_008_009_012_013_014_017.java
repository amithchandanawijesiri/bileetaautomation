package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression3;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_ST_008_009_012_013_014_017 extends InventoryAndWarehouseModuleRegression3 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Stock Take",
				"Verify that user can adding products to the grid via + icon AND Verify that Rack Code, Default UOM, System Balance will display when adding the products to the grid through + icon AND Verify that user can draft the stock take with multiple products or single product AND Verify that variance qty will display after drafting the stock take AND Verify that adjusted Qty display as \"0\" until do the adjsutment AND Verify that user can release the drafted stock take",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create new warehouse with a rack")
	@Step("Login to the Entution")
	public void verifyCreateNewWarehouseWithARack_IN_ST_008_009_012_013_014_017() throws Exception {
		createNewWarehouseWithARack_IN_ST_008_009_012_013_014_017();
	}

	@Test(dependsOnMethods = "verifyCreateNewWarehouseWithARack_IN_ST_008_009_012_013_014_017")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Stock Adjustment")
	public void verifyStockAdjustmentForNewWarehouse_IN_ST_008_009_012_013_014_017() throws Exception {
		stockAdjustmentForNewWarehouse_IN_ST_008_009_012_013_014_017();
	}

	@Test(dependsOnMethods = "verifyStockAdjustmentForNewWarehouse_IN_ST_008_009_012_013_014_017")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify pop-up, grid and draft function. Verify varience quantity and adjusted quantity. Verify release function")
	public void verifyStockTake_IN_ST_008_009_012_013_014_017() throws Exception {
		stockTake_IN_ST_008_009_012_013_014_017();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}

}
