package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.AdministrationModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})

public class Con_TC_001 extends InventoryAndWarehouseModule {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Verify that user can successfully navigate to the login page",
				"Con_TC_001");
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate Entution Home Page")
	public void verifyNavigateToTheLoginPage() throws Exception {
		navigateToTheLoginPage();

	}
	
	@Test(dependsOnMethods = "verifyNavigateToTheLoginPage")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check login page avilability")
	public void checkAndVerifyTheLogo() throws Exception {
		verifyTheLogo();

	}
	
	@Test(dependsOnMethods = "checkAndVerifyTheLogo")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Login process")
	public void verifyUserLogin() throws Exception {
		userLogin();

	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
