package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_100 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that user allow to outbound the qty which already removed from manual reservation",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = { "inventoryAndWarehouseModule.IN_TO_099.verifySalesOrderOutboundShipment_IN_IO_099" })
	@Severity(SeverityLevel.CRITICAL)
	@Description("Remove reserved products from allocation manager")
	@Step("Login to the Entution")
	public void verifyRemoveAllocation_IN_TO_100() throws Exception {
		removeAllocation_IN_TO_100();
	}

	@Test(dependsOnMethods = "verifyRemoveAllocation_IN_TO_100")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify quantity outbound")
	public void verifySalesOrderOutboundShipment_IN_IO_100() throws Exception {
		salesOrderOutboundShipment_IN_IO_100();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}

}
