package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_IRO_Emp_008 extends InventoryAndWarehouseModuleRegression{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		InventoryAndWarehouseModule obj = new InventoryAndWarehouseModule();
		
		common.setUp("Inventory and Warehousing", "", "",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"inventoryAndWarehouseModule.AddQuantity_03.verifyAddQuantityThroughStockAdjustment","inventoryAndWarehouseModule.IN_IRO_Emp_006.verifyInternelReturnOrder_IN_IRO_Emp_006"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify product adding part to the grid")
	public void verifyAndCheckWhetherUserCanAddProductToTheGrid_IN_IRO_Emp_008() throws Exception {
		checkWhetherUserCanAddProductToTheGrid_IN_IRO_Emp_008();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
