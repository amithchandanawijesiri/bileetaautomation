package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})
public class IW_TC_011 extends InventoryAndWarehouseModule{
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Verify that user can be able to return the dispatched product Qty via Internal Return Order [ Employee Request]",
				"IW_TC_011");
	}
	
	@Test(dependsOnMethods = "inventoryAndWarehouseModule.IW_TC_010.verifyCheckWarehouseForProdctAvailabilityInternelOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Internel Return Order form")
	@Step("Login, Navigate to inventory and warehousing")
	public void verifyLoadInternelReturnOrderForm() throws Exception {
		loadInternelReturnOrderForm();
	}
	@Test(dependsOnMethods = "verifyLoadInternelReturnOrderForm")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill Internel Return Order")
	public void verifyFillInternelReturnOrder() throws Exception {
		fillInternelReturnOrder();
	}
	
	@Test(dependsOnMethods = "verifyFillInternelReturnOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Select Document")
	public void verifySelectDocumentsThatNeedToReturnIRO() throws Exception {
		selectDocumentsThatNeedToReturnIRO();
	}
	
	
	@Test(dependsOnMethods = "verifySelectDocumentsThatNeedToReturnIRO")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check product availability")
	public void verifyCheckWarehouseForProdctAvailabilityInternelReteturnOrder() throws Exception {
		checkWarehouseForProdctAvailabilityInternelReteturnOrder();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
