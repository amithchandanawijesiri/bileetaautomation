package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression4;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_PC_060 extends InventoryAndWarehouseModuleRegression4 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Product Conversion",
				"Verify that only warehouse code display in Product Conversion form", this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Configure warehouse code only")
	public void verifyConfigureWarehouseCodeOnly_IN_PC_060() throws Exception {
		configureWarehouseCodeOnly_IN_PC_060();
	}
	
	@Test(dependsOnMethods = "verifyConfigureWarehouseCodeOnly_IN_PC_060")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check if the warehouse only shows the code")
	public void verifyReleaseProductConversionAndCheckWarehouseCodeOnly_IN_PC_060() throws Exception {
		releaseProductConversionAndCheckWarehouseCodeOnly_IN_PC_060();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
