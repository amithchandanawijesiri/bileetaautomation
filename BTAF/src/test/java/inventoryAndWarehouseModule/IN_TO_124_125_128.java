package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression3;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_124_125_128 extends InventoryAndWarehouseModuleRegression3 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that user allow to complete shipment acceptance onece for multiple products AND Verify that stock will be deducted  from \"From warehouse\" after complete the shipment acceptance for all products onece AND Verify that outbound shipment will release after complete the shipment acceptance",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order with Shipment Acceptance configuration")
	@Step("Login to the Entution")
	public void verifyTransferOrder_IN_TO_124_125_128() throws Exception {
		transferOrder_IN_TO_124_125_128();
	}

	@Test(dependsOnMethods = "verifyTransferOrder_IN_TO_124_125_128")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Outbound Shipment")
	public void verifyOutboundShipment_IN_TO_124_125_128() throws Exception {
		outboundShipment_IN_TO_124_125_128();
	}

	@Test(dependsOnMethods = "verifyOutboundShipment_IN_TO_124_125_128")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Release shipment acceptance for multiple products")
	public void verifyReleaseShipmentAcceptance_IN_TO_124_125_128() throws Exception {
		releaseShipmentAcceptance_IN_TO_124_125_128();
	}

	@Test(dependsOnMethods = "verifyReleaseShipmentAcceptance_IN_TO_124_125_128")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check released status of Outbound Shipment")
	public void verifyAndCheckReleasedStatusOfOutboundShipment_IN_TO_124_125_128() throws Exception {
		checkReleasedStatusOfOutboundShipment_IN_TO_124_125_128();
	}

	@Test(dependsOnMethods = "verifyAndCheckReleasedStatusOfOutboundShipment_IN_TO_124_125_128")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Release Inbound Shipment")
	public void verifyReleseInboundShipment_IN_TO_124_125_128() throws Exception {
		releseInboundShipment_IN_TO_124_125_128();
	}

	@Test(dependsOnMethods = "verifyReleseInboundShipment_IN_TO_124_125_128")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check stock updatance")
	public void verifyAndCheckStockUpdatance_IN_TO_124_125_128() throws Exception {
		checkStockUpdatance_IN_TO_124_125_128();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		removeShipmentAcceptanceTransferOrderToOutboundShipment();
		common.totalTime(startTime);
		common.driverClose();
	}

}
