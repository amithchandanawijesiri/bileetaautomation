package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})
public class IW_TC_002 extends InventoryAndWarehouseModule{
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Verify that created product Group details loaded under Product Information",
				"IW_TC_002");
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to prduct information form")
	@Step("Login, Navigate to inventory and warehousing")
	public void verifyThatUserCanNavigateToTheProductInformationForm() throws Exception {
		verifyThatUseeCanNavigateToTheProductInformationForm();
	}
	
	@Test(dependsOnMethods = "verifyThatUserCanNavigateToTheProductInformationForm")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check product group aailability")
	public void verifyToProductGroupAvailabilty() throws Exception {
		verifyProductGroupAvailabilty();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
