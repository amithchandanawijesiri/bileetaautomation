package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_049 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that user can transfer multipe lots through transfer order and stock update accordingly [ LOT FIFO, Serial FIFO, Batch FIFO ]",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {
			"inventoryAndWarehouseModule.AddQuantityGBV_01.verifyAddQuantityThroughStockAdjustment"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create LOT FIFO product")
	@Step("Login to the Entution")
	public void verifyCreateLotFifoProduct_IN_TO_049() throws Exception {
		createLotFifoProduct_IN_TO_049();
	}

	@Test(dependsOnMethods = "verifyCreateLotFifoProduct_IN_TO_049")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create SERIEL FIFO product")
	public void verifyCreateSerielFifoProduct_IN_TO_049() throws Exception {
		createSerielFifoProduct_IN_TO_049();
	}

	@Test(dependsOnMethods = "verifyCreateSerielFifoProduct_IN_TO_049")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create BATCH FIFO product")
	public void verifyCreateBatchFifoProduct_IN_TO_049() throws Exception {
		createBatchFifoProduct_IN_TO_049();
	}

	@Test(dependsOnMethods = "verifyCreateBatchFifoProduct_IN_TO_049")
	@Severity(SeverityLevel.CRITICAL)
	@Description("First Stock Adjustment")
	public void verifyFirstStockAdjustment_IN_TO_049() throws Exception {
		firstStockAdjustment_IN_TO_049();
	}

	@Test(dependsOnMethods = "verifyFirstStockAdjustment_IN_TO_049")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Second Stock Adjustment")
	public void verifySecondStockAdjustment_IN_TO_049() throws Exception {
		secondStockAdjustment_IN_TO_049();
	}

	@Test(dependsOnMethods = "verifySecondStockAdjustment_IN_TO_049")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order journey")
	public void verifyTransferOrderJourney_IN_TO_049() throws Exception {
		transferOrderJourney_IN_TO_049();
	}

	@Test(dependsOnMethods = "verifyTransferOrderJourney_IN_TO_049")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check stock updatance according to FIFO")
	public void verifyAndCheckStockUpdatedAccordingToFifo_IN_TO_049() throws Exception {
		checkStockUpdatedAccordingToFifo_IN_TO_049();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
