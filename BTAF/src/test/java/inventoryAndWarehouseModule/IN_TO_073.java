package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_073 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that journal entries going correctly when doing Transfer Order when stock having in the Rack",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete warehouse creation")
	@Step("Login to the Entution")
	public void verifyWarehouseCreationWithTwoRacks_IN_TO_073() throws Exception {
		warehouseCreationWithTwoRacks_IN_TO_073();
	}

	@Test(dependsOnMethods = "verifyWarehouseCreationWithTwoRacks_IN_TO_073")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Stock Adjustment")
	public void verifyStockAdjustmentRack01_IN_TO_073() throws Exception {
		stockAdjustmentRack01_IN_TO_073();
	}

	@Test(dependsOnMethods = "verifyStockAdjustmentRack01_IN_TO_073")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Sales Order journey")
	public void verifySalesOrderOutbountShipmentToSalesInvoice_IN_TO_073() throws Exception {
		salesOrderOutbountShipmentToSalesInvoice_IN_TO_073();
	}

	@Test(dependsOnMethods = "verifySalesOrderOutbountShipmentToSalesInvoice_IN_TO_073")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Sales Return Order journey")
	public void verifySalesReturnOrderInboundShipmentToSalesInvoice_IN_TO_073() throws Exception {
		salesReturnOrderInboundShipmentToSalesInvoice_IN_TO_073();
	}

	@Test(dependsOnMethods = "verifySalesReturnOrderInboundShipmentToSalesInvoice_IN_TO_073")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order")
	public void verifyTransferOrder_IN_TO_073() throws Exception {
		transferOrder_IN_TO_073();
	}
	
	@Test(dependsOnMethods = "verifyTransferOrder_IN_TO_073")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Outbound Shipment")
	public void verifyOutboundShipment_IN_TO_073() throws Exception {
		outboundShipment_IN_TO_073();
	}
	
	@Test(dependsOnMethods = "verifyOutboundShipment_IN_TO_073")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Inbound Shipment")
	public void verifyInboundShipment_IN_TO_073() throws Exception {
		inboundShipment_IN_TO_073();
	}
	
	@Test(dependsOnMethods = "verifyInboundShipment_IN_TO_073")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check journel entry")
	public void verifyAndCheckJournelEntry_IN_TO_073() throws Exception {
		checkJournelEntry_IN_TO_073();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
