package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_IO_055_058_059 extends InventoryAndWarehouseModuleRegression{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"inventoryAndWarehouseModule.AddQuantity_02.verifyAddQuantityThroughStockAdjustment"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internel Order")
	@Step("Login to the Entution")
	public void verifyReverseInternelOrder1() throws Exception {
		reverseInternelOrder1();
	}
	
	@Test(dependsOnMethods = "verifyReverseInternelOrder1")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft / Release and Reverse Internel Order")
	public void verifyReverseInternelOrder2() throws Exception {
		reverseInternelOrder2();
	}
	
	@Test(dependsOnMethods = "verifyReverseInternelOrder2")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Comapre stock after released Internel Order")
	public void verifyAndComapreTheStockAfterReveserInternelOrder_IN_IO_055_058() throws Exception {
		comapreTheStockAfterReveserInternelOrder_IN_IO_055_058();
	}
	
	@Test(dependsOnMethods = "verifyAndComapreTheStockAfterReveserInternelOrder_IN_IO_055_058")
	@Severity(SeverityLevel.CRITICAL)
	@Description("User revesed product quntity")
	public void verifyUserCanUseRevesedQuantityForInternelOrder_IN_IO_059() throws Exception {
		usedRevesedQuantityForInternelOrder_IN_IO_059();
	}
	
	@Test(dependsOnMethods = "verifyUserCanUseRevesedQuantityForInternelOrder_IN_IO_059")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Reverse finale Internel Dispatch Order")
	public void verifyReverseIDO_IN_IO_059() throws Exception {
		reverseIDO_IN_IO_059();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
