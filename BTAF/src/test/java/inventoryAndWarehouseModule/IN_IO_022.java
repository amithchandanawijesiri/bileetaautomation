package inventoryAndWarehouseModule;

import org.springframework.context.annotation.DependsOn;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_IO_022 extends InventoryAndWarehouseModule{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"inventoryAndWarehouseModule.AddQuantity_01.verifyAddQuantityThroughStockAdjustment","inventoryAndWarehouseModule.IN_IO_015.verifyReleaseInternelOrderIN_IO_015"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Duplicate and verify duplicated Internel Order")
	@Step("Login to the Entution")
	public void verifyDuplicateInternelOrderAndVerifyFialdsRegression01() throws Exception {
		duplicateInternelOrderAndVerifyFialdsRegression01();

	}
	
	@Test(dependsOnMethods = "verifyDuplicateInternelOrderAndVerifyFialdsRegression01")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft And Release Internel Order")
	public void verifyDraftReleaseDuplicatedRelesedInternelOrder() throws Exception {
		draftReleaseDuplicatedRelesedInternelOrder();

	}
	
	
	@Test(dependsOnMethods = "verifyDraftReleaseDuplicatedRelesedInternelOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft And Release Internel Dispatch Order")
	public void verifyDraftReleaseDuplicatedRelesedInternelDispatchOrder() throws Exception {
		draftReleaseDuplicatedRelesedInternelDispatchOrder();

	}
	
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
