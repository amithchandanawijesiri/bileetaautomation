package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression4;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_PC_028 extends InventoryAndWarehouseModuleRegression4 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Product Conversion",
				"Verify that user can be Edit the Drafted Product Conversion", this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Stock Adjustment for new warehouse")
	public void verifyStockAdjustmentForNewWarehouse_IN_PC_028() throws Exception {
		stockAdjustmentForNewWarehouse_IN_PC_028();
	}
	
	@Test(dependsOnMethods = {"verifyStockAdjustmentForNewWarehouse_IN_PC_028"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft Product Conversion")
	public void verifyDrftProductConversionWithThreeLines_IN_PC_028() throws Exception {
		drftProductConversionWithThreeLines_IN_PC_028();
	}
	
	@Test(dependsOnMethods = "verifyDrftProductConversionWithThreeLines_IN_PC_028")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Edit drafted Product Conversion")
	public void verifyEditDraftedProductConversion_IN_PC_028() throws Exception {
		editDraftedProductConversion_IN_PC_028();
	}
	
	@Test(dependsOnMethods = "verifyEditDraftedProductConversion_IN_PC_028")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check updates on updated Product Conversion")
	public void verifyAndCheckUpdatedInformations_IN_PC_028() throws Exception {
		checkUpdatedInformations_IN_PC_028();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
