package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_IO_018 extends InventoryAndWarehouseModule{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"inventoryAndWarehouseModule.AddQuantity_01.verifyAddQuantityThroughStockAdjustment"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to New Internel Order and fill Internel Order")
	@Step("Login to the Entution")
	public void verifyNavigateToInternelOrderAndFill_IN_IO_018() throws Exception {
		navigateToInternelOrderAndFill_IN_IO_018();

	}
	
	@Test(dependsOnMethods = "verifyNavigateToInternelOrderAndFill_IN_IO_018")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft, Release and Verifi Go To Page Link")
	public void verifyDraftReleseAndGoToPageLinkAvailabilityAndNavigateTiInternelDispatchOrder18() throws Exception {
		verifydraftReleseAndGoToPageLinkAvailabilityAndNavigateTiInternelDispatchOrder();

	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
