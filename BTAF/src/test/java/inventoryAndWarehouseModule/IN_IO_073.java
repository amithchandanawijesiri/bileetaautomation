package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_IO_073 extends InventoryAndWarehouseModuleRegression{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"inventoryAndWarehouseModule.AddQuantity_03.verifyAddQuantityThroughStockAdjustment"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Outbound Loan Order part")
	@Step("Login to the Entution")
	public void verifyOutboundLoanOrder_IN_IO_073() throws Exception {
		outboundLoanOrder_IN_IO_073();
	}
	
	@Test(dependsOnMethods = "verifyOutboundLoanOrder_IN_IO_073")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Outbound Shipment / Inbound Shipment")
	public void verifyOutboundShipmentAndInboundShipment_IN_IO_073() throws Exception {
		outboundShipmentAndInboundShipment_IN_IO_073();
	}
	
	@Test(dependsOnMethods = "verifyOutboundShipmentAndInboundShipment_IN_IO_073")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internel Order with inbound quantity")
	public void verifyInternelOrder_IN_IO_073() throws Exception {
		internelOrder_IN_IO_073();
	}
	
	@Test(dependsOnMethods = "verifyInternelOrder_IN_IO_073")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internel Dispatch Order")
	public void verifyInternelDispatchOrder_073() throws Exception {
		internelDispatchOrder_073();
	}
	
	@Test(dependsOnMethods = "verifyInternelDispatchOrder_073")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify stock deduction")
	public void verifyComapreTheStockThatTranferedFromTheWarehouse_IN_IO_073() throws Exception {
		comapreTheStockThatTranferedFromTheWarehouse_IN_IO_073();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
//		common.driverClose();
	}
}
