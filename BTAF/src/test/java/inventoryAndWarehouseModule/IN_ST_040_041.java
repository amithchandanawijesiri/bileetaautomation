package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression3;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_ST_040_041 extends InventoryAndWarehouseModuleRegression3 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Stock Take",
				"Verify that user allow to convert to stock take in to stock adjustment partially AND Verify that convert to stock adjustment action will appear when complete the stock adjustment partially for the relevant stock take",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Stock Take")
	@Step("Login to the Entution")
	public void verifyStockTake_IN_ST_040_041() throws Exception {
		stockTake_IN_ST_040_041();
	}

	@Test(dependsOnMethods = "verifyStockTake_IN_ST_040_041")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete first Stock Adjustment")
	public void verifyFirstStockAdjustment_IN_ST_040_041() throws Exception {
		firstStockAdjustment_IN_ST_040_041();
	}
	
	@Test(dependsOnMethods = "verifyFirstStockAdjustment_IN_ST_040_041")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete second Stock Adjustment")
	public void verifySecondStockAdjustment_IN_ST_040_041() throws Exception {
		secondStockAdjustment_IN_ST_040_041();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}

}
