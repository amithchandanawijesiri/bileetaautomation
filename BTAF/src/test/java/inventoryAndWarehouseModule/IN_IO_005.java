package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_IO_005 extends InventoryAndWarehouseModule{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Internel Order",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"inventoryAndWarehouseModule.AddQuantity_01.verifyAddQuantityThroughStockAdjustment"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Inactive Campaign")
	public void verifyInactivaeCampaign() throws Exception {

		inactivaeCampaign();

	}
	
	@Test(dependsOnMethods = "verifyInactivaeCampaign")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify Inactive Campaign")
	@Step("Navigate to New Internel Order")
	public void verifyCheckInactiveCampaignAreNotVisible() throws Exception {

		checkInactiveCampaignAreNotVisible();

	}
	
	@Test(dependsOnMethods = "verifyCheckInactiveCampaignAreNotVisible")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Active Campaign")
	public void verifyActivaeCampaign() throws Exception {

		activaeCampaign();

	}
	
	@Test(dependsOnMethods = "verifyCheckInactiveCampaignAreNotVisible")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify Valid Campaign")
	@Step("Navigate to New Internel Order")
	public void verifyCheckAactiveCampaignAreVisible() throws Exception {

		checkAactiveCampaignAreVisible();

	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
	
	
}
