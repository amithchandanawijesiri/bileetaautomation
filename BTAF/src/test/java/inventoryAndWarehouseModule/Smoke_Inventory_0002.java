package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleSmoke;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;

import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})

public class Smoke_Inventory_0002 extends InventoryAndWarehouseModuleSmoke {
	TestCommonMethods common = new TestCommonMethods();

	
	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Verify that User can be able to create Warehouse",
				"Smoke_Inventory_0002");
	}
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify user can navigate to the warehouse creation form")
	@Step("Login, Navigate to inventory and warehousing")
	public void verifyThatNewWarehouseCretionFormLoading() throws Exception {
		verifyNewWarehouseCretionFormLoading();
	}
	
	@Test(dependsOnMethods = "verifyThatNewWarehouseCretionFormLoading")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify user can filling the warehouse form")
	public void verifyFillWarehouseForm() throws Exception {
		fillWarehouseForm();
	}
	
	@Test(dependsOnMethods = "verifyFillWarehouseForm")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify user can draft a warehouse")
	public void toDraft() throws Exception {
		draft("Warehouse Creation Form");
	}
	
	
	@Test(dependsOnMethods = "toDraft")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify user can relese a warehouse")
	public void toRelese() throws Exception {
		releaseWarehouse();
	}
	


	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
