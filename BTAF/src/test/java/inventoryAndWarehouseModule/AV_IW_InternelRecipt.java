package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class AV_IW_InternelRecipt extends InventoryAndWarehouseModule{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Action Verification - Internel Recipt",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Internel Recipt")
	@Step("Login, Navigate to Inventory And Warehouse Module")
	public void verifyNavigateToInternelRecipt() throws Exception {

		navigateToInternelRecipt();
	}
	
	@Test(dependsOnMethods = "verifyNavigateToInternelRecipt")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Internel Recipt")
	@Step("Login, Navigate to Inventory And Warehouse Module")
	public void verifyFillAndNavigateToInternelRecipt() throws Exception {

		fillAndNavigateToInternelRecipt();
	}
	
	
	@Test(dependsOnMethods = "verifyFillAndNavigateToInternelRecipt")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Internel Recipt")
	public void verifyFillAndNavigateToInternelRecipt2() throws Exception {

		fillAndNavigateToInternelRecipt2();
	}

	@Test(dependsOnMethods = "verifyFillAndNavigateToInternelRecipt2")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Action Verification - Internel Recipt")
	public void verifyinternelReciptActionVerification() throws Exception {
		internelReciptActionVerification();
	}
	
	@Test(dependsOnMethods = "verifyinternelReciptActionVerification")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Action Section Verification - Internel Recipt")
	public void verifyActionSectionVerificationInternelRecipt() throws Exception {
		actionSectionVerificationInternelRecipt();
	}
	
	@Test(dependsOnMethods = "verifyinternelReciptActionVerification")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Action Section Verification - Internel Recipt")
	public void verifyOtherActionVerificationInternelRecipt() throws Exception {
		otherActionVerificationInternelRecipt();
	}
	
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
