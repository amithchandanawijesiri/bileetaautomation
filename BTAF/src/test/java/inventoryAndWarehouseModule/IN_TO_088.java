package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_088 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that user can release the trasnfer order if validate stock disabled for the Trasnfer order form",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Disable stock validation")
	@Step("Login to the Entution")
	public void verifyDisableStockValidate_IN_TO_088() throws Exception {
		disableStockValidate_IN_TO_088();
	}

	@Test(dependsOnMethods = "verifyDisableStockValidate_IN_TO_088")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create relevant product without quantity")
	public void verifyCreateProductsWithoutQuantityTreeProductsOnly() throws Exception {
		createProductsWithoutQuantityTreeProductsOnly();
	}
	
	@Test(dependsOnMethods = "verifyCreateProductsWithoutQuantityTreeProductsOnly")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order")
	public void verifyTransferOrder_IN_TO_088() throws Exception {
		transferOrder_IN_TO_088();
	}
	
	@Test(dependsOnMethods = "verifyTransferOrder_IN_TO_088")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Outbound Shipment stock validation")
	public void verifyOutboundShipment_IN_TO_088() throws Exception {
		outboundShipment_IN_TO_088();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
