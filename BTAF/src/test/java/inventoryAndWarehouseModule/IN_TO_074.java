package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_074 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify tht Captured Qty will be as per the captured qty in Outbound Shipment",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order part")
	@Step("Login to the Entution")
	public void verifyTransferOrder_IN_TO_074() throws Exception {
		transferOrder_IN_TO_074();
	}

	@Test(dependsOnMethods = "verifyTransferOrder_IN_TO_074")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Outbound Shipment")
	public void verifyOutboundShipment_IN_TO_074() throws Exception {
		outboundShipment_IN_TO_074();
	}

	@Test(dependsOnMethods = "verifyOutboundShipment_IN_TO_074")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Inbound Shipment")
	public void verifyInboundShipment_IN_TO_074() throws Exception {
		inboundShipment_IN_TO_074();
	}

	@Test(dependsOnMethods = "verifyInboundShipment_IN_TO_074")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check captured quantity")
	public void verifyAndCheckCapturedQuantityInTransferOrder_IN_TO_74() throws Exception {
		checkCapturedQuantityInTransferOrder_IN_TO_74();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
