package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_097 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that user cannot reserve the qty which is greater than order Qty or OnHand Qty",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Configure manual stock reservation")
	@Step("Login to the Entution")
	public void verifyConfigureManuelStockReservatio_IN_TO_097() throws Exception {
		configureManuelStockReservatio_IN_TO_097();
	}

	@Test(dependsOnMethods = "verifyConfigureManuelStockReservatio_IN_TO_097")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create new warehouse")
	public void verifyCreateWarehouse_IN_TO_097() throws Exception {
		createWarehouse_IN_TO_097();
	}
	
	@Test(dependsOnMethods = "verifyCreateWarehouse_IN_TO_097")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Try to reserve quantity")
	public void verifyTransferOrder_IN_TO_097() throws Exception {
		transferOrder_IN_TO_097();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}

}
