package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_IO_060 extends InventoryAndWarehouseModuleRegression {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "", this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = { "inventoryAndWarehouseModule.AddQuantity_03.verifyAddQuantityThroughStockAdjustment" })
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internel Order")
	@Step("Login to the Entution")
	public void verifyReverseInternelOrderAfterDraftInternelDispatchOrder1() throws Exception {
		reverseInternelOrderAfterDraftInternelDispatchOrder1();
	}

	@Test(dependsOnMethods = "verifyReverseInternelOrderAfterDraftInternelDispatchOrder1")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internel Dispatch Order and reverse Internel Order")
	public void verifyReverseInternelOrderAfterDraftInternelDispatchOrder2() throws Exception {
		reverseInternelOrderAfterDraftInternelDispatchOrder2();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
