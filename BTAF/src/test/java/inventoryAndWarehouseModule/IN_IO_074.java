package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_IO_074 extends InventoryAndWarehouseModuleRegression{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"inventoryAndWarehouseModule.AddQuantity_03.verifyAddQuantityThroughStockAdjustment"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create warehouse with two tacks")
	@Step("Login to the Entution")
	public void verifyWarehouseCreationWithTwoRacks_IN_IO_074() throws Exception {
		warehouseCreationWithTwoRacks_IN_IO_074();
	}
	
	@Test(dependsOnMethods = "verifyWarehouseCreationWithTwoRacks_IN_IO_074")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Stock Adjustment")
	public void verifyStockAdjustmentRack01_IN_IO_074() throws Exception {
		stockAdjustmentRack01_IN_IO_074();
	}
	
	@Test(dependsOnMethods = "verifyStockAdjustmentRack01_IN_IO_074")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Rack Transfer")
	public void verifyRackTransfer_IN_IO_074() throws Exception {
		rackTransfer_IN_IO_074();
	}
	
	@Test(dependsOnMethods = "verifyRackTransfer_IN_IO_074")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internel Order")
	public void verifyInternalOrder_IN_IO_074() throws Exception {
		internelOrder_IN_IO_074();
	}
	
	@Test(dependsOnMethods = "verifyInternalOrder_IN_IO_074")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify Internal Dispatch Order")
	public void verifyInternalDispatchOrder_074() throws Exception {
		internelDispatchOrder_074();
	}
	
	@Test(dependsOnMethods = "verifyInternalDispatchOrder_074")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify stock deduction")
	public void verifyComapreTheStockThatTranferedFromTheWarehouse_IN_IO_074() throws Exception {
		comapreTheStockThatTranferedFromTheWarehouse_IN_IO_074();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
