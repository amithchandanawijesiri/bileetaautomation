package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_IO_016 extends InventoryAndWarehouseModule{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Verify that user can release the drafted internal Order [ Comes through \"Draft and New\" ]",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"inventoryAndWarehouseModule.AddQuantity_01.verifyAddQuantityThroughStockAdjustment","inventoryAndWarehouseModule.IN_IO_013.verifyFillDrafAndNewInternelOrderIN_IO_013"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Find Inbound Loan Order")
	@Step("Login to the Entution")
	public void verifyFindInternoOrderWhichComesFromDraftAndNewButton() throws Exception {
		findInternoOrderWhichComesFromDraftAndNewButton();

	}
	
	@Test(dependsOnMethods = "verifyFindInternoOrderWhichComesFromDraftAndNewButton")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Release Internel Order")
	public void verifyReleaseInternoOrderWhichComesFromDraftAndNewButton() throws Exception {
		releaseInternoOrderWhichComesFromDraftAndNewButton();

	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
