package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_IRO_Emp_018 extends InventoryAndWarehouseModuleRegression{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"inventoryAndWarehouseModule.AddQuantity_04.verifyAddQuantityThroughStockAdjustment"})
	@Severity(SeverityLevel.CRITICAL)
	@Step("Login to the Entution")
	@Description("Complete Return Order")
	public void verifyInternelOrderEmp01_IN_IRO_Emp_018() throws Exception {
		internelOrderEmp01_IN_IRO_Emp_018();
	}
	
	@Test(dependsOnMethods = "verifyInternelOrderEmp01_IN_IRO_Emp_018")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internal Dispatch Order")
	public void verifyInternelDispatchOrderEmp01_IN_IRO_Emp_018() throws Exception {
		internelDispatchOrderEmp01_IN_IRO_Emp_018();
	}
	
	@Test(dependsOnMethods = "verifyInternelDispatchOrderEmp01_IN_IRO_Emp_018")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft / Release Internal Return Order")
	public void verifyDraftReleaseInternelReturnOrderWithMultipleProducts_IN_IRO_Emp_018() throws Exception {
		draftReleaseInternelReturnOrderWithMultipleProducts_IN_IRO_Emp_018();
	}
	
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
	}
}
