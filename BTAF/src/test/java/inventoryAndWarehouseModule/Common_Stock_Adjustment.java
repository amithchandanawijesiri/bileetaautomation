package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.genarateInventoryAndWarehouseInformation;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })
public class Common_Stock_Adjustment extends InventoryAndWarehouseModule {

	@BeforeClass
	public void setUp() throws Exception {
		initiateBrowser();
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify user can navigate to new stock adjustment form")
	@Step("Login, Navigate to inventory and warehousing")
	public void verifyNavigateToStockAdjustmentForm() throws Exception {
		genarateInventoryAndWarehouseInformation obj = new genarateInventoryAndWarehouseInformation();
			obj.commonStockAdjustment(readTestCreation("LotFifo_IN_IO_031"),
					readTestCreation("BatchFifo_IN_IO_031"),
					readTestCreation("SerielFifo_IN_IO_031"));
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		TestCommonMethods common = new TestCommonMethods();
		common.driverClose();

	}

}
