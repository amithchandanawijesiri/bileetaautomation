package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_IO_024_026 extends InventoryAndWarehouseModule{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"inventoryAndWarehouseModule.AddQuantity_01.verifyAddQuantityThroughStockAdjustment"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill Internel Order")
	@Step("Login to the Entution")
	public void verifyFilInternelOrder_IN_IO_024() throws Exception {
		filInternelOrder_IN_IO_024();

	}
	
	@Test(dependsOnMethods = "verifyFilInternelOrder_IN_IO_024")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Relese Internel Dispath Order partially by Go To Page Link")
	public void verifyReleaseDispatchOrderPartialByGoToPageLink() throws Exception {
		releaseDispatchOrderPartialByGoToPageLink();

	}
	
	
	@Test(dependsOnMethods = "verifyReleaseDispatchOrderPartialByGoToPageLink")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify stock deduction")
	public void verifyComapreTheStockThatTranferedFromTheWarehouse_IN_IO_024_26() throws Exception {
		comapreTheStockThatTranferedFromTheWarehouse_IN_IO_024_26();

	}
	
	
	@Test(dependsOnMethods = "verifyComapreTheStockThatTranferedFromTheWarehouse_IN_IO_024_26")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Relese Internel Dispath Order partially by Tile Menu")
	public void verifyReleaseDispatchOrderPartialByTileMenu() throws Exception {
		releaseDispatchOrderPartialByTileMenu();

	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
