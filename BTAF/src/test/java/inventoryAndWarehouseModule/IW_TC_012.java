package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({ TestListener.class })

public class IW_TC_012 extends InventoryAndWarehouseModule {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "",
				"Verify that user can be able to inbound product as a loan via Inbound Loan Order", "IW_TC_012");
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Inbound Loan Order page")
	@Step("Login, Navigate to inventory and warehousing")
	public void verifyNavigateToInboundLoanOrderOutboundShipment() throws Exception {
		navigateToInboundLoanOrderOutboundShipment();
	}
	
	@Test(dependsOnMethods = "verifyNavigateToInboundLoanOrderOutboundShipment")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Inbound Loan Order form")
	public void verifyNavigateNewInboundLoanOrderForm() throws Exception {
		navigateNewInboundLoanOrderForm();
	}

	@Test(dependsOnMethods = "verifyNavigateNewInboundLoanOrderForm")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill Inbound Loan Order form")
	public void verifyFillInboundLOanOrderForm() throws Exception {
		fillInboundLOanOrderForm();
	}

	@Test(dependsOnMethods = "verifyFillInboundLOanOrderForm")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Inbound Loan Order draft and releese")
	public void verifyInboundLOanOrderDraftAndReleese() throws Exception {
		inboundLOanOrderDraftAndReleese();
	}

	@Test(dependsOnMethods = "verifyInboundLOanOrderDraftAndReleese")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Inbound Shipment draft and releese")
	public void verifyInbpondShipmentInboundLOanOrderDraftAndReleese() throws Exception {
		inbpondShipmentInboundLOanOrderCheckoutDraftAndReleese();
	}
	
	@Test(dependsOnMethods = "verifyInbpondShipmentInboundLOanOrderDraftAndReleese")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check product availability")
	public void verifyCheckWarehouseForProductAvailabilityInboundLoanOrder() throws Exception {
		checkWarehouseForProdctAvailabilityInboundLoanOrder();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}
}
