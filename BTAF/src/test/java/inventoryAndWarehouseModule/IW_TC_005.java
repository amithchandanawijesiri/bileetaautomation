package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })
public class IW_TC_005 extends InventoryAndWarehouseModule {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Verify that User can be able to do Stock Adjustmen",
				"IW_TC_005");

	}

//	@Test(dependsOnMethods = "inventoryAndWarehouseModule.IW_TC_003.verifyCreateSerielFifoProduct")
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify user can navigate to new stock adjustment form")
	@Step("Login, Navigate to inventory and warehousing")
	public void verifyNavigateToStockAdjustmentForm() throws Exception {
		navigateToStockAdjustmentForm();
	}

	@Test(dependsOnMethods = "verifyNavigateToStockAdjustmentForm")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify user can complete stock adjustment form")
	public void stockAdjustmentFormFilling() throws Exception {
		stockAdjustmentFormFill();
	}

	@Test(dependsOnMethods = "stockAdjustmentFormFilling")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft, seriel Batch capture and release Stock Adjustment")
	public void verifyDraftSerielBatchCaptureAndReleaseStockAdjustment() throws Exception {
		draftSerielBatchCaptureAndReleaseStockAdjustment();
	}

	@Test(dependsOnMethods = "verifyDraftSerielBatchCaptureAndReleaseStockAdjustment")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check warehouses for stock availability")
	public void verifyCheckWarehouseForProdctAvailability() throws Exception {
		checkWarehouseForProdctAvailability();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
