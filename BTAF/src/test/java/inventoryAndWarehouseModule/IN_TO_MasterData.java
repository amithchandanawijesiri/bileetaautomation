package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_MasterData extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();
	InventoryAndWarehouseModule obj = new InventoryAndWarehouseModule();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order", "Master Data", this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Master Creations - Warehouse 01")
	@Step("Login to the Entution")
	public void verifyCreateWarehouse01Regression() throws Exception {
		createWarehouse01Regression();
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Master Creations - Warehouse 02")
	@Step("Login to the Entution")
	public void verifyCreateWarehouse02Regression() throws Exception {
		createWarehouse02Regression();
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Master Creations - Warehouse 03 - Not Auto Lot Geanarate")
	@Step("Login to the Entution")
	public void verifyCreateWarehouse03RegressionNotAutoLotGenarate() throws Exception {
		createWarehouse03RegressionNotAutoLotGenarate();
	}

	@Test(dependsOnMethods = "verifyCreateWarehouse01Regression")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Master Creations - Common Warehouse")
	public void verifyCreateNewWarehouse() throws Exception {
		getInvObjReg().createNewWarehouse();
	}

	@Test(dependsOnMethods = "verifyCreateNewWarehouse")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Master Creations - Common Menufacturer")
	public void verifyCreateCommonMenufacturer() throws Exception {
		obj.menufacturerCommon();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
