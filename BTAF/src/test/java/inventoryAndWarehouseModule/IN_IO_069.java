package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_IO_069 extends InventoryAndWarehouseModuleRegression{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"inventoryAndWarehouseModule.AddQuantity_03.verifyAddQuantityThroughStockAdjustment"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Product Conversion fro dispatch")
	@Step("Login to the Entution")
	public void verifyProductConversionForDispatch_IN_IO_069() throws Exception {
		productConversionForDispatch_IN_IO_069();
	}
	
	@Test(dependsOnMethods = "verifyProductConversionForDispatch_IN_IO_069")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internel Order with converted product")
	public void verifyInternelOrder_IN_IO_069() throws Exception {
		internelOrder_IN_IO_069();
	}
	
	@Test(dependsOnMethods = "verifyInternelOrder_IN_IO_069")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internel Dispatch Order with converted product")
	public void verifyInternelDispatchOredr_IN_IO_069() throws Exception {
		internelDispatchOredr_IN_IO_069();
	}
	
	@Test(dependsOnMethods = "verifyInternelDispatchOredr_IN_IO_069")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify quantity deduction")
	public void verifyAndComapreTheStockThatTranferedFromTheWarehouse_IN_IO_069() throws Exception {
		comapreTheStockThatTranferedFromTheWarehouse_IN_IO_069();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
