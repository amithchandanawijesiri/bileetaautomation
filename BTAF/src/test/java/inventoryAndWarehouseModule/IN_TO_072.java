package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_072 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that user can be able to transfer serial/batches which inbound via Stock Take [Stock Adjustment]",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Stock Take")
	@Step("Login to the Entution")
	public void verifyStockTake_IN_TO_072() throws Exception {
		stockTake_IN_TO_072();
	}

	@Test(dependsOnMethods = "verifyStockTake_IN_TO_072")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Plus Stock Adjustment")
	public void verifyStockAdjustment_IN_TO_072() throws Exception {
		stockAdjustment_IN_TO_072();
	}

	@Test(dependsOnMethods = "verifyStockAdjustment_IN_TO_072")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order")
	public void verifyTransferOrder_IN_TO_072() throws Exception {
		transferOrder_IN_TO_072();
	}

	@Test(dependsOnMethods = "verifyTransferOrder_IN_TO_072")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Outbound Shipment")
	public void verifyOutboundShipmen_IN_TO_072() throws Exception {
		outboundShipmen_IN_TO_072();
	}

	@Test(dependsOnMethods = "verifyOutboundShipmen_IN_TO_072")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Inbound Shipment")
	public void verifyInboundShipment_IN_TO_072() throws Exception {
		inboundShipment_IN_TO_072();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
