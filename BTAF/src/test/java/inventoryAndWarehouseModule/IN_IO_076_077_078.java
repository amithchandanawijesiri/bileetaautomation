package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_IO_076_077_078 extends InventoryAndWarehouseModuleRegression{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"inventoryAndWarehouseModule.AddQuantity_03.verifyAddQuantityThroughStockAdjustment"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internel Order")
	@Step("Login to the Entution")
	public void verifyInternelOrder_IN_IO_076_077_078() throws Exception {
		internelOrder_IN_IO_076_077_078();
	}
	
	@Test(dependsOnMethods = "verifyInternelOrder_IN_IO_076_077_078")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internel Dispatch Order")
	public void verifyInternelDispatchOrder_IN_IO_076_077_078() throws Exception {
		internelDispatchOrder_IN_IO_076_077_078();
	}
	
	@Test(dependsOnMethods = "verifyInternelDispatchOrder_IN_IO_076_077_078")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify Journel Entry")
	public void verifyCheckJournelEntrySection_IN_IO_076_077_078() throws Exception {
		checkJournelEntrySection_IN_IO_076_077_078();
	}
	
	@Test(dependsOnMethods = "verifyCheckJournelEntrySection_IN_IO_076_077_078")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify History Details")
	public void verifyCheckHistoryDetails_IN_IO_076_077_078() throws Exception {
		checkHistoryDetails_IN_IO_076_077_078();
	}
	
	@Test(dependsOnMethods = "verifyCheckHistoryDetails_IN_IO_076_077_078")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify Dockflow avilability")
	public void verifyAndCheckDockflow_IN_IO_076_077_078() throws Exception {
		checkDockflow_IN_IO_076_077_078();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
