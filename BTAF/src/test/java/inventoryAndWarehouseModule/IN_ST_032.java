package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression3;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_ST_032 extends InventoryAndWarehouseModuleRegression3 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Stock Take",
				"Verify that user cannot be done any transactions with the particular warehouse when stock take is in draft stage",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Stock Adjustment for new warehouse")
	@Step("Login to the Entution")
	public void verifyStockAdjustmentForStockTakeWarehouse_IN_ST_032() throws Exception {
		stockAdjustmentForStockTakeWarehouse_IN_ST_032();
	}
	
	@Test(dependsOnMethods = "verifyStockAdjustmentForStockTakeWarehouse_IN_ST_032")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft Stock Take")
	public void verifyDraftStockTake_IN_ST_032() throws Exception {
		draftStockTake_IN_ST_032();
	}
	
	@Test(dependsOnMethods = "verifyDraftStockTake_IN_ST_032")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Sales Order")
	public void verifySalesOrder_IN_ST_032() throws Exception {
		salesOrder_IN_ST_032();
	}
	
	@Test(dependsOnMethods = "verifySalesOrder_IN_ST_032")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check validation")
	public void verifyOutboundShipment_IN_ST_032() throws Exception {
		outboundShipment_IN_ST_032();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}

}
