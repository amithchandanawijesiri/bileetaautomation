package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_050 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that captured batch/serials will be outbounded/inbounded accordingly regardless of the form serial/batch captured [ Capturing in Transfer Order ]",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {
			"inventoryAndWarehouseModule.AddQuantityGBV_01.verifyAddQuantityThroughStockAdjustment" })
	@Severity(SeverityLevel.CRITICAL)
	@Description("Transfer Order")
	@Step("Login to the Entution")
	public void verifyTransferOrder_IN_TO_050() throws Exception {
		transferOrder_IN_TO_050();
	}

	@Test(dependsOnMethods = "verifyTransferOrder_IN_TO_050")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify capture properties Transfer Order")
	@Step("Login to the Entution")
	public void verifyThatSerielLotExpiryInTransferOrder_IN_TO_050() throws Exception {
		verifySerielLotExpiryInTransferOrder_IN_TO_050();
	}

	@Test(dependsOnMethods = "verifyThatSerielLotExpiryInTransferOrder_IN_TO_050")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Outbound Shipment")
	public void verifyOutboundShipment_IN_TO_050() throws Exception {
		outboundShipment_IN_TO_050();
	}

	@Test(dependsOnMethods = "verifyOutboundShipment_IN_TO_050")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify capture properties - Outbound Shipment")
	public void verifyAndCheckBatchLotExpiryInOutboundShipment() throws Exception {
		checkBatchLotExpiryInOutboundShipment();
	}

	@Test(dependsOnMethods = "verifyAndCheckBatchLotExpiryInOutboundShipment")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Inbound Shipment")
	public void verifyInboundShipment_IN_TO_050() throws Exception {
		inboundShipment_IN_TO_050();
	}

	@Test(dependsOnMethods = "verifyInboundShipment_IN_TO_050")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify capture properties Inbound Shipment")
	public void verifyAndCheckCapturedBatchLotAndExpiryWithInboundShipment_IN_TO_050() throws Exception {
		checkCapturedBatchLotAndExpiryWithInboundShipment_IN_TO_050();
	}

	@Test(dependsOnMethods = "verifyAndCheckCapturedBatchLotAndExpiryWithInboundShipment_IN_TO_050")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check bincard reports")
	public void verifyAndCheckBinCardReport_IN_TO_050() throws Exception {
		checkBinCardReport_IN_TO_050();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
