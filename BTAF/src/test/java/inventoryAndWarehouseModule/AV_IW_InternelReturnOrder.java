package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class AV_IW_InternelReturnOrder extends InventoryAndWarehouseModule{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Action Verification - Internel Return Order",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Internel Return Order")
	@Step("Login, Navigate to Inventory And Warehouse Module")
	public void verifyNavigateToInterneReturnOrder1() throws Exception {

		navigateToInterneReturnOrder1();

	}
	@Test(dependsOnMethods = "verifyNavigateToInterneReturnOrder1")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Internel Return Order")
	public void verifyNavigateToInterneReturnOrder2() throws Exception {

		navigateToInterneReturnOrder2();

	}
	
	@Test(dependsOnMethods = "verifyNavigateToInterneReturnOrder2")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Internel Return Order")
	public void verifyNavigateToInterneReturnOrder3() throws Exception {

		navigateToInterneReturnOrder3();

	}
	
	@Test(dependsOnMethods = "verifyNavigateToInterneReturnOrder3")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Internel Return Order")
	public void verifyNavigateToInterneReturnOrder4() throws Exception {

		navigateToInterneReturnOrder4();

	}
	
	@Test(dependsOnMethods = "verifyNavigateToInterneReturnOrder4")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Action Verification")
	public void verifyActionVerificationInternelReturnOrder() throws Exception {

		actionVerificationInternelReturnOrder();

	}
	
	@Test(dependsOnMethods = "verifyActionVerificationInternelReturnOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Action Section Verification")
	public void verifyActionSectionVerificationInternelReturnOrder() throws Exception {

		actionSectionVerificationInternelReturnOrder();

	}
	
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
//		common.driverClose();

	}

}
