package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_087 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that user cannot capture more than batch qty in Transfer Order form",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Journey configuration")
	@Step("Login to the Entution")
	public void verifyConfigureAutomaticReservationAndValidateStockEnabled_IN_TO_087() throws Exception {
		configureAutomaticReservationAndValidateStockEnabled_IN_TO_087();
	}

	@Test(dependsOnMethods = "verifyConfigureAutomaticReservationAndValidateStockEnabled_IN_TO_087")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create new warehouse")
	public void verifyCreateNewWarehouse_IN_TO_087() throws Exception {
		createNewWarehouse_IN_TO_087();
	}
	
	@Test(dependsOnMethods = "verifyCreateNewWarehouse_IN_TO_087")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Stock Adjustment")
	public void verifyStockAdjustment_IN_TO_087() throws Exception {
		stockAdjustment_IN_TO_087();
	}

	@Test(dependsOnMethods = "verifyStockAdjustment_IN_TO_087")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Transfer Order 01")
	public void verifyTransferOrder_01_IN_TO_087() throws Exception {
		transferOrder_01_IN_TO_087();
	}
	
	@Test(dependsOnMethods = "verifyTransferOrder_01_IN_TO_087")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Transfer Order 02")
	public void verifyTransferOrder_02_IN_TO_087() throws Exception {
		transferOrder_02_IN_TO_087();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		resetJourneyConfiguration();
		common.totalTime(startTime);
		common.driverClose();

	}

}
