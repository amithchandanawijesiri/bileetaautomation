package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression3;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_107 extends InventoryAndWarehouseModuleRegression3 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that user can use QC pass serial/Batch quantities in any transactions",
				this.getClass().getSimpleName());
	}
	
	@Test(dependsOnMethods = {"inventoryAndWarehouseModule.IN_TO_102.verifyAndCheckBincardAfterQCAcceptance_IN_TO_102"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Sales Order with QC passed quantity")
	@Step("Login to the Entution")
	public void verifySalesOrderWithQCPassQuantity_IN_TO_107() throws Exception {
		salesOrderWithQCPassQuantity_IN_TO_107();
	}
	
	@Test(dependsOnMethods = {"verifySalesOrderWithQCPassQuantity_IN_TO_107"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Outbound Shipment")
	@Step("Login to the Entution")
	public void verifyOutboundShipment_IN_TO_107() throws Exception {
		outboundShipment_IN_TO_107();
	}

	@Test(dependsOnMethods = {"verifyOutboundShipment_IN_TO_107"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Inbound Shipment")
	@Step("Login to the Entution")
	public void verifyInboundShipemtn_IN_TO_107() throws Exception {
		inboundShipemtn_IN_TO_107();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}

}
