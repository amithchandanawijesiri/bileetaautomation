package inventoryAndWarehouseModule;


	import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
	import org.testng.annotations.Test;

	import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
	import bileeta.BTAF.Utilities.TestCommonMethods;
	import io.qameta.allure.Description;
	import io.qameta.allure.Severity;
	import io.qameta.allure.SeverityLevel;

	import org.testng.annotations.Listeners;
	import bileeta.BTAF.Utilities.TestListener;
	import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})

	public class Con_TC_002 extends InventoryAndWarehouseModule {
		TestCommonMethods common = new TestCommonMethods();

		@BeforeClass
		public void setUp() throws Exception {
			common.setUp("Inventory and Warehousing", "", "Verify that user can successfully login ti the Entution",
					"Con_TC_002");
		}

		@Test
		@Severity(SeverityLevel.CRITICAL)
		@Description("Navigate to Inventory And Warehouse Module")
		public void verifyNavigateInventoryAndWarehouseModule() throws Exception {
			navigateInventoryAndWarehouseModule();
		}
		
		@AfterClass(alwaysRun = true)
		public void AfterClass() throws Exception {
			common.totalTime(startTime);
			common.driverClose();

		}


}
