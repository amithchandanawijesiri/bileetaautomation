package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression4;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_PC_036 extends InventoryAndWarehouseModuleRegression4 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Product Conversion",
				"Vreify that user can do product conversions with having backdate", this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Back date Stock Adjustment")
	public void verifyBackDateStockAdjustment_IN_PC_036() throws Exception {
		backDateStockAdjustment_IN_PC_036();
	}
	
	@Test(dependsOnMethods = "verifyBackDateStockAdjustment_IN_PC_036")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Release back date Product Conversion")
	public void verifyReleaseProductConversionWithBackDate_IN_PC_036() throws Exception {
		releaseProductConversionWithBackDate_IN_PC_036();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
