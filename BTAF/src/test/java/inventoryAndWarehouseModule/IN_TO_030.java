package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_030 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that user can reverse the released inbound shipment", this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = "inventoryAndWarehouseModule.AddQuantityGBV_01.verifyAddQuantityThroughStockAdjustment")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Reverse Inbound Shipement")
	@Step("Login to the Entution")
	public void verifyReverseRelesedInboundShipment01() throws Exception {
		reverseRelesedInboundShipment01();
	}

	@Test(dependsOnMethods = "verifyReverseRelesedInboundShipment01")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Reverse Inbound Shipement")
	@Step("Login to the Entution")
	public void verifyReverseRelesedInboundShipment02() throws Exception {
		reverseRelesedInboundShipment02();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
