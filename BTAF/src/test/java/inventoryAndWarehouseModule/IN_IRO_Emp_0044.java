package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_IRO_Emp_0044 extends InventoryAndWarehouseModuleRegression {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		setUpForDependent("Inventory and Warehousing", "", "", this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = { "inventoryAndWarehouseModule.AddQuantity_04.verifyAddQuantityThroughStockAdjustment",
			"inventoryAndWarehouseModule.IN_IRO_Emp_0043.verifyCheckHistory_IN_IRO_Emp_043" })
	@Severity(SeverityLevel.CRITICAL)
	@Step("Login to the Entution , Complete Internal Return Order journey")
	@Description("Verify Dockflow")
	public void verifyCheckDockflow_IN_IRO_Emp_044() throws Exception {
		checkDockflow_IN_IRO_Emp_044();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
