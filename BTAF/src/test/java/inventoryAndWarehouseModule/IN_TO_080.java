package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_080 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that warehouse code and description display in Trasnfer order / Outbound shipment/ Inbound shipment forms",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Configure warehouse code and description in balancing laval settings")
	@Step("Login to the Entution")
	public void verifyConfigureWarehouseCodeAndDescription_IN_TO_080() throws Exception {
		configureWarehouseCodeAndDescription_IN_TO_080();
	}

	@Test(dependsOnMethods = "verifyConfigureWarehouseCodeAndDescription_IN_TO_080")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order journey")
	public void verifyCompleteTransferOrderJourney_IN_TO_080() throws Exception {
		completeTransferOrderJourney_IN_TO_080();
	}

	@Test(dependsOnMethods = "verifyCompleteTransferOrderJourney_IN_TO_080")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check whether code and description configuration applied")
	public void verifyAndCheckCodeAndDescriptionOfWarehouseFieldsTransferOrderJourney_IN_TO_080() throws Exception {
		checkCodeAndDescriptionOfWarehouseFieldsTransferOrderJourney_IN_TO_080();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
