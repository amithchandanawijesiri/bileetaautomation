package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class AV_IW_ProductInformation extends InventoryAndWarehouseModule {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Action Verification - Product Information",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to the Product Information and verify Copy From action")
	@Step("Login to the Entution, Navigate to Inventory And Warehouse Module")
	public void verifyNavigateProductInformationAndVerifyCopyFromActionVerification() throws Exception {

		navigateProductInformationAndVerifyCopyFromActionVerification();

	}

	@Test(dependsOnMethods = "verifyNavigateProductInformationAndVerifyCopyFromActionVerification")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill product information form")
	public void verifyCerateNewProductActionVerification() throws Exception {

		cerateNewProductActionVerification();

	}

	@Test(dependsOnMethods = "verifyCerateNewProductActionVerification")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Action Verification - Product Information")
	public void verifyActionsProductInformation() throws Exception {

		actionsProductInformation();

	}

	@Test(dependsOnMethods = "verifyActionsProductInformation")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Action Section Verification - Product Information")
	public void verifyActionSectionVerificationProductInformation() throws Exception {

		actionSectionVerificationProductInformation();

	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
