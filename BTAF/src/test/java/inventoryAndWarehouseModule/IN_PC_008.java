package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression4;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_PC_008 extends InventoryAndWarehouseModuleRegression4 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Product Conversion",
				"Verify that Last Cost price for the selected product will dispaly accordingly", this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check Cost Price with product master")
	@Step("Login to the Entution")
	public void verifyAndCheckAndStoreLastCostPriceOfTheProduct_IN_PC_008() throws Exception {
		checkAndStoreLastCostPriceOfTheProduct_IN_PC_008();
	}
	
	@Test(dependsOnMethods = "verifyAndCheckAndStoreLastCostPriceOfTheProduct_IN_PC_008")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Compare and verify Last Cost Price")
	@Step("Login to the Entution")
	public void verifyCompareCostPriceWithGridLastCostPrice_IN_PC_008() throws Exception {
		compareCostPriceWithGridLastCostPrice_IN_PC_008();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
