package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_068 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that serials/batches generated via Barcode Generator, can be transfer from one warehouse to another",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create dispatch only products / Purshase Order / Barcode Genarate")
	@Step("Login to the Entution")
	public void verifyGenarateBarcode_IN_TO_068() throws Exception {
		genarateBarcode_IN_TO_068();
	}

	@Test(dependsOnMethods = "verifyGenarateBarcode_IN_TO_068")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order")
	public void verifyTransferOrder_IN_TO_068() throws Exception {
		transferOrder_IN_TO_068();
	}

	@Test(dependsOnMethods = "verifyTransferOrder_IN_TO_068")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Outbound Shipment")
	public void verifyOutboundShipmen_IN_TO_068() throws Exception {
		outboundShipmen_IN_TO_068();
	}

	@Test(dependsOnMethods = "verifyOutboundShipmen_IN_TO_068")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Inbound Shipment")
	public void verifyInboundShipment_IN_TO_068() throws Exception {
		inboundShipment_IN_TO_068();
	}

	@Test(dependsOnMethods = "verifyInboundShipment_IN_TO_068")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check stock updatance")
	public void verifyAndCheckStockUpdatance_068() throws Exception {
		checkStockUpdatance_068();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
