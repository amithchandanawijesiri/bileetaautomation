package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_042 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that user allow to complete the trasnfer order with multiple outbound shipments and multiple inbound shipments",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {
			"inventoryAndWarehouseModule.IN_TO_032_034.verifySetStockReservationManuelForTransferOrderJourney",
			"inventoryAndWarehouseModule.AddQuantityGBV_01.verifyAddQuantityThroughStockAdjustment" })
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order journey with multiple Outbound Shipments and multiple Inbound Shipments")
	@Step("Login to the Entution")
	public void verifyCompleteTransferOrderJourneyWithMultipleOutboundShipmentAndMultipleInboundShipments01()
			throws Exception {
		completeTransferOrderJourneyWithMultipleOutboundShipmentAndMultipleInboundShipments01();
	}

	@Test(dependsOnMethods = "verifyCompleteTransferOrderJourneyWithMultipleOutboundShipmentAndMultipleInboundShipments01")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order journey with multiple Outbound Shipments and multiple Inbound Shipments")
	public void verifyCompleteTransferOrderJourneyWithMultipleOutboundShipmentAndMultipleInboundShipments02()
			throws Exception {
		completeTransferOrderJourneyWithMultipleOutboundShipmentAndMultipleInboundShipments02();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
