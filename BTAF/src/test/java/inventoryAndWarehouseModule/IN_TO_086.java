package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_086 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that user cannot capture more than batch qty in Trasnfer Order",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create new warehouse")
	@Step("Login to the Entution")
	public void verifyCreateNewWarehouse_IN_TO_086() throws Exception {
		createNewWarehouse_IN_TO_086();
	}

	@Test(dependsOnMethods = "verifyCreateNewWarehouse_IN_TO_086")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Stock Adjustment")
	public void verifyStockAdjustment_IN_TO_086() throws Exception {
		stockAdjustment_IN_TO_086();
	}
	
	@Test(dependsOnMethods = "verifyStockAdjustment_IN_TO_086")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Journey Configuration")
	public void verifyJourneyConfiguration_IN_TO_086() throws Exception {
		journeyConfiguration_IN_TO_086();
	}

	@Test(dependsOnMethods = "verifyJourneyConfiguration_IN_TO_086")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Transfer Order")
	public void verifyTransferOrderWithQuantityGreaterThanOverHead_IN_TO_086() throws Exception {
		transferOrderWithQuantityGreaterThanOverHead_IN_TO_086();
	}
	
	@Test(dependsOnMethods = "verifyTransferOrderWithQuantityGreaterThanOverHead_IN_TO_086")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check validation")
	public void verifyAndCheckValidation_IN_TO_086() throws Exception {
		checkValidation_IN_TO_086();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		resetJourneyConfiguration_IN_TO_086();
		common.totalTime(startTime);
		common.driverClose();

	}

}
