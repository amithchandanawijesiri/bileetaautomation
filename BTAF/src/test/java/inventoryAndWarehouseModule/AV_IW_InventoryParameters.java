package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;

public class AV_IW_InventoryParameters extends InventoryAndWarehouseModule{
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Action Verification - Inventory Parameters",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to the Inventory Parameter page")
	@Step("Login to the Entution, Navigate to Inventory And Warehouse Module")
	public void verifyNavigateToInventoryParameters() throws Exception {

		navigateToInventoryParameters();

	}
	
	@Test(dependsOnMethods = "verifyNavigateToInventoryParameters")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Validate update button")
	public void verifyValidateUpdateButtonInventoryParameterActionVerfication() throws Exception {

		validateUpdateButtonInventoryParameterActionVerfication();

	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
