package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_IO_025_I_026_I_027 extends InventoryAndWarehouseModule{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"inventoryAndWarehouseModule.AddQuantity_01.verifyAddQuantityThroughStockAdjustment"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill Internel Order")
	@Step("Login to the Entution")
	public void verifyFillInternelOrderForIN_IO_025_026_027() throws Exception {
		fillInternelOrderForIN_IO_025_026_027();

	}
	
	@Test(dependsOnMethods = "verifyFillInternelOrderForIN_IO_025_026_027")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Darft / Release and Seriel/Batch Capture")
	public void verifyDraftReleaseAndBatchCapture_IN_IO_025_I_026_027() throws Exception {
		draftReleaseAndBatchCapture_IN_IO_025_I_026_027();

	}
	
	@Test(dependsOnMethods = "verifyDraftReleaseAndBatchCapture_IN_IO_025_I_026_027")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify stock deduction")
	public void verifyComapreTheStockThatTranferedFromTheWarehouse_IN_IO_025_I_026_I_027() throws Exception {
		comapreTheStockThatTranferedFromTheWarehouse_IN_IO_025_I_026_I_027();

	}
	
	@Test(dependsOnMethods = "verifyComapreTheStockThatTranferedFromTheWarehouse_IN_IO_025_I_026_I_027")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify costing according to the deducted quantity")
	public void verifyValidateCosting_IN_IO_025_I_026_I_027() throws Exception {
		validateCosting_IN_IO_025_I_026_I_027();

	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
