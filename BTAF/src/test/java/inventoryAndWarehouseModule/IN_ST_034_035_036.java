package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression3;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_ST_034_035_036 extends InventoryAndWarehouseModuleRegression3 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Stock Take",
				"Verify that user allow to convert the stock take in to stock adjustment fully AND Verify that RefDoc display accordingly in stock adjustment AND Verify that stock update according to the \"+\" or \"-\" adjustments AND Verify that convert to stock adjustment action will not display when complete the stock adjustment  for the relevant stock take",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Stock Take")
	@Step("Login to the Entution")
	public void verifyStockTake_IN_ST_034_035_036() throws Exception {
		stockTake_IN_ST_034_035_036();
	}

	@Test(dependsOnMethods = "verifyStockTake_IN_ST_034_035_036")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Stock Adjustment")
	public void verifyStockAdjustment_IN_ST_034_035_036() throws Exception {
		stockAdjustment_IN_ST_034_035_036();
	}
	
	@Test(dependsOnMethods = "verifyStockAdjustment_IN_ST_034_035_036")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify stock update")
	public void verifyAndCheckStockUpdations_IN_ST_034_035_036() throws Exception {
		checkStockUpdations_IN_ST_034_035_036();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}

}
