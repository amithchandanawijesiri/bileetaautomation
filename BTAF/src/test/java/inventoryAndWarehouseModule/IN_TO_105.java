package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression3;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_105 extends InventoryAndWarehouseModuleRegression3 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that user not allow to do QC acceptance for QC disabled products",
				this.getClass().getSimpleName());
	}

	
	
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create QC disabled products and complete Stock Adjustment")
	@Step("Login to the Entution")
	public void verifyCreateCommonQCDisabledProductsAndStockAdjustment_IN_TO_105() throws Exception {
		createCommonQCDisabledProductsAndStockAdjustment_IN_TO_105();
	}
	
	@Test(dependsOnMethods = {"verifyCreateCommonQCDisabledProductsAndStockAdjustment_IN_TO_105"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order")
	@Step("Login to the Entution")
	public void verifyTransferOrder_IN_TO_105() throws Exception {
		transferOrder_IN_TO_105();
	}

	@Test(dependsOnMethods = "verifyTransferOrder_IN_TO_105")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Outbound Shipment")
	public void verifyOutboundShipmen_IN_TO_105() throws Exception {
		outboundShipmen_IN_TO_105();
	}
	
	@Test(dependsOnMethods = "verifyOutboundShipmen_IN_TO_105")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Inbound Shipment")
	public void verifyInboundShipment_IN_TO_105() throws Exception {
		inboundShipment_IN_TO_105();
	}
	
	@Test(dependsOnMethods = "verifyInboundShipment_IN_TO_105")
	@Severity(SeverityLevel.CRITICAL)
	@Description("QC Acceptance process")
	public void verifyQcAcceptanceCheck_IN_TO_105() throws Exception {
		qcAcceptanceCheck_IN_TO_105();
	}
	
	@Test(dependsOnMethods = "verifyQcAcceptanceCheck_IN_TO_105")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check stock updatance")
	public void verifyAndCheckStockUpdateAccordingly_IN_TO_105() throws Exception {
		checkStockUpdateAccordingly_IN_TO_105();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		removeQCAcceptanceTransferOrderToInboundShipment();
		common.totalTime(startTime);
		common.driverClose();
	}

}
