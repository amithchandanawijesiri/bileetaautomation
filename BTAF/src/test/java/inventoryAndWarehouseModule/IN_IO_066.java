package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_IO_066 extends InventoryAndWarehouseModuleRegression{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Internal Order", "Verify that batches are loading only relevant to the Post Date when dispatch via Internal Dispatch Order",
				this.getClass().getSimpleName());
		
	}

	@Test(dependsOnMethods = {"inventoryAndWarehouseModule.AddQuantity_03.verifyAddQuantityThroughStockAdjustment"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Purchase Order with today Post Date")
	@Step("Login to the Entution")
	public void verifyPurchaseOrderToPurchaseInvoiceSingleSerielProductTodayAsPostDate() throws Exception {
		purchaseOrderToPurchaseInvoiceSingleSerielProductTodayAsPostDate();
	}
	
	@Test(dependsOnMethods = "verifyPurchaseOrderToPurchaseInvoiceSingleSerielProductTodayAsPostDate")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Purchase Order with earlier date as Post Date")
	public void verifyPurchaseOrderToPurchaseInvoiceSingleSerielProductBeforePostDate() throws Exception {
		purchaseOrderToPurchaseInvoiceSingleSerielProductBeforePostDate();
	}
	
	@Test(dependsOnMethods = "verifyPurchaseOrderToPurchaseInvoiceSingleSerielProductBeforePostDate")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internel Order and verify Post Date")
	public void verifyInternelOrderForVerifyWithPostDate() throws Exception {
		internelOrderForVerifyWithPostDate();
	}
	
	@Test(dependsOnMethods = "verifyInternelOrderForVerifyWithPostDate")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Internel Dispatch Order and verify batches")
	public void verifyDraftReleaseIOAndDrftReleaseInternelDispatchOrderAndVerifyWithPostDate() throws Exception {
		draftReleaseIOAndDrftReleaseInternelDispatchOrderAndVerifyWithPostDate();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
