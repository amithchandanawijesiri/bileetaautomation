package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})
public class IW_TC_007 extends InventoryAndWarehouseModule{
	
		TestCommonMethods common = new TestCommonMethods();

		@BeforeClass
		public void setUp() throws Exception {
			common.setUp("Inventory and Warehousing", "", "Verify that User can be able to transfer product From one warehouse to another warehouse through Interdepartmental transfer order",
					"IW_TC_007");
		}
		
		@Test
		@Severity(SeverityLevel.CRITICAL)
		@Description("Verify user can navigate Inter Department Transfer Order page")
		@Step("Login, Navigate to inventory and warehousing")
		public void verifyNavigateToInterDepartmentTransferOrderPage() throws Exception {
			navigateToInterDepartmentTransferOrderPage();
		}
		
		@Test(dependsOnMethods = "verifyNavigateToInterDepartmentTransferOrderPage")
		@Severity(SeverityLevel.CRITICAL)
		@Description("Verify user can navigate to new  Inter Department Transfer Order order form")
		public void verifyNavigateToInterDepartmentTransferOrderForm() throws Exception {
			navigateToInterDepartmentPurchaseOrderForm();
		}
		
		@Test(dependsOnMethods = "verifyNavigateToInterDepartmentTransferOrderForm")
		@Severity(SeverityLevel.CRITICAL)
		@Description("Verify user can fill Inter Department Transfer Order form")
		public void verifyCompleteInterDepartmentTransferOrder() throws Exception {
			completeInterDepartmentTransferOrder();
		}
		
		@Test(dependsOnMethods = "verifyCompleteInterDepartmentTransferOrder")
		@Severity(SeverityLevel.CRITICAL)
		@Description("Verify user can relese Inter Department Transfer Order form")
		public void verifyReleseInterDepartmentTransferOrder() throws Exception {
			releseInterDepartmentTransferOrder();
		}
		
		@Test(dependsOnMethods = "verifyReleseInterDepartmentTransferOrder")
		@Severity(SeverityLevel.CRITICAL)
		@Description("Draft,release outbound shipment and Seriel Batch Capture")
		public void verifyDraftReleaseOutboundShipmentAndSerielBatchCapture() throws Exception {
			draftReleaseOutboundShipmentAndSerielBatchCapture();
		}
		
		@Test(dependsOnMethods = "verifyDraftReleaseOutboundShipmentAndSerielBatchCapture")
		@Severity(SeverityLevel.CRITICAL)
		@Description("Draft and Release Inbound Shipment")
		public void verifyInboundShipmentInterDepartmentTransferOrder() throws Exception {
			inboundShipmentInterDepartmentTransferOrder();
		}
		
		@Test(dependsOnMethods = "verifyInboundShipmentInterDepartmentTransferOrder")
		@Severity(SeverityLevel.CRITICAL)
		@Description("Check product availability deduction")
		public void verifyCheckWarehouseForProdctAvailabilityInterDepartmentalTransferOrderTransferOrder() throws Exception {
			checkWarehouseForProdctAvailabilityInterDepartmentalTransferOrderTransferOrder();
		}
		
		@AfterClass(alwaysRun = true)
		public void AfterClass() throws Exception {
			common.totalTime(startTime);
			common.driverClose();

		}
		
}
