package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class AV_IW_AllocationManager extends InventoryAndWarehouseModule{

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Action Verification - Allocation Manager",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to the new warehouse form and verify 'Copy From' button")
	@Step("Login, Navigate to Inventory And Warehouse Module")
	public void verifyNavigateToNewWarehouseFormAndVerifyCopyFormActionVerification() throws Exception {
		
		navigateNewWarehouseFormAndVerifyCopyFormActionVerification();

	}

	@Test(dependsOnMethods = "verifyNavigateToNewWarehouseFormAndVerifyCopyFormActionVerification")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill new warehouse form")
	public void verifyFillwarehouseInformationFormActionVerification() throws Exception {

		fillwarehouseInformationFormActionVerification();

	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
