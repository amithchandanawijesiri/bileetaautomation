package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression3;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_ST_051 extends InventoryAndWarehouseModuleRegression3 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Stock Take",
				"Verify that user can do stock take for converted product Qty via product conversion and stock update properly",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Stock Adjustment for new warehouse")
	@Step("Login to the Entution")
	public void verifyStockAdjustmentForNewWarehouse_IN_ST_051() throws Exception {
		stockAdjustmentForNewWarehouse_IN_ST_051();
	}

	@Test(dependsOnMethods = "verifyStockAdjustmentForNewWarehouse_IN_ST_051")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Product Conversion")
	public void verifyProductConversion_IN_ST_051() throws Exception {
		productConversion_IN_ST_051();
	}

	@Test(dependsOnMethods = "verifyProductConversion_IN_ST_051")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Stock Take")
	public void verifyStockTake_IN_ST_051() throws Exception {
		stockTake_IN_ST_051();
	}

	@Test(dependsOnMethods = "verifyStockTake_IN_ST_051")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete converted Stock Adjustment")
	public void verifyReleaseStockAdjustment_IN_ST_051() throws Exception {
		releaseStockAdjustment_IN_ST_051();
	}
	
	@Test(dependsOnMethods = "verifyReleaseStockAdjustment_IN_ST_051")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check stock update")
	public void verifyAndCheckStockUpdate_IN_ST_051() throws Exception {
		checkStockUpdate_IN_ST_051();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}

}
