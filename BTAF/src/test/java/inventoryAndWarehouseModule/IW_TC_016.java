package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})


public class IW_TC_016 extends InventoryAndWarehouseModule{
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "", "Verify that user can be able to create Assembly Cost Estimation",
				"IW_TC_016");
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Assembly Cost Estimation Form")
	@Step("Login, Navigate to Inventory and Warehousing")
	public void verifyNavigateToAccemblyCostEstimationForm() throws Exception {
		navigateToAccemblyCostEstimationForm();
	}
	
	
	
	@Test(dependsOnMethods = "verifyNavigateToAccemblyCostEstimationForm")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill Assembly Cost Estimation Form")
	public void verifyFillAssemblyCostEstimationForm() throws Exception {
		fillAssemblyCostEstimationFormSummary();
	}
	
	@Test(dependsOnMethods = "verifyFillAssemblyCostEstimationForm")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill Assembly Cost Estimation details")
	public void verifyFilldeatailsInDetailsTab() throws Exception {
		filldeatailsInDetailsTab();
	}
	
	@Test(dependsOnMethods = "verifyFilldeatailsInDetailsTab")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Checkout, Draft And Releese")
	public void verifyCheckoutDraftReleseACE() throws Exception {
		checkoutDraftReleseACE();
	}
	
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}
}
