package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression3;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_ST_059 extends InventoryAndWarehouseModuleRegression3 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Stock Take",
				"Verify that only product code display in product grid of stock take and stock adjustment",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check Stock Take with product code only")
	public void verifyStockTake_IN_ST_059() throws Exception {
		stockTake_IN_ST_059();
	}
	
	@Test(dependsOnMethods = "verifyStockTake_IN_ST_059")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check Stock Adjustment with product code only")
	public void verifyStockAdjustment_IN_ST_059() throws Exception {
		stockAdjustment_IN_ST_059();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}

}
