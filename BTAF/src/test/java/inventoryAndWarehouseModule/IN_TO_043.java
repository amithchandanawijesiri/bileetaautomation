package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_043 extends InventoryAndWarehouseModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that user allow to complete the trasnfer order with multiple outbound shipments and multiple inbound shipments",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {
			"inventoryAndWarehouseModule.IN_TO_032_034.verifySetStockReservationManuelForTransferOrderJourney",
			"inventoryAndWarehouseModule.AddQuantityGBV_01.verifyAddQuantityThroughStockAdjustment" })
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Transfer Order")
	@Step("Login to the Entution")
	public void verifyCompleteTransferOrder_IN_TO_043() throws Exception {
		completeTransferOrder_IN_TO_043();
	}

	@Test(dependsOnMethods = "verifyCompleteTransferOrder_IN_TO_043")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Outbound Shipment")
	public void verifyOutboundShipment01_IN_TO_043() throws Exception {
		outboundShipment01_IN_TO_043();
	}

	@Test(dependsOnMethods = "verifyOutboundShipment01_IN_TO_043")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Inbound Shipment")
	public void verifyInboundShipment01_IN_TO_043() throws Exception {
		inboundShipment01_IN_TO_043();
	}

	@Test(dependsOnMethods = "verifyInboundShipment01_IN_TO_043")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Outbound Shipment")
	public void verifyOutboundShipment02_IN_TO_043() throws Exception {
		outboundShipment02_IN_TO_043();
	}

	@Test(dependsOnMethods = "verifyOutboundShipment02_IN_TO_043")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Inbound Shipment")
	public void verifyInboundShipent02_IN_TO_043() throws Exception {
		inboundShipent02_IN_TO_043();
	}

	@Test(dependsOnMethods = "verifyInboundShipent02_IN_TO_043")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check stock updatance")
	public void verifyCheckStockUpdateAccordingly_IN_TO_043() throws Exception {
		checkStockUpdateAccordingly_IN_TO_043();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
