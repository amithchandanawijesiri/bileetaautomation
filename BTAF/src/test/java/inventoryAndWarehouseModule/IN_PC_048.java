package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression4;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_PC_048 extends InventoryAndWarehouseModuleRegression4 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Product Conversion",
				"Verify that converted product stock can use for other transactions", this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Stock Adjustment for new warehouse")
	public void verifyStockAdjustmentForNewWarehouse_IN_PC_048() throws Exception {
		stockAdjustmentForNewWarehouse_IN_PC_048();
	}
	
	@Test(dependsOnMethods = "verifyStockAdjustmentForNewWarehouse_IN_PC_048")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Product Conversion")
	public void verifyProductConversion_IN_PC_048() throws Exception {
		productConversion_IN_PC_048();
	}
	
	@Test(dependsOnMethods = "verifyProductConversion_IN_PC_048")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Use converted Seriels/Batches for Internal Order to Internal Dispatch Order")
	public void verifyInternalOrderAndInternalDispatchOrder_IN_PC_048() throws Exception {
		internalOrderAndInternalDispatchOrder_IN_PC_048();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
