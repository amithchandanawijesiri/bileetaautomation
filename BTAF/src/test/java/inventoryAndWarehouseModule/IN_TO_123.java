package inventoryAndWarehouseModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.InventoryAndWarehouseModuleRegression3;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class IN_TO_123 extends InventoryAndWarehouseModuleRegression3 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Inventory and Warehousing", "Transfer Order",
				"Verify that stock will not update in \"From Warehouse\" before complete the shipment acceptance",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Configure shipment accesptance and copmplete Transfer Order")
	@Step("Login to the Entution")
	public void verifyTransferOrder_IN_TO_123() throws Exception {
		transferOrder_IN_TO_123();
	}

	@Test(dependsOnMethods = "verifyTransferOrder_IN_TO_123")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Outbound Shipment")
	public void verifyOutboundShipment_IN_TO_123() throws Exception {
		outboundShipment_IN_TO_123();
	}

	@Test(dependsOnMethods = "verifyOutboundShipment_IN_TO_123")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check stock updatance")
	public void verifyAndCheckStockUpdatance_IN_TO_123() throws Exception {
		checkStockUpdatance_IN_TO_123();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		removeShipmentAcceptanceTransferOrderToOutboundShipment();
		common.totalTime(startTime);
		common.driverClose();
	}

}
