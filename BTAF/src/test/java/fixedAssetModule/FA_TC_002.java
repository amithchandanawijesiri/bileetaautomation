package fixedAssetModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FixedAsset;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({TestListener.class})
public class FA_TC_002 extends FixedAsset{

	TestCommonMethods common = new TestCommonMethods();
	
	
	
	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Fixed Asset", "", "Verify that Fixed asset can be created through generating Internal Order", this.getClass().getSimpleName());
	}
	
	@Test//(priority = 1)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Click on navigation menu")
	public void navigateSideBar() throws Exception {
		
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
	    navigateToSideBar();
	}
	
	@Test(dependsOnMethods ="navigateSideBar")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Inventory & Warehousing module should be clicked successfully")
	
	public void inventorynwarehousingBtn() throws Exception {
		
		navigateInventoy();
	}
	
	
	
	@Test(dependsOnMethods = "inventorynwarehousingBtn")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Internal Order by-page")
	
	public void internalOrderButtonClick() throws Exception {
		
		navigateToInternalOrder();
	}
	
	
	@Test(dependsOnMethods = "internalOrderButtonClick")
	@Severity(SeverityLevel.CRITICAL)
	@Description("List Journey should be displayed")
	
	public void navigateToInternalJourneyView() throws Exception {
		
		navigateToInternalOrderJourneyTab();
	}
	
	@Test(dependsOnMethods = "navigateToInternalJourneyView")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Internal Order page should be displayed")
	
	public void AddingNewInternalOrder() throws Exception {
		
		navigateToNewInternalOrder();
	}
	
	@Test(dependsOnMethods = "AddingNewInternalOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Should be able to fill all mandatory fields")
	
	public void fillingInternalOrderMandatoryFields() throws Exception {
		
		fillingInternalOrderForm();
	}
	
	@Test(dependsOnMethods = "fillingInternalOrderMandatoryFields")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Should be able to view advance settings")
	
	public void advanceTab() throws Exception {
		
		advancedSettingsInventory();
	}

	@Test(dependsOnMethods = "advanceTab")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Should be able to view fixed asset tab")
	
	public void fixedAssetTab() throws Exception {
		
		fixedAssetInternal();
	
	}
	
	@Test(dependsOnMethods = "fixedAssetTab")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Should be able to view Asset group and asset book will be loaded")
	
	public void addingAssetGroup() throws Exception {
		
		assetGrpInternal();
	}
	
	@Test(dependsOnMethods = "addingAssetGroup")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Should be able to draft and release")
	
	public void draftandrelease() throws Exception {
		
		draftNreleaseInventory();
	}
	
	@Test(dependsOnMethods = "draftandrelease")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Able to click on Convert to Internal Dispatch Order & It should navigate to above page where fields were auto filled.")
	
	public void navigateDispatchOrder() throws Exception {
		
		navigateToDispatchOrder();
	}
	
	
	@Test(dependsOnMethods = "navigateDispatchOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description(" Able to release the document successfully.")
	
	public void releaseDispatchOrder() throws Exception {
		
		completeNreleaseInventory();
	}
	

	@Test(dependsOnMethods = "releaseDispatchOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Journal Entry page loading")
	
	public void journalEntry() throws Exception {
		
		displayJournalEntryInventory();
	}
	
	@Test(dependsOnMethods = "journalEntry")
	@Severity(SeverityLevel.CRITICAL)
	@Description("able to release the created FA Book Information")
	
	public void fixedAssetInformation() throws Exception {
		
		navigateTofixedAssetInfo();
	}
	
	@Test(dependsOnMethods = "fixedAssetInformation")
	@Severity(SeverityLevel.CRITICAL)
	@Description("able to navigate to bin card")
	
	public void binCard() throws Exception {
		
		navigateToBinCard();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}
	
}
