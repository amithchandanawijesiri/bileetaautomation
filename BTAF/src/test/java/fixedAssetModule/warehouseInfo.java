package fixedAssetModule;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FixedAsset;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class warehouseInfo extends FixedAsset {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Fixed Asset", "", "Verify the ability of creating a new fixed asset group",
				this.getClass().getSimpleName());
	}

	@Test 
	@Severity(SeverityLevel.CRITICAL)
	@Description("Click on navigation menu")
	public void navigateSideBar() throws Exception {

		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		navigateToSideBar();
		warehouseInfo();
	}
	

}
