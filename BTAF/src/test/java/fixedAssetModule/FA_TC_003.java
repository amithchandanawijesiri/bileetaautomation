package fixedAssetModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FixedAsset;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({TestListener.class})

public class FA_TC_003 extends FixedAsset{

	TestCommonMethods common = new TestCommonMethods();

	
	
	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Fixed Asset", "", "Verify that Fixed asset can be created through generating Internal Order", this.getClass().getSimpleName());
	}
	
	@Test//(priority = 1)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Click on navigation menu")
	public void navigateSideBar() throws Exception {
		
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		navigateToSideBar();
	}
	
	@Test(dependsOnMethods ="navigateSideBar")//,priority = 2)
	@Severity(SeverityLevel.CRITICAL)
	@Description("All Modules should be displayed")
	
	public void ProjectBtn() throws Exception {
		
		navigateToProject();
	}
	
	
	@Test(dependsOnMethods = "ProjectBtn")//, priority = 3)
	@Severity(SeverityLevel.CRITICAL)
	@Description(" Project module should be clicked successfully")
	
	public void projectButtonClick() throws Exception {
		
		navigateToAddProject();
	}
	
	
	@Test(dependsOnMethods = "projectButtonClick")//, priority = 4)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Project page should be displayed")
	
	public void newProjectAdding() throws Exception {
		
		navigateToNewProject();
	}
	
	@Test(dependsOnMethods = "newProjectAdding")//, priority = 5)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Should be able to fill all mandatory fields")
	
	public void fillingMandatoryFields() throws Exception {
		
		fillingProjectForm();
	}
	
	@Test(dependsOnMethods = "fillingMandatoryFields")//, priority = 6)
	@Severity(SeverityLevel.CRITICAL)
	@Description(" Project page should be displayed again")
	
	public void projectDraftNRelease() throws Exception {
		
		draftNReleaseProject();
	}
	
	@Test(dependsOnMethods = "projectDraftNRelease")//, priority = 7)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Task looup should be displayed")
	
	public void ProjectUpdate() throws Exception {
		
		updateProject();
	}
	
	@Test(dependsOnMethods = "ProjectUpdate")//, priority = 8)
	@Severity(SeverityLevel.CRITICAL)
	@Description(" Project page should be displayed again")
	
	public void ProjectTask() throws Exception {
		
		taskProject();
	}
	
	@Test(dependsOnMethods = "ProjectTask")//, priority = 9)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Outbound payment advice page should be displayed again")
	
	public void ProjectNavigateToOutboundAdvice() throws Exception {
		
		navigateToOutboundPaymentAdvice();
	}
	
	@Test(dependsOnMethods = "ProjectNavigateToOutboundAdvice")//, priority = 10)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Should able to create accrual voucher")
	
	public void ProjectNewNavigateToOutboundAdvice() throws Exception {
		
		navigateToNewOutboundPaymentAdvice();
	}
	
	@Test(dependsOnMethods = "ProjectNewNavigateToOutboundAdvice")//, priority = 11)
	@Severity(SeverityLevel.CRITICAL)
	@Description("outbound payment advice page should be displayed again")
	
	public void ProjectJournalAccrual() throws Exception {
		
		journalAccrualVoucher();
	}
	
	@Test(dependsOnMethods = "ProjectJournalAccrual")//, priority = 12)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Vendor lookup should be displayed")
	
	public void ProjectVendorLookup() throws Exception {
		
		vendorDetailsLookup();
	}
	
	
	@Test(dependsOnMethods = "ProjectVendorLookup")//, priority = 13)
	@Severity(SeverityLevel.CRITICAL)
	@Description("add a GL account lookup should be displayed again")
	
	public void ProjectGLAcc() throws Exception {
		
		GLACCLookup();
	}
	
	@Test(dependsOnMethods = "ProjectGLAcc")//, priority = 14)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Able to re-direct to Cost Allocation page")
	
	public void ProjectCostAllocation() throws Exception {
		
		costAllocationProject();
	}
	
	@Test(dependsOnMethods = "ProjectCostAllocation")//, priority = 15)
	@Severity(SeverityLevel.CRITICAL)
	@Description("User will navigate to Task completion page")
	
	public void ProjectEndCost() throws Exception {
		
		endCostAllocation();
	}
	
	@Test(dependsOnMethods = "ProjectEndCost")//, priority = 16)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Project task changed to Completed stage")
	
	public void ProjectoutBoundPayment() throws Exception {
		
		draftNReleaseOutBoundPaymentAdvice();
	}
	
	@Test(dependsOnMethods = "ProjectoutBoundPayment")//, priority = 17)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Journal entry should be displayed")
	
	public void ProjectNavigateToAgain() throws Exception {
		
		navigateToProjectAgain();
	}
	
	@Test(dependsOnMethods = "ProjectNavigateToAgain")//, priority = 18)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Able to do necessary changes and can check for accuracy of FA  page")
	
	public void ProjectReleaseDetails() throws Exception {
		
		ProjectRelease();
	}

	@Test(dependsOnMethods = "ProjectReleaseDetails")//, priority = 19)
	@Severity(SeverityLevel.CRITICAL)
	@Description("User can view the fixed asset logo")
	
	public void CompletionProject() throws Exception {
		
		ProjectCompletion();
	}
	
	@Test(dependsOnMethods = "CompletionProject")//, priority = 20)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Should be able to view asset book")
	
	public void JournalEntryProject() throws Exception {
		
		ProjectJournalEntry();
	}
	
	@Test(dependsOnMethods = "JournalEntryProject")//, priority = 21)
	@Severity(SeverityLevel.CRITICAL)
	@Description("fixed asset information Logo displayed")
	
	public void ReleaseAssetInfoProject() throws Exception {
		
		ProjectReleaseAssetInfo();
	}
	

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
