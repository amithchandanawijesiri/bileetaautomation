package fixedAssetModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FixedAsset;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({TestListener.class})

public class actionFAupdateGroup extends FixedAsset {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Fixed Asset", "", "Verify the ability of creating a new fixed asset book.",
				this.getClass().getSimpleName());
	}

	@Test // (priority = 1)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Click on navigation menu")
	public void navigateSideBar() throws Exception {

		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();

		navigateToSideBar();
	}
	
	@Test(dependsOnMethods ="navigateSideBar")//,priority = 2)
	@Severity(SeverityLevel.CRITICAL)
	@Description("View fixed asset module")
	public void FixedAssetMenu() throws Exception {
		
		navigateToFixedAssetMenu();
	}
	
	@Test(dependsOnMethods ="FixedAssetMenu")//,priority = 2)
	@Severity(SeverityLevel.CRITICAL)
	@Description("new fixed asset book creation")
	public void NewFixedAssetBook() throws Exception {
		
		actioneditFixedAssetGroup();
		
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}


}
