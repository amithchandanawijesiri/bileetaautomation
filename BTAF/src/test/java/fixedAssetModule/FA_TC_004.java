package fixedAssetModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FixedAsset;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({TestListener.class})
public class FA_TC_004 extends FixedAsset{

	TestCommonMethods common = new TestCommonMethods();
	
	
	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Fixed Asset", "", "Verify that Fixed asset can be created through generating Internal Order", this.getClass().getSimpleName());
	}
	
	@Test//(priority = 1)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Click on navigation menu")
	public void navigateSideBar() throws Exception {
		
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		navigateToSideBar();
	}
	
	@Test(dependsOnMethods ="navigateSideBar")//,priority = 2)
	@Severity(SeverityLevel.CRITICAL)
	@Description("View fixed asset module")
	public void FixedAssetMenu() throws Exception {
		
		navigateToFixedAssetMenu();
	}
	
	@Test(dependsOnMethods ="FixedAssetMenu")//,priority = 3)
	@Severity(SeverityLevel.CRITICAL)
	@Description("View fixed asset Book by page")
	public void FixedAssetBookCreation() throws Exception {
		
		navigateToFixedAssetCreation();
	}
	
	@Test(dependsOnMethods ="FixedAssetBookCreation")//,priority = 4)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Should be able to view asset book")
	public void FixedAssetBookView() throws Exception {
		
		viewAssetBook();
	}
	
	@Test(dependsOnMethods ="FixedAssetBookView")//,priority = 5)
	@Severity(SeverityLevel.CRITICAL)
	@Description("should be navigate to fixed asset depreciation page")
	public void FixedAssetBookDepreciation() throws Exception {
		
		navigateToFixedAssetDepreciation();
	}
	
	@Test(dependsOnMethods ="FixedAssetBookDepreciation")//,priority = 6)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Able to flag the assets to depreciate")
	public void FlagFixedAssetBook() throws Exception {
		
		flagDepreciation();
	}
	
	@Test(dependsOnMethods ="FlagFixedAssetBook")//,priority = 7)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Able to do depreciation for selected assets and those asset information net book value and accumilated depreciation value should be updated with latest values")
	public void releaseFixedAssetBook() throws Exception {
		
		releaseDepreciation();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}
}
