package fixedAssetModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FixedAsset;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({TestListener.class})
public class FA_TC_007 extends FixedAsset{

	TestCommonMethods common = new TestCommonMethods();
	
	
	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Fixed Asset", "", "Verify that fixed asset can be split and can transfer to existing Asset", this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Click on navigation menu")
	public void navigateSideBar() throws Exception {
		
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		
	    navigateToSideBar();
	}
	
	@Test(dependsOnMethods ="navigateSideBar")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Procurement should be clicked successfully")
	
	public void procurementBtn() throws Exception {
		
		navigateToProcurement();
	}
	
	@Test(dependsOnMethods = "procurementBtn")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Purchase Order by-page")
	
	public void purchaseOrderButtonClick() throws Exception {
		
		navigateToPurchaseOrder();
	}
	
	
	@Test(dependsOnMethods = "purchaseOrderButtonClick")
	@Severity(SeverityLevel.CRITICAL)
	@Description("List Journey should be displayed")
	
	public void navigateToJourneyView() throws Exception {
		
		navigateToJourneyTab();
	}
	
	@Test(dependsOnMethods = "navigateToJourneyView")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Purchase Order page should be displayed")
	
	public void AddingNewPurchaseOrder() throws Exception {
		
		navigateToNewPurchaseOrder();
	}
	
	@Test(dependsOnMethods = "AddingNewPurchaseOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Should be able to fill all mandatory fields")
	
	public void fillingMandatoryFields() throws Exception {
		
		fillingPurchaseOrderForm();
	}
	
	
	@Test(dependsOnMethods = "fillingMandatoryFields")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Should be able to view advance settings")
	
	public void advanceTab() throws Exception {
		
		advancedSettings();
	}
	
	@Test(dependsOnMethods = "advanceTab")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Should be able to view fixed asset tab")
	
	public void fixedAssetTab() throws Exception {
		
		fixedAsset();
	
	}
	
	@Test(dependsOnMethods = "fixedAssetTab")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Should be able to view Asset group and asset book will be loaded")
	
	public void addingAssetGroup() throws Exception {
		
		assetGrp();
	}
	
	@Test(dependsOnMethods = "addingAssetGroup")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Should be able to checkout")
	
	public void checkout() throws Exception {
		
		checkOutProcurement();
	}
	
	@Test(dependsOnMethods = "checkout")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Should be able to draft and release")
	
	public void draftandrelease() throws Exception {
		
		draftNreleaseProcurement();
	}
	
	@Test(dependsOnMethods = "draftandrelease")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Able to click on Convert to Purchase Invoice & page should navigate to PI where fields were auto filled")
	
	public void purchaseInvoiceByPage() throws Exception {
		
		purchaseInvoicePage();
	}
	
	@Test(dependsOnMethods = "purchaseInvoiceByPage")
	@Severity(SeverityLevel.CRITICAL)
	@Description(" Able to release the purchase Invoice successfully")
	
	public void ReleasepurchaseInvoiceByPage() throws Exception {
		
		purchaseInvoiceRelease();
	}
	
	@Test(dependsOnMethods = "ReleasepurchaseInvoiceByPage")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Journal entry should be displayed")
	
	public void PurchaseinvoiceJournal() throws Exception {
		
		purchaseInvoiceJournalEntry();
	}
	
	@Test(dependsOnMethods = "PurchaseinvoiceJournal")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Created draft FA information number will be displayed")
	
	public void PurchaseinvoiceFA() throws Exception {
		
		purchaseInvoiceFAPage();
	}
	
	@Test(dependsOnMethods = "PurchaseinvoiceFA")
	@Severity(SeverityLevel.CRITICAL)
	@Description(" User should be able to release the created FA Book Information")
	
	public void releaseFA() throws Exception {
		
		releaseFABook();
	}
	
	
	@Test(dependsOnMethods ="releaseFA")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Should navigate to fixed asset split page")
	public void FixedAssetBook7() throws Exception {
		
		navigateToFixedAssetInfoBySplitPage();
	}
	
	@Test(dependsOnMethods ="FixedAssetBook7")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Should display the page for split the asset to existing asset")
	public void splitExisting() throws Exception {
		
		navigateToFixedAssetInfoBySplitExisting();
	}
	
	@Test(dependsOnMethods ="splitExisting")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Should be able to split successfully. splitted asset  & the existing asset which received split asset should be update with new net book value & journal entry should be update as it is.")
	public void ApplysplitExisting() throws Exception {
		
		ApplySplitExisting();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}
}
