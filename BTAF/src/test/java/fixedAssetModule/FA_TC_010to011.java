package fixedAssetModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FixedAsset;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({TestListener.class})

public class FA_TC_010to011 extends FixedAsset{

	TestCommonMethods common = new TestCommonMethods();
	
	
	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Fixed Asset", "", "Verify that user can delete drafted fixed asset information which is generatd through PO > PI (service ) journey", this.getClass().getSimpleName());
	}
	
	@Test//(priority = 1)
	@Severity(SeverityLevel.CRITICAL)
	@Description("User login and Click on navigation menu")
	public void clickNav() throws Exception {

		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		navigateToSideBar();
	}
	
	@Test(dependsOnMethods = "clickNav")//,priority = 2)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can delete drafted fixed asset information which is generatd through PO > PI (service ) journey")
	public void VerThatUserCanDeleteDraftedFixedAssetInformationWhichIsGeneratdThroughPOtoPIServiceJourney() throws Exception {
	
		VerifyThatUserCanDeleteDraftedFixedAssetInformationWhichIsGeneratdThroughPOtoPIServiceJourney();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}
}