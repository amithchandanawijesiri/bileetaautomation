package fixedAssetModule;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FixedAsset;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class newVendor extends FixedAsset{

	TestCommonMethods common = new TestCommonMethods();

	
	
	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Fixed Asset", "", "", this.getClass().getSimpleName());
	}
	
	@Test//(priority = 1)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Click on navigation menu")
	public void navigateSideBar() throws Exception {
		
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		navigateToSideBar();
		vendorInfo();
	}
	

}
