package fixedAssetModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FixedAsset;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class Smoke_Fixed_Asset_03 extends FixedAsset {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Fixed Asset", "", "Verify the ability of creating a new fixed asset group",
				this.getClass().getSimpleName());
	}

	@Test 
	@Severity(SeverityLevel.CRITICAL)
	@Description("Click on navigation menu")
	public void navigateSideBar() throws Exception {

		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();

		navigateToSideBar();
	}
	
	@Test(dependsOnMethods ="navigateSideBar")
	@Severity(SeverityLevel.CRITICAL)
	@Description("View fixed asset module")
	public void FixedAssetMenu() throws Exception {
		
		navigateToFixedAssetMenu();
	}
	
	@Test(dependsOnMethods ="FixedAssetMenu")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify the ability of creating a new fixed asset group")
	public void fixedAssetGroup() throws Exception {
		
		creatingNewFixedAssetGroup();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
