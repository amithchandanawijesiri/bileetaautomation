package fixedAssetModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FixedAsset;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({TestListener.class})
public class FA_TC_009 extends FixedAsset{

	TestCommonMethods common = new TestCommonMethods();
	
	
	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Fixed Asset", "", "Verify that Asset can be dispose by Selling an asset", this.getClass().getSimpleName());
	}
	
	@Test//(priority = 1)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Click on navigation menu")
	public void navigateSideBar() throws Exception {
		
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		
	    navigateToSideBar();
	}
	
	@Test(dependsOnMethods ="navigateSideBar")//,priority = 2)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Procurement should be clicked successfully")
	
	public void procurementBtn() throws Exception {
		
		navigateToProcurement();
	}
	
	@Test(dependsOnMethods = "procurementBtn")//, priority = 3)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Purchase Order by-page")
	
	public void purchaseOrderButtonClick() throws Exception {
		
		navigateToPurchaseOrder();
	}
	
	
	@Test(dependsOnMethods = "purchaseOrderButtonClick")//, priority = 4)
	@Severity(SeverityLevel.CRITICAL)
	@Description("List Journey should be displayed")
	
	public void navigateToJourneyView() throws Exception {
		
		navigateToJourneyTab();
	}
	
	@Test(dependsOnMethods = "navigateToJourneyView")//, priority = 5)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Purchase Order page should be displayed")
	
	public void AddingNewPurchaseOrder() throws Exception {
		
		navigateToNewPurchaseOrder();
	}
	
	@Test(dependsOnMethods = "AddingNewPurchaseOrder")//, priority = 6)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Should be able to fill all mandatory fields")
	
	public void fillingMandatoryFields() throws Exception {
		
		fillingPurchaseOrderForm();
	}
	
	
	@Test(dependsOnMethods = "fillingMandatoryFields")//, priority = 7)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Should be able to view advance settings")
	
	public void advanceTab() throws Exception {
		
		advancedSettings();
	}
	
	@Test(dependsOnMethods = "advanceTab")//, priority = 8)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Should be able to view fixed asset tab")
	
	public void fixedAssetTab() throws Exception {
		
		fixedAsset();
	
	}
	
	@Test(dependsOnMethods = "fixedAssetTab")//, priority = 9)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Should be able to view Asset group and asset book will be loaded")
	
	public void addingAssetGroup() throws Exception {
		
		assetGrp();
	}
	
	@Test(dependsOnMethods = "addingAssetGroup")//, priority = 10)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Should be able to checkout")
	
	public void checkout() throws Exception {
		
		checkOutProcurement();
	}
	
	@Test(dependsOnMethods = "checkout")//, priority = 11)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Should be able to draft and release")
	
	public void draftandrelease() throws Exception {
		
		draftNreleaseProcurement();
	}
	
	@Test(dependsOnMethods = "draftandrelease")//, priority = 12)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Able to click on Convert to Purchase Invoice & page should navigate to PI where fields were auto filled")
	
	public void purchaseInvoiceByPage() throws Exception {
		
		purchaseInvoicePage();
	}
	
	@Test(dependsOnMethods = "purchaseInvoiceByPage")//, priority = 13)
	@Severity(SeverityLevel.CRITICAL)
	@Description(" Able to release the purchase Invoice successfully")
	
	public void ReleasepurchaseInvoiceByPage() throws Exception {
		
		purchaseInvoiceRelease();
	}
	
	@Test(dependsOnMethods = "ReleasepurchaseInvoiceByPage")//, priority = 14)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Journal entry should be displayed")
	
	public void PurchaseinvoiceJournal() throws Exception {
		
		purchaseInvoiceJournalEntry();
	}
	
	@Test(dependsOnMethods = "PurchaseinvoiceJournal")//, priority = 15)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Created draft FA information number will be displayed")
	
	public void PurchaseinvoiceFA() throws Exception {
		
		purchaseInvoiceFAPage();
	}
	
	@Test(dependsOnMethods = "PurchaseinvoiceFA")//, priority = 16)
	@Severity(SeverityLevel.CRITICAL)
	@Description(" User should be able to release the created FA Book Information")
	
	public void releaseFA() throws Exception {
		
		releaseFABook();
	}
	
	
	@Test(dependsOnMethods ="releaseFA")//,priority = 17)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Should be able to navigate to Fixed asset disposal page")
	public void FixedAssetBook9() throws Exception {
		
		navigateToFixedAssetInfoByDisposalSellingAsset();
	}
	
	@Test(dependsOnMethods ="FixedAssetBook9")//,priority = 18)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Should be able to set disposal date & select selling an asset option")
	public void FixedAssetSellingDisposal() throws Exception {
		
		addSellingAnAssetDisposal();
	}
	
	@Test(dependsOnMethods ="FixedAssetSellingDisposal")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Should be able to dispose asset by selling")
	public void ApplyFixedAssetSellingDisposal() throws Exception {
		
		ApplyDisposalSelling();
	}
	
	@Test(dependsOnMethods ="ApplyFixedAssetSellingDisposal")
	@Severity(SeverityLevel.CRITICAL)
	@Description("User should be able to access to create free text invoice")
	public void fixedTextInvoice() throws Exception {
		
		ApplyFreetextInvoice();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}
}
