package procumentModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.AdministrationModule;
import bileeta.BTAF.PageObjects.ProcumentModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({TestListener.class})

public class Com_TC_001 extends ProcumentModule{

	TestCommonMethods common = new TestCommonMethods();
	
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Procument", "", "Verify that user can successfully loing to the Entution with valid Username and valid Password ", this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("open the browser and enter the application URL")
	public void loginProcument() throws Exception {
	
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();	
	}
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		  
		common.driverClose();
		 
	}
}
