package procumentModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.AdministrationModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.ProcumentModuleSmoke;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({TestListener.class})

public class SMOKE_PRO_02 extends ProcumentModuleSmoke{

	
	TestCommonMethods common = new TestCommonMethods();
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Procument", "", "\r\n" + "Verify whether system allows user to release vendor agreement", this.getClass().getSimpleName());
		
	}
	@Test//(priority = 1)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Click on navigation menu")
	public void clickNav() throws Exception {
	
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		clickNavigation();
	}
	@Test(dependsOnMethods = "clickNav")//,priority = 2)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Click on Procurement menu")
	public void clickPro() throws Exception {
	
		clickProbutton();
	}
	
	@Test(dependsOnMethods = "clickPro")//,priority = 4)
	@Severity(SeverityLevel.CRITICAL)
	@Description("add Vendor Agreement")
	public void addVenAgreement() throws Exception {
	
		addVendorAgreement();
	}
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		  
		common.driverClose();
		 
	}
}
