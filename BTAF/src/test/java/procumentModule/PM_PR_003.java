package procumentModule;

	import org.testng.annotations.AfterClass;
	import org.testng.annotations.BeforeClass;
	import org.testng.annotations.Test;

	import bileeta.BTAF.PageObjects.AdministrationModule;
	import bileeta.BTAF.PageObjects.ProcumentModule;
	import bileeta.BTAF.Utilities.TestCommonMethods;
	import io.qameta.allure.Description;
	import io.qameta.allure.Severity;
	import io.qameta.allure.SeverityLevel;

	public class PM_PR_003 extends ProcumentModule{
		
		TestCommonMethods common = new TestCommonMethods();
		AdministrationModule admin = new AdministrationModule();
		
		@BeforeClass
		public void setUp() throws Exception
		{
			common.setUp("Procument", "", "\r\n" + "Verify whether system allows user to release the rocord", this.getClass().getSimpleName());
		
		}
		
		@Test(priority = 1)
		@Severity(SeverityLevel.CRITICAL)
		@Description("User login and Click on navigation menu")
		public void clickNav() throws Exception {

			navigateToTheLoginPage();
			verifyTheLogo();
			userLogin();
			clickNavigation();
		}
		
		@Test(dependsOnMethods = "clickNav",priority = 2)
		@Severity(SeverityLevel.CRITICAL)
		@Description("Click on Procurement menu")
		public void clickPro() throws Exception {
		
			clickProbutton();
		}
		
		@Test(dependsOnMethods = "clickPro",priority = 3)
		@Severity(SeverityLevel.CRITICAL)
		@Description("relese Purchase Requisition")
		public void relPurchaseReq() throws Exception {
		
			relesePurchaseRequisition();
		}

		
		
		@AfterClass(alwaysRun=true)
		public void AfterClass() throws Exception 
		{
			common.totalTime(startTime);
			  
			common.driverClose();
			 
		}
	}
