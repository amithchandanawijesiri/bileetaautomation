package procumentModule;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProcumentModule;
import bileeta.BTAF.PageObjects.ProcumentModuleSmoke;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({TestListener.class})

public class SMOKE_PRO_05 extends ProcumentModuleSmoke {

	TestCommonMethods common = new TestCommonMethods();
	
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Procument", "", "\r\n" + "Verify whether User can View and Select the Requisition in the Purchase Desk", this.getClass().getSimpleName());
	
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify whether User can View and Select the Requisition in the Purchase Desk")
	public void VerWhetherUserCanViewAndSelectTheRequisitionInThePurchaseDesk() throws Exception {

		VerifyWhetherUserCanViewAndSelectTheRequisitionInThePurchaseDesk();
	}
	

	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		  
		common.driverClose();
		 
	}
	
}
