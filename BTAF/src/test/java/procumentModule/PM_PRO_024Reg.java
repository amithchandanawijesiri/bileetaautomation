package procumentModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.AdministrationModule;
import bileeta.BTAF.PageObjects.ProcumentModule;
import bileeta.BTAF.PageObjects.ProcumentModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({TestListener.class})

public class PM_PRO_024Reg extends ProcumentModuleRegression{

	TestCommonMethods common = new TestCommonMethods();
	AdministrationModule admin = new AdministrationModule();
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Procument", "", "\r\n" + "Verify that tax break down displayed the tax details when user add the differnet tax groups for different products", this.getClass().getSimpleName());
	
	}
	
	@Test//(priority = 1)
	@Severity(SeverityLevel.CRITICAL)
	@Description("User login and Click on navigation menu")
	public void clickNav() throws Exception {

		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		clickNavigation();
		clickProbutton();
	}
	
	@Test(dependsOnMethods = "clickNav")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that tax break down displayed the tax details when user add the differnet tax groups for different products")
	public void VerThatTaxBreakDownDisplayedTheTaxDetailsWhenUserAddTheDiffernetTaxGroupsForDifferentProducts() throws Exception {
	
		VerifyThatTaxBreakDownDisplayedTheTaxDetailsWhenUserAddTheDiffernetTaxGroupsForDifferentProducts();
	}
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		  
		common.driverClose();
		 
	}
}