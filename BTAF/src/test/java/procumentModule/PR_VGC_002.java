package procumentModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.AdministrationModule;
import bileeta.BTAF.PageObjects.ProcumentModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({TestListener.class})

public class PR_VGC_002 extends ProcumentModule{

	TestCommonMethods common = new TestCommonMethods();
	AdministrationModule admin = new AdministrationModule();
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Procument", "", "Verify whether system allows user to draft vendor group", this.getClass().getSimpleName());
		
	}
	@Test//(priority = 1)
	@Severity(SeverityLevel.CRITICAL)
	@Description("User login and Click on navigation menu")
	public void clickNav() throws Exception {
	
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		clickNavigation();
	}
	@Test(dependsOnMethods = "clickNav")//,priority = 2)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Click on Procurement menu")
	public void clickPro() throws Exception {
	
		clickProbutton();
	}
	@Test(dependsOnMethods = "clickPro")//,priority = 3)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Click on Vendor Group Configuration link in navigation menu")
	public void Vendorgroupbutton()  throws Exception {

		clickVendorgroupbutton();	
	}
	
	@Test(dependsOnMethods = "Vendorgroupbutton")//,priority = 4)
	@Severity(SeverityLevel.CRITICAL)
	@Description(" Click on 'New Vendor Group Configuration' button")
	public void NewVendorgroupbutton()  throws Exception {
	
		clickNewVendorgroupbutton();
	}
	
	@Test(dependsOnMethods = "NewVendorgroupbutton")//,priority = 5)
	@Severity(SeverityLevel.CRITICAL)
	@Description(" add Vendor Group Configuration")
	public void VendorGroupConfiguration()  throws Exception {
	
		addVendorGroupConfiguration();
	}
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		  
		common.driverClose();
		 
	}
}
