package procumentModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.AdministrationModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.ProcumentModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({TestListener.class})

public class PM_PR_002 extends ProcumentModule{
	
	TestCommonMethods common = new TestCommonMethods();
	AdministrationModule admin = new AdministrationModule();
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Procument", "", "\r\n" + "\r\n" + "Verify whether system allows user to draft the record", this.getClass().getSimpleName());
	
	}
	
	@Test//(priority = 1)
	@Severity(SeverityLevel.CRITICAL)
	@Description("User login and Click on navigation menu")
	public void clickNav() throws Exception {

		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		InventoryAndWarehouseModule obj = new InventoryAndWarehouseModule();
		clickNavigation();
	}
	
	@Test(dependsOnMethods = "clickNav")//,priority = 2)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Click on Procurement menu")
	public void clickPro() throws Exception {
	
		clickProbutton();
	}
	
	@Test(dependsOnMethods = "clickPro")//,priority = 3)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Click on Procurement menu")
	public void newPurchaseReq() throws Exception {
	
		newPurchaseRequisition();
	}

	@Test(dependsOnMethods = "newPurchaseReq")//,priority = 4)
	@Severity(SeverityLevel.CRITICAL)
	@Description("add Purchase Requisition")
	public void addPurchaseReq() throws Exception {
	
		addPurchaseRequisition();
	}
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		  
		common.driverClose();
		 
	}
}
