package procumentModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.AdministrationModule;
import bileeta.BTAF.PageObjects.ProcumentModuleSmoke;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class SMOKE_PRO_01 extends ProcumentModuleSmoke{
	
	TestCommonMethods common = new TestCommonMethods();
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Procument", "", "Verify whether system allows user to draft vendor information", this.getClass().getSimpleName());
		
	}
	@Test//(priority = 1)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Click on navigation menu")
	public void clickNav() throws Exception {
	
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		clickNavigation();
	}
	@Test(dependsOnMethods = "clickNav")//,priority = 2)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Click on Procurement menu")
	public void clickPro() throws Exception {
	
		clickProbutton();
	}
	@Test(dependsOnMethods = "clickPro")//,priority = 3)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Click on Vendor information link in navigation menu")
	public void newVen()  throws Exception {

		newVendor();
	}
	
	@Test(dependsOnMethods = "newVen")//,priority = 4)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Add new vendor information")
	public void addNewVen()  throws Exception {

		addVendor();
	}
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		  
		common.driverClose();
		 
	}

}
