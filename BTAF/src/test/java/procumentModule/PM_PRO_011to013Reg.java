package procumentModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.AdministrationModule;
import bileeta.BTAF.PageObjects.ProcumentModule;
import bileeta.BTAF.PageObjects.ProcumentModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({TestListener.class})

public class PM_PRO_011to013Reg extends ProcumentModuleRegression{

	TestCommonMethods common = new TestCommonMethods();
	AdministrationModule admin = new AdministrationModule();
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Procument", "", "\r\n" + "Verify that cannot enter the characters for following columns in the product grid  Qty,Unit,Price,Discount%,Discount", this.getClass().getSimpleName());
	
	}
	
	@Test//(priority = 1)
	@Severity(SeverityLevel.CRITICAL)
	@Description("User login and Click on navigation menu")
	public void clickNav() throws Exception {

		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		clickNavigation();
		clickProbutton();
	}
	
	@Test(dependsOnMethods = "clickNav")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that cannot enter the characters for following columns in the product grid  Qty,Unit,Price,Discount%,Discount")
	public void VerThatCannotEnterTheCharactersForFollowingColumnsInTheProductGridQtyUnitPriceDiscountPresentageDiscount() throws Exception {
	
		VerifyThatCannotEnterTheCharactersForFollowingColumnsInTheProductGridQtyUnitPriceDiscountPresentageDiscount();
	}
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		  
		common.driverClose();
		 
	}
}
