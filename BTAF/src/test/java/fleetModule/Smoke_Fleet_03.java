package fleetModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FleetManagementSmoke;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class Smoke_Fleet_03 extends FleetManagementSmoke {
TestCommonMethods common = new TestCommonMethods();
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("FleetManagement", "", "\r\n" + "Verify that creating package information", this.getClass().getSimpleName());
	
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Open the browser and enter the application URL")
	public void _navigateToTheLoginPage() throws Exception
	{
		navigateToTheLoginPage();
	}
	
	@Test(dependsOnMethods = "_navigateToTheLoginPage")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that 'Entution' header is available on the page")
	public void _verifyTheLogo() throws Exception
	{
		verifyTheLogo();
		
	}
	
	@Test(dependsOnMethods = "_verifyTheLogo")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that login to the system")
	public void _userLogin() throws Exception
	{
		userLogin();
		
	}
	
	@Test(dependsOnMethods = "_userLogin")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Navigation menus")
	public void _menuNavigation() throws Exception
	{
		navigationmenu();
		
	}
	
	@Test(dependsOnMethods = "_menuNavigation")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that forms Fleet Management module")
	public void _fleetNavigation() throws Exception
	{
		navigate_fleet_management();
		
	}
	
	@Test(dependsOnMethods = "_fleetNavigation")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that navigation of package information bypage")
	public void _packageInformationBypageNavigation() throws Exception
	{
		NavigationPackageInformationByPage();
		
	}
	
	@Test(dependsOnMethods = "_packageInformationBypageNavigation")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that navigation of package information new page")
	public void _packageInformationNewpageNavigation() throws Exception
	{
		NavigationPackageInformationNewPage();
		
	}
	
	@Test(dependsOnMethods = "_packageInformationNewpageNavigation")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that creation of package")
	public void _packageInformationCreation() throws Exception
	{
		VerifyThatAbleToCreatePackage();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		  
		common.driverClose();
	
    }


}


