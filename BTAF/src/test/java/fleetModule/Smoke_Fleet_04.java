package fleetModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FleetManagementSmoke;

import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestCommonMethods;
import bileeta.BTAF.Utilities.TestListener;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
@Listeners({TestListener.class})

public class Smoke_Fleet_04  extends FleetManagementSmoke {
	
TestCommonMethods common = new TestCommonMethods();
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("FleetManagement", "", "\r\n" + "Verify that creating fleet parameters", this.getClass().getSimpleName());
	
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Open the browser and enter the application URL")
	public void _navigateToTheLoginPage() throws Exception
	{
		navigateToTheLoginPage();
	}
	
	@Test(dependsOnMethods = "_navigateToTheLoginPage")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that 'Entution' header is available on the page")
	public void _verifyTheLogo() throws Exception
	{
		verifyTheLogo();
		
	}
	
	@Test(dependsOnMethods = "_verifyTheLogo")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that login to the system")
	public void _userLogin() throws Exception
	{
		userLogin();
		
	}
	
	@Test(dependsOnMethods = "_userLogin")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Navigation menus")
	public void _menuNavigation() throws Exception
	{
		navigationmenu();
		
	}
	
	@Test(dependsOnMethods = "_menuNavigation")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Navigation Gate Pass By page")
	public void _gatePassByPageNavigation() throws Exception
	{
		NavigationGatePassByPage();
		
	}
	
	
	@Test(dependsOnMethods = "_gatePassByPageNavigation")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Gate Pass new form")
	public void _gatePassNewPageNavigation() throws Exception
	{
		NavigationGatePassNewPage();
		
	}
	
	@Test(dependsOnMethods = "_gatePassNewPageNavigation")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Gate Pass creation")
	public void _gatePassCreation() throws Exception
	{
		VerifyThatAbleToCreateVehicleGatePass();
		
	}
	
	@Test(dependsOnMethods = "_gatePassCreation")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Gate Pass Checkout")
	public void _gatePassCheckout() throws Exception
	{
		VerifyGatePassCheckout();
		
	}
	
	@Test(dependsOnMethods = "_gatePassCheckout")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Gate Pass Checkout")
	public void _gatePassCheckIn() throws Exception
	{
		VerifyGatePassCheckIn();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		  
		common.driverClose();
	
    }

}
