package fleetModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BATF.Pages.FleetManagementModuleData;
import bileeta.BTAF.PageObjects.FleetManagementModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class TC_CAL_YY_D_02 extends FleetManagementModule{

	TestCommonMethods common = new TestCommonMethods();
	
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Fleet", "", "Verify that Excess mileage, Excess mileage charge calculate correctly when not having any driver or vehicle replacement- Daily package [Not having excess]", this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("open the browser and enter the application URL")
	public void LoginFleet() throws Exception {
	
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		clickNavigation();
		ClickFleet();
	}
	
	@Test(dependsOnMethods = "LoginFleet")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Excess mileage, Excess mileage charge calculate correctly when not having any driver or vehicle replacement- Daily package [Not having excess]")
	public void VerThatExcessMileageExcessMileageChargeCalculateCorrectlyWhenNotHavingAnyDriverOrVehicleReplacementDailyPackWithoutExcess() throws Exception {
	
		VerifyThatExcessMileageExcessMileageChargeCalculateCorrectlyWhenNotHavingAnyDriverOrVehicleReplacementDailyPackWithoutExcess();
	}
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		  
		common.driverClose();
		 
	}
}
