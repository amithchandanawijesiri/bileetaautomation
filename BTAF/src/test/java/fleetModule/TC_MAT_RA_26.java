package fleetModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BATF.Pages.FleetManagementModuleData;
import bileeta.BTAF.PageObjects.FleetManagementModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class TC_MAT_RA_26 extends FleetManagementModule{

	TestCommonMethods common = new TestCommonMethods();
	
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Fleet", "", "Verify that user allow to create rent agreement with having 'Location To Yard' alllocation type and company Driver and Hourly package", this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("open the browser and enter the application URL")
	public void LoginFleet() throws Exception {
	
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		clickNavigation();
		ClickFleet();
	}
	
	@Test(dependsOnMethods = "LoginFleet")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user allow to create rent agreement with having 'Location To Yard' alllocation type and company Driver and Hourly package")
	public void VerThatUserAllowToCreateRentAgreementWithHavingLocationToYardAlllocationTypeAndCompanyDriverAndHourlyPackage() throws Exception {
	
		VerifyThatUserAllowToCreateRentAgreementWithHavingLocationToYardAlllocationTypeAndCompanyDriverAndHourlyPackage();
	}
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		  
		common.driverClose();
		 
	}
}