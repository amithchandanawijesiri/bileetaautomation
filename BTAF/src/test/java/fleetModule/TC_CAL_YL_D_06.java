package fleetModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BATF.Pages.FleetManagementModuleData;
import bileeta.BTAF.PageObjects.FleetManagementModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class TC_CAL_YL_D_06 extends FleetManagementModule{

	TestCommonMethods common = new TestCommonMethods();
	
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Fleet", "", "Verify that Excess hours, Excess hours charge, Excess mileage,Excess mileage charge calculate correctly when having both vehicle replacement and Driver replacement", this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("open the browser and enter the application URL")
	public void LoginFleet() throws Exception {
	
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		clickNavigation();
		ClickFleet();
	}
	
	@Test(dependsOnMethods = "LoginFleet")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Excess hours, Excess hours charge, Excess mileage,Excess mileage charge calculate correctly when having both vehicle replacement and Driver replacement")
	public void VerThatExcessHoursExcessHoursChargeExcessMileageExcessMileageChargeCalculateCorrectlyWhenHavingBothVehicleReplacementAndDriverReplacement2YardToLocation() throws Exception {
	
		VerifyThatExcessHoursExcessHoursChargeExcessMileageExcessMileageChargeCalculateCorrectlyWhenHavingBothVehicleReplacementAndDriverReplacement2YardToLocation();
	}
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		  
		common.driverClose();
		 
	}
}