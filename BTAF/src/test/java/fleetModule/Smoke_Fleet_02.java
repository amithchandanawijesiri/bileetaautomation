package fleetModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;


import bileeta.BTAF.PageObjects.FleetManagementSmoke;

import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestCommonMethods;
import bileeta.BTAF.Utilities.TestListener;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
@Listeners({TestListener.class})

public class Smoke_Fleet_02 extends FleetManagementSmoke{
	
TestCommonMethods common = new TestCommonMethods();
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("FleetManagement", "", "\r\n" + "Verify that creating fleet parameters", this.getClass().getSimpleName());
	
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Open the browser and enter the application URL")
	public void _navigateToTheLoginPage() throws Exception
	{
		navigateToTheLoginPage();
	}
	
	@Test(dependsOnMethods = "_navigateToTheLoginPage")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that 'Entution' header is available on the page")
	public void _verifyTheLogo() throws Exception
	{
		verifyTheLogo();
		
	}
	
	@Test(dependsOnMethods = "_verifyTheLogo")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that login to the system")
	public void _userLogin() throws Exception
	{
		userLogin();
		
	}
	
	@Test(dependsOnMethods = "_userLogin")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Navigation menus")
	public void _menuNavigation() throws Exception
	{
		navigationmenu();
		
	}
	
	@Test(dependsOnMethods = "_menuNavigation")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Vehicle Information Bypage")
	public void _vehicleInformationBypageNavigation() throws Exception
	{
		NavigationVehicleInformationByPage();
		
	}
	
	@Test(dependsOnMethods = "_vehicleInformationBypageNavigation")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Vehicle Information New page")
	public void _vehicleInformationNewpageNavigation() throws Exception
	{
		NavigationVehicleInformationNewPage();
		
	}
	
	@Test(dependsOnMethods = "_vehicleInformationNewpageNavigation")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Vehicle Creation")
	public void _vehicleCreation() throws Exception
	{
		VerifyThatAbleToCreateVehicle();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		  
		common.driverClose();
	
    }


}
