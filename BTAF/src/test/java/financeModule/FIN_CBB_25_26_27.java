package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleRegression;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class FIN_CBB_25_26_27 extends FinanceModuleRegression {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "Cash Bank Book", "Verify whether user can release a Bank Book with Lease Facility AND Verify whether Lease Facility Bank Book can't be viewed in Bank Adjustment AND Verify whether Lease Facility Bank Book can't be viewed in Bank Reconciliaton",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Release Bank Book with Lease Facility")
	@Step("Login to the Entution")
	public void verifyBankBookWithLeaseFacility_FIN_CBB_25_26_27() throws Exception {
		bankBookWithLeaseFacility_FIN_CBB_25_26_27();
	}
	
	@Test(dependsOnMethods = {"verifyBankBookWithLeaseFacility_FIN_CBB_25_26_27"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check account with Bank Adjustment and Bank Reconcilation")
	public void verifyAndCheckWithBankAdjustmentAndReconcilation_FIN_CBB_25_26_27() throws Exception {
		checkWithBankAdjustmentAndReconcilation_FIN_CBB_25_26_27();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
