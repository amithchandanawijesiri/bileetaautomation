package financeModule;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleSmoke;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})

public class Smoke_Finance_11 extends FinanceModuleSmoke{
TestCommonMethods common = new TestCommonMethods();
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Finance", "", "Verify the ability of creating a manual journal entry", this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Journel Entry Page")
	@Step("Login to the Entution, Navigate to the Finance Module")
	public void verifyNavigateToJourneEntryPage() throws Exception
	{
		navigateToJourneEntryPage();
	}
	
	@Test(dependsOnMethods = "verifyNavigateToJourneEntryPage")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to New Journel Entry Form")
	public void verifyNavigateToNewJournelEntryForm() throws Exception
	{
		navigateToNewJournelEntryForm();
	}
	
	@Test(dependsOnMethods = "verifyNavigateToNewJournelEntryForm")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill Bank Adjustment details")
	public void verifyFillBankAdjustment() throws Exception
	{
		fillBankAdjustment();
	}
	
	@Test(dependsOnMethods = "verifyFillBankAdjustment")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Drafe Journel Entry")
	public void verifyDraftJournelEntrySmoke() throws Exception
	{
		draftJournelEntrySmoke();
	}
	
	@Test(dependsOnMethods = "verifyDraftJournelEntrySmoke")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Release Journel Entry")
	public void verifyReleaseJournelEntrySmoke() throws Exception
	{
		releaseJournelEntrySmoke();
	}
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		common.driverClose();
		 
	}
}
