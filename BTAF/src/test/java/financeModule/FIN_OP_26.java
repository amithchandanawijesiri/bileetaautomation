package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleRegression4;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class FIN_OP_26 extends FinanceModuleRegression4 {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "Outbound Payment",
				"Verify the Outbound Payment when Paying currency is Base currency and filter currency is foreign currency", this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Release a Vendor Credit Memo")
	@Step("Login to the Entution")
	public void verifyOutboundPaymentVendorPaymentVoucherUSD_1_FIN_OP_26() throws Exception {
		outboundPaymentVendorPaymentVoucherUSD_1_FIN_OP_26();
	}
	
	@Test(dependsOnMethods = "verifyOutboundPaymentVendorPaymentVoucherUSD_1_FIN_OP_26")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Release the Vendor Payment Voucher - Base Currency:LKR and Filter Currency:USD")
	public void verifyOutboundPaymentVendorPaymentVoucherUSD_2_FIN_OP_26() throws Exception {
		outboundPaymentVendorPaymentVoucherUSD_2_FIN_OP_26();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
