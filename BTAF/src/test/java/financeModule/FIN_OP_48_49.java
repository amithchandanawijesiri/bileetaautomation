package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleRegression4;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class FIN_OP_48_49 extends FinanceModuleRegression4 {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "Outbound Payment",
				"Verify that user can edit drafted Outbound Payment (Update & New) AND Verify that user can't Duplicate a Drafted Outbound Payment",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Release a Employee Payment Advice")
	@Step("Login to the Entution")
	public void verifyOpdateAndNewOutboundPaymentEmployeePaymentVoucher_1_FIN_OP_48_49() throws Exception {
		updateAndNewOutboundPaymentEmployeePaymentVoucher_1_FIN_OP_48_49();
	}

	@Test(dependsOnMethods = "verifyOpdateAndNewOutboundPaymentEmployeePaymentVoucher_1_FIN_OP_48_49")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Release a Employee Payment Voucher - Edit / Update & New")
	public void verifyUpdateAndNewOutboundPaymentEmployeePaymentVoucher_2_FIN_OP_48_49() throws Exception {
		updateAndNewOutboundPaymentEmployeePaymentVoucher_2_FIN_OP_48_49();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
