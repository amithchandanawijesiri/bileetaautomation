package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleRegression2;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class FIN_OPA_14_to_19 extends FinanceModuleRegression2 {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "Outbound Payment Advice", "Vendor Credit Memo", this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Outbound Payment Advice - Vendor Credit Memo")
	@Step("Login to the Entution")
	public void verifyOutboundPaymentAdviceVendorCreditMemo_FIN_OPA_14_to_19() throws Exception {
		outboundPaymentAdviceVendorCreditMemo_FIN_OPA_14_to_19();
	}

	@Test(dependsOnMethods = "verifyOutboundPaymentAdviceVendorCreditMemo_FIN_OPA_14_to_19")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Outbound Payment - Vendor Credit Memo")
	public void verifyOutboundPaymentVendorCreditMemo_FIN_OPA_14_to_19() throws Exception {
		outboundPaymentVendorCreditMemo_FIN_OPA_14_to_19();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
