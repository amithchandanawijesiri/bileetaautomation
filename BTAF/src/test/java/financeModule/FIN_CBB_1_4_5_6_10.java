package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleRegression;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class FIN_CBB_1_4_5_6_10 extends FinanceModuleRegression {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "Cash Bank Book",
				"Verify whether user able to navigate to Cash/Bank Book by-page AND Verify whether user able to view only specified existing datas when user used find function AND Navigation to Cash/Bank Book  - New page AND Verify new Cash/Bank Book can be generate  - Cash Book AND Verify that user can use Draft",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create Cash Book")
	@Step("Login to the Entution")
	public void verifyCashBankBook_FIN_CBB_1_4_5_6_10() throws Exception {
		cashBankBook_FIN_CBB_1_4_5_6_10();
	}
	
	@Test(dependsOnMethods = "verifyCashBankBook_FIN_CBB_1_4_5_6_10")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Find released Cash/Bank Book")
	@Step("Login to the Entution")
	public void verifyFindReleasedCashBook_FIN_CBB_1_4_5_6_10() throws Exception {
		findReleasedCashBook_FIN_CBB_1_4_5_6_10();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
