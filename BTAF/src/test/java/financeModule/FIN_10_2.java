package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleRegression3;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class FIN_10_2 extends FinanceModuleRegression3 {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		
		common.setUp("Finance", "", "\r\n" + "Verify whether if user don't have permission to view Inbound Payment Advice by-page, user want able to navigate to by-page", this.getClass().getSimpleName());
	}

	@Test//(priority = 1)
	@Severity(SeverityLevel.CRITICAL)
	@Description("User login and Click on navigation menu")
	public void clickNav() throws Exception {

		navigateToTheLoginPage();
		verifyTheLogo();
		userLoginNew();
		clickNavigation();
		ClickFinanceButton();
	}
	
	@Test(dependsOnMethods = "clickNav")//,priority = 2)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify whether if user don't have permission to view Inbound Payment Advice by-page, user want able to navigate to by-page")
	public void VerWhetherIfUserDontHavePermissionToViewInboundPaymentAdviceByPageUserWantAbleToNavigateToByPage() throws Exception {
	
		VerifyWhetherIfUserDontHavePermissionToViewInboundPaymentAdviceByPageUserWantAbleToNavigateToByPage();
	}
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		  
		common.driverClose();
		 
	}
}



