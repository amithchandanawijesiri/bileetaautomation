package financeModule;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})

public class Fin_TC_015 extends FinanceModule{
TestCommonMethods common = new TestCommonMethods();
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Finance", "", "Verify that user can successfully create Outbound Payment  - Vendor Payment Voucher", this.getClass().getSimpleName());
	}
	@Test(dependsOnMethods = "financeModule.Fin_TC_014.verifyCheckoutDraftAndReleaseVendorCreditMemo")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Customer Payment Voucher")
	@Step("Login to the Entution, Navigate to the Finance Module")
	public void verifyNavigateToVendorCreditVoucher() throws Exception
	{
		navigateToVendorCreditVoucher();
	}
	
	@Test(dependsOnMethods = "verifyNavigateToVendorCreditVoucher")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill Vendor Payment Voucher")
	public void verifyFillOutboundPaymentVendorReciptVoucher() throws Exception
	{
		fillOutboundPaymentVendorReciptVoucher();
	}
	
	@Test(dependsOnMethods = "verifyFillOutboundPaymentVendorReciptVoucher")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill details tab")
	public void verifySlectPaymentAdviceAndFillDetailsTab() throws Exception
	{
		slectPaymentAdviceAndFillDetailsTab();
	}
	
	@Test(dependsOnMethods = "verifySlectPaymentAdviceAndFillDetailsTab")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Checkout, Draft and Release Outbound Payment - Vendor Payment Voucher")
	public void verifyCheckoutDraftAndReleaseOutboundPayment_VendorPayment() throws Exception
	{
		checkoutDraftAndReleaseOutboundPayment_VendorPayment();
	}
	
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		common.driverClose();
		 
	}

}
