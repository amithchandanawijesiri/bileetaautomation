package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})


public class Com_TC_001 extends FinanceModule{
	
	TestCommonMethods common = new TestCommonMethods();
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Finance", "", "Verify that user can successfully loing to the Entution with valid Username and valid Password ", this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Open the browser and enter the application URL")
	public void verifyNavigateToTheLoginPage() throws Exception
	{
		navigateToTheLoginPage();
	}
	@Test(dependsOnMethods = "verifyNavigateToTheLoginPage")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify the logo")
	public void verifyToTheLogo() throws Exception
	{
		verifyTheLogo();
	}
	@Test(dependsOnMethods = "verifyToTheLogo")
	@Severity(SeverityLevel.CRITICAL)
	@Description("User login to the system")
	public void verifyUserLogin() throws Exception
	{
		userLogin();
	}
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		common.driverClose();
		 
	}
	
}
