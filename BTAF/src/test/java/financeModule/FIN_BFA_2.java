package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleRegression2;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class FIN_BFA_2 extends FinanceModuleRegression2 {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "Bank Facility Agreement", "Verify whether if user don't have permission to view Bank Facility Agreement by-page, user want able to navigate to by-page",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Configure User Permission and check with Bank Facility Agreement")
	@Step("Login to the Entution")
	public void verifyConfigureUserPermissionAndCheckBankAdjustment_FIN_BFA_2() throws Exception {
		configureUserPermissionAndCheckBankAdjustment_FIN_BFA_2();
	}
	
	@Test(dependsOnMethods = "verifyConfigureUserPermissionAndCheckBankAdjustment_FIN_BFA_2")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Bank Facility Agreement - Reset User Permission")
	public void verifyResetBFAUserPermissions_FIN_BFA_2() throws Exception {
		resetBFAUserPermissions_FIN_BFA_2();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
