package financeModule;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleSmoke;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})

public class Smoke_Finance_02 extends FinanceModuleSmoke{
TestCommonMethods common = new TestCommonMethods();
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Finance", "", "Verify the ability of creating a loan using the bank facility agreement", this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to New Bank Facility Agreement")
	@Step("Login to the Entution, Navigate to the Finance Module")
	public void verifyNavigateToNewBankFacilityAgreement() throws Exception
	{
		navigateToNewBankFacilityAgreement();
	}
	
	@Test(dependsOnMethods = "verifyNavigateToNewBankFacilityAgreement")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill Bank Facility Agreement")
	public void verifyFillBankFacilityAgreementSmoke() throws Exception
	{
		fillBankFacilityAgreementSmoke();
	}
	
	@Test(dependsOnMethods = "verifyFillBankFacilityAgreementSmoke")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft Bank Facility Agreement")
	public void verifyDraftBankFacilityAgreementSmoke() throws Exception
	{
		draftBankFacilityAgreementSmoke();
	}
	
	@Test(dependsOnMethods = "verifyDraftBankFacilityAgreementSmoke")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Release Bank Facility Agreement")
	public void verifyReleaseBankFacilityAgreementSmoke() throws Exception
	{
		releaseBankFacilityAgreementSmoke();
	}
	
	@Test(dependsOnMethods = "verifyReleaseBankFacilityAgreementSmoke")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify loan creation and juornel entry validity")
	public void verifyLoanCreationAndJournelEntryValidity() throws Exception
	{
		loanCreationAndJournelEntryValidity();
	}
	
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		common.driverClose();
		 
	}
}
