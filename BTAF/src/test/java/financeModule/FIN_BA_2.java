package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleRegression;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class FIN_BA_2 extends FinanceModuleRegression {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "Bank Adjustment", "Verify whether if user don't have permission to view Bank Adjustment by-page, user want able to navigate to by-page",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check Bank Adjustment by page after restrict user permission")
	@Step("Login to the Entution")
	public void verifyConfigureUserPermissionAndCheckBankAdjustment_FIN_BA_2() throws Exception {
		configureUserPermissionAndCheckBankAdjustment_FIN_BA_2();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		resetBankAdjustmentUserPermissions_FIN_BA_2();
		common.totalTime(startTime);
		common.driverClose();

	}

}
