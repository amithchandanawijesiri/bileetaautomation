package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})

public class Fin_TC_007 extends FinanceModule{
TestCommonMethods common = new TestCommonMethods();
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Finance", "", "Verify that user can successfully create Inbound Payment  - Customer Receipt Voucher", this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to the Customer Recipt Voucher Form")
	@Step("User login to the Entution, Navigate to the Finance Module")
	public void verifyNavigateToTheCustomerReciftVoucherForm() throws Exception
	{
		navigateToTheCustomerReciptVoucherForm();
	}
	
	@Test(dependsOnMethods = "verifyNavigateToTheCustomerReciftVoucherForm")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill the Customer Recipt Voucher")
	public void verifyFillCRVDeatails() throws Exception
	{
		fillCRVDeatails();
	}
	
	@Test(dependsOnMethods = "verifyFillCRVDeatails")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Checkout, Draft and Releese Customer Recipt Voucher")
	public void verifyCheckoutDraftReleeseCrv7() throws Exception
	{
//		checkoutDraftreleaseCrv7();
	}
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		common.driverClose();
		 
	}
	
}
