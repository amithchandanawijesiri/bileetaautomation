package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleRegression2;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class FIN_BFA_34_35_36_40_I_41_I extends FinanceModuleRegression2 {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "Bank Facility Agreement",
				"Verify that user can create Lease Facility Agreement / Journel Entries / Tag Fixed Assert",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create Lease Fasility Agreement")
	@Step("Login to the Entution")
	public void verifyLeaseFacilityAgreement1_FIN_BFA_34_35_36_40_I_41_I() throws Exception {
		leaseFacilityAgreement1_FIN_BFA_34_35_36_40_I_41_I();
	}

	@Test(dependsOnMethods = "verifyLeaseFacilityAgreement1_FIN_BFA_34_35_36_40_I_41_I")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create Lease Fasility Agreement")
	public void verifyLeaseFacilityAgreement2_FIN_BFA_34_35_36_40_I_41_I() throws Exception {
		leaseFacilityAgreement2_FIN_BFA_34_35_36_40_I_41_I();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		
		common.totalTime(startTime);
		common.driverClose();

	}

}
