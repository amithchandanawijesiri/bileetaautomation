package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleRegression2;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class FIN_PC_26 extends FinanceModuleRegression2 {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "Petty Cash",
				"Verify that system does not allow to reverse a petty cash transaction if there is already an petty cash transaction for ahead date (Post Date)",
				this.getClass().getSimpleName());
	}

	/*
	 * Note: This test case only can execute one time . If u need to execute again
	 * reverse last document on current date
	 */

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Try to reverse last ceated Petty Cash")
	@Step("Login to the Entution")
	public void verifyPettyCash_FIN_PC_26() throws Exception {
		pettyCash_FIN_PC_26();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
