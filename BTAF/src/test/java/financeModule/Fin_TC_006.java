package financeModule;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})

public class Fin_TC_006 extends FinanceModule{
TestCommonMethods common = new TestCommonMethods();
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Finance", "", "Verify that user can successfully create Inbound Payment Advice - Customer Debit ", this.getClass().getSimpleName());
	}
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Customer Debit Memo Form")
	@Step("Login to the Entution, Navigate to the Finance Module")
	public void verifyNavigattToInboundPaymentAdviceCustomerDebitMemoForm() throws Exception
	{
		navigattToInboundPaymentAdviceCustomerDebitMemoForm();
	}
	
	@Test(dependsOnMethods = "verifyNavigattToInboundPaymentAdviceCustomerDebitMemoForm")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill Customer Debit Memo")
	public void verifyFillCustomerDebitMemoForm() throws Exception
	{
		fillCustomerDebitMemoForm();
	}
	
	@Test(dependsOnMethods = "verifyFillCustomerDebitMemoForm")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Find GL account and do cost allocations")
	public void verifyFindGlAndCostAllocatioCustomerDebitMemo() throws Exception
	{
		findGlAndCostAllocatioCustomerDebitMemo();
	}
	
	@Test(dependsOnMethods ="verifyFindGlAndCostAllocatioCustomerDebitMemo" )
	@Severity(SeverityLevel.CRITICAL)
	@Description("Checkout, Draft and Relesse Customer Debit Memo")
	public void verifyCheckoutDraftReleeseCustomerDebitMemo() throws Exception
	{
		checkoutDraftreleaseCustomerDebitMemo();
	}
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		common.driverClose();
		 
	}
}
