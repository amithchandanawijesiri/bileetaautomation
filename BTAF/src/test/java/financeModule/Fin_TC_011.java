package financeModule;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})

public class Fin_TC_011 extends FinanceModule{
TestCommonMethods common = new TestCommonMethods();
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Finance", "", "Verify that user can successfully create Outbound Payment  - Vendor Payment Voucher - Convert from Outbound Payment advice", this.getClass().getSimpleName());
	}
	@Test(dependsOnMethods = "financeModule.Fin_TC_010.verifyCheckoutDarftReleeseOutboundPayment")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Outbound Payment Advice")
	@Step("Login to the Entution, Navigate to the Finance Module")
	public void verifyNavigateToOutboundPaymentAdvice() throws Exception
	{
		navigateToOutboundPaymentAdvice();
	}
	
	@Test(dependsOnMethods = "verifyNavigateToOutboundPaymentAdvice")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Outbound Payment Advice conver to Outbound Payment")
	public void verifyConverToOutboundPayment() throws Exception
	{
		converToOutboundPayment();
	}
	
	@Test(dependsOnMethods = "verifyConverToOutboundPayment")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill Outbound Payment")
	public void verifyFillOutboundPayment() throws Exception
	{
		fillOutboundPayment();
	}
	
	@Test(dependsOnMethods = "verifyFillOutboundPayment")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill details page of Outbound Payment")
	public void verifyFillDetailPage() throws Exception
	{
		fillDetailPage();
	}
	
	@Test(dependsOnMethods = "verifyFillDetailPage")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Checkout, Draft and Release Outbound Payment")
	public void verifyDraftReleeseChecoutOP() throws Exception
	{
		draftreleaseChecoutOP();
	}
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		common.driverClose();
	}
}
