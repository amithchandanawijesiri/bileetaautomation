package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleRegression;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class FIN_BD_32_33_34 extends FinanceModuleRegression {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "Bank Deposit",
				"Verify that Bank Deposit can be released with all types of cheque Inbound Payments and Verify Journel Entry / Deposit Amount fields",
				this.getClass().getSimpleName());
	}

	@Test /*
			 * (dependsOnMethods = {
			 * "financeModule.FIN_BD_28.verifyInboundPayment_FIN_BD_28",
			 * "financeModule.FIN_BD_29.verifyInboundPayment_FIN_BD_29",
			 * "financeModule.FIN_BD_30.verifyInboundPayment_FIN_BD_30",
			 * "financeModule.FIN_BD_31.verifyInboundPayment_FIN_BD_31" })
			 */
	@Severity(SeverityLevel.CRITICAL)
	@Description("Bank deposit with all types of cheque Inbound Payments")
	public void verifyBankDeposit_FIN_BD_32_33_34() throws Exception {
		bankDeposit_FIN_BD_32_33_34();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
