package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleRegression;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class FIN_BD_9 extends FinanceModuleRegression {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "Bank Deposit", "Verify that if filter \"Cheque No\" used to view availble deposits, then only advices related to entered cheque no.  is available",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Inbound Payment Advice")
	@Step("Login to the Entution")
	public void verifyInboundPaymentAdvice_FIN_BD_9() throws Exception {
		inboundPaymentAdvice_FIN_BD_9();
	}

	@Test(dependsOnMethods = "verifyInboundPaymentAdvice_FIN_BD_9")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Inbound Payment")
	public void verifyInboundPayment_FIN_BD_9() throws Exception {
		inboundPayment_FIN_BD_9();
	}

	@Test(dependsOnMethods = "verifyInboundPayment_FIN_BD_9")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Payment filtered by cheque")
	public void verifyBankDepositFilterPaymentByChequeNumber_FIN_BD_9() throws Exception {
		bankDepositFilterPaymentByChequeNumber_FIN_BD_9();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
