package financeModule;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;
import java.util.*;

public class doc_flow_click {

	private WebDriver driver1;
	private Map<String, Object> vars;
	JavascriptExecutor js;

	@BeforeClass
	public void setUp() {
		String location = System.getProperty("user.dir");
		System.setProperty("webdriver.chrome.driver", location+"\\Web_Drivers\\chromedriver.exe");
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-gpu");
		options.addArguments("--disable-extensions");
		options.setExperimentalOption("useAutomationExtension", false);
		options.addArguments("--proxy-server='direct://'");
		options.addArguments("--proxy-bypass-list=*");
		options.addArguments("--start-maximized");
		driver1 = new ChromeDriver();
		vars = new HashMap<String, Object>();
	}

	@AfterClass
	public void tearDown() {
		driver1.quit();
	}

	public String waitForWindow(int timeout) {
		try {
			Thread.sleep(timeout);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Set<String> whNow = driver1.getWindowHandles();
		Set<String> whThen = (Set<String>) vars.get("window_handles");
		if (whNow.size() > whThen.size()) {
			whNow.removeAll(whThen);
		}
		return whNow.iterator().next();
	}

	@Test
	public void untitled() {
		driver1.get("http://124.43.19.5:2022/Web/sales/salesinvoice/viewsalesinvoice.aspx?id=67419153525901&pid=81");
		driver1.manage().window().setSize(new Dimension(1382, 745));
		driver1.findElement(By.id("eleLoadAction")).click();
		driver1.findElement(By.cssSelector(".pic48-documentflow")).click();
		vars.put("window_handles", driver1.getWindowHandles());
		driver1.findElement(By.id("fi2")).click();
		vars.put("win7305", waitForWindow(2000));
		driver1.switchTo().window(vars.get("win7305").toString());
	}
}
