package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleRegression2;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class FIN_PC_19 extends FinanceModuleRegression2 {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "Petty Cash", "Verify that user can use Draft and New",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify Draft & New button")
	@Step("Login to the Entution")
	public void verifyPettyCashDraftAndNew1_FIN_PC_19() throws Exception {
		pettyCashDraftAndNew1_FIN_PC_19();
	}
	
	@Test(dependsOnMethods = "verifyPettyCashDraftAndNew1_FIN_PC_19")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify Draft & New button")
	public void verifyPettyCashDraftAndNew2_FIN_PC_19() throws Exception {
		pettyCashDraftAndNew2_FIN_PC_19();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
