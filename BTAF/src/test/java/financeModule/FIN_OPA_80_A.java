package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleRegression4;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class FIN_OPA_80_A extends FinanceModuleRegression4 {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "Outbound Payment Advice - Employee Payment Advice",
				"Verify that user can use Draft and New", this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft & New Employee Payment Advice")
	@Step("Login to the Entution")
	public void verifyOutboundPaymentAdviceEmployeePaymentAdviceDraftAndNew1_FIN_OPA_80_A() throws Exception {
		outboundPaymentAdviceEmployeePaymentAdviceDraftAndNew1_FIN_OPA_80_A();
	}
	
	@Test(dependsOnMethods = "verifyOutboundPaymentAdviceEmployeePaymentAdviceDraftAndNew1_FIN_OPA_80_A")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft & New Employee Payment Advice")
	public void verifyOutboundPaymentAdviceEmployeePaymentAdviceDraftAndNew2_FIN_OPA_80_A() throws Exception {
		outboundPaymentAdviceEmployeePaymentAdviceDraftAndNew2_FIN_OPA_80_A();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
