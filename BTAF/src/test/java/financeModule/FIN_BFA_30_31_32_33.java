package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleRegression2;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class FIN_BFA_30_31_32_33 extends FinanceModuleRegression2 {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "Bank Facility Agreement",
				"Verify that user can create Bank Cash/Bank Book including 'Lease Facility' option AND Verify that Lease Facility check box is inactive after the releasing the Cash/Bank book AND Verify that Bank Reconciliation is not available for above lease facility bank book AND Verify that lease facility agreements are not allowed to process payments through bank adjustment",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify Cash Bank Book - Lease Facility")
	@Step("Login to the Entution")
	public void verifyBankBookWithLeaseFacility1_FIN_BFA_30_31_32_33() throws Exception {
		bankBookWithLeaseFacility1_FIN_BFA_30_31_32_33();
	}

	@Test(dependsOnMethods = "verifyBankBookWithLeaseFacility1_FIN_BFA_30_31_32_33")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify Cash Bank Book - Lease Facility")
	public void verifyBankBookWithLeaseFacility2_FIN_BFA_30_31_32_33() throws Exception {
		bankBookWithLeaseFacility2_FIN_BFA_30_31_32_33();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
