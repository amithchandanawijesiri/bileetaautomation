package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleRegression;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class FIN_BD_21 extends FinanceModuleRegression {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "Bank Deposit", "Verify that correct exchange rate taken to cheque deposit, if payment cheque is in USD",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Inbound Payment Advice - Exchange rate : 100")
	@Step("Login to the Entution")
	public void verifyInboundPaymentAdvice_FIN_BD_21() throws Exception {
		inboundPaymentAdvice_FIN_BD_21();
	}

	@Test(dependsOnMethods = "verifyInboundPaymentAdvice_FIN_BD_21")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Inbound Payment - Exchange rate : 125")
	public void verifyInboundPayment_FIN_BD_21() throws Exception {
		inboundPayment_FIN_BD_21();
	}

	@Test(dependsOnMethods = "verifyInboundPayment_FIN_BD_21")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Bank Deposit and verify Journel Entry - Exchange rate : 150")
	public void verifyBankDeposit_FIN_BD_21() throws Exception {
		bankDeposit_FIN_BD_21();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
