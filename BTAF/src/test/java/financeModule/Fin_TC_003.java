package financeModule;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})

public class Fin_TC_003 extends FinanceModule{
TestCommonMethods common = new TestCommonMethods();
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Finance", "", "Verify that user can successfully create Inbound Payment  - Customer Receipt Voucher - Convert from Inbound Payment advice", this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Sales Invoise")
	@Step("User login to the Entution")
	public void verifyNavigateToSalesInvoice() throws Exception
	{
		navigateToSalesInvoice();
	}
	
	@Test(dependsOnMethods = "verifyNavigateToSalesInvoice")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create Sales Invoice")
	public void verifyComplteSlaesInvoice() throws Exception
	{
		complteSlaesInvoice();
	}
	
	@Test(dependsOnMethods = "verifyComplteSlaesInvoice")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft and releese Sales Invoice and check auto genarated Inbound Payment Advice")
	public void verifyDraftReleeseAndCheckDockFlowSalesInvoice() throws Exception
	{
		draftreleaseAndCheckDockFlowSalesInvoice();
	}
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		common.driverClose();
		 
	}
}
