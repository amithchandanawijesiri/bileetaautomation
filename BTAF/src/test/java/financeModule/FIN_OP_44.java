package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleRegression4;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class FIN_OP_44 extends FinanceModuleRegression4 {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "Outbound Payment", "Veridy that user can able to return the cheque",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = "financeModule.FIN_OP_43.verifyOutboundPaymentEmployeePaymentVoucher_2_FIN_OP_43")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Return the cheque")
	@Step("Login to the Entution")
	public void verifyOutboundPaymentEmployeePaymentVoucherCheckReturn_FIN_OP_44() throws Exception {
		outboundPaymentEmployeePaymentVoucherCheckReturn_FIN_OP_44();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
