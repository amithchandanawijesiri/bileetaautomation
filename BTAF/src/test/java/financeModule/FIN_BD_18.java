package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleRegression;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class FIN_BD_18 extends FinanceModuleRegression {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "Bank Deposit", "Verify that user can use Draft and New",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Second Inbound Payment Advice")
	@Step("Login to the Entution")
	public void verifyInboundPaymentAdvice1_FIN_BD_18() throws Exception {
		inboundPaymentAdvice1_FIN_BD_18();
	}

	@Test(dependsOnMethods = "verifyInboundPaymentAdvice1_FIN_BD_18")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Second Inbound Payment")
	public void verifyInboundPayment1_FIN_BD_18() throws Exception {
		inboundPayment1_FIN_BD_18();
	}
	
	@Test(dependsOnMethods = "verifyInboundPayment1_FIN_BD_18")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Second Inbound Payment Advice")
	public void verifyInboundPaymentAdvice2_FIN_BD_18() throws Exception {
		inboundPaymentAdvice2_FIN_BD_18();
	}

	@Test(dependsOnMethods = "verifyInboundPaymentAdvice2_FIN_BD_18")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Second Inbound Payment")
	public void verifyInboundPayment2_FIN_BD_18() throws Exception {
		inboundPayment2_FIN_BD_18();
	}

	@Test(dependsOnMethods = "verifyInboundPayment2_FIN_BD_18")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check Draft & New function")
	public void verifyBankDeposit_FIN_BD_18() throws Exception {
		bankDeposit_FIN_BD_18();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
