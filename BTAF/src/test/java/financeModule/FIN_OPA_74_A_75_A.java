package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleRegression2;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class FIN_OPA_74_A_75_A extends FinanceModuleRegression2 {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "Outbound Payment Advice",
				"Verify that user can use Draft AND Verify that user can edit drafted Outbound Payment Advice (Update)", this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Edit / Update drafted Employee Payment Advice")
	@Step("Login to the Entution")
	public void verifyDraftEditOutboundPaymentAdviceEmplyeePaymentAdvice_FIN_OPA_74_A_75_A() throws Exception {
		draftEditOutboundPaymentAdviceEmplyeePaymentAdvice_FIN_OPA_74_A_75_A();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
