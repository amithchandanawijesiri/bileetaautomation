package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleRegression;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class FIN_CBB_20 extends FinanceModuleRegression {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "Cash Bank Book", "Verify whether if cash/bank book type is Petty cash, One-off or IOU amount in petty cash can't exceed float amount given in cash/bank book",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create Petty Cash Account")
	@Step("Login to the Entution")
	public void verifyThatCreatePettyCashAccount_FIN_CBB_20() throws Exception {
		createPettyCashAccount_FIN_CBB_20();
	}
	
	@Test(dependsOnMethods = {"verifyThatCreatePettyCashAccount_FIN_CBB_20"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Petty Cash Part")
	@Step("Login to the Entution")
	public void verifyPettyCashPart_FIN_CBB_20() throws Exception {
		pettyCashPart_FIN_CBB_20();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
