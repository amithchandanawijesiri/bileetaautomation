package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleSmoke;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})


public class Smoke_Finance_06 extends FinanceModuleSmoke{
	
TestCommonMethods common = new TestCommonMethods();
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Finance", "", "Verify that user can successfully create Inbound Payment Advice - Customer Advance", this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to the Finance Module")
	@Step("User login to the Entution")
	public void verifyNavigateToTheFananceModule() throws Exception
	{
		navigateToTheFananceModule();
	}

	@Test(dependsOnMethods = "verifyNavigateToTheFananceModule")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to the Inbound Payment Advice")
	public void verifyNavigateToIPAForm() throws Exception
	{
		navigateToIPAForm();
	}
	
	@Test(dependsOnMethods = "verifyNavigateToIPAForm")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill Inbound Payment Advice")
	public void verifyFillIPAFormDetails() throws Exception
	{
		fillIPAFormDetails();
	}
	
	
	@Test(dependsOnMethods = "verifyFillIPAFormDetails")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Checkout, Draft and Releese Inbound Payment Advice")
	public void verifyCheckoutDraftReleeseIPA() throws Exception
	{
		checkoutDraftreleaseIPA();
	}
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		common.driverClose();
		 
	}
	
}
