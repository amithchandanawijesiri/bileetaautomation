package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleRegression4;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class FIN_OP_50 extends FinanceModuleRegression4 {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "Outbound Payment", "Verify that user can Delete a Drafted Outbound Payment",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Release a Employee Payment Advice")
	@Step("Login to the Entution")
	public void verifyDeleterDraftedOutboundPaymentEmployeePaymentVoucher_1_FIN_OP_50() throws Exception {
		deleterDraftedOutboundPaymentEmployeePaymentVoucher_1_FIN_OP_50();
	}

	@Test(dependsOnMethods = "verifyDeleterDraftedOutboundPaymentEmployeePaymentVoucher_1_FIN_OP_50")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Delete a Employee Payment Voucher")
	public void verifyDeleterDraftedOutboundPaymentEmployeePaymentVoucher_2_FIN_OP_50() throws Exception {
		deleterDraftedOutboundPaymentEmployeePaymentVoucher_2_FIN_OP_50();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
