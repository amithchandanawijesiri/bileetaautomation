package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleRegression2;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class FIN_PC_23_24 extends FinanceModuleRegression2 {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "Petty Cash", "Verify that user can release Settlement Petty Cash AND Verify that only unsettled IOU Nos will loading to the look up",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Settlement type Petty Cash Release and verify unsettled IOU")
	@Step("Login to the Entution")
	public void verifyPettyCashSettlement1_FIN_PC_23_24() throws Exception {
		pettyCashSettlement1_FIN_PC_23_24();
	}
	
	@Test(dependsOnMethods = "verifyPettyCashSettlement1_FIN_PC_23_24")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Settlement type Petty Cash Release and verify settled IOU")
	public void verifyPettyCashSettlement2_FIN_PC_23_24() throws Exception {
		pettyCashSettlement2_FIN_PC_23_24();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
