package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class Fin_TC_008 extends FinanceModule {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "",
				"Verify that user can successfully create Inbound Payment  - Customer Receipt Voucher - Convert from Inbound Payment advice",
				this.getClass().getSimpleName());
	}
	
	@Test(dependsOnMethods = "financeModule.Fin_TC_006.verifyCheckoutDraftReleeseCustomerDebitMemo")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Find relevent Inbound Payment Advice")
	@Step("User login to the Entution, Navigate to the Finance Module")
	public void verifyFindReleventInboundPayment08() throws Exception {
		findReleventInboundPayment08();
	}
	
	@Test(dependsOnMethods = "verifyFindReleventInboundPayment08")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill Inbound Payment Form")
	public void verifyFillInboundPayment8() throws Exception {
		fillInboundPayment8();
	}
	
	@Test(dependsOnMethods = "verifyFillInboundPayment8")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Checkout, Draft and Releese Inbound Payment")
	public void verifyCheckoutDarftReleeseInboundPaymentTC08() throws Exception {
		checkoutDarftreleaseInboundPaymentTC08();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}
}
