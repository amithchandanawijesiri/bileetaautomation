package financeModule;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleSmoke;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})


public class Smoke_Finance_04_1 extends FinanceModuleSmoke{
TestCommonMethods common = new TestCommonMethods();
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Finance", "", "Verify that user can create IOU type petty cash", this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Petty Cash by-page")
	@Step("Login to the Entution, Navigate to the Finance Module")
	public void verifyNavigateToPettyCashPage() throws Exception
	{
		navigateToPettyCashPage();
	}
	
	@Test(dependsOnMethods = "verifyNavigateToPettyCashPage")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Petty Cash new page")
	public void verifyNewPettyCashIOU() throws Exception
	{
		newPettyCashIOU();
	}
	
	@Test(dependsOnMethods = "verifyNewPettyCashIOU")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill new Petty Cash form")
	public void verifyFillNewPettyCashFormIOU() throws Exception
	{
		fillNewPettyCashFormIOU();
	}
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		common.driverClose();
		 
	}
}
