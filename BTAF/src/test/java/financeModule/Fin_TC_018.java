package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class Fin_TC_018 extends FinanceModule {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "", "Verify that user can release the purchase invoice payment related to letter of guarantee through created letter of guarantee",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Find relevent Letter Of Gurentee")
	@Step("Login to the Entution, Navigate to the Finance Module")
	public void verifyFindReleventLOG() throws Exception {
		findReleventLOGAndReleasePayment();
	}
	
	@Test(dependsOnMethods = "verifyFindReleventLOG")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check Outbound Payment availability")
	public void verifyCheckOutboundPaymentAvailabilityLOG() throws Exception {
		checkOutboundPaymentAvailabilityLOG();
	}
	

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
