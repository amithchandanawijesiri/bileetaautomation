package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleRegression2;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class FIN_OPA_62_63 extends FinanceModuleRegression2 {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "Outbound Payment Advice",
				"Verify that user can convert Payment Voucher to Outbound Payment AND Verify that partial payment is not allowed to the Payment Voucher", this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Payment Voucher to Outbound Payment")
	@Step("Login to the Entution")
	public void verifyOutboundPaymentAdviceToOotboundPayment1_FIN_OPA_62_63() throws Exception {
		outboundPaymentAdviceToOotboundPayment1_FIN_OPA_62_63();
	}
	
	@Test(dependsOnMethods = {"verifyOutboundPaymentAdviceToOotboundPayment1_FIN_OPA_62_63"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Payment Voucher to Outbound Payment")
	public void verifyOutboundPaymentAdviceToOotboundPayment2_FIN_OPA_62_63() throws Exception {
		outboundPaymentAdviceToOotboundPayment2_FIN_OPA_62_63();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
