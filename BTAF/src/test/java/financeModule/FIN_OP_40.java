package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleRegression4;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class FIN_OP_40 extends FinanceModuleRegression4 {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "Outbound Payment",
				"Verify that user can't able to do fund transfer to petty cash including IOU balance",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create a Petty Cash Account")
	@Step("Login to the Entution")
	public void verifyCreatePettyCashAccount_FIN_OP_40() throws Exception {
		createPettyCashAccount_FIN_OP_40();
	}

	@Test(dependsOnMethods = "verifyCreatePettyCashAccount_FIN_OP_40")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fund transfer to created Petty Cash Account")
	public void verifyFundTransferToCreatedPettyCashBook_FIN_OP_40() throws Exception {
		fundTransferToCreatedPettyCashBook_FIN_OP_40();
	}

	@Test(dependsOnMethods = "verifyFundTransferToCreatedPettyCashBook_FIN_OP_40")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Make unsettled Petty Cash")
	public void verifyPettyCash_FIN_OP_40() throws Exception {
		pettyCash_FIN_OP_40();
	}

	@Test(dependsOnMethods = "verifyPettyCash_FIN_OP_40")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fund transfer validation")
	public void verifyFundTransferToValidate_FIN_OP_40() throws Exception {
		fundTransferToValidate_FIN_OP_40();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
