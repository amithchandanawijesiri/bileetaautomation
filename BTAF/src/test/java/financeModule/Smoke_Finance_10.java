package financeModule;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})

public class Smoke_Finance_10 extends FinanceModule{
TestCommonMethods common = new TestCommonMethods();
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Finance", "", "Verify that user can successfully create  bank reconcilliation", this.getClass().getSimpleName());
	}
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Bank Reconcilation Form")
	@Step("Login to the Entution, Navigate to the Finance Module")
	public void verifyNavigateToBankREconcilationForm() throws Exception
	{
		navigateToBankREconcilationFormSmoke();
	}
	
	@Test(dependsOnMethods = "verifyNavigateToBankREconcilationForm")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill Bank Reconcilation")
	public void verifyFillBankReconcilation() throws Exception
	{
		fillBankReconcilationSmoke();
	}
	
	@Test(dependsOnMethods = "verifyFillBankReconcilation")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify adjustment part")
	public void verifyCompleteAdjustmentBankReconcilation() throws Exception
	{
		completeAdjustmentBankReconcilationSmoke();
	}
	
	@Test(dependsOnMethods = "verifyCompleteAdjustmentBankReconcilation")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft and Release Bank Reconcilation")
	public void verfyDraftReleaseBankReconcilation() throws Exception
	{
		draftReleaseBankReconcilationSmoke();
	}
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		common.driverClose();
		 
	}
}
