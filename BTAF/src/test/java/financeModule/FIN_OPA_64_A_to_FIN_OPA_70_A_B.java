package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleRegression2;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class FIN_OPA_64_A_to_FIN_OPA_70_A_B extends FinanceModuleRegression2 {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "Outbound Payment Advice",
				"Employee Advance Payment", this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Outbound Payment Advice - Employee Advance Payment")
	@Step("Login to the Entution")
	public void verifyOutboundPaymentAdviceEmployeePaymentAdvice_FIN_OPA_64_A_to_FIN_OPA_70_A_B() throws Exception {
		outboundPaymentAdviceEmployeePaymentAdvice_FIN_OPA_64_A_to_FIN_OPA_70_A_B();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
