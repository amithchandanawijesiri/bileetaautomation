package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleRegression4;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class FIN_OP_52 extends FinanceModuleRegression4 {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "Outbound Payment", "Verify that user can use Draft and New",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Release the first Employee Payment Advice")
	@Step("Login to the Entution")
	public void verifyOutboundPaymentEmployeePaymentVoucherDraftAndNew1_FIN_OP_52() throws Exception {
		outboundPaymentEmployeePaymentVoucherDraftAndNew1_FIN_OP_52();
	}

	@Test(dependsOnMethods = "verifyOutboundPaymentEmployeePaymentVoucherDraftAndNew1_FIN_OP_52")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Release the second Employee Payment Advice")
	public void verifyOutboundPaymentEmployeePaymentVoucherDraftAndNew2_FIN_OP_52() throws Exception {
		outboundPaymentEmployeePaymentVoucherDraftAndNew2_FIN_OP_52();
	}
	
	@Test(dependsOnMethods = "verifyOutboundPaymentEmployeePaymentVoucherDraftAndNew2_FIN_OP_52")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Employee Payment Voucher - Draft & New")
	public void verifyOutboundPaymentEmployeePaymentVoucherDraftAndNew3_FIN_OP_52() throws Exception {
		outboundPaymentEmployeePaymentVoucherDraftAndNew3_FIN_OP_52();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
