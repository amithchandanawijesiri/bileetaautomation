package financeModule;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleSmoke;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})

public class Smoke_Finance_12 extends FinanceModuleSmoke{
TestCommonMethods common = new TestCommonMethods();
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Finance", "", "Verify that user can successfully create Letter of Guarantee", this.getClass().getSimpleName());
	}
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Letter Of Gurentee Form")
	@Step("Login to the Entution, Navigate to the Finance Module")
	public void verifyCreatePurchaseOrderAndNavigateToLetterOfGurentee() throws Exception
	{
		createPurchaseOrderAndNavigateToLetterOfGurentee();
	}
	
	@Test(dependsOnMethods = "verifyCreatePurchaseOrderAndNavigateToLetterOfGurentee")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill Letter Of Gurentee Form")
	public void verifyFillLetterOfGurentee() throws Exception
	{
		fillLetterOfGurentee();
	}
	
	@Test(dependsOnMethods = "verifyFillLetterOfGurentee")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft and Releese Letter Of Gurentee")
	public void verifyDrfatReleseLetterOfGurentee() throws Exception
	{
		drfatReleseLetterOfGurentee();
	}
	
	@Test(dependsOnMethods = "verifyDrfatReleseLetterOfGurentee")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check Credit Extension Availability")
	public void verifyCheckCreditExtentionAvailability() throws Exception
	{
		checkCreditExtentionAvailability();
	}
	
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		common.driverClose();
		 
	}
	
}
