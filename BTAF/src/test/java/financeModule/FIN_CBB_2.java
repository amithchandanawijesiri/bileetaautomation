package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleRegression;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class FIN_CBB_2 extends FinanceModuleRegression {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "Cash Bank Book", "Verify whether if user don't have permission to view Cash/Bank Book by-page, user want able to navigate to by-page",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check Cash/Bank Book with all user permissions")
	@Step("Login to the Entution")
	public void verifyConfigureUserPermissionAndCheckCashBankBook_FIN_CBB_2() throws Exception {
		configureUserPermissionAndCheckCashBankBook_FIN_CBB_2();
	}
	
	@Test(dependsOnMethods = "verifyConfigureUserPermissionAndCheckCashBankBook_FIN_CBB_2")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Reset user permissions")
	@Step("Login to the Entution")
	public void verifyResetCashBankBookUserPermissions_FIN_CBB_2() throws Exception {
		resetCashBankBookUserPermissions_FIN_CBB_2();
	}
	

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
