package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleRegression;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class FIN_BD_26_27 extends FinanceModuleRegression {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "Bank Deposit",
				"Verify that Bank Deposit can be released with all types of cash Inbound Payments and Verify Journel Entry",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = { "financeModule.FIN_BD_22.verifyInboundPayment_FIN_BD_22",
			"financeModule.FIN_BD_23.verifyInboundPayment_FIN_BD_23",
			"financeModule.FIN_BD_24.verifyInboundPayment_FIN_BD_24",
			"financeModule.FIN_BD_25.verifyInboundPayment_FIN_BD_25" })
	@Severity(SeverityLevel.CRITICAL)
	@Description("Bank deposit with all types of cash Inbound Payments")
	public void verifyBankDeposit_FIN_BD_26_27() throws Exception {
		bankDeposit_FIN_BD_26_27();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
