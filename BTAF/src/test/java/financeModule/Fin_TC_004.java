package financeModule;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})

public class Fin_TC_004 extends FinanceModule{
TestCommonMethods common = new TestCommonMethods();
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Finance", "", "Verify that user can successfully create Bank Deposit", this.getClass().getSimpleName());
	}
	
	@Test(dependsOnMethods = "financeModule.Fin_TC_002.verifyCheckoutDraftReleeseCustomerRecipt")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Bank Deposit Form")
	@Step("User login to the Entution, Navigate to the Finance Module")
	public void verifyNavigateToBankDepositForm() throws Exception
	{
		navigateToBankDepositForm();
	}
	
	@Test(dependsOnMethods = "verifyNavigateToBankDepositForm")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill Bank Deposit Deatails")
	public void verifyFillBankDepositDeatils() throws Exception
	{
		fillBankDepositDeatils();
	}
	
	@Test(dependsOnMethods = "verifyFillBankDepositDeatils")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Find relevant Inbound Payment")
	public void verifyFindInboundPaymentForBankDeposit() throws Exception
	{
		findInboundPaymentForBankDeposit();
	}
	
	@Test(dependsOnMethods = "verifyFindInboundPaymentForBankDeposit")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Drfat and Relesse Bank Deposit")
	public void verifyDraftReleeseBankDeposit() throws Exception
	{
		draftreleaseBankDeposit();
	}
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		common.driverClose();
		 
	}
}
