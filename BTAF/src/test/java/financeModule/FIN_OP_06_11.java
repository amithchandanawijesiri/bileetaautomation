package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleRegression4;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class FIN_OP_06_11 extends FinanceModuleRegression4 {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "Outbound Payment",
				"Verify that user can create customer payment voucher using Cash payment method AND Verify the Outbound Payment when Paying currency and filter currency are base currency", this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Release a Customer Refund")
	@Step("Login to the Entution")
	public void verifyOutboundPaymentCustomerPaymentVoucher_1_FIN_OP_06_11() throws Exception {
		outboundPaymentCustomerPaymentVoucher_1_FIN_OP_06_11();
	}
	
	@Test(dependsOnMethods = "verifyOutboundPaymentCustomerPaymentVoucher_1_FIN_OP_06_11")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Release the Customer Payment Voucher")
	public void verifyOutboundPaymentCustomerPaymentVoucher_2_FIN_OP_06_11() throws Exception {
		outboundPaymentCustomerPaymentVoucher_2_FIN_OP_06_11();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
