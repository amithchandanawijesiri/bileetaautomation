package financeModule;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleSmoke;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})

public class Smoke_Finance_01 extends FinanceModuleSmoke{
TestCommonMethods common = new TestCommonMethods();
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Finance", "", "Verify the ability of creating a new bank adjustment", this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Bank Adjustment by-page")
	@Step("Login to the Entution, Navigate to the Finance Module")
	public void verifyNavigateToBankAdjustmentPageSmoke() throws Exception
	{
		navigateToBankAdjustmentPageSmoke();
	}
	
	@Test(dependsOnMethods = "verifyNavigateToBankAdjustmentPageSmoke")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to new Bank Adjustment form")
	public void verifyNavigateToNewBankAdjustmentFormSmoke() throws Exception
	{
		navigateToNewBankAdjustmentFormSmoke();
	}
	
	@Test(dependsOnMethods = "verifyNavigateToNewBankAdjustmentFormSmoke")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill Bank Adjustment form")
	public void verifyFilaBankAdjustmentDetailsSmoke() throws Exception
	{
		filaBankAdjustmentDetailsSmoke();
	}
	
	@Test(dependsOnMethods = "verifyFilaBankAdjustmentDetailsSmoke")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Checkout Bank Adjustment")
	public void verifyCheckoutBankAdjustmentSmoke() throws Exception
	{
		checkoutBankAdjustmentSmoke();
	}
	
	@Test(dependsOnMethods = "verifyFilaBankAdjustmentDetailsSmoke")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft Bank Adjustment")
	public void verifyDraftBankAdjustmentSmoke() throws Exception
	{
		draftBankAdjustmentSmoke();
	}
	
	@Test(dependsOnMethods = "verifyDraftBankAdjustmentSmoke")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Released Bank Adjustment")
	public void verifyReleaseBankAdjustmentSmoke() throws Exception
	{
		releaseBankAdjustmentSmoke();
	}
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		common.driverClose();
		 
	}
}
