package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleRegression;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class FIN_BD_35_36_37 extends FinanceModuleRegression {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "Bank Deposit", "Verify that Deposit Amount can be modified as partial deposits for Cash payments AND Verify that same cash inbound payment can be deposited to 2 different bank accounts partially AND Verify Bank Deposits are included in the doc flow of the Inbound Payment",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Inbound Payment Advice")
	@Step("Login to the Entution")
	public void verifyInboundPaymentAdvice_FIN_BD_35_36_37() throws Exception {
		inboundPaymentAdvice_FIN_BD_35_36_37();
	}

	@Test(dependsOnMethods = "verifyInboundPaymentAdvice_FIN_BD_35_36_37")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Inbound Payment")
	public void verifyInboundPayment_FIN_BD_35_36_37() throws Exception {
		inboundPayment_FIN_BD_35_36_37();
	}

	@Test(dependsOnMethods = "verifyInboundPayment_FIN_BD_35_36_37")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete first Bank Deposit")
	public void verifyBankDeposit1_FIN_BD_35_36_37() throws Exception {
		bankDeposit1_FIN_BD_35_36_37();
	}
	
	@Test(dependsOnMethods = "verifyBankDeposit1_FIN_BD_35_36_37")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete second Bank Deposit")
	public void verifyBankDeposit2_FIN_BD_35_36_37() throws Exception {
		bankDeposit2_FIN_BD_35_36_37();
	}
	
	@Test(dependsOnMethods = "verifyBankDeposit2_FIN_BD_35_36_37")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify DocFlow")
	public void verifyAndCheckDocFlowInboundPayment_FIN_BD_35_36_37() throws Exception {
		checkDocFlowInboundPayment_FIN_BD_35_36_37();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
