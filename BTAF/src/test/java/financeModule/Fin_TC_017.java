package financeModule;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})

public class Fin_TC_017 extends FinanceModule{
TestCommonMethods common = new TestCommonMethods();
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Finance", "", "Verify that user can convert letter of guaratee to bank facility agreement", this.getClass().getSimpleName());
	}
	
	@Test(dependsOnMethods = "financeModule.Fin_TC_016.verifyCheckCreditExtentionAvailability")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Find relevent Letter Of Gurentee")
	@Step("Login to the Entution, Navigate to the Finance Module")
	public void verifyFindReleventLetterOFGurentee() throws Exception
	{
		findReleventLetterOFGurentee();
	}
	
	@Test(dependsOnMethods = "verifyFindReleventLetterOFGurentee")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Convert to Bank Facility Agreement and fill it")
	public void verifyConvertToBankFacilityAgreementAndFillAgreement() throws Exception
	{
		convertToBankFacilityAgreementAndFillAgreement();
	}
	
	
	@Test(dependsOnMethods = "verifyConvertToBankFacilityAgreementAndFillAgreement")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft and Release Bank Facility Agreement")
	public void verifyDraftReleaseBankFacilityAgreement() throws Exception
	{
		draftReleaseBankFacilityAgreement();
	}
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		common.driverClose();
		 
	}
	
}
