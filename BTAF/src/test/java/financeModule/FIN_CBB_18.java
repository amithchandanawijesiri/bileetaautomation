package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleRegression;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class FIN_CBB_18 extends FinanceModuleRegression {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "Cash Bank Book", "Verify whether Inactive Cash/Bank Book can't be viewable in during pay book generation",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"financeModule.FIN_CBB_1_4_5_6_10.verifyFindReleasedCashBook_FIN_CBB_1_4_5_6_10"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check inactive cash accounts with PAy Book Generation")
	@Step("Login to the Entution")
	public void verifyInactiveCashBookAndCheckItWithPayBookGenaration_FIN_CBB_18() throws Exception {
		inactiveCashBookAndCheckItWithPayBookGenaration_FIN_CBB_18();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
