package financeModule;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleSmoke;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})

public class Smoke_Finance_08 extends FinanceModuleSmoke{
	
TestCommonMethods common = new TestCommonMethods();
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Finance", "Finance", "Verify that user can successfully create Inbound Payment  - Customer Receipt Voucher - Convert from Inbound Payment advice", this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Prepare Inbound Payment Advice")
	@Step("User login to the Entution, Navigate to the Finance Module")
	public void verifyFinTC001() throws Exception
	{
		finTC001();
	}
	
	@Test(dependsOnMethods = "verifyFinTC001")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill Inbound Payment - Customer Recipt Voucher")
	public void verifyFillInboundPaymentCustomerReceiptVouche() throws Exception
	{
		fillInboundPaymentCustomerReceiptVouche();
	}
	
	@Test(dependsOnMethods = "verifyFillInboundPaymentCustomerReceiptVouche")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Checkout Draft Releese Customer Recipt")
	public void verifyCheckoutDraftReleeseCustomerRecipt() throws Exception
	{
		checkoutDraftreleaseCustomerRecipt();
	}
	
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		common.driverClose();
		 
	}
}
