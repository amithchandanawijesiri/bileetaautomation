package financeModule;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})

public class Fin_TC_010 extends FinanceModule{
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "",
				"Verify that user can successfully create Outbound Payment Advice - Vendor Advance",
				this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Outbound Payment Advice page")
	@Step("User login to the Entution, Navigate to the Finance Module")
	public void verifyNavigateToOutboundPaymentInvoiceForm() throws Exception {
		navigateToOutboundPaymentInvoiceForm();
	}
	
	@Test(dependsOnMethods = "verifyNavigateToOutboundPaymentInvoiceForm")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill Outbound Payment Advice")
	public void verifyFillOutboundPaymentAdvice() throws Exception {
		fillOutboundPaymentAdvice();
	}
	
	@Test(dependsOnMethods = "verifyFillOutboundPaymentAdvice")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Checkout, Draft and Release Outbound Payment Advice")
	public void verifyCheckoutDarftReleeseOutboundPayment() throws Exception {
		checkoutDarftreleaseOutboundPayment();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
