package financeModule;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})

public class Fin_TC_014 extends FinanceModule{
TestCommonMethods common = new TestCommonMethods();
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Finance", "", "Verify that user can successfully create Outbound Payment Advice - Vendor Credit Memo", this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Vendor Debit Memo")
	@Step("Login to the Entution, Navigate to the Finance Module")
	public void verifyNavigateToVendorCrediMemo() throws Exception
	{
		navigateToVendorCrediMemo();
	}
	
	@Test(dependsOnMethods = "verifyNavigateToVendorCrediMemo")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill Vendor Credit Memo")
	public void verifyFillVendorCreditMemo() throws Exception
	{
		fillVendorCreditMemo();
	}
	
	@Test(dependsOnMethods = "verifyFillVendorCreditMemo")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Checkout, Draft and Release Vendor Credit Memo")
	public void verifyCheckoutDraftAndReleaseVendorCreditMemo() throws Exception
	{
		checkoutDraftAndReleaseVendorCreditMemo();
	}
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		common.driverClose();
		 
	}

}
