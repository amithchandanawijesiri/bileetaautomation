package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class Fin_TC_009 extends FinanceModule {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "", "Verify that user can successfully create Bank Deposit",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = "financeModule.Fin_TC_008.verifyCheckoutDarftReleeseInboundPaymentTC08")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to Bank Deposit Form")
	@Step("Login to the Entution, Navigate to the Finance Module")
	public void verifyNavigateToBankDepositform() throws Exception {
		navigateToBankDepositform09();
	}
	
	@Test(dependsOnMethods = "verifyNavigateToBankDepositform")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Bank Deposit Form")
	public void verifyComplteBankDeposiForm() throws Exception {
		complteBankDeposiForm();
	}
	
	@Test(dependsOnMethods = "verifyComplteBankDeposiForm")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Draft and Releese Bank Deposit Form")
	public void verifyDraftReleeseBAnkDeposit09() throws Exception {
		draftreleaseBAnkDeposit09();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
