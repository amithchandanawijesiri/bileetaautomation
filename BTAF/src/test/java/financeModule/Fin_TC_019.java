package financeModule;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;import org.testng.annotations.Listeners;import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})

public class Fin_TC_019 extends FinanceModule{
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "", "Verify that user can settle bank facility agreement through bank adjustment",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Navigate to New Bank Adjustment form")
	@Step("Login to the Entution, Navigate to the Finance Module")
	public void verifyNavigateToNewBankAdjustment() throws Exception {
		navigateToNewBankAdjustment();
	}
	
	@Test(dependsOnMethods = "verifyNavigateToNewBankAdjustment")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Fill Bank Adjustment")
	public void verifyFillBankAdjutment() throws Exception {
		fillBankAdjutment();
	}
	
	@Test(dependsOnMethods = "verifyFillBankAdjutment")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Checkout,Draft and Release Bank Adjustment")
	public void veifyCheckoutDraftAndReleaseBankAdjustment() throws Exception {
		checkoutDraftAndReleaseBankAdjustment();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}
}
