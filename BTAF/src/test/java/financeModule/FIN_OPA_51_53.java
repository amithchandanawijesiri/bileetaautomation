package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleRegression2;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class FIN_OPA_51_53 extends FinanceModuleRegression2 {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "Outbound Payment Advice",
				"Verify that released Customer refund can be set-of using set-off function in action AND Verify that user can reverse the set-off value", this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Customer Refund - Set Off / Set Off Reverse")
	@Step("Login to the Entution")
	public void verifyOutboundPaymentAdviceCustomerRefundSetOffAndSetOffReverse1_FIN_OPA_51_53() throws Exception {
		outboundPaymentAdviceCustomerRefundSetOffAndSetOffReverse1_FIN_OPA_51_53();
	}
	
	@Test(dependsOnMethods = {"verifyOutboundPaymentAdviceCustomerRefundSetOffAndSetOffReverse1_FIN_OPA_51_53"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Customer Refund - Set Off / Set Off Reverse")
	@Step("Login to the Entution")
	public void verifyOutboundPaymentAdviceCustomerRefundSetOffAndSetOffReverse2_FIN_OPA_51_53() throws Exception {
		outboundPaymentAdviceCustomerRefundSetOffAndSetOffReverse2_FIN_OPA_51_53();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
