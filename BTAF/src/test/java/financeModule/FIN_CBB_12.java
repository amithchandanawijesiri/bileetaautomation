package financeModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.FinanceModuleRegression;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class FIN_CBB_12 extends FinanceModuleRegression {
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Finance", "Cash Bank Book", "Verify whether the system prevents the same account number from being added more than once",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"financeModule.FIN_CBB_1_4_5_6_10.verifyFindReleasedCashBook_FIN_CBB_1_4_5_6_10"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Check Cash/Bank Book with duplicated account number")
	@Step("Login to the Entution")
	public void verifyAndCheckAlreadyUsedAccountNumberAsAccountNumber_FIN_CBB_12() throws Exception {
		checkAlreadyUsedAccountNumberAsAccountNumber_FIN_CBB_12();
	}
	

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
