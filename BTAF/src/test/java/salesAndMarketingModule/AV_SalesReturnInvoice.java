package salesAndMarketingModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.SalesAndMarketingModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class AV_SalesReturnInvoice extends SalesAndMarketingModule  {
	
	TestCommonMethods common = new TestCommonMethods();
	SalesAndMarketingModule sales = new SalesAndMarketingModule();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Sales and Marketing", "",
				"Action verification sales return invoice",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void checkSalesAndMarketing() throws Exception {

		sales.checkNavigateToTheLoginPage();
		sales.verifyTheLogo();
		sales.checkUserLogin();
		sales.checkNavigationMenu();
		sales.checkSalesAndMarketingModule();
		sales.clickOnSalesAndMarketing();

	}
	
	@Test(dependsOnMethods = "checkSalesAndMarketing")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void clickSalesOrder() throws Exception {

		sales.checkClickSalesReturnInvoice();

	}
	
	@Test(dependsOnMethods = "clickSalesOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void SalesReturnInvoiceReminders() throws Exception {

		sales.AVSalesReturnInvoiceReminders();
		
	}
	
	@Test(dependsOnMethods = "SalesReturnInvoiceReminders")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void SalesReturnInvoiceHistory() throws Exception {

		sales.AVSalesReturnInvoiceHistory();
		
	}

//	@Test(dependsOnMethods = "SalesReturnInvoiceHistory")
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("Verify that user can go to sales and marketing page")
//	public void SalesReturnInvoiceDocFlow() throws Exception {
//
//		sales.AVSalesReturnInvoiceDocFlow();
//		
//	}
	
	@Test(dependsOnMethods = "SalesReturnInvoiceHistory")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void SalesReturnInvoiceActivities() throws Exception {

		sales.AVSalesReturnInvoiceActivities();
		
	}
	
	@Test(dependsOnMethods = "SalesReturnInvoiceActivities")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void SalesReturnInvoiceHold() throws Exception {

		sales.AVSalesReturnInvoiceHold();
		
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {

		common.totalTime(startTime);
		common.driverClose();
	}
}
