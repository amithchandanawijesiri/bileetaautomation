package salesAndMarketingModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.SalesAndMarketingModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class AV_AccountGroupConfig extends SalesAndMarketingModule{
	
	TestCommonMethods common = new TestCommonMethods();
	SalesAndMarketingModule sales = new SalesAndMarketingModule();
	
	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Sales and Marketing", "",
				"Verify that user is able to create sales order (Sales order to Sales invoice)",
				this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void checkSalesAndMarketing() throws Exception {

		sales.checkNavigateToTheLoginPage();
		sales.checkUserLogin();
		sales.checkNavigationMenu();
		sales.checkSalesAndMarketingModule();
		sales.clickOnSalesAndMarketing();

	}
	
	@Test(dependsOnMethods = "checkSalesAndMarketing")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void checkDraft() throws Exception {

		sales.accountGroupDraft();
		
	}
	
	@Test(dependsOnMethods = "checkDraft")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void checkEdit() throws Exception {

		sales.accountGroupEditBtn();

	}
	
	@Test(dependsOnMethods = "checkEdit")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void checkUpdate() throws Exception {

		sales.accountGroupUpdate();

	}
	
	@Test(dependsOnMethods = "checkUpdate")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void checkRelease() throws Exception {

		sales.accountGroupRelease();

	}
	
	@Test(dependsOnMethods = "checkRelease")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void checkHistory() throws Exception {

		sales.accountGroupHistory();

	}
	
	@Test(dependsOnMethods = "checkHistory")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void checkActivity() throws Exception {

		sales.accountGroupActivity();

	}
	
//	@Test(dependsOnMethods = "checkSalesAndMarketing")
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("Verify that Sales and Marketing module is available")
//	public void checkDraftNew() throws Exception {
//
//		sales.accountGroupDraftNew();
//
//	}
	
//	@Test(dependsOnMethods = "checkUpdate")
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("Verify that Sales and Marketing module is available")
//	public void checkUpdateAndNew() throws Exception {
//
//		sales.accountGroupUpdateAndNew();
//
//	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {

		common.totalTime(startTime);
		common.driverClose();
	}
	
}
