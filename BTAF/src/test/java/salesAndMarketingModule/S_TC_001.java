package salesAndMarketingModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.AdministrationModule;
import bileeta.BTAF.PageObjects.InventoryAndWarehouseModule;
import bileeta.BTAF.PageObjects.SalesAndMarketingModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class S_TC_001 extends SalesAndMarketingModule {

	TestCommonMethods common = new TestCommonMethods();
	SalesAndMarketingModule sales = new SalesAndMarketingModule();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Sales and Marketing", "",
				"Verify that user is able to create sales order (Sales order to Sales invoice)",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void checkSalesAndMarketing() throws Exception {

		sales.checkNavigateToTheLoginPage();
		sales.verifyTheLogo();
		sales.checkUserLogin();
		InventoryAndWarehouseModule inven = new InventoryAndWarehouseModule();
		inven.menufacturerCommon();
		inven.createCommonProducts();
		sales.checkNavigationMenu();
		sales.checkAdminModule();
		sales.clickOnAdministration();
		sales.configureJourneyOne();
		
		sales.checkNavigationMenu();
		sales.stockAdjustment();

	}

	@Test(dependsOnMethods = "checkSalesAndMarketing")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void clickSalesAndMarketing() throws Exception {

		sales.checkNavigationMenu();
		sales.checkSalesAndMarketingModule();
		sales.clickOnSalesAndMarketing();

	}

	@Test(dependsOnMethods = "clickSalesAndMarketing")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user is able to see the sales order page")
	public void clickSalesOrder() throws Exception {

		sales.checkClickSalesOrderPage();

	}

	@Test(dependsOnMethods = "clickSalesOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales order page")
	public void checkSalesOrderPage() throws Exception {

		sales.checkSalesOrderHeader();

	}

	@Test(dependsOnMethods = "checkSalesOrderPage")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can see the 'Sales Order to Sales Invoice' journey")
	public void checkJourney() throws Exception {

		sales.checkJourneySalesOrderToSalesInvoice();
	}

	@Test(dependsOnMethods = "checkJourney")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can fill the mandatory fields in sales order")
	public void fillFormData() throws Exception {

		sales.fillDetailsSalesOrderToSalesInvoice();
		
	}

	@Test(dependsOnMethods = "fillFormData")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user is able to do the outbound shipment for sales order")
	public void outBoundShipment() throws Exception {

		sales.outboundShipmentSalesOrderToSalesInvoice();
	}

	@Test(dependsOnMethods = "outBoundShipment")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user is able to do the sales invoice")
	public void salesInvoice() throws Exception {

		sales.salesInvoiceSalesOrderToSalesInvoice();
					
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {

		common.totalTime(startTime);
		common.driverClose();
	}
	
}
