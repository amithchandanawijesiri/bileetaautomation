package salesAndMarketingModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.SalesAndMarketingModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class AV_PricingModel extends SalesAndMarketingModule {
	
	TestCommonMethods common = new TestCommonMethods();
	SalesAndMarketingModule sales = new SalesAndMarketingModule();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Sales and Marketing", "", "Verify that user is able to create an Active vehicle information",
				this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void checkSalesAndMarketing() throws Exception {

		sales.checkNavigateToTheLoginPage();
		sales.verifyTheLogo();
		sales.checkUserLogin();
		sales.checkNavigationMenu();
		sales.checkSalesAndMarketingModule();

	}
	
	@Test(dependsOnMethods = "checkSalesAndMarketing")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void clickSalesAndMarketing() throws Exception {
		
		sales.clickOnSalesAndMarketing();
		
	}
	
	@Test(dependsOnMethods = "clickSalesAndMarketing")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to pricing model page")
	public void clickSalesOrder() throws Exception {
		
		sales.checkClickPricingModelPageAV();
		
	}
	
	@Test(dependsOnMethods = "clickSalesOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can fill the mandatory fields")
	public void pricingModelDraft() throws Exception {
		
		sales.AVPricingModelDraft();		
	}
	
	@Test(dependsOnMethods = "pricingModelDraft")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can fill the mandatory fields")
	public void pricingModelEdit() throws Exception {
		
		sales.AVPricingModelEdit();		
	}
	
	@Test(dependsOnMethods = "pricingModelEdit")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can fill the mandatory fields")
	public void pricingModelUpdate() throws Exception {
		
		sales.AVPricingModelUpdate();		
	}
	
	@Test(dependsOnMethods = "pricingModelUpdate")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can fill the mandatory fields")
	public void pricingModelUpdateAndNew() throws Exception {
		
		sales.AVPricingModelUpdateAndNew();		
	}
	
	@Test(dependsOnMethods = "pricingModelUpdateAndNew")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can fill the mandatory fields")
	public void pricingModelRelease() throws Exception {
		
		sales.AVPricingModelRelease();		
	}
	
	@Test(dependsOnMethods = "pricingModelRelease")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can fill the mandatory fields")
	public void pricingModelActivities() throws Exception {
		
		sales.AVPricingModelActivities();		
	}
	
	@Test(dependsOnMethods = "pricingModelActivities")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can fill the mandatory fields")
	public void pricingModelHistory() throws Exception {
		
		//sales.AVPricingModelHistory();		
	}
	
	@Test(dependsOnMethods = "pricingModelActivities")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can fill the mandatory fields")
	public void pricingModelAddNewRule() throws Exception {
		
		sales.AVPricingModelAddNewRule();	
	}
	
	@Test(dependsOnMethods = "pricingModelAddNewRule")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can fill the mandatory fields")
	public void pricingModelDeleteRule() throws Exception {
		
		sales.AVPricingModelDeleteRule();	
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {

		common.totalTime(startTime);
		common.driverClose();
	}
	
}
