package salesAndMarketingModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.SalesAndMarketingModuleSmoke;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class SMOKE_SALES_0002 extends SalesAndMarketingModuleSmoke {

	TestCommonMethods common = new TestCommonMethods();
	SalesAndMarketingModuleSmoke sales = new SalesAndMarketingModuleSmoke();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Sales and Marketing", "",
				"Verify that user is able to create sales order (Sales order to Sales invoice)",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void checkSalesAndMarketing() throws Exception {

		checkNavigateToTheLoginPage();
		// sales.verifyTheLogo();
		checkUserLogin();
		checkNavigationMenu();
		checkSalesAndMarketingModule();
	}

	@Test(dependsOnMethods = "checkSalesAndMarketing")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void clickSalesAndMarketing() throws Exception {

		clickOnSalesAndMarketing();

	}

	@Test(dependsOnMethods = "clickSalesAndMarketing")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user is able to see the sales order page")
	public void clickSalesOrder() throws Exception {

		checkClickSalesOrderPage();

	}

	@Test(dependsOnMethods = "clickSalesOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales order page")
	public void checkSalesOrderPage() throws Exception {

		checkSalesOrderHeader();

	}

	@Test(dependsOnMethods = "checkSalesOrderPage")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can see the Sales Order to Sales Invoice journey")
	public void checkJourney() throws Exception {

		checkJourneySalesOrderToSalesInvoice();
	}

	@Test(dependsOnMethods = "checkJourney")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can fill the mandatory fields")
	public void fillFormData() throws Exception {

		fillDetailsSalesOrderToSalesInvoiceSmoke1();
	}

	@Test(dependsOnMethods = "fillFormData")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user is able to do the outbound shipment for sales order")
	public void outBoundShipment() throws Exception {

		outboundShipmentSalesOrderToSalesInvoiceSmoke1();
	}

	@Test(dependsOnMethods = "outBoundShipment")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user is able to do the sales invoice")
	public void salesInvoice() throws Exception {

		salesInvoiceSalesOrderToSalesInvoiceSmoke1();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {

		common.totalTime(startTime);
		common.driverClose();
	}
}
