package salesAndMarketingModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.SalesAndMarketingModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class AV_SalesUnit extends SalesAndMarketingModule {

	TestCommonMethods common = new TestCommonMethods();
	SalesAndMarketingModule sales = new SalesAndMarketingModule();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Sales and Marketing", "", "Verify that user is able to create an Active vehicle information",
				this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void checkSalesAndMarketing() throws Exception {

		sales.checkNavigateToTheLoginPage();
		sales.verifyTheLogo();
		sales.checkUserLogin();
		sales.checkNavigationMenu();
		sales.checkSalesAndMarketingModule();

	}
	
	@Test(dependsOnMethods = "checkSalesAndMarketing")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void clickSalesAndMarketing() throws Exception {
		
		sales.clickOnSalesAndMarketing();
		
	}
	
	@Test(dependsOnMethods = "clickSalesAndMarketing")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to pricing model page")
	public void clickSalesOrder() throws Exception {
		
		sales.checkClickSalesUnit();
		
	}
	
	@Test(dependsOnMethods = "clickSalesOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can fill the mandatory fields")
	public void salesUnitDraft() throws Exception {
		
		sales.AVSalesUnitDraft();		
	}
	
	@Test(dependsOnMethods = "clickSalesOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can fill the mandatory fields")
	public void salesUnitEdit() throws Exception {
		
		sales.AVSalesUnitEdit();		
	}
	
	@Test(dependsOnMethods = "salesUnitEdit")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can fill the mandatory fields")
	public void salesUnitUpdate() throws Exception {
		
		sales.AVSalesUnitUpdate();		
	}
	
	@Test(dependsOnMethods = "salesUnitUpdate")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can fill the mandatory fields")
	public void salesUnitUpdateAndNew() throws Exception {
		
		sales.AVSalesUnitUpdateAndNew();	
	}
	
	@Test(dependsOnMethods = "salesUnitUpdateAndNew")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can fill the mandatory fields")
	public void salesUnitRelease() throws Exception {
		
		sales.AVSalesUnitRelease();	
	}
	
	@Test(dependsOnMethods = "salesUnitRelease")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can fill the mandatory fields")
	public void salesUnitHistory() throws Exception {
		
		sales.AVSalesUnitHistory();
	}
	
	@Test(dependsOnMethods = "salesUnitRelease")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can fill the mandatory fields")
	public void salesUnitActivities() throws Exception {
		
		sales.AVSalesUnitActivities();	
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {

		common.totalTime(startTime);
		common.driverClose();
	}
}
