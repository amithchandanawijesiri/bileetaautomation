package salesAndMarketingModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.SalesAndMarketingModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})

public class S_TC_010 extends SalesAndMarketingModule{
	
	TestCommonMethods common = new TestCommonMethods();
	SalesAndMarketingModule sales = new SalesAndMarketingModule();
	
	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Sales and Marketing", "",
				"Verify that user is able to create Sales Return Order (Sales Return Order  to Sales Return Invoice)",
				this.getClass().getSimpleName());	
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void checkSalesAndMarketing() throws Exception {
		
		sales.checkNavigateToTheLoginPage();
		sales.verifyTheLogo();
		sales.checkUserLogin();
//		sales.checkNavigationMenu();
//		sales.checkSalesAndMarketingModule();
//		sales.clickOnSalesAndMarketing();
//		sales.checkClickSalesOrderPage();
//		sales.checkSalesOrderHeader();
//		sales.checkJourneySalesOrderToSalesInvoice();
//		sales.fillDetailsSalesOrderToSalesInvoice();
//		sales.outboundShipmentSalesOrderToSalesInvoice();
//		sales.salesInvoiceSalesOrderToSalesInvoice();
			
	}
	
	@Test(dependsOnMethods = "checkSalesAndMarketing")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void clickSalesAndMarketing() throws Exception {
		
		sales.checkNavigationMenu();
		sales.checkSalesAndMarketingModule();
		sales.clickOnSalesAndMarketing();
		
	}
	
	@Test(dependsOnMethods = "clickSalesAndMarketing")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void clickSalesOrder() throws Exception {
		
		sales.checkClickSalesReturnOrderPage() ;
		
	}
	
	@Test(dependsOnMethods = "clickSalesOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can see the Sales Order to Sales Invoice journey")
	public void checkJourney() throws Exception {
		
		sales.checkJourneySalesReturnOrderToSalesReturnInvoice();	
	}
	
	@Test(dependsOnMethods = "checkJourney")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can fill the mandatory fields")
	public void fillFormData() throws Exception {
		
		sales.fillDetailsSalesReturnOrderToSalesReturnInvoice();		
	}
	
//	@AfterClass(alwaysRun = true)
//	public void	AfterClass()throws Exception{
//		
//		common.totalTime(startTime);
//		common.driverClose();
//	}
	
}
