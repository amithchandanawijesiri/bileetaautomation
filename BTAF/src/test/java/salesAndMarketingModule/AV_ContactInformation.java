package salesAndMarketingModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.SalesAndMarketingModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class AV_ContactInformation extends SalesAndMarketingModule {
	
	TestCommonMethods common = new TestCommonMethods();
	SalesAndMarketingModule sales = new SalesAndMarketingModule();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Sales and Marketing", "", "Verify that user is able to create an Active vehicle information",
				this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void checkSalesAndMarketing() throws Exception {

		sales.checkNavigateToTheLoginPage();
		sales.verifyTheLogo();
		sales.checkUserLogin();
		sales.checkNavigationMenu();
		sales.checkSalesAndMarketingModule();

	}

	@Test(dependsOnMethods = "checkSalesAndMarketing")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void clickSalesAndMarketing() throws Exception {

		sales.clickOnSalesAndMarketing();
	}
	
	@Test(dependsOnMethods = "clickSalesAndMarketing")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void contactInformtionEdit() throws Exception {

		sales.AVContactInformationEdit();
	}
	
	@Test(dependsOnMethods = "contactInformtionEdit")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void contactInformtionUpdate() throws Exception {

		sales.AVContactInformationUpdate();
	}
	
	@Test(dependsOnMethods = "contactInformtionUpdate")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void contactInformtionUpdateAndNew() throws Exception {

		sales.AVContactInformationUpdateAndNew();
	}
	
	@Test(dependsOnMethods = "contactInformtionUpdateAndNew")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void contactInformtionHistory() throws Exception {

		sales.AVContactInformationHistory();
	}
	
	@Test(dependsOnMethods = "contactInformtionHistory")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void contactInformtionActivities() throws Exception {

		sales.AVContactInformationActivities();
	}
	
	@Test(dependsOnMethods = "contactInformtionActivities")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void contactInformtionNewInquiry() throws Exception {

		sales.AVContactInformationNewInquiry();
	}
	
	@Test(dependsOnMethods = "contactInformtionNewInquiry")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void contactInformtionStatus() throws Exception {

		sales.AVContactInformationStatus();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {

		common.totalTime(startTime);
		common.driverClose();

	}

}
