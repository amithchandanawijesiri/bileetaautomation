package salesAndMarketingModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.SalesAndMarketingModuleSmoke;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class SMOKE_SALES_0005 extends SalesAndMarketingModuleSmoke {

	TestCommonMethods common = new TestCommonMethods();
	SalesAndMarketingModuleSmoke sales = new SalesAndMarketingModuleSmoke();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Sales and Marketing", "",
				"Verify that user is able to create Sales Return Order (Sales Return Order  to Sales Return Invoice)",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"salesAndMarketingModule.SMOKE_SALES_0001.checkSalesAndMarketing"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void checkSalesAndMarketingD() throws Exception {

		sales.checkNavigateToTheLoginPage();
		sales.verifyTheLogo();
		sales.checkUserLogin();
		sales.checkNavigationMenu();
		sales.checkSalesAndMarketingModule();
		sales.clickOnSalesAndMarketing();
		sales.checkClickSalesOrderPage();
		sales.checkSalesOrderHeader();
		sales.checkJourneySalesOrderToSalesInvoice();
		sales.fillDetailsSalesOrderToSalesInvoiceSmoke1();
		sales.outboundShipmentSalesOrderToSalesInvoiceSmoke1();
		sales.salesInvoiceSalesOrderToSalesInvoiceSmoke1();

	}

	@Test(dependsOnMethods = "checkSalesAndMarketingD")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void clickSalesAndMarketing() throws Exception {

		sales.checkNavigationMenu();
		sales.checkSalesAndMarketingModule();
		sales.clickOnSalesAndMarketing();

	}

	@Test(dependsOnMethods = "clickSalesAndMarketing")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void clickSalesOrder() throws Exception {

		sales.checkClickSalesReturnOrderPage();

	}

	@Test(dependsOnMethods = "clickSalesOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can see the Sales Order to Sales Invoice journey")
	public void checkJourney() throws Exception {

		sales.checkJourneySalesReturnOrderToSalesReturnInvoice();
	}

	@Test(dependsOnMethods = "checkJourney")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can fill the mandatory fields")
	public void fillFormData() throws Exception {

		sales.fillDetailsSalesReturnOrderToSalesReturnInvoiceSmoke();

	}

	@Test(dependsOnMethods = "fillFormData")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user is able to do the sales invoice")
	public void veriyBinCard() throws Exception {

		sales.checkNavigationMenu();
		sales.checkInventoryWarehousing();
		sales.verifyBinCardUpdatingInbound();

	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {

		common.totalTime(startTime);
		common.driverClose();
	}

}
