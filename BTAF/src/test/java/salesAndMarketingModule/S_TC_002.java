package salesAndMarketingModule;

import org.testng.annotations.AfterClass;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.SalesAndMarketingModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;@Listeners({TestListener.class})

public class S_TC_002 extends SalesAndMarketingModule {

	TestCommonMethods common = new TestCommonMethods();
	SalesAndMarketingModule sales = new SalesAndMarketingModule();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Sales and Marketing", "",
				"Verify that user is able to create sales order (Sales order to Sales invoice [Service])",
				this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void checkSalesAndMarketing() throws Exception {

		sales.checkNavigateToTheLoginPage();
		sales.verifyTheLogo();
		sales.checkUserLogin();
		sales.checkNavigationMenu();
		sales.checkSalesAndMarketingModule();

	}

	@Test(dependsOnMethods = "checkSalesAndMarketing")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void clickSalesAndMarketing() throws Exception {

		sales.clickOnSalesAndMarketing();

	}

	@Test(dependsOnMethods = "clickSalesAndMarketing")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user is able to see the sales order")
	public void clickSalesOrder() throws Exception {

		sales.checkClickSalesOrderPage();

	}

	@Test(dependsOnMethods = "clickSalesOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales order page")
	public void checkSalesOrderPage() throws Exception {
		
		sales.checkSalesOrderHeader();
		
	}
	
	@Test(dependsOnMethods = "checkSalesOrderPage")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can see the journey sales order to sales invoice[service]")
	public void checkJourney() throws Exception {

		sales.checkJouneyLoading2();
	}
	 
	
	@Test(dependsOnMethods = "checkJourney")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user is able release the sales order")
	public void fillFormData() throws Exception {

		sales.fillDataSalesOrderSalesInvoiceServ();
	}
	 
	@Test(dependsOnMethods = "fillFormData")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user is able release the sales invoice")
	public void salesInvoiceService() throws Exception {

		sales.SalesInvoiceService();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {

		common.totalTime(startTime);
		common.driverClose();
	}
	 
}
