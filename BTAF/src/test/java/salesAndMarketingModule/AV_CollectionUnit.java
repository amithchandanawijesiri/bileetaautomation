package salesAndMarketingModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.SalesAndMarketingModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class AV_CollectionUnit extends SalesAndMarketingModule{

	TestCommonMethods common = new TestCommonMethods();
	SalesAndMarketingModule sales = new SalesAndMarketingModule();
	
	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Sales and Marketing", "",
				"Verify that user is able to create sales order (Sales order to Sales invoice)",
				this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void checkSalesAndMarketing() throws Exception {

		sales.checkNavigateToTheLoginPage();
		sales.checkUserLogin();
		sales.checkNavigationMenu();
		sales.checkSalesAndMarketingModule();
		sales.clickOnSalesAndMarketing();
		sales.checkClickCollectionUnit();
		sales.checkCollectionUnitPage();

	}
	
//	@Test(dependsOnMethods = "checkSalesAndMarketing")
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("Verify that user can fill the mandatory fields")
//	public void accountInfDraftNew() throws Exception {
//		
//		sales.collectionUnitDraftNew();		
//	}
	
//	@Test(dependsOnMethods = "checkSalesAndMarketing")
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("Verify that Sales and Marketing module is available")
//	public void checkDraft() throws Exception {
//
//		sales.collectionUnitDraft();
//		
//	}
	
//	@Test(dependsOnMethods = "checkSalesAndMarketing")
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("Verify that Sales and Marketing module is available")
//	public void checkEdit() throws Exception {
//
//		sales.checkNavigationMenu();
//		sales.checkSalesAndMarketingModule();
//		sales.clickOnSalesAndMarketing();
//		sales.checkClickCollectionUnit();
//		sales.checkCollectionUnitPage();
//		sales.collectionUnitEdit();
//		
//	}
	
//	@Test(dependsOnMethods = "checkSalesAndMarketing")
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("Verify that Sales and Marketing module is available")
//	public void collectionUniUpdate() throws Exception {
//
//		sales.checkNavigationMenu();
//		sales.checkSalesAndMarketingModule();
//		sales.clickOnSalesAndMarketing();
//		sales.checkClickCollectionUnit();
//		sales.checkCollectionUnitPage();
//		sales.collectionUnitUpdate();
//		
//	}
	
	@Test(dependsOnMethods = "checkSalesAndMarketing")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void collectionUniRelease() throws Exception {

		sales.checkNavigationMenu();
		sales.checkSalesAndMarketingModule();
		sales.clickOnSalesAndMarketing();
		sales.checkClickCollectionUnit();
		sales.checkCollectionUnitPage();
		sales.collectionUnitRelease();
		
	}
	
	@Test(dependsOnMethods = "collectionUniRelease")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void collectionUniHistory() throws Exception {

		sales.collectionUnitHistory();
		
	}
	
	@Test(dependsOnMethods = "collectionUniHistory")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void collectionUniActivity() throws Exception {

		sales.collectionUnitActivities();
		
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {

		common.totalTime(startTime);
		//common.driverClose();
	}
	
}
