package salesAndMarketingModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.SalesAndMarketingModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class AV_LeadInformation  extends SalesAndMarketingModule {

	TestCommonMethods common = new TestCommonMethods();
	SalesAndMarketingModule sales = new SalesAndMarketingModule();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Sales and Marketing", "", "Verify that user is able to create an Active vehicle information",
				this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void checkSalesAndMarketing() throws Exception {

		sales.checkNavigateToTheLoginPage();
		sales.verifyTheLogo();
		sales.checkUserLogin();
		sales.checkNavigationMenu();
		sales.checkSalesAndMarketingModule();

	}

	@Test(dependsOnMethods = "checkSalesAndMarketing")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void clickSalesAndMarketing() throws Exception {

		sales.clickOnSalesAndMarketing();
	}
	
	@Test(dependsOnMethods = "clickSalesAndMarketing")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void clickSalesOrder() throws Exception {
		
		sales.checkClickLeadInformationPage();
		
	}
	
	@Test(dependsOnMethods = "clickSalesOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void leadInformtionDraft() throws Exception {

		sales.AVLeadInformationDraft();
	}

	@Test(dependsOnMethods = "leadInformtionDraft")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void leadInformtionEdit() throws Exception {

		sales.AVLeadInformationEdit();
	}
	
	@Test(dependsOnMethods = "leadInformtionDraft")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void leadInformtionUpdate() throws Exception {

		sales.AVLeadInformationUpdate();
	}
	
	@Test(dependsOnMethods = "leadInformtionDraft")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void leadInformtionUpdateAndNew() throws Exception {

		sales.AVLeadInformationUpdateAndNew();
	}
	
	@Test(dependsOnMethods = "leadInformtionUpdateAndNew")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void leadInformtionRelease() throws Exception {

		sales.AVLeadInformationRelease();
	}
	
	@Test(dependsOnMethods = "leadInformtionRelease")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void leadInformtionHistory() throws Exception {

		sales.AVLeadInformationHistory();
	}
	
	@Test(dependsOnMethods = "leadInformtionHistory")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void leadInformtionActivities() throws Exception {

		sales.AVLeadInformationActivities();
	}
	
	@Test(dependsOnMethods = "leadInformtionActivities")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void leadInformtionDuplicate() throws Exception {

		sales.AVLeadInformationDuplicate();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {

		common.totalTime(startTime);
		common.driverClose();

	}	
}
