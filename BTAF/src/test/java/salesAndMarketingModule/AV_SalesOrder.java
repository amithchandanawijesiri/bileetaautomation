package salesAndMarketingModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.SalesAndMarketingModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class AV_SalesOrder extends SalesAndMarketingModule {

	TestCommonMethods common = new TestCommonMethods();
	SalesAndMarketingModule sales = new SalesAndMarketingModule();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Sales and Marketing", "",
				"Verify that user is able to create sales order (Sales order to Sales invoice)",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void checkSalesAndMarketing() throws Exception {

		sales.checkNavigateToTheLoginPage();
		sales.verifyTheLogo();
		sales.checkUserLogin();
//		sales.checkNavigationMenu();
//		sales.checkSalesAndMarketingModule();
//		sales.clickOnSalesAndMarketing();
//		sales.checkClickSalesOrderPage();
//		sales.checkSalesOrderHeader();
//		sales.checkJourneySalesOrderToSalesInvoice();
//		sales.salesOrderActionVerification();

	}
	
//	@Test(dependsOnMethods = "checkSalesAndMarketing")
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("Verify that Sales and Marketing module is available")
//	public void salesOrderDraftNew() throws Exception {
//
//		sales.checkNavigationMenu();
//		sales.checkSalesAndMarketingModule();
//		sales.clickOnSalesAndMarketing();
//		sales.checkClickSalesOrderPage();
//		sales.checkSalesOrderHeader();
//		sales.checkJourneySalesOrderToSalesInvoice();
//		sales.salesOrderActionVerificationDraftNew();
//
//	}
	
//	@Test(dependsOnMethods = "checkSalesAndMarketing")
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("Verify that Sales and Marketing module is available")
//	public void salesOrderRelease() throws Exception {
//
//		sales.checkNavigationMenu();
//		sales.checkSalesAndMarketingModule();
//		sales.clickOnSalesAndMarketing();
//		sales.checkClickSalesOrderPage();
//		sales.checkSalesOrderHeader();
//		sales.checkJourneySalesOrderToSalesInvoice();
//		sales.salesOrderActionVerificationRelease();
//		
//	}
	
//	@Test(dependsOnMethods = "checkSalesAndMarketing")
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("Verify that Sales and Marketing module is available")
//	public void salesOrderEdit() throws Exception {
//
//		sales.checkNavigationMenu();
//		sales.checkSalesAndMarketingModule();
//		sales.clickOnSalesAndMarketing();
//		sales.checkClickSalesOrderPage();
//		sales.checkSalesOrderHeader();
//		sales.checkJourneySalesOrderToSalesInvoice();
//		sales.salesOrderActionVerificationEdit();
//		
//	}
	
//	@Test(dependsOnMethods = "checkSalesAndMarketing")
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("Verify that Sales and Marketing module is available")
//	public void salesOrderUpdate() throws Exception {
//
//		sales.checkNavigationMenu();
//		sales.checkSalesAndMarketingModule();
//		sales.clickOnSalesAndMarketing();
//		sales.checkClickSalesOrderPage();
//		sales.checkSalesOrderHeader();
//		sales.checkJourneySalesOrderToSalesInvoice();
//		sales.salesOrderActionVerificationUpdate();
//		
//	}
	
//	@Test(dependsOnMethods = "checkSalesAndMarketing")
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("Verify that Sales and Marketing module is available")
//	public void salesOrderUpdateAndNew() throws Exception {
//
//		sales.checkNavigationMenu();
//		sales.checkSalesAndMarketingModule();
//		sales.clickOnSalesAndMarketing();
//		sales.checkClickSalesOrderPage();
//		sales.checkSalesOrderHeader();
//		sales.checkJourneySalesOrderToSalesInvoice();
//		sales.salesOrderActionVerificationUpdateAndNew();
//		
//	}
	
	@Test(dependsOnMethods = "checkSalesAndMarketing")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void salesOrderPurchaseOrder() throws Exception {

		sales.checkNavigationMenu();
		sales.checkSalesAndMarketingModule();
		sales.clickOnSalesAndMarketing();
		sales.checkClickSalesOrderPage();
		sales.checkSalesOrderHeader();
		sales.checkJourneySalesOrderToSalesInvoice();
		sales.salesOrderActionVerificationPurchaseOrder();
		
	}
	
	@Test(dependsOnMethods = "salesOrderPurchaseOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void salesOrderReminder() throws Exception {

		sales.salesOrderActionVerificationReminder();
		
	}
	
	@Test(dependsOnMethods = "salesOrderReminder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void salesOrderHistory() throws Exception {

		sales.salesOrderActionVerificationHistory();
		
	}
	
	@Test(dependsOnMethods = "salesOrderHistory")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void salesOrderDocFlow() throws Exception {

		sales.salesOrderActionVerificationDocFlow();
		
	}
	
	@Test(dependsOnMethods = "salesOrderDocFlow")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void salesOrderInboundCustomerMemo() throws Exception {

		sales.salesOrderActionVerificationInboundCustomerMemo();
		
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {

		common.totalTime(startTime);
		//common.driverClose();
	}

}
