package salesAndMarketingModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.SalesAndMarketingModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class AV_SalesQuotation extends SalesAndMarketingModule {
	
	TestCommonMethods common = new TestCommonMethods();
	SalesAndMarketingModule sales = new SalesAndMarketingModule();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Sales and Marketing", "",
				"Verify that user is able to create sales Quotation with a lead(Sales Quotation to Sales Invoice)",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void checkSalesAndMarketing() throws Exception {

		sales.checkNavigateToTheLoginPage();
		sales.verifyTheLogo();
		sales.checkUserLogin();
		sales.checkNavigationMenu();
		sales.checkSalesAndMarketingModule();

	}

	@Test(dependsOnMethods = "checkSalesAndMarketing")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void clickSalesAndMarketing() throws Exception {

		sales.clickOnSalesAndMarketing();

	}

	@Test(dependsOnMethods = "clickSalesAndMarketing")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void clickSalesOrder() throws Exception {

		sales.checkClickSalesQuatationPage();

	}

	@Test(dependsOnMethods = "clickSalesOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can see the Sales Order to Sales Invoice journey")
	public void checkJourney() throws Exception {

		sales.checkJourneySalesQuatationToSalesInvoice();
	}

	@Test(dependsOnMethods = "checkJourney")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can fill the mandatory fields")
	public void salesQuotationDraft() throws Exception {

		sales.AVSalesQuotationDraft();
	}
	
	@Test(dependsOnMethods = "salesQuotationDraft")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can fill the mandatory fields")
	public void salesQuotationEdit() throws Exception {

		sales.AVSalesQuotationEdit();
	}
	
	@Test(dependsOnMethods = "salesQuotationEdit")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can fill the mandatory fields")
	public void salesQuotationUpdate() throws Exception {

		sales.AVSalesQuotationUpdate();
	}
	
	@Test(dependsOnMethods = "salesQuotationUpdate")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can fill the mandatory fields")
	public void salesQuotationRelease() throws Exception {

		sales.AVSalesQuotationRelease();
	}
	
	@Test(dependsOnMethods = "salesQuotationRelease")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can fill the mandatory fields")
	public void salesQuotationReminder() throws Exception {

		sales.AVSalesQuotationReminder();
	}
	
	@Test(dependsOnMethods = "salesQuotationReminder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can fill the mandatory fields")
	public void salesQuotationHistory() throws Exception {

		sales.AVSalesQuotationHistory();
	}
	
	@Test(dependsOnMethods = "salesQuotationHistory")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can fill the mandatory fields")
	public void salesQuotationDocFlow() throws Exception {

		sales.AVSalesQuotationDocFlow();
	}

//	@Test(dependsOnMethods = "salesQuotationDocFlow")
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("Verify that user can fill the mandatory fields")
//	public void salesQuotationConvertAccount() throws Exception {
//
//		sales.AVSalesQuotationConvertToAccount();
//	}
	
	@Test(dependsOnMethods = "salesQuotationDocFlow")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can fill the mandatory fields")
	public void salesQuotationCapacityPlanning() throws Exception {

		sales.AVSalesQuotationCapacityPlanning();
	}
	
	@Test(dependsOnMethods = "salesQuotationCapacityPlanning")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can fill the mandatory fields")
	public void salesQuotationConfirm() throws Exception {

		sales.checkNavigationMenu();
		sales.checkSalesAndMarketingModule();
		sales.clickOnSalesAndMarketing();
		sales.checkClickSalesQuatationPage();
		sales.checkJourneySalesQuatationToSalesInvoice();
		sales.AVSalesQuotationConfirm();
	}
	
	@Test(dependsOnMethods = "salesQuotationConfirm")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can fill the mandatory fields")
	public void salesQuotationDuplicate() throws Exception {

		sales.AVSalesQuotationDuplicate();
	}
	
	@Test(dependsOnMethods = "salesQuotationDuplicate")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can fill the mandatory fields")
	public void salesQuotationDraftNew() throws Exception {

		sales.AVSalesQuotationDraftNew();
	}
	
	@Test(dependsOnMethods = "salesQuotationDuplicate")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can fill the mandatory fields")
	public void salesQuotationNewVersion() throws Exception {

		sales.AVSalesQuotationNewVersion();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {

		common.totalTime(startTime);
		common.driverClose();
	}
	
}
