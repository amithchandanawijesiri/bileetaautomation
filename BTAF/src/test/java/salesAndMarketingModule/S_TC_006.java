package salesAndMarketingModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.AdministrationModule;
import bileeta.BTAF.PageObjects.SalesAndMarketingModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class S_TC_006 extends SalesAndMarketingModule {


	TestCommonMethods common = new TestCommonMethods();
	SalesAndMarketingModule sales = new SalesAndMarketingModule();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Sales and Marketing", "", "Verify that user is able to create sales Invoice (Service)",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void checkSalesAndMarketing() throws Exception {

		sales.checkNavigateToTheLoginPage();
		sales.verifyTheLogo();
		sales.checkUserLogin();
		sales.checkNavigationMenu();
		sales.checkSalesAndMarketingModule();

	}

	@Test(dependsOnMethods = "checkSalesAndMarketing")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void clickSalesAndMarketing() throws Exception {

		sales.clickOnSalesAndMarketing();

	}

	@Test(dependsOnMethods = "clickSalesAndMarketing")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void clickSalesInvoice() throws Exception {

		sales.checkClickSalesInvoiceService6();

	}

	@Test(dependsOnMethods = "clickSalesInvoice")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void checkJourneyInvoice() throws Exception {

		sales.checkJouneyLoading6();

	}

	@Test(dependsOnMethods = "checkJourneyInvoice")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can fill the mandatory fields")
	public void fillFormData() throws Exception {

		sales.fillDetailsSalesInvoiceSer6();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {

		common.totalTime(startTime);
		common.driverClose();
	}

}
