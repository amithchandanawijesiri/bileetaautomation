package salesAndMarketingModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.SalesAndMarketingModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class AV_AccountInformation extends SalesAndMarketingModule {

	TestCommonMethods common = new TestCommonMethods();
	SalesAndMarketingModule sales = new SalesAndMarketingModule();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Sales and Marketing", "",
				"Verify that user is able to create sales order (Sales order to Sales invoice)",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void checkSalesAndMarketing() throws Exception {

		sales.checkNavigateToTheLoginPage();
		sales.checkUserLogin();
		sales.checkNavigationMenu();
		sales.checkSalesAndMarketingModule();
		sales.clickOnSalesAndMarketing();

	}

	@Test(dependsOnMethods = "checkSalesAndMarketing")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to Account Information page")
	public void clickSalesOrder() throws Exception {

		sales.checkClickAccountInformation();
		sales.checkAccountInformationPage();

	}

	@Test(dependsOnMethods = "clickSalesOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void accountInformationDraft() throws Exception {

		sales.AVaccountInformationDraft();

	}

	@Test(dependsOnMethods = "accountInformationDraft")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void accountInformationEdit() throws Exception {

		sales.AVaccountInformationEdit();

	}

	@Test(dependsOnMethods = "accountInformationEdit")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void accountInformationUpdate() throws Exception {

		sales.AVAccountInformationUpdate();
	}

	@Test(dependsOnMethods = "accountInformationUpdate")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can fill the mandatory fields")
	public void accountInfUpdateNew() throws Exception {

		sales.AVaccountInformationUpdateNew();
	}

	@Test(dependsOnMethods = "accountInfUpdateNew")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void accountInfRelease() throws Exception {

		sales.AVaccountInfRelease();

	}

	@Test(dependsOnMethods = "accountInfRelease")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void accountInfHistory() throws Exception {

		sales.AVaccountInfHistory();

	}

	@Test(dependsOnMethods = "accountInfHistory")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void checkActivity() throws Exception {

		//sales.accountGroupActivity();

	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {

		common.totalTime(startTime);
		common.driverClose();
	}

}
