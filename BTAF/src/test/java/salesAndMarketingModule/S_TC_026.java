package salesAndMarketingModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.SalesAndMarketingModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class S_TC_026 extends SalesAndMarketingModule {

	TestCommonMethods common = new TestCommonMethods();
	SalesAndMarketingModule sales = new SalesAndMarketingModule();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Sales and Marketing", "", "Verify that user is able to create a delivery plan",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Inventory & Warehousing module is available")
	public void checkInventoryWarehousing() throws Exception {

		sales.checkNavigateToTheLoginPage();
		sales.checkUserLogin();
		//sales.checkNavigationMenu();
		//sales.checkSalesAndMarketingModule();
		//sales.clickOnSalesAndMarketing();
		//sales.checkClickSalesOrderPage();
		//sales.checkSalesOrderHeader();
		//sales.checkJourneySalesOrderToSalesInvoice();
		//sales.fillDetailsSalesOrderToSalesInvoice();
		//sales.outboundShipmentSalesOrderToSalesInvoice();
		//sales.salesInvoiceSalesOrderToSalesInvoice();
	}

	@Test(dependsOnMethods = "checkInventoryWarehousing")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to delivery plan page")
	public void clickInventory() throws Exception {

		sales.checkNavigationMenu();
		sales.checkInventoryWarehousing();
		sales.checkClickDeliveryPlan();
	}

	@Test(dependsOnMethods = "clickInventory")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user is able to create a delivery plan")
	public void clickDelivery() throws Exception {

		sales.createDeliveryPlan();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {

		common.totalTime(startTime);
		common.driverClose();
	}

}
