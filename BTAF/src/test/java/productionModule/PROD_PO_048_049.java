package productionModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProductionModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class PROD_PO_048_049 extends ProductionModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Production", "Production Order",
				"Verify whether system is loading a new production order in another tab when user clicks on (+) in a drafted production order AND Verify whether system is loading a new production order in another tab when user clicks on (+) in front of a released production order",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Order")
	public void verifyProductionOrder_1_PROD_PO_048_049() throws Exception {
		productionOrder_1_PROD_PO_048_049();
	}

	@Test(dependsOnMethods = "verifyProductionOrder_1_PROD_PO_048_049")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Order")
	public void verifyProductionOrder_2_PROD_PO_048_049() throws Exception {
		productionOrder_2_PROD_PO_048_049();
	}

	@Test(dependsOnMethods = "verifyProductionOrder_2_PROD_PO_048_049")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Order")
	public void verifyProductionOrder_3_PROD_PO_048_049() throws Exception {
		productionOrder_3_PROD_PO_048_049();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
