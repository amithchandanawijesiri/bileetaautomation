package productionModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProductionModule;
import bileeta.BTAF.PageObjects.ProductionModuleSmoke;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class Smoke_Production_0001_2_L extends ProductionModuleSmoke{

TestCommonMethods common = new TestCommonMethods();
	
	
	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Production", "Smoke", "Bill Of Operation", this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Release Bill Of Operation")
	public void verifyBillOfOperation_Smoke_Production_0001_2() throws Exception {
		billOfOperation_Smoke_Production_0001_2();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
