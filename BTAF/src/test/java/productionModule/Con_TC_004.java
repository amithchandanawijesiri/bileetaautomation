package productionModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProductionModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({TestListener.class})

public class Con_TC_004 extends ProductionModule{
	
	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Production", "", "Verify that user can create a group level BOM", this.getClass().getSimpleName());
	}
	
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Click on navigation menu")
	public void navigateSideBar() throws Exception {
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
	    navigateToSideBar();
	}
	
	@Test(dependsOnMethods ="navigateSideBar")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Inventory should be clicked successfully")
	
	public void InventoryBtnGroup() throws Exception {
		
		navigateToInventoryGroup();
	}
	
	@Test(dependsOnMethods ="InventoryBtnGroup")
	@Severity(SeverityLevel.CRITICAL)
	@Description("The Bill of materials by should be loaded successfully")
	
	public void BillOfMaterialNavigateGroup() throws Exception {
		
		navigateToBillOfMaterialGroup();
	}
	
	@Test(dependsOnMethods ="BillOfMaterialNavigateGroup")
	@Severity(SeverityLevel.CRITICAL)
	@Description("The New bill of material page should be loaded successfully")
	
	public void BillOfMaterialNewGroup() throws Exception {
		
		navigateTonewBOMGroup();
	}
	

	@Test(dependsOnMethods ="BillOfMaterialNewGroup")
	@Severity(SeverityLevel.CRITICAL)
	@Description("The product group should be selected from the dropdown under the product details")
	
	public void BillOfMaterialFillGroup() throws Exception {
		
		fllingBOMFormGroup();
	}
	
	@Test(dependsOnMethods ="BillOfMaterialFillGroup")
	@Severity(SeverityLevel.CRITICAL)
	@Description("The bill of materials should be released successfully")
	
	public void BillOfMaterialCompleteGroup() throws Exception {
		
		completeNreleaseBOMGroup();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}
}
