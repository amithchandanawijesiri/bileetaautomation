package productionModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProductionModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class PROD_PO_052 extends ProductionModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Production", "Production Order",
				"Verify whether user can draft a production order and navigate to a new production order form using ‘Draft & New’ function",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Order")
	public void verifyProductionOrder_1_PROD_PO_052() throws Exception {
		productionOrder_1_PROD_PO_052();
	}

	@Test(dependsOnMethods = "verifyProductionOrder_1_PROD_PO_052")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Order")
	public void verifyProductionOrder_2_PROD_PO_052() throws Exception {
		productionOrder_2_PROD_PO_052();
	}

	@Test(dependsOnMethods = "verifyProductionOrder_2_PROD_PO_052")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Order")
	public void verifyProductionOrder_3_PROD_PO_052() throws Exception {
		productionOrder_3_PROD_PO_052();
	}
	
	@Test(dependsOnMethods = "verifyProductionOrder_3_PROD_PO_052")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Order")
	public void verifyProductionOrder_4_PROD_PO_052() throws Exception {
		productionOrder_4_PROD_PO_052();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
