package productionModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProductionModule;
import bileeta.BTAF.PageObjects.ProductionModuleSmoke;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class Smoke_Production_0001_3_L extends ProductionModuleSmoke{

TestCommonMethods common = new TestCommonMethods();
	
	
	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Production", "Smoke", "Production Model", this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Release Production Model")
	public void verifyProductionModel_Smoke_Production_0001_3_1() throws Exception {
		productionModel_Smoke_Production_0001_3_1();
	}
	
	@Test(dependsOnMethods = "verifyProductionModel_Smoke_Production_0001_3_1")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Release Production Model")
	public void verifyProductionModel_Smoke_Production_0001_3_2() throws Exception {
		productionModel_Smoke_Production_0001_3_2();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
