package productionModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProductionModule;
import bileeta.BTAF.PageObjects.ProductionModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class PROD_PO_018_031_032 extends ProductionModuleRegression {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Production", "Production Order",
				"Verify that system is enabled the production order type respect to the selected production model (For Infite type production model)",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create Infinite Enbled Production Model")
	public void verifyProuctionModelInfiniteEnabledRegression() throws Exception {
		prouctionModelInfiniteEnabledRegression();
	}
	
	@Test(dependsOnMethods = "verifyProuctionModelInfiniteEnabledRegression")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Order")
	public void verifyProductionOrder_1_PROD_PO_018() throws Exception {
		productionOrder_1_PROD_PO_018();
	}
	
	@Test(dependsOnMethods = "verifyProductionOrder_1_PROD_PO_018")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Order")
	public void verifyProductionOrder_2_PROD_PO_018_031_032() throws Exception {
		productionOrder_2_PROD_PO_018_031_032();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
