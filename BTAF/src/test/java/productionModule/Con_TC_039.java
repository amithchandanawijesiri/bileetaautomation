package productionModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProductionModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class Con_TC_039 extends ProductionModule{

TestCommonMethods common = new TestCommonMethods();
	
	
	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Production", "", "Verify that user can generate prodction internal receipt successfully", this.getClass().getSimpleName());
	}
	
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Click on navigation menu")
	public void navigateSideBar() throws Exception {
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
	    navigateToSideBar();
	    navigateToProduction();
	    productionLabelPrint();
	    productionCosting();
	}
	
//	@Test(dependsOnMethods ="navigateSideBar")
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("Production module should be clicked successfully")
//	
//	public void ProductionBtn() throws Exception {
//		
//		navigateToProduction();
//	}
//
//	@Test(dependsOnMethods ="ProductionBtn")
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("QC Completed successfully")
//	
//	public void productionLabel() throws Exception {
//		
//		productionLabelPrint();
//	}
//	
//	@Test(dependsOnMethods ="productionLabel")
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("production costing successfully")
//	
//	public void productionCost() throws Exception {
//		
//		productionCosting();
//	}

	@Test(dependsOnMethods ="navigateSideBar")
	@Severity(SeverityLevel.CRITICAL)
	@Description("prodction internal receipt successfully")
	
	public void productionOrderInternalReceipt() throws Exception {
		
		internalReceipt();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}
}
