package productionModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProductionModule;
import bileeta.BTAF.PageObjects.ProductionModuleSmoke;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class Smoke_Production_0002 extends ProductionModuleSmoke{

TestCommonMethods common = new TestCommonMethods();
	
	
	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Production", "", "Production Smoke Test Case", this.getClass().getSimpleName());
	}
	
	@Test(dependsOnMethods = "productionModule.Smoke_Production_0001.allinone") 
	@Severity(SeverityLevel.CRITICAL)
	@Description("All in one")
	public void allone() throws Exception {
		AllInOne();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
