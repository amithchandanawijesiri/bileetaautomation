package productionModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProductionModule;
import bileeta.BTAF.PageObjects.ProductionModuleRegression2;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;

public class PROD_PC_MERGE_001 extends ProductionModuleRegression2 {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Production", "Production Control",
				"Production Control",
				this.getClass().getSimpleName());
	}

	@Test(priority = 1)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Control")
	@Step("Login to the system")
	public void verifyProductionOrderForProductionControl() throws Exception {
		productionOrderForProductionControl();
	}
	
	@Test(priority = 2)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Control")
	@Step("Login to the system")
	public void verifyProductionControl_PROD_PC_003_005_006_007_008_009_011_012() throws Exception {
		productionControl_PROD_PC_003_005_006_007_008_009_011_012();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
