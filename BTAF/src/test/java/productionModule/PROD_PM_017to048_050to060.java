package productionModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProductionModule;
import bileeta.BTAF.PageObjects.ProductionModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;

import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({TestListener.class})

public class PROD_PM_017to048_050to060 extends ProductionModuleRegression{

	TestCommonMethods common =new TestCommonMethods();
	
	
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Production", "Production Model", "Complete the Production Model", this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Step("Navigate to a New Production Model")
	@Description("Complete Production Model Form")
	public void verifyProductionModelFormInspection1_PROD_PM_017to048_050to060() throws Exception {
		productionModelFormInspection1_PROD_PM_017to048_050to060();
	}
	
	@Test(dependsOnMethods = "verifyProductionModelFormInspection1_PROD_PM_017to048_050to060")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Production Model Form")
	public void verifyProductionModelFormInspection2_PROD_PM_017to048_050to060() throws Exception {
		productionModelFormInspection2_PROD_PM_017to048_050to060();
	}
	
	@Test(dependsOnMethods = "verifyProductionModelFormInspection2_PROD_PM_017to048_050to060")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Production Model Form")
	public void verifyProductionModelFormInspection3_PROD_PM_017to48_050to060() throws Exception {
		productionModelFormInspection3_PROD_PM_017to048_050to060();
	}
	
	@Test(dependsOnMethods = "verifyProductionModelFormInspection3_PROD_PM_017to48_050to060")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Production Model")
	public void verifyProductionModelFormInspection4_PROD_PM_017to048_050to060() throws Exception {
		productionModelFormInspection4_PROD_PM_017to048_050to060();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}
}
