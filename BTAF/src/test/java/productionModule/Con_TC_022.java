package productionModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProductionModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({TestListener.class})

public class Con_TC_022 extends ProductionModule{

TestCommonMethods common = new TestCommonMethods();
	
	
	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Production", "", "Verify whether user added BOO details are displaying on the Bill of operations tab", this.getClass().getSimpleName());
	}
	
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Click on navigation menu")
	public void navigateSideBar() throws Exception {
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
	    navigateToSideBar();
	    navigateToProduction();
	    navigateToProductionModel();
	    ProductionModelForm();
	    ProductionModelFormOnlyMRPEnable();
	    BOODetails();
	}
	
//	@Test(dependsOnMethods ="navigateSideBar")
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("Production module should be clicked successfully")
//	
//	public void ProductionBtn() throws Exception {
//		
//		navigateToProduction();
//	}
//
//	@Test(dependsOnMethods ="ProductionBtn")
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("Production model should be clicked successfully")
//	
//	public void ProductionModel() throws Exception {
//		
//		navigateToProductionModel();
//	}
//	
//	@Test(dependsOnMethods ="ProductionModel")
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("Production Model Form Filled successfully")
//	
//	public void newProductionModelFormMRP() throws Exception {
//		
//		ProductionModelFormOnlyMRPEnable();
//	}
//	
//	@Test(dependsOnMethods ="newProductionModelFormMRP")
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("Bill Of Operation clicked successfully")
//	
//	public void BillOfOperation() throws Exception {
//		
//		BOODetails();
//	}
	
	@Test(dependsOnMethods ="navigateSideBar")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Bill Of Operation tab viewed successfully")
	
	public void BillOfOperationTab() throws Exception {
		
		BOODetailsTabSupply();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}


}
