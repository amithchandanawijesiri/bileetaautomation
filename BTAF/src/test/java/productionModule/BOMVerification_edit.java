package productionModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProductionModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class BOMVerification_edit extends ProductionModule{

TestCommonMethods common = new TestCommonMethods();
	
	
	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Production", "", "edit,update,duplicate,draft&new", this.getClass().getSimpleName());
	}
	
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Click on navigation menu")
	public void navigateSideBar() throws Exception {
		navigateToTheLogin();
		verifyThelogo();
		userlogin();
		navigateTosideBar();
		navigateToProductionmenu();
		navigatetoInventory();
		
	}
	
	
	@Test(dependsOnMethods ="navigateSideBar")
	@Severity(SeverityLevel.CRITICAL)
	@Description("The Bill of materials by should be loaded successfully")
	public void bomEdit()throws Exception {
		navigatetoBillOfMaterial();
		navigatetonewBOM();
		fillingBOMform();
		editNUpdateBOM();
	}
	
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}


}
