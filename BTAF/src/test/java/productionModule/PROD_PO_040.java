package productionModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProductionModule;
import bileeta.BTAF.PageObjects.ProductionModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class PROD_PO_040 extends ProductionModuleRegression {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Production", "Production Order",
				"Verify whether the user is able to Update the Production order before release the production order",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Order")
	public void verifyProductionOrder_1_PROD_PO_040() throws Exception {
		productionOrder_1_PROD_PO_040();
	}

	@Test(dependsOnMethods = "verifyProductionOrder_1_PROD_PO_040")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Order")
	public void verifyProductionOrder_2_PROD_PO_040() throws Exception {
		productionOrder_2_PROD_PO_040();
	}

	@Test(dependsOnMethods = "verifyProductionOrder_2_PROD_PO_040")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Order - Edit / Update")
	public void verifyProductionOrder_3_PROD_PO_040() throws Exception {
		productionOrder_3_PROD_PO_040();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
