package productionModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProductionModule;
import bileeta.BTAF.PageObjects.ProductionModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;

import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class PROD_PM_077 extends ProductionModuleRegression {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Production", "Production Model",
				"Verify whether user can ‘Edit’ production model using ‘Edit’ function when document status is on ‘Draft’ state",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Step("Login to the System")
	@Description("Production Model")
	public void verifyProductionModelInfiniteEnabled_1_PROD_PM_077() throws Exception {
		productionModelInfiniteEnabled_1_PROD_PM_077();
	}

	@Test(dependsOnMethods = "verifyProductionModelInfiniteEnabled_1_PROD_PM_077")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Model")
	public void verifyProductionModelInfiniteEnabled_2_PROD_PM_077() throws Exception {
		productionModelInfiniteEnabled_2_PROD_PM_077();
	}

	@Test(dependsOnMethods = "verifyProductionModelInfiniteEnabled_2_PROD_PM_077")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Model")
	public void verifyProductionModelInfiniteEnabled_3_PROD_PM_077() throws Exception {
		productionModelInfiniteEnabled_3_PROD_PM_077();
	}

	@Test(dependsOnMethods = "verifyProductionModelInfiniteEnabled_3_PROD_PM_077")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Edit Production Model in drafted status")
	public void verifyProductionModelInfiniteEnabled_4_PROD_PM_077() throws Exception {
		productionModelInfiniteEnabled_4_PROD_PM_077();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}
}
