package productionModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProductionModule;
import bileeta.BTAF.PageObjects.ProductionModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({TestListener.class})

public class PROD_PM_004_006to0016 extends ProductionModuleRegression{

	TestCommonMethods common =new TestCommonMethods();
	
	
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Production", "Production Model", "Verify by-page", this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Login to the system by entering the authentication and check for Production Module")
	public void verifyByPageInspection_PROD_PM_004_006to0016() throws Exception {
		byPageInspection_PROD_PM_004_006to0016();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}
}
