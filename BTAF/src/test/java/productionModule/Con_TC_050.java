package productionModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProductionModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class Con_TC_050 extends ProductionModule{

TestCommonMethods common = new TestCommonMethods();
	
	
	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Production", "", "Verify that user can successfully delete a existing pricing profile", this.getClass().getSimpleName());
	}
	
	
	@Test//(priority = 1)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Click on navigation menu")
	public void navigateSideBar() throws Exception {
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
	    navigateToSideBar();
	}
	
	@Test(dependsOnMethods ="navigateSideBar")//,priority = 2)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production module should be clicked successfully")
	
	public void ProductionBtn() throws Exception {
		
		navigateToProduction();
	}
	
	
	@Test(dependsOnMethods ="ProductionBtn")//,priority = 2)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production module should be clicked successfully")
	
	public void createNewPricingProfile() throws Exception {
		
		newPricingProfile();
	}
	
	@Test(dependsOnMethods ="createNewPricingProfile")//,priority = 2)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production module should be clicked successfully")
	
	public void estimateNewPricingProfile() throws Exception {
		
		estimatePricingProfile();
	}
	
	@Test(dependsOnMethods ="estimateNewPricingProfile")//,priority = 2)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production module should be clicked successfully")
	
	public void actualCalculationNewPricingProfile() throws Exception {
		
		actualCalculationPricingProfile();
	}
	

	@Test(dependsOnMethods ="actualCalculationNewPricingProfile")//,priority = 2)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production module should be clicked successfully")
	
	public void deleteNewPricingProfile() throws Exception {
		
		deletePricingProfile();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}
}
