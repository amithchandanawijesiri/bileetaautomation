package productionModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProductionModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class BOOVerification_edit extends ProductionModule{

TestCommonMethods common = new TestCommonMethods();
	
	
	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Production", "", "Verify of BOO", this.getClass().getSimpleName());
	}
	
	
	@Test//(priority = 1)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Click on navigation menu")
	public void navigateSideBar() throws Exception {
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
	    navigateToSideBar();
	}
	
	@Test(dependsOnMethods ="navigateSideBar")//,priority = 2)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production module should be clicked successfully")
	
	public void ProductionBtn() throws Exception {
		
		navigateToProduction1();
	}
	
	@Test(dependsOnMethods ="ProductionBtn")//,priority = 3)
	@Severity(SeverityLevel.CRITICAL)
	@Description("The Bill of operations by page should be loaded successfully")
	
	public void BillOfOperationBtn() throws Exception {
		
		navigateToBillOfOperation1();
	}
	
	
	@Test(dependsOnMethods ="BillOfOperationBtn")//,priority = 4)
	@Severity(SeverityLevel.CRITICAL)
	@Description("The New bill of operations page should be loaded successfully")
	
	public void newBillOfOperationBtn() throws Exception {
		
		navigateTonewBOO1();
	}
	
	@Test(dependsOnMethods ="newBillOfOperationBtn")//,priority = 5)
	@Severity(SeverityLevel.CRITICAL)
	@Description("User entered description under the summary section should be displayed successfully")
	
	public void ProductionFillForm() throws Exception {
		
		fillBOOForm1();
	}
	
	@Test(dependsOnMethods ="ProductionFillForm")//,priority = 6)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Supply operation should be added successfully")
	
	public void ProductionOperationSupply() throws Exception {
		
		OperationActivitySupply1();
	}
	
	@Test(dependsOnMethods ="ProductionOperationSupply")//,priority = 7)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Supply operation should be added successfully")
	
	public void ProductionOperationOperation() throws Exception {
		
		OperationActivityOperation1();
	}
	
	
	@Test(dependsOnMethods ="ProductionOperationOperation")//,priority = 8)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Supply operation should be added successfully")
	
	public void ProductionQC() throws Exception {
		
		QCUnderTheBillOfOperations1();
	
	}
	
	@Test(dependsOnMethods ="ProductionQC")//,priority = 7)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Supply operation should be added successfully")
	
	public void BOORelease() throws Exception {
		
		editNUpdateBOO();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}
}
