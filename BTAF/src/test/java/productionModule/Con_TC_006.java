package productionModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProductionModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({TestListener.class})

public class Con_TC_006 extends ProductionModule{

TestCommonMethods common = new TestCommonMethods();
	
	
	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Production", "", "Verify whether user can fill summary data of bill of operations successully", this.getClass().getSimpleName());
	}
	
	
	@Test//(priority = 1)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Click on navigation menu")
	public void navigateSideBar() throws Exception {
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
	    navigateToSideBar();
	}
	
	@Test(dependsOnMethods ="navigateSideBar")//,priority = 2)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production module should be clicked successfully")
	
	public void ProductionBtn() throws Exception {
		
		navigateToProduction();
	}
	
	@Test(dependsOnMethods ="ProductionBtn")//,priority = 3)
	@Severity(SeverityLevel.CRITICAL)
	@Description("The Bill of operations by page should be loaded successfully")
	
	public void BillOfOperationBtn() throws Exception {
		
		navigateToBillOfOperation();
	}
	
	
	@Test(dependsOnMethods ="BillOfOperationBtn")//,priority = 4)
	@Severity(SeverityLevel.CRITICAL)
	@Description("The New bill of operations page should be loaded successfully")
	
	public void newBillOfOperationBtn() throws Exception {
		
		navigateTonewBOO();
	}
	
	@Test(dependsOnMethods ="newBillOfOperationBtn")//,priority = 5)
	@Severity(SeverityLevel.CRITICAL)
	@Description("User entered description under the summary section should be displayed successfully")
	
	public void ProductionFillForm() throws Exception {
		
		fillBOOForm();
	}
	
	@Test(dependsOnMethods ="ProductionFillForm")//,priority = 6)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Supply operation should be added successfully")
	
	public void ProductionOperationSupply() throws Exception {
		
		OperationActivitySupply();
	}
	
	@Test(dependsOnMethods ="ProductionOperationSupply")//,priority = 7)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Supply operation should be added successfully")
	
	public void ProductionOperationOperation() throws Exception {
		
		OperationActivityOperation();
	}
	
	
	@Test(dependsOnMethods ="ProductionOperationOperation")//,priority = 8)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Supply operation should be added successfully")
	
	public void ProductionQC() throws Exception {
		
		QCUnderTheBillOfOperations();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}
}
