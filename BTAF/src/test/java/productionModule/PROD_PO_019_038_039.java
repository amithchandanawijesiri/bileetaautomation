package productionModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProductionModule;
import bileeta.BTAF.PageObjects.ProductionModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class PROD_PO_019_038_039 extends ProductionModuleRegression {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Production", "Production Order",
				"Verify that system is enabled the production order type respect to the selected production model (For MRP+Infinite type production model)",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create MRP and Infinite Enbled Production Model")
	public void verifyProuctionModelInfiniteMRPEnabledRegression() throws Exception {
		prouctionModelInfiniteMRPEnabledRegression();
	}
	
	@Test(dependsOnMethods = "verifyProuctionModelInfiniteMRPEnabledRegression")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Order")
	public void verifyProductionOrder_1_PROD_PO_019() throws Exception {
		productionOrder_1_PROD_PO_019();
	}
	
	@Test(dependsOnMethods = "verifyProductionOrder_1_PROD_PO_019")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Order")
	public void verifyProductionOrder_2_PROD_PO_019() throws Exception {
		productionOrder_2_PROD_PO_019();
	}
	
	@Test(dependsOnMethods = "verifyProductionOrder_2_PROD_PO_019")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Order - Draft and Duplicate")
	public void verifyProductionOrder_3_PROD_PO_038_039() throws Exception {
		productionOrder_3_PROD_PO_038_039();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
