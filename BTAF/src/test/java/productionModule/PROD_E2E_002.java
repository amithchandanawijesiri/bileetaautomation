package productionModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProductionMultiFlatForm;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;

public class PROD_E2E_002 extends ProductionMultiFlatForm {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Production", "Production Multi Platfor Test Cases", "End To End Scenarios",
				this.getClass().getSimpleName());
	}

	/**
	 * Not able to run between 00.00 to 05.30 , Internal Receipt generate with draft
	 * status.
	 */

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Bill Of Material")
	@Step("Login to the system")
	public void verifyBillOfMaterial_PROD_E2E_002() throws Exception {
		loginBiletaAutomation();
		billOfMaterial_PROD_E2E_002();
	}

	@Test(dependsOnMethods = "verifyBillOfMaterial_PROD_E2E_002")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Overhead Creation")
	public void verifyCreateOverheadInformations() throws Exception {
		createOverheadInformations();
	}

	@Test(dependsOnMethods = "verifyCreateOverheadInformations")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Resourse Creation")
	public void verifyCreateResourses() throws Exception {
		createResourses();
	}

	@Test(dependsOnMethods = "verifyCreateResourses")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Bill of Operation Creation")
	public void verifyBillOfOperation_PROD_E2E_002() throws Exception {
		billOfOperation_PROD_E2E_002();
	}

	@Test(dependsOnMethods = "verifyBillOfOperation_PROD_E2E_002")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Unit Creation")
	public void verifyProductionUnit() throws Exception {
		productionUnit();
	}

	@Test(dependsOnMethods = "verifyProductionUnit")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Pricing Profile Creation")
	public void verifyPricingProfile() throws Exception {
		pricingProfile();
	}

	@Test(dependsOnMethods = "verifyPricingProfile")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Model Creation")
	public void verifyCreateProductionModel() throws Exception {
		productionModel1_PROD_E2E_002();
		productionModel2_PROD_E2E_002();
		productionModel3_PROD_E2E_002();
		productionModel4_PROD_E2E_002();
	}

	@Test(dependsOnMethods = "verifyCreateProductionModel")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Order Creation")
	public void verifyCreateProductionOrder() throws Exception {
		productionOrder_1_PROD_E2E_002();
		productionOrder_2_PROD_E2E_002();
	}
	
	@Test(dependsOnMethods = "verifyCreateProductionOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Internal Order to Internal Dispatch Order Journey")
	public void verifyInternalOrderToInternalDispatchOrder_PROD_E2E_002() throws Exception {
		internalOrderToInternalDispatchOrder_PROD_E2E_002();
	}
	

	@Test(dependsOnMethods = "verifyInternalOrderToInternalDispatchOrder_PROD_E2E_002")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Shop Floor Update")
	public void verifyShopFloorUpdate() throws Exception {
		shopFloorUpdate_PROD_E2E_002();
	}

	@Test(dependsOnMethods = "verifyShopFloorUpdate")
	@Severity(SeverityLevel.CRITICAL)
	@Description("QC")
	public void verifyQc() throws Exception {
		qc_PROD_E2E_002();
	}
	
	@Test(dependsOnMethods = "verifyQc")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Costing")
	public void verifyProductionCosting_PROD_E2E_002() throws Exception {
		productionCosting_PROD_E2E_002();
	}

	@Test(dependsOnMethods = "verifyProductionCosting_PROD_E2E_002")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Generate Internal Receipt")
	public void verifyInternalReciptAndJournelEntry() throws Exception {
		internalReciptAndJournelEntry_PROD_E2E_002();
	}
	
	@Test(dependsOnMethods = "verifyInternalReciptAndJournelEntry")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Shop Floor Update Second Batch")
	public void verifyShopFloorUpdateSecondBatch() throws Exception {
		shopFloorUpdateSecondBatch_PROD_E2E_002();
	}

	@Test(dependsOnMethods = "verifyShopFloorUpdateSecondBatch")
	@Severity(SeverityLevel.CRITICAL)
	@Description("QC Second Batch")
	public void verifyQcSecondBatch() throws Exception {
		qcSecondBatchPROD_E2E_002();
	}
	
	@Test(dependsOnMethods = "verifyQcSecondBatch")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Costing Second Batch")
	public void verifyProductionCostingSecondBatch_PROD_E2E_002() throws Exception {
		productionCostingSecondBatch_PROD_E2E_002();
	}

	@Test(dependsOnMethods = "verifyProductionCostingSecondBatch_PROD_E2E_002")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Generate Internal Receipt Second Batch")
	public void verifyInternalReciptAndJournelEntrySecondBatch() throws Exception {
		internalReciptAndJournelEntrySecondBatch_PROD_E2E_002();
	}
	
	@Test(dependsOnMethods = "verifyInternalReciptAndJournelEntrySecondBatch")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Shop Floor Update Third Batch")
	public void verifyShopFloorUpdateThirdBatch() throws Exception {
		shopFloorUpdateThirdBatch_PROD_E2E_002();
	}

	@Test(dependsOnMethods = "verifyShopFloorUpdateThirdBatch")
	@Severity(SeverityLevel.CRITICAL)
	@Description("QC Third Batch")
	public void verifyQcThirdBatch() throws Exception {
		qcThirdBatchPROD_E2E_002();
	}
	
	@Test(dependsOnMethods = "verifyQcThirdBatch")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Costing Third Batch")
	public void verifyProductionCostingThirdBatch_PROD_E2E_002() throws Exception {
		productionCostingThirdBatch_PROD_E2E_002();
	}

	@Test(dependsOnMethods = "verifyProductionCostingThirdBatch_PROD_E2E_002")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Generate Internal Receipt Third Batch")
	public void verifyInternalReciptAndJournelEntryThirdBatch() throws Exception {
		internalReciptAndJournelEntryThirdBatch_PROD_E2E_002();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
