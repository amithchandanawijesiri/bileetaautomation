package productionModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProductionModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class BOMVerification_delete extends ProductionModule{

TestCommonMethods common = new TestCommonMethods();
	
	
	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Production", "", "Production delete", this.getClass().getSimpleName());
	}
	
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Click on navigation menu")
	public void navigateSideBar() throws Exception {
		navigateToTheLogin();
		verifyThelogo();
		userlogin();
		navigateTosideBar();
		navigateToProductionmenu();
	}
	
	@Test(dependsOnMethods ="navigateSideBar")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Inventory should be clicked successfully")
	
	public void InventoryBtn() throws Exception {
		
		newOutputProduct();
		navigatetoInventory();
	}
	
	@Test(dependsOnMethods ="InventoryBtn")
	@Severity(SeverityLevel.CRITICAL)
	@Description("The Bill of materials by should be loaded successfully")
	
	public void BillOfMaterialNavigate() throws Exception {
		
		navigatetoBillOfMaterial();
	}
	
	@Test(dependsOnMethods ="BillOfMaterialNavigate")
	@Severity(SeverityLevel.CRITICAL)
	@Description("The New bill of material page should be loaded successfully")
	
	public void BillOfMaterialNew() throws Exception {
		
		navigatetonewBOM();
	}
	

	@Test(dependsOnMethods ="BillOfMaterialNew")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Mandatory fields should be successfully")
	
	public void BillOfMaterialFill() throws Exception {
		
		fillingBOMform();
	}
	
	@Test(dependsOnMethods ="BillOfMaterialFill")
	@Severity(SeverityLevel.CRITICAL)
	@Description("product level BOM drafted and released successfully")
	
	public void BillOfMaterialComplete() throws Exception {
		
		completenreleaseBOM();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
