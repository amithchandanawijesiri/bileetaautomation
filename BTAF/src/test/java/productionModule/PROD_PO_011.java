package productionModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProductionModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class PROD_PO_011 extends ProductionModule{
	
TestCommonMethods common = new TestCommonMethods();
	
	
	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Production", "", "Verify whether user can create new production order using ‘Duplicate’ function when document status is on ‘Draft’ state", this.getClass().getSimpleName());
	}
	
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("user can create new production order using ‘Duplicate’ function when document status is on ‘Draft’ state")
	public void pro_po_011() throws Exception {
		
		PROD_PO_011();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
