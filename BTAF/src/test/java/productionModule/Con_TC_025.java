package productionModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProductionModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({TestListener.class})

public class Con_TC_025 extends ProductionModule{

TestCommonMethods common = new TestCommonMethods();
	
	
	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Production", "", "Verify whether user able to add QC operations to production model", this.getClass().getSimpleName());
	}
	
	
	@Test//(priority = 1)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Click on navigation menu")
	public void navigateSideBar() throws Exception {
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
	    navigateToSideBar();
	    navigateToProduction();
	    navigateToProductionModel();
	    ProductionModelForm();
	    ProductionModelFormOnlyMRPEnable();
	    BOODetails();
	    BOODetailsTabSupply();
	    BOODetailsTabOperation();
	}
	
//	@Test(dependsOnMethods ="navigateSideBar")//,priority = 2)
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("Production module should be clicked successfully")
//	
//	public void ProductionBtn() throws Exception {
//		
//		navigateToProduction();
//	}
//
//	@Test(dependsOnMethods ="ProductionBtn")//,priority = 3)
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("Production model should be clicked successfully")
//	
//	public void ProductionModel() throws Exception {
//		
//		navigateToProductionModel();
//	}
//	
//	@Test(dependsOnMethods ="ProductionModel")//,priority = 4)
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("Production Model Form Filled successfully")
//	
//	public void newProductionModelFormMRP() throws Exception {
//		
//		ProductionModelFormOnlyMRPEnable();
//	}
//	
//	@Test(dependsOnMethods ="newProductionModelFormMRP")//,priority = 5)
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("Bill Of Operation successfully")
//	
//	public void BillOfOperation() throws Exception {
//		
//		BOODetails();
//	}
//	
//	
//	@Test(dependsOnMethods ="BillOfOperation")//,priority = 6)
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("Bill Of Operation tab viewed successfully")
//	
//	public void BillOfOperationTabSupply() throws Exception {
//		
//		BOODetailsTabSupply();
//	}
//	
//	@Test(dependsOnMethods ="BillOfOperationTabSupply")//,priority = 7)
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("Bill Of Operation tab viewed successfully")
//	
//	public void BillOfOperationTabOperation() throws Exception {
//		
//		BOODetailsTabOperation();
//	}
	
	@Test(dependsOnMethods ="navigateSideBar")//,priority = 8)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Bill Of Operation tab viewed successfully")
	
	public void BillOfOperationTabQC() throws Exception {
		
		BOODetailsTabQC();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
