package productionModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProductionModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({TestListener.class})

public class Con_TC_030 extends ProductionModule{

TestCommonMethods common = new TestCommonMethods();
	
	
	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Production", "", "Verify whether user can fill summary data of production order successfully & Verify that user able to run the MRP successfully", this.getClass().getSimpleName());
	}
	
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Click on navigation menu")
	public void navigateSideBar() throws Exception {
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
	    navigateToSideBar();
	    navigateToProduction();
	    navigateTonewProductionOrderPage();
	    informationProductionOrder();
	}
	
//	@Test(dependsOnMethods ="navigateSideBar")//,priority = 2)
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("Production module should be clicked successfully")
//	
//	public void ProductionBtn() throws Exception {
//		
//		navigateToProduction();
//	}
//
//	@Test(dependsOnMethods ="ProductionBtn")//,priority = 3)
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("Production order should be clicked successfully")
//	
//	public void ProductionOrder() throws Exception {
//		
//		navigateTonewProductionOrderPage();
//	}
//	
//	@Test(dependsOnMethods ="ProductionOrder")//,priority = 4)
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("Production order information should be filled successfully")
//	
//	public void ProductionOrderInformation() throws Exception {
//		
//		informationProductionOrder();
//	}
	
	@Test(dependsOnMethods ="navigateSideBar")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production order form should be filled successfully")
	
	public void ProductionOrderForm() throws Exception {
		
		fillProductionOrder();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}
}
