package productionModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProductionModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class ModelVerification_delete extends ProductionModule{

TestCommonMethods common = new TestCommonMethods();
	
	
	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Production", "", "Verify whether user can navigate to new production model page successfully", this.getClass().getSimpleName());
	}
	
	
	@Test//(priority = 1)
	@Severity(SeverityLevel.CRITICAL)
	@Description("Click on navigation menu")
	public void navigateSideBar() throws Exception {
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
	    navigateToSideBar();
		navigateToProduction1();
		navigateToBillOfOperation1();
		navigateTonewBOO1();
		fillBOOForm1();
		OperationActivitySupply1();
		OperationActivityOperation1();
		QCUnderTheBillOfOperations1();
		completeNReleaseBOO();
	}
	
	@Test(dependsOnMethods ="navigateSideBar")
	@Severity(SeverityLevel.CRITICAL)
	@Description("The New production model page should be loaded successfully")
	
	public void ProductionBtn() throws Exception {
		navigateToSideBar();
		navigateToProduction1();
		navToProductionModel();
	}

	@Test(dependsOnMethods ="ProductionBtn")
	@Severity(SeverityLevel.CRITICAL)
	@Description("The production model link should be available on the page")
	
	public void ProductionModel() throws Exception {
		
		ProductionModelForm1();
		ProductionModelFormOnlyMRPEnable1();
		BOODetails1();
		completenreleaseProductionModel();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}
}
