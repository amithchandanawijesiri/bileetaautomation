package productionModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProductionModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class Con_TC_053 extends ProductionModule{

TestCommonMethods common = new TestCommonMethods();
	
	
	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Production", "", "Verify whether user can enter summary information of Bulk Prodution Order", this.getClass().getSimpleName());
	}
	
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Click on navigation menu")
	public void navigateSideBar() throws Exception {
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
	    navigateToSideBar();
	}
	
	@Test(dependsOnMethods ="navigateSideBar")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production module should be clicked successfully")
	
	public void ProductionBtn() throws Exception {
		
		navigateToProduction();
	}
	
	@Test(dependsOnMethods ="ProductionBtn")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Bulk Production should be clicked successfully")
	
	public void BulkProductionForm() throws Exception {
		
		navigateToBulkProductionForm();
	}
	
	@Test(dependsOnMethods ="ProductionBtn")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Bulk Production page should be displayed successfully")
	
	public void SummaryBulkProduction() throws Exception {
		
		SummaryBulkProductionOrder();
	}
	
	@Test(dependsOnMethods ="SummaryBulkProduction")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Bulk Production page should be displayed successfully")
	
	public void BulkProductionOrder() throws Exception {
		
		ProductBulkProductionOrder();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
