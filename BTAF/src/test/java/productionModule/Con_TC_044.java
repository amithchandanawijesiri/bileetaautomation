package productionModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProductionModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({TestListener.class})

public class Con_TC_044 extends ProductionModule{

TestCommonMethods common = new TestCommonMethods();
	
	
	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Production", "", "Verify that user can sucessfully create the Overhead for production", this.getClass().getSimpleName());
	}
	
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Click on navigation menu")
	public void navigateSideBar() throws Exception {
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
	    navigateToSideBar();
	}
	
	@Test(dependsOnMethods ="navigateSideBar")//,priority = 2)
	@Severity(SeverityLevel.CRITICAL)
	@Description("View production module")
	public void ProductionMenu() throws Exception {
		
		navigateToProductionMenu();
	}
	
	
	@Test(dependsOnMethods ="ProductionMenu")//,priority = 2)
	@Severity(SeverityLevel.CRITICAL)
	@Description("View production module")
	public void overheadInfo() throws Exception {
		
		navigateToOverheadInfo();
	}
	
	@Test(dependsOnMethods ="overheadInfo")//,priority = 2)
	@Severity(SeverityLevel.CRITICAL)
	@Description("View production module")
	public void overheadInfoCreate() throws Exception {
		
		createOverheadInformation();
	}

	@Test(dependsOnMethods ="overheadInfoCreate")//,priority = 2)
	@Severity(SeverityLevel.CRITICAL)
	@Description("View production module")
	public void overheadInfoSearch() throws Exception {
		
		searchcreatedOverheadInformation();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}
	
}
