package productionModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProductionModule;
import bileeta.BTAF.PageObjects.ProductionModuleSmoke;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class Smoke_Production_0003 extends ProductionModuleSmoke{

TestCommonMethods common = new TestCommonMethods();
	
	
	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Production", "Smoke", "Production Order - Run MRP", this.getClass().getSimpleName());
	}
	
	@Test(dependsOnMethods = {"productionModule.Smoke_Production_0002_L.verifyProductionOrder_Smoke_Production_0002"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Run MRP")
	public void verifyMrpRun_Smoke_Production_0003() throws Exception {
		mrpRun_Smoke_Production_0003();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
