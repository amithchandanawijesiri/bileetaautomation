package productionModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProductionModule;
import bileeta.BTAF.PageObjects.ProductionModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;

import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({TestListener.class})

public class PROD_PM_065 extends ProductionModuleRegression{

	TestCommonMethods common =new TestCommonMethods();
	
	
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Production", "Production Model", "Verify whether the user can not draft the Production model without giving the batch qty", this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Step("Login to the System")
	@Description("Production Model")
	public void verifyProductionModelMRPEnabled_1_PROD_PM_065() throws Exception {
		productionModelMRPEnabled_1_PROD_PM_065();
	}
	
	@Test(dependsOnMethods = "verifyProductionModelMRPEnabled_1_PROD_PM_065")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Model")
	public void verifyProductionModelMRPEnabled_2_PROD_PM_065() throws Exception {
		productionModelMRPEnabled_2_PROD_PM_065();
	}
	
	@Test(dependsOnMethods = "verifyProductionModelMRPEnabled_2_PROD_PM_065")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Model")
	public void verifyProductionModelMRPEnabled_3_PROD_PM_065() throws Exception {
		productionModelMRPEnabled_3_PROD_PM_065();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}
}
