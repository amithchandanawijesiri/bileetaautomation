package productionModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProductionModule;
import bileeta.BTAF.PageObjects.ProductionModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;

import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class PROD_PM_091_092_095_096_097 extends ProductionModuleRegression {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Production", "Production Model",
				"Production Order",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Step("Login to the System")
	@Description("Production Model")
	public void verifyProductionModel_1_PROD_PM_091_092_095_096_097() throws Exception {
		productionModel_1_PROD_PM_091_092_095_096_097();
	}
	
	@Test(dependsOnMethods = "verifyProductionModel_1_PROD_PM_091_092_095_096_097")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Model")
	public void verifyProductionModel_2_PROD_PM_091_092_095_096_097() throws Exception {
		productionModel_2_PROD_PM_091_092_095_096_097();
	}

	@Test(dependsOnMethods = "verifyProductionModel_2_PROD_PM_091_092_095_096_097")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Model")
	public void verifyProductionModel_3_PROD_PM_091_092_095_096_097() throws Exception {
		productionModel_3_PROD_PM_091_092_095_096_097();
	}
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}
}
