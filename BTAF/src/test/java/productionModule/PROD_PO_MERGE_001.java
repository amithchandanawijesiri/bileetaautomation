package productionModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProductionModule;
import bileeta.BTAF.PageObjects.ProductionModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class PROD_PO_MERGE_001 extends ProductionModuleRegression {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Production", "Production Order", "Production Order Form", this.getClass().getSimpleName());
	}

	/* Time zone bug can be a reason for fail this test case  - Don't run between 00.00 to 05.30*/
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Order")
	public void verifyPreRequsitesPORegression() throws Exception {
		productionTypeProductRegression();
		prouctionModelOneMRPEnabledRegression();
		prouctionModelTwoRegression();
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Order")
	public void verifyProductionOrder_PROD_PO_001_to_PROD_PO_011() throws Exception {
		productionOrder_PROD_PO_001_to_PROD_PO_011();
	}

	@Test(dependsOnMethods = "verifyProductionOrder_PROD_PO_001_to_PROD_PO_011")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Order")
	public void verifyProductionOrder_PROD_PO_012_014_015_016_017_024() throws Exception {

		productionOrder_PROD_PO_012_014_015_016_017_024();
	}

	@Test(dependsOnMethods = "verifyProductionOrder_PROD_PO_012_014_015_016_017_024")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Duplicate Production Order")
	public void verifyProductionOrder_PROD_PO_025() throws Exception {

		productionOrder_PROD_PO_025();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
