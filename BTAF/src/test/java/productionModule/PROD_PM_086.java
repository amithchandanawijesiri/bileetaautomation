package productionModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProductionModule;
import bileeta.BTAF.PageObjects.ProductionModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;

import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })

public class PROD_PM_086 extends ProductionModuleRegression {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Production", "Production Model",
				"Verify whether user can ‘Edit’ production model using ‘Edit’ function when document status is on ‘Released’ state",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Step("Login to the System")
	@Description("Production Model")
	public void verifyProductionModelInfiniteAndMRPEnabled_1_PROD_PM_086() throws Exception {
		productionModelInfiniteAndMRPEnabled_1_PROD_PM_086();
	}

	@Test(dependsOnMethods = "verifyProductionModelInfiniteAndMRPEnabled_1_PROD_PM_086")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Model")
	public void verifyProductionModelInfiniteAndMRPEnabled_2_PROD_PM_086() throws Exception {
		productionModelInfiniteAndMRPEnabled_2_PROD_PM_086();
	}

	@Test(dependsOnMethods = "verifyProductionModelInfiniteAndMRPEnabled_2_PROD_PM_086")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Model")
	public void verifyProductionModelInfiniteAndMRPEnabled_3_PROD_PM_086() throws Exception {
		productionModelInfiniteAndMRPEnabled_3_PROD_PM_086();
	}
	
	@Test(dependsOnMethods = "verifyProductionModelInfiniteAndMRPEnabled_3_PROD_PM_086")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Edit Production Model in drafted state")
	public void verifyProductionModelInfiniteAndMRPEnabled_4_PROD_PM_086() throws Exception {
		productionModelInfiniteAndMRPEnabled_4_PROD_PM_086();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}
}
