package productionModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProductionMultiFlatForm;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;

public class PROD_E2E_003 extends ProductionMultiFlatForm {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Production", "Production Multi Platfor Test Cases", "End To End Scenarios - With Standard Cost",
				this.getClass().getSimpleName());
	}

	/**
	 * Not able to run between 00.00 to 05.30 , Internal Receipt generate with draft
	 * status.
	 */

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Bill Of Material")
	@Step("Login to the system")
	public void verifyBillOfMaterial_PROD_E2E_003() throws Exception {
		loginBiletaAutomation();
		billOfMaterial_PROD_E2E_003();
	}

	@Test(dependsOnMethods = "verifyBillOfMaterial_PROD_E2E_003")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Overhead Creation")
	public void verifyCreateOverheadInformations() throws Exception {
		createOverheadInformations();
	}

	@Test(dependsOnMethods = "verifyCreateOverheadInformations")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Resourse Creation")
	public void verifyCreateResourses() throws Exception {
		createResourses();
	}

	@Test(dependsOnMethods = "verifyCreateResourses")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Bill of Operation Creation")
	public void verifyBillOfOperation_PROD_E2E_003() throws Exception {
		billOfOperation_PROD_E2E_003();
	}

	@Test(dependsOnMethods = "verifyBillOfOperation_PROD_E2E_003")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Unit Creation")
	public void verifyProductionUnit() throws Exception {
		productionUnit();
	}

	@Test(dependsOnMethods = "verifyProductionUnit")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Pricing Profile Creation")
	public void verifyPricingProfile() throws Exception {
		pricingProfile();
	}

	@Test(dependsOnMethods = "verifyPricingProfile")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Model Creation")
	public void verifyCreateProductionModel() throws Exception {
		productionModel1_PROD_E2E_003();
		productionModel2_PROD_E2E_003();
		productionModel3_PROD_E2E_003();
		productionModel4_PROD_E2E_003();
	}

	@Test(dependsOnMethods = "verifyCreateProductionModel")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Order Creation")
	public void verifyCreateProductionOrder() throws Exception {
		productionOrder_1_PROD_E2E_003();
		productionOrder_2_PROD_E2E_003();
	}

	@Test(dependsOnMethods = "verifyCreateProductionOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Run MRP")
	public void verifyMrp() throws Exception {
		mrpRun1_PROD_E2E_003();
		mrpRun2_PROD_E2E_003();
	}

	@Test(dependsOnMethods = "verifyMrp")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Shop Floor Update")
	public void verifyShopFloorUpdate() throws Exception {
		shopFloorUpdate_PROD_E2E_003();
	}

	@Test(dependsOnMethods = "verifyShopFloorUpdate")
	@Severity(SeverityLevel.CRITICAL)
	@Description("QC")
	public void verifyQc() throws Exception {
		qc_PROD_E2E_003();
	}

	@Test(dependsOnMethods = "verifyQc")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Generate Internal Receipt")
	public void verifyInternalReciptAndJournelEntry() throws Exception {
		internalReciptAndJournelEntry_PROD_E2E_003();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
