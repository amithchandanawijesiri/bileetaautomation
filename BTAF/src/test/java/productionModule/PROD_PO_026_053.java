package productionModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProductionModule;
import bileeta.BTAF.PageObjects.ProductionModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class PROD_PO_026_053 extends ProductionModuleRegression {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Production", "Production Order",
				"Verify whether the user is able to Update the production order before release the production order AND Verify whether user can view the production order history",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Order")
	public void verifyProductionOrder_1_PROD_PO_026_053() throws Exception {
		productionOrder_1_PROD_PO_026_053();
	}

	@Test(dependsOnMethods = "verifyProductionOrder_1_PROD_PO_026_053")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Production Order")
	public void verifyProductionOrder_2_PROD_PO_026_053() throws Exception {
		productionOrder_2_PROD_PO_026_053();
	}

	@Test(dependsOnMethods = "verifyProductionOrder_2_PROD_PO_026_053")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Edit and Update the Production Order / View History")
	public void verifyProductionOrder_3_PROD_PO_026_053() throws Exception {
		productionOrder_3_PROD_PO_026_053();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();

	}

}
