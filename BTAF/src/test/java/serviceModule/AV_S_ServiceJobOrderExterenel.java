package serviceModule;

import org.testng.annotations.AfterClass;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ServiceModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({TestListener.class})


public class AV_S_ServiceJobOrderExterenel extends ServiceModule{
	TestCommonMethods common = new TestCommonMethods();
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Service", "", "Verify that user can successfully loing to the Entution with valid Username and valid Password ", this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Open the browser and enter the application URL")
	public void _navigateToTheLoginPage() throws Exception
	{
		navigateToTheLoginPage();
	}
	
	@Test(dependsOnMethods = "_navigateToTheLoginPage")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that 'Entution' header is available on the page")
	public void _verifyTheLogo() throws Exception
	{
		verifyTheLogo();
		
	}
	
	@Test(dependsOnMethods = "_verifyTheLogo")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the user login")
	public void _verifyuserlogin() throws Exception
	{
		userLogin();
		
	}
	

	@Test(dependsOnMethods = "_verifyuserlogin")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the user login")
	public void _navigationmenu()  throws Exception
	{
		navigationmenu();
		
	}
	
	@Test(dependsOnMethods = "_navigationmenu")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _btn_service() throws Exception
	{
		btn_service();
		
	}
	
	@Test(dependsOnMethods = "_btn_service")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _btn_ServiceJobOrder() throws Exception
	{
		btn_ServiceJobOrder();
		
	}
	
	@Test(dependsOnMethods = "_btn_ServiceJobOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _searchlocation() throws Exception
	{
		searchlocation();
		
	}
	
	@Test(dependsOnMethods = "_searchlocation")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _task() throws Exception
	{
		task();
		
	}
	
	
	@Test(dependsOnMethods = "_task")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _selectbatchproduct() throws Exception
	{
		selectbatchproduct();
		
	}
	

	@Test(dependsOnMethods = "_selectbatchproduct")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _draftandnew() throws Exception
	{
		draftandnew();
		
	}
	
	@Test(dependsOnMethods = "_draftandnew")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _btn_ServiceJobOrderhalf2() throws Exception
	{
		btn_ServiceJobOrderhalf();
		
	}
	
	@Test(dependsOnMethods = "_btn_ServiceJobOrderhalf2")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _searchlocation2() throws Exception
	{
		searchlocation();
		
	}
	
	@Test(dependsOnMethods = "_searchlocation2")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _task2() throws Exception
	{
		task();
		
	}
	
	@Test(dependsOnMethods = "_task2")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _Verificationdraft() throws Exception
	{
		Verificationdraft();
		
	}
	
	
	@Test(dependsOnMethods = "_Verificationdraft")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _Edit() throws Exception
	{
		Edit();
		
	}
	
	
	@Test(dependsOnMethods = "_Edit")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _UpdateandNew() throws Exception
	{
		UpdateandNew();
		
	}
	
	@Test(dependsOnMethods = "_UpdateandNew")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _btn_ServiceJobOrderhalf() throws Exception
	{
		btn_ServiceJobOrderhalf();
		
	}
	
	@Test(dependsOnMethods = "_btn_ServiceJobOrderhalf")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _searchlocation1() throws Exception
	{
		searchlocation();
		
	}
	
	@Test(dependsOnMethods = "_searchlocation1")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _task1() throws Exception
	{
		task();
		
	}
	
	
//	@Test(dependsOnMethods = "_task")
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("verify the service module")
//	public void _selectbatchproduct() throws Exception
//	{
//		selectbatchproduct();
//		
//	}
	
	@Test(dependsOnMethods = "_task1")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _Update() throws Exception
	{
		Update();
		
	}
	
	@Test(dependsOnMethods = "_Update")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _Release() throws Exception
	{
		Release();
		
	}
	

//	@Test(dependsOnMethods = "_Release")
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("verify the service module")
//	public void _ActualUpdate() throws Exception
//	{
//		ActualUpdate();
//		
//	}
//	
//	@Test(dependsOnMethods = "_ActualUpdate")
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("verify the service module")
//	public void _AddProductTagNo() throws Exception
//	{
//		AddProductTagNo();
//		
//	}
//	
//	@Test(dependsOnMethods = "_AddProductTagNo")
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("verify the service module")
//	public void _createInteralOrder() throws Exception
//	{
//		createInteralOrder();
//		
//	}
//	
//	@Test(dependsOnMethods = "_AddProductTagNo")
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("verify the service module")
//	public void _createInteralReturnOrder() throws Exception
//	{
//		createInteralReturnOrder();
//		
//	}
//	
//	
//	@Test(dependsOnMethods = "_createInteralReturnOrder")
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("verify the service module")
//	public void _createInvoiceProposal() throws Exception
//	{
//		createInvoiceProposal();
//		
//	}
	
//
//	@Test(dependsOnMethods = "_Release")
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("verify the service module")
//	public void _CreateServiceQuotation() throws Exception
//	{
//		CreateServiceQuotation();
//		
//	}
//	
//	@Test(dependsOnMethods = "_CreateServiceQuotation")
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("verify the service module")
//	public void _CustomerAdvance() throws Exception
//	{
//		CustomerAdvance();
//		
//	}
//	
	
	@Test(dependsOnMethods = "_Release")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _ResponceDetails() throws Exception
	{
		ResponceDetails();
		
	}
	
	@Test(dependsOnMethods = "_ResponceDetails")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _RMA() throws Exception
	{
		RMA();
		
	}
	
	@Test(dependsOnMethods = "_RMA")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _SubContractOrder() throws Exception
	{
		SubContractOrder();
		
	}
	
	@Test(dependsOnMethods = "_SubContractOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _Stop() throws Exception
	{
		Stop();
		
	}
	@AfterClass(alwaysRun=true)
	  public void AfterClass() throws Exception 
	  {
		  common.totalTime(startTime);
		  
		  common.driverClose();
		 
	  }
}
