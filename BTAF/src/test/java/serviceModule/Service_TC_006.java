package serviceModule;

import org.testng.annotations.AfterClass;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ServiceModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({TestListener.class})

public class Service_TC_006 extends ServiceModule{
	
	TestCommonMethods common = new TestCommonMethods();
//	ServiceModule service = new ServiceModule();
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Service", "", "Verify that user can successfully loing to the Entution with valid Username and valid Password ", this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Open the browser and enter the application URL")
	public void _navigateToTheLoginPage() throws Exception
	{
		navigateToTheLoginPage();
	}
	
	@Test(dependsOnMethods = "_navigateToTheLoginPage")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that 'Entution' header is available on the page")
	public void _verifyTheLogo() throws Exception
	{
		verifyTheLogo();
		
	}
	
	@Test(dependsOnMethods = "_verifyTheLogo")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the user login")
	public void _verifyuserlogin() throws Exception
	{
		userLogin();
		
	}
	

	@Test(dependsOnMethods = "_verifyuserlogin")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the user login")
	public void _navigationmenu()  throws Exception
	{
		navigationmenu();
		
	}
	
	@Test(dependsOnMethods = "_navigationmenu")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _btn_service() throws Exception
	{
		btn_service();
		
	}
	
	@Test(dependsOnMethods = "_btn_service")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _btn_ServiceJobOrder() throws Exception
	{
		btn_ServiceJobOrder();
		
	}
	
	@Test(dependsOnMethods = "_btn_ServiceJobOrder")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _searchlocation() throws Exception
	{
		searchlocation();
		
	}
	
	@Test(dependsOnMethods = "_searchlocation")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _task() throws Exception
	{
		task();
		
	}
	
	
	@Test(dependsOnMethods = "_task")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _selectbatchproduct() throws Exception
	{
		selectbatchproduct();
		
	}
	
	@Test(dependsOnMethods = "_selectbatchproduct")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _selectFIFOproduct() throws Exception
	{
		selectFIFOproduct();
		
	}
	
	@Test(dependsOnMethods = "_selectFIFOproduct")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _selectSERVICEproduct() throws Exception
	{
		selectSERVICEproduct();
		
	}
	
		
	@Test(dependsOnMethods = "_selectSERVICEproduct")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _selectlabour() throws Exception
	{
		selectlabour();
		
	}
	
	@Test(dependsOnMethods = "_selectlabour")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _selectsubcontract() throws Exception
	{
		selectsubcontract();
		
	}
	
	@Test(dependsOnMethods = "_selectsubcontract")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _selectexpense() throws Exception
	{
		selectexpense();
		
	}
	
	@Test(dependsOnMethods = "_selectsubcontract")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _selectlotproduct() throws Exception
	{
		selectlotproduct();
		
	}
	
	@Test(dependsOnMethods = "_selectlotproduct")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _selectSerialBatchProduct() throws Exception
	{
		selectSerialBatchProduct();
		
	}
	
	@Test(dependsOnMethods = "_selectSerialBatchProduct")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _selectResources() throws Exception
	{
		selectResources();
		
	}
	
	@Test(dependsOnMethods = "_selectResources")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _draftandrelease() throws Exception
	{
	draftandrelease();
		
	}
	
	
	@Test(dependsOnMethods = "_draftandrelease")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the service module")
	public void _SubcontractOrderAction() throws Exception
	{
		SubcontractOrderAction();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		  
		common.driverClose();
	
		 
	}
}