package serviceModule;

import org.testng.annotations.AfterClass;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ServiceModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({TestListener.class})


public class NewWarrantyProfile extends ServiceModule{
	TestCommonMethods common = new TestCommonMethods();
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Service", "", "Verify that user can successfully loing to the Entution with valid Username and valid Password ", this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Open the browser and enter the application URL")
	public void _navigateToTheLoginPage() throws Exception
	{
		navigateToTheLoginPage();
	}
	
	@Test(dependsOnMethods = "_navigateToTheLoginPage")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that 'Entution' header is available on the page")
	public void _verifyTheLogo() throws Exception
	{
		verifyTheLogo();
		
	}
	
	@Test(dependsOnMethods = "_verifyTheLogo")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the user login")
	public void _verifyuserlogin() throws Exception
	{
		userLogin();
		
	}
	

	@Test(dependsOnMethods = "_verifyuserlogin")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the user login")
	public void _navigationmenu()  throws Exception
	{
		navigationmenu();
		
	}
	
	
	@Test(dependsOnMethods = "_navigationmenu")
	@Severity(SeverityLevel.CRITICAL)
	@Description
	public void _CreateNewWarrantyProfile() throws Exception
	{
		CreateNewWarrantyProfile();
		
	}
	
	@AfterClass(alwaysRun=true)
	  public void AfterClass() throws Exception 
	  {
		  common.totalTime(startTime);
		  
		  common.driverClose();
		 
	  }
}

