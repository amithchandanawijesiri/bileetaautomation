package serviceModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ServiceModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({TestListener.class})

public class Service_DWS_051to053 extends ServiceModuleRegression{

	TestCommonMethods common = new TestCommonMethods();
	
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Service", "", "Verify that same employee can be added to 2 different job orders when  timeIn and Time out are different", this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("open the browser and enter the application URL")
	public void loginService() throws Exception {
	
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		clickNavigation();
	}
	
	@Test(dependsOnMethods = "loginService")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that same employee can be added to 2 different job orders when  timeIn and Time out are different")
	public void VerThatSameEmployeeCanBeAddedToTwoDifferentJobOrdersWhenTimeInAndTimeOutAreDifferent() throws Exception {
	
		VerifyThatSameEmployeeCanBeAddedToTwoDifferentJobOrdersWhenTimeInAndTimeOutAreDifferent();
	}
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		  
		common.driverClose();
		 
	}
}