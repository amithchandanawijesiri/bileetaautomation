package serviceModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ServiceModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({TestListener.class})

public class Service_HA_050 extends ServiceModuleRegression{

	TestCommonMethods common = new TestCommonMethods();
	
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Service", "", "Verify that Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Monthly)", this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("open the browser and enter the application URL")
	public void loginService() throws Exception {
	
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		clickNavigation();
	}
	
	@Test(dependsOnMethods = "loginService")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Monthly)")
	public void VerThatScheduleCodeLookUpOnlyDisplayedTheCodesWhichAreRelatedToTheBillingBasisBillingBasisMonthly() throws Exception {
	
		VerifyThatScheduleCodeLookUpOnlyDisplayedTheCodesWhichAreRelatedToTheBillingBasisBillingBasisMonthly();
	}
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		  
		common.driverClose();
		 
	}
}

