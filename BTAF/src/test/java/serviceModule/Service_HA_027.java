package serviceModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ServiceModuleRegression;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({TestListener.class})

public class Service_HA_027 extends ServiceModuleRegression{

	TestCommonMethods common = new TestCommonMethods();
	
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Service", "", "Verify that status column of the Actual update look up is changing to pending to \"Billed\", after running the service invoice", this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("open the browser and enter the application URL")
	public void loginService() throws Exception {
	
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		clickNavigation();
	}
	
	@Test(dependsOnMethods = "loginService")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that status column of the Actual update look up is changing to pending to \"Billed\", after running the service invoice")
	public void VerThatStatusColumnOfTheActualUpdateLookUpIsChangingToPendingToBilledAfterRunningTheServiceInvoice() throws Exception {
	
		VerifyThatStatusColumnOfTheActualUpdateLookUpIsChangingToPendingToBilledAfterRunningTheServiceInvoice();
	}
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		  
		common.driverClose();
		 
	}
}


