package common;

import org.testng.annotations.Test;


import bileeta.BTAF.Utilities.SendTestSummery;
import bileeta.BTAF.Utilities.TestBase;
import bileeta.BTAF.Utilities.TestCommonMethods;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

public class TestInitiateTest extends TestBase{

	
	
	TestCommonMethods common = new  TestCommonMethods();
	
	
  @BeforeSuite
  public void initiateTheTest() throws Exception 
  {
	  readParameters();
	  
	 
  }

  
  @AfterSuite
  public void sendEmail() throws Exception
  {
	 
	  
	  common.driverQuit();
  }
  
  
  
}
