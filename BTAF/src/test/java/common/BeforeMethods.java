package common;

import org.testng.annotations.Test;

import bileeta.BTAF.Utilities.ReadXL;

import java.io.File;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;

public class BeforeMethods extends ReadXL{

	
  @BeforeTest
  public void beforeClass() {
	  
	    File directory = new File(resultFolder);
		File[] files = directory.listFiles();
		for (File file : files)
		{
		   if (!file.delete())
		   {
		     System.out.println("Failed to delete "+file);
		   }
		} 
  }

}
