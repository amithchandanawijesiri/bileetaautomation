package common;

import org.testng.annotations.Test;

import bileeta.BTAF.Utilities.SendTestSummery;
import bileeta.BTAF.Utilities.TestCommonMethods;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;

public class AfterMethods {
 
	SendTestSummery summery = new SendTestSummery();
	TestCommonMethods common = new  TestCommonMethods();
  @AfterSuite
  public void beforeClass() throws Exception {
	  
	  common.finalyze();
	  summery.sendMail();
  }

}
