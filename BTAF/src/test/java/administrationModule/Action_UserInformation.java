package administrationModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.AdministrationModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({TestListener.class})



public class Action_UserInformation extends AdministrationModule{
	
	TestCommonMethods common = new TestCommonMethods();
	AdministrationModule admin = new AdministrationModule();
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Administration", "", "Verify that user can successfully loing to the Entution with valid Username and valid Password ", this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Open the browser and enter the application URL")
	public void navigateToTheLoginPage() throws Exception
	{
		admin.navigateToTheLoginPage();
	}
	
	@Test(dependsOnMethods ="navigateToTheLoginPage" )
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that 'Entution' header is available on the page")
	public void verifyTheLogo() throws Exception
	{
		admin.verifyTheLogo();
		
	}
	
	@Test(dependsOnMethods ="verifyTheLogo" )
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the user login")
	public void verifyuserlogin() throws Exception
	{
		admin.userLogin();
		
	}
	
	@Test(dependsOnMethods ="verifyuserlogin" )
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the navigation")
	public void navigationmenu() throws Exception
	{

		admin.navigationmenu();
	}
	
	@Test(dependsOnMethods ="navigationmenu" )
    @Severity(SeverityLevel.CRITICAL)
	@Description("verify the navigation")
	public void verifybtn_administration() throws Exception
	{
     admin.btn_administration();
		
	}
	@Test(dependsOnMethods ="verifybtn_administration" )
    @Severity(SeverityLevel.CRITICAL)
	@Description("verify the userinformation")
	public void verifyclickuserinformation() throws Exception
	{
    admin.clickuserinformation();
		
	}
	
	@Test(dependsOnMethods ="verifyclickuserinformation" )
    @Severity(SeverityLevel.CRITICAL)
	@Description("verify the userinformation")
	public void ActionUserInformation() throws Exception
	{
    admin.ActionUserInformation();
		
	}
	
	@AfterClass(alwaysRun=true)
	  public void AfterClass() throws Exception 
	  {
		  common.totalTime(startTime);
		  
		  common.driverClose();
		 
	  }
}
	