package administrationModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.AdministrationModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({TestListener.class})

public class Admin_TC_008 extends AdministrationModule{
	
	TestCommonMethods common = new TestCommonMethods();
	AdministrationModule admin = new AdministrationModule();
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Administration", "", "Verify that user can successfully loing to the Entution with valid Username and valid Password ", this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Open the browser and enter the application URL")
	public void navigateToTheLoginPage() throws Exception
	{
		admin.navigateToTheLoginPage();
	}
	
	@Test(dependsOnMethods = "navigateToTheLoginPage")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that 'Entution' header is available on the page")
	public void verifyTheLogo() throws Exception
	{
		admin.verifyTheLogo();
		
	}
	
	@Test(dependsOnMethods = "verifyTheLogo")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verfy that user can re generate password via forgot password")
	public void clickforgotpassword() throws Exception
	{
		admin.clickforgotpassword();
		
	}
	
	@Test(dependsOnMethods = "clickforgotpassword")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verfy that user cangive their mail id")
	public void userLogintomail() throws Exception
	{
		admin.userLogintomail();
		
	}
	
	@Test(dependsOnMethods = "userLogintomail" )
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verfy that user cangive their mail id")
	public void navigateToTheMailPage() throws Exception
	{
		admin.navigateToTheMailPage();
		
	}
	
	@Test(dependsOnMethods = "navigateToTheMailPage")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verfy the account")
	public void uservalidate() throws Exception
	{
		admin.uservalidate();
		
	}
	
//	@Test(dependsOnMethods = "uservalidate")
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("Check the inbox")
//    public void isMailRead() throws Exception
//	{
//		admin.IsMailRead();
//		
//	}
	
	
//	
//	@Test(dependsOnMethods = "uservalidate")
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("Check the inbox")
//    public void IsPasswordResetSent1() throws Exception
//	{
//		admin.IsPasswordResetSent();
//		
//	}
	
//	@Test(dependsOnMethods = "IsPasswordResetSent1")
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("Check the inbox")
//    public void OpenMailRead() throws Exception
//	{
//		admin.OpenMailRead();
//		
//	}
	

	@AfterClass(alwaysRun=true)
	  public void AfterClass() throws Exception 
	  {
		  common.totalTime(startTime);
		  
		  common.driverClose();
		 
	  }
	
}