package administrationModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.AdministrationModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({TestListener.class})

public class Admin_TC_007 extends AdministrationModule{
	
	TestCommonMethods common = new TestCommonMethods();
	AdministrationModule admin = new AdministrationModule();
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Administration", "", "Verify that user can successfully loing to the Entution with valid Username and valid Password ", this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Open the browser and enter the application URL")
	public void navigateToTheLoginPage() throws Exception
	{
		admin.navigateToTheLoginPage();
	}
	
	
	@Test(dependsOnMethods = "navigateToTheLoginPage")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that 'Entution' header is available on the page")
	public void verifyTheLogo() throws Exception
	{
		admin.verifyTheLogo();
	}
	
	@Test(dependsOnMethods = "verifyTheLogo")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the user login")
	public void verifyTheLogoUserLogin() throws Exception
	{
		admin.userLogin();
	}
	
	@Test(dependsOnMethods = "verifyTheLogoUserLogin")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can change his own password him self")

	public void clickconfigurationbutton() throws Exception {
		
		admin.clickconfigurationbutton();
	}
	
	@Test(dependsOnMethods = "clickconfigurationbutton")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can change his own password him self")

	public void changepassword() throws Exception {
		
		admin.changepassword();
	}
	
	
//	@Test(dependsOnMethods = "changepassword")
//	@Severity(SeverityLevel.CRITICAL)
//	@Description("Open the browser and enter the application URL")
//	public void navigateToTheLoginPage1() throws Exception
//	{
//		admin.navigateToTheLoginPage();
//	}
//	
	
	@Test(dependsOnMethods = "changepassword")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that 'Entution' header is available on the page")
	public void verifyTheLogo1() throws Exception
	{
		admin.verifyTheLogo();
	}
	
	@Test(dependsOnMethods = "verifyTheLogo1")
	@Severity(SeverityLevel.CRITICAL)
	@Description("verify the user login")
	public void verifyTheLogoUserLogin1() throws Exception
	{
		admin.userLoginwithnewpassword();
	}
	
	@AfterClass(alwaysRun=true)
	  public void AfterClass() throws Exception 
	  {
		  common.totalTime(startTime);
		  
		  common.driverClose();
		 
	  }
	
}