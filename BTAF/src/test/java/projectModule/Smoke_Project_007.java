package projectModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProjectModule;
import bileeta.BTAF.PageObjects.ProjectModuleSmoke;
import bileeta.BTAF.PageObjects.SalesAndMarketingModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })
public class Smoke_Project_007 extends ProjectModuleSmoke {

	TestCommonMethods common = new TestCommonMethods();
	SalesAndMarketingModule sales = new SalesAndMarketingModule();
	ProjectModule project = new ProjectModule();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Project", "Actual Update", "Verify the ability to do the actual update of input products and resources via project action \"Actual Update\"",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"projectModule.Smoke_Project_004.verifyReleaseInternalAndInternalDispatchOrders_Smoke_Project_004"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Actual Update")
	public void verifyActualUpdate_Smoke_Project_007() throws Exception {
		actualUpdate_Smoke_Project_007();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
