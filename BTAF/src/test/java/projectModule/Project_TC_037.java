package projectModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProjectModule;
import bileeta.BTAF.PageObjects.SalesAndMarketingModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class Project_TC_037 extends ProjectModule {
	
	TestCommonMethods common = new TestCommonMethods();
	SalesAndMarketingModule sales = new SalesAndMarketingModule();
	ProjectModule project = new ProjectModule();
		
	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Project", "",
				"Verify that expenses recorded from the payment voucher shows in project costing summary",
				this.getClass().getSimpleName());	
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that project module is available")
	public void checkProject() throws Exception {
		
		sales.checkNavigateToTheLoginPage();
		sales.verifyTheLogo();
		sales.checkUserLogin();
		sales.checkNavigationMenu();
		project.checkProjectModule();
		project.clickOnProjectModule();
	}
	

	@Test(dependsOnMethods = "checkProject")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify the payment voucher")
	public void costAllocation() throws Exception {

		project.verifyPaymentVoucher();

	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {

		common.totalTime(startTime);
		common.driverClose();
	}
}
