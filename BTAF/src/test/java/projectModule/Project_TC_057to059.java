package projectModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProjectModule;
import bileeta.BTAF.PageObjects.ProjectModuleBusinessScenarios;
import bileeta.BTAF.PageObjects.SalesAndMarketingModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class Project_TC_057to059 extends ProjectModuleBusinessScenarios{
	

	TestCommonMethods common = new TestCommonMethods();
	SalesAndMarketingModule sales = new SalesAndMarketingModule();
	ProjectModule project = new ProjectModule();
		
	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Project", "","Verify the ability of drafting and releasing a project quotation with mandatory data",
				this.getClass().getSimpleName());	
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("User login and Click on navigation menu")
	public void clickNav() throws Exception {

		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		clickNavigation();
	}
	
	@Test(dependsOnMethods = "clickNav")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Click on Project menu")
	public void clickProj() throws Exception {
	
		clickProjectbutton();
	}
	
	@Test(dependsOnMethods = "clickProj")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify the ability of drafting and releasing a project quotation with mandatory data")
	public void VerTheAbilityOfDraftingAndReleasingaProjectQuotationWithMandatoryData() throws Exception {
	
		VerifyTheAbilityOfDraftingAndReleasingaProjectQuotationWithMandatoryData();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {

		common.totalTime(startTime);
		common.driverClose();
	}
}
