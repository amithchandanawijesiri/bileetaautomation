package projectModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProjectModule;
import bileeta.BTAF.PageObjects.SalesAndMarketingModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class Project_TC_042 extends ProjectModule {

	TestCommonMethods common = new TestCommonMethods();
	SalesAndMarketingModule sales = new SalesAndMarketingModule();
	ProjectModule project = new ProjectModule();
		
	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Project", "",
				"Verify the ability to do cost allocation for project tasks using accrual voucher",
				this.getClass().getSimpleName());	
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void checkProject() throws Exception {
		
		sales.checkNavigateToTheLoginPage();
		sales.verifyTheLogo();
		sales.checkUserLogin();
		sales.checkNavigationMenu();

	}
	
	@Test(dependsOnMethods = "checkProject")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user is able to do the vendor refund")
	public void vendorRefundd() throws Exception {

		project.vendorRefund();

	}
	
	@Test(dependsOnMethods = "vendorRefundd")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void customerRefund() throws Exception {

		sales.checkNavigationMenu();
		project.checkProjectModule();
		project.clickOnProjectModule();	
		project.vendorRefundCostingSummary();
		
	}
	
	@Test(dependsOnMethods = "customerRefund")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void costAllocation() throws Exception {

		sales.checkNavigationMenu();
		project.checkProjectModule();	
		project.clickOnProjectModule();
		project.checkOverviewCustomerRefund();

	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {

		common.totalTime(startTime);
		common.driverClose();
	}

}
