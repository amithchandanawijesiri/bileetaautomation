package projectModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProjectModule;
import bileeta.BTAF.PageObjects.ProjectModuleSmoke;
import bileeta.BTAF.PageObjects.SalesAndMarketingModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })
public class Smoke_Project_001 extends ProjectModuleSmoke {

	TestCommonMethods common = new TestCommonMethods();
	SalesAndMarketingModule sales = new SalesAndMarketingModule();
	ProjectModule project = new ProjectModule();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Project", "Project Quatation", "Verify the ability of creating a project quotation",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Product Quatation")
	public void verifyProjectQuatation1_Smoke_Project_001() throws Exception {
		projectQuatation1_Smoke_Project_001();
	}
	
	@Test(dependsOnMethods = "verifyProjectQuatation1_Smoke_Project_001")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Complete Product Quatation")
	public void verifyProjectQuatation2_Smoke_Project_001() throws Exception {
		projectQuatation2_Smoke_Project_001();
	}
	
	@Test(dependsOnMethods = "verifyProjectQuatation2_Smoke_Project_001")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Convert to Project part")
	public void verifyConvertToProject_Smoke_Project_001() throws Exception {
		convertToProject_Smoke_Project_001();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
