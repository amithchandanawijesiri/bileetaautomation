package projectModule;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.AdministrationModule;
import bileeta.BTAF.PageObjects.ProcumentModule;
import bileeta.BTAF.PageObjects.ProjectModule;
import bileeta.BTAF.PageObjects.ProjectModuleBusinessScenarios;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({TestListener.class})

public class Project_TC_007to008 extends ProjectModuleBusinessScenarios{

	TestCommonMethods common = new TestCommonMethods();
		
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Project", "", "Verify the accuracy of labour rates calculation after recording actual hours through the daily worksheet form and its reflection on costing summary/overview", this.getClass().getSimpleName());
	
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("User login and Click on navigation menu")
	public void clickNav() throws Exception {
	
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		clickNavigation();
	}
	
	@Test(dependsOnMethods = "clickNav")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify the accuracy of labour rates calculation")
	public void Proj_TC_007to008() throws Exception {
	
		Project_TC_007to008();
	}

	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		  
		common.driverClose();
		 
	}
}

