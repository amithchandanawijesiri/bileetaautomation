package projectModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProjectModule;
import bileeta.BTAF.PageObjects.SalesAndMarketingModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class Project_TC_048 extends ProjectModule{
	

	TestCommonMethods common = new TestCommonMethods();
	SalesAndMarketingModule sales = new SalesAndMarketingModule();
	ProjectModule project = new ProjectModule();
		
	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Project", "",
				"Verify the ability to do cost allocation for project tasks using accrual voucher",
				this.getClass().getSimpleName());	
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void checkProject() throws Exception {
		
		sales.checkNavigateToTheLoginPage();
		sales.verifyTheLogo();
		sales.checkUserLogin();
		sales.checkNavigationMenu();
		project.checkFinanceModule();		
	}
	
	@Test(dependsOnMethods = "checkProject")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void clickProject() throws Exception {

		project.clickOnProjectModule();
	}
	
	@Test(dependsOnMethods = "clickProject")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void getValue() throws Exception {

		project.getValueExpenseBankAdjBefore();

	}
	
	@Test(dependsOnMethods = "getValue")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void bankAdj() throws Exception {

		sales.checkNavigationMenu();
		project.bankAdjustment();

	}
	
	@Test(dependsOnMethods = "bankAdj")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void checkProjectMod() throws Exception {
		
		sales.checkNavigationMenu();
		project.checkProjectModule();
		project.clickOnProjectModule();
	}
	
	@Test(dependsOnMethods = "checkProjectMod")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void costAllocation() throws Exception {

		project.checkCostingBankAdj();

	}
	
	@Test(dependsOnMethods = "costAllocation")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void checkProjecttt() throws Exception {
		
		sales.checkNavigationMenu();
		project.checkProjectModule();		
	}
	
	@Test(dependsOnMethods = "checkProjecttt")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void clickProjectModu() throws Exception {

		project.clickOnProjectModule();

	}
	
	@Test(dependsOnMethods = "clickProjectModu")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void bankAdjCostingSum() throws Exception {

		project.checkBankCostingSummary();

	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {

		common.totalTime(startTime);
		common.driverClose();
	}
	
}
