package projectModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProjectModule;
import bileeta.BTAF.PageObjects.ProjectModuleSmoke;
import bileeta.BTAF.PageObjects.SalesAndMarketingModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })
public class Smoke_Project_002 extends ProjectModuleSmoke {

	TestCommonMethods common = new TestCommonMethods();
	SalesAndMarketingModule sales = new SalesAndMarketingModule();
	ProjectModule project = new ProjectModule();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Project", "Project Creation", "Verify the ability of creating a new project",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Create a Project")
	public void verifyCreateProject_Smoke_Project_002() throws Exception {
		createProject_Smoke_Project_002();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
