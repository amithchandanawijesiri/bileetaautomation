package projectModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProjectModule;
import bileeta.BTAF.PageObjects.ProjectModuleBusinessScenarios;
import bileeta.BTAF.PageObjects.SalesAndMarketingModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class Project_TC_043to045 extends ProjectModuleBusinessScenarios {

	TestCommonMethods common = new TestCommonMethods();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Project", "", "Verify the ability to do cost allocation for project tasks using petty cash",
				this.getClass().getSimpleName());
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("User login and Click on navigation menu")
	public void clickNav() throws Exception {

		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		clickNavigation();
	}
	
	@Test(dependsOnMethods = "clickNav")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Click on Project menu")
	public void clickProj() throws Exception {
	
		clickProjectbutton();
	}
	
	@Test(dependsOnMethods = "clickProj")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify the ability to do cost allocation for project tasks using petty cash")
	public void VerTheAbilityToDoCostAllocationForProjectTasksUsingPettyCash() throws Exception {
	
		VerifyTheAbilityToDoCostAllocationForProjectTasksUsingPettyCash();
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {

		common.totalTime(startTime);
		common.driverClose();
	}

}
