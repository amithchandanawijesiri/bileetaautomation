package projectModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProjectModule;
import bileeta.BTAF.PageObjects.ProjectModuleSmoke;
import bileeta.BTAF.PageObjects.SalesAndMarketingModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })
public class Smoke_Project_004 extends ProjectModuleSmoke {

	TestCommonMethods common = new TestCommonMethods();
	SalesAndMarketingModule sales = new SalesAndMarketingModule();
	ProjectModule project = new ProjectModule();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Project", "Internal Order / Internal Dispatch Order", "Verify creating internal order/internal dispatch order by project action \"Create internal order\"",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"projectModule.Smoke_Project_003.verifyAddTasks1_Smoke_Project_003"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Release Internal Order and Internal Dispatch Order")
	public void verifyReleaseInternalAndInternalDispatchOrders_Smoke_Project_004() throws Exception {
		releaseInternalAndInternalDispatchOrders_Smoke_Project_004();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
