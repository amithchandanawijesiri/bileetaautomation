package projectModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProjectModule;
import bileeta.BTAF.PageObjects.ProjectModuleSmoke;
import bileeta.BTAF.PageObjects.SalesAndMarketingModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })
public class Smoke_Project_005 extends ProjectModuleSmoke {

	TestCommonMethods common = new TestCommonMethods();
	SalesAndMarketingModule sales = new SalesAndMarketingModule();
	ProjectModule project = new ProjectModule();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Project", "Daily Work Sheet", "Verify the ability to record labour hours for project via Service-->Daily Worksheet(Projects)",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"projectModule.Smoke_Project_003.verifyAddTasks1_Smoke_Project_003"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Record Work Hours")
	public void verifyReleasesDailyWorkSheet_Smoke_Project_005() throws Exception {
		releasesDailyWorkSheet_Smoke_Project_005();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
