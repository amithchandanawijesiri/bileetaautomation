package projectModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProjectModule;
import bileeta.BTAF.PageObjects.SalesAndMarketingModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class Project_TC_030 extends ProjectModule {

	TestCommonMethods common = new TestCommonMethods();
	SalesAndMarketingModule sales = new SalesAndMarketingModule();
	ProjectModule project = new ProjectModule();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Project", "", "Verify the ability to do cost allocation for project tasks using accrual voucher",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that project module is available")
	public void checkProjectModule() throws Exception {

		sales.checkNavigateToTheLoginPage();
		sales.verifyTheLogo();
		sales.checkUserLogin();
		sales.checkNavigationMenu();
		project.checkProjectModule();
		project.clickOnProjectModule();

	}

	@Test(dependsOnMethods = "checkProjectModule")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Get the existing expense value")
	public void expenseExistingValue() throws Exception {

		project.getValueExpenseBefore();
	}

	@Test(dependsOnMethods = "expenseExistingValue")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that finance module is available")
	public void checkFinanceModule() throws Exception {

		sales.checkNavigationMenu();
		project.checkFinanceModule();
		project.clickOnFinance();
	}

	@Test(dependsOnMethods = "checkFinanceModule")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify the ability to do cost allocation for project tasks using accrual voucher")
	public void costAllocation() throws Exception {

		project.costAllocation();
	}

	@Test(dependsOnMethods = "costAllocation")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that expenses recorded from the accrual voucher shows in project costing summary")
	public void checkExpenseInAccrual() throws Exception {

		sales.checkNavigationMenu();
		project.checkProjectModule();
		project.clickOnProjectModule();
		project.checkExpensesAccrual();

	}

	@Test(dependsOnMethods = "checkExpenseInAccrual")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that expenses recorded from the accrual voucher shows in project overview")
	public void checkValueInOverview() throws Exception {

		sales.checkNavigationMenu();
		project.checkProjectModule();
		project.clickOnProjectModule();
		project.checkOverViewAccrual();

	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {

		common.totalTime(startTime);
		common.driverClose();
	}
}
