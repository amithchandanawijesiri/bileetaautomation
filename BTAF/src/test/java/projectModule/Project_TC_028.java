package projectModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.AdministrationModule;
import bileeta.BTAF.PageObjects.ProcumentModule;
import bileeta.BTAF.PageObjects.ProjectModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({TestListener.class})

public class Project_TC_028 extends ProjectModule{

TestCommonMethods common = new TestCommonMethods();
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Project", "", "Verify that expenses recorded from the service job order shows in project costing summary under inter department expenses", this.getClass().getSimpleName());
	
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that expenses recorded from the service job order")
	public void VerThatExpensesRecordedFromTheServiceJobOrder() throws Exception {


		VerifyThatExpensesRecordedFromTheServiceJobOrder();
	}
	
	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		  
		common.driverClose();
		 
	}
}
