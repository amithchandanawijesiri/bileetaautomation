package projectModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProjectModule;
import bileeta.BTAF.PageObjects.SalesAndMarketingModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class Project_TC_033 extends ProjectModule {

	TestCommonMethods common = new TestCommonMethods();
	SalesAndMarketingModule sales = new SalesAndMarketingModule();
	ProjectModule project = new ProjectModule();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Project", "", "Verify the ability to do cost allocation for project tasks using customer refund",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user is able to go to navigation menu")
	public void checkProject() throws Exception {

		sales.checkNavigateToTheLoginPage();
		sales.verifyTheLogo();
		sales.checkUserLogin();
		sales.checkNavigationMenu();
		
	}

	@Test(dependsOnMethods = "checkProject")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify the ability to do cost allocation for project tasks using customer refund ")
	public void costAllocationCustomerRefund() throws Exception {

		project.customerRefund();

	}
	
	@Test(dependsOnMethods = "costAllocationCustomerRefund")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that expenses recorded from the customer refund shows in project costing summary")
	public void checkCustomerRefundCostingSummary() throws Exception {

		sales.checkNavigationMenu();
		project.checkProjectModule();
		project.clickOnProjectModule();	
		project.getValueCustomerRefund();

	}
	
	@Test(dependsOnMethods = "checkCustomerRefundCostingSummary")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void customerReundOverview() throws Exception {

		sales.checkNavigationMenu();
		project.checkProjectModule();	
		project.clickOnProjectModule();
		project.chekOverviewCustomerRefund();
		
	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {

		common.totalTime(startTime);
		common.driverClose();
	}

}
