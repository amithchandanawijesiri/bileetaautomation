package projectModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProjectModule;
import bileeta.BTAF.PageObjects.ProjectModuleSmoke;
import bileeta.BTAF.PageObjects.SalesAndMarketingModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;

@Listeners({ TestListener.class })
public class Smoke_Project_003 extends ProjectModuleSmoke {

	TestCommonMethods common = new TestCommonMethods();
	SalesAndMarketingModule sales = new SalesAndMarketingModule();
	ProjectModule project = new ProjectModule();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Project", "Add Tasks", "Verify the ability of adding task to the project",
				this.getClass().getSimpleName());
	}

	@Test(dependsOnMethods = {"projectModule.Smoke_Project_002.verifyCreateProject_Smoke_Project_002"})
	@Severity(SeverityLevel.CRITICAL)
	@Description("Add tasks to the project")
	public void verifyAddTasks1_Smoke_Project_003() throws Exception {
		addTasks1_Smoke_Project_003();
	}

	@Test(dependsOnMethods = "verifyAddTasks1_Smoke_Project_003")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Add tasks to the project")
	public void verifyAddTasks2_Smoke_Project_003() throws Exception {
		addTasks2_Smoke_Project_003();
	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {
		common.totalTime(startTime);
		common.driverClose();
	}
}
