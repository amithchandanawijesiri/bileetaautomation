package projectModule;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.AdministrationModule;
import bileeta.BTAF.PageObjects.ProcumentModule;
import bileeta.BTAF.PageObjects.ProjectModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Listeners;
import bileeta.BTAF.Utilities.TestListener;
@Listeners({TestListener.class})

public class Project_TC_008 extends ProjectModule{

	TestCommonMethods common = new TestCommonMethods();
		
	@BeforeClass
	public void setUp() throws Exception
	{
		common.setUp("Project", "", "Verify the accuracy of labour rates calculation after recording actual hours through the daily worksheet form and its reflection on costing summary/overview", this.getClass().getSimpleName());
	
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("User login and Click on navigation menu")
	public void clickNav() throws Exception {
	
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		clickNavigation();
	}
	
	@Test(dependsOnMethods = "clickNav")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Click on Project menu")
	public void clickProj() throws Exception {
	
		clickProjectbutton();
	}
	
	
	
	@Test(dependsOnMethods = "clickProj")
	@Severity(SeverityLevel.CRITICAL)
	@Description("create a project")
	public void createProj() throws Exception {
	
		createProject();
	}

	@Test(dependsOnMethods = "createProj")
	@Severity(SeverityLevel.CRITICAL)
	@Description("adding a project task")
	public void addAprojectTask() throws Exception {
	
		addingAprojectTask();
	}
	
	@Test(dependsOnMethods = "addAprojectTask")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify the accuracy of labour rates calculation")
	public void VerTheAccuracyOfLabourRatesCalculation() throws Exception {
	
		VerifyTheAccuracyOfLabourRatesCalculation();
	}

	@AfterClass(alwaysRun=true)
	public void AfterClass() throws Exception 
	{
		common.totalTime(startTime);
		  
		common.driverClose();
		 
	}
}

