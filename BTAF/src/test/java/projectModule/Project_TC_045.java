package projectModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProjectModule;
import bileeta.BTAF.PageObjects.SalesAndMarketingModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class Project_TC_045 extends ProjectModule {

	TestCommonMethods common = new TestCommonMethods();
	SalesAndMarketingModule sales = new SalesAndMarketingModule();
	ProjectModule project = new ProjectModule();

	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Project", "", "Verify the ability to do cost allocation for project tasks using accrual voucher",
				this.getClass().getSimpleName());
	}

	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void checkProject() throws Exception {

		sales.checkNavigateToTheLoginPage();
		sales.verifyTheLogo();
		sales.checkUserLogin();
		sales.checkNavigationMenu();
		project.getValueExpensePettyBefore();
	}

	@Test(dependsOnMethods = "checkProject")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void pettyCash() throws Exception {

		sales.checkNavigationMenu();
		project.pettyCash();

	}
	
	@Test(dependsOnMethods = "pettyCash")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void checkProjectPettyCash() throws Exception {

		sales.checkNavigationMenu();
		project.checkProjectModule();
		project.clickOnProjectModule();
	
	}

	@Test(dependsOnMethods = "checkProjectPettyCash")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void getValue() throws Exception {

		project.getValueExpensePettyAfter();

	}
	
	@Test(dependsOnMethods = "getValue")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void costAllocation() throws Exception {

//		sales.checkNavigationMenu();
		project.checkValuePettyCashCostingSummary();

	}
	
	@Test(dependsOnMethods = "costAllocation")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that Sales and Marketing module is available")
	public void checkProjectT() throws Exception {
		
		sales.checkNavigationMenu();
		project.checkProjectModule();	
		project.clickOnProjectModule();
	}
	
	@Test(dependsOnMethods = "checkProjectT")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that user can go to sales and marketing page")
	public void costOverviewPetty() throws Exception {

		project.checkOverviewPetty();

	}

	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {

		common.totalTime(startTime);
		common.driverClose();
	}

}
