package projectModule;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bileeta.BTAF.PageObjects.ProjectModule;
import bileeta.BTAF.PageObjects.SalesAndMarketingModule;
import bileeta.BTAF.Utilities.TestCommonMethods;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

public class Project_TC_036 extends ProjectModule {

	TestCommonMethods common = new TestCommonMethods();
	SalesAndMarketingModule sales = new SalesAndMarketingModule();
	ProjectModule project = new ProjectModule();
		
	@BeforeClass
	public void setUp() throws Exception {
		common.setUp("Project", "",
				"Verify the ability to do cost allocation for project tasks using payment voucher",
				this.getClass().getSimpleName());	
	}
	
	@Test
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify that finance module is available")
	public void checkProject() throws Exception {
		
		sales.checkNavigateToTheLoginPage();
		sales.verifyTheLogo();
		sales.checkUserLogin();
		sales.checkNavigationMenu();
		project.checkProjectModule();
		project.clickOnProjectModule();
		project.getValueExpenseBeforePaymentVoucher();
		
	}
	
	@Test(dependsOnMethods = "checkProject")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify the ability to do cost allocation for project tasks using payment voucher")
	public void allocationUsingPaymentVoucher() throws Exception {
		
		sales.checkNavigationMenu();
		project.checkProjectModule();
		project.clickOnProjectModule();
		project.paymentVoucher();

	}
	
	@Test(dependsOnMethods = "allocationUsingPaymentVoucher")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify the payment voucher")
	public void verifyThePaymentVoucher() throws Exception {

		sales.checkNavigationMenu();
		project.checkProjectModule();
		project.clickOnProjectModule();
		project.verifyPaymentVoucher();

	}
	
	@Test(dependsOnMethods = "verifyThePaymentVoucher")
	@Severity(SeverityLevel.CRITICAL)
	@Description("Verify payment voucher value in overview")
	public void costAllocation() throws Exception {

		sales.checkNavigationMenu();
		project.checkProjectModule();
		project.clickOnProjectModule();
		project.paymentVoucherInOverview();

	}
	
	@AfterClass(alwaysRun = true)
	public void AfterClass() throws Exception {

		common.totalTime(startTime);
		common.driverClose();
	
	}
	
}
