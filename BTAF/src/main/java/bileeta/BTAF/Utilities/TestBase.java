package bileeta.BTAF.Utilities;

import java.awt.Dimension;

import java.awt.Toolkit;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.FlagTerm;
import javax.script.ScriptException;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import bileeta.BTAF.PageObjects.SalesAndMarketingModuleRegression;
import bileeta.BTAF.PageObjects.SalesAndMarketingModuleSmoke;
import bileeta.BTAF.Utilities.TestListener;

import bileeta.BATF.Pages.AdministrationModuleData;
import bileeta.BATF.Pages.AdministrationModuleDataSmoke;
import bileeta.BATF.Pages.FinanceModuleData;
import bileeta.BATF.Pages.FinanceModuleDataSmoke;
import bileeta.BATF.Pages.FixedAssetData;
import bileeta.BATF.Pages.FixedAssetDataSmoke;
import bileeta.BATF.Pages.FleetManagementModuleData;
import bileeta.BATF.Pages.FleetManagementModuleSmokeData;
import bileeta.BATF.Pages.InventoryAndWarehouseModuleData;
import bileeta.BATF.Pages.InventoryAndWarehouseModuleDataSmoke;
import bileeta.BATF.Pages.ProcumentModuleData;
import bileeta.BATF.Pages.ProcumentModuleDataSmoke;
import bileeta.BATF.Pages.ProductionModuleData;
import bileeta.BATF.Pages.ProductionModuleDataSmoke;
import bileeta.BATF.Pages.ProjectModuleData;
import bileeta.BATF.Pages.ProjectModuleDataSmoke;
import bileeta.BATF.Pages.SalesAndMarketingModuleData;
import bileeta.BATF.Pages.SalesAndMarketingModuleDataSmoke;
import bileeta.BATF.Pages.ServiceModuleData;
import bileeta.BATF.Pages.ServiceModuleDataSmoke;

public class TestBase extends ReadXL {
	public static WebDriver driver;
	ReadXL xl = new ReadXL();
	private static WebDriver jsWaitDriver;
	private static WebDriverWait jsWait;
	private static JavascriptExecutor jsExec;

	public static String moduleName;
	public static String subModuleName;
	public static String testCaseName;
	public static String testId;
	public static String mailReadEmail;
	public static String mailReadPassword;
	public static String trackCode;
	public static String tcOutPut;
	String parentWinHandle;

	public static double randNumber;

	public static String browser;
	public static String OS;
	public static String siteURL;

	/** Gmail reading variables **/
	public static String bodyText = "";
	public static String subject;
	public static String suitType = "";

	public static java.util.Date startTime;
	public static long WAIT = 1000;
	public String alertText = "";
	public static String excelReadFileName = "";
	public static WriteResults wResult = new WriteResults();

	public static TestListener tls = new TestListener();

	/** variables for property file **/
	public static HashMap<String, String> getParameterConfig = new HashMap<String, String>();

	/** variables for write results files **/
	public static String writeSheetName;

	public static HashMap<String, String> getParameterFinance = new HashMap<String, String>();
	public static HashMap<String, String> getParameterFinanceSmoke = new HashMap<String, String>();
	public static HashMap<String, String> getParameterService = new HashMap<String, String>();
	public static HashMap<String, String> getParameterServiceSmoke = new HashMap<String, String>();
	public static HashMap<String, String> getParameterSalesAndMarketing = new HashMap<String, String>();
	public static HashMap<String, String> getParameterSalesAndMarketingSmoke = new HashMap<String, String>();
	public static HashMap<String, String> getParameterFixedAsset = new HashMap<String, String>();
	public static HashMap<String, String> getParameterFixedAssetSmoke = new HashMap<String, String>();
	public static HashMap<String, String> getParameterProject = new HashMap<String, String>();
	public static HashMap<String, String> getParameterProjectSmoke = new HashMap<String, String>();
	public static HashMap<String, String> getParameterInvertoryAndWarehouse = new HashMap<String, String>();
	public static HashMap<String, String> getParameterInvertoryAndWarehouseSmoke = new HashMap<String, String>();
	public static HashMap<String, String> getParameterProcurement = new HashMap<String, String>();
	public static HashMap<String, String> getParameterProcurementSmoke = new HashMap<String, String>();
	public static HashMap<String, String> getParameterAdministration = new HashMap<String, String>();
	public static HashMap<String, String> getParameterAdministrationSmoke = new HashMap<String, String>();
	public static HashMap<String, String> getParameterProduction = new HashMap<String, String>();
	public static HashMap<String, String> getParameterProductionSmoke = new HashMap<String, String>();
	public static HashMap<String, String> getParameterFleet = new HashMap<String, String>();
	public static HashMap<String, String> getParameterFleetSmoke = new HashMap<String, String>();

	public static HashMap<String, String> getDataFinance = new HashMap<String, String>();
	public static HashMap<String, String> getDataFinanceSmoke = new HashMap<String, String>();
	public static HashMap<String, String> getDataService = new HashMap<String, String>();
	public static HashMap<String, String> getDataServiceSmoke = new HashMap<String, String>();
	public static HashMap<String, String> getDataSalesAndMarketing = new HashMap<String, String>();
	public static HashMap<String, String> getDataSalesAndMarketingSmoke = new HashMap<String, String>();
	public static HashMap<String, String> getDataFixedAsset = new HashMap<String, String>();
	public static HashMap<String, String> getDataFixedAssetSmoke = new HashMap<String, String>();
	public static HashMap<String, String> getDataProject = new HashMap<String, String>();
	public static HashMap<String, String> getDataProjectSmoke = new HashMap<String, String>();
	public static HashMap<String, String> getDataInvertoryAndWarehouse = new HashMap<String, String>();
	public static HashMap<String, String> getDataInvertoryAndWarehouseSmoke = new HashMap<String, String>();
	public static HashMap<String, String> getDataProcurement = new HashMap<String, String>();
	public static HashMap<String, String> getDataProcurementSmoke = new HashMap<String, String>();
	public static HashMap<String, String> getDataAdministration = new HashMap<String, String>();
	public static HashMap<String, String> getDataAdministrationSmoke = new HashMap<String, String>();
	public static HashMap<String, String> getDataProduction = new HashMap<String, String>();
	public static HashMap<String, String> getDataProductionSmoke = new HashMap<String, String>();
	public static HashMap<String, String> getDataFleet = new HashMap<String, String>();
	public static HashMap<String, String> getDataFleetSmoke = new HashMap<String, String>();

	public void initiateBrowser() throws Exception {
		browser = findElementInXLSheet(getParameterConfig, "browser");
		siteURL = findElementInXLSheet(getParameterConfig, "site url");

		if (browser.equalsIgnoreCase("firefox")) {
			getFireFoxDriver();
		} else if (browser.equalsIgnoreCase("Chrome")) {

			getChromeDriver();
		} else {

		}

		driver.manage().window().maximize();
		Calendar cal = Calendar.getInstance();

		startTime = cal.getTime();
		openPage(siteURL);

	}

	public WebDriver getFireFoxDriver() throws Exception {
		try {
			String location = System.getProperty("user.dir");
			System.setProperty("webdriver.gecko.driver", driverFolder + "\\geckodriver.exe");
			driver = new FirefoxDriver();

		} catch (Exception e) {
			System.out.println(e);
		}
		return driver;
	}

	public WebDriver getChromeDriver() throws Exception {
		try {
			String location = System.getProperty("user.dir");
			System.setProperty("webdriver.chrome.driver", location + "\\Web_Drivers\\chromedriver.exe");

			ChromeOptions options = new ChromeOptions();
			options.addArguments("--disable-gpu");
			options.addArguments("--disable-extensions");
			options.setExperimentalOption("useAutomationExtension", false);
			options.addArguments("--proxy-server='direct://'");
			options.addArguments("--proxy-bypass-list=*");
			options.addArguments("--start-maximized");
			driver = new ChromeDriver(options);

		} catch (Exception e) {
			System.out.println(e);
		}
		return driver;
	}

	public static double getRandomNumber() {
		randNumber = (Math.random() * ((9999 - 1) + 1)) + 1;
		return randNumber;
	}

	public void waitAllRequest() throws Exception {

		Thread.sleep(600);
		/*
		 * int count =0;
		 * 
		 * do { Thread.sleep(1000); count++; } while
		 * (isDisplayedTemp("//div[@class='waitbox-container']") && count <6);
		 * 
		 */

	}

	private static void ajaxComplete() {
		jsExec.executeScript("var callback = arguments[arguments.length - 1];" + "var xhr = new XMLHttpRequest();"
				+ "xhr.open('GET', '/Ajax_call', true);" + "xhr.onreadystatechange = function() {"
				+ "  if (xhr.readyState == 4) {" + "    callback(xhr.responseText);" + "  }" + "};" + "xhr.send();");
	}

	private static void waitForJQueryLoad() {
		try {
			ExpectedCondition<Boolean> jQueryLoad = driver -> ((Long) ((JavascriptExecutor) driver)
					.executeScript("return jQuery.active") == 0);

			boolean jqueryReady = (Boolean) jsExec.executeScript("return jQuery.active==0");

			if (!jqueryReady) {
				jsWait.until(jQueryLoad);
			}
		} catch (WebDriverException ignored) {
		}
	}

	private static void waitForAngularLoad() {
		String angularReadyScript = "return angular.element(document).injector().get('$http').pendingRequests.length === 0";
		angularLoads(angularReadyScript);
	}

	private static void waitUntilJSReady() {
		try {
			ExpectedCondition<Boolean> jsLoad = driver -> ((JavascriptExecutor) driver)
					.executeScript("return document.readyState").toString().equals("complete");

			boolean jsReady = jsExec.executeScript("return document.readyState").toString().equals("complete");

			if (!jsReady) {
				jsWait.until(jsLoad);
			}
		} catch (WebDriverException ignored) {
		}
	}

	private static void waitUntilJQueryReady() {
		Boolean jQueryDefined = (Boolean) jsExec.executeScript("return typeof jQuery != 'undefined'");
		if (jQueryDefined) {
			poll(20);

			waitForJQueryLoad();

			poll(20);
		}
	}

	public void waitForAnimationToComplete(String css) {
		ExpectedCondition<Boolean> angularLoad = driver -> {
			int loadingElements = this.driver.findElements(By.cssSelector(css)).size();
			return loadingElements == 0;
		};
		jsWait.until(angularLoad);
	}

	private static void poll(long milis) {
		try {
			Thread.sleep(milis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void waitUntilAngularReady() {
		try {
			Boolean angularUnDefined = (Boolean) jsExec.executeScript("return window.angular === undefined");
			if (!angularUnDefined) {
				Boolean angularInjectorUnDefined = (Boolean) jsExec
						.executeScript("return angular.element(document).injector() === undefined");
				if (!angularInjectorUnDefined) {
					poll(20);

					waitForAngularLoad();

					poll(20);
				}
			}
		} catch (WebDriverException ignored) {
		}
	}

	public static void waitUntilAngular5Ready() {
		try {
			Object angular5Check = jsExec
					.executeScript("return getAllAngularRootElements()[0].attributes['ng-version']");
			if (angular5Check != null) {
				Boolean angularPageLoaded = (Boolean) jsExec
						.executeScript("return window.getAllAngularTestabilities().findIndex(x=>!x.isStable()) === -1");
				if (!angularPageLoaded) {
					poll(20);

					waitForAngular5Load();

					poll(20);
				}
			}
		} catch (WebDriverException ignored) {
		}
	}

	private static void waitForAngular5Load() {
		String angularReadyScript = "return window.getAllAngularTestabilities().findIndex(x=>!x.isStable()) === -1";
		angularLoads(angularReadyScript);
	}

	private static void angularLoads(String angularReadyScript) {
		try {
			ExpectedCondition<Boolean> angularLoad = driver -> Boolean
					.valueOf(((JavascriptExecutor) driver).executeScript(angularReadyScript).toString());

			boolean angularReady = Boolean.valueOf(jsExec.executeScript(angularReadyScript).toString());

			if (!angularReady) {
				jsWait.until(angularLoad);
			}
		} catch (WebDriverException ignored) {
		}
	}

	public static void waitForAngular() {
		final String javaScriptToLoadAngular = "var injector = window.angular.element('body').injector();"
				+ "var $http = injector.get('$http');" + "return ($http.pendingRequests.length === 0)";

		ExpectedCondition<Boolean> pendingHttpCallsCondition = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript(javaScriptToLoadAngular).equals(true);
			}
		};
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(pendingHttpCallsCondition);
	}

	public void readParameters() throws Exception {
		// Reading common parameters
		OS = System.getProperty("os.name").toLowerCase();
		String currentLocation = System.getProperty("user.dir");

		if (OS.contains("win")) {

			propertiesFolder = currentLocation + "\\TestProperties\\";
			dataFolder = currentLocation + "\\TestData\\";
			resultFolder = currentLocation + "\\Test_Results\\";
			screenShot = currentLocation + "\\Screenshots\\";
			mediaFolder = currentLocation + "\\Multimedia\\";
			driverFolder = currentLocation + "\\Web_Drivers\\";
		} else if (OS.contains("x")) {

			propertiesFolder = currentLocation + "/Properties/";
			dataFolder = currentLocation + "/TestData/";
			resultFolder = currentLocation + "/Test_Results/";
			screenShot = currentLocation + "/Screenshots/";
			mediaFolder = currentLocation + "/Multimedia/";
			driverFolder = currentLocation + "\\Web_Drivers\\";
		}

		// Reading element locators
		getParameterConfig = xl.readParameters("Configurations");
		getParameterFinance = xl.readParameters("Finance");
		getParameterFinanceSmoke = xl.readParameters("Finance-Smoke");
		getParameterService = xl.readParameters("Service");
		getParameterServiceSmoke = xl.readParameters("Service-Smoke");
		getParameterSalesAndMarketing = xl.readParameters("Sales & Marketing");
		getParameterSalesAndMarketingSmoke = xl.readParameters("Sales & Marketing-Smoke");
		getParameterFixedAsset = xl.readParameters("Fixed Asset");
		getParameterFixedAssetSmoke = xl.readParameters("Fixed Asset-Smoke");
		getParameterProject = xl.readParameters("Project");
		getParameterProjectSmoke = xl.readParameters("Project-Smoke");
		getParameterInvertoryAndWarehouse = xl.readParameters("Inventory and warehouse");
		getParameterInvertoryAndWarehouseSmoke = xl.readParameters("Inventory and warehouse-Smoke");
		getParameterProcurement = xl.readParameters("Procument");
		getParameterProcurementSmoke = xl.readParameters("Procument-Smoke");
		getParameterAdministration = xl.readParameters("Administration");
		getParameterAdministrationSmoke = xl.readParameters("Administration Smoke");
		getParameterProduction = xl.readParameters("Production");
		getParameterProductionSmoke = xl.readParameters("Production-Smoke");
		getParameterFleet = xl.readParameters("Fleet");
		getParameterFleetSmoke = xl.readParameters("Fleet-Smoke");

		// Reading data
		getDataFinance = xl.readData("Finance");
		getDataFinanceSmoke = xl.readData("Finance-Smoke");
		getDataService = xl.readData("Service");
		getDataServiceSmoke = xl.readData("Service-Smoke");
		getDataSalesAndMarketing = xl.readData("Sales & Marketing");
		getDataSalesAndMarketingSmoke = xl.readData("Sales & Marketing-Smoke");
		getDataFixedAsset = xl.readData("Fixed Asset");
		getDataFixedAssetSmoke = xl.readData("Fixed Asset-Smoke");
		getDataProject = xl.readData("Project");
		getDataProjectSmoke = xl.readData("Project-Smoke");
		getDataInvertoryAndWarehouse = xl.readData("Inventory and warehouse");
		getDataInvertoryAndWarehouseSmoke = xl.readData("Inventory and warehouse-Smoke");
		getDataProcurement = xl.readData("Procument");
		getDataProcurementSmoke = xl.readData("Procument-Smoke");
		getDataAdministration = xl.readData("Administration");
		getDataAdministrationSmoke = xl.readData("Administration Smoke");
		getDataProduction = xl.readData("Production");
		getDataProductionSmoke = xl.readData("Production-Smoke");
		getDataFleet = xl.readData("Fleet");
		getDataFleetSmoke = xl.readData("Fleet-Smoke");

		// calling to the main function
		AdministrationModuleData.readData();
		AdministrationModuleData.readElementlocators();
		AdministrationModuleDataSmoke.readData();
		AdministrationModuleDataSmoke.readElementlocators();
		FinanceModuleData.readData();
		FinanceModuleData.readElementlocators();
		FinanceModuleDataSmoke.readData();
		FinanceModuleDataSmoke.readElementlocators();
		FixedAssetData.readData();
		FixedAssetData.readElementlocators();
		FixedAssetDataSmoke.readData();
		FixedAssetDataSmoke.readElementlocators();
		ProcumentModuleData.readData();
		ProcumentModuleData.readElementlocators();
		ProcumentModuleDataSmoke.readData();
		ProcumentModuleDataSmoke.readElementlocators();
		ProjectModuleData.readData();
		ProjectModuleData.readElementlocators();
		ProjectModuleDataSmoke.readData();
		ProjectModuleDataSmoke.readElementlocators();
		ProcumentModuleDataSmoke.readElementlocators();
		SalesAndMarketingModuleData.readData();
		SalesAndMarketingModuleData.readElementlocators();
		SalesAndMarketingModuleDataSmoke.readData();
		SalesAndMarketingModuleDataSmoke.readElementlocators();
		ServiceModuleData.readData();
		ServiceModuleData.readElementlocators();
		ServiceModuleDataSmoke.readData();
		ServiceModuleDataSmoke.readElementlocators();
		ProductionModuleData.readData();
		ProductionModuleData.readElementlocators();
		ProductionModuleDataSmoke.readData();
		ProductionModuleDataSmoke.readElementlocators();
		InventoryAndWarehouseModuleData.readData();
		InventoryAndWarehouseModuleData.readElementlocators();
		InventoryAndWarehouseModuleDataSmoke.readData();
		InventoryAndWarehouseModuleDataSmoke.readElementlocators();
		FleetManagementModuleData.readData();
		FleetManagementModuleData.readElementlocators();
		FleetManagementModuleSmokeData.readData();
		FleetManagementModuleSmokeData.readElementlocators();
	}

	public static String findElementInXLSheet(HashMap<String, String> mapName, String parameterName) throws Exception {
		String parameterValue = "";

		if (mapName.containsKey(parameterName.trim())) {
			parameterValue = mapName.get(parameterName).trim();
		}
		return parameterValue;
	}

	public void resultSheetName(String resultSheet) throws Exception {
		writeSheetName = resultSheet;
	}

	public void writeTestResults(String testCase, String expectedResults, String actualResults, String stat)
			throws Exception {
		boolean status = false;
		boolean isAssert;

		if (stat.equalsIgnoreCase("pass")) {

			isAssert = false;
			status = true;
		} else {
			isAssert = true;
			status = false;
		}

		String screenshotpath = System.getProperty("user.dir");

		if (!testCase.isEmpty()) {
			writeTestResultsToExcel(testCase, expectedResults, actualResults, status);
		}

		if (!status) {
			String logMsg = "";
			String failTc = "";
			String failTc1 = "";
			logMsg = "************************************\r\n" + " " + writeSheetName + " - " + testCase
					+ "\r\n Expected Results:" + expectedResults + "\r\n Actual Results:" + actualResults
					+ "\r\n************************************";

			// failTc = writeFileName+"-"+testCase;
			failTc = "( " + writeSheetName + " ) - " + testCase;
			System.out.println(wResult.readTime);
			System.out.println(writeSheetName);
			System.out.println("fail TC - " + failTc);

			if (failTc.length() > 100) {
				failTc1 = failTc.substring(0, 100).replaceAll("([^a-zA-Z]|\\s)+", "_");

			} else {
				failTc1 = failTc.replaceAll("([^a-zA-Z]|\\s)+", "_");
			}

			failTc = "";
		}

		if (isAssert) {
			Assert.assertEquals(actualResults, expectedResults);
			tls.saveScreenshotPNG(driver);
		}

	}

	public static void writeTestResultsToExcel(String testCase, String expectedResults, String actualResults,
			boolean status) throws Exception {

		wResult.writeTestResult(testCase, expectedResults, actualResults, status, writeSheetName, moduleName,
				subModuleName);

		if (status) {
			System.out.println(actualResults);
		}
	}

	public By getLocator(String locatorValue) throws Exception {
		By findLocator = null;

		if (locatorValue.indexOf("_Id") > -1) {
			findLocator = By.id(locatorValue.replaceAll("_Id", "").trim());
		} else if (locatorValue.indexOf("_Name") > -1 || locatorValue.indexOf("_name") > -1) {
			findLocator = By.name(locatorValue.replaceAll("_Name", "").replaceAll("_name", "").trim());
		} else if (locatorValue.indexOf("_ClassName") > -1 || locatorValue.indexOf("_className") > -1) {
			findLocator = By.className(locatorValue.replaceAll("_ClassName", "").replaceAll("_className", "").trim());
		} else if (locatorValue.indexOf("_Link") > -1) {
			findLocator = By.linkText(locatorValue.replaceAll("_Link", "").trim());
		} else if (locatorValue.indexOf("_link") > -1) {
			findLocator = By.linkText(locatorValue.replaceAll("_link", "").trim());
		} else if (locatorValue.indexOf("_Css") > -1 || locatorValue.indexOf("_css") > -1) {
			findLocator = By.cssSelector(locatorValue.replaceAll("_Css", "").replaceAll("_css", "").trim());
		} else if (locatorValue.indexOf("_TagName") > -1 || locatorValue.indexOf("_tagName") > -1) {
			findLocator = By.tagName(locatorValue.replaceAll("_TagName", "").replaceAll("_tagName", "").trim());
		} else if (locatorValue.indexOf("_Xpath") > -1 || locatorValue.indexOf("_") > -1) {
			findLocator = By.xpath(locatorValue.replaceAll("_Xpath", "").replaceAll("_xpath", "").trim());
		} else {
			findLocator = By.xpath(locatorValue.trim());
		}
		return findLocator;
	}

	public static void errorLogs(String message) throws IOException {
		try {
			File log = new File(logFolder + "/Error log-" + wResult.readTime + ".txt");

			if (log.exists() == false) {
				log.createNewFile();
			}
			PrintWriter out = new PrintWriter(new FileWriter(log, true));
			out.append(message + "\r\n");
			out.close();
			System.out.println(message);
		} catch (IOException e) {
			// System.out.println("COULD NOT LOG!!"+e.getMessage());
		}
	}

	public void openPage(String url) throws Exception {
		try {
			driver.get(url);
		} catch (Exception e) {
			throw new AssertionError("Login page is not displayed.");
		}
	}

	public void switchToFrame(String frameId) throws Exception {
		try {
			driver.switchTo().frame(driver.findElement(getLocator(frameId)));
		} catch (Exception e) {
			errorLogs("Exception" + e.getMessage());
		}
	}

	public int getCount(String path) throws Exception {
		int count = 0;

		try {
			List<WebElement> optionCount = driver.findElements(By.xpath(path));
			count = optionCount.size();
			Thread.sleep(200);
		} catch (Exception e) {
			System.out.println(e);
		}
		return count;
	}

	public void pressEnter(String locator) throws Exception {
		try {
			driver.findElement(getLocator(locator)).sendKeys(Keys.ENTER);

		}

		catch (Exception e) {
			errorLogs("Exception" + e.getMessage());
		}
	}

	public void clearCookies() throws Exception {
		driver.manage().deleteAllCookies();
	}

	public void pageScrollDown() throws Exception {
		JavascriptExecutor jsx = (JavascriptExecutor) driver;
		jsx.executeScript("window.scrollBy(0,600)", "");
	}

	public void pageScrollUp() throws Exception {
		JavascriptExecutor jsx = (JavascriptExecutor) driver;
		jsx.executeScript("window.scrollBy(0,100)", "");
	}

	public void pageScrollUpToTop() throws Exception {
		JavascriptExecutor jsx = (JavascriptExecutor) driver;
		jsx.executeScript("window.scrollBy(0,-600)", "");
	}

	public void pageScrollUpToBottom() throws Exception {
		JavascriptExecutor jsx = (JavascriptExecutor) driver;
		jsx.executeScript("window.scrollBy(0,600)", "");
	}

	public boolean isElementPresent(String locator) throws Exception {

		boolean found = false;
		int tries = 0;

		if (!locator.isEmpty()) {
			while (!found && tries < 2) {
				tries += 1;
				try {
					driver.findElement(getLocator(locator));
					found = true; // FOUND IT
					break;

				} catch (Exception e) {
					found = false;

					errorLogs("Locator " + locator + " is not found");
				}
			}

			if (!found) {
				System.out.println("Locator " + locator + " is not found");
			}
		}
		return found;
	}

	public boolean isErrorElementPresent(String locator) throws Exception {
		boolean found = false;
		int tries = 0;

		while (!found && tries < 2) {
			tries += 1;
			try {
				driver.findElement(getLocator(locator));
				found = true; // FOUND IT
				break;

			} catch (Exception e) {
				found = false;
				errorLogs("Locator " + locator + " Element is not present");
			}
		}
		return found;
	}

	public static boolean pendingApproval() throws InterruptedException {
		mailReadEmail = "bileeta.automation@gmail.com";
		mailReadPassword = "Bileeta123";
		boolean isMailFound = false;
		Properties props = System.getProperties();

		final String username = mailReadEmail;
		final String password = mailReadPassword;
		String host = "smtp.gmail.com";
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", "587");
		// Get the Session object.
		try {
			Session session = Session.getInstance(props, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			});
			Store store = session.getStore("imaps");
			store.connect("imap.gmail.com", username, password);

			Folder inbox = store.getFolder("Inbox");
			inbox.open(Folder.READ_WRITE);
			FlagTerm ft = new FlagTerm(new Flags(Flags.Flag.SEEN), false);
			Message messages[] = inbox.search(ft);
			System.out.println("No of new emails " + messages.length);
			for (Message message : messages) {

				subject = message.getContent().toString();

				String content = message.getContentType();

				if (subject.contains("Please Approve the Case No")) {

					isMailFound = true;
					break;
				}

			}

		} catch (NoSuchProviderException e) {
			e.printStackTrace();

		} catch (MessagingException e) {
			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}

		return isMailFound;
	}

	public boolean isDisplayed(String locator) throws Exception {

		boolean found = false;
		int tries = 0;

		while (!found && tries < 5) {
			tries += 1;
			try {
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				if (driver.findElement(getLocator(locator)).isDisplayed()) {
					found = true; // FOUND IT
					break;
				} else {
					Thread.sleep(200);

				}
			} catch (Exception e) {
				found = false;
			}
		}

		if (!found) {
			errorLogs("Locator " + locator + " Element is not displyed");
		}
		return found;
	}

	public boolean isDisplayedTemp(String locator) throws Exception {

		boolean found = false;
		int tries = 0;

		while (!found && tries < 5) {
			tries += 1;
			try {
				if (driver.findElement(getLocator(locator)).isDisplayed()) {
					found = true; // FOUND IT
					break;
				} else {
					Thread.sleep(200);

				}
			} catch (Exception e) {
				found = false;
			}
		}

		if (!found) {
			errorLogs("Locator " + locator + " Element is not displyed");
		}
		return found;
	}

	public boolean isSelected(String locator) throws Exception {
		boolean found = false;
		int tries = 0;

		while (!found && tries < 2) {
			tries += 1;
			try {
				if (driver.findElement(getLocator(locator)).isSelected()) {
					found = true; // FOUND IT
					break;
				} else {
					Thread.sleep(200);
				}

			} catch (Exception e) {
				found = false;
			}
		}
		return found;
	}

	public boolean isEnabled(String locator) throws Exception {
		boolean found = false;
		int tries = 0;

		while (!found && tries < 2) {
			tries += 1;
			try {
				if (driver.findElement(getLocator(locator)).isEnabled()) {
					found = true; // FOUND IT
					break;
				} else {
					Thread.sleep(WAIT);
				}
			} catch (Exception e) {
				found = false;
				errorLogs("Locator " + locator + " Element is not present");
			}
		}
		return found;
	}

	public int getRowCount(String locator, String tagName) throws Exception {
		int noOfRows = 0;

		try {
			noOfRows = driver.findElement(getLocator(locator)).findElements(By.tagName(tagName)).size();
		} catch (Exception e) {
			errorLogs("Exception" + e.getMessage());
		}
		return noOfRows;
	}

	public void pageRefersh() throws Exception {
		driver.navigate().refresh();

	}

	public boolean isAlertPresent() throws IOException {
		try {
			driver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException e) {
			errorLogs("Exception" + e.getMessage());
			return false;
		}
	}

	public String getAlert() throws Exception {
		boolean acceptNextAlert = true;

		try {
			Alert alert = driver.switchTo().alert();
			alertText = driver.switchTo().alert().getText();

			if (acceptNextAlert) {
				alert.accept();
			} else {
				alert.dismiss();
			}
		} catch (Exception e) {
			errorLogs("Exception" + e.getMessage());
		}

		return alertText;
	}

	public String closeAlertAndGetItsText() {
		boolean acceptNextAlert = true;
		try {
			Alert alert = driver.switchTo().alert();
			String alertText = alert.getText();
			if (acceptNextAlert) {
				alert.accept();
			} else {
				alert.dismiss();
			}
			return alertText;
		} finally {
			acceptNextAlert = true;
		}
	}

	public void acceptAlert() throws Exception {
		try {
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			errorLogs("Exception" + e.getMessage());
		}
	}

	public void switchToFrameId(String frameId) throws Exception {
		try {
			driver.switchTo().frame(driver.findElement(getLocator(frameId)));
		} catch (Exception e) {
			errorLogs("Exception" + e.getMessage());
		}
	}

	public void switchToNextFrame(int id) throws Exception {
		try {
			driver.switchTo().frame(id);
		} catch (Exception e) {
			errorLogs("Exception" + e.getMessage());
		}
	}

	public void backToDefaultPage() throws Exception {
		try {
			driver.switchTo().defaultContent();
		} catch (Exception e) {
			errorLogs("Exception" + e.getMessage());
		}
	}

	public boolean sendKeys(String locator, String value) throws Exception {
		boolean sendKey = true;
		try {
			driver.findElement(getLocator(locator)).clear();
			driver.findElement(getLocator(locator)).click();
			driver.findElement(getLocator(locator)).sendKeys(value);
		} catch (Exception e) {
			errorLogs("Exception" + e.getMessage());
			sendKey = false;
		}
		return sendKey;
	}

	public void switchWindow() throws InterruptedException {
		parentWinHandle = driver.getWindowHandle();
		Set<String> winHandles = driver.getWindowHandles();
		// Loop through all handles
		for (String handle : winHandles) {
			if (!handle.equals(parentWinHandle)) {
				driver.switchTo().window(handle);

			}
		}
	}

	public void closeWindow() throws InterruptedException {
		Thread.sleep(1000);
		driver.close();
		driver.switchTo().window(parentWinHandle);

	}

	public void waitImplicitBase(String element, int seconds) throws Exception {
		driver.manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);
		if (!isDisplayed(element)) {
			System.out.println("Network speed is low...");
		}

	}

	public boolean click(String locator) throws Exception {
		// waitImplicitBase(locator,10);
		// waitAllRequest();
		boolean clickLink = true;
		try {
			driver.findElement(getLocator(locator)).click();
			Thread.sleep(WAIT * 1);

		} catch (Exception e) {
			clickLink = false;
		}
		return clickLink;
	}

	public boolean keyEnter(String locator) throws Exception {
		boolean keyEnterkey = true;
		try {
			driver.findElement(getLocator(locator)).sendKeys(Keys.RETURN);
			Thread.sleep(WAIT * 2);
		} catch (Exception e) {
			keyEnterkey = false;
		}
		return keyEnterkey;
	}

	public boolean RightClickOnDropDown(String locator) {

		boolean dropDown = true;
		try {
			Actions builder = new Actions(driver);

			builder.click(driver.findElement(By.xpath(locator))).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN)
					.sendKeys(Keys.ENTER).perform();

		} catch (Exception e) {

			dropDown = false;
		}

		return dropDown;
	}

	public boolean selectText(String locator, String selectText) throws Exception {
		boolean flag = true;
		try {
			new Select(driver.findElement(getLocator(locator))).selectByVisibleText(selectText);
		} catch (Exception e) {
			errorLogs("Exception" + e.getMessage());
			flag = false;
		}
		return flag;
	}

	public boolean selectTextByContains(String locator, String selectText) throws Exception {
		boolean flag = true;
		try {
			List<WebElement> options = driver.findElements(getLocator(locator + "/option"));

			for (WebElement option : options) {
				if (option.getText().contains(selectText)) {
					option.click();
					break;
				}
			}
		} catch (Exception e) {
			System.out.println();
			 errorLogs("Exception"+e.getMessage());
			 flag = false;
		}
		return flag;
	}

	public void selectIndex(String locator, int index) throws Exception {
		try {
			new Select(driver.findElement(getLocator(locator))).selectByIndex(index);
		} catch (Exception e) {
			errorLogs("Exception" + e.getMessage());
		}
	}

	public int selectedValueInDropdown(String locator) throws Exception {
		int currentValue = 0;
		try {
			if (isElementPresent(locator)) {
				currentValue = Integer.parseInt(
						new Select(driver.findElement(getLocator(locator))).getFirstSelectedOption().getText());
			} else {
				currentValue = 0;
			}
		} catch (Exception e) {
			currentValue = 0;
			errorLogs("Exception" + e.getMessage());
		}
		return currentValue;
	}

	public String getSelectedOptionInDropdown(String locator) throws Exception {
		String currentOption = "";
		try {
			Select drodown = new Select(driver.findElement(By.xpath(locator)));
			currentOption = drodown.getFirstSelectedOption().getText();
		} catch (Exception e) {
			errorLogs("Exception" + e.getMessage());
		}
		return currentOption;
	}

	public String getText(String locator) throws Exception {

		String textValue = "";
		try {
			textValue = driver.findElement(getLocator(locator)).getText().trim();
		} catch (Exception e) {
			errorLogs("Exception" + e.getMessage());
		}
		return textValue;
	}

	public String getAttribute(String locator, String attributeType) throws Exception {

		String attributeValue = "";
		try {
			if (isElementPresent(locator)) {
				attributeValue = driver.findElement(getLocator(locator)).getAttribute(attributeType).trim();
			} else {
				attributeValue = "";
			}
		} catch (Exception e) {
			errorLogs("Exception" + e.getMessage());
		}
		return attributeValue;
	}

	public String getTitle() throws Exception {
		String title = "";
		try {
			title = driver.getTitle().trim();
		} catch (Exception e) {
			errorLogs("Exception" + e.getMessage());
		}
		return title;

	}

	public String getCurrentUrl() throws Exception {
		String currentUrl = "";
		try {
			currentUrl = driver.getCurrentUrl();
		} catch (Exception e) {
			errorLogs("Exception" + e.getMessage());
		}
		return currentUrl;
	}

	public void goBack() throws Exception {
		driver.navigate().back();
	}

	public boolean getPageSource(String searchText) throws Exception {
		boolean pageSourceValue = true;
		try {
			if (driver.getPageSource().indexOf(searchText) >= 0) {
				pageSourceValue = true;
			} else {
				pageSourceValue = false;
			}
		} catch (Exception e) {
			pageSourceValue = false;
			errorLogs("Exception" + e.getMessage());
		}
		return pageSourceValue;
	}

	public String getPageContent() throws Exception {
		String pageSourceValue = "";
		try {
			if (driver.getPageSource() != null) {
				pageSourceValue = driver.getPageSource().trim();
			}
		} catch (Exception e) {
			pageSourceValue = "No Page source";
			errorLogs("No Page source");
		}

		return pageSourceValue;
	}

	public int getTagListSize(String locator, String tag) throws Exception {
		int size = 0;

		try {
			size = driver.findElement(getLocator(locator)).findElements(getLocator(tag)).size();
		} catch (Exception e) {
			errorLogs("Exception" + e.getMessage());
		}
		return size;
	}

	public void getTagList(String locator, String tag) throws Exception {
		try {
			driver.findElement(getLocator(locator)).findElements(getLocator(tag));
		} catch (Exception e) {
			errorLogs("Exception" + e.getMessage());
		}
	}

	public int getElementSize(String element) throws Exception {
		int size = 0;
		try {
			size = driver.findElements(getLocator(element)).size();
		} catch (Exception e) {
			errorLogs("Exception" + e.getMessage());
		}
		return size;
	}

	public int getDropdownSize(String element, String tagName) throws Exception {
		int size = 0;
		try {
			size = driver.findElement(getLocator(element)).findElements(By.tagName(tagName)).size();
		} catch (Exception e) {
			errorLogs("Exception" + e.getMessage());
		}
		return size;
	}

	public String getSelectedValueFromDropdown(String element, String tagName, int optionValue) throws Exception {
		String dropdownValue = "";
		try {
			dropdownValue = driver.findElement(getLocator(element)).findElements(By.tagName(tagName)).get(optionValue)
					.getText();
		} catch (Exception e) {
			errorLogs("Exception" + e.getMessage());
		}
		return dropdownValue;
	}

	public void mouseMove(String mouseMoveElement) throws Exception {
		if (isElementPresent(mouseMoveElement)) {
			Actions actions = new Actions(driver);
			WebElement menuHoverLink = driver.findElement(getLocator(mouseMoveElement));
			actions.moveToElement(menuHoverLink).build().perform();
			Thread.sleep(1000);
		}
	}

	public void ExecuteJS(String PathKey) {

		try {

			JavascriptExecutor js;
			if (driver instanceof JavascriptExecutor) {
				((JavascriptExecutor) driver).executeScript(PathKey);

			} else {

			}

		} catch (Exception e) {

			System.out.println(e);

		}

	}

	public void changeCSS(String locator, String css) {
		try {
			driver.findElement(getLocator(locator)).click();
			Thread.sleep(WAIT * 1);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			WebElement element = driver.findElement(getLocator(locator));
			js.executeScript("arguments[0].setAttribute('style', '" + css + "')", element);

		} catch (Exception e) {

		}

	}

	public static boolean IsPasswordResetSent() throws InterruptedException {
		mailReadEmail = "bileeta.automation@gmail.com";
		mailReadPassword = "Bileeta123";
		boolean isMailFound = false;
		Properties props = System.getProperties();

		final String username = mailReadEmail;
		final String password = mailReadPassword;
		String host = "smtp.gmail.com";
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", "587");
		// Get the Session object.
		try {
			Session session = Session.getInstance(props, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			});
			Store store = session.getStore("imaps");
			store.connect("imap.gmail.com", username, password);

			Folder inbox = store.getFolder("Inbox");
			inbox.open(Folder.READ_WRITE);
			FlagTerm ft = new FlagTerm(new Flags(Flags.Flag.SEEN), false);
			Message messages[] = inbox.search(ft);
			System.out.println("No of new emails " + messages.length);
			for (Message message : messages) {

				subject = message.getContent().toString();

				String content = message.getContentType();

				if (subject.contains("Entution received a request to reset the password for your account")) {

					isMailFound = true;
					break;
				}

			}

		} catch (NoSuchProviderException e) {
			e.printStackTrace();

		} catch (MessagingException e) {
			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}

		return isMailFound;
	}

	public static boolean IsMailRead() throws InterruptedException {
		mailReadEmail = "bileeta.automation@gmail.com";
		mailReadPassword = "Bileeta123";
		boolean isMailFound = false;
		Properties props = System.getProperties();

		final String username = mailReadEmail;
		final String password = mailReadPassword;
		String host = "smtp.gmail.com";
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", "587");
		// Get the Session object.
		try {
			Session session = Session.getInstance(props, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			});
			Store store = session.getStore("imaps");
			store.connect("imap.gmail.com", username, password);

			Folder inbox = store.getFolder("Inbox");
			inbox.open(Folder.READ_WRITE);
			FlagTerm ft = new FlagTerm(new Flags(Flags.Flag.SEEN), false);
			Message messages[] = inbox.search(ft);
			System.out.println("No of new emails " + messages.length);
			for (Message message : messages) {

				subject = message.getContent().toString();

				String content = message.getContentType();

				if (subject.contains("Your account has been created successfully.")) {

					isMailFound = true;
					break;
				}

			}

		} catch (NoSuchProviderException e) {
			e.printStackTrace();

		} catch (MessagingException e) {
			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}

		return isMailFound;
	}

	public static void mailRead(String subjectName, String custemerName, String email, String pw)
			throws InterruptedException {
		boolean isMailFound = false;
		Properties props = System.getProperties();

		final String username = email;
		final String password = pw;
		String host = "smtp.gmail.com";
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", "587");
		// Get the Session object.
		try {
			Session session = Session.getInstance(props, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			});
			Store store = session.getStore("imaps");
			store.connect("imap.gmail.com", email, pw);

			Folder inbox = store.getFolder("Inbox");
			inbox.open(Folder.READ_WRITE);
			FlagTerm ft = new FlagTerm(new Flags(Flags.Flag.SEEN), false);
			Message messages[] = inbox.search(ft);
			System.out.println("total " + messages.length);
			if (messages.length == 0) {
				try {
					mailBoxAgainRead(subjectName, custemerName, email, pw);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} else {
				for (Message message : messages) {

					try {
						Object content = message.getContent();
						if (content instanceof Multipart) {
							StringBuffer messageContent = new StringBuffer();
							Multipart multipart = (Multipart) content;
							for (int i = 0; i < multipart.getCount(); i++) {
								Part part = multipart.getBodyPart(i);
								if (part.isMimeType("text/plain")) {
									messageContent.append(part.getContent().toString());
								}
							}
							System.out.println(messageContent.toString());

						}

					} catch (IOException e) {
						e.printStackTrace();
					}

					subject = message.getSubject();

					String content = message.getContentType();
					if (message.getContent() instanceof Multipart) {
						Multipart part = (Multipart) message.getContent();
						BodyPart bodyPart = part.getBodyPart(0);
						part.getContentType();
						part.getCount();
						((MimeMultipart) part).getPreamble();
					}
					try {
						if (subject.indexOf(subjectName) > -1) {
							System.out.println("has subject");
							printParts(message, custemerName);
							isMailFound = true;
							break;
						} else {
							isMailFound = false;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

					Flags flags = message.getFlags();
					message.setFlag(Flags.Flag.SEEN, true);
				}
				if (!isMailFound) {
					mailBoxAgainRead(subjectName, custemerName, email, pw);
				}
			}
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
			mailBoxAgainRead(subjectName, custemerName, email, pw);
			// System.exit(1);
		} catch (MessagingException e) {
			e.printStackTrace();

			mailBoxAgainRead(subjectName, custemerName, email, pw);
			// System.exit(2);
		} catch (IOException e) {

			e.printStackTrace();
			mailBoxAgainRead(subjectName, custemerName, email, pw);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void mailBoxAgainRead(String subjectName, String custemerName, String email, String pw)
			throws InterruptedException {
		Properties props = System.getProperties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "993");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "993");
		props.setProperty("mail.store.protocol", "imaps");

		try {
			Thread.sleep(10000);

			Session session = Session.getInstance(props, null);
			session.setDebug(true);
			Store store = session.getStore("imaps");
			store.connect("imap.gmail.com", email, "19881106");
			Folder inbox = store.getFolder("Inbox");
			inbox.open(Folder.READ_WRITE);
			FlagTerm ft = new FlagTerm(new Flags(Flags.Flag.SEEN), false);
			Message messages[] = inbox.search(ft);
			for (Message message : messages) {
				subject = message.getSubject();
				// String content = message.getContentType();
				if (message.getContent() instanceof Multipart) {
					Multipart part = (Multipart) message.getContent();
					// BodyPart bodyPart = part.getBodyPart(0);
					part.getContentType();
					part.getCount();
					((MimeMultipart) part).getPreamble();
				}
				try {
					if (subject.indexOf(subjectName) > -1) {
						System.out.println("has subject");
						printParts(message, custemerName);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

				// Flags flags = message.getFlags();
				message.setFlag(Flags.Flag.SEEN, true);
			}

		} catch (NoSuchProviderException e) {
			e.printStackTrace();
			// System.exit(1);
		} catch (MessagingException e) {
			e.printStackTrace();
			// System.exit(2);
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public static void printParts(Part p, String custemerName) throws Exception {
		Object obj = p.getContent();
		if (obj instanceof String) {
			if (((String) obj).toLowerCase().indexOf(custemerName.toLowerCase()) > -1) {
				bodyText = (String) obj;
			} else {
				bodyText = "";
			}
		} else if (obj instanceof Multipart) {
			Multipart mp = (Multipart) obj;
			int count = mp.getCount();
			for (int i = 0; i < count; i++) {
				printParts(mp.getBodyPart(i), custemerName);
			}
		} else if (obj instanceof InputStream) {
			@SuppressWarnings("unused")
			InputStream is = (InputStream) obj;
			@SuppressWarnings("unused")
			int c;
		}
	}

	public boolean doubleClick(String locator) throws Exception {

		Thread.sleep(500);

		boolean clickLink = true;
		try {

			Actions actions = new Actions(driver);
			WebElement elementLocator = driver.findElement(getLocator(locator));
			actions.doubleClick(elementLocator).perform();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(getLocator(locator)));
			Thread.sleep(500);
		} catch (Exception e) {
			clickLink = false;
		}
		return clickLink;
	}

	public void explicitWait(String locator, int seconds) throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, seconds);
			wait.until(ExpectedConditions.visibilityOfElementLocated(getLocator(locator)));
		} catch (Exception e) {
		}
	}

	public void waitUntillNotVisible(String locator, int seconds) throws Exception {
		boolean flagVisible = false;
		for (int i = 1; i <= seconds; i++) {
			if (isDisplayedQuickCheck(locator)) {
				flagVisible = false;
			} else {
				flagVisible = true;
			}
			if (flagVisible)
				break;
			Thread.sleep(900);
		}
	}

	public void explicitWaitUntillClickable(String locator, int seconds) throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, seconds);
			wait.until(ExpectedConditions.elementToBeClickable(getLocator(locator)));
		} catch (Exception e) {
		}
	}
	
	public void explicitWaitUntillPresent(String locator, int seconds) throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, seconds);
			wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(getLocator(locator)));
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public boolean isDisplayedQuickCheck(String locator) throws Exception {
		boolean found = false;

		try {
			if (driver.findElement(getLocator(locator)).isDisplayed()) {
				found = true; // FOUND IT
			}
		} catch (Exception e) {
		}
		return found;
	}

	public void sendKeysLookup(String locator, String value) throws InterruptedException {
		boolean set_txt;
		int tries = 0;
		do {
			try {
				set_txt = true;
				driver.findElement(getLocator(locator)).clear();
				driver.findElement(getLocator(locator)).click();
				driver.findElement(getLocator(locator)).sendKeys(value);
			} catch (Exception e) {
				set_txt = false;
				Thread.sleep(1000);
				tries++;
			}
			if (tries > 15) {
				break;
			}
		} while (!set_txt);
	}

	public boolean isEnabledQuickCheck(String locator) throws Exception {
		boolean found = false;
		try {
			if (driver.findElement(getLocator(locator)).isEnabled()) {
				found = true; // FOUND IT
			}
		} catch (Exception e) {

		}
		return found;
	}

}
