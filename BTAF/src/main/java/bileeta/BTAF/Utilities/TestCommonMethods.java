package bileeta.BTAF.Utilities;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

public class TestCommonMethods extends TestBase{
	
	WriteResults write = new WriteResults();
	
	public void setUp(String modulename,String submodulename,String testcasename,String testid) throws Exception
	{
		
		trackCode="";
		moduleName=modulename;
		
		subModuleName=submodulename;
		
		testCaseName=testcasename;
		
		wResult.createFile("Automation-Results");
		
		testId=testid;
		
		resultSheetName(testid);

		initiateBrowser();

	}
	
	public String totalTime(Date startTime,Date endTime) throws Exception
	{		
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS");
		SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm:ss.SSS");
    	 	
    	SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss.SSS");
    	
    	java.util.Date date1 = format.parse(sdf.format(startTime));
    	java.util.Date date2 = format.parse(sdf1.format(endTime));
    	
    	long diff = date2.getTime() - date1.getTime();
    	
	    long diffMiliSeconds = diff % 1000; 
	    long diffSeconds = diff / 1000 % 60;  
	    long diffMinutes = diff / (60 * 1000) % 60;       
	    long diffHours = diff / (60 * 60 * 1000);  
	    
	    String totalTime =diffSeconds+"."+diffMiliSeconds;
	    
	    if(diffHours!=0)
	    {
	    	totalTime =  diffHours+" hr " +diffMinutes+" min "+ diffSeconds+ " sec";
	    }
	    else
	    {
	    	totalTime =  diffMinutes+" min "+ diffSeconds+ " sec";
	    }   
	    return totalTime;		
	}

	public void driverClose()
	{
		try {
	        Set<String> windows = driver.getWindowHandles();
	        Iterator<String> iter = windows.iterator();
	        String[] winNames=new String[windows.size()];
	        int i=0;
	        while (iter.hasNext()) {
	            winNames[i]=iter.next();
	            i++;
	        }

	        if(winNames.length > 1) {
	            for(i = winNames.length; i > 1; i--) {
	            	driver.switchTo().window(winNames[i - 1]);
	            	driver.close();
	            }
	        }
	        driver.switchTo().window(winNames[0]);
	        driver.close();
	    }
	    catch(Exception e){         
	        e.printStackTrace();
	    } 
	}
	
	public void driverQuit()
	{
		try{
		    driver.quit();
		   }catch (Exception e){
		      System.out.println(e);
		      }  
	}
	public void totalTime(java.util.Date startTime) throws Exception
	{		
		Thread.sleep(3000);
		java.util.Date endTime;		
		Calendar cal = Calendar.getInstance(); 		
		endTime = cal.getTime();
		String totalTime = totalTime(startTime, endTime);
		writeTestResults("Total time to execute ", "Total time =",totalTime,"pass");
	}	

	public void finalyze() throws Exception
	{
		write.writeTestSummaryResult(moduleName,subModuleName,testCaseName,write.totalTC,write.passTC,write.failTC,write.skipTC,write.totalTime);
    	
		write.totalTC = 0;
		write.passTC = 0;
		write.failTC = 0;
		write.skipTC = 0;
		write.totalTime ="";
	}

}
