package bileeta.BTAF.Utilities;

import java.io.*;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.File;
import java.io.FileInputStream;

public class WriteResults extends TestBase {

	public String readTime;
	public String fileName = "";
	public String readTime1;
	public static ReadXL readXL = new ReadXL();
	Format formatter1;
	Date date1 = new Date();
	Format formatter;
	Date date = new Date();

	public int rcount = 0;
	public int rcount1 = 0;
	public String writeFileName = "Results - ";
	public String wFileName = "";

	public XSSFWorkbook workBook;
	public XSSFSheet outputSheet;
	public XSSFSheet sheet;
	public POIFSFileSystem inputFile;
	public FileOutputStream outputFile;

	public XSSFRow row1;

	private static int TEST_CASE_CELL = 0;
	private static int EXPECTED_RESULT_CELL = 1;
	private static int ACTUAL_RESULT_CELL = 2;
	private static int TEST_CASE_STATUS_CELL = 3;
	private static int TEST_CASE_TRACKCODE_CELL = 4;

	private static int TEST_CASE_STATUS1 = 2;
	private static int TEST_CLASS = 0;
	private static int TEST_CASE_DESC = 1;
	// private static String EXCEL_FILE_NAME = "";

	public static XSSFCellStyle cellStyle;
	public static XSSFCellStyle cellStylepass;
	public static XSSFCellStyle cellStylefail;

	public HashMap<String, String> getParameters = new HashMap<String, String>();
	public HashMap<String, String> getConfigParametersXpath = new HashMap<String, String>();
	// public ReadXl readParametrs = new ReadXl();

	public String excelFileName = "";
	public XSSFWorkbook wb;
	public FileOutputStream fileOut;
	public static java.util.Date startTime;

	private static int pageName = 0;
	private static int maxQueryTime = 1;
	private static int totalMageTime = 2;
	private static int totalQueries = 3;
	private static int maxQueries = 4;

	private static int summaryModuleName = 0;
	private static int summarySubmoduleName = 1;
	private static int summarySheetName = 2;
	private static int summaryTotalTC = 3;
	private static int summarySkipTC = 4;
	private static int summaryPassTC = 5;
	private static int summaryFailTC = 6;
	private static int summaryTotalTime = 7;
	private static int summarySuccessRate = 8;

	public static int totalTC = 0;
	public static int passTC = 0;
	public static int skipTC = 0;
	public static int failTC = 0;
	public static String totalTime = "";
	// private static int skipTC = 0;

	public static java.util.List<String> modules = new ArrayList<String>();
	public static java.util.List<String> submodules = new ArrayList<String>();
	public static java.util.List<String> senarios = new ArrayList<String>();
	public static java.util.List<String> executedTC = new ArrayList<String>();
	public static java.util.List<String> passedTC = new ArrayList<String>();
	public static java.util.List<String> failedTC = new ArrayList<String>();
	public static java.util.List<String> SuccessRate = new ArrayList<String>();
	public static java.util.List<String> skipedTC = new ArrayList<String>();
	public static java.util.List<String> SenarioExecutionTime = new ArrayList<String>();
	public static java.util.List<String> faildResponsesenarios = new ArrayList<String>();
	public static java.util.List<String> faildResponsesenariosDesc = new ArrayList<String>();

	public static String findElementInXLSheet(HashMap<String, String> mapName, String parameterName) throws Exception {
		String parameterValue = "";
		if (mapName.containsKey(parameterName)) {
			parameterValue = mapName.get(parameterName).trim();
		}
		return parameterValue;

	}

	public String getWriteFileName() throws Exception {
		fileName = findElementInXLSheet(getConfigParametersXpath, "save results file name");

		formatter = new SimpleDateFormat("yyyy-MM-dd-HH-mm");
		readTime = formatter.format(date);

		formatter1 = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		readTime1 = formatter1.format(date1);

		wFileName = excelReadFileName + fileName + "-" + moduleName + ".xlsx";
		return wFileName;

	}

	public String WriteResult() throws Exception {
		return getWriteFileName();
	}

	public void createFile(String suiteName) throws Exception {
		Calendar cal = Calendar.getInstance();
		startTime = cal.getTime();
		excelReadFileName = suiteName;

		File directory = new File(screenShot);
		File[] files = directory.listFiles();
		for (File file : files) {
			if (!file.delete()) {
				System.out.println("Failed to delete " + file);
			}
		}
		readTime = "";

		resultFolder = resultFolder;
		excelFileName = resultFolder + WriteResult();// name of excel file

		FileOutputStream fileOut = new FileOutputStream(resultFolder + WriteResult());

		workBook = new XSSFWorkbook();
		workBook.write(fileOut);

		fileOut.flush();
		fileOut.close();
	}

	public XSSFFont setFont() throws Exception {

		XSSFFont font = workBook.createFont();
		font.setFontName("Calibri");
		font.setColor((short) 0);
		font.setFontHeightInPoints((short) 11);

		return font;
	}

	public XSSFFont setPassFont() throws Exception {

		XSSFFont passfont = workBook.createFont();
		passfont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		passfont.setFontName("Calibri");
		passfont.setColor((short) 17);
		passfont.setFontHeightInPoints((short) 11);
		return passfont;
	}

	public XSSFFont setFailFont() throws Exception {

		XSSFFont failfont = workBook.createFont();
		failfont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		failfont.setFontName("Calibri");
		failfont.setColor((short) 10);
		failfont.setFontHeightInPoints((short) 11);
		return failfont;
	}

	public XSSFFont setHeaderFont() throws Exception {

		XSSFFont headerfont = workBook.createFont();
		headerfont.setFontName("Calibri");
		headerfont.setColor((short) 1);
		headerfont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerfont.setFontHeightInPoints((short) 11);
		return headerfont;
	}

	public void writeTestResult(String testCaseName, String expectedResult, String actualResult,
			boolean verificationStatus, String sheetName, String mainModule, String subModule) throws Exception {
		resultFolder = resultFolder;
		File file = new File(resultFolder + WriteResult());
		FileInputStream fis = new FileInputStream(file);
		workBook = new XSSFWorkbook(fis);

		if (workBook.getSheet(sheetName) == null) {
			if (skipTC == 0) {
				skipTC = 0;
			}

			rcount = 0;
			outputSheet = workBook.createSheet(sheetName);
			outputSheet.setColumnWidth(0, 12000);
			outputSheet.setColumnWidth(1, 13500);
			outputSheet.setColumnWidth(2, 15200);
			outputSheet.setColumnWidth(3, 4500);
			outputSheet.setColumnWidth(4, 4500);

		} else {
			outputSheet = workBook.getSheet(sheetName);
			rcount = outputSheet.getLastRowNum();
		}

		if (rcount == 0) {
			createHeaderXml(outputSheet, workBook, sheetName);
		}

		row1 = outputSheet.createRow(rcount + 1);

		XSSFRow row = row1;

		row.setHeightInPoints((3 * outputSheet.getDefaultRowHeightInPoints()));

		cellStyle = workBook.createCellStyle();
		cellStylepass = workBook.createCellStyle();
		cellStylefail = workBook.createCellStyle();

		cellStyle.setBorderTop((short) 1);
		cellStyle.setBorderBottom((short) 1);
		cellStyle.setBorderLeft((short) 1);
		cellStyle.setBorderRight((short) 1);

		cellStylepass.setBorderTop((short) 1);
		cellStylepass.setBorderBottom((short) 1);
		cellStylepass.setBorderLeft((short) 1);
		cellStylepass.setBorderRight((short) 1);

		cellStylefail.setBorderTop((short) 1);
		cellStylefail.setBorderBottom((short) 1);
		cellStylefail.setBorderLeft((short) 1);
		cellStylefail.setBorderRight((short) 1);

		cellStyle.getWrapText();

		cellStyle.setFont(setFont());
		cellStyle.setWrapText(true);
		cellStyle.setVerticalAlignment(XSSFCellStyle.VERTICAL_TOP);

		XSSFCell cellR1 = row.createCell(TEST_CASE_CELL);
		XSSFCell cellR2 = row.createCell(EXPECTED_RESULT_CELL);
		XSSFCell cellR3 = row.createCell(ACTUAL_RESULT_CELL);
		XSSFCell cellR4 = row.createCell(TEST_CASE_STATUS_CELL);
		XSSFCell cellR5 = row.createCell(TEST_CASE_TRACKCODE_CELL);

		cellR1.setCellValue(testCaseName);
		cellR2.setCellValue(expectedResult);
		cellR3.setCellValue(actualResult);
		cellR5.setCellValue(trackCode);

		cellR1.setCellStyle(cellStyle);
		cellR2.setCellStyle(cellStyle);
		cellR3.setCellStyle(cellStyle);
		cellR5.setCellStyle(cellStyle);

		if (testCaseName.indexOf("Total time to execute") < 0) {
			totalTC++;
		}

		if (verificationStatus == true) {

			cellR4.setCellValue("PASS");
			cellR4.setCellStyle(cellStylepass);
			cellStylepass.setFont(setPassFont());
			cellStylepass.setWrapText(true);
			cellStylepass.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);

			if (testCaseName.indexOf("Total time to execute") < 0) {
				passTC++;
			}

		} else {

			cellR4.setCellValue("FAIL");
			cellR4.setCellStyle(cellStylefail);
			cellStylefail.setFont(setFailFont());
			if (testCaseName.indexOf("Total time to execute") < 0) {
				failTC++;
			}

		}

		fileOut = new FileOutputStream(resultFolder + WriteResult());

		workBook.write(fileOut);
		fileOut.flush();
		fileOut.close();

		if (testCaseName.indexOf("Total time to execute") >= 0) {
			totalTime = actualResult;

			System.out.println("*******************");
			System.out.println("Name - " + sheetName);
			System.out.println("Total TC " + totalTC);
			System.out.println("Total skip TC " + skipTC);
			System.out.println("Total Fail TC " + failTC);
			System.out.println("Total Pass TC " + passTC);
			System.out.println("*******************");
			/*
			 * writeTestSummaryResult(mainModule,subModule,testCaseName,totalTC,passTC,
			 * failTC,skipTC,totalTime);
			 * 
			 * totalTC = 0; passTC = 0; failTC = 0; skipTC = 0; totalTime ="";
			 */
		}

		trackCode = "";
	}

	public void sheetOrder() throws Exception {
		resultFolder = resultFolder;
		File file = new File(resultFolder + WriteResult());
		FileInputStream fis = new FileInputStream(file);
		workBook = new XSSFWorkbook(fis);

		// System.out.println("No of sheets"+workBook.getNumberOfSheets());
		workBook.setSheetOrder("Home Page", 1);

	}

	public void createHeaderXml(XSSFSheet outputSheet, XSSFWorkbook workBook, String sheetName) throws Exception {

		XSSFRow row = outputSheet.createRow(0);

		XSSFCellStyle cellStyle = workBook.createCellStyle();
		cellStyle = workBook.createCellStyle();
		cellStyle.setFillForegroundColor(HSSFColor.BROWN.index);
		cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		cellStyle.setBorderTop((short) 1); // single line border
		cellStyle.setBorderBottom((short) 1); // single line border
		cellStyle.setBorderLeft((short) 1);
		cellStyle.setBorderRight((short) 1);

		cellStyle.setFont(setHeaderFont());

		XSSFCell cellH1 = row.createCell(0);
		XSSFCell cellH2 = row.createCell(1);
		XSSFCell cellH3 = row.createCell(2);
		XSSFCell cellH4 = row.createCell(3);
		XSSFCell cellH5 = row.createCell(4);

		if (sheetName.equals("SEO Tracking")) {
			cellH1.setCellValue("Attributes");
			cellH2.setCellValue("Expected Code And Attributes");
			cellH3.setCellValue("Actual Code");
			cellH4.setCellValue("Test Status");
		} else {
			cellH1.setCellValue("Test Case");
			cellH2.setCellValue("Expected Result");
			cellH3.setCellValue("Actual Result");
			cellH4.setCellValue("Test Status");
			cellH5.setCellValue("Track Code");
		}

		cellH1.setCellStyle(cellStyle);
		cellH2.setCellStyle(cellStyle);
		cellH3.setCellStyle(cellStyle);
		cellH4.setCellStyle(cellStyle);
		cellH5.setCellStyle(cellStyle);
	}

	public void writeSkipTestResult(String className, String description, String status, String sheetName)
			throws Exception {
		resultFolder = resultFolder;
		File file = new File(resultFolder + WriteResult());
		FileInputStream fis = new FileInputStream(file);
		workBook = new XSSFWorkbook(fis);

		if (status.equalsIgnoreCase("Found fail Response")) {
			sheetName = "Error Response";

		}

		if (workBook.getSheet(sheetName) == null) {
			rcount = 0;
			outputSheet = workBook.createSheet(sheetName);
			// System.out.println("skip row"+rcount);
		} else {
			outputSheet = workBook.getSheet(sheetName);
			rcount = outputSheet.getLastRowNum();
			// System.out.println("skip row"+rcount);
		}

		if (rcount == 0) {
			createHeaderForSkipResults(outputSheet, workBook);
		}

		row1 = outputSheet.createRow(rcount + 1);

		XSSFRow row = row1;

		XSSFCellStyle cellStyle = workBook.createCellStyle();
		XSSFCellStyle cellStyleSkip = workBook.createCellStyle();

		cellStyle = workBook.createCellStyle();
		cellStyleSkip = workBook.createCellStyle();

		/*
		 * cellStyleSkip.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
		 * cellStyleSkip.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		 * 
		 */
		XSSFFont skipfont = workBook.createFont();

		skipfont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		cellStyleSkip.setFont(skipfont);
		cellStyleSkip.setWrapText(true);

		cellStyle.setBorderTop((short) 1);
		cellStyle.setBorderBottom((short) 1);
		cellStyle.setBorderLeft((short) 1);
		cellStyle.setBorderRight((short) 1);

		cellStyleSkip.setBorderTop((short) 1);
		cellStyleSkip.setBorderBottom((short) 1);
		cellStyleSkip.setBorderLeft((short) 1);
		cellStyleSkip.setBorderRight((short) 1);

		outputSheet.setColumnWidth(0, 12000);
		outputSheet.setColumnWidth(1, 12000);
		outputSheet.setColumnWidth(2, 7500);

		/*
		 * font.setFontName("Calibri"); font.setColor((short) 0);
		 * font.setFontHeightInPoints((short)11);
		 */

		cellStyle.setFont(setFont());
		cellStyle.setWrapText(true);

		skipfont.setFontName("Calibri");
		skipfont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		cellStyleSkip.setWrapText(true);
		skipfont.setColor((short) 18);
		skipfont.setFontHeightInPoints((short) 11);
		cellStyleSkip.setFont(skipfont);

		XSSFCell cellR1 = row.createCell(TEST_CLASS);
		XSSFCell cellR2 = row.createCell(TEST_CASE_DESC);
		XSSFCell cellR3 = row.createCell(TEST_CASE_STATUS1);

		cellR1.setCellValue(className);
		cellR2.setCellValue(description);
		cellR3.setCellValue(status);

		cellR1.setCellStyle(cellStyle);
		cellR2.setCellStyle(cellStyle);
		cellR3.setCellStyle(cellStyleSkip);

		fileOut = new FileOutputStream(resultFolder + WriteResult());

		// write this workbook to an Outputstream.
		workBook.write(fileOut);
		fileOut.flush();
		fileOut.close();

		// ++rowCount;
		if (sheetName.equalsIgnoreCase("Skip Results")) {
			skipTC++;
		}
		if (status.equalsIgnoreCase("Found fail Response")) {
			faildResponsesenarios.add(className);
			faildResponsesenariosDesc.add(description);
		}

	}

	public void createHeaderForSkipResults(XSSFSheet outputSheet, XSSFWorkbook workBook) throws Exception {

		XSSFRow row = outputSheet.createRow(0);

		XSSFCellStyle cellStyle = workBook.createCellStyle();
		cellStyle = workBook.createCellStyle();

		// XSSFColor myColor = new XSSFColor(Color.black);
		// cellStyle.setFillForegroundColor(myColor);
		// cellStyle.setFillForegroundColor(Color.GRAY);
		// cellStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		cellStyle.setFillForegroundColor(HSSFColor.BROWN.index);
		cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		cellStyle.setBorderTop((short) 1); // single line border
		cellStyle.setBorderBottom((short) 1); // single line border
		cellStyle.setBorderLeft((short) 1);
		cellStyle.setBorderRight((short) 1);

		cellStyle.setFont(setHeaderFont());

		XSSFCell cellH1 = row.createCell(0);
		XSSFCell cellH2 = row.createCell(1);
		XSSFCell cellH3 = row.createCell(2);

		cellH1.setCellValue("Main Class Name");
		cellH2.setCellValue("Scenarios");
		cellH3.setCellValue("Test Status");

		cellH1.setCellStyle(cellStyle);
		cellH2.setCellStyle(cellStyle);
		cellH3.setCellStyle(cellStyle);

	}

	public void writeTestResultForProfiler(String pName, String qTime, String mageTime, String tQueries, String mQuery,
			String sheetName) throws Exception {

		resultFolder = resultFolder;
		File file = new File(resultFolder + WriteResult());
		FileInputStream fis = new FileInputStream(file);
		workBook = new XSSFWorkbook(fis);

		if (workBook.getSheet(sheetName) == null) {

			rcount = 0;
			outputSheet = workBook.createSheet(sheetName);
			// System.out.println("rcount"+rcount);

			outputSheet.setColumnWidth(0, 10000);
			outputSheet.setColumnWidth(1, 5000);
			outputSheet.setColumnWidth(2, 4000);
			outputSheet.setColumnWidth(3, 12000);
			outputSheet.setColumnWidth(4, 18000);

		} else {
			outputSheet = workBook.getSheet(sheetName);
			rcount = outputSheet.getLastRowNum();

			// System.out.println("rcount"+rcount);
		}

		if (rcount == 0) {
			createHeaderForMagentoProfiler(outputSheet, workBook, sheetName);
		}

		row1 = outputSheet.createRow(rcount + 1);

		XSSFRow row = row1;

		row.setHeightInPoints((5 * outputSheet.getDefaultRowHeightInPoints()));

		cellStyle = workBook.createCellStyle();
		cellStylepass = workBook.createCellStyle();
		cellStylefail = workBook.createCellStyle();

		cellStyle.setBorderTop((short) 1);
		cellStyle.setBorderBottom((short) 1);
		cellStyle.setBorderLeft((short) 1);
		cellStyle.setBorderRight((short) 1);

		cellStylepass.setBorderTop((short) 1);
		cellStylepass.setBorderBottom((short) 1);
		cellStylepass.setBorderLeft((short) 1);
		cellStylepass.setBorderRight((short) 1);

		cellStylefail.setBorderTop((short) 1);
		cellStylefail.setBorderBottom((short) 1);
		cellStylefail.setBorderLeft((short) 1);
		cellStylefail.setBorderRight((short) 1);

		cellStyle.getWrapText();

		cellStyle.setFont(setFont());
		cellStyle.setWrapText(true);
		cellStyle.setVerticalAlignment(XSSFCellStyle.VERTICAL_TOP);

		XSSFCell cellR1 = row.createCell(pageName);
		XSSFCell cellR2 = row.createCell(maxQueryTime);
		XSSFCell cellR3 = row.createCell(totalMageTime);
		XSSFCell cellR4 = row.createCell(totalQueries);
		XSSFCell cellR5 = row.createCell(maxQueries);

		// mageTime
		cellR1.setCellValue(pName);

		cellR2.setCellValue(qTime);
		cellR3.setCellValue(mageTime);
		cellR4.setCellValue(tQueries);
		cellR5.setCellValue(mQuery);

		cellR1.setCellStyle(cellStyle);
		cellR2.setCellStyle(cellStyle);
		cellR3.setCellStyle(cellStyle);
		cellR4.setCellStyle(cellStyle);
		cellR5.setCellStyle(cellStyle);

		cellR1.setCellStyle(cellStyle);
		cellR2.setCellStyle(cellStyle);
		cellR3.setCellStyle(cellStyle);

		fileOut = new FileOutputStream(resultFolder + WriteResult());

		// write this workbook to an Outputstream.
		workBook.write(fileOut);
		fileOut.flush();
		fileOut.close();

		// System.out.println("vv"+rcount);

	}

	private void createHeaderForMagentoProfiler(XSSFSheet outputSheet, XSSFWorkbook workBook, String sheetName)
			throws Exception {

		XSSFRow row = outputSheet.createRow(0);

		XSSFCellStyle cellStyle = workBook.createCellStyle();
		cellStyle = workBook.createCellStyle();
		cellStyle.setFillForegroundColor(HSSFColor.BROWN.index);
		cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		cellStyle.setBorderTop((short) 1); // single line border
		cellStyle.setBorderBottom((short) 1); // single line border
		cellStyle.setBorderLeft((short) 1);
		cellStyle.setBorderRight((short) 1);

		cellStyle.setFont(setHeaderFont());

		XSSFCell cellH1 = row.createCell(0);
		XSSFCell cellH2 = row.createCell(1);
		XSSFCell cellH3 = row.createCell(2);
		XSSFCell cellH4 = row.createCell(3);
		XSSFCell cellH5 = row.createCell(4);

		if (sheetName.equalsIgnoreCase("Sumary Results")) {
			cellH1.setCellValue("Page Name");
		} else {
			cellH1.setCellValue("Code Profiler");
		}

		if (sheetName.equalsIgnoreCase("Magento Profiler Results")) {
			cellH1.setCellValue("Page Name");
			cellH2.setCellValue("Max Query Time(ms)");
			cellH3.setCellValue("Mage Time(ms)");
			cellH4.setCellValue("Total number of queries");
			cellH5.setCellValue("Max Query");

		} else {
			cellH2.setCellValue("Profiler Time");
			cellH3.setCellValue("Web Page Tool time");
			cellH4.setCellValue("Web Page Tool Result");
		}

		cellH1.setCellStyle(cellStyle);
		cellH2.setCellStyle(cellStyle);
		cellH3.setCellStyle(cellStyle);
		cellH4.setCellStyle(cellStyle);
		cellH5.setCellStyle(cellStyle);

	}

	public void writeTestSummaryResult(String module, String submodule, String sheetName, int totalTC, int passTC,
			int failTC, int skipTC, String totalTime) throws Exception {
		resultFolder = readXL.resultFolder;
		File file = new File(resultFolder + WriteResult());
		FileInputStream fis = new FileInputStream(file);
		workBook = new XSSFWorkbook(fis);

		rcount = 0;

		if (workBook.getSheet("Summary Results") == null) {
			rcount = 0;
			outputSheet = workBook.createSheet("Summary Results");
			outputSheet.setColumnWidth(0, 12000);
			outputSheet.setColumnWidth(1, 12000);
			outputSheet.setColumnWidth(2, 12000);
			outputSheet.setColumnWidth(3, 5000);
			outputSheet.setColumnWidth(4, 5000);
			outputSheet.setColumnWidth(5, 5000);
			outputSheet.setColumnWidth(6, 5000);
			outputSheet.setColumnWidth(7, 5000);
			outputSheet.setColumnWidth(8, 5000);

		} else {
			outputSheet = workBook.getSheet("Summary Results");
			rcount = outputSheet.getLastRowNum();
		}

		if (rcount == 0) {

			// createHeaderForSummary(workBook.createSheet("Summary Results"),
			// workBook,"Summary Results");
			createHeaderForSummary(outputSheet, workBook, "Summary Results");
		}

		row1 = outputSheet.createRow(rcount + 1);

		XSSFRow row = row1;

		row.setHeightInPoints((1 * outputSheet.getDefaultRowHeightInPoints()));

		cellStyle = workBook.createCellStyle();
		cellStylepass = workBook.createCellStyle();
		cellStylefail = workBook.createCellStyle();

		cellStyle.setBorderTop((short) 1);
		cellStyle.setBorderBottom((short) 1);
		cellStyle.setBorderLeft((short) 1);
		cellStyle.setBorderRight((short) 1);

		cellStylepass.setBorderTop((short) 1);
		cellStylepass.setBorderBottom((short) 1);
		cellStylepass.setBorderLeft((short) 1);
		cellStylepass.setBorderRight((short) 1);

		cellStylefail.setBorderTop((short) 1);
		cellStylefail.setBorderBottom((short) 1);
		cellStylefail.setBorderLeft((short) 1);
		cellStylefail.setBorderRight((short) 1);

		cellStyle.getWrapText();

		cellStyle.setFont(setFont());
		cellStyle.setWrapText(true);
		cellStyle.setVerticalAlignment(XSSFCellStyle.VERTICAL_TOP);

		// private static int summaryTotalTime = 4;
		XSSFCell cellR1 = row.createCell(summaryModuleName);
		XSSFCell cellR2 = row.createCell(summarySubmoduleName);
		XSSFCell cellR3 = row.createCell(summarySheetName);
		XSSFCell cellR4 = row.createCell(summaryTotalTC);
		XSSFCell cellR5 = row.createCell(summarySkipTC);
		XSSFCell cellR6 = row.createCell(summaryPassTC);
		XSSFCell cellR7 = row.createCell(summaryFailTC);
		XSSFCell cellR8 = row.createCell(summaryTotalTime);
		XSSFCell cellR9 = row.createCell(summarySuccessRate);

		if (totalTC > 0) {
			if (passTC * 100 / totalTC >= 75) {

				cellR7.setCellStyle(cellStylepass);
				cellStylepass.setFont(setPassFont());
				cellStylepass.setWrapText(true);
				cellStylepass.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);

			} else {
				cellR7.setCellStyle(cellStylefail);
				cellStylefail.setFont(setFailFont());
				cellStylepass.setWrapText(true);
				cellStylepass.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
			}
		} else {
			cellR7.setCellStyle(cellStyle);
		}

		cellR1.setCellValue(module);
		cellR2.setCellValue(submodule);
		cellR3.setCellValue(sheetName);
		cellR4.setCellValue(totalTC + skipTC);
		cellR5.setCellValue(skipTC);
		cellR6.setCellValue(passTC);
		cellR7.setCellValue(failTC);
		cellR8.setCellValue(totalTime);

		if (totalTC > 0) {
			cellR9.setCellValue(passTC * 100 / totalTC + "%");
		} else {
			cellR9.setCellValue(0 + "%");
		}

		// System.out.println(sheetName);
		// System.out.println(totalTC);
		// System.out.println(passTC);
		// System.out.println(failTC);
		// System.out.println(skipTC);
		// System.out.println((passTC)*100/totalTC);

		cellR1.setCellStyle(cellStyle);
		cellR2.setCellStyle(cellStyle);
		cellR3.setCellStyle(cellStyle);
		cellR4.setCellStyle(cellStyle);
		cellR5.setCellStyle(cellStyle);
		cellR6.setCellStyle(cellStyle);
		cellR7.setCellStyle(cellStyle);
		cellR8.setCellStyle(cellStyle);
		cellR9.setCellStyle(cellStyle);

		fileOut = new FileOutputStream(resultFolder + WriteResult());

		// write this workbook to an Outputstream.
		workBook.write(fileOut);
		fileOut.flush();
		fileOut.close();

		// System.out.println("vv"+rcount);
		modules.add(module);
		submodules.add(submodule);
		senarios.add(sheetName);
		executedTC.add(String.valueOf(totalTC + skipTC));
		passedTC.add(String.valueOf(passTC));
		failedTC.add(String.valueOf(failTC));
		skipedTC.add(String.valueOf(skipTC));
		SenarioExecutionTime.add(totalTime);
		SuccessRate.add(String.valueOf((passTC) * 100 / (totalTC)));

	}

	public void createHeaderForSummary(XSSFSheet outputSheet, XSSFWorkbook workBook, String sheetName)
			throws Exception {

		XSSFRow row = outputSheet.createRow(0);

		XSSFCellStyle cellStyle = workBook.createCellStyle();
		cellStyle = workBook.createCellStyle();
		cellStyle.setFillForegroundColor(HSSFColor.BROWN.index);
		cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		cellStyle.setBorderTop((short) 1); // single line border
		cellStyle.setBorderBottom((short) 1); // single line border
		cellStyle.setBorderLeft((short) 1);
		cellStyle.setBorderRight((short) 1);

		cellStyle.setFont(setHeaderFont());

		XSSFCell cellH1 = row.createCell(0);
		XSSFCell cellH2 = row.createCell(1);
		XSSFCell cellH3 = row.createCell(2);
		XSSFCell cellH4 = row.createCell(3);
		XSSFCell cellH5 = row.createCell(4);
		XSSFCell cellH6 = row.createCell(5);
		XSSFCell cellH7 = row.createCell(6);
		XSSFCell cellH8 = row.createCell(7);
		XSSFCell cellH9 = row.createCell(8);

		cellH1.setCellValue("Module");
		cellH2.setCellValue("Sub Module");
		cellH3.setCellValue("Scenarios");
		cellH4.setCellValue("#Cases");
		cellH5.setCellValue("Skiped");
		cellH6.setCellValue("Passed");
		cellH7.setCellValue("Failed");
		cellH8.setCellValue("Time(mm:ss)");
		cellH9.setCellValue("Success Rate %");

		cellH1.setCellStyle(cellStyle);
		cellH2.setCellStyle(cellStyle);
		cellH3.setCellStyle(cellStyle);
		cellH4.setCellStyle(cellStyle);
		cellH5.setCellStyle(cellStyle);
		cellH6.setCellStyle(cellStyle);
		cellH7.setCellStyle(cellStyle);
		cellH8.setCellStyle(cellStyle);
		cellH9.setCellStyle(cellStyle);

	}

}