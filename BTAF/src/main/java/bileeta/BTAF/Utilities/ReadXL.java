package bileeta.BTAF.Utilities;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadXL{
	
	
	
	public int totalRows =0;
	
	public int i =0;
	public int y =0;
	
	public String description = "";
	public String paraXpath = "";
	
	public static String propertiesFolder = "";
	public static String dataFolder = "";
	public static String resultFolder = "";
	public static String screenShot = "";
	public static String mediaFolder = "";
	public static String driverFolder="";
	public static String logFolder="";
	
	public HashMap<String, String> readParameters(String moduleName)throws Exception {
		
		HashMap<String, String> getParametersXpath = new HashMap<String, String>();
		y=0;
		
		FileInputStream fi=new FileInputStream(new File(propertiesFolder+"Properties.xlsx"));
		Workbook w = new XSSFWorkbook(fi);
		Sheet sheetname=w.getSheet(moduleName);
		totalRows = sheetname.getLastRowNum();
		DataFormatter df = new DataFormatter();
	 	
		for(i=1;i<=totalRows;i++){
	 		if(y<totalRows){
	 			Row rowvalue= sheetname.getRow(i);
	 			Cell cellvalue= rowvalue.getCell(0);
	 			description = df.formatCellValue(cellvalue);
	 			
	 			rowvalue= sheetname.getRow(i);
	 			cellvalue= rowvalue.getCell(1);
	 			paraXpath = df.formatCellValue(cellvalue);
	 			
	 			getParametersXpath.put(description, paraXpath);
	 			
	 			y++;
	 			
	 		}
	 	}
	 
	 	return getParametersXpath;
		
	}
	
	public HashMap<String, String> readData(String moduleName)throws Exception {
		
		HashMap<String, String> getData = new HashMap<String, String>();
		y=0;
		
		FileInputStream fi=new FileInputStream(new File(dataFolder+"TestData.xlsx"));
		Workbook w = new XSSFWorkbook(fi);
		Sheet sheetname=w.getSheet(moduleName);
		totalRows = sheetname.getLastRowNum();
		DataFormatter df = new DataFormatter();
	 	
		for(i=1;i<=totalRows;i++){
	 		if(y<totalRows){
	 			Row rowvalue= sheetname.getRow(i);
	 			Cell cellvalue= rowvalue.getCell(0);
	 			description = df.formatCellValue(cellvalue);
	 			
	 			rowvalue= sheetname.getRow(i);
	 			cellvalue= rowvalue.getCell(1);
	 			paraXpath = df.formatCellValue(cellvalue);
	 			
	 			getData.put(description, paraXpath);
	 			
	 			y++;
	 			
	 		}
	 	}
	 
	 	return getData;
		
	}
	

}
