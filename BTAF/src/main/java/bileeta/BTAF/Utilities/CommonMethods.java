package bileeta.BTAF.Utilities;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class CommonMethods {

	public String getCurrentDateTimeAsUniqueCode() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		String curDate = dtf.format(now);
		curDate = curDate.replaceAll("\\s", "");
		String currentDateTimeCode = curDate.replaceAll("/", "").replaceAll(":", "");
		return currentDateTimeCode;
	}

	public String currentDay() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		String date = dtf.format(now);

		char first_char = date.charAt(8);
		String first_letter = Character.toString(first_char);
		char second_char = date.charAt(9);
		String second_letter = Character.toString(second_char);

		String final_date;
		if (first_letter.equals("0")) {
			final_date = second_letter;
		} else {
			final_date = first_letter + second_letter;
		}
		return final_date;
	}

	public String currentDate() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		LocalDateTime now = LocalDateTime.now();
		String date = dtf.format(now);
		return date;
	}
}
