package bileeta.BTAF.Utilities;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.io.FileUtils;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemOptions;
import org.apache.commons.vfs2.Selectors;
import org.apache.commons.vfs2.VFS;
import org.apache.commons.vfs2.impl.DefaultFileSystemConfigBuilder;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import org.testng.annotations.*;

public class SendTestSummery extends TestBase {

	ReadXL readXL = new ReadXL();
	WriteResults wResult = new WriteResults();

	public String resultFolder = "";
	public String mailTo;
	public String[] mailList;
	public String[] to;

	public int totalExecutedTC = 0;
	public int totalPassedTC = 0;
	public int totalFailedTC = 0;
	public int totalFailedResponse = 0;
	public int totalSkipedTC = 0;
	public String uJDocumentName = "";

	public void testResults() throws Exception {
		try {
			sendMail();
		} catch (Exception exc1) {
			Thread.sleep(1000);
			sendMail();
			System.out.println("execption email sending" + exc1);
		}

	}

	static SendTestSummery sms = new SendTestSummery();

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		sms.sendMail();
	}

	public void sendMail() throws Exception {
		Thread.sleep(6000);

		wResult.getWriteFileName();

		resultFolder = readXL.resultFolder;

		final String username = "bileeta.automation@gmail.com";// change accordingly
		final String password = "Bileeta123";// change accordingly

		// Assuming you are sending email through relay.jangosmtp.net
		String host = "smtp.gmail.com";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", "587");
		props.put("mail.smtp.ssl.trust", "smtp.gmail.com");

		// Get the Session object.
		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		String from = "Automation Team";
		MimeMessage msg = new MimeMessage(session);

		String getMailList = "";

		String content = resultsSummary1();

		getMailList = "nuwan.sameera@bileeta.com,madhushan.fernando@bileeta.com,aruna.hennayaka@bileeta.com,madhushika.manamperi@bileeta.com,harshani.abeywickrama@bileeta.com,nilani.kayarasan@bileeta.com,sithara.indumini@bileeta.com,madushani.bandara@bileeta.com,dinushi.balasooriya@bileeta.com,manasha.ranasinghe@bileeta.com,dinushi.balasooriya@bileeta.com";
//		getMailList = "madhushan.fernando@bileeta.com";

		System.out.println("getMailList" + getMailList);

		InternetAddress[] addressTo = null;

		String subjectName = " (Regression Testing/Smoke Testing/Business Scenarios) ";

		String subject = "Summery Of Test Automation Results" + subjectName;

		msg.setFrom(new InternetAddress("amithcwijesiri@gmail.com", from));

		if (!getMailList.isEmpty()) {

			if (getMailList.indexOf(",") < 0 && getMailList.indexOf(";") < 0) {

				msg.setRecipients(RecipientType.TO, getMailList);
			} else {
				if (getMailList.indexOf(",") > -1) {
					mailList = getMailList.split(",");
				} else if (getMailList.indexOf(";") > -1) {
					mailList = getMailList.split(";");
				}
				for (int i = 0; i < mailList.length; i++) {
					if (mailList[i].indexOf("@") == -1 || mailList[i].indexOf(".") == -1) {
						System.out.println("Following email address is invalid" + mailList[i]);

					} else {
						to = mailList;
					}

				}
				addressTo = new InternetAddress[to.length];
				for (int i = 0; i < to.length; i++) {
					addressTo[i] = new InternetAddress(to[i]);
				}
				// msg.setFrom(new InternetAddress(from));

				msg.setRecipients(RecipientType.TO, addressTo);
			}

			try {

				BodyPart messageBodyPart = new MimeBodyPart();
				Multipart multipart = new MimeMultipart();

				if (!content.isEmpty()) {

					messageBodyPart = new MimeBodyPart();
					messageBodyPart.setContent(content, "text/html");
					multipart.addBodyPart(messageBodyPart);

//					  messageBodyPart = new MimeBodyPart(); DataSource fds1 = new
//					  FileDataSource(readXL.mediaFolder+"/logo.png");
//					 messageBodyPart.setDataHandler(new DataHandler(fds1));
//					  messageBodyPart.setHeader("Content-ID", "<image1>");
//					  multipart.addBodyPart(messageBodyPart);

					messageBodyPart = new MimeBodyPart();
					DataSource fds1 = new FileDataSource(readXL.mediaFolder + "/logo.png");
					messageBodyPart.setDataHandler(new DataHandler(fds1));
					messageBodyPart.setHeader("Content-ID", "<image1>");
					messageBodyPart.setFileName(readXL.mediaFolder + "/logo.png"); // <= HERE
					multipart.addBodyPart(messageBodyPart);

//					 MimeBodyPart imagePart = new MimeBodyPart();
//		                imagePart.setHeader("Content-ID", "<image1>");
//		                imagePart.setDisposition(MimeBodyPart.INLINE);
//		                 
//		                String imageFilePath =readXL.mediaFolder+"/logo.png";
//		                try {
//		                    imagePart.attachFile(imageFilePath);
//		                } catch (IOException ex) {
//		                    ex.printStackTrace();
//		                }
//		 
//		                multipart.addBodyPart(imagePart);

				}

				msg.setSubject(
						subject + " - " + totalFailedTC + " Tests Failed out of " + (totalFailedTC + totalPassedTC));

				// Attaching the excel sheet

				File folder = new File(resultFolder);
				File[] listOfFiles = folder.listFiles();

				for (int i = 0; i < listOfFiles.length; i++) {
					if (listOfFiles[i].isFile()) {
						messageBodyPart = new MimeBodyPart();
						String filename = resultFolder + listOfFiles[i].getName();
						DataSource source = new FileDataSource(filename);
						messageBodyPart.setDataHandler(new DataHandler(source));
						messageBodyPart.setFileName(listOfFiles[i].getName());
						multipart.addBodyPart(messageBodyPart);
						System.out.println("File " + listOfFiles[i].getName());

					}
				}

				msg.setContent(multipart);

				// Send the complete message parts

				Transport transport = session.getTransport("smtp");
				transport.send(msg);
				System.out.println("E-mail sent.. !");

			} catch (Exception exc) {
				System.out.println(exc);
			}

		}

	}

	public static String findElementInXLSheet(HashMap<String, String> mapName, String parameterName) throws Exception {
		String parameterValue = "";
		if (mapName.containsKey(parameterName)) {
			parameterValue = mapName.get(parameterName).trim();

		}

		return parameterValue;
	}

	public String resultsSummary1() throws Exception {
		String summary = headerContent() + messageContent() + footerContent();

		return summary;
	}

	public String messageContent() throws Exception {
		String tableContent = "";
		String tableContentAfterAddedRows = "";
		String module = "";
		String submodule = "";
		String senario = "";
		String totalTC = "";
		String passedTC = "";
		String failTC = "";
		String successRate = "";
		String skipTC = "";
		String totalTime = "";
		String backgroundColor = "";
		String statusColor = "";
		String skipColor = "#C0C0C0";
		int responseStatus = 0;
		String responseError = "";
		String tempMod = "";
		String tempSubMod = "";

		// System.out.println("wResult.senarios.size()"+WriteResults.senarios.size());
		// for(int i = 1;i < 5; i++)
		for (int i = 0; i < WriteResults.senarios.size(); i++) {

			if (i % 2 == 0) {
				backgroundColor = "#f0d9fc";
			} else {
				backgroundColor = "#dcbbed";
			}

			tableContent = bodyContent();

			module = WriteResults.modules.get(i);
			submodule = WriteResults.submodules.get(i);
			senario = WriteResults.senarios.get(i);
			totalTC = WriteResults.executedTC.get(i);
			passedTC = WriteResults.passedTC.get(i);
			failTC = WriteResults.failedTC.get(i);
			successRate = WriteResults.SuccessRate.get(i);
			skipTC = WriteResults.skipedTC.get(i);
			totalTime = WriteResults.SenarioExecutionTime.get(i);

			totalExecutedTC = totalExecutedTC + Integer.parseInt(totalTC);
			totalPassedTC = totalPassedTC + Integer.parseInt(passedTC);
			totalFailedTC = totalFailedTC + Integer.parseInt(failTC);
			totalSkipedTC = totalSkipedTC + Integer.parseInt(skipTC);

			if (module == tempMod) {
				tempMod = module;
				module = "";
			}
			if (submodule == tempSubMod) {
				tempSubMod = submodule;
				submodule = "";
			}
			// System.out.println("senario"+senario);
			// System.out.println("totalTC"+totalTC);
			// System.out.println("passedTC"+passedTC);
			// System.out.println("failTC"+failTC);
			// System.out.println("successRate"+successRate);
			tableContent = tableContent

					.replaceAll("module", module).replaceAll("subcat", submodule).replaceAll("scenario", senario)
					.replaceAll("totalTC", totalTC).replaceAll("passedTC", passedTC).replaceAll("failedTC", failTC)
					.replaceAll("SkipTC", skipTC).replaceAll("SuccessRate", successRate + "%")
					.replaceAll("TotalTime", totalTime).replaceAll("setBGColor", backgroundColor);
			if (i == 0) {
				tempMod = module;
				tempSubMod = submodule;
			}

			// System.out.println("tableContent"+tableContent);
			tableContentAfterAddedRows = tableContentAfterAddedRows + tableContent;

			// System.out.println(tableContentAfterAddedRows+"tableContentAfterAddedRows");
		}

		for (int i = 0; i < WriteResults.faildResponsesenarios.size(); i++) {

			if (i % 2 == 0) {
				backgroundColor = "#f4f4f4";
			} else {
				backgroundColor = "#dddbdb";
			}

			/*
			 * if(Integer.parseInt(WriteResults.SuccessRate.get(i)) >= 75) { statusColor =
			 * "#00B62C"; } else { statusColor = "#D80000"; }
			 */
			tableContent = bodyContent();

			senario = WriteResults.faildResponsesenarios.get(i);
			totalTC = WriteResults.faildResponsesenariosDesc.get(i);

			totalFailedTC++;
			responseStatus = Integer
					.parseInt(totalTC.substring(totalTC.indexOf("response got it:") + 16, totalTC.indexOf("-")).trim());
			responseError = totalTC.substring(totalTC.indexOf("-") + 1).trim();

			// System.out.println("responseStatus"+responseStatus);
			// System.out.println("responseError"+responseError);

			if (responseStatus > 0) {
				totalFailedResponse++;
			} else if (!responseError.isEmpty() && !responseError.equalsIgnoreCase("Connection timed out: connect")) {
				totalFailedResponse++;
			}

			tableContent = tableContent.replaceAll("senario", senario).replaceAll("totalTC", totalTC)
					.replaceAll("passedTC", "").replaceAll("failedTC", "").replaceAll("SkipTC", "")
					.replaceAll("SuccessRate", "").replaceAll("TotalTime", "")
					.replaceAll("setBGColor", backgroundColor);

			tableContentAfterAddedRows = tableContentAfterAddedRows + tableContent;

		}

		return tableContentAfterAddedRows;
	}

	public String headerContent() throws Exception {

		BufferedReader in = new BufferedReader(new FileReader(readXL.mediaFolder + "/header.txt"));

		String line;
		String headerTxt = "";
		while ((line = in.readLine()) != null) {
			headerTxt = headerTxt + "\n" + line;
		}
		in.close();

		String tableContent = headerTxt;

		// Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		String browserName = "FireFox";
		String browserVersion = "60.3";
		if (browserVersion.equals(" ")) {
			browserVersion = "Latest Version";
		}
		String osname = System.getProperty("os.name");

		// System.out.println("Project Name"+wResult.fileName);
		tableContent = tableContent.replaceAll("ResultDate", wResult.readTime1)
				.replaceAll("Version", browserName + " - " + browserVersion).replaceAll("OS", osname);
		return tableContent;
	}

	public String bodyContent() throws Exception {

		BufferedReader in = new BufferedReader(new FileReader(readXL.mediaFolder + "/body.txt"));

		String line;
		String bodyTxt = "";
		while ((line = in.readLine()) != null) {
			bodyTxt = bodyTxt + "\n" + line;
		}
		in.close();

		return bodyTxt;
	}

	public String footerContent() throws Exception {
		BufferedReader in = new BufferedReader(new FileReader(readXL.mediaFolder + "/footer.txt"));

		String line;
		String footerTxt = "";
		while ((line = in.readLine()) != null) {
			footerTxt = footerTxt + "\n" + line;
		}
		in.close();

		return footerTxt;
	}

}
