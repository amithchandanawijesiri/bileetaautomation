package bileeta.BTAF.PageObjects;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.sound.midi.Soundbank;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.formula.functions.Replace;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.ClickAction;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import bileeta.BATF.Pages.InventoryAndWarehouseModuleData;

public class InventoryAndWarehouseModule extends InventoryAndWarehouseModuleData {
	// Common Variables
	static String product_group;
	static String employee = "Test";
	static String warehouse_1;
	static String user = System.getProperty("user.name");

	// Common writeIWData
	private static Workbook wb;
	private static Sheet sh;
	private static FileInputStream fis;
	private static FileOutputStream fos;
	private static Row row;
	private static Cell cell;
	private static Cell cell1;
	private static Cell cell2;

	/* IW_TC_003 */
	Map<String, String> products_map = new HashMap<String, String>();
	List<Map<String, String>> products = new ArrayList<Map<String, String>>();

	/* IW_TC_004 */
	private static String warehouse_create;

	/* IW_TC_005 */
	static String[] product_availability = new String[7];
	static String[] product_availability_after = new String[7];

	// IW_TC_014
	static String beforeCheckoutUnits;

	// IW_TC_022
	private static String inboundShipmentDoc;

	/* IW_TC_021 */
	private static String changed_seriel;

	// IW_TC_023
	private static String inbound_shipment_qc_acceptance;

	// IW_TC_023
	public static String inbound_shipment_shipment_acceptance;

	/* AV_IW */
	private static String url_released_status;
	private static String url_new_status;
	private static String url_draft_status;

	/* Regression 01 */
	private static String current_status_url;

	/* IN_IO_008 */
	private static String oh_quantity;

	/* Common objects */
	genarateInventoryAndWarehouseInformation obj = new genarateInventoryAndWarehouseInformation();

	/* IN_IO_024 */
	boolean flag_partial_released = false;

	// --------------START LOGIN------------------

	public void navigateToTheLoginPage() throws Exception {
		openPage(siteURL);

	}

	public void verifyTheLogo() throws Exception {
		explicitWait(siteLogo, 40);
		if (isDisplayed(siteLogo)) {

			writeTestResults("Verify that 'Entution' header is available on the page",
					"user should be able to see the logo", "Logo is displayed sucessfully", "pass");
		} else {
			writeTestResults("Verify that 'Entution' header is available on the page",
					"user should be able to see the logo", "Logo does'nt displayed", "fail");
		}
	}

	public void userLogin() throws Exception {
		explicitWaitUntillClickable(txt_username, 40);
		sendKeys(txt_username, userNameData);

		if (isDisplayed(txt_username)) {
			writeTestResults("Verify user can enter a Username ", "user should be able to enter a Username",
					"User successfully enter a Username", "pass");
		} else {
			writeTestResults("Verify user can enter a Username ", "user should be able to enter a Username",
					"User couldn't enter a Username", "fail");
		}

		Thread.sleep(1000);
		sendKeys(txt_password, passwordData);
		if (isDisplayed(txt_password)) {
			writeTestResults("Verify user can enter a Password ", "user should be able to enter a Password",
					"User successfully enter a Password", "pass");
		} else {
			writeTestResults("Verify user can enter a Password ", "user should be able to enter a Password",
					"User couldn't enter a Password", "fail");
		}

		Thread.sleep(1000);
		click(btn_login);

//		Thread.sleep(6000);
//		driver.switchTo().alert().dismiss();

		explicitWait(lnk_home, 50);
		if (isDisplayed(lnk_home)) {

			writeTestResults("Verify that user can login to the 'Entution' ", "User should be login to the 'Entution'",
					"User login to the 'Entution' sucessfully", "pass");
		} else {
			writeTestResults("Verify that user can login to the 'Entution' ", "User should be login to the 'Entution'",
					"User does'nt login to the 'Entution'", "fail");

		}

	}

	// ---------------END LOGIN-------------------

	// Com_TC_002
	public void navigateInventoryAndWarehouseModule() throws Exception {
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		explicitWait(navigation_pane, 30);
		Thread.sleep(2000);
		explicitWait(navigation_pane, 30);
		click(navigation_pane);
		sleepCusomized(inventory_module);
		if (isDisplayed(inventory_module)) {
			writeTestResults("Verify user can navigate to navigation menu and able to view authorized modules ",
					"user should be able to navigate to navigation menu and able to view authorized modules",
					"User successfully navigate to navigation menu and able to view authorized modules", "pass");
		} else {
			writeTestResults("Verify user can navigate to navigation menu and able to view authorized modules ",
					"user should be able to navigate to navigation menu and able to view authorized modules",
					"User couldn't navigate to navigation menu and able to view authorized modules", "fail");
		}

		explicitWait(inventory_module, 7);
		click(inventory_module);
		if (isDisplayed(lnk_product_group_configuration)) {
			writeTestResults("Verify user can load Inventory module successfully",
					"user should be able load Inventory module successfully",
					"User successfully load Inventory module successfully", "pass");
		} else {
			writeTestResults("Verify user can load Inventory module successfully",
					"user should be able load Inventory module successfully",
					"User couldn't load Inventory module successfully", "fail");
		}
	}

	// IW_TC_001
	public void navigateNewProductGroupPage() throws Exception {
		navigateInventoryAndWarehouseModule();
		click(lnk_product_group_configuration);
		if (isDisplayed(header_productGroupConfigPage)) {
			writeTestResults("Verify user can navigate Product Group Configuration bypage",
					"Product Group Configuration bypage should be loaded",
					"Product Group Configuration bypage successfully loaded", "pass");
		} else {
			writeTestResults("Verify user can navigate Product Group Configuration bypage",
					"Product Group Configuration bypage should be loaded",
					"Product Group Configuration bypage does'nt loaded", "fail");
		}

		click(btn_new_group_configuration);
		if (isDisplayed(header_newProductGroupForm)) {
			writeTestResults("Verify user can navigate new Product Group Configuration form",
					"New Product Group Configuration form should be loaded",
					"New Product Group Configuration form successfully loaded", "pass");
		} else {
			writeTestResults("Verify user can navigate new Product Group Configuration form",
					"New Product Group Configuration form should be loaded",
					"New Product Group Configuration form does'nt loaded", "fail");
		}
	}

	public void configureNewProductGroup() throws Exception {
		click(btn_product_group_plus_mark_table);

		int rowCount = driver.findElements(By.xpath(tbl_productGroup)).size();

		for (int i = 1; i <= rowCount; i++) {
			// row count
		}

		final String finle_btn_product_group_plus_mark_second = btn_product_group_plus_mark_second;
		String plus_mark_second = finle_btn_product_group_plus_mark_second.replace("last_row",
				String.valueOf(rowCount));
		click(plus_mark_second);

		rowCount = rowCount + 1;
		final String finle_txt_product_group = txt_product_group;
		String txt_product_group_updated = finle_txt_product_group.replace("txt_last_row", String.valueOf(rowCount));
		product_group = "PG_GSTM" + currentTimeAndDate();
		sendKeys(txt_product_group_updated, product_group);

		click(btn_productGroupUpdate);

		Select comboBox = new Select(driver.findElement(By.xpath(dropdown_productGroupConfig)));
		selectText(dropdown_productGroupConfig, product_group);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		selectText(dropdown_productGroupConfig, product_group);

		if (selectedComboValue.equals(product_group)) {
			writeTestResults("Verify user fill mandatory fiealds", "User should be able to fill mandatory fiealds",
					"User successfully filled mandatory fiealds", "pass");
		} else {
			writeTestResults("Verify user fill mandatory fiealds", "User should be able to fill mandatory fiealds",
					"User couldn't filled mandatory fiealds", "fail");
		}
	}

	public void inventoryPurchaseAndSalesProductGroupConfig() throws Exception {
		click(tab_pg_details);

		click(btn_inventory_active);
		click(btn_product_function_confirmation);

		if (isDisplayed(header_warehouseInforProductGroupConfig)) {
			writeTestResults("Verify user Active Inventory", "User should be able to Active Inventory",
					"User successfully Active Inventory", "pass");
		} else {
			writeTestResults("Verify user Active Inventory", "User should be able to Active Inventory",
					"User couldn't Active Inventory", "fail");
		}

		click(btn_purchase_active);
		click(btn_purchaseActiveConfirmation);

		if (isDisplayed(header_producInfoProductGroupConfig)) {
			writeTestResults("Verify user Active Purchase Information",
					"User should be able to Active Purchase Information",
					"User successfully Active Purchase Information", "pass");
		} else {
			writeTestResults("Verify user Active Purchase Information",
					"User should be able to Active Purchase Information", "User couldn't Active Purchase Information",
					"fail");
		}
		click(btn_sales_active);
		click(btn_salesActiveConfirmation);
		if (isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			writeTestResults("Verify user Active Sales Information", "User should be able to Active Sales Information",
					"User successfully Active Sales Information", "pass");
		} else {
			writeTestResults("Verify user Active Sales Information", "User should be able to Active Sales Information",
					"User couldn't Active Sales Information", "fail");
		}

		// sample fields(not all)
		Select comboBox = new Select(driver.findElement(By.xpath(dropdown_outboundCostingMethodProductGroupConfig)));
		selectText(dropdown_outboundCostingMethodProductGroupConfig, outboundCostingMethodProductGroupConfig);
		String selectedOutboundCostingMethod = comboBox.getFirstSelectedOption().getText();
		selectText(dropdown_outboundCostingMethodProductGroupConfig, outboundCostingMethodProductGroupConfig);

		click(chk_allowInventoryWithoutCostingProductGroupConfig);
		click(chk_batchProductProductGroupConfig);
		sendKeys(txt_tollerenceProductGroupConfig, tollerenceProductGroupConfig);
		sendKeys(txt_minPriceProductGroupConfig, minPriceProductGroupConfig);
		click(txt_maxPriceProductGroupConfig);

		if (selectedOutboundCostingMethod.equals(outboundCostingMethodProductGroupConfig)) {

			if (isEnabled(chk_allowInventoryWithoutCostingProductGroupConfig)) {

				if (isEnabled(chk_batchProductProductGroupConfig)) {

					String tollerence_edit1 = getAttribute(txt_tollerenceProductGroupConfig, "tpv");

					String tollerence_edit2 = tollerence_edit1.replace("\"}", "");
					String tollerence_edit3 = tollerence_edit2.replace("\"", "");
					String tollerence_edit4 = tollerence_edit3.replace("{pv:,val:", "");

					if (tollerence_edit4.equals(tollerenceProductGroupConfig)) {
						String miniPrice1 = getAttribute(txt_minPriceProductGroupConfig, "tpv");
						String miniPrice2 = miniPrice1.replace("\"}", "");
						String miniPrice3 = miniPrice2.replace("\"", "");
						String miniPrice4 = miniPrice3.replace("{pv:,val:", "");

						if (miniPrice4.equals(minPriceProductGroupConfig)) {
							writeTestResults("Verify user can fill information in Details Tab",
									"User should be able to fill information in Details Tab",
									"User successfully fill information in Details Tab", "pass");
						} else {
							writeTestResults("Verify user can fill information in Details Tab",
									"User should be able to fill information in Details Tab",
									"User couldn't fill information in Details Tab", "fail");
						}
					} else {

					}
				} else {

				}
			} else {
			}

		} else {

		}
		writeIWData("Product Group", product_group, 1);
	}

	public void draftReleeseProductGroupConfig() throws Exception {
		draft("Product Group Configuration");
		relese("Product Group Configuration");
	}

	// IW_TC_002
	public void verifyThatUseeCanNavigateToTheProductInformationForm() throws Exception {
		navigateInventoryAndWarehouseModule();
		click(btn_productInformation);

		if (isDisplayed(header_productInformation)) {

			writeTestResults("Verify that product information page is availbale",
					"User should be navigate to the product information page",
					"User sucessfully navigate to the product information page", "pass");
		} else {
			writeTestResults("Verify that product information page is availbale",
					"User should be navigate to the product information page",
					"User does'nt navigate to the product information page", "fail");
		}
		click(btn_newProduct);
		sleepCusomized(dropdown_productGroup);
		if (isDisplayed(dropdown_productGroup)) {
			writeTestResults("Verify user can navigate to new product information by-page",
					"user should be able navigate to new product information by-page",
					"User successfully navigate to new product information by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to new product information by-page",
					"user should be able navigate to new product information by-page",
					"User couldn't navigate to new product information by-page", "fail");
		}
	}

	public void verifyProductGroupAvailabilty() throws Exception {
		selectText(dropdown_productGroup, product_group); // need get from product group config TC

		String lastCreatedProductGroup = readIWData(1);
		Select comboBox = new Select(driver.findElement(By.xpath(dropdown_productGroup)));
		selectText(dropdown_productGroup, lastCreatedProductGroup);
		String productGroup = comboBox.getFirstSelectedOption().getText();

		System.out.println(productGroup);
		System.out.println(lastCreatedProductGroup);
		if (productGroup.equals(lastCreatedProductGroup)) {
			writeTestResults("Verify that created product group is available",
					"User successfully find the created product group",
					"User sucessfully find the created product group", "pass");
		} else {
			writeTestResults("Verify that created product group is available",
					"User successfully find the created product group", "User does'nt find the created product group",
					"fail");
		}

		click(tab_detail);
		if (isDisplayed(header_inventoryBatchProduct)) {

			writeTestResults("Verify destils are load under details tab according to the selected Product Group",
					"Deatails should be loaded under details tab according to the selected Product Group",
					"Deatails successfully loaded under details tab according to the selected Product Group", "pass");
		} else {
			writeTestResults("Verify destils are load under details tab according to the selected Product Group",
					"Deatails should be loaded under details tab according to the selected Product Group",
					"Deatails does'nt loaded under details tab according to the selected Product Group", "fail");

		}

	}

	// IW_TC_003
	public void createBatchSpecificProduct() throws Exception {
		navigateInventoryAndWarehouseModule();
		sleepCusomized(btn_product_info);
		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		customizeLoadingDelay(btn_new_product, 8);
		if (isDisplayed(btn_new_product)) {

			writeTestResults("Verify that product information page is avilbale",
					"User should be navigate to the information page",
					"User sucessfully navigate to the product information page", "pass");
		} else {
			writeTestResults("Verify that product information page is avilbale",
					"User should be navigate to the information page",
					"User not navigate to the product information page", "Fail");
		}

		Thread.sleep(1500);
		click(btn_new_product);
		Thread.sleep(4000);
		if (isDisplayed(header_new_product)) {

			writeTestResults("Verify that product information form is availbale",
					"User should be navigate to the information form page",
					"User sucessfully navigate to the product information form", "pass");
		} else {
			writeTestResults("Verify that product information form is availbale",
					"User should be navigate to the information form",
					"User not navigate to the product information form", "Fail");

		}

		// product code
		sendKeys(txt_product_code, (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "-BATCH-SPECIFIC"));

		// product description
		sendKeys(txt_product_desc, productDesription);

		// product group
		/* productionGroupBatchProductCreation_noneed */
		selectText(dropdown_product_group, obj.readIWCreations("ProductGroup"));

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		String menufacturur = obj.readIWCreations("Menufacturer");
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturur);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		Thread.sleep(3000);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", menufacturur));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);
		if (isDisplayed(txt_product_code) && isDisplayed(txt_product_desc)
				&& isDisplayed(btn_menufactururSearchBatchProduct) && isDisplayed(dropdown_defoult_uom_group)
				&& isDisplayed(dropdown_defoult_uom)) {

			writeTestResults("Verify user can fill Mandatory Fields", "User should be able to fill Mandatory Fields",
					"User sucessfully fill Mandatory Fields", "pass");
		} else {
			writeTestResults("Verify user can fill Mandatory Fields", "User should be able to fill Mandatory Fields",
					"User couldn't fill Mandatory Fields", "fail");

		}

		// product width
		customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		if (isDisplayed(txt_width) && isDisplayed(drop_lengthUnitBtachProduct) && isDisplayed(txt_height)
				&& isDisplayed(drop_widthUnitBtachProduct) && isDisplayed(txt_length)
				&& isDisplayed(drop_heightUnitBtachProduct)) {

			writeTestResults("Verify user can fill Measurement Information",
					"User should be able to fill Measurement Information",
					"User sucessfully fill Measurement Information", "pass");
		} else {
			writeTestResults("Verify user can fill Measurement Information",
					"User should be able to fill Measurement Information", "User couldn't fill Measurement Information",
					"fail");

		}

		click(tab_detail);
		if (isDisplayed(header_inventoryBatchProduct)) {

			writeTestResults("Verify destils are load under details tab", "Deatails should be loaded under details tab",
					"Deatails successfully loaded under details tab", "pass");
		} else {
			writeTestResults("Verify destils are load under details tab", "Deatails should be loaded under details tab",
					"Deatails does'nt loaded under details tab", "fail");

		}
		customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);

		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}
		Thread.sleep(4000);
		if (isDisplayed(header_warehouseInforProductGroupConfig)) {
			writeTestResults("Verify user can Activate Inventory", "Deatails should be able to Activate Inventory",
					"Deatails successfully Activate Inventory", "pass");
		} else {
			writeTestResults("Verify user can Activate Inventory", "Deatails should be able to Activate Inventory",
					"Deatails couldn't Activate Inventory", "fail");

		}
		Thread.sleep(3000);
		click(chk_batchProductDispatchOnlyBatchProduct);
		Thread.sleep(3000);
		pageScrollDown();
		pageScrollUpToTop();
		selectText(dropdown_outbound_costing_method, outboundCostingMethod);
		Thread.sleep(3000);
		Select comboBox = new Select(driver.findElement(By.xpath(dropdown_outbound_costing_method)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		if (selectedComboValue.equals(outboundCostingMethod)) {
			writeTestResults("Verify user can select 'Specific' as Outbound Costing Method",
					"User should be able to select 'Specific' as Outbound Costing Method",
					"User successfully select 'Specific' as Outbound Costing Method", "pass");
		} else {

			selectText(dropdown_outbound_costing_method, outboundCostingMethod);
			Thread.sleep(2000);
			selectedComboValue = comboBox.getFirstSelectedOption().getText();
			if (selectedComboValue.equals(outboundCostingMethod)) {
				writeTestResults("Verify user can select 'Specific' as Outbound Costing Method",
						"User should be able to select 'Specific' as Outbound Costing Method",
						"User successfully select 'Specific' as Outbound Costing Method", "pass");

			} else {
				writeTestResults("Verify user can select 'Specific' as Outbound Costing Method",
						"User should be able to select 'Specific' as Outbound Costing Method",
						"User couldn't select 'Specific' as Outbound Costing Method", "fail");

			}

		}

		click(checkbox_without_costing);
		click(chk_allocationBatchProduct);

		if (isSelected(checkbox_without_costing) && isSelected(chk_allocationBatchProduct)) {
			writeTestResults("Verify user can enable 'Allow Inventory Without Costing' and 'Allow Allocation'",
					"User should be able to enable 'Allow Inventory Without Costing' and 'Allow Allocation",
					"User successfully enable 'Allow Inventory Without Costing' and 'Allow Allocation", "pass");
		} else {
			writeTestResults("Verify user can enable 'Allow Inventory Without Costing' and 'Allow Allocation'",
					"User should be able to enable 'Allow Inventory Without Costing' and 'Allow Allocation",
					"User couldn't enable 'Allow Inventory Without Costing' and 'Allow Allocation", "fail");

		}

		click(checkbox_validate_expiery);
		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (isSelected(checkbox_validate_expiery) && isDisplayed(txt_tollerence_date)) {
			writeTestResults("Verify user can enable 'Validate Expiry Date' and Enter 'Tollerence'",
					"User should be able to enable 'Validate Expiry Date' and Enter 'Tollerence'",
					"User successfully enable 'Validate Expiry Date' and Enter 'Tollerence'", "pass");
		} else {
			writeTestResults("Verify user can enable 'Validate Expiry Date' and Enter 'Tollerence'",
					"User should be able to enable 'Validate Expiry Date' and Enter 'Tollerence'",
					"User couldn't enable 'Validate Expiry Date' and Enter 'Tollerence'", "fail");

		}

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		if (isDisplayed(header_producInfoProductGroupConfig)) {
			writeTestResults("Verify user can Activate Purchase", "Deatails should be able to Activate Purchase",
					"Deatails successfully Activate Purchase", "pass");
		} else {
			writeTestResults("Verify user can Activate Purchase", "Deatails should be able to Activate Purchase",
					"Deatails couldn't Activate Purchase", "fail");

		}

		if (!isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}
		if (isSelected(checkbox_qc_acceptance)) {
			writeTestResults("Verify user can enable 'QC Acceptance'", "User should be able to enable 'QC Acceptance'",
					"User successfully enable 'QC Acceptance'", "pass");
		} else {
			writeTestResults("Verify user can enable 'QC Acceptance'", "User should be able to enable 'QC Acceptance'",
					"User couldn't enable 'QC Acceptance'", "fail");
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		if (isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			writeTestResults("Verify user can Activate Sales", "Deatails should be able to Activate Sales",
					"Deatails successfully Activate Sales", "pass");
		} else {
			writeTestResults("Verify user can Activate Sales", "Deatails should be able to Activate Sales",
					"Deatails couldn't Activate Sales", "fail");

		}

		Thread.sleep(1000);
		if (!isSelected(checkbox_validate_expiery_date)) {
			click(checkbox_validate_expiery_date);
		}
		Thread.sleep(1000);
		if (isSelected(checkbox_validate_expiery_date)) {
			writeTestResults("Verify user can enable Validate Expiry date checkbox'",
					"User should be able to enable Validate Expiry date checkbox'",
					"User successfully enable Validate Expiry date checkbox'", "pass");
		} else {
			writeTestResults("Verify user can enable Validate Expiry date checkbox'",
					"User should be able to enable Validate Expiry date checkbox'",
					"User couldn't enable Validate Expiry date checkbox'", "fail");
		}

		click(btn_draft);
		customizeLoadingDelay(btn_releese2, 20);
		if (isDisplayed(btn_releese2)) {

			writeTestResults("Verify user can draft a Batch Specific Product",
					"User should be able to draft Batch Specific Product",
					"User sucessfully draft a Batch Specific Productt", "pass");
		} else {
			writeTestResults("Verify user can draft a Batch Specific Product",
					"User should be able to draft Batch Specific Product", "User can't draft a Batch Specific Product",
					"Fail");

		}

		click(btn_releese2);
		Thread.sleep(1000);

		String header1 = getText(header_draft);
		if (!header1.equalsIgnoreCase("( Released )")) {
			Thread.sleep(7000);
			header1 = getText(header_draft);
		}
		if (header1.equalsIgnoreCase("( Released )")) {
			trackCode = getText(lbl_creationsDocNo);
			products_map.put("BatchSpecific", trackCode);
			products.add(0, products_map);
			writeTestResults("Verify user can release a Batch Specific Product",
					"User should be able to release Batch Specific Product",
					"User sucessfully release a Batch Specific Product", "pass");
		} else {
			writeTestResults("Verify user can release a Batch Specific Product",
					"User should be able to release Batch Specific Product",
					"User can't release a Batch Specific Product", "Fail");
		}
		writeTestCreations("BatchSpecific", products.get(0).get("BatchSpecific"), 0);
	}

	public void createBatchFifoProduct() throws Exception {
		sleepCusomized(navigation_pane);
		click(navigation_pane);
		sleepCusomized(inventory_module);
		if (isDisplayed(inventory_module)) {
			writeTestResults("Verify user can navigate to navigation menu and able to view authorized modules ",
					"user should be able to navigate to navigation menu and able to view authorized modules",
					"User successfully navigate to navigation menu and able to view authorized modules", "pass");
		} else {
			writeTestResults("Verify user can navigate to navigation menu and able to view authorized modules ",
					"user should be able to navigate to navigation menu and able to view authorized modules",
					"User couldn't navigate to navigation menu and able to view authorized modules", "fail");
		}

		click(inventory_module);
		if (isDisplayed(lnk_product_group_configuration)) {
			writeTestResults("Verify user can load Inventory module successfully",
					"user should be able load Inventory module successfully",
					"User successfully load Inventory module successfully", "pass");
		} else {
			writeTestResults("Verify user can load Inventory module successfully",
					"user should be able load Inventory module successfully",
					"User couldn't load Inventory module successfully", "fail");
		}
		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		customizeLoadingDelay(btn_new_product, 8);
		if (isDisplayed(btn_new_product)) {

			writeTestResults("Verify that product information page is avilbale",
					"User should be navigate to the information page",
					"User sucessfully navigate to the product information page", "pass");
		} else {
			writeTestResults("Verify that product information page is avilbale",
					"User should be navigate to the information page",
					"User not navigate to the product information page", "Fail");

		}

		Thread.sleep(1500);
		click(btn_new_product);
		sleepCusomized(header_new_product);
		if (isDisplayed(header_new_product)) {

			writeTestResults("Verify that product information form is availbale",
					"User should be navigate to the information form page",
					"User sucessfully navigate to the product information form", "pass");
		} else {
			writeTestResults("Verify that product information form is availbale",
					"User should be navigate to the information form",
					"User not navigate to the product information form", "Fail");

		}

		// product code
		sendKeys(txt_product_code, (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "-BATCH-FIFO"));

		// product description
		sendKeys(txt_product_desc, productDesription);

		// product group
		/* productionGroupBatchProductCreation_noneed */
		selectText(dropdown_product_group, obj.readIWCreations("ProductGroup"));

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		String menufacturur = obj.readIWCreations("Menufacturer");
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturur);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", menufacturur));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);
		if (isDisplayed(txt_product_code) && isDisplayed(txt_product_desc)
				&& isDisplayed(btn_menufactururSearchBatchProduct) && isDisplayed(dropdown_defoult_uom_group)
				&& isDisplayed(dropdown_defoult_uom)) {

			writeTestResults("Verify user can fill Mandatory Fields", "User should be able to fill Mandatory Fields",
					"User sucessfully fill Mandatory Fields", "pass");
		} else {
			writeTestResults("Verify user can fill Mandatory Fields", "User should be able to fill Mandatory Fields",
					"User couldn't fill Mandatory Fields", "fail");

		}
		// product width
		customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		if (isDisplayed(txt_width) && isDisplayed(drop_lengthUnitBtachProduct) && isDisplayed(txt_height)
				&& isDisplayed(drop_widthUnitBtachProduct) && isDisplayed(txt_length)
				&& isDisplayed(drop_heightUnitBtachProduct)) {

			writeTestResults("Verify user can fill Measurement Information",
					"User should be able to fill Measurement Information",
					"User sucessfully fill Measurement Information", "pass");
		} else {
			writeTestResults("Verify user can fill Measurement Information",
					"User should be able to fill Measurement Information", "User couldn't fill Measurement Information",
					"fail");

		}

		click(tab_detail);
		if (isDisplayed(header_inventoryBatchProduct)) {

			writeTestResults("Verify destils are load under details tab", "Deatails should be loaded under details tab",
					"Deatails successfully loaded under details tab", "pass");
		} else {
			writeTestResults("Verify destils are load under details tab", "Deatails should be loaded under details tab",
					"Deatails does'nt loaded under details tab", "fail");

		}
		customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);

		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		if (isDisplayed(header_warehouseInforProductGroupConfig)) {
			writeTestResults("Verify user can Activate Inventory", "Deatails should be able to Activate Inventory",
					"Deatails successfully Activate Inventory", "pass");
		} else {
			writeTestResults("Verify user can Activate Inventory", "Deatails should be able to Activate Inventory",
					"Deatails couldn't Activate Inventory", "fail");

		}

		click(chk_batchProductDispatchOnlyBatchProduct);
		Thread.sleep(3000);
		pageScrollDown();
		pageScrollUpToTop();
		selectText(dropdown_outbound_costing_method, "FIFO");
		Thread.sleep(2000);
		Select comboBox = new Select(driver.findElement(By.xpath(dropdown_outbound_costing_method)));
		Thread.sleep(1500);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		if (selectedComboValue.equals("FIFO")) {
			writeTestResults("Verify user can select 'FIFO' as Outbound Costing Method",
					"User should be able to select 'FIFO' as Outbound Costing Method",
					"User successfully select 'FIFO' as Outbound Costing Method", "pass");
		} else {
			writeTestResults("Verify user can select 'FIFO' as Outbound Costing Method",
					"User should be able to select 'FIFO' as Outbound Costing Method",
					"User couldn't select 'FIFO' as Outbound Costing Method", "fail");

		}

		click(checkbox_without_costing);
		click(chk_allocationBatchProduct);

		if (isSelected(checkbox_without_costing) && isSelected(chk_allocationBatchProduct)) {
			writeTestResults("Verify user can enable 'Allow Inventory Without Costing' and 'Allow Allocation'",
					"User should be able to enable 'Allow Inventory Without Costing' and 'Allow Allocation",
					"User successfully enable 'Allow Inventory Without Costing' and 'Allow Allocation", "pass");
		} else {
			writeTestResults("Verify user can enable 'Allow Inventory Without Costing' and 'Allow Allocation'",
					"User should be able to enable 'Allow Inventory Without Costing' and 'Allow Allocation",
					"User couldn't enable 'Allow Inventory Without Costing' and 'Allow Allocation", "fail");

		}

		click(checkbox_validate_expiery);
		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (isSelected(checkbox_validate_expiery) && isDisplayed(txt_tollerence_date)) {
			writeTestResults("Verify user can enable 'Validate Expiry Date' and Enter 'Tollerence'",
					"User should be able to enable 'Validate Expiry Date' and Enter 'Tollerence'",
					"User successfully enable 'Validate Expiry Date' and Enter 'Tollerence'", "pass");
		} else {
			writeTestResults("Verify user can enable 'Validate Expiry Date' and Enter 'Tollerence'",
					"User should be able to enable 'Validate Expiry Date' and Enter 'Tollerence'",
					"User couldn't enable 'Validate Expiry Date' and Enter 'Tollerence'", "fail");

		}

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		if (isDisplayed(header_producInfoProductGroupConfig)) {
			writeTestResults("Verify user can Activate Purchase", "Deatails should be able to Activate Purchase",
					"Deatails successfully Activate Purchase", "pass");
		} else {
			writeTestResults("Verify user can Activate Purchase", "Deatails should be able to Activate Purchase",
					"Deatails couldn't Activate Purchase", "fail");

		}

		if (!isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}
		if (isSelected(checkbox_qc_acceptance)) {
			writeTestResults("Verify user can enable 'QC Acceptance'", "User should be able to enable 'QC Acceptance'",
					"User successfully enable 'QC Acceptance'", "pass");
		} else {
			writeTestResults("Verify user can enable 'QC Acceptance'", "User should be able to enable 'QC Acceptance'",
					"User couldn't enable 'QC Acceptance'", "fail");
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		if (isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			writeTestResults("Verify user can Activate Sales", "Deatails should be able to Activate Sales",
					"Deatails successfully Activate Sales", "pass");
		} else {
			writeTestResults("Verify user can Activate Sales", "Deatails should be able to Activate Sales",
					"Deatails couldn't Activate Sales", "fail");

		}
		Thread.sleep(1000);
		if (!isSelected(checkbox_validate_expiery_date)) {
			click(checkbox_validate_expiery_date);
		}

		Thread.sleep(1000);
		if (isSelected(checkbox_validate_expiery_date)) {
			writeTestResults("Verify user can enable Validate Expiry date checkbox'",
					"User should be able to enable Validate Expiry date checkbox'",
					"User successfully enable Validate Expiry date checkbox'", "pass");
		} else {
			writeTestResults("Verify user can enable Validate Expiry date checkbox'",
					"User should be able to enable Validate Expiry date checkbox'",
					"User couldn't enable Validate Expiry date checkbox'", "fail");
		}

		click(btn_draft);
		customizeLoadingDelay(btn_releese2, 20);
		if (isDisplayed(btn_releese2)) {

			writeTestResults("Verify user can draft a Batch FIFO Product",
					"User should be able to draft Batch FIFO Product", "User sucessfully draft a Batch FIFO Productt",
					"pass");
		} else {
			writeTestResults("Verify user can draft a Batch FIFO Product",
					"User should be able to draft Batch FIFO Product", "User can't draft a Batch FIFO Product", "Fail");

		}

		click(btn_releese2);
		Thread.sleep(1000);
		String header1 = getText(header_draft);
		if (!header1.equalsIgnoreCase("( Released )")) {
			Thread.sleep(7000);
			header1 = getText(header_draft);
		}
		if (header1.equalsIgnoreCase("( Released )")) {
			trackCode = getText(lbl_creationsDocNo);
			products_map.put("BatchFifo", trackCode);
			products.add(0, products_map);
			writeTestResults("Verify user can release a Batch FIFO Product",
					"User should be able to release Batch FIFO Product",
					"User sucessfully release a Batch FIFO Product", "pass");
		} else {
			writeTestResults("Verify user can release a Batch FIFO Product",
					"User should be able to release Batch FIFO Product", "User can't release a Batch FIFO Product",
					"Fail");
		}

		writeTestCreations("BatchFifo", products.get(0).get("BatchFifo"), 1);
	}

	public void createLotProduct() throws Exception {
		sleepCusomized(navigation_pane);
		click(navigation_pane);
		sleepCusomized(inventory_module);
		if (isDisplayed(inventory_module)) {
			writeTestResults("Verify user can navigate to navigation menu and able to view authorized modules ",
					"user should be able to navigate to navigation menu and able to view authorized modules",
					"User successfully navigate to navigation menu and able to view authorized modules", "pass");
		} else {
			writeTestResults("Verify user can navigate to navigation menu and able to view authorized modules ",
					"user should be able to navigate to navigation menu and able to view authorized modules",
					"User couldn't navigate to navigation menu and able to view authorized modules", "fail");
		}

		click(inventory_module);
		if (isDisplayed(lnk_product_group_configuration)) {
			writeTestResults("Verify user can load Inventory module successfully",
					"user should be able load Inventory module successfully",
					"User successfully load Inventory module successfully", "pass");
		} else {
			writeTestResults("Verify user can load Inventory module successfully",
					"user should be able load Inventory module successfully",
					"User couldn't load Inventory module successfully", "fail");
		}
		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		customizeLoadingDelay(btn_new_product, 8);
		if (isDisplayed(btn_new_product)) {

			writeTestResults("Verify that product information page is avilbale",
					"User should be navigate to the information page",
					"User sucessfully navigate to the product information page", "pass");
		} else {
			writeTestResults("Verify that product information page is avilbale",
					"User should be navigate to the information page",
					"User not navigate to the product information page", "Fail");

		}

		Thread.sleep(1500);
		click(btn_new_product);
		sleepCusomized(header_new_product);
		if (isDisplayed(header_new_product)) {

			writeTestResults("Verify that product information form is availbale",
					"User should be navigate to the information form page",
					"User sucessfully navigate to the product information form", "pass");
		} else {
			writeTestResults("Verify that product information form is availbale",
					"User should be navigate to the information form",
					"User not navigate to the product information form", "Fail");

		}

		// product code
		sendKeys(txt_product_code, (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "-LOT"));

		// product description
		sendKeys(txt_product_desc, productDesription);

		// product group
		/* productionGroupBatchProductCreation_noneed */
		selectText(dropdown_product_group, obj.readIWCreations("ProductGroup"));

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		String menufacturur = obj.readIWCreations("Menufacturer");
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturur);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		Thread.sleep(3000);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", menufacturur));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);
		if (isDisplayed(txt_product_code) && isDisplayed(txt_product_desc)
				&& isDisplayed(btn_menufactururSearchBatchProduct) && isDisplayed(dropdown_defoult_uom_group)
				&& isDisplayed(dropdown_defoult_uom)) {

			writeTestResults("Verify user can fill Mandatory Fields", "User should be able to fill Mandatory Fields",
					"User sucessfully fill Mandatory Fields", "pass");
		} else {
			writeTestResults("Verify user can fill Mandatory Fields", "User should be able to fill Mandatory Fields",
					"User couldn't fill Mandatory Fields", "fail");

		}
		// product width
		customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		if (isDisplayed(txt_width) && isDisplayed(drop_lengthUnitBtachProduct) && isDisplayed(txt_height)
				&& isDisplayed(drop_widthUnitBtachProduct) && isDisplayed(txt_length)
				&& isDisplayed(drop_heightUnitBtachProduct)) {

			writeTestResults("Verify user can fill Measurement Information",
					"User should be able to fill Measurement Information",
					"User sucessfully fill Measurement Information", "pass");
		} else {
			writeTestResults("Verify user can fill Measurement Information",
					"User should be able to fill Measurement Information", "User couldn't fill Measurement Information",
					"fail");

		}

		click(tab_detail);
		if (isDisplayed(header_inventoryBatchProduct)) {

			writeTestResults("Verify destils are load under details tab", "Deatails should be loaded under details tab",
					"Deatails successfully loaded under details tab", "pass");
		} else {
			writeTestResults("Verify destils are load under details tab", "Deatails should be loaded under details tab",
					"Deatails does'nt loaded under details tab", "fail");

		}
		customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);

		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		if (isDisplayed(header_warehouseInforProductGroupConfig)) {
			writeTestResults("Verify user can Activate Inventory", "Deatails should be able to Activate Inventory",
					"Deatails successfully Activate Inventory", "pass");
		} else {
			writeTestResults("Verify user can Activate Inventory", "Deatails should be able to Activate Inventory",
					"Deatails couldn't Activate Inventory", "fail");

		}

		click(checkbox_without_costing);
		click(chk_allocationBatchProduct);

		if (isSelected(checkbox_without_costing) && isSelected(chk_allocationBatchProduct)) {
			writeTestResults("Verify user can enable 'Allow Inventory Without Costing' and 'Allow Allocation'",
					"User should be able to enable 'Allow Inventory Without Costing' and 'Allow Allocation",
					"User successfully enable 'Allow Inventory Without Costing' and 'Allow Allocation", "pass");
		} else {
			writeTestResults("Verify user can enable 'Allow Inventory Without Costing' and 'Allow Allocation'",
					"User should be able to enable 'Allow Inventory Without Costing' and 'Allow Allocation",
					"User couldn't enable 'Allow Inventory Without Costing' and 'Allow Allocation", "fail");

		}

		sendKeys(txt_tollerence_date, tollerenceDate);
		Thread.sleep(700);
		click(btn_roqBatchProduct);

		if (isDisplayed(txt_tollerence_date)) {
			writeTestResults("Verify user can Enter 'Tollerence'", "User should be able to Enter 'Tollerence'",
					"User successfully Enter 'Tollerence'", "pass");
		} else {
			writeTestResults("Verify user can Enter 'Tollerence'", "User should be able to Enter 'Tollerence'",
					"User couldn't Enter 'Tollerence'", "fail");

		}

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		if (isDisplayed(header_producInfoProductGroupConfig)) {
			writeTestResults("Verify user can Activate Purchase", "Deatails should be able to Activate Purchase",
					"Deatails successfully Activate Purchase", "pass");
		} else {
			writeTestResults("Verify user can Activate Purchase", "Deatails should be able to Activate Purchase",
					"Deatails couldn't Activate Purchase", "fail");

		}

		sleepCusomized(checkbox_qc_acceptance);
		if (!isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}
		if (isSelected(checkbox_qc_acceptance)) {
			writeTestResults("Verify user can enable 'QC Acceptance'", "User should be able to enable 'QC Acceptance'",
					"User successfully enable 'QC Acceptance'", "pass");
		} else {
			writeTestResults("Verify user can enable 'QC Acceptance'", "User should be able to enable 'QC Acceptance'",
					"User couldn't enable 'QC Acceptance'", "fail");
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		if (isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			writeTestResults("Verify user can Activate Sales", "Deatails should be able to Activate Sales",
					"Deatails successfully Activate Sales", "pass");
		} else {
			writeTestResults("Verify user can Activate Sales", "Deatails should be able to Activate Sales",
					"Deatails couldn't Activate Sales", "fail");

		}

		click(btn_draft);
		customizeLoadingDelay(btn_releese2, 20);
		if (isDisplayed(btn_releese2)) {

			writeTestResults("Verify user can draft a Lot Product", "User should be able to draft Lot Product",
					"User sucessfully draft a Lot Productt", "pass");
		} else {
			writeTestResults("Verify user can draft a Lot Product", "User should be able to draft Lot Product",
					"User can't draft a Lot Product", "Fail");

		}

		click(btn_releese2);
		Thread.sleep(1000);

		String header1 = getText(header_draft);
		if (!header1.equalsIgnoreCase("( Released )")) {
			Thread.sleep(7000);
			header1 = getText(header_draft);
		}
		if (header1.equalsIgnoreCase("( Released )")) {
			trackCode = getText(lbl_creationsDocNo);
			products_map.put("Lot", trackCode);
			products.add(0, products_map);
			writeTestResults("Verify user can release a Lot Product", "User should be able to release Lot Product",
					"User sucessfully release a Lot Product", "pass");
		} else {
			writeTestResults("Verify user can release a Lot Product", "User should be able to release Lot Product",
					"User can't release a Lot Product", "Fail");
		}
		writeTestCreations("Lot", products.get(0).get("Lot"), 2);
	}

	public void createSerielSpecificProduct() throws Exception {
		sleepCusomized(navigation_pane);
		click(navigation_pane);
		sleepCusomized(inventory_module);
		if (isDisplayed(inventory_module)) {
			writeTestResults("Verify user can navigate to navigation menu and able to view authorized modules ",
					"user should be able to navigate to navigation menu and able to view authorized modules",
					"User successfully navigate to navigation menu and able to view authorized modules", "pass");
		} else {
			writeTestResults("Verify user can navigate to navigation menu and able to view authorized modules ",
					"user should be able to navigate to navigation menu and able to view authorized modules",
					"User couldn't navigate to navigation menu and able to view authorized modules", "fail");
		}

		click(inventory_module);
		if (isDisplayed(lnk_product_group_configuration)) {
			writeTestResults("Verify user can load Inventory module successfully",
					"user should be able load Inventory module successfully",
					"User successfully load Inventory module successfully", "pass");
		} else {
			writeTestResults("Verify user can load Inventory module successfully",
					"user should be able load Inventory module successfully",
					"User couldn't load Inventory module successfully", "fail");
		}
		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		customizeLoadingDelay(btn_new_product, 8);
		if (isDisplayed(btn_new_product)) {

			writeTestResults("Verify that product information page is avilbale",
					"User should be navigate to the information page",
					"User sucessfully navigate to the product information page", "pass");
		} else {
			writeTestResults("Verify that product information page is avilbale",
					"User should be navigate to the information page",
					"User not navigate to the product information page", "Fail");

		}

		Thread.sleep(1500);
		click(btn_new_product);
		sleepCusomized(header_new_product);
		if (isDisplayed(header_new_product)) {

			writeTestResults("Verify that product information form is availbale",
					"User should be navigate to the information form page",
					"User sucessfully navigate to the product information form", "pass");
		} else {
			writeTestResults("Verify that product information form is availbale",
					"User should be navigate to the information form",
					"User not navigate to the product information form", "Fail");

		}

		// product code
		sendKeys(txt_product_code, (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "-SERIEL-SPECIFIC"));

		// product description
		sendKeys(txt_product_desc, productDesription);

		// product group
		/* productionGroupBatchProductCreation_noneed */
		selectText(dropdown_product_group, obj.readIWCreations("ProductGroup"));

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		String menufacturur = obj.readIWCreations("Menufacturer");
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturur);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", menufacturur));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);
		if (isDisplayed(txt_product_code) && isDisplayed(txt_product_desc)
				&& isDisplayed(btn_menufactururSearchBatchProduct) && isDisplayed(dropdown_defoult_uom_group)
				&& isDisplayed(dropdown_defoult_uom)) {

			writeTestResults("Verify user can fill Mandatory Fields", "User should be able to fill Mandatory Fields",
					"User sucessfully fill Mandatory Fields", "pass");
		} else {
			writeTestResults("Verify user can fill Mandatory Fields", "User should be able to fill Mandatory Fields",
					"User couldn't fill Mandatory Fields", "fail");

		}
		// product width
		customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		if (isDisplayed(txt_width) && isDisplayed(drop_lengthUnitBtachProduct) && isDisplayed(txt_height)
				&& isDisplayed(drop_widthUnitBtachProduct) && isDisplayed(txt_length)
				&& isDisplayed(drop_heightUnitBtachProduct)) {

			writeTestResults("Verify user can fill Measurement Information",
					"User should be able to fill Measurement Information",
					"User sucessfully fill Measurement Information", "pass");
		} else {
			writeTestResults("Verify user can fill Measurement Information",
					"User should be able to fill Measurement Information", "User couldn't fill Measurement Information",
					"fail");

		}

		click(tab_detail);
		if (isDisplayed(header_inventoryBatchProduct)) {

			writeTestResults("Verify destils are load under details tab", "Deatails should be loaded under details tab",
					"Deatails successfully loaded under details tab", "pass");
		} else {
			writeTestResults("Verify destils are load under details tab", "Deatails should be loaded under details tab",
					"Deatails does'nt loaded under details tab", "fail");

		}
		customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);

		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		if (isDisplayed(header_warehouseInforProductGroupConfig)) {
			writeTestResults("Verify user can Activate Inventory", "Deatails should be able to Activate Inventory",
					"Deatails successfully Activate Inventory", "pass");
		} else {
			writeTestResults("Verify user can Activate Inventory", "Deatails should be able to Activate Inventory",
					"Deatails couldn't Activate Inventory", "fail");

		}

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,-200)");
		click(btn_chkIsSeriel);
		Thread.sleep(3000);
		pageScrollDown();
		pageScrollUpToTop();
		selectText(dropdown_outbound_costing_method, outboundCostingMethod);
		Thread.sleep(2000);
		Select comboBox = new Select(driver.findElement(By.xpath(dropdown_outbound_costing_method)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		if (selectedComboValue.equals(outboundCostingMethod)) {
			writeTestResults("Verify user can select 'Specific' as Outbound Costing Method",
					"User should be able to select 'Specific' as Outbound Costing Method",
					"User successfully select 'Specific' as Outbound Costing Method", "pass");
		} else {
			selectText(dropdown_outbound_costing_method, outboundCostingMethod);
			Thread.sleep(2000);
			selectedComboValue = comboBox.getFirstSelectedOption().getText();
			if (selectedComboValue.equals(outboundCostingMethod)) {
				writeTestResults("Verify user can select 'Specific' as Outbound Costing Method",
						"User should be able to select 'Specific' as Outbound Costing Method",
						"User successfully select 'Specific' as Outbound Costing Method", "pass");
			} else {
				writeTestResults("Verify user can select 'Specific' as Outbound Costing Method",
						"User should be able to select 'Specific' as Outbound Costing Method",
						"User couldn't select 'Specific' as Outbound Costing Method", "fail");
			}
		}

		click(checkbox_without_costing);
		click(chk_allocationBatchProduct);

		if (isSelected(checkbox_without_costing) && isSelected(chk_allocationBatchProduct)) {
			writeTestResults("Verify user can enable 'Allow Inventory Without Costing' and 'Allow Allocation'",
					"User should be able to enable 'Allow Inventory Without Costing' and 'Allow Allocation",
					"User successfully enable 'Allow Inventory Without Costing' and 'Allow Allocation", "pass");
		} else {
			writeTestResults("Verify user can enable 'Allow Inventory Without Costing' and 'Allow Allocation'",
					"User should be able to enable 'Allow Inventory Without Costing' and 'Allow Allocation",
					"User couldn't enable 'Allow Inventory Without Costing' and 'Allow Allocation", "fail");

		}

		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (isDisplayed(txt_tollerence_date)) {
			writeTestResults("Verify user canEnter 'Tollerence'", "User should be able toEnter 'Tollerence'",
					"User successfullyEnter 'Tollerence'", "pass");
		} else {
			writeTestResults("Verify user canEnter 'Tollerence'", "User should be able toEnter 'Tollerence'",
					"User couldn'tEnter 'Tollerence'", "fail");

		}

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		if (isDisplayed(header_producInfoProductGroupConfig)) {
			writeTestResults("Verify user can Activate Purchase", "Deatails should be able to Activate Purchase",
					"Deatails successfully Activate Purchase", "pass");
		} else {
			writeTestResults("Verify user can Activate Purchase", "Deatails should be able to Activate Purchase",
					"Deatails couldn't Activate Purchase", "fail");
		}

		if (!isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}
		if (isSelected(checkbox_qc_acceptance)) {
			writeTestResults("Verify user can enable 'QC Acceptance'", "User should be able to enable 'QC Acceptance'",
					"User successfully enable 'QC Acceptance'", "pass");
		} else {
			writeTestResults("Verify user can enable 'QC Acceptance'", "User should be able to enable 'QC Acceptance'",
					"User couldn't enable 'QC Acceptance'", "fail");
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		if (isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			writeTestResults("Verify user can Activate Sales", "Deatails should be able to Activate Sales",
					"Deatails successfully Activate Sales", "pass");
		} else {
			writeTestResults("Verify user can Activate Sales", "Deatails should be able to Activate Sales",
					"Deatails couldn't Activate Sales", "fail");

		}

		click(btn_draft);
		customizeLoadingDelay(btn_releese2, 20);
		if (isDisplayed(btn_releese2)) {

			writeTestResults("Verify user can draft a Seriel Specific Product",
					"User should be able to draft Seriel Specific", "User sucessfully draft a Seriel Specific Productt",
					"pass");
		} else {
			writeTestResults("Verify user can draft a Seriel Specific Product",
					"User should be able to draft Seriel Specific Product",
					"User can't draft a Seriel Specific Product", "Fail");

		}

		click(btn_releese2);
		Thread.sleep(1000);

		String header1 = getText(header_draft);
		if (!header1.equalsIgnoreCase("( Released )")) {
			Thread.sleep(7000);
			header1 = getText(header_draft);
		}
		if (header1.equalsIgnoreCase("( Released )")) {
			trackCode = getText(lbl_creationsDocNo);
			products_map.put("SerirlSpecific", trackCode);
			products.add(0, products_map);
			writeTestResults("Verify user can release a Seriel Specific Product",
					"User should be able to release Seriel Specific Product",
					"User sucessfully release a Seriel Specific Product", "pass");
		} else {
			writeTestResults("Verify user can release a Seriel Specific Product",
					"User should be able to release Seriel Specific Product",
					"User can't release a Seriel Specific Product", "Fail");
		}
		writeTestCreations("SerielSpecific", products.get(0).get("SerirlSpecific"), 3);
	}

	public void createSerielBatchSpecificProduct() throws Exception {
		sleepCusomized(navigation_pane);
		click(navigation_pane);
		sleepCusomized(inventory_module);
		if (isDisplayed(inventory_module)) {
			writeTestResults("Verify user can navigate to navigation menu and able to view authorized modules ",
					"user should be able to navigate to navigation menu and able to view authorized modules",
					"User successfully navigate to navigation menu and able to view authorized modules", "pass");
		} else {
			writeTestResults("Verify user can navigate to navigation menu and able to view authorized modules ",
					"user should be able to navigate to navigation menu and able to view authorized modules",
					"User couldn't navigate to navigation menu and able to view authorized modules", "fail");
		}

		click(inventory_module);
		if (isDisplayed(lnk_product_group_configuration)) {
			writeTestResults("Verify user can load Inventory module successfully",
					"user should be able load Inventory module successfully",
					"User successfully load Inventory module successfully", "pass");
		} else {
			writeTestResults("Verify user can load Inventory module successfully",
					"user should be able load Inventory module successfully",
					"User couldn't load Inventory module successfully", "fail");
		}
		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		customizeLoadingDelay(btn_new_product, 8);
		if (isDisplayed(btn_new_product)) {

			writeTestResults("Verify that product information page is avilbale",
					"User should be navigate to the information page",
					"User sucessfully navigate to the product information page", "pass");
		} else {
			writeTestResults("Verify that product information page is avilbale",
					"User should be navigate to the information page",
					"User not navigate to the product information page", "Fail");

		}

		Thread.sleep(1500);
		click(btn_new_product);
		sleepCusomized(header_new_product);
		if (isDisplayed(header_new_product)) {

			writeTestResults("Verify that product information form is availbale",
					"User should be navigate to the information form page",
					"User sucessfully navigate to the product information form", "pass");
		} else {
			writeTestResults("Verify that product information form is availbale",
					"User should be navigate to the information form",
					"User not navigate to the product information form", "Fail");

		}

		// product code
		sendKeys(txt_product_code,
				(currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "-SERIEL-BATCH-SPECIFIC"));

		// product description
		sendKeys(txt_product_desc, productDesription);

		// product group
		/* productionGroupBatchProductCreation_noneed */
		selectText(dropdown_product_group, obj.readIWCreations("ProductGroup"));

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		String menufacturur = obj.readIWCreations("Menufacturer");
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturur);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", menufacturur));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);
		if (isDisplayed(txt_product_code) && isDisplayed(txt_product_desc)
				&& isDisplayed(btn_menufactururSearchBatchProduct) && isDisplayed(dropdown_defoult_uom_group)
				&& isDisplayed(dropdown_defoult_uom)) {

			writeTestResults("Verify user can fill Mandatory Fields", "User should be able to fill Mandatory Fields",
					"User sucessfully fill Mandatory Fields", "pass");
		} else {
			writeTestResults("Verify user can fill Mandatory Fields", "User should be able to fill Mandatory Fields",
					"User couldn't fill Mandatory Fields", "fail");

		}
		// product width
		customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		if (isDisplayed(txt_width) && isDisplayed(drop_lengthUnitBtachProduct) && isDisplayed(txt_height)
				&& isDisplayed(drop_widthUnitBtachProduct) && isDisplayed(txt_length)
				&& isDisplayed(drop_heightUnitBtachProduct)) {

			writeTestResults("Verify user can fill Measurement Information",
					"User should be able to fill Measurement Information",
					"User sucessfully fill Measurement Information", "pass");
		} else {
			writeTestResults("Verify user can fill Measurement Information",
					"User should be able to fill Measurement Information", "User couldn't fill Measurement Information",
					"fail");

		}

		click(tab_detail);
		if (isDisplayed(header_inventoryBatchProduct)) {

			writeTestResults("Verify destils are load under details tab", "Deatails should be loaded under details tab",
					"Deatails successfully loaded under details tab", "pass");
		} else {
			writeTestResults("Verify destils are load under details tab", "Deatails should be loaded under details tab",
					"Deatails does'nt loaded under details tab", "fail");

		}
		customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);

		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		if (isDisplayed(header_warehouseInforProductGroupConfig)) {
			writeTestResults("Verify user can Activate Inventory", "Deatails should be able to Activate Inventory",
					"Deatails successfully Activate Inventory", "pass");
		} else {
			writeTestResults("Verify user can Activate Inventory", "Deatails should be able to Activate Inventory",
					"Deatails couldn't Activate Inventory", "fail");

		}

		/*
		 * if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
		 * click(btn_inventoryActiveDispatchOnlyBatchProduct);
		 * click(btn_product_function_confirmation); }
		 */
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,-200)");
		click(btn_chkIsSeriel);
		click(chk_batchProductDispatchOnlyBatchProduct);
		Thread.sleep(3000);
		pageScrollDown();
		pageScrollUpToTop();
		selectText(dropdown_outbound_costing_method, outboundCostingMethod);
		Select comboBox = new Select(driver.findElement(By.xpath(dropdown_outbound_costing_method)));
		Thread.sleep(2000);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		if (selectedComboValue.equals(outboundCostingMethod)) {
			writeTestResults("Verify user can select 'Specific' as Outbound Costing Method",
					"User should be able to select 'Specific' as Outbound Costing Method",
					"User successfully select 'Specific' as Outbound Costing Method", "pass");
		} else {
			selectText(dropdown_outbound_costing_method, outboundCostingMethod);
			Thread.sleep(2000);
			selectedComboValue = comboBox.getFirstSelectedOption().getText();
			if (selectedComboValue.equals(outboundCostingMethod)) {
				writeTestResults("Verify user can select 'Specific' as Outbound Costing Method",
						"User should be able to select 'Specific' as Outbound Costing Method",
						"User successfully select 'Specific' as Outbound Costing Method", "pass");
			} else {
				writeTestResults("Verify user can select 'Specific' as Outbound Costing Method",
						"User should be able to select 'Specific' as Outbound Costing Method",
						"User couldn't select 'Specific' as Outbound Costing Method", "fail");
			}
		}

		click(checkbox_without_costing);
		click(chk_allocationBatchProduct);

		if (isSelected(checkbox_without_costing) && isSelected(chk_allocationBatchProduct)) {
			writeTestResults("Verify user can enable 'Allow Inventory Without Costing' and 'Allow Allocation'",
					"User should be able to enable 'Allow Inventory Without Costing' and 'Allow Allocation",
					"User successfully enable 'Allow Inventory Without Costing' and 'Allow Allocation", "pass");
		} else {
			writeTestResults("Verify user can enable 'Allow Inventory Without Costing' and 'Allow Allocation'",
					"User should be able to enable 'Allow Inventory Without Costing' and 'Allow Allocation",
					"User couldn't enable 'Allow Inventory Without Costing' and 'Allow Allocation", "fail");

		}

		click(checkbox_validate_expiery);
		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (isSelected(checkbox_validate_expiery) && isDisplayed(txt_tollerence_date)) {
			writeTestResults("Verify user can enable 'Validate Expiry Date' and Enter 'Tollerence'",
					"User should be able to enable 'Validate Expiry Date' and Enter 'Tollerence'",
					"User successfully enable 'Validate Expiry Date' and Enter 'Tollerence'", "pass");
		} else {
			writeTestResults("Verify user can enable 'Validate Expiry Date' and Enter 'Tollerence'",
					"User should be able to enable 'Validate Expiry Date' and Enter 'Tollerence'",
					"User couldn't enable 'Validate Expiry Date' and Enter 'Tollerence'", "fail");

		}

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		if (isDisplayed(header_producInfoProductGroupConfig)) {
			writeTestResults("Verify user can Activate Purchase", "Deatails should be able to Activate Purchase",
					"Deatails successfully Activate Purchase", "pass");
		} else {
			writeTestResults("Verify user can Activate Purchase", "Deatails should be able to Activate Purchase",
					"Deatails couldn't Activate Purchase", "fail");

		}

		if (!isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}
		if (isSelected(checkbox_qc_acceptance)) {
			writeTestResults("Verify user can enable 'QC Acceptance'", "User should be able to enable 'QC Acceptance'",
					"User successfully enable 'QC Acceptance'", "pass");
		} else {
			writeTestResults("Verify user can enable 'QC Acceptance'", "User should be able to enable 'QC Acceptance'",
					"User couldn't enable 'QC Acceptance'", "fail");
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		if (isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			writeTestResults("Verify user can Activate Sales", "Deatails should be able to Activate Sales",
					"Deatails successfully Activate Sales", "pass");
		} else {
			writeTestResults("Verify user can Activate Sales", "Deatails should be able to Activate Sales",
					"Deatails couldn't Activate Sales", "fail");

		}
		Thread.sleep(1000);
		if (!isSelected(checkbox_validate_expiery_date)) {
			click(checkbox_validate_expiery_date);
		}
		Thread.sleep(1000);
		if (isSelected(checkbox_validate_expiery_date)) {
			writeTestResults("Verify user can enable Validate Expiry date checkbox'",
					"User should be able to enable Validate Expiry date checkbox'",
					"User successfully enable Validate Expiry date checkbox'", "pass");
		} else {
			writeTestResults("Verify user can enable Validate Expiry date checkbox'",
					"User should be able to enable Validate Expiry date checkbox'",
					"User couldn't enable Validate Expiry date checkbox'", "fail");
		}

		click(btn_draft);
		customizeLoadingDelay(btn_releese2, 20);
		if (isDisplayed(btn_releese2)) {

			writeTestResults("Verify user can draft a Seriel Batch Specific Product",
					"User should be able to draft Seriel Batch Specific Product",
					"User sucessfully draft a Seriel Batch Specific Productt", "pass");
		} else {
			writeTestResults("Verify user can draft a Seriel Batch Specific Product",
					"User should be able to draft Seriel Batch Specific Product",
					"User can't draft a Seriel Batch Specific Product", "Fail");

		}

		click(btn_releese2);
		Thread.sleep(1000);

		String header1 = getText(header_draft);
		if (!header1.equalsIgnoreCase("( Released )")) {
			Thread.sleep(7000);
			header1 = getText(header_draft);
		}
		if (header1.equalsIgnoreCase("( Released )")) {
			trackCode = getText(lbl_creationsDocNo);
			products_map.put("SerielBatchSpecific", trackCode);
			products.add(0, products_map);
			writeTestResults("Verify user can release a Seriel Batch Specific Product",
					"User should be able to release Seriel Batch Specific Product",
					"User sucessfully release a Seriel Batch Specific Product", "pass");
		} else {
			writeTestResults("Verify user can release a Seriel Batch Specific Product",
					"User should be able to release Seriel Batch Specific Product",
					"User can't release a Seriel Batch Specific Product", "Fail");
		}
		writeTestCreations("SerielBatchSpecific", products.get(0).get("SerielBatchSpecific"), 4);
	}

	public void createSerielBatchFifoProduct() throws Exception {
		sleepCusomized(navigation_pane);
		click(navigation_pane);
		sleepCusomized(inventory_module);
		if (isDisplayed(inventory_module)) {
			writeTestResults("Verify user can navigate to navigation menu and able to view authorized modules ",
					"user should be able to navigate to navigation menu and able to view authorized modules",
					"User successfully navigate to navigation menu and able to view authorized modules", "pass");
		} else {
			writeTestResults("Verify user can navigate to navigation menu and able to view authorized modules ",
					"user should be able to navigate to navigation menu and able to view authorized modules",
					"User couldn't navigate to navigation menu and able to view authorized modules", "fail");
		}

		click(inventory_module);
		if (isDisplayed(lnk_product_group_configuration)) {
			writeTestResults("Verify user can load Inventory module successfully",
					"user should be able load Inventory module successfully",
					"User successfully load Inventory module successfully", "pass");
		} else {
			writeTestResults("Verify user can load Inventory module successfully",
					"user should be able load Inventory module successfully",
					"User couldn't load Inventory module successfully", "fail");
		}
		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		customizeLoadingDelay(btn_new_product, 8);
		if (isDisplayed(btn_new_product)) {

			writeTestResults("Verify that product information page is avilbale",
					"User should be navigate to the information page",
					"User sucessfully navigate to the product information page", "pass");
		} else {
			writeTestResults("Verify that product information page is avilbale",
					"User should be navigate to the information page",
					"User not navigate to the product information page", "Fail");

		}

		Thread.sleep(1500);
		click(btn_new_product);
		sleepCusomized(header_new_product);
		if (isDisplayed(header_new_product)) {

			writeTestResults("Verify that product information form is availbale",
					"User should be navigate to the information form page",
					"User sucessfully navigate to the product information form", "pass");
		} else {
			writeTestResults("Verify that product information form is availbale",
					"User should be navigate to the information form",
					"User not navigate to the product information form", "Fail");

		}

		// product code
		sendKeys(txt_product_code,
				(currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "-SERIEL-BATCH-FIFO"));

		// product description
		sendKeys(txt_product_desc, productDesription);

		// product group
		/* productionGroupBatchProductCreation_noneed */
		selectText(dropdown_product_group, obj.readIWCreations("ProductGroup"));

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		String menufacturur = obj.readIWCreations("Menufacturer");
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturur);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", menufacturur));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);
		if (isDisplayed(txt_product_code) && isDisplayed(txt_product_desc)
				&& isDisplayed(btn_menufactururSearchBatchProduct) && isDisplayed(dropdown_defoult_uom_group)
				&& isDisplayed(dropdown_defoult_uom)) {

			writeTestResults("Verify user can fill Mandatory Fields", "User should be able to fill Mandatory Fields",
					"User sucessfully fill Mandatory Fields", "pass");
		} else {
			writeTestResults("Verify user can fill Mandatory Fields", "User should be able to fill Mandatory Fields",
					"User couldn't fill Mandatory Fields", "fail");

		}
		// product width
		customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		if (isDisplayed(txt_width) && isDisplayed(drop_lengthUnitBtachProduct) && isDisplayed(txt_height)
				&& isDisplayed(drop_widthUnitBtachProduct) && isDisplayed(txt_length)
				&& isDisplayed(drop_heightUnitBtachProduct)) {

			writeTestResults("Verify user can fill Measurement Information",
					"User should be able to fill Measurement Information",
					"User sucessfully fill Measurement Information", "pass");
		} else {
			writeTestResults("Verify user can fill Measurement Information",
					"User should be able to fill Measurement Information", "User couldn't fill Measurement Information",
					"fail");

		}

		click(tab_detail);
		if (isDisplayed(header_inventoryBatchProduct)) {

			writeTestResults("Verify destils are load under details tab", "Deatails should be loaded under details tab",
					"Deatails successfully loaded under details tab", "pass");
		} else {
			writeTestResults("Verify destils are load under details tab", "Deatails should be loaded under details tab",
					"Deatails does'nt loaded under details tab", "fail");

		}
		customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);

		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		if (isDisplayed(header_warehouseInforProductGroupConfig)) {
			writeTestResults("Verify user can Activate Inventory", "Deatails should be able to Activate Inventory",
					"Deatails successfully Activate Inventory", "pass");
		} else {
			writeTestResults("Verify user can Activate Inventory", "Deatails should be able to Activate Inventory",
					"Deatails couldn't Activate Inventory", "fail");

		}
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,-200)");
		click(btn_chkIsSeriel);
		click(chk_batchProductDispatchOnlyBatchProduct);
		Thread.sleep(3000);
		pageScrollDown();
		pageScrollUpToTop();
		selectText(dropdown_outbound_costing_method, "FIFO");
		Select comboBox = new Select(driver.findElement(By.xpath(dropdown_outbound_costing_method)));
		Thread.sleep(1500);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		if (selectedComboValue.equals("FIFO")) {
			writeTestResults("Verify user can select 'FIFO' as Outbound Costing Method",
					"User should be able to select 'FIFO' as Outbound Costing Method",
					"User successfully select 'FIFO' as Outbound Costing Method", "pass");
		} else {
			writeTestResults("Verify user can select 'FIFO' as Outbound Costing Method",
					"User should be able to select 'FIFO' as Outbound Costing Method",
					"User couldn't select 'FIFO' as Outbound Costing Method", "fail");

		}

		click(checkbox_without_costing);
		click(chk_allocationBatchProduct);

		if (isSelected(checkbox_without_costing) && isSelected(chk_allocationBatchProduct)) {
			writeTestResults("Verify user can enable 'Allow Inventory Without Costing' and 'Allow Allocation'",
					"User should be able to enable 'Allow Inventory Without Costing' and 'Allow Allocation",
					"User successfully enable 'Allow Inventory Without Costing' and 'Allow Allocation", "pass");
		} else {
			writeTestResults("Verify user can enable 'Allow Inventory Without Costing' and 'Allow Allocation'",
					"User should be able to enable 'Allow Inventory Without Costing' and 'Allow Allocation",
					"User couldn't enable 'Allow Inventory Without Costing' and 'Allow Allocation", "fail");

		}

		click(checkbox_validate_expiery);
		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (isSelected(checkbox_validate_expiery) && isDisplayed(txt_tollerence_date)) {
			writeTestResults("Verify user can enable 'Validate Expiry Date' and Enter 'Tollerence'",
					"User should be able to enable 'Validate Expiry Date' and Enter 'Tollerence'",
					"User successfully enable 'Validate Expiry Date' and Enter 'Tollerence'", "pass");
		} else {
			writeTestResults("Verify user can enable 'Validate Expiry Date' and Enter 'Tollerence'",
					"User should be able to enable 'Validate Expiry Date' and Enter 'Tollerence'",
					"User couldn't enable 'Validate Expiry Date' and Enter 'Tollerence'", "fail");

		}
		Thread.sleep(1500);
		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			Thread.sleep(1500);
			click(btn_purchase_active);
			Thread.sleep(1500);
			click(btn_purchaseActiveConfirmation);
		}

		if (isDisplayed(header_producInfoProductGroupConfig)) {
			writeTestResults("Verify user can Activate Purchase", "Deatails should be able to Activate Purchase",
					"Deatails successfully Activate Purchase", "pass");
		} else {
			writeTestResults("Verify user can Activate Purchase", "Deatails should be able to Activate Purchase",
					"Deatails couldn't Activate Purchase", "fail");

		}

		if (!isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}
		if (isSelected(checkbox_qc_acceptance)) {
			writeTestResults("Verify user can enable 'QC Acceptance'", "User should be able to enable 'QC Acceptance'",
					"User successfully enable 'QC Acceptance'", "pass");
		} else {
			writeTestResults("Verify user can enable 'QC Acceptance'", "User should be able to enable 'QC Acceptance'",
					"User couldn't enable 'QC Acceptance'", "fail");
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		if (isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			writeTestResults("Verify user can Activate Sales", "Deatails should be able to Activate Sales",
					"Deatails successfully Activate Sales", "pass");
		} else {
			writeTestResults("Verify user can Activate Sales", "Deatails should be able to Activate Sales",
					"Deatails couldn't Activate Sales", "fail");

		}
		Thread.sleep(1000);
		if (!isSelected(checkbox_validate_expiery_date)) {
			click(checkbox_validate_expiery_date);
		}
		Thread.sleep(1000);
		if (isSelected(checkbox_validate_expiery_date)) {
			writeTestResults("Verify user can enable Validate Expiry date checkbox'",
					"User should be able to enable Validate Expiry date checkbox'",
					"User successfully enable Validate Expiry date checkbox'", "pass");
		} else {
			writeTestResults("Verify user can enable Validate Expiry date checkbox'",
					"User should be able to enable Validate Expiry date checkbox'",
					"User couldn't enable Validate Expiry date checkbox'", "fail");
		}

		click(btn_draft);
		customizeLoadingDelay(btn_releese2, 20);
		if (isDisplayed(btn_releese2)) {

			writeTestResults("Verify user can draft a Seriel Batch Fifo Product",
					"User should be able to draft Seriel Batch Fifo Product",
					"User sucessfully draft a Seriel Batch Fifo Productt", "pass");
		} else {
			writeTestResults("Verify user can draft a Seriel Batch Fifo Product",
					"User should be able to draft Seriel Batch Fifo Product",
					"User can't draft a Seriel Batch Fifo Product", "Fail");

		}

		click(btn_releese2);
		Thread.sleep(1000);

		String header1 = getText(header_draft);
		if (!header1.equalsIgnoreCase("( Released )")) {
			Thread.sleep(7000);
			header1 = getText(header_draft);
		}
		if (header1.equalsIgnoreCase("( Released )")) {
			trackCode = getText(lbl_creationsDocNo);
			products_map.put("SerielBatchFifo", trackCode);
			products.add(0, products_map);
			writeTestResults("Verify user can release a Seriel Batch Fifo Product",
					"User should be able to release Seriel Batch Fifo Product",
					"User sucessfully release a Seriel Batch Fifo Product", "pass");
		} else {
			writeTestResults("Verify user can release a Seriel Batch Fifo Product",
					"User should be able to release Seriel Batch Fifo Product",
					"User can't release a Seriel Batch Fifo Product", "Fail");
		}
		writeTestCreations("SerielBatchFifo", products.get(0).get("SerielBatchFifo"), 5);
	}

	public void createSerielFifoProduct() throws Exception {
		sleepCusomized(navigation_pane);
		click(navigation_pane);
		sleepCusomized(inventory_module);
		if (isDisplayed(inventory_module)) {
			writeTestResults("Verify user can navigate to navigation menu and able to view authorized modules ",
					"user should be able to navigate to navigation menu and able to view authorized modules",
					"User successfully navigate to navigation menu and able to view authorized modules", "pass");
		} else {
			writeTestResults("Verify user can navigate to navigation menu and able to view authorized modules ",
					"user should be able to navigate to navigation menu and able to view authorized modules",
					"User couldn't navigate to navigation menu and able to view authorized modules", "fail");
		}

		click(inventory_module);
		if (isDisplayed(lnk_product_group_configuration)) {
			writeTestResults("Verify user can load Inventory module successfully",
					"user should be able load Inventory module successfully",
					"User successfully load Inventory module successfully", "pass");
		} else {
			writeTestResults("Verify user can load Inventory module successfully",
					"user should be able load Inventory module successfully",
					"User couldn't load Inventory module successfully", "fail");
		}
		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		customizeLoadingDelay(btn_new_product, 8);
		if (isDisplayed(btn_new_product)) {

			writeTestResults("Verify that product information page is avilbale",
					"User should be navigate to the information page",
					"User sucessfully navigate to the product information page", "pass");
		} else {
			writeTestResults("Verify that product information page is avilbale",
					"User should be navigate to the information page",
					"User not navigate to the product information page", "Fail");

		}

		Thread.sleep(1500);
		click(btn_new_product);
		sleepCusomized(header_new_product);
		if (isDisplayed(header_new_product)) {

			writeTestResults("Verify that product information form is availbale",
					"User should be navigate to the information form page",
					"User sucessfully navigate to the product information form", "pass");
		} else {
			writeTestResults("Verify that product information form is availbale",
					"User should be navigate to the information form",
					"User not navigate to the product information form", "Fail");

		}

		// product code
		sendKeys(txt_product_code, (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "-SERIEL-FIFO"));

		// product description
		sendKeys(txt_product_desc, productDesription);

		// product group
		/* productionGroupBatchProductCreation_noneed */
		selectText(dropdown_product_group, obj.readIWCreations("ProductGroup"));

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		String menufacturur = obj.readIWCreations("Menufacturer");
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturur);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", menufacturur));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);
		if (isDisplayed(txt_product_code) && isDisplayed(txt_product_desc)
				&& isDisplayed(btn_menufactururSearchBatchProduct) && isDisplayed(dropdown_defoult_uom_group)
				&& isDisplayed(dropdown_defoult_uom)) {

			writeTestResults("Verify user can fill Mandatory Fields", "User should be able to fill Mandatory Fields",
					"User sucessfully fill Mandatory Fields", "pass");
		} else {
			writeTestResults("Verify user can fill Mandatory Fields", "User should be able to fill Mandatory Fields",
					"User couldn't fill Mandatory Fields", "fail");

		}
		// product width
		customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		if (isDisplayed(txt_width) && isDisplayed(drop_lengthUnitBtachProduct) && isDisplayed(txt_height)
				&& isDisplayed(drop_widthUnitBtachProduct) && isDisplayed(txt_length)
				&& isDisplayed(drop_heightUnitBtachProduct)) {

			writeTestResults("Verify user can fill Measurement Information",
					"User should be able to fill Measurement Information",
					"User sucessfully fill Measurement Information", "pass");
		} else {
			writeTestResults("Verify user can fill Measurement Information",
					"User should be able to fill Measurement Information", "User couldn't fill Measurement Information",
					"fail");

		}

		click(tab_detail);
		if (isDisplayed(header_inventoryBatchProduct)) {

			writeTestResults("Verify destils are load under details tab", "Deatails should be loaded under details tab",
					"Deatails successfully loaded under details tab", "pass");
		} else {
			writeTestResults("Verify destils are load under details tab", "Deatails should be loaded under details tab",
					"Deatails does'nt loaded under details tab", "fail");

		}
		customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);

		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		if (isDisplayed(header_warehouseInforProductGroupConfig)) {
			writeTestResults("Verify user can Activate Inventory", "Deatails should be able to Activate Inventory",
					"Deatails successfully Activate Inventory", "pass");
		} else {
			writeTestResults("Verify user can Activate Inventory", "Deatails should be able to Activate Inventory",
					"Deatails couldn't Activate Inventory", "fail");

		}
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,-200)");
		click(btn_chkIsSeriel);
		Thread.sleep(3000);
		pageScrollDown();
		pageScrollUpToTop();
		selectText(dropdown_outbound_costing_method, "FIFO");
		Select comboBox = new Select(driver.findElement(By.xpath(dropdown_outbound_costing_method)));
		Thread.sleep(2000);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		if (selectedComboValue.equals("FIFO")) {
			writeTestResults("Verify user can select 'FIFO' as Outbound Costing Method",
					"User should be able to select 'FIFO' as Outbound Costing Method",
					"User successfully select 'FIFO' as Outbound Costing Method", "pass");
		} else {
			writeTestResults("Verify user can select 'FIFO' as Outbound Costing Method",
					"User should be able to select 'FIFO' as Outbound Costing Method",
					"User couldn't select 'FIFO' as Outbound Costing Method", "fail");

		}

		click(checkbox_without_costing);
		click(chk_allocationBatchProduct);

		if (isSelected(checkbox_without_costing) && isSelected(chk_allocationBatchProduct)) {
			writeTestResults("Verify user can enable 'Allow Inventory Without Costing' and 'Allow Allocation'",
					"User should be able to enable 'Allow Inventory Without Costing' and 'Allow Allocation",
					"User successfully enable 'Allow Inventory Without Costing' and 'Allow Allocation", "pass");
		} else {
			writeTestResults("Verify user can enable 'Allow Inventory Without Costing' and 'Allow Allocation'",
					"User should be able to enable 'Allow Inventory Without Costing' and 'Allow Allocation",
					"User couldn't enable 'Allow Inventory Without Costing' and 'Allow Allocation", "fail");

		}

		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (isDisplayed(txt_tollerence_date)) {
			writeTestResults("Verify user canEnter 'Tollerence'", "User should be able toEnter 'Tollerence'",
					"User successfullyEnter 'Tollerence'", "pass");
		} else {
			writeTestResults("Verify user canEnter 'Tollerence'", "User should be able toEnter 'Tollerence'",
					"User couldn'tEnter 'Tollerence'", "fail");

		}

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		if (isDisplayed(header_producInfoProductGroupConfig)) {
			writeTestResults("Verify user can Activate Purchase", "Deatails should be able to Activate Purchase",
					"Deatails successfully Activate Purchase", "pass");
		} else {
			writeTestResults("Verify user can Activate Purchase", "Deatails should be able to Activate Purchase",
					"Deatails couldn't Activate Purchase", "fail");
		}

		if (!isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}
		if (isSelected(checkbox_qc_acceptance)) {
			writeTestResults("Verify user can enable 'QC Acceptance'", "User should be able to enable 'QC Acceptance'",
					"User successfully enable 'QC Acceptance'", "pass");
		} else {
			writeTestResults("Verify user can enable 'QC Acceptance'", "User should be able to enable 'QC Acceptance'",
					"User couldn't enable 'QC Acceptance'", "fail");
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		if (isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			writeTestResults("Verify user can Activate Sales", "Deatails should be able to Activate Sales",
					"Deatails successfully Activate Sales", "pass");
		} else {
			writeTestResults("Verify user can Activate Sales", "Deatails should be able to Activate Sales",
					"Deatails couldn't Activate Sales", "fail");

		}

		click(btn_draft);
		customizeLoadingDelay(btn_releese2, 20);
		if (isDisplayed(btn_releese2)) {

			writeTestResults("Verify user can draft a Seriel FIFO Product",
					"User should be able to draft Seriel FIFO Product", "User sucessfully draft a Seriel FIFO Productt",
					"pass");
		} else {
			writeTestResults("Verify user can draft a Seriel FIFO Product",
					"User should be able to draft Seriel FIFO Product", "User can't draft a Seriel FIFO Product",
					"Fail");

		}

		click(btn_releese2);
		Thread.sleep(1000);
		String header1 = getText(header_draft);
		if (!header1.equalsIgnoreCase("( Released )")) {
			Thread.sleep(7000);
			header1 = getText(header_draft);
		}
		if (header1.equalsIgnoreCase("( Released )")) {
			trackCode = getText(lbl_creationsDocNo);
			products_map.put("SerirlFifo", trackCode);
			products.add(0, products_map);
			writeTestResults("Verify user can release a Seriel FIFO Product",
					"User should be able to release Seriel FIFO Product",
					"User sucessfully release a Seriel FIFO Product", "pass");
		} else {
			writeTestResults("Verify user can release a Seriel FIFO Product",
					"User should be able to release Seriel FIFO Product", "User can't release a Seriel FIFO Product",
					"Fail");
		}
		writeTestCreations("SerirlFifo", products.get(0).get("SerirlFifo"), 6);
	}

	// IW_TC_004
	public void verifyNewWarehouseCretionFormLoading() throws Exception {
		navigateInventoryAndWarehouseModule();
		click(btn_warehouseInfo);
		if (isDisplayed(header_warehouseInformationPage)) {

			writeTestResults("Verify user can navigate to the warehouse creation page",
					"User should be able to navigate the warehouse creation page",
					"User sucessfully navigate to the warehouse creation page", "pass");
		} else {
			writeTestResults("Verify user can navigate to the warehouse creation page",
					"User should be able to navigate the warehouse creation page",
					"User not navigate to the warehouse creation page", "Fail");
		}

		click(btn_newWarehouse);

		if (isDisplayed(header_warehouseCretionForm)) {

			writeTestResults("Verify user can navigate to the warehouse creation form",
					"User should be able to navigate the warehouse creation form",
					"User sucessfully navigate to the warehouse creation form", "pass");
		} else {
			writeTestResults("Verify user can navigate to the warehouse creation form",
					"User should be able to navigate the warehouse creation form",
					"User not navigate to the warehouse creation form", "Fail");
		}
	}

	public void fillWarehouseForm() throws Exception {
		warehouse_create = currentTimeAndDate().replaceAll("/", "").replaceAll(":", "");
		sleepCusomized(txt_warehouseCode);
		sendKeys(txt_warehouseCode, warehouse_create);

		sendKeys(txt_warehouseName, warehouseName);

		selectText(dropdown_businessUnit, businessUnit);

		if (isDisplayed(txt_warehouseCode) && isDisplayed(txt_warehouseName) && isDisplayed(dropdown_businessUnit)) {

			writeTestResults("Verify user can fill mandatory fields", "User should be able to fill mandatory fields",
					"User sucessfully fill mandatory fields", "pass");
		} else {
			writeTestResults("Verify user can fill mandatory fields", "User should be able to fill mandatory fields",
					"User couldn't fill mandatory fields", "fail");
		}

		if (!isSelected(checkbox_isAllocation)) {
			click(checkbox_isAllocation);
		}

		if (isSelected(checkbox_isAllocation)) {

			writeTestResults("Verify user enable is 'Is Allocation Store' checkbox",
					"User should be able to enable is 'Is Allocation Store' checkbox",
					"User sucessfully enable is 'Is Allocation Store' checkbox", "pass");
		} else {
			writeTestResults("Verify user enable is 'Is Allocation Store' checkbox",
					"User should be able to enable is 'Is Allocation Store' checkbox",
					"User couldn't enable is 'Is Allocation Store' checkbox", "fail");
		}

		selectText(dropdown_qurantineWarehouse, qurantineWarehouse);
		if (isDisplayed(dropdown_qurantineWarehouse)) {

			writeTestResults("Verify user select 'Qurantine Warehouse'",
					"User should be able to select 'Qurantine Warehouse'",
					"User sucessfully select 'Qurantine Warehouse'", "pass");
		} else {
			writeTestResults("Verify user select 'Qurantine Warehouse'",
					"User should be able to select 'Qurantine Warehouse'", "User couldn't select 'Qurantine Warehouse'",
					"fail");
		}

		if (!isSelected(checkbox_autoLot)) {
			click(checkbox_autoLot);
		}

		if (isSelected(checkbox_autoLot)) {

			writeTestResults("Verify user enable is 'Auto Generate Lot No' checkbox",
					"User should be able to enable is 'Auto Generate Lot No' checkbox",
					"User sucessfully enable is 'Auto Generate Lot Noe' checkbox", "pass");
		} else {
			writeTestResults("Verify user enable is 'Auto Generate Lot No' checkbox",
					"User should be able to enable is 'Auto Generate Lot No' checkbox",
					"User couldn't enable is 'Auto Generate Lot No' checkbox", "fail");
		}

		selectText(dropdown_lotBook, lotBookWarehouse);
		if (isDisplayed(dropdown_lotBook)) {

			writeTestResults("Verify user select 'Lot Book No'", "User should be able to select 'Lot Book No'",
					"User sucessfully select 'Lot Book No'", "pass");
		} else {
			writeTestResults("Verify user select 'Lot Book No'", "User should be able to select 'Lot Book No'",
					"User couldn't select 'Lot Book No'", "fail");
		}

	}

	public void releaseWarehouse() throws Exception {
		click(btn_relese);
		Thread.sleep(3000);
		trackCode = warehouse_create;
		if (isDisplayed(btn_releseConfirmation)) {

			writeTestResults("Verify user can releese Warehouse", "User should be able to releese Warehouse",
					"User sucessfully releese Warehouse", "pass");
		} else {
			writeTestResults("Verify user can releese Warehouse", "User should be able to releese Warehouse",
					"User couldn't releese Warehouse", "fail");
		}
	}

	// common
	public void draft(String document) throws Exception {
		customizeLoadingDelay(btn_draft, 15);
		click(btn_draft);
		Thread.sleep(3000);
		customizeLoadingDelay(btn_releese2, 20);
		if (isDisplayed(btn_relese)) {

			writeTestResults("Verify user can draft \"\'" + document + "\'\"",
					"User should be able to draft \'" + document + "\'", "User sucessfully draft \'" + document + "\'",
					"pass");
		} else {
			writeTestResults("Verify user can draft \'" + document + "\'",
					"User should be able to draft \'" + document + "\'", "User couldn't draft \'" + document + "\'",
					"fail");
		}

	}

	/* Common method */
	public void handeledSendKeys(String locator, String value) throws InterruptedException {
		boolean set_txt;
		int tries = 0;
		do {
			try {
				set_txt = true;
				driver.findElement(getLocator(locator)).clear();
				driver.findElement(getLocator(locator)).click();
				driver.findElement(getLocator(locator)).sendKeys(value);
			} catch (Exception e) {
				set_txt = false;
				Thread.sleep(1000);
				tries++;
			}
			if (tries > 15) {
				break;
			}
		} while (!set_txt);
	}

	/* Common method */
	void customizeLoadingDelayAndContinue(String xpath, int seconds) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, seconds);
			wait.until(ExpectedConditions.visibilityOfElementLocated(getLocator(xpath)));
		} catch (Exception e) {
		}

	}

	// common
	public void relese(String document) throws Exception {
		click(btn_relese);
		Thread.sleep(3000);
		explicitWait(btn_releseConfirmation, 50);
		trackCode = getText(lbl_creationsDocNo);
		if (isDisplayed(btn_releseConfirmation)) {

			writeTestResults("Verify user can releese \'" + document + "\'",
					"User should be able to releese \'" + document + "\'",
					"User sucessfully releese \'" + document + "\'", "pass");
		} else {
			writeTestResults("Verify user can releese \'" + document + "\'",
					"User should be able to releese \'" + document + "\'", "User couldn't releese \'" + document + "\'",
					"fail");
		}

	}

	// Common generate product ID
	public String genProductId() {

		String SALTCHARS = "1234567890";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < 5) { // length of the random string.
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.charAt(index));
		}
		String generatedString = "GST" + salt.toString();
		return generatedString;
	}

	/* Common generate random id */
	public String genRandomID() {

		String SALTCHARS = "123456789";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < 5) { // length of the random string.
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.charAt(index));
		}
		String generatedString = "" + salt.toString();
		return generatedString;
	}

	// IW_TC_005
	public void navigateToStockAdjustmentForm() throws Exception {
		navigateInventoryAndWarehouseModule();
		click(btn_stockAdjustment);
		customizeLoadingDelay(btn_newStockAdjustment, 30);
		if (isDisplayed(btn_newStockAdjustment)) {
			writeTestResults("Verify user can navigate new stock adjustment page",
					"User should be able to navigate new stock adjustment page",
					"User sucessfully navigate new stock adjustment page", "pass");
		} else {
			writeTestResults("Verify user can navigate new stock adjustment page",
					"User should be able to navigate new stock adjustment page",
					"User not navigate new stock adjustment page", "Fail");
		}
		Thread.sleep(1000);
		click(btn_newStockAdjustment);

		explicitWait(header_newStockAdj, 50);
		if (isDisplayed(header_newStockAdj)) {

			writeTestResults("Verify user can navigate new stock adjustment form",
					"User should be able to navigate new stock adjustment form",
					"User sucessfully navigate new stock adjustment form", "pass");
		} else {
			writeTestResults("Verify user can navigate new stock adjustment form",
					"User should be able to navigate new stock adjustment form",
					"User not navigate new stock adjustment form", "Fail");
		}
	}

	public void stockAdjustmentFormFill() throws Exception {
		// ware house name should be available
		sendKeys(txt_descStockAdj, descStockAdj);

		selectText(dropdown_warehouseStockAdj, warehouseStockAdj);

		if (isDisplayed(txt_descStockAdj) && isDisplayed(dropdown_warehouseStockAdj)) {
			writeTestResults("Verify user can fill mandatory fields in summary tab",
					"User should be able to fill mandatory fields in summary tab",
					"User sucessfully fill mandatory fields in summary tab", "pass");
		} else {
			writeTestResults("Verify user can fill mandatory fields in summary tab",
					"User should be able to fill mandatory fields in summary tab",
					"User not fill mandatory fields in summary tab", "Fail");
		}

		/* Product add to the grid */
		int j = 0;
		for (int i = 1; i < 8; i++) {
			String str_i = String.valueOf(i);
			click(btn_productLookupStockAdjForLoop.replace("row", str_i));
			Thread.sleep(3000);
			customizeLoadingDelay(txt_productSearch, 15);
			if (isDisplayed(txt_productSearch)) {

				writeTestResults("Verify product search window availability",
						"User can be display product search window", "User sucessfully navigate product search window",
						"pass");
			} else {
				writeTestResults("Verify product search window availability",
						"User can be display product search window", "User not navigate product search window", "Fail");
			}
			String product = "";

			if (i == 1) {
				product = "BatchSpecific";
			} else if (i == 2) {
				product = "BatchFifo";
			} else if (i == 3) {
				product = "Lot";
			} else if (i == 4) {
				product = "SerielSpecific";
			} else if (i == 5) {
				product = "SerielBatchSpecific";
			} else if (i == 6) {
				product = "SerielBatchFifo";
			} else if (i == 7) {
				product = "SerirlFifo";
			} else {
				System.out.println("Product type cannt identified.........");
			}

			handeledSendKeys(txt_productSearch, readTestCreation(product));
			pressEnter(txt_productSearch);
			String product_path = result_cpmmonProduct.replace("product", readTestCreation(product));
			customizeLoadingDelay(product_path, 30);
			if (isDisplayed(product_path)) {
				writeTestResults("Verify searched products was loaded", "Searched products should be loaded",
						"Searched products successfully loaded", "pass");
			} else {
				writeTestResults("Verify searched products was loaded", "Searched products should be loaded",
						"Searched products doesn't loaded", "fail");
			}
			doubleClick(product_path);
			Thread.sleep(1500);
			String txt_qty = txt_qtyProductGridStockAdjustment.replace("row", str_i);
			String txt_cost = txt_costProductGridStockAdjustment.replace("row", str_i);

			sendKeys(txt_qty, productQuantity1);
			if (isDisplayed(txt_qty)) {
				writeTestResults("Verify selected products should be added to the grid",
						"Selected products should be added to the grid",
						"Selected products successfully added to the grid", "pass");
			} else {
				writeTestResults("Verify selected products should be added to the grid",
						"Selected products should be added to the grid", "Selected products doesn't added to the grid",
						"pass");
			}

			sendKeys(txt_cost, productCost1);
			if (isDisplayed(txt_qty) && isDisplayed(txt_cost)) {
				writeTestResults("Verify user can add Quantity and Cost per unit for products",
						"User should be able to add Quantity and Cost per unit for products",
						"User sucessfully add Quantity and Cost per unit for products", "pass");
			} else {
				writeTestResults("Verify user can add Quantity and Cost per unit for products",
						"User should be able to add Quantity and Cost per unit for products",
						"User couldn't add Quantity and Cost per unit for products", "Fail");
			}
			sleepCusomized(btn_avilabilityCheckWidget);
			click(btn_avilabilityCheckWidget);
			if (j < 7) {
				Thread.sleep(2000);
				Actions action1 = new Actions(driver);
				WebElement we1 = driver.findElement(By.xpath(btn_avilabilytCheckSecondMenu));
				action1.moveToElement(we1).build().perform();
				sleepCusomized(btn_avilabilytCheckSecondMenu);
				click(btn_avilabilytCheckSecondMenu);
				/*
				 * ONLY NEEDED WHEN RUN ALONE try { product_availability[j] = driver
				 * .findElement(By.xpath(td_availableStockWarehouseGST_Warehouse)).getText().
				 * trim();
				 * 
				 * } catch (Exception e) { System.out.println("0"); }
				 */
				if (product_availability[j] != null) {
					System.out.println(product_availability[j]);
				} else {
					product_availability[j] = ",0";
				}
				click(btn_closeAvailabilityCheckPopup);
			}
			j++;

			sleepCusomized(btn_addNewRow);
			click(btn_addNewRow);
		}

		click(btn_deleteEmotyRowStockAdjustment);
	}

	public void draftSerielBatchCaptureAndReleaseStockAdjustment() throws Exception {
		draft("Stock Adjustment");
		Thread.sleep(4000);
		click(brn_serielBatchCapture);
		if (isDisplayed(btn_serielBatchCaptureProduct1ILOOutboundShipment)) {

			writeTestResults("Verify user navigate to the product capture page",
					"User should be able navigate to the product capture page",
					"User sucessfully navigate to the product capture page", "pass");
		} else {
			writeTestResults("Verify user navigate to the product capture page",
					"User should be able navigate to the product capture page",
					"User does'nt navigate to the product capture page", "fail");
		}

		/* Product 01 */
		sleepCusomized(btn_serielBatchCaptureProduct1ILOOutboundShipment);
		String item_xpath;
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "1");
		click(item_xpath);
		if (isDisplayed(item_xpath)) {
			writeTestResults("Verify product 1 serial/batch capturing details are loaded",
					"Product 1 serial/batch capturing details should be loaded",
					"Product 1 serial/batch capturing details are sucessfully loaded", "pass");
		} else {
			writeTestResults("Verify product 1 serial/batch capturing details are loaded",
					"Product 1 serial/batch capturing details should be loaded",
					"Product 1 serial/batch capturing details doesn't loaded", "fail");
		}
		sendKeys(txt_batchNumberCaptureILOOutboundShipment, genBatchSpecificSerielNumber(200));
		sendKeys(txt_lotNumberStockAdjustment,
				genProductId() + currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_expiryDateStockAdjustmentProductCapture);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		String date = getAttribute(txt_expiryDateStockAdjustmentProductCapture, "value");
		writeRegression01("ProductExpiryDate_StockAdjustment", date, 38);
		pressEnter(txt_lotNumberStockAdjustment);
		Thread.sleep(1000);
		customizeLoadingDelay(btn_updateCapture, 50);
		click(btn_updateCapture);
		customizeLoadingDelay((item_xpath + span_afterCaptureSerialGreen), 30);
		String pro = getAttribute(btn_captureItem1, "class");

		if (pro.equals("fli-number green-vackground-fli-number")) {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User sucessfully capture 'Seriel/Batch'", "pass");
		} else {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User couldn't capture 'Seriel/Batch'", "fail");
		}
		/* Product 02 */
		Thread.sleep(3000);
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "2");
		click(item_xpath);
		if (isDisplayed(item_xpath)) {
			writeTestResults("Verify product 2 serial/batch capturing details are loaded",
					"Product 2 serial/batch capturing details should be loaded",
					"Product 2 serial/batch capturing details are sucessfully loaded", "pass");
		} else {
			writeTestResults("Verify product 2 serial/batch capturing details are loaded",
					"Product 2 serial/batch capturing details should be loaded",
					"Product 2 serial/batch capturing details doesn't loaded", "fail");
		}

		sendKeys(txt_batchNumberCaptureILOOutboundShipment, genBatchFifoSerielNumber(200));
		sendKeys(txt_lotNumberStockAdjustment,
				genProductId() + currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_expiryDateStockAdjustmentProductCapture);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_lotNumberStockAdjustment);
		Thread.sleep(1000);
		customizeLoadingDelay(btn_updateCapture, 50);
		click(btn_updateCapture);
		customizeLoadingDelay((item_xpath + span_afterCaptureSerialGreen), 30);
		String pro1 = getAttribute(btn_captureItem2, "class");

		if (pro1.equals("fli-number green-vackground-fli-number")) {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User sucessfully capture 'Seriel/Batch'", "pass");
		} else {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User couldn't capture 'Seriel/Batch'", "fail");
		}
		/* Product 04 */
		Thread.sleep(3000);
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "4");
		click(item_xpath);
		if (isDisplayed(item_xpath)) {
			writeTestResults("Verify product 4 serial/batch capturing details are loaded",
					"Product 4 serial/batch capturing details should be loaded",
					"Product 4 serial/batch capturing details are sucessfully loaded", "pass");
		} else {
			writeTestResults("Verify product 4 serial/batch capturing details are loaded",
					"Product 4 serial/batch capturing details should be loaded",
					"Product 4 serial/batch capturing details doesn't loaded", "fail");
		}

		click(chk_productCaptureRange);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$('#attrSlide-txtSerialCount').val(200)");

		String lot4 = genSerirlSpecificSerielNumber(200);
		String final_lot_script = "$('#attrSlide-txtSerialFrom').val(\"" + lot4 + "\")";
		jse.executeScript(final_lot_script);
		Thread.sleep(1000);
		sendKeys(txt_searielLotNo, genProductId() + currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_dateSerielProductProductCapturePage);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_dateSerielProductProductCapturePage);
		Thread.sleep(1000);
		customizeLoadingDelay(btn_updateCapture, 50);
		click(btn_updateCapture);
		customizeLoadingDelay((item_xpath + span_afterCaptureSerialGreen), 30);
		String pro4 = getAttribute(item_xpath, "class");

		if (pro4.equals("fli-number green-vackground-fli-number")) {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User sucessfully capture 'Seriel/Batch'", "pass");
		} else {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User couldn't capture 'Seriel/Batch'", "fail");
		}
		/* Product 05 */
		Thread.sleep(3000);
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "5");
		click(item_xpath);
		if (isDisplayed(item_xpath)) {
			writeTestResults("Verify product 5 serial/batch capturing details are loaded",
					"Product 5 serial/batch capturing details should be loaded",
					"Product 5 serial/batch capturing details are sucessfully loaded", "pass");
		} else {
			writeTestResults("Verify product 5 serial/batch capturing details are loaded",
					"Product 5 serial/batch capturing details should be loaded",
					"Product 5 serial/batch capturing details doesn't loaded", "fail");
		}

		click(chk_productCaptureRange);
		jse.executeScript("$('#attrSlide-txtSerialCount').val(200)");

		String lot41 = genSerielBatchSpecificSerielNumber(200);
		String final_lot_script1 = "$('#attrSlide-txtSerialFrom').val(\"" + lot41 + "\")";
		jse.executeScript(final_lot_script1);

		sendKeys(txt_searielLotNo, genProductId() + currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));

		sendKeys(txt_serielBatchProductCaptureStockAdjustment,
				genProductId() + currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));

		click(txt_dateSerielProductProductCapturePage);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_dateSerielProductProductCapturePage);
		Thread.sleep(1000);
		customizeLoadingDelay(btn_updateCapture, 50);
		click(btn_updateCapture);
		customizeLoadingDelay((item_xpath + span_afterCaptureSerialGreen), 30);
		String pro5 = getAttribute(item_xpath, "class");

		if (pro5.equals("fli-number green-vackground-fli-number")) {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User sucessfully capture 'Seriel/Batch'", "pass");
		} else {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User couldn't capture 'Seriel/Batch'", "fail");
		}

		/* Product 06 */
		Thread.sleep(2000);
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "6");
		Actions action = new Actions(driver);
		WebElement we = driver.findElement(By.xpath(item_xpath));
		action.moveToElement(we).build().perform();

		click(item_xpath);
		sleepCusomized(item_xpath);
		if (isDisplayed(item_xpath)) {
			writeTestResults("Verify product 6 serial/batch capturing details are loaded",
					"Product 6 serial/batch capturing details should be loaded",
					"Product 6 serial/batch capturing details are sucessfully loaded", "pass");
		} else {
			writeTestResults("Verify product 6 serial/batch capturing details are loaded",
					"Product 6 serial/batch capturing details should be loaded",
					"Product 6 serial/batch capturing details doesn't loaded", "fail");
		}

		click(chk_productCaptureRange);
		jse.executeScript("$('#attrSlide-txtSerialCount').val(200)");

		String lot411 = genSerielBatchFifoNumber(200);
		String final1_lot_script1 = "$('#attrSlide-txtSerialFrom').val(\"" + lot411 + "\")";
		jse.executeScript(final1_lot_script1);

		sendKeys(txt_searielLotNo, genProductId() + currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));

		sendKeys(txt_serielBatchProductCaptureStockAdjustment,
				genProductId() + currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));

		click(txt_dateSerielProductProductCapturePage);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_dateSerielProductProductCapturePage);
		Thread.sleep(1000);
		customizeLoadingDelay(btn_updateCapture, 50);
		click(btn_updateCapture);
		customizeLoadingDelay((item_xpath + span_afterCaptureSerialGreen), 50);
		String pro6 = getAttribute(item_xpath, "class");

		if (pro6.equals("fli-number green-vackground-fli-number")) {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User sucessfully capture 'Seriel/Batch'", "pass");
		} else {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User couldn't capture 'Seriel/Batch'", "fail");
		}

		/* Product 07 */
		Thread.sleep(2000);
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "7");
		Actions action1 = new Actions(driver);
		WebElement we1 = driver.findElement(By.xpath(item_xpath));
		action1.moveToElement(we1).build().perform();

		click(item_xpath);
		sleepCusomized(item_xpath);
		if (isDisplayed(item_xpath)) {
			writeTestResults("Verify product 7 serial/batch capturing details are loaded",
					"Product 7 serial/batch capturing details should be loaded",
					"Product 7 serial/batch capturing details are sucessfully loaded", "pass");
		} else {
			writeTestResults("Verify product 7 serial/batch capturing details are loaded",
					"Product 7 serial/batch capturing details should be loaded",
					"Product 7 serial/batch capturing details doesn't loaded", "fail");
		}

		click(chk_productCaptureRange);
		jse.executeScript("$('#attrSlide-txtSerialCount').val(200)");

		String lot4111 = genSerielFifoSerielNumber(200);
		String final1_lot_script11 = "$('#attrSlide-txtSerialFrom').val(\"" + lot4111 + "\")";
		jse.executeScript(final1_lot_script11);

		sendKeys(txt_searielLotNo, genProductId() + currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));

		click(txt_dateSerielProductProductCapturePage);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_dateSerielProductProductCapturePage);
		Thread.sleep(1000);
		customizeLoadingDelay(btn_updateCapture, 50);
		click(btn_updateCapture);
		customizeLoadingDelay((item_xpath + span_afterCaptureSerialGreen), 30);
		String pro7 = getAttribute(item_xpath, "class");

		if (pro7.equals("fli-number green-vackground-fli-number")) {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User sucessfully capture 'Seriel/Batch'", "pass");
		} else {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User couldn't capture 'Seriel/Batch'", "fail");
		}

		Thread.sleep(3000);
		click(btn_productCaptureBackButton);
		Thread.sleep(1000);
		if (isDisplayed(btn_relese)) {
			writeTestResults("Verify user can navigated to Stock Adjustment Form",
					"User should be navigated to Stock Adjustment Form",
					"User successfully navigated to Stock Adjustment Form", "pass");
		} else {
			writeTestResults("Verify user can navigated to Stock Adjustment Form",
					"User should be navigated to Stock Adjustment Form",
					"User couldn't navigated to Stock Adjustment Form", "fail");
		}
		click(btn_relese);
		customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 50);
		if (isDisplayed(lbl_relesedConfirmation.replace("(Released)", "( Released )"))) {

			writeTestResults("Verify user can releese Stock Adjustment",
					"User should be able to releese Stock Adjustment", "User sucessfully releese Stock Adjustment",
					"pass");
		} else {
			writeTestResults("Verify user can releese Stock Adjustment",
					"User should be able to releese Stock Adjustment", "User couldn't releese Stock Adjustment",
					"fail");
		}
	}

	public void checkWarehouseForProdctAvailability() throws Exception {
		String[] product_availability_after = new String[7];
		int row_position = 1;
		for (int i = 0; i < 7; i++) {
			String row_str = String.valueOf(row_position);
			sleepCusomized(btn_availabilityCheckWidgetAfterrelesed.replace("row", row_str));
			click(btn_availabilityCheckWidgetAfterrelesed.replace("row", row_str));
			Thread.sleep(2000);
			Actions action1 = new Actions(driver);
			WebElement we1 = driver.findElement(By.xpath(btn_avilabilytCheckSecondMenu));
			action1.moveToElement(we1).build().perform();
			click(btn_avilabilytCheckSecondMenu);
			customizeLoadingDelay(td_availableStockGstmWarehouseAfterRelesed, 20);
			product_availability_after[i] = getText(td_availableStockGstmWarehouseAfterRelesed);
			System.out.println(product_availability_after[i]);
			click(btn_closeAvailabilityCheckPopup);
			row_position++;
		}

		boolean warehouse_check_flag = false;
		for (int j = 0; j < 7; j++) {
			String qty = product_availability[j].replaceAll(",", "");
			int qty_int = Integer.parseInt(qty);
			String after_qty = product_availability_after[j].replaceAll(",", "");
			try {
				int after_qty_int = Integer.parseInt(after_qty);
				int adding_qty = Integer.parseInt(productQuantity1);
				if (after_qty_int == (qty_int + adding_qty)) {
					warehouse_check_flag = true;
				} else {
					warehouse_check_flag = false;
					break;
				}
			} catch (Exception e) {
				System.out.println(e);
			}

		}

		if (warehouse_check_flag) {
			writeTestResults("Verify stock was transfered to the destination",
					"Stock should be transfered to the destination", "Stock successfully transfered to the destination",
					"pass");
		} else {
			writeTestResults("Verify stock was transfered to the destination",
					"Stock should be transfered to the destination", "Stock doesn't transfered to the destination",
					"fail");
		}
	}

	// IW_TC_006
	public void navigateToTransferOrderform() throws Exception {
		navigateInventoryAndWarehouseModule();

		click(btn_transferOrder);
		if (isDisplayed(btn_newTransferOrder)) {

			writeTestResults("Verify user can navigate transfer order by-page",
					"User should be able to navigate transfer order by-page",
					"User sucessfully navigate transfer order by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate transfer order by-page",
					"User should be able to navigate transfer order by-page",
					"User not navigate transfer order by-page", "Fail");
		}

		click(btn_newTransferOrder);

		if (isDisplayed(header_newTransferOrder)) {

			writeTestResults("Verify user can navigate transfer order form",
					"User should be able to navigate transfer order form",
					"User sucessfully navigate transfer order form", "pass");
		} else {
			writeTestResults("Verify user can navigate transfer order form",
					"User should be able to navigate transfer order form", "User not navigate transfer order form",
					"Fail");
		}
	}

	public void completeTransferOrderForm() throws Exception {
		// From Ware
		selectText(txt_fromWareTO, fromWareTransferOrder);

		// To ware
		selectText(txt_toWareTO, toWareTransferOrder);

		if (isDisplayed(txt_toWareTO) && isDisplayed(txt_fromWareTO)) {
			writeTestResults(
					"Verify user can select From warehouse and To warehouse which are mandatory and warehouse addresses are loaded",
					"User should be able to select From warehouse and To warehouse which are mandatory and warehouse addresses should be loaded",
					"User successfully select From warehouse and To warehouse which are mandatory and warehouse addresses successfully loaded",
					"pass");
		} else {
			writeTestResults(
					"Verify user can select From warehouse and To warehouse which are mandatory and warehouse addresses are loaded",
					"User should be able to select From warehouse and To warehouse which are mandatory and warehouse addresses should be loaded",
					"User couldn't select From warehouse and To warehouse which are mandatory and warehouse addresses doesn't loaded",
					"fail");
		}

		/* Product add to the grid */
		int j = 0;
		for (int i = 1; i < 8; i++) {
			String str_i = String.valueOf(i);
			explicitWaitUntillClickable(
					btn_productLookupTOForLoop.replace("row", str_i).replace("tblStockAdj", "tblTransferOrder"),
					20);
			click(btn_productLookupTOForLoop.replace("row", str_i).replace("tblStockAdj", "tblTransferOrder"));// table[@id='tblTransferOrder']/tbody/tr/td[7]/span
			if (isDisplayed(txt_productSearch)) {

				writeTestResults("Verify product search window availability",
						"User can be display product search window", "User sucessfully navigate product search window",
						"pass");
			} else {
				writeTestResults("Verify product search window availability",
						"User can be display product search window", "User not navigate product search window", "Fail");
			}
			String product = "";

			if (i == 1) {
				product = "BatchSpecific";
			} else if (i == 2) {
				product = "BatchFifo";
			} else if (i == 3) {
				product = "Lot";
			} else if (i == 4) {
				product = "SerielSpecific";
			} else if (i == 5) {
				product = "SerielBatchSpecific";
			} else if (i == 6) {
				product = "SerielBatchFifo";
			} else if (i == 7) {
				product = "SerirlFifo";
			} else {
				System.out.println("Product type cannt identified.........");
			}
			sleepCusomized(txt_productSearch);
			sendKeys(txt_productSearch, readTestCreation(product));
			pressEnter(txt_productSearch);
			Thread.sleep(3000);
			sleepCusomized(btn_resultProduct);
			if (isDisplayed(btn_resultProduct)) {
				writeTestResults("Verify searched products was loaded", "Searched products should be loaded",
						"Searched products successfully loaded", "pass");
			} else {
				writeTestResults("Verify searched products was loaded", "Searched products should be loaded",
						"Searched products doesn't loaded", "fail");
			}
			sleepCusomized(btn_resultProduct);
			doubleClick(btn_resultProduct);
			String txt_qty = txt_qtyProductGridStockTransferOrder.replace("row", str_i);
			String txt_cost = txt_costProductGridStockTransferOrder.replace("row", str_i);

			Thread.sleep(2000);
			sendKeys(txt_cost, productTOCost);
			if (isDisplayed(txt_qty) && isDisplayed(txt_cost)) {
				writeTestResults("Verify user can add Quantity and Cost per unit for products",
						"User should be able to add Quantity and Cost per unit for products",
						"User sucessfully add Quantity and Cost per unit for products", "pass");
			} else {
				writeTestResults("Verify user can add Quantity and Cost per unit for products",
						"User should be able to add Quantity and Cost per unit for products",
						"User couldn't add Quantity and Cost per unit for products", "Fail");
			}

			Thread.sleep(2000);
			sendKeys(txt_qty, qtyTransferOrder);
			if (isDisplayed(txt_qty)) {
				writeTestResults("Verify selected products should be added to the grid",
						"Selected products should be added to the grid",
						"Selected products successfully added to the grid", "pass");
			} else {
				writeTestResults("Verify selected products should be added to the grid",
						"Selected products should be added to the grid", "Selected products doesn't added to the grid",
						"fail");
			}

			Thread.sleep(2000);
			Actions action12 = new Actions(driver);
			WebElement we12 = driver.findElement(By.xpath(btn_availabilityMoveFirstButtonTransferOrder));
			action12.moveToElement(we12).build().perform();
			Thread.sleep(1500);
			click(btn_avilabilityCheckWidget);
			if (j < 7) {
				Thread.sleep(2000);
				Actions action1 = new Actions(driver);
				WebElement we1 = driver.findElement(By.xpath(btn_avilabilytCheckSecondMenu));
				action1.moveToElement(we1).build().perform();
				click(btn_avilabilytCheckSecondMenu);
				try {
					product_availability[j] = driver.findElement(By.xpath(td_availableStockWarehouseGST_Warehouse))
							.getText().trim();
				} catch (Exception e) {
					System.out.println("0");
				}
				if (product_availability[j] != null) {
					System.out.println(product_availability[j]);
				} else {
					product_availability[j] = ",0";
				}
				sleepCusomized(btn_closeAvailabilityCheckPopup);
				click(btn_closeAvailabilityCheckPopup);
			}
			j++;

			sleepCusomized(btn_addNewRow);
			click(btn_addNewRow);
		}

		click(btn_deleteEmotyRowStockAdjustment.replace("tblStockAdj", "tblTransferOrder"));

		Thread.sleep(2000);
		String ckeckoutBefore = getText(lbl_bannerTotal);

		click(btn_checkout);
		Thread.sleep(3000);
		String ckeckoutAfter = getText(lbl_bannerTotal);
		if (!ckeckoutBefore.equals(ckeckoutAfter)) {
			writeTestResults("Verify user can checkout the Transfer Order",
					"User should be able to checkout the Transfer Order", "User successfully checkout the Transfer Order",
					"pass");
		} else {
			writeTestResults("Verify user can checkout the Transfer Order",
					"User should be able to checkout the Transfer Order", "User could'nt checkout the Transfer Order", "fail");
		}
	}

	public void releseTransferOrder() throws Exception {
		click(btn_draft);
		explicitWait(header_draftedTrsansferOrder, 40);
		if (isDisplayed(header_draftedTrsansferOrder)) {
			writeTestResults("Verify user can draft the Transfer Order",
					"User should be able to draft the Transfer Order", "User successfully draft the Transfer Order",
					"pass");
		} else {
			writeTestResults("Verify user can draft the Transfer Order",
					"User should be able to draft the Transfer Order", "User could'nt draft the Transfer Order", "fail");
		}
		Thread.sleep(4000);

		click(brn_serielBatchCapture);

		/* Product 01 */
		explicitWait(btn_captureItem1, 40);
		if (isDisplayed(btn_captureItem1)) {
			writeTestResults(
					"Verify user can navigated Serial/Batch window and products which going to Outbound should be loaded for item 1",
					"User should be navigated Serial/Batch window and products which going to Outbound should be loaded for item 1",
					"User successfully navigated Serial/Batch window and products which going to Outbound should be loaded for item 1",
					"pass");
		} else {
			writeTestResults(
					"Verify user can navigated Serial/Batch window and products which going to Outbound should be loaded for item 1",
					"User should be navigated Serial/Batch window and products which going to Outbound should be loaded for item 1",
					"User couldn't navigated Serial/Batch window and products which going to Outbound doesn't loaded for item 1",
					"fail");
		}

		click(btn_captureItem1);
		if (isDisplayed(btn_capturePageDocIcon)) {
			writeTestResults("Verify Serial/Batch capturing details are enabled",
					"Serial/Batch capturing details should be enabled",
					"Serial/Batch capturing details successfully enabled", "pass");
		} else {
			writeTestResults("Verify Serial/Batch capturing details are enabled",
					"Serial/Batch capturing details should be enabled",
					"Serial/Batch capturing details doesn't enabled", "fail");
		}

		click(btn_capturePageDocIcon);
		if (isDisplayed(chk_availableProductsTransferOrderOutboundShipment.replace("row", "1"))) {
			writeTestResults("Verify existing Batches are loaded as pop-up",
					"Existing batches should be loaded as pop-up", "Existing batches successfully loaded as pop-up",
					"pass");
		} else {
			writeTestResults("Verify existing batches are loaded as pop-up",
					"Existing batches should be loaded as pop-up", "Existing batches doesn't loaded as pop-up", "fail");
		}

		click(chk_availableProductsTransferOrderOutboundShipment.replace("row", "1"));

		String qty = getText(lbl_qtyBatchSpecifificCapturePage);
		String qty_capture = getText(lbl_availQuantityProductCapturuPage);

		int qty_int = Integer.parseInt(qty);
		int qty_capture_int = Integer.parseInt(qty_capture);

		int i = 2;
		while (qty_int > qty_capture_int) {
			String i_str = String.valueOf(i);
			click(chk_deleteItemCapturePage);
			click(btn_deleteRowProductCaptururePage);
			click(btn_capturePageDocIcon);
			click(chk_availableProductsTransferOrderOutboundShipment.replace("row", i_str));
			Thread.sleep(2000);
			qty_capture = getText(lbl_availQuantityProductCapturuPage);
			qty_capture_int = Integer.parseInt(qty_capture);
		}

		if (qty_int <= qty_capture_int) {
			writeTestResults("Verify selected batches are added to the grid",
					"Selected batches should be added to the grid", "Selected batches successfully added to the grid",
					"pass");
		} else {
			writeTestResults("Verify selected batches are added to the grid",
					"Selected batches should be added to the grid", "Selected batches doesn't added to the grid",
					"fail");
		}

		click(btn_updateCapture);

		String pro = getAttribute(btn_captureItem1, "class");

		if (pro.equals("fli-number green-vackground-fli-number")
				&& pro.equals("fli-number green-vackground-fli-number")) {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User sucessfully capture 'Seriel/Batch'", "pass");
		} else {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User couldn't capture 'Seriel/Batch'", "fail");
		}

		/* Product 04 */
		String item_xpath = btn_itemProductCapturePage.replace("_product_number", "4");
		sleepCusomized(item_xpath);

		if (isDisplayed(item_xpath)) {
			writeTestResults(
					"Verify user can navigated Serial/Batch window and products which going to Outbound should be loaded for item 4",
					"User should be navigated Serial/Batch window and products which going to Outbound should be loaded for item 4",
					"User successfully navigated Serial/Batch window and products which going to Outbound should be loaded for item 4",
					"pass");
		} else {
			writeTestResults(
					"Verify user can navigated Serial/Batch window and products which going to Outbound should be loaded for item 4",
					"User should be navigated Serial/Batch window and products which going to Outbound should be loaded for item 4",
					"User couldn't navigated Serial/Batch window and products which going to Outbound doesn't loaded for item 4",
					"fail");
		}

		click(item_xpath);
		if (isDisplayed(chk_productCaptureRange)) {
			writeTestResults("Verify Serial/Batch capturing details are enabled",
					"Serial/Batch capturing details should be enabled",
					"Serial/Batch capturing details successfully enabled", "pass");
		} else {
			writeTestResults("Verify Serial/Batch capturing details are enabled",
					"Serial/Batch capturing details should be enabled",
					"Serial/Batch capturing details doesn't enabled", "fail");
		}
		click(chk_productCaptureRange);

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$('#attrSlide-txtSerialCount').val(" + qtyTransferOrder + ")");

		String lot4 = getOngoinfSerirlSpecificSerielNumber(10);
		String final_lot_script = "$('#attrSlide-txtSerialFrom').val(\"" + lot4 + "\")";
		jse.executeScript(final_lot_script);

		pressEnter(txt_seriealProductCapture);

		if (qty_int <= qty_capture_int) {
			writeTestResults("Verify selected batches are added to the grid",
					"Selected batches should be added to the grid", "Selected batches successfully added to the grid",
					"pass");
		} else {
			writeTestResults("Verify selected batches are added to the grid",
					"Selected batches should be added to the grid", "Selected batches doesn't added to the grid",
					"fail");
		}

		click(btn_updateCapture);

		String pro1 = getAttribute(btn_captureItem1, "class");

		if (pro1.equals("fli-number green-vackground-fli-number")) {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User sucessfully capture 'Seriel/Batch'", "pass");
		} else {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User couldn't capture 'Seriel/Batch'", "fail");
		}

		/* Product 05 */
		String item_xpath1 = btn_itemProductCapturePage.replace("_product_number", "5");
		sleepCusomized(item_xpath1);
		if (isDisplayed(item_xpath1)) {
			writeTestResults(
					"Verify user can navigated Serial/Batch window and products which going to Outbound should be loaded for item 5",
					"User should be navigated Serial/Batch window and products which going to Outbound should be loaded for item 5",
					"User successfully navigated Serial/Batch window and products which going to Outbound should be loaded for item 5",
					"pass");
		} else {
			writeTestResults(
					"Verify user can navigated Serial/Batch window and products which going to Outbound should be loaded for item 5",
					"User should be navigated Serial/Batch window and products which going to Outbound should be loaded for item 5",
					"User couldn't navigated Serial/Batch window and products which going to Outbound doesn't loaded for item 5",
					"fail");
		}
		click(item_xpath1);

		if (isDisplayed(chk_productCaptureRange)) {
			writeTestResults("Verify Serial/Batch capturing details are enabled",
					"Serial/Batch capturing details should be enabled",
					"Serial/Batch capturing details successfully enabled", "pass");
		} else {
			writeTestResults("Verify Serial/Batch capturing details are enabled",
					"Serial/Batch capturing details should be enabled",
					"Serial/Batch capturing details doesn't enabled", "fail");
		}
		click(chk_productCaptureRange);

		jse.executeScript("$('#attrSlide-txtSerialCount').val(" + qtyTransferOrder + ")");

		String final_lot_script1 = "$('#attrSlide-txtSerialFrom').val(\""
				+ getOngoingSerielBatchSpecificSerielNumber(10) + "\")";
		jse.executeScript(final_lot_script1);

		pressEnter(txt_seriealProductCapture);

		if (qty_int <= qty_capture_int) {
			writeTestResults("Verify selected batches are added to the grid",
					"Selected batches should be added to the grid", "Selected batches successfully added to the grid",
					"pass");
		} else {
			writeTestResults("Verify selected batches are added to the grid",
					"Selected batches should be added to the grid", "Selected batches doesn't added to the grid",
					"fail");
		}

		click(btn_updateCapture);

		String pro2 = getAttribute(btn_captureItem1, "class");
		String pro4 = getAttribute(item_xpath, "class");
		String pro5 = getAttribute(item_xpath1, "class");

		if (pro2.equals("fli-number green-vackground-fli-number")
				&& pro4.equals("fli-number green-vackground-fli-number")
				&& pro5.equals("fli-number green-vackground-fli-number")) {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User sucessfully capture 'Seriel/Batch'", "pass");
		} else {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User couldn't capture 'Seriel/Batch'", "fail");
		}

		sleepCusomized(btn_backButtonUpdateCapture);
		click(btn_backButtonUpdateCapture);

		trackCode = getText(lbl_docNo);
		click(btn_relese);
		customizeLoadingDelay(btn_goToPageLink, 30);
		if (isDisplayed(btn_goToPageLink)) {

			writeTestResults("Verify user can releese Transfer Order", "User should be able to releese Transfer Order",
					"User sucessfully releese Transfer Order", "pass");
		} else {
			writeTestResults("Verify user can releese Transfer Order", "User should be able to releese Transfer Order",
					"User couldn't releese Transfer Order", "fail");
		}

	}

	public void outboundShipment() throws Exception {
		click(btn_goToPageLink);
		switchWindow();
		draft("Outbound Shipment");
		/*
		 * Erlier system allows to connitune outbound shipment without capturing seriel
		 * batch in Transfer Order
		 */
//		sleepCusomized(brn_serielBatchCapture);
//		click(brn_serielBatchCapture);
//
//		/* Product 01 */
//		Thread.sleep(2000);
//		if (isDisplayed(btn_captureItem1)) {
//			writeTestResults(
//					"Verify user can navigated Serial/Batch window and products which going to Outbound should be loaded for item 1",
//					"User should be navigated Serial/Batch window and products which going to Outbound should be loaded for item 1",
//					"User successfully navigated Serial/Batch window and products which going to Outbound should be loaded for item 1",
//					"pass");
//		} else {
//			writeTestResults(
//					"Verify user can navigated Serial/Batch window and products which going to Outbound should be loaded for item 1",
//					"User should be navigated Serial/Batch window and products which going to Outbound should be loaded for item 1",
//					"User couldn't navigated Serial/Batch window and products which going to Outbound doesn't loaded for item 1",
//					"fail");
//		}
//
//		click(btn_captureItem1);
//		if (isDisplayed(btn_capturePageDocIcon)) {
//			writeTestResults("Verify Serial/Batch capturing details are enabled",
//					"Serial/Batch capturing details should be enabled",
//					"Serial/Batch capturing details successfully enabled", "pass");
//		} else {
//			writeTestResults("Verify Serial/Batch capturing details are enabled",
//					"Serial/Batch capturing details should be enabled",
//					"Serial/Batch capturing details doesn't enabled", "fail");
//		}
//
//		click(btn_capturePageDocIcon);
//		if (isDisplayed(chk_availableProductsTransferOrderOutboundShipment.replace("row", "1"))) {
//			writeTestResults("Verify existing Batches are loaded as pop-up",
//					"Existing batches should be loaded as pop-up", "Existing batches successfully loaded as pop-up",
//					"pass");
//		} else {
//			writeTestResults("Verify existing batches are loaded as pop-up",
//					"Existing batches should be loaded as pop-up", "Existing batches doesn't loaded as pop-up", "fail");
//		}
//
//		click(chk_availableProductsTransferOrderOutboundShipment.replace("row", "1"));
//
//		String qty = getText(lbl_qtyBatchSpecifificCapturePage);
//		String qty_capture = getText(lbl_captureQtyBatchSpecifificCapturePage);
//
//		int qty_int = Integer.parseInt(qty);
//		int qty_capture_int = Integer.parseInt(qty_capture);
//
//		int i = 2;
//		while (qty_int < qty_capture_int) {
//			String i_str = String.valueOf(i);
//			click(btn_capturePageDocIcon);
//			click(chk_availableProductsTransferOrderOutboundShipment.replace("row", i_str));
//		}
//
//		if (qty_int <= qty_capture_int) {
//			writeTestResults("Verify selected batches are added to the grid",
//					"Selected batches should be added to the grid", "Selected batches successfully added to the grid",
//					"pass");
//		} else {
//			writeTestResults("Verify selected batches are added to the grid",
//					"Selected batches should be added to the grid", "Selected batches doesn't added to the grid",
//					"fail");
//		}
//
//		click(btn_updateCapture);
//
//		String pro = getAttribute(btn_captureItem1, "class");
//
//		if (pro.equals("fli-number green-vackground-fli-number")
//				&& pro.equals("fli-number green-vackground-fli-number")) {
//			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
//					"User sucessfully capture 'Seriel/Batch'", "pass");
//		} else {
//			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
//					"User couldn't capture 'Seriel/Batch'", "fail");
//		}
//
//		/* Product 04 */
//		String item_xpath = btn_itemProductCapturePage.replace("_product_number", "4");
//		sleepCusomized(item_xpath);
//
//		if (isDisplayed(item_xpath)) {
//			writeTestResults(
//					"Verify user can navigated Serial/Batch window and products which going to Outbound should be loaded for item 4",
//					"User should be navigated Serial/Batch window and products which going to Outbound should be loaded for item 4",
//					"User successfully navigated Serial/Batch window and products which going to Outbound should be loaded for item 4",
//					"pass");
//		} else {
//			writeTestResults(
//					"Verify user can navigated Serial/Batch window and products which going to Outbound should be loaded for item 4",
//					"User should be navigated Serial/Batch window and products which going to Outbound should be loaded for item 4",
//					"User couldn't navigated Serial/Batch window and products which going to Outbound doesn't loaded for item 4",
//					"fail");
//		}
//
//		click(item_xpath);
//		Thread.sleep(2000);
//		if (isDisplayed(chk_productCaptureRange)) {
//			writeTestResults("Verify Serial/Batch capturing details are enabled",
//					"Serial/Batch capturing details should be enabled",
//					"Serial/Batch capturing details successfully enabled", "pass");
//		} else {
//			writeTestResults("Verify Serial/Batch capturing details are enabled",
//					"Serial/Batch capturing details should be enabled",
//					"Serial/Batch capturing details doesn't enabled", "fail");
//		}
//		click(chk_productCaptureRange);
//
//		JavascriptExecutor jse = (JavascriptExecutor) driver;
//		jse.executeScript("$('#attrSlide-txtSerialCount').val(" + qtyTransferOrder + ")");
//
//		String lot4 = getOngoinfSerirlSpecificSerielNumber(10);
//		String final_lot_script = "$('#attrSlide-txtSerialFrom').val(\"" + lot4 + "\")";
//		jse.executeScript(final_lot_script);
//
//		pressEnter(txt_seriealProductCapture);
//
//		if (qty_int <= qty_capture_int) {
//			writeTestResults("Verify selected batches are added to the grid",
//					"Selected batches should be added to the grid", "Selected batches successfully added to the grid",
//					"pass");
//		} else {
//			writeTestResults("Verify selected batches are added to the grid",
//					"Selected batches should be added to the grid", "Selected batches doesn't added to the grid",
//					"fail");
//		}
//
//		click(btn_updateCapture);
//
//		String pro1 = getAttribute(btn_captureItem1, "class");
//
//		if (pro1.equals("fli-number green-vackground-fli-number")) {
//			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
//					"User sucessfully capture 'Seriel/Batch'", "pass");
//		} else {
//			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
//					"User couldn't capture 'Seriel/Batch'", "fail");
//		}
//
//		/* Product 05 */
//		String item_xpath1 = btn_itemProductCapturePage.replace("_product_number", "5");
//		sleepCusomized(item_xpath1);
//		if (isDisplayed(item_xpath1)) {
//			writeTestResults(
//					"Verify user can navigated Serial/Batch window and products which going to Outbound should be loaded for item 5",
//					"User should be navigated Serial/Batch window and products which going to Outbound should be loaded for item 5",
//					"User successfully navigated Serial/Batch window and products which going to Outbound should be loaded for item 5",
//					"pass");
//		} else {
//			writeTestResults(
//					"Verify user can navigated Serial/Batch window and products which going to Outbound should be loaded for item 5",
//					"User should be navigated Serial/Batch window and products which going to Outbound should be loaded for item 5",
//					"User couldn't navigated Serial/Batch window and products which going to Outbound doesn't loaded for item 5",
//					"fail");
//		}
//		click(item_xpath1);
//
//		if (isDisplayed(chk_productCaptureRange)) {
//			writeTestResults("Verify Serial/Batch capturing details are enabled",
//					"Serial/Batch capturing details should be enabled",
//					"Serial/Batch capturing details successfully enabled", "pass");
//		} else {
//			writeTestResults("Verify Serial/Batch capturing details are enabled",
//					"Serial/Batch capturing details should be enabled",
//					"Serial/Batch capturing details doesn't enabled", "fail");
//		}
//		click(chk_productCaptureRange);
//
//		jse.executeScript("$('#attrSlide-txtSerialCount').val(" + qtyTransferOrder + ")");
//
//		String final_lot_script1 = "$('#attrSlide-txtSerialFrom').val(\""
//				+ getOngoingSerielBatchSpecificSerielNumber(10) + "\")";
//		jse.executeScript(final_lot_script1);
//
//		pressEnter(txt_seriealProductCapture);
//
//		if (qty_int <= qty_capture_int) {
//			writeTestResults("Verify selected batches are added to the grid",
//					"Selected batches should be added to the grid", "Selected batches successfully added to the grid",
//					"pass");
//		} else {
//			writeTestResults("Verify selected batches are added to the grid",
//					"Selected batches should be added to the grid", "Selected batches doesn't added to the grid",
//					"fail");
//		}
//
//		click(btn_updateCapture);
//
//		String pro2 = getAttribute(btn_captureItem1, "class");
//		String pro4 = getAttribute(item_xpath, "class");
//		String pro5 = getAttribute(item_xpath1, "class");
//
//		if (pro2.equals("fli-number green-vackground-fli-number")
//				&& pro4.equals("fli-number green-vackground-fli-number")
//				&& pro5.equals("fli-number green-vackground-fli-number")) {
//			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
//					"User sucessfully capture 'Seriel/Batch'", "pass");
//		} else {
//			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
//					"User couldn't capture 'Seriel/Batch'", "fail");
//		}
//
//		sleepCusomized(btn_backButtonUpdateCapture);
//		click(btn_backButtonUpdateCapture);

		sleepCusomized(btn_relese);
		if (isDisplayed(btn_relese)) {

			writeTestResults("Verify user can navigated to Outbound Shipment Form",
					"User should be able to navigated to Outbound Shipment Form",
					"User sucessfully navigated to Outbound Shipment Form", "pass");
		} else {
			writeTestResults("Verify user can navigated to Outbound Shipment Form",
					"User should be able to navigated to Outbound Shipment Form",
					"User couldn't navigated to Outbound Shipment Form", "fail");
		}
		sleepCusomized(lbl_docNo);
		trackCode = getText(lbl_docNo);
		click(btn_relese);
		sleepCusomized(btn_goToPageLink);
		if (isDisplayed(btn_goToPageLink)) {

			writeTestResults("Verify user can releese Outbound Shipment",
					"User should be able to releese Outbound Shipment", "User sucessfully releese Outbound Shipment",
					"pass");
		} else {
			writeTestResults("Verify user can releese Outbound Shipment",
					"User should be able to releese Outbound Shipment", "User couldn't releese Outbound Shipment",
					"fail");
		}

		sleepCusomized(btn_goToPageLink);
		if (isDisplayed(btn_goToPageLink)) {

			writeTestResults("Verify user can complete outbound shipment",
					"User can be able to complete outbound shipment", "User sucessfully complete outbound shipment",
					"pass");
		} else {
			writeTestResults("Verify user can complete outbound shipment",
					"User can be able to complete outbound shipment", "User not complete outbound shipment", "Fail");
		}
	}

	public void inboundShipment() throws Exception {
		click(btn_goToPageLink);
		switchWindow();
		implisitWait(10);
		click(btn_checkout);

		draft("Inbound Shipment");

		relese("Inbound Shipment");
		Thread.sleep(3000);
		sleepCusomized(lbl_docNo);

	}

	public void checkWarehouseForProdctAvailabilityTransferOrder() throws Exception {
		String[] product_availability_after = new String[7];
		int row_position = 1;
		for (int i = 0; i < 7; i++) {
			String row_str = String.valueOf(row_position);
			sleepCusomized(btn_productAvilabilityWidgetShipmentDetailsTable.replace("row", row_str));
			click(btn_productAvilabilityWidgetShipmentDetailsTable.replace("row", row_str));
			Thread.sleep(2000);
			Actions action1 = new Actions(driver);
			WebElement we1 = driver.findElement(By.xpath(btn_avilabilytCheckSecondMenu));
			action1.moveToElement(we1).build().perform();
			click(btn_avilabilytCheckSecondMenu);
			sleepCusomized(td_availableStockGstmWarehouseAfterRelesed);
			product_availability_after[i] = getText(td_availableStockGstmWarehouseAfterRelesed);
			System.out.println(product_availability_after[i]);
			click(btn_closeAvailabilityCheckPopup);
			row_position++;
		}

		boolean warehouse_check_flag = false;
		for (int j = 0; j < 7; j++) {
			String qty = product_availability[j].replaceAll(",", "");
			int qty_int = Integer.parseInt(qty);
			String after_qty = product_availability_after[j].replaceAll(",", "");
			try {
				int after_qty_int = Integer.parseInt(after_qty);
				int adding_qty = Integer.parseInt(qtyTransferOrder);
				if (after_qty_int == (qty_int - adding_qty)) {
					warehouse_check_flag = true;
				} else {
					warehouse_check_flag = false;
					break;
				}
			} catch (Exception e) {
				System.out.println(e);
			}

		}

		if (warehouse_check_flag) {
			writeTestResults("Verify stock was transfered to the destination",
					"Stock should be transfered to the destination", "Stock successfully transfered to the destination",
					"pass");
		} else {
			writeTestResults("Verify stock was transfered to the destination",
					"Stock should be transfered to the destination", "Stock doesn't transfered to the destination",
					"fail");
		}

	}

	// IW_TC_007
	public void navigateToInterDepartmentTransferOrderPage() throws Exception {
		navigateInventoryAndWarehouseModule();
		click(btn_InterDepartmentTransferOrder);
		if (isDisplayed(btn_verifyInternelOrderPage)) {

			writeTestResults("Verify user able to navigate Inter Department Transfer Order page",
					"User should be able to navigateInter Department Transfer Order page",
					"User sucessfully navigateInter Department Transfer Order page", "pass");
		} else {
			writeTestResults("Verify user able to navigate Inter Department Transfer Order page",
					"User should be able to navigateInter Department Transfer Order page",
					"User does'nt navigate Inter Department Transfer Order page", "Fail");
		}

	}

	public void navigateToInterDepartmentPurchaseOrderForm() throws Exception {
		click(btn_newInternelReturnOrder);
		explicitWait(btn_draft2, 40);
		if (isDisplayed(btn_draft2)) {
			writeTestResults("Verify user able to navigate New Inter Department Transfer Order form",
					"User should be able to navigate New Inter Department Transfer Order form",
					"User sucessfully navigate New Inter Department Transfer Order form", "pass");
		} else {
			writeTestResults("Verify user able to navigate  New Inter Department Transfer Order form",
					"User should be able to navigate New Inter Department Transfer Order form",
					"User does'nt navigate  New Inter Department Transfer Order form", "Fail");
		}

	}

	public void completeInterDepartmentTransferOrder() throws Exception {
		// From ware
		selectText(txt_fromWareTO, fromWareInterDepartmentlTransferOrder);

		// To ware
		selectText(txt_toWareTO, toWareInterDepartmentlTransferOrder);

		if (isDisplayed(txt_fromWareTO) && isDisplayed(txt_toWareTO)) {
			writeTestResults(
					"Verify user can select from and destination warehouses and addresses of relevant warehouses are loaded",
					"User should be able to select from and destination warehouses and addresses of relevant warehouses should be loaded",
					"User successfully select from and destination warehouses and addresses of relevant warehouses successfully loaded",
					"pass");
		} else {
			writeTestResults(
					"Verify user can select from and destination warehouses and addresses of relevant warehouses are loaded",
					"User should be able to select from and destination warehouses and addresses of relevant warehouses should be loaded",
					"User couldn't select from and destination warehouses and addresses of relevant warehouses successfully loaded",
					"fail");
		}

		selectText(drop_salesPriceModel, salesPriceModelInterDepartmentalTransferOrder);
		click(btn_salesPriceModelConfirmation);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_salesPriceModel)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(salesPriceModelInterDepartmentalTransferOrder)) {
			writeTestResults("Verify user can select Sales Price Model",
					"User should be able to select Sales Price Model", "User successfully select Sales Price Model",
					"pass");
		} else {
			writeTestResults("Verify user can select Sales Price Model",
					"User should be able to select select Sales Price Model",
					"User couldn't select select Sales Price Model", "fail");
		}

		/* Product add to the grid */
		int j = 0;
		for (int i = 1; i < 8; i++) {
			String str_i = String.valueOf(i);
			click(btn_productLookupStockAdjForLoop.replace("row", str_i).replace("tblStockAdj", "tblTransferOrder"));
			if (isDisplayed(txt_productSearch)) {

				writeTestResults("Verify Product Lookup is pop-up", "Product Lookup should be pop-up",
						"Product Lookup successfully pop-up", "pass");
			} else {
				writeTestResults("Verify Product Lookup is pop-up", "Product Lookup should be pop-up",
						"Product Lookup doesn't pop-up", "fail");
			}

			String product = "";

			if (i == 1) {
				product = "BatchSpecific";
			} else if (i == 2) {
				product = "BatchFifo";
			} else if (i == 3) {
				product = "Lot";
			} else if (i == 4) {
				product = "SerielSpecific";
			} else if (i == 5) {
				product = "SerielBatchSpecific";
			} else if (i == 6) {
				product = "SerielBatchFifo";
			} else if (i == 7) {
				product = "SerirlFifo";
			} else {
				System.out.println("Product type cannot identified.........");
			}
			sleepCusomized(txt_productSearch);
			sendKeys(txt_productSearch, readTestCreation(product));
			pressEnter(txt_productSearch);
			Thread.sleep(3000);

			sleepCusomized(btn_resultProduct);
			if (isDisplayed(btn_resultProduct)) {
				writeTestResults("Verify searched products was loaded", "Searched products should be loaded",
						"Searched products successfully loaded", "pass");
			} else {
				writeTestResults("Verify searched products was loaded", "Searched products should be loaded",
						"Searched products doesn't loaded", "fail");
			}
			sleepCusomized(btn_resultProduct);
			doubleClick(btn_resultProduct);

//			Selected products should be added to the Grid and OnHand Quantity [From][To], Default UOM should be loaded for selected product

			String txt_qty = txt_qtyProductGridInterDepartmentTransferOrder.replace("row", str_i);

			if (isDisplayed(btn_avilabilityCheckWidget)) {
				writeTestResults(
						"Verify selected products should be added to the Grid and OnHand Quantity [From][To], Default UOM were loaded for selected product",
						"Selected products should be added to the Grid and OnHand Quantity [From][To], Default UOM should be loaded for selected product",
						"Selected products successfully added to the Grid and OnHand Quantity [From][To], Default UOM successfully loaded for selected product",
						"pass");
			} else {
				writeTestResults(
						"Verify selected products should be added to the Grid and OnHand Quantity [From][To], Default UOM were loaded for selected product",
						"Selected products should be added to the Grid and OnHand Quantity [From][To], Default UOM should be loaded for selected product",
						"Selected products doesn't added to the Grid and OnHand Quantity [From][To], Default UOM successfully loaded for selected product",
						"fail");
			}

			sendKeys(txt_qty, qtyInterDepartmentTransferOrder);
			if (isDisplayed(txt_qty)) {
				writeTestResults("Verify selected products should be added to the grid",
						"Selected products should be added to the grid",
						"Selected products successfully added to the grid", "pass");
			} else {
				writeTestResults("Verify selected products should be added to the grid",
						"Selected products should be added to the grid", "Selected products doesn't added to the grid",
						"pass");
			}

			click(btn_avilabilityCheckWidget);
			if (j < 7) {
				Thread.sleep(2000);
				Actions action1 = new Actions(driver);
				WebElement we1 = driver.findElement(By.xpath(btn_avilabilytCheckSecondMenu));
				action1.moveToElement(we1).build().perform();
				click(btn_avilabilytCheckSecondMenu);
				try {
					Thread.sleep(3000);
					product_availability[j] = driver.findElement(By.xpath(td_availableStockWarehouseGST_Warehouse))
							.getText().trim();
				} catch (Exception e) {
					System.out.println("0");
				}
				if (product_availability[j] != null) {
					System.out.println(product_availability[j]);
				} else {
					product_availability[j] = ",0";
				}
				sleepCusomized(txt_closePopupProductAvailabilityInterDepartmentTransferOrder);
				click(txt_closePopupProductAvailabilityInterDepartmentTransferOrder);
			}
			j++;

			sleepCusomized(btn_addNewRow);
			click(btn_addNewRow);
		}

		click(btn_deleteEmotyRowStockAdjustment.replace("tblStockAdj", "tblTransferOrder"));

	}

	public void releseInterDepartmentTransferOrder() throws Exception {
		sleepCusomized(lbl_checkoutUnitsBalance014);
		beforeCheckoutUnits = getText(lbl_checkoutUnitsBalance014);
		click(btn_checkout2);
		Thread.sleep(5000);
		Thread.sleep(6000);
		explicitWaitUntillClickable(btn_checkout2, 40);
		String afterCheckoutUnits = getText(lbl_checkoutUnitsBalanceAfter014);

		if (!beforeCheckoutUnits.equals(afterCheckoutUnits)) {

			writeTestResults("Verify user can checkout the 'Inter Department Transfer Order'",
					"User should be able checkout the 'Inter Department Transfer Order",
					"User sucessfully checkout the 'Inter Department Transfer Order", "pass");
		} else {
			writeTestResults("Verify user can checkout the 'Inter Department Transfer Order'",
					"User should be able to checkout the 'Inter Department Transfer Order",
					"User couldn't checkout the 'Inter Department Transfer Order", "fail");
		}

		draft("Inter Department Transfer Order");

		click(btn_relese);
		Thread.sleep(3000);
		explicitWait(btn_goToPageLink, 50);
		Thread.sleep(2000);
		click(btn_goToPageLink);
		Thread.sleep(2000);
		pageRefersh();
		explicitWait(btn_releseConfirmation, 60);
		trackCode = getText(lbl_creationsDocNo);
		if (isDisplayed(btn_releseConfirmation)) {

			writeTestResults("Verify user can releese 'Inter Department Transfer Order'",
					"User should be able to releese 'Inter Department Transfer Order'",
					"User sucessfully releese 'Inter Department Transfer Order'", "pass");
		} else {
			writeTestResults("Verify user can releese 'Inter Department Transfer Order'",
					"User should be able to releese 'Inter Department Transfer Order'",
					"User couldn't releese 'Inter Department Transfer Order'", "fail");
		}

	}

	public void draftReleaseOutboundShipmentAndSerielBatchCapture() throws Exception {
		switchWindow();
		explicitWait(btn_checkout2, 20);
		click(btn_checkout2);
		Thread.sleep(3000);
		if (isDisplayed(btn_checkout2)) {

			writeTestResults("Verify user can checkout the 'Outbound Shipment'",
					"User should be able checkout the 'Outbound Shipment",
					"User sucessfully checkout the 'Outbound Shipment", "pass");
		} else {
			writeTestResults("Verify user can checkout the 'Outbound Shipment'",
					"User should be able to checkout the 'Outbound Shipment",
					"User couldn't checkout the 'Outbound Shipment", "fail");
		}

		draft("Outbound Shipment");

		customizeLoadingDelay(brn_serielBatchCapture, 10);
		click(brn_serielBatchCapture);

		/* Product 01 */
		if (isDisplayed(btn_captureItem1)) {
			writeTestResults(
					"Verify user can navigated Serial/Batch window and products which going to Outbound should be loaded for item 1",
					"User should be navigated Serial/Batch window and products which going to Outbound should be loaded for item 1",
					"User successfully navigated Serial/Batch window and products which going to Outbound should be loaded for item 1",
					"pass");
		} else {
			writeTestResults(
					"Verify user can navigated Serial/Batch window and products which going to Outbound should be loaded for item 1",
					"User should be navigated Serial/Batch window and products which going to Outbound should be loaded for item 1",
					"User couldn't navigated Serial/Batch window and products which going to Outbound doesn't loaded for item 1",
					"fail");
		}

		click(btn_captureItem1);
		if (isDisplayed(btn_capturePageDocIcon)) {
			writeTestResults("Verify Serial/Batch capturing details are enabled",
					"Serial/Batch capturing details should be enabled",
					"Serial/Batch capturing details successfully enabled", "pass");
		} else {
			writeTestResults("Verify Serial/Batch capturing details are enabled",
					"Serial/Batch capturing details should be enabled",
					"Serial/Batch capturing details doesn't enabled", "fail");
		}

		click(btn_capturePageDocIcon);
		if (isDisplayed(chk_availableProductsTransferOrderOutboundShipment.replace("row", "1"))) {
			writeTestResults("Verify existing Batches are loaded as pop-up",
					"Existing batches should be loaded as pop-up", "Existing batches successfully loaded as pop-up",
					"pass");
		} else {
			writeTestResults("Verify existing batches are loaded as pop-up",
					"Existing batches should be loaded as pop-up", "Existing batches doesn't loaded as pop-up", "fail");
		}

		click(chk_availableProductsTransferOrderOutboundShipment.replace("row", "1"));

		String qty = getText(lbl_qtyBatchSpecifificCapturePage);
		String qty_capture = getText(lbl_availQuantityProductCapturuPage);

		int qty_int = Integer.parseInt(qty);
		int qty_capture_int = Integer.parseInt(qty_capture);

		int i = 2;
		while (qty_int >= qty_capture_int) {
			String i_str = String.valueOf(i);
			click(chk_deleteItemCapturePage);
			click(btn_deleteRowProductCaptururePage);
			click(btn_capturePageDocIcon);
			click(chk_availableProductsTransferOrderOutboundShipment.replace("row", i_str));
			Thread.sleep(2000);
			qty_capture = getText(lbl_availQuantityProductCapturuPage);
			qty_capture_int = Integer.parseInt(qty_capture);
		}

		if (qty_int <= qty_capture_int) {
			writeTestResults("Verify selected batches are added to the grid",
					"Selected batches should be added to the grid", "Selected batches successfully added to the grid",
					"pass");
		} else {
			writeTestResults("Verify selected batches are added to the grid",
					"Selected batches should be added to the grid", "Selected batches doesn't added to the grid",
					"fail");
		}

		click(btn_updateCapture);

		String pro = getAttribute(btn_captureItem1, "class");

		if (pro.equals("fli-number green-vackground-fli-number")
				&& pro.equals("fli-number green-vackground-fli-number")) {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User sucessfully capture 'Seriel/Batch'", "pass");
		} else {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User couldn't capture 'Seriel/Batch'", "fail");
		}

		/* Product 04 */
		String item_xpath = btn_itemProductCapturePage.replace("_product_number", "4");
		sleepCusomized(item_xpath);

		if (isDisplayed(item_xpath)) {
			writeTestResults(
					"Verify user can navigated Serial/Batch window and products which going to Outbound should be loaded for item 4",
					"User should be navigated Serial/Batch window and products which going to Outbound should be loaded for item 4",
					"User successfully navigated Serial/Batch window and products which going to Outbound should be loaded for item 4",
					"pass");
		} else {
			writeTestResults(
					"Verify user can navigated Serial/Batch window and products which going to Outbound should be loaded for item 4",
					"User should be navigated Serial/Batch window and products which going to Outbound should be loaded for item 4",
					"User couldn't navigated Serial/Batch window and products which going to Outbound doesn't loaded for item 4",
					"fail");
		}

		click(item_xpath);
		if (isDisplayed(chk_productCaptureRange)) {
			writeTestResults("Verify Serial/Batch capturing details are enabled",
					"Serial/Batch capturing details should be enabled",
					"Serial/Batch capturing details successfully enabled", "pass");
		} else {
			writeTestResults("Verify Serial/Batch capturing details are enabled",
					"Serial/Batch capturing details should be enabled",
					"Serial/Batch capturing details doesn't enabled", "fail");
		}
		click(chk_productCaptureRange);

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$('#attrSlide-txtSerialCount').val(" + qtyTransferOrder + ")");

		String lot4 = getOngoinfSerirlSpecificSerielNumber(10);
		String final_lot_script = "$('#attrSlide-txtSerialFrom').val(\"" + lot4 + "\")";
		jse.executeScript(final_lot_script);

		Thread.sleep(2000);
		pressEnter(txt_seriealProductCapture);

		if (qty_int <= qty_capture_int) {
			writeTestResults("Verify selected batches are added to the grid",
					"Selected batches should be added to the grid", "Selected batches successfully added to the grid",
					"pass");
		} else {
			writeTestResults("Verify selected batches are added to the grid",
					"Selected batches should be added to the grid", "Selected batches doesn't added to the grid",
					"fail");
		}
		Thread.sleep(2500);
		click(btn_updateCapture);
		Thread.sleep(2000);
		String pro1 = getAttribute(item_xpath, "class");
		if (!pro1.equals("fli-number green-vackground-fli-number")) {
			Thread.sleep(6000);
			pro1 = getAttribute(item_xpath, "class");
		}
		if (pro1.equals("fli-number green-vackground-fli-number")) {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User sucessfully capture 'Seriel/Batch'", "pass");
		} else {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User couldn't capture 'Seriel/Batch'", "fail");
		}

		/* Product 05 */
		String item_xpath1 = btn_itemProductCapturePage.replace("_product_number", "5");
		sleepCusomized(item_xpath1);
		if (isDisplayed(item_xpath1)) {
			writeTestResults(
					"Verify user can navigated Serial/Batch window and products which going to Outbound should be loaded for item 5",
					"User should be navigated Serial/Batch window and products which going to Outbound should be loaded for item 5",
					"User successfully navigated Serial/Batch window and products which going to Outbound should be loaded for item 5",
					"pass");
		} else {
			writeTestResults(
					"Verify user can navigated Serial/Batch window and products which going to Outbound should be loaded for item 5",
					"User should be navigated Serial/Batch window and products which going to Outbound should be loaded for item 5",
					"User couldn't navigated Serial/Batch window and products which going to Outbound doesn't loaded for item 5",
					"fail");
		}
		click(item_xpath1);

		if (isDisplayed(chk_productCaptureRange)) {
			writeTestResults("Verify Serial/Batch capturing details are enabled",
					"Serial/Batch capturing details should be enabled",
					"Serial/Batch capturing details successfully enabled", "pass");
		} else {
			writeTestResults("Verify Serial/Batch capturing details are enabled",
					"Serial/Batch capturing details should be enabled",
					"Serial/Batch capturing details doesn't enabled", "fail");
		}
		click(chk_productCaptureRange);

		jse.executeScript("$('#attrSlide-txtSerialCount').val(" + qtyTransferOrder + ")");

		String final_lot_script1 = "$('#attrSlide-txtSerialFrom').val(\""
				+ getOngoingSerielBatchSpecificSerielNumber(10) + "\")";
		jse.executeScript(final_lot_script1);

		pressEnter(txt_seriealProductCapture);

		if (qty_int <= qty_capture_int) {
			writeTestResults("Verify selected batches are added to the grid",
					"Selected batches should be added to the grid", "Selected batches successfully added to the grid",
					"pass");
		} else {
			writeTestResults("Verify selected batches are added to the grid",
					"Selected batches should be added to the grid", "Selected batches doesn't added to the grid",
					"fail");
		}

		click(btn_updateCapture);
		Thread.sleep(2000);
		String pro2 = getAttribute(btn_captureItem1, "class");
		String pro4 = getAttribute(item_xpath, "class");
		String pro5 = getAttribute(item_xpath1, "class");
		if (!pro5.equals("fli-number green-vackground-fli-number")) {
			Thread.sleep(6000);
			pro5 = getAttribute(item_xpath, "class");
		}
		if (pro2.equals("fli-number green-vackground-fli-number")
				&& pro4.equals("fli-number green-vackground-fli-number")
				&& pro5.equals("fli-number green-vackground-fli-number")) {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User sucessfully capture 'Seriel/Batch'", "pass");
		} else {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User couldn't capture 'Seriel/Batch'", "fail");
		}

		sleepCusomized(btn_backButtonUpdateCapture);
		click(btn_backButtonUpdateCapture);

		sleepCusomized(btn_relese);
		if (isDisplayed(btn_relese)) {

			writeTestResults("Verify user can navigated to Outbound Shipment Form",
					"User should be able to navigated to Outbound Shipment Form",
					"User sucessfully navigated to Outbound Shipment Form", "pass");
		} else {
			writeTestResults("Verify user can navigated to Outbound Shipment Form",
					"User should be able to navigated to Outbound Shipment Form",
					"User couldn't navigated to Outbound Shipment Form", "fail");
		}
		Thread.sleep(3000);
		trackCode = getText(lbl_creationsDocNo);
		click(btn_relese);
		sleepCusomized(btn_goToPageLink);
		if (isDisplayed(btn_goToPageLink)) {

			writeTestResults("Verify user can releese Outbound Shipment",
					"User should be able to releese Outbound Shipment", "User sucessfully releese Outbound Shipment",
					"pass");
		} else {
			writeTestResults("Verify user can releese Outbound Shipment",
					"User should be able to releese Outbound Shipment", "User couldn't releese Outbound Shipment",
					"fail");
		}

		sleepCusomized(btn_goToPageLink);
		if (isDisplayed(btn_goToPageLink)) {

			writeTestResults("Verify user can complete Outbound Shipment",
					"User can be able to complete Outbound Shipment", "User sucessfully complete Outbound Shipment",
					"pass");
		} else {
			writeTestResults("Verify user can complete Outbound Shipment",
					"User can be able to complete Outbound Shipment", "User not complete Outbound Shipment", "Fail");
		}

	}

	public void inboundShipmentInterDepartmentTransferOrder() throws Exception {
		click(btn_goToPageLink);
		switchWindow();
		click(btn_checkout);
		if (isDisplayed(btn_checkout)) {

			writeTestResults("Verify user can navigated to Inbound Shipment",
					"User should be able to navigated to Inbound Shipment",
					"User sucessfully navigated to Inbound Shipment", "pass");
		} else {
			writeTestResults("Verify user can navigated to Inbound Shipment",
					"User should be able to navigated to Inbound Shipment",
					"User couldn't navigated to Inbound Shipment", "fail");
		}
		Thread.sleep(2000);
		click(btn_draft);
		customizeLoadingDelay(btn_draft, 40);
		if (isDisplayed(btn_relese)) {

			writeTestResults("Verify user can draft \'Inbound Shipment\'",
					"User should be able to draft \'Inbound Shipment\'", "User sucessfully draft \'Inbound Shipment\'",
					"pass");
		} else {
			writeTestResults("Verify user can draft \'Inbound Shipment\'",
					"User should be able to draft \'Inbound Shipment\'", "User couldn't draft \'Inbound Shipment\'",
					"fail");
		}

		click(btn_relese);
		customizeLoadingDelay(header_releasedInboundShipment, 40);
		trackCode = getText(lbl_creationsDocNo);
		if (isDisplayed(header_releasedInboundShipment)) {

			writeTestResults("Verify user can releese \'Inbound Shipment\'",
					"User should be able to releese \'Inbound Shipment\'",
					"User sucessfully releese \'Inbound Shipment\'", "pass");
		} else {
			writeTestResults("Verify user can releese \'Inbound Shipment\'",
					"User should be able to releese \'Inbound Shipment\'", "User couldn't releese \'Inbound Shipment\'",
					"fail");
		}

	}

	public void checkWarehouseForProdctAvailabilityInterDepartmentalTransferOrderTransferOrder() throws Exception {
		String[] product_availability_after = new String[7];
		int row_position = 1;
		for (int i = 0; i < 7; i++) {
			String row_str = String.valueOf(row_position);
			customizeLoadingDelay(btn_productAvilabilityWidgetShipmentDetailsTable.replace("row", row_str), 40);
			click(btn_productAvilabilityWidgetShipmentDetailsTable.replace("row", row_str));
			Thread.sleep(2000);
			Actions action1 = new Actions(driver);
			WebElement we1 = driver.findElement(By.xpath(btn_avilabilytCheckSecondMenu));
			action1.moveToElement(we1).build().perform();
			click(btn_avilabilytCheckSecondMenu);
			sleepCusomized(td_availableStockGstmWarehouseAfterRelesed);
			product_availability_after[i] = getText(td_availableStockGstmWarehouseAfterRelesed);
			System.out.println(product_availability_after[i]);
			click(btn_closeAvailabilityCheckPopup);
			row_position++;
		}

		boolean warehouse_check_flag = false;
		for (int j = 0; j < 7; j++) {
			String qty = product_availability[j].replaceAll(",", "");
			int qty_int = Integer.parseInt(qty);
			String after_qty = product_availability_after[j].replaceAll(",", "");
			try {
				int after_qty_int = Integer.parseInt(after_qty);
				int adding_qty = Integer.parseInt(qtyTransferOrder);
				if (after_qty_int == (qty_int - adding_qty)) {
					warehouse_check_flag = true;
				} else {
					warehouse_check_flag = false;
					break;
				}
			} catch (Exception e) {
				System.out.println(e);
			}

		}

		if (warehouse_check_flag) {
			writeTestResults("Verify stock was transfered to the destination",
					"Stock should be transfered to the destination", "Stock successfully transfered to the destination",
					"pass");
		} else {
			writeTestResults("Verify stock was transfered to the destination",
					"Stock should be transfered to the destination", "Stock doesn't transfered to the destination",
					"fail");
		}

	}

	// IW_TC_008
	public void naigateToProductConversionForm() throws Exception {
		navigateInventoryAndWarehouseModule();

		click(btn_productConversion);
		if (isDisplayed(bnt_newProductConversion)) {

			writeTestResults("Verify user can navigate to the 'Product Conversion' page",
					"User can navigate to the 'Product Conversion' page",
					"User sucessfully navigate to the 'Product Conversion' page", "pass");
		} else {
			writeTestResults("Verify user can navigate to the 'Product Conversion' page",
					"User can navigate to the 'Product Conversion' page",
					"User couldn't navigate to the 'Product Conversion' page", "fail");
		}

		click(bnt_newProductConversion);

		if (isDisplayed(lbl_bokNoProductConversion)) {

			writeTestResults("Verify user can navigate to the 'Product Conversion' form",
					"User can navigate to the 'Product Conversion' form",
					"User sucessfully navigate to the 'Product Conversion' form", "pass");
		} else {
			writeTestResults("Verify user can navigate to the 'Product Conversion' form",
					"User can navigate to the 'Product Conversion' form",
					"User couldn't navigate to the 'Product Conversion' form", "pass");
		}
	}

	public void fillProductConversionForm() throws Exception {
		selectText(drop_wareProductCon, fromWareTO);

		if (isDisplayed(drop_wareProductCon)) {

			writeTestResults("Verify user can select warehouse", "User can select warehouse",
					"User sucessfully select warehouse", "pass");
		} else {
			writeTestResults("Verify user can select warehouse", "User can select warehouse",
					"User couldn't select warehouse", "fail");
		}

		click(btn_productLookupOroductConversion);
		sleepCusomized(heder_popupProductLookupProductConversion);
		if (isDisplayed(heder_popupProductLookupProductConversion)) {

			writeTestResults("Verify user can navigate to Product Lookup Popup",
					"User can navigate to Product Lookup Popup", "User sucessfully navigate to Product Lookup Popup",
					"pass");
		} else {
			writeTestResults("Verify user can navigate to Product Lookup Popup",
					"User can navigate to Product Lookup Popup", "User couldn't navigate to Product Lookup Popup",
					"fail");
		}

		sendKeys(txt_productSearch, productStockConversion);
		pressEnter(txt_productSearch);
		Thread.sleep(3000);
		sleepCusomized(btn_resultProduct);
		doubleClick(btn_resultProduct);

		if (isDisplayed(div_productNameProductGrigPrductConversion)) {

			writeTestResults("Verify user can select 'Batch Product'", "User should be able to select 'Batch Product'",
					"User sucessfully select 'Batch Product'", "pass");
		} else {
			writeTestResults("Verify user can select 'Batch Product'", "User should be able to select 'Batch Product'",
					"User couldn't select 'Batch Product'", "fail");
		}

		sendKeys(txt_quantityProductConversion, qtyProductConvertionFromProduct);

		if (isDisplayed(txt_quantityProductConversion)) {

			writeTestResults("Verify user can enter input product quantity",
					"User should be able to enter input product quantity",
					"User sucessfully enter input product quantity", "pass");
		} else {
			writeTestResults("Verify user can enter input product quantity",
					"User should be able to enter input product quantity", "User couldn't enter input product quantity",
					"fail");
		}

		click(btn_serialNosFromProductConversion);

		Thread.sleep(3000);
		if (isDisplayed(btn_seraialBatchCaptureDocIconProductConversion)) {

			writeTestResults("Verify user can navigate to Serial/Batch Capture Popup",
					"User can navigate to Serial/Batch Capture Popup",
					"User sucessfully navigate to Serial/Batch Capture Popup", "pass");
		} else {
			writeTestResults("Verify user can navigate to Serial/Batch Capture Popup",
					"User can navigate to Serial/Batch Capture Popup",
					"User couldn't navigate to Serial/Batch Capture Popup", "fail");
		}

		sleepCusomized(btn_seraialBatchCaptureDocIconProductConversion);
		click(btn_seraialBatchCaptureDocIconProductConversion);

		if (isDisplayed(header_existinfSerialBatchProductConversion)) {

			writeTestResults("Verify user can navigate to Existing Serial/Batch Capture Popup",
					"User can navigate to Existing Serial/Batch Capture Popup",
					"User sucessfully navigate to Existing Serial/Batch Capture Popup", "pass");
		} else {
			writeTestResults("Verify user can navigate to Existing Serial/Batch Capture Popup",
					"User can navigate to Existing Serial/Batch Capture Popup",
					"User couldn't navigate to Existing Serial/Batch Capture Popup", "fail");
		}

		click(chk_batchSelectProductConversion);
		if (isDisplayed(lnk_resutlSeraialBatchCaptureProductConversion)) {

			writeTestResults("Verify selected batches are loaded", "Selected batches sohuld be loaded",
					"Selected batches sucessfully loaded", "pass");
		} else {

			writeTestResults("Verify selected batches are loaded", "Selected batches sohuld be loaded",
					"Selected batches does'nt loaded", "fail");
		}

		sendKeys(txt_qtyToProductProductConversion, qtyProductConvertionFromProduct);
		if (isDisplayed(txt_qtyToProductProductConversion)) {

			writeTestResults("Verify user can add quantity", "user should be able to add quantity",
					"User sucessfully add quantity", "pass");
		} else {

			writeTestResults("Verify user can add quantity", "user should be able to add quantity",
					"User couldn't add quantity", "fail");
		}

		click(btn_applyProductBatchCaptureProductConversion);

		if (isDisplayed(header_productConversionPage)) {

			writeTestResults("Verify user can redirected to 'Product Conversion Form'",
					"User should be redirected to 'Product Conversion Form'",
					"User sucessfully redirected to 'Product Conversion Form'", "pass");
		} else {

			writeTestResults("Verify user can redirected to 'Product Conversion Form'",
					"User should be redirected to 'Product Conversion Form'",
					"User couldn't redirected to 'Product Conversion Form'", "fail");
		}

		Thread.sleep(2000);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript(btn_productLoocupToProductProductConversion);

		sleepCusomized(heder_popupProductLookupProductConversion);
		if (isDisplayed(heder_popupProductLookupProductConversion)) {

			writeTestResults("Verify user can navigate to Product Lookup Popup",
					"User can navigate to Product Lookup Popup", "User sucessfully navigate to Product Lookup Popup",
					"pass");
		} else {
			writeTestResults("Verify user can navigate to Product Lookup Popup",
					"User can navigate to Product Lookup Popup", "User couldn't navigate to Product Lookup Popup",
					"fail");
		}

		// Product 02
		sleepCusomized(txt_productSearch);
		sendKeys(txt_productSearch, productTo08);
		pressEnter(txt_productSearch);
		Thread.sleep(3000);
		sleepCusomized(btn_resultProduct);
		doubleClick(btn_resultProduct);

		if (isDisplayed(div_productNameProductGrigPrductConversionToProduc1)) {

			writeTestResults("Verify user can select 'Batch Product'", "User should be able to select 'Batch Product'",
					"User sucessfully select 'Batch Product'", "pass");
		} else {
			writeTestResults("Verify user can select 'Batch Product'", "User should be able to select 'Batch Product'",
					"User couldn't select 'Batch Product'", "fail");
		}

		sendKeys(txt_costToProductOroductConversion, toProductCoseProductConversion);

		if (isDisplayed(div_productNameProductGrigPrductConversionToProduc1)) {

			writeTestResults("Verify user can add cost price", "User should be able to add cost price",
					"User sucessfully add cost price", "pass");
		} else {
			writeTestResults("Verify user can add cost price", "User should be able to add cost price",
					"User couldn't add cost price", "fail");
		}

		if (isDisplayed(txt_quntityToProductProductConversion)) {

			writeTestResults("Verify user can add cost price", "User should be able to add cost price",
					"User sucessfully add cost price", "pass");
		} else {
			writeTestResults("Verify user can add cost price", "User should be able to add cost price",
					"User couldn't add cost price", "fail");
		}

		click(btn_serialNosFromProductConversionToProduc);

		Thread.sleep(3000);
		if (isDisplayed(btn_seraialBatchCaptureDocIconProductConversion)) {
			writeTestResults("Verify user can navigate to Nos Window Popup Popup",
					"User can navigate to Nos Window Popup", "User sucessfully navigate to Nos Window Popup Popup",
					"pass");
		} else {
			writeTestResults("Verify user can navigate to Nos Window Popup Popup",
					"User can navigate to Nos Window Popup Popup", "User couldn't navigate to Nos Window Popup Popup",
					"fail");
		}

		sendKeys(txt_batchNoCaptureProductConversionNosPopup, genProductId() + currentTimeAndDate());
		click(btn_exoiryDateProductConversionToProduct);
		click(btn_clanderFw);
		click(btn_date31ProductConversionToProduct);
		String lot_to_pro = lotNoProductConversionToProduct + genProductId() + currentTimeAndDate();
		sendKeys(txt_LotNoProductConversion, lot_to_pro);

		if (isDisplayed(txt_batchNoCaptureProductConversionNosPopup) && isDisplayed(txt_LotNoProductConversion)
				&& isDisplayed(btn_exoiryDateProductConversionToProduct)) {

			writeTestResults("Verify user can enter valid Serial No, Lot No and Expiry Date",
					"User should be able to enter valid Serial No, Lot No and Expiry Date",
					"User sucessfully enter valid Serial No, Lot No and Expiry Date", "pass");
		} else {
			writeTestResults("Verify user can enter valid Serial No, Lot No and Expiry Date",
					"User should be able to enter valid Serial No, Lot No and Expiry Date",
					"User couldn't enter valid Serial No, Lot No and Expiry Date", "fail");
		}

		pressEnter(txt_batchNoCaptureProductConversionNosPopup);

		Thread.sleep(3000);
		String result_id_capturegrid = getText(lbl_resultCaptureToProductProductConversion);
		if (result_id_capturegrid.equals(lot_to_pro)) {

			writeTestResults("Verify Serial details are added to the grid",
					"Serial details should be added to the grid", "Serial details are successfully to the grid",
					"pass");
		} else {
			writeTestResults("Verify Serial details are added to the grid",
					"Serial details should be added to the grid", "Serial details does'nt added to the grid", "fail");
		}

		click(btn_applyProductBatchCaptureProductConversion);
		if (isDisplayed(header_productConversionPage)) {

			writeTestResults("Verify user can redirected to 'Product Conversion Form'",
					"User should be redirected to 'Product Conversion Form'",
					"User sucessfully redirected to 'Product Conversion Form'", "pass");
		} else {

			writeTestResults("Verify user can redirected to 'Product Conversion Form'",
					"User should be redirected to 'Product Conversion Form'",
					"User couldn't redirected to 'Product Conversion Form'", "fail");
		}

	}

	public void productConversionDraftReelese() throws Exception {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		Thread.sleep(1000);
		jse.executeScript(btn_draftBayJavaScrptFirstButton);
		Thread.sleep(3000);
		explicitWait(btn_relese, 50);
		if (isDisplayed(btn_relese)) {

			writeTestResults("Verify user can draft 'Product Conversion'",
					"User should be able to draft 'Product Conversion'", "User sucessfully draft 'Product Conversion'",
					"pass");
		} else {
			writeTestResults("Verify user can draft 'Product Conversion'",
					"User should be able to draft 'Product Conversion'", "User couldn't draft 'Product Conversion'",
					"fail");
		}
		click(btn_relese);
		customizeLoadingDelay(lbl_releasedStatusProductConversion, 40);

		if (isDisplayed(lbl_releasedStatusProductConversion)) {
			trackCode = getText(lbl_creationsDocNo);
			writeTestResults("Verify user can releese Product Conversion",
					"User should be able to releese Product Conversion", "User sucessfully releese Product Conversion",
					"pass");
		} else {
			writeTestResults("Verify user can releese Product Conversion",
					"User should be able to releese Product Conversion", "User couldn't releese Product Conversion",
					"fail");
		}
	}

	// IW_TC_009
	public void navigateToNewStockTakePage() throws Exception {

		navigateInventoryAndWarehouseModule();
		click(btn_stockTake);
		Thread.sleep(3000);
		String header = getText(header_page);
		if (header.equals("Stock Take")) {
			writeTestResults("Verify user can navigate to Stock Take by-page",
					"User should be able to navigate to Stock Take by-page",
					"User successfully navigate to Stock Take by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Stock Take by-page",
					"User should be able to navigate to Stock Take by-page",
					"User could'nt navigate to Stock Take by-page", "fail");
		}

		explicitWait(btn_newStockTake, 50);
		click(btn_newStockTake);
		explicitWait(header_newStockTake, 50);
		String status = getText(lbl_docStatus1);
		if (status.equals("New")) {
			writeTestResults("Verify user can navigate to new Stock Take page",
					"User should be able to navigate to new Stock Take page",
					"User successfully navigate to new Stock Take page", "pass");
		} else {
			writeTestResults("Verify user can navigate to new Stock Take page",
					"User should be able to navigate to new Stock Take page",
					"User could'nt navigate to new Stock Take page", "fail");
		}

	}

	public void stockTakeProcess() throws Exception {
		selectText(drop_warehouseStockTske, warehouseStockTake);
		if (isDisplayed(drop_warehouseStockTske)) {
			writeTestResults("Verify user can select warehouse", "User should be able to select warehouse",
					"User successfully select warehouse", "pass");
		} else {
			writeTestResults("Verify user can select warehouse", "User should be able to select warehouse",
					"User could'nt select warehouse", "fail");
		}
		click(btn_refreshStockTake);
		Thread.sleep(2000);
		explicitWait(row_stockTakeTable, 40);
		if (isDisplayed(row_stockTakeTable)) {
			writeTestResults(
					"Verify products were laoded which exist in the system with Rack Code, Default UOM and System Balance",
					"Products should be laoded which exist in the system with Rack Code, Default UOM and System Balance",
					"Products successfully laoded which exist in the system with Rack Code, Default UOM and System Balance",
					"pass");
		} else {
			writeTestResults(
					"Verify products were laoded which exist in the system with Rack Code, Default UOM and System Balance",
					"Products should be laoded which exist in the system with Rack Code, Default UOM and System Balance",
					"Products doesn't laoded which exist in the system with Rack Code, Default UOM and System Balance",
					"pass");
		}

		sendKeys(txt_filterProductStockTake, batchProduct1StockTake);
		String product1_quantity = getText(txt_systemQuantityStockTake);
		int product1_quantity1 = Integer.parseInt(product1_quantity);
		int product1_quantity2 = product1_quantity1 + 5;
		String product1_final = String.valueOf(product1_quantity2);
		sendKeys(txt_physicalQuantityStockTake, product1_final);
		click(txt_systemQuantityStockTake);
		String product1_after_quantity = getAttribute(txt_physicalQuantityStockTake, "value");
		boolean flag1 = !product1_quantity.equals(product1_after_quantity);

		sendKeys(txt_filterProductStockTake, batchProduct2StockTake);
		String product2_quantity1 = getText(txt_systemQuantityStockTake);
		String replace_comma_product_quantity = product2_quantity1.replace(",", "");
		int product2_quantity11 = Integer.parseInt(replace_comma_product_quantity);
		int product2_quantity2 = product2_quantity11 - 5;
		String product2_final = String.valueOf(product2_quantity2);
		sendKeys(txt_physicalQuantityStockTake, product2_final);
		click(txt_systemQuantityStockTake);
		String product2_after_quantity = getAttribute(txt_physicalQuantityStockTake, "value");
		boolean flag2 = !product2_quantity1.equals(product2_after_quantity);
		if (flag1 && flag2) {
			writeTestResults("Verify user can change the physical balances",
					"User should be able to change the physical balances",
					"User successfully change the physical balances", "pass");
		} else {
			writeTestResults("Verify user can change the physical balances",
					"User should be able to change the physical balances", "User could'nt change the physical balances",
					"fail");
		}
	}

	public void draftReleeseStockTake() throws Exception {
		click(btn_draft);
		Thread.sleep(3000);
		if (isDisplayed(btn_relese)) {
			writeTestResults("Verify user can draft Stock Take with the changed Qty",
					"User should be able to draft Stock Take with the changed Qty",
					"User successfully draft Stock Take with the changed Qty", "pass");
		} else {
			writeTestResults("Verify user can draft Stock Take with the changed Qty",
					"User should be able to draft Stock Take with the changed Qty",
					"User could'nt draft Stock Take with the changed Qty", "fail");
		}

		click(btn_relese);
		Thread.sleep(3000);
		trackCode = getText(lbl_creationsDocNo);
		String varience_pro1 = getText(td_varianvcePro1);
		String varience_pro2 = getText(td_varianvcePro2);

		if (varience_pro1.equals("5") && varience_pro2.equals("-5")) {
			writeTestResults(
					"Verify user can release Stock Take with the changed qty and variance quantity displayed correctly",
					"User should be able to release Stock Take with the changed qty and variance quantity displayed correctly",
					"User successfully release Stock Take with the changed qty and variance quantity displayed correctly",
					"pass");
		} else {
			writeTestResults(
					"Verify user can release Stock Take with the changed qty and variance quantity displayed correctly",
					"User should be able to release Stock Take with the changed qty and variance quantity displayed correctly",
					"User could'nt release Stock Take with the changed qty and variance quantity not displayed correctly",
					"fail");
		}
	}

	public void convertStockAdjustmentStockTake() throws Exception {
		click(btn_action);
		if (isDisplayed(btn_convertToStockAdjustment)) {
			writeTestResults("Verify set of actions were loaded", "Set of actions should be loaded",
					"Set of actions successfully loaded", "pass");
		} else {
			writeTestResults("Verify set of actions were loaded", "Set of actions should be loaded",
					"Set of actions couldn't loaded", "fail");
		}

		click(btn_convertToStockAdjustment);
		sleepCusomized(heade_page);
		Thread.sleep(3000);
		String status = getText(heade_page);
		if (status.equals("Stock Adjustment")) {
			writeTestResults("Verify user can navigate to Stock Adjustment new page with the details",
					"User should be able to navigate to Stock Adjustment new page with the details",
					"User successfully navigate to Stock Adjustment new page with the details", "pass");
		} else {
			writeTestResults("Verify user can navigate to Stock Adjustment new page with the details",
					"User should be able to navigate to Stock Adjustment new page with the details",
					"User could'nt navigate to Stock Adjustment new page with the details", "fail");
		}

		sendKeys(txt_descStockAdj, currentTimeAndDate());
		if (isDisplayed(txt_descStockAdj)) {
			writeTestResults("Verify user can navigate to Stock Adjustment new page with the details",
					"User should be able to navigate to Stock Adjustment new page with the details",
					"User successfully navigate to Stock Adjustment new page with the details", "pass");
		} else {
			writeTestResults("Verify user can navigate to Stock Adjustment new page with the details",
					"User should be able to navigate to Stock Adjustment new page with the details",
					"User could'nt navigate to Stock Adjustment new page with the details", "fail");
		}
	}

	public void draftReleaseStockAdjustment() throws Exception {
		click(btn_draft);
		Thread.sleep(3000);
		String status1 = getText(header_draft);
		if (status1.equals("( Draft )")) {
			writeTestResults("Verify user can draft Stock Adjustment", "User should be able to draft Stock Adjustment",
					"User successfully draft Stock Adjustment", "pass");
		} else {
			writeTestResults("Verify user can draft Stock Adjustment", "User should be able to draft Stock Adjustment",
					"User could'nt draft Stock Adjustment", "fail");
		}

		sleepCusomized(brn_serielBatchCapture);
		click(brn_serielBatchCapture);
		click(btn_captureItem1);
		sendKeys(txt_batchNoCapturePage, currentTimeAndDate());
		sendKeys(txt_lotNoCapturePageStockTake, currentTimeAndDate());
		pressEnter(txt_lotNoCapturePageStockTake);
		click(btn_updateCapture);

		click(btn_captureItem2);
		click(btn_capturePageDocIcon);
		click(chk_captureFirstRowSelect);
		click(btn_updateCapture);

		String pro1 = getAttribute(btn_serielBatchCaptureProduct1ILOOutboundShipment, "class");
		String pro2 = getAttribute(btn_serielBatchCaptureProduct2ILOOutboundShipment, "class");

		if (pro1.equals("fli-number green-vackground-fli-number")
				&& pro2.equals("fli-number green-vackground-fli-number")) {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User sucessfully capture 'Seriel/Batch'", "pass");
		} else {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User couldn't capture 'Seriel/Batch'", "fail");
		}
		pageRefersh();
		sleepCusomized(btn_releese2);
		click(btn_releese2);
		Thread.sleep(3000);
		trackCode = getText(lbl_creationsDocNo);
		String status2 = getText(lbl_docStatus);
		if (status2.equals("( Released )")) {
			writeTestResults("Verify user can release Stock Adjustment",
					"User should be able to release Stock Adjustment", "User successfully release Stock Adjustment",
					"pass");
		} else {
			writeTestResults("Verify user can release Stock Adjustment",
					"User should be able to release Stock Adjustment", "User could'nt release Stock Adjustment",
					"fail");
		}

	}

	// IW_TC_010
	public void navigateToInternelOrderPage() throws Exception {
		navigateInventoryAndWarehouseModule();
		click(btn_internelOrder);
		if (isDisplayed(btn_verifyInternelOrderPage)) {

			writeTestResults("Verify user sucessfully navigate to Internel Order page",
					"User should be navigate to Internel Order Page",
					"User sucessfully navigate to Internel Order Page", "pass");
		} else {
			writeTestResults("Verify user sucessfully navigate to Internel Order page",
					"User should be navigate to Internel Order Page", "User does'nt navigate to Internel Order Page",
					"fail");
		}

	}

	public void navigateToNewInternelOrderForm() throws Exception {

		click(btn_newInternelOrder);

		click(btn_employeeRequestJourneyInternelOrder);

	}

	public void fillNewInternelOrderForm() throws Exception {
		explicitWait(txt_tiltleInternelOrder, 50);
		sendKeys(txt_tiltleInternelOrder, titleInternelOrder);

		// Order date get default by current date

		click(btn_searchRequester);

		pressEnter(txt_RequesterSearch);

		doubleClick(lnk_searchResultRequester);

		employee = getAttribute(lnk_searchResultRequester, "title");

		/* Product add to the grid */
		int j = 0;
		for (int i = 1; i < 8; i++) {
			String str_i = String.valueOf(i);

			String lookup_path = btn_productLookupInterDeprtmentTransferOrderForLoop.replace("row", str_i)
					.replace("column", "11").replace("tblStockAdj", "tblInternalRequest");
			click(lookup_path);
			sleepCusomized(txt_productSearch);
			if (isDisplayed(txt_productSearch)) {

				writeTestResults("Verify Product Lookup is pop-up", "Product Lookup should be pop-up",
						"Product Lookup successfully pop-up", "pass");
			} else {
				writeTestResults("Verify Product Lookup is pop-up", "Product Lookup should be pop-up",
						"Product Lookup doesn't pop-up", "fail");
			}

			String product = "";

			if (i == 1) {
				product = "BatchSpecific";
			} else if (i == 2) {
				product = "BatchFifo";
			} else if (i == 3) {
				product = "Lot";
			} else if (i == 4) {
				product = "SerielSpecific";
			} else if (i == 5) {
				product = "SerielBatchSpecific";
			} else if (i == 6) {
				product = "SerielBatchFifo";
			} else if (i == 7) {
				product = "SerirlFifo";
			} else {
				System.out.println("Product type cannt identified.........");
			}
			sleepCusomized(txt_productSearch);
			sendKeys(txt_productSearch, readTestCreation(product));
			pressEnter(txt_productSearch);
			Thread.sleep(3000);
			sleepCusomized(btn_resultProduct);
			if (isDisplayed(btn_resultProduct)) {
				writeTestResults("Verify searched products was loaded", "Searched products should be loaded",
						"Searched products successfully loaded", "pass");
			} else {
				writeTestResults("Verify searched products was loaded", "Searched products should be loaded",
						"Searched products doesn't loaded", "fail");
			}

			customizeLoadingDelay(result_cpmmonProduct.replace("product", readTestCreation(product)), 30);
			doubleClick(result_cpmmonProduct.replace("product", readTestCreation(product)));

//			Selected products should be added to the Grid and OnHand Quantity [From][To], Default UOM should be loaded for selected product

			String txt_qty = txt_quantityInGrid.replace("row", str_i).replace("tblName", "tblInternalRequest");

			explicitWaitUntillClickable(txt_qty, 7);

			if (isDisplayed(btn_avilabilityCheckWidget)) {
				writeTestResults("Verify selected products should be added to the grid",
						"Selected products should be added to the grid",
						"Selected products successfully added to the grid", "pass");
			} else {
				writeTestResults("Verify selected products should be added to the grid",
						"Selected products should be added to the grid", "Selected products doesn't added to the grid",
						"fail");
			}

			Thread.sleep(2000);
			sendKeys(txt_qty, qtyInterDepartmentTransferOrder);
			if (isDisplayed(txt_qty)) {
				writeTestResults("Verify user can set quantity", "User should be able set quantity",
						"User successfully set quantity ", "pass");
			} else {
				writeTestResults("Verify user can set quantity", "User should be able set quantity",
						"User couldn't set quantity ", "fail");
			}

			click(btn_avilabilityCheckWidget);
			if (j < 7) {
				Thread.sleep(2000);
				Actions action1 = new Actions(driver);
				WebElement we1 = driver.findElement(By.xpath(btn_avilabilytCheckSecondMenu));
				action1.moveToElement(we1).build().perform();
				sleepCusomized(btn_avilabilytCheckSecondMenu);
				click(btn_avilabilytCheckSecondMenu);
				try {
					explicitWait(td_availableStockWarehouseGST_Warehouse, 8);
					product_availability[j] = driver.findElement(By.xpath(td_availableStockWarehouseGST_Warehouse))
							.getText().trim();
				} catch (Exception e) {
					System.out.println("0");
				}
				if (product_availability[j] != null) {
					System.out.println(product_availability[j]);
				} else {
					product_availability[j] = ",0";
				}
				sleepCusomized(txt_closePopupProductAvailabilityInterDepartmentTransferOrder);
				click(txt_closePopupProductAvailabilityInterDepartmentTransferOrder);
			}
			j++;

			sleepCusomized(btn_addNewRow);
			click(btn_addNewRow);
		}

		click(btn_deleteEmotyRowStockAdjustment.replace("tblStockAdj", "tblInternalRequest"));

		click(btn_detailTab);
		selectText(drop_warehouseInternelOrderEmployeeRequest, warehouseInternelOrderEmployeeRequest);
		if (isDisplayed(drop_warehouseInternelOrderEmployeeRequest)) {
			writeTestResults("Verify user can select warehouse", "User should be able to select warehouse",
					"User successfully select warehouse", "pass");
		} else {
			writeTestResults("Verify user can select warehouse", "User should be able to select warehouse",
					"User couldn't select warehouse", "fail");
		}

		writeIWData("Employee", employee, 0);
	}

	public void draftReleseInternelOrder() throws Exception {

		click(btn_draftInternelOrder);
		customizeLoadingDelay(btn_relese, 40);
		trackCode = getText(lbl_creationsDocNo);
		if (isDisplayed(btn_relese)) {

			writeTestResults("Verify user can br draft above document", "User should be able to draft above document",
					"User sucessfully draft above document", "pass");
		} else {
			writeTestResults("Verify user can br draft above document", "User should be able to draft above document",
					"User not draft above document", "Fail");
		}
		sleepCusomized(btn_relese);
		click(btn_relese);
		Thread.sleep(2000);
		explicitWait(btn_goToPageLink, 50);
		click(btn_goToPageLink);
		Thread.sleep(2000);
		pageRefersh();
		explicitWait(btn_releseConfirmation, 50);
		if (isDisplayed(btn_releseConfirmation)) {

			writeTestResults("Verify user can releese Internel Order", "User should be able to releese Internel Order",
					"User sucessfully releese Internel Order", "pass");
		} else {
			writeTestResults("Verify user can releese Internel Order", "User should be able to releese Internel Order",
					"User couldn't releese Internel Order", "fail");
		}

	}

	public void draftReleseInternelDispatchOrder() throws Exception {
		switchWindow();
		customizeLoadingDelay(btn_draftInternelDispatchOrder, 40);
		Thread.sleep(3000);
		click(btn_draftInternelDispatchOrder);
		if (isDisplayed(btn_relese)) {

			writeTestResults("Verify user can br draft Internel Dispath Order",
					"User should be able to draft Internel Dispath Order",
					"User sucessfully draft Internel Dispath Order", "pass");
		} else {
			writeTestResults("Verify user can br draft Internel Dispath Order",
					"User should be able to draft Internel Dispath Order", "User not draft Internel Dispath Order",
					"Fail");
		}
		customizeLoadingDelay(brn_serielBatchCapture, 40);
		click(brn_serielBatchCapture);

		/* Product 01 */
		if (isDisplayed(btn_captureItem1)) {
			writeTestResults(
					"Verify user can navigated Serial/Batch window and products which going to Outbound should be loaded for item 1",
					"User should be navigated Serial/Batch window and products which going to Outbound should be loaded for item 1",
					"User successfully navigated Serial/Batch window and products which going to Outbound should be loaded for item 1",
					"pass");
		} else {
			writeTestResults(
					"Verify user can navigated Serial/Batch window and products which going to Outbound should be loaded for item 1",
					"User should be navigated Serial/Batch window and products which going to Outbound should be loaded for item 1",
					"User couldn't navigated Serial/Batch window and products which going to Outbound doesn't loaded for item 1",
					"fail");
		}

		click(btn_captureItem1);
		Thread.sleep(1000);
		if (isDisplayed(btn_capturePageDocIcon)) {
			writeTestResults("Verify Serial/Batch capturing details are enabled",
					"Serial/Batch capturing details should be enabled",
					"Serial/Batch capturing details successfully enabled", "pass");
		} else {
			writeTestResults("Verify Serial/Batch capturing details are enabled",
					"Serial/Batch capturing details should be enabled",
					"Serial/Batch capturing details doesn't enabled", "fail");
		}

		click(btn_capturePageDocIcon);
		if (isDisplayed(chk_availableProductsTransferOrderOutboundShipment.replace("row", "1"))) {
			writeTestResults("Verify existing Batches are loaded as pop-up",
					"Existing batches should be loaded as pop-up", "Existing batches successfully loaded as pop-up",
					"pass");
		} else {
			writeTestResults("Verify existing batches are loaded as pop-up",
					"Existing batches should be loaded as pop-up", "Existing batches doesn't loaded as pop-up", "fail");
		}

		click(chk_availableProductsTransferOrderOutboundShipment.replace("row", "1"));

		String qty = getText(lbl_qtyBatchSpecifificCapturePage);
		String qty_capture = getText(lbl_availQuantityProductCapturuPage);

		int qty_int = Integer.parseInt(qty);
		int qty_capture_int = Integer.parseInt(qty_capture);

		int i = 2;
		while (qty_int >= qty_capture_int) {
			String i_str = String.valueOf(i);
			click(chk_deleteItemCapturePage);
			click(btn_deleteRowProductCaptururePage);
			click(btn_capturePageDocIcon);
			click(chk_availableProductsTransferOrderOutboundShipment.replace("row", i_str));
			Thread.sleep(2000);
			qty_capture = getText(lbl_availQuantityProductCapturuPage);
			qty_capture_int = Integer.parseInt(qty_capture);
		}

		if (qty_int <= qty_capture_int) {
			writeTestResults("Verify selected batches are added to the grid",
					"Selected batches should be added to the grid", "Selected batches successfully added to the grid",
					"pass");
		} else {
			writeTestResults("Verify selected batches are added to the grid",
					"Selected batches should be added to the grid", "Selected batches doesn't added to the grid",
					"fail");
		}

		click(btn_updateCapture);

		String pro = getAttribute(btn_captureItem1, "class");

		if (pro.equals("fli-number green-vackground-fli-number")
				&& pro.equals("fli-number green-vackground-fli-number")) {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User sucessfully capture 'Seriel/Batch'", "pass");
		} else {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User couldn't capture 'Seriel/Batch'", "fail");
		}

		/* Product 04 */
		String item_xpath = btn_itemProductCapturePage.replace("_product_number", "4");
		sleepCusomized(item_xpath);

		if (isDisplayed(item_xpath)) {
			writeTestResults(
					"Verify user can navigated Serial/Batch window and products which going to Outbound should be loaded for item 4",
					"User should be navigated Serial/Batch window and products which going to Outbound should be loaded for item 4",
					"User successfully navigated Serial/Batch window and products which going to Outbound should be loaded for item 4",
					"pass");
		} else {
			writeTestResults(
					"Verify user can navigated Serial/Batch window and products which going to Outbound should be loaded for item 4",
					"User should be navigated Serial/Batch window and products which going to Outbound should be loaded for item 4",
					"User couldn't navigated Serial/Batch window and products which going to Outbound doesn't loaded for item 4",
					"fail");
		}

		click(item_xpath);
		if (isDisplayed(chk_productCaptureRange)) {
			writeTestResults("Verify Serial/Batch capturing details are enabled",
					"Serial/Batch capturing details should be enabled",
					"Serial/Batch capturing details successfully enabled", "pass");
		} else {
			writeTestResults("Verify Serial/Batch capturing details are enabled",
					"Serial/Batch capturing details should be enabled",
					"Serial/Batch capturing details doesn't enabled", "fail");
		}
		click(chk_productCaptureRange);

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$('#attrSlide-txtSerialCount').val(" + qtyTransferOrder + ")");

		String lot4 = getOngoinfSerirlSpecificSerielNumber(10);
		String final_lot_script = "$('#attrSlide-txtSerialFrom').val(\"" + lot4 + "\")";
		jse.executeScript(final_lot_script);

		pressEnter(txt_seriealProductCapture);

		if (qty_int <= qty_capture_int) {
			writeTestResults("Verify selected batches are added to the grid",
					"Selected batches should be added to the grid", "Selected batches successfully added to the grid",
					"pass");
		} else {
			writeTestResults("Verify selected batches are added to the grid",
					"Selected batches should be added to the grid", "Selected batches doesn't added to the grid",
					"fail");
		}

		click(btn_updateCapture);

		String pro1 = getAttribute(btn_captureItem1, "class");

		if (pro1.equals("fli-number green-vackground-fli-number")) {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User sucessfully capture 'Seriel/Batch'", "pass");
		} else {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User couldn't capture 'Seriel/Batch'", "fail");
		}

		/* Product 05 */
		String item_xpath1 = btn_itemProductCapturePage.replace("_product_number", "5");
		Thread.sleep(4000);
		if (isDisplayed(item_xpath1)) {
			writeTestResults(
					"Verify user can navigated Serial/Batch window and products which going to Outbound should be loaded for item 5",
					"User should be navigated Serial/Batch window and products which going to Outbound should be loaded for item 5",
					"User successfully navigated Serial/Batch window and products which going to Outbound should be loaded for item 5",
					"pass");
		} else {
			writeTestResults(
					"Verify user can navigated Serial/Batch window and products which going to Outbound should be loaded for item 5",
					"User should be navigated Serial/Batch window and products which going to Outbound should be loaded for item 5",
					"User couldn't navigated Serial/Batch window and products which going to Outbound doesn't loaded for item 5",
					"fail");
		}
		click(item_xpath1);

		Thread.sleep(2000);
		if (isDisplayed(chk_productCaptureRange)) {
			writeTestResults("Verify Serial/Batch capturing details are enabled",
					"Serial/Batch capturing details should be enabled",
					"Serial/Batch capturing details successfully enabled", "pass");
		} else {
			writeTestResults("Verify Serial/Batch capturing details are enabled",
					"Serial/Batch capturing details should be enabled",
					"Serial/Batch capturing details doesn't enabled", "fail");
		}
		click(chk_productCaptureRange);

		Thread.sleep(1000);
		jse.executeScript("$('#attrSlide-txtSerialCount').val(" + qtyTransferOrder + ")");

		String final_lot_script1 = "$('#attrSlide-txtSerialFrom').val(\""
				+ getOngoingSerielBatchSpecificSerielNumber(10) + "\")";
		jse.executeScript(final_lot_script1);

		pressEnter(txt_seriealProductCapture);

		Thread.sleep(1000);
		if (qty_int <= qty_capture_int) {
			writeTestResults("Verify selected batches are added to the grid",
					"Selected batches should be added to the grid", "Selected batches successfully added to the grid",
					"pass");
		} else {
			writeTestResults("Verify selected batches are added to the grid",
					"Selected batches should be added to the grid", "Selected batches doesn't added to the grid",
					"fail");
		}

		click(btn_updateCapture);

		String pro2 = getAttribute(btn_captureItem1, "class");
		String pro4 = getAttribute(item_xpath, "class");
		String pro5 = getAttribute(item_xpath1, "class");

		if (pro2.equals("fli-number green-vackground-fli-number")
				&& pro4.equals("fli-number green-vackground-fli-number")
				&& pro5.equals("fli-number green-vackground-fli-number")) {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User sucessfully capture 'Seriel/Batch'", "pass");
		} else {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User couldn't capture 'Seriel/Batch'", "fail");
		}

		sleepCusomized(btn_backButtonUpdateCapture);
		click(btn_backButtonUpdateCapture);
		click(btn_relese);
		customizeLoadingDelay(lbl_relesedConfirmation, 45);
		trackCode = getText(lbl_creationsDocNo);
		String internel_dispatch_order = getText(lbl_docNo);
		writeIWData("Internel Dispatch Order", internel_dispatch_order, 9);
		Thread.sleep(4000);
		if (isDisplayed(btn_releseConfirmation)) {

			writeTestResults("Verify user can be relese Internel Dispath Order",
					"User should be able to relese Internel Dispath Order",
					"User sucessfully relese Internel Dispath Order", "pass");
		} else {
			writeTestResults("Verify user can br relese Internel Dispath Order",
					"User should be able to relese Internel Dispath Order", "User not relese Internel Dispath Order",
					"Fail");
		}

	}

	public void checkWarehouseForProdctAvailabilityInternelOrder() throws Exception {
		String[] product_availability_after = new String[7];
		int row_position = 1;
		for (int i = 0; i < 7; i++) {
			String row_str = String.valueOf(row_position);
			customizeLoadingDelay(btn_availabilityCheckWidgetAfterrelesed.replace("tblStockAdj", "tblDispatchOrder")
					.replace("row", row_str), 30);
			click(btn_availabilityCheckWidgetAfterrelesed.replace("tblStockAdj", "tblDispatchOrder").replace("row",
					row_str));
			Thread.sleep(3000);
			Actions action1 = new Actions(driver);
			WebElement we1 = driver.findElement(By.xpath(btn_avilabilytCheckSecondMenu));
			action1.moveToElement(we1).build().perform();
			click(btn_avilabilytCheckSecondMenu);
			customizeLoadingDelay(td_availableStockGstmWarehouseAfterRelesed, 30);
			product_availability_after[i] = getText(td_availableStockGstmWarehouseAfterRelesed);
			if (row_position == 1) {
				writeIWData("Return Product 01 - IW_TC_011", product_availability_after[i], 17);
				System.out.println("Get balance to Inventory Details Document");
			}
			if (row_position == 2) {
				writeIWData("Return Product 02 - IW_TC_011", product_availability_after[i], 18);
				System.out.println("Get balance to Inventory Details Document");
			}
			System.out.println(product_availability_after[i]);
			click(btn_closeAvailabilityCheckPopup);
			row_position++;
		}

		boolean warehouse_check_flag = false;
		for (int j = 0; j < 7; j++) {
			String qty = product_availability[j].replaceAll(",", "");
			int qty_int = Integer.parseInt(qty);
			String after_qty = product_availability_after[j].replaceAll(",", "");
			try {
				int after_qty_int = Integer.parseInt(after_qty);
				int adding_qty = Integer.parseInt(qtyTransferOrder);
				if (after_qty_int == (qty_int - adding_qty)) {
					warehouse_check_flag = true;
				} else {
					warehouse_check_flag = false;
					break;
				}
			} catch (Exception e) {
				System.out.println(e);
			}

		}

		if (warehouse_check_flag) {
			writeTestResults("Verify stock was transfered from the warehouse",
					"Stock should be transfered from the warehouse", "Stock successfully transfered from the warehouse",
					"pass");
		} else {
			writeTestResults("Verify stock was transfered from the warehouse",
					"Stock should be transfered from the warehouse", "Stock doesn't transfered from the warehouse",
					"fail");
		}

	}

	// IW_TC_011
	public void loadInternelReturnOrderForm() throws Exception {
		navigateInventoryAndWarehouseModule();
		click(btn_internelReturnOrder);
		if (isDisplayed(heder_internelReturnOrder)) {

			writeTestResults("Verify user can navigate to the Internel Return Order Page",
					"User should be able to navigate to the Internel Return Order Page",
					"User sucessfully navigate to the Internel Return Order Page", "pass");
		} else {
			writeTestResults("Verify user can navigate to the Internel Return Order Page",
					"User should be able to navigate to the Internel Return Order Page",
					"User couldn't navigate to the Internel Return Order Page", "fail");
		}

		click(btn_newInternelReturnOrder);
		if (isDisplayed(header_newJourneyPopupIRO)) {

			writeTestResults("Verify user can navigate to the Journey popup",
					"User should be able to navigate to the Journey popup",
					"User sucessfully navigate to Journey popup", "pass");
		} else {
			writeTestResults("Verify user can navigate to the Journey popup",
					"User should be able to navigate to the Journey popup",
					"User couldn't navigate to the Journey popup", "fail");
		}

		click(btn_employeeReturnJourneyIRO);
		if (isDisplayed(header_newIRO)) {
			writeTestResults("Verify user can navigate to the new Internel Return Order form",
					"User should be able to navigate to the new Internel Return Order form",
					"User sucessfully navigate to the new Internel Return Order form", "pass");
		} else {
			writeTestResults("Verify user can navigate to the new Internel Return Order form",
					"User should be able to navigate to the new Internel Return Order form",
					"User couldn't navigate to the new Internel Return Order form", "fail");
		}

	}

	public void fillInternelReturnOrder() throws Exception {
		click(btn_searchIconEmployeeIRO);
		sendKeys(txt_employeeSearchRequesterWindowIRO, readIWData(0));
		pressEnter(txt_employeeSearchRequesterWindowIRO);
		doubleClick(lnk_resultRequesterIRO);
		Thread.sleep(2000);
		String requester = driver.findElement(By.xpath(txt_RequesterIRO)).getAttribute("value");
		String requester_iw = readIWData(0).replaceAll("]", "").substring(8, 28);
		if (requester.contains(requester_iw)) {
			writeTestResults("Verify user can select requester", "User should be able to select requester",
					"User sucessfully select requester", "pass");
		} else {
			writeTestResults("Verify user can select requester", "User should be able to select requester",
					"User couldn't select requester", "fail");
		}

		Select comboBoxWare = new Select(driver.findElement(By.xpath(dropdown_toWarehouseIRO)));
		selectText(dropdown_toWarehouseIRO, warehouseGstWarehouseNegombo);
		String selectedComboValueWare = comboBoxWare.getFirstSelectedOption().getText();
		selectText(dropdown_toWarehouseIRO, warehouseGstWarehouseNegombo);

		if (selectedComboValueWare.equals(warehouseGstWarehouseNegombo)) {
			writeTestResults("Verify user can select To Warehouse", "User should be able to select To Warehouse",
					"User sucessfully select To Warehouse", "pass");
		} else {
			writeTestResults("Verify user can select To Warehouse", "User should be able to select To Warehouse",
					"User couldn't select To Warehouse", "fail");
		}

	}

	public void selectDocumentsThatNeedToReturnIRO() throws Exception {
		click(btn_docIconSelectInternelOrderIRO);

		if (isDisplayed(header_documentListIRO)) {
			writeTestResults("Verify user can navigate to Document List",
					"User should be able to navigate Document List", "User sucessfully navigate to Document List",
					"pass");
		} else {
			writeTestResults("Verify user can navigate to Document List",
					"User should be able to navigate Document List", "User couldn't navigate to Document List", "fail");
		}

		sendKeys(txt_internelOrderSearchIRO, readIWData(9));
		click(btn_serchDocInternelReturnOrder);
		customizeLoadingDelay(td_serchedDocNoIRO, 10);
		String relevent_io = readIWData(9);
		Thread.sleep(3000);
		String serched_io = getText(td_serchedDocNoIRO);
		if (relevent_io.equals(serched_io)) {
			writeTestResults("Verify dispatched products  list relevant to the Internal Order were loaded",
					"Dispatched products  list relevant to the Internal Order should be loaded",
					"Dispatched products  list relevant to the Internal Order successfully loaded", "pass");
		} else {
			writeTestResults("Verify dispatched products  list relevant to the Internal Order were loaded",
					"Dispatched products  list relevant to the Internal Order should be loaded",
					"Dispatched products  list relevant to the Internal Order were not loaded", "fail");
		}

		click(lnk_resultInternelOrderIRO1);
		click(lnk_resultInternelOrderIRO2);

		if (isDisplayed(lnk_resultInternelOrderIRO1) && isDisplayed(lnk_resultInternelOrderIRO2)) {
			writeTestResults("Verify user can select products", "User should be able to select products",
					"User successfully select products", "pass");
		} else {
			writeTestResults("Verify user can select products", "User should be able to select products",
					"User couldn't select products", "fail");
		}

		String pop_pro1 = getText(lbl_product1DocList).replaceAll(" ", "");
		String pop_pro2 = getText(lbl_product2DocList).replaceAll(" ", "");

		click(btn_applyInternelOrderIRO);

		String pro1 = getText(lbl_product1IRO).replaceAll(" ", "");
		String pro2 = getText(lbl_product2IRO).replaceAll(" ", "");

		System.out.println(pop_pro1);
		System.out.println(pro1);
		System.out.println(pop_pro2);
		System.out.println(pro2);

		if (pop_pro1.equals(pro1) && pop_pro2.equals(pro2)) {
			writeTestResults("Verify selected proudcts were added to the grid with the relevant details",
					"Selected proudcts should be added to the grid with the relevant details",
					"Selected proudcts successfully added to the grid with the relevant details", "pass");
		} else {
			writeTestResults("Verify selected proudcts were added to the grid with the relevant details",
					"Selected proudcts should be added to the grid with the relevant details",
					"Selected proudcts doesn't added to the grid with the relevant details", "fail");
		}

		click(btn_draft);
		Thread.sleep(4000);
		if (!getText(lbl_docStatus).equals("(Draft)")) {
			Thread.sleep(4000);
		}
		if (getText(lbl_docStatus).equals("(Draft)")) {
			writeTestResults("Verify user can draft Internel Return Order",
					"User should be able to draft Internel Return Order",
					"User successfully draft Internel Return Order", "pass");
		} else {
			writeTestResults("Verify user can draft Internel Return Order",
					"User should be able to draft Internel Return Order", "User couldn't draft Internel Return Order",
					"fail");
		}

		click(btn_relese);
		if (isDisplayed(btn_goToPageLink)) {
			writeTestResults("Verify user can release Internel Return Order",
					"User should be able to release Internel Return Order",
					"User successfully release Internel Return Order", "pass");
		} else {
			writeTestResults("Verify user can release Internel Return Order",
					"User should be able to release Internel Return Order",
					"User couldn't release Internel Return Order", "fail");
		}

		click(btn_goToPageLink);
		switchWindow();
		sleepCusomized(header_pageLabel);
		if (getText(header_pageLabel).equals("Internal Receipt")) {
			writeTestResults("Verify user can navigated to Internal Receipt Form with the details of product, Employee",
					"User should be navigated to Internal Receipt Form with the details of product, Employee",
					"User successfully navigated to Internal Receipt Form with the details of product, Employee",
					"pass");
		} else {
			writeTestResults("Verify user can navigated to Internal Receipt Form with the details of product, Employee",
					"User should be able to navigated to Internal Receipt Form with the details of product, Employee",
					"User couldn't navigated to Internal Receipt Form with the details of product, Employee", "fail");
		}

		click(btn_draft);
		sleepCusomized(btn_relese);
		if (getText(lbl_docStatus).equals("(Draft)")) {
			writeTestResults("Verify user can draft Internal Receipt ",
					"User should be able to draft Internal Receipt ", "User successfully draft Internal Receipt ",
					"pass");
		} else {
			writeTestResults("Verify user can draft Internal Receipt ",
					"User should be able to draft Internal Receipt ", "User couldn't draft Internal Receipt ", "fail");
		}
		sleepCusomized(brn_serielBatchCapture);
		click(brn_serielBatchCapture);
		sleepCusomized(btn_captureItem1IRO);
		click(btn_captureItem1IRO);
		click(chk_selectAllCaptureItem);
		click(btn_updateCapture);
		Thread.sleep(2000);
		click(btn_captureItem2IRO);
		Thread.sleep(2000);
		click(btn_updateCapture);

		click(btn_productCaptureBackButton);
		click(btn_relese);
		sleepCusomized(btn_print);
		Thread.sleep(1000);
		String status2 = getText(lbl_docStatus);
		if (!status2.equalsIgnoreCase("(Released)")) {
			Thread.sleep(7000);
			status2 = getText(lbl_docStatus);
		}
		if (getText(lbl_docStatus).equals("(Released)")) {
			writeTestResults("Verify user can release Internal Receipt ",
					"User should be able to release Internal Receipt ", "User successfully release Internal Receipt ",
					"pass");
		} else {
			writeTestResults("Verify user can release Internal Receipt ",
					"User should be able to release Internal Receipt ", "User couldn't release Internal Receipt ",
					"fail");
		}

	}

	public void checkWarehouseForProdctAvailabilityInternelReteturnOrder() throws Exception {
		String[] product_availability_after = new String[7];
		int row_position = 1;
		for (int i = 0; i < 2; i++) {
			String row_str = String.valueOf(row_position);
			sleepCusomized(btn_availabilityCheckWidgetAfterrelesed.replace("tblStockAdj", "tblShipDetails")
					.replace("row", row_str));
			click(btn_availabilityCheckWidgetAfterrelesed.replace("tblStockAdj", "tblShipDetails").replace("row",
					row_str));
			Thread.sleep(2000);
			Actions action1 = new Actions(driver);
			WebElement we1 = driver.findElement(By.xpath(btn_avilabilytCheckSecondMenu));
			action1.moveToElement(we1).build().perform();
			click(btn_avilabilytCheckSecondMenu);
			sleepCusomized(td_availableStockGstmWarehouseAfterRelesed);
			product_availability_after[i] = getText(td_availableStockGstmWarehouseAfterRelesed);
			System.out.println(product_availability_after[i]);
			click(btn_closeAvailabilityCheckPopup);
			row_position++;
		}

		boolean warehouse_check_flag = false;
		for (int j = 0; j < 2; j++) {
			String qty;
			if (j == 0) {
				qty = readIWData(17).replaceAll(",", "");
			} else {
				qty = readIWData(18).replaceAll(",", "");
			}

			int qty_int = Integer.parseInt(qty);
			String after_qty = product_availability_after[j].replaceAll(",", "");
			try {
				int after_qty_int = Integer.parseInt(after_qty);
				int adding_qty = Integer.parseInt(qtyInterDepartmentTransferOrder);
				if (after_qty_int == (qty_int + adding_qty)) {
					warehouse_check_flag = true;
				} else {
					warehouse_check_flag = false;
					break;
				}
			} catch (Exception e) {
				System.out.println(e);
			}

		}

		if (warehouse_check_flag) {
			writeTestResults("Verify stock was transfered to the destination",
					"Stock should be transfered to the destination", "Stock successfully transfered to the destination",
					"pass");
		} else {
			writeTestResults("Verify stock was transfered to the destination",
					"Stock should be transfered to the destination", "Stock doesn't transfered to the destination",
					"fail");
		}

		switchWindow();
		closeWindow();
	}

	// IW_TC_012
	public void navigateToInboundLoanOrderOutboundShipment() throws Exception {
		navigateInventoryAndWarehouseModule();
		click(btn_inboundLoanOrder);
		if (isDisplayed(header_ILOPage)) {

			writeTestResults("Verify user can be relese Internel Dispath Order",
					"User should be able to relese Internel Dispath Order",
					"User sucessfully relese Internel Dispath Order", "pass");
		} else {
			writeTestResults("Verify user can br relese Internel Dispath Order",
					"User should be able to relese Internel Dispath Order", "User not relese Internel Dispath Order",
					"Fail");
		}
	}

	public void navigateNewInboundLoanOrderForm() throws Exception {
		click(btn_newInboundLoanOrder);
	}

	public void fillInboundLOanOrderForm() throws Exception {
		explicitWait(btn_vendorSearchILO, 50);
		click(btn_vendorSearchILO);
		explicitWait(txt_vendorSearchILO, 10);
		Thread.sleep(1500);
		pressEnter(txt_vendorSearchILO);

		Thread.sleep(3000);
		doubleClick(lnk_resultVendorILO);

		click(btn_addressILO);

		sendKeys(txt_billingAddressILO, billingAddressILO);

		sendKeys(txt_deliveryAddressILO, deliveryAddressILO);

		click(btn_applyAddressILO);

		/* Product add to the grid */
		int j = 0;
		for (int i = 1; i < 8; i++) {
			String str_i = String.valueOf(i);

			String lookup_path = btn_productLookupInterDeprtmentTransferOrderForLoop.replace("row", str_i)
					.replace("column", "8").replace("tblStockAdj", "tblLoanOrder");
			click(lookup_path);
			if (isDisplayed(txt_productSearch)) {

				writeTestResults("Verify Product Lookup is pop-up", "Product Lookup should be pop-up",
						"Product Lookup successfully pop-up", "pass");
			} else {
				writeTestResults("Verify Product Lookup is pop-up", "Product Lookup should be pop-up",
						"Product Lookup doesn't pop-up", "fail");
			}

			String product = "";

			if (i == 1) {
				product = "BatchSpecific";
			} else if (i == 2) {
				product = "BatchFifo";
			} else if (i == 3) {
				product = "Lot";
			} else if (i == 4) {
				product = "SerielSpecific";
			} else if (i == 5) {
				product = "SerielBatchSpecific";
			} else if (i == 6) {
				product = "SerielBatchFifo";
			} else if (i == 7) {
				product = "SerirlFifo";
			} else {
				System.out.println("Product type cannt identified.........");
			}
			sleepCusomized(txt_productSearch);
			sendKeys(txt_productSearch, readTestCreation(product));
			pressEnter(txt_productSearch);
			Thread.sleep(3000);

			sleepCusomized(btn_resultProduct);
			if (isDisplayed(btn_resultProduct)) {
				writeTestResults("Verify searched products was loaded", "Searched products should be loaded",
						"Searched products successfully loaded", "pass");
			} else {
				writeTestResults("Verify searched products was loaded", "Searched products should be loaded",
						"Searched products doesn't loaded", "fail");
			}
			sleepCusomized(btn_resultProduct);
			doubleClick(btn_resultProduct);

			String txt_qty = txt_qtyProductGridInboundLoanOrder.replace("row", str_i);

			if (isDisplayed(btn_avilabilityCheckWidget)) {
				writeTestResults("Verify selected products should be added to the grid",
						"Selected products should be added to the grid",
						"Selected products successfully added to the grid", "pass");
			} else {
				writeTestResults("Verify selected products should be added to the grid",
						"Selected products should be added to the grid", "Selected products doesn't added to the grid",
						"fail");
			}

			sendKeys(txt_qty, qtyInterDepartmentTransferOrder);
			if (isDisplayed(txt_qty)) {
				writeTestResults("Verify user can set quantity", "User should be able set quantity",
						"User successfully set quantity ", "pass");
			} else {
				writeTestResults("Verify user can set quantity", "User should be able set quantity",
						"User couldn't set quantity ", "fail");
			}

			explicitWaitUntillClickable(btn_avilabilityCheckWidget, 20);
			Thread.sleep(500);
			click(btn_avilabilityCheckWidget);
			if (j < 7) {
				Thread.sleep(2000);
				Actions action13 = new Actions(driver);
				WebElement we13 = driver.findElement(By.xpath(btn_avilabilytCheckSecondMenu));
				action13.moveToElement(we13).build().perform();
				sleepCusomized(btn_avilabilytCheckSecondMenu);
				click(btn_avilabilytCheckSecondMenu);
				try {
					product_availability[j] = driver.findElement(By.xpath(td_availableStockWarehouseGST_Warehouse))
							.getText().trim();
				} catch (Exception e) {
					System.out.println("0");
				}
				if (product_availability[j] != null) {
					System.out.println(product_availability[j]);
				} else {
					product_availability[j] = ",0";
				}
				Thread.sleep(1000);
				click(txt_closePopupProductAvailabilityInterDepartmentTransferOrder);
			}
			j++;
			sleepCusomized(btn_addNewRow);
			click(btn_addNewRow);
		}

		click(btn_deleteEmotyRowStockAdjustment.replace("tblStockAdj", "tblLoanOrder"));

		click(btn_tabInboundLoanOrder);
		selectText(drop_warehouseInternelOrderEmployeeRequest, warehouseInternelOrderEmployeeRequest);
		if (isDisplayed(drop_warehouseInternelOrderEmployeeRequest)) {
			writeTestResults("Verify user can select warehouse", "User should be able to select warehouse",
					"User successfully select warehouse", "pass");
		} else {
			writeTestResults("Verify user can select warehouse", "User should be able to select warehouse",
					"User couldn't select warehouse", "fail");
		}
	}

	public void inboundLOanOrderDraftAndReleese() throws Exception {
		switchWindow();

		click(btn_draft);
		Thread.sleep(3000);
		if (isDisplayed(btn_relese)) {

			writeTestResults("Verify user can br draft Inbound Loan Order document",
					"User should be able to draft Inbound Loan Order", "User sucessfully draft Inbound Loan Order",
					"pass");
		} else {
			writeTestResults("Verify user can br draft Inbound Loan Order",
					"User should be able to draft Inbound Loan Order", "User does'nt draft Inbound Loan Order", "Fail");
		}

		sleepCusomized(btn_relese);
		click(btn_relese);
		explicitWait(btn_goToPageLink, 30);
		click(btn_goToPageLink);
		pageRefersh();
		explicitWait(btn_releseConfirmation, 40);
		if (isDisplayed(btn_releseConfirmation)) {

			writeTestResults("Verify user can be relese Inbound Loan Order",
					"User should be able to relese Inbound Loan Order", "User sucessfully relese Inbound Loan Order",
					"pass");
		} else {
			writeTestResults("Verify user can br relese Inbound Loan Order",
					"User should be able to relese Inbound Loan Order", "User not relese Inbound Loan Order", "Fail");
		}

		switchWindow();
	}

	public void inbpondShipmentInboundLOanOrderCheckoutDraftAndReleese() throws Exception {
		explicitWait(btn_checkoutILOOutboundShipment, 30);
		click(btn_checkoutILOOutboundShipment);
		Thread.sleep(1000);
		click(btn_draft);
		Thread.sleep(5000);
		if (isDisplayed(btn_editOutboundShipmentILO)) {
			writeTestResults("Verify user can be draft Inbound Shipment",
					"User should be able to draft Inbound Shipment", "User sucessfully draft Inbound Shipment", "pass");
		} else {
			writeTestResults("Verify user can be draft Inbound Shipment",
					"User should be able to draft Inbound Shipment", "User does'nt draft Inbound Shipment", "fail");
		}

		sleepCusomized(brn_serielBatchCapture);
		click(brn_serielBatchCapture);
		if (isDisplayed(btn_serielBatchCaptureProduct1ILOOutboundShipment)) {

			writeTestResults("Verify user navigate to the product capture page",
					"User should be able navigate to the product capture page",
					"User sucessfully navigate to the product capture page", "pass");
		} else {
			writeTestResults("Verify user navigate to the product capture page",
					"User should be able navigate to the product capture page",
					"User does'nt navigate to the product capture page", "fail");
		}
		/* Product 01 */
		sleepCusomized(btn_serielBatchCaptureProduct1ILOOutboundShipment);
		String item_xpath;
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "1");
		click(item_xpath);
		if (isDisplayed(item_xpath)) {
			writeTestResults("Verify product 1 serial/batch capturing details are loaded",
					"Product 1 serial/batch capturing details should be loaded",
					"Product 1 serial/batch capturing details are sucessfully loaded", "pass");
		} else {
			writeTestResults("Verify product 1 serial/batch capturing details are loaded",
					"Product 1 serial/batch capturing details should be loaded",
					"Product 1 serial/batch capturing details doesn't loaded", "fail");
		}
		sendKeys(txt_batchNumberCaptureILOOutboundShipment, genBatchSpecificSerielNumberIW_TC_012(10));
		sendKeys(txt_lotNumberStockAdjustment,
				genProductId() + currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_expiryDateStockAdjustmentProductCapture);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_lotNumberStockAdjustment);
		Thread.sleep(3000);
		click(btn_updateCapture);
		String pro4 = getAttribute(item_xpath, "class");

		if (pro4.equals("fli-number green-vackground-fli-number")) {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User sucessfully capture 'Seriel/Batch'", "pass");
		} else {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User couldn't capture 'Seriel/Batch'", "fail");
		}

		/* Product 02 */
		Thread.sleep(3000);
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "2");
		click(item_xpath);
		if (isDisplayed(item_xpath)) {
			writeTestResults("Verify product 2 serial/batch capturing details are loaded",
					"Product 2 serial/batch capturing details should be loaded",
					"Product 2 serial/batch capturing details are sucessfully loaded", "pass");
		} else {
			writeTestResults("Verify product 2 serial/batch capturing details are loaded",
					"Product 2 serial/batch capturing details should be loaded",
					"Product 2 serial/batch capturing details doesn't loaded", "fail");
		}

		sendKeys(txt_batchNumberCaptureILOOutboundShipment, genBatchFifoSerielNumberIW_TC_012(10));
		sendKeys(txt_lotNumberStockAdjustment,
				genProductId() + currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_expiryDateStockAdjustmentProductCapture);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_lotNumberStockAdjustment);
		Thread.sleep(3000);
		click(btn_updateCapture);
		pro4 = getAttribute(item_xpath, "class");

		if (pro4.equals("fli-number green-vackground-fli-number")) {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User sucessfully capture 'Seriel/Batch'", "pass");
		} else {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User couldn't capture 'Seriel/Batch'", "fail");
		}

		/* Product 04 */
		Thread.sleep(3000);
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "4");
		click(item_xpath);
		if (isDisplayed(item_xpath)) {
			writeTestResults("Verify product 4 serial/batch capturing details are loaded",
					"Product 4 serial/batch capturing details should be loaded",
					"Product 4 serial/batch capturing details are sucessfully loaded", "pass");
		} else {
			writeTestResults("Verify product 4 serial/batch capturing details are loaded",
					"Product 4 serial/batch capturing details should be loaded",
					"Product 4 serial/batch capturing details doesn't loaded", "fail");
		}

		click(chk_productCaptureRange);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$('#attrSlide-txtSerialCount').val(10)");

		String lot4 = genSerirlSpecificSerielNumberIW_TC_012(10);
		String final_lot_script = "$('#attrSlide-txtSerialFrom').val(\"" + lot4 + "\")";
		jse.executeScript(final_lot_script);
		Thread.sleep(1000);
		sendKeys(txt_searielLotNo, genProductId() + currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_dateSerielProductProductCapturePage);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_dateSerielProductProductCapturePage);
		Thread.sleep(3000);
		click(btn_updateCapture);
		pro4 = getAttribute(item_xpath, "class");

		if (pro4.equals("fli-number green-vackground-fli-number")) {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User sucessfully capture 'Seriel/Batch'", "pass");
		} else {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User couldn't capture 'Seriel/Batch'", "fail");
		}

		/* Product 05 */
		Thread.sleep(3000);
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "5");
		click(item_xpath);
		if (isDisplayed(item_xpath)) {
			writeTestResults("Verify product 5 serial/batch capturing details are loaded",
					"Product 5 serial/batch capturing details should be loaded",
					"Product 5 serial/batch capturing details are sucessfully loaded", "pass");
		} else {
			writeTestResults("Verify product 5 serial/batch capturing details are loaded",
					"Product 5 serial/batch capturing details should be loaded",
					"Product 5 serial/batch capturing details doesn't loaded", "fail");
		}

		click(chk_productCaptureRange);
		jse.executeScript("$('#attrSlide-txtSerialCount').val(10)");

		String lot41 = genSerielBatchSpecificSerielNumberIW_TC_012(10);
		String final_lot_script1 = "$('#attrSlide-txtSerialFrom').val(\"" + lot41 + "\")";
		jse.executeScript(final_lot_script1);

		sendKeys(txt_searielLotNo, genProductId() + currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));

		sendKeys(txt_serielBatchProductCaptureStockAdjustment,
				genProductId() + currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));

		click(txt_dateSerielProductProductCapturePage);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_dateSerielProductProductCapturePage);
		Thread.sleep(3000);
		click(btn_updateCapture);
		pro4 = getAttribute(item_xpath, "class");

		if (pro4.equals("fli-number green-vackground-fli-number")) {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User sucessfully capture 'Seriel/Batch'", "pass");
		} else {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User couldn't capture 'Seriel/Batch'", "fail");
		}

		/* Product 06 */
		Thread.sleep(2000);
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "6");
		Actions action = new Actions(driver);
		WebElement we = driver.findElement(By.xpath(item_xpath));
		action.moveToElement(we).build().perform();

		click(item_xpath);
		sleepCusomized(item_xpath);
		if (isDisplayed(item_xpath)) {
			writeTestResults("Verify product 6 serial/batch capturing details are loaded",
					"Product 6 serial/batch capturing details should be loaded",
					"Product 6 serial/batch capturing details are sucessfully loaded", "pass");
		} else {
			writeTestResults("Verify product 6 serial/batch capturing details are loaded",
					"Product 6 serial/batch capturing details should be loaded",
					"Product 6 serial/batch capturing details doesn't loaded", "fail");
		}

		click(chk_productCaptureRange);
		jse.executeScript("$('#attrSlide-txtSerialCount').val(10)");

		String lot411 = genSerielBatchFifoNumberIW_TC_012(10);
		String final1_lot_script1 = "$('#attrSlide-txtSerialFrom').val(\"" + lot411 + "\")";
		jse.executeScript(final1_lot_script1);

		sendKeys(txt_searielLotNo, genProductId() + currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));

		sendKeys(txt_serielBatchProductCaptureStockAdjustment,
				genProductId() + currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));

		click(txt_dateSerielProductProductCapturePage);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_dateSerielProductProductCapturePage);
		Thread.sleep(3000);
		click(btn_updateCapture);
		pro4 = getAttribute(item_xpath, "class");

		if (pro4.equals("fli-number green-vackground-fli-number")) {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User sucessfully capture 'Seriel/Batch'", "pass");
		} else {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User couldn't capture 'Seriel/Batch'", "fail");
		}

		/* Product 07 */
		Thread.sleep(2000);
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "7");
		Actions action1 = new Actions(driver);
		WebElement we1 = driver.findElement(By.xpath(item_xpath));
		action.moveToElement(we1).build().perform();

		click(item_xpath);
		sleepCusomized(item_xpath);
		if (isDisplayed(item_xpath)) {
			writeTestResults("Verify product 7 serial/batch capturing details are loaded",
					"Product 7 serial/batch capturing details should be loaded",
					"Product 7 serial/batch capturing details are sucessfully loaded", "pass");
		} else {
			writeTestResults("Verify product 7 serial/batch capturing details are loaded",
					"Product 7 serial/batch capturing details should be loaded",
					"Product 7 serial/batch capturing details doesn't loaded", "fail");
		}

		click(chk_productCaptureRange);
		jse.executeScript("$('#attrSlide-txtSerialCount').val(10)");

		String lot4111 = genSerielBatchFifoNumberIW_TC_012(10);
		String final1_lot_script11 = "$('#attrSlide-txtSerialFrom').val(\"" + lot4111 + "\")";
		jse.executeScript(final1_lot_script11);

		sendKeys(txt_searielLotNo, genProductId() + currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));

		click(txt_dateSerielProductProductCapturePage);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_dateSerielProductProductCapturePage);
		Thread.sleep(3000);
		click(btn_updateCapture);
		pro4 = getAttribute(item_xpath, "class");

		if (pro4.equals("fli-number green-vackground-fli-number")) {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User sucessfully capture 'Seriel/Batch'", "pass");
		} else {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User couldn't capture 'Seriel/Batch'", "fail");
		}

		Thread.sleep(3000);
		click(btn_productCaptureBackButton);

		sleepCusomized(btn_releese2);
		click(btn_releese2);
		explicitWait(header_releasedInboundShipment, 60);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedInboundShipment)) {
			trackCode = getText(lbl_docNo);
			writeTestResults("Verify user can be relese Inbound Shipment",
					"User should be able to relese Inbound Shipment", "User sucessfully relese Inbound Shipment",
					"pass");
		} else {
			writeTestResults("Verify user can br relese Inbound Shipment",
					"User should be able to relese Inbound Shipment", "User not relese Inbound Shipment", "fail");
		}
	}

	public void checkWarehouseForProdctAvailabilityInboundLoanOrder() throws Exception {
		String[] product_availability_after = new String[7];
		int row_position = 1;
		for (int i = 0; i < 7; i++) {
			String row_str = String.valueOf(row_position);
			Thread.sleep(2000);
			String btn_widget_pro = btn_productAvilabilityWidgetShipmentDetailsTable.replace("row", row_str);
			sleepCusomized(btn_widget_pro);
			click(btn_widget_pro);
			Thread.sleep(2000);
			Actions action1 = new Actions(driver);
			WebElement we1 = driver.findElement(By.xpath(btn_avilabilytCheckSecondMenu));
			action1.moveToElement(we1).build().perform();
			click(btn_avilabilytCheckSecondMenu);
			sleepCusomized(td_availableStockGstmWarehouseAfterRelesed);
			product_availability_after[i] = getText(td_availableStockGstmWarehouseAfterRelesed);
			System.out.println(product_availability_after[i]);
			click(btn_closeAvailabilityCheckPopup);
			row_position++;
		}

		boolean warehouse_check_flag = false;
		for (int j = 0; j < 7; j++) {
			String qty = product_availability[j].replaceAll(",", "");
			int qty_int = Integer.parseInt(qty);
			String after_qty = product_availability_after[j].replaceAll(",", "");
			try {
				int after_qty_int = Integer.parseInt(after_qty);
				int adding_qty = Integer.parseInt(qtyInterDepartmentTransferOrder);
				if (after_qty_int == (qty_int + adding_qty)) {
					warehouse_check_flag = true;
				} else {
					warehouse_check_flag = false;
					break;
				}
			} catch (Exception e) {
				System.out.println(e);
			}

		}

		if (warehouse_check_flag) {
			writeTestResults("Verify stock was transfered to the destination",
					"Stock should be transfered to the destination", "Stock successfully transfered to the destination",
					"pass");
		} else {
			writeTestResults("Verify stock was transfered to the destination",
					"Stock should be transfered to the destination", "Stock doesn't transfered to the destination",
					"fail");
		}
		closeWindow();
		switchWindow();
	}

	// IW_TC_013
	public void navigateToOutboundLoanOrder() throws Exception {
		navigateInventoryAndWarehouseModule();
		click(btn_outboundLoanOrder);

		if (isDisplayed(btn_newOutboundLoanOrder)) {

			writeTestResults("Verify user can navigate to Outbound Loan Order page",
					"User should be navigate to Outbound Loan Order page",
					"User sucessfully navigate to Outbound Loan Order page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Outbound Loan Order page",
					"User should be navigate to Outbound Loan Order page",
					"User does'nt navigate to Outbound Loan Order page", "fail");
		}

	}

	public void OutboundLoanOrderFormAvailability() throws Exception {
		click(btn_newOutboundLoanOrder);
		if (isDisplayed(btn_customerAccountSearchLookupILO)) {

			writeTestResults("Verify user can navigate to Outbound Loan Order form",
					"User should be navigate to Outbound Loan Order form",
					"User sucessfully navigate to Outbound Loan Order form", "pass");
		} else {
			writeTestResults("Verify user can navigate to Outbound Loan Order form",
					"User should be navigate to Outbound Loan Order form",
					"User does'nt navigate to Outbound Loan Order form", "fail");
		}
	}

	public void OutboundLoanOrderFormFilling() throws Exception {

		click(btn_customerAccountSearchLookupILO);

		sendKeys(txt_accountSearchILO, customerAccountILO2);

		pressEnter(txt_accountSearchILO);
		Thread.sleep(3000);
		doubleClick(lnk_resultCustomerAccount);

		click(btn_billingAddressILO);
		sendKeys(txt_billingAddressILO, address1ILO);
		sendKeys(txt_shippingAddressILO, address2ILO);
		click(btn_applyAddressILO);
		selectIndex(dropdown_salesUnitILO, 3);

		/* Product add to the grid */
		int j = 0;
		for (int i = 1; i < 8; i++) {
			String str_i = String.valueOf(i);

			String lookup_path = btn_productLookupInterDeprtmentTransferOrderForLoop.replace("row", str_i)
					.replace("column", "8").replace("tblStockAdj", "tblLoanOrder");
			click(lookup_path);
			if (isDisplayed(txt_productSearch)) {

				writeTestResults("Verify Product Lookup is pop-up", "Product Lookup should be pop-up",
						"Product Lookup successfully pop-up", "pass");
			} else {
				writeTestResults("Verify Product Lookup is pop-up", "Product Lookup should be pop-up",
						"Product Lookup doesn't pop-up", "fail");
			}

			String product = "";

			if (i == 1) {
				product = "BatchSpecific";
			} else if (i == 2) {
				product = "BatchFifo";
			} else if (i == 3) {
				product = "Lot";
			} else if (i == 4) {
				product = "SerielSpecific";
			} else if (i == 5) {
				product = "SerielBatchSpecific";
			} else if (i == 6) {
				product = "SerielBatchFifo";
			} else if (i == 7) {
				product = "SerirlFifo";
			} else {
				System.out.println("Product type cannt identified.........");
			}
			sleepCusomized(txt_productSearch);
			sendKeys(txt_productSearch, readTestCreation(product));
			pressEnter(txt_productSearch);
			Thread.sleep(3000);

			sleepCusomized(btn_resultProduct);
			if (isDisplayed(btn_resultProduct)) {
				writeTestResults("Verify searched products was loaded", "Searched products should be loaded",
						"Searched products successfully loaded", "pass");
			} else {
				writeTestResults("Verify searched products was loaded", "Searched products should be loaded",
						"Searched products doesn't loaded", "fail");
			}
			explicitWait(result_cpmmonProduct.replace("product", readTestCreation(product)), 50);
			doubleClick(result_cpmmonProduct.replace("product", readTestCreation(product)));

//			Selected products should be added to the Grid and OnHand Quantity [From][To], Default UOM should be loaded for selected product

			String txt_qty = txt_qtyProductGridInboundLoanOrder.replace("row", str_i);

			if (isDisplayed(btn_avilabilityCheckWidget)) {
				writeTestResults("Verify selected products should be added to the grid",
						"Selected products should be added to the grid",
						"Selected products successfully added to the grid", "pass");
			} else {
				writeTestResults("Verify selected products should be added to the grid",
						"Selected products should be added to the grid", "Selected products doesn't added to the grid",
						"fail");
			}

			sendKeys(txt_qty, qtyInterDepartmentTransferOrder);
			if (isDisplayed(txt_qty)) {
				writeTestResults("Verify user can set quantity", "User should be able set quantity",
						"User successfully set quantity ", "pass");
			} else {
				writeTestResults("Verify user can set quantity", "User should be able set quantity",
						"User couldn't set quantity ", "fail");
			}

			click(btn_avilabilityCheckWidget);
			if (j < 7) {
				sleepCusomized(btn_avilabilytCheckSecondMenu);
				Thread.sleep(2000);
				Actions action1 = new Actions(driver);
				WebElement we1 = driver.findElement(By.xpath(btn_avilabilytCheckSecondMenu));
				action1.moveToElement(we1).build().perform();
				click(btn_avilabilytCheckSecondMenu);
				try {
					product_availability[j] = driver.findElement(By.xpath(td_availableStockWarehouseGST_Warehouse))
							.getText().trim();
				} catch (Exception e) {
					System.out.println("0");
				}
				if (product_availability[j] != null) {
					System.out.println(product_availability[j]);
				} else {
					product_availability[j] = ",0";
				}
				sleepCusomized(txt_closePopupProductAvailabilityInterDepartmentTransferOrder);
				click(txt_closePopupProductAvailabilityInterDepartmentTransferOrder);
			}
			j++;
			sleepCusomized(btn_addNewRow);
			click(btn_addNewRow);
		}

		click(btn_deleteEmotyRowStockAdjustment.replace("tblStockAdj", "tblLoanOrder"));

		click(btn_tabInboundLoanOrder);
		selectText(drop_warehouseInternelOrderEmployeeRequest, warehouseInternelOrderEmployeeRequest);
		if (isDisplayed(drop_warehouseInternelOrderEmployeeRequest)) {
			writeTestResults("Verify user can select warehouse", "User should be able to select warehouse",
					"User successfully select warehouse", "pass");
		} else {
			writeTestResults("Verify user can select warehouse", "User should be able to select warehouse",
					"User couldn't select warehouse", "fail");
		}

		click(btn_detailsTabILO);

		click(chkbox_underDeliveryILO);
		click(chkbox_partielDeliveryILO);

		click(btn_reqSlipDateILO);
		click(btn_reqSlipDate17ILO);

		click(btn_conSlipDateILO);
		click(btn_conSlipDate18ILO);

		click(btn_reqReciptDateILO);
		click(btn_reqReciptDate19ILO);

		click(btn_conReciptDateILO);
		click(btn_conReciptDate20ILO);

		draft("Outbound Loan Order");
		Thread.sleep(5000);
		writeIWData("Outbound Loan Order", getText(lbl_relesedInboundShipmentNoBarcodeGeneratorPage), 8);
	}

	public void releseOutboundLoanOrder() throws Exception {
		Thread.sleep(4000);
		click(btn_releseILO);

		/*
		 * if (isDisplayed(btn_popoupOkLastStep)) { switchWindow();
		 * click(btn_popoupOkLastStep); }
		 */

		if (isDisplayed(btn_goToNextTaskILO)) {

			writeTestResults("Verify user can relese Outbound Loan Order",
					"User should be able to relese Outbound Loan Order", "User sucessfully relese Outbound Loan Order",
					"pass");
		} else {
			writeTestResults("Verify user can relese Outbound Loan Order",
					"User should be able to relese Outbound Loan Order",
					"User does'nt able to releese Outbound Loan Order", "fail");
		}

	}

	public void draftOutboundShipmentILO() throws Exception {
		Thread.sleep(4000);
		click(btn_goToNextTaskILO);
		Thread.sleep(2000);
		switchWindow();
		customizeLoadingDelay(btn_draft, 40);
		click(btn_draft);
		if (isDisplayed(btn_relese)) {

			writeTestResults("Verify user can be draft Outbound Shipment",
					"User should be able to draft Outbound Shipment", "User sucessfully draft Outbound Shipment",
					"pass");
		} else {
			writeTestResults("Verify user can be draft Outbound Shipment",
					"User should be able to draft Outbound Shipment", "User not draft Outbound Shipment", "Fail");
		}

		sleepCusomized(brn_serielBatchCapture);
		click(brn_serielBatchCapture);

		/* Product 01 */
		if (isDisplayed(btn_captureItem1)) {
			writeTestResults(
					"Verify user can navigated Serial/Batch window and products which going to Outbound should be loaded for item 1",
					"User should be navigated Serial/Batch window and products which going to Outbound should be loaded for item 1",
					"User successfully navigated Serial/Batch window and products which going to Outbound should be loaded for item 1",
					"pass");
		} else {
			writeTestResults(
					"Verify user can navigated Serial/Batch window and products which going to Outbound should be loaded for item 1",
					"User should be navigated Serial/Batch window and products which going to Outbound should be loaded for item 1",
					"User couldn't navigated Serial/Batch window and products which going to Outbound doesn't loaded for item 1",
					"fail");
		}

		click(btn_captureItem1);
		if (isDisplayed(btn_capturePageDocIcon)) {
			writeTestResults("Verify Serial/Batch capturing details are enabled",
					"Serial/Batch capturing details should be enabled",
					"Serial/Batch capturing details successfully enabled", "pass");
		} else {
			writeTestResults("Verify Serial/Batch capturing details are enabled",
					"Serial/Batch capturing details should be enabled",
					"Serial/Batch capturing details doesn't enabled", "fail");
		}

		click(btn_capturePageDocIcon);
		if (isDisplayed(chk_availableProductsTransferOrderOutboundShipment.replace("row", "1"))) {
			writeTestResults("Verify existing Batches are loaded as pop-up",
					"Existing batches should be loaded as pop-up", "Existing batches successfully loaded as pop-up",
					"pass");
		} else {
			writeTestResults("Verify existing batches are loaded as pop-up",
					"Existing batches should be loaded as pop-up", "Existing batches doesn't loaded as pop-up", "fail");
		}

		click(chk_availableProductsTransferOrderOutboundShipment.replace("row", "1"));

		String qty = getText(lbl_qtyBatchSpecifificCapturePage);
		String qty_capture = getText(lbl_availQuantityProductCapturuPage);

		int qty_int = Integer.parseInt(qty);
		int qty_capture_int = Integer.parseInt(qty_capture);

		int i = 2;
		while (qty_int > qty_capture_int) {
			String i_str = String.valueOf(i);
			click(chk_deleteItemCapturePage);
			click(btn_deleteRowProductCaptururePage);
			click(btn_capturePageDocIcon);
			click(chk_availableProductsTransferOrderOutboundShipment.replace("row", i_str));
			Thread.sleep(2000);
			qty_capture = getText(lbl_availQuantityProductCapturuPage);
			qty_capture_int = Integer.parseInt(qty_capture);
		}

		if (qty_int <= qty_capture_int) {
			writeTestResults("Verify selected batches are added to the grid",
					"Selected batches should be added to the grid", "Selected batches successfully added to the grid",
					"pass");
		} else {
			writeTestResults("Verify selected batches are added to the grid",
					"Selected batches should be added to the grid", "Selected batches doesn't added to the grid",
					"fail");
		}

		click(btn_updateCapture);

		String pro = getAttribute(btn_captureItem1, "class");

		if (pro.equals("fli-number green-vackground-fli-number")
				&& pro.equals("fli-number green-vackground-fli-number")) {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User sucessfully capture 'Seriel/Batch'", "pass");
		} else {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User couldn't capture 'Seriel/Batch'", "fail");
		}

		/* Product 04 */
		String item_xpath = btn_itemProductCapturePage.replace("_product_number", "4");
		sleepCusomized(item_xpath);

		if (isDisplayed(item_xpath)) {
			writeTestResults(
					"Verify user can navigated Serial/Batch window and products which going to Outbound should be loaded for item 4",
					"User should be navigated Serial/Batch window and products which going to Outbound should be loaded for item 4",
					"User successfully navigated Serial/Batch window and products which going to Outbound should be loaded for item 4",
					"pass");
		} else {
			writeTestResults(
					"Verify user can navigated Serial/Batch window and products which going to Outbound should be loaded for item 4",
					"User should be navigated Serial/Batch window and products which going to Outbound should be loaded for item 4",
					"User couldn't navigated Serial/Batch window and products which going to Outbound doesn't loaded for item 4",
					"fail");
		}

		click(item_xpath);
		if (isDisplayed(chk_productCaptureRange)) {
			writeTestResults("Verify Serial/Batch capturing details are enabled",
					"Serial/Batch capturing details should be enabled",
					"Serial/Batch capturing details successfully enabled", "pass");
		} else {
			writeTestResults("Verify Serial/Batch capturing details are enabled",
					"Serial/Batch capturing details should be enabled",
					"Serial/Batch capturing details doesn't enabled", "fail");
		}
		click(chk_productCaptureRange);

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$('#attrSlide-txtSerialCount').val(" + qtyTransferOrder + ")");

		String lot4 = getOngoinfSerirlSpecificSerielNumber(10);
		String final_lot_script = "$('#attrSlide-txtSerialFrom').val(\"" + lot4 + "\")";
		jse.executeScript(final_lot_script);

		pressEnter(txt_seriealProductCapture);

		if (qty_int <= qty_capture_int) {
			writeTestResults("Verify selected batches are added to the grid",
					"Selected batches should be added to the grid", "Selected batches successfully added to the grid",
					"pass");
		} else {
			writeTestResults("Verify selected batches are added to the grid",
					"Selected batches should be added to the grid", "Selected batches doesn't added to the grid",
					"fail");
		}

		click(btn_updateCapture);

		String pro1 = getAttribute(btn_captureItem1, "class");

		if (pro1.equals("fli-number green-vackground-fli-number")) {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User sucessfully capture 'Seriel/Batch'", "pass");
		} else {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User couldn't capture 'Seriel/Batch'", "fail");
		}

		/* Product 05 */
		String item_xpath1 = btn_itemProductCapturePage.replace("_product_number", "5");
		sleepCusomized(item_xpath1);
		if (isDisplayed(item_xpath1)) {
			writeTestResults(
					"Verify user can navigated Serial/Batch window and products which going to Outbound should be loaded for item 5",
					"User should be navigated Serial/Batch window and products which going to Outbound should be loaded for item 5",
					"User successfully navigated Serial/Batch window and products which going to Outbound should be loaded for item 5",
					"pass");
		} else {
			writeTestResults(
					"Verify user can navigated Serial/Batch window and products which going to Outbound should be loaded for item 5",
					"User should be navigated Serial/Batch window and products which going to Outbound should be loaded for item 5",
					"User couldn't navigated Serial/Batch window and products which going to Outbound doesn't loaded for item 5",
					"fail");
		}
		click(item_xpath1);

		if (isDisplayed(chk_productCaptureRange)) {
			writeTestResults("Verify Serial/Batch capturing details are enabled",
					"Serial/Batch capturing details should be enabled",
					"Serial/Batch capturing details successfully enabled", "pass");
		} else {
			writeTestResults("Verify Serial/Batch capturing details are enabled",
					"Serial/Batch capturing details should be enabled",
					"Serial/Batch capturing details doesn't enabled", "fail");
		}
		click(chk_productCaptureRange);

		jse.executeScript("$('#attrSlide-txtSerialCount').val(" + qtyTransferOrder + ")");

		String final_lot_script1 = "$('#attrSlide-txtSerialFrom').val(\""
				+ getOngoingSerielBatchSpecificSerielNumber(10) + "\")";
		jse.executeScript(final_lot_script1);

		pressEnter(txt_seriealProductCapture);

		if (qty_int <= qty_capture_int) {
			writeTestResults("Verify selected batches are added to the grid",
					"Selected batches should be added to the grid", "Selected batches successfully added to the grid",
					"pass");
		} else {
			writeTestResults("Verify selected batches are added to the grid",
					"Selected batches should be added to the grid", "Selected batches doesn't added to the grid",
					"fail");
		}

		click(btn_updateCapture);

		String pro2 = getAttribute(btn_captureItem1, "class");
		String pro4 = getAttribute(item_xpath, "class");
		String pro5 = getAttribute(item_xpath1, "class");

		if (pro2.equals("fli-number green-vackground-fli-number")
				&& pro4.equals("fli-number green-vackground-fli-number")
				&& pro4.equals("fli-number green-vackground-fli-number")) {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User sucessfully capture 'Seriel/Batch'", "pass");
		} else {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User couldn't capture 'Seriel/Batch'", "fail");
		}

		sleepCusomized(btn_backButtonUpdateCapture);
		click(btn_backButtonUpdateCapture);

	}

	public void releseOutboundShipmentILO() throws Exception {
		Thread.sleep(5000);
		click(btn_releese2);
		customizeLoadingDelay(lbl_releasedStatusCommonWithoutSpace, 50);
		/*
		 * if (isDisplayed(btn_popoupOkLastStep)) { switchWindow();
		 * click(btn_popoupOkLastStep); }
		 */
		if (isDisplayed(lbl_releasedStatusCommonWithoutSpace)) {
			trackCode = getText(lbl_docNo);
			writeTestResults("Verify user can be relese Outbound Shipment",
					"User should be able to relese above document", "User sucessfully relese Outbound Shipment",
					"pass");
		} else {
			writeTestResults("Verify user can br relese Outbound Shipment",
					"User should be able to relese Outbound Shipment", "User not relese Outbound Shipment", "Fail");
		}
	}

	public void checkWarehouseForProdctAvailabilityOutboundLoanOrder() throws Exception {
		String[] product_availability_after = new String[7];
		int row_position = 1;
		for (int i = 0; i < 7; i++) {
			String row_str = String.valueOf(row_position);
			String availablity_widget_xpath = btn_availabilityCheckWidgetAfterrelesed
					.replace("tblStockAdj", "tblShipDetails").replace("row", row_str);
			sleepCusomized(availablity_widget_xpath);
			Actions action1 = new Actions(driver);
			WebElement we1 = driver.findElement(By.xpath(availablity_widget_xpath));
			action1.moveToElement(we1).build().perform();
			click(availablity_widget_xpath);
			Thread.sleep(2000);
			Actions action12 = new Actions(driver);
			WebElement we12 = driver.findElement(By.xpath(btn_avilabilytCheckSecondMenu));
			action12.moveToElement(we12).build().perform();
			click(btn_avilabilytCheckSecondMenu);
			sleepCusomized(td_availableStockGstmWarehouseAfterRelesed);
			product_availability_after[i] = getText(td_availableStockGstmWarehouseAfterRelesed);
			System.out.println(product_availability_after[i]);
			click(btn_closeAvailabilityCheckPopup);
			row_position++;
		}

		boolean warehouse_check_flag = false;
		for (int j = 0; j < 7; j++) {
			String qty = product_availability[j].replaceAll(",", "");
			int qty_int = Integer.parseInt(qty);
			String after_qty = product_availability_after[j].replaceAll(",", "");
			try {
				int after_qty_int = Integer.parseInt(after_qty);
				int adding_qty = Integer.parseInt(qtyInterDepartmentTransferOrder);
				if (after_qty_int == (qty_int - adding_qty)) {
					warehouse_check_flag = true;
				} else {
					warehouse_check_flag = false;
					break;
				}
			} catch (Exception e) {
				System.out.println(e);
			}

		}

		if (warehouse_check_flag) {
			writeTestResults("Verify stock was deducted from the destination",
					"Stock should be deducted from the destination", "Stock successfully deducted from the destination",
					"pass");
		} else {
			writeTestResults("Verify stock was deducted from the destination",
					"Stock should be deducted from the destination", "Stock doesn't deducted from the destination",
					"fail");
		}
		closeWindow();
	}

	// IW_TC_014
	public void findReleventOLODocument() throws Exception {
		navigateToOutboundLoanOrder();

		sendKeys(txt_outboundLoanOrderSearch, readIWData(8));
		pressEnter(txt_outboundLoanOrderSearch);

		String doc_num = readIWData(8);
		String finalResultClick = lnl_resultOLO.replace("olo_doc_num", doc_num);
		Thread.sleep(4000);
		click(finalResultClick);
		sleepCusomized(btn_releseConfirmation);
		if (isDisplayed(btn_releseConfirmation)) {

			writeTestResults("Verify user can find Outbound Loan Order",
					"User should be able to find Outbound Loan Order", "User sucessfully find Outbound Loan Order",
					"pass");
		} else {
			writeTestResults("Verify user can find Outbound Loan Order",
					"User should be able to find Outbound Loan Order", "User couldn't find Outbound Loan Order",
					"fail");
		}
		Thread.sleep(4000);
		beforeCheckoutUnits = getText(lbl_checkoutUnitsBalance014);

	}

	public void checkAvailabitiyAndSetPalanQuantity() throws Exception {
		int row_position = 1;
		for (int i = 0; i < 7; i++) {
			String row_str = String.valueOf(row_position);
			sleepCusomized(btn_availabilityWidgetOutboundLoanOrder.replace("row", row_str));
			click(btn_availabilityWidgetOutboundLoanOrder.replace("row", row_str));
			click(btn_avilabilytCheckSecondMenu);
			sleepCusomized(td_availableStockGstmWarehouseAfterRelesed);
			product_availability[i] = getText(td_availableStockGstmWarehouseAfterRelesed);
			System.out.println(product_availability[i]);
			click(btn_closeAvailabilityCheckPopup);
			row_position++;
		}

		click(btn_actionOLO014);
		if (isDisplayed(btn_convertToInboundShipment)) {

			writeTestResults("Verify Action Button options are available", "Action Button options should be available",
					"Action Button options sucessfully available", "pass");
		} else {
			writeTestResults("Verify Action Button options are available", "Action Button options should be available",
					"Action Button options does'nt available", "fail");
		}
		Thread.sleep(4);
		Actions action1 = new Actions(driver);
		WebElement we1 = driver.findElement(By.xpath(btn_convertToInboundShipment));
		action1.moveToElement(we1).build().perform();
		click(btn_convertToInboundShipment);
		if (isDisplayed(txt_inboundQuantity)) {

			writeTestResults("Verify navigate to 'Inbound Shipment'",
					"User should be navigate to the 'Inbound Shipment' ",
					"User sucessfully navigate to the 'Inbound Shipment'", "pass");
		} else {
			writeTestResults("Verify navigate to 'Inbound Shipment'",
					"User should be navigate to the 'Inbound Shipment' ",
					"User couldn't navigate to the 'Inbound Shipment'", "fail");
		}
		
		for (int i = 1; i < 8; i++) {
			String str_i = String.valueOf(i);
			String planed_qty = txt_inboundQuantity1.replace("2", str_i);
			sendKeysLookup(planed_qty, qtyPlaned);
		}
		String enteredQty = getAttribute(txt_inboundQuantity1.replace("2", "4"), "value");
		if (enteredQty.equals(qtyPlaned)) {

			writeTestResults("Verify user can add quantity'", "User should be able to add quantity",
					"User sucessfully add quantity", "pass");
		} else {
			writeTestResults("Verify user can add quantity'", "User should be able to add quantity",
					"User couldn't add quantity", "fail");
		}
	}

	public void checkoutDraftReleseBatchCapture() throws Exception {
		click(btn_checkout2);
		Thread.sleep(3000);
		String afterCheckoutUnits = getText(lbl_checkoutUnitsBalanceAfter014);

		if (!beforeCheckoutUnits.equals(afterCheckoutUnits)) {

			writeTestResults("Verify user can checkout the 'Inbound Shipment'",
					"User should be able checkout the 'Inbound Shipment",
					"User sucessfully checkout the 'Inbound Shipment", "pass");
		} else {
			writeTestResults("Verify user can checkout the 'Inbound Shipment'",
					"User should be able to checkout the 'Inbound Shipment",
					"User couldn't checkout the 'Inbound Shipment", "fail");
		}

		draft("Inbound Shipment");

		sleepCusomized(brn_serielBatchCapture);
		click(brn_serielBatchCapture);
		if (isDisplayed(header_searielBatchCapturePage)) {

			writeTestResults("Verify user can navigate to 'Seriel/Batch Capture Page'",
					"User should be able to navigate to 'Seriel/Batch Capture Page'",
					"User sucessfully navigate to 'Seriel/Batch Capture Page'", "pass");
		} else {
			writeTestResults("Verify user can navigate to 'Seriel/Batch Capture Page'",
					"User should be able to navigate to 'Seriel/Batch Capture Page'",
					"User couldn't navigate to 'Seriel/Batch Capture Page'", "fail");
		}

		final String capture_item = btn_serielBatchCaptureProduct1ILOOutboundShipment.replace("1", "Z");

		for (int i = 1; i < 8; i++) {
			String str_i = String.valueOf(i);
			String pro_path = capture_item.replace("Z", str_i);
			Actions action = new Actions(driver);
			WebElement we = driver.findElement(By.xpath(pro_path));
			action.moveToElement(we).build().perform();
			click(pro_path);
			if (i <= 3) {
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("$(\"#attr-divSerialBatchNos\").jqxGrid('setcellvalue', 0, \"Qty\", \"1\");");
				click(btn_updateCapture);
			}
			if (i > 3) {
				click(chk_cpatureSelection1_014.replace("[2]", ""));
				click(chk_cpatureSelection1_014);
				click(btn_delRowsCapturePage);
				click(btn_updateCapture);
			}
			String pro = getAttribute(pro_path, "class");

			if (pro.equals("fli-number green-vackground-fli-number")) {
				writeTestResults("Verify user can capture 'Seriel/Batch'",
						"User should be able to capture 'Seriel/Batch'", "User sucessfully capture 'Seriel/Batch'",
						"pass");
			} else {
				writeTestResults("Verify user can capture 'Seriel/Batch'",
						"User should be able to capture 'Seriel/Batch'", "User couldn't capture 'Seriel/Batch'",
						"fail");
			}
		}

		click(btn_productCaptureBackButton);
		if (isDisplayed(header_draft)) {
			writeTestResults("Verify user can navigate to the 'Inbound Shipment Form'",
					"User should be navigate to the 'Inbound Shipment Form'",
					"User sucessfully navigate to the 'Inbound Shipment Form'", "pass");
		} else {
			writeTestResults("Verify user can navigate to the 'Inbound Shipment Form'",
					"User should be navigate to the 'Inbound Shipment Form'",
					"User couldn't navigate to the 'Inbound Shipment Form'", "fail");
		}

		sleepCusomized(btn_relese);
		click(btn_relese);
		customizeLoadingDelay(lbl_relesedConfirmation, 30);
		trackCode = getText(lbl_creationsDocNo);

		if (isDisplayed(lbl_relesedConfirmation)) {
			writeTestResults("Verify user can release Inbound Shipment",
					"User should be able to release Inbound Shipment", "User successfully release Inbound Shipment",
					"pass");
		} else {
			writeTestResults("Verify user can release Inbound Shipment",
					"User should be able to release Inbound Shipment", "User couldn't release Inbound Shipment",
					"fail");
		}

	}

	public void checkWarehouseForProdctAvailabilityConvertToInboundShipmentOutboundLoanOrder() throws Exception {
		String[] product_availability_after = new String[7];
		int row_position = 1;
		for (int i = 0; i < 7; i++) {
			String row_str = String.valueOf(row_position);
			sleepCusomized(btn_productAvilabilityWidgetShipmentDetailsTable.replace("row", row_str));
			click(btn_productAvilabilityWidgetShipmentDetailsTable.replace("row", row_str));
			Thread.sleep(1000);
			click(btn_avilabilytCheckSecondMenu);
			Thread.sleep(3000);
			product_availability_after[i] = getText(td_availableStockGstmWarehouseAfterRelesed);
			System.out.println(product_availability_after[i]);
			click(btn_closeAvailabilityCheckPopup);
			row_position++;
		}

		boolean warehouse_check_flag = false;
		for (int j = 0; j < 7; j++) {
			String qty = product_availability[j].replaceAll(",", "");
			int qty_int = Integer.parseInt(qty);
			String after_qty = product_availability_after[j].replaceAll(",", "");
			try {
				int after_qty_int = Integer.parseInt(after_qty);
				int adding_qty = Integer.parseInt(qtyPlaned);
				if (after_qty_int == (qty_int + adding_qty)) {
					warehouse_check_flag = true;
				} else {
					warehouse_check_flag = false;
					break;
				}
			} catch (Exception e) {
				System.out.println(e);
			}

		}

		if (warehouse_check_flag) {
			writeTestResults("Verify stock was transfered to the warehouse",
					"Stock should be transfered to the warehouse", "Stock successfully transfered to the warehouse",
					"pass");
		} else {
			writeTestResults("Verify stock was transfered to the warehouse",
					"Stock should be transfered to the warehouse", "Stock doesn't transfered to the warehouse", "fail");
		}

	}
	/*
	 * txt_inboundQuantity chk_cpatureSelection1_015 cell_afterAvailableQuantity
	 * btn_releseConfirmation
	 */

	// IW_TC_015
	public void navigateToSalesInvoiceAndConvertToSalesInvoice() throws Exception {
		findReleventOLODocument();

		click(btn_action);
		if (isDisplayed(btn_conToSalesInvoiceOLO)) {
			writeTestResults("Verify set of options loaded under the action",
					"Set of options should be loaded under the action",
					"Set of options successfully loaded under the action", "pass");
		} else {
			writeTestResults("Verify set of options loaded under the action",
					"Set of options should be loaded under the action",
					"Set of options doesn't loaded under the action", "fail");
		}

		click(btn_conToSalesInvoiceOLO);
		sleepCusomized(header_pageLabel);
		String header = getText(header_pageLabel);

		boolean flag = false;

		sleepCusomized(drop_downCurencyOLOSlaesInvoic);
		String curency = getAttribute(drop_downCurencyOLOSlaesInvoic, "disabled");
		if (curency.equals("true")) {
			flag = true;
		} else {
			flag = false;
		}

		String address = getAttribute(txt_billingAddressILOSalesInvoice, "value");
		if (!address.equals("")) {
			flag = true;
		} else {
			flag = false;
		}

		String shipping_address = getAttribute(txt_shippingAddressILOSalesInvoice, "value");
		if (!shipping_address.equals("")) {
			flag = true;
		} else {
			flag = false;
		}

		String sales_unit = getText(dropdownSalesUnitOLOSalesInvoice);
		if (!sales_unit.equals("")) {
			flag = true;
		} else {
			flag = false;
		}

		if (header.equals("Sales Invoice") && flag == true) {
			writeTestResults("Verify Sales Invoice was loaded with the details",
					"Sales Invoice should be loaded with the details",
					"Sales Invoice successfully loaded with the details", "pass");
		} else {
			writeTestResults("Verify Sales Invoice was loaded with the details",
					"Sales Invoice should be loaded with the details", "Sales Invoice doesn't loaded with the details",
					"fail");
		}
	}

	public void cehckoutDraftReleaseSalesInvoice() throws Exception {
		click(btn_checkoutOLOSalesInvoice);
		Thread.sleep(3000);

		if (isDisplayed(lbl_checkoutUnitsBalanceAfter014)) {

			writeTestResults("Verify user can checkout the Sales Invoice",
					"User should be able checkout the Sales Invoice", "User sucessfully checkout the Sales Invoice",
					"pass");
		} else {
			writeTestResults("Verify user can checkout the Sales Invoice",
					"User should be able to checkout the Sales Invoice", "User couldn't checkout the Sales Invoice",
					"fail");
		}

		click(btn_draftOLOSalesInvoice);
		customizeLoadingDelay(btn_relese, 40);
		if (isDisplayed(btn_relese)) {

			writeTestResults("Verify user can br draft Sales Invoice", "User should be able to draft Sales Invoice",
					"User sucessfully draft Sales Invoice", "pass");
		} else {
			writeTestResults("Verify user can br draft Sales Invoice", "User should be able to draft Sales Invoice",
					"User couldn't draft Sales Invoice", "Fail");
		}

		Thread.sleep(3000);
		click(brn_serielBatchCapture);
		for (int i = 1; i < 4; i++) {
			Thread.sleep(2000);
			String item = String.valueOf(i);
			click(btn_itemProductCapturePage.replace("_product_number", item));
			Thread.sleep(2000);
			JavascriptExecutor j7 = (JavascriptExecutor) driver;
			j7.executeScript("$(\"#attr-divSerialBatchNos\").jqxGrid('setcellvalue', 0, \"Qty\", \"9\");");
			Thread.sleep(2000);
			click(btn_updateCapture);
			Thread.sleep(3000);
		}
		for (int i = 4; i < 8; i++) {
			Thread.sleep(2000);
			String item = String.valueOf(i);
			Actions action = new Actions(driver);
			WebElement we = driver.findElement(By.xpath(btn_itemProductCapturePage.replace("_product_number", item)));
			action.moveToElement(we).build().perform();
			click(btn_itemProductCapturePage.replace("_product_number", item));
			Thread.sleep(2000);
			click(btn_updateCapture);
			Thread.sleep(3000);
		}
		click(btn_productCaptureBackButton);
		Thread.sleep(3000);

		click(btn_relese);
		customizeLoadingDelay(lbl_relesedConfirmation, 40);
		trackCode = getText(lbl_docNo);

		if (isDisplayed(lbl_relesedConfirmation)) {
			writeTestResults("Verify user can releese Sales Invoice", "Sales Invoice should be releesed",
					"Sales Invoice successfully released", "pass");
		} else {
			writeTestResults("Verify user can releese Sales Invoice", "Sales Invoice should be releesed",
					"Sales Invoice doesn't releesed", "fail");
		}

	}

	// IW_TC_016
	public void navigateToAccemblyCostEstimationForm() throws Exception {
		navigateInventoryAndWarehouseModule();
		explicitWait(btn_assemblyCostEstimation, 10);
		click(btn_assemblyCostEstimation);
		if (isDisplayed(btn_newAssemblyCostAstimation)) {
			writeTestResults("Verify user can navigate to the 'Assembly Cost Estmation Page'",
					"User should be navigate to the 'Assembly Cost Estmation Page'",
					"User sucessfully navigate to the 'Assembly Cost Estmation Page'", "pass");
		} else {
			writeTestResults("Verify user can navigate to the 'Assembly Cost Estmation Page'",
					"User should be navigate to the 'Assembly Cost Estmation Page'",
					"User couldn't navigate to the 'Assembly Cost Estmation Page'", "fail");
		}

		click(btn_newAssemblyCostAstimation);
		if (isDisplayed(txt_assemblyCostEstiamtionDescription)) {
			writeTestResults("Verify user can navigate to the 'Assembly Cost Estmation Form'",
					"User should be navigate to the 'Assembly Cost Estmation Form'",
					"User sucessfully navigate to the 'Assembly Cost Estmation Form'", "pass");
		} else {
			writeTestResults("Verify user can navigate to the 'Assembly Cost Estmation Form'",
					"User should be navigate to the 'Assembly Cost Estmation Form'",
					"User couldn't navigate to the 'Assembly Cost Estmation Form'", "fail");
		}
	}

	public void fillAssemblyCostEstimationFormSummary() throws Exception {
		sendKeys(txt_assemblyCostEstiamtionDescription, descriptionAssemblyCostEstimation);

		if (isDisplayed(txt_assemblyCostEstiamtionDescription)) {
			writeTestResults("Verify added discription is avilable", "Added description should be available",
					"Added description sucessfully available", "pass");
		} else {
			writeTestResults("Verify added discription is avilable", "Added description should be available",
					"Added description does'nt available", "fail");
		}

		click(btn_searchCustomeACE);
		sendKeys(txt_cusSearchACE, customerAccountACE);
		pressEnter(txt_cusSearchACE);

		if (isDisplayed(lnk_resultCusAccountACE)) {
			writeTestResults("Verify user can add 'Customer Account'", "User should be able to add 'Customer Account'",
					"User successfully add a 'Customer Account'", "pass");
		} else {
			writeTestResults("Verify user can add 'Customer Account'", "User should be able to add 'Customer Account'",
					"User couldn't add a 'Customer Account'", "fail");
		}
		doubleClick(lnk_resultCusAccountACE);

		click(btn_searchAssemblyProACE);
		sleepCusomized(txt_AssemblyProductSearchACE);
		sendKeys(txt_AssemblyProductSearchACE, assemblyProductACE);
		pressEnter(txt_AssemblyProductSearchACE);
		sleepCusomized(lnk_resltProductAssemblyACE);
		if (isDisplayed(lnk_resltProductAssemblyACE)) {
			writeTestResults("Verify user can add 'Assembly Product'", "User should be able to add 'Assembly Product'",
					"User successfully add a 'Assembly Product'", "pass");
		} else {
			writeTestResults("Verify user can add 'Assembly Product'", "User should be able to add 'Assembly Product'",
					"User couldn't add a 'Assembly Product'", "fail");
		}
		doubleClick(lnk_resltProductAssemblyACE);

		click(btn_searchPricingProfileACE);
		sendKeys(txt_pricingProfileACE, pricingProfileACE);
		pressEnter(txt_pricingProfileACE);
		if (isDisplayed(lnk_pricingProfileACE)) {
			writeTestResults("Verify user can add 'Pricing Profile'", "User should be able to add 'Pricing Profile'",
					"User successfully add a 'Pricing Profile'", "pass");
		} else {
			writeTestResults("Verify user can add 'Pricing Profile'", "User should be able to add 'Pricing Profile'",
					"User couldn't add a 'Pricing Profile'", "fail");
		}
		doubleClick(lnk_pricingProfileACE);

		click(tab_estimationDeatailsACE);

		if (isDisplayed(drop_typeACE1)) {
			writeTestResults("Verify estimation details are loaded", "Estimation details should be loaded",
					"Estimation details successfully loaded", "pass");
		} else {
			writeTestResults("Verify estimation details are loaded", "Estimation details should be loaded",
					"Estimation details couldn't loaded", "fail");
		}
	}

	public void filldeatailsInDetailsTab() throws Exception {
		/* Row 1 */
		Select comboBox = new Select(driver.findElement(By.xpath(drop_typeACE1)));
		selectText(drop_typeACE1, typeEstmationAce1);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		if (selectedComboValue.equals(typeEstmationAce1)) {
			writeTestResults("Verify user can select type as Input Product",
					"User should be able to select type as Input Product",
					"User successfully select type as Input Product", "pass");
		} else {
			writeTestResults("Verify user can select type as Input Product",
					"User should be able to select type as Input Product", "User couldn't select type as Input Product",
					"fail");
		}

		click(btn_searchProductEstmationAce1);
		sleepCusomized(txt_productSearch);
		sendKeys(txt_productSearch, product1EstimationDeatailsACE);
		pressEnter(txt_productSearch);
		sleepCusomized(btn_resultProduct);
		if (isDisplayed(btn_resultProduct)) {
			writeTestResults("Verify user can select product", "User should be able to select product",
					"User successfully select product", "pass");
		} else {
			writeTestResults("Verify user can select product", "User should be able to select product",
					"User couldn't select product", "fail");
		}
		sleepCusomized(btn_resultProduct);
		doubleClick(btn_resultProduct);

		sendKeys(txt_estimatedQuantityACE, qtyInputProduct1ACE);
		sendKeys(txt_costEstmatedACE1Input, costInputProduct1ACE);
		if (!isSelected(chk_billableEstmationRow1ACE)) {
			click(chk_billableEstmationRow1ACE);
		}
		if (isDisplayed(txt_estimatedQuantityACE) && isDisplayed(txt_costEstmatedACE1Input)
				&& isSelected(chk_billableEstmationRow1ACE)) {
			writeTestResults("Verify user can add Estimated Qty and Cost with enabling Billable check box",
					"User should be able to add Estimated Qty and Cost with enabling Billable check box",
					"User successfully add Estimated Qty and Cost with enabling Billable check box", "pass");
		} else {
			writeTestResults("Verify user can add Estimated Qty and Cost with enabling Billable check box",
					"User should be able to add Estimated Qty and Cost with enabling Billable check box",
					"User couldn't add Estimated Qty and Cost with enabling Billable check box", "fail");
		}

		/* Row 02 */
		click(btn_adnewRow1ACE);
		if (isDisplayed(drop_typeACE2)) {
			writeTestResults("Verify new line is enabled", "New line should be enabled",
					"New line successfully enabled", "pass");
		} else {
			writeTestResults("Verify new line is enabled", "New line should be enabled", "New line couldn't enabled",
					"fail");
		}

		Select comboBox1 = new Select(driver.findElement(By.xpath(drop_typeACE2)));
		selectText(drop_typeACE2, typeEstmationAce2);
		String selectedComboValue1 = comboBox1.getFirstSelectedOption().getText();

		if (selectedComboValue1.equals(typeEstmationAce2)) {
			writeTestResults("Verify user can select type as Output Product",
					"User should be able to select type as Output Product",
					"User successfully select type as Output Product", "pass");
		} else {
			writeTestResults("Verify user can select type as Output Product",
					"User should be able to select type as Output Product",
					"User couldn't select type as Output Product", "fail");
		}

		click(btn_searchProduct2ACE);
		sleepCusomized(txt_productSearch);
		sendKeys(txt_productSearch, product2EstimationDeatailsACE);
		pressEnter(txt_productSearch);
		sleepCusomized(btn_resultProduct);
		if (isDisplayed(btn_resultProduct)) {
			writeTestResults("Verify user can select product", "User should be able to select product",
					"User successfully select product", "pass");
		} else {
			writeTestResults("Verify user can select product", "User should be able to select product",
					"User couldn't select product", "fail");
		}
		sleepCusomized(btn_resultProduct);
		doubleClick(btn_resultProduct);

		sendKeys(txt_estimatedQuantityACE2, qtyInputProduct2ACE);
		sendKeys(txt_costEstmatedACEOutput2, costInputProduct2ACE);
		if (!isSelected(chk_billableEstmationRow2ACE)) {
			click(chk_billableEstmationRow2ACE);
		}
		if (isDisplayed(txt_estimatedQuantityACE2) && isDisplayed(txt_costEstmatedACEOutput2)
				&& isSelected(chk_billableEstmationRow2ACE)) {
			writeTestResults("Verify user can add Estimated Qty and Cost with enabling Billable check box",
					"User should be able to add Estimated Qty and Cost with enabling Billable check box",
					"User successfully add Estimated Qty and Cost with enabling Billable check box", "pass");
		} else {
			writeTestResults("Verify user can add Estimated Qty and Cost with enabling Billable check box",
					"User should be able to add Estimated Qty and Cost with enabling Billable check box",
					"User couldn't add Estimated Qty and Cost with enabling Billable check box", "fail");
		}

		/* Row 3 */
		click(btn_adaNewRow3);

		if (isDisplayed(drop_typeExpenceACE)) {
			writeTestResults("Verify new line is enabled", "New line should be enabled",
					"New line successfully enabled", "pass");
		} else {
			writeTestResults("Verify new line is enabled", "New line should be enabled", "New line couldn't enabled",
					"fail");
		}

		Select comboBox2 = new Select(driver.findElement(By.xpath(drop_typeExpenceACE)));
		selectText(drop_typeExpenceACE, typeEstmationAce3Expence);
		String selectedComboValue2 = comboBox2.getFirstSelectedOption().getText();

		if (selectedComboValue2.equals(typeEstmationAce3Expence)) {
			writeTestResults("Verify user can select type as Expense", "User should be able to select type as Expense",
					"User successfully select type as Expense", "pass");
		} else {
			writeTestResults("Verify user can select type as Expense", "User should be able to select type as Expense",
					"User couldn't select type as Expense", "fail");
		}

		selectIndex(drop_estiamationRow3ReferenceCodeExpenceACE, 2);

		if (isDisplayed(drop_estiamationRow3ReferenceCodeExpenceACE)) {
			writeTestResults("Verify user can select Reference Code", "User should be able to select Reference Code",
					"User successfully select Reference Code", "pass");
		} else {
			writeTestResults("Verify user can select Reference Code", "User should be able to select Reference Code",
					"User couldn't select Reference Code", "fail");
		}

		click(btn_searchServiceACE3ProductLookup);

		sleepCusomized(txt_productSearch);
		sendKeys(txt_productSearch, product3EstimationDeatailsACESerivice);
		pressEnter(txt_productSearch);
		sleepCusomized(btn_resultProduct);
		if (isDisplayed(btn_resultProduct)) {
			writeTestResults("Verify user can select Service", "User should be able to select Service",
					"User successfully select Service", "pass");
		} else {
			writeTestResults("Verify user can select Service", "User should be able to select Service",
					"User couldn't select Service", "fail");
		}
		sleepCusomized(btn_resultProduct);
		doubleClick(btn_resultProduct);

		sendKeys(txt_costServiceProductEstmationACE, costInputService3ACE);
		if (!isSelected(chk_billableEstmationRow3ACE)) {
			click(chk_billableEstmationRow3ACE);
		}
		if (isDisplayed(txt_costServiceProductEstmationACE) && isSelected(chk_billableEstmationRow3ACE)) {
			writeTestResults("Verify user can add Cost with enabling Billable check box",
					"User should be able to add Cost with enabling Billable check box",
					"User successfully add Cost with enabling Billable check box", "pass");
		} else {
			writeTestResults("Verify user can add Cost with enabling Billable check box",
					"User should be able to add Cost with enabling Billable check box",
					"User couldn't add Cost with enabling Billable check box", "fail");
		}
		/* ROW 4 */
		click(btn_addNewRow4EstmationDetailsACE);

		if (isDisplayed(drop_type4ServiceACE)) {
			writeTestResults("Verify new line is enabled", "New line should be enabled",
					"New line successfully enabled", "pass");
		} else {
			writeTestResults("Verify new line is enabled", "New line should be enabled", "New line couldn't enabled",
					"fail");
		}

		Select comboBox3 = new Select(driver.findElement(By.xpath(drop_type4ServiceACE)));
		selectText(drop_type4ServiceACE, typeEstmationAce4);
		String selectedComboValue3 = comboBox3.getFirstSelectedOption().getText();

		if (selectedComboValue3.equals(typeEstmationAce4)) {
			writeTestResults("Verify user can select type as Services",
					"User should be able to select type as Services", "User successfully select type as Services",
					"pass");
		} else {
			writeTestResults("Verify user can select type as Services",
					"User should be able to select type as Services", "User couldn't select type as Services", "fail");
		}

		click(btn_searchProductLookuop4ACE);

		sleepCusomized(txt_productSearch);
		sendKeys(txt_productSearch, product4EstimationDeatailsACEService);
		pressEnter(txt_productSearch);
		sleepCusomized(btn_resultProduct);
		if (isDisplayed(btn_resultProduct)) {
			writeTestResults("Verify user can select Service", "User should be able to select Service",
					"User successfully select Service", "pass");
		} else {
			writeTestResults("Verify user can Select service", "User should be able to select Service",
					"User couldn't select Service", "fail");
		}
		sleepCusomized(btn_resultProduct);
		doubleClick(btn_resultProduct);

		sendKeys(txt_cost4ServiceProductEstimationACE, costInputService4ACE);
		if (!isSelected(chk_billable4ServiceACeEstimation)) {
			click(chk_billable4ServiceACeEstimation);
		}
		if (isDisplayed(txt_cost4ServiceProductEstimationACE) && isSelected(chk_billable4ServiceACeEstimation)) {
			writeTestResults("Verify user can add Cost with enabling Billable check box",
					"User should be able to add Cost with enabling Billable check box",
					"User successfully add Cost with enabling Billable check box", "pass");
		} else {
			writeTestResults("Verify user can add Cost with enabling Billable check box",
					"User should be able to add Cost with enabling Billable check box",
					"User couldn't add Cost with enabling Billable check box", "fail");
		}
	}

	public void checkoutDraftReleseACE() throws Exception {
		click(tab_summaryACE);
		if (isDisplayed(btn_checkoutACE)) {
			writeTestResults("Verify user can navigate to the Summary Tab",
					"User should be able navigate to the Summary Tab", "User successfully navigate to the Summary Tab",
					"pass");
		} else {
			writeTestResults("Verify user can navigate to the Summary Tab",
					"User should be able navigate to the Summary Tab", "User couldn't navigate to the Summary Tab",
					"fail");
		}

		click(btn_checkoutACE);
		Thread.sleep(3000);
		String total_profit = getText(lbl_totalProfitACE);

		if (!total_profit.equals("0.000")) {
			writeTestResults("Verify user can checkout 'Assembly Cost Estimation'",
					"User should be able to checkout 'Assembly Cost Estimation'",
					"User successfully checkout 'Assembly Cost Estimation'", "pass");
		} else {
			writeTestResults("Verify user can checkout 'Assembly Cost Estimation'",
					"User should be able to checkout 'Assembly Cost Estimation'",
					"User couldn't checkout 'Assembly Cost Estimation'", "fail");
		}
		draft("Assembly Cost Estimation");
		relese("Assembly Cost Estimation");
		Thread.sleep(3000);
		writeIWData("Accembly Cost Estimation", getText(lbl_relesedACEDocNumber), 7);

	}

	// IW_TC_017
	public void navigateToAssemblyOrderForm() throws Exception {
		navigateInventoryAndWarehouseModule();
		click(btn_assemblyOredr);
		if (isDisplayed(btn_newAssemblyOrder)) {
			writeTestResults("Verify user can navigate to Assembly Order Page",
					"User should be able navigate to Assembly Order Page",
					"User successfully navigate to Assembly Order Page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Assembly Order Page",
					"User should be able navigate to Assembly Order Page",
					"User couldn't navigate to Assembly Order Page", "fail");
		}

		click(btn_newAssemblyOrder);
		if (isDisplayed(btn_assemblyProductSearch)) {
			writeTestResults("Verify user can navigate to Assembly Order Form",
					"User should be able navigate to Assembly Order Form",
					"User successfully navigate to Assembly Order Form", "pass");
		} else {
			writeTestResults("Verify user can navigate to Assembly Order Form",
					"User should be able navigate to Assembly Order Form",
					"User couldn't navigate to Assembly Order Form", "fail");
		}
	}

	public void fillAsemblyOrderForm() throws Exception {
		click(btn_assemblyProductSearch);
		sleepCusomized(txt_productSearch);
		sendKeys(txt_productSearch, assemblyProductAO);
		pressEnter(txt_productSearch);
		sleepCusomized(btn_resultProduct);
		if (isDisplayed(btn_resultProduct)) {
			writeTestResults("Verify user can select type as Assembly Product",
					"User should be able to select type as Assembly Product",
					"User successfully select type as Assembly Product", "pass");
		} else {
			writeTestResults("Verify user can select type as Assembly Product",
					"User should be able to select type as Assembly Product",
					"User couldn't select type as Assembly Product", "fail");
		}
		Thread.sleep(3000);
		sleepCusomized(btn_resultProduct);
		doubleClick(btn_resultProduct);

		sendKeys(txt_serialNoAO, genProductId() + currentTimeAndDate());

		selectIndex(drop_assemblyGroup, 0);
		if (isDisplayed(drop_assemblyGroup)) {
			writeTestResults("Verify user can select Assembly Group", "User should be able to select Assembly Group",
					"User successfully select Assembly Group", "pass");
		} else {
			writeTestResults("Verify user can select Assembly Group", "User should be able to select Assembly Group",
					"User couldn'nt select Assembly Group", "fail");
		}

		click(btn_emloyeeSerchAO);
		sleepCusomized(txt_employee);
		sendKeys(txt_employee, employee1);
		pressEnter(txt_employee);
		sleepCusomized(lnk_resultEmployee);
		if (isDisplayed(lnk_resultEmployee)) {
			writeTestResults("Verify user can select Responsible Employee",
					"User should be able to select Responsible Employee",
					"User successfully select Responsible Employee", "pass");
		} else {
			writeTestResults("Verify user can select Responsible Employee",
					"User should be able to select Responsible Employee", "User couldn'nt select Responsible Employee",
					"fail");
		}
		doubleClick(lnk_resultEmployee);

		selectIndex(drop_priorityAO, 1);
		if (isDisplayed(drop_priorityAO)) {
			writeTestResults("Verify user can select Priority", "User should be able to select Priority",
					"User successfully select Priority", "pass");
		} else {
			writeTestResults("Verify user can select Priority", "User should be able to select Priority",
					"User couldn'nt select Priority", "fail");
		}

		click(btn_searchSCostEstimtioAo);
		sleepCusomized(txt_costEstimation);
		sendKeys(txt_costEstimation, readIWData(7));
		pressEnter(txt_costEstimation);
		Thread.sleep(3000);
		explicitWait(lnk_resultCostEstimation, 25);
		if (isDisplayed(lnk_resultCostEstimation)) {
			writeTestResults("Verify user can select Cost Estimation", "User should be able to select Cost Estimation",
					"User successfully select Cost Estimation", "pass");
		} else {
			writeTestResults("Verify user can select Cost Estimation", "User should be able to select Cost Estimation",
					"User couldn'nt select Cost Estimation", "fail");
		}
		doubleClick(lnk_resultCostEstimation);

		selectIndex(drop_barcodeAO, 0);

		click(btn_searchProductionUnit);
		sleepCusomized(txt_productionUnit);
		sendKeys(txt_productionUnit, productionUnit1);
		pressEnter(txt_productionUnit);
		sleepCusomized(lnk_resultProductUnit);
		if (isDisplayed(lnk_resultProductUnit)) {
			writeTestResults("Verify user can select Production Unit", "User should be able to select Production Unit",
					"User successfully select Production Unit", "pass");
		} else {
			writeTestResults("Verify user can select Production Unit", "User should be able to select Production Unit",
					"User couldn'nt select Production Unit", "fail");
		}
		doubleClick(lnk_resultProductUnit);

		Thread.sleep(3000);
		selectText(drop_warehouseWIPAO, warehouseWIPAO);
		selectText(drop_inputWareAO, warehouseAO);
		selectText(drop_wareOutAO, warehouseAO);

		if (isDisplayed(txt_serialNoAO)) {
			writeTestResults("Verify user can add New Serial Number", "User should be able to add New Serial Number",
					"User successfully add New Serial Number", "pass");
		} else {
			writeTestResults("Verify user can add New Serial Number", "User should be able to add New Serial Number",
					"User couldn't add New Serial Number", "fail");
		}

		click(date_firstClickAO);
		String _28 = date_secondSeclectioAO.replace("date", "28");
		Thread.sleep(2000);
		click(btn_clanderFw);
		click(_28);

		click(date_secondClickAO);
		Thread.sleep(2000);
		click(btn_clanderFw);
		click(_28);

		if (isDisplayed(date_firstClickAO) && isDisplayed(date_secondClickAO)) {
			writeTestResults("Verify user can select Requested Date and Requested End Date from the Calender",
					"User should be able to select Requested Date and Requested End Date from the Calender",
					"User successfully select Requested Date and Requested End Date from the Calender", "pass");
		} else {
			writeTestResults("Verify user can select Requested Date and Requested End Date from the Calender",
					"User should be able to select Requested Date and Requested End Date from the Calender",
					"User couldn't select Requested Date and Requested End Date from the Calender", "fail");
		}
	}

	public void productTabAndEstimation() throws Exception {
		click(tab_productAO);
		if (isDisplayed(drop_warehouseProductAO1)) {
			writeTestResults("Verify products which defined in Cost Estmation are loaded under Product Tab",
					"Products which defined in Cost Estmation should be loaded under Product Tab",
					"Products which defined in Cost Estmation successfully loaded under Product Tab", "pass");
		} else {
			writeTestResults("Verify products which defined in Cost Estmation are loaded under Product Tab",
					"Products which defined in Cost Estmation should be loaded under Product Tab",
					"Products which defined in Cost Estmation does'nt loaded under Product Tab", "fail");
		}

		selectText(drop_warehouseProductAO1, fromWareTO);

		if (isDisplayed(drop_warehouseProductAO1)) {
			writeTestResults("Verify user can select warehouses", "User should be able to select warehouses",
					"User successfully select warehouse", "pass");
		} else {
			writeTestResults("Verify user can select warehouses", "User should be able to select warehouses",
					"User couldn't select warehouse", "fail");
		}

		click(tab_estimationDeatailsAO);

		if (isDisplayed(btn_availabilityCheckForVErifyEstmationProductAO)) {
			writeTestResults("Verify that Service products details are loaded according to the Cost Estimation",
					"Service products details should be loaded according to the Cost Estimation",
					"Service products details are successfully loaded according to the Cost Estimation", "pass");
		} else {
			writeTestResults("Verify that Service products details are loaded according to the Cost Estimation",
					"Service products details should be loaded according to the Cost Estimation",
					"Service products details does'nt loaded according to the Cost Estimation", "fail");
		}

	}

	public void draftAndReleeseAO() throws Exception {
		draft("Assembly Order");
		relese("Assembly Order");
	}

	// IW_TC_019
	public void serielGeneratedViaBarcodeGenerator() throws Exception {
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		menufacturerCommon();
		batchProductCommonDispatchOnly();

		removeShipmentAcceptance();

		customizeLoadingDelay(navigation_pane, 12);
		click(navigation_pane);

		customizeLoadingDelay(btn_procumentModule, 12);
		click(btn_procumentModule);

		customizeLoadingDelay(btn_purchaseOrder, 12);
		click(btn_purchaseOrder);

		click(btn_newPrurchaseOrder);

		click(btn_purchaseOrderToInboundShipmentToPurchaseInvoiceJourney);

		sleepCusomized(btn_vendorSearchPurchaseOredr);
		click(btn_vendorSearchPurchaseOredr);

		sleepCusomized(txt_vendorSearch23);
		sendKeys(txt_vendorSearch23, vrndorBarcodeGenarate);
		pressEnter(txt_vendorSearch23);
		Thread.sleep(2500);
		sleepCusomized(lnk_resultVendorSeacrh23);
		doubleClick(lnk_resultVendorSeacrh23);

		click(btn_serachProductLookup23);

		sleepCusomized(txt_productSearch);

		sendKeys(txt_productSearch, readIWData(3));

		pressEnter(txt_productSearch);

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,-200)");

		sleepCusomized(lnk_resultProductPurchaseOrder);

		doubleClick(lnk_resultProductPurchaseOrder);

		selectText(drop_warehousePurchaseOrder, warehousePurchaseOrder);

		sendKeys(txt_qtyProductPurchaseOrder, qtyPurchaseORder);

		sendKeys(txt_unitPricePurchaseOrder, unitPricePurchaseOrder);

		click(btn_checkoutPurchaaseOrdeer);

		click(btn_avilabilityCheckWidget);

		sleepCusomized(btn_avilabilytCheckSecondMenu);
		click(btn_avilabilytCheckSecondMenu);

		InventoryAndWarehouseModuleRegression obj = new InventoryAndWarehouseModuleRegression();
		obj.customizeLoadingDelayAndContinue(lbl_productAvailability019, 5);
		try {
			product_availability[0] = driver.findElement(By.xpath(lbl_productAvailability019)).getText().trim();
		} catch (Exception e) {
			product_availability[0] = "0";
			System.out.println(product_availability[0]);
		}

		sleepCusomized(btn_closeAvailabilityCheckPopup);
		click(btn_closeAvailabilityCheckPopup);

		click(btn_draft2);

		Thread.sleep(3000);
		click(btn_releese2);
		sleepCusomized(btn_goToPageLink);
		click(btn_goToPageLink);
		switchWindow();
		customizeLoadingDelay(btn_checkout2, 15);
		Thread.sleep(2000);
		click(btn_checkout2);

		Thread.sleep(300);
		click(btn_draft2);

		Thread.sleep(4000);
		click(btn_releese2);
		Thread.sleep(4000);

		sleepCusomized(btn_goToPageLink);
		click(btn_closeGoToLinkPopup);

		Thread.sleep(3000);
		inbound_shipment_shipment_acceptance = getText(lbl_inboundShipmentDocNoShipmentAcceptance);
		String inbound_shipment_doc = getAttribute(lbl_relesedInboundShipmentNoBarcodeGeneratorPage, "orderid");

		pageRefersh();

// end IW_TC_012 // Current TC Start Actions action1 = new Actions(driver);
//		  WebElement we1 =
//		  driver.findElement(By.xpath(lbl_relesedInboundShipmentNoBarcodeGeneratorPage)
//		  ); action1.moveToElement(we1).build().perform();

		System.out.println(inbound_shipment_doc);
		sleepCusomized(navigation_pane);
		click(navigation_pane);
		sleepCusomized(inventory_module);
		click(inventory_module);
		if (isDisplayed(btn_barcodeGenerator)) {
			writeTestResults("Verify user can navigate to Inventory Module",
					"User should be navigate to Inventory Module", "User successfully navigate to Inventory Module",
					"pass");
		} else {
			writeTestResults("Verify user can navigate to Inventory Module",
					"User should be navigate to Inventory Module", "User couldn't navigate to Inventory Module",
					"fail");
		}
		sleepCusomized(btn_barcodeGenerator);
		click(btn_barcodeGenerator);
		Actions action = new Actions(driver);
		WebElement we = driver.findElement(By.xpath(icon_inboundShipmentBarcodeGeneratorPage));
		action.moveToElement(we).build().perform();
		sleepCusomized(icon_inboundShipmentBarcodeGeneratorPage);
		if (isDisplayed(icon_inboundShipmentBarcodeGeneratorPage)) {
			writeTestResults("Verify user can navigated to Barcode Generator Form",
					"User should be navigated to Barcode Generator Form",
					"User successfully navigated to Barcode Generator Form", "pass");
		} else {
			writeTestResults("Verify user can navigated to Barcode Generator Form",
					"User should be navigated to Barcode Generator Form",
					"User couldn't navigated to Barcode Generator Form", "fail");
		}

		click(icon_inboundShipmentBarcodeGeneratorPage);
		if (isDisplayed(dropdown_warehouseBarcodeGeneratorPage)) {
			writeTestResults("Verify relevant filtering details are available",
					"Relevant filtering details should be available",
					"Relevant filtering details successfully available", "pass");
		} else {
			writeTestResults("Verify relevant filtering details are available",
					"Relevant filtering details should be available", "Relevant filtering details doesn't available",
					"fail");
		}

		selectText(dropdown_warehouseBarcodeGeneratorPage, warehousePurchaseOrder);
		if (isDisplayed(dropdown_warehouseBarcodeGeneratorPage)) {
			writeTestResults("Verify user can select the warehouse", "User should be able to select the warehouse",
					"User successfully select the warehouse", "pass");
		} else {
			writeTestResults("Verify user can select the warehouse", "User should be able to select the warehouse",
					"User couldn't select the warehouse", "fail");
		}

		click(span_dateRangeBarcodeGeneratorPage);

		if (isDisplayed(btn_todayDateSetBarcodeGeneratorPage)) {
			writeTestResults("Verify user can select Date", "User should be able to select Date",
					"User successfully select Date", "pass");
		} else {
			writeTestResults("Verify user can select Date", "User should be able to select Date",
					"User couldn't select Date", "fail");
		}
		click(btn_todayDateSetBarcodeGeneratorPage);
		if (isDisplayed(txt_searchBarcodeGeneratorPage)) {
			writeTestResults("Verify user can enter Inbound Shipment Doc Number",
					"User should be able to enter Inbound Shipment Doc Number",
					"User successfully enter Inbound Shipment Doc Number", "pass");
		} else {
			writeTestResults("Verify user can enter Inbound Shipment Doc Number",
					"User should be able to enter Inbound Shipment Doc Number",
					"User couldn't enter Inbound Shipment Doc Number", "fail");
		}

		sendKeys(txt_searchBarcodeGeneratorPage, inbound_shipment_doc);

		click(btn_docSaecrhBarcodeGeneratorPage);

		if (isDisplayed(btn_barcodeGenerateBarcodeGeneratorPage)) {
			writeTestResults("Verify product details were loaded in the grid",
					"Product details should be loaded in the grid", "Product details successfully loaded in the grid",
					"pass");
		} else {
			writeTestResults("Verify product details were loaded in the grid",
					"Product details should be loaded in the grid", "Product details doesn't loaded in the grid",
					"fail");
		}
		click(btn_barcodeGenerateBarcodeGeneratorPage);

		if (isDisplayed(dropdown_batchBookGenerateBarcodeGeneratorPage)) {
			writeTestResults("Verify barcode configurations were loaded", "Barcode configuration should be loaded",
					"Barcode configuration successfully loaded", "pass");
		} else {
			writeTestResults("Verify barcode configurations were loaded", "Barcode configuration should be loaded",
					"Barcode configuration doesn't loaded", "fail");
		}

		if (isDisplayed(dropdown_batchBookGenerateBarcodeGeneratorPage)) {
			writeTestResults("Verify user can select seriel book", "User should be able to select seriel book",
					"User successfully select seriel book", "pass");
		} else {
			writeTestResults("Verify user can select seriel book", "User should be able to select seriel book",
					"User couldn't select seriel book", "fail");
		}

		selectText(dropdown_batchBookGenerateBarcodeGeneratorPage, "BG");

		if (isDisplayed(txt_lotGenerateBarcodeGeneratorPage)) {
			writeTestResults("Verify user can enter valid lot number", "User should be able to enter valid lot number",
					"User successfully enter valid lot number", "pass");
		} else {
			writeTestResults("Verify user can enter valid lot number", "User should be able to enter valid lot number",
					"User couldn't enter valid lot number", "fail");
		}

		sendKeys(txt_lotGenerateBarcodeGeneratorPage, currentTimeAndDate());

		click(btn_generateGenerateWindowBarcodeGeneratorPage);

		if (isDisplayed(btn_updateGenerateWindowBarcodeGeneratorPage)) {
			writeTestResults("Verify generated serials were loaded in the grid with relevant details",
					"Generated serials should be loaded in the grid with relevant details",
					"Generated serials successfully loaded in the grid with relevant details", "pass");
		} else {
			writeTestResults("Verify generated serials were loaded in the grid with relevant details",
					"Generated serials should be loaded in the grid with relevant details",
					"Generated serials doesn't loaded in the grid with relevant details", "fail");
		}

		click(btn_updateGenerateWindowBarcodeGeneratorPage);
		changeCSS(btn_dismissError, "z-index:-9999");
		if (isDisplayed(btn_viewBarcodeGenarate)) {
			writeTestResults("Verify data saved message was pop-up and Generate button was changed as \"View\"",
					"Data saved successfully message should be pop-up and Generate button should be changed as \"View\"",
					"Data saved successfully message was pop-up and Generate button successfully changed as \"View\"",
					"pass");
		} else {
			writeTestResults("Verify data saved message was pop-up and Generate button should be changed as \"View\"",
					"Data saved successfully message should be pop-up and Generate button should be changed as \"View\"",
					"Data saved successfully message doesn't pop-up and Generate button doesn't changed as \"View\"",
					"fail");
		}

		click(btn_viewBarcodeGenarate);

		if (isDisplayed(btn_printBarcodeGenarate)) {
			writeTestResults("Verify generated serials were displayed in grid",
					"Generated serials should be displayed in grid", "Generated serials successfully displayed in grid",
					"pass");
		} else {
			writeTestResults("Verify generated serials were displayed in grid",
					"Generated serials should be displayed in grid", "Generated serials doesn't displayed in grid",
					"fail");
		}

		switchWindow();
		pageRefersh();
		String[] product_availability_after = new String[7];

		sleepCusomized(btn_availabilityCheckWidgetAfterrelesed.replace("tblStockAdj", "tblPurchaseOrder")
				.replace("row", "1").replace("5", "6"));
		click(btn_availabilityCheckWidgetAfterrelesed.replace("tblStockAdj", "tblPurchaseOrder").replace("row", "1")
				.replace("5", "6"));
		click(btn_avilabilytCheckSecondMenu);
		customizeLoadingDelay(lbl_productAvailability019, 25);
		product_availability_after[0] = getText(lbl_productAvailability019);
		System.out.println(product_availability_after[0]);
		click(btn_closeAvailabilityCheckPopup);

		Thread.sleep(2000);
		boolean warehouse_check_flag = false;
		String qty = product_availability[0].replaceAll(",", "");
		int qty_int = Integer.parseInt(qty);
		String after_qty = product_availability_after[0].replaceAll(",", "");
		try {
			int after_qty_int = Integer.parseInt(after_qty);
			int adding_qty = Integer.parseInt(qtyPurchaseORder);
			if (after_qty_int == (qty_int + adding_qty)) {
				warehouse_check_flag = true;
			} else {
				warehouse_check_flag = false;
			}
		} catch (Exception e) {
			System.out.println(e);
		}

		if (warehouse_check_flag) {
			writeTestResults("Verify stock was transfered to the destination",
					"Stock should be transfered to the destination", "Stock successfully transfered to the destination",
					"pass");
		} else {
			writeTestResults("Verify stock was transfered to the destination",
					"Stock should be transfered to the destination", "Stock doesn't transfered to the destination",
					"fail");
		}
		removeShipmentAcceptance();
	}

	// IW_TC_020
	public void navigateToSerielNumberViewer() throws Exception {
		navigateInventoryAndWarehouseModule();

		if (isDisplayed(btn_serialNumberViewer)) {
			click(btn_serialNumberViewer);
			if (isDisplayed(btn_productSearchSerialNumberViewer)) {

				writeTestResults("Verify user can navigate to Seriel Number Viewer page",
						"User shoud be navigate to the Seriel Number Viewer page",
						"User sucessfully navigate to Seriel Number Viewer page", "pass");
			} else {
				writeTestResults("Verify user can navigate to Seriel Number Viewer page",
						"User shoud be navigate to the Seriel Number Viewer page",
						"User does'nt navigate to Seriel Number Viewer page", "fail");
			}
		}
	}

	public void searchForm() throws Exception {

		click(btn_productSearchSerialNumberViewer);

		sendKeys(txt_productSearch, productSerialNumberViewer);
		pressEnter(txt_productSearch);
		Thread.sleep(6000);
		doubleClick(btn_resultProduct);

		selectText(drop_warehouseSerialBacthViewer, fromWareTO);

		click(checkbox_inStockProductSerielBatchViewer);

	}

	public void searchResultAvailability() throws Exception {
		Thread.sleep(5000);
		explicitWaitUntillClickable(btn_serachProductSerielBatchViewer, 30);
		click(btn_serachProductSerielBatchViewer);
		InventoryAndWarehouseModuleRegression obj = new InventoryAndWarehouseModuleRegression();
		obj.customizeLoadingDelayAndContinue(header_resultCountSerielBatchViewer, 180);
		if (isDisplayed(header_resultCountSerielBatchViewer)) {
			writeTestResults("Verify search result availability", "User shoud be see the result serial numbers",
					"User sucessfully see the result serials", "pass");
		} else {
			writeTestResults("Verify search result availability", "User shoud be see the result serial numbers",
					"User does'nt see the result serials", "fail");
		}
	}

	// IW_TC_021
	public void serialEditWindowLoad() throws Exception {
		navigateToSerielNumberViewer();

		click(btn_productSearchSerialNumberViewer);
		sendKeys(txt_productSearch, serielChangingProduct);
		pressEnter(txt_productSearch);
		Thread.sleep(3000);
		sleepCusomized(btn_resultProduct);
		doubleClick(btn_resultProduct);
		selectText(drop_warehouseSerialBacthViewer, warehouseChangedSerielOutbound);
		click(checkbox_inStockProductSerielBatchViewer);

		searchResultAvailability();
		sleepCusomized(btn_editSearial);

		Thread.sleep(4000);
		click(btn_editSearial);
		if (isDisplayed(txt_searielNumberEditTo)) {

			writeTestResults("Verify that edit window is available", "User shoud be open to edit window",
					"User sucessfully open edit window", "pass");
		} else {
			writeTestResults("Verify that edit window is available", "User shoud be open to edit window",
					"User does'nt open edit window", "fail");
		}
	}

	public void changeSerial() throws Exception {
		changed_seriel = currentTimeAndDate().replaceAll("/", "").replaceAll(":", "");
		sendKeys(txt_searielNumberEditTo, changed_seriel);

		click(btn_changeEditedSeriel);

		if (isDisplayed(btn_productSearchSerialNumberViewer)) {

			writeTestResults("Verify that user sucessfully edit serial number",
					"User shoud be able to edit serial number", "User sucessfully edit serial number", "pass");
		} else {
			writeTestResults("Verify that user sucessfully edit serial number",
					"User shoud be able to edit serial number", "User sucessfully edit serial number", "fail");
		}
	}

	public void completeIW_TC_013WithChangegSearial() throws Exception {
		Thread.sleep(6000);
		sleepCusomized(navigation_pane);
		click(navigation_pane);
		sleepCusomized(inventory_module);
		if (isDisplayed(inventory_module)) {
			writeTestResults("Verify user can navigate to navigation menu and able to view authorized modules ",
					"user should be able to navigate to navigation menu and able to view authorized modules",
					"User successfully navigate to navigation menu and able to view authorized modules", "pass");
		} else {
			writeTestResults("Verify user can navigate to navigation menu and able to view authorized modules ",
					"user should be able to navigate to navigation menu and able to view authorized modules",
					"User couldn't navigate to navigation menu and able to view authorized modules", "fail");
		}

		click(inventory_module);
		if (isDisplayed(lnk_product_group_configuration)) {
			writeTestResults("Verify user can load Inventory module successfully",
					"user should be able load Inventory module successfully",
					"User successfully load Inventory module successfully", "pass");
		} else {
			writeTestResults("Verify user can load Inventory module successfully",
					"user should be able load Inventory module successfully",
					"User couldn't load Inventory module successfully", "fail");
		}
		click(btn_outboundLoanOrder);

		if (isDisplayed(btn_newOutboundLoanOrder)) {

			writeTestResults("Verify user can navigate to Outbound Loan Order page",
					"User should be navigate to Outbound Loan Order page",
					"User sucessfully navigate to Outbound Loan Order page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Outbound Loan Order page",
					"User should be navigate to Outbound Loan Order page",
					"User does'nt navigate to Outbound Loan Order page", "fail");
		}

		click(btn_newOutboundLoanOrder);
		explicitWait(btn_customerAccountSearchLookupILO, 40);
		if (isDisplayed(btn_customerAccountSearchLookupILO)) {

			writeTestResults("Verify user can navigate to Outbound Loan Order form",
					"User should be navigate to Outbound Loan Order form",
					"User sucessfully navigate to Outbound Loan Order form", "pass");
		} else {
			writeTestResults("Verify user can navigate to Outbound Loan Order form",
					"User should be navigate to Outbound Loan Order form",
					"User does'nt navigate to Outbound Loan Order form", "fail");
		}

		click(btn_customerAccountSearchLookupILO);

		sendKeys(txt_accountSearchILO, customerAccountILO);

		pressEnter(txt_accountSearchILO);
		Thread.sleep(3000);
		doubleClick(lnk_resultCustomerAccount);

		click(btn_billingAddressILO);
		sendKeys(txt_billingAddressILO, address1ILO);
		sendKeys(txt_shippingAddressILO, address2ILO);
		click(btn_applyAddressILO);
		selectIndex(dropdown_salesUnitILO, 3);

		click(btn_productLookup13OLO);

		// btn_productLookupSearchILO
		sendKeys(txt_productSearch, serielChangingProduct);
		pressEnter(txt_productSearch);
		Thread.sleep(3000);
		sleepCusomized(btn_resultProduct);
		doubleClick(btn_resultProduct);
		sleepCusomized(dropdown_warehouse1ILO);
		selectText(dropdown_warehouse1ILO, warehouseChangedSerielOutbound);
		sendKeys(txt_quantity1ILO, outboundQuantityChangedSeriel);

		click(btn_detailsTabILO);

		click(chkbox_underDeliveryILO);
		click(chkbox_partielDeliveryILO);

		click(btn_reqSlipDateILO);
		click(btn_reqSlipDate17ILO);

		click(btn_conSlipDateILO);
		click(btn_conSlipDate18ILO);

		click(btn_reqReciptDateILO);
		click(btn_reqReciptDate19ILO);

		click(btn_conReciptDateILO);
		click(btn_conReciptDate20ILO);

		draft("Outbound Loan Order");
		Thread.sleep(3000);
		trackCode = getText(lbl_docNo);
		click(btn_releseILO);
		implisitWait(10);
		Thread.sleep(2000);
		if (isDisplayed(btn_goToNextTaskILO)) {

			writeTestResults("Verify user can relese Outbound Loan Order",
					"User should be able to relese Outbound Loan Order", "User sucessfully relese Outbound Loan Order",
					"pass");
		} else {
			writeTestResults("Verify user can relese Outbound Loan Order",
					"User should be able to relese Outbound Loan Order",
					"User does'nt able to releese Outbound Loan Order", "fail");
		}

		Thread.sleep(4000);
		click(btn_goToNextTaskILO);
		switchWindow();
		Thread.sleep(1000);
		explicitWaitUntillClickable(btn_draft, 30);
		click(btn_draft);
		explicitWaitUntillClickable(btn_relese, 40);
		if (isDisplayed(btn_relese)) {

			writeTestResults("Verify user can br draft Outbound Shipment",
					"User should be able to draft Outbound Shipment", "User sucessfully draft Outbound Shipment",
					"pass");
		} else {
			writeTestResults("Verify user can br draft Outbound Shipment",
					"User should be able to draft Outbound Shipment", "User not draft Outbound Shipment", "Fail");
		}

		sleepCusomized(brn_serielBatchCapture);
		click(brn_serielBatchCapture);
		if (isDisplayed(header_searielBatchCapturePage)) {

			writeTestResults("Verify user can navigate to 'Seriel/Batch Capture Page'",
					"User should be able to navigate to 'Seriel/Batch Capture Page'",
					"User sucessfully navigate to 'Seriel/Batch Capture Page'", "pass");
		} else {
			writeTestResults("Verify user can navigate to 'Seriel/Batch Capture Page'",
					"User should be able to navigate to 'Seriel/Batch Capture Page'",
					"User couldn't navigate to 'Seriel/Batch Capture Page'", "fail");
		}

		click(btn_serielBatchCaptureProduct1ILOOutboundShipment);
		if (isDisplayed(btn_serielBatchCaptureProduct1ILOOutboundShipment)) {
			writeTestResults("Verify serial capturing details are loaded", "Serial capturing details should be loaded",
					"Serial capturing details are sucessfully loaded", "pass");
		} else {
			writeTestResults("Verify serial capturing details are loaded", "Serial capturing details should be loaded",
					"Serial capturing details doesn't loaded", "fail");
		}

		sendKeys(txt_serialNumberCaptureILOOutboundShipment, changed_seriel);
		pressEnter(txt_serialNumberCaptureILOOutboundShipment);

		if (isDisplayed(txt_serialNumberCaptureILOOutboundShipment)) {

			writeTestResults("Verify user can enter valid Serial No, Lot No and Expiry Date",
					"User should be able to enter valid Serial No, Lot No and Expiry Date",
					"User sucessfully enter valid Serial No, Lot No and Expiry Date", "pass");
		} else {
			writeTestResults("Verify user can enter valid Serial No, Lot No and Expiry Date",
					"User should be able to enter valid Serial No, Lot No and Expiry Date",
					"User couldn't enter valid Serial No, Lot No and Expiry Date", "fail");
		}
		Thread.sleep(1000);
		click(btn_updateCapture);

		Thread.sleep(2000);
		click(btn_productCaptureBackButton);
		sleepCusomized(btn_releese2);
		click(btn_releese2);
		sleepCusomized(header_outboundShipment);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_outboundShipment)) {

			writeTestResults("Verify user can be relese Outbound Shipment",
					"User should be able to relese above document", "User sucessfully relese Outbound Shipment",
					"pass");
		} else {
			writeTestResults("Verify user can br relese Outbound Shipment",
					"User should be able to relese Outbound Shipment", "User not relese Outbound Shipment", "Fail");
		}
	}

	// IW_TC_022
	public void setQcAcceptance() throws Exception {

		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		waitImplicit(navigation_pane, 60);
		click(navigation_pane);
		click(btn_adminModule);

		click(btn_balancingLevelAdminModule);
		click(btn_genarelBalancingLevelSetting);
		customizeLoadingDelay(chk_qcAcceptGeneral, 10);
		if (!isSelected(chk_qcAcceptGeneral)) {
			click(chk_qcAcceptGeneral);
		}
		click(btn_updateGenaralSectionBalancingLevel);
		Thread.sleep(3000);
		pageRefersh();
		customizeLoadingDelay(btn_deleestShipmentAceptance, 20);
		System.out.println("Wait For Removing Shipment Acceptance........");
		for (int i = 0; i < 12; i++) {
			click(btn_deleestShipmentAceptance);
		}

		selectText(dropdown_QCAcceptanaceJourneyAdminModule, selectionQCQcceptancePurchaseOrderInboundShipment);
		click(btn_updateBalancingLevelQCAcceptance);
		Thread.sleep(2000);
		pageRefersh();

	}

	public void createProductAndCompletePurchaseOrder() throws Exception {

		System.out.println("Creating QC Accept Product..........");
		createProductQCAcceptance();

		/*-----------Purchase Order------------*/
		System.out.println("Creating Purchase Order...........");
		click(navigation_pane);

		click(btn_procumentModule);

		customizeLoadingDelay(btn_purchaseOrder, 10);
		click(btn_purchaseOrder);

		click(btn_newPrurchaseOrder);

		click(btn_purchaseOrderToInboundShipmentToPurchaseInvoiceJourney);

		click(btn_vendorSearchPurchaseOredr);

		sendKeysLookup(txt_vendorSearch23, vendor);
		pressEnter(txt_vendorSearch23);
		Thread.sleep(2000);
		explicitWait(lnk_infoOnLoolkupInfoReplace.replace("info", vendor), 30);

		doubleClick(lnk_infoOnLoolkupInfoReplace.replace("info", vendor));

		explicitWait(btn_serachProductLookup23, 30);
		click(btn_serachProductLookup23);

		customizeLoadingDelay(txt_productSearch, 10);

		sendKeys(txt_productSearch, readIWData(6));

		pressEnter(txt_productSearch);

		Thread.sleep(3000);
		customizeLoadingDelay(lnk_resultProductPurchaseOrder, 10);

		doubleClick(lnk_resultProductPurchaseOrder);

		selectText(drop_warehousePurchaseOrder, warehousePurchaseOrder);

		sendKeys(txt_qtyProductPurchaseOrder, qtyPurchaseORder);

		sendKeys(txt_unitPricePurchaseOrder, unitPricePurchaseOrder);

		click(btn_checkoutPurchaaseOrdeer);

		click(btn_draft2);

		sleepCusomized(btn_releese2);
		Thread.sleep(2000);
		click(btn_releese2);
		implisitWait(10);
		Thread.sleep(2000);
		if (isDisplayed(btn_goToPageLink)) {

			writeTestResults("Verify user can releese Purchase Order", "User should be abe to releese Purchase Order",
					"User sucessfully releese Purchase Order", "pass");
		} else {
			writeTestResults("Verify user can releese Purchase Order", "User should be abe to releese Purchase Order",
					"User couldn't releese Purchase Order", "fail");
		}
		click(btn_goToPageLink);
		switchWindow();
		Thread.sleep(1000);
		customizeLoadingDelay(btn_checkout2, 15);
		Thread.sleep(1500);
		click(btn_checkout2);

		click(btn_draft2);
		customizeLoadingDelay(brn_serielBatchCapture, 15);
		Thread.sleep(2000);
		click(brn_serielBatchCapture);

		click(btn_serielBatchCaptureProduct1ILOOutboundShipment);

		sendKeys(txt_batchNoProductCapture022, genProductId());
		sendKeys(txt_lotNoCapturePageStockTake, genProductId());
		pressEnter(txt_lotNoCapturePageStockTake);

		Thread.sleep(4000);

		click(btn_updateCapture);

		pageRefersh();
		sleepCusomized(btn_releese2);
		Thread.sleep(2000);
		inbound_shipment_qc_acceptance = getText(lbl_inboundShipmentDocNoShipmentAcceptance);
		click(btn_releese2);
		Thread.sleep(4000);
		if (isDisplayed(btn_goToNextTaskILO)) {

			writeTestResults("Verify user can releese Inbound Shipment",
					"User should be abe to releese Inbound Shipment", "User sucessfully releese Inbound Shipment",
					"pass");
		} else {
			writeTestResults("Verify user can releese Inbound Shipment",
					"User should be abe to releese Inbound Shipment", "User couldn't releese Inbound Shipment", "fail");
		}

		sleepCusomized(btn_goToNextTaskILO);
		click(btn_goToNextTaskILO);

		switchWindow();
		sleepCusomized(btn_checkout);
		click(btn_checkout);

		sleepCusomized(btn_draft);
		click(btn_draft);

		sleepCusomized(btn_releese2);
		Thread.sleep(2000);
		click(btn_releese2);

	}

	public void qcAcceptanceProcessSet() throws Exception {
		Thread.sleep(4000);
		pageRefersh();
		customizeLoadingDelay(navigation_pane, 7);
		click(navigation_pane);
		customizeLoadingDelay(inventory_module, 7);
		click(inventory_module);
		customizeLoadingDelay(btn_qcAcceptance, 7);
		click(btn_qcAcceptance);
		sleepCusomized(header_QCAcceptancePage);
		if (isDisplayed(header_QCAcceptancePage)) {

			writeTestResults("Verify that user sucessfully navigate to the QC Acceptance page",
					"User shoud be able to navigate to the QC Acceptance page",
					"User sucessfully navigate to the QC Acceptance page", "pass");
		} else {
			writeTestResults("Verify that user sucessfully navigate to the QC Acceptance page",
					"User shoud be able to navigate to the QC Acceptance page",
					"User couldn't navigate to the QC Acceptance page", "fail");
		}

		selectText(drpdown_selectionQCAcceptance, selectionInboundShipment);
		if (getText(drpdown_selectionQCAcceptance).equals(selectionInboundShipment)) {

			writeTestResults("Verify that user can select Outbound Shipment option from dropdown",
					"User shoud be able to select Outbound Shipment option from dropdown",
					"User sucessfully select Outbound Shipment option from dropdown", "pass");
		} else {
			writeTestResults("Verify that user can select Outbound Shipment option from dropdown",
					"User shoud be able to select Outbound Shipment option from dropdown",
					"User sucessfully select Outbound Shipment option from dropdown", "pass");

		}
		waitImplicit(btn_lastDocInboundSelectionQCAcceptance, 3);

		sendKeys(txt_docSearchQCAcceptance, inbound_shipment_qc_acceptance);
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_docSearchQCAcceptance)).sendKeys(Keys.ARROW_DOWN);
		pressEnter(txt_docSearchQCAcceptance);
		int rowCount = driver.findElements(By.xpath(tbl_qcAcceptanceProduct)).size();
		if (rowCount >= 1) {
			writeTestResults("Verify that QC Acceptamce products were loaded",
					"QC Acceptamce products should be loaded", "QC Acceptamce products successfully loaded", "pass");
		} else {
			writeTestResults("Verify that QC Acceptamce products were loaded",
					"QC Acceptamce products should be loaded", "QC Acceptamce products were not loaded", "fail");
		}

	}

	public void checkQualityAndPassFail() throws Exception {
		Thread.sleep(3000);
		click(btn_serielNosQCAcceptance);
		if (isDisplayed(header_QCAcceptanceForm)) {
			writeTestResults("Verify Seriel Batch No window is popup", "Seriel Batch No window should be popup",
					"Seriel Batch No window popup sucessfully", "pass");
		} else {
			writeTestResults("Verify Seriel Batch No window is popup", "Seriel Batch No window should be popup",
					"Seriel Batch No window does'nt popup", "fail");
		}

		sendKeys(txt_passQtyQCAcceptance, passQtyQCAcceptance);
		sendKeys(txt_failQtyQCAcceptance, failQtyQCAcceptance);
		if (isDisplayed(txt_passQtyQCAcceptance) && isDisplayed(txt_failQtyQCAcceptance)) {
			writeTestResults("Verify user can set pass and fail quantity",
					"User should be able to set pass and fail quantity", "User successfully set pass ad fail quantity",
					"pass");
		} else {
			writeTestResults("Verify user can set pass and fail quantity",
					"User should be able to set pass and fail quantity", "User couldn't set pass ad fail quantity",
					"fail");

		}

		sendKeys(txt_reasonQCAcceptance, reasonFailQuantityQcAcceptance);

		if (isDisplayed(txt_reasonQCAcceptance)) {
			writeTestResults("Verify user can set reason for fail quantity",
					"User should be able to set reason for fail quantity",
					"User successfully set reason for fail quantity", "pass");
		} else {
			writeTestResults("Verify user can set reason for fail quantity",
					"User should be able to set reason for fail quantity", "User couldn't set reason for fail quantity",
					"fail");
		}

		click(btn_releese2);
	}

	public void checkProductStockAvailability() throws Exception {
		sleepCusomized(txt_docSearchQCAcceptance);
		if (isDisplayed(txt_docSearchQCAcceptance)) {
			writeTestResults("Verify user can navigated to QC window", "User should be able to navigated to QC window",
					"User successfully navigated to QC window", "pass");
		} else {
			writeTestResults("Verify user can navigated to QC window", "User should be able to navigated to QC window",
					"User couldn't navigated to QC window", "fail");
		}

		closeWindow();
		switchWindow();
		switchWindow();
		sleepCusomized(btn_closeGoToLinkPopup);
		click(btn_closeGoToLinkPopup);
		sleepCusomized(header_inboundShipmemtQC);
		if (isDisplayed(header_inboundShipmemtQC)) {
			writeTestResults("Verify user can navigate to Inbound Shipment form",
					"User should be able to navigate to Inbound Shipment form",
					"User successfully navigate to Inbound Shipment form", "pass");
		} else {
			writeTestResults("Verify user can navigate to Inbound Shipment form",
					"User should be able to navigate to Inbound Shipment form",
					"User couldn't navigate to Inbound Shipment form", "fail");
		}

		click(widget_checkAvailabilityProductQC);
		if (isDisplayed(btn_productAvailabilityQC)) {
			writeTestResults("Verify user can view widget options", "User should be able to view widget options",
					"User successfully view widget options", "pass");
		} else {
			writeTestResults("Verify user can view widget options", "User should be able to view widget options",
					"User couldn't view widget options", "fail");
		}

		click(btn_productAvailabilityQC);
		String header = getText(header_productAvailabilityPopup);
		if (header.equals("Product Availability")) {
			writeTestResults("Verify user can view product availability window pop-up",
					"User should be able to view product availability window pop-up",
					"User successfully view product availability window pop-up", "pass");
		} else {
			writeTestResults("Verify user can view product availability window pop-up",
					"User should be able to view product availability window pop-up",
					"User couldn't view product availability window pop-up", "fail");
		}

		Thread.sleep(5000);
		explicitWait(td_productAvailableQuantityQC, 40);
		Thread.sleep(2000);
		String availabilty = getText(td_productAvailableQuantityQC);
		if (availabilty.equals(passQtyQCAcceptance)) {
			writeTestResults("Verify QC Accepted product count is loaded to the warehouse",
					"QC Accepted product count should be loaded to the warehouse",
					"QC Accepted product count successfully loaded to the warehouse", "pass");
		} else {
			writeTestResults("Verify QC Accepted product count is loaded to the warehouse",
					"QC Accepted product count should be loaded to the warehouse",
					"QC Accepted product count doesn't loaded to the warehouse", "fail");
		}

		pageRefersh();
		customizeLoadingDelay(navigation_pane, 7);
		click(navigation_pane);
		customizeLoadingDelay(btn_adminModule, 7);
		click(btn_adminModule);
		customizeLoadingDelay(btn_balancingLevelAdminModule, 7);
		click(btn_balancingLevelAdminModule);

		customizeLoadingDelay(btn_deleestShipmentAceptance, 20);
		System.out.println("Wait For Removing Shipment Acceptance........");
		for (int i = 0; i < 12; i++) {
			click(btn_deleestShipmentAceptance);
		}
		click(btn_updateBalancingLevelQCAcceptance);
		pageRefersh();
		customizeLoadingDelay(btn_genarelBalancingLevelSetting, 20);
		click(btn_genarelBalancingLevelSetting);
		customizeLoadingDelay(chk_qcAcceptGeneral, 10);
		if (isSelected(chk_qcAcceptGeneral)) {
			click(chk_qcAcceptGeneral);
		}
		click(btn_updateGenaralSectionBalancingLevel);
		Thread.sleep(3000);
		pageRefersh();
	}

	// IW_TC_023
	public void cofigureShipmentAcceptance() throws Exception {

		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		waitImplicit(navigation_pane, 60);
		click(navigation_pane);
		click(btn_adminModule);

		click(btn_balancingLevelAdminModule);
		click(btn_genarelBalancingLevelSetting);
		customizeLoadingDelay(chk_shipemtAceptanceGeneral, 10);
		if (!isSelected(chk_shipemtAceptanceGeneral)) {
			click(chk_shipemtAceptanceGeneral);
		}
		click(btn_updateGenaralSectionBalancingLevel);
		Thread.sleep(3000);
		pageRefersh();
		customizeLoadingDelay(drop_inboundShipmentAsseptance, 20);
		selectText(drop_inboundShipmentAsseptance, shipmentAcceptanceJourney);
		click(btn_updateBalancingLevelQCAcceptance);
		pageRefersh();

		if (isDisplayed(btn_updateBalancingLevelQCAcceptance)) {

			writeTestResults("Verify user can configure Shipment Acceptance",
					"User should be abe to configure Shipment Acceptanec",
					"User sucessfully configure Shipment Acceptanec", "pass");
		} else {
			writeTestResults("Verify user can configure Shipment Acceptance",
					"User should be abe to configure Shipment Acceptanec",
					"User couldn't configure Shipment Acceptanec", "fail");
		}

	}

	public void purchaseOrderCommonForShipmentAcceptance() throws Exception {
		customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);

		click(btn_procumentModule);

		customizeLoadingDelay(btn_purchaseOrder, 10);
		click(btn_purchaseOrder);

		click(btn_newPrurchaseOrder);

		click(btn_purchaseOrderToInboundShipmentToPurchaseInvoiceJourney);

		explicitWait(btn_vendorSearchPurchaseOredr, 40);
		click(btn_vendorSearchPurchaseOredr);

		sendKeysLookup(txt_vendorSearch23, vendor);
		pressEnter(txt_vendorSearch23);
		Thread.sleep(2000);
		explicitWait(lnk_infoOnLoolkupInfoReplace.replace("info", vendor), 30);

		doubleClick(lnk_infoOnLoolkupInfoReplace.replace("info", vendor));

		explicitWait(btn_serachProductLookup23, 10);
		click(btn_serachProductLookup23);

		customizeLoadingDelay(txt_productSearch, 10);
		Thread.sleep(3000);

		sendKeys(txt_productSearch, purchsaeOrderProduct);

		pressEnter(txt_productSearch);

		Thread.sleep(2500);

		customizeLoadingDelay(lnk_resultProductPurchaseOrder, 10);
		Thread.sleep(2000);
		doubleClick(lnk_resultProductPurchaseOrder);

		selectText(drop_warehousePurchaseOrder, warehousePurchaseOrder);

		sendKeys(txt_qtyProductPurchaseOrder, qtyPurchaseORder);

		sendKeys(txt_unitPricePurchaseOrder, unitPricePurchaseOrder);

		click(btn_checkoutPurchaaseOrdeer);

		click(btn_avilabilityCheckWidget);

		sleepCusomized(btn_avilabilytCheckSecondMenu);
		click(btn_avilabilytCheckSecondMenu);

		try {
			product_availability[0] = driver.findElement(By.xpath(lbl_productAvailability019)).getText().trim();
		} catch (Exception e) {
			System.out.println("0");
		}

		if (product_availability[0] != null) {
			System.out.println(product_availability[0]);
		} else {
			product_availability[0] = ",0";
		}
		sleepCusomized(btn_closeAvailabilityCheckPopup);
		click(btn_closeAvailabilityCheckPopup);
		click(btn_draft2);

		Thread.sleep(3000);
		click(btn_releese2);
		sleepCusomized(btn_goToPageLink);
		if (isDisplayed(btn_goToPageLink)) {
			writeTestResults("Verify user can releese Purchase Order", "User should be abe to releese Purchase Order",
					"User sucessfully releese Purchase Order", "pass");
		} else {
			writeTestResults("Verify user can releese Purchase Order", "User should be abe to releese Purchase Order",
					"User couldn't releese Purchase Order", "fail");
		}
		click(btn_goToPageLink);
		switchWindow();
		explicitWait(btn_checkout2, 40);
		click(btn_checkout2);

		click(btn_draft2);

		Thread.sleep(4000);
		click(btn_releese2);
		customizeLoadingDelay(lbl_releseForPickingInboundShipment, 80);
		String status = getText(lbl_releseForPickingInboundShipment);
		if (status.equals("(Released For Picking)")) {
			writeTestResults("Verify user can releese Inbound Shipment",
					"User should be abe to releese Inbound Shipment", "User sucessfully releese Inbound Shipment",
					"pass");
		} else {
			writeTestResults("Verify user can releese Inbound Shipment",
					"User should be abe to releese Inbound Shipment", "User couldn't releese Inbound Shipment", "fail");
		}

		inbound_shipment_shipment_acceptance = getText(lbl_inboundShipmentDocNoShipmentAcceptance);

	}

	public void shipmentAcceptance() throws Exception {
		sleepCusomized(navigation_pane);
		click(navigation_pane);
		sleepCusomized(inventory_module);
		click(inventory_module);

		click(btn_shipmentAsseptance);
		if (isDisplayed(drop_documentTypeShipmentAcceptance)) {
			writeTestResults("Verify user can navigated to the Shipment Acceptance Form",
					"User should be navigated to Shipment Acceptance Form",
					"User sucessfully navigated to the Shipment Acceptance Form", "pass");
		} else {
			writeTestResults("Verify user can navigated to the Shipment Acceptance Form",
					"User should be navigated to Shipment Acceptance Form",
					"User couldn't navigated to the Shipment Acceptance Form", "fail");
		}

		customizeLoadingDelay(drop_documentTypeShipmentAcceptance, 30);
		selectText(drop_documentTypeShipmentAcceptance, documentTypeShipmentAceptance);

		Thread.sleep(3000);
		customizeLoadingDelay(drop_documentTypeShipmentAcceptance, 30);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_documentTypeShipmentAcceptance)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		if (selectedComboValue.equals(documentTypeShipmentAceptance)) {

			writeTestResults("Verify user can slect the Inbound Shipment option in dropdown",
					"User should be able to slect the Inbound Shipment option in dropdown",
					"User sucessfully slect the Inbound Shipment option in dropdown", "pass");
		} else {
			writeTestResults("Verify user can slect the Inbound Shipment option in dropdown",
					"User should be able to slect the Inbound Shipment option in dropdown",
					"User couldn't slect the Inbound Shipment option in dropdown", "fail");
		}

		Thread.sleep(4000);
		customizeLoadingDelay(txt_docNoInboundShipmentShipmentAcceptance, 30);
		handeledSendKeys(txt_docNoInboundShipmentShipmentAcceptance, inbound_shipment_shipment_acceptance);
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_docNoInboundShipmentShipmentAcceptance)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(1000);
		pressEnter(txt_docNoInboundShipmentShipmentAcceptance);
		Thread.sleep(5000);
		if (isDisplayed(btn_serielNosShipmentAcceptance)) {

			writeTestResults("Verify products which required to do Shipment Acceptance should be loaded",
					"Products which required to do Shipment Acceptance should be loaded",
					"Products which required to do Shipment Acceptance successfully loaded", "pass");
		} else {
			writeTestResults("Verify products which required to do Shipment Acceptance should be loaded",
					"Products which required to do Shipment Acceptance should be loaded",
					"Products which required to do Shipment Acceptance doesn't loaded", "fail");
		}

		explicitWait(div_productShipmentAcceptanceOnShipmentAcceptanceGrid, 20);
		click(btn_serielNosShipmentAcceptance);

		Thread.sleep(3700);
		if (isDisplayed(chk_rangeCaptureAShipmentAsseptance)) {

			writeTestResults("Verify Serial Batch Nos window pop-up", "Serial Batch Nos window should be pop-up",
					"Serial Batch Nos window successfully pop-up", "pass");
		} else {
			writeTestResults("Verify Serial Batch Nos window pop-up", "Serial Batch Nos window should be pop-up",
					"Serial Batch Nos window doesn't pop-up", "fail");
		}

		click(chk_rangeCaptureAShipmentAsseptance);
		sendKeys(txt_rangecountShipmentAcceptance, qtyPurchaseORder);
		if (isSelected(chk_rangeCaptureAShipmentAsseptance) && isDisplayed(txt_rangecountShipmentAcceptance)) {

			writeTestResults("Verify user can enable range checkbox and add count",
					"User should be able to enable range checkbox and add count",
					"User successfully enable range checkbox and add count", "pass");
		} else {
			writeTestResults("Verify user can enable range checkbox and add count",
					"User should be able to enable range checkbox and add count",
					"User couldn't enable range checkbox and add count", "fail");
		}

		sendKeys(txt_serielNumberStartShipmentAcceptance, serialNumberStartShipmentAcceptance + currentTimeAndDate());
		sendKeys(txt_lotNumberStartShipmentAcceptance, lotNoShipmentAcceptance + currentTimeAndDate());

		if (isDisplayed(txt_serielNumberStartShipmentAcceptance) && isDisplayed(txt_lotNumberStartShipmentAcceptance)) {

			writeTestResults("Verify user can add New Serial Number/Batch Number and Lot Number",
					"User should be able to add New Serial Number/Batch Number and Lot Number",
					"User successfully add New Serial Number/Batch Number and Lot Number", "pass");
		} else {
			writeTestResults("Verify user can add New Serial Number/Batch Number and Lot Number",
					"User should be able to add New Serial Number/Batch Number and Lot Number",
					"User couldn't add New Serial Number/Batch Number and Lot Number", "fail");
		}

		click(plicker_exoiryDateShipmentAcceptance);
		doubleClick(btn_arrowToNExtMotnthShipmentAcceptanse);
		click(btn_date28ShipmentAcceptanse);
		pressEnter(plicker_exoiryDateShipmentAcceptance);

		if (isDisplayed(plicker_exoiryDateShipmentAcceptance)) {
			writeTestResults("Verify user can select Expiary Date", "User should be able to select Expiary Date",
					"User successfully select Expiary Date", "pass");
		} else {
			writeTestResults("Verify user can select Expiary Date", "User should be able to select Expiary Date",
					"User couldn't select Expiary Date", "fail");
		}
		sleepCusomized(btn_applyShipmentAcceptanceCaptureProdut);
		click(btn_applyShipmentAcceptanceCaptureProdut);
		waitImplicit(btn_releseShipmentAcceptance, 5);
		if (isDisplayed(btn_releseShipmentAcceptance)) {
			writeTestResults("Verify user can navigated to Shipment Acceptance Form",
					"User should be navigated to Shipment Acceptance Form",
					"User successfully navigated to Shipment Acceptance Form", "pass");
		} else {
			writeTestResults("Verify user can navigated to Shipment Acceptance Form",
					"User should be navigated to Shipment Acceptance Form",
					"User couldn't navigated to Shipment Acceptance Form", "fail");
		}
		Thread.sleep(4000);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript(chk_releseSelectionShipmentAcceptance);
		if (isSelected(chk_verifyReleeseIsSelected)) {
			writeTestResults("Verify user can enable Release checkbox",
					"User should be able to enable Release checkbox", "User successfully enable Release checkbox",
					"pass");
		} else {
			writeTestResults("Verify user can enable Release checkbox",
					"User should be able to enable Release checkbox", "User couldn't enable Release checkbox", "fail");
		}

		js.executeScript(btn_releseShipmentAcceptanceJS);
		customizeLoadingDelay(lnk_wiewInboundShipmenthipmentAcceptance, 40);
		Thread.sleep(2000);
		click(lnk_wiewInboundShipmenthipmentAcceptance);
		Thread.sleep(4000);
		switchWindow();
		customizeLoadingDelay(lblDocumentStatusAfterShipmentAccewptanceOutboundShipment, 40);
		Thread.sleep(4500);
		System.out.println(getText(lblDocumentStatusAfterShipmentAccewptanceOutboundShipment));
		String captured_outbound_shipment_status = getText(lblDocumentStatusAfterShipmentAccewptanceOutboundShipment);

		if (captured_outbound_shipment_status.equals("(Released)")) {
			writeTestResults("Verify that captured Qty should be released in Inbound Shipment",
					"Captured Qty should be released in Inbound Shipment",
					"Captured Qty Successfully released in Inbound Shipment", "pass");
		} else {
			writeTestResults("Verify that captured Qty should be released in Inbound Shipment",
					"Captured Qty should be released in Inbound Shipment",
					"Captured Qty doesn't released in Inbound Shipment", "fail");
		}

		switchWindow();

		if (isDisplayed(lnk_wiewInboundShipmenthipmentAcceptance)) {
			writeTestResults("Verify user can navigated to Inbound Shipment",
					"User should be navigated to Inbound Shipment", "User successfully navigated to Inbound Shipment",
					"pass");
		} else {
			writeTestResults("Verify user can navigated to Inbound Shipment",
					"User should be navigated to Inbound Shipment", "User vouldn't navigated to Inbound Shipment",
					"fail");
		}

	}

	public void compltePurchaseInvoiseShipmentAcceptance() throws Exception {

		click(btn_goToPurchaseInvoiveInboundShipmet);
		switchWindow();
		if (isDisplayed(btn_draftPurchaseInvoiseShipmentAcceptance)) {
			writeTestResults("Verify user can navigated to Purchase Invoice Form",
					"User should be navigated to Purchase Invoice Form",
					"User successfully navigated to Purchase Invoice Form", "pass");
		} else {
			writeTestResults("Verify user can navigated to Purchase Invoice Form",
					"User should be navigated to Purchase Invoice Form",
					"User could'nt navigated to Purchase Invoice Form", "fail");
		}

		click(btn_checkoutPurchaseInvoiceShipmentAcceptance);
		if (isDisplayed(btn_checkoutPurchaseInvoiceShipmentAcceptance)) {
			writeTestResults("Verify user can checkout Purchase Invoice", "Purchase Invoice should be checkout",
					"Purchase Invoice successfully checkout", "pass");
		} else {
			writeTestResults("Verify user can checkout Purchase Invoice", "Purchase Invoice should be checkout",
					"Purchase Invoice doesn't checkout", "fail");
		}

		click(btn_draftPurchaseInvoiseShipmentAcceptance);
		if (isDisplayed(btn_releesePurchseInvoiceshipmentAcceptance)) {
			writeTestResults("Verify user can draft Purchase Invoice", "Purchase Invoice should be drafted",
					"Purchase Invoice successfully drafted", "pass");
		} else {
			writeTestResults("Verify user can draft Purchase Invoice", "Purchase Invoice should be drafted",
					"Purchase Invoice doesn't drafted", "fail");
		}

		sleepCusomized(btn_releesePurchseInvoiceshipmentAcceptance);
		click(btn_releesePurchseInvoiceshipmentAcceptance);
		Thread.sleep(4000);
		sleepCusomized(header_draft);
		String header = getText(header_draft);

		if (!header.equalsIgnoreCase("(Released)")) {
			Thread.sleep(7000);
			header = getText(header_draft);
		}
		if (header.equalsIgnoreCase("(Released)")) {
			writeTestResults("Verify user can releese Purchase Invoice", "Purchase Invoice should be releesed",
					"Purchase Invoice successfully released", "pass");
		} else {
			writeTestResults("Verify user can releese Purchase Invoice", "Purchase Invoice should be releesed",
					"Purchase Invoice doesn't releesed", "fail");
		}

		String[] product_availability_after = new String[7];

		sleepCusomized(
				btn_availabilityCheckWidgetAfterrelesed.replace("tblStockAdj", "tblPurchaseInv").replace("row", "1"));
		click(btn_availabilityCheckWidgetAfterrelesed.replace("tblStockAdj", "tblPurchaseInv").replace("row", "1"));
		Thread.sleep(2000);
		Actions action1 = new Actions(driver);
		WebElement we1 = driver.findElement(By.xpath(btn_avilabilytCheckSecondMenu));
		action1.moveToElement(we1).build().perform();
		click(btn_avilabilytCheckSecondMenu);
		sleepCusomized(lbl_productAvailability019);
		product_availability_after[0] = getText(lbl_productAvailability019);
		System.out.println(product_availability_after[0]);
		click(btn_closeAvailabilityCheckPopup);

		boolean warehouse_check_flag = false;
		String qty = product_availability[0].replaceAll(",", "");
		int qty_int = Integer.parseInt(qty);
		String after_qty = product_availability_after[0].replaceAll(",", "");
		try {
			int after_qty_int = Integer.parseInt(after_qty);
			int adding_qty = Integer.parseInt(qtyPurchaseORder);
			if (after_qty_int == (qty_int + adding_qty)) {
				warehouse_check_flag = true;
			} else {
				warehouse_check_flag = false;
			}
		} catch (Exception e) {
			System.out.println(e);
		}

		if (warehouse_check_flag) {
			writeTestResults("Verify stock was transfered to the destination",
					"Stock should be transfered to the destination", "Stock successfully transfered to the destination",
					"pass");
		} else {
			writeTestResults("Verify stock was transfered to the destination",
					"Stock should be transfered to the destination", "Stock doesn't transfered to the destination",
					"fail");
		}
		removeShipmentAcceptance();
		closeWindow();
		switchWindow();
		closeWindow();
		switchWindow();
		closeWindow();
	}

	// Common
	public String currentTimeAndDate() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
//		System.out.println(dtf.format(now));
		String curDate = dtf.format(now);
//		System.out.println(curDate);

		curDate = curDate.replaceAll("\\s", "");
//		System.out.println(curDate);
		return curDate;
	}
	
	public String currentTimeAsAMPMAndDate() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd hh:mm:ss");
		LocalDateTime now = LocalDateTime.now();
//		System.out.println(dtf.format(now));
		String curDate = dtf.format(now);
//		System.out.println(curDate);

		curDate = curDate.replaceAll("\\s", "");
//		System.out.println(curDate);
		return curDate;
	}

	// Common
	public void waitImplicit(String element, int seconds) throws Exception {
		driver.manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);
		if (!isDisplayed(element)) {
			System.out.println("Network speed is low...");
		}

	}

	// Common Implicit Wait
	public void implisitWait(int seconds) throws Exception {
		driver.manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);
		Thread.sleep(500);
	}

	// Common
	public void writeIWData(String name, String variable, int data_row) throws InvalidFormatException, IOException {
		genarateInventoryAndWarehouseInformation obj = new genarateInventoryAndWarehouseInformation();
		String path = obj.getProjectPath("*src*test*java*inventoryAndWarehouseModule*IW_DATA.xlsx");
		fis = new FileInputStream(path);
		wb = WorkbookFactory.create(fis);
		sh = wb.getSheet("Sheet1");
		/* int noOfRows = sh.getLastRowNum(); */
		/* System.out.println(noOfRows); */
		row = sh.createRow(data_row);
		cell = row.createCell(1);
		cell1 = row.createCell(0);
		cell.setCellValue(variable);
		cell1.setCellValue(name);
		System.out.println(cell.getStringCellValue());
		fos = new FileOutputStream(path);
		wb.write(fos);
		fos.flush();
		fos.close();
		/* System.out.println("Done"); */
	}

	// Common - Write Test Creations
	public void writeTestCreations(String name, String variable, int data_row)
			throws InvalidFormatException, IOException {
		String path = obj.getProjectPath("*TestCreations*TestCreations.xlsx");
		fis = new FileInputStream(path);
		wb = WorkbookFactory.create(fis);
		sh = wb.getSheet("Sheet1");
		/* int noOfRows = sh.getLastRowNum(); */
		/* System.out.println(noOfRows); */
		row = sh.createRow(data_row);
		cell = row.createCell(1);
		cell1 = row.createCell(0);
		cell.setCellValue(variable);
		cell1.setCellValue(name);
		System.out.println(cell.getStringCellValue());
		fos = new FileOutputStream(path);
		wb.write(fos);
		fos.flush();
		fos.close();
		/* System.out.println("Done"); */
	}

	// Common
	public String readIWData(int data_row) throws IOException, InvalidFormatException {
		genarateInventoryAndWarehouseInformation obj = new genarateInventoryAndWarehouseInformation();
		String path = obj.getProjectPath("*src*test*java*inventoryAndWarehouseModule*IW_DATA.xlsx");
		fis = new FileInputStream(path);
		wb = WorkbookFactory.create(fis);
		sh = wb.getSheet("Sheet1");
		row = sh.getRow(data_row);
		cell = row.getCell(1);

		String employee_readed = cell.getStringCellValue();
		return employee_readed;
	}

	// Common - Test Creation
	public String readTestCreation(String product_category) throws IOException, InvalidFormatException {
		genarateInventoryAndWarehouseInformation obj = new genarateInventoryAndWarehouseInformation();
		String path = obj.getProjectPath("*TestCreations*TestCreations.xlsx");
		fis = new FileInputStream(path);
		wb = WorkbookFactory.create(fis);
		sh = wb.getSheet("Sheet1");
		int totalRowsTestCreation = sh.getLastRowNum();
		Map<String, String> products_creation_map = new HashMap<String, String>();
		List<Map<String, String>> products_creation = new ArrayList<Map<String, String>>();
		String product_type = "505-Not Found";

		for (int i = 0; i <= totalRowsTestCreation;) {
			row = sh.getRow(i);
			cell = row.getCell(0);
			cell2 = row.getCell(1);

			String test_creation_row1 = cell.getStringCellValue();
			String test_creation_row2 = cell2.getStringCellValue();

			products_creation_map.put(test_creation_row1, test_creation_row2);
			products_creation.add(i, products_creation_map);
			i++;
		}
		if (products_creation_map.containsKey(product_category.trim())) {
			product_type = products_creation.get(0).get(product_category);
		}
		return product_type;
	}

	// Common dispatch only batch product create
	public void batchProductCommonDispatchOnly() throws Exception {
		customizeLoadingDelay(navigation_pane, 20);
		click(navigation_pane);
		customizeLoadingDelay(inventory_module, 20);
		click(inventory_module);

		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);

		InventoryAndWarehouseModuleRegression obj1 = new InventoryAndWarehouseModuleRegression();
		obj1.customizeLoadingDelay(btn_new_product, 8);
		Thread.sleep(1500);
		click(btn_new_product);

		String product_code = currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "-Madhushan_DispatchOnly";

		explicitWait(txt_product_code, 40);
		sendKeys(txt_product_code, product_code);

		sendKeys(txt_product_desc, productDesription);

		selectText(dropdown_productGroupDispatchOnlyBatchProduct, readIWData(1));

		click(btn_menufacturerSearchDispatchOnlyBatchProduct);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, readIWData(2));
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		waitImplicit(lnk_resultMenufacturerDispatchOnlyBatchProduct, 6);
		sleepCusomized(lnk_resultMenufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacturerDispatchOnlyBatchProduct);

		selectIndex(dropdownDefoultUOMGroupDispatchOnlyBatchProduct, 1);

		selectIndex(dropdownDefoultUOMDispatchOnlyBatchProduct, 1);

		click(tab_deatilsDispatchOnlyBatchProduct);

		if (!isSelected(chk_batchProductDispatchOnlyBatchProduct)) {
			click(chk_batchProductDispatchOnlyBatchProduct);
		}
		selectIndex(dropdown_dispatchSelectionDispatchOnlyBatchProduct, 1);

		click(btn_draft2);

		click(btn_releese2);
		customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 40);
		writeIWData("Product Code", product_code, 3);
		Thread.sleep(3500);

	}

	// Common QC create Acceptance product
	public void createProductQCAcceptance() throws Exception {
		explicitWait(navigation_pane, 30);
		Thread.sleep(2000);
		explicitWait(navigation_pane, 30);
		click(navigation_pane);

		sleepCusomized(inventory_module);
		click(inventory_module);

		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);

		InventoryAndWarehouseModuleRegression obj1 = new InventoryAndWarehouseModuleRegression();
		obj1.customizeLoadingDelay(btn_new_product, 20);
		Thread.sleep(1500);
		click(btn_new_product);

		String product_code = currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "-Madhushan_QC_Accept";

		obj1.customizeLoadingDelay(txt_product_code, 8);
		sendKeys(txt_product_code, product_code);

		sendKeys(txt_product_desc, productDesription);

		selectText(dropdown_productGroupDispatchOnlyBatchProduct, readIWData(1));

		click(btn_menufacturerSearchDispatchOnlyBatchProduct);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, readIWData(2));
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		waitImplicit(lnk_resultMenufacturerDispatchOnlyBatchProduct, 6);
		doubleClick(lnk_resultMenufacturerDispatchOnlyBatchProduct);

		click(tab_deatilsDispatchOnlyBatchProduct);
		if (!isDisplayed(chk_batchProductDispatchOnlyBatchProduct)) {
			click(btn_inventory_active);
			click(btn_product_function_confirmation);
		}

		if (!isSelected(chk_batchProductDispatchOnlyBatchProduct)) {
			click(chk_batchProductDispatchOnlyBatchProduct);
		}

		click(chk_QCAccept);

		Thread.sleep(3000);
		pageScrollDown();
		pageScrollUpToTop();
		selectText(dropdown_outbound_costing_method, "FIFO");

		click(tab_summaryQCAcceptanec);
		selectIndex(dropdownDefoultUOMGroupDispatchOnlyBatchProduct, 1);
		sleepCusomized(dropdownDefoultUOMDispatchOnlyBatchProduct);
		selectIndex(dropdownDefoultUOMDispatchOnlyBatchProduct, 1);

		click(btn_draft2);

		sleepCusomized(btn_releese2);
		click(btn_releese2);

		writeIWData("Product Code", product_code, 6);
	}

	// Common manufacturer create
	public void menufacturerCommon() throws Exception {
		waitImplicit(navigation_pane, 15);
		click(navigation_pane);
		sleepCusomized(inventory_module);
		click(inventory_module);
		click(btn_menufacturerInformtion);
		click(btn_newMenufacturer);

		String menufacturerCode = (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "-MADHUSHAN")
				.substring(4);

		sleepCusomized(menufacturerCode);
		sendKeys(txt_menufacturerCode, menufacturerCode);

		String menufacturerName = currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "-MADHUSHAN";
		sendKeys(txt_nameMenufacturer, menufacturerName);
		click(btn_upadateMenufacturer);

		writeIWData("Menufacturer Code", menufacturerCode, 2);
	}

	// Common create stock adjustment
	public void commonStockAdjustmentQC_batchProductOnly(String productCost, String productQty, int rowIndex)
			throws Exception {
		do {
			waitImplicit(navigation_pane, 1);
			int WaitingTimeCount = 0;
			WaitingTimeCount = +1;
			if (WaitingTimeCount == 20) {
				break;
			}
		} while (!isDisplayed(navigation_pane));

		if (isDisplayed(navigation_pane)) {
			click(navigation_pane);
		} else {

		}

		sleepCusomized(inventory_module);
		click(inventory_module);
		click(btn_stockAdjustment);
		click(btn_newStockAdjustment);

		sendKeys(txt_descStockAdj, descStockAdj);
		selectText(dropdown_warehouseStockAdj, readIWData(4));

		click(btn_productLookupStockAdj);

		sendKeys(txt_productSearch, readIWData(rowIndex));
		pressEnter(txt_productSearch);
		Thread.sleep(3000);
		sleepCusomized(btn_resultProduct);
		doubleClick(btn_resultProduct);
		sendKeys(txt_qtyProductGrid1, productQty);
		sendKeys(txt_costProductGrid1, productCost);

		click(btn_draft);

		click(brn_serielBatchCapture);

		click(btn_serielBatchCaptureProduct1ILOOutboundShipment);

		sendKeys(txt_batchNumberCaptureILOOutboundShipment, currentTimeAndDate());

		pressEnter(txt_batchNumberCaptureILOOutboundShipment);

		Thread.sleep(4000);

		click(btn_updateCapture);

		pageRefersh();

		click(btn_relese);
	}

	// Common
	public void sleepCusomized(String locator) throws Exception {
		Thread.sleep(3000);
		/*
		 * int count = 0; do { count = count + 1; Thread.sleep(2000); if
		 * (isDisplayed(locator) || count > 5) { break; }
		 * 
		 * } while (!isDisplayed(locator));
		 */
	}

	// Common
	public void sleepCusomizer(String locator) throws Exception {

		int count = 0;
		do {
			count = count + 1;
			Thread.sleep(2000);
			if (isDisplayed(locator) || count > 5) {
				break;
			}

		} while (!isDisplayed(locator));

	}

	/* Common - Purchase Order -For Fin_TC_016 */
	public String purchaseOrder() throws Exception {
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		removeShipmentAcceptance();
		click(navigation_pane);

		click(btn_procumentModule);

		click(btn_purchaseOrder);

		click(btn_newPrurchaseOrder);

		click(btn_purchaseOrderToInboundShipmentToPurchaseInvoiceJourney);

		click(btn_vendorSearchPurchaseOredr);

		sleepCusomized(txt_vendorSearch23);
		FinanceModule obj = new FinanceModule();
		sendKeys(txt_vendorSearch23, obj.vebdorLetterOfGurentee);
		pressEnter(txt_vendorSearch23);
		sleepCusomized(lnk_resultVendorSeacrh23);
		doubleClick(lnk_resultVendorSeacrh23);

		click(btn_serachProductLookup23);

		sleepCusomized(txt_productSearch);

		sendKeys(txt_productSearch, readIWData(6));

		pressEnter(txt_productSearch);

		sleepCusomized(lnk_resultProductPurchaseOrder);
		Thread.sleep(2000);

		doubleClick(lnk_resultProductPurchaseOrder);

		selectText(drop_warehousePurchaseOrder, warehousePurchaseOrder);

		sendKeys(txt_qtyProductPurchaseOrder, qtyPurchaseORder);

		sendKeys(txt_unitPricePurchaseOrder, unitPricePurchaseOrder);

		sleepCusomized(btn_checkoutPurchaaseOrdeer);
		click(btn_checkoutPurchaaseOrdeer);

		click(btn_draft2);

		explicitWaitUntillClickable(btn_releese2, i);
		String doc_no = getText(lbl_docNo);
		click(btn_releese2);
		explicitWaitUntillClickable(btn_goToPageLink, i);

		if (isDisplayed(btn_goToPageLink)) {

			writeTestResults("Verify user can releese Purchase Order", "User should be abe to releese Purchase Order",
					"User sucessfully releese Purchase Order", "pass");
		} else {
			writeTestResults("Verify user can releese Purchase Order", "User should be abe to releese Purchase Order",
					"User couldn't releese Purchase Order", "fail");
		}
		click(btn_goToPageLink);
		switchWindow();
		sleepCusomized(btn_checkout2);
		click(btn_checkout2);

		click(btn_draft2);
		sleepCusomized(brn_serielBatchCapture);
		Thread.sleep(2000);
		click(brn_serielBatchCapture);

		click(btn_serielBatchCaptureProduct1ILOOutboundShipment);

		sendKeys(txt_batchNumberCaptureILOOutboundShipment, currentTimeAndDate());
		sendKeys(txt_lotNoCapturePage1, genProductId());
		pressEnter(txt_lotNoCapturePage1);

		Thread.sleep(3000);

		click(btn_updateCapture);

		pageRefersh();
		sleepCusomized(btn_releese2);
		Thread.sleep(2000);
		inbound_shipment_qc_acceptance = getText(lbl_inboundShipmentDocNoShipmentAcceptance);
		click(btn_releese2);
		Thread.sleep(4000);
		if (isDisplayed(btn_goToNextTaskILO)) {

			writeTestResults("Verify user can releese Inbound Shipment",
					"User should be abe to releese Inbound Shipment", "User sucessfully releese Inbound Shipment",
					"pass");
		} else {
			writeTestResults("Verify user can releese Inbound Shipment",
					"User should be abe to releese Inbound Shipment", "User couldn't releese Inbound Shipment", "fail");
		}

		sleepCusomized(btn_goToNextTaskILO);
		click(btn_goToNextTaskILO);

		switchWindow();
		sleepCusomized(btn_checkout);
		click(btn_checkout);

		sleepCusomized(btn_draft);
		click(btn_draft);

		explicitWaitUntillClickable(btn_releese2, 60);
		click(btn_releese2);
		explicitWait(header_releasedPurchaseInvoice, 60);
		sleepCusomized(lbl_docNo);
		trackCode = getText(lbl_docNo);
		return doc_no;
	}

	/* Common - Remove Shipment Acceptance */ /* Use:Fin_TC_016 */
	public void removeShipmentAcceptance() throws Exception {
		waitImplicit(navigation_pane, 60);
		click(navigation_pane);
		click(btn_adminModule);

		customizeLoadingDelay(btn_balancingLevelAdminModule, 20);
		click(btn_balancingLevelAdminModule);

		customizeLoadingDelay(span_countOfJourneyInShipmentAcceptance, 40);
		Thread.sleep(1000);
		int journryCount = getCount(span_countOfJourneyInShipmentAcceptance);
		for (int i = 0; i < journryCount; i++) {
			click(btn_deleestShipmentAceptance);
		}
		System.out.println("Removing Shipment Acceptance........");
		click(btn_updateBalancingLevelQCAcceptance);
		pageRefersh();
		customizeLoadingDelay(btn_genarelBalancingLevelSetting, 30);
		click(btn_genarelBalancingLevelSetting);
		customizeLoadingDelay(chk_shipemtAceptanceGeneral, 10);
		if (isSelected(chk_shipemtAceptanceGeneral)) {
			click(chk_shipemtAceptanceGeneral);
		}
		click(btn_updateGenaralSectionBalancingLevel);
		Thread.sleep(3000);
		pageRefersh();
	}

	/* Create Common Products */
	public void createCommonProducts() throws Exception {
		pageRefersh();
		sleepCusomized(navigation_pane);
		waitImplicit(navigation_pane, 10);
		click(navigation_pane);
		sleepCusomized(inventory_module);
		click(inventory_module);
		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		customizeLoadingDelay(btn_new_product, 20);

		Thread.sleep(1500);
		click(btn_new_product);

		// product code
		String product_code = (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "-BATCH-SPECIFIC");
		sendKeys(txt_product_code, product_code);

		// product description
		sendKeys(txt_product_desc, productDesription);

		// product group
		selectText(dropdown_product_group, productionGroupBatchProductCreation);

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		String menufacturur = readIWData(2);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturur);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", readIWData(2)));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);

		// product width
		customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		click(tab_detail);
		Thread.sleep(3000);
		customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);
		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		Thread.sleep(3000);
		click(chk_batchProductDispatchOnlyBatchProduct);
		Thread.sleep(3000);
		pageScrollDown();
		pageScrollUpToTop();
		selectText(dropdown_outbound_costing_method, outboundCostingMethod);
		Select comboBox = new Select(driver.findElement(By.xpath(dropdown_outbound_costing_method)));
		Thread.sleep(3000);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (!selectedComboValue.equals(outboundCostingMethod)) {
			selectText(dropdown_outbound_costing_method, outboundCostingMethod);
			Thread.sleep(2000);
		}

		click(checkbox_without_costing);
		click(chk_allocationBatchProduct);

		click(checkbox_validate_expiery);
		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		if (!isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		Thread.sleep(1000);
		if (!isSelected(checkbox_validate_expiery_date)) {
			click(checkbox_validate_expiery_date);
		}

		sendKeys(txt_minPriceTextProductCreation, minPriceCoommnonCreateProduct);
		Thread.sleep(1000);
		sendKeys(txt_maxPriceTextProductCreation, maxPriceCoommnonCreateProduct);
		Thread.sleep(1000);

		Thread.sleep(1000);

		click(btn_draft);

		customizeLoadingDelay(btn_releese2, 30);
		click(btn_releese2);
		customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 40);
		products_map.put("BatchSpecific", product_code);
		products.add(0, products_map);
		writeTestCreations("BatchSpecific", products.get(0).get("BatchSpecific"), 0);

		// Service
		customizeLoadingDelay(navigation_pane, 40);
		Thread.sleep(2000);
		customizeLoadingDelay(navigation_pane, 40);

		click(navigation_pane);

		sleepCusomized(inventory_module);
		click(inventory_module);
		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		customizeLoadingDelay(btn_new_product, 20);

		Thread.sleep(1500);
		click(btn_new_product);

		// product code
		String product_code_service = (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "-Service");
		sendKeys(txt_product_code, product_code_service);

		// product description
		sendKeys(txt_product_desc, productDesription);

		// product group
		selectText(dropdown_product_group, productGroupServiceCommonProductCreate);

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		String menufacturerServ = readIWData(2);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturerServ);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", readIWData(2)));

		click(tab_detail);

		click(btn_purchase_active);
		click(btn_purchaseActiveConfirmation);

		click(btn_sales_active);
		click(btn_salesActiveConfirmation);

		sendKeys(txt_minPriceTextProductCreation, minPriceCoommnonCreateProduct);
		Thread.sleep(1000);
		sendKeys(txt_maxPriceTextProductCreation, maxPriceCoommnonCreateProduct);
		Thread.sleep(1000);
		Thread.sleep(1000);

		click(btn_draft);
		customizeLoadingDelay(btn_releese2, 40);

		click(btn_releese2);
		customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 40);
		products_map.put("Service", product_code_service);
		products.add(0, products_map);
		writeTestCreations("Service", products.get(0).get("Service"), 7);

		sleepCusomized(navigation_pane);

		click(navigation_pane);
		sleepCusomized(inventory_module);

		click(inventory_module);
		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		customizeLoadingDelay(btn_new_product, 20);

		Thread.sleep(1500);
		click(btn_new_product);

		// product code
		String product_code_batch_fifo = (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "-BATCH-FIFO");
		sendKeys(txt_product_code, product_code_batch_fifo);

		// product description
		sendKeys(txt_product_desc, productDesription);

		// product group
		selectText(dropdown_product_group, productionGroupBatchProductCreation);

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturur);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", readIWData(2)));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);
		// product width
		customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		click(tab_detail);
		customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);

		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		click(chk_batchProductDispatchOnlyBatchProduct);
		Thread.sleep(3000);
		pageScrollDown();
		pageScrollUpToTop();
		selectText(dropdown_outbound_costing_method, "FIFO");
		Thread.sleep(1500);
		click(checkbox_without_costing);
		click(chk_allocationBatchProduct);

		click(checkbox_validate_expiery);
		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		if (!isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		sendKeys(txt_minPriceTextProductCreation, minPriceCoommnonCreateProduct);
		Thread.sleep(1000);
		sendKeys(txt_maxPriceTextProductCreation, maxPriceCoommnonCreateProduct);
		Thread.sleep(1000);

		Thread.sleep(1000);
		if (!isSelected(checkbox_validate_expiery_date)) {
			click(checkbox_validate_expiery_date);
		}
		Thread.sleep(1000);

		click(btn_draft);
		customizeLoadingDelay(btn_releese2, 40);

		click(btn_releese2);
		Thread.sleep(1000);
		customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 40);

		products_map.put("BatchFifo", product_code_batch_fifo);
		products.add(0, products_map);
		writeTestCreations("BatchFifo", products.get(0).get("BatchFifo"), 1);
		sleepCusomized(navigation_pane);
		click(navigation_pane);
		sleepCusomized(inventory_module);

		click(inventory_module);
		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		customizeLoadingDelay(btn_new_product, 20);

		Thread.sleep(1500);
		click(btn_new_product);

		// product code
		String product_code_lot = (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "-LOT");
		sendKeys(txt_product_code, product_code_lot);

		// product description
		sendKeys(txt_product_desc, productDesription);

		// product group
		selectText(dropdown_product_group, productionGroupBatchProductCreation);

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturur);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", readIWData(2)));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);
		// product width
		customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		click(tab_detail);
		customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);

		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		click(checkbox_without_costing);
		click(chk_allocationBatchProduct);

		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		sleepCusomized(checkbox_qc_acceptance);
		if (!isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		sendKeys(txt_minPriceTextProductCreation, minPriceCoommnonCreateProduct);
		Thread.sleep(1000);
		sendKeys(txt_maxPriceTextProductCreation, maxPriceCoommnonCreateProduct);
		Thread.sleep(1000);

		click(btn_draft);
		customizeLoadingDelay(btn_releese2, 40);

		click(btn_releese2);
		customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 40);
		products_map.put("Lot", product_code_lot);
		products.add(0, products_map);
		writeTestCreations("Lot", products.get(0).get("Lot"), 2);
		sleepCusomized(navigation_pane);
		click(navigation_pane);
		sleepCusomized(inventory_module);

		click(inventory_module);
		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		customizeLoadingDelay(btn_new_product, 20);

		Thread.sleep(1500);
		click(btn_new_product);

		// product code
		String product_code_seriel_specific = (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")
				+ "-SERIEL-SPECIFIC");
		sendKeys(txt_product_code, product_code_seriel_specific);

		// product description
		sendKeys(txt_product_desc, productDesription);

		// product group
		selectText(dropdown_product_group, productionGroupBatchProductCreation);

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturur);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", readIWData(2)));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);
		// product width
		customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		click(tab_detail);
		customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);

		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,-200)");
		click(btn_chkIsSeriel);
		Thread.sleep(3000);
		pageScrollDown();
		pageScrollUpToTop();
		selectText(dropdown_outbound_costing_method, outboundCostingMethod);
		Thread.sleep(1500);
		click(checkbox_without_costing);
		click(chk_allocationBatchProduct);

		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		if (!isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		sendKeys(txt_minPriceTextProductCreation, minPriceCoommnonCreateProduct);
		Thread.sleep(1000);
		sendKeys(txt_maxPriceTextProductCreation, maxPriceCoommnonCreateProduct);
		Thread.sleep(1000);

		click(btn_draft);
		customizeLoadingDelay(btn_releese2, 40);

		click(btn_releese2);
		customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 40);

		products_map.put("SerirlSpecific", product_code_seriel_specific);
		products.add(0, products_map);
		writeTestCreations("SerielSpecific", products.get(0).get("SerirlSpecific"), 3);
		sleepCusomized(navigation_pane);
		click(navigation_pane);
		sleepCusomized(inventory_module);

		click(inventory_module);
		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		customizeLoadingDelay(btn_new_product, 20);

		Thread.sleep(1500);
		click(btn_new_product);

		// product code
		String product_code_seriel_batch_specific = (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")
				+ "-SERIEL-BATCH-SPECIFIC");
		sendKeys(txt_product_code, product_code_seriel_batch_specific);

		// product description
		sendKeys(txt_product_desc, productDesription);

		// product group
		selectText(dropdown_product_group, productionGroupBatchProductCreation);

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturur);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", readIWData(2)));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);
		// product width
		customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		click(tab_detail);
		customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);

		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		/*
		 * if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
		 * click(btn_inventoryActiveDispatchOnlyBatchProduct);
		 * click(btn_product_function_confirmation); }
		 */
		js.executeScript("window.scrollBy(0,-200)");
		click(btn_chkIsSeriel);
		click(chk_batchProductDispatchOnlyBatchProduct);
		Thread.sleep(3000);
		pageScrollDown();
		pageScrollUpToTop();
		selectText(dropdown_outbound_costing_method, outboundCostingMethod);
		Thread.sleep(1500);
		click(checkbox_without_costing);
		click(chk_allocationBatchProduct);

		click(checkbox_validate_expiery);
		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		if (!isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		sendKeys(txt_minPriceTextProductCreation, minPriceCoommnonCreateProduct);
		Thread.sleep(1000);
		sendKeys(txt_maxPriceTextProductCreation, maxPriceCoommnonCreateProduct);
		Thread.sleep(1000);

		Thread.sleep(1000);
		if (!isSelected(checkbox_validate_expiery_date)) {
			click(checkbox_validate_expiery_date);
		}
		Thread.sleep(1000);

		click(btn_draft);
		customizeLoadingDelay(btn_releese2, 40);

		click(btn_releese2);
		customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 40);

		products_map.put("SerielBatchSpecific", product_code_seriel_batch_specific);
		products.add(0, products_map);
		writeTestCreations("SerielBatchSpecific", products.get(0).get("SerielBatchSpecific"), 4);
		sleepCusomized(navigation_pane);
		click(navigation_pane);
		sleepCusomized(inventory_module);

		click(inventory_module);
		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		customizeLoadingDelay(btn_new_product, 20);

		Thread.sleep(1500);
		click(btn_new_product);

		// product code
		String product_code_seriel_batch_fifo = (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")
				+ "-SERIEL-BATCH-FIFO");
		sendKeys(txt_product_code, product_code_seriel_batch_fifo);

		// product description
		sendKeys(txt_product_desc, productDesription);

		// product group
		selectText(dropdown_product_group, productionGroupBatchProductCreation);

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturur);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", readIWData(2)));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);
		// product width
		customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		click(tab_detail);
		customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);

		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		js.executeScript("window.scrollBy(0,-200)");
		click(btn_chkIsSeriel);
		click(chk_batchProductDispatchOnlyBatchProduct);
		Thread.sleep(3000);
		pageScrollDown();
		pageScrollUpToTop();
		selectText(dropdown_outbound_costing_method, "FIFO");
		Thread.sleep(1500);
		click(checkbox_without_costing);
		click(chk_allocationBatchProduct);

		click(checkbox_validate_expiery);
		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		if (!isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		sendKeys(txt_minPriceTextProductCreation, minPriceCoommnonCreateProduct);
		Thread.sleep(1000);
		sendKeys(txt_maxPriceTextProductCreation, maxPriceCoommnonCreateProduct);
		Thread.sleep(1000);

		Thread.sleep(1000);
		if (!isSelected(checkbox_validate_expiery_date)) {
			click(checkbox_validate_expiery_date);
		}
		Thread.sleep(1000);

		click(btn_draft);
		customizeLoadingDelay(btn_releese2, 40);

		click(btn_releese2);
		customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 40);

		products_map.put("SerielBatchFifo", product_code_seriel_batch_fifo);
		products.add(0, products_map);
		writeTestCreations("SerielBatchFifo", products.get(0).get("SerielBatchFifo"), 5);
		sleepCusomized(navigation_pane);
		click(navigation_pane);

		click(inventory_module);
		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		customizeLoadingDelay(btn_new_product, 20);

		Thread.sleep(1500);
		click(btn_new_product);

		Thread.sleep(1500);
		click(btn_new_product);

		// product code
		String product_code_seriel_fifi = (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")
				+ "-SERIEL-FIFO");
		sendKeys(txt_product_code, product_code_seriel_fifi);

		// product description
		sendKeys(txt_product_desc, productDesription);

		// product group
		selectText(dropdown_product_group, productionGroupBatchProductCreation);

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturur);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", readIWData(2)));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);
		// product width
		customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		click(tab_detail);
		customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);

		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		js.executeScript("window.scrollBy(0,-200)");
		click(btn_chkIsSeriel);
		selectText(dropdown_outbound_costing_method, "FIFO");
		Thread.sleep(1500);
		click(checkbox_without_costing);
		click(chk_allocationBatchProduct);

		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		if (!isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		sendKeys(txt_minPriceTextProductCreation, minPriceCoommnonCreateProduct);
		Thread.sleep(1000);
		sendKeys(txt_maxPriceTextProductCreation, maxPriceCoommnonCreateProduct);
		Thread.sleep(1000);

		click(btn_draft);
		customizeLoadingDelay(btn_releese2, 40);

		click(btn_releese2);
		customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 40);

		products_map.put("SerirlFifo", product_code_seriel_fifi);
		products.add(0, products_map);
		writeTestCreations("SerirlFifo", products.get(0).get("SerirlFifo"), 6);
	}

	public void createCommonProductsWithUniqueDescriptionForBatchSpecific() throws Exception {
		pageRefersh();
		sleepCusomized(navigation_pane);
		waitImplicit(navigation_pane, 10);
		click(navigation_pane);
		sleepCusomized(inventory_module);
		click(inventory_module);
		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		customizeLoadingDelay(btn_new_product, 8);

		Thread.sleep(1500);
		click(btn_new_product);

		// product code
		String product_code = (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "-BATCH-SPECIFIC");
		sendKeys(txt_product_code, product_code);

		// product description
		sendKeys(txt_product_desc, "DESC" + product_code);

		// product group
		selectText(dropdown_product_group, productionGroupBatchProductCreation);

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		String menufacturur = readIWData(2);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturur);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", readIWData(2)));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);

		// product width
		customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		click(tab_detail);
		Thread.sleep(3000);
		customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);
		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		Thread.sleep(3000);
		click(chk_batchProductDispatchOnlyBatchProduct);
		Thread.sleep(3000);
		pageScrollDown();
		pageScrollUpToTop();
		selectText(dropdown_outbound_costing_method, outboundCostingMethod);
		Select comboBox = new Select(driver.findElement(By.xpath(dropdown_outbound_costing_method)));
		Thread.sleep(3000);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (!selectedComboValue.equals(outboundCostingMethod)) {
			selectText(dropdown_outbound_costing_method, outboundCostingMethod);
			Thread.sleep(2000);
		}

		click(checkbox_without_costing);
		click(chk_allocationBatchProduct);

		click(checkbox_validate_expiery);
		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		if (!isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		Thread.sleep(1000);
		if (!isSelected(checkbox_validate_expiery_date)) {
			click(checkbox_validate_expiery_date);
		}

		sendKeys(txt_minPriceTextProductCreation, minPriceCoommnonCreateProduct);
		Thread.sleep(1000);
		sendKeys(txt_maxPriceTextProductCreation, maxPriceCoommnonCreateProduct);
		Thread.sleep(1000);

		Thread.sleep(1000);

		click(btn_draft);

		customizeLoadingDelay(btn_releese2, 30);
		click(btn_releese2);
		customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 40);
		products_map.put("BatchSpecific", product_code);
		products.add(0, products_map);
		writeTestCreations("BatchSpecific", products.get(0).get("BatchSpecific"), 0);

		// Service
		customizeLoadingDelay(navigation_pane, 40);
		Thread.sleep(2000);
		customizeLoadingDelay(navigation_pane, 40);

		click(navigation_pane);

		sleepCusomized(inventory_module);
		click(inventory_module);
		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		customizeLoadingDelay(btn_new_product, 8);

		Thread.sleep(1500);
		click(btn_new_product);

		// product code
		String product_code_service = (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "-Service");
		sendKeys(txt_product_code, product_code_service);

		// product description
		sendKeys(txt_product_desc, productDesription);

		// product group
		selectText(dropdown_product_group, productGroupServiceCommonProductCreate);

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		String menufacturerServ = readIWData(2);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturerServ);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", readIWData(2)));

		click(tab_detail);

		click(btn_purchase_active);
		click(btn_purchaseActiveConfirmation);

		click(btn_sales_active);
		click(btn_salesActiveConfirmation);

		sendKeys(txt_minPriceTextProductCreation, minPriceCoommnonCreateProduct);
		Thread.sleep(1000);
		sendKeys(txt_maxPriceTextProductCreation, maxPriceCoommnonCreateProduct);
		Thread.sleep(1000);
		Thread.sleep(1000);

		click(btn_draft);
		customizeLoadingDelay(btn_releese2, 40);

		click(btn_releese2);
		customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 40);
		products_map.put("Service", product_code_service);
		products.add(0, products_map);
		writeTestCreations("Service", products.get(0).get("Service"), 7);

		sleepCusomized(navigation_pane);

		click(navigation_pane);
		sleepCusomized(inventory_module);

		click(inventory_module);
		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		customizeLoadingDelay(btn_new_product, 8);

		Thread.sleep(1500);
		click(btn_new_product);

		// product code
		String product_code_batch_fifo = (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "-BATCH-FIFO");
		sendKeys(txt_product_code, product_code_batch_fifo);

		// product description
		sendKeys(txt_product_desc, productDesription);

		// product group
		selectText(dropdown_product_group, productionGroupBatchProductCreation);

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturur);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", readIWData(2)));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);
		// product width
		customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		click(tab_detail);
		customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);

		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		click(chk_batchProductDispatchOnlyBatchProduct);
		Thread.sleep(3000);
		pageScrollDown();
		pageScrollUpToTop();
		selectText(dropdown_outbound_costing_method, "FIFO");
		Thread.sleep(1500);
		click(checkbox_without_costing);
		click(chk_allocationBatchProduct);

		click(checkbox_validate_expiery);
		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		if (!isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		sendKeys(txt_minPriceTextProductCreation, minPriceCoommnonCreateProduct);
		Thread.sleep(1000);
		sendKeys(txt_maxPriceTextProductCreation, maxPriceCoommnonCreateProduct);
		Thread.sleep(1000);

		Thread.sleep(1000);
		if (!isSelected(checkbox_validate_expiery_date)) {
			click(checkbox_validate_expiery_date);
		}
		Thread.sleep(1000);

		click(btn_draft);
		customizeLoadingDelay(btn_releese2, 40);

		click(btn_releese2);
		Thread.sleep(1000);
		customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 40);

		products_map.put("BatchFifo", product_code_batch_fifo);
		products.add(0, products_map);
		writeTestCreations("BatchFifo", products.get(0).get("BatchFifo"), 1);
		sleepCusomized(navigation_pane);
		click(navigation_pane);
		sleepCusomized(inventory_module);

		click(inventory_module);
		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		customizeLoadingDelay(btn_new_product, 8);

		Thread.sleep(1500);
		click(btn_new_product);

		// product code
		String product_code_lot = (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "-LOT");
		sendKeys(txt_product_code, product_code_lot);

		// product description
		sendKeys(txt_product_desc, productDesription);

		// product group
		selectText(dropdown_product_group, productionGroupBatchProductCreation);

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturur);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", readIWData(2)));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);
		// product width
		customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		click(tab_detail);
		customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);

		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		click(checkbox_without_costing);
		click(chk_allocationBatchProduct);

		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		sleepCusomized(checkbox_qc_acceptance);
		if (!isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		sendKeys(txt_minPriceTextProductCreation, minPriceCoommnonCreateProduct);
		Thread.sleep(1000);
		sendKeys(txt_maxPriceTextProductCreation, maxPriceCoommnonCreateProduct);
		Thread.sleep(1000);

		click(btn_draft);
		customizeLoadingDelay(btn_releese2, 40);

		click(btn_releese2);
		customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 40);
		products_map.put("Lot", product_code_lot);
		products.add(0, products_map);
		writeTestCreations("Lot", products.get(0).get("Lot"), 2);
		sleepCusomized(navigation_pane);
		click(navigation_pane);
		sleepCusomized(inventory_module);

		click(inventory_module);
		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		customizeLoadingDelay(btn_new_product, 8);

		Thread.sleep(1500);
		click(btn_new_product);

		// product code
		String product_code_seriel_specific = (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")
				+ "-SERIEL-SPECIFIC");
		sendKeys(txt_product_code, product_code_seriel_specific);

		// product description
		sendKeys(txt_product_desc, "DESC" + product_code_seriel_specific);

		// product group
		selectText(dropdown_product_group, productionGroupBatchProductCreation);

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturur);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", readIWData(2)));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);
		// product width
		customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		click(tab_detail);
		customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);

		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,-200)");
		click(btn_chkIsSeriel);
		Thread.sleep(3000);
		pageScrollDown();
		pageScrollUpToTop();
		selectText(dropdown_outbound_costing_method, outboundCostingMethod);
		Thread.sleep(1500);
		click(checkbox_without_costing);
		click(chk_allocationBatchProduct);

		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		if (!isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		sendKeys(txt_minPriceTextProductCreation, minPriceCoommnonCreateProduct);
		Thread.sleep(1000);
		sendKeys(txt_maxPriceTextProductCreation, maxPriceCoommnonCreateProduct);
		Thread.sleep(1000);

		click(btn_draft);
		customizeLoadingDelay(btn_releese2, 40);

		click(btn_releese2);
		customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 40);

		products_map.put("SerirlSpecific", product_code_seriel_specific);
		products.add(0, products_map);
		writeTestCreations("SerielSpecific", products.get(0).get("SerirlSpecific"), 3);
		sleepCusomized(navigation_pane);
		click(navigation_pane);
		sleepCusomized(inventory_module);

		click(inventory_module);
		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		customizeLoadingDelay(btn_new_product, 8);

		Thread.sleep(1500);
		click(btn_new_product);

		// product code
		String product_code_seriel_batch_specific = (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")
				+ "-SERIEL-BATCH-SPECIFIC");
		sendKeys(txt_product_code, product_code_seriel_batch_specific);

		// product description
		sendKeys(txt_product_desc, productDesription);

		// product group
		selectText(dropdown_product_group, productionGroupBatchProductCreation);

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturur);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", readIWData(2)));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);
		// product width
		customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		click(tab_detail);
		customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);

		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		/*
		 * if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
		 * click(btn_inventoryActiveDispatchOnlyBatchProduct);
		 * click(btn_product_function_confirmation); }
		 */
		js.executeScript("window.scrollBy(0,-200)");
		click(btn_chkIsSeriel);
		click(chk_batchProductDispatchOnlyBatchProduct);
		Thread.sleep(3000);
		pageScrollDown();
		pageScrollUpToTop();
		selectText(dropdown_outbound_costing_method, outboundCostingMethod);
		Thread.sleep(1500);
		click(checkbox_without_costing);
		click(chk_allocationBatchProduct);

		click(checkbox_validate_expiery);
		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		if (!isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		sendKeys(txt_minPriceTextProductCreation, minPriceCoommnonCreateProduct);
		Thread.sleep(1000);
		sendKeys(txt_maxPriceTextProductCreation, maxPriceCoommnonCreateProduct);
		Thread.sleep(1000);

		Thread.sleep(1000);
		if (!isSelected(checkbox_validate_expiery_date)) {
			click(checkbox_validate_expiery_date);
		}
		Thread.sleep(1000);

		click(btn_draft);
		customizeLoadingDelay(btn_releese2, 40);

		click(btn_releese2);
		customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 40);

		products_map.put("SerielBatchSpecific", product_code_seriel_batch_specific);
		products.add(0, products_map);
		writeTestCreations("SerielBatchSpecific", products.get(0).get("SerielBatchSpecific"), 4);
		sleepCusomized(navigation_pane);
		click(navigation_pane);
		sleepCusomized(inventory_module);

		click(inventory_module);
		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		customizeLoadingDelay(btn_new_product, 8);

		Thread.sleep(1500);
		click(btn_new_product);

		// product code
		String product_code_seriel_batch_fifo = (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")
				+ "-SERIEL-BATCH-FIFO");
		sendKeys(txt_product_code, product_code_seriel_batch_fifo);

		// product description
		sendKeys(txt_product_desc, productDesription);

		// product group
		selectText(dropdown_product_group, productionGroupBatchProductCreation);

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturur);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", readIWData(2)));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);
		// product width
		customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		click(tab_detail);
		customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);

		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		js.executeScript("window.scrollBy(0,-200)");
		click(btn_chkIsSeriel);
		click(chk_batchProductDispatchOnlyBatchProduct);
		Thread.sleep(3000);
		pageScrollDown();
		pageScrollUpToTop();
		selectText(dropdown_outbound_costing_method, "FIFO");
		Thread.sleep(1500);
		click(checkbox_without_costing);
		click(chk_allocationBatchProduct);

		click(checkbox_validate_expiery);
		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		if (!isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		sendKeys(txt_minPriceTextProductCreation, minPriceCoommnonCreateProduct);
		Thread.sleep(1000);
		sendKeys(txt_maxPriceTextProductCreation, maxPriceCoommnonCreateProduct);
		Thread.sleep(1000);

		Thread.sleep(1000);
		if (!isSelected(checkbox_validate_expiery_date)) {
			click(checkbox_validate_expiery_date);
		}
		Thread.sleep(1000);

		click(btn_draft);
		customizeLoadingDelay(btn_releese2, 40);

		click(btn_releese2);
		customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 40);

		products_map.put("SerielBatchFifo", product_code_seriel_batch_fifo);
		products.add(0, products_map);
		writeTestCreations("SerielBatchFifo", products.get(0).get("SerielBatchFifo"), 5);
		sleepCusomized(navigation_pane);
		click(navigation_pane);

		click(inventory_module);
		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		customizeLoadingDelay(btn_new_product, 8);

		Thread.sleep(1500);
		click(btn_new_product);

		Thread.sleep(1500);
		click(btn_new_product);

		// product code
		String product_code_seriel_fifi = (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")
				+ "-SERIEL-FIFO");
		sendKeys(txt_product_code, product_code_seriel_fifi);

		// product description
		sendKeys(txt_product_desc, productDesription);

		// product group
		selectText(dropdown_product_group, productionGroupBatchProductCreation);

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturur);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", readIWData(2)));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);
		// product width
		customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		click(tab_detail);
		customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);

		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		js.executeScript("window.scrollBy(0,-200)");
		click(btn_chkIsSeriel);
		selectText(dropdown_outbound_costing_method, "FIFO");
		Thread.sleep(1500);
		click(checkbox_without_costing);
		click(chk_allocationBatchProduct);

		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		if (!isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		sendKeys(txt_minPriceTextProductCreation, minPriceCoommnonCreateProduct);
		Thread.sleep(1000);
		sendKeys(txt_maxPriceTextProductCreation, maxPriceCoommnonCreateProduct);
		Thread.sleep(1000);

		click(btn_draft);
		customizeLoadingDelay(btn_releese2, 40);

		click(btn_releese2);
		customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 40);

		products_map.put("SerirlFifo", product_code_seriel_fifi);
		products.add(0, products_map);
		writeTestCreations("SerirlFifo", products.get(0).get("SerirlFifo"), 6);
	}

	public void createCommonQCdisabledProducts() throws Exception {
		pageRefersh();
		sleepCusomized(navigation_pane);
		waitImplicit(navigation_pane, 10);
		click(navigation_pane);
		sleepCusomized(inventory_module);
		click(inventory_module);
		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		customizeLoadingDelay(btn_new_product, 8);

		Thread.sleep(1500);
		click(btn_new_product);

		// product code
		String product_code = (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "-BATCH-SPECIFIC");
		sendKeys(txt_product_code, product_code);

		// product description
		sendKeys(txt_product_desc, productDesription);

		// product group
		selectText(dropdown_product_group, productionGroupBatchProductCreation);

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		String menufacturur = readIWData(2);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturur);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", readIWData(2)));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);

		// product width
		customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		click(tab_detail);
		Thread.sleep(3000);
		customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);
		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		Thread.sleep(3000);
		click(chk_batchProductDispatchOnlyBatchProduct);
		Thread.sleep(3000);
		pageScrollDown();
		pageScrollUpToTop();
		selectText(dropdown_outbound_costing_method, outboundCostingMethod);
		Select comboBox = new Select(driver.findElement(By.xpath(dropdown_outbound_costing_method)));
		Thread.sleep(3000);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (!selectedComboValue.equals(outboundCostingMethod)) {
			selectText(dropdown_outbound_costing_method, outboundCostingMethod);
			Thread.sleep(2000);
		}

		click(checkbox_without_costing);
		click(chk_allocationBatchProduct);

		click(checkbox_validate_expiery);
		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		if (isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		Thread.sleep(1000);
		if (!isSelected(checkbox_validate_expiery_date)) {
			click(checkbox_validate_expiery_date);
		}

		sendKeys(txt_minPriceTextProductCreation, minPriceCoommnonCreateProduct);
		Thread.sleep(1000);
		sendKeys(txt_maxPriceTextProductCreation, maxPriceCoommnonCreateProduct);
		Thread.sleep(1000);

		Thread.sleep(1000);

		click(btn_draft);

		customizeLoadingDelay(btn_releese2, 30);
		click(btn_releese2);
		customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 40);
		products_map.put("BatchSpecific", product_code);
		products.add(0, products_map);
		writeTestCreations("BatchSpecific", products.get(0).get("BatchSpecific"), 0);

		// Service
		customizeLoadingDelay(navigation_pane, 40);
		Thread.sleep(2000);
		customizeLoadingDelay(navigation_pane, 40);

		click(navigation_pane);

		sleepCusomized(inventory_module);
		click(inventory_module);
		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		customizeLoadingDelay(btn_new_product, 8);

		Thread.sleep(1500);
		click(btn_new_product);

		// product code
		String product_code_service = (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "-Service");
		sendKeys(txt_product_code, product_code_service);

		// product description
		sendKeys(txt_product_desc, productDesription);

		// product group
		selectText(dropdown_product_group, productGroupServiceCommonProductCreate);

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		String menufacturerServ = readIWData(2);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturerServ);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", readIWData(2)));

		click(tab_detail);

		click(btn_purchase_active);
		click(btn_purchaseActiveConfirmation);

		click(btn_sales_active);
		click(btn_salesActiveConfirmation);

		sendKeys(txt_minPriceTextProductCreation, minPriceCoommnonCreateProduct);
		Thread.sleep(1000);
		sendKeys(txt_maxPriceTextProductCreation, maxPriceCoommnonCreateProduct);
		Thread.sleep(1000);
		Thread.sleep(1000);

		click(btn_draft);
		customizeLoadingDelay(btn_releese2, 40);

		click(btn_releese2);
		customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 40);
		products_map.put("Service", product_code_service);
		products.add(0, products_map);
		writeTestCreations("Service", products.get(0).get("Service"), 7);

		sleepCusomized(navigation_pane);

		click(navigation_pane);
		sleepCusomized(inventory_module);

		click(inventory_module);
		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		customizeLoadingDelay(btn_new_product, 8);

		Thread.sleep(1500);
		click(btn_new_product);

		// product code
		String product_code_batch_fifo = (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "-BATCH-FIFO");
		sendKeys(txt_product_code, product_code_batch_fifo);

		// product description
		sendKeys(txt_product_desc, productDesription);

		// product group
		selectText(dropdown_product_group, productionGroupBatchProductCreation);

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturur);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", readIWData(2)));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);
		// product width
		customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		click(tab_detail);
		customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);

		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		click(chk_batchProductDispatchOnlyBatchProduct);
		Thread.sleep(3000);
		pageScrollDown();
		pageScrollUpToTop();
		selectText(dropdown_outbound_costing_method, "FIFO");
		Thread.sleep(1500);
		click(checkbox_without_costing);
		click(chk_allocationBatchProduct);

		click(checkbox_validate_expiery);
		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		if (isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		sendKeys(txt_minPriceTextProductCreation, minPriceCoommnonCreateProduct);
		Thread.sleep(1000);
		sendKeys(txt_maxPriceTextProductCreation, maxPriceCoommnonCreateProduct);
		Thread.sleep(1000);

		Thread.sleep(1000);
		if (!isSelected(checkbox_validate_expiery_date)) {
			click(checkbox_validate_expiery_date);
		}
		Thread.sleep(1000);

		click(btn_draft);
		customizeLoadingDelay(btn_releese2, 40);

		click(btn_releese2);
		Thread.sleep(1000);
		customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 40);

		products_map.put("BatchFifo", product_code_batch_fifo);
		products.add(0, products_map);
		writeTestCreations("BatchFifo", products.get(0).get("BatchFifo"), 1);
		sleepCusomized(navigation_pane);
		click(navigation_pane);
		sleepCusomized(inventory_module);

		click(inventory_module);
		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		customizeLoadingDelay(btn_new_product, 8);

		Thread.sleep(1500);
		click(btn_new_product);

		// product code
		String product_code_lot = (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "-LOT");
		sendKeys(txt_product_code, product_code_lot);

		// product description
		sendKeys(txt_product_desc, productDesription);

		// product group
		selectText(dropdown_product_group, productionGroupBatchProductCreation);

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturur);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", readIWData(2)));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);
		// product width
		customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		click(tab_detail);
		customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);

		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		click(checkbox_without_costing);
		click(chk_allocationBatchProduct);

		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		sleepCusomized(checkbox_qc_acceptance);
		if (isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		sendKeys(txt_minPriceTextProductCreation, minPriceCoommnonCreateProduct);
		Thread.sleep(1000);
		sendKeys(txt_maxPriceTextProductCreation, maxPriceCoommnonCreateProduct);
		Thread.sleep(1000);

		click(btn_draft);
		customizeLoadingDelay(btn_releese2, 40);

		click(btn_releese2);
		customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 40);
		products_map.put("Lot", product_code_lot);
		products.add(0, products_map);
		writeTestCreations("Lot", products.get(0).get("Lot"), 2);
		sleepCusomized(navigation_pane);
		click(navigation_pane);
		sleepCusomized(inventory_module);

		click(inventory_module);
		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		customizeLoadingDelay(btn_new_product, 8);

		Thread.sleep(1500);
		click(btn_new_product);

		// product code
		String product_code_seriel_specific = (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")
				+ "-SERIEL-SPECIFIC");
		sendKeys(txt_product_code, product_code_seriel_specific);

		// product description
		sendKeys(txt_product_desc, productDesription);

		// product group
		selectText(dropdown_product_group, productionGroupBatchProductCreation);

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturur);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", readIWData(2)));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);
		// product width
		customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		click(tab_detail);
		customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);

		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,-200)");
		click(btn_chkIsSeriel);
		Thread.sleep(3000);
		pageScrollDown();
		pageScrollUpToTop();
		selectText(dropdown_outbound_costing_method, outboundCostingMethod);
		Thread.sleep(1500);
		click(checkbox_without_costing);
		click(chk_allocationBatchProduct);

		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		if (isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		sendKeys(txt_minPriceTextProductCreation, minPriceCoommnonCreateProduct);
		Thread.sleep(1000);
		sendKeys(txt_maxPriceTextProductCreation, maxPriceCoommnonCreateProduct);
		Thread.sleep(1000);

		click(btn_draft);
		customizeLoadingDelay(btn_releese2, 40);

		click(btn_releese2);
		customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 40);

		products_map.put("SerirlSpecific", product_code_seriel_specific);
		products.add(0, products_map);
		writeTestCreations("SerielSpecific", products.get(0).get("SerirlSpecific"), 3);
		sleepCusomized(navigation_pane);
		click(navigation_pane);
		sleepCusomized(inventory_module);

		click(inventory_module);
		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		customizeLoadingDelay(btn_new_product, 8);

		Thread.sleep(1500);
		click(btn_new_product);

		// product code
		String product_code_seriel_batch_specific = (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")
				+ "-SERIEL-BATCH-SPECIFIC");
		sendKeys(txt_product_code, product_code_seriel_batch_specific);

		// product description
		sendKeys(txt_product_desc, productDesription);

		// product group
		selectText(dropdown_product_group, productionGroupBatchProductCreation);

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturur);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", readIWData(2)));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);
		// product width
		customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		click(tab_detail);
		customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);

		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		/*
		 * if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
		 * click(btn_inventoryActiveDispatchOnlyBatchProduct);
		 * click(btn_product_function_confirmation); }
		 */
		js.executeScript("window.scrollBy(0,-200)");
		click(btn_chkIsSeriel);
		click(chk_batchProductDispatchOnlyBatchProduct);
		Thread.sleep(3000);
		pageScrollDown();
		pageScrollUpToTop();
		selectText(dropdown_outbound_costing_method, outboundCostingMethod);
		Thread.sleep(1500);
		click(checkbox_without_costing);
		click(chk_allocationBatchProduct);

		click(checkbox_validate_expiery);
		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		if (isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		sendKeys(txt_minPriceTextProductCreation, minPriceCoommnonCreateProduct);
		Thread.sleep(1000);
		sendKeys(txt_maxPriceTextProductCreation, maxPriceCoommnonCreateProduct);
		Thread.sleep(1000);

		Thread.sleep(1000);
		if (!isSelected(checkbox_validate_expiery_date)) {
			click(checkbox_validate_expiery_date);
		}
		Thread.sleep(1000);

		click(btn_draft);
		customizeLoadingDelay(btn_releese2, 40);

		click(btn_releese2);
		customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 40);

		products_map.put("SerielBatchSpecific", product_code_seriel_batch_specific);
		products.add(0, products_map);
		writeTestCreations("SerielBatchSpecific", products.get(0).get("SerielBatchSpecific"), 4);
		sleepCusomized(navigation_pane);
		click(navigation_pane);
		sleepCusomized(inventory_module);

		click(inventory_module);
		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		customizeLoadingDelay(btn_new_product, 8);

		Thread.sleep(1500);
		click(btn_new_product);

		// product code
		String product_code_seriel_batch_fifo = (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")
				+ "-SERIEL-BATCH-FIFO");
		sendKeys(txt_product_code, product_code_seriel_batch_fifo);

		// product description
		sendKeys(txt_product_desc, productDesription);

		// product group
		selectText(dropdown_product_group, productionGroupBatchProductCreation);

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturur);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", readIWData(2)));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);
		// product width
		customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		click(tab_detail);
		customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);

		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		js.executeScript("window.scrollBy(0,-200)");
		click(btn_chkIsSeriel);
		click(chk_batchProductDispatchOnlyBatchProduct);
		Thread.sleep(3000);
		pageScrollDown();
		pageScrollUpToTop();
		selectText(dropdown_outbound_costing_method, "FIFO");
		Thread.sleep(1500);
		click(checkbox_without_costing);
		click(chk_allocationBatchProduct);

		click(checkbox_validate_expiery);
		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		if (isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		sendKeys(txt_minPriceTextProductCreation, minPriceCoommnonCreateProduct);
		Thread.sleep(1000);
		sendKeys(txt_maxPriceTextProductCreation, maxPriceCoommnonCreateProduct);
		Thread.sleep(1000);

		Thread.sleep(1000);
		if (!isSelected(checkbox_validate_expiery_date)) {
			click(checkbox_validate_expiery_date);
		}
		Thread.sleep(1000);

		click(btn_draft);
		customizeLoadingDelay(btn_releese2, 40);

		click(btn_releese2);
		customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 40);

		products_map.put("SerielBatchFifo", product_code_seriel_batch_fifo);
		products.add(0, products_map);
		writeTestCreations("SerielBatchFifo", products.get(0).get("SerielBatchFifo"), 5);
		sleepCusomized(navigation_pane);
		click(navigation_pane);

		click(inventory_module);
		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		customizeLoadingDelay(btn_new_product, 8);

		Thread.sleep(1500);
		click(btn_new_product);

		Thread.sleep(1500);
		click(btn_new_product);

		// product code
		String product_code_seriel_fifi = (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")
				+ "-SERIEL-FIFO");
		sendKeys(txt_product_code, product_code_seriel_fifi);

		// product description
		sendKeys(txt_product_desc, productDesription);

		// product group
		selectText(dropdown_product_group, productionGroupBatchProductCreation);

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturur);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", readIWData(2)));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);
		// product width
		customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		click(tab_detail);
		customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);

		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		js.executeScript("window.scrollBy(0,-200)");
		click(btn_chkIsSeriel);
		selectText(dropdown_outbound_costing_method, "FIFO");
		Thread.sleep(1500);
		click(checkbox_without_costing);
		click(chk_allocationBatchProduct);

		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		if (isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		sendKeys(txt_minPriceTextProductCreation, minPriceCoommnonCreateProduct);
		Thread.sleep(1000);
		sendKeys(txt_maxPriceTextProductCreation, maxPriceCoommnonCreateProduct);
		Thread.sleep(1000);

		click(btn_draft);
		customizeLoadingDelay(btn_releese2, 40);

		click(btn_releese2);
		customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 40);

		products_map.put("SerirlFifo", product_code_seriel_fifi);
		products.add(0, products_map);
		writeTestCreations("SerirlFifo", products.get(0).get("SerirlFifo"), 6);
	}

	/* Common generate batch specific serial number */
	public String genBatchSpecificSerielNumber(int count) throws InvalidFormatException, IOException {
		String readed_id = (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")).substring(4);
		int id = Integer.parseInt(readed_id);
		id += count;
		String id_out = Integer.toString(id);
		writeIWData("BatchSpecificSerielNumber", id_out, 10);
		String quate = "MADHUSHAN7-";
		if (user.equals("Madhushan")) {
			quate = "MADHUSHAN2-";
		}
		String out_final = quate + id_out;
		return out_final;
	}

	/* Common generate batch fifo serial number */
	public String genBatchFifoSerielNumber(int count) throws InvalidFormatException, IOException {
		String readed_id = (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")).substring(4);
		int id = Integer.parseInt(readed_id);
		id += count;
		String id_out = Integer.toString(id);
		writeIWData("BatchFifoSerielNumber", id_out, 11);
		String quate = "MADHUSHAN7-";
		if (user.equals("Madhushan")) {
			quate = "MADHUSHAN2-";
		}
		String out_final = quate + id_out;
		return out_final;

	}

	/* Common generate seriel specific serial number */
	public String genSerirlSpecificSerielNumber(int count) throws InvalidFormatException, IOException {
		String readed_id = (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")).substring(4);
		int id = Integer.parseInt(readed_id);
		id += count;
		String id_out = Integer.toString(id);
		writeIWData("SerirlSpecificSerielNumber", id_out, 12);
		String quate = "MADHUSHAN7-";
		if (user.equals("Madhushan")) {
			quate = "MADHUSHAN2-";
		}

		String out_final = quate + id_out;
		return out_final;

	}

	/* Common generate seriel batch specific serial number */
	public String genSerielBatchSpecificSerielNumber(int count) throws InvalidFormatException, IOException {
		String readed_id = (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")).substring(4);
		int id = Integer.parseInt(readed_id);
		id += count;
		String id_out = Integer.toString(id);
		writeIWData("SerirlBatchSpecificSerielNumber", id_out, 13);
		String quate = "MADHUSHAN7-";
		if (user.equals("Madhushan")) {
			quate = "MADHUSHAN2-";
		}
		String out_final = quate + id_out;
		return out_final;

	}

	/* Common generate serial batch fifo number */
	public String genSerielBatchFifoNumber(int count) throws InvalidFormatException, IOException {
		String readed_id = (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")).substring(4);
		int id = Integer.parseInt(readed_id);
		id += count;
		String id_out = Integer.toString(id);
		writeIWData("SerirlBatchFifoSerielNumber", id_out, 14);
		String quate = "MADHUSHAN7-";
		if (user.equals("Madhushan")) {
			quate = "MADHUSHAN2-";
		}
		String out_final = quate + id_out;
		return out_final;

	}

	/* Common generate seriel fifo serial number */
	public String genSerielFifoSerielNumber(int count) throws InvalidFormatException, IOException {
		String readed_id = (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")).substring(4);
		int id = Integer.parseInt(readed_id);
		id += count;
		String id_out = Integer.toString(id);
		writeIWData("SerirlFifoSerielNumber", id_out, 15);
		String quate = "MADHUSHAN7-";
		if (user.equals("Madhushan")) {
			quate = "MADHUSHAN2-";
		}
		String out_final = quate + id_out;
		return out_final;
	}

	/* Common generate batch specific serial number IW_TC_012 */
	public String genBatchSpecificSerielNumberIW_TC_012(int count) throws InvalidFormatException, IOException {
		String readed_id = readIWData(19);
		int id = Integer.parseInt(readed_id);
		id += count;
		String id_out = Integer.toString(id);
		String out_final = "MADHUSHAN/ILO-" + id_out;
		writeIWData("BatchSpecificSerielNumber/ILO", id_out, 19);
		return out_final;
	}

	/* Common generate batch fifo serial number IW_TC_012 */
	public String genBatchFifoSerielNumberIW_TC_012(int count) throws InvalidFormatException, IOException {
		String readed_id = readIWData(20);
		int id = Integer.parseInt(readed_id);
		id += count;
		String id_out = Integer.toString(id);
		String out_final = "MADHUSHAN/ILO-" + id_out;
		writeIWData("BatchFifoSerielNumber/ILO", id_out, 20);
		return out_final;

	}

	/* Common generate seriel specific serial number IW_TC_012 */
	public String genSerirlSpecificSerielNumberIW_TC_012(int count) throws InvalidFormatException, IOException {
		String readed_id = readIWData(21);
		int id = Integer.parseInt(readed_id);
		id += count;
		String id_out = Integer.toString(id);
		String out_final = "MADHUSHAN/ILO-" + id_out;
		writeIWData("SerirlSpecificSerielNumber/ILO", id_out, 21);
		return out_final;

	}

	/* Common generate seriel batch specific serial number IW_TC_012 */
	public String genSerielBatchSpecificSerielNumberIW_TC_012(int count) throws InvalidFormatException, IOException {
		String readed_id = readIWData(22);
		int id = Integer.parseInt(readed_id);
		id += count;
		String id_out = Integer.toString(id);
		String out_final = "MADHUSHAN/ILO-" + id_out;
		writeIWData("SerirlBatchSpecificSerielNumber/ILO", id_out, 22);
		return out_final;

	}

	/* Common generate serial batch fifo number IW_TC_012 */
	public String genSerielBatchFifoNumberIW_TC_012(int count) throws InvalidFormatException, IOException {
		String readed_id = readIWData(23);
		int id = Integer.parseInt(readed_id);
		id += count;
		String id_out = Integer.toString(id);
		String out_final = "MADHUSHAN/ILO-" + id_out;
		writeIWData("SerirlBatchFifoSerielNumber/ILO", id_out, 23);
		return out_final;

	}

	/* Common generate seriel fifo serial number IW_TC_012 */
	public String genSerielFifoSerielNumberIW_TC_012(int count) throws InvalidFormatException, IOException {
		String readed_id = readIWData(24);
		int id = Integer.parseInt(readed_id);
		id += count;
		String id_out = Integer.toString(id);
		String out_final = "MADHUSHAN/ILO-" + id_out;
		writeIWData("SerirlFifoSerielNumber/ILO", id_out, 24);
		return out_final;
	}

	/* Get ongoing serial specific serial number */
	public String getOngoinfSerirlSpecificSerielNumber(int count) throws InvalidFormatException, IOException {
		String readed_id = readIWData(12);
		String quate = "MADHUSHAN7-";
		if (user.equals("Madhushan")) {
			quate = "MADHUSHAN2-";
		}
		String out_final = quate + readed_id;
		int id = Integer.parseInt(readed_id);
		int id_next = id + count;
		String id_next_str = Integer.toString(id_next);
		String out_final_write_next = id_next_str;
		writeIWData("SerialSpecificSerielNumber", out_final_write_next, 12);
		return out_final;

	}

	/* Get ongoing serial batch specific serial number */
	public String getOngoingSerielBatchSpecificSerielNumber(int count) throws InvalidFormatException, IOException {
		String readed_id = readIWData(13);
		String quate = "MADHUSHAN7-";
		if (user.equals("Madhushan")) {
			quate = "MADHUSHAN2-";
		}
		String out_final = quate + readed_id;
		int id = Integer.parseInt(readed_id);
		int id_next = id + count;
		String id_next_str = Integer.toString(id_next);
		String out_final_write_next = id_next_str;
		writeIWData("SerialBatchSpecificSerielNumber", out_final_write_next, 13);
		return out_final;

	}

	/* Get ongoing serial batch specific serial number */
	public String getOngoingSerielBatchFifoSerielNumber(int count) throws InvalidFormatException, IOException {
		String readed_id = readIWData(14);
		String quate = "MADHUSHAN7-";
		if (user.equals("Madhushan")) {
			quate = "MADHUSHAN2-";
		}
		String out_final = quate + readed_id;
		int id = Integer.parseInt(readed_id);
		int id_next = id + count;
		String id_next_str = Integer.toString(id_next);
		String out_final_write_next = id_next_str;
		writeIWData("SerialBatchFifoSerielNumber", out_final_write_next, 14);
		return out_final;

	}

	/* Common stock adjustment for add quantity */
	public void addQuantityThroughStockAdjustment() throws Exception {
		actionVerificationLogin();
		menufacturerCommon();
		createCommonProducts();
		customizeLoadingDelay(navigation_pane, 50);
		Thread.sleep(3000);
		customizeLoadingDelay(navigation_pane, 20);
		click(navigation_pane);
		customizeLoadingDelay(inventory_module, 40);
		click(inventory_module);
		customizeLoadingDelay(btn_stockAdjustment, 40);
		click(btn_stockAdjustment);
		customizeLoadingDelay(btn_newStockAdjustment, 40);
		click(btn_newStockAdjustment);

		sendKeys(txt_descStockAdj, descStockAdj);

		selectText(dropdown_warehouseStockAdj, warehouseStockAdj);

		/* Product add to the grid */
		int j = 0;
		for (int i = 1; i < 8; i++) {
			String str_i = String.valueOf(i);
			click(btn_productLookupStockAdjForLoop.replace("row", str_i));
			Thread.sleep(3000);
			customizeLoadingDelay(txt_productSearch, 15);

			String product = "";

			if (i == 1) {
				product = "BatchSpecific";
			} else if (i == 2) {
				product = "BatchFifo";
			} else if (i == 3) {
				product = "Lot";
			} else if (i == 4) {
				product = "SerielSpecific";
			} else if (i == 5) {
				product = "SerielBatchSpecific";
			} else if (i == 6) {
				product = "SerielBatchFifo";
			} else if (i == 7) {
				product = "SerirlFifo";
			} else {
				System.out.println("Product type cannt identified.........");
			}

			handeledSendKeys(txt_productSearch, readTestCreation(product));
			pressEnter(txt_productSearch);
			String product_path = result_cpmmonProduct.replace("product", readTestCreation(product));
			customizeLoadingDelay(product_path, 30);
			doubleClick(product_path);
			Thread.sleep(1500);
			String txt_qty = txt_qtyProductGridStockAdjustment.replace("row", str_i);
			String txt_cost = txt_costProductGridStockAdjustment.replace("row", str_i);

			sendKeys(txt_qty, productQuantity1);

			sendKeys(txt_cost, productCost1);
			sleepCusomized(btn_avilabilityCheckWidget);

			sleepCusomized(btn_addNewRow);
			click(btn_addNewRow);
		}

		click(btn_deleteEmotyRowStockAdjustment);

		click(btn_draftActionVerification);
		customizeLoadingDelay(brn_serielBatchCapture, 40);
		click(brn_serielBatchCapture);

		/* Product 01 */
		Thread.sleep(8000);
		String item_xpath;
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "4");
		click(item_xpath);
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "1");
		Actions action = new Actions(driver);
		WebElement we = driver.findElement(By.xpath(item_xpath));
		action.moveToElement(we).build().perform();
		click(item_xpath);
		sendKeys(txt_batchNumberCaptureILOOutboundShipment, genBatchSpecificSerielNumber(200));
		sendKeys(txt_lotNumberStockAdjustment,
				genProductId() + currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_expiryDateStockAdjustmentProductCapture);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		String date = getAttribute(txt_expiryDateStockAdjustmentProductCapture, "value");
		writeRegression01("ProductExpiryDate_StockAdjustment", date, 38);
		pressEnter(txt_lotNumberStockAdjustment);
		Thread.sleep(1000);
		customizeLoadingDelay(btn_updateCapture, 50);
		click(btn_updateCapture);
		customizeLoadingDelay((item_xpath + span_afterCaptureSerialGreen), 30);

		/* Product 02 */
		Thread.sleep(3000);
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "2");
		WebElement we1 = driver.findElement(By.xpath(item_xpath));
		action.moveToElement(we1).build().perform();
		click(item_xpath);

		sendKeys(txt_batchNumberCaptureILOOutboundShipment, genBatchFifoSerielNumber(200));
		sendKeys(txt_lotNumberStockAdjustment,
				genProductId() + currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_expiryDateStockAdjustmentProductCapture);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_lotNumberStockAdjustment);
		Thread.sleep(1000);
		customizeLoadingDelay(btn_updateCapture, 50);
		click(btn_updateCapture);
		customizeLoadingDelay((item_xpath + span_afterCaptureSerialGreen), 30);

		/* Product 04 */
		Thread.sleep(3000);
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "4");
		WebElement we2 = driver.findElement(By.xpath(item_xpath));
		action.moveToElement(we2).build().perform();
		click(item_xpath);

		click(chk_productCaptureRange);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$('#attrSlide-txtSerialCount').val(200)");

		String lot4 = genSerirlSpecificSerielNumber(200);
		String final_lot_script = "$('#attrSlide-txtSerialFrom').val(\"" + lot4 + "\")";
		jse.executeScript(final_lot_script);
		Thread.sleep(1000);
		sendKeys(txt_searielLotNo, genProductId() + currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_dateSerielProductProductCapturePage);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_dateSerielProductProductCapturePage);
		Thread.sleep(1000);
		customizeLoadingDelay(btn_updateCapture, 50);
		click(btn_updateCapture);
		customizeLoadingDelay((item_xpath + span_afterCaptureSerialGreen), 30);

		/* Product 05 */
		Thread.sleep(3000);
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "5");
		WebElement we3 = driver.findElement(By.xpath(item_xpath));
		action.moveToElement(we3).build().perform();
		click(item_xpath);

		click(chk_productCaptureRange);
		jse.executeScript("$('#attrSlide-txtSerialCount').val(200)");

		String lot41 = genSerielBatchSpecificSerielNumber(200);
		String final_lot_script1 = "$('#attrSlide-txtSerialFrom').val(\"" + lot41 + "\")";
		jse.executeScript(final_lot_script1);

		sendKeys(txt_searielLotNo, genProductId() + currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));

		sendKeys(txt_serielBatchProductCaptureStockAdjustment,
				genProductId() + currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));

		click(txt_dateSerielProductProductCapturePage);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_dateSerielProductProductCapturePage);
		Thread.sleep(1000);
		customizeLoadingDelay(btn_updateCapture, 50);
		click(btn_updateCapture);
		customizeLoadingDelay((item_xpath + span_afterCaptureSerialGreen), 30);

		/* Product 06 */
		Thread.sleep(2000);
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "6");
		WebElement we4 = driver.findElement(By.xpath(item_xpath));
		action.moveToElement(we4).build().perform();

		click(item_xpath);
		sleepCusomized(item_xpath);

		click(chk_productCaptureRange);
		jse.executeScript("$('#attrSlide-txtSerialCount').val(200)");

		String lot411 = genSerielBatchFifoNumber(200);
		String final1_lot_script1 = "$('#attrSlide-txtSerialFrom').val(\"" + lot411 + "\")";
		jse.executeScript(final1_lot_script1);

		sendKeys(txt_searielLotNo, genProductId() + currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));

		sendKeys(txt_serielBatchProductCaptureStockAdjustment,
				genProductId() + currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));

		click(txt_dateSerielProductProductCapturePage);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_dateSerielProductProductCapturePage);
		Thread.sleep(1000);
		customizeLoadingDelay(btn_updateCapture, 50);
		click(btn_updateCapture);
		customizeLoadingDelay((item_xpath + span_afterCaptureSerialGreen), 30);

		/* Product 07 */
		Thread.sleep(2000);
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "7");
		WebElement we5 = driver.findElement(By.xpath(item_xpath));
		action.moveToElement(we5).build().perform();

		click(item_xpath);

		customizeLoadingDelay(chk_productCaptureRange, 20);
		click(chk_productCaptureRange);
		jse.executeScript("$('#attrSlide-txtSerialCount').val(200)");

		String lot4111 = genSerielFifoSerielNumber(200);
		String final1_lot_script11 = "$('#attrSlide-txtSerialFrom').val(\"" + lot4111 + "\")";
		jse.executeScript(final1_lot_script11);

		sendKeys(txt_searielLotNo, genProductId() + currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));

		click(txt_dateSerielProductProductCapturePage);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_dateSerielProductProductCapturePage);
		Thread.sleep(1000);
		customizeLoadingDelay(btn_updateCapture, 50);
		click(btn_updateCapture);
		customizeLoadingDelay((item_xpath + span_afterCaptureSerialGreen), 30);

		Thread.sleep(3000);
		click(btn_productCaptureBackButton);
		Thread.sleep(1000);
		click(btn_relese);
		customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 60);
	}

	/* IW_TC_018 */
	public void navigateToNewSalesOrder() throws Exception {
		FinanceModule obj = new FinanceModule();
		obj.commonLogin();
		implisitWait(15);
		click(navigation_pane);
		implisitWait(10);
		if (isDisplayed(inventory_module)) {
			writeTestResults("Verify user can navigate to navigation menu and able to view authorized modules ",
					"user should be able to navigate to navigation menu and able to view authorized modules",
					"User successfully navigate to navigation menu and able to view authorized modules", "pass");
		} else {
			writeTestResults("Verify user can navigate to navigation menu and able to view authorized modules ",
					"user should be able to navigate to navigation menu and able to view authorized modules",
					"User couldn't navigate to navigation menu and able to view authorized modules", "fail");
		}

		click(btn_salesModule);
		if (isDisplayed(btn_salesOrder)) {
			writeTestResults("Verify user can navigate to sales modules ",
					"user should be able to navigate to sales modules", "User successfully navigate to sales modules",
					"pass");
		} else {
			writeTestResults("Verify user can navigate to sales modules ",
					"user should be able to navigate to sales modules", "User couldn't navigate to sales modules",
					"fail");
		}

		click(btn_salesOrder);
		if (isDisplayed(btn_newSalesOrder)) {
			writeTestResults("Verify user can navigate to sales Sales Order by-page ",
					"user should be able to navigate to sales Sales Order by-page",
					"User successfully navigate to sales Sales Order by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to sales Sales Order by-page ",
					"user should be able to navigate to sales Sales Order by-page",
					"User couldn't navigate to sales Sales Order by-page", "fail");
		}

		click(btn_newSalesOrder);
		if (isDisplayed(btn_salesOrderToSalesInvoiceJourney)) {
			writeTestResults("Verify user can see Sales Order to Sales Invoice journey",
					"user should be able to see Sales Order to Sales Invoice journey",
					"User successfully see Sales Order to Sales Invoice journey", "pass");
		} else {
			writeTestResults("Verify user can see Sales Order to Sales Invoice journey ",
					"user should be able to see Sales Order to Sales Invoice journey",
					"User couldn't see Sales Order to Sales Invoice journey", "fail");
		}

		click(btn_salesOrderToSalesInvoiceJourney);
		if (isDisplayed(header_salesOrder)) {
			writeTestResults("Verify user can navigate to new Sales Order form",
					"user should be able to navigate to new Sales Order form",
					"User successfully navigate to new Sales Order form", "pass");
		} else {
			writeTestResults("Verify user can navigate to new Sales Order form ",
					"user should be able to navigate to new Sales Order form",
					"User couldn't navigate to new Sales Order form", "fail");
		}
	}

	public void fillSalesOrderwithAssemblyProduct() throws Exception {
		click(btn_lookupCustomerAccount);
		sleepCusomized(txt_searchCustomerSalesOrder);
		sendKeys(txt_searchCustomerSalesOrder, customerAccountSAlesOrder);
		pressEnter(txt_searchCustomerSalesOrder);
		doubleClick(lnk_resultCustomerAccount);
		Thread.sleep(1500);
		String selected_customer = driver.findElement(By.xpath(txt_customerAccountOnInterface)).getAttribute("value");
		String selected_customer_sub = selected_customer.substring(0, 15);
		if (selected_customer_sub.equals(customerAccountSAlesOrder)) {
			writeTestResults("Verify user can chosse relevent customer account from the lookup",
					"user should be able to chosse relevent customer account from the lookup",
					"User successfully chosse relevent customer account from the lookup", "pass");
		} else {
			writeTestResults("Verify user can chosse relevent customer account from the lookup ",
					"user should be able to chosse relevent customer account from the lookup",
					"User couldn't chosse relevent customer account from the lookup", "fail");
		}

		selectText(drop_curencySalesOrder, currencySalesOrder);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_curencySalesOrder)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(currencySalesOrder)) {
			writeTestResults("Verify user can select currency type", "User should be able to select currency type",
					"User successfully select currency type", "pass");
		} else {
			writeTestResults("Verify user can select currency type ", "User should be able to select currency type",
					"User couldn't select currency type", "fail");
		}
		String selected_billing_address = driver.findElement(By.xpath(txt_biilingAdressFrontPage))
				.getAttribute("value");
		String selected_shipping_address = driver.findElement(By.xpath(txt_shippingAddressFrontPage))
				.getAttribute("value");
		if ((!selected_billing_address.equals("")) && (!selected_shipping_address.equals(""))) {
			writeTestResults(
					"Verify shipping address and billing address were seletcted according to the selected customer",
					"Shipping address and billing address should be seletcted according to the selected customer",
					"Shipping address and billing address successfully seletcted according to the selected customer",
					"pass");
		} else {
			writeTestResults(
					"Verify shipping address and billing address were seletcted according to the selected customer",
					"Shipping address and billing address should be seletcted according to the selected customer",
					"Shipping address and billing address doesn't seletcted according to the selected customer",
					"fail");
		}

		selectText(drop_salesUnitSalesOrder, salesUnitSalesOrder);
		Select comboBox1 = new Select(driver.findElement(By.xpath(drop_salesUnitSalesOrder)));
		String selectedComboValue1 = comboBox1.getFirstSelectedOption().getText();
		if (selectedComboValue1.equals(salesUnitSalesOrder)) {
			writeTestResults("Verify user can select sales unit", "User should be able to select sales unit",
					"User successfully select sales unit", "pass");
		} else {
			writeTestResults("Verify user can select sales unit ", "User should be able to select sales unit",
					"User couldn't select sales unit", "fail");
		}

		Select comboBox12 = new Select(driver.findElement(By.xpath(drop_accountOrner)));
		String selectedComboValue12 = comboBox12.getFirstSelectedOption().getText();
		if (!selectedComboValue12.equals("")) {
			writeTestResults("Verify account owner was seletcted according to the selected sales unit",
					"Account owner should be seletcted according to the selected sales unit",
					"Account owner successfully seletcted according to the selected sales unit", "pass");
		} else {
			writeTestResults("Verify account owner was seletcted according to the selected sales unit",
					"Account owner should be seletcted according to the selected sales unit",
					"Account owner doesn't seletcted according to the selected sales unit", "fail");
		}

		click(btn_productLoockup);
		sleepCusomized(txt_productSearch);
		sendKeys(txt_productSearch, assemblyProductSalesOrder);
		pressEnter(txt_productSearch);
		if (isDisplayed(resul_assemblyProductProductSearchSalesOrder)) {
			writeTestResults("Verify user can select Assembly Product to the product grid",
					"User should be able to select Assembly Product to the product grid",
					"User successfully select Assembly Product to the product grid", "pass");
		} else {
			writeTestResults("Verify user can select Assembly Product to the product grid",
					"User should be able to select Assembly Product to the product grid",
					"User couldn't select Assembly Product to the product grid", "fail");
		}
		doubleClick(resul_assemblyProductProductSearchSalesOrder);

		Thread.sleep(2000);
		sendKeys(txt_quantitySalesOrder, quantitySalesOrder);
		Thread.sleep(1000);
		String selected_qty = driver.findElement(By.xpath(txt_quantitySalesOrder)).getAttribute("value");
		if (selected_qty.equals(quantitySalesOrder)) {
			writeTestResults("Verify user can set quantity", "User should be able to set quantity",
					"User successfully set quantity", "pass");
		} else {
			writeTestResults("Verify user can set quantity", "User should be able to set quantity",
					"User couldn't set quantity", "fail");
		}

		selectText(drop_warehouseProductGridSalesOrder, warehouseSalesOrder);
		Thread.sleep(1000);
		Select comboBox123 = new Select(driver.findElement(By.xpath(drop_warehouseProductGridSalesOrder)));
		String selectedComboValue123 = comboBox123.getFirstSelectedOption().getText();
		if (selectedComboValue123.equals(warehouseSalesOrder)) {
			writeTestResults("Verify user can select warehouse", "User should be able to select warehouse",
					"User successfully select warehouse", "pass");
		} else {
			writeTestResults("Verify user can select warehouse", "User should be able to select warehouse",
					"User couldn't select warehouse", "fail");
		}

		click(btn_checkout);
		Thread.sleep(2000);

	}

	public void navigateToCostEstimationSalesOrder() throws Exception {
		explicitWait(btn_actionSalesOrder, 40);
		click(btn_actionSalesOrder);
		explicitWait(btn_costEstimation, 20);
		if (isDisplayed(btn_costEstimation)) {
			writeTestResults("Verify Cost Estimation icon is displayed under action",
					"Cost Estimation icon should be displayed under action",
					"Cost Estimation icon successfully displayed under action", "pass");
		} else {
			writeTestResults("Verify Cost Estimation icon is displayed under action",
					"Cost Estimation icon should be displayed under action",
					"Cost Estimation icon couldn't displayed under action", "fail");
		}

		click(btn_costEstimation);
		if (isDisplayed(btn_essambyEstimationSalesOrder)) {
			writeTestResults("Verify product list which exist in Sales Order were loaded",
					"Product list which exist in Sales Order should be loaded",
					"Product list which exist in Sales Order successfully loaded", "pass");
		} else {
			writeTestResults("Verify product list which exist in Sales Order were loaded",
					"Product list which exist in Sales Order should be loaded",
					"Product list which exist in Sales Order doesn't loaded", "fail");
		}

		sleepCusomized(btn_essambyEstimationSalesOrder);
		Actions action = new Actions(driver);
		WebElement we = driver.findElement(By.xpath(btn_essambyEstimationSalesOrder));
		action.click(we).build().perform();
		if (isDisplayed(btn_addNewCostEstimationSalesOrder)) {
			writeTestResults("Verify Assembly Estimation searching window was displayed",
					"Assembly Estimation searching window will be displayed",
					"Assembly Estimation searching window successfully displayed", "pass");
		} else {
			writeTestResults("Verify Assembly Estimation searching window was displayed",
					"Assembly Estimation searching window will be displayed",
					"Assembly Estimation searching window doesn't displayed", "fail");
		}

		sleepCusomized(btn_addNewCostEstimationSalesOrder);
		click(btn_addNewCostEstimationSalesOrder);

		sleepCusomized(txt_productAssemblyCostEstimationAssemblyProcess);
		String product = getAttribute(txt_productAssemblyCostEstimationAssemblyProcess, "disabled");

		String customer = getAttribute(txt_customerAssemblyCostEstimationAssemblyProcess, "disabled");

		String header = getText(header_assemblyCostEstimation);

		if (product.equals("true") && customer.equals("true") && header.equals("Assembly Cost Estimation")) {
			writeTestResults(
					"Verify user can navigated to Assembly Cost Estimation Form with the Assembly proudct and Customer",
					"User should be navigated to Assembly Cost Estimation Form with the Assembly proudct and Customer",
					"User successfully navigated to Assembly Cost Estimation Form with the Assembly proudct and Customer",
					"pass");
		} else {
			writeTestResults(
					"Verify user can navigated to Assembly Cost Estimation Form with the Assembly proudct and Customer",
					"User should be navigated to Assembly Cost Estimation Form with the Assembly proudct and Customer",
					"User couldn't navigated to Assembly Cost Estimation Form with the Assembly proudct and Customer",
					"fail");
		}
	}

	public void fillAssemblyCostEstimationAssemblyProcess() throws Exception {
		sendKeys(txt_descriptonAssemblyCostEstimationAssemblyProcess, descriptionAssemblyCostEstimation);

		if (isDisplayed(txt_descriptonAssemblyCostEstimationAssemblyProcess)) {
			writeTestResults("Verify added discription is avilable", "Added description should be available",
					"Added description sucessfully available", "pass");
		} else {
			writeTestResults("Verify added discription is avilable", "Added description should be available",
					"Added description does'nt available", "fail");
		}

		click(btn_searchPricingProfileAssemblyCostEstimationAssemblyProcess);
		sendKeys(txt_pricingProfileACE, pricingProfileACE);
		pressEnter(txt_pricingProfileACE);
		if (isDisplayed(lnk_pricingProfileACE)) {
			writeTestResults("Verify user can add 'Pricing Profile'", "User should be able to add 'Pricing Profile'",
					"User successfully add a 'Pricing Profile'", "pass");
		} else {
			writeTestResults("Verify user can add 'Pricing Profile'", "User should be able to add 'Pricing Profile'",
					"User couldn't add a 'Pricing Profile'", "fail");
		}
		doubleClick(lnk_pricingProfileACE);

		click(btn_tabEstimationDeatailsAseemblyCostEstmationAssemblyProcess);

		if (isDisplayed(drop_typeACE1.replace("commonTbl", "tblQESTcommon"))) {
			writeTestResults("Verify estimation details are loaded", "Estimation details should be loaded",
					"Estimation details successfully loaded", "pass");
		} else {
			writeTestResults("Verify estimation details are loaded", "Estimation details should be loaded",
					"Estimation details couldn't loaded", "fail");
		}

		/* Row 1 */
		Select comboBox = new Select(driver.findElement(By.xpath(drop_typeACE1.replace("commonTbl", "tblQESTcommon"))));
		selectText(drop_typeACE1.replace("commonTbl", "tblQESTcommon"), typeEstmationAce1);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		if (selectedComboValue.equals(typeEstmationAce1)) {
			writeTestResults("Verify user can select type as Input Product",
					"User should be able to select type as Input Product",
					"User successfully select type as Input Product", "pass");
		} else {
			writeTestResults("Verify user can select type as Input Product",
					"User should be able to select type as Input Product", "User couldn't select type as Input Product",
					"fail");
		}

		click(btn_searchProductEstmationAce1);
		Thread.sleep(2000);
		sleepCusomized(txt_productSearch);
		sendKeys(txt_productSearch, product1EstimationDeatailsACE);
		pressEnter(txt_productSearch);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,-200)");
		sleepCusomized(btn_resultProduct);
		if (isDisplayed(lnk_resultInputProductAssemblyProcessCostEstimation)) {
			writeTestResults("Verify user can select product", "User should be able to select product",
					"User successfully select product", "pass");
		} else {
			writeTestResults("Verify user can select product", "User should be able to select product",
					"User couldn't select product", "fail");
		}
		doubleClick(lnk_resultInputProductAssemblyProcessCostEstimation);
		final String tbl_cost_estmation = tbl_costEstimationProductAssemblyProcess;
		sendKeys(tbl_cost_estmation.replace("row", "1").replace("column", "14"), qtyInputProduct1ACE);
		sendKeys(tbl_cost_estmation.replace("row", "1").replace("column", "16"), costInputProduct1ACE);

		if (!isSelected(tbl_cost_estmation.replace("row", "1").replace("column", "18"))) {
			click(tbl_cost_estmation.replace("row", "1").replace("column", "18"));
		}
		if (isDisplayed(tbl_costEstimationProductAssemblyProcess.replace("row", "1").replace("column", "14"))
				&& isDisplayed(tbl_costEstimationProductAssemblyProcess.replace("row", "1").replace("column", "16"))
				&& isSelected(tbl_cost_estmation.replace("row", "1").replace("column", "18"))) {
			writeTestResults("Verify user can add Estimated Qty and Cost with enabling Billable check box",
					"User should be able to add Estimated Qty and Cost with enabling Billable check box",
					"User successfully add Estimated Qty and Cost with enabling Billable check box", "pass");
		} else {
			writeTestResults("Verify user can add Estimated Qty and Cost with enabling Billable check box",
					"User should be able to add Estimated Qty and Cost with enabling Billable check box",
					"User couldn't add Estimated Qty and Cost with enabling Billable check box", "fail");
		}

		/* Row 02 */
		click(btn_adnewRow1ACE.replace("commonTbl", "tblQESTcommon"));
		if (isDisplayed(drop_typeACE2.replace("commonTbl", "tblQESTcommon"))) {
			writeTestResults("Verify new line is enabled", "New line should be enabled",
					"New line successfully enabled", "pass");
		} else {
			writeTestResults("Verify new line is enabled", "New line should be enabled", "New line couldn't enabled",
					"fail");
		}

		Select comboBox1 = new Select(
				driver.findElement(By.xpath(drop_typeACE2.replace("commonTbl", "tblQESTcommon"))));
		selectText(drop_typeACE2.replace("commonTbl", "tblQESTcommon"), typeEstmationAce2);
		String selectedComboValue1 = comboBox1.getFirstSelectedOption().getText();

		if (selectedComboValue1.equals(typeEstmationAce2)) {
			writeTestResults("Verify user can select type as Output Product",
					"User should be able to select type as Output Product",
					"User successfully select type as Output Product", "pass");
		} else {
			writeTestResults("Verify user can select type as Output Product",
					"User should be able to select type as Output Product",
					"User couldn't select type as Output Product", "fail");
		}

		click(btn_searchProduct2ACE);
		sleepCusomized(txt_productSearch);
		sendKeys(txt_productSearch, product2EstimationDeatailsACE);
		pressEnter(txt_productSearch);
		Thread.sleep(4000);
		if (isDisplayed(btn_resultProduct)) {
			writeTestResults("Verify user can select product", "User should be able to select product",
					"User successfully select product", "pass");
		} else {
			writeTestResults("Verify user can select product", "User should be able to select product",
					"User couldn't select product", "fail");
		}
		sleepCusomized(btn_resultProduct);
		doubleClick(btn_resultProduct);
		Thread.sleep(2000);
		sendKeys(tbl_cost_estmation.replace("row", "2").replace("column", "14"), qtyInputProduct2ACE);
		sendKeys(tbl_cost_estmation.replace("row", "2").replace("column", "16"), costInputProduct2ACE);
		if (!isSelected(tbl_cost_estmation.replace("row", "2").replace("column", "18"))) {
			click(tbl_cost_estmation.replace("row", "2").replace("column", "18"));
		}
		if (isDisplayed(tbl_cost_estmation.replace("row", "2").replace("column", "14"))
				&& isDisplayed(tbl_cost_estmation.replace("row", "2").replace("column", "16"))
				&& isSelected(tbl_cost_estmation.replace("row", "2").replace("column", "18"))) {
			writeTestResults("Verify user can add Estimated Qty and Cost with enabling Billable check box",
					"User should be able to add Estimated Qty and Cost with enabling Billable check box",
					"User successfully add Estimated Qty and Cost with enabling Billable check box", "pass");
		} else {
			writeTestResults("Verify user can add Estimated Qty and Cost with enabling Billable check box",
					"User should be able to add Estimated Qty and Cost with enabling Billable check box",
					"User couldn't add Estimated Qty and Cost with enabling Billable check box", "fail");
		}

		/* Row 3 */
		click(btn_adaNewRow3.replace("commonTbl", "tblQESTcommon"));

		if (isDisplayed(drop_typeExpenceACE.replace("commonTbl", "tblQESTcommon"))) {
			writeTestResults("Verify new line is enabled", "New line should be enabled",
					"New line successfully enabled", "pass");
		} else {
			writeTestResults("Verify new line is enabled", "New line should be enabled", "New line couldn't enabled",
					"fail");
		}

		Select comboBox2 = new Select(
				driver.findElement(By.xpath(drop_typeExpenceACE.replace("commonTbl", "tblQESTcommon"))));
		selectText(drop_typeExpenceACE.replace("commonTbl", "tblQESTcommon"), typeEstmationAce3Expence);
		String selectedComboValue2 = comboBox2.getFirstSelectedOption().getText();

		if (selectedComboValue2.equals(typeEstmationAce3Expence)) {
			writeTestResults("Verify user can select type as Expense", "User should be able to select type as Expense",
					"User successfully select type as Expense", "pass");
		} else {
			writeTestResults("Verify user can select type as Expense", "User should be able to select type as Expense",
					"User couldn't select type as Expense", "fail");
		}

		selectIndex(drop_estiamationRow3ReferenceCodeExpenceACE.replace("commonTbl", "tblQESTcommon"), 2);

		if (isDisplayed(drop_estiamationRow3ReferenceCodeExpenceACE.replace("commonTbl", "tblQESTcommon"))) {
			writeTestResults("Verify user can select Reference Code", "User should be able to select Reference Code",
					"User successfully select Reference Code", "pass");
		} else {
			writeTestResults("Verify user can select Reference Code", "User should be able to select Reference Code",
					"User couldn't select Reference Code", "fail");
		}

		click(btn_searchServiceACE3ProductLookup);

		sleepCusomized(txt_productSearch);
		sendKeys(txt_productSearch, expenceService);
		pressEnter(txt_productSearch);
		sleepCusomized(btn_resultProduct);
		if (isDisplayed(btn_resultProduct)) {
			writeTestResults("Verify user can select Service", "User should be able to select Service",
					"User successfully select Service", "pass");
		} else {
			writeTestResults("Verify user can select Service", "User should be able to select Service",
					"User couldn't select Service", "fail");
		}
		sleepCusomized(btn_resultProduct);
		doubleClick(btn_resultProduct);
		Thread.sleep(1000);
		sendKeys(tbl_cost_estmation.replace("row", "3").replace("column", "16"), costInputService3ACE);
		if (!isSelected(tbl_cost_estmation.replace("row", "3").replace("column", "18"))) {
			click(tbl_cost_estmation.replace("row", "3").replace("column", "18"));
		}
		if (isDisplayed(tbl_cost_estmation.replace("row", "3").replace("column", "16"))
				&& isSelected(tbl_cost_estmation.replace("row", "3").replace("column", "18"))) {
			writeTestResults("Verify user can add Cost with enabling Billable check box",
					"User should be able to add Cost with enabling Billable check box",
					"User successfully add Cost with enabling Billable check box", "pass");
		} else {
			writeTestResults("Verify user can add Cost with enabling Billable check box",
					"User should be able to add Cost with enabling Billable check box",
					"User couldn't add Cost with enabling Billable check box", "fail");
		}
		/* ROW 4 */
		click(btn_addNewRow4EstmationDetailsACE.replace("commonTbl", "tblQESTcommon"));

		if (isDisplayed(drop_type4ServiceACE.replace("commonTbl", "tblQESTcommon"))) {
			writeTestResults("Verify new line is enabled", "New line should be enabled",
					"New line successfully enabled", "pass");
		} else {
			writeTestResults("Verify new line is enabled", "New line should be enabled", "New line couldn't enabled",
					"fail");
		}

		Select comboBox3 = new Select(
				driver.findElement(By.xpath(drop_type4ServiceACE.replace("commonTbl", "tblQESTcommon"))));
		selectText(drop_type4ServiceACE.replace("commonTbl", "tblQESTcommon"), typeEstmationAce4);
		String selectedComboValue3 = comboBox3.getFirstSelectedOption().getText();

		if (selectedComboValue3.equals(typeEstmationAce4)) {
			writeTestResults("Verify user can select type as Services",
					"User should be able to select type as Services", "User successfully select type as Services",
					"pass");
		} else {
			writeTestResults("Verify user can select type as Services",
					"User should be able to select type as Services", "User couldn't select type as Services", "fail");
		}

		click(btn_searchProductLookuop4ACE);

		sleepCusomized(txt_productSearch);
		sendKeys(txt_productSearch, serviceAssemblyProcessCostEstimation);
		pressEnter(txt_productSearch);
		sleepCusomized(btn_resultProduct);
		if (isDisplayed(btn_resultProduct)) {
			writeTestResults("Verify user can select Service", "User should be able to select Service",
					"User successfully select Service", "pass");
		} else {
			writeTestResults("Verify user can Select service", "User should be able to select Service",
					"User couldn't select Service", "fail");
		}
		sleepCusomized(btn_resultProduct);
		doubleClick(btn_resultProduct);

		sendKeys(tbl_cost_estmation.replace("row", "4").replace("column", "16"), costInputService4ACE);
		if (!isSelected(tbl_cost_estmation.replace("row", "4").replace("column", "18"))) {
			click(tbl_cost_estmation.replace("row", "4").replace("column", "18"));
		}
		if (isDisplayed(tbl_cost_estmation.replace("row", "4").replace("column", "16"))
				&& isSelected(tbl_cost_estmation.replace("row", "4").replace("column", "18"))) {
			writeTestResults("Verify user can add Cost with enabling Billable check box",
					"User should be able to add Cost with enabling Billable check box",
					"User successfully add Cost with enabling Billable check box", "pass");
		} else {
			writeTestResults("Verify user can add Cost with enabling Billable check box",
					"User should be able to add Cost with enabling Billable check box",
					"User couldn't add Cost with enabling Billable check box", "fail");
		}
	}

	public void checkoutAssemblyCostEstimationApplyToMainGrid() throws Exception {
		click(btn_summaryCostEstimationAssemblyProcess);
		if (isDisplayed(btn_checkoutACE)) {
			writeTestResults("Verify user can navigate to the Summary Tab",
					"User should be able navigate to the Summary Tab", "User successfully navigate to the Summary Tab",
					"pass");
		} else {
			writeTestResults("Verify user can navigate to the Summary Tab",
					"User should be able navigate to the Summary Tab", "User couldn't navigate to the Summary Tab",
					"fail");
		}

		Actions action = new Actions(driver);
		WebElement we = driver.findElement(By.xpath(btn_checkoutCostEstimationAssemblyProcess));
		action.click(we).build().perform();
		Thread.sleep(3000);
		String total_profit = getText(lbl_ttalProfitCostEstimationAssemblyProcess);

		if (!total_profit.equals("0.00000")) {
			writeTestResults("Verify user can checkout 'Assembly Cost Estimation'",
					"User should be able to checkout 'Assembly Cost Estimation'",
					"User successfully checkout 'Assembly Cost Estimation'", "pass");
		} else {
			writeTestResults("Verify user can checkout 'Assembly Cost Estimation'",
					"User should be able to checkout 'Assembly Cost Estimation'",
					"User couldn't checkout 'Assembly Cost Estimation'", "fail");
		}

		click(btn_genrateEstimationAssemblyProcess);
		sleepCusomized(btn_applyToMAinGridAssemblyProcess);
		if (isDisplayed(btn_applyToMAinGridAssemblyProcess)) {
			writeTestResults("Verify user can navigated to Product List window",
					"User should be able to navigated to Product List window",
					"User successfully navigated to Product List window", "pass");
		} else {
			writeTestResults("Verify user can navigated to Product List window",
					"User should be able to navigated to Product List window",
					"User couldn't navigated to Product List window", "fail");
		}

		Actions action1 = new Actions(driver);
		WebElement we1 = driver.findElement(By.xpath(btn_applyToMAinGridAssemblyProcess));
		action1.moveToElement(we1).build().perform();
		click(btn_applyToMAinGridAssemblyProcess);

		Thread.sleep(2000);
		sleepCusomized(lbl_esitimationTdAssemblyProcess);
		String estimation_doc = getText(lbl_esitimationTdAssemblyProcess);
		String unit_cost = getAttribute(lbl_unitCostAfterApplyCostEstimationAssmblyProcess, "disabled");

		if (!estimation_doc.equals("") && unit_cost.equals("true")) {
			writeTestResults("Verify estimation is added to the grid and unit price is similar to the confirmed income",
					"Estimation should be added to the grid and unit price should be similar to the confirmed income",
					"Estimation successfully added to the grid and unit price successfully similar to the confirmed income",
					"pass");
		} else {
			writeTestResults("Verify estimation is added to the grid and unit price is similar to the confirmed income",
					"Estimation should be added to the grid and unit price should be similar to the confirmed income",
					"Estimation doesn't added to the grid and unit price doesn't similar to the confirmed income",
					"fail");
		}

	}

	public void draftAndReleaseSalesOrder() throws Exception {
		click(btn_checkout);
		Thread.sleep(3000);
		String total_profit = getText(lbl_bannerTotalAssemblyProcess);

		if (!total_profit.equals("TOTAL (LKR)" + "0")) {
			writeTestResults("Verify user can checkout 'Assembly Cost Estimation'",
					"User should be able to checkout 'Assembly Cost Estimation'",
					"User successfully checkout 'Assembly Cost Estimation'", "pass");
		} else {
			writeTestResults("Verify user can checkout 'Assembly Cost Estimation'",
					"User should be able to checkout 'Assembly Cost Estimation'",
					"User couldn't checkout 'Assembly Cost Estimation'", "fail");
		}

		click(btn_draft);
		if (isDisplayed(btn_releese2)) {

			writeTestResults("Verify user can draft Sales Order", "User should be able to draft Sales Order",
					"User sucessfully draft Sales Ordert", "pass");
		} else {
			writeTestResults("Verify user can draft Sales Order", "User should be able to draft Sales Order",
					"User can't draft Sales Order", "Fail");

		}

		sleepCusomized(btn_releese2);
		click(btn_releese2);
		Thread.sleep(4000);
		trackCode = getText(lbl_creationsDocNo);
		if (isDisplayed(btn_okAssemblyProcess)) {
			writeTestResults("Verify user can releese Sales Order", "Sales Order should be releesed",
					"Sales Order successfully released", "pass");
		} else {
			writeTestResults("Verify user can releese Sales Order", "Sales Order should be releesed",
					"Sales Order doesn't releesed", "fail");
		}

		click(btn_okAssemblyProcess);
		if (isDisplayed(header_salesOrder)) {
			writeTestResults("Verify Sales Order Form is refresh", "Sales Order Form should be refresh",
					"Sales Order successfully refresh", "pass");
		} else {
			writeTestResults("Verify Sales Order Form is refresh", "Sales Order Form should be refresh",
					"Sales Order doesn't refresh", "fail");
		}
	}

	public void genarateAssemblyOrderAssemblyProcess() throws Exception {
		sleepCusomized(btn_action);
		click(btn_action);
		if (isDisplayed(btn_genarateAssemblyOrder)) {

			writeTestResults("Verify list of actions are loaded", "List of actions should be loaded",
					"List of actions successfully loaded", "pass");
		} else {
			writeTestResults("Verify list of actions are loaded", "List of actions should be loaded",
					"List of actions doesn't loaded", "fail");

		}

		click(btn_genarateAssemblyOrder);
		if (isDisplayed(header_popupAssemblyOrder)) {

			writeTestResults("Verify Generate Assembly Order tasks window is pop-up",
					"Generate Assembly Order tasks window should pop-up",
					"Generate Assembly Order Tasks window successfully pop-up", "pass");
		} else {
			writeTestResults("Verify Generate Assembly Order tasks window is pop-up",
					"Generate Assembly Order tasks window should pop-up",
					"Generate Assembly Order Tasks window doesn't pop-up", "fail");
		}

		customizeLoadingDelay(btn_taskAssignUserLookup, 20);
		Thread.sleep(1000);
		click(btn_taskAssignUserLookup);
		sleepCusomized(txt_userSearch);
		sendKeys(txt_userSearch, taskAssignUser);
		pressEnter(txt_userSearch);
		sleepCusomized(btn_resultUserNialPeeris);
		doubleClick(btn_resultUserNialPeeris);
		String user = getAttribute(txt_userPopupPageAssemblyOrerAssembluProcess, "value");
		if (user.equals(taskAssignUser)) {

			writeTestResults("Verify user can select task assign user",
					"User should be able to select task assign user", "User successfully select task assign user",
					"pass");
		} else {
			writeTestResults("Verify user can select task assign user",
					"User should be able to select task assign user", "User couldn't select task assign user", "fail");
		}

		click(chk_assemblyOrderAssemblyPopup);
		if (isSelected(chk_assemblyOrderAssemblyPopup)) {

			writeTestResults("Verify user can enable select check box",
					"User should be able to User should be able to enable select check box",
					"User successfully enable select check box", "pass");
		} else {
			writeTestResults("Verify user can enable select check box",
					"User should be able to enable select check box", "User couldn't enable select check box", "fail");
		}

		click(btn_applyAssemblyPopupAssemblyOrder);

		if (isDisplayed(header_salesOrder)) {
			writeTestResults("Verify Sales Order Form was reloaded", "Sales Order Form should be reloaded",
					"Sales Order Form successfully reloaded", "pass");
		} else {
			writeTestResults("Verify Sales Order Form was reloaded", "Sales Order Form should be reloaded",
					"Sales Order Form doesn't reloaded", "fail");
		}

		Thread.sleep(2000);
		writeIWData("Assembly Order No", getText(lbl_creationsDocNo), 16);

		click(btn_entutionLogo);
		if (isDisplayed(btn_taskTile)) {

			writeTestResults("Verify set of tiles were loaded", "Set of tiles should be loaded",
					"Set of tiles successfully loaded", "pass");
		} else {
			writeTestResults("Verify set of tiles were loaded", "Set of tiles should be loaded",
					"Set of tiles doesn't loaded", "fail");
		}

		click(btn_taskTile);
		if (isDisplayed(btn_assemblyOrderTile)) {

			writeTestResults("Verify user can navigated to task schedule", "User should be navigated to task schedule",
					"User successfully navigated to task schedule", "pass");
		} else {
			writeTestResults("Verify user can navigated to task schedule", "User should be navigated to task schedule",
					"User couldn't navigated to task schedule", "fail");
		}

		click(btn_assemblyOrderTile);

		if (isDisplayed(btn_arrowGoToTaskReleventAssemblyOrder.replace("doc_no", readIWData(16)))) {
			writeTestResults("Verify set of transactions were loaded", "Set of transactions should be loaded",
					"Set of transactions successfully loaded", "pass");
		} else {
			writeTestResults("Verify set of transactions were loaded", "Set of transactions should be loaded",
					"Set of transactions doesn't loaded", "fail");
		}

		click(btn_arrowGoToTaskReleventAssemblyOrder.replace("doc_no", readIWData(16)));
		switchWindow();
		String header = getText(header_pageLabel);
		if (header.equals("Assembly Order")) {
			writeTestResults(
					"Verify Assembly Order was loaded with the Product, Referance Doc, Customer which relevant to the Sales Order",
					"Assembly Order should be loaded with the Product, Referance Doc, Customer which relevant to the Sales Order",
					"Assembly Order successfully loaded with the Product, Referance Doc, Customer which relevant to the Sales Order",
					"pass");
		} else {
			;
			writeTestResults(
					"Verify Assembly Order was loaded with the Product, Referance Doc, Customer which relevant to the Sales Order",
					"Assembly Order should be loaded with the Product, Referance Doc, Customer which relevant to the Sales Order",
					"Assembly Order doesn't loaded with the Product, Referance Doc, Customer which relevant to the Sales Order",
					"fail");
		}

	}

	public void fillAssemblyOrderAssemblyProcess() throws Exception {
		selectIndex(drop_assemblyGroup, 0);
		if (isDisplayed(drop_assemblyGroup)) {
			writeTestResults("Verify user can select Assembly Group", "User should be able to select Assembly Group",
					"User successfully select Assembly Group", "pass");
		} else {
			writeTestResults("Verify user can select Assembly Group", "User should be able to select Assembly Group",
					"User couldn'nt select Assembly Group", "fail");
		}

		click(btn_emloyeeSerchAO);
		sleepCusomized(txt_employee);
		sendKeys(txt_employee, employee1);
		pressEnter(txt_employee);
		sleepCusomized(lnk_resultEmployee);
		if (isDisplayed(lnk_resultEmployee)) {
			writeTestResults("Verify user can select Responsible Employee",
					"User should be able to select Responsible Employee",
					"User successfully select Responsible Employee", "pass");
		} else {
			writeTestResults("Verify user can select Responsible Employee",
					"User should be able to select Responsible Employee", "User couldn'nt select Responsible Employee",
					"fail");
		}
		doubleClick(lnk_resultEmployee);

		sendKeys(txt_serialNoAO, genProductId() + currentTimeAndDate());

		selectIndex(drop_priorityAO, 1);
		if (isDisplayed(drop_priorityAO)) {
			writeTestResults("Verify user can select Priority", "User should be able to select Priority",
					"User successfully select Priority", "pass");
		} else {
			writeTestResults("Verify user can select Priority", "User should be able to select Priority",
					"User couldn'nt select Priority", "fail");
		}
		selectIndex(drop_barcodeAO, 1);

		click(btn_searchProductionUnit);
		sleepCusomized(txt_productionUnit);
		sendKeys(txt_productionUnit, productionUnit1);
		pressEnter(txt_productionUnit);
		sleepCusomized(lnk_resultProductUnit);
		if (isDisplayed(lnk_resultProductUnit)) {
			writeTestResults("Verify user can select Production Unit", "User should be able to select Production Unit",
					"User successfully select Production Unit", "pass");
		} else {
			writeTestResults("Verify user can select Production Unit", "User should be able to select Production Unit",
					"User couldn'nt select Production Unit", "fail");
		}
		doubleClick(lnk_resultProductUnit);

		Thread.sleep(3000);
		selectText(drop_warehouseWIPAO, warehouseWIPAO);
		selectText(drop_inputWareAO, warehouseAO);
		click("YES_Link");
		selectText(drop_wareOutAO, warehouseAO);

		if (isDisplayed(txt_serialNoAO)) {
			writeTestResults("Verify user can add New Serial Number", "User should be able to add New Serial Number",
					"User successfully add New Serial Number", "pass");
		} else {
			writeTestResults("Verify user can add New Serial Number", "User should be able to add New Serial Number",
					"User couldn't add New Serial Number", "fail");
		}

		click(date_firstClickAO);
		String _28 = date_secondSeclectioAO.replace("date", "28");
		Thread.sleep(2000);
		click(btn_clanderFw);
		click(_28);

		click(date_secondClickAO);
		Thread.sleep(2000);
		click(btn_clanderFw);
		click(_28);

		if (isDisplayed(date_firstClickAO) && isDisplayed(date_secondClickAO)) {
			writeTestResults("Verify user can select Requested Date and Requested End Date from the Calender",
					"User should be able to select Requested Date and Requested End Date from the Calender",
					"User successfully select Requested Date and Requested End Date from the Calender", "pass");
		} else {
			writeTestResults("Verify user can select Requested Date and Requested End Date from the Calender",
					"User should be able to select Requested Date and Requested End Date from the Calender",
					"User couldn't select Requested Date and Requested End Date from the Calender", "fail");
		}
	}

	public void productTabAndEstimationAssemblyProcess() throws Exception {
		click(tab_productAO);
		if (isDisplayed(drop_warehouseProductAO1)) {
			writeTestResults("Verify products which defined in Cost Estmation are loaded under Product Tab",
					"Products which defined in Cost Estmation should be loaded under Product Tab",
					"Products which defined in Cost Estmation successfully loaded under Product Tab", "pass");
		} else {
			writeTestResults("Verify products which defined in Cost Estmation are loaded under Product Tab",
					"Products which defined in Cost Estmation should be loaded under Product Tab",
					"Products which defined in Cost Estmation does'nt loaded under Product Tab", "fail");
		}

		selectText(drop_warehouseProductAO1, fromWareTO);

		if (isDisplayed(drop_warehouseProductAO1)) {
			writeTestResults("Verify user can select warehouses", "User should be able to select warehouses",
					"User successfully select warehouse", "pass");
		} else {
			writeTestResults("Verify user can select warehouses", "User should be able to select warehouses",
					"User couldn't select warehouse", "fail");
		}

		click(tab_estimationDeatailsAO);

		if (isDisplayed(btn_availabilityCheckForVErifyEstmationProductAO)) {
			writeTestResults("Verify that Service products details are loaded according to the Cost Estimation",
					"Service products details should be loaded according to the Cost Estimation",
					"Service products details are successfully loaded according to the Cost Estimation", "pass");
		} else {
			writeTestResults("Verify that Service products details are loaded according to the Cost Estimation",
					"Service products details should be loaded according to the Cost Estimation",
					"Service products details does'nt loaded according to the Cost Estimation", "fail");
		}
	}

	public void draftReleeseAssemblyOrderAssemblyProcess() throws Exception {
		draft("Assembly Order");
		relese("Assembly Order");
	}

	public void jobStartAndNavigateToInternelOrder() throws Exception {
		click(btn_action);
		if (isDisplayed(btn_jobStart)) {
			writeTestResults("Verify list of actions were loaded", "List of actions should be loaded",
					"List of actions successfully loaded", "pass");
		} else {
			writeTestResults("Verify list of actions were loaded", "List of actions should be loaded",
					"List of actions doesn't loaded", "fail");
		}

		click(btn_jobStart);
		if (isDisplayed(txt_timeStartAssemblyProcess)) {
			writeTestResults("Verify start process window is pop-up", "Start process window should pop-up",
					"Start process window successfully pop-up", "pass");
		} else {
			writeTestResults("Verify start process window is pop-up", "Start process window should pop-up",
					"Start process window doesn't pop-up", "fail");
		}

		if (isDisplayed(txt_timeStartAssemblyProcess) && isDisplayed(txt_timeEndAssemblyProcess)) {
			writeTestResults("User can select start time and end time",
					"User should be able to select start time and end time",
					"User successfully select start time and end time", "pass");
		} else {
			writeTestResults("User can select start time and end time",
					"User should be able to select start time and end time",
					"User couldn't select start time and end time", "fail");
		}

		click(btn_startAssemblyProcess);
		Thread.sleep(3000);
		String ststus = getText(lbl_statusAssemblyOrder);
		if (ststus.equals("0%")) {
			Thread.sleep(2000);
			ststus = getText(lbl_statusAssemblyOrder);
		}

		if (ststus.equals("RUNNING")) {
			writeTestResults("Verify Assembly Order Form status was changed as Running",
					"Assembly Order Form status should be changed as Running",
					"Assembly Order Form status successfully changed as Running", "pass");
		} else {
			writeTestResults("Verify Assembly Order Form status was changed as Running",
					"Assembly Order Form status should be changed as Running",
					"Assembly Order Form status doesn't changed as Running", "fail");
		}

		click(btn_action);
		if (isDisplayed(btn_cretaeInternelOrderAssemblyProcess)) {
			writeTestResults("Verify list of actions was loaded", "List of actions should be loaded",
					"List of actions successfully loaded", "pass");
		} else {
			writeTestResults("Verify list of actions was loaded", "List of actions should be loaded",
					"List of actions doesn't loaded", "fail");
		}

		click(btn_cretaeInternelOrderAssemblyProcess);
		if (isDisplayed(header_internelOrderPopup)) {
			writeTestResults("Verify Generate Internal Order window is pop-up with input products",
					"Generate Internal Order window should be pop-up with input products",
					"Generate Internal Order window successfully pop-up with input products", "pass");
		} else {
			writeTestResults("Verify Generate Internal Order window is pop-up with input products",
					"Generate Internal Order window should be pop-up with input products",
					"Generate Internal Order window doesn't pop-up with input products", "fail");
		}

		click(chk_InternelOrerProductAssemblyProcess);
		if (isSelected(chk_InternelOrerProductAssemblyProcess)) {
			writeTestResults("Verify user can select relevant products",
					"User should be able to select relevant products", "User successfully select relevant products",
					"pass");
		} else {
			writeTestResults("Verify user can select relevant products",
					"User should be able to select relevant products", "User doesn't select relevant products", "fail");
		}

		click(btn_applyInternelOrderAssemblyProcess);
		Thread.sleep(3000);
		switchWindow();
		sleepCusomized(header_pageLabel);
		String header = getText(header_pageLabel);
		if (header.equals("Internal Order")) {
			writeTestResults(
					"Verify user can navigated to the Internal Order form with relevant product details and Referance Doc details",
					"User should be navigated to the Internal Order form with relevant product details and Referance Doc details",
					"User successfully navigated to the Internal Order form with relevant product details and Referance Doc details",
					"pass");
		} else {
			writeTestResults(
					"Verify user can navigated to the Internal Order form with relevant product details and Referance Doc details",
					"User should be navigated to the Internal Order form with relevant product details and Referance Doc details",
					"User couldn't navigated to the Internal Order form with relevant product details and Referance Doc details",
					"fail");
		}
	}

	public void fillInternelOrderAssemblyProcess() throws Exception {
		sendKeys(txt_tiltleInternelOrder, titleInternelOrder);
		if (isDisplayed(txt_tiltleInternelOrder)) {
			writeTestResults("Verify user can enter title", "User should be able to enter title",
					"User successfully enter title", "pass");
		} else {
			writeTestResults("Verify user can enter title", "User should be able to enter title",
					"User couldn't enter title", "fail");
		}
		sendKeys(txt_shipAddressAssemblyProcess, address2ILO);
		if (isDisplayed(txt_shipAddressAssemblyProcess)) {
			writeTestResults("Verify user can enter Shiping Address", "User should be able to enter Shiping Address",
					"User successfully enter Shiping Address", "pass");
		} else {
			writeTestResults("Verify user can enter Shiping Address", "User should be able to enter Shiping Address",
					"User couldn't enter Shiping Address", "fail");
		}

		click(btn_draftInternelOrder);
		explicitWaitUntillClickable(btn_relese, 40);
		Thread.sleep(3000);
		trackCode = getText(lbl_creationsDocNo);
		if (isDisplayed(btn_relese)) {

			writeTestResults("Verify user can br draft above document", "User should be able to draft above document",
					"User sucessfully draft above document", "pass");
		} else {
			writeTestResults("Verify user can br draft above document", "User should be able to draft above document",
					"User not draft above document", "Fail");
		}
		sleepCusomized(btn_relese);
		click(btn_relese);
		explicitWaitUntillClickable(btn_goToPageLink, 40);
		click(btn_goToPageLink);
		Thread.sleep(2000);
		pageRefersh();
		if (isDisplayed(btn_releseConfirmation)) {

			writeTestResults("Verify user can releese Internel Order", "User should be able to releese Internel Order",
					"User sucessfully releese Internel Order", "pass");
		} else {
			writeTestResults("Verify user can releese Internel Order", "User should be able to releese Internel Order",
					"User couldn't releese Internel Order", "fail");
		}
		
		switchWindow();
		explicitWaitUntillClickable(btn_draftInternelDispatchOrder,40);
		click(btn_draftInternelDispatchOrder);
		if (isDisplayed(btn_relese)) {

			writeTestResults("Verify user can br draft Internel Dispath Order",
					"User should be able to draft Internel Dispath Order",
					"User sucessfully draft Internel Dispath Order", "pass");
		} else {
			writeTestResults("Verify user can br draft Internel Dispath Order",
					"User should be able to draft Internel Dispath Order", "User not draft Internel Dispath Order",
					"Fail");
		}
		sleepCusomized(btn_relese);
		click(btn_relese);
		customizeLoadingDelay(lbl_relesedConfirmation, 45);
		trackCode = getText(lbl_creationsDocNo);
		String internel_dispatch_order = getText(lbl_docNo);
		writeIWData("Internel Dispatch Order", internel_dispatch_order, 9);
		if (isDisplayed(btn_releseConfirmation)) {

			writeTestResults("Verify user can be relese Internel Dispath Order",
					"User should be able to relese Internel Dispath Order",
					"User sucessfully relese Internel Dispath Order", "pass");
		} else {
			writeTestResults("Verify user can br relese Internel Dispath Order",
					"User should be able to relese Internel Dispath Order", "User not relese Internel Dispath Order",
					"Fail");
		}

		closeWindow();
		switchWindow();
		pageRefersh();

	}

	public void updateOutputProductAssemblyProcess() throws Exception {
		click(btn_action);
		if (isDisplayed(btn_outputProductUpdateAssemblyProcess)) {
			writeTestResults("Verify list of actions were loaded", "List of actions should be loaded",
					"List of actions successfully loaded", "pass");
		} else {
			writeTestResults("Verify list of actions were loaded", "List of actions should be loaded",
					"List of actions doesn't loaded", "fail");
		}

		click(btn_outputProductUpdateAssemblyProcess);
		if (isDisplayed(header_updateOutputProdictAssemblyProcess)) {
			writeTestResults("verify update output products  window is pop-up",
					"Update output products  window should pop-up",
					"Update output products  window successfully pop-up", "pass");
		} else {
			writeTestResults("verify update output products  window is pop-up",
					"Update output products  window should pop-up", "Update output products  window doesn't pop-up",
					"fail");
		}

		click(chk_productThatUpdateAssemblyProcess);
		if (isSelected(chk_productThatUpdateAssemblyProcess)) {
			writeTestResults("Verify user can select proudct to update",
					"User should be able to select proudct to update", "User successfully select proudct to update",
					"pass");
		} else {
			writeTestResults("Verify user can select proudct to update",
					"User should be able to select proudct to update", "User couldn't select proudct to update",
					"fail");
		}

		click(btn_applyAssemblyProcess);
		sleepCusomized(header_pageLabel);
		String header = getText(header_pageLabel);
		if (header.equals("Assembly Order")) {
			writeTestResults("Verify Assembly Order Form was reloaded", "Assembly Order Form should be reloaded",
					"Assembly Order Form successfully reloaded", "pass");
		} else {
			writeTestResults("Verify Assembly Order Form was reloaded", "Assembly Order Form should be reloaded",
					"Assembly Order Form doesn't reloaded", "fail");
		}
	}

	public void jobCompleteAssemblyProcess() throws Exception {
		click(btn_action);
		if (isDisplayed(btn_jobComplteAssemblyProcess)) {
			writeTestResults("Verify list of actions were loaded", "List of actions should be loaded",
					"List of actions successfully loaded", "pass");
		} else {
			writeTestResults("Verify list of actions were loaded", "List of actions should be loaded",
					"List of actions doesn't loaded", "fail");
		}

		click(btn_jobComplteAssemblyProcess);
		if (isDisplayed(btn_complteJobComplteAssemblyProcess)) {
			writeTestResults("Verify complete process window is pop-up", "Complete process window should be pop-up",
					"Complete process window successfully pop-up", "pass");
		} else {
			writeTestResults("Verify complete process window is pop-up", "Complete process window should be pop-up",
					"Complete process window doesn't pop-up", "fail");
		}

		click(btn_complteJobComplteAssemblyProcess);
		Thread.sleep(3000);
		String ststus = getText(lbl_statusAssemblyOrder);
		if (ststus.equals("0%") || ststus.equals("RUNNING")) {
			Thread.sleep(2000);
			ststus = getText(lbl_statusAssemblyOrder);
		}

		if (ststus.equals("COMPLETED")) {
			writeTestResults(
					"Verify Assembly Order form should be reloaded and Assembly Order Form status was changed as Completed",
					"Assembly Order form should be reloaded and Assembly Order Form status should be changed as Completed",
					"Assembly Order form successfully reloaded and Assembly Order Form status successfully changed as Completed",
					"pass");
		} else {
			writeTestResults(
					"Verify Assembly Order form should be reloaded and Assembly Order Form status was changed as Completed",
					"Assembly Order form should be reloaded and Assembly Order Form status should be changed as Completed",
					"Assembly Order form doesn't reloaded and Assembly Order Form status doesn't changed as Completed",
					"pass");
		}
	}

	public void converToInternelReciptAndDraft() throws Exception {
		click(btn_action);
		if (isDisplayed(btn_convertToInternelReciptAssemblyProcess)) {
			writeTestResults("Verify list of actions were loaded", "List of actions should be loaded",
					"List of actions successfully loaded", "pass");
		} else {
			writeTestResults("Verify list of actions were loaded", "List of actions should be loaded",
					"List of actions doesn't loaded", "fail");
		}

		click(btn_convertToInternelReciptAssemblyProcess);
		if (isDisplayed(header_genarateInternelReciptAssemblyProcess)) {
			writeTestResults("Verify generate Internal Receipt window is pop-up",
					"Generate Internal Receipt window should pop-up",
					"Generate Internal Receipt window successfully pop-up", "pass");
		} else {
			writeTestResults("Verify generate Internal Receipt window is pop-up",
					"Generate Internal Receipt window should pop-up", "Generate Internal Receipt window doesn't pop-up",
					"fail");
		}

		click(chk_selectAllInternelReciptAssemblyProcess);
		if (isSelected(chk_selectAllInternelReciptAssemblyProcess)) {
			writeTestResults("Verify user can enable select all check box",
					"User should be able to enable select all check box",
					"User successfully enable select all check box", "pass");
		} else {
			writeTestResults("Verify user can enable select all check box",
					"User should be able to enable select all check box", "User coudn't enable select all check box",
					"fail");
		}

		click(btn_checkoutInternelReciptsAssemblyProcess);
		if (isDisplayed(btn_checkoutInternelReciptsAssemblyProcess)) {
			writeTestResults("Verify user can checkout", "User should be able to checkout",
					"User successfully checkout", "pass");
		} else {
			writeTestResults("Verify user can checkout", "User should be able to checkout", "User coudn't checkout",
					"fail");
		}

		click(btn_applyAssemblyProcess);
		sleepCusomized(header_pageLabel);
		String header = getText(header_pageLabel);
		boolean flag;
		if (header.equals("Assembly Order")) {
			flag = true;
		}
		switchWindow();
		sleepCusomized(header_pageLabel);
		String header1 = getText(header_pageLabel);
		if (header1.equals("Internal Receipt")) {
			flag = true;
		} else {
			flag = false;
		}
		if (flag) {
			writeTestResults(
					"Verify user can navigated to Intenal Receipt Form with the products which required to inbound and Assembly Order Form was reloaded",
					"User should be navigated to Intenal Receipt Form with the products which required to inbound and Assembly Order Form should be reloaded",
					"User successfully navigated to Intenal Receipt Form with the products which required to inbound and Assembly Order Form successfully reloaded",
					"pass");
		} else {
			writeTestResults(
					"Verify user can navigated to Intenal Receipt Form with the products which required to inbound and Assembly Order Form was reloaded",
					"User should be navigated to Intenal Receipt Form with the products which required to inbound and Assembly Order Form should be reloaded",
					"User couldn't navigated to Intenal Receipt Form and Assembly Order Form doesn't reloaded", "fail");
		}

		click(btn_draft);
		if (isDisplayed(btn_relese)) {

			writeTestResults("Verify user can be draft Internel Recipt", "User should be able to draft Internel Recipt",
					"User sucessfully draft Internel Recipt", "pass");
		} else {
			writeTestResults("Verify user can br draft Internel Recipt", "User should be able to draft Internel Recipt",
					"User couldn't draft Internel Recipt", "Fail");
		}
	}

	public void serielBatchCaptureAndReleseInternelReciptAssemblyProcess() throws Exception {
		sleepCusomized(brn_serielBatchCapture);
		click(brn_serielBatchCapture);
		if (isDisplayed(btn_itemNumber2ProductCaptureWindowAssemblyProduct)) {
			writeTestResults("Verify user can navigated to serial batch captuing window",
					"User should be able to navigated to serial batch captuing window",
					"User sucessfully navigated to serial batch captuing window", "pass");
		} else {
			writeTestResults("Verify user can br navigated to serial batch captuing window",
					"User should be able to navigated to serial batch captuing window",
					"User coldn't navigated to serial batch captuing window", "Fail");
		}

		click(btn_itemNumber2ProductCaptureWindowAssemblyProduct);
		if (isDisplayed(div_verifySerielAutoLoadAssemblyProductAssemblyProcess)) {

			writeTestResults(
					"Verify serial were loaded automatically for assembly product [ serial which added in Assembly order]",
					"Serial should be loaded automatically for assembly product [ serial which added in Assembly order]",
					"Serial successfully loaded automatically for assembly product [ serial which added in Assembly order]",
					"pass");
		} else {
			writeTestResults(
					"Verify serial were loaded automatically for assembly product [ serial which added in Assembly order]",
					"Serial should be loaded automatically for assembly product [ serial which added in Assembly order]",
					"Serial doesn't loaded automatically for assembly product [ serial which added in Assembly order]",
					"fail");
		}

		click(btn_updateCapture);
		String pro = getAttribute(btn_itemNumber2ProductCaptureWindowAssemblyProduct, "class");

		if (pro.equals("fli-number green-vackground-fli-number")) {
			writeTestResults("Verify user can capture seriel for assembly product",
					"User should be able to capture seriel for assembly product",
					"User sucessfully capture seriel for assembly product", "pass");
		} else {
			writeTestResults("Verify user can br capture seriel for assembly product",
					"User should be able to capture seriel for assembly product",
					"User coldn't capture seriel for assembly product", "Fail");
		}

		click(btn_itemNumber3ProductCaptureWindowAssemblyProcess);
		click(btn_updateCapture);
		String pro1 = getAttribute(btn_itemNumber3ProductCaptureWindowAssemblyProcess, "class");

		if (pro1.equals("fli-number green-vackground-fli-number")) {
			writeTestResults("Verify serial was loaded for the output update product and user can capture",
					"Serial should be loaded for the output update product and should be able to capture",
					"Serial successfully loaded for the output update product and user successfully capture", "pass");
		} else {
			writeTestResults("Verify serial was loaded for the output update product and user can capture",
					"Serial should be loaded for the output update product and should be able to capture",
					"Serial couldn't loaded for the output update product and user couldn't capture", "fail");
		}

		click(btn_itemNumber1ProductCaptureWindowAssmblyProcess);
		sendKeys(txt_batchNumberCaptureILOOutboundShipment, genBatchFifoSerielNumber(200));
		sendKeys(txt_lotNumberStockAdjustment,
				genProductId() + currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_expiryDateStockAdjustmentProductCapture);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_lotNumberStockAdjustment);
		Thread.sleep(3000);

		if (isDisplayed(txt_batchNumberCaptureILOOutboundShipment) && isDisplayed(txt_lotNumberStockAdjustment)
				&& isDisplayed(txt_expiryDateStockAdjustmentProductCapture)) {
			writeTestResults("Verify user can add serials as a new product",
					"User should be able to add serials as a new product",
					"User sucessfully add serials as a new product", "pass");
		} else {
			writeTestResults("Verify user can br add serials as a new product",
					"User should be able to add serials as a new product", "User coldn't add serials as a new product",
					"Fail");
		}

		click(btn_updateCapture);
		String pro2 = getAttribute(btn_itemNumber1ProductCaptureWindowAssmblyProcess, "class");

		if (pro2.equals("fli-number green-vackground-fli-number")) {
			writeTestResults("Verify user can capture seriel/bath for new product",
					"User should be able to capture seriel/bath for new product",
					"User sucessfully capture seriel/bath for new product", "pass");
		} else {
			writeTestResults("Verify user can br capture seriel/bath for new product",
					"User should be able to capture seriel/bath for new product",
					"User coldn't capture seriel/bath for new product", "Fail");
		}

		click(btn_backButtonUpdateCapture);
		sleepCusomized(header_pageLabel);
		String header1 = getText(header_pageLabel);
		if (header1.equals("Internal Receipt")) {
			writeTestResults("Verify user can navigated to Internal Receipt Form",
					"User should be able to navigated to Internal Receipt Form",
					"User sucessfully navigated to Internal Receipt Form", "pass");
		} else {
			writeTestResults("Verify user can br navigated to Internal Receipt Form",
					"User should be able to navigated to Internal Receipt Form",
					"User coldn't navigated to Internal Receipt Form", "Fail");
		}

		click(btn_relese);
		customizeLoadingDelay(lbl_releasedStatusCommonWithoutSpace, 40);
		if (isDisplayed(lbl_releasedStatusCommonWithoutSpace)) {
			trackCode = getText(lbl_docNo);
			writeTestResults("Verify user can release Internel Recipt", "Internel Recipt should be released",
					"Internel Recipt successfully released", "pass");
		} else {
			writeTestResults("Verify user can release Internel Recipt", "Internel Recipt should be released",
					"Internel Recipt doesn't released", "fail");
		}
	}

	/* Common remove QC Acceptance */
	public void removeQcAcceptance() throws Exception {

		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		waitImplicit(navigation_pane, 60);
		click(navigation_pane);
		click(btn_adminModule);

		click(btn_balancingLevelAdminModule);
		System.out.println("Wait For Removing Shipment Acceptance........");
		for (int i = 0; i < 12; i++) {
			click(btn_deleestShipmentAceptance);
		}
		click(btn_updateBalancingLevelQCAcceptance);
		pageRefersh();

	}

	/******************* Action Verification *******************/

	/* AV_IW_ProductGroup */
	public void actionVerificationLogin() throws Exception {
		openPage(siteURL);
		Thread.sleep(2000);
		customizeLoadingDelay(txt_username, 40);
		sendKeys(txt_username, userNameData);
		Thread.sleep(1000);
		sendKeys(txt_password, passwordData);
		Thread.sleep(2000);
		click(btn_login);
		waitImplicit(navigation_pane, 10);
		if (isDisplayedQuickCheck(div_loginVerification)) {
			openPage(siteURL);
			Thread.sleep(2000);
			customizeLoadingDelay(txt_username, 40);
			sendKeys(txt_username, userNameData);
			Thread.sleep(1000);
			sendKeys(txt_password, passwordData);
			Thread.sleep(2000);
			click(btn_login);
			waitImplicit(navigation_pane, 10);
		}
	}

	public void navigateToInventoryAndWarehouseModuleActionVerification() throws Exception {
		actionVerificationLogin();
		waitImplicit(navigation_pane, 10);
		click(navigation_pane);
		sleepCusomized(inventory_module);
		click(inventory_module);
	}

	public void navigateNewProductGroupPageActionVerification() throws Exception {
		click(lnk_product_group_configuration);
		click(btn_new_group_configuration);
		url_new_status = getCurrentUrl();
		click(btn_copyFromActionVerification);
		if (isDisplayed(header_productGroupLookupActionVerification)) {
			writeTestResults("Verify 'Copy From' button is working", "'Copy From' button should be working",
					"'Copy From' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Copy From' button is working", "'Copy From' button should be working",
					"'Copy From' button doesn't working", "fail");
		}
		click(btn_closeLookupActionVerification);
	}

	public void configureNewProductGroupActionVerification() throws Exception {
		sleepCusomized(btn_product_group_plus_mark_table);
		click(btn_product_group_plus_mark_table);

		int rowCount = driver.findElements(By.xpath(tbl_productGroup)).size();

		for (int i = 1; i <= rowCount; i++) {
			// row count
		}
		final String finle_btn_product_group_plus_mark_second = btn_product_group_plus_mark_second;
		String plus_mark_second = finle_btn_product_group_plus_mark_second.replace("last_row",
				String.valueOf(rowCount));
		click(plus_mark_second);

		rowCount = rowCount + 1;
		final String finle_txt_product_group = txt_product_group;
		String txt_product_group_updated = finle_txt_product_group.replace("txt_last_row", String.valueOf(rowCount));
		product_group = "PG_GSTM" + currentTimeAndDate();
		sendKeys(txt_product_group_updated, product_group);

		click(btn_productGroupUpdate);

		sleepCusomized(dropdown_productGroupConfig);
		Select comboBox = new Select(driver.findElement(By.xpath(dropdown_productGroupConfig)));
		selectText(dropdown_productGroupConfig, product_group);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		selectText(dropdown_productGroupConfig, product_group);

	}

	public void inventoryPurchaseAndSalesProductGroupConfigActionVerfication() throws Exception {
		click(tab_pg_details);

		click(btn_inventory_active);
		click(btn_product_function_confirmation);

	}

	public void actionsProductGoup() throws Exception {
		click(btn_draft2);
		sleepCusomized(btn_releese2);
		if (isDisplayed(btn_releese2)) {
			writeTestResults("Verify 'Draft' button is working", "'Draft' button should be working",
					"'Draft' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Draft' button is working", "'Draft' button should be working",
					"'Draft' button doesn't working", "fail");
		}

		sleepCusomized(btn_relese);
		click(btn_relese);
		Thread.sleep(4000);
		trackCode = getText(lbl_creationsDocNo);
		String header1 = getText(header_draft);
		if (!header1.equalsIgnoreCase("( Released )")) {
			Thread.sleep(7000);
		}
		if (header1.equalsIgnoreCase("( Released )")) {
			writeTestResults("Verify 'Release' button is working", "'Release' button should be working",
					"'Release' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Release' button is working", "'Release' button should be working",
					"'Release' button doesn't working", "fail");
		}

		url_released_status = getCurrentUrl();

		click(btn_editActionVerification);
		if (isDisplayed(btn_updateActionVerification)) {
			writeTestResults("Verify 'Edit' button is working", "'Edit' button should be working",
					"'Edit' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Edit' button is working", "'Edit' button should be working",
					"'Edit' button doesn't working", "fail");
		}

		click(btn_updateActionVerification);
		if (isDisplayed(btn_editActionVerification)) {
			writeTestResults("Verify 'Update' button is working", "'Update' button should be working",
					"'Update' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Update' button is working", "'Update' button should be working",
					"'Update' button doesn't working", "fail");
		}

		click(btn_editActionVerification);
		click(btn_updateAndNewActionVerification);
		if (isDisplayed(btn_draft2)) {
			writeTestResults("Verify 'Update & New' button is working", "'Update & New' button should be working",
					"'Update & New' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Update & New' button is working", "'Update & New' button should be working",
					"'Update & New' button doesn't working", "fail");
		}

		openPage(url_released_status);
		click(btn_duplicateActionVerification);
		if (isDisplayed(btn_draft2)) {
			writeTestResults("Verify 'Duplicate' button is working", "'Duplicate' button should be working",
					"'Duplicate' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Duplicate' button is working", "'Duplicate' button should be working",
					"'Duplicate' button doesn't working", "fail");
		}

		openPage(url_released_status);

	}

	public void actionSectionVerification() throws Exception {
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_historyActionVerification);
		sleepCusomized(div_historyTableActionVerification);
		if (isDisplayed(div_historyTableActionVerification)) {
			writeTestResults("Verify 'History' button is working", "'History' button should be working",
					"'History' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'History' button is working", "'History' button should be working",
					"'History' button doesn't working", "fail");
		}

		openPage(url_released_status);
		click(btn_action);
		click(btn_activitiesActionVerification);
		if (isDisplayed(btn_activitiesHeaderActionVrification)) {
			writeTestResults("Verify 'Activities' button is working", "'Activities' button should be working",
					"'Activities' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Activities' button is working", "'Activities' button should be working",
					"'Activities' button doesn't working", "fail");
		}

		openPage(url_released_status);
		click(btn_action);
		click(btn_configureCategoriesActionVerification);
		if (isDisplayed(btn_configureCategoriesHeaderActionVrification)) {
			writeTestResults("Verify 'Configure Categories' button is working",
					"'Configure Categories' button should be working",
					"'Configure Categories' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Configure Categories' button is working",
					"'Configure Categories' button should be working", "'Configure Categories' button doesn't working",
					"fail");
		}

		openPage(url_new_status);

		sleepCusomized(btn_product_group_plus_mark_table);
		click(btn_product_group_plus_mark_table);

		int rowCount = driver.findElements(By.xpath(tbl_productGroup)).size();

		for (int i = 1; i <= rowCount; i++) {
			// row count
		}
		final String finle_btn_product_group_plus_mark_second = btn_product_group_plus_mark_second;
		String plus_mark_second = finle_btn_product_group_plus_mark_second.replace("last_row",
				String.valueOf(rowCount));
		click(plus_mark_second);

		rowCount = rowCount + 1;
		final String finle_txt_product_group = txt_product_group;
		String txt_product_group_updated = finle_txt_product_group.replace("txt_last_row", String.valueOf(rowCount));
		product_group = "PG_GSTM" + currentTimeAndDate();
		sendKeys(txt_product_group_updated, product_group);

		click(btn_productGroupUpdate);

		sleepCusomized(dropdown_productGroupConfig);
		Select comboBox = new Select(driver.findElement(By.xpath(dropdown_productGroupConfig)));
		selectText(dropdown_productGroupConfig, product_group);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		selectText(dropdown_productGroupConfig, product_group);

		click(tab_pg_details);

		click(btn_inventory_active);
		click(btn_product_function_confirmation);

		click(btn_draft2);
		sleepCusomized(btn_releese2);

		click(btn_action);
		click(btn_deleteActionVrification);
		click(btn_deleteConfirationActionVerification);
		String header1 = getText(header_draft);
		if (!header1.equalsIgnoreCase("( Deleted )")) {
			Thread.sleep(7000);
			header1 = getText(header_draft);
		}
		if (header1.equalsIgnoreCase("( Deleted )")) {
			writeTestResults("Verify 'Delete' button is working", "'Delete' button should be working",
					"'Delete' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Delete' button is working", "'Delete' button should be working",
					"'Delete' button doesn't working", "fail");
		}

	}
	/* AV_IW_WarehouseInformation */

	public void navigateNewWarehouseFormAndVerifyCopyFormActionVerification() throws Exception {
		navigateToInventoryAndWarehouseModuleActionVerification();
		click(btn_warehouseInfo);

		click(btn_newWarehouse);
		url_new_status = getCurrentUrl();
		click(btn_copyFromActionVerification);
		if (isDisplayed(header_warehouseLookupActionVerification)) {
			writeTestResults("Verify 'Copy From' button is working", "'Copy From' button should be working",
					"'Copy From' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Copy From' button is working", "'Copy From' button should be working",
					"'Copy From' button doesn't working", "fail");
		}
		click(btn_closeLookupActionVerification);
	}

	public void fillwarehouseInformationFormActionVerification() throws Exception {
		warehouse_create = "Ware-GSTM" + currentTimeAndDate().substring(11);
		sleepCusomized(txt_warehouseCode);
		sendKeys(txt_warehouseCode, warehouse_create);

		sendKeys(txt_warehouseName, warehouseName);

		selectText(dropdown_businessUnit, businessUnit);

		if (!isSelected(checkbox_isAllocation)) {
			click(checkbox_isAllocation);
		}

		selectText(dropdown_qurantineWarehouse, qurantineWarehouse);

		if (!isSelected(checkbox_autoLot)) {
			click(checkbox_autoLot);
		}

		selectText(dropdown_lotBook, lotBookWarehouse);
	}

	public void actionsWarehouseInformation() throws Exception {
		click(btn_draft2);
		sleepCusomized(btn_releese2);
		if (isDisplayed(btn_releese2)) {
			writeTestResults("Verify 'Draft' button is working", "'Draft' button should be working",
					"'Draft' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Draft' button is working", "'Draft' button should be working",
					"'Draft' button doesn't working", "fail");
		}

		sleepCusomized(btn_relese);
		click(btn_relese);
		Thread.sleep(4000);
		trackCode = warehouse_create;
		String header1 = getText(header_draft);
		if (!header1.equalsIgnoreCase("( Released )")) {
			Thread.sleep(7000);
		}
		if (header1.equalsIgnoreCase("(Released)")) {
			writeTestResults("Verify 'Release' button is working", "'Release' button should be working",
					"'Release' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Release' button is working", "'Release' button should be working",
					"'Release' button doesn't working", "fail");
		}

		url_released_status = getCurrentUrl();

		click(btn_editActionVerification);
		if (isDisplayed(btn_updateActionVerification)) {
			writeTestResults("Verify 'Edit' button is working", "'Edit' button should be working",
					"'Edit' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Edit' button is working", "'Edit' button should be working",
					"'Edit' button doesn't working", "fail");
		}

		click(btn_updateActionVerification);
		if (isDisplayed(btn_editActionVerification)) {
			writeTestResults("Verify 'Update' button is working", "'Update' button should be working",
					"'Update' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Update' button is working", "'Update' button should be working",
					"'Update' button doesn't working", "fail");
		}

		click(btn_editActionVerification);
		click(btn_updateAndNewActionVerification);
		if (isDisplayed(btn_draft2)) {
			writeTestResults("Verify 'Update & New' button is working", "'Update & New' button should be working",
					"'Update & New' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Update & New' button is working", "'Update & New' button should be working",
					"'Update & New' button doesn't working", "fail");
		}

		openPage(url_released_status);
		click(btn_duplicateActionVerification);
		if (isDisplayed(btn_draft2)) {
			writeTestResults("Verify 'Duplicate' button is working", "'Duplicate' button should be working",
					"'Duplicate' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Duplicate' button is working", "'Duplicate' button should be working",
					"'Duplicate' button doesn't working", "fail");
		}

		openPage(url_released_status);

	}

	public void actionSectionVerificationWarehouse() throws Exception {
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_historyActionVerification);
		sleepCusomized(div_historyTableActionVerification);
		if (isDisplayed(div_historyTableActionVerification)) {
			writeTestResults("Verify 'History' button is working", "'History' button should be working",
					"'History' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'History' button is working", "'History' button should be working",
					"'History' button doesn't working", "fail");
		}

		openPage(url_released_status);
		click(btn_action);
		click(btn_activitiesActionVerification);
		if (isDisplayed(btn_activitiesHeaderActionVrification)) {
			writeTestResults("Verify 'Activities' button is working", "'Activities' button should be working",
					"'Activities' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Activities' button is working", "'Activities' button should be working",
					"'Activities' button doesn't working", "fail");
		}

		openPage(url_new_status);
		warehouse_create = "Ware-GSTM" + currentTimeAndDate().substring(11);
		sleepCusomized(txt_warehouseCode);
		sendKeys(txt_warehouseCode, warehouse_create);

		sendKeys(txt_warehouseName, warehouseName);

		selectText(dropdown_businessUnit, businessUnit);

		if (!isSelected(checkbox_isAllocation)) {
			click(checkbox_isAllocation);
		}

		selectText(dropdown_qurantineWarehouse, qurantineWarehouse);

		if (!isSelected(checkbox_autoLot)) {
			click(checkbox_autoLot);
		}

		selectText(dropdown_lotBook, lotBookWarehouse);

		click(btn_draft2);
		sleepCusomized(btn_releese2);

		click(btn_action);
		click(btn_deleteActionVrification);
		click(btn_deleteConfirationActionVerification);
		String header1 = getText(header_draft);
		if (!header1.equalsIgnoreCase("( Deleted )")) {
			Thread.sleep(7000);
			header1 = getText(header_draft);
		}
		if (header1.equalsIgnoreCase("(Deleted)")) {
			writeTestResults("Verify 'Delete' button is working", "'Delete' button should be working",
					"'Delete' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Delete' button is working", "'Delete' button should be working",
					"'Delete' button doesn't working", "fail");
		}

	}

	/* AV_IW_AllocationManager */
	public void navigateToAllocationManager() throws Exception {
		navigateToInventoryAndWarehouseModuleActionVerification();

		click(btn_allocationManger);
	}

	public void allocationManagerActionVerification() throws Exception {
		click(chk_manageDocumentAllocationManagerActionVerification);

		click(btn_action);

		click(btn_genarateAllocationMangerActionVerification);

		if (isDisplayed(header_allocationManagerActionVerification)) {
			writeTestResults("Verify 'Generate Allocation Manager' button is working",
					"'Generate Allocation Manager' button should be working",
					"'Generate Allocation Manager' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Generate Allocation Manager' button is working",
					"'Generate Allocation Manager' button should be working",
					"'Generate Allocation Manager' button doesn't working", "fail");
		}
	}

	/* AV_IW_ProductInformation */
	public void navigateProductInformationAndVerifyCopyFromActionVerification() throws Exception {
		navigateToInventoryAndWarehouseModuleActionVerification();
		sleepCusomized(btn_product_info);
		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		InventoryAndWarehouseModuleRegression obj1 = new InventoryAndWarehouseModuleRegression();
		obj1.customizeLoadingDelay(btn_new_product, 8);
		Thread.sleep(1500);
		click(btn_new_product);
		url_new_status = getCurrentUrl();
		click(btn_copyFromActionVerification);
		if (isDisplayed(header_productLookupActionVerification)) {
			writeTestResults("Verify 'Copy From' button is working", "'Copy From' button should be working",
					"'Copy From' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Copy From' button is working", "'Copy From' button should be working",
					"'Copy From' button doesn't working", "fail");
		}
		click(btn_closeLookupActionVerification);
	}

	public void cerateNewProductActionVerification() throws Exception {
		sendKeys(txt_product_code, (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "-BATCH-SPECIFIC"));

		// product description
		sendKeys(txt_product_desc, productDesription);

		// product group
		selectText(dropdown_product_group, productionGroupBatchProductCreation);

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		String menufacturur = readIWData(2);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturur);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", readIWData(2)));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);

		// product width
		customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		click(tab_detail);
		customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);

		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		click(chk_batchProductDispatchOnlyBatchProduct);
		Thread.sleep(2000);
		Select comboBox = new Select(driver.findElement(By.xpath(dropdown_outbound_costing_method)));
		selectText(dropdown_outbound_costing_method, outboundCostingMethod);
		Thread.sleep(1500);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		click(checkbox_without_costing);
		click(chk_allocationBatchProduct);

		click(checkbox_validate_expiery);
		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		if (!isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		Thread.sleep(1000);
		if (!isSelected(checkbox_validate_expiery_date)) {
			click(checkbox_validate_expiery_date);
		}
		Thread.sleep(1000);

	}

	public void actionsProductInformation() throws Exception {
		click(btn_draft2);
		sleepCusomized(btn_releese2);
		if (isDisplayed(btn_releese2)) {
			writeTestResults("Verify 'Draft' button is working", "'Draft' button should be working",
					"'Draft' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Draft' button is working", "'Draft' button should be working",
					"'Draft' button doesn't working", "fail");
		}

		sleepCusomized(btn_relese);
		click(btn_relese);
		Thread.sleep(4000);
		trackCode = getText(lbl_creationsDocNo);
		String header1 = getText(header_draft);
		if (!header1.equalsIgnoreCase("( Released )")) {
			Thread.sleep(7000);
		}
		if (header1.equalsIgnoreCase("( Released )")) {
			writeTestResults("Verify 'Release' button is working", "'Release' button should be working",
					"'Release' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Release' button is working", "'Release' button should be working",
					"'Release' button doesn't working", "fail");
		}

		url_released_status = getCurrentUrl();

		click(btn_editActionVerification);
		if (isDisplayed(btn_updateActionVerification)) {
			writeTestResults("Verify 'Edit' button is working", "'Edit' button should be working",
					"'Edit' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Edit' button is working", "'Edit' button should be working",
					"'Edit' button doesn't working", "fail");
		}

		click(btn_updateActionVerification);
		if (isDisplayed(btn_editActionVerification)) {
			writeTestResults("Verify 'Update' button is working", "'Update' button should be working",
					"'Update' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Update' button is working", "'Update' button should be working",
					"'Update' button doesn't working", "fail");
		}

		click(btn_editActionVerification);
		click(btn_updateAndNewActionVerification);
		if (isDisplayed(btn_draft2)) {
			writeTestResults("Verify 'Update & New' button is working", "'Update & New' button should be working",
					"'Update & New' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Update & New' button is working", "'Update & New' button should be working",
					"'Update & New' button doesn't working", "fail");
		}

		openPage(url_released_status);
		click(btn_duplicateActionVerification);
		if (isDisplayed(btn_draft2)) {
			writeTestResults("Verify 'Duplicate' button is working", "'Duplicate' button should be working",
					"'Duplicate' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Duplicate' button is working", "'Duplicate' button should be working",
					"'Duplicate' button doesn't working", "fail");
		}

		openPage(url_released_status);
		click(btn_action);
		click(btn_remindersAllocationMangerActionVerification);
		if (isDisplayed(header_reminderActionVerification)) {
			writeTestResults("Verify 'Reminders' button is working", "'Reminders' button should be working",
					"'Reminders' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Reminders' button is working", "'Reminders' button should be working",
					"'Reminders' button doesn't working", "fail");
		}

		openPage(url_released_status);
		click(btn_action);
		click(btn_sellingPriceActionVerification);
		if (isDisplayed(header_sellingPriceActionVerification)) {
			writeTestResults("Verify 'Selling Price' button is working", "'Selling Price' button should be working",
					"'Selling Price' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Selling Price' button is working", "'Selling Price' button should be working",
					"'Selling Price' button doesn't working", "fail");
		}

		openPage(url_released_status);
		click(btn_action);
		click(btn_PLUCodeAllocationMangerActionVerification);
		if (isDisplayed(header_updatePluCodesActionVerification)) {
			writeTestResults("Verify 'PLU Code' button is working", "'PLU Code' button should be working",
					"'PLU Code' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'PLU Code' button is working", "'PLU Code' button should be working",
					"'PLU Code' button doesn't working", "fail");
		}

		openPage(url_released_status);

	}

	public void actionSectionVerificationProductInformation() throws Exception {
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_historyActionVerification);
		sleepCusomized(div_historyTableActionVerification);
		if (isDisplayed(div_historyTableActionVerification)) {
			writeTestResults("Verify 'History' button is working", "'History' button should be working",
					"'History' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'History' button is working", "'History' button should be working",
					"'History' button doesn't working", "fail");
		}

		openPage(url_released_status);
		click(btn_action);
		click(btn_activitiesActionVerification);
		if (isDisplayed(btn_activitiesHeaderActionVrification)) {
			writeTestResults("Verify 'Activities' button is working", "'Activities' button should be working",
					"'Activities' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Activities' button is working", "'Activities' button should be working",
					"'Activities' button doesn't working", "fail");
		}

		openPage(url_new_status);
		sendKeys(txt_product_code, (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "-BATCH-SPECIFIC"));

		// product description
		sendKeys(txt_product_desc, productDesription);

		// product group
		selectText(dropdown_product_group, productionGroupBatchProductCreation);

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		String menufacturur = readIWData(2);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturur);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", readIWData(2)));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);

		// product width
		customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		click(tab_detail);
		customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);

		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		click(chk_batchProductDispatchOnlyBatchProduct);
		Thread.sleep(2000);
		Select comboBox = new Select(driver.findElement(By.xpath(dropdown_outbound_costing_method)));
		selectText(dropdown_outbound_costing_method, outboundCostingMethod);
		Thread.sleep(1500);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		click(checkbox_without_costing);
		click(chk_allocationBatchProduct);

		click(checkbox_validate_expiery);
		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		if (!isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		Thread.sleep(1000);
		if (!isSelected(checkbox_validate_expiery_date)) {
			click(checkbox_validate_expiery_date);
		}
		Thread.sleep(1000);

		click(btn_draft2);
		sleepCusomized(btn_releese2);

		click(btn_action);
		click(btn_deleteActionVrification);
		click(btn_deleteConfirationActionVerification);
		String header1 = getText(header_draft);
		if (!header1.equalsIgnoreCase("( Deleted )")) {
			Thread.sleep(7000);
			header1 = getText(header_draft);
		}
		if (header1.equalsIgnoreCase("( Deleted )")) {
			writeTestResults("Verify 'Delete' button is working", "'Delete' button should be working",
					"'Delete' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Delete' button is working", "'Delete' button should be working",
					"'Delete' button doesn't working", "fail");
		}

	}

	/* AV_IW_InventoryParameters */
	public void navigateToInventoryParameters() throws Exception {
		navigateToInventoryAndWarehouseModuleActionVerification();
		click(btn_inventoryParameters);
	}

	public void validateUpdateButtonInventoryParameterActionVerfication() throws Exception {
		click(btn_plusMarkInventoryParameters);

		String inventory = "Madhushan-" + currentTimeAndDate().replace("/", "").replace(":", "");

		sendKeys(txt_parameterActionVerification, inventory);

		click(btn_updateActionVerification);

		Thread.sleep(2000);

		String validate = getText(para_validator);

		if (validate.equalsIgnoreCase("Successfully Saved.")) {
			writeTestResults("Verify 'Update' button is working", "'Update' button should be working",
					"'Update' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Update' button is working", "'Update' button should be working",
					"'Update' button doesn't working", "fail");
		}
	}

	/* AV_IW_AssemblyCostEstimation */
	public void navigateToAssemblyCostEstimationAndVerifyCopyFromActionVerification() throws Exception {
		navigateToInventoryAndWarehouseModuleActionVerification();
		click(btn_assemblyCostEstimation);

		click(btn_newAssemblyCostAstimation);
		url_new_status = getCurrentUrl();
		click(btn_copyFromActionVerification);
		if (isDisplayed(header_costEstimationActionVerification)) {
			writeTestResults("Verify 'Copy From' button is working", "'Copy From' button should be working",
					"'Copy From' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Copy From' button is working", "'Copy From' button should be working",
					"'Copy From' button doesn't working", "fail");
		}
		click(btn_closeLookupActionVerification);
	}

	public void fillAssemblyCostEstimationActionVerfication() throws Exception {
		sendKeys(txt_assemblyCostEstiamtionDescription, descriptionAssemblyCostEstimation);

		click(btn_searchCustomeACE);
		sendKeys(txt_cusSearchACE, customerAccountACE);
		pressEnter(txt_cusSearchACE);

		doubleClick(lnk_resultCusAccountACE);

		click(btn_searchAssemblyProACE);
		sleepCusomized(txt_AssemblyProductSearchACE);
		sendKeys(txt_AssemblyProductSearchACE, assemblyProductACE);
		pressEnter(txt_AssemblyProductSearchACE);
		sleepCusomized(lnk_resltProductAssemblyACE);
		doubleClick(lnk_resltProductAssemblyACE);

		click(btn_searchPricingProfileACE);
		sendKeys(txt_pricingProfileACE, pricingProfileACE);
		pressEnter(txt_pricingProfileACE);
		doubleClick(lnk_pricingProfileACE);

		click(tab_estimationDeatailsACE);

		/* Row 1 */
		Select comboBox = new Select(driver.findElement(By.xpath(drop_typeACE1)));
		selectText(drop_typeACE1, typeEstmationAce1);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		click(btn_searchProductEstmationAce1);
		sleepCusomized(txt_productSearch);
		sendKeys(txt_productSearch, product1EstimationDeatailsACE);
		pressEnter(txt_productSearch);
		sleepCusomized(btn_resultProduct);
		doubleClick(btn_resultProduct);
		Thread.sleep(1000);

		sendKeys(txt_estimatedQuantityACE, qtyInputProduct1ACE);
		sendKeys(txt_costEstmatedACE1Input, costInputProduct1ACE);
		if (!isSelected(chk_billableEstmationRow1ACE)) {
			click(chk_billableEstmationRow1ACE);
		}

		/* Row 02 */
		click(btn_adnewRow1ACE);

		Select comboBox1 = new Select(driver.findElement(By.xpath(drop_typeACE2)));
		selectText(drop_typeACE2, typeEstmationAce2);
		String selectedComboValue1 = comboBox1.getFirstSelectedOption().getText();

		click(btn_searchProduct2ACE);
		sleepCusomized(txt_productSearch);
		sendKeys(txt_productSearch, product2EstimationDeatailsACE);
		pressEnter(txt_productSearch);
		sleepCusomized(btn_resultProduct);
		doubleClick(btn_resultProduct);
		Thread.sleep(1000);

		sendKeys(txt_estimatedQuantityACE2, qtyInputProduct2ACE);
		sendKeys(txt_costEstmatedACEOutput2, costInputProduct2ACE);
		if (!isSelected(chk_billableEstmationRow2ACE)) {
			click(chk_billableEstmationRow2ACE);
		}

		/* Row 3 */
		click(btn_adaNewRow3);

		Select comboBox2 = new Select(driver.findElement(By.xpath(drop_typeExpenceACE)));
		selectText(drop_typeExpenceACE, typeEstmationAce3Expence);
		String selectedComboValue2 = comboBox2.getFirstSelectedOption().getText();

		selectIndex(drop_estiamationRow3ReferenceCodeExpenceACE, 2);

		click(btn_searchServiceACE3ProductLookup);

		sleepCusomized(txt_productSearch);
		sendKeys(txt_productSearch, product3EstimationDeatailsACESerivice);
		pressEnter(txt_productSearch);
		sleepCusomized(btn_resultProduct);
		doubleClick(btn_resultProduct);
		Thread.sleep(1000);

		sendKeys(txt_costServiceProductEstmationACE, costInputService3ACE);
		if (!isSelected(chk_billableEstmationRow3ACE)) {
			click(chk_billableEstmationRow3ACE);
		}
		/* ROW 4 */
		click(btn_addNewRow4EstmationDetailsACE);

		Select comboBox3 = new Select(driver.findElement(By.xpath(drop_type4ServiceACE)));
		selectText(drop_type4ServiceACE, typeEstmationAce4);
		String selectedComboValue3 = comboBox3.getFirstSelectedOption().getText();

		click(btn_searchProductLookuop4ACE);

		sleepCusomized(txt_productSearch);
		sendKeys(txt_productSearch, product4EstimationDeatailsACEService);
		pressEnter(txt_productSearch);
		sleepCusomized(btn_resultProduct);
		doubleClick(btn_resultProduct);
		Thread.sleep(1000);

		sendKeys(txt_cost4ServiceProductEstimationACE, costInputService4ACE);
		if (!isSelected(chk_billable4ServiceACeEstimation)) {
			click(chk_billable4ServiceACeEstimation);
		}
		click(tab_summaryACE);

		click(btn_checkoutACE);
		Thread.sleep(3000);
	}

	public void actionsAssemblyCostEstimation() throws Exception {
		click(btn_draft2);
		sleepCusomized(btn_releese2);
		if (isDisplayed(btn_releese2)) {
			writeTestResults("Verify 'Draft' button is working", "'Draft' button should be working",
					"'Draft' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Draft' button is working", "'Draft' button should be working",
					"'Draft' button doesn't working", "fail");
		}

		url_draft_status = getCurrentUrl();

		click(btn_editActionVerification);
		if (isDisplayed(btn_updateActionVerification)) {
			writeTestResults("Verify 'Edit' button is working", "'Edit' button should be working",
					"'Edit' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Edit' button is working", "'Edit' button should be working",
					"'Edit' button doesn't working", "fail");
		}

		click(btn_checkoutACE);
		click(btn_updateActionVerification);
		if (isDisplayed(btn_editActionVerification)) {
			writeTestResults("Verify 'Update' button is working", "'Update' button should be working",
					"'Update' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Update' button is working", "'Update' button should be working",
					"'Update' button doesn't working", "fail");
		}

		click(btn_editActionVerification);
		click(btn_checkoutACE);
		click(btn_updateAndNewActionVerification);
		if (isDisplayed(btn_draft2)) {
			writeTestResults("Verify 'Update & New' button is working", "'Update & New' button should be working",
					"'Update & New' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Update & New' button is working", "'Update & New' button should be working",
					"'Update & New' button doesn't working", "fail");
		}

		openPage(url_draft_status);
		sleepCusomized(btn_relese);
		click(btn_relese);
		Thread.sleep(4000);
		trackCode = getText(lbl_creationsDocNo);
		String header1 = getText(header_draft);
		if (!header1.equalsIgnoreCase("(Released)")) {
			Thread.sleep(7000);
		}
		if (header1.equalsIgnoreCase("(Released)")) {
			writeTestResults("Verify 'Release' button is working", "'Release' button should be working",
					"'Release' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Release' button is working", "'Release' button should be working",
					"'Release' button doesn't working", "fail");
		}

		url_released_status = getCurrentUrl();
		openPage(url_released_status);
		click(btn_duplicateActionVerification);
		if (isDisplayed(btn_draft2)) {
			writeTestResults("Verify 'Duplicate' button is working", "'Duplicate' button should be working",
					"'Duplicate' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Duplicate' button is working", "'Duplicate' button should be working",
					"'Duplicate' button doesn't working", "fail");
		}

		openPage(url_released_status);
		click(btn_action);
		click(btn_remindersAllocationMangerActionVerification);
		if (isDisplayed(header_reminderActionVerification)) {
			writeTestResults("Verify 'Reminders' button is working", "'Reminders' button should be working",
					"'Reminders' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Reminders' button is working", "'Reminders' button should be working",
					"'Reminders' button doesn't working", "fail");
		}

		openPage(url_released_status);

	}

	public void actionSectionVerificationAssemblyCostEstimation() throws Exception {
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_historyActionVerification);
		sleepCusomized(div_historyTableActionVerification);
		if (isDisplayed(div_historyTableActionVerification)) {
			writeTestResults("Verify 'History' button is working", "'History' button should be working",
					"'History' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'History' button is working", "'History' button should be working",
					"'History' button doesn't working", "fail");
		}

		openPage(url_released_status);
		click(btn_action);
		click(btn_reverseActionVrification);
		click(btn_reverseConfirationActionVerification);
		String header12 = getText(header_draft);
		if (!header12.equalsIgnoreCase("(Reversed)")) {
			Thread.sleep(7000);
			header12 = getText(header_draft);
		}
		if (header12.equalsIgnoreCase("(Reversed)")) {
			writeTestResults("Verify 'Reverse' button is working", "'Reverse' button should be working",
					"'Reverse' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Reverse' button is working", "'Reverse' button should be working",
					"'Reverse' button doesn't working", "fail");
		}

		openPage(url_new_status);

		sendKeys(txt_assemblyCostEstiamtionDescription, descriptionAssemblyCostEstimation);

		click(btn_searchCustomeACE);
		sendKeys(txt_cusSearchACE, customerAccountACE);
		pressEnter(txt_cusSearchACE);

		doubleClick(lnk_resultCusAccountACE);

		click(btn_searchAssemblyProACE);
		sleepCusomized(txt_AssemblyProductSearchACE);
		sendKeys(txt_AssemblyProductSearchACE, assemblyProductACE);
		pressEnter(txt_AssemblyProductSearchACE);
		sleepCusomized(lnk_resltProductAssemblyACE);
		doubleClick(lnk_resltProductAssemblyACE);

		click(btn_searchPricingProfileACE);
		sendKeys(txt_pricingProfileACE, pricingProfileACE);
		pressEnter(txt_pricingProfileACE);
		doubleClick(lnk_pricingProfileACE);

		click(tab_estimationDeatailsACE);

		/* Row 1 */
		Select comboBox = new Select(driver.findElement(By.xpath(drop_typeACE1)));
		selectText(drop_typeACE1, typeEstmationAce1);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		click(btn_searchProductEstmationAce1);
		sleepCusomized(txt_productSearch);
		sendKeys(txt_productSearch, product1EstimationDeatailsACE);
		pressEnter(txt_productSearch);
		sleepCusomized(btn_resultProduct);
		doubleClick(btn_resultProduct);
		Thread.sleep(1000);

		sendKeys(txt_estimatedQuantityACE, qtyInputProduct1ACE);
		sendKeys(txt_costEstmatedACE1Input, costInputProduct1ACE);
		if (!isSelected(chk_billableEstmationRow1ACE)) {
			click(chk_billableEstmationRow1ACE);
		}

		/* Row 02 */
		click(btn_adnewRow1ACE);

		Select comboBox1 = new Select(driver.findElement(By.xpath(drop_typeACE2)));
		selectText(drop_typeACE2, typeEstmationAce2);
		String selectedComboValue1 = comboBox1.getFirstSelectedOption().getText();

		click(btn_searchProduct2ACE);
		sleepCusomized(txt_productSearch);
		sendKeys(txt_productSearch, product2EstimationDeatailsACE);
		pressEnter(txt_productSearch);
		sleepCusomized(btn_resultProduct);
		doubleClick(btn_resultProduct);
		Thread.sleep(1000);

		sendKeys(txt_estimatedQuantityACE2, qtyInputProduct2ACE);
		sendKeys(txt_costEstmatedACEOutput2, costInputProduct2ACE);
		if (!isSelected(chk_billableEstmationRow2ACE)) {
			click(chk_billableEstmationRow2ACE);
		}

		/* Row 3 */
		click(btn_adaNewRow3);

		Select comboBox2 = new Select(driver.findElement(By.xpath(drop_typeExpenceACE)));
		selectText(drop_typeExpenceACE, typeEstmationAce3Expence);
		String selectedComboValue2 = comboBox2.getFirstSelectedOption().getText();

		selectIndex(drop_estiamationRow3ReferenceCodeExpenceACE, 2);

		click(btn_searchServiceACE3ProductLookup);

		sleepCusomized(txt_productSearch);
		sendKeys(txt_productSearch, product3EstimationDeatailsACESerivice);
		pressEnter(txt_productSearch);
		sleepCusomized(btn_resultProduct);
		doubleClick(btn_resultProduct);
		Thread.sleep(1000);

		sendKeys(txt_costServiceProductEstmationACE, costInputService3ACE);
		if (!isSelected(chk_billableEstmationRow3ACE)) {
			click(chk_billableEstmationRow3ACE);
		}
		/* ROW 4 */
		click(btn_addNewRow4EstmationDetailsACE);

		Select comboBox3 = new Select(driver.findElement(By.xpath(drop_type4ServiceACE)));
		selectText(drop_type4ServiceACE, typeEstmationAce4);
		String selectedComboValue3 = comboBox3.getFirstSelectedOption().getText();

		click(btn_searchProductLookuop4ACE);

		sleepCusomized(txt_productSearch);
		sendKeys(txt_productSearch, product4EstimationDeatailsACEService);
		pressEnter(txt_productSearch);
		sleepCusomized(btn_resultProduct);
		doubleClick(btn_resultProduct);
		Thread.sleep(1000);

		sendKeys(txt_cost4ServiceProductEstimationACE, costInputService4ACE);
		if (!isSelected(chk_billable4ServiceACeEstimation)) {
			click(chk_billable4ServiceACeEstimation);
		}
		click(tab_summaryACE);

		click(btn_checkoutACE);
		Thread.sleep(3000);

		click(btn_draft2);
		sleepCusomized(btn_releese2);

		click(btn_action);
		click(btn_deleteActionVrification);
		click(btn_deleteConfirationActionVerification);
		String header1 = getText(header_draft);
		if (!header1.equalsIgnoreCase("(Deleted)")) {
			Thread.sleep(7000);
			header1 = getText(header_draft);
		}
		if (header1.equalsIgnoreCase("(Deleted)")) {
			writeTestResults("Verify 'Delete' button is working", "'Delete' button should be working",
					"'Delete' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Delete' button is working", "'Delete' button should be working",
					"'Delete' button doesn't working", "fail");
		}

	}

	/* AV_IW_StockTake */
	public void navigateToNewStockTakePageActionVerification() throws Exception {
		navigateToInventoryAndWarehouseModuleActionVerification();
		sleepCusomized(btn_stockTake);
		click(btn_stockTake);
		sleepCusomized(btn_newStockTake);

		click(btn_newStockTake);
		sleepCusomized(drop_warehouseStockTske);

	}

	public void stockTakeProcessActionVerification() throws Exception {
		selectText(drop_warehouseStockTske, warehouseStockTake);
		sleepCusomized(btn_refreshStockTake);
		click(btn_refreshStockTake);

		sleepCusomized(txt_filterProductStockTake);
		sendKeys(txt_filterProductStockTake, batchProduct1StockTake);
		String product1_quantity = getText(txt_systemQuantityStockTake);
		int product1_quantity1 = Integer.parseInt(product1_quantity);
		int product1_quantity2 = product1_quantity1 + 5;
		String product1_final = String.valueOf(product1_quantity2);
		sendKeys(txt_physicalQuantityStockTake, product1_final);
		click(txt_systemQuantityStockTake);

		sendKeys(txt_filterProductStockTake, batchProduct2StockTake);
		String product2_quantity1 = getText(txt_systemQuantityStockTake);
		String replace_comma_product_quantity = product2_quantity1.replace(",", "");
		int product2_quantity11 = Integer.parseInt(replace_comma_product_quantity);
		int product2_quantity2 = product2_quantity11 - 5;
		String product2_final = String.valueOf(product2_quantity2);
		sendKeys(txt_physicalQuantityStockTake, product2_final);
		click(txt_systemQuantityStockTake);
	}

	public void actionVerificationStockTake() throws Exception {
		click(btn_draftAndNewActionVerification);
		int rowCount = driver.findElements(By.xpath(table_stockTakeActionVerification)).size();
		if (rowCount == 0) {
			writeTestResults("Verify 'Draft & New' button is working", "'Draft & New' button should be working",
					"'Draft & New' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Draft & New' button is working", "'Draft & New' button should be working",
					"'Draft & New' button doesn't working", "fail");
		}

		sleepCusomized(navigation_pane);
		Thread.sleep(2000);
		click(navigation_pane);

		sleepCusomized(inventory_module);
		click(inventory_module);

		click(btn_stockTake);

		click(btn_dtaftedStockTakeActionVerification);

		sleepCusomized(btn_releese2);
		click(btn_releese2);

		waitImplicit(navigation_pane, 10);
		click(navigation_pane);

		sleepCusomized(inventory_module);
		click(inventory_module);

		click(btn_stockTake);
		Thread.sleep(3000);

		click(btn_newStockTake);
		Thread.sleep(3000);

		selectText(drop_warehouseStockTske, warehouseStockTake);
		sleepCusomized(btn_refreshStockTake);
		click(btn_refreshStockTake);

		sleepCusomized(txt_filterProductStockTake);
		sendKeys(txt_filterProductStockTake, batchProduct1StockTake);
		String product1_quantity = getText(txt_systemQuantityStockTake);
		int product1_quantity1 = Integer.parseInt(product1_quantity);
		int product1_quantity2 = product1_quantity1 + 5;
		String product1_final = String.valueOf(product1_quantity2);
		sendKeys(txt_physicalQuantityStockTake, product1_final);
		click(txt_systemQuantityStockTake);

		sendKeys(txt_filterProductStockTake, batchProduct2StockTake);
		String product2_quantity1 = getText(txt_systemQuantityStockTake);
		String replace_comma_product_quantity = product2_quantity1.replace(",", "");
		int product2_quantity11 = Integer.parseInt(replace_comma_product_quantity);
		int product2_quantity2 = product2_quantity11 - 5;
		String product2_final = String.valueOf(product2_quantity2);
		sendKeys(txt_physicalQuantityStockTake, product2_final);
		click(txt_systemQuantityStockTake);

		click(btn_draft2);
		sleepCusomized(btn_releese2);
		if (isDisplayed(btn_releese2)) {
			writeTestResults("Verify 'Draft' button is working", "'Draft' button should be working",
					"'Draft' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Draft' button is working", "'Draft' button should be working",
					"'Draft' button doesn't working", "fail");
		}

		click(btn_editActionVerification);
		if (isDisplayed(btn_updateActionVerification)) {
			writeTestResults("Verify 'Edit' button is working", "'Edit' button should be working",
					"'Edit' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Edit' button is working", "'Edit' button should be working",
					"'Edit' button doesn't working", "fail");
		}

		click(btn_editActionVerification);
		sleepCusomized(btn_updateActionVerification);
		click(btn_updateActionVerification);
		sleepCusomized(btn_relese);
		if (isDisplayed(btn_relese)) {
			writeTestResults("Verify 'Update' button is working", "'Update' button should be working",
					"'Update' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Update' button is working", "'Update' button should be working",
					"'Update' button doesn't working", "fail");
		}

		click(btn_editActionVerification);
		sleepCusomized(btn_updateAndNewActionVerification);
		click(btn_updateAndNewActionVerification);
		int rowCount1 = driver.findElements(By.xpath(table_stockTakeActionVerification)).size();
		if (rowCount1 == 0) {
			writeTestResults("Verify 'Update & New' button is working", "'Update & New' button should be working",
					"'Update & New' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Update & New' button is working", "'Update & New' button should be working",
					"'Update & New' button doesn't working", "fail");
		}

		sleepCusomized(navigation_pane);
		Thread.sleep(2000);
		click(navigation_pane);

		click(inventory_module);

		click(btn_stockTake);

		click(btn_dtaftedStockTakeActionVerification);

		sleepCusomized(btn_relese);
		click(btn_relese);
		Thread.sleep(4000);
		trackCode = getText(lbl_creationsDocNo);
		String header1 = getText(header_draft);
		if (!header1.equalsIgnoreCase("(Released)")) {
			Thread.sleep(7000);
		}
		if (header1.equalsIgnoreCase("(Released)")) {
			writeTestResults("Verify 'Release' button is working", "'Release' button should be working",
					"'Release' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Release' button is working", "'Release' button should be working",
					"'Release' button doesn't working", "fail");
		}

		url_released_status = getCurrentUrl();
	}

	public void actionSectionVerificationStockTake() throws Exception {
		waitImplicit(navigation_pane, 10);
		sleepCusomized(navigation_pane);
		click(navigation_pane);
		sleepCusomized(inventory_module);
		click(inventory_module);
		sleepCusomized(btn_stockTake);
		click(btn_stockTake);
		sleepCusomized(btn_newStockTake);
		click(btn_newStockTake);
		sleepCusomized(drop_warehouseStockTske);
		selectText(drop_warehouseStockTske, warehouseStockTake);
		sleepCusomized(btn_refreshStockTake);
		click(btn_refreshStockTake);

		sleepCusomized(txt_filterProductStockTake);
		sendKeys(txt_filterProductStockTake, batchProduct1StockTake);
		String product1_quantity = getText(txt_systemQuantityStockTake);
		int product1_quantity1 = Integer.parseInt(product1_quantity);
		int product1_quantity2 = product1_quantity1 + 5;
		String product1_final = String.valueOf(product1_quantity2);
		sendKeys(txt_physicalQuantityStockTake, product1_final);
		click(txt_systemQuantityStockTake);

		sendKeys(txt_filterProductStockTake, batchProduct2StockTake);
		String product2_quantity1 = getText(txt_systemQuantityStockTake);
		String replace_comma_product_quantity = product2_quantity1.replace(",", "");
		int product2_quantity11 = Integer.parseInt(replace_comma_product_quantity);
		int product2_quantity2 = product2_quantity11 - 5;
		String product2_final = String.valueOf(product2_quantity2);
		sendKeys(txt_physicalQuantityStockTake, product2_final);
		click(txt_systemQuantityStockTake);
		click(btn_draft);
		sleepCusomized(btn_releese2);
		click(btn_action);
		click(btn_deleteActionVrification);
		click(btn_deleteConfirationActionVerification);
		String header1 = getText(header_draft);
		if (!header1.equalsIgnoreCase("(Deleted)")) {
			Thread.sleep(7000);
			header1 = getText(header_draft);
		}
		if (header1.equalsIgnoreCase("(Deleted)")) {
			writeTestResults("Verify 'Delete' button is working", "'Delete' button should be working",
					"'Delete' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Delete' button is working", "'Delete' button should be working",
					"'Delete' button doesn't working", "fail");
		}

		waitImplicit(navigation_pane, 10);
		sleepCusomized(navigation_pane);
		click(navigation_pane);
		sleepCusomized(inventory_module);
		click(inventory_module);
		sleepCusomized(btn_stockTake);
		click(btn_stockTake);
		sleepCusomized(btn_newStockTake);
		click(btn_newStockTake);
		sleepCusomized(drop_warehouseStockTske);
		selectText(drop_warehouseStockTske, warehouseStockTake);
		sleepCusomized(btn_refreshStockTake);
		click(btn_refreshStockTake);

		sleepCusomized(txt_filterProductStockTake);
		sendKeys(txt_filterProductStockTake, batchProduct1StockTake);
		String product1_quantity3 = getText(txt_systemQuantityStockTake);
		int product1_quantity13 = Integer.parseInt(product1_quantity3);
		int product1_quantity23 = product1_quantity13 + 5;
		String product1_final3 = String.valueOf(product1_quantity23);
		sendKeys(txt_physicalQuantityStockTake, product1_final3);
		click(txt_systemQuantityStockTake);

		sendKeys(txt_filterProductStockTake, batchProduct2StockTake);
		String product2_quantity13 = getText(txt_systemQuantityStockTake);
		String replace_comma_product_quantity3 = product2_quantity13.replace(",", "");
		int product2_quantity113 = Integer.parseInt(replace_comma_product_quantity3);
		int product2_quantity23 = product2_quantity113 - 5;
		String product2_final3 = String.valueOf(product2_quantity23);
		sendKeys(txt_physicalQuantityStockTake, product2_final3);
		click(txt_systemQuantityStockTake);
		click(btn_draft);
		sleepCusomized(btn_releese2);
		click(btn_releese2);
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_remindersAllocationMangerActionVerification);
		if (isDisplayed(header_reminderActionVerification)) {
			writeTestResults("Verify 'Reminders' button is working", "'Reminders' button should be working",
					"'Reminders' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Reminders' button is working", "'Reminders' button should be working",
					"'Reminders' button doesn't working", "fail");
		}
		click(btn_closeLookupActionVerification);
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_historyActionVerification);
		sleepCusomized(div_historyTableActionVerification);
		if (isDisplayed(div_historyTableActionVerification)) {
			writeTestResults("Verify 'History' button is working", "'History' button should be working",
					"'History' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'History' button is working", "'History' button should be working",
					"'History' button doesn't working", "fail");
		}

		click(btn_action);
		click(btn_activitiesActionVerification);
		if (isDisplayed(btn_activitiesHeaderActionVrification)) {
			writeTestResults("Verify 'Activities' button is working", "'Activities' button should be working",
					"'Activities' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Activities' button is working", "'Activities' button should be working",
					"'Activities' button doesn't working", "fail");
		}
		pageRefersh();

		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_reverseActionVrification);
		click(btn_reverseConfirationActionVerification);
		String header12 = getText(header_draft);
		if (!header12.equalsIgnoreCase("(Reversed)")) {
			Thread.sleep(7000);
			header12 = getText(header_draft);
		}
		if (header12.equalsIgnoreCase("(Reversed)")) {
			writeTestResults("Verify 'Reverse' button is working", "'Reverse' button should be working",
					"'Reverse' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Reverse' button is working", "'Reverse' button should be working",
					"'Reverse' button doesn't working", "fail");
		}
		click(btn_closeLookupActionVerification);

		openPage(url_released_status);
		click(btn_action);
		click(btn_convertToStockAdjustment);
		String header123 = getText(header_pageLabel);
		if (!header123.equalsIgnoreCase("Stock Adjustment")) {
			Thread.sleep(7000);
			header123 = getText(header_pageLabel);
		}

		if (header123.equalsIgnoreCase("Stock Adjustment")) {
			writeTestResults("Verify 'Convert to Stock Adjustment' button is working",
					"'Convert to Stock Adjustment' button should be working",
					"'Convert to Stock Adjustment' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Convert to Stock Adjustment' button is working",
					"'Convert to Stock Adjustment' button should be working",
					"'Convert to Stock Adjustment' button doesn't working", "fail");
		}
	}

	/* AV_TC_InterdepartmentalTransferOrder */
	public void navigateToInterDepartmentalTaransferOrderActoionVerification() throws Exception {
		navigateToInventoryAndWarehouseModuleActionVerification();
		click(btn_InterDepartmentTransferOrder);
		click(btn_newInternelReturnOrder);
	}

	public void fillInterDepartmentalTransferOrderActionVerification() throws Exception {
		selectText(txt_fromWareTO, fromWareInterDepartmentlTransferOrder);

		// To ware
		selectText(txt_toWareTO, toWareInterDepartmentlTransferOrder);

		selectText(drop_salesPriceModel, salesPriceModelInterDepartmentalTransferOrder);
		click(btn_salesPriceModelConfirmation);

		/* Product add to the grid */
		for (int i = 1; i < 3; i++) {
			String str_i = String.valueOf(i);
			click(btn_productLookupStockAdjForLoop.replace("row", str_i).replace("tblStockAdj", "tblTransferOrder"));

			String product = "";

			if (i == 1) {
				product = "BatchSpecific";
			} else if (i == 2) {
				product = "BatchFifo";
			} else if (i == 3) {
				product = "Lot";
			} else if (i == 4) {
				product = "SerielSpecific";
			} else if (i == 5) {
				product = "SerielBatchSpecific";
			} else if (i == 6) {
				product = "SerielBatchFifo";
			} else if (i == 7) {
				product = "SerirlFifo";
			} else {
				System.out.println("Product type cannt identified.........");
			}
			sleepCusomized(txt_productSearch);
			sendKeys(txt_productSearch, readTestCreation(product));
			pressEnter(txt_productSearch);
			Thread.sleep(3000);
			sleepCusomized(btn_resultProduct);
			doubleClick(btn_resultProduct);

			String txt_qty = txt_qtyProductGridInterDepartmentTransferOrder.replace("row", str_i);

			sendKeys(txt_qty, qtyInterDepartmentTransferOrder);

			sleepCusomized(btn_addNewRow);
			click(btn_addNewRow);
		}

		click(btn_deleteEmotyRowStockAdjustment.replace("tblStockAdj", "tblTransferOrder").replace("8", "3"));
	}

	public void verifyActionsInterdepartmentalTransferOrderActionVerification() throws Exception {
		click(btn_checkout);
		click(btn_draftAndNewActionVerification);
		int rowCount = driver
				.findElements(
						By.xpath(table_stockTakeActionVerification.replace("tblProductHeader", "tblTransferOrder")))
				.size();
		if (rowCount == 1) {
			writeTestResults("Verify 'Draft & New' button is working", "'Draft & New' button should be working",
					"'Draft & New' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Draft & New' button is working", "'Draft & New' button should be working",
					"'Draft & New' button doesn't working", "fail");
		}

		fillInterDepartmentalTransferOrderActionVerification();

		click(btn_checkout);
		click(btn_draft2);
		sleepCusomized(btn_releese2);
		if (isDisplayed(btn_releese2)) {
			writeTestResults("Verify 'Draft' button is working", "'Draft' button should be working",
					"'Draft' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Draft' button is working", "'Draft' button should be working",
					"'Draft' button doesn't working", "fail");
		}

		click(btn_editActionVerification);
		if (isDisplayed(btn_updateActionVerification)) {
			writeTestResults("Verify 'Edit' button is working", "'Edit' button should be working",
					"'Edit' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Edit' button is working", "'Edit' button should be working",
					"'Edit' button doesn't working", "fail");
		}

		click(btn_checkout);
		sleepCusomized(btn_updateActionVerification);
		click(btn_updateActionVerification);
		sleepCusomized(btn_relese);
		if (isDisplayed(btn_relese)) {
			writeTestResults("Verify 'Update' button is working", "'Update' button should be working",
					"'Update' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Update' button is working", "'Update' button should be working",
					"'Update' button doesn't working", "fail");
		}

		click(btn_editActionVerification);
		sleepCusomized(btn_checkout);
		click(btn_checkout);
		sleepCusomized(btn_updateAndNewActionVerification);
		click(btn_updateAndNewActionVerification);
		int rowCount_update_and_new = driver
				.findElements(
						By.xpath(table_stockTakeActionVerification.replace("tblProductHeader", "tblTransferOrder")))
				.size();
		if (rowCount_update_and_new == 1) {
			writeTestResults("Verify 'Update & New' button is working", "'Update & New' button should be working",
					"'Update & New' button successfully working", "pass");
		} else {

			writeTestResults("Verify 'Update & New' button is working", "'Update & New' button should be working",
					"'Update & New' button doesn't working", "fail");
		}

		fillInterDepartmentalTransferOrderActionVerification();

		click(btn_checkout);
		click(btn_draft);
		sleepCusomized(btn_releese2);
		click(btn_releese2);
		if (isDisplayed(btn_goToPageLink)) {
			writeTestResults("Verify 'Release' button is working", "'Release' button should be working",
					"'Release' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Release' button is working", "'Release' button should be working",
					"'Release' button doesn't working", "fail");
		}

	}

	public void actionSectionSectionVerfication() throws Exception {
		click(btn_closeGoToLinkPopup);
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_remindersAllocationMangerActionVerification);
		if (isDisplayed(header_reminderActionVerification)) {
			writeTestResults("Verify 'Reminders' button is working", "'Reminders' button should be working",
					"'Reminders' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Reminders' button is working", "'Reminders' button should be working",
					"'Reminders' button doesn't working", "fail");
		}

		click(btn_closeLookupActionVerification);
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_historyActionVerification);
		sleepCusomized(div_historyTableActionVerification);
		if (isDisplayed(div_historyTableActionVerification)) {
			writeTestResults("Verify 'History' button is working", "'History' button should be working",
					"'History' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'History' button is working", "'History' button should be working",
					"'History' button doesn't working", "fail");
		}

		click(btn_closeLookupActionVerification);
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_dockflowActionVrification);
		sleepCusomized(header_documentFlowActionVerification);
		if (isDisplayed(header_documentFlowActionVerification)) {
			writeTestResults("Verify 'Doc Flow' button is working", "'Doc Flow' button should be working",
					"'Doc Flow' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Doc Flow' button is working", "'Doc Flow' button should be working",
					"'Doc Flow' button doesn't working", "fail");
		}

		pageRefersh();

		click(btn_action);
		click(btn_activitiesActionVerification);
		if (isDisplayed(btn_activitiesHeaderActionVrification)) {
			writeTestResults("Verify 'Activities' button is working", "'Activities' button should be working",
					"'Activities' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Activities' button is working", "'Activities' button should be working",
					"'Activities' button doesn't working", "fail");
		}
		pageRefersh();

		url_released_status = getCurrentUrl();
		openPage(url_released_status);
		click(btn_duplicateActionVerification);
		if (isDisplayed(btn_draft2)) {
			writeTestResults("Verify 'Duplicate' button is working", "'Duplicate' button should be working",
					"'Duplicate' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Duplicate' button is working", "'Duplicate' button should be working",
					"'Duplicate' button doesn't working", "fail");
		}

		openPage(url_released_status);

		click(btn_action);
		click(btn_reverseActionVrification);
		click(btn_reverseConfirationActionVerification);
		String header12 = getText(header_draft);
		if (!header12.equalsIgnoreCase("( Reversed )")) {
			Thread.sleep(7000);
			header12 = getText(header_draft);
		}
		if (header12.equalsIgnoreCase("( Reversed )")) {
			writeTestResults("Verify 'Reverse' button is working", "'Reverse' button should be working",
					"'Reverse' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Reverse' button is working", "'Reverse' button should be working",
					"'Reverse' button doesn't working", "fail");
		}

	}

	/* AV_IW_OutboundLoanOrder */
	public void naviagteToOutboundLOanOrderFormActionVerification() throws Exception {
		navigateToInventoryAndWarehouseModuleActionVerification();
		click(btn_outboundLoanOrder);
		click(btn_newOutboundLoanOrder);
	}

	public void fillOutboundLoanOrderActionVerification() throws Exception {
		sleepCusomized(btn_customerAccountSearchLookupILO);
		click(btn_customerAccountSearchLookupILO);

		sendKeys(txt_accountSearchILO, customerAccountILO2);

		pressEnter(txt_accountSearchILO);
		Thread.sleep(3000);
		doubleClick(lnk_resultCustomerAccount);

		click(btn_billingAddressILO);
		sendKeys(txt_billingAddressILO, address1ILO);
		sendKeys(txt_shippingAddressILO, address2ILO);
		click(btn_applyAddressILO);
		selectIndex(dropdown_salesUnitILO, 3);

		/* Product add to the grid */
		for (int i = 1; i < 2; i++) {
			String str_i = String.valueOf(i);

			String lookup_path = btn_productLookupInterDeprtmentTransferOrderForLoop.replace("row", str_i)
					.replace("column", "8").replace("tblStockAdj", "tblLoanOrder");
			sleepCusomized(lookup_path);
			click(lookup_path);

			String product = "";

			if (i == 1) {
				product = "BatchFifo";
			} else if (i == 2) {
				product = "BatchSpecific";
			} else if (i == 3) {
				product = "Lot";
			} else if (i == 4) {
				product = "SerielSpecific";
			} else if (i == 5) {
				product = "SerielBatchSpecific";
			} else if (i == 6) {
				product = "SerielBatchFifo";
			} else if (i == 7) {
				product = "SerirlFifo";
			} else {
				System.out.println("Product type cannt identified.........");
			}
			sleepCusomized(txt_productSearch);
			sendKeys(txt_productSearch, readTestCreation(product));
			pressEnter(txt_productSearch);
			Thread.sleep(3000);

			sleepCusomized(btn_resultProduct);
			doubleClick(btn_resultProduct);

			String txt_qty = txt_qtyProductGridInboundLoanOrder.replace("row", str_i);

			sendKeys(txt_qty, qtyInterDepartmentTransferOrder);

			sleepCusomized(btn_addNewRow);
			click(btn_addNewRow);
		}

		click(btn_deleteEmotyRowStockAdjustment.replace("tblStockAdj", "tblLoanOrder").replace("8", "2"));

		click(btn_tabInboundLoanOrder);
		selectText(drop_warehouseInternelOrderEmployeeRequest, warehouseInternelOrderEmployeeRequest);

		click(btn_detailsTabILO);

		click(chkbox_underDeliveryILO);
		click(chkbox_partielDeliveryILO);

		click(btn_reqSlipDateILO);
		click(btn_reqSlipDate17ILO);

		click(btn_conSlipDateILO);
		click(btn_conSlipDate18ILO);

		click(btn_reqReciptDateILO);
		click(btn_reqReciptDate19ILO);

		click(btn_conReciptDateILO);
		click(btn_conReciptDate20ILO);
	}

	public void actionVerficationOutboundLoanOrder() throws Exception {
		click(btn_draft2);
		sleepCusomized(btn_releese2);
		if (isDisplayed(btn_releese2)) {
			writeTestResults("Verify 'Draft' button is working", "'Draft' button should be working",
					"'Draft' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Draft' button is working", "'Draft' button should be working",
					"'Draft' button doesn't working", "fail");
		}

		click(btn_releese2);
		if (isDisplayed(btn_goToPageLink)) {
			writeTestResults("Verify 'Release' button is working", "'Release' button should be working",
					"'Release' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Release' button is working", "'Release' button should be working",
					"'Release' button doesn't working", "fail");
		}
		click(btn_closeGoToLinkPopup);

		waitImplicit(navigation_pane, 10);
		click(navigation_pane);

		click(inventory_module);
		click(btn_outboundLoanOrder);
		click(btn_newOutboundLoanOrder);
		fillOutboundLoanOrderActionVerification();
		click(btn_draftAndNewActionVerification);
		int rowCount = driver
				.findElements(By.xpath(table_stockTakeActionVerification.replace("tblProductHeader", "tblLoanOrder")))
				.size();
		if (rowCount == 1) {
			writeTestResults("Verify 'Draft & New' button is working", "'Draft & New' button should be working",
					"'Draft & New' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Draft & New' button is working", "'Draft & New' button should be working",
					"'Draft & New' button doesn't working", "fail");
		}

		fillOutboundLoanOrderActionVerification();
		click(btn_draft);
		click(btn_editActionVerification);
		if (isDisplayed(btn_updateActionVerification)) {
			writeTestResults("Verify 'Edit' button is working", "'Edit' button should be working",
					"'Edit' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Edit' button is working", "'Edit' button should be working",
					"'Edit' button doesn't working", "fail");
		}

		sleepCusomized(btn_updateActionVerification);
		click(btn_updateActionVerification);
		sleepCusomized(btn_relese);
		if (isDisplayed(btn_relese)) {
			writeTestResults("Verify 'Update' button is working", "'Update' button should be working",
					"'Update' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Update' button is working", "'Update' button should be working",
					"'Update' button doesn't working", "fail");
		}

		click(btn_editActionVerification);
		sleepCusomized("Sleep");
		click(btn_updateAndNewActionVerification);
		sleepCusomized("Sleep");
		if (isDisplayed(btn_draft2)) {
			writeTestResults("Verify 'Update & New' button is working", "'Update & New' button should be working",
					"'Update & New' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Update & New' button is working", "'Update & New' button should be working",
					"'Update & New' button doesn't working", "fail");
		}
	}

	public void actionSectionVerifcationOutboundLOanOrder() throws Exception {
		fillOutboundLoanOrderActionVerification();
		click(btn_draft2);
		sleepCusomized(btn_relese);
		click(btn_action);
		click(btn_deleteActionVrification);
		click(btn_deleteConfirationActionVerification);
		String header1 = getText(header_draft);
		if (!header1.equalsIgnoreCase("(Deleted)")) {
			Thread.sleep(7000);
			header1 = getText(header_draft);
		}
		if (header1.equalsIgnoreCase("(Deleted)")) {
			writeTestResults("Verify 'Delete' button is working", "'Delete' button should be working",
					"'Delete' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Delete' button is working", "'Delete' button should be working",
					"'Delete' button doesn't working", "fail");
		}
		FinanceModule obj = new FinanceModule();
		obj.commonLogin();
		waitImplicit(navigation_pane, 10);
		click(navigation_pane);

		click(inventory_module);
		click(btn_outboundLoanOrder);
		sleepCusomized(btn_newOutboundLoanOrder);
		click(btn_newOutboundLoanOrder);
		fillOutboundLoanOrderActionVerification();
		click(btn_draft2);
		sleepCusomized(btn_relese);
		click(btn_relese);

		sleepCusomized(btn_goToPageLink);
		click(btn_goToPageLink);
		switchWindow();
		sleepCusomized(btn_draft);
		click(btn_draft);
		sleepCusomized(btn_relese);
		click(btn_relese);
		switchWindow();
		pageRefersh();

		url_released_status = getCurrentUrl();

		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_remindersAllocationMangerActionVerification);
		if (isDisplayed(header_reminderActionVerification)) {
			writeTestResults("Verify 'Reminders' button is working", "'Reminders' button should be working",
					"'Reminders' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Reminders' button is working", "'Reminders' button should be working",
					"'Reminders' button doesn't working", "fail");
		}

		sleepCusomized(btn_closeLookupActionVerification);
		click(btn_closeLookupActionVerification);
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_historyActionVerification);
		sleepCusomized(div_historyTableActionVerification);
		if (isDisplayed(div_historyTableActionVerification)) {
			writeTestResults("Verify 'History' button is working", "'History' button should be working",
					"'History' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'History' button is working", "'History' button should be working",
					"'History' button doesn't working", "fail");
		}
		pageRefersh();
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_dockflowActionVrification);
		sleepCusomized(header_documentFlowActionVerification);
		if (isDisplayed(header_documentFlowActionVerification)) {
			writeTestResults("Verify 'Doc Flow' button is working", "'Doc Flow' button should be working",
					"'Doc Flow' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Doc Flow' button is working", "'Doc Flow' button should be working",
					"'Doc Flow' button doesn't working", "fail");
		}

		openPage(url_released_status);
		click(btn_action);
		click(btn_convertToInboundLoanOrderActionVerification);
		String header123 = getText(header_pageLabel);
		if (!header123.equalsIgnoreCase("Inbound Loan Order")) {
			Thread.sleep(7000);
			header123 = getText(header_pageLabel);
		}

		if (header123.equalsIgnoreCase("Inbound Loan Order")) {
			writeTestResults("Verify 'Convert to Inbound Loan Order' button is working",
					"'Convert to Inbound Loan Order' button should be working",
					"'Convert to Inbound Loan Order' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Convert to Inbound Loan Order' button is working",
					"'Convert to Inbound Loan Order' button should be working",
					"'Convert to Inbound Loan Order' button doesn't working", "fail");
		}

		openPage(url_released_status);
		click(btn_action);
		click(btn_convertToInboundShimentActionVerification);
		String header1234 = getText(header_pageLabel);
		if (!header1234.equalsIgnoreCase("Inbound Shipment")) {
			Thread.sleep(7000);
			header1234 = getText(header_pageLabel);
		}

		if (header1234.equalsIgnoreCase("Inbound Shipment")) {
			writeTestResults("Verify 'Convert to Inbound Shipment' button is working",
					"'Convert to Inbound Shipment' button should be working",
					"'Convert to Inbound Shipment' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Convert to Inbound Shipment' button is working",
					"'Convert to Inbound Shipment' button should be working",
					"'Convert to Inbound Shipment' button doesn't working", "fail");
		}

		openPage(url_released_status);
		click(btn_action);
		click(btn_convertToSalesInvoiceActionVerification);
		String header5 = getText(header_pageLabel);
		if (!header5.equalsIgnoreCase("Sales Invoice")) {
			Thread.sleep(7000);
			header5 = getText(header_pageLabel);
		}

		if (header5.equalsIgnoreCase("Sales Invoice")) {
			writeTestResults("Verify 'Convert to Sales Invoice' button is working",
					"'Convert to Sales Invoice' button should be working",
					"'Convert to Sales Invoice' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Convert to Sales Invoice' button is working",
					"'Convert to Sales Invoice' button should be working",
					"'Convert to Sales Invoice' button doesn't working", "fail");
		}

	}

	/* common */
	public boolean isDisplayedCustomized(String locator) throws Exception {
		boolean found = false;

		if (driver.findElement(getLocator(locator)).isDisplayed()) {
			found = true; // FOUND IT
		}
		if (found = false) {
			Thread.sleep(2000);
			if (driver.findElement(getLocator(locator)).isDisplayed()) {
				found = true; // FOUND IT
			}
		}
		return found;
	}

	/* common */
	public boolean isDisplayedQuickCheck(String locator) throws Exception {
		boolean found = false;

		try {
			if (driver.findElement(getLocator(locator)).isDisplayed()) {
				found = true; // FOUND IT
			}
		} catch (Exception e) {
		}
		return found;
	}

	/* AV_IW_AssemblyPrposal */
	public void navigateToAssemblyProposalActionVerification() throws Exception {
		navigateToInventoryAndWarehouseModuleActionVerification();
		Thread.sleep(2000);
		click(btn_assemblyProposal);
		Thread.sleep(2000);
		click(btn_newAssemblyProcess);
		url_new_status = getCurrentUrl();

	}

	public void fillAssemblyProposalActionVerification() throws Exception {
		Thread.sleep(3000);
		click(btn_productLoockupActionVerfication);
		Thread.sleep(2000);
		sendKeys(txt_productSearchActionVerifcation, assemblyProductAssemblyProposal);
		pressEnter(txt_productSearchActionVerifcation);
		Thread.sleep(3000);
		sleepCusomized(btn_resultAssemblyProductActionVerification);
		doubleClick(btn_resultAssemblyProductActionVerification);

		selectIndex(drop_priorityAssemblyProposal, 1);

		Thread.sleep(1000);
		click(datepliker_requestDateActionVerification);
		FinanceModule obj = new FinanceModule();
		String date = obj.currentDay() + "_Link";
		click(date);

		click(datepliker_requestEndDateActionVerification);
		click(btn_clanderFw);
		click("28_Link");

		click(btn_productionUnitLookupActionVerification);
		Thread.sleep(3000);

		sendKeys(txt_productionUnitSearchActionVerification, assemblyProcessProductionUnitActionVerification);
		pressEnter(txt_productionUnitSearchActionVerification);

		Thread.sleep(3000);
		doubleClick(result_productionUnitActionVerification);

		Thread.sleep(2000);

	}

	public void actionVerificationAssemblyProposal() throws Exception {
		click(btn_draftAndNewActionVerification);
		Thread.sleep(3000);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_warehouseAssemblyProcessActionVerification)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals("--None--")) {
			writeTestResults("Verify 'Draft & New' button is working", "'Draft & New' button should be working",
					"'Draft & New' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Draft & New' button is working", "'Draft & New' button should be working",
					"'Draft & New' button doesn't working", "fail");
		}

		fillAssemblyProposalActionVerification();
		click(btn_draft2);
		sleepCusomized(btn_releese2);
		if (isDisplayed(btn_releese2)) {
			writeTestResults("Verify 'Draft' button is working", "'Draft' button should be working",
					"'Draft' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Draft' button is working", "'Draft' button should be working",
					"'Draft' button doesn't working", "fail");
		}

		click(btn_editActionVerification);
		if (isDisplayed(btn_updateActionVerification)) {
			writeTestResults("Verify 'Edit' button is working", "'Edit' button should be working",
					"'Edit' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Edit' button is working", "'Edit' button should be working",
					"'Edit' button doesn't working", "fail");
		}

		click(btn_updateActionVerification);
		if (isDisplayed(btn_editActionVerification)) {
			writeTestResults("Verify 'Update' button is working", "'Update' button should be working",
					"'Update' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Update' button is working", "'Update' button should be working",
					"'Update' button doesn't working", "fail");
		}

		click(btn_editActionVerification);
		sleepCusomized("Sleep");
		click(btn_updateAndNewActionVerification);
		sleepCusomized("Sleep");
		if (isDisplayed(btn_draft2)) {
			writeTestResults("Verify 'Update & New' button is working", "'Update & New' button should be working",
					"'Update & New' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Update & New' button is working", "'Update & New' button should be working",
					"'Update & New' button doesn't working", "fail");
		}

		fillAssemblyProposalActionVerification();
		click(btn_draftActionVerification);
		click(btn_releese2);
		Thread.sleep(4000);
		trackCode = getText(lbl_creationsDocNo);
		String header1 = getText(header_draft);
		if (!header1.equalsIgnoreCase("(Released)")) {
			Thread.sleep(7000);
		}
		if (header1.equalsIgnoreCase("(Released)")) {
			writeTestResults("Verify 'Release' button is working", "'Release' button should be working",
					"'Release' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Release' button is working", "'Release' button should be working",
					"'Release' button doesn't working", "fail");
		}

		url_released_status = getCurrentUrl();

	}

	public void actionSectionVerificationAssemblyProposal() throws Exception {
		openPage(url_new_status);
		fillAssemblyProposalActionVerification();
		click(btn_draft2);
		Thread.sleep(4000);
		click(btn_action);
		click(btn_deleteActionVrification);
		Thread.sleep(2000);
		click(btn_deleteConfirationActionVerification);
		Thread.sleep(2000);
		String header1 = getText(header_draft);
		if (!header1.equalsIgnoreCase("(Deleted)")) {
			Thread.sleep(7000);
			header1 = getText(header_draft);
		}
		if (header1.equalsIgnoreCase("(Deleted)")) {
			writeTestResults("Verify 'Delete' button is working", "'Delete' button should be working",
					"'Delete' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Delete' button is working", "'Delete' button should be working",
					"'Delete' button doesn't working", "fail");
		}

		openPage(url_released_status);
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_remindersAllocationMangerActionVerification);
		if (isDisplayed(header_reminderActionVerification)) {
			writeTestResults("Verify 'Reminders' button is working", "'Reminders' button should be working",
					"'Reminders' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Reminders' button is working", "'Reminders' button should be working",
					"'Reminders' button doesn't working", "fail");
		}

		sleepCusomized(btn_closeLookupActionVerification);
		click(btn_closeLookupActionVerification);
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_historyActionVerification);
		sleepCusomized(div_historyTableActionVerification);
		if (isDisplayed(div_historyTableActionVerification)) {
			writeTestResults("Verify 'History' button is working", "'History' button should be working",
					"'History' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'History' button is working", "'History' button should be working",
					"'History' button doesn't working", "fail");
		}

		pageRefersh();
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_dockflowActionVrification);
		sleepCusomized(header_documentFlowActionVerification);
		if (isDisplayed(header_documentFlowActionVerification)) {
			writeTestResults("Verify 'Doc Flow' button is working", "'Doc Flow' button should be working",
					"'Doc Flow' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Doc Flow' button is working", "'Doc Flow' button should be working",
					"'Doc Flow' button doesn't working", "fail");
		}

		openPage(url_released_status);
		click(btn_action);
		click(btn_convertToAssemblyOrderActionVerification);
		Thread.sleep(3000);
		if (isDisplayed(header_assemblyOrderTaskActionVerification)) {
			writeTestResults("Verify 'Genarate Assembly Orders' button is working",
					"'Genarate Assembly Orders' button should be working",
					"'Genarate Assembly Orders' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Genarate Assembly Orders' button is working",
					"'Genarate Assembly Orders' button should be working",
					"'Genarate Assembly Orders' button doesn't working", "fail");
		}

		openPage(url_released_status);

		click(btn_action);
		click(btn_reverseActionVrification);
		click(btn_reverseConfirationActionVerification);
		String header12 = getText(header_draft);
		if (!header12.equalsIgnoreCase("(Reversed)")) {
			Thread.sleep(7000);
			header12 = getText(header_draft);
		}
		if (header12.equalsIgnoreCase("(Reversed)")) {
			writeTestResults("Verify 'Reverse' button is working", "'Reverse' button should be working",
					"'Reverse' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Reverse' button is working", "'Reverse' button should be working",
					"'Reverse' button doesn't working", "fail");
		}

		openPage(url_new_status);
		fillAssemblyProposalActionVerification();
		click(btn_draft2);
		sleepCusomized(btn_relese);
		click(btn_duplicateActionVerification);
		if (isDisplayed(btn_draft2)) {
			writeTestResults("Verify 'Duplicate' button is working", "'Duplicate' button should be working",
					"'Duplicate' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Duplicate' button is working", "'Duplicate' button should be working",
					"'Duplicate' button doesn't working", "fail");
		}

		openPage(url_new_status);
		click(btn_copyFromActionVerification);
		if (isDisplayed(header_assemblyProposalActionVerification)) {
			writeTestResults("Verify 'Copy From' button is working", "'Copy From' button should be working",
					"'Copy From' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Copy From' button is working", "'Copy From' button should be working",
					"'Copy From' button doesn't working", "fail");
		}

	}

	/* AV_IW_RackTransfer */
	public void navigateToNewRackTransferActionVerification() throws Exception {
		navigateToInventoryAndWarehouseModuleActionVerification();
		Thread.sleep(2000);
		click(btn_rackTransfer);
		Thread.sleep(3000);
		click(btn_newRackTransfer);
	}

	public void fillRackTransferActionVerification() throws Exception {
		Thread.sleep(3000);
		url_new_status = getCurrentUrl();
		selectText(drop_warehouseRackTransfer, warehouseRackTransfer);
		selectText(drop_filterBy, filterByRackTransfer);

		click(btn_productDocIconRackTransfer);
		Thread.sleep(3000);
		click(chk_productSelectRackTransfer);
		click(btn_applyProductDocIconRackTransfer);
		Thread.sleep(2000);
		click(btn_rackTransferNosIcon);
		Thread.sleep(3000);

		sendKeys(txt_quantityRackTransfer, rackTransferQuantity);
		click(btn_applyProductDocIconRackTransfer);
		Thread.sleep(2000);
	}

	public void actionVerificationRackTransfer() throws Exception {
		click(btn_draftAndNewActionVerification);
		Thread.sleep(3000);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_warehouseRackTransfer)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (!selectedComboValue.equals(warehouseRackTransfer)) {
			writeTestResults("Verify 'Draft & New' button is working", "'Draft & New' button should be working",
					"'Draft & New' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Draft & New' button is working", "'Draft & New' button should be working",
					"'Draft & New' button doesn't working", "fail");
		}
		openPage(url_new_status);
		Thread.sleep(3000);
		fillRackTransferActionVerification();
		click(btn_draft2);
		sleepCusomized(btn_releese2);
		if (isDisplayed(btn_releese2)) {
			writeTestResults("Verify 'Draft' button is working", "'Draft' button should be working",
					"'Draft' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Draft' button is working", "'Draft' button should be working",
					"'Draft' button doesn't working", "fail");
		}

		click(btn_editActionVerification);
		Thread.sleep(2000);
		if (isDisplayed(btn_updateActionVerification)) {
			writeTestResults("Verify 'Edit' button is working", "'Edit' button should be working",
					"'Edit' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Edit' button is working", "'Edit' button should be working",
					"'Edit' button doesn't working", "fail");
		}
		click(btn_updateActionVerification);
		if (isDisplayed(btn_editActionVerification)) {
			writeTestResults("Verify 'Update' button is working", "'Update' button should be working",
					"'Update' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Update' button is working", "'Update' button should be working",
					"'Update' button doesn't working", "fail");
		}

		click(btn_editActionVerification);
		sleepCusomized("Sleep");
		click(btn_updateAndNewActionVerification);
		sleepCusomized("Sleep");
		if (isDisplayed(btn_draft2)) {
			writeTestResults("Verify 'Update & New' button is working", "'Update & New' button should be working",
					"'Update & New' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Update & New' button is working", "'Update & New' button should be working",
					"'Update & New' button doesn't working", "fail");
		}

		openPage(url_new_status);
		Thread.sleep(3000);
		fillRackTransferActionVerification();
		click(btn_draftActionVerification);
		Thread.sleep(3000);
		click(brn_serielBatchCapture);
		click(btn_captureItem1);
		click(btn_updateCapture);
		click(btn_backButtonUpdateCapture);
		click(btn_releese2);
		Thread.sleep(3000);
		trackCode = getText(lbl_creationsDocNo);
		String header1 = getText(header_draft);
		if (!header1.equalsIgnoreCase("( Released )")) {
			Thread.sleep(7000);
		}
		if (header1.equalsIgnoreCase("( Released )")) {
			writeTestResults("Verify 'Release' button is working", "'Release' button should be working",
					"'Release' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Release' button is working", "'Release' button should be working",
					"'Release' button doesn't working", "fail");
		}

		url_released_status = getCurrentUrl();
	}

	public void actionSectionVerificationRackTransfer() throws Exception {
		openPage(url_new_status);
		Thread.sleep(3000);
		fillRackTransferActionVerification();
		click(btn_draft2);
		Thread.sleep(4000);
		click(btn_action);
		Thread.sleep(1000);
		click(btn_deleteActionVrification);
		Thread.sleep(2000);
		click(btn_deleteConfirationActionVerification);
		Thread.sleep(3000);
		String header1 = getText(header_draft);
		if (!header1.equalsIgnoreCase("( Deleted )")) {
			Thread.sleep(7000);
			header1 = getText(header_draft);
		}
		if (header1.equalsIgnoreCase("( Deleted )")) {
			writeTestResults("Verify 'Delete' button is working", "'Delete' button should be working",
					"'Delete' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Delete' button is working", "'Delete' button should be working",
					"'Delete' button doesn't working", "fail");
		}

		openPage(url_released_status);
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_remindersAllocationMangerActionVerification);
		if (isDisplayed(header_reminderActionVerification)) {
			writeTestResults("Verify 'Reminders' button is working", "'Reminders' button should be working",
					"'Reminders' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Reminders' button is working", "'Reminders' button should be working",
					"'Reminders' button doesn't working", "fail");
		}

		sleepCusomized(btn_closeLookupActionVerification);
		click(btn_closeLookupActionVerification);
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_historyActionVerification);
		sleepCusomized(div_historyTableActionVerification);
		if (isDisplayed(div_historyTableActionVerification)) {
			writeTestResults("Verify 'History' button is working", "'History' button should be working",
					"'History' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'History' button is working", "'History' button should be working",
					"'History' button doesn't working", "fail");
		}

		pageRefersh();

		click(btn_action);
		click(btn_activitiesActionVerification);
		if (isDisplayed(btn_activitiesHeaderActionVrification)) {
			writeTestResults("Verify 'Activities' button is working", "'Activities' button should be working",
					"'Activities' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Activities' button is working", "'Activities' button should be working",
					"'Activities' button doesn't working", "fail");
		}

	}

	public void otherActionVerificationRackTransfer() throws Exception {
		openPage(url_new_status);
		Thread.sleep(3000);
		fillRackTransferActionVerification();
		click(btn_draftActionVerification);
		Thread.sleep(3000);
		click(brn_serielBatchCapture);
		if (isDisplayed(btn_captureItem1)) {
			writeTestResults("Verify 'Seriel Batch' button is working", "'Seriel Batch' button should be working",
					"'Seriel Batch' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Seriel Batch' button is working", "'Seriel Batch' button should be working",
					"'Seriel Batch' button doesn't working", "fail");
		}

	}

	/* AV_IW_AssemblyOrder */
	public void navigateToAssemblyOrderActionVerification() throws Exception {
		navigateToInventoryAndWarehouseModuleActionVerification();
		click(btn_assemblyOredr);
		Thread.sleep(3000);
		click(btn_newAssemblyOrder);
		url_new_status = getCurrentUrl();
	}

	public void fillAssemblyOrderActionVerification() throws Exception {
		Thread.sleep(3000);
		click(btn_assemblyProductSearch);
		sleepCusomized(txt_productSearch);
		sendKeys(txt_productSearch, assemblyProductAO);
		pressEnter(txt_productSearch);
		sleepCusomized(btn_resultProduct);
		Thread.sleep(3000);
		sleepCusomized(btn_resultProduct);
		doubleClick(btn_resultProduct);

		sendKeys(txt_serialNoAO, genProductId() + currentTimeAndDate());

		selectIndex(drop_assemblyGroup, 0);

		click(btn_emloyeeSerchAO);
		sleepCusomized(txt_employee);
		sendKeys(txt_employee, employee1);
		pressEnter(txt_employee);
		sleepCusomized(lnk_resultEmployee);
		doubleClick(lnk_resultEmployee);

		selectIndex(drop_priorityAO, 1);

		click(btn_searchSCostEstimtioAo);
		sleepCusomized(txt_costEstimation);
		sendKeys(txt_costEstimation, readIWData(7));
		pressEnter(txt_costEstimation);
		sleepCusomized(lnk_resultCostEstimation);
		doubleClick(lnk_resultCostEstimation);

		selectIndex(drop_barcodeAO, 0);

		click(btn_searchProductionUnit);
		sleepCusomized(txt_productionUnit);
		sendKeys(txt_productionUnit, productionUnit1);
		pressEnter(txt_productionUnit);
		sleepCusomized(lnk_resultProductUnit);
		doubleClick(lnk_resultProductUnit);

		Thread.sleep(3000);
		selectText(drop_warehouseWIPAO, warehouseWIPAO);
		selectText(drop_inputWareAO, warehouseAO);
		selectText(drop_wareOutAO, warehouseAO);

		click(date_firstClickAO);
		String _28 = date_secondSeclectioAO.replace("date", "28");
		Thread.sleep(2000);
		click(btn_clanderFw);
		click(_28);

		click(date_secondClickAO);
		Thread.sleep(2000);
		click(btn_clanderFw);
		click(_28);

		click(tab_productAO);

		click(tab_estimationDeatailsAO);

	}

	public void actionVerificationAssemblyOrder() throws Exception {
		click(btn_draftAndNewActionVerification);
		Thread.sleep(3000);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_warehouseWIPAO)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (!selectedComboValue.equals(warehouseWIPAO)) {
			writeTestResults("Verify 'Draft & New' button is working", "'Draft & New' button should be working",
					"'Draft & New' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Draft & New' button is working", "'Draft & New' button should be working",
					"'Draft & New' button doesn't working", "fail");
		}

		Thread.sleep(3000);
		fillAssemblyOrderActionVerification();
		click(btn_draft2);
		sleepCusomized(btn_releese2);
		if (isDisplayed(btn_releese2)) {
			writeTestResults("Verify 'Draft' button is working", "'Draft' button should be working",
					"'Draft' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Draft' button is working", "'Draft' button should be working",
					"'Draft' button doesn't working", "fail");
		}

		Thread.sleep(1000);
		click(btn_editActionVerification);
		Thread.sleep(3000);
		if (isDisplayed(btn_updateActionVerification)) {
			writeTestResults("Verify 'Edit' button is working", "'Edit' button should be working",
					"'Edit' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Edit' button is working", "'Edit' button should be working",
					"'Edit' button doesn't working", "fail");
		}

		click(btn_updateActionVerification);
		if (isDisplayed(btn_editActionVerification)) {
			writeTestResults("Verify 'Update' button is working", "'Update' button should be working",
					"'Update' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Update' button is working", "'Update' button should be working",
					"'Update' button doesn't working", "fail");
		}

		click(btn_editActionVerification);
		sleepCusomized("Sleep");
		click(btn_updateAndNewActionVerification);
		sleepCusomized("Sleep");
		if (isDisplayed(btn_draft2)) {
			writeTestResults("Verify 'Update & New' button is working", "'Update & New' button should be working",
					"'Update & New' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Update & New' button is working", "'Update & New' button should be working",
					"'Update & New' button doesn't working", "fail");
		}

		fillAssemblyOrderActionVerification();
		click(btn_draftActionVerification);
		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(3000);
		trackCode = getText(lbl_creationsDocNo);
		String header1 = getText(header_draft);
		if (!header1.equalsIgnoreCase("(Released)")) {
			Thread.sleep(7000);
		}
		if (header1.equalsIgnoreCase("(Released)")) {
			writeTestResults("Verify 'Release' button is working", "'Release' button should be working",
					"'Release' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Release' button is working", "'Release' button should be working",
					"'Release' button doesn't working", "fail");
		}

		url_released_status = getCurrentUrl();
	}

	public void actionSectionVerificationAssemblyOrder() throws Exception {
		openPage(url_released_status);
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_remindersAllocationMangerActionVerification);
		if (isDisplayed(header_reminderActionVerification)) {
			writeTestResults("Verify 'Reminders' button is working", "'Reminders' button should be working",
					"'Reminders' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Reminders' button is working", "'Reminders' button should be working",
					"'Reminders' button doesn't working", "fail");
		}

		openPage(url_new_status);
		fillAssemblyOrderActionVerification();
		click(btn_draftActionVerification);
		Thread.sleep(4000);
		click(btn_action);
		Thread.sleep(1000);
		click(btn_deleteActionVrification);
		Thread.sleep(2000);
		click(btn_deleteConfirationActionVerification);
		Thread.sleep(3000);
		String header1 = getText(header_draft);
		if (!header1.equalsIgnoreCase("(Deleted)")) {
			Thread.sleep(7000);
			header1 = getText(header_draft);
		}
		if (header1.equalsIgnoreCase("(Deleted)")) {
			writeTestResults("Verify 'Delete' button is working", "'Delete' button should be working",
					"'Delete' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Delete' button is working", "'Delete' button should be working",
					"'Delete' button doesn't working", "fail");
		}

		openPage(url_released_status);
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_historyActionVerification);
		sleepCusomized(div_historyTableActionVerification);
		if (isDisplayed(div_historyTableActionVerification)) {
			writeTestResults("Verify 'History' button is working", "'History' button should be working",
					"'History' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'History' button is working", "'History' button should be working",
					"'History' button doesn't working", "fail");
		}

		pageRefersh();
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_dockflowActionVrification);
		sleepCusomized(header_documentFlowActionVerification);
		if (isDisplayed(header_documentFlowActionVerification)) {
			writeTestResults("Verify 'Doc Flow' button is working", "'Doc Flow' button should be working",
					"'Doc Flow' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Doc Flow' button is working", "'Doc Flow' button should be working",
					"'Doc Flow' button doesn't working", "fail");
		}

		openPage(url_new_status);
		Thread.sleep(3000);
		fillAssemblyOrderActionVerification();
		click(btn_draftActionVerification);
		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(3000);
		click(btn_action);
		click(btn_convertToCommonActionVerification.replace("action", "Change Serial Details"));
		Thread.sleep(3000);
		if (isDisplayed(header_commonActionVerification.replace("title", "Change Serial Details"))) {
			writeTestResults("Verify 'Change Serial Details' button is working",
					"'Change Serial Details' button should be working",
					"'Change Serial Details' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Change Serial Details' button is working",
					"'Change Serial Details' button should be working",
					"'Change Serial Details' button doesn't working", "fail");
		}
		url_released_status = getCurrentUrl();
		pageRefersh();
		Thread.sleep(3000);
		click(btn_action);
		click(btn_convertToCommonActionVerification.replace("action", "Create Internal Order"));
		Thread.sleep(3000);
		if (isDisplayed(header_commonActionVerification.replace("title", "Generate Internal Order"))) {
			writeTestResults("Verify 'Create Internal Order' button is working",
					"'Create Internal Order' button should be working",
					"'Create Internal Order' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Create Internal Order' button is working",
					"'Create Internal Order' button should be working",
					"'Create Internal Order' button doesn't working", "fail");
		}

		openPage(url_released_status);

		click(btn_action);
		click(btn_convertToCommonActionVerification.replace("action", "Job Start"));
		Thread.sleep(3000);
		if (isDisplayed(header_JobStartHoltActionVerification)) {
			writeTestResults("Verify 'Job Start' button is working", "'Job Start' button should be working",
					"'Job Start' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Job Start' button is working", "'Job Start' button should be working",
					"'Job Start' button doesn't working", "fail");
		}

		Thread.sleep(2000);
		click(btn_stratJobActionVerificationAssemblyOrder);
		Thread.sleep(4000);
		click(btn_action);
		Thread.sleep(2000);
		click(btn_completeJobActionVerificationAssemblyOrder);
		Thread.sleep(3000);
		if (isDisplayed(header_JobStartHoltActionVerification.replace("Start Process", "Complete Process"))) {
			writeTestResults("Verify 'Job Close' button is working", "'Job Close' button should be working",
					"'Job Close' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Job Close' button is working", "'Job Close' button should be working",
					"'Job Close' button doesn't working", "fail");
		}

		openPage(url_released_status);

		click(btn_action);
		click(btn_holdActionSectionVerification);
		Thread.sleep(2000);
		sendKeys(txt_changeReson, changeReasonHoldUnholdActionVerification);
		click(btn_okAssemblyOrderHoldUnholdActionVerification);
		Thread.sleep(3000);
		String header = getText(header_draft);
		if (!header.equalsIgnoreCase("(Hold)")) {
			Thread.sleep(5000);
			header = getText(header_draft);
		}
		if (header.equalsIgnoreCase("(Hold)")) {
			writeTestResults("Verify 'Hold' button is working", "'Hold' button should be working",
					"'Hold' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Hold' button is working", "'Hold' button should be working",
					"'Hold' button doesn't working", "fail");
		}
		openPage(url_released_status);
		Thread.sleep(3000);
		click(btn_action);
		click(btn_unholdActionSectionVerification);
		Thread.sleep(2000);
		sendKeys(txt_changeReson, changeReasonHoldUnholdActionVerification);
		click(btn_okAssemblyOrderHoldUnholdActionVerification);
		Thread.sleep(3000);
		String header12 = getText(header_draft);
		if (!header12.equalsIgnoreCase("(Released)")) {
			Thread.sleep(5000);
			header12 = getText(header_draft);
		}
		if (header12.equalsIgnoreCase("(Released)")) {
			writeTestResults("Verify 'Unnold' button is working", "'Unnold' button should be working",
					"'Unnold' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Unnold' button is working", "'Unnold' button should be working",
					"'Unnold' button doesn't working", "fail");
		}

		openPage(url_released_status);
		Thread.sleep(3000);
		click(btn_action);
		click(btn_closeActionSectionVerificatio);
		Thread.sleep(2000);
		sendKeys(txt_changeReson, changeReasonHoldUnholdActionVerification);
		click(btn_okAssemblyOrderHoldUnholdActionVerification);
		Thread.sleep(3000);
		String header2 = getText(header_draft);
		if (!header2.equalsIgnoreCase("(Closed)")) {
			Thread.sleep(5000);
			header2 = getText(header_draft);
		}
		if (header2.equalsIgnoreCase("(Closed)")) {
			writeTestResults("Verify 'Close' button is working", "'Close' button should be working",
					"'Close' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Close' button is working", "'Close' button should be working",
					"'Close' button doesn't working", "fail");
		}
		openPage(url_new_status);
		Thread.sleep(3000);
		fillAssemblyOrderActionVerification();
		click(btn_draftActionVerification);
		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(3000);
		click(btn_action);
		click(btn_convertToCommonActionVerification.replace("action", "Job Start"));
		Thread.sleep(3000);
		Thread.sleep(2000);
		click(btn_stratJobActionVerificationAssemblyOrder);
		Thread.sleep(4000);
		click(btn_action);
		Thread.sleep(2000);
		click(btn_completeJobActionVerificationAssemblyOrder);
		Thread.sleep(3000);
		click(btn_stratJobActionVerificationAssemblyOrder.replace("Start", "Complete"));
		Thread.sleep(4000);
		click(btn_action);
		click(btn_convertToCommonActionVerification.replace("action", "Convert to Internal Receipt"));
		Thread.sleep(3000);
		if (isDisplayed(header_commonActionVerification.replace("title", "Generate Internal Receipt"))) {
			writeTestResults("Verify 'Convert to Internal Receipt' button is working",
					"'Convert to Internal Receipt' button should be working",
					"'Convert to Internal Receipt' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Convert to Internal Receipt' button is working",
					"'Convert to Internal Receipt' button should be working",
					"'Convert to Internal Receipt' button doesn't working", "fail");
		}
		pageRefersh();
		click(btn_action);
		click(btn_reverseActionVrification);
		click(btn_reverseConfirationActionVerification);
		String header123 = getText(header_draft);
		if (!header123.equalsIgnoreCase("(Reversed)")) {
			Thread.sleep(7000);
			header123 = getText(header_draft);
		}
		if (header123.equalsIgnoreCase("(Reversed)")) {
			writeTestResults("Verify 'Reverse' button is working", "'Reverse' button should be working",
					"'Reverse' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Reverse' button is working", "'Reverse' button should be working",
					"'Reverse' button doesn't working", "fail");
		}

	}

	public void otherActionVerificationAssemblyOrder() throws Exception {
		click(btn_duplicateActionVerification);
		if (isDisplayed(btn_draft2)) {
			writeTestResults("Verify 'Duplicate' button is working", "'Duplicate' button should be working",
					"'Duplicate' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Duplicate' button is working", "'Duplicate' button should be working",
					"'Duplicate' button doesn't working", "fail");
		}

		openPage(url_new_status);
		isDisplayed(btn_copyFromActionVerification);
		click(btn_copyFromActionVerification);
		if (isDisplayed(header_assemblyProposalActionVerification)) {
			writeTestResults("Verify 'Copy From' button is working", "'Copy From' button should be working",
					"'Copy From' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Copy From' button is working", "'Copy From' button should be working",
					"'Copy From' button doesn't working", "fail");
		}
	}

	/* AV_IW_InboundShipment */
	public void inboundLOanOrderForInboundShipments() throws Exception {
		navigateToInventoryAndWarehouseModuleActionVerification();
		click(btn_inboundLoanOrder);
		click(btn_newInboundLoanOrder);
		url_new_status = getCurrentUrl();
		click(btn_vendorSearchILO);
		switchWindow();
		pressEnter(txt_vendorSearchILO);

		Thread.sleep(3000);
		doubleClick(lnk_resultVendorILO);

		click(btn_addressILO);

		sendKeys(txt_billingAddressILO, billingAddressILO);

		sendKeys(txt_deliveryAddressILO, deliveryAddressILO);

		click(btn_applyAddressILO);

		/* Product add to the grid */
		int j = 0;
		for (int i = 1; i < 2; i++) {
			String str_i = String.valueOf(i);

			String lookup_path = btn_productLookupInterDeprtmentTransferOrderForLoop.replace("row", str_i)
					.replace("column", "8").replace("tblStockAdj", "tblLoanOrder");
			click(lookup_path);

			String product = "";

			if (i == 1) {
				product = "BatchSpecific";
			} else if (i == 2) {
				product = "BatchFifo";
			} else if (i == 3) {
				product = "Lot";
			} else if (i == 4) {
				product = "SerielSpecific";
			} else if (i == 5) {
				product = "SerielBatchSpecific";
			} else if (i == 6) {
				product = "SerielBatchFifo";
			} else if (i == 7) {
				product = "SerirlFifo";
			} else {
				System.out.println("Product type cannt identified.........");
			}
			sleepCusomized(txt_productSearch);
			sendKeys(txt_productSearch, readTestCreation(product));
			pressEnter(txt_productSearch);
			Thread.sleep(3000);
			doubleClick(btn_resultProduct);

			String txt_qty = txt_qtyProductGridInboundLoanOrder.replace("row", str_i);

			sendKeys(txt_qty, qtyInterDepartmentTransferOrder);

		}

		click(btn_deleteEmotyRowStockAdjustment.replace("tblStockAdj", "tblLoanOrder").replace("8", "2"));

		click(btn_tabInboundLoanOrder);
		selectText(drop_warehouseInternelOrderEmployeeRequest, warehouseInternelOrderEmployeeRequest);

		switchWindow();

		click(btn_draft);
		Thread.sleep(3000);

		sleepCusomized(btn_relese);
		click(btn_relese);

		click(btn_goToPageLink);
		Thread.sleep(4000);
		switchWindow();
	}

	public void actionVerificationInboundShipment() throws Exception {
		click(btn_checkout);
		click(btn_draft2);
		sleepCusomized(btn_releese2);
		if (isDisplayed(btn_releese2)) {
			writeTestResults("Verify 'Draft' button is working", "'Draft' button should be working",
					"'Draft' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Draft' button is working", "'Draft' button should be working",
					"'Draft' button doesn't working", "fail");
		}

		Thread.sleep(1000);
		click(btn_editActionVerification);
		Thread.sleep(4000);
		if (isDisplayed(btn_updateActionVerification)) {
			writeTestResults("Verify 'Edit' button is working", "'Edit' button should be working",
					"'Edit' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Edit' button is working", "'Edit' button should be working",
					"'Edit' button doesn't working", "fail");
		}

		click(btn_checkout);
		click(btn_updateActionVerification);
		if (isDisplayed(btn_editActionVerification)) {
			writeTestResults("Verify 'Update' button is working", "'Update' button should be working",
					"'Update' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Update' button is working", "'Update' button should be working",
					"'Update' button doesn't working", "fail");
		}

		sleepCusomized("Sleep");
		click(brn_serielBatchCapture);
		String item_xpath;
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "1");
		click(item_xpath);
		if (isDisplayed(btn_captureItem1)) {
			writeTestResults("Verify 'Seriel Batch' button is working", "'Seriel Batch' button should be working",
					"'Seriel Batch' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Seriel Batch' button is working", "'Seriel Batch' button should be working",
					"'Seriel Batch' button doesn't working", "fail");
		}
		sendKeys(txt_batchNumberCaptureILOOutboundShipment,
				genProductId() + currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_lotNumberStockAdjustment,
				genProductId() + currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_expiryDateStockAdjustmentProductCapture);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_lotNumberStockAdjustment);
		Thread.sleep(3000);
		click(btn_updateCapture);
		click(btn_backButtonUpdateCapture);
		sleepCusomized(btn_relese);
		click(btn_relese);
		Thread.sleep(4000);
		trackCode = getText(lbl_creationsDocNo);
		String header1 = getText(header_draft);
		if (!header1.equalsIgnoreCase("(Released)")) {
			Thread.sleep(5000);
			header1 = getText(header_draft);
		}
		if (header1.equalsIgnoreCase("(Released)")) {
			writeTestResults("Verify 'Release' button is working", "'Release' button should be working",
					"'Release' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Release' button is working", "'Release' button should be working",
					"'Release' button doesn't working", "fail");
		}
		url_released_status = getCurrentUrl();
	}

	public void actionSectionVerificationInboundShipment() throws Exception {
		click(btn_print);
		Thread.sleep(2000);
		if (isDisplayed(drop_printInboundShipmentActionVerification)) {
			writeTestResults("Verify 'Print' button is working", "'Print' button should be working",
					"'Print' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Print' button is working", "'Print' button should be working",
					"'Print' button doesn't working", "fail");
		}

		openPage(url_released_status);
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_remindersAllocationMangerActionVerification);
		if (isDisplayed(header_reminderActionVerification)) {
			writeTestResults("Verify 'Reminders' button is working", "'Reminders' button should be working",
					"'Reminders' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Reminders' button is working", "'Reminders' button should be working",
					"'Reminders' button doesn't working", "fail");
		}

		openPage(url_released_status);
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_historyActionVerification);
		sleepCusomized(div_historyTableActionVerification);
		if (isDisplayed(div_historyTableActionVerification)) {
			writeTestResults("Verify 'History' button is working", "'History' button should be working",
					"'History' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'History' button is working", "'History' button should be working",
					"'History' button doesn't working", "fail");
		}

		openPage(url_released_status);
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_dockflowActionVrification);
		sleepCusomized(header_documentFlowActionVerification);
		if (isDisplayed(header_documentFlowActionVerification)) {
			writeTestResults("Verify 'Doc Flow' button is working", "'Doc Flow' button should be working",
					"'Doc Flow' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Doc Flow' button is working", "'Doc Flow' button should be working",
					"'Doc Flow' button doesn't working", "fail");
		}

		openPage(url_released_status);
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_activitiesActionVerification);
		if (isDisplayed(btn_activitiesHeaderActionVrification)) {
			writeTestResults("Verify 'Activities' button is working", "'Activities' button should be working",
					"'Activities' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Activities' button is working", "'Activities' button should be working",
					"'Activities' button doesn't working", "fail");
		}

		openPage(url_released_status);
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_reverseActionVrification);
		click(btn_reverseConfirmInboundShipmentActionVerification);
		click(btn_reverseConfirationActionVerification);
		String header123 = getText(header_draft);
		if (!header123.equalsIgnoreCase("(Reversed)")) {
			Thread.sleep(7000);
			header123 = getText(header_draft);
		}
		if (header123.equalsIgnoreCase("(Reversed)")) {
			writeTestResults("Verify 'Reverse' button is working", "'Reverse' button should be working",
					"'Reverse' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Reverse' button is working", "'Reverse' button should be working",
					"'Reverse' button doesn't working", "fail");
		}
	}

	/* AV_IW_InboundLoanOrder */
	public void navigateToNewInboundLanOrderActionVerification() throws Exception {
		navigateToInventoryAndWarehouseModuleActionVerification();
		click(btn_inboundLoanOrder);
		click(btn_newInboundLoanOrder);
		url_new_status = getCurrentUrl();

	}

	public void fillInboundLOanOrderActionVerification() throws Exception {
		click(btn_vendorSearchILO);
		switchWindow();
		pressEnter(txt_vendorSearchILO);

		Thread.sleep(3000);
		doubleClick(lnk_resultVendorILO);

		click(btn_addressILO);

		sendKeys(txt_billingAddressILO, billingAddressILO);

		sendKeys(txt_deliveryAddressILO, deliveryAddressILO);

		click(btn_applyAddressILO);

		/* Product add to the grid */
		int j = 0;
		for (int i = 1; i < 2; i++) {
			String str_i = String.valueOf(i);

			String lookup_path = btn_productLookupInterDeprtmentTransferOrderForLoop.replace("row", str_i)
					.replace("column", "8").replace("tblStockAdj", "tblLoanOrder");
			click(lookup_path);

			String product = "";

			if (i == 1) {
				product = "BatchSpecific";
			} else if (i == 2) {
				product = "BatchFifo";
			} else if (i == 3) {
				product = "Lot";
			} else if (i == 4) {
				product = "SerielSpecific";
			} else if (i == 5) {
				product = "SerielBatchSpecific";
			} else if (i == 6) {
				product = "SerielBatchFifo";
			} else if (i == 7) {
				product = "SerirlFifo";
			} else {
				System.out.println("Product type cannt identified.........");
			}
			sleepCusomized(txt_productSearch);
			sendKeys(txt_productSearch, readTestCreation(product));
			pressEnter(txt_productSearch);
			Thread.sleep(3500);
			doubleClick(btn_resultProduct);

			String txt_qty = txt_qtyProductGridInboundLoanOrder.replace("row", str_i);

			sendKeys(txt_qty, qtyInterDepartmentTransferOrder);

		}

		click(btn_deleteEmotyRowStockAdjustment.replace("tblStockAdj", "tblLoanOrder").replace("8", "2"));

		click(btn_tabInboundLoanOrder);
		selectText(drop_warehouseInternelOrderEmployeeRequest, warehouseInternelOrderEmployeeRequest);
	}

	public void actionVerificationInboundLoanOrder() throws Exception {
		click(btn_draftAndNewActionVerification);
		Thread.sleep(3000);
		String address = getAttribute(txt_biilingAdressFrontPage, "value");
		if ("".equals(address)) {
			writeTestResults("Verify 'Draft & New' button is working", "'Draft & New' button should be working",
					"'Draft & New' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Draft & New' button is working", "'Draft & New' button should be working",
					"'Draft & New' button doesn't working", "fail");
		}

		fillInboundLOanOrderActionVerification();

		click(btn_draft2);
		sleepCusomized(btn_releese2);
		if (isDisplayed(btn_releese2)) {
			writeTestResults("Verify 'Draft' button is working", "'Draft' button should be working",
					"'Draft' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Draft' button is working", "'Draft' button should be working",
					"'Draft' button doesn't working", "fail");
		}

		Thread.sleep(1000);
		click(btn_editActionVerification);
		Thread.sleep(4000);
		if (isDisplayed(btn_updateActionVerification)) {
			writeTestResults("Verify 'Edit' button is working", "'Edit' button should be working",
					"'Edit' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Edit' button is working", "'Edit' button should be working",
					"'Edit' button doesn't working", "fail");
		}

		click(btn_updateActionVerification);
		if (isDisplayed(btn_editActionVerification)) {
			writeTestResults("Verify 'Update' button is working", "'Update' button should be working",
					"'Update' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Update' button is working", "'Update' button should be working",
					"'Update' button doesn't working", "fail");
		}

		Thread.sleep(3000);
		click(btn_editActionVerification);
		sleepCusomized("Sleep");
		click(btn_updateAndNewActionVerification);
		Thread.sleep(3000);
		String address_draft_and_new = getAttribute(txt_biilingAdressFrontPage, "value");
		if ("".equals(address_draft_and_new)) {
			writeTestResults("Verify 'Update & New' button is working", "'Update & New' button should be working",
					"'Update & New' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Update & New' button is working", "'Update & New' button should be working",
					"'Update & New' button doesn't working", "fail");
		}

		Thread.sleep(2000);
		fillInboundLOanOrderActionVerification();
		click(btn_draft);
		Thread.sleep(4000);
		click(btn_relese);
		Thread.sleep(4000);
		pageRefersh();
		Thread.sleep(2000);
		trackCode = getText(lbl_creationsDocNo);
		String header1 = getText(header_draft);
		if (!header1.equalsIgnoreCase("(Released)")) {
			Thread.sleep(5000);
			header1 = getText(header_draft);
		}
		if (header1.equalsIgnoreCase("(Released)")) {
			writeTestResults("Verify 'Release' button is working", "'Release' button should be working",
					"'Release' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Release' button is working", "'Release' button should be working",
					"'Release' button doesn't working", "fail");
		}
		url_released_status = getCurrentUrl();
	}

	public void actionSectionVerificationInboundLoanOrder() throws Exception {
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_remindersAllocationMangerActionVerification);
		if (isDisplayed(header_reminderActionVerification)) {
			writeTestResults("Verify 'Reminders' button is working", "'Reminders' button should be working",
					"'Reminders' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Reminders' button is working", "'Reminders' button should be working",
					"'Reminders' button doesn't working", "fail");
		}

		openPage(url_released_status);
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_historyActionVerification);
		sleepCusomized(div_historyTableActionVerification);
		if (isDisplayed(div_historyTableActionVerification)) {
			writeTestResults("Verify 'History' button is working", "'History' button should be working",
					"'History' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'History' button is working", "'History' button should be working",
					"'History' button doesn't working", "fail");
		}

		openPage(url_released_status);
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_dockflowActionVrification);
		sleepCusomized(header_documentFlowActionVerification);
		if (isDisplayed(header_documentFlowActionVerification)) {
			writeTestResults("Verify 'Doc Flow' button is working", "'Doc Flow' button should be working",
					"'Doc Flow' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Doc Flow' button is working", "'Doc Flow' button should be working",
					"'Doc Flow' button doesn't working", "fail");
		}

		openPage(url_released_status);
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_activitiesActionVerification);
		if (isDisplayed(btn_activitiesHeaderActionVrification)) {
			writeTestResults("Verify 'Activities' button is working", "'Activities' button should be working",
					"'Activities' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Activities' button is working", "'Activities' button should be working",
					"'Activities' button doesn't working", "fail");
		}

		openPage(url_released_status);
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_reverseActionVrification);
		Thread.sleep(2000);
		JavascriptExecutor j7 = (JavascriptExecutor) driver;
		j7.executeScript("$(\"#txtrdcmnReverseDate\").datepicker(\"setDate\", new Date())");
		click(btn_reverseConfirmInboundShipmentActionVerification);
		click(btn_reverseConfirationActionVerification);
		String header123 = getText(header_draft);
		if (!header123.equalsIgnoreCase("(Reversed)")) {
			Thread.sleep(7000);
			header123 = getText(header_draft);
		}
		if (header123.equalsIgnoreCase("(Reversed)")) {
			writeTestResults("Verify 'Reverse' button is working", "'Reverse' button should be working",
					"'Reverse' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Reverse' button is working", "'Reverse' button should be working",
					"'Reverse' button doesn't working", "fail");
		}

		openPage(url_new_status);
		fillInboundLOanOrderActionVerification();
		click(btn_draft);
		Thread.sleep(3500);
		click(btn_action);
		Thread.sleep(1000);
		click(btn_deleteActionVrification);
		Thread.sleep(2000);
		click(btn_deleteConfirationActionVerification);
		Thread.sleep(3000);
		String header1 = getText(header_draft);
		if (!header1.equalsIgnoreCase("(Deleted)")) {
			Thread.sleep(7000);
			header1 = getText(header_draft);
		}
		if (header1.equalsIgnoreCase("(Deleted)")) {
			writeTestResults("Verify 'Delete' button is working", "'Delete' button should be working",
					"'Delete' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Delete' button is working", "'Delete' button should be working",
					"'Delete' button doesn't working", "fail");
		}

		openPage(url_new_status);
		fillInboundLOanOrderActionVerification();
		click(btn_draft);
		sleepCusomized(btn_relese);
		click(btn_relese);
		Thread.sleep(3000);
		click(btn_goToPageLink);
		Thread.sleep(3000);
		switchWindow();
		click(btn_checkoutILOOutboundShipment);
		Thread.sleep(1000);
		click(btn_draft);
		Thread.sleep(3000);
		sleepCusomized(brn_serielBatchCapture);

		click(brn_serielBatchCapture);
		sleepCusomized(btn_serielBatchCaptureProduct1ILOOutboundShipment);
		String item_xpath;
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "1");
		click(item_xpath);
		Thread.sleep(2000);
		sendKeys(txt_batchNumberCaptureILOOutboundShipment,
				genProductId() + currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_lotNumberStockAdjustment,
				genProductId() + currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_expiryDateStockAdjustmentProductCapture);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_lotNumberStockAdjustment);
		Thread.sleep(3000);
		click(btn_updateCapture);

		Thread.sleep(3000);
		click(btn_productCaptureBackButton);
		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(3000);
		switchWindow();
		url_released_status = getCurrentUrl();
		pageRefersh();
		Thread.sleep(3000);
		click(btn_action);
		click(btn_convertToOutboundLoanOrderActionVerification);
		String header4 = getText(header_pageLabel);
		if (!header4.equalsIgnoreCase("Outbound Loan Order")) {
			Thread.sleep(7000);
			header4 = getText(header_pageLabel);
		}

		if (header4.equalsIgnoreCase("Outbound Loan Order")) {
			writeTestResults("Verify 'Convert to Outbound Loan Order' button is working",
					"'Convert to Outbound Loan Order' button should be working",
					"'Convert to Outbound Loan Order' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Convert to Outbound Loan Order' button is working",
					"'Convert to Outbound Loan Order' button should be working",
					"'Convert to Outbound Loan Order' button doesn't working", "fail");
		}

		openPage(url_released_status);
		implisitWait(7);
		Thread.sleep(1000);
		click(btn_action);
		click(btn_convertToOutboundShimentActionVerification);
		String header1234 = getText(header_pageLabel);
		if (!header1234.equalsIgnoreCase("Outbound Shipment")) {
			Thread.sleep(7000);
			header1234 = getText(header_pageLabel);
		}

		if (header1234.equalsIgnoreCase("Outbound Shipment")) {
			writeTestResults("Verify 'Convert to Outbound Shipment' button is working",
					"'Convert to Outbound Shipment' button should be working",
					"'Convert to Outbound Shipment' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Convert to Outbound Shipment' button is working",
					"'Convert to Outbound Shipment' button should be working",
					"'Convert to Outbound Shipment' button doesn't working", "fail");
		}

		openPage(url_released_status);
		implisitWait(7);
		Thread.sleep(1000);
		click(btn_action);
		click(btn_convertToPurchaseInvoiceActionVerification);
		String header5 = getText(header_pageLabel);
		if (!header5.equalsIgnoreCase("Purchase Invoice")) {
			Thread.sleep(7000);
			header5 = getText(header_pageLabel);
		}

		if (header5.equalsIgnoreCase("Purchase Invoice")) {
			writeTestResults("Verify 'Convert to Purchase Invoice' button is working",
					"'Convert to Purchase Invoice' button should be working",
					"'Convert to Purchase Invoice' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Convert to Purchase Invoice' button is working",
					"'Convert to Purchase Invoice' button should be working",
					"'Convert to Purchase Invoice' button doesn't working", "fail");
		}
	}

	public void actionSectionOtherVerificationInboundLoanOrder() throws Exception {
		openPage(url_released_status);
		implisitWait(7);
		Thread.sleep(2000);
		click(btn_duplicateActionVerification);
		if (isDisplayed(btn_draft2)) {
			writeTestResults("Verify 'Duplicate' button is working", "'Duplicate' button should be working",
					"'Duplicate' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Duplicate' button is working", "'Duplicate' button should be working",
					"'Duplicate' button doesn't working", "fail");
		}

		openPage(url_new_status);
		implisitWait(7);
		isDisplayed(btn_copyFromActionVerification);
		click(btn_copyFromActionVerification);
		if (isDisplayed(header_assemblyProposalActionVerification.replace("Assembly Order", "Inbound loan Order"))) {
			writeTestResults("Verify 'Copy From' button is working", "'Copy From' button should be working",
					"'Copy From' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Copy From' button is working", "'Copy From' button should be working",
					"'Copy From' button doesn't working", "fail");
		}
	}

	/* AV_TC_InternelRecipt */
	public void navigateToInternelRecipt() throws Exception {
		actionVerificationLogin();

	}

	public void fillAndNavigateToInternelRecipt() throws Exception {
		waitImplicit(navigation_pane, 10);
		click(navigation_pane);
		sleepCusomized(inventory_module);
		click(inventory_module);
		click(btn_internelOrder);
		click(btn_newInternelOrder);
		click(btn_employeeRequestJourneyInternelOrder);
		sendKeys(txt_tiltleInternelOrder, titleInternelOrder);

		// Order date get default by current date

		click(btn_searchRequester);

		pressEnter(txt_RequesterSearch);

		doubleClick(lnk_searchResultRequester);

		employee = getAttribute(lnk_searchResultRequester, "title");

		/* Product add to the grid */
		int j = 0;
		for (int i = 1; i < 2; i++) {
			String str_i = String.valueOf(i);

			String lookup_path = btn_productLookupInterDeprtmentTransferOrderForLoop.replace("row", str_i)
					.replace("column", "10").replace("tblStockAdj", "tblInternalRequest");
			click(lookup_path);
			sleepCusomized(txt_productSearch);

			String product = "";

			if (i == 1) {
				product = "Lot";
			} else {
				System.out.println("Product type cannt identified.........");
			}
			sleepCusomized(txt_productSearch);
			sendKeys(txt_productSearch, readTestCreation(product));
			pressEnter(txt_productSearch);
			Thread.sleep(3000);
			sleepCusomized(btn_resultProduct);
			sleepCusomized(btn_resultProduct);
			doubleClick(btn_resultProduct);

//			Selected products should be added to the Grid and OnHand Quantity [From][To], Default UOM should be loaded for selected product

			String txt_qty = txt_quantityInGrid.replace("row", str_i).replace("tblName", "tblInternalRequest");

			customizeLoadingDelay(txt_qty, 7);

			sendKeys(txt_qty, qtyInterDepartmentTransferOrder);

			sleepCusomized(btn_addNewRow);
			click(btn_addNewRow);
		}

		click(btn_deleteEmotyRowStockAdjustment.replace("8", "2").replace("tblStockAdj", "tblInternalRequest"));

		click(btn_detailTab);
		selectText(drop_warehouseInternelOrderEmployeeRequest, warehouseInternelOrderEmployeeRequest);
		writeIWData("Employee", employee, 0);

		click(btn_draftInternelOrder);
		Thread.sleep(3000);
		sleepCusomized(btn_relese);
		click(btn_relese);

		Thread.sleep(4000);
		click(btn_goToPageLink);
		implisitWait(7);
	}

	public void fillAndNavigateToInternelRecipt2() throws Exception {
		switchWindow();
		sleepCusomized(btn_draftInternelDispatchOrder);
		click(btn_draftInternelDispatchOrder);
		sleepCusomized(btn_relese);
		click(btn_relese);
		customizeLoadingDelay(lbl_relesedConfirmation, 45);
		trackCode = getText(lbl_creationsDocNo);
		String internel_dispatch_order = getText(lbl_docNo);
		writeIWData("Internel Dispatch Order", internel_dispatch_order, 9);
		Thread.sleep(4000);

		waitImplicit(navigation_pane, 10);
		click(navigation_pane);
		sleepCusomized(inventory_module);

		click(inventory_module);

		click(btn_internelReturnOrder);

		click(btn_newInternelReturnOrder);

		click(btn_employeeReturnJourneyIRO);

		click(btn_searchIconEmployeeIRO);
		sendKeys(txt_employeeSearchRequesterWindowIRO, readIWData(0));
		pressEnter(txt_employeeSearchRequesterWindowIRO);
		doubleClick(lnk_resultRequesterIRO);

		selectText(dropdown_toWarehouseIRO, warehouseGstWarehouseNegombo);

		click(btn_docIconSelectInternelOrderIRO);

		sendKeys(txt_internelOrderSearchIRO, readIWData(9));
		click(btn_serchDocInternelReturnOrder);

		Thread.sleep(3000);

		click(lnk_resultInternelOrderIRO1);
		click(lnk_resultInternelOrderIRO2);

		click(btn_applyInternelOrderIRO);

		click(btn_draft);
		sleepCusomized(btn_relese);
		Thread.sleep(1000);

		click(btn_relese);

		click(btn_goToPageLink);
		switchWindow();
		sleepCusomized(header_pageLabel);

	}

	public void internelReciptActionVerification() throws Exception {
		click(btn_draft2);
		sleepCusomized(btn_releese2);
		if (isDisplayed(btn_releese2)) {
			writeTestResults("Verify 'Draft' button is working", "'Draft' button should be working",
					"'Draft' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Draft' button is working", "'Draft' button should be working",
					"'Draft' button doesn't working", "fail");
		}
		Thread.sleep(1000);
		click(btn_editActionVerification);
		Thread.sleep(4000);
		if (isDisplayed(btn_updateActionVerification)) {
			writeTestResults("Verify 'Edit' button is working", "'Edit' button should be working",
					"'Edit' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Edit' button is working", "'Edit' button should be working",
					"'Edit' button doesn't working", "fail");
		}

		click(btn_updateActionVerification);
		if (isDisplayed(btn_editActionVerification)) {
			writeTestResults("Verify 'Update' button is working", "'Update' button should be working",
					"'Update' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Update' button is working", "'Update' button should be working",
					"'Update' button doesn't working", "fail");
		}
		Thread.sleep(3000);

		sleepCusomized(brn_serielBatchCapture);
		click(brn_serielBatchCapture);
		sleepCusomized(btn_captureItem1IRO);
		click(btn_captureItem1IRO);
		click(chk_selectAllCaptureItem);
		click(btn_updateCapture);
		Thread.sleep(2000);
		click(btn_productCaptureBackButton);
		Thread.sleep(2000);
		click(btn_relese);
		Thread.sleep(4000);
		trackCode = getText(lbl_creationsDocNo);
		String header1 = getText(header_draft);
		if (!header1.equalsIgnoreCase("(Released)")) {
			Thread.sleep(5000);
			header1 = getText(header_draft);
		}
		if (header1.equalsIgnoreCase("(Released)")) {
			writeTestResults("Verify 'Release' button is working", "'Release' button should be working",
					"'Release' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Release' button is working", "'Release' button should be working",
					"'Release' button doesn't working", "fail");
		}
		url_released_status = getCurrentUrl();
	}

	public void actionSectionVerificationInternelRecipt() throws Exception {
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_remindersAllocationMangerActionVerification);
		if (isDisplayed(header_reminderActionVerification)) {
			writeTestResults("Verify 'Reminders' button is working", "'Reminders' button should be working",
					"'Reminders' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Reminders' button is working", "'Reminders' button should be working",
					"'Reminders' button doesn't working", "fail");
		}

		openPage(url_released_status);
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_historyActionVerification);
		sleepCusomized(div_historyTableActionVerification);
		if (isDisplayed(div_historyTableActionVerification)) {
			writeTestResults("Verify 'History' button is working", "'History' button should be working",
					"'History' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'History' button is working", "'History' button should be working",
					"'History' button doesn't working", "fail");
		}

		openPage(url_released_status);
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_dockflowActionVrification);
		sleepCusomized(header_documentFlowActionVerification);
		if (isDisplayed(header_documentFlowActionVerification)) {
			writeTestResults("Verify 'Doc Flow' button is working", "'Doc Flow' button should be working",
					"'Doc Flow' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Doc Flow' button is working", "'Doc Flow' button should be working",
					"'Doc Flow' button doesn't working", "fail");
		}

		openPage(url_released_status);
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_activitiesActionVerification);
		if (isDisplayed(btn_activitiesHeaderActionVrification)) {
			writeTestResults("Verify 'Activities' button is working", "'Activities' button should be working",
					"'Activities' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Activities' button is working", "'Activities' button should be working",
					"'Activities' button doesn't working", "fail");
		}

		openPage(url_released_status);
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_journelActionVerification);
		if (isDisplayed(btn_journelHeaderActionVrification)) {
			writeTestResults("Verify 'Journel' button is working", "'Journel' button should be working",
					"'Journel' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Journel' button is working", "'Journel' button should be working",
					"'Journel' button doesn't working", "fail");
		}

		openPage(url_released_status);
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_reverseActionVrification);
		Thread.sleep(2000);
		JavascriptExecutor j7 = (JavascriptExecutor) driver;
		j7.executeScript("$(\"#txtrdcmnReverseDate\").datepicker(\"setDate\", new Date())");
		click(btn_reverseConfirmInboundShipmentActionVerification);
		click(btn_reverseConfirationActionVerification);
		String header123 = getText(header_draft);
		if (!header123.equalsIgnoreCase("(Reversed)")) {
			Thread.sleep(7000);
			header123 = getText(header_draft);
		}
		if (header123.equalsIgnoreCase("(Reversed)")) {
			writeTestResults("Verify 'Reverse' button is working", "'Reverse' button should be working",
					"'Reverse' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Reverse' button is working", "'Reverse' button should be working",
					"'Reverse' button doesn't working", "fail");
		}

		fillAndNavigateToInternelRecipt();
		fillAndNavigateToInternelRecipt2();
		click(btn_draft);
		Thread.sleep(3500);
		click(btn_action);
		Thread.sleep(1000);
		click(btn_deleteActionVrification);
		Thread.sleep(2000);
		click(btn_deleteConfirationActionVerification);
		Thread.sleep(3000);
		String header1 = getText(header_draft);
		if (!header1.equalsIgnoreCase("(Deleted)")) {
			Thread.sleep(7000);
			header1 = getText(header_draft);
		}
		if (header1.equalsIgnoreCase("(Deleted)")) {
			writeTestResults("Verify 'Delete' button is working", "'Delete' button should be working",
					"'Delete' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Delete' button is working", "'Delete' button should be working",
					"'Delete' button doesn't working", "fail");
		}

	}

	public void otherActionVerificationInternelRecipt() throws Exception {
		fillAndNavigateToInternelRecipt();
		fillAndNavigateToInternelRecipt2();
		click(btn_draft2);
		implisitWait(7);
		sleepCusomized("Sleep");
		click(brn_serielBatchCapture);
		String item_xpath;
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "1");
		click(item_xpath);
		if (isDisplayed(btn_captureItem1)) {
			writeTestResults("Verify 'Seriel Batch' button is working", "'Seriel Batch' button should be working",
					"'Seriel Batch' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Seriel Batch' button is working", "'Seriel Batch' button should be working",
					"'Seriel Batch' button doesn't working", "fail");
		}
		Thread.sleep(3000);
		click(btn_productCaptureBackButton);
		Thread.sleep(3000);
		sleepCusomized(brn_serielBatchCapture);
		click(brn_serielBatchCapture);
		sleepCusomized(btn_captureItem1IRO);
		click(btn_captureItem1IRO);
		click(chk_selectAllCaptureItem);
		click(btn_updateCapture);
		Thread.sleep(2000);
		click(btn_productCaptureBackButton);
		Thread.sleep(2000);
		click(btn_releese2);
		implisitWait(7);
		click(btn_print);
		Thread.sleep(2000);
		if (isDisplayed(drop_printInboundShipmentActionVerification)) {
			writeTestResults("Verify 'Print' button is working", "'Print' button should be working",
					"'Print' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Print' button is working", "'Print' button should be working",
					"'Print' button doesn't working", "fail");
		}

	}

	/* AV_IW_InternelDispatchOrder */
	public void loginInternelDispatchOrder() throws Exception {
		actionVerificationLogin();
	}

	public void navigateToInternelDispatchOrder() throws Exception {
		waitImplicit(navigation_pane, 10);
		click(navigation_pane);
		sleepCusomized(inventory_module);
		click(inventory_module);
		click(btn_internelOrder);
		click(btn_newInternelOrder);
		click(btn_employeeRequestJourneyInternelOrder);
		sendKeys(txt_tiltleInternelOrder, titleInternelOrder);

		// Order date get default by current date

		click(btn_searchRequester);

		pressEnter(txt_RequesterSearch);

		Thread.sleep(3000);
		doubleClick(lnk_searchResultRequester);

		employee = getAttribute(lnk_searchResultRequester, "title");

		/* Product add to the grid */
		int j = 0;
		for (int i = 1; i < 2; i++) {
			String str_i = String.valueOf(i);

			String lookup_path = btn_productLookupInterDeprtmentTransferOrderForLoop.replace("row", str_i)
					.replace("column", "10").replace("tblStockAdj", "tblInternalRequest");
			click(lookup_path);
			sleepCusomized(txt_productSearch);

			String product = "";

			if (i == 1) {
				product = "Lot";
			} else {
				System.out.println("Product type cannt identified.........");
			}
			sleepCusomized(txt_productSearch);
			String product_dispatch_order = readTestCreation(product);
			sendKeys(txt_productSearch, product_dispatch_order);
			pressEnter(txt_productSearch);
			Thread.sleep(4000);
			doubleClick(btn_resultProduct);

//			Selected products should be added to the Grid and OnHand Quantity [From][To], Default UOM should be loaded for selected product

			String txt_qty = txt_quantityInGrid.replace("row", str_i).replace("tblName", "tblInternalRequest");

			customizeLoadingDelay(txt_qty, 7);

			sendKeys(txt_qty, qtyInterDepartmentTransferOrder);

			sleepCusomized(btn_addNewRow);
			click(btn_addNewRow);
		}

		click(btn_deleteEmotyRowStockAdjustment.replace("8", "2").replace("tblStockAdj", "tblInternalRequest"));

		click(btn_detailTab);
		selectText(drop_warehouseInternelOrderEmployeeRequest, warehouseInternelOrderEmployeeRequest);
		writeIWData("Employee", employee, 0);

		click(btn_draftInternelOrder);
		Thread.sleep(3000);
		sleepCusomized(btn_relese);
		click(btn_relese);

		Thread.sleep(4000);
		click(btn_goToPageLink);
		switchWindow();
		implisitWait(7);
	}

	public void actionVerificationInternelDispatchOrder() throws Exception {
		click(btn_draft2);
		sleepCusomized(btn_releese2);
		if (isDisplayed(btn_releese2)) {
			writeTestResults("Verify 'Draft' button is working", "'Draft' button should be working",
					"'Draft' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Draft' button is working", "'Draft' button should be working",
					"'Draft' button doesn't working", "fail");
		}

		Thread.sleep(1000);
		click(btn_editActionVerification);
		Thread.sleep(4000);
		if (isDisplayed(btn_updateActionVerification)) {
			writeTestResults("Verify 'Edit' button is working", "'Edit' button should be working",
					"'Edit' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Edit' button is working", "'Edit' button should be working",
					"'Edit' button doesn't working", "fail");
		}

		click(btn_updateActionVerification);
		implisitWait(7);
		if (isDisplayed(btn_editActionVerification)) {
			writeTestResults("Verify 'Update' button is working", "'Update' button should be working",
					"'Update' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Update' button is working", "'Update' button should be working",
					"'Update' button doesn't working", "fail");
		}

		Thread.sleep(2000);
		click(btn_relese);
		Thread.sleep(4000);
		trackCode = getText(lbl_creationsDocNo);
		String header1 = getText(header_draft);
		if (!header1.equalsIgnoreCase("(Released)")) {
			Thread.sleep(5000);
			header1 = getText(header_draft);
		}
		if (header1.equalsIgnoreCase("(Released)")) {
			writeTestResults("Verify 'Release' button is working", "'Release' button should be working",
					"'Release' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Release' button is working", "'Release' button should be working",
					"'Release' button doesn't working", "fail");
		}
		url_released_status = getCurrentUrl();
	}

	public void actionSectionVerificationInternelDispatchOrder() throws Exception {
		Thread.sleep(3000);
		click(btn_availabilityCheckWidgetActionVerification);
		Thread.sleep(2000);
		click(btn_productAvailabilityRackWiseActionVerification);
		Thread.sleep(2000);
		if (isDisplayed(btn_rackWiseProductAvailabilityHeaderActionVrification)) {
			writeTestResults("Verify 'Product Availablitiy Rack Wise' button is working",
					"'Product Availablitiy Rack Wise' button should be working",
					"'Product Availablitiy Rack Wise' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Product Availablitiy Rack Wise' button is working",
					"'Product Availablitiy Rack Wise' button should be working",
					"'Product Availablitiy Rack Wise' button doesn't working", "fail");
		}

		openPage(url_released_status);
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_remindersAllocationMangerActionVerification);
		if (isDisplayed(header_reminderActionVerification)) {
			writeTestResults("Verify 'Reminders' button is working", "'Reminders' button should be working",
					"'Reminders' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Reminders' button is working", "'Reminders' button should be working",
					"'Reminders' button doesn't working", "fail");
		}

		openPage(url_released_status);
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_historyActionVerification);
		sleepCusomized(div_historyTableActionVerification);
		if (isDisplayed(div_historyTableActionVerification)) {
			writeTestResults("Verify 'History' button is working", "'History' button should be working",
					"'History' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'History' button is working", "'History' button should be working",
					"'History' button doesn't working", "fail");
		}

		openPage(url_released_status);
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_dockflowActionVrification);
		sleepCusomized(header_documentFlowActionVerification);
		if (isDisplayed(header_documentFlowActionVerification)) {
			writeTestResults("Verify 'Doc Flow' button is working", "'Doc Flow' button should be working",
					"'Doc Flow' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Doc Flow' button is working", "'Doc Flow' button should be working",
					"'Doc Flow' button doesn't working", "fail");
		}

		openPage(url_released_status);
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_activitiesActionVerification);
		if (isDisplayed(btn_activitiesHeaderActionVrification)) {
			writeTestResults("Verify 'Activities' button is working", "'Activities' button should be working",
					"'Activities' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Activities' button is working", "'Activities' button should be working",
					"'Activities' button doesn't working", "fail");
		}

	}

	/* AV_IW_InternelReturnOrder */
	public void navigateToInterneReturnOrder1() throws Exception {
		actionVerificationLogin();

	}

	public void navigateToInterneReturnOrder2() throws Exception {
		waitImplicit(navigation_pane, 10);
		click(navigation_pane);
		sleepCusomized(inventory_module);
		click(inventory_module);
		click(btn_internelOrder);
		click(btn_newInternelOrder);
		click(btn_employeeRequestJourneyInternelOrder);

	}

	public void navigateToInterneReturnOrder3() throws Exception {
		url_new_status = getCurrentUrl();

		sendKeys(txt_tiltleInternelOrder, titleInternelOrder);

		// Order date get default by current date

		click(btn_searchRequester);

		pressEnter(txt_RequesterSearch);

		doubleClick(lnk_searchResultRequester);

		employee = getAttribute(lnk_searchResultRequester, "title");

		/* Product add to the grid */
		int j = 0;
		for (int i = 1; i < 2; i++) {
			String str_i = String.valueOf(i);

			String lookup_path = btn_productLookupInterDeprtmentTransferOrderForLoop.replace("row", str_i)
					.replace("column", "10").replace("tblStockAdj", "tblInternalRequest");
			click(lookup_path);
			sleepCusomized(txt_productSearch);

			String product = "";

			if (i == 1) {
				product = "Lot";
			} else {
				System.out.println("Product type cannt identified.........");
			}
			sleepCusomized(txt_productSearch);
			sendKeys(txt_productSearch, readTestCreation(product));
			pressEnter(txt_productSearch);
			Thread.sleep(3000);
			sleepCusomized(btn_resultProduct);
			sleepCusomized(btn_resultProduct);
			doubleClick(btn_resultProduct);

//			Selected products should be added to the Grid and OnHand Quantity [From][To], Default UOM should be loaded for selected product

			String txt_qty = txt_quantityInGrid.replace("row", str_i).replace("tblName", "tblInternalRequest");

			customizeLoadingDelay(txt_qty, 7);

			sendKeys(txt_qty, qtyInterDepartmentTransferOrder);

			sleepCusomized(btn_addNewRow);
			click(btn_addNewRow);
		}

		click(btn_deleteEmotyRowStockAdjustment.replace("8", "2").replace("tblStockAdj", "tblInternalRequest"));

		click(btn_detailTab);
		selectText(drop_warehouseInternelOrderEmployeeRequest, warehouseInternelOrderEmployeeRequest);
		writeIWData("Employee", employee, 0);

		click(btn_draftInternelOrder);
		Thread.sleep(3000);
		sleepCusomized(btn_relese);
		click(btn_relese);

		Thread.sleep(4000);
		click(btn_goToPageLink);
		implisitWait(7);
	}

	public void navigateToInterneReturnOrder4() throws Exception {
		switchWindow();
		sleepCusomized(btn_draftInternelDispatchOrder);
		click(btn_draftInternelDispatchOrder);
		sleepCusomized(btn_relese);
		click(btn_relese);
		customizeLoadingDelay(lbl_relesedConfirmation, 45);
		trackCode = getText(lbl_creationsDocNo);
		String internel_dispatch_order = getText(lbl_docNo);
		writeIWData("Internel Dispatch Order", internel_dispatch_order, 9);
		Thread.sleep(4000);

		waitImplicit(navigation_pane, 10);
		click(navigation_pane);
		sleepCusomized(inventory_module);

		click(inventory_module);

		click(btn_internelReturnOrder);

		click(btn_newInternelReturnOrder);

		click(btn_employeeReturnJourneyIRO);

		click(btn_searchIconEmployeeIRO);
		sendKeys(txt_employeeSearchRequesterWindowIRO, readIWData(0));
		pressEnter(txt_employeeSearchRequesterWindowIRO);
		doubleClick(lnk_resultRequesterIRO);

		selectText(dropdown_toWarehouseIRO, warehouseGstWarehouseNegombo);

		click(btn_docIconSelectInternelOrderIRO);

		sendKeys(txt_internelOrderSearchIRO, readIWData(9));
		click(btn_serchDocInternelReturnOrder);

		Thread.sleep(3000);

		click(lnk_resultInternelOrderIRO1);
		click(lnk_resultInternelOrderIRO2);

		click(btn_applyInternelOrderIRO);
	}

	public void actionVerificationInternelReturnOrder() throws Exception {
		click(btn_draftAndNewActionVerification);
		implisitWait(7);

		Select comboBox = new Select(driver.findElement(By.xpath(dropdown_toWarehouseIRO)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if ("--None--".equals(selectedComboValue)) {
			writeTestResults("Verify 'Draft & New' button is working", "'Draft & New' button should be working",
					"'Draft & New' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Draft & New' button is working", "'Draft & New' button should be working",
					"'Draft & New' button doesn't working", "fail");
		}

		openPage(url_new_status);
		implisitWait(8);
		Thread.sleep(1000);
		navigateToInterneReturnOrder3();
		navigateToInterneReturnOrder4();

		click(btn_draft2);
		sleepCusomized(btn_releese2);
		if (isDisplayed(btn_releese2)) {
			writeTestResults("Verify 'Draft' button is working", "'Draft' button should be working",
					"'Draft' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Draft' button is working", "'Draft' button should be working",
					"'Draft' button doesn't working", "fail");
		}

		Thread.sleep(1000);
		click(btn_editActionVerification);
		Thread.sleep(4000);
		if (isDisplayed(btn_updateActionVerification)) {
			writeTestResults("Verify 'Edit' button is working", "'Edit' button should be working",
					"'Edit' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Edit' button is working", "'Edit' button should be working",
					"'Edit' button doesn't working", "fail");
		}

		click(btn_updateActionVerification);
		implisitWait(7);
		if (isDisplayed(btn_editActionVerification)) {
			writeTestResults("Verify 'Update' button is working", "'Update' button should be working",
					"'Update' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Update' button is working", "'Update' button should be working",
					"'Update' button doesn't working", "fail");
		}

		Thread.sleep(3000);
		click(btn_editActionVerification);
		sleepCusomized("Sleep");
		click(btn_updateAndNewActionVerification);
		implisitWait(7);

		Select comboBox1 = new Select(driver.findElement(By.xpath(dropdown_toWarehouseIRO)));
		String selectedComboValue1 = comboBox1.getFirstSelectedOption().getText();
		if ("--None--".equals(selectedComboValue1)) {
			writeTestResults("Verify 'Update & New' button is working", "'Update & New' button should be working",
					"'Update & New' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Update & New' button is working", "'Update & New' button should be working",
					"'Update & New' button doesn't working", "fail");
		}

		openPage(url_new_status);
		implisitWait(8);
		navigateToInterneReturnOrder3();
		navigateToInterneReturnOrder4();
		click(btn_draft);
		implisitWait(7);
		Thread.sleep(1000);
		click(btn_relese);
		implisitWait(7);
		pageRefersh();
		implisitWait(7);
		Thread.sleep(1000);
		trackCode = getText(lbl_creationsDocNo);
		String header1 = getText(header_draft);
		if (!header1.equalsIgnoreCase("(Released)")) {
			Thread.sleep(5000);
			header1 = getText(header_draft);
		}
		if (header1.equalsIgnoreCase("(Released)")) {
			writeTestResults("Verify 'Release' button is working", "'Release' button should be working",
					"'Release' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Release' button is working", "'Release' button should be working",
					"'Release' button doesn't working", "fail");
		}
		url_released_status = getCurrentUrl();
	}

	public void actionSectionVerificationInternelReturnOrder() throws Exception {
		click(btn_action);
		click(btn_remindersAllocationMangerActionVerification);
		if (isDisplayed(header_reminderActionVerification)) {
			writeTestResults("Verify 'Reminders' button is working", "'Reminders' button should be working",
					"'Reminders' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'Reminders' button is working", "'Reminders' button should be working",
					"'Reminders' button doesn't working", "fail");
		}

		openPage(url_released_status);
		sleepCusomized(btn_action);
		click(btn_action);
		click(btn_historyActionVerification);
		sleepCusomized(div_historyTableActionVerification);
		if (isDisplayed(div_historyTableActionVerification)) {
			writeTestResults("Verify 'History' button is working", "'History' button should be working",
					"'History' button successfully working", "pass");
		} else {
			writeTestResults("Verify 'History' button is working", "'History' button should be working",
					"'History' button doesn't working", "fail");
		}
	}

	/************************** Regression 01 ******************************/
	/* IN_IO_001 */
	public void navigateToInternelOrderReg01() throws Exception {
		actionVerificationLogin();
		customizeLoadingDelay(navigation_pane, 30);
		click(navigation_pane);
		customizeLoadingDelay(inventory_module, 30);
		click(inventory_module);
		customizeLoadingDelay(btn_internelOrder, 30);
		click(btn_internelOrder);
		customizeLoadingDelay(header_page, 30);
		Thread.sleep(3000);
		String page_name = getText(header_page);
		if (page_name.equals("Internal Order")) {
			writeTestResults("Verify user sucessfully navigate to Internel Order By page",
					"User should be navigate to Internel Order By Page",
					"User sucessfully navigate to Internel Order By Page", "pass");
		} else {
			writeTestResults("Verify user sucessfully navigate to Internel Order By page",
					"User should be navigate to Internel Order By Page",
					"User does'nt navigate to Internel Order By Page", "fail");
		}

		current_status_url = getCurrentUrl();
		writeRegression01("IN_IO_001", current_status_url, 0);

	}

	/* IN_IO_002 */
	public void navigateToNewInternelOrderReg01() throws Exception {
		actionVerificationLogin();
		openPage(readIWRegression01("IN_IO_001"));
		click(btn_newInternelOrder);
		implisitWait(10);
		Thread.sleep(2000);
		if (isDisplayed(header_journeyList)) {
			writeTestResults("Verify Journey list is poped up", "Journey list should be poped up",
					"Journey list successfully poped up", "pass");
		} else {
			writeTestResults("Verify Journey list is poped up", "Journey list should be poped up",
					"Journey list doesn't poped up", "fail");
		}

		click(btn_employeeRequestJourneyInternelOrder);

		customizeLoadingDelay(header_newsStatus, 30);
		Thread.sleep(3000);
		String doc_status = getText(header_newsStatus);
		if (doc_status.equals("New")) {
			writeTestResults("Verify New Internal Order form is open", "New Internal Order form should be open",
					"New Internal Order form successfully open", "pass");
		} else {
			writeTestResults("Verify New Internal Order form is open", "New Internal Order form should be open",
					"New Internal Order form doesn't open", "fail");
		}
		current_status_url = getCurrentUrl();
		writeRegression01("IN_IO_002", current_status_url, 1);
	}

	/* common */
	public void setUpForDependent(String modulename, String submodulename, String testcasename, String testid)
			throws Exception {

		trackCode = "";
		moduleName = modulename;

		subModuleName = submodulename;

		testCaseName = testcasename;

		wResult.createFile("Automation-Results");

		testId = testid;

		resultSheetName(testid);

	}

	/* common */
	public void writeRegression01(String name, String variable, int data_row)
			throws InvalidFormatException, IOException {
		genarateInventoryAndWarehouseInformation obj = new genarateInventoryAndWarehouseInformation();
		String path = obj.getProjectPath("*src*test*java*inventoryAndWarehouseModule*IW_REGRESSION01.xlsx");
		fis = new FileInputStream(path);
		wb = WorkbookFactory.create(fis);
		sh = wb.getSheet("Sheet1");
		row = sh.createRow(data_row);
		cell = row.createCell(1);
		cell1 = row.createCell(0);
		cell.setCellValue(variable);
		cell1.setCellValue(name);
		/* System.out.println(cell.getStringCellValue()); */
		fos = new FileOutputStream(path);
		wb.write(fos);
		fos.flush();
		fos.close();
	}

	/* common */
	public String readIWRegression01(String name) throws IOException, InvalidFormatException {
		genarateInventoryAndWarehouseInformation obj = new genarateInventoryAndWarehouseInformation();
		String path = obj.getProjectPath("*src*test*java*inventoryAndWarehouseModule*IW_REGRESSION01.xlsx");
		fis = new FileInputStream(path);
		wb = WorkbookFactory.create(fis);
		sh = wb.getSheet("Sheet1");

		Map<String, String> iw_creation_map = new HashMap<String, String>();
		List<Map<String, String>> iw_creation = new ArrayList<Map<String, String>>();

		int totRows = sh.getLastRowNum();
		for (i = 0; i <= totRows; i++) {
			row = sh.getRow(i);
			cell = row.getCell(0);
			cell2 = row.getCell(1);

			String test_creation_row1 = cell.getStringCellValue();
			String test_creation_row2 = cell2.getStringCellValue();

			iw_creation_map.put(test_creation_row1, test_creation_row2);
			iw_creation.add(i, iw_creation_map);

		}

		String iw_info = iw_creation.get(0).get(name);

		return iw_info;
	}

	/* IN_IO_003 */
	public void selectEmployeeInternelOrderRegression01() throws Exception {
		actionVerificationLogin();
		openPage(readIWRegression01("IN_IO_002"));

		click(btn_searchRequester);
		sendKeys(txt_RequesterSearch, "A.W.MKL.Hesha Asanka");
		pressEnter(txt_RequesterSearch);
		implisitWait(8);
		String employye_on_selection_window = getText(lnk_searchResultRequester);

		if (employye_on_selection_window.contains("A.W.MKL.Hesha Asanka")) {
			writeTestResults("Verify valid employees are loaded", "Valid Employees should be loaded",
					"Valid employees successfully loaded", "pass");
		} else {
			writeTestResults("Verify valid employees are loaded", "Valid Employees should be loaded",
					"Valid employees doesn't loaded", "fail");
		}

		doubleClick(lnk_searchResultRequester);

		String employee_front_page = getAttribute(lnk_searchResultRequester, "title");
		if ((employee_front_page.contains("A.W.MKL.Hesha Asanka")) || (employee_front_page.equals("000001"))) {
			writeTestResults("Verify user can select the particualr user",
					"User should be able to select the particualr user", "User successfully select the particualr user",
					"pass");
		} else {
			writeTestResults("Verify user can select the particualr user",
					"User should be able to select the particualr user", "User doesn't select the particualr user",
					"fail");
		}

	}

	/* IN_IO_004 */
	public void iwIo001() throws Exception {
		actionVerificationLogin();
		balancingLevelConfigRegression01("Code Only");
		checkEmployeeAccordingToBalancingLevelSetting("Code Only");
		balancingLevelConfigRegression01("Name Only");
		checkEmployeeAccordingToBalancingLevelSetting("Name Only");
		balancingLevelConfigRegression01("Code and Description");
		checkEmployeeAccordingToBalancingLevelSetting("Code and Description");
	}

	public void balancingLevelConfigRegression01(String serch_by) throws Exception {
		click(navigation_pane);
		click(btn_adminModule);
		click(btn_balancingLevelAdminModule);
		implisitWait(10);
		click(btn_genarelBalancingLevelSetting);
		Thread.sleep(2000);
		selectText(drop_employeeSerchConfig, serch_by);
		Thread.sleep(100);
		click(btn_updateBalncilgLevelEmployeeSerchConfig);
		Thread.sleep(2000);
	}

	public void checkEmployeeAccordingToBalancingLevelSetting(String search_by) throws Exception {
		String employee = "";
		if (search_by.equals("Name Only")) {
			employee = employeeNameRegressionNew1;
		}

		if (search_by.equals("Code Only")) {
			employee = mmployeeCodeRegressionNew1;
		}

		if (search_by.equals("Code and Description")) {
			employee = mmployeeCodeRegressionNew1 + " [" + employeeNameRegressionNew1 + "]";
		}

		openPage(readIWRegression01("IN_IO_002"));

		click(btn_searchRequester);

		sendKeys(txt_RequesterSearch, employee);
		pressEnter(txt_RequesterSearch);

		doubleClick(lnk_searchResultRequester);

		String employee_front_page = getAttribute(lnk_searchResultRequester, "title");
		if (employee_front_page.equals(employee)) {
			writeTestResults(
					"Verify that selected employee display according to the set up done by balancing level setting ("
							+ search_by + ")",
					"Employee display according to the set up done by balancing level setting (" + search_by + ")",
					"Employee successfully display according to the set up done by balancing level setting ("
							+ search_by + ")",
					"pass");
		} else {
			writeTestResults(
					"Employee display according to the set up done by balancing level setting (" + search_by + ")",
					"Employee display according to the set up done by balancing level setting (" + search_by + ")",
					"Employee doesn't display according to the set up done by balancing level setting (" + search_by
							+ ")",
					"fail");
		}
	}

	/* IN_IO_005 */
	public void inactivaeCampaign() throws InvalidFormatException, IOException, Exception {
		actionVerificationLogin();
		openPage(readIWRegression01("IN_IO_002"));
		implisitWait(10);
		click(navigation_pane);
		click(btn_salesModule);
		click(btn_campaigsSalesModule);
		sendKeys(txt_searchCampaign, campaign_iwIO005);
		pressEnter(txt_searchCampaign);
		Thread.sleep(3000);
		click(btn_resultCampaignSalesModule);
		implisitWait(10);
		click(btn_edit);
		click(btn_statusDocument);
		selectIndex(drop_docStatus, 3);
		click(btn_applyStatusMenu);
		Thread.sleep(1000);
		click(btn_update);
		implisitWait(10);
	}

	public void checkInactiveCampaignAreNotVisible() throws Exception {
		openPage(readIWRegression01("IN_IO_002"));
		implisitWait(10);
		Thread.sleep(1000);
		click(btn_campaignLookup);
		sendKeys(txt_searchCampaign, campaign_iwIO005);
		pressEnter(txt_searchCampaign);
		Thread.sleep(3000);
		if (!isDisplayedQuickCheck(btn_resultCampaign)) {
			writeTestResults("Verify inactive campaigns doesn't loaded", "Inactive campaigns shouldn't be loaded",
					"Inactive campaigns does'nt loaded", "pass");
		} else {
			writeTestResults("Verify inactive campaigns doesn't loaded", "Inactive campaigns shouldn't be loaded",
					"Inactive campaigns were loaded", "fail");
		}

	}

	public void activaeCampaign() throws InvalidFormatException, IOException, Exception {
		openPage(readIWRegression01("IN_IO_002"));
		implisitWait(10);
		click(navigation_pane);
		click(btn_salesModule);
		click(btn_campaigsSalesModule);
		sendKeys(txt_searchCampaign, campaign_iwIO005);
		pressEnter(txt_searchCampaign);
		Thread.sleep(3000);
		click(btn_resultCampaignSalesModule);
		implisitWait(10);
		click(btn_edit);
		click(btn_statusDocument);
		selectIndex(drop_docStatus, 2);
		click(btn_applyStatusMenu);
		Thread.sleep(1000);
		click(btn_update);
		implisitWait(10);
	}

	public void checkAactiveCampaignAreVisible() throws Exception {
		openPage(readIWRegression01("IN_IO_002"));
		implisitWait(10);
		Thread.sleep(1000);
		click(btn_campaignLookup);
		sendKeys(txt_searchCampaign, campaign_iwIO005);
		pressEnter(txt_searchCampaign);
		implisitWait(8);
		doubleClick(btn_resultCampaign);
		Thread.sleep(1500);
		String campaign_front_page = getAttribute(txt_campaignFrontPage, "value");
		if (campaign_front_page.equals(campaign_iwIO005)) {
			writeTestResults("Verify user can select the valid campaign",
					"User should be able to select the valid campaign", "User successfully select the valid campaign",
					"pass");
		} else {
			writeTestResults("Verify user can select the valid campaign",
					"User should be able to select the valid campaign", "User doesn't select the valid campaign",
					"fail");
		}
	}

	/* IN_IO_006 */
	public void checkProductAutoSearch() throws Exception {
		/* Only need when run alone */
		actionVerificationLogin();
		openPage(readIWRegression01("IN_IO_002"));
		implisitWait(8);
		/****************************/

		String product = readTestCreation("BatchFifo");
		String pro_num = product.substring(0, 14);
		sendKeys(txt_productAutoSearch, pro_num);
		Thread.sleep(2000);
		driver.findElement(getLocator(txt_productAutoSearch)).sendKeys(Keys.ARROW_DOWN);
		pressEnter(txt_productAutoSearch);
		Thread.sleep(1000);
		String product_fieald = getAttribute(txt_productAutoSearch, "value");
		if (product.equals(product_fieald)) {
			writeTestResults("Verify auto search option is enabled and able to pick a product",
					"Auto search option should be enabled and should  be able to pick a product",
					"Auto search option successfully enabled and able to pick a product", "pass");
		} else {
			writeTestResults("Verify auto search option is enabled and able to pick a product",
					"Auto search option should be enabled and should  be able to pick a product",
					"Auto search option doesn't enabled or user couldn't able to pick a product", "fail");
		}
	}

	public void checkVarientMainProductIsNotLoading() throws Exception {
		click(btn_productLoockupCommon);
		Thread.sleep(2500);
		customizeLoadingDelay(txt_productSearch, 15);
		sendKeys(txt_productSearch, varientProduct);
		pressEnter(txt_productSearch);
		implisitWait(10);
		Thread.sleep(3500);

		boolean varient_main_pro_check = false;
		varient_main_pro_check = (!isDisplayedQuickCheck(result_varientMainProduct));

		boolean product_lookup_visibility = false;
		if (isDisplayedQuickCheck(header_productLookup)) {
			product_lookup_visibility = true;
		}

		sendKeys(txt_productSearch, readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		implisitWait(10);
		Thread.sleep(3500);
		String replacement = readTestCreation("BatchSpecific");
		String product = result_cpmmonProduct.replace("product", replacement);
		customizeLoadingDelay(product, 15);
		doubleClick(product);
		String front_page_product = getText(txt_prductGridSelectedProductOnlyOneRowIsThere).substring(0, 29);

		boolean product_selection = false;
		product_selection = (front_page_product).equals(replacement);
		if (product_selection && product_lookup_visibility) {
			writeTestResults("Verify product lookup is enabled and user can pick a product",
					"Product lookup should be enabled and should be able to pick a product",
					"Product lookup successfully enabled and user can pick a product", "pass");
		} else {
			writeTestResults("Verify product lookup is enabled and user can pick a product",
					"Product lookup should be enabled and should be able to pick a product",
					"Product lookup doesn't enabled and user couldn't pick a product", "fail");
		}

		if (varient_main_pro_check) {
			writeTestResults("Verify main product of the variants doesn't loaded in the lookup",
					"Main product of the variants should not be loaded in the lookup",
					"Main product of the variants doesn't loaded in the lookup", "pass");
		} else {
			writeTestResults("Verify main product of the variants doesn't loaded in the lookup",
					"Main product of the variants should not be loaded in the lookup",
					"Main product of the variants loaded in the lookup", "fail");
		}
	}

	/* IN_IO_007 */
	public void checkUOMaccordingToTheSelectedProduct() throws Exception {
		actionVerificationLogin();
		openPage(readIWRegression01("IN_IO_002"));
		implisitWait(8);
		click(btn_productLoockupCommon);
		Thread.sleep(2500);
		implisitWait(7);
		sendKeys(txt_productSearch, readTestCreation("BatchSpecific"));
		Thread.sleep(3500);
		pressEnter(txt_productSearch);
		implisitWait(10);
		Thread.sleep(5000);
		String replacement = readTestCreation("BatchSpecific");
		String product = result_cpmmonProduct.replace("product", replacement);
		doubleClick(product);
		Thread.sleep(2000);
		String front_page_product = getText(td_uomWhenThereIsOneRow);
		if (front_page_product.equals("EACH")) {
			writeTestResults("Verify default UOM of the particular product is loaded",
					"Default UOM of the particular product should be loaded",
					"Default UOM of the particular product successfully loaded", "pass");
		} else {
			writeTestResults("Verify default UOM of the particular product is loaded",
					"Default UOM of the particular product should be loaded",
					"Default UOM of the particular product doesn't loaded", "fail");
		}
	}

	/* IN_IO_008 */
	public void checkAndStoreOHquantity() throws InvalidFormatException, IOException, Exception {
		actionVerificationLogin();
		openPage(readIWRegression01("IN_IO_002"));
		implisitWait(8);
		click(btn_productLoockupCommon);
		Thread.sleep(2500);
		implisitWait(7);
		sendKeys(txt_productSearch, readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		implisitWait(10);
		Thread.sleep(3500);
		String replacement = readTestCreation("BatchSpecific");
		String product = result_cpmmonProduct.replace("product", replacement);
		doubleClick(product);
		Thread.sleep(1500);
		selectText(drop_wareWhenOnlyOneRowInGrid, warehouseInternelOrderEmployeeRequest);
		Thread.sleep(1500);
		oh_quantity = getAttribute(td_OHWhenThereIsOneRow, "value");

	}

	public void checkWarehouseQuantity() throws Exception {
		click(widget_checkAvailabilityWhenOneRowIsThere);
		Thread.sleep(2000);
		Actions action1 = new Actions(driver);
		WebElement we1 = driver.findElement(By.xpath(btn_productAvailabilitySecondMenuWhenOneRowIsThere));
		action1.moveToElement(we1).build().perform();
		click(btn_productAvailabilitySecondMenuWhenOneRowIsThere);
		Thread.sleep(3000);
		if (isDisplayedQuickCheck(td_availableStockGstmWarehouseAfterRelesed)) {
			product_availability_after[0] = getText(td_availableStockGstmWarehouseAfterRelesed);
		} else {
			product_availability_after[0] = "0";
		}
		click(btn_closeAvailabilityCheckPopup);
	}

	public void checkWarehouseQuantityWithDisplayedOhQuantity() throws Exception {
		if (oh_quantity.equals(product_availability_after[0])) {
			writeTestResults("Verify on hand qty is display according to the selected warehouse",
					"On hand qty should display according to the selected warehouse",
					"On hand qty successfully displayed according to the selected warehouse", "pass");
		} else {
			writeTestResults("Verify on hand qty is display according to the selected warehouse",
					"On hand qty should display according to the selected warehouse",
					"On hand qty doesn't displayed according to the selected warehouse", "fail");
		}
	}

	/* IN_IO_009 */
	public void balancingLevelConfigProductInformationRegression01(String serch_by) throws Exception {
		Thread.sleep(1000);
		click(navigation_pane);
		click(btn_adminModule);
		click(btn_balancingLevelAdminModule);
		implisitWait(10);
		click(btn_inentoryBalancingLevelSetting);
		Thread.sleep(2000);
		selectText(drop_productSerchConfig, serch_by);
		Thread.sleep(100);
		click(btn_updateBalncilgLevelProductSerchConfig);
		Thread.sleep(2000);
	}

	public void checkProductAccordingToBalancingLevelSetting(String search_by) throws Exception {
		String product_final_search = "";
		if (search_by.equals("Name Only")) {
			product_final_search = productDescription_IW_IO_009;

		}

		if (search_by.equals("Code Only")) {
			product_final_search = productCode_IW_IO_009;
		}

		if (search_by.equals("Code and Description")) {
			product_final_search = productCode_IW_IO_009 + " [" + productDescription_IW_IO_009 + "]";
		}

		openPage(readIWRegression01("IN_IO_002"));

		implisitWait(8);
		click(btn_productLoockupCommon);
		Thread.sleep(2500);
		customizeLoadingDelay(txt_productSearch, 15);
		Thread.sleep(2500);
		sendKeys(txt_productSearch, product_final_search);
		pressEnter(txt_productSearch);
		implisitWait(10);
		Thread.sleep(3500);
		String replacement = productCode_IW_IO_009;
		String product = result_cpmmonProduct.replace("product", replacement);
		customizeLoadingDelay(link_commonDoubleCrickProductResult, 15);
		Thread.sleep(700);
		doubleClick(link_commonDoubleCrickProductResult);
		Thread.sleep(1500);

		String product_front_page = getText(txt_prductGridSelectedProductOnlyOneRowIsThere);
		if (product_front_page.equals(product_final_search)) {
			writeTestResults(
					"Verify that selected product display according to the set up done by balancing level setting ("
							+ search_by + ")",
					"Product display according to the set up done by balancing level setting (" + search_by + ")",
					"Product successfully display according to the set up done by balancing level setting (" + search_by
							+ ")",
					"pass");
		} else {
			writeTestResults(
					"Verify that selected product display according to the set up done by balancing level setting ("
							+ search_by + ")",
					"Product display according to the set up done by balancing level setting (" + search_by + ")",
					"Product doesn't display according to the set up done by balancing level setting (" + search_by
							+ ")",
					"fail");
		}
	}

	public void iWIo009() throws Exception {
		actionVerificationLogin();
		balancingLevelConfigProductInformationRegression01("Code Only");
		checkProductAccordingToBalancingLevelSetting("Code Only");
		balancingLevelConfigProductInformationRegression01("Name Only");
		checkProductAccordingToBalancingLevelSetting("Name Only");
		balancingLevelConfigProductInformationRegression01("Code and Description");
		checkProductAccordingToBalancingLevelSetting("Code and Description");
	}

	/* IN_IO_010 */
	public void checkUserCanDraftWithoutMandatoryFields() throws Exception {
		actionVerificationLogin();
		openPage(readIWRegression01("IN_IO_002"));
		implisitWait(10);
		click(btn_draftCommon);
		Thread.sleep(700);
		String message = getText(para_validator);
		System.out.println();
		if (message.contains("ERROR")) {
			writeTestResults(
					"Verify validation messages is pop-up when draft the internal order without having mandatory fields",
					"Validation messages should pop-up when draft the internal order without having mandatory fields",
					"Validation messages successfully pop-up when draft the internal order without having mandatory fields",
					"pass");
		} else {
			writeTestResults(
					"Verify validation messages is pop-up when draft the internal order without having mandatory fields",
					"Validation messages should pop-up when draft the internal order without having mandatory fields",
					"Validation messages doesn't pop-up when draft the internal order without having mandatory fields",
					"fail");
		}
	}

	/* IN_IO_011 */
	public void checkUserCanDraftAndNewWithoutMandatoryFields() throws Exception {
		actionVerificationLogin();
		openPage(readIWRegression01("IN_IO_002"));
		implisitWait(10);
		click(btn_draftAndNewCommon);
		Thread.sleep(700);
		String message = getText(para_validator);
		System.out.println();
		if (message.contains("ERROR")) {
			writeTestResults(
					"Verify validation messages is pop-up when draft the internal order without having mandatory fields",
					"Validation messages should pop-up when draft the internal order without having mandatory fields",
					"Validation messages successfully pop-up when draft the internal order without having mandatory fields",
					"pass");
		} else {
			writeTestResults(
					"Verify validation messages is pop-up when draft the internal order without having mandatory fields",
					"Validation messages should pop-up when draft the internal order without having mandatory fields",
					"Validation messages doesn't pop-up when draft the internal order without having mandatory fields",
					"fail");
		}
	}

	/* IN_IO_012 */
	public void navigateToNewInternelOrder() throws Exception {
		actionVerificationLogin();
		openPage(readIWRegression01("IN_IO_002"));
		implisitWait(10);
	}

	public void fillInternelOrder() throws Exception {
		customizeLoadingDelay(txt_tiltleInternelOrder, 25);
		sendKeys(txt_tiltleInternelOrder, titleInternelOrder);

		click(btn_searchRequester);
		handeledSendKeys(txt_RequesterSearch, "");
		pressEnter(txt_RequesterSearch);

		doubleClick(lnk_searchResultRequester);

		/* Product add to the grid */
		int j = 0;
		for (int i = 1; i < 2; i++) {
			String str_i = String.valueOf(i);

			String lookup_path = btn_productLookupInterDeprtmentTransferOrderForLoop.replace("row", str_i)
					.replace("column", "10").replace("tblStockAdj", "tblInternalRequest");
			click(lookup_path);
			sleepCusomized(txt_productSearch);
			String product = "";

			if (i == 1) {
				product = "BatchFifo";
			} else if (i == 2) {
				product = "BatchSpecific";
			} else if (i == 3) {
				product = "Lot";
			} else if (i == 4) {
				product = "SerielSpecific";
			} else if (i == 5) {
				product = "SerielBatchSpecific";
			} else if (i == 6) {
				product = "SerielBatchFifo";
			} else if (i == 7) {
				product = "SerirlFifo";
			} else {
				System.out.println("Product type cannt identified.........");
			}
			customizeLoadingDelay(txt_productSearch, 15);
			sendKeys(txt_productSearch, readTestCreation(product));
			pressEnter(txt_productSearch);
			Thread.sleep(5000);
			doubleClick(btn_resultProduct);

			String txt_qty = txt_quantityInGrid.replace("row", str_i).replace("tblName", "tblInternalRequest");

			customizeLoadingDelay(txt_qty, 7);
			sendKeys(txt_qty, qtyInterDepartmentTransferOrder);

		}

		click(btn_detailTab);
		selectText(drop_warehouseInternelOrderEmployeeRequest, warehouseInternelOrderEmployeeRequest);
	}

	public void draftInterelOrderRegression_IN_IO_012() throws Exception {
		click(btn_draftCommon);
		customizeLoadingDelay(lbl_creationsDocNo, 40);
		customizeLoadingDelay(btn_releaseCommon, 40);
		trackCode = getText(lbl_creationsDocNo);
		writeRegression01("Drafted_IO_IN_IO_012", trackCode, 2);
		if (isDisplayed(btn_releaseCommon)) {
			writeTestResults("Verify Internel Order is draft", "Internal Order should draft ",
					"Internal Order successfully draft ", "pass");
			writeRegression01("Drafted_Link_IO_IN_IO_012", getCurrentUrl(), 3);
		} else {
			writeTestResults("Verify Internel Order is draft", "Internal Order should draft ",
					"Internal Order doesn't draft ", "fail");
		}
	}

	/* IN_IO_013 */
	public void fillDrafAndNewInternelOrderIN_IO_013() throws Exception {
		actionVerificationLogin();
		openPage(readIWRegression01("IN_IO_002"));
		implisitWait(10);
		fillInternelOrder();
		click(btn_draftAndNewCommon);
		implisitWait(10);
		fillInternelOrder();
		click(btn_draftCommon);
		customizeLoadingDelay(btn_releaseCommon, 50);
		trackCode = getText(lbl_creationsDocNo);
		writeRegression01("IN_IO_013_DarftedInternelOrderWhichComesFromDraftAndNew", trackCode, 4);
		if (isDisplayed(btn_releaseCommon)) {
			writeTestResults("Verify New Internel Order is draft", "New Internel Order should draft ",
					"New Internel Order successfully draft ", "pass");
		} else {
			writeTestResults("Verify New Internel Order is draft", "New Internel Order should draft ",
					"New Internel Order doesn't draft ", "fail");
		}

	}

	/* IN_IO_014 */
	public void verifyCopyFromProcessInternelOrder() throws InvalidFormatException, IOException, Exception {
		actionVerificationLogin();
		openPage(readIWRegression01("IN_IO_002"));
		implisitWait(10);
		click(btn_copyFromCommon);
		Thread.sleep(1000);
		boolean io_lookup_flag = isDisplayedQuickCheck(header_IOLookup);
		sendKeys(txt_internelOrderLookupSearch, readIWRegression01("Drafted_IO_IN_IO_012"));
		pressEnter(txt_internelOrderLookupSearch);
		Thread.sleep(3000);
		String result_io_xpath = btn_resultIO.replace("doc_no", readIWRegression01("Drafted_IO_IN_IO_012"));
		boolean io_availability_flag = isDisplayedQuickCheck(result_io_xpath);
		doubleClick(result_io_xpath);

		if (io_lookup_flag && io_availability_flag) {
			writeTestResults("Verify existing  internal orders were loaded and user can select the particular order",
					"Existing  internal orders should be loaded and user should allow to select the particular order",
					"Existing  internal orders successfully loaded and user can select the particular order", "pass");
		} else {
			writeTestResults("Verify existing  internal orders were loaded and user can select the particular order",
					"Existing  internal orders should be loaded and user should allow to select the particular order",
					"Existing  internal orders doesn't loaded and user couldn't select the particular order", "fail");
		}

	}

	public void changeesAndDraftInternelOrder() throws Exception {
		String tittle = currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "-Madhushan";
		sendKeys(txt_tiltleInternelOrder, tittle);
		click(tab_summaryCommon);
		String title_front_page = getAttribute(txt_tiltleInternelOrder, "value");
		if (title_front_page.equals(tittle)) {
			writeTestResults("Verify user can do any changes", "User should be able to do any changes",
					"User can do any changes", "pass");
		} else {
			writeTestResults("Verify user can do any changes", "User should be able to do any changes",
					"User couldn't do any changes", "fail");
		}

		click(btn_draftCommon);
		implisitWait(10);
		Thread.sleep(3000);
		trackCode = getText(lbl_creationsDocNo);
		writeRegression01("DraftedInternelOrderThatGetFromCopyFrom_IN_IO_014", trackCode, 5);
		if (isDisplayed(btn_releaseCommon)) {
			writeTestResults("Verify user can draft the internal order",
					"User should be able to draft the internal order", "User successfully draft the internal order",
					"pass");
		} else {
			writeTestResults("Verify user can draft the internal order",
					"User should be able to draft the internal order", "User couldn't draft the internal order",
					"fail");
		}
	}

	/* IN_IO_015 */
	public void releaseInternelOrderIN_IO_015() throws InvalidFormatException, IOException, Exception {
		actionVerificationLogin();
		openPage(readIWRegression01("Drafted_Link_IO_IN_IO_012"));
		customizeLoadingDelay(btn_releaseCommon, 40);
		click(btn_releaseCommon);
		Thread.sleep(1000);
		pageRefersh();
		implisitWait(12);
		Thread.sleep(3000);
		String header1 = getText(header_draft);
		if (header1.equalsIgnoreCase("(Released)")) {
			trackCode = getText(lbl_creationsDocNo);
			writeRegression01("RelesedInternelOrderIN_IO_015", trackCode, 6);
			writeTestResults("Verify Internal Order was released", "Internal Order should be released",
					"Internal Order successfully released", "pass");
			writeRegression01("ReleasedIO_Link_IN_IO_015", getCurrentUrl(), 9);
		} else {
			writeTestResults("Verify Internal Order was released", "Internal Order should be released",
					"Internal Order doesn't released", "fail");
		}
	}

	/* IN_IO_016 */
	public void findInternoOrderWhichComesFromDraftAndNewButton() throws Exception {
		actionVerificationLogin();
		openPage(readIWRegression01("IN_IO_001"));
		customizeLoadingDelay(txt_internelOrderLookupSearch, 50);
		Thread.sleep(2000);
		handeledSendKeys(txt_internelOrderLookupSearch,
				readIWRegression01("IN_IO_013_DarftedInternelOrderWhichComesFromDraftAndNew"));
		pressEnter(txt_internelOrderLookupSearch);
		String result_xpaht = link_resultDocInList.replace("doc_num",
				readIWRegression01("IN_IO_013_DarftedInternelOrderWhichComesFromDraftAndNew"));
		customizeLoadingDelay(result_xpaht, 50);
		Thread.sleep(2500);
		click(result_xpaht);
		customizeLoadingDelay(btn_releaseCommon, 40);

	}

	public void releaseInternoOrderWhichComesFromDraftAndNewButton() throws Exception {
		click(btn_releaseCommon);
		Thread.sleep(1000);
		pageRefersh();
		implisitWait(12);
		Thread.sleep(3000);
		String header1 = getText(header_draft);
		if (header1.equalsIgnoreCase("(Released)")) {
			trackCode = getText(lbl_creationsDocNo);
			writeTestResults("Verify user can release the drafted internal Order [ Comes through 'Draft & New' ]",
					"User shoul release the drafted internal Order [ Comes through 'Draft & New' ]",
					"User shoul release the drafted internal Order [ Comes through 'Draft & New' ]", "pass");
		} else {
			writeTestResults("Verify user can release the drafted internal Order [ Comes through 'Draft & New' ]",
					"User shoul release the drafted internal Order [ Comes through 'Draft & New' ]",
					"User couldn't release the drafted internal Order [ Comes through 'Draft & New' ]", "fail");
		}
	}

	/* IN_IO_017 */
	public void findInternoOrderWhichComesFromDraftAndNewButton17() throws Exception {
		actionVerificationLogin();
		openPage(readIWRegression01("IN_IO_001"));
		customizeLoadingDelay(txt_internelOrderLookupSearch, 40);
		handeledSendKeys(txt_internelOrderLookupSearch,
				readIWRegression01("DraftedInternelOrderThatGetFromCopyFrom_IN_IO_014"));
		pressEnter(txt_internelOrderLookupSearch);
		String result_xpaht = link_resultDocInList.replace("doc_num",
				readIWRegression01("DraftedInternelOrderThatGetFromCopyFrom_IN_IO_014"));
		customizeLoadingDelay(result_xpaht, 40);
		Thread.sleep(3000);
		click(result_xpaht);
		Thread.sleep(2000);

	}

	public void releaseInternoOrderWhichComesFromDraftAndNewButton17() throws Exception {
		customizeLoadingDelay(btn_releaseCommon, 40);
		click(btn_releaseCommon);
		Thread.sleep(1000);
		pageRefersh();
		implisitWait(12);
		Thread.sleep(5000);
		String header1 = getText(header_draft);
		if (header1.equalsIgnoreCase("(Released)")) {
			trackCode = getText(lbl_creationsDocNo);
			writeTestResults("Verify user can release the drafted internal Order [ Comes through 'Copy From' ]",
					"User shoul release the drafted internal Order [ Comes through 'Copy From' ]",
					"User shoul release the drafted internal Order [ Comes through 'Copy From' ]", "pass");
		} else {
			writeTestResults("Verify user can release the drafted internal Order [ Comes through 'Copy From' ]",
					"User shoul release the drafted internal Order [ Comes through 'Copy From' ]",
					"User couldn't release the drafted internal Order [ Comes through 'Copy From' ]", "fail");
		}
	}

	/* IN_IO_018 */
	public void navigateToInternelOrderAndFill_IN_IO_018() throws Exception {
		actionVerificationLogin();
		openPage(readIWRegression01("IN_IO_002"));
		implisitWait(10);
		sendKeys(txt_tiltleInternelOrder, titleInternelOrder);

		click(btn_searchRequester);

		pressEnter(txt_RequesterSearch);

		doubleClick(lnk_searchResultRequester);

		/* Product add to the grid */
		int j = 0;
		for (int i = 1; i < 2; i++) {
			String str_i = String.valueOf(i);

			String lookup_path = btn_productLookupInterDeprtmentTransferOrderForLoop.replace("row", str_i)
					.replace("column", "10").replace("tblStockAdj", "tblInternalRequest");
			click(lookup_path);
			sleepCusomized(txt_productSearch);
			String product = "";

			if (i == 1) {
				product = "BatchSpecific";
			} else if (i == 2) {
				product = "BatchFifo";
			} else if (i == 3) {
				product = "Lot";
			} else if (i == 4) {
				product = "SerielSpecific";
			} else if (i == 5) {
				product = "SerielBatchSpecific";
			} else if (i == 6) {
				product = "SerielBatchFifo";
			} else if (i == 7) {
				product = "SerirlFifo";
			} else {
				System.out.println("Product type cannt identified.........");
			}
			customizeLoadingDelay(txt_productSearch, 15);
			sendKeys(txt_productSearch, readTestCreation(product));
			pressEnter(txt_productSearch);
			Thread.sleep(5000);
			doubleClick(btn_resultProduct);

			String txt_qty = txt_quantityInGrid.replace("row", str_i).replace("tblName", "tblInternalRequest");

			customizeLoadingDelay(txt_qty, 7);
			sendKeys(txt_qty, qtyInterDepartmentTransferOrder);

		}

		click(btn_detailTab);
		selectText(drop_warehouseInternelOrderEmployeeRequest, warehouseInternelOrderEmployeeRequest);
	}

	public void verifydraftReleseAndGoToPageLinkAvailabilityAndNavigateTiInternelDispatchOrder() throws Exception {
		click(btn_draftCommon);
		implisitWait(10);
		click(btn_releaseCommon);
		Thread.sleep(1000);
		implisitWait(12);
		click(btn_goToPageLink);
		switchWindow();
		implisitWait(2000);
		String page_name = getText(heade_page);
		if (page_name.equals("Internal Dispatch Order")) {
			writeTestResults("Verify user can navigate to Internal Dispatch Order new form",
					"User should be navigate to Internal Dispatch Order new form",
					"User successfully navigate to Internal Dispatch Order new form", "pass");
		} else {
			writeTestResults("Verify user can navigate to Internal Dispatch Order new form",
					"User should be navigate to Internal Dispatch Order new form",
					"User doesn't navigate to Internal Dispatch Order new form", "fail");
		}

	}

	/* IN_IO_019 */
	public void naviagteToInternelDispatchOrderByTaskMenu() throws Exception {
		actionVerificationLogin();
		customizeLoadingDelay(btn_taskTileCommon, 30);
		Thread.sleep(2000);
		click(btn_taskTileCommon);
		customizeLoadingDelay(btn_internelDispatchOrderTile, 30);
		click(btn_internelDispatchOrderTile);
		Thread.sleep(2000);
		customizeLoadingDelay(txt_taskSearch, 30);
		sendKeys(txt_taskSearch, readIWRegression01("RelesedInternelOrderIN_IO_015"));
		Thread.sleep(1600);
		driver.findElement(getLocator(txt_taskSearch)).sendKeys(Keys.ARROW_DOWN);
		pressEnter(txt_taskSearch);
		Thread.sleep(2000);
		String dispatch_order = btn_releventTaskOpenLink.replace("doc_num",
				readIWRegression01("RelesedInternelOrderIN_IO_015"));
		click(dispatch_order);
		switchWindow();
		boolean flag_header = false;
		boolean flag_product = false;
		if (isDisplayedQuickCheck(header_InternelDispatchOrder)) {
			flag_header = true;
		}

		String product_front_page = getText(
				txt_prductGridSelectedProductOnlyOneRowIsThere.replace("el-resize9", "el-resize8"));
		String product = readTestCreation("BatchSpecific");
		if (product_front_page.contains(product) || product_front_page.contains("Test Product")) {
			flag_product = true;
		}

		if (flag_header && flag_product) {
			writeTestResults(
					"Verify user can navigate to Internal dispatch Order new form with having relevant details of internal Order",
					"User should be navigate to Internal dispatch Order new form with having relevant details of internal Order",
					"User successfully navigate to Internal dispatch Order new form with having relevant details of internal Order",
					"pass");
			writeRegression01("IN_IO_019_IDO_whichComesFromTask_Event", getCurrentUrl(), 7);
		} else {
			writeTestResults(
					"Verify user can navigate to Internal dispatch Order new form with having relevant details of internal Order",
					"User should be navigate to Internal dispatch Order new form with having relevant details of internal Order",
					"User couldn't navigate to Internal dispatch Order or form doesn't with relevant details of internal Order",
					"fail");
		}
	}

	/* IN_IO_020 */
	public void draftInternelDispatchOrderWhichComesFromTaskEvent() throws Exception {
		actionVerificationLogin();
		implisitWait(8);
		openPage(readIWRegression01("IN_IO_019_IDO_whichComesFromTask_Event"));
		customizeLoadingDelay(btn_draftCommon, 40);
		click(btn_draftCommon);
		implisitWait(10);
		customizeLoadingDelay(lbl_creationsDocNo, 40);
		trackCode = getText(lbl_creationsDocNo);
		if (isDisplayed(btn_releaseCommon)) {
			writeTestResults("Verify user can draft the Internal Dispatch Order",
					"Internal Dispatch Order should be drafted", "Internal Dispatch Order successfully drafted",
					"pass");
			writeRegression01("DraftedIDO_IN_IO_020", getCurrentUrl(), 8);
		} else {
			writeTestResults("Verify user can draft the Internal Dispatch Order",
					"Internal Dispatch Order should be drafted", "Internal Dispatch Order doesn't drafted", "fail");
		}
	}

	/* IN_IO_021 */
	public void releaseInternelDispatchOrderWhichComesFromTaskEvent() throws Exception {
		actionVerificationLogin();
		openPage(readIWRegression01("DraftedIDO_IN_IO_020"));
		customizeLoadingDelay(btn_releaseCommon, 50);
		click(btn_releaseCommon);
		customizeLoadingDelay(lbl_relesedConfirmation, 30);

		if (isDisplayed(lbl_relesedConfirmation)) {
			trackCode = getText(lbl_creationsDocNo);
			writeTestResults("Verify user can release the Internal Dispatch Order",
					"Internal Dispatch Order should be released", "Internal Dispatch Order successfully released",
					"pass");
		} else {
			writeTestResults("Verify user can release the Internal Dispatch Order",
					"Internal Dispatch Order should be released", "Internal Dispatch Order doesn't released", "fail");
		}
	}

	/* IN_IO_022 */
	public void duplicateInternelOrderAndVerifyFialdsRegression01() throws Exception {
		actionVerificationLogin();
		implisitWait(8);
		openPage(readIWRegression01("ReleasedIO_Link_IN_IO_015"));
		implisitWait(8);
		Thread.sleep(2000);

		String before_duplicate[] = new String[4];
		before_duplicate[0] = getText(lbl_titleIOFrontPage);
		before_duplicate[1] = getText(lbl_requesterIOFrontPage);
		before_duplicate[2] = getText(lbl_productIOFrontPage);
		before_duplicate[3] = getText(lbl_warehouseIOFrontPage);

		click(btn_duplicate);
		implisitWait(8);
		Thread.sleep(2000);
		String after_duplicate[] = new String[4];
		after_duplicate[0] = getAttribute(txt_nameInternelOrder, "value");
		after_duplicate[1] = getAttribute(txt_requstrerInternelOrderFrontPage, "value");
		after_duplicate[2] = getText(txt_productInternelOrderFrontPage);
		Select comboBox = new Select(driver.findElement(By.xpath(txt_warehouseInternelOrderFrontPage)));
		after_duplicate[3] = comboBox.getFirstSelectedOption().getText();

		boolean verify_fields = false;
		for (int i = 0; i < 4; i++) {
			if (after_duplicate[i].contains(before_duplicate[i])) {
				verify_fields = true;
			} else {
				verify_fields = false;
			}
		}

		if (verify_fields) {
			writeTestResults("Verify details were loaded according to duplicated Internal Order",
					"Details should be loaded according to duplicated Internal Order",
					"Details successfully loaded according to duplicated Internal Order", "pass");
		} else {
			writeTestResults("Verify details were loaded according to duplicated Internal Order",
					"Details should be loaded according to duplicated Internal Order",
					"Details doesn't loaded according to duplicated Internal Order", "fail");
		}
	}

	public void draftReleaseDuplicatedRelesedInternelOrder() throws InvalidFormatException, IOException, Exception {
		click(btn_draftCommon);
		implisitWait(10);
		boolean doc_status_flag = false;
		if (isDisplayed(btn_releaseCommon)) {
			trackCode = getText(lbl_creationsDocNo);
			doc_status_flag = true;
		} else {
			doc_status_flag = false;
		}

		implisitWait(10);
		Thread.sleep(1000);
		click(btn_releaseCommon);
		implisitWait(12);
		Thread.sleep(3000);
		if (isDisplayed(btn_goToPageLink)) {
			doc_status_flag = true;
		} else {
			doc_status_flag = false;
		}

		if (doc_status_flag) {
			writeTestResults("Verify Internal Order was Drafted and Released",
					"Internal Order should be Drafted and Released", "Internal Order successfully Drafted and Released",
					"pass");
		} else {
			writeTestResults("Verify Internal Order was Drafted and Released",
					"Internal Order should be Drafted and Released", "Internal Order doesn't Drafted and Released",
					"fail");
		}

	}

	public void draftReleaseDuplicatedRelesedInternelDispatchOrder()
			throws InvalidFormatException, IOException, Exception {
		click(btn_goToPageLink);
		switchWindow();
		implisitWait(2000);
		boolean doc_status_flsg = false;
		String page_name = getText(heade_page);
		if (page_name.equals("Internal Dispatch Order")) {
			doc_status_flsg = true;
		} else {
			doc_status_flsg = false;
		}

		click(btn_draftCommon);
		implisitWait(10);
		if (isDisplayed(btn_releaseCommon)) {
			doc_status_flsg = true;
		} else {
			doc_status_flsg = false;
		}

		click(btn_releaseCommon);
		customizeLoadingDelay(lbl_relesedConfirmation, 45);
		if (isDisplayed(lbl_relesedConfirmation)) {
			writeTestResults("Verify Internel Dispatch Order was Drafted and Released",
					"Internel Dispatch Order should be Drafted and Released",
					"Internel Dispatch Order successfully Drafted and Released", "pass");
		} else {
			writeTestResults("Verify Internel Dispatch Order was Drafted and Released",
					"Internel Dispatch Order should be Drafted and Released",
					"Internel Dispatch Order doesn't Drafted and Released", "fail");
		}
	}

	/* IN_IO_023 */
	public void fillMandatoryFieldsWithMultipleProductsIN_IO_023() throws Exception {
		actionVerificationLogin();
		implisitWait(10);
		Thread.sleep(1000);
		click(navigation_pane);
		Thread.sleep(500);
		click(btn_adminModule);
		Thread.sleep(500);
		click(btn_journeyConfiguration);
		implisitWait(8);
		Thread.sleep(500);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript(btn_journeyConfigInternelOrderEmployeeRequest);

		if (!isSelected(chk_stockValidate)) {
			click(chk_stockValidate);
		}

		selectText(drop_stockReservationMethod, stockReservationMethod);
		click(btnn_updateEmployeeRequestJourneyConfiguration);
		Thread.sleep(1000);
		openPage(readIWRegression01("IN_IO_002"));
		implisitWait(10);
		sendKeys(txt_tiltleInternelOrder, titleInternelOrder);

		// Order date get default by current date

		click(btn_searchRequester);

		pressEnter(txt_RequesterSearch);

		doubleClick(lnk_searchResultRequester);

		employee = getAttribute(lnk_searchResultRequester, "title");

		/* Product add to the grid */
		int j = 0;
		for (int i = 1; i < 8; i++) {
			String str_i = String.valueOf(i);

			String lookup_path = btn_productLookupInterDeprtmentTransferOrderForLoop.replace("row", str_i)
					.replace("column", "10").replace("tblStockAdj", "tblInternalRequest");
			click(lookup_path);
			Thread.sleep(3000);

			String product = "";

			if (i == 1) {
				product = "BatchSpecific";
			} else if (i == 2) {
				product = "BatchFifo";
			} else if (i == 3) {
				product = "Lot";
			} else if (i == 4) {
				product = "SerielSpecific";
			} else if (i == 5) {
				product = "SerielBatchSpecific";
			} else if (i == 6) {
				product = "SerielBatchFifo";
			} else if (i == 7) {
				product = "SerirlFifo";
			} else {
				System.out.println("Product type cannt identified.........");
			}
			Thread.sleep(2500);
			customizeLoadingDelay(txt_productSearch, 15);
			sendKeys(txt_productSearch, readTestCreation(product));
			pressEnter(txt_productSearch);
			Thread.sleep(6000);
			;
			doubleClick(btn_resultProduct);

//			Selected products should be added to the Grid and OnHand Quantity [From][To], Default UOM should be loaded for selected product

			String txt_qty = txt_quantityInGrid.replace("row", str_i).replace("tblName", "tblInternalRequest");

			customizeLoadingDelay(txt_qty, 7);
			Thread.sleep(1000);
			sendKeys(txt_qty, qtyInterDepartmentTransferOrder);
			Thread.sleep(3000);
			click(btn_addNewRow);
		}

		click(btn_deleteEmotyRowStockAdjustment.replace("tblStockAdj", "tblInternalRequest"));

		click(btn_detailTab);
		selectText(drop_warehouseInternelOrderEmployeeRequest, warehouseInternelOrderEmployeeRequest);

		String fields[] = new String[4];
		fields[0] = getAttribute(txt_nameInternelOrder, "value");
		fields[1] = getAttribute(txt_requstrerInternelOrderFrontPage, "value");
		fields[2] = getText(txt_productInternelOrderFrontPage);
		Select comboBox = new Select(driver.findElement(By.xpath(txt_warehouseInternelOrderFrontPage)));
		fields[3] = comboBox.getFirstSelectedOption().getText();

		boolean verify_fields = false;
		for (int i = 0; i < 4; i++) {
			if ((fields[i].equals("")) || (fields[i].equals(" "))) {
				verify_fields = true;
			} else {
				verify_fields = false;
			}
		}

		if (verify_fields) {
			writeTestResults("Verify user user can fill mandatory fields with multiple products",
					"User should be able to fill mandatory fields with multiple products",
					"User successfully fill mandatory fields with multiple products", "pass");
		} else {
			writeTestResults("Verify user user can fill mandatory fields with multiple products",
					"User should be able to fill mandatory fields with multiple products",
					"User doesn't fill mandatory fields with multiple products", "fail");
		}

	}

	public void draftReleseInternelOrderAndInternelDispatchOrder_IN_IO_023()
			throws InvalidFormatException, IOException, Exception {
		draftReleaseDuplicatedRelesedInternelOrder();
		click(btn_goToPageLink);
		switchWindow();
		implisitWait(2000);

		int row_position = 1;
		for (int i = 0; i < 7; i++) {
			String row_str = String.valueOf(row_position);
			Thread.sleep(3000);
			Actions action11 = new Actions(driver);
			WebElement we11 = driver.findElement(By.xpath(btn_availabilityCheckWidgetAfterrelesed
					.replace("tblStockAdj", "tblDispatchOrder").replace("row", row_str)));
			action11.moveToElement(we11).build().perform();
			click(btn_availabilityCheckWidgetAfterrelesed.replace("tblStockAdj", "tblDispatchOrder").replace("row",
					row_str));
			Thread.sleep(2000);
			Actions action1 = new Actions(driver);
			WebElement we1 = driver.findElement(By.xpath(btn_avilabilytCheckSecondMenu));
			action1.moveToElement(we1).build().perform();
			click(btn_avilabilytCheckSecondMenu);
			Thread.sleep(5000);
			if (!isDisplayedQuickCheck(td_availableStockGstmWarehouseAfterRelesed)) {
				Thread.sleep(5000);
			}
			product_availability[i] = getText(td_availableStockGstmWarehouseAfterRelesed);
			System.out.println(product_availability[i]);
			click(btn_closeAvailabilityCheckPopup);
			row_position++;
		}

		boolean doc_status_flsg = false;
		String page_name = getText(heade_page);
		if (page_name.equals("Internal Dispatch Order")) {
			doc_status_flsg = true;
		} else {
			doc_status_flsg = false;
		}

		click(btn_draftCommon);
		implisitWait(10);
		if (isDisplayed(btn_releaseCommon)) {
			doc_status_flsg = true;
		} else {
			doc_status_flsg = false;
		}

		click(brn_serielBatchCapture);

		/* Product 01 */

		click(btn_captureItem1);
		Thread.sleep(1000);

		click(btn_capturePageDocIcon);

		click(chk_availableProductsTransferOrderOutboundShipment.replace("row", "1"));

		String qty = getText(lbl_qtyBatchSpecifificCapturePage);
		String qty_capture = getText(lbl_availQuantityProductCapturuPage);

		int qty_int = Integer.parseInt(qty);
		int qty_capture_int = Integer.parseInt(qty_capture);

		int i = 2;
		while (qty_int >= qty_capture_int) {
			String i_str = String.valueOf(i);
			click(chk_deleteItemCapturePage);
			click(btn_deleteRowProductCaptururePage);
			click(btn_capturePageDocIcon);
			click(chk_availableProductsTransferOrderOutboundShipment.replace("row", i_str));
			Thread.sleep(2000);
			qty_capture = getText(lbl_availQuantityProductCapturuPage);
			qty_capture_int = Integer.parseInt(qty_capture);
		}

		click(btn_updateCapture);

		/* Product 04 */
		String item_xpath = btn_itemProductCapturePage.replace("_product_number", "4");
		sleepCusomized(item_xpath);

		click(item_xpath);
		click(chk_productCaptureRange);

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$('#attrSlide-txtSerialCount').val(" + qtyTransferOrder + ")");

		String lot4 = getOngoinfSerirlSpecificSerielNumber(10);
		String final_lot_script = "$('#attrSlide-txtSerialFrom').val(\"" + lot4 + "\")";
		jse.executeScript(final_lot_script);

		pressEnter(txt_seriealProductCapture);

		click(btn_updateCapture);

		/* Product 05 */
		String item_xpath1 = btn_itemProductCapturePage.replace("_product_number", "5");
		Thread.sleep(4000);
		click(item_xpath1);

		Thread.sleep(2000);
		click(chk_productCaptureRange);

		Thread.sleep(1000);
		jse.executeScript("$('#attrSlide-txtSerialCount').val(" + qtyTransferOrder + ")");

		String final_lot_script1 = "$('#attrSlide-txtSerialFrom').val(\""
				+ getOngoingSerielBatchSpecificSerielNumber(10) + "\")";
		jse.executeScript(final_lot_script1);

		pressEnter(txt_seriealProductCapture);

		Thread.sleep(1000);

		click(btn_updateCapture);

		sleepCusomized(btn_backButtonUpdateCapture);
		click(btn_backButtonUpdateCapture);

		click(btn_releaseCommon);
		customizeLoadingDelay(lbl_relesedConfirmation, 45);
		String header1 = getText(header_draft);
		if (header1.equalsIgnoreCase("(Released)")) {
			Thread.sleep(8000);
			header1 = getText(header_draft);
		}
		if (header1.equalsIgnoreCase("(Released)")) {
			trackCode = getText(lbl_creationsDocNo);
			doc_status_flsg = true;
		} else {
			doc_status_flsg = false;
		}

		if (doc_status_flsg) {
			writeTestResults("Verify Internel Dispatch Order was Drafted and Released",
					"Internel Dispatch Order should be Drafted and Released",
					"Internel Dispatch Order successfully Drafted and Released", "pass");
		} else {
			writeTestResults("Verify Internel Dispatch Order was Drafted and Released",
					"Internel Dispatch Order should be Drafted and Released",
					"Internel Dispatch Order doesn't Drafted and Released", "fail");
		}

		int row_position1 = 1;
		for (int j = 0; j < 7; j++) {
			String row_str = String.valueOf(row_position1);
			sleepCusomized(btn_availabilityCheckWidgetAfterrelesed.replace("tblStockAdj", "tblDispatchOrder")
					.replace("row", row_str));
			Actions action11 = new Actions(driver);
			WebElement we11 = driver.findElement(By.xpath(btn_availabilityCheckWidgetAfterrelesed
					.replace("tblStockAdj", "tblDispatchOrder").replace("row", row_str)));
			action11.moveToElement(we11).build().perform();
			click(btn_availabilityCheckWidgetAfterrelesed.replace("tblStockAdj", "tblDispatchOrder").replace("row",
					row_str));
			Thread.sleep(2000);
			Actions action1 = new Actions(driver);
			WebElement we1 = driver.findElement(By.xpath(btn_avilabilytCheckSecondMenu));
			action1.moveToElement(we1).build().perform();
			click(btn_avilabilytCheckSecondMenu);
			Thread.sleep(5000);
			product_availability_after[j] = getText(td_availableStockGstmWarehouseAfterRelesed);
			System.out.println(product_availability_after[j]);
			click(btn_closeAvailabilityCheckPopup);
			row_position1++;
		}
	}

	public void comapreTheStockThatTranferedFromTheWarehouse() throws Exception {
		boolean warehouse_check_flag = false;
		for (int j = 0; j < 7; j++) {
			String qty = product_availability[j].replaceAll(",", "");
			int qty_int = Integer.parseInt(qty);
			String after_qty = product_availability_after[j].replaceAll(",", "");
			try {
				int after_qty_int = Integer.parseInt(after_qty);
				int adding_qty = Integer.parseInt(qtyTransferOrder);
				if (after_qty_int == (qty_int - adding_qty)) {
					warehouse_check_flag = true;
				} else {
					warehouse_check_flag = false;
					break;
				}
			} catch (Exception e) {
				System.out.println(e);
			}

		}

		if (warehouse_check_flag) {
			writeTestResults("Verify stock is display correctly by deducting the Qty",
					"Stock should display correctly by deducting the Qty",
					"Stock successfully display correctly by deducting the Qty", "pass");
		} else {
			writeTestResults("Verify stock is display correctly by deducting the Qty",
					"Stock should display correctly by deducting the Qty",
					"Stock doesn't display correctly by deducting the Qty", "fail");
		}
	}

	/* IN_IO_024_026 */
	public void filInternelOrder_IN_IO_024() throws Exception {
		actionVerificationLogin();
		click(navigation_pane);
		Thread.sleep(500);
		click(btn_adminModule);
		Thread.sleep(500);
		click(btn_journeyConfiguration);
		implisitWait(8);
		Thread.sleep(500);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript(btn_journeyConfigInternelOrderEmployeeRequest);

		selectText(drop_stockReservationMethod, stockReservationMethod);

		click(btnn_updateEmployeeRequestJourneyConfiguration);
		Thread.sleep(1000);
		openPage(readIWRegression01("IN_IO_002"));
		implisitWait(10);
		sendKeys(txt_tiltleInternelOrder, titleInternelOrder);

		// Order date get default by current date

		click(btn_searchRequester);

		pressEnter(txt_RequesterSearch);

		doubleClick(lnk_searchResultRequester);

		employee = getAttribute(lnk_searchResultRequester, "title");

		/* Product add to the grid */
		int j = 0;
		for (int i = 1; i < 8; i++) {
			String str_i = String.valueOf(i);

			String lookup_path = btn_productLookupInterDeprtmentTransferOrderForLoop.replace("row", str_i)
					.replace("column", "10").replace("tblStockAdj", "tblInternalRequest");
			click(lookup_path);
			Thread.sleep(3000);

			String product = "";

			if (i == 1) {
				product = "BatchSpecific";
			} else if (i == 2) {
				product = "BatchFifo";
			} else if (i == 3) {
				product = "Lot";
			} else if (i == 4) {
				product = "SerielSpecific";
			} else if (i == 5) {
				product = "SerielBatchSpecific";
			} else if (i == 6) {
				product = "SerielBatchFifo";
			} else if (i == 7) {
				product = "SerirlFifo";
			} else {
				System.out.println("Product type cannt identified.........");
			}
			Thread.sleep(2000);
			customizeLoadingDelay(txt_productSearch, 15);
			sendKeys(txt_productSearch, readTestCreation(product));
			pressEnter(txt_productSearch);
			Thread.sleep(6000);
			;
			doubleClick(btn_resultProduct);

//			Selected products should be added to the Grid and OnHand Quantity [From][To], Default UOM should be loaded for selected product

			String txt_qty = txt_quantityInGrid.replace("row", str_i).replace("tblName", "tblInternalRequest");

			customizeLoadingDelay(txt_qty, 7);
			sendKeys(txt_qty, qtyInterDepartmentTransferOrder);
			Thread.sleep(3000);
			click(btn_addNewRow);
		}

		click(btn_deleteEmotyRowStockAdjustment.replace("tblStockAdj", "tblInternalRequest"));

		click(btn_detailTab);
		selectText(drop_warehouseInternelOrderEmployeeRequest, warehouseInternelOrderEmployeeRequest);

	}

	public void releaseDispatchOrderPartialByGoToPageLink() throws Exception {
		click(btn_draftCommon);
		customizeLoadingDelay(btn_releaseCommon, 35);
		trackCode = getText(lbl_creationsDocNo);
		writeRegression01("RelesedInternelOrderIN_IO_024", trackCode, 10);
		click(btn_releaseCommon);
		implisitWait(12);
		Thread.sleep(3000);
		click(btn_goToPageLink);
		switchWindow();
		implisitWait(20);
		click(btn_deleteRowRoReplaceInternelDispatchOrder.replace("row", "2"));
		click(btn_deleteRowRoReplaceInternelDispatchOrder.replace("row", "3"));
		click(btn_deleteRowRoReplaceInternelDispatchOrder.replace("row", "6"));
		click(btn_deleteRowRoReplaceInternelDispatchOrder.replace("row", "7"));
		implisitWait(20);
		int row_position = 1;
		for (int i = 0; i < 3; i++) {
			String row_str = String.valueOf(row_position);
			Thread.sleep(3000);
			click(btn_availabilityCheckWidgetAfterrelesed.replace("tblStockAdj", "tblDispatchOrder").replace("row",
					row_str));
			Thread.sleep(2000);
			Actions action1 = new Actions(driver);
			WebElement we1 = driver.findElement(By.xpath(btn_avilabilytCheckSecondMenu));
			action1.moveToElement(we1).build().perform();
			click(btn_avilabilytCheckSecondMenu);
			Thread.sleep(5000);
			product_availability[i] = getText(td_availableStockGstmWarehouseAfterRelesed);
			System.out.println(product_availability[i]);
			click(btn_closeAvailabilityCheckPopup);
			row_position++;
			if (row_position == 2) {
				row_position++;
				row_position++;
			}
		}
		click(btn_draftCommon);
		implisitWait(10);

		click(brn_serielBatchCapture);

		/* Product 01 */

		click(btn_captureItem1);
		Thread.sleep(1000);

		click(btn_capturePageDocIcon);

		click(chk_availableProductsTransferOrderOutboundShipment.replace("row", "1"));

		String qty = getText(lbl_qtyBatchSpecifificCapturePage);
		String qty_capture = getText(lbl_availQuantityProductCapturuPage);

		int qty_int = Integer.parseInt(qty);
		int qty_capture_int = Integer.parseInt(qty_capture);

		int i = 2;
		while (qty_int >= qty_capture_int) {
			String i_str = String.valueOf(i);
			click(chk_deleteItemCapturePage);
			click(btn_deleteRowProductCaptururePage);
			click(btn_capturePageDocIcon);
			click(chk_availableProductsTransferOrderOutboundShipment.replace("row", i_str));
			Thread.sleep(2000);
			qty_capture = getText(lbl_availQuantityProductCapturuPage);
			qty_capture_int = Integer.parseInt(qty_capture);
		}

		click(btn_updateCapture);

		/* Product 04 */
		String item_xpath = btn_itemProductCapturePage.replace("_product_number", "2");
		sleepCusomized(item_xpath);

		click(item_xpath);
		click(chk_productCaptureRange);

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$('#attrSlide-txtSerialCount').val(" + qtyTransferOrder + ")");

		String lot4 = getOngoinfSerirlSpecificSerielNumber(10);
		String final_lot_script = "$('#attrSlide-txtSerialFrom').val(\"" + lot4 + "\")";
		jse.executeScript(final_lot_script);

		pressEnter(txt_seriealProductCapture);

		click(btn_updateCapture);

		/* Product 05 */
		String item_xpath1 = btn_itemProductCapturePage.replace("_product_number", "3");
		Thread.sleep(4000);
		click(item_xpath1);

		Thread.sleep(2000);
		click(chk_productCaptureRange);

		Thread.sleep(1000);
		jse.executeScript("$('#attrSlide-txtSerialCount').val(" + qtyTransferOrder + ")");

		String final_lot_script1 = "$('#attrSlide-txtSerialFrom').val(\""
				+ getOngoingSerielBatchSpecificSerielNumber(10) + "\")";
		jse.executeScript(final_lot_script1);

		pressEnter(txt_seriealProductCapture);

		Thread.sleep(1000);

		click(btn_updateCapture);

		sleepCusomized(btn_backButtonUpdateCapture);
		click(btn_backButtonUpdateCapture);

		click(btn_releaseCommon);
		implisitWait(12);
		Thread.sleep(8000);
		String header1 = getText(header_draft);
		if (header1.equalsIgnoreCase("(Released)")) {
			trackCode = getText(lbl_creationsDocNo);
			flag_partial_released = true;
		} else {
			flag_partial_released = false;
		}

		int row_position1 = 1;
		for (int j = 0; j < 3; j++) {
			String row_str = String.valueOf(row_position1);
			sleepCusomized(btn_availabilityCheckWidgetAfterrelesed.replace("tblStockAdj", "tblDispatchOrder")
					.replace("row", row_str));
			click(btn_availabilityCheckWidgetAfterrelesed.replace("tblStockAdj", "tblDispatchOrder").replace("row",
					row_str));
			customizeLoadingDelay(btn_avilabilytCheckSecondMenu, 20);
			click(btn_avilabilytCheckSecondMenu);
			Thread.sleep(5000);
			product_availability_after[j] = getText(td_availableStockGstmWarehouseAfterRelesed);
			System.out.println(product_availability_after[j]);
			click(btn_closeAvailabilityCheckPopup);
			row_position1++;
		}

	}

	public void comapreTheStockThatTranferedFromTheWarehouse_IN_IO_024_26() throws Exception {
		boolean warehouse_check_flag = false;
		for (int j = 0; j < 3; j++) {
			String qty = product_availability[j].replaceAll(",", "");
			int qty_int = Integer.parseInt(qty);
			String after_qty = product_availability_after[j].replaceAll(",", "");
			try {
				int after_qty_int = Integer.parseInt(after_qty);
				int adding_qty = Integer.parseInt(qtyTransferOrder);
				if (after_qty_int == (qty_int - adding_qty)) {
					warehouse_check_flag = true;
				} else {
					warehouse_check_flag = false;
					break;
				}
			} catch (Exception e) {
				System.out.println(e);
			}

		}

		if (warehouse_check_flag) {
			writeTestResults("Verify stock is display correctly by deducting the Qty partially",
					"Stock should display correctly by deducting the Qty partially",
					"Stock successfully display correctly by deducting the Qty partially", "pass");
		} else {
			writeTestResults("Verify stock is display correctly by deducting the Qty partially",
					"Stock should display correctly by deducting the Qty partially",
					"Stock doesn't display correctly by deducting the Qty partially", "fail");
		}
	}

	public void releaseDispatchOrderPartialByTileMenu() throws Exception {
		implisitWait(8);
		click(btn_homeIcon);
		implisitWait(8);
		Thread.sleep(1000);
		click(btn_taskTileCommon);
		Thread.sleep(1000);
		click(btn_internelDispatchOrderTile);
		Thread.sleep(2000);
		sendKeys(txt_taskSearch, readIWRegression01("RelesedInternelOrderIN_IO_024"));
		Thread.sleep(1600);
		driver.findElement(getLocator(txt_taskSearch)).sendKeys(Keys.ARROW_DOWN);
		pressEnter(txt_taskSearch);
		Thread.sleep(2000);
		String dispatch_order = btn_releventTaskOpenLink.replace("doc_num",
				readIWRegression01("RelesedInternelOrderIN_IO_024"));
		customizeLoadingDelay(dispatch_order, 30);
		click(dispatch_order);
		switchWindow();
		implisitWait(8);
		Thread.sleep(2000);
		click(btn_closeGoToLinkPopup);
		Thread.sleep(2000);
		customizeLoadingDelay(btn_draftCommon, 30);
		click(btn_draftCommon);
		customizeLoadingDelay(btn_releaseCommon, 30);
		Thread.sleep(1000);
		click(btn_releaseCommon);
		customizeLoadingDelay(header_draft, 30);
		Thread.sleep(4000);
		String header1 = getText(header_draft);
		if (header1.equalsIgnoreCase("(Released)")) {
			trackCode = getText(lbl_creationsDocNo);
			flag_partial_released = true;
		} else {
			flag_partial_released = false;
		}

		if (flag_partial_released) {
			writeTestResults("Verify user can complete the internal order with multiple dispatch orders",
					"User should allow to complete the internal order with multiple dispatch orders",
					"User successfully complete the internal order with multiple dispatch orders", "pass");
		} else {
			writeTestResults("Verify user can complete the internal order with multiple dispatch orders",
					"User should allow to complete the internal order with multiple dispatch orders",
					"User doesn't complete the internal order with multiple dispatch orders", "fail");
		}
	}

	/* IN_IO_025_I_026_I_027 */
	public void fillInternelOrderForIN_IO_025_026_027() throws InvalidFormatException, IOException, Exception {
		actionVerificationLogin();
		openPage(readIWRegression01("IN_IO_002"));
		implisitWait(10);
		sendKeys(txt_tiltleInternelOrder, titleInternelOrder);

		click(btn_searchRequester);

		pressEnter(txt_RequesterSearch);

		doubleClick(lnk_searchResultRequester);

		employee = getAttribute(lnk_searchResultRequester, "title");

		/* Product add to the grid */
		int j = 0;
		for (int i = 1; i < 2; i++) {
			String str_i = String.valueOf(i);

			String lookup_path = btn_productLookupInterDeprtmentTransferOrderForLoop.replace("row", str_i)
					.replace("column", "10").replace("tblStockAdj", "tblInternalRequest");
			click(lookup_path);
			Thread.sleep(3000);
			customizeLoadingDelay(txt_productSearch, 15);
			sendKeys(txt_productSearch, batchSpecificProductWithDifferentLots);
			pressEnter(txt_productSearch);
			Thread.sleep(6000);
			;
			doubleClick(btn_resultProduct);

//		Selected products should be added to the Grid and OnHand Quantity [From][To], Default UOM should be loaded for selected product

			String txt_qty = txt_quantityInGrid.replace("row", str_i).replace("tblName", "tblInternalRequest");

			customizeLoadingDelay(txt_qty, 7);
			sendKeys(txt_qty, qtyInterDepartmentTransferOrder);
			Thread.sleep(3000);
			click(btn_addNewRow);
		}

		click(btn_deleteEmotyRowStockAdjustment.replace("tblStockAdj", "tblInternalRequest").replace("8", "2"));

		click(btn_detailTab);
		selectText(drop_warehouseInternelOrderEmployeeRequest, warehouseInternelOrderEmployeeRequest);
	}

	public void draftReleaseAndBatchCapture_IN_IO_025_I_026_027() throws Exception {
		click(btn_draftCommon);
		implisitWait(10);
		Thread.sleep(3000);
		click(btn_releaseCommon);
		implisitWait(12);
		Thread.sleep(3000);
		click(btn_goToPageLink);
		switchWindow();
		implisitWait(20);
		int row_position = 1;
		for (int i = 0; i < 1; i++) {
			String row_str = String.valueOf(row_position);
			Thread.sleep(3000);
			click(btn_availabilityCheckWidgetAfterrelesed.replace("tblStockAdj", "tblDispatchOrder").replace("row",
					row_str));
			Thread.sleep(2000);
			Actions action1 = new Actions(driver);
			WebElement we1 = driver.findElement(By.xpath(btn_avilabilytCheckSecondMenu));
			action1.moveToElement(we1).build().perform();
			click(btn_avilabilytCheckSecondMenu);
			Thread.sleep(5000);
			if (!isDisplayedQuickCheck(td_availableStockGstmWarehouseAfterRelesed)) {
				Thread.sleep(5000);
			}
			product_availability[i] = getText(td_availableStockGstmWarehouseAfterRelesed);
			System.out.println(product_availability[i]);
			click(btn_closeAvailabilityCheckPopup);
			row_position++;
		}
		click(btn_draftCommon);
		implisitWait(10);
		Thread.sleep(2000);
		click(brn_serielBatchCapture);
		Thread.sleep(1000);
		click(btn_captureItem1);
		Thread.sleep(1000);

		click(btn_capturePageDocIcon);

		click(chk_availableProductsTransferOrderOutboundShipment.replace("row", "1"));
		Thread.sleep(2000);
		JavascriptExecutor j7 = (JavascriptExecutor) driver;
		j7.executeScript("$(\"#attr-divSerialBatchNos\").jqxGrid('setcellvalue', 0, \"Qty\", \"5\");");

		click(btn_capturePageDocIcon);

		click(chk_availableProductsTransferOrderOutboundShipment.replace("row", "2"));
		click(btn_updateCapture);

		Thread.sleep(3000);
		click(btn_backButtonUpdateCapture);

		click(btn_releaseCommon);
		implisitWait(12);
		Thread.sleep(8000);
		String header1 = getText(header_draft);
		if (header1.equalsIgnoreCase("(Released)")) {
			trackCode = getText(lbl_creationsDocNo);
			flag_partial_released = true;
		} else {
			flag_partial_released = false;
		}
		if (flag_partial_released) {
			writeTestResults("Verify user can dispatch the batch qty which having different lots for same batch",
					"User should allow to dispatch the batch qty which having different lots for same batch",
					"User successfully dispatch the batch qty which having different lots for same batch", "pass");
		} else {
			writeTestResults("Verify user can dispatch the batch qty which having different lots for same batch",
					"User should allow to dispatch the batch qty which having different lots for same batch",
					"User doesn't dispatch the batch qty which having different lots for same batch", "fail");
		}

		int row_position1 = 1;
		for (int j = 0; j < 1; j++) {
			String row_str = String.valueOf(row_position1);
			sleepCusomized(btn_availabilityCheckWidgetAfterrelesed.replace("tblStockAdj", "tblDispatchOrder")
					.replace("row", row_str));
			click(btn_availabilityCheckWidgetAfterrelesed.replace("tblStockAdj", "tblDispatchOrder").replace("row",
					row_str));
			Thread.sleep(2000);
			Actions action1 = new Actions(driver);
			WebElement we1 = driver.findElement(By.xpath(btn_avilabilytCheckSecondMenu));
			action1.moveToElement(we1).build().perform();
			click(btn_avilabilytCheckSecondMenu);
			Thread.sleep(5000);
			product_availability_after[j] = getText(td_availableStockGstmWarehouseAfterRelesed);
			System.out.println(product_availability_after[j]);
			click(btn_closeAvailabilityCheckPopup);
			row_position1++;
		}
	}

	public void comapreTheStockThatTranferedFromTheWarehouse_IN_IO_025_I_026_I_027() throws Exception {
		boolean warehouse_check_flag = false;
		for (int j = 0; j < 1; j++) {
			String qty = product_availability[j].replaceAll(",", "");
			int qty_int = Integer.parseInt(qty);
			String after_qty = product_availability_after[j].replaceAll(",", "");
			try {
				int after_qty_int = Integer.parseInt(after_qty);
				int adding_qty = Integer.parseInt(qtyTransferOrder);
				if (after_qty_int == (qty_int - adding_qty)) {
					warehouse_check_flag = true;
				} else {
					warehouse_check_flag = false;
					break;
				}
			} catch (Exception e) {
				System.out.println(e);
			}

		}

		if (warehouse_check_flag) {
			writeTestResults("Verify stock was deducted which the qty of particular lots whcih having same batch",
					"Stock should be deducted which the qty of particular lots whcih having same batch",
					"Stock successfully deducted with the qty of particular lots which having same batch", "pass");
		} else {
			writeTestResults("Verify stock was deducted which the qty of particular lots whcih having same batch",
					"Stock should be deducted which the qty of particular lots whcih having same batch",
					"Stock doesn't deducted which the qty of particular lots whcih having same batch", "fail");
		}

	}

	public void validateCosting_IN_IO_025_I_026_I_027() throws Exception {
		click(btn_action);
		Thread.sleep(1000);
		click(btn_journelActionVerification);
		Thread.sleep(3000);

		String value = getText(lbl_docValueJournelEntryInternelDispatchOrder);
		if (value.equals(costForBatchProductsWithDifferentLots)) {
			writeTestResults("Verify the costing of the particular Lots",
					"Costing should be updated according to the particular lots whcih having same batch",
					"Costing successfully updated according to the particular lots whcih having same batch", "pass");
		} else {
			writeTestResults("Verify the costing of the particular Lots",
					"Costing should be updated according to the particular lots whcih having same batch",
					"Costing doesn't updated according to the particular lots whcih having same batch", "fail");
		}
	}

	/* IN_IO_028_029_030 */
	public void fillInternelOrderForIN_IO_028_029_030() throws InvalidFormatException, IOException, Exception {
		actionVerificationLogin();
		openPage(readIWRegression01("IN_IO_002"));
		implisitWait(10);
		sendKeys(txt_tiltleInternelOrder, titleInternelOrder);

		click(btn_searchRequester);

		pressEnter(txt_RequesterSearch);

		doubleClick(lnk_searchResultRequester);

		employee = getAttribute(lnk_searchResultRequester, "title");

		/* Product add to the grid */
		int j = 0;
		for (int i = 1; i < 2; i++) {
			String str_i = String.valueOf(i);

			String lookup_path = btn_productLookupInterDeprtmentTransferOrderForLoop.replace("row", str_i)
					.replace("column", "10").replace("tblStockAdj", "tblInternalRequest");
			click(lookup_path);
			Thread.sleep(3000);
			customizeLoadingDelay(txt_productSearch, 15);
			sendKeys(txt_productSearch, batchSpecificProductWithDifferentBatchesSameLot);
			pressEnter(txt_productSearch);
			Thread.sleep(6000);
			;
			doubleClick(btn_resultProduct);

//		Selected products should be added to the Grid and OnHand Quantity [From][To], Default UOM should be loaded for selected product

			String txt_qty = txt_quantityInGrid.replace("row", str_i).replace("tblName", "tblInternalRequest");

			customizeLoadingDelay(txt_qty, 7);
			sendKeys(txt_qty, qtyInterDepartmentTransferOrder);
			Thread.sleep(3000);
			click(btn_addNewRow);
		}

		click(btn_deleteEmotyRowStockAdjustment.replace("tblStockAdj", "tblInternalRequest").replace("8", "2"));

		click(btn_detailTab);
		selectText(drop_warehouseInternelOrderEmployeeRequest, warehouseInternelOrderEmployeeRequest);
	}

	public void draftReleaseAndBatchCapture_IN_IO_028_029_030() throws Exception {
		click(btn_draftCommon);
		implisitWait(10);
		Thread.sleep(3000);
		click(btn_releaseCommon);
		implisitWait(12);
		Thread.sleep(3000);
		click(btn_goToPageLink);
		switchWindow();
		implisitWait(20);
		int row_position = 1;
		for (int i = 0; i < 1; i++) {
			String row_str = String.valueOf(row_position);
			Thread.sleep(3000);
			click(btn_availabilityCheckWidgetAfterrelesed.replace("tblStockAdj", "tblDispatchOrder").replace("row",
					row_str));
			Thread.sleep(2000);
			Actions action1 = new Actions(driver);
			WebElement we1 = driver.findElement(By.xpath(btn_avilabilytCheckSecondMenu));
			action1.moveToElement(we1).build().perform();
			click(btn_avilabilytCheckSecondMenu);
			Thread.sleep(5000);
			if (!isDisplayedQuickCheck(td_availableStockGstmWarehouseAfterRelesed)) {
				Thread.sleep(5000);
			}
			product_availability[i] = getText(td_availableStockGstmWarehouseAfterRelesed);
			System.out.println(product_availability[i]);
			click(btn_closeAvailabilityCheckPopup);
			row_position++;
		}
		click(btn_draftCommon);
		implisitWait(10);
		Thread.sleep(2000);
		click(brn_serielBatchCapture);
		Thread.sleep(1000);
		click(btn_captureItem1);
		Thread.sleep(1000);

		click(btn_capturePageDocIcon);

		click(chk_availableProductsTransferOrderOutboundShipment.replace("row", "1"));
		Thread.sleep(2000);
		JavascriptExecutor j7 = (JavascriptExecutor) driver;
		j7.executeScript("$(\"#attr-divSerialBatchNos\").jqxGrid('setcellvalue', 0, \"Qty\", \"5\");");

		click(btn_capturePageDocIcon);

		click(chk_availableProductsTransferOrderOutboundShipment.replace("row", "2"));
		click(btn_updateCapture);

		Thread.sleep(3000);
		click(btn_backButtonUpdateCapture);

		click(btn_releaseCommon);
		implisitWait(12);
		Thread.sleep(8000);
		String header1 = getText(header_draft);
		if (header1.equalsIgnoreCase("(Released)")) {
			trackCode = getText(lbl_creationsDocNo);
			flag_partial_released = true;
		} else {
			flag_partial_released = false;
		}
		if (flag_partial_released) {
			writeTestResults("Verify user can dispatch the batch qty which having different batches for same lot",
					"User should allow to dispatch the batch qty which having different batches for same lot",
					"User successfully dispatch the batch qty which having different batches for same lot", "pass");
		} else {
			writeTestResults("Verify user can dispatch the batch qty which having different batches for same lot",
					"User should allow to dispatch the batch qty which having different batches for same lot",
					"User doesn't dispatch the batch qty which having different batches for same lot", "fail");
		}

		int row_position1 = 1;
		for (int j = 0; j < 1; j++) {
			String row_str = String.valueOf(row_position1);
			sleepCusomized(btn_availabilityCheckWidgetAfterrelesed.replace("tblStockAdj", "tblDispatchOrder")
					.replace("row", row_str));
			click(btn_availabilityCheckWidgetAfterrelesed.replace("tblStockAdj", "tblDispatchOrder").replace("row",
					row_str));
			Thread.sleep(2000);
			Actions action1 = new Actions(driver);
			WebElement we1 = driver.findElement(By.xpath(btn_avilabilytCheckSecondMenu));
			action1.moveToElement(we1).build().perform();
			click(btn_avilabilytCheckSecondMenu);
			Thread.sleep(5000);
			product_availability_after[j] = getText(td_availableStockGstmWarehouseAfterRelesed);
			System.out.println(product_availability_after[j]);
			click(btn_closeAvailabilityCheckPopup);
			row_position1++;
		}
	}

	public void comapreTheStockThatTranferedFromTheWarehouse_IN_IO_028_029_030() throws Exception {
		boolean warehouse_check_flag = false;
		for (int j = 0; j < 1; j++) {
			String qty = product_availability[j].replaceAll(",", "");
			int qty_int = Integer.parseInt(qty);
			String after_qty = product_availability_after[j].replaceAll(",", "");
			try {
				int after_qty_int = Integer.parseInt(after_qty);
				int adding_qty = Integer.parseInt(qtyTransferOrder);
				if (after_qty_int == (qty_int - adding_qty)) {
					warehouse_check_flag = true;
				} else {
					warehouse_check_flag = false;
					break;
				}
			} catch (Exception e) {
				System.out.println(e);
			}

		}

		if (warehouse_check_flag) {
			writeTestResults("Verify stock was deducted which the qty of particular lot whcih having different batches",
					"Stock should be deducted which the qty of particular lot whcih having different batches",
					"Stock successfully deducted with the qty of particular lots which having same batch", "pass");
		} else {
			writeTestResults("Verify stock was deducted which the qty of particular lot whcih having different batches",
					"Stock should be deducted which the qty of particular lot whcih having different batches",
					"Stock doesn't deducted which the qty of particular lot whcih having different batches", "fail");
		}

	}

	public void validateCosting_IN_IO_028_029_030() throws Exception {
		click(btn_action);
		Thread.sleep(1000);
		click(btn_journelActionVerification);
		Thread.sleep(4000);

		String value = getText(lbl_docValueJournelEntryInternelDispatchOrder);
		if (value.equals(costForBbatchSpecificProductWithDifferentBatchesSameLot)) {
			writeTestResults("Verify the costing of the particular Lots",
					"Costing should be updated according to the particular lot whcih having different batches",
					"Costing successfully updated according to the particular lot whcih having different batches",
					"pass");
		} else {
			writeTestResults("Verify the costing of the particular Lots",
					"Costing should be updated according to the particular lot whcih having different batches",
					"Costing doesn't updated according to the particular lot whcih having different batches", "fail");
		}
	}

	/* IN_IO_031_032_033 */
	public void stockAdjustment_IN_IO_031_032_033() throws Exception {
		customizeLoadingDelay(navigation_pane, 20);
		click(navigation_pane);
		customizeLoadingDelay(inventory_module, 20);
		click(inventory_module);
		customizeLoadingDelay(btn_stockAdjustment, 20);
		click(btn_stockAdjustment);
		customizeLoadingDelay(btn_newStockAdjustment, 20);
		click(btn_newStockAdjustment);
		customizeLoadingDelay(txt_descStockAdj, 20);
		sendKeys(txt_descStockAdj, descStockAdj);

		selectText(dropdown_warehouseStockAdj, warehouseStockAdj);

		/* Product add to the grid */
		int j = 0;
		for (int i = 1; i < 4; i++) {
			String str_i = String.valueOf(i);
			click(btn_productLookupStockAdjForLoop.replace("row", str_i));
			Thread.sleep(3000);
			String product = "";

			if (i == 1) {
				product = "LotFifo_IN_IO_031";
			} else if (i == 2) {
				product = "BatchFifo_IN_IO_031";
			} else if (i == 3) {
				product = "SerielFifo_IN_IO_031";
			} else {
				System.out.println("Product type cannt identified.........");
			}
			handeledSendKeys(txt_productSearch, readTestCreation(product));
			pressEnter(txt_productSearch);
			String replacement = readTestCreation(product);
			String product_path = result_cpmmonProduct.replace("product", replacement);
			customizeLoadingDelay(product_path, 15);
			doubleClick(product_path);
			String txt_qty = txt_qtyProductGridStockAdjustment.replace("row", str_i);
			String txt_cost = txt_costProductGridStockAdjustment.replace("row", str_i);

			sendKeys(txt_qty, stockAdjustmentQuantiyt_IN_IO_031_032_033);

			sendKeys(txt_cost, productCost1);

			sleepCusomized(btn_addNewRow);
			click(btn_addNewRow);
		}

		click(btn_deleteEmotyRowStockAdjustment.replace("8", "4"));

		customizeLoadingDelay(btn_draftActionVerification, 30);
		click(btn_draftActionVerification);
		customizeLoadingDelay(brn_serielBatchCapture, 30);
		click(brn_serielBatchCapture);

		/* Serial Batch FIFO */
		String item_xpath;
		Thread.sleep(3000);
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "2");
		customizeLoadingDelay(item_xpath, 20);
		click(item_xpath);

		sendKeys(txt_batchNumberCaptureILOOutboundShipment,
				currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_lotNumberStockAdjustment,
				genProductId() + currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_expiryDateStockAdjustmentProductCapture);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_lotNumberStockAdjustment);
		Thread.sleep(1000);
		customizeLoadingDelay(btn_updateCapture, 50);
		click(btn_updateCapture);

		/* Serial FIFO */
		Thread.sleep(2000);
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "3");
		Actions action1 = new Actions(driver);
		WebElement we1 = driver.findElement(By.xpath(item_xpath));
		action1.moveToElement(we1).build().perform();

		click(item_xpath);
		sleepCusomized(item_xpath);

		click(chk_productCaptureRange);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$('#attrSlide-txtSerialCount').val(5)");

		String lot4111 = currentTimeAndDate().replaceAll("/", "").replaceAll(":", "");
		String final1_lot_script11 = "$('#attrSlide-txtSerialFrom').val(\"" + lot4111 + "\")";
		jse.executeScript(final1_lot_script11);

		sendKeys(txt_searielLotNo, genProductId() + currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));

		click(txt_dateSerielProductProductCapturePage);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_dateSerielProductProductCapturePage);
		Thread.sleep(1000);
		customizeLoadingDelay(btn_updateCapture, 50);
		click(btn_updateCapture);
		Thread.sleep(5500);
		customizeLoadingDelay(btn_productCaptureBackButton, 20);
		click(btn_productCaptureBackButton);
		Thread.sleep(1000);
		click(btn_relese);
		customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 40);

	}

	public void fillInternelOrderForIN_IO_031_032_033() throws InvalidFormatException, IOException, Exception {
		actionVerificationLogin();
		stockAdjustment_IN_IO_031_032_033();
		stockAdjustment_IN_IO_031_032_033();
		openPage(readIWRegression01("IN_IO_002"));
		customizeLoadingDelay(txt_tiltleInternelOrder, 40);
		sendKeys(txt_tiltleInternelOrder, titleInternelOrder);

		click(btn_searchRequester);

		pressEnter(txt_RequesterSearch);

		doubleClick(lnk_searchResultRequester);

		employee = getAttribute(lnk_searchResultRequester, "title");

		/* Product add to the grid */
		for (int i = 1; i < 4; i++) {
			String product = "";

			if (i == 1) {
				product = "LotFifo_IN_IO_031";
			} else if (i == 2) {
				product = "BatchFifo_IN_IO_031";
			} else if (i == 3) {
				product = "SerielFifo_IN_IO_031";
			} else if (i == 4) {
				product = "";
			} else if (i == 5) {
				product = "";
			} else if (i == 6) {
				product = "";
			} else if (i == 7) {
				product = "";
			} else {
				System.out.println("Product type cannt identified.........");
			}
			String str_i = String.valueOf(i);

			String lookup_path = btn_productLookupInterDeprtmentTransferOrderForLoop.replace("row", str_i)
					.replace("column", "10").replace("tblStockAdj", "tblInternalRequest");
			click(lookup_path);
			handeledSendKeys(txt_productSearch, readTestCreation(product));
			pressEnter(txt_productSearch);
			Thread.sleep(6000);
			doubleClick(btn_resultProduct);

//		Selected products should be added to the Grid and OnHand Quantity [From][To], Default UOM should be loaded for selected product

			String txt_qty = txt_quantityInGrid.replace("row", str_i).replace("tblName", "tblInternalRequest");

			customizeLoadingDelay(txt_qty, 7);
			sendKeys(txt_qty, qtyInterDepartmentTransferOrder);
			Thread.sleep(3000);
			click(btn_addNewRow);
		}

		click(btn_deleteEmotyRowStockAdjustment.replace("tblStockAdj", "tblInternalRequest").replace("8", "4"));

		click(btn_detailTab);
		selectText(drop_warehouseInternelOrderEmployeeRequest, warehouseInternelOrderEmployeeRequest);
		click(btn_summaryTab);
		int row_position = 1;
		for (int i = 0; i < 3; i++) {
			String row_str = String.valueOf(row_position);
			customizeLoadingDelay(btn_availabilityCheckWidgetAfterrelesed.replace("tblStockAdj", "tblInternalRequest")
					.replace("row", row_str).replace("5", "7"), 20);
			click(btn_availabilityCheckWidgetAfterrelesed.replace("tblStockAdj", "tblInternalRequest")
					.replace("row", row_str).replace("5", "7"));
			Thread.sleep(2000);
			Actions action1 = new Actions(driver);
			WebElement we1 = driver.findElement(By.xpath(btn_avilabilytCheckSecondMenu));
			action1.moveToElement(we1).build().perform();
			click(btn_avilabilytCheckSecondMenu);
			Thread.sleep(5000);
			if (!isDisplayedQuickCheck(td_availableStockGstmWarehouseAfterRelesed)) {
				Thread.sleep(5000);
			}
			product_availability[i] = getText(td_availableStockGstmWarehouseAfterRelesed);
			System.out.println(product_availability[i]);
			click(btn_closeAvailabilityCheckPopup);
			row_position++;
		}
	}

	public void draftReleaseAndBatchCapture_IN_IO_031_032_033() throws Exception {
		click(btn_draftCommon);
		implisitWait(10);
		Thread.sleep(3000);
		click(btn_releaseCommon);
		implisitWait(12);
		Thread.sleep(3000);
		click(btn_goToPageLink);
		switchWindow();
		implisitWait(20);
		click(btn_draftCommon);
		implisitWait(10);
		Thread.sleep(2000);
		click(btn_releaseCommon);
		customizeLoadingDelay(lbl_relesedConfirmation, 60);
		if (isDisplayed(lbl_relesedConfirmation)) {
			writeTestResults("Verify user can dispatch the qty with multiple lots as FIFO",
					"User should allow to dispatch the qty with multiple lots as FIFO",
					"User successfully allow to dispatch the qty with multiple lots as FIFO", "pass");
		} else {
			writeTestResults("Verify user can dispatch the qty with multiple lots as FIFO",
					"User should allow to dispatch the qty with multiple lots as FIFO",
					"User doesn't allow to dispatch the qty with multiple lots as FIFO", "fail");
		}

		int row_position1 = 1;
		for (int j = 0; j < 3; j++) {
			String row_str = String.valueOf(row_position1);
			sleepCusomized(btn_availabilityCheckWidgetAfterrelesed.replace("tblStockAdj", "tblDispatchOrder")
					.replace("row", row_str));
			click(btn_availabilityCheckWidgetAfterrelesed.replace("tblStockAdj", "tblDispatchOrder").replace("row",
					row_str));
			Thread.sleep(2000);
			Actions action1 = new Actions(driver);
			WebElement we1 = driver.findElement(By.xpath(btn_avilabilytCheckSecondMenu));
			action1.moveToElement(we1).build().perform();
			click(btn_avilabilytCheckSecondMenu);
			Thread.sleep(5000);
			product_availability_after[j] = getText(td_availableStockGstmWarehouseAfterRelesed);
			System.out.println(product_availability_after[j]);
			click(btn_closeAvailabilityCheckPopup);
			row_position1++;
		}
	}

	public void comapreTheStockThatTranferedFromTheWarehouse_IN_IO_031_032_033() throws Exception {
		boolean warehouse_check_flag = false;
		for (int j = 0; j < 3; j++) {
			String qty = product_availability[j].replaceAll(",", "");
			int qty_int = Integer.parseInt(qty);
			String after_qty = product_availability_after[j].replaceAll(",", "");
			try {
				int after_qty_int = Integer.parseInt(after_qty);
				int adding_qty = Integer.parseInt(qtyTransferOrder);
				if (after_qty_int == (qty_int - adding_qty)) {
					warehouse_check_flag = true;
				} else {
					warehouse_check_flag = false;
					break;
				}
			} catch (Exception e) {
				System.out.println(e);
			}

		}

		if (warehouse_check_flag) {
			writeTestResults("Verify stock was deducted with the relevant lots as FIFO",
					"Stock should be deducted with the relevant lots as FIFO",
					"Stock successfully deducted with the relevant lots as FIFO", "pass");
		} else {
			writeTestResults("Verify stock was deducted with the relevant lots as FIFO",
					"Stock should be deducted with the relevant lots as FIFO",
					"Stock doesn't deducted with the relevant lots as FIFO", "fail");
		}

	}

	public void validateCosting_IN_IO_031_032_033() throws Exception {
		click(btn_action);
		Thread.sleep(1000);
		click(btn_journelActionVerification);
		Thread.sleep(4000);
		customizeLoadingDelay(lbl_docValueJournelEntryInternelDispatchOrder, 20);
		String value = getText(lbl_docValueJournelEntryInternelDispatchOrder);
		if (value.equals(costOfProductForIN_IO_031_032_033)) {
			writeTestResults("Verify costing was updated according to the lots which outbound as FIFO",
					"Costing should be updated according to the lots which outbound as FIFO",
					"Costing successfully updated according to the lots which outbound as FIFO", "pass");
		} else {
			writeTestResults("Verify costing was updated according to the lots which outbound as FIFO",
					"Costing should be updated according to the lots which outbound as FIFO",
					"Costing doesn't updated according to the lots which outbound as FIFO", "fail");
		}
	}

	/* IN_IO_034 */
	public void fillAndDraftReleaseInternelOrderGreaterThanOHQuantity() throws Exception {
		actionVerificationLogin();
		click(navigation_pane);
		Thread.sleep(500);
		click(btn_adminModule);
		Thread.sleep(500);
		click(btn_journeyConfiguration);
		implisitWait(8);
		Thread.sleep(500);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript(btn_journeyConfigInternelOrderEmployeeRequest);

		if (!isSelected(chk_stockValidate)) {
			click(chk_stockValidate);
		}
		selectText(drop_stockReservationMethod, stockReservationMethod);

		click(btnn_updateEmployeeRequestJourneyConfiguration);
		Thread.sleep(1000);
		openPage(readIWRegression01("IN_IO_002"));
		implisitWait(10);
		sendKeys(txt_tiltleInternelOrder, titleInternelOrder);

		click(btn_searchRequester);

		pressEnter(txt_RequesterSearch);

		doubleClick(lnk_searchResultRequester);

		employee = getAttribute(lnk_searchResultRequester, "title");

		/* Product add to the grid */
		int j = 0;
		for (int i = 1; i < 2; i++) {
			String str_i = String.valueOf(i);

			String lookup_path = btn_productLookupInterDeprtmentTransferOrderForLoop.replace("row", str_i)
					.replace("column", "10").replace("tblStockAdj", "tblInternalRequest");
			click(lookup_path);
			Thread.sleep(3000);
			customizeLoadingDelay(txt_productSearch, 15);
			sendKeys(txt_productSearch, ohVerifyProduct);
			pressEnter(txt_productSearch);
			Thread.sleep(6000);
			;
			doubleClick(btn_resultProduct);

//		Selected products should be added to the Grid and OnHand Quantity [From][To], Default UOM should be loaded for selected product

			String txt_qty = txt_quantityInGrid.replace("row", str_i).replace("tblName", "tblInternalRequest");

			customizeLoadingDelay(txt_qty, 7);
			sendKeys(txt_qty, qtyInterDepartmentTransferOrder);
			Thread.sleep(3000);
			click(btn_addNewRow);
		}

		click(btn_deleteEmotyRowStockAdjustment.replace("tblStockAdj", "tblInternalRequest").replace("8", "2"));

		click(btn_detailTab);
		selectText(drop_warehouseInternelOrderEmployeeRequest, warehouseInternelOrderEmployeeRequest);
		click(btn_draftCommon);
		implisitWait(10);
		Thread.sleep(3000);
		click(btn_releaseCommon);
		implisitWait(10);
		Thread.sleep(3000);
		String message = getText(para_validator);
		System.out.println();
		if (message.contains("ERROR")) {
			writeTestResults("Verify user couldn't release the document and validation messages is pop-up",
					"User should not be able to release the document and validation messages should pop-up",
					"User couldn't release the document and validation messages successfully pop-up", "pass");
		} else {
			writeTestResults("Verify user couldn't release the document and validation messages is pop-up",
					"User should not be able to release the document and validation messages should pop-up",
					"User can release the document or validation messages doesn't pop-up", "fail");
		}
	}

	/* IN_IO_035 */
	public void fillAndDraftReleaseInternelOrderGreaterThanOHQuantity_IN_IO_035() throws Exception {
		actionVerificationLogin();
		click(navigation_pane);
		Thread.sleep(500);
		click(btn_adminModule);
		Thread.sleep(500);
		click(btn_journeyConfiguration);
		implisitWait(8);
		Thread.sleep(500);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript(btn_journeyConfigInternelOrderEmployeeRequest);

		if (isSelected(chk_stockValidate)) {
			click(chk_stockValidate);
		}
		selectText(drop_stockReservationMethod, stockReservationMethod);

		click(btnn_updateEmployeeRequestJourneyConfiguration);
		Thread.sleep(1000);
		openPage(readIWRegression01("IN_IO_002"));
		implisitWait(10);
		sendKeys(txt_tiltleInternelOrder, titleInternelOrder);

		click(btn_searchRequester);

		pressEnter(txt_RequesterSearch);

		doubleClick(lnk_searchResultRequester);

		employee = getAttribute(lnk_searchResultRequester, "title");

		/* Product add to the grid */
		int j = 0;
		for (int i = 1; i < 2; i++) {
			String str_i = String.valueOf(i);

			String lookup_path = btn_productLookupInterDeprtmentTransferOrderForLoop.replace("row", str_i)
					.replace("column", "10").replace("tblStockAdj", "tblInternalRequest");
			click(lookup_path);
			Thread.sleep(3000);
			customizeLoadingDelay(txt_productSearch, 15);
			sendKeys(txt_productSearch, ohVerifyProduct);
			pressEnter(txt_productSearch);
			Thread.sleep(6000);
			;
			doubleClick(btn_resultProduct);

//		Selected products should be added to the Grid and OnHand Quantity [From][To], Default UOM should be loaded for selected product

			String txt_qty = txt_quantityInGrid.replace("row", str_i).replace("tblName", "tblInternalRequest");

			customizeLoadingDelay(txt_qty, 7);
			sendKeys(txt_qty, qtyInterDepartmentTransferOrder);
			Thread.sleep(3000);
			click(btn_addNewRow);
		}

		click(btn_deleteEmotyRowStockAdjustment.replace("tblStockAdj", "tblInternalRequest").replace("8", "2"));

		click(btn_detailTab);
		selectText(drop_warehouseInternelOrderEmployeeRequest, warehouseInternelOrderEmployeeRequest);
		click(btn_draftCommon);
		implisitWait(10);
		Thread.sleep(3000);
		click(btn_releaseCommon);
		implisitWait(12);
		pageRefersh();
		implisitWait(10);
		Thread.sleep(5000);
		String header1 = getText(header_draft);
		if (!header1.equalsIgnoreCase("(Released)")) {
			Thread.sleep(5000);
		}
		if (header1.equalsIgnoreCase("(Released)")) {
			trackCode = getText(lbl_creationsDocNo);
			flag_partial_released = true;
		} else {
			flag_partial_released = false;
		}
		if (flag_partial_released) {
			writeTestResults("Verify user can release the document", "User should be able to release the document",
					"User successfully release the document", "pass");
		} else {
			writeTestResults("Verify user can release the document", "User should be able to release the document",
					"User doesn't release the document", "fail");
		}

	}

	/* IN_IO_036 */
	public void journeyConfigureFor_IN_IO_036() throws Exception {
		actionVerificationLogin();
		click(navigation_pane);
		Thread.sleep(500);
		click(btn_adminModule);
		Thread.sleep(500);
		click(btn_journeyConfiguration);
		implisitWait(8);
		Thread.sleep(500);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript(btn_journeyConfigInternelOrderEmployeeRequest);

		if (!isSelected(chk_stockValidate)) {
			click(chk_stockValidate);
		}

		selectText(drop_stockReservationMethod, stockReservationMethod);
		click(btnn_updateEmployeeRequestJourneyConfiguration);
		Thread.sleep(1000);
	}

	public void fillInternelOrder_IN_IO_036() throws InvalidFormatException, IOException, Exception {
		click(navigation_pane);
		click(inventory_module);
		Thread.sleep(1000);
		click(btn_internelOrder);
		click(btn_newInternelOrder);
		implisitWait(8);
		click(btn_employeeRequestJourneyInternelOrder);
		implisitWait(10);
		sendKeys(txt_tiltleInternelOrder, titleInternelOrder);

		click(btn_searchRequester);

		pressEnter(txt_RequesterSearch);

		doubleClick(lnk_searchResultRequester);

		employee = getAttribute(lnk_searchResultRequester, "title");

		/* Product add to the grid */
		int j = 0;
		for (int i = 1; i < 2; i++) {
			String str_i = String.valueOf(i);

			String lookup_path = btn_productLookupInterDeprtmentTransferOrderForLoop.replace("row", str_i)
					.replace("column", "10").replace("tblStockAdj", "tblInternalRequest");
			click(lookup_path);
			Thread.sleep(3000);
			customizeLoadingDelay(txt_productSearch, 15);
			sendKeys(txt_productSearch, reservationVerifyProduct);
			pressEnter(txt_productSearch);
			Thread.sleep(6000);
			;
			doubleClick(btn_resultProduct);

//		Selected products should be added to the Grid and OnHand Quantity [From][To], Default UOM should be loaded for selected product

			String txt_qty = txt_quantityInGrid.replace("row", str_i).replace("tblName", "tblInternalRequest");

			customizeLoadingDelay(txt_qty, 7);
			sendKeys(txt_qty, qtyInterDepartmentTransferOrder);
			Thread.sleep(3000);
			click(btn_addNewRow);
		}

		click(btn_deleteEmotyRowStockAdjustment.replace("tblStockAdj", "tblInternalRequest").replace("8", "2"));

		click(btn_detailTab);
		selectText(drop_warehouseInternelOrderEmployeeRequest, warehouseInternelOrderEmployeeRequest);
	}

	public void manuelReserveStockProcessAndVerifyStockReservation_IN_IO_036() throws Exception {
		click(tab_summaryCommon);
		Thread.sleep(3000);
		click(btn_advanceWidgetInternelOrder);
		Thread.sleep(4000);
		sendKeys(txt_reservedQuantityAdvanceMenu, productReserveQuantity);
		Thread.sleep(3000);
		click(btn_aplyAdvanceMenuInternelOrder);
		Thread.sleep(2000);
		click(btn_draftCommon);
		implisitWait(10);
		Thread.sleep(3000);
		click(btn_releaseCommon);
		Thread.sleep(700);
		pageRefersh();
		implisitWait(8);
		Thread.sleep(2000);
		trackCode = getText(lbl_creationsDocNo);
		writeRegression01("QuantityReserveDocNumber_IN_IO_036", trackCode, 11);
		click(navigation_pane);
		click(inventory_module);
		Thread.sleep(1200);
		click(btn_allocationManger);
		sendKeys(txt_docSearchAllocationManager, readIWRegression01("QuantityReserveDocNumber_IN_IO_036"));
		click(btn_documentSearchAllocationManager);
		Thread.sleep(2500);
		String path = lbl_reserverdQuantityAllocationManager
				.replace("doc_num", readIWRegression01("QuantityReserveDocNumber_IN_IO_036"))
				.replace("product", reservationVerifyProduct);
		String reserve_qty = getText(path);
		reserve_qty = reserve_qty.replace(".00", "");

		if (reserve_qty.equals(productReserveQuantity)) {
			writeTestResults("Verify entered Qty was shown reserved in Allocation Manager product wise",
					"Entered Qty should be shown reserved in Allocation Manager product wise",
					"Entered Qty successfully shown reserved in Allocation Manager product wise", "pass");
		} else {
			writeTestResults("Verify entered Qty was shown reserved in Allocation Manager product wise",
					"Entered Qty should be shown reserved in Allocation Manager product wise",
					"Entered Qty doesn't shown reserved in Allocation Manager product wise", "fail");
		}
	}

	/* common */
	public void customizeLoadingDelay(String xpath, int seconds) {
		WebDriverWait wait = new WebDriverWait(driver, seconds);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
	}

	public void customizeLoadingDelayExternalUse(String xpath, int seconds) {
		WebDriverWait wait = new WebDriverWait(driver, seconds);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
	}

	public void customizeLoadingDelayUntillClickable(String xpath, int seconds) {
		WebDriverWait wait = new WebDriverWait(driver, seconds);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
	}

	/* Create products for production smoke */
	public void createProductionTypeProductSmokeProduction() throws Exception {
		openPage(siteURL);
		Thread.sleep(2000);
		explicitWait(txt_username, 40);
		sendKeys(txt_username, gbvBileetaUsername);
		Thread.sleep(1000);
		sendKeys(txt_password, gbvBileetaPassword);
		Thread.sleep(2000);
		click(btn_login);
		explicitWait(navigation_pane, 40);
		if (isDisplayedQuickCheck(div_loginVerification)) {
			openPage(siteURL);
			Thread.sleep(2000);
			explicitWait(txt_username, 40);
			sendKeys(txt_username, gbvBileetaUsername);
			Thread.sleep(1000);
			sendKeys(txt_password, gbvBileetaPassword);
			Thread.sleep(2000);
			click(btn_login);
			explicitWait(navigation_pane, 40);
		}

		sleepCusomized(navigation_pane);

		click(navigation_pane);
		sleepCusomized(inventory_module);

		click(inventory_module);
		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		customizeLoadingDelay(btn_new_product, 8);

		Thread.sleep(1500);
		click(btn_new_product);

		// product code
		String product_code_batch_fifo = (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")
				+ "-BATCH-FIFO-PT");
		sendKeys(txt_product_code, product_code_batch_fifo);

		// product description
		sendKeys(txt_product_desc, product_code_batch_fifo);

		// product group
		selectText(dropdown_product_group, productGroupSmokeProduction);

		sendKeys(txt_basePriceProductInformation, quantityTen);

		click(chk_allowDecimal);

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturer);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", menufacturer));

		sendKeys(txt_genaricNameProductInformation, product_code_batch_fifo);

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);

		click(tab_detail);
		customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);

		Thread.sleep(3000);
		pageScrollDown();
		pageScrollUpToTop();
		selectText(dropdown_outbound_costing_method, "FIFO");
		Thread.sleep(1500);

		selectText(drop_mnufacturingTypeProductInformation, menufacturerTypeProduction);
		Thread.sleep(1500);
		String selectedMenufacType = getSelectedOptionInDropdown(drop_mnufacturingTypeProductInformation);
		if (!selectedMenufacType.equals(menufacturerTypeProduction)) {
			writeTestResults("Verify menufacturing type select as 'Production'",
					"Production type should be 'Production'", "Production type doesn't select as 'Production'", "fail");
		}

		click(checkbox_batch_product);

		click(btn_draft);
		customizeLoadingDelay(btn_releese2, 40);

		click(btn_releese2);
		Thread.sleep(1000);
		customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 40);

		products_map.put("BatchFifo", product_code_batch_fifo);
		products.add(0, products_map);
		writeTestCreations("BatchFifoProductionSmokeProductionType", products.get(0).get("BatchFifo"), 21);
	}

	public void createBatchFifoProductOneSmokeProduction() throws Exception {
		sleepCusomized(navigation_pane);

		click(navigation_pane);
		sleepCusomized(inventory_module);

		click(inventory_module);
		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		customizeLoadingDelay(btn_new_product, 8);

		Thread.sleep(1500);
		click(btn_new_product);

		// product code
		String product_code_batch_fifo = (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "-BATCH-FIFO");
		sendKeys(txt_product_code, product_code_batch_fifo);

		// product description
		sendKeys(txt_product_desc, productDesription);

		// product group
		selectText(dropdown_product_group, productionGroupBatchProductCreation);

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturer);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", menufacturer));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);
		// product width
		customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		click(tab_detail);
		customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);

		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		click(chk_batchProductDispatchOnlyBatchProduct);
		Thread.sleep(3000);
		pageScrollDown();
		pageScrollUpToTop();
		selectText(dropdown_outbound_costing_method, "FIFO");
		Thread.sleep(1500);
		click(checkbox_without_costing);
		click(chk_allocationBatchProduct);

		click(checkbox_validate_expiery);
		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		if (!isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		sendKeys(txt_minPriceTextProductCreation, minPriceCoommnonCreateProduct);
		Thread.sleep(1000);
		sendKeys(txt_maxPriceTextProductCreation, maxPriceCoommnonCreateProduct);
		Thread.sleep(1000);

		Thread.sleep(1000);
		if (!isSelected(checkbox_validate_expiery_date)) {
			click(checkbox_validate_expiery_date);
		}
		Thread.sleep(1000);

		click(btn_draft);
		customizeLoadingDelay(btn_releese2, 40);

		click(btn_releese2);
		Thread.sleep(1000);
		customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 40);

		products_map.put("BatchFifo", product_code_batch_fifo);
		products.add(0, products_map);
		writeTestCreations("BatchFifoProductionSmokeOne", products.get(0).get("BatchFifo"), 22);
	}

	public void createBatchFifoProductTwoSmokeProduction() throws Exception {
		sleepCusomized(navigation_pane);

		click(navigation_pane);
		sleepCusomized(inventory_module);

		click(inventory_module);
		customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		customizeLoadingDelay(btn_new_product, 8);

		Thread.sleep(1500);
		click(btn_new_product);

		// product code
		String product_code_batch_fifo = (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "-BATCH-FIFO");
		sendKeys(txt_product_code, product_code_batch_fifo);

		// product description
		sendKeys(txt_product_desc, productDesription);

		// product group
		selectText(dropdown_product_group, productionGroupBatchProductCreation);

		click(chk_allowDecimal);

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturer);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", menufacturer));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);
		// product width
		customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		click(tab_detail);
		customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);

		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		click(chk_batchProductDispatchOnlyBatchProduct);
		Thread.sleep(3000);
		pageScrollDown();
		pageScrollUpToTop();
		selectText(dropdown_outbound_costing_method, "FIFO");
		Thread.sleep(1500);
		click(checkbox_without_costing);
		click(chk_allocationBatchProduct);

		click(checkbox_validate_expiery);
		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		if (!isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		sendKeys(txt_minPriceTextProductCreation, minPriceCoommnonCreateProduct);
		Thread.sleep(1000);
		sendKeys(txt_maxPriceTextProductCreation, maxPriceCoommnonCreateProduct);
		Thread.sleep(1000);

		Thread.sleep(1000);
		if (!isSelected(checkbox_validate_expiery_date)) {
			click(checkbox_validate_expiery_date);
		}
		Thread.sleep(1000);

		click(btn_draft);
		customizeLoadingDelay(btn_releese2, 40);

		click(btn_releese2);
		Thread.sleep(1000);
		customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 40);

		products_map.put("BatchFifo", product_code_batch_fifo);
		products.add(0, products_map);
		writeTestCreations("BatchFifoProductionSmokeTwo", products.get(0).get("BatchFifo"), 23);
	}

	public void stockAdjustmentProductionSmoke() throws Exception {
		customizeLoadingDelay(navigation_pane, 50);
		Thread.sleep(3000);
		customizeLoadingDelay(navigation_pane, 20);
		click(navigation_pane);
		customizeLoadingDelay(inventory_module, 40);
		click(inventory_module);
		customizeLoadingDelay(btn_stockAdjustment, 40);
		click(btn_stockAdjustment);
		customizeLoadingDelay(btn_newStockAdjustment, 40);
		click(btn_newStockAdjustment);

		sendKeys(txt_descStockAdj, descStockAdj);

		selectText(dropdown_warehouseStockAdj, inputWarehouseSmokeProduction);

		/* Product add to the grid */
		for (int i = 1; i < 3; i++) {
			String str_i = String.valueOf(i);
			click(btn_productLookupStockAdjForLoop.replace("row", str_i));
			Thread.sleep(3000);
			customizeLoadingDelay(txt_productSearch, 15);

			String product = "";

			if (i == 1) {
				product = "BatchFifoProductionSmokeOne";
			} else if (i == 2) {
				product = "BatchFifoProductionSmokeTwo";
			} else {
				System.out.println("Product type cannt identified.........");
			}

			handeledSendKeys(txt_productSearch, readTestCreation(product));
			pressEnter(txt_productSearch);
			Thread.sleep(1500);
			String product_path = result_cpmmonProduct.replace("product", readTestCreation(product));
			customizeLoadingDelay(product_path, 30);
			doubleClick(product_path);
			Thread.sleep(1500);
			String txt_qty = txt_qtyProductGridStockAdjustment.replace("row", str_i);
			String txt_cost = txt_costProductGridStockAdjustment.replace("row", str_i);

			sendKeys(txt_qty, productQuantity1);

			sendKeys(txt_cost, productCost1);
			sleepCusomized(btn_avilabilityCheckWidget);

			sleepCusomized(btn_addNewRow);
			click(btn_addNewRow);
		}

		click(btn_deleteLastRowOfTheGrid);

		click(btn_draftActionVerification);
		customizeLoadingDelay(brn_serielBatchCapture, 40);
		click(brn_serielBatchCapture);

		/* Product 01 */
		customizeLoadingDelay(btn_serielBatchCaptureProduct1ILOOutboundShipment, 40);
		Thread.sleep(3000);
		String item_xpath;
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "1");
		click(item_xpath);
		sendKeys(txt_batchNumberCaptureILOOutboundShipment, genBatchFifoSerielNumber(200));
		sendKeys(txt_lotNumberStockAdjustment,
				genProductId() + currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_expiryDateStockAdjustmentProductCapture);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_lotNumberStockAdjustment);
		Thread.sleep(1000);
		customizeLoadingDelay(btn_updateCapture, 50);
		click(btn_updateCapture);
		customizeLoadingDelay((item_xpath + span_afterCaptureSerialGreen), 30);

		/* Product 02 */
		Thread.sleep(3000);
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "2");
		click(item_xpath);

		sendKeys(txt_batchNumberCaptureILOOutboundShipment, genBatchFifoSerielNumber(200));
		sendKeys(txt_lotNumberStockAdjustment,
				genProductId() + currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_expiryDateStockAdjustmentProductCapture);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_lotNumberStockAdjustment);
		Thread.sleep(1000);
		customizeLoadingDelay(btn_updateCapture, 50);
		click(btn_updateCapture);
		customizeLoadingDelay((item_xpath + span_afterCaptureSerialGreen), 30);

		Thread.sleep(3000);
		click(btn_productCaptureBackButton);
		Thread.sleep(1000);
		click(btn_relese);
		customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 60);
		if (isDisplayed(lbl_relesedConfirmation.replace("(Released)", "( Released )"))) {
			writeTestResults("Verify that user can release Stock Adjustment",
					"User shpuld be able to release Stock Adjustment", "User successfully release Stock Adjustment",
					"pass");
		} else {
			writeTestResults("Verify that user can release Stock Adjustment",
					"User shpuld be able to release Stock Adjustment", "User doen't release Stock Adjustment", "fail");

		}
	}

	// ============================================RegressionAruna======================================================================
	// IN_SA_001
	public void VerifyThatNavigationToStockAdjustmentNewpage() throws Exception {

		click(inventory_module);
		Thread.sleep(3000);
		click(btn_stockAdjustment);
		Thread.sleep(3000);
		click(btn_newStockAdjustment);
		Thread.sleep(5000);
		if (getText(txt_pageHeaderLableA).contentEquals("Stock Adjustment - New")) {
			writeTestResults("Verify that navigation to Stock Adjustment- New page",
					"'User can navigate to Stock Adjustment- New page",
					"User navigate to Stock Adjustment- New page Succesfully", "pass");
		} else {
			writeTestResults("Verify that navigation to Stock Adjustment- New page",
					"'User can navigate to Stock Adjustment- New page",
					"User can't navigate to Stock Adjustment- New page", "fail");
		}

	}

	// IN_SA_002
	public void VerifyThatUserCannotNavigateToStockAdjustment() throws Exception {

		click(btn_AdministrationA);
		click(btn_JourneyConfigurationA);
		Thread.sleep(3000);
		JavascriptExecutor js1 = (JavascriptExecutor) driver;
		js1.executeScript("$(\"#divFlowList\").children().eq(87).click()");

		Thread.sleep(3000);
		click(btn_ProcessStatusA);
		Thread.sleep(3000);
		click(btn_YesA);

		Thread.sleep(6000);
		click(navigation_pane);
		click(inventory_module);
		click(btn_stockAdjustment);
		click(btn_newStockAdjustment);
		Thread.sleep(3000);
		if (getText(txt_pageHeaderLableA).contentEquals("Stock Adjustment - New")) {
			writeTestResults("Verify that navigation to Stock Adjustment- New page",
					"'User can navigate to Stock Adjustment- New page",
					"User navigate to Stock Adjustment- New page Succesfully", "pass");
		} else {
			writeTestResults("Verify that navigation to Stock Adjustment- New page",
					"'User can navigate to Stock Adjustment- New page",
					"User can't navigate to Stock Adjustment- New page", "fail");
		}
		Thread.sleep(3000);
		pageRefersh();
	}

	// IN_SA_003
	public void VerifytheFieldsInStockAdjustmentNewpage() throws Exception {

		Thread.sleep(3000);
		if (isDisplayed(txt_PreparerA)) {
			writeTestResults("Verify Preparer Field Show in Stock Adjustment- New page",
					"Preparer Field Show in Stock Adjustment- New page", "Preparer Field is Display", "pass");
		} else {
			writeTestResults("Verify Preparer Field Show in Stock Adjustment- New page",
					"Preparer Field Show in Stock Adjustment- New page", "Preparer Field is Not Display", "fail");
		}
		Thread.sleep(3000);
		if (isDisplayed(txt_DescriptionA)) {
			writeTestResults("Verify Description Field Show in Stock Adjustment- New page",
					"Description Field Show in Stock Adjustment- New page", "Description Field is Display", "pass");
		} else {
			writeTestResults("Verify Description Field Show in Stock Adjustment- New page",
					"Description Field Show in Stock Adjustment- New page", "Description Field is Not Display", "fail");
		}
		Thread.sleep(3000);
		if (isDisplayed(txt_WarehouseA)) {
			writeTestResults("Verify Warehouse Field Show in Stock Adjustment- New page",
					"Warehouse Field Show in Stock Adjustment- New page", "Warehouse Field is Display", "pass");
		} else {
			writeTestResults("Verify Warehouse Field Show in Stock Adjustment- New page",
					"Warehouse Field Show in Stock Adjustment- New page", "Warehouse Field is Not Display", "fail");
		}
		Thread.sleep(3000);
		if (isDisplayed(txt_AdjustmentGroupA)) {
			writeTestResults("Verify AdjustmentGroup Field Show in Stock Adjustment- New page",
					"AdjustmentGroup Field Show in Stock Adjustment- New page", "AdjustmentGroup Field is Display",
					"pass");
		} else {
			writeTestResults("Verify AdjustmentGroup Field Show in Stock Adjustment- New page",
					"AdjustmentGroup Field Show in Stock Adjustment- New page", "AdjustmentGroup Field is Not Display",
					"fail");
		}
		Thread.sleep(3000);
		if (isDisplayed(txt_ProductA)) {
			writeTestResults("Verify Product Field Show in Stock Adjustment- New page",
					"Product Field Show in Stock Adjustment- New page", "Product Field is Display", "pass");
		} else {
			writeTestResults("Verify Product Field Show in Stock Adjustment- New page",
					"Product Field Show in Stock Adjustment- New page", "Product Field is Not Display", "fail");
		}
	}

	// IN_SA_004
	public void VerifyThatAutomaticallyLoadingSystemLoggedUser() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();
		Thread.sleep(3000);
		JavascriptExecutor js1 = (JavascriptExecutor) driver;
		js1.executeScript("$(\"#txtPreparer\").val()");
//				String User = "js1.executeScript(\"$(\"#txtPreparer\").val()\")";
//				System.out.println(User);
//				if(getText(txt_PreparerA).contentEquals("Nihal Peiris")) {
//					writeTestResults("Verify that automatically loading system logged user as preparer ", "automatically loading system logged user as preparer","automatically loading system logged user as preparer is correct", "pass");
//				}else {
//					writeTestResults("Verify that automatically loading system logged user as preparer ", "automatically loading system logged user as preparer","automatically loading system logged user as preparer is not correct", "fail");
//				}
	}

	// IN_SA_005
	public void VerifyThatUserCanAddCharactersAndValuesToDescription() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();
		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");
		Thread.sleep(3000);
		selectIndex(txt_WarehouseA, 1);

		ScrollDownAruna();

		Thread.sleep(3000);
		sendKeys(txt_ProductA, ProductAddTxt2A);
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		sendKeys(txt_ProductQtyA, "5");
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

		Thread.sleep(3000);
		click(btn_draft2);

		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		if (isDisplayed(btn_releese2)) {
			writeTestResults("Verify that user can add characters and values to Description",
					"user can add characters and values to Description",
					"add characters and values to Description correct", "pass");
		} else {
			writeTestResults("Verify that user can add characters and values to Description",
					"user can add characters and values to Description",
					"add characters and values to Description incorrect", "fail");
		}

		Thread.sleep(3000);
		click(btn_releese2);

		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageReleaseA)) {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Fail", "fail");
		}

	}

	// IN_SA_006
	public void VerifyThatUserCanSelectAWarehouse() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();
		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		ScrollDownAruna();

		Thread.sleep(3000);
		sendKeys(txt_ProductA, ProductAddTxt2A);
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		sendKeys(txt_ProductQtyA, "5");
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

		Thread.sleep(3000);
		selectText(txt_WarehouseA, WarehouseCodeAndDicriptionA);

		Thread.sleep(3000);
		String selectedOption = new Select(driver.findElement(By.xpath(txt_WarehouseA))).getFirstSelectedOption()
				.getText();
		System.out.println(selectedOption);

		Thread.sleep(3000);
		if (selectedOption.contentEquals(WarehouseCodeAndDicriptionA)) {
			writeTestResults("Verify that user can select a Warehouse", "user can select a Warehouse",
					"user can select a Warehouse correct", "pass");
		} else {
			writeTestResults("Verify that user can select a Warehouse", "user can select a Warehouse",
					"user can't select a Warehouse", "fail");
		}

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageReleaseA)) {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Fail", "fail");
		}
	}

	// IN_SA_007
	public void VerifyThatOnlyWarehouseCodeDisplayStockAdjustmentForm() throws Exception {

		click(btn_AdministrationA);
		click(btn_BalancingLevelSettingsA);
		click(btn_GeneralA);
		Thread.sleep(3000);
		selectText(sel_selectWarehouseSerchTypeA, "Code Only");
		Thread.sleep(3000);
		click(btn_JourneyConfigurationUpdateA);
		pageRefersh();

		Thread.sleep(6000);
		click(navigation_pane);
		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		ScrollDownAruna();

		Thread.sleep(3000);
		sendKeys(txt_ProductA, ProductAddTxt2A);
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		sendKeys(txt_ProductQtyA, "5");
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

		Thread.sleep(3000);
		selectText(txt_WarehouseA, WarehouseCodeA);

		Thread.sleep(3000);
		String selectedOption = new Select(driver.findElement(By.xpath(txt_WarehouseA))).getFirstSelectedOption()
				.getText();
		System.out.println(selectedOption);

		Thread.sleep(3000);
		if (selectedOption.contentEquals(WarehouseCodeA)) {
			writeTestResults("Verify that only warehouse code display Stock adjustment form",
					"only warehouse code display Stock adjustment form",
					"only warehouse code display Stock adjustment form Correct", "pass");
		} else {
			writeTestResults("Verify that only warehouse code display Stock adjustment form",
					"only warehouse code display Stock adjustment form",
					"only warehouse code display Stock adjustment form Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageReleaseA)) {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Fail", "fail");
		}

		Thread.sleep(6000);
		click(navigation_pane);
		click(btn_AdministrationA);
		click(btn_BalancingLevelSettingsA);
		click(btn_GeneralA);
		Thread.sleep(3000);
		selectText(sel_selectWarehouseSerchTypeA, "Code and Description");
		Thread.sleep(3000);
		click(btn_JourneyConfigurationUpdateA);
		pageRefersh();
	}

	// IN_SA_008
	public void VerifyThatOnlyWarehouseDescriptionDisplayStockAdjustmentForm() throws Exception {

		click(btn_AdministrationA);
		click(btn_BalancingLevelSettingsA);
		click(btn_GeneralA);
		Thread.sleep(3000);
		selectText(sel_selectWarehouseSerchTypeA, "Name Only");
		Thread.sleep(3000);
		click(btn_JourneyConfigurationUpdateA);
		pageRefersh();

		Thread.sleep(6000);
		click(navigation_pane);
		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		ScrollDownAruna();

		Thread.sleep(3000);
		sendKeys(txt_ProductA, ProductAddTxt2A);
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		sendKeys(txt_ProductQtyA, "5");
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

		Thread.sleep(3000);
		selectText(txt_WarehouseA, WarehouseDicriptionA);

		Thread.sleep(3000);
		String selectedOption = new Select(driver.findElement(By.xpath(txt_WarehouseA))).getFirstSelectedOption()
				.getText();
		System.out.println(selectedOption);

		Thread.sleep(3000);
		if (selectedOption.contentEquals(WarehouseDicriptionA)) {
			writeTestResults("Verify that only warehouse Description display Stock adjustment form",
					"only warehouse Description display Stock adjustment form",
					"only warehouse Description display Stock adjustment form Correct", "pass");
		} else {
			writeTestResults("Verify that only warehouse Description display Stock adjustment form",
					"only warehouse Description display Stock adjustment form",
					"only warehouse Description display Stock adjustment form Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageReleaseA)) {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Fail", "fail");
		}

		Thread.sleep(6000);
		click(navigation_pane);
		click(btn_AdministrationA);
		click(btn_BalancingLevelSettingsA);
		click(btn_GeneralA);
		Thread.sleep(3000);
		selectText(sel_selectWarehouseSerchTypeA, "Code and Description");
		Thread.sleep(3000);
		click(btn_JourneyConfigurationUpdateA);
		pageRefersh();
	}

	// IN_SA_009
	public void VerifyThatOnlyWarehouseCodeAndDescriptionDisplayStockAdjustmentForm() throws Exception {

		click(btn_AdministrationA);
		click(btn_BalancingLevelSettingsA);
		click(btn_GeneralA);
		Thread.sleep(3000);
		selectText(sel_selectWarehouseSerchTypeA, "Code and Description");
		Thread.sleep(3000);
		click(btn_JourneyConfigurationUpdateA);
		pageRefersh();

		Thread.sleep(6000);
		click(navigation_pane);
		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		ScrollDownAruna();

		Thread.sleep(3000);
		sendKeys(txt_ProductA, ProductAddTxt2A);
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		sendKeys(txt_ProductQtyA, "5");
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

		Thread.sleep(3000);
		selectText(txt_WarehouseA, WarehouseCodeAndDicriptionA);

		Thread.sleep(3000);
		String selectedOption = new Select(driver.findElement(By.xpath(txt_WarehouseA))).getFirstSelectedOption()
				.getText();
		System.out.println(selectedOption);

		Thread.sleep(3000);
		if (selectedOption.contentEquals(WarehouseCodeAndDicriptionA)) {
			writeTestResults("Verify that only warehouse Code and Description display Stock adjustment form",
					"only warehouse Code and Description display Stock adjustment form",
					"only warehouse Code and Description display Stock adjustment form Correct", "pass");
		} else {
			writeTestResults("Verify that only warehouse Code and Description display Stock adjustment form",
					"only warehouse Code and Description display Stock adjustment form",
					"only warehouse Code and Description display Stock adjustment form Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageReleaseA)) {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Fail", "fail");
		}

		Thread.sleep(6000);
		click(navigation_pane);
		click(btn_AdministrationA);
		click(btn_BalancingLevelSettingsA);
		click(btn_GeneralA);
		Thread.sleep(3000);
		selectText(sel_selectWarehouseSerchTypeA, "Code and Description");
		Thread.sleep(3000);
		click(btn_JourneyConfigurationUpdateA);
		pageRefersh();
	}

	// IN_SA_010
	public void VerifyThatActiveAdjustmentGroupsAreLoaidngInAdjustmentGroup() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");
		Thread.sleep(3000);
		selectText(txt_AdjustmentGroupA, "Para");
		Thread.sleep(3000);
		selectIndex(txt_WarehouseA, 1);

		Thread.sleep(3000);
		String selectedOption = new Select(driver.findElement(By.xpath(txt_AdjustmentGroupA))).getFirstSelectedOption()
				.getText();
		System.out.println(selectedOption);

		Thread.sleep(3000);
		if (selectedOption.contentEquals("Para")) {
			writeTestResults("Verify that user can select a Adjustment Group", "user can select a Adjustment Group",
					"user can select a Adjustment Group correct", "pass");
		} else {
			writeTestResults("Verify that user can select a Adjustment Group", "user can select a Adjustment Group",
					"user can't select a Adjustment Group", "fail");
		}

		ScrollDownAruna();

		Thread.sleep(3000);
		sendKeys(txt_ProductA, ProductAddTxt2A);
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		sendKeys(txt_ProductQtyA, "5");
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageReleaseA)) {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Fail", "fail");
		}
	}

	// IN_SA_011
	public void VerifyThatInactiveAdjustmentGroupsAreNotLoading() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		click(btn_AddAdjustmentGroupA);
		Thread.sleep(3000);
		click(btn_InactiveAdjustmentGroupA);
		Thread.sleep(3000);
		click(btn_AddAdjustmentGroupUpdateA);

		Thread.sleep(3000);
		selectText(txt_AdjustmentGroupA, "Para");

		Thread.sleep(3000);
		String selectedOption = new Select(driver.findElement(By.xpath(txt_AdjustmentGroupA))).getFirstSelectedOption()
				.getText();

		if (selectedOption.contentEquals("--None--")) {
			writeTestResults("Verify That Inactive Adjustment Groups Are Not Loading",
					"Inactive Adjustment Groups Are Not Loading",
					"Inactive Adjustment Groups Are Not Loading Successfull", "pass");
		} else {
			writeTestResults("Verify That Inactive Adjustment Groups Are Not Loading",
					"Inactive Adjustment Groups Are Not Loading", "Inactive Adjustment Groups Are Not Loading Fail",
					"fail");
		}

		Thread.sleep(3000);
		click(btn_AddAdjustmentGroupA);
		Thread.sleep(3000);
		click(btn_InactiveAdjustmentGroupA);
		Thread.sleep(3000);
		click(btn_AddAdjustmentGroupUpdateA);

	}

	// IN_SA_012
	public void VerifyThatUserAllowToAdd() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();
		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		click(btn_AddAdjustmentGroupA);
		Thread.sleep(3000);
		click(btn_AdjustmentGroupAddA);
		LocalTime myObj = LocalTime.now();
		Thread.sleep(3000);
		sendKeys(txt_AdjustmentGroupTxtA, AdjustmentGroupTxtA + myObj);
		Thread.sleep(3000);
		click(btn_AddAdjustmentGroupUpdateA);

		Thread.sleep(3000);
		selectText(txt_AdjustmentGroupA, AdjustmentGroupTxtA + myObj);

		Thread.sleep(3000);
		String selectedOption = new Select(driver.findElement(By.xpath(txt_AdjustmentGroupA))).getFirstSelectedOption()
				.getText();

		Thread.sleep(5000);
		if (selectedOption.contentEquals(AdjustmentGroupTxtA + myObj)) {
			writeTestResults(
					"Verify that user allow to add adjustment group using \"+\" icon and added groups are loading in the dropdown",
					"user allow to add adjustment group using \"+\" icon and added groups are loading in the dropdown",
					"user allow to add adjustment group using \"+\" icon and added groups are loading in the dropdown successfull",
					"pass");

		} else {
			writeTestResults(
					"Verify that user allow to add adjustment group using \"+\" icon and added groups are loading in the dropdown",
					"user allow to add adjustment group using \"+\" icon and added groups are loading in the dropdown",
					"user allow to add adjustment group using \"+\" icon and added groups are loading in the dropdown fail",
					"fail");
		}

		Thread.sleep(3000);
		selectIndex(txt_WarehouseA, 1);

		ScrollDownAruna();

		Thread.sleep(3000);
		sendKeys(txt_ProductA, ProductAddTxt2A);
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		sendKeys(txt_ProductQtyA, "5");
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageReleaseA)) {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Fail", "fail");
		}
	}

	// IN_SA_013
	public void VerifyThatUserCanUseAutoCompleteForActiveProductCodeDescription() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();
		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		selectIndex(txt_WarehouseA, 1);

		ScrollDownAruna();

		Thread.sleep(3000);
		sendKeys(txt_ProductA, ProductAddTxt2A);

		Thread.sleep(4000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(4000);
		pressEnter(txt_ProductA);

		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, "5");
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

		Thread.sleep(3000);
		if (getText(txt_ProductLoadTxtA).contentEquals("ArunaIP2 [ArunaIP2dicription]")) {
			writeTestResults("Verify That User Can Use Auto Complete For Active Product Code Description",
					"User Can Use Auto Complete For Active Product Code Description",
					"User Can Use Auto Complete For Active Product correct", "pass");

		} else {
			writeTestResults("Verify That User Can Use Auto Complete For Active Product Code Description",
					"User Can Use Auto Complete For Active Product Code Description",
					"user can't Use Auto Complete For Active Product", "fail");
		}

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageReleaseA)) {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Fail", "fail");
		}
	}

	// IN_SA_014
	public void VerifyThatUserCanAddAProductTheProductGridUsingProductLookup() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();
		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		selectIndex(txt_WarehouseA, 1);

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, ProductAddTxt2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		if (getText(txt_ProductLoadTxtA).contentEquals("ArunaIP2 [ArunaIP2dicription]")) {
			writeTestResults("Verify that user can add a product the Product Grid using Product Lookup",
					"user can add a product the Product Grid using Product Lookup",
					"user can add a product the Product Grid using Product Lookup correct", "pass");

		} else {
			writeTestResults("Verify that user can add a product the Product Grid using Product Lookup",
					"user can add a product the Product Grid using Product Lookup",
					"user can't add a product the Product Grid using Product Lookup correct", "fail");
		}

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageReleaseA)) {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Fail", "fail");
		}
	}

	// IN_SA_015
	public void VerifyThatOnlyProductCodeDisplayInProductGridOfStockAdjustment() throws Exception {

		click(btn_AdministrationA);
		click(btn_BalancingLevelSettingsA);
		selectText(sel_ConfigProA, "Code Only");
		Thread.sleep(3000);
		click(btn_ConfigProUpdateA);
		pageRefersh();

		Thread.sleep(6000);
		click(navigation_pane);
		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		selectIndex(txt_WarehouseA, 1);

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, ProductAddTxt2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		if (getText(txt_ProductLoadTxtA).contentEquals("ArunaIP2")) {
			writeTestResults("Verify that only product code display in product grid of Stock Adjustment",
					"only product code display in product grid of Stock Adjustment",
					"only product code display in product grid of Stock Adjustment correct", "pass");

		} else {
			writeTestResults("Verify that only product code display in product grid of Stock Adjustment",
					"only product code display in product grid of Stock Adjustment",
					"only product code display in product grid of Stock Adjustment incorrect", "fail");
		}

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageReleaseA)) {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Fail", "fail");
		}

		Thread.sleep(6000);
		click(navigation_pane);
		click(btn_AdministrationA);
		click(btn_BalancingLevelSettingsA);
		selectText(sel_ConfigProA, "Code and Description");
		Thread.sleep(3000);
		click(btn_ConfigProUpdateA);
		pageRefersh();
	}

	// IN_SA_016
	public void VerifyThatOnlyProductDescriptionDisplayInProductGridOfStockAdjustment() throws Exception {

		click(btn_AdministrationA);
		click(btn_BalancingLevelSettingsA);
		selectText(sel_ConfigProA, "Name Only");
		Thread.sleep(3000);
		click(btn_ConfigProUpdateA);
		pageRefersh();

		Thread.sleep(6000);
		click(navigation_pane);
		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		selectIndex(txt_WarehouseA, 1);

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, ProductAddTxtDicA);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		if (getText(txt_ProductLoadTxtA).contentEquals("ArunaIP2dicription")) {
			writeTestResults("Verify that only product Description display in product grid of Stock Adjustment",
					"only product Description display in product grid of Stock Adjustment",
					"only product Description display in product grid of Stock Adjustment correct", "pass");

		} else {
			writeTestResults("Verify that only product Description display in product grid of Stock Adjustment",
					"only product Description display in product grid of Stock Adjustment",
					"only product Description display in product grid of Stock Adjustment incorrect", "fail");
		}

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageReleaseA)) {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Fail", "fail");
		}

		Thread.sleep(6000);
		click(navigation_pane);
		click(btn_AdministrationA);
		click(btn_BalancingLevelSettingsA);
		selectText(sel_ConfigProA, "Code and Description");
		Thread.sleep(3000);
		click(btn_ConfigProUpdateA);
		pageRefersh();
	}

	// IN_SA_017
	public void VerifyThatProductCodeAndDescriptionDisplayInProductGridOfStockAdjustment() throws Exception {

		click(btn_AdministrationA);
		click(btn_BalancingLevelSettingsA);
		selectText(sel_ConfigProA, "Code and Description");
		Thread.sleep(3000);
		click(btn_ConfigProUpdateA);
		pageRefersh();

		Thread.sleep(6000);
		click(navigation_pane);
		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		selectIndex(txt_WarehouseA, 1);

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, ProductAddTxt2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		if (getText(txt_ProductLoadTxtA).contentEquals("ArunaIP2 [ArunaIP2dicription]")) {
			writeTestResults("Verify that  product code and Description display in product grid of Stock Adjustment",
					"product code and Description display in product grid of Stock Adjustment",
					"product code and Description display in product grid of Stock Adjustment correct", "pass");

		} else {
			writeTestResults("Verify that  product code and Description display in product grid of Stock Adjustment",
					"product code and Description display in product grid of Stock Adjustment",
					"product code and Description display in product grid of Stock Adjustment incorrect", "fail");
		}

		Thread.sleep(3000);
		click(btn_ProductAddSelDeleteA);

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, ProductAddTxtDicA);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		if (getText(txt_ProductLoadTxtA).contentEquals("ArunaIP2 [ArunaIP2dicription]")) {
			writeTestResults("Verify that only product Description display in product grid of Stock Adjustment",
					"only product Description display in product grid of Stock Adjustment",
					"only product Description display in product grid of Stock Adjustment correct", "pass");

		} else {
			writeTestResults("Verify that only product Description display in product grid of Stock Adjustment",
					"only product Description display in product grid of Stock Adjustment",
					"only product Description display in product grid of Stock Adjustment incorrect", "fail");
		}

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageReleaseA)) {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Fail", "fail");
		}

		Thread.sleep(6000);
		click(navigation_pane);
		click(btn_AdministrationA);
		click(btn_BalancingLevelSettingsA);
		selectText(sel_ConfigProA, "Code and Description");
		Thread.sleep(3000);
		click(btn_ConfigProUpdateA);
		pageRefersh();
	}

	// IN_SA_018
	public void VerifyThatUserCanOnlyEnterNumericValuesToQty() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		selectIndex(txt_WarehouseA, 1);

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, ProductAddTxt2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		sendKeys(txt_ProductAddQtyA, "-5");

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		if ((getText(txt_ProductAddAfterQtyA).contentEquals("-5.00"))) {
			writeTestResults("Verify that User can enter (-) value to Qty", "User can enter (-) value to Qty",
					"User can enter (-) value to Qty correct", "pass");

		} else {
			writeTestResults("Verify that User can enter (-) value to Qty", "User can enter (-) value to Qty",
					"User can't enter (-) value to Qty", "fail");
		}

		Thread.sleep(3000);
		click(btn_edit);

		Thread.sleep(3000);
		sendKeys(txt_ProductAddQtyA, "10.5");

		Thread.sleep(3000);
		click(btn_update);

		Thread.sleep(3000);
		if ((getText(txt_ProductAddAfterQtyA).contentEquals("10.50"))) {
			writeTestResults("Verify that User can enter (.) value to Qty", "User can enter (.) value to Qty",
					"User can enter (.) value to Qty correct", "pass");

		} else {
			writeTestResults("Verify that User can enter (.) value to Qty", "User can enter (.) value to Qty",
					"User can't enter (.) value to Qty", "fail");
		}

		Thread.sleep(3000);
		click(btn_edit);

		Thread.sleep(3000);
		sendKeys(txt_ProductAddQtyA, "!@#$%^&*");

		Thread.sleep(3000);
		click(btn_update);

		Thread.sleep(3000);
		if (!(getText(txt_ProductAddAfterQtyA).equals("!@#$%^&*"))) {
			writeTestResults("Verify that User should not be able to enter characters and special characters to Qty",
					"User should not be able to enter characters and special characters to Qty",
					"User should not be able to enter characters and special characters to Qty correct", "pass");

		} else {
			writeTestResults("Verify that User should not be able to enter characters and special characters to Qty",
					"User should not be able to enter characters and special characters to Qty",
					"User should not be able to enter characters and special characters to Qty incorrect", "fail");
		}
	}

	// IN_SA_019
	public void VerifyThatUserCanEnterDecimalValueAsQty() throws Exception {

		click(inventory_module);
		click(btn_productInformation);

		Thread.sleep(3000);
		sendKeys(txt_ProductSerchA, ProductAddTxt2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductSerchA);
		Thread.sleep(3000);
		doubleClick(sel_ProductSerchSelA);

		Thread.sleep(3000);
		if ((isSelected(btn_AllowDecimalA))) {
			writeTestResults("Verify that Product allow the Decimal Value", "Product allow the Decimal Value",
					"Product is allow the Decimal Value ", "pass");

		} else {
			writeTestResults("Verify that Product allow the Decimal Value", "Product allow the Decimal Value",
					"Product is not allow the Decimal Value", "fail");
		}

		Thread.sleep(6000);
		click(navigation_pane);
		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		selectIndex(txt_WarehouseA, 1);

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, ProductAddTxt2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		sendKeys(txt_ProductAddQtyA, "10.5");

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		if ((getText(txt_ProductAddAfterQtyA).contentEquals("10.50"))) {
			writeTestResults("Verify that User can enter (.) value to Qty", "User can enter (.) value to Qty",
					"User can enter (.) value to Qty correct", "pass");

		} else {
			writeTestResults("Verify that User can enter (.) value to Qty", "User can enter (.) value to Qty",
					"User can't enter (.) value to Qty", "fail");
		}

		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageReleaseA)) {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Fail", "fail");
		}
	}

	// IN_SA_020
	public void VerifyThatUserCannotEnterDecimalValueAsQty() throws Exception {

		click(inventory_module);
		click(btn_productInformation);

		Thread.sleep(3000);
		sendKeys(txt_ProductSerchA, Pro2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductSerchA);
		Thread.sleep(3000);
		doubleClick(sel_ProductSerchSelA);

		Thread.sleep(3000);
		if ((!isSelected(btn_AllowDecimalA))) {
			writeTestResults("Verify that Product allow the Decimal Value", "Product allow the Decimal Value",
					"Product is not allow the Decimal Value ", "pass");

		} else {
			writeTestResults("Verify that Product allow the Decimal Value", "Product allow the Decimal Value",
					"Product is allow the Decimal Value", "fail");
		}

		Thread.sleep(6000);
		click(navigation_pane);
		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		selectIndex(txt_WarehouseA, 1);

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, Pro2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		sendKeys(txt_ProductAddQtyA, "10.5");

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		if (!(getText(txt_ProductAddAfterQtyA).contentEquals("10.50"))) {
			writeTestResults("Verify that User can enter (.) value to Qty", "User can enter (.) value to Qty",
					"User can't enter (.) value to Qty correct", "pass");

		} else {
			writeTestResults("Verify that User can enter (.) value to Qty", "User can enter (.) value to Qty",
					"User can enter (.) value to Qty", "fail");
		}

		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageReleaseA)) {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Fail", "fail");
		}
	}

	// IN_SA_021
	public void VerifyThatUserAllowToAddDecimalValuesIfProductChangeToDecimalAllowProduct() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		selectIndex(txt_WarehouseA, 1);

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, Pro1A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		sendKeys(txt_ProductAddQtyA, "10.5");

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		if ((getText(txt_ProductAddAfterQtyA).contentEquals("10.50"))) {
			writeTestResults("Verify that User can enter (.) value to Qty", "User can enter (.) value to Qty",
					"User can enter (.) value to Qty correct", "pass");

		} else {
			writeTestResults("Verify that User can enter (.) value to Qty", "User can enter (.) value to Qty",
					"User can't enter (.) value to Qty", "fail");
		}

		Thread.sleep(3000);
		click(btn_edit);

		Thread.sleep(3000);
		click(btn_ProductAddSelDeleteA);

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, Pro2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		sendKeys(txt_ProductAddQtyA, "10.5");

		Thread.sleep(3000);
		click(btn_update);

		Thread.sleep(3000);
		if (!(getText(txt_ProductAddAfterQtyA).contentEquals("10.50"))) {
			writeTestResults("Verify that User can enter (.) value to Qty", "User can enter (.) value to Qty",
					"User can't enter (.) value to Qty correct", "pass");

		} else {
			writeTestResults("Verify that User can enter (.) value to Qty", "User can enter (.) value to Qty",
					"User can enter (.) value to Qty", "fail");
		}
	}

	// IN_SA_022
	public void VerifyThatUserCanAddMultipleProductsToTheProductGridUsingPlusMark() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		selectIndex(txt_WarehouseA, 1);

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, Pro1A);
		Thread.sleep(3000);
		String val1 = Pro1A;
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		click(btn_ProductAdd2A);
		Thread.sleep(3000);
		click(btn_Product2A);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, Pro2A);
		String val2 = Pro2A;
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(4000);
		doubleClick(sel_ProductAddSelA);

		String tval1 = getText(txt_tval1A);
		String tval2 = getText(txt_tval2A);

		String[] val = { val1, val2 };
		for (String arr : val) {
			System.out.println(arr);
		}

		for (i = 0; i < val.length; i++) {
			if (tval1.contains(val[i])) {
				System.out.println("user can add product 1 to the  Product Grid using Plus Mark successfull");
				writeTestResults("Verify that user can add multiple products to the  Product Grid using Plus Mark",
						"user can add multiple products to the  Product Grid using Plus Mark",
						"user can add product 1 to the  Product Grid using Plus Mark successfull", "pass");
			} else if (tval2.contains(val[i])) {
				System.out.println("user can add product 2 to the  Product Grid using Plus Mark successfull");
				writeTestResults("Verify that user can add multiple products to the  Product Grid using Plus Mark",
						"user can add multiple products to the  Product Grid using Plus Mark",
						"user can add product 2 to the  Product Grid using Plus Mark successfull", "pass");
			} else {
				System.out.println("User Can Add Products To The Grid Fail");
				writeTestResults("Verify that user can add multiple products to the  Product Grid using Plus Mark",
						"user can add multiple products to the  Product Grid using Plus Mark",
						"User Can Add Products To The Grid Fail", "fail");
			}
		}

	}

	// IN_SA_023
	public void VerifyThatUserCanAddMultipleProductsToTheGridOnce() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		selectIndex(txt_WarehouseA, 1);

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		pressEnter(txt_ProductSerchA);

		click(sel_pro1A);
		Thread.sleep(3000);
		String val1 = getText(sel_pro1A);
		click(sel_pro2A);
		Thread.sleep(3000);
		String val2 = getText(sel_pro2A);
		click(sel_pro3A);
		Thread.sleep(3000);
		String val3 = getText(sel_pro3A);
		doubleClick(sel_pro3A);

		String tval1 = getText(txt_tval1A);
		String tval2 = getText(txt_tval2A);
		String tval3 = getText(txt_tval3A);

		String[] val = { val1, val2, val3 };
//							for(String arr:val) {
//								System.out.println(arr);
//							}

		for (i = 0; i < val.length; i++) {
			if (tval1.contains(val[i])) {
				System.out.println("User Can Add Product 1 To The Grid Once Successfull");
				writeTestResults("Verify That User Can Add Multiple Products To The Grid Once",
						"User Can Add Multiple Products To The Grid Once",
						"User Can Add Product 1 To The Grid Once Successfull", "pass");
			} else if (tval2.contains(val[i])) {
				System.out.println("User Can Add Product 2 To The Grid Once Successfull");
				writeTestResults("Verify That User Can Add Multiple Products To The Grid Once",
						"User Can Add Multiple Products To The Grid Once",
						"User Can Add Product 2 To The Grid Once Successfull", "pass");
			} else if (tval3.contains(val[i])) {
				System.out.println("User Can Add Product 3 To The Grid Once Successfull");
				writeTestResults("Verify That User Can Add Multiple Products To The Grid Once",
						"User Can Add Multiple Products To The Grid Once",
						"User Can Add Product 3 To The Grid Once Successfull", "pass");
			} else {
				System.out.println("User Can Add Products To The Grid Once Fail");
				writeTestResults("Verify That User Can Add Multiple Products To The Grid Once",
						"User Can Add Multiple Products To The Grid Once",
						"User Can Add Products To The Grid Once Fail", "fail");
			}
		}
	}

	// IN_SA_024
	public void VerifyThatProductExtendedDescriptionWillAppear() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		selectIndex(txt_WarehouseA, 1);

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, ProductAddTxt2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		click(btn_ExDescriptionA);

		Thread.sleep(3000);
		if (isDisplayed(txt_ExDescriptiontxtA)) {
			writeTestResults("Verify That Product Extended Description Will Appear",
					"Product Extended Description show as a pop up",
					"Product Extended Description show as a pop up Successfull", "pass");

		} else {
			writeTestResults("Verify That Product Extended Description Will Appear",
					"Product Extended Description show as a pop up",
					"Product Extended Description show as a pop up Fail", "fail");
		}
	}

	// IN_SA_025
	public void VerifyThatDefaultUOMRelevantToTheSelectedProductsWillVisibleInTheGrid() throws Exception {

		click(inventory_module);
		click(btn_productInformation);

		Thread.sleep(3000);
		sendKeys(txt_ProductSerchA, ProductAddTxt2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductSerchA);
		Thread.sleep(3000);
		doubleClick(sel_ProductSerchSelA);
		Thread.sleep(3000);
		String UOM = getText(txt_DefaultUOMA);

		Thread.sleep(6000);
		click(navigation_pane);
		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		selectIndex(txt_WarehouseA, 1);

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, ProductAddTxt2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		if (UOM.contentEquals(getText(txt_DefaultUOMtxtA))) {
			writeTestResults("Verify that default UOM relevant to the selected products will visible in the grid",
					"default UOM relevant to the selected products will visible in the grid",
					"default UOM relevant to the selected products will visible in the grid Successfull", "pass");

		} else {
			writeTestResults("Verify that default UOM relevant to the selected products will visible in the grid",
					"default UOM relevant to the selected products will visible in the grid",
					"default UOM relevant to the selected products will visible in the grid Fail", "fail");
		}

	}

	// IN_SA_026
	public void VerifyThatOnHandQtyOfRelevantToTheSelectedProducts() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		selectIndex(txt_WarehouseA, 1);

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, ProductAddTxt2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		if (!getText(txt_OnHandQtyA).contentEquals("0.00")) {
			writeTestResults("Verify That OnHand Qty Of Relevant To The Selected Products",
					"Display OnHand Qty Of Relevant To The Selected Products",
					"Display OnHand Qty Of Relevant To The Selected Products Successfull", "pass");

		} else {
			writeTestResults("Verify That OnHand Qty Of Relevant To The Selected Products",
					"Display OnHand Qty Of Relevant To The Selected Products",
					"Display OnHand Qty Of Relevant To The Selected Products Fail", "fail");
		}
	}

	// IN_SA_027
	public void VerifyThatReservedQtyOfRelevantToTheSelectedProductsAreVisible() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, ProductAddTxtA);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		if (!getText(txt_ReservedQtyA).contentEquals("0.00")) {
			writeTestResults("Verify That Reserved Qty Of Relevant To The Selected Products",
					"Display Reserved Qty Of Relevant To The Selected Products",
					"Display Reserved Qty Of Relevant To The Selected Products Successfull", "pass");

		} else {
			writeTestResults("Verify That Reserved Qty Of Relevant To The Selected Products",
					"Display Reserved Qty Of Relevant To The Selected Products",
					"Display Reserved Qty Of Relevant To The Selected Products Fail", "fail");
		}
	}

	// IN_SA_028
	public void VerifyThatRacksRelevantToThePartiuclarWarehosueWillBeLoaded() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		selectIndex(txt_WarehouseA, 1);

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, ProductAddTxt2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		selectText(btn_RackA, RackTxtA);
		Thread.sleep(3000);
		String selectedOption = new Select(driver.findElement(By.xpath(btn_RackA))).getFirstSelectedOption().getText();

		Thread.sleep(3000);
		if (selectedOption.contentEquals(RackTxtA)) {
			writeTestResults("Verify that Racks relevant to the partiuclar warehosue will be loaded",
					"Racks relevant to the partiuclar warehosue will be loaded",
					"Racks relevant to the partiuclar warehosue will be loaded Successfull", "pass");

		} else {
			writeTestResults("Verify that Racks relevant to the partiuclar warehosue will be loaded",
					"Racks relevant to the partiuclar warehosue will be loaded",
					"Racks relevant to the partiuclar warehosue will be loaded Fail", "fail");
		}

	}

	// IN_SA_029
	public void VerifyThatUserCannotDraftTheStockAdjustmentWithoutFillingMandatoryFields() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		selectIndex(txt_WarehouseA, 1);

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, ProductAddTxt2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		sendKeys(txt_ProductAddQtyA, productQuantityA);

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_ValidatorReleasedA)) {
			writeTestResults("Verify that user cannot draft the stock adjustment without filling mandatory fields",
					"user cannot draft the stock adjustment without filling mandatory fields",
					"user cannot draft the stock adjustment without filling mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user cannot draft the stock adjustment without filling mandatory fields",
					"user cannot draft the stock adjustment without filling mandatory fields",
					"user cannot draft the stock adjustment without filling mandatory fields Fail", "fail");
		}
	}

	// IN_SA_030
	public void VerifyThatWarningOccuredWhenAddProductsInProductLookupWithoutSelectingWarehouse() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		selectIndex(txt_WarehouseA, 0);

		Thread.sleep(3000);
		click(btn_ProductAddA);

		Thread.sleep(3000);
		if (isDisplayed(txt_selectWarehouseWarningA)) {
			writeTestResults(
					"Verify that warning occured when add products in Product Lookup without selecting Warehouse",
					"warning occured when add products in Product Lookup without selecting Warehouse",
					"warning occured when add products in Product Lookup without selecting Warehouse Successfull",
					"pass");

		} else {
			writeTestResults(
					"Verify that warning occured when add products in Product Lookup without selecting Warehouse",
					"warning occured when add products in Product Lookup without selecting Warehouse",
					"warning occured when add products in Product Lookup without selecting Warehouse Fail", "fail");
		}
	}

	// IN_SA_031
	public void VerifyThatWarningOccuredWhenAddProductsInQuicksearchWithoutSelectingWarehouse() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		selectIndex(txt_WarehouseA, 0);

		ScrollDownAruna();

		Thread.sleep(3000);
		sendKeys(txt_ProductA, ProductAddTxt2A);

		Thread.sleep(4000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(4000);
		pressEnter(txt_ProductA);

		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, productQuantityA);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

		Thread.sleep(3000);
		if (isDisplayed(txt_selectWarehouseWarningA)) {
			writeTestResults(
					"Verify that warning occured when add products in Quick search without selecting Warehouse",
					"warning occured when add products in Quick search without selecting Warehouse",
					"warning occured when add products in Quick search without selecting Warehouse Successfull",
					"pass");

		} else {
			writeTestResults(
					"Verify that warning occured when add products in Quick search without selecting Warehouse",
					"warning occured when add products in Quick search without selecting Warehouse",
					"warning occured when add products in Quick search without selecting Warehouse Fail", "fail");
		}
	}

	// IN_SA_032
	public void VerifyThatUserCanDraftTheStockAdjustmentWithHavingMandatoryFields() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		selectIndex(txt_WarehouseA, 1);

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, ProductAddTxt2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		sendKeys(txt_ProductAddQtyA, productQuantityA);

		Thread.sleep(3000);
		click(btn_draft2);

		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

	}

	// IN_SA_033
	public void VerifyThatUserCanReleaseTheDraftedStockAdjustment() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, ProductAddTxt2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		sendKeys(txt_ProductAddQtyA, productQuantityA);

		Thread.sleep(3000);
		click(btn_draft2);

		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageReleaseA)) {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Fail", "fail");
		}

	}

	// IN_SA_034
	public void VerifyThatUserCanUseDraftAndNew() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		selectIndex(txt_WarehouseA, 1);

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, ProductAddTxt2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		sendKeys(txt_ProductAddQtyA, productQuantityA);

		Thread.sleep(3000);
		click(btn_draftAndNewActionVerification);

		Thread.sleep(5000);
		if (isDisplayed(btn_draft2)) {
			writeTestResults("Verify that user can draft and New stock adjustment",
					"user can draft and New stock adjustment", "user can draft and New stock adjustment Successfull",
					"pass");

		} else {
			writeTestResults("Verify that user can draft and New stock adjustment",
					"user can draft and New stock adjustment", "user can draft and New stock adjustment Fail", "fail");
		}

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		selectIndex(txt_WarehouseA, 1);

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, ProductAddTxt2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		sendKeys(txt_ProductAddQtyA, productQuantityA);

		Thread.sleep(3000);
		click(btn_draft2);

		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);

		Thread.sleep(3000);
		click(btn_StockAdjustmentByPageA);

		sendKeys(txt_StockAdjustmentSerchA, trackCode);
		Thread.sleep(3000);
		pressEnter(txt_StockAdjustmentSerchA);
		Thread.sleep(3000);
		doubleClick(sel_StockAdjustmentSerchselA);

		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}
	}

	// IN_SA_035
	public void VerifyThatUserCanDeleteDraftedStockAdjustments() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		selectIndex(txt_WarehouseA, 1);

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, ProductAddTxt2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		sendKeys(txt_ProductAddQtyA, productQuantityA);

		Thread.sleep(3000);
		click(btn_draft2);

		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_action);

		Thread.sleep(3000);
		click(btn_deleteActionVrification);
		Thread.sleep(3000);
		click(btn_deleteConfirationActionVerification);
		System.out.println();
		Thread.sleep(5000);
		if (getText(lbl_docStatus).contentEquals("( Deleted )")) {
			writeTestResults("Verify That User Can Delete Drafted Stock Adjustments",
					"User Can Delete Drafted Stock Adjustments",
					"User Can Delete Drafted Stock Adjustments Successfull", "pass");

		} else {
			writeTestResults("Verify That User Can Delete Drafted Stock Adjustments",
					"User Can Delete Drafted Stock Adjustments", "User Can Delete Drafted Stock Adjustments Fail",
					"fail");
		}

	}

	// IN_SA_036
	public void VerifyThatUserCanEditAndUpdateTheDraftedStockAdjustment() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		selectIndex(txt_WarehouseA, 1);

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, ProductAddTxt2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		sendKeys(txt_ProductAddQtyA, "10");

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_editActionVerification);
		Thread.sleep(5000);
		if (isDisplayed(btn_updateActionVerification)) {
			writeTestResults("Verify That User Can Edit The Drafted Stock Adjustment",
					"User Can Edit The Drafted Stock Adjustment",
					"User Can Edit The Drafted Stock Adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify That User Can Edit The Drafted Stock Adjustment",
					"User Can Edit The Drafted Stock Adjustment", "User Can Edit The Drafted Stock Adjustment Fail",
					"fail");
		}

		Thread.sleep(3000);
		click(btn_ProductAdd2A);
		Thread.sleep(3000);
		click(btn_Product2A);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, Pro2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		click(btn_ProductAddSelDeleteA);

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, Pro1A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		sendKeys(txt_ProductAddQtyA, productQuantityA);
		Thread.sleep(3000);
		sendKeys(txt_CostPerUnitA, "100");
		Thread.sleep(3000);
		selectIndex(txt_WarehouseA, 2);

		Thread.sleep(3000);
		click(btn_updateActionVerification);
		Thread.sleep(5000);
		if (isDisplayed(btn_editActionVerification)) {
			writeTestResults("Verify That User Can Update The Drafted Stock Adjustment",
					"User Can Update The Drafted Stock Adjustment",
					"User Can Update The Drafted Stock Adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify That User Can Update The Drafted Stock Adjustment",
					"User Can Update The Drafted Stock Adjustment", "User Can Update The Drafted Stock Adjustment Fail",
					"fail");
		}
	}

	// IN_SA_037
	public void VerifyThatUserCanUseUpdateAndNew() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		selectIndex(txt_WarehouseA, 1);

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, ProductAddTxt2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		sendKeys(txt_ProductAddQtyA, productQuantityA);

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_editActionVerification);

		Thread.sleep(3000);
		click(btn_updateAndNewActionVerification);
		Thread.sleep(5000);
		if (isDisplayed(btn_draft2)) {
			writeTestResults("Verify That User Can Update And New The Drafted Stock Adjustment",
					"User Can Update And New The Drafted Stock Adjustment",
					"User Can Update And New The Drafted Stock Adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify That User Can Update And New The Drafted Stock Adjustment",
					"User Can Update And New The Drafted Stock Adjustment",
					"User Can Update And New The Drafted Stock Adjustment Fail", "fail");
		}

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		selectIndex(txt_WarehouseA, 1);

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, ProductAddTxt2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		sendKeys(txt_ProductAddQtyA, productQuantityA);

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);

		Thread.sleep(3000);
		click(btn_StockAdjustmentByPageA);

		sendKeys(txt_StockAdjustmentSerchA, trackCode);
		Thread.sleep(3000);
		pressEnter(txt_StockAdjustmentSerchA);
		Thread.sleep(3000);
		doubleClick(sel_StockAdjustmentSerchselA);

		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}
	}

	// IN_SA_038
	public void VerifyThatUserCanUseCopyFromOption() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		selectIndex(txt_WarehouseA, 1);

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, ProductAddTxt2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		sendKeys(txt_ProductAddQtyA, productQuantityA);

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);

		Thread.sleep(6000);
		click(navigation_pane);
		click(inventory_module);
		click(btn_stockAdjustment);
		click(btn_newStockAdjustment);
		Thread.sleep(3000);
		click(btn_copyFromActionVerification);

		sendKeys(txt_StockAdjustmentSerchA, trackCode);
		Thread.sleep(3000);
		pressEnter(txt_StockAdjustmentSerchA);
		Thread.sleep(3000);
		doubleClick(sel_StockAdjustmentSerchCopyselA);

		Thread.sleep(3000);
		click(btn_draft2);

		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageReleaseA)) {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Fail", "fail");
		}
	}

	// IN_SA_039
	public void VerifyThatUserCanSelectExistingStockAdjustmentWhichIsIncludeInNewlyCreatedTemplate() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		selectIndex(txt_WarehouseA, 1);

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, ProductAddTxt2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		sendKeys(txt_ProductAddQtyA, productQuantityA);

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);

		Thread.sleep(6000);
		click(navigation_pane);
		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		click(btn_copyFromActionVerification);
		Thread.sleep(3000);
		selectText(txt_filterListIdA, "Drafted");
		Thread.sleep(3000);
		sendKeys(txt_StockAdjustmentSerchA, trackCode);
		Thread.sleep(3000);
		pressEnter(txt_StockAdjustmentSerchA);
		Thread.sleep(3000);
		doubleClick(sel_StockAdjustmentSerchCopyselA);

		Thread.sleep(3000);
		click(btn_draft2);

		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageReleaseA)) {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Fail", "fail");
		}
	}

	// IN_SA_040
	public void VerifyThatUserCanDuplicateTheDraftedStockAdjustment() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		selectIndex(txt_WarehouseA, 1);

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, ProductAddTxt2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		sendKeys(txt_ProductAddQtyA, productQuantityA);

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_duplicateActionVerification);

		Thread.sleep(5000);
		if (isDisplayed(btn_draft2)) {
			writeTestResults("Verify That User Can Duplicate The Drafted Stock Adjustment",
					"User Can Duplicate The Drafted Stock Adjustment",
					"User Can Duplicate The Drafted Stock Adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify That User Can Duplicate The Drafted Stock Adjustment",
					"User Can Duplicate The Drafted Stock Adjustment",
					"User Can Duplicate The Drafted Stock Adjustment Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_draft2);

		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageReleaseA)) {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Fail", "fail");
		}

	}

	// IN_SA_041
	public void VerifyThatUserCanDuplicateTheReleasedStockAdjustment() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		selectIndex(txt_WarehouseA, 1);

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, ProductAddTxt2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		sendKeys(txt_ProductAddQtyA, productQuantityA);

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageReleaseA)) {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_duplicateActionVerification);

		Thread.sleep(5000);
		if (isDisplayed(btn_draft2)) {
			writeTestResults("Verify that user can duplicate the released stock adjustment",
					"User Can Duplicate The released stock adjustment",
					"User Can Duplicate The released stock adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that user can duplicate the released stock adjustment",
					"User Can Duplicate The released stock adjustment",
					"User Can Duplicate The released stock adjustment Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_draft2);

		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageReleaseA)) {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Fail", "fail");
		}

	}

	// IN_SA_042
	public void VerifyThatQtyWillBeAddedToTheStockWhenDoingPlusAdjustment() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, ProductAddTxt2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		sendKeys(txt_ProductAddQtyA, "50.00");

		Thread.sleep(5000);
		click(widget_checkAvailabilityWhenOneRowIsThere);
		Thread.sleep(3000);
		click(btn_ProductAvailabilityA);

		Thread.sleep(3000);
		float BeforeValue1 = Float.parseFloat(getText(txt_ProductAvailabilityValueA).replace(",", ""));

		float BeforeValue2 = BeforeValue1 + Float.parseFloat("50.00");
		System.out.println("Before Value:" + BeforeValue1);
		JavascriptExecutor j1 = (JavascriptExecutor) driver;
		j1.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageReleaseA)) {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Fail", "fail");
		}

		Thread.sleep(5000);
		click(widget_checkAvailabilityWhenOneRowIsThere);
		Thread.sleep(3000);
		click(btn_ProductAvailabilityA);
		Thread.sleep(3000);
		Float AfterValue = Float.parseFloat(getText(txt_ProductAvailabilityAfterValueA).replace(",", ""));
		System.out.println("After Value:" + AfterValue);
		JavascriptExecutor j2 = (JavascriptExecutor) driver;
		j2.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");

		Thread.sleep(5000);
		if (AfterValue == BeforeValue2) {
			writeTestResults("Verify that Qty will be added to the stock when doing plus adjustment",
					"Qty will be added to the stock when doing plus adjustment",
					"Qty will be added to the stock when doing plus adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that Qty will be added to the stock when doing plus adjustment",
					"Qty will be added to the stock when doing plus adjustment",
					"Qty will be added to the stock when doing plus adjustment Fail", "fail");
		}

	}

	// IN_SA_043
	public void VerifyThatUserAllowToReverseThePlusStockAdjustment() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, ProductAddTxt2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		sendKeys(txt_ProductAddQtyA, "50.00");

		Thread.sleep(5000);
		click(widget_checkAvailabilityWhenOneRowIsThere);
		Thread.sleep(3000);
		click(btn_ProductAvailabilityA);

		Thread.sleep(3000);
		float BeforeValue1 = Float.parseFloat(getText(txt_ProductAvailabilityValueA).replace(",", ""));

		float BeforeValue2 = BeforeValue1 + Float.parseFloat("50.00");
		System.out.println("Before Value:" + BeforeValue1);
		JavascriptExecutor j1 = (JavascriptExecutor) driver;
		j1.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageReleaseA)) {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Fail", "fail");
		}

		Thread.sleep(5000);
		click(widget_checkAvailabilityWhenOneRowIsThere);
		Thread.sleep(3000);
		click(btn_ProductAvailabilityA);
		Thread.sleep(3000);
		Float AfterValue1 = Float.parseFloat(getText(txt_ProductAvailabilityAfterValueA).replace(",", ""));
		System.out.println("After Value:" + AfterValue1);
		JavascriptExecutor j2 = (JavascriptExecutor) driver;
		j2.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");

		Thread.sleep(5000);
		if (AfterValue1 == BeforeValue2) {
			writeTestResults("Verify that Qty will be added to the stock when doing plus adjustment",
					"Qty will be added to the stock when doing plus adjustment",
					"Qty will be added to the stock when doing plus adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that Qty will be added to the stock when doing plus adjustment",
					"Qty will be added to the stock when doing plus adjustment",
					"Qty will be added to the stock when doing plus adjustment Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_action);
		Thread.sleep(3000);
		click(btn_reverseActionVrification);
		Thread.sleep(3000);
		click(btn_reversebtnA);
		Thread.sleep(3000);
		click(btn_reverseConfirationActionVerification);

		Thread.sleep(5000);
		if (getText(lbl_docStatus).contentEquals("( Reversed )")) {
			writeTestResults("Verify that stock adjustment Reverced", "user can Reverced stock adjustment",
					"user can Reverced stock adjustment Successfully", "pass");

		} else {
			writeTestResults("Verify that stock adjustment Reverced", "user can Reverced stock adjustment",
					"user can't Reverced stock adjustment", "fail");
		}

		Thread.sleep(5000);
		click(widget_checkAvailabilityWhenOneRowIsThere);
		Thread.sleep(3000);
		click(btn_ProductAvailabilityA);
		Thread.sleep(3000);
		Float AfterValue2 = Float.parseFloat(getText(txt_ProductAvailabilityAfterValueA).replace(",", ""));
		System.out.println("After Value:" + AfterValue2);
		JavascriptExecutor j3 = (JavascriptExecutor) driver;
		j3.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");

		Thread.sleep(5000);
		if (AfterValue2 == BeforeValue1) {
			writeTestResults(
					"Verify that User allow to reverse the plus stock adjustment and Qty will be deducted from the stock",
					"allow to reverse the plus stock adjustment and Qty will be deducted from the stock",
					"allow to reverse the plus stock adjustment and Qty will be deducted from the stock Successfull",
					"pass");

		} else {
			writeTestResults(
					"Verify that User allow to reverse the plus stock adjustment and Qty will be deducted from the stock",
					"allow to reverse the plus stock adjustment and Qty will be deducted from the stock",
					"allow to reverse the plus stock adjustment and Qty will be deducted from the stock Fail", "fail");
		}
	}

	// IN_SA_044
	public void VerifyThatQtyWillBeAddedToTheStockWhenDoingMinusAdjustment() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, ProductAddTxt2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		sendKeys(txt_ProductAddQtyA, "-100.00");

		Thread.sleep(5000);
		click(widget_checkAvailabilityWhenOneRowIsThere);
		Thread.sleep(3000);
		click(btn_ProductAvailabilityA);

		Thread.sleep(3000);
		float BeforeValue1 = Float.parseFloat(getText(txt_ProductAvailabilityValueA).replace(",", ""));

		float BeforeValue2 = BeforeValue1 + Float.parseFloat("-100.00");
		System.out.println("Before Value:" + BeforeValue1);
		JavascriptExecutor j1 = (JavascriptExecutor) driver;
		j1.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageReleaseA)) {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Fail", "fail");
		}

		Thread.sleep(5000);
		click(widget_checkAvailabilityWhenOneRowIsThere);
		Thread.sleep(3000);
		click(btn_ProductAvailabilityA);
		Thread.sleep(3000);
		Float AfterValue = Float.parseFloat(getText(txt_ProductAvailabilityAfterValueA).replace(",", ""));
		System.out.println("After Value:" + AfterValue);
		JavascriptExecutor j2 = (JavascriptExecutor) driver;
		j2.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");

		Thread.sleep(5000);
		if (AfterValue == BeforeValue2) {
			writeTestResults("Verify that Qty will be added to the stock when doing minus adjustment",
					"Qty will be added to the stock when doing minus adjustment",
					"Qty will be added to the stock when doing minus adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that Qty will be added to the stock when doing minus adjustment",
					"Qty will be added to the stock when doing minus adjustment",
					"Qty will be added to the stock when doing minus adjustment Fail", "fail");
		}
	}

	// IN_SA_045
	public void VerifyThatUserAllowToReverseTheMinusStockAdjustment() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, ProductAddTxt2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		sendKeys(txt_ProductAddQtyA, "-100.00");

		Thread.sleep(5000);
		click(widget_checkAvailabilityWhenOneRowIsThere);
		Thread.sleep(3000);
		click(btn_ProductAvailabilityA);

		Thread.sleep(3000);
		float BeforeValue1 = Float.parseFloat(getText(txt_ProductAvailabilityValueA).replace(",", ""));

		float BeforeValue2 = BeforeValue1 + Float.parseFloat("-100.00");
		System.out.println("Before Value:" + BeforeValue1);
		JavascriptExecutor j1 = (JavascriptExecutor) driver;
		j1.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageReleaseA)) {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Fail", "fail");
		}

		Thread.sleep(5000);
		click(widget_checkAvailabilityWhenOneRowIsThere);
		Thread.sleep(3000);
		click(btn_ProductAvailabilityA);
		Thread.sleep(3000);
		Float AfterValue1 = Float.parseFloat(getText(txt_ProductAvailabilityAfterValueA).replace(",", ""));
		System.out.println("After Value:" + AfterValue1);
		JavascriptExecutor j2 = (JavascriptExecutor) driver;
		j2.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");

		Thread.sleep(5000);
		if (AfterValue1 == BeforeValue2) {
			writeTestResults("Verify that Qty will be added to the stock when doing minus stock adjustment",
					"Qty will be added to the stock when doing minus stock adjustment",
					"Qty will be added to the stock when doing minus stock adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that Qty will be added to the stock when doing minus stock adjustment",
					"Qty will be added to the stock when doing minus stock adjustment",
					"Qty will be added to the stock when doing minus stock adjustment Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_action);
		Thread.sleep(3000);
		click(btn_reverseActionVrification);
		Thread.sleep(3000);
		click(btn_reversebtnA);
		Thread.sleep(3000);
		click(btn_reverseConfirationActionVerification);

		Thread.sleep(5000);
		if (getText(lbl_docStatus).contentEquals("( Reversed )")) {
			writeTestResults("Verify that stock adjustment Reverced", "user can Reverced stock adjustment",
					"user can Reverced stock adjustment Successfully", "pass");

		} else {
			writeTestResults("Verify that stock adjustment Reverced", "user can Reverced stock adjustment",
					"user can't Reverced stock adjustment", "fail");
		}

		Thread.sleep(5000);
		click(widget_checkAvailabilityWhenOneRowIsThere);
		Thread.sleep(3000);
		click(btn_ProductAvailabilityA);
		Thread.sleep(3000);
		Float AfterValue2 = Float.parseFloat(getText(txt_ProductAvailabilityAfterValueA).replace(",", ""));
		System.out.println("After Value:" + AfterValue2);
		JavascriptExecutor j3 = (JavascriptExecutor) driver;
		j3.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");

		Thread.sleep(5000);
		if (AfterValue2 == BeforeValue1) {
			writeTestResults(
					"Verify that User allow to reverse the minus stock adjustment and Qty will be added to the stock again",
					"allow to reverse the minus stock adjustment and Qty will be added to the stock again",
					"allow to reverse the minus stock adjustment and Qty will be added to the stock again Successfull",
					"pass");

		} else {
			writeTestResults(
					"Verify that User allow to reverse the minus stock adjustment and Qty will be added to the stock again",
					"allow to reverse the minus stock adjustment and Qty will be added to the stock again",
					"allow to reverse the minus stock adjustment and Qty will be added to the stock again Fail",
					"fail");
		}
	}

	// IN_SA_046
	public void VerifyThatUserAllowToDoBothPlusAndMinusAdjustmentWithSameStockAdjustment() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, Pro1A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		sendKeys(txt_ProductAddQtyA, "-100.00");

		Thread.sleep(5000);
		click(widget_checkAvailabilityWhenOneRowIsThere);
		Thread.sleep(3000);
		click(btn_ProductAvailabilityA);

		Thread.sleep(3000);
		float BeforeValue1 = Float.parseFloat(getText(txt_ProductAvailabilityValueA).replace(",", ""));

		float BeforeValue2 = BeforeValue1 + Float.parseFloat("-100.00");
		System.out.println("Before Value:" + BeforeValue1);
		JavascriptExecutor j1 = (JavascriptExecutor) driver;
		j1.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");

		Thread.sleep(3000);
		click(btn_ProductAdd2A);
		Thread.sleep(3000);
		click(btn_Product2A);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, Pro2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		sendKeys(txt_ProductAddQty2A, "100.00");

		Thread.sleep(5000);
		click(btn_MasterInfo2A);
		Thread.sleep(3000);
		click(btn_ProductAvailabilityA);

		Thread.sleep(3000);
		float BeforeValue3 = Float.parseFloat(getText(txt_ProductAvailabilityValueA).replace(",", ""));

		float BeforeValue4 = BeforeValue3 + Float.parseFloat("100.00");
		System.out.println("Before Value:" + BeforeValue3);
		JavascriptExecutor j2 = (JavascriptExecutor) driver;
		j2.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageReleaseA)) {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Fail", "fail");
		}

		Thread.sleep(5000);
		click(widget_checkAvailabilityWhenOneRowIsThere);
		Thread.sleep(3000);
		click(btn_ProductAvailabilityA);
		Thread.sleep(3000);
		Float AfterValue = Float.parseFloat(getText(txt_ProductAvailabilityAfterValueA).replace(",", ""));
		System.out.println("After Value:" + AfterValue);
		JavascriptExecutor j3 = (JavascriptExecutor) driver;
		j3.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");

		Thread.sleep(5000);
		if (AfterValue == BeforeValue2) {
			writeTestResults("Verify that Qty will be added to the stock when doing minus adjustment",
					"Qty will be added to the stock when doing minus adjustment",
					"Qty will be added to the stock when doing minus adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that Qty will be added to the stock when doing minus adjustment",
					"Qty will be added to the stock when doing minus adjustment",
					"Qty will be added to the stock when doing minus adjustment Fail", "fail");
		}

		Thread.sleep(5000);
		click(btn_AfterMasterInfo2A);
		Thread.sleep(3000);
		click(btn_ProductAvailabilityA);
		Thread.sleep(3000);
		Float AfterValue2 = Float.parseFloat(getText(txt_ProductAvailabilityAfterValueA).replace(",", ""));
		System.out.println("After Value:" + AfterValue2);
		JavascriptExecutor j4 = (JavascriptExecutor) driver;
		j4.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");

		Thread.sleep(5000);
		if (AfterValue2 == BeforeValue4) {
			writeTestResults("Verify that Qty will be added to the stock when doing plus adjustment",
					"Qty will be added to the stock when doing plus adjustment",
					"Qty will be added to the stock when doing plus adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that Qty will be added to the stock when doing plus adjustment",
					"Qty will be added to the stock when doing plus adjustment",
					"Qty will be added to the stock when doing plus adjustment Fail", "fail");
		}
	}

	// IN_SA_047
	public void VerifyThatUserCannotReverseTheStockAdjustmentIfUseTheStock() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		LocalTime myObj = LocalTime.now();

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, "20200306095706-SERIEL-SPECIFIC");
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_SerialBatchA);
		Thread.sleep(3000);
		click(btn_ItemNumber1A);
		Thread.sleep(3000);
		sendKeys(txt_SerialNoA, SerialNoA + myObj);
		Thread.sleep(3000);
		sendKeys(txt_LotNoA, LotNoA + myObj);
		Thread.sleep(3000);
		click(txt_ExpiryDateA);
		Thread.sleep(3000);
		selectIndex(txt_ExpiryDateNextYearA, 78);
		Thread.sleep(3000);
		doubleClick(sel_ExpiryDateSelA);
		Thread.sleep(3000);
		pressEnter(txt_SerialNoA);
		Thread.sleep(3000);
		click(btn_updateSerialA);
		Thread.sleep(3000);
		click(btn_closeProductList);

		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);
		String Val1 = trackCode;
		Thread.sleep(5000);
		if (isDisplayed(txt_pageReleaseA)) {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_StockAdjustmentByPageA);
		click(btn_newStockAdjustment);

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, "20200306095706-SERIEL-SPECIFIC");
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		sendKeys(txt_ProductAddQtyA, "-1");

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_SerialBatchA);
		Thread.sleep(3000);
		click(btn_ItemNumber1A);
		Thread.sleep(3000);
		sendKeys(txt_SerialNoA, SerialNoA + myObj);
		Thread.sleep(3000);
		pressEnter(txt_SerialNoA);
		Thread.sleep(3000);
		click(btn_updateSerialA);
		Thread.sleep(3000);
		click(btn_closeProductList);

		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageReleaseA)) {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_StockAdjustmentByPageA);

		sendKeys(txt_StockAdjustmentSerchA, Val1);
		Thread.sleep(3000);
		pressEnter(txt_StockAdjustmentSerchA);
		Thread.sleep(3000);
		doubleClick(sel_StockAdjustmentSerchselA);

		Thread.sleep(3000);
		click(btn_action);
		Thread.sleep(3000);
		click(btn_reverseActionVrification);
		Thread.sleep(3000);
		click(btn_reversebtnA);
		Thread.sleep(3000);
		click(btn_reverseConfirationActionVerification);

		Thread.sleep(5000);
		if (isDisplayed(txt_ValidatorReverceA)) {
			writeTestResults(
					"Verify that user cannot reverse the stock adjustment if use the stock for any transactions",
					"user cannot reverse the stock adjustment if use the stock for any transactions",
					"user cannot reverse the stock adjustment if use the stock for any transactions Successfull",
					"pass");

		} else {
			writeTestResults(
					"Verify that user cannot reverse the stock adjustment if use the stock for any transactions",
					"user cannot reverse the stock adjustment if use the stock for any transactions",
					"user cannot reverse the stock adjustment if use the stock for any transactions Fail", "fail");
		}
	}

	// IN_SA_048
	public void VerifyThatUserCanViewHistoryOfaDraftedOrReleasedStockAdjustment() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, ProductAddTxt2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_action);
		Thread.sleep(3000);
		click(btn_historyActionVerification);

		Thread.sleep(5000);
		if (isDisplayed(txt_historyForDraftA)) {
			writeTestResults("Verify that user can view history of a Drafted Stock Adjustment",
					"user can view history of a Drafted Stock Adjustment",
					"user can view history of a Drafted Stock Adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that user can view history of a Drafted Stock Adjustment",
					"user can view history of a Drafted Stock Adjustment",
					"user can view history of a Drafted Stock Adjustment Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageReleaseA)) {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_action);
		Thread.sleep(3000);
		click(btn_historyActionVerification);

		Thread.sleep(5000);
		if (isDisplayed(txt_historyForDraftA)) {
			writeTestResults("Verify that user can view history of a Released Stock Adjustment",
					"user can view history of a Released Stock Adjustment",
					"user can view history of a Released Stock Adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that user can view history of a Released Stock Adjustment",
					"user can view history of a Released Stock Adjustment",
					"user can view history of a Released Stock Adjustment Fail", "fail");
		}
	}

	// IN_SA_049
	public void VerifyThatJournalEntryUpdateAccordingToTheStockValues() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, Pro1A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		sendKeys(txt_ProductAddQtyA, "100.00");

		Thread.sleep(3000);
		click(btn_ProductAdd2A);
		Thread.sleep(3000);
		click(btn_Product2A);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, Pro2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		sendKeys(txt_ProductAddQty2A, "-100.00");

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageReleaseA)) {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_action);
		Thread.sleep(3000);
		click(btn_journelActionVerification);

	}

	// IN_SA_050
	public void VerifyThatAveragePriceIsDisplayedInCostPerUnitField() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, Pro1A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		sendKeys(txt_ProductAddQtyA, productQuantityA);

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageReleaseA)) {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Fail", "fail");
		}

		System.out.println(getText(txt_CostPerUnitAfterValueA));

		Thread.sleep(3000);
		click(widget_checkAvailabilityWhenOneRowIsThere);
		Thread.sleep(3000);
		click(btn_ProductRelatedPriceA);
		System.out.println(getText(txt_ProductRelatedPriceValueA));

		Thread.sleep(5000);
		if (getText(txt_CostPerUnitAfterValueA).equals(getText(txt_ProductRelatedPriceValueA))) {
			writeTestResults("Verify that Average price is displayed in Cost per Unit field",
					"Average price is displayed in Cost per Unit field",
					"Average price is displayed in Cost per Unit field Successfull", "pass");

		} else {
			writeTestResults("Verify that Average price is displayed in Cost per Unit field",
					"Average price is displayed in Cost per Unit field",
					"Average price is displayed in Cost per Unit field Fail", "fail");
		}
	}

	// IN_SA_051
	public void VerifyThatUserCanInboundStockForParticularRacksAndRackwiseStockWillUpdate() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		Thread.sleep(3000);
		click(btn_ProductAddA);
		Thread.sleep(3000);
		sendKeys(txt_ProductAddTxtA, Pro2A);
		Thread.sleep(3000);
		pressEnter(txt_ProductAddTxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProductAddSelA);

		Thread.sleep(3000);
		selectText(btn_RackA, "--Free Area--");

		Thread.sleep(3000);
		sendKeys(txt_ProductAddQtyA, productQuantityA);

		Thread.sleep(3000);
		click(widget_checkAvailabilityWhenOneRowIsThere);
		Thread.sleep(3000);
		click(btn_RackwiseAvailableQtyA);
		Thread.sleep(3000);
		String Val1 = getText(txt_RackwiseAvailableQtyValueA).replace(",", "");
		Thread.sleep(3000);
		int RW1 = Integer.parseInt(Val1) + Integer.parseInt(productQuantityA);
		System.out.println(RW1);
		JavascriptExecutor j9 = (JavascriptExecutor) driver;
		Thread.sleep(3000);
		j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageReleaseA)) {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Fail", "fail");
		}

		Thread.sleep(3000);
		click(widget_checkAvailabilityWhenOneRowIsThere);
		Thread.sleep(3000);
		click(btn_RackwiseAvailableQtyA);
		Thread.sleep(3000);
		int RW2 = Integer.parseInt(getText(txt_RackwiseAvailableQtyValueA).replace(",", ""));
		System.out.println(RW2);
		Thread.sleep(3000);
		j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");

		Thread.sleep(5000);
		if (RW1 == RW2) {
			writeTestResults("Verify that user can inbound stock for particular racks and rackwise stock will update",
					"user can inbound stock for particular racks and rackwise stock will update",
					"user can inbound stock for particular racks and rackwise stock will update Successfull", "pass");

		} else {
			writeTestResults("Verify that user can inbound stock for particular racks and rackwise stock will update",
					"user can inbound stock for particular racks and rackwise stock will update",
					"user can inbound stock for particular racks and rackwise stock will update Fail", "fail");
		}

	}

	// IN_SA_052
	public void VerifyThatSystemWillNotAllowToAddStockForMainProductWhichHavingVariants() throws Exception {

		click(inventory_module);
		click(btn_productInformation);

		Thread.sleep(3000);
		sendKeys(txt_ProductSerchA, SampleProA);
		Thread.sleep(3000);
		pressEnter(txt_ProductSerchA);
		Thread.sleep(3000);
		doubleClick(sel_ProductSerchSelA);

		Thread.sleep(3000);
		click(btn_duplicateActionVerification);

		Thread.sleep(3000);
		LocalTime c1 = LocalTime.now();
		sendKeys(txt_product_code, SampleProA + c1);

		Thread.sleep(3000);
		click(btn_draft2);

		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(3000);
		click(btn_ClassificationA);

		Thread.sleep(3000);
		click(btn_VariantGroupbtnA);
		Thread.sleep(3000);
		sendKeys(txt_VariantGrouptxtA, VariantGrouptxtA);
		Thread.sleep(3000);
		pressEnter(txt_VariantGrouptxtA);
		Thread.sleep(3000);
		doubleClick(sel_VariantGroupselA);

		Thread.sleep(3000);
		selectIndex(txt_VariantGroupGradeA, 1);
		Thread.sleep(3000);
		click(btn_VariantUpdateA);

		Thread.sleep(6000);
		click(navigation_pane);

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		ScrollDownAruna();

		sendKeys(txt_ProductA, SampleProA + c1);
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		sendKeys(txt_ProductQtyA, "1");
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

	}

	// IN_SA_053
	public void VerifyThatValidationWillAppearWithoutTakingTimesWhenCapturingExistingSerialBatches() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		ScrollDownAruna();

		sendKeys(txt_ProductA, "20200306095706-SERIEL-SPECIFIC");
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		sendKeys(txt_ProductQtyA, "1");
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_releese2);

		Thread.sleep(5000);
		if (isDisplayed(txt_ValidatorReleasedA)) {
			writeTestResults(
					"Verify that validation will appear without taking times when capturing existing serial/Batches",
					"validation will appear without taking times when capturing existing serial/Batches",
					"validation will appear without taking times when capturing existing serial/Batches Successfull",
					"pass");

		} else {
			writeTestResults(
					"Verify that validation will appear without taking times when capturing existing serial/Batches",
					"validation will appear without taking times when capturing existing serial/Batches",
					"validation will appear without taking times when capturing existing serial/Batches Fail", "fail");
		}
	}

	// IN_SA_054
	public void VerifyThatUserAllowsToAddSameProductMultipleTimesWithDifferentSerialBatchesLots() throws Exception {

		VerifyThatNavigationToStockAdjustmentNewpage();

		Thread.sleep(3000);
		sendKeys(txt_DescriptionA, "Testing1.1");

		ScrollDownAruna();

		sendKeys(txt_ProductA, "20200306095706-SERIEL-SPECIFIC");
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		sendKeys(txt_ProductQtyA, "1");
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

		sendKeys(txt_ProductA, "20200306095706-SERIEL-SPECIFIC");
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		sendKeys(txt_ProductQtyA, "1");
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

		Thread.sleep(5000);
		click(widget_checkAvailabilityWhenOneRowIsThere);
		Thread.sleep(3000);
		click(btn_ProductAvailabilityA);

		Thread.sleep(3000);
		float BeforeValue1 = Float.parseFloat(getText(txt_ProductAvailabilityValueA).replace(",", ""));
		float BeforeValue2 = BeforeValue1 + Float.parseFloat("2");
		System.out.println("Before Value:" + BeforeValue1);
		JavascriptExecutor j1 = (JavascriptExecutor) driver;
		j1.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");

		Thread.sleep(3000);
		click(btn_draft2);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageDraftA)) {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Successfull", "pass");

		} else {
			writeTestResults("Verify that user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields",
					"user can draft the stock adjustment with having mandatory fields Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_SerialBatchA);
		Thread.sleep(3000);
		click(btn_ItemNumber1A);
		Thread.sleep(3000);
		LocalTime myObj1 = LocalTime.now();
		sendKeys(txt_SerialNoA, SerialNoA + myObj1);
		Thread.sleep(3000);
		sendKeys(txt_LotNoA, LotNoA + myObj1);
		Thread.sleep(3000);
		click(txt_ExpiryDateA);
		Thread.sleep(3000);
		selectIndex(txt_ExpiryDateNextYearA, 78);
		Thread.sleep(3000);
		doubleClick(sel_ExpiryDateSelA);
		Thread.sleep(3000);
		pressEnter(txt_SerialNoA);
		Thread.sleep(3000);
		click(btn_updateSerialA);

		Thread.sleep(3000);
		click(btn_ItemNumber2A);
		Thread.sleep(3000);
		LocalTime myObj2 = LocalTime.now();
		sendKeys(txt_SerialNoA, SerialNoA + myObj2);
		Thread.sleep(3000);
		sendKeys(txt_LotNoA, LotNoA + myObj2);
		Thread.sleep(3000);
		click(txt_ExpiryDateA);
		Thread.sleep(3000);
		selectIndex(txt_ExpiryDateNextYearA, 78);
		Thread.sleep(3000);
		doubleClick(sel_ExpiryDateSelA);
		Thread.sleep(3000);
		pressEnter(txt_SerialNoA);
		Thread.sleep(3000);
		click(btn_updateSerialA);

		Thread.sleep(3000);
		click(btn_closeProductList);

		Thread.sleep(3000);
		click(btn_releese2);
		Thread.sleep(2000);
		trackCode = getText(txt_HeaderA);
		Thread.sleep(5000);
		if (isDisplayed(txt_pageReleaseA)) {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Successfull", "pass");

		} else {
			writeTestResults("Verify that user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment",
					"user can release the drafted stock adjustment Fail", "fail");
		}

		Thread.sleep(5000);
		click(widget_checkAvailabilityWhenOneRowIsThere);
		Thread.sleep(3000);
		click(btn_ProductAvailabilityA);
		Thread.sleep(3000);
		Float AfterValue = Float.parseFloat(getText(txt_ProductAvailabilityAfterValueA).replace(",", ""));
		System.out.println("After Value:" + AfterValue);
		JavascriptExecutor j3 = (JavascriptExecutor) driver;
		j3.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");

		Thread.sleep(5000);
		if (AfterValue == BeforeValue2) {
			writeTestResults(
					"Verify that user allows to add same product multiple times with different serial/Batches/Lots",
					"user allows to add same product multiple times with different serial/Batches/Lots",
					"user allows to add same product multiple times with different serial/Batches/Lots Successfull",
					"pass");

		} else {
			writeTestResults(
					"Verify that user allows to add same product multiple times with different serial/Batches/Lots",
					"user allows to add same product multiple times with different serial/Batches/Lots",
					"user allows to add same product multiple times with different serial/Batches/Lots Fail", "fail");
		}
	}

	// scrollDown

	public void ScrollDownAruna() throws Exception {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,400)");
	}
}