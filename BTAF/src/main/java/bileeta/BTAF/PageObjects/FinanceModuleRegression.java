package bileeta.BTAF.PageObjects;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.ClickAction;
import org.openqa.selenium.support.ui.Select;

import bileeta.BATF.Pages.FinanceModuleData;

public class FinanceModuleRegression extends FinanceModuleData {
	/* common objects */
	private InventoryAndWarehouseModuleRegression2 invObjReg2 = new InventoryAndWarehouseModuleRegression2();
	private InventoryAndWarehouseModuleRegression invObjReg = new InventoryAndWarehouseModuleRegression();
	private InventoryAndWarehouseModule invObj = new InventoryAndWarehouseModule();
	private FinanceModule finObj = new FinanceModule();

	/* Getters and Setters */
	public InventoryAndWarehouseModule getInvObj() {
		return invObj;
	}

	public InventoryAndWarehouseModuleRegression2 getInvObjReg2() {
		return invObjReg2;
	}

	public InventoryAndWarehouseModuleRegression getInvObjReg() {
		return invObjReg;
	}

	public FinanceModule getFinObj() {
		return finObj;
	}

	/* Variables */
	String customerRecipt;
	String vendorRecipt;
	String glRecipt;
	String employeeRecipt;
	String releasedBankAdjustment_FIN_BA_4_7_8_9;
	String releasedCashBankBook_FIN_CBB_1_4_5_6_10;
	String bankAccount_FIN_CBB_25_26_27;

	/* FIN_BD_1_2 */
	public void navigateToBankDepositFormAndValidateMandatoryFields_FIN_BD_1_2() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_bankDeposit, 30);
		if (isDisplayed(btn_bankDeposit)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}

		click(btn_bankDeposit);
		explicitWait(header_bankDeposit, 30);
		if (isDisplayed(header_bankDeposit)) {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page",
					"User successfully navigate to Bank Deposit by page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page", "User doesn't navigate to Bank Deposit by page",
					"fail");
		}

		click(btn_newBankDeposit);
		explicitWait(header_newBankDeposit, 30);
		if (isDisplayed(header_newBankDeposit)) {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page successfully displayed",
					"pass");
		} else {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page doesn't displayed", "fail");
		}

		click(btn_draft);
		Thread.sleep(2000);

		if (isDisplayed(lbl_mainValidation)) {
			writeTestResults("Verify main validation is appear", "Main validation should appear",
					"Main validation successfully appear", "pass");
		} else {
			writeTestResults("Verify main validation is appear", "Main validation should appear",
					"Main validation doesn't appear", "fail");
		}

		changeCSS(btn_closeMainValidation, "z-index:-9999");

		Thread.sleep(1000);

		if (isDisplayed(lbl_errorPayMethodDropDown)) {
			writeTestResults("Verify payment method mandatory field validation is apper",
					"Payment method mandatory fiald validation should apper",
					"Payment method mandatory fiald validation successfully appear", "pass");
		} else {
			writeTestResults("Verify payment method mandatory field validation is apper",
					"Payment method mandatory fiald validation should apper",
					"Payment method mandatory fiald validation doesn't appear", "fail");
		}

		if (isDisplayed(drop_payMethodWithErrorBorder)) {
			writeTestResults("Verify payment method mandatory field error border is apper",
					"Payment method mandatory fiald error border should apper",
					"Payment method mandatory fiald error border successfully appear", "pass");
		} else {
			writeTestResults("Verify payment method mandatory field error border is apper",
					"Payment method mandatory fiald error border should apper",
					"Payment method mandatory fiald error border doesn't appear", "fail");
		}

		if (isDisplayed(lbl_errorBankDropDown)) {
			writeTestResults("Verify bank dropdown mandatory field validation is apper",
					"Bank dropdown mandatory fiald validation should apper",
					"Bank dropdown mandatory fiald validation successfully appear", "pass");
		} else {
			writeTestResults("Verify bank dropdown mandatory field validation is apper",
					"Bank dropdown mandatory fiald validation should apper",
					"Bank dropdown mandatory fiald validation doesn't appear", "fail");
		}

		if (isDisplayed(drop_BankWithErrorBorder)) {
			writeTestResults("Verify bank dropdown mandatory field error border is apper",
					"Bank dropdown mandatory fiald error border should apper",
					"Bank dropdown mandatory fiald error border successfully appear", "pass");
		} else {
			writeTestResults("Verify bank dropdown mandatory field error border is apper",
					"Bank dropdown mandatory fiald error border should apper",
					"Bank dropdown mandatory fiald error border doesn't appear", "fail");
		}

		if (isDisplayed(lbl_errorBankAccountDropDown)) {
			writeTestResults("Verify bank account mandatory field validation is apper",
					"Bank account mandatory fiald validation should apper",
					"Bank account mandatory fiald validation successfully appear", "pass");
		} else {
			writeTestResults("Verify bank account mandatory field validation is apper",
					"Bank account mandatory fiald validation should apper",
					"Bank account mandatory fiald validation doesn't appear", "fail");
		}

		click(btn_bankDepositGridErrorOpen);
		Thread.sleep(1000);
		if (isDisplayed(lbl_gridErroeBallonBankDeposit)) {
			writeTestResults("Verify Bank Deposit grid error is appear", "Bank Deposit grid error should appear",
					"Bank Deposit grid error successfully appear", "pass");
		} else {
			writeTestResults("Verify Bank Deposit grid error is appear", "Bank Deposit grid error should appear",
					"Bank Deposit grid error doesn't appear", "fail");
		}
	}

	/* FIN_BD_3_4_5_6_7_10 */
	public void bankDepositBasicFields_FIN_BD_3_4_5_6_7_10() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_bankDeposit, 30);
		if (isDisplayed(btn_bankDeposit)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}

		click(btn_bankDeposit);
		explicitWait(header_bankDeposit, 30);
		if (isDisplayed(header_bankDeposit)) {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page",
					"User successfully navigate to Bank Deposit by page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page", "User doesn't navigate to Bank Deposit by page",
					"fail");
		}

		click(btn_newBankDeposit);
		explicitWait(header_newBankDeposit, 30);
		if (isDisplayed(header_newBankDeposit)) {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page successfully displayed",
					"pass");
		} else {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page doesn't displayed", "fail");
		}

		selectText(drop_payMethodBankDeposi, payMethodCash);
		String selectedComboValue = getSelectedOptionInDropdown(drop_payMethodBankDeposi);
		if (selectedComboValue.equals(payMethodCash)) {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User successfully select any Pay Method from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User doesn't select any Pay Method from the drop down where the field will populate the selected value",
					"fail");
		}

		sendKeys(txt_descriptinArea, description);
		String descriptionStr = getAttribute(txt_descriptinArea, "value");
		if (descriptionStr.equals(description)) {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User successfully enter characters, numbers, alphanumeric and special characters to Description",
					"pass");
		} else {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User doesn't enter characters, numbers, alphanumeric and special characters to Description",
					"fail");
		}

		selectText(drop_bankNameBD, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bankNameBD);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_accountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_accountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		/* Get account according to bank */
		((JavascriptExecutor) driver).executeScript("window.open('about:blank','_blank');");
		switchWindow();
		openPage(site_Url);
		explicitWait(navigation_pane, 30);
		Thread.sleep(2000);
		explicitWait(navigation_pane, 30);
		click(navigation_pane);
		Thread.sleep(500);
		click(btn_financeModule);
		Thread.sleep(500);
		click(btn_cashBankBook);
		explicitWait(txt_searchCashBankBook, 40);
		sendKeys(txt_searchCashBankBook, sampathBank);
		pressEnter(txt_searchCashBankBook);
		Thread.sleep(1500);
		explicitWait(td_bankAccountRowReplace.replace("row", "1"), 40);
		Thread.sleep(2000);

		int recCount = getCount(count_bankAccount);

		List<String> accountsOnByPage = new ArrayList<String>();
		accountsOnByPage.add("--None--");

		for (int i = 1; i <= recCount; i++) {
			String record = getText(td_bankAccountRowReplace.replace("row", String.valueOf(i)));
			try {
				accountsOnByPage.add(record);
			} catch (Exception e) {
				System.out.println(e);
			}

		}

		switchWindow();
		List<String> bankAccountOnDropdown = new ArrayList<String>();
		Select dropBank = new Select(driver.findElement(By.xpath(drop_accountNo)));
		List<WebElement> op = dropBank.getOptions();
		int size = op.size();
		for (int i = 0; i < size; i++) {
			String options = op.get(i).getText();
			bankAccountOnDropdown.add(options);
		}

		List<String> noneOption = new ArrayList<String>();
		noneOption.add("--None--");
		if (!accountsOnByPage.equals(noneOption)) {
			if (accountsOnByPage.equals(bankAccountOnDropdown)) {
				writeTestResults("Verify that drop down only contains the account nos relate to the selected bank",
						"Drop down should contains the account nos relate to the selected bank only",
						"Drop down only contains the account nos relate to the selected bank", "pass");
			} else {
				writeTestResults("Verify that drop down only contains the account nos relate to the selected bank",
						"Drop down should contains the account nos relate to the selected bank only",
						"Drop down not only contains the account nos relate to the selected bank", "fail");
			}
		} else {
			writeTestResults("Verify that accounts are available relevant to the bank",
					"At least one account should be available relevent to the bank",
					"There are no accounts available relevent to the bank", "fail");
		}

		selectText(drop_paymentMethod, payMethodCheque);
		if (getSelectedOptionInDropdown(drop_paymentMethod).equals(payMethodCheque)) {
			writeTestResults("Verify user can assign cheque pay method", "Should be ble to assign cheque pay method",
					"User successfully assign cheque pay method", "pass");
		} else {
			writeTestResults("Verify user can assign cheque pay method", "Should be ble to assign cheque pay method",
					"User doesn't assign cheque pay method", "fail");
		}

		String diabilityOfCashBook = getAttribute(drop_cashBookFilter, "disabled");
		if (diabilityOfCashBook.equals("true")) {
			writeTestResults("Verify user not able to access the cash book",
					"User hould not be able to access the cash book", "User not able to access the cash book", "pass");
		} else {
			writeTestResults("Verify user not able to access the cash book",
					"User hould not be able to access the cash book", "User able to access the cash book", "fail");
		}
	}

	/* FIN_BD_9 */
	public void inboundPaymentAdvice_FIN_BD_9() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_inboundPaymentAdvice, 30);
		if (isDisplayed(btn_inboundPaymentAdvice)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}
		click(btn_inboundPaymentAdvice);
		explicitWait(btn_newInboundPaymentAdvice, 30);
		if (isDisplayed(btn_newInboundPaymentAdvice)) {

			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User successfully navigate to Inbound payment advice by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User could'nt navigate to Inbound payment advice by-page", "fail");
		}

		click(btn_newInboundPaymentAdvice);
		if (isDisplayed(btn_CusAdvanceJourneyIPA)) {

			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User successfully view the list of journey",
					"pass");
		} else {
			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User could'nt view the list of journey",
					"fail");
		}

		click(btn_CusAdvanceJourneyIPA);
		explicitWait(lnl_postBusinessUnitIPA, 30);
		if (isDisplayed(lnl_postBusinessUnitIPA)) {

			writeTestResults("Verify user able to navigate to Customer Advance/ Customer Debit Memo",
					"User should be able to navigate to Customer Advance/ Customer Debit Memo",
					"User successfully navigate to Customer Advance/ Customer Debit Memo", "pass");
		} else {
			writeTestResults("Verify user able to navigate to Customer Advance/ Customer Debit Memo",
					"User should be able to navigate to Customer Advance/ Customer Debit Memo",
					"User doesn't navigate to Customer Advance/ Customer Debit Memo", "fail");
		}

		Thread.sleep(1000);
		click(lnl_postBusinessUnitIPA);
		Thread.sleep(700);
		selectText(drop_postBusinessUnit, postBusinessUnitSch);
		Thread.sleep(700);
		click(tab_subbaryIPA);
		Thread.sleep(2000);
		String get_post_business_unit = getText(lnl_postBusinessUnitIPA);
		System.out.println(get_post_business_unit);
		if (get_post_business_unit.equals(postBusinessUnitSch)) {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User successfully select post business unit from the drop down", "pass");
		} else {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User could'nt select post business unit from the drop down", "fail");
		}

		click(btn_searchCustomerIPA);
		if (isDisplayed(txt_cusAccountSearchIPA)) {
			writeTestResults("Verify user can click on Search button in Customer and Customer Lookup should pop up",
					"User should be click on Search button in Customer and Customer Lookup should pop up",
					"User successfully click on Search button in Customer and Customer Lookup should pop up", "pass");
		} else {

			writeTestResults("Verify user can click on Search button in Customer and Customer Lookup should pop up",
					"User should be click on Search button in Customer and Customer Lookup should pop up",
					"User could'nt click on Search button in Customer and Customer Lookup should pop up", "fail");
		}

		explicitWait(txt_cusAccountSearchIPA, 30);
		sendKeys(txt_cusAccountSearchIPA, customerAccount);
		pressEnter(txt_cusAccountSearchIPA);
		Thread.sleep(1500);
		explicitWait(lnk_resultCustomerAccountIPA, 30);
		if (isDisplayed(lnk_resultCustomerAccountIPA)) {
			writeTestResults("Verify user can search an existing customer by entering Customer name/code",
					"User should be able to search an existing customer by entering Customer name/code",
					"User successfully search an existing customer by entering Customer name/code", "pass");
		} else {

			writeTestResults("Verify user can search an existing customer by entering Customer name/code",
					"User should be able to search an existing customer by entering Customer name/code",
					"User could'nt search an existing customer by entering Customer name/code", "fail");
		}

		doubleClick(lnk_resultCustomerAccountIPA);

		sendKeys(txt_refNoIPA, testAllCharcterWordIPA);

		sendKeys(txt_descIPA, testAllCharcterWordIPA);
		Thread.sleep(2000);

		Select comboBox = new Select(driver.findElement(By.xpath(drop_cusrencyIPA)));
		selectText(drop_cusrencyIPA, currencyLKR);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		if (selectedComboValue.equals(currencyLKR)) {
			writeTestResults("Verify user can choose the currency from the drop down",
					"User should be able to choose the currency from the drop down",
					"User successfully choose the currency from the drop down", "pass");
		} else {

			writeTestResults("Verify user can choose the currency from the drop down",
					"User should be able to choose the currency from the drop down",
					"User could'nt choose the currency from the drop down", "fail");
		}

		sendKeys(txt_amountIPA, fiveThousandEightHundred);
		Thread.sleep(2000);
		String ckeckoutBefore = getText(btn_documentDataIPA);

		click(btn_checkout);
		Thread.sleep(3000);
		String ckeckoutAfter = getText(btn_documentDataIPA);
		if (!ckeckoutBefore.equals(ckeckoutAfter)) {
			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User successfully checkout Inbound Payment Advice", "pass");
		} else {

			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User doesn't checkout Inbound Payment Advice", "fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 40);
		if (isDisplayed(btn_reelese)) {

			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice",
					"User sucessfully draft Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice", "User could'nt draft Inboumd Payment Advice",
					"fail");
		}

		Thread.sleep(3000);
		click(btn_reelese);
		explicitWait(header_releasedInboundPaymentAdvice, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedInboundPaymentAdvice)) {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User sucessfully release Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User could'nt release Inboumd Payment Advice", "fail");
		}
	}

	public void inboundPayment_FIN_BD_9() throws Exception {
		click(btn_actionCRV);

		click(btn_converToInboundPaymentCRV);
		click(btn_detailTabCRV);

		click(btn_summaryTab);

		selectText(drop_paidCurrencyCRV, currencyLKR);

		selectText(drop_paymentMethodCRV, payMethodCheque);

		selectText(drop_bankNameInboundPayment, bank);

		String cheque = getInvObj().currentTimeAndDate().replace("/", "").replace(":", "");
		getFinObj().writeFinanceData("ChequeNumber_FIN_BD_9", cheque, 26);
		sendKeys(txt_checkNoInboundPayment, cheque);

		click(btn_detailTabCRV);

		click(btn_detailTabCRV);
		selectText(drop_filterCurrencyCRV, currencyLKR);

		click(chk_firstAdviceOnGridInboundPayment);

		click(btn_checkout);
		Thread.sleep(4000);
		String bannerTotalCRV = getText(btn_documentDataCRV);

		if (!bannerTotalCRV.equals("0")) {
			writeTestResults("Verify user can checkout Cheque Customer Receipt",
					"User should be able to checkout the Cheque Customer Receipt",
					"User successfully checkout Cheque Customer Receipt", "pass");
		} else {

			writeTestResults("Verify user can checkout Cheque Customer Receipt",
					"User should be able to checkout the Cheque Customer Receipt",
					"User doesn't checkout Cheque Customer Receipt", "fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 30);
		if (isDisplayed(btn_reelese)) {
			writeTestResults("Verify user can draft Cheque Customer Receipt",
					"User should be able to draft Cheque Customer Receipt",
					"User sucessfully draft Cheque Customer Receipt", "pass");
		} else {
			writeTestResults("Verify user can draft Cheque Customer Receipt",
					"User should be able to draft Cheque Customer Receipt",
					"User could'nt draft Cheque Customer Receipt", "fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedInboundPayment, 30);
		trackCode = getText(lbl_docNo);
		getFinObj().writeFinanceData("CustomerReciptCheque_FIN_BD_9", trackCode, 27);
		if (isDisplayed(header_releasedInboundPayment)) {
			writeTestResults("Verify user can release Cheque Customer Receipt",
					"User should be able to release Cheque Customer Receipt",
					"User sucessfully release Cheque Customer Receipt", "pass");
		} else {
			writeTestResults("Verify user can release Cheque Customer Receipt",
					"User should be able to release Cheque Customer Receipt",
					"User could'nt release Cheque Customer Receipt", "fail");
		}
	}

	public void bankDepositFilterPaymentByChequeNumber_FIN_BD_9() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_bankDeposit, 30);
		if (isDisplayed(btn_bankDeposit)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}

		click(btn_bankDeposit);
		explicitWait(header_bankDeposit, 30);
		if (isDisplayed(header_bankDeposit)) {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page",
					"User successfully navigate to Bank Deposit by page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page", "User doesn't navigate to Bank Deposit by page",
					"fail");
		}

		click(btn_newBankDeposit);
		explicitWait(header_newBankDeposit, 30);
		if (isDisplayed(header_newBankDeposit)) {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page successfully displayed",
					"pass");
		} else {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page doesn't displayed", "fail");
		}

		selectText(drop_payMethodBankDeposi, payMethodCheque);
		String selectedComboValue = getSelectedOptionInDropdown(drop_payMethodBankDeposi);
		if (selectedComboValue.equals(payMethodCheque)) {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User successfully select any Pay Method from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User doesn't select any Pay Method from the drop down where the field will populate the selected value",
					"fail");
		}

		sendKeys(txt_descriptinArea, description);
		String descriptionStr = getAttribute(txt_descriptinArea, "value");
		if (descriptionStr.equals(description)) {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User successfully enter characters, numbers, alphanumeric and special characters to Description",
					"pass");
		} else {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User doesn't enter characters, numbers, alphanumeric and special characters to Description",
					"fail");
		}

		selectText(drop_bankNameBD, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bankNameBD);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_accountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_accountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		sendKeys(txt_chequeFilterBy, getFinObj().readFinanceData("ChequeNumber_FIN_BD_9"));
		String chequeNumberLOaded = getAttribute(txt_chequeFilterBy, "value");
		if (chequeNumberLOaded.equals(getFinObj().readFinanceData("ChequeNumber_FIN_BD_9"))) {
			writeTestResults("Verify user able to enter a Cheque No in the field",
					"User should be able to enter a Cheque No in the field",
					"User successfully enter a Cheque No in the field", "pass");
		} else {
			writeTestResults("Verify user able to enter a Cheque No in the field",
					"User should be able to enter a Cheque No in the field",
					"User doesn't enter a Cheque No in the field.", "fail");
		}

		click(btn_viewBankDeposit);
		Thread.sleep(2000);
		explicitWait(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerReciptCheque_FIN_BD_9")), 30);
		if (isDisplayed(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerReciptCheque_FIN_BD_9")))
				& getCount(count_paymentBankDeposit) == 1) {
			writeTestResults("Verify that Cheque Payment related to the Cheque No. entered is only be retrieved",
					"Cheque Payment related to the Cheque No. entered should only be retrieved",
					"Cheque Payment related to the Cheque No. entered only retrieved", "pass");
		} else {
			writeTestResults("Verify that Cheque Payment related to the Cheque No. entered is only be retrieved",
					"Cheque Payment related to the Cheque No. entered should only be retrieved",
					"Not only Cheque Payment related to the Cheque No. entered retrieved", "fail");
		}

	}

	/* FIN_BD_12_13_14_15_17 */
	public void inboundPaymentAdvice_FIN_BD_12_13_14_15_17() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_inboundPaymentAdvice, 30);
		if (isDisplayed(btn_inboundPaymentAdvice)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}
		click(btn_inboundPaymentAdvice);
		explicitWait(btn_newInboundPaymentAdvice, 30);
		if (isDisplayed(btn_newInboundPaymentAdvice)) {

			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User successfully navigate to Inbound payment advice by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User could'nt navigate to Inbound payment advice by-page", "fail");
		}

		click(btn_newInboundPaymentAdvice);
		if (isDisplayed(btn_CusAdvanceJourneyIPA)) {

			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User successfully view the list of journey",
					"pass");
		} else {
			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User could'nt view the list of journey",
					"fail");
		}

		click(btn_CusAdvanceJourneyIPA);
		explicitWait(lnl_postBusinessUnitIPA, 30);
		if (isDisplayed(lnl_postBusinessUnitIPA)) {

			writeTestResults("Verify user can navigate to new customer advance form",
					"User should navigate to new customer advance form",
					"User successfully navigate to new customer advance form", "pass");
		} else {
			writeTestResults("Verify user can navigate to new customer advance form",
					"User should navigate to new customer advance form",
					"User could'nt navigate to new customer advance form", "fail");
		}

		Thread.sleep(1000);
		click(lnl_postBusinessUnitIPA);
		Thread.sleep(700);
		selectText(drop_postBusinessUnit, postBusinessUnitSch);
		Thread.sleep(700);
		click(tab_subbaryIPA);
		Thread.sleep(2000);
		String get_post_business_unit = getText(lnl_postBusinessUnitIPA);
		System.out.println(get_post_business_unit);
		if (get_post_business_unit.equals(postBusinessUnitSch)) {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User successfully select post business unit from the drop down", "pass");
		} else {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User could'nt select post business unit from the drop down", "fail");
		}

		click(btn_searchCustomerIPA);
		if (isDisplayed(txt_cusAccountSearchIPA)) {
			writeTestResults("Verify user can click on Search button in Customer and Customer Lookup should pop up",
					"User should be click on Search button in Customer and Customer Lookup should pop up",
					"User successfully click on Search button in Customer and Customer Lookup should pop up", "pass");
		} else {

			writeTestResults("Verify user can click on Search button in Customer and Customer Lookup should pop up",
					"User should be click on Search button in Customer and Customer Lookup should pop up",
					"User could'nt click on Search button in Customer and Customer Lookup should pop up", "fail");
		}

		explicitWait(txt_cusAccountSearchIPA, 30);
		sendKeys(txt_cusAccountSearchIPA, customerAccount);
		pressEnter(txt_cusAccountSearchIPA);
		Thread.sleep(1500);
		explicitWait(lnk_resultCustomerAccountIPA, 30);
		if (isDisplayed(lnk_resultCustomerAccountIPA)) {
			writeTestResults("Verify user can search an existing customer by entering Customer name/code",
					"User should be able to search an existing customer by entering Customer name/code",
					"User successfully search an existing customer by entering Customer name/code", "pass");
		} else {

			writeTestResults("Verify user can search an existing customer by entering Customer name/code",
					"User should be able to search an existing customer by entering Customer name/code",
					"User could'nt search an existing customer by entering Customer name/code", "fail");
		}

		doubleClick(lnk_resultCustomerAccountIPA);

		sendKeys(txt_refNoIPA, testAllCharcterWordIPA);

		sendKeys(txt_descIPA, testAllCharcterWordIPA);
		Thread.sleep(2000);

		Select comboBox = new Select(driver.findElement(By.xpath(drop_cusrencyIPA)));
		selectText(drop_cusrencyIPA, currencyLKR);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		if (selectedComboValue.equals(currencyLKR)) {
			writeTestResults("Verify user can choose the currency from the drop down",
					"User should be able to choose the currency from the drop down",
					"User successfully choose the currency from the drop down", "pass");
		} else {

			writeTestResults("Verify user can choose the currency from the drop down",
					"User should be able to choose the currency from the drop down",
					"User could'nt choose the currency from the drop down", "fail");
		}

		sendKeys(txt_amountIPA, amountThousand);
		Thread.sleep(2000);
		String ckeckoutBefore = getText(btn_documentDataIPA);

		click(btn_checkout);
		Thread.sleep(3000);
		String ckeckoutAfter = getText(btn_documentDataIPA);
		if (!ckeckoutBefore.equals(ckeckoutAfter)) {
			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User successfully checkout Inbound Payment Advice", "pass");
		} else {

			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User doesn't checkout Inbound Payment Advice", "fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 40);
		if (isDisplayed(btn_reelese)) {

			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice",
					"User sucessfully draft Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice", "User could'nt draft Inboumd Payment Advice",
					"fail");
		}

		Thread.sleep(3000);
		click(btn_reelese);
		explicitWait(header_releasedInboundPaymentAdvice, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedInboundPaymentAdvice)) {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User sucessfully release Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User could'nt release Inboumd Payment Advice", "fail");
		}
	}

	public void inboundPayment_FIN_BD_12_13_14_15_17() throws Exception {
		click(btn_actionCRV);

		click(btn_converToInboundPaymentCRV);
		click(btn_detailTabCRV);

		click(btn_summaryTab);

		selectText(drop_paidCurrencyCRV, currencyLKR);

		selectText(drop_paymentMethodCRV, payMethodCash);

		click(btn_detailTabCRV);

		click(btn_detailTabCRV);
		selectText(drop_filterCurrencyCRV, currencyLKR);

		sendKeys(txt_payingAmountCRV, amountThousandFiveHundred);

		click(chk_firstAdviceOnGridInboundPayment);

		click(btn_checkout);
		Thread.sleep(4000);
		String bannerTotalCRV = getText(btn_documentDataCRV);

		if (!bannerTotalCRV.equals("0")) {
			writeTestResults("Verify user can checkout Inbound Payment",
					"User should be able to checkout the Inbound Payment", "User successfully checkout Inbound Payment",
					"pass");
		} else {

			writeTestResults("Verify user can checkout Inbound Payment",
					"User should be able to checkout the Inbound Payment", "User doesn't checkout Inbound Payment",
					"fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 30);
		if (isDisplayed(btn_reelese)) {
			writeTestResults("Verify user can draft Inbound Payment", "User should be able to draft Inbound Payment",
					"User sucessfully draft Inbound Payment", "pass");
		} else {
			writeTestResults("Verify user can draft Inbound Payment", "User should be able to draft Inbound Payment",
					"User could'nt draft Inbound Payment", "fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedInboundPayment, 30);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedInboundPayment)) {
			writeTestResults("Verify user can release Inbound Payment",
					"User should be able to release Inbound Payment", "User sucessfully release Inbound Payment",
					"pass");
		} else {
			writeTestResults("Verify user can release Inbound Payment",
					"User should be able to release Inbound Payment", "User could'nt release Inbound Payment", "fail");
		}
	}

	public void bankDeposit_FIN_BD_12_13_14_15_17() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_bankDeposit, 30);
		if (isDisplayed(btn_bankDeposit)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}

		click(btn_bankDeposit);
		explicitWait(header_bankDeposit, 30);
		if (isDisplayed(header_bankDeposit)) {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page",
					"User successfully navigate to Bank Deposit by page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page", "User doesn't navigate to Bank Deposit by page",
					"fail");
		}

		click(btn_newBankDeposit);
		explicitWait(header_newBankDeposit, 30);
		if (isDisplayed(header_newBankDeposit)) {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page successfully displayed",
					"pass");
		} else {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page doesn't displayed", "fail");
		}

		selectText(drop_payMethodBankDeposi, payMethodCash);
		String selectedComboValue = getSelectedOptionInDropdown(drop_payMethodBankDeposi);
		if (selectedComboValue.equals(payMethodCash)) {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User successfully select any Pay Method from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User doesn't select any Pay Method from the drop down where the field will populate the selected value",
					"fail");
		}

		sendKeys(txt_descriptinArea, description);
		String descriptionStr = getAttribute(txt_descriptinArea, "value");
		if (descriptionStr.equals(description)) {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User successfully enter characters, numbers, alphanumeric and special characters to Description",
					"pass");
		} else {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User doesn't enter characters, numbers, alphanumeric and special characters to Description",
					"fail");
		}

		selectText(drop_bankNameBD, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bankNameBD);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_accountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_accountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_viewBankDeposit);
		explicitWait(chk_firstAdviceOnGridInBankDeposit, 30);
		click(chk_firstAdviceOnGridInBankDeposit);
		click(btn_draft);
		explicitWait(header_draftedBankDeposit, 30);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_draftedBankDeposit)) {
			writeTestResults("Verify user can temporarily saves the document",
					"User should be able to temporarily saves the document",
					"User successfully temporarily saves the document", "pass");
		} else {
			writeTestResults("Verify user can temporarily saves the document",
					"User should be able to temporarily saves the document",
					"User doesn't temporarily saves the document", "fail");
		}

		if (!isDisplayedQuickCheck(btn_duplicate)) {
			writeTestResults("Verify user not access to Duplicate button", "User should not access to Duplicate button",
					"User not access to Duplicate button", "pass");
		} else {
			writeTestResults("Verify user not access to Duplicate button", "User should not access to Duplicate button",
					"User can access to Duplicate button", "fail");
		}

		click(btn_edit);
		explicitWait(btn_updateMainDoc, 40);
		if (isDisplayed(btn_updateMainDoc)) {
			writeTestResults("Verify Bank Deposit page will be editable", "Bank Deposit page will be editable",
					"Bank Deposit successfully open as editable", "pass");
		} else {
			writeTestResults("Verify Bank Deposit page will be editable", "Bank Deposit page will be editable",
					"Bank Deposit doesn't open as editable", "pass");
		}

		Thread.sleep(2000);
		sendKeys(txt_abountSelectedInboundPaymentBankDeposit, amountThousand);

		Thread.sleep(1500);
		String depositAmount = getAttribute(txt_abountSelectedInboundPaymentBankDeposit, "value");

		if (depositAmount.equals(amountThousand)) {
			writeTestResults("Verify user able to change values", "User should be able to change values",
					"User successfully change values", "pass");
		} else {
			writeTestResults("Verify user able to change values", "User should be able to change values",
					"User doesn't change values", "fail");
		}

		click(btn_updateMainDoc);
		explicitWait(btn_edit, 30);
		if (isDisplayed(btn_edit)) {
			writeTestResults("Verify Bank Deposit is updated successfully", "Bank Deposit will be updated successfully",
					"Bank Deposit updated successfully", "pass");
		} else {
			writeTestResults("Verify Bank Deposit is updated successfully", "Bank Deposit will be updated successfully",
					"Bank Deposit doesn't updated", "fail");
		}

		String updatedAmount = getText(div_updatedAmountBankDeposit);

		if (updatedAmount.equals("1000.000000")) {
			writeTestResults("Verify updated amound successfully displayed", "Verify updated amount is displayed",
					"Updated amount successfully displayed", "pass");
		} else {
			writeTestResults("Verify updated amound successfully displayed", "Verify updated amount is displayed",
					"Updated amount doesn't displayed", "fail");
		}

		click(btn_action);
		explicitWait(btn_history, 10);
		if (isDisplayed(btn_history)) {
			writeTestResults("Verify action drop down menu is displayed", "Action drop down menu should be displayed",
					"Action drop down menu successfully displayed", "pass");
		} else {
			writeTestResults("Verify action drop down menu is displayed", "Action drop down menu should be displayed",
					"Action drop down menu doesn't displayed", "fail");
		}

		click(btn_history);
		explicitWait(lbl_draftDescriptionOnHistoryTab, 20);
		if (isDisplayed(lbl_draftDescriptionOnHistoryTab.replace("row", "2"))
				&& isDisplayed(lbl_updateDescriptionOnHistoryTab.replace("row", "1"))
				&& isDisplayed(lbl_draftOnHistoryTab.replace("row", "2"))
				&& isDisplayed(lbl_updateOnHistoryTab.replace("row", "1"))) {
			writeTestResults("Verify history for the document is displayed",
					"History for the document should be displayed", "History for the document successfully displayed",
					"pass");
		} else {
			writeTestResults("Verify history for the document is displayed",
					"History for the document should be displayed", "History for the document doesn't displayed",
					"fail");
		}

		click(btn_edit);
		explicitWait(btn_updateMainDoc, 40);
		if (isDisplayed(btn_updateMainDoc)) {
			writeTestResults("Verify Bank Deposit page will be editable", "Bank Deposit page will be editable",
					"Bank Deposit successfully open as editable", "pass");
		} else {
			writeTestResults("Verify Bank Deposit page will be editable", "Bank Deposit page will be editable",
					"Bank Deposit doesn't open as editable", "pass");
		}

		sendKeys(txt_abountSelectedInboundPaymentBankDeposit, amountThousandFiveHundred);

		String depositAmountSecond = getAttribute(txt_abountSelectedInboundPaymentBankDeposit, "value");

		if (depositAmountSecond.equals(amountThousandFiveHundred)) {
			writeTestResults("Verify user able to change values", "User should be able to change values",
					"User successfully change values", "pass");
		} else {
			writeTestResults("Verify user able to change values", "User should be able to change values",
					"User doesn't change values", "fail");
		}

		click(btn_updateAndNew);
		explicitWait(header_newBankDeposit, 30);
		if (isDisplayed(header_newBankDeposit)) {
			writeTestResults("Verify user can get a Bank Deposit - New page",
					"User should get a Bank Deposit - New page", "User successfully get a Bank Deposit - New page",
					"pass");
		} else {
			writeTestResults("Verify user can get a Bank Deposit - New page",
					"User should get a Bank Deposit - New page", "User doesn't get a Bank Deposit - New page", "fail");
		}
	}

	/* FIN_BD_16 */
	public void inboundPaymentAdvice_FIN_BD_16() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_inboundPaymentAdvice, 30);
		if (isDisplayed(btn_inboundPaymentAdvice)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}
		click(btn_inboundPaymentAdvice);
		explicitWait(btn_newInboundPaymentAdvice, 30);
		if (isDisplayed(btn_newInboundPaymentAdvice)) {

			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User successfully navigate to Inbound payment advice by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User could'nt navigate to Inbound payment advice by-page", "fail");
		}

		click(btn_newInboundPaymentAdvice);
		if (isDisplayed(btn_CusAdvanceJourneyIPA)) {

			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User successfully view the list of journey",
					"pass");
		} else {
			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User could'nt view the list of journey",
					"fail");
		}

		click(btn_CusAdvanceJourneyIPA);
		explicitWait(lnl_postBusinessUnitIPA, 30);
		if (isDisplayed(lnl_postBusinessUnitIPA)) {

			writeTestResults("Verify user can navigate to new customer advance form",
					"User should navigate to new customer advance form",
					"User successfully navigate to new customer advance form", "pass");
		} else {
			writeTestResults("Verify user can navigate to new customer advance form",
					"User should navigate to new customer advance form",
					"User could'nt navigate to new customer advance form", "fail");
		}

		Thread.sleep(1000);
		click(lnl_postBusinessUnitIPA);
		Thread.sleep(700);
		selectText(drop_postBusinessUnit, postBusinessUnitSch);
		Thread.sleep(700);
		click(tab_subbaryIPA);
		Thread.sleep(2000);
		String get_post_business_unit = getText(lnl_postBusinessUnitIPA);
		System.out.println(get_post_business_unit);
		if (get_post_business_unit.equals(postBusinessUnitSch)) {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User successfully select post business unit from the drop down", "pass");
		} else {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User could'nt select post business unit from the drop down", "fail");
		}

		click(btn_searchCustomerIPA);
		if (isDisplayed(txt_cusAccountSearchIPA)) {
			writeTestResults("Verify user can click on Search button in Customer and Customer Lookup should pop up",
					"User should be click on Search button in Customer and Customer Lookup should pop up",
					"User successfully click on Search button in Customer and Customer Lookup should pop up", "pass");
		} else {

			writeTestResults("Verify user can click on Search button in Customer and Customer Lookup should pop up",
					"User should be click on Search button in Customer and Customer Lookup should pop up",
					"User could'nt click on Search button in Customer and Customer Lookup should pop up", "fail");
		}

		explicitWait(txt_cusAccountSearchIPA, 30);
		sendKeys(txt_cusAccountSearchIPA, customerAccount);
		pressEnter(txt_cusAccountSearchIPA);
		Thread.sleep(1500);
		explicitWait(lnk_resultCustomerAccountIPA, 30);
		if (isDisplayed(lnk_resultCustomerAccountIPA)) {
			writeTestResults("Verify user can search an existing customer by entering Customer name/code",
					"User should be able to search an existing customer by entering Customer name/code",
					"User successfully search an existing customer by entering Customer name/code", "pass");
		} else {

			writeTestResults("Verify user can search an existing customer by entering Customer name/code",
					"User should be able to search an existing customer by entering Customer name/code",
					"User could'nt search an existing customer by entering Customer name/code", "fail");
		}

		doubleClick(lnk_resultCustomerAccountIPA);

		sendKeys(txt_refNoIPA, testAllCharcterWordIPA);

		sendKeys(txt_descIPA, testAllCharcterWordIPA);
		Thread.sleep(2000);

		Select comboBox = new Select(driver.findElement(By.xpath(drop_cusrencyIPA)));
		selectText(drop_cusrencyIPA, currencyLKR);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		if (selectedComboValue.equals(currencyLKR)) {
			writeTestResults("Verify user can choose the currency from the drop down",
					"User should be able to choose the currency from the drop down",
					"User successfully choose the currency from the drop down", "pass");
		} else {

			writeTestResults("Verify user can choose the currency from the drop down",
					"User should be able to choose the currency from the drop down",
					"User could'nt choose the currency from the drop down", "fail");
		}

		sendKeys(txt_amountIPA, amountThousand);
		Thread.sleep(2000);
		String ckeckoutBefore = getText(btn_documentDataIPA);

		click(btn_checkout);
		Thread.sleep(3000);
		String ckeckoutAfter = getText(btn_documentDataIPA);
		if (!ckeckoutBefore.equals(ckeckoutAfter)) {
			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User successfully checkout Inbound Payment Advice", "pass");
		} else {

			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User doesn't checkout Inbound Payment Advice", "fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 40);
		if (isDisplayed(btn_reelese)) {

			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice",
					"User sucessfully draft Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice", "User could'nt draft Inboumd Payment Advice",
					"fail");
		}

		Thread.sleep(3000);
		click(btn_reelese);
		explicitWait(header_releasedInboundPaymentAdvice, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedInboundPaymentAdvice)) {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User sucessfully release Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User could'nt release Inboumd Payment Advice", "fail");
		}
	}

	public void inboundPayment_FIN_BD_16() throws Exception {
		click(btn_actionCRV);

		click(btn_converToInboundPaymentCRV);
		click(btn_detailTabCRV);

		click(btn_summaryTab);

		selectText(drop_paidCurrencyCRV, currencyLKR);

		selectText(drop_paymentMethodCRV, payMethodCash);

		click(btn_detailTabCRV);

		click(btn_detailTabCRV);
		selectText(drop_filterCurrencyCRV, currencyLKR);

		sendKeys(txt_payingAmountCRV, amountThousandFiveHundred);

		click(chk_firstAdviceOnGridInboundPayment);

		click(btn_checkout);
		Thread.sleep(4000);
		String bannerTotalCRV = getText(btn_documentDataCRV);

		if (!bannerTotalCRV.equals("0")) {
			writeTestResults("Verify user can checkout Inbound Payment",
					"User should be able to checkout the Inbound Payment", "User successfully checkout Inbound Payment",
					"pass");
		} else {

			writeTestResults("Verify user can checkout Inbound Payment",
					"User should be able to checkout the Inbound Payment", "User doesn't checkout Inbound Payment",
					"fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 30);
		if (isDisplayed(btn_reelese)) {
			writeTestResults("Verify user can draft Inbound Payment", "User should be able to draft Inbound Payment",
					"User sucessfully draft Inbound Payment", "pass");
		} else {
			writeTestResults("Verify user can draft Inbound Payment", "User should be able to draft Inbound Payment",
					"User could'nt draft Inbound Payment", "fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedInboundPayment, 30);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedInboundPayment)) {
			writeTestResults("Verify user can release Inbound Payment",
					"User should be able to release Inbound Payment", "User sucessfully release Inbound Payment",
					"pass");
		} else {
			writeTestResults("Verify user can release Inbound Payment",
					"User should be able to release Inbound Payment", "User could'nt release Inbound Payment", "fail");
		}
	}

	public void bankDeposit_FIN_BD_16() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_bankDeposit, 30);
		if (isDisplayed(btn_bankDeposit)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}

		click(btn_bankDeposit);
		explicitWait(header_bankDeposit, 30);
		if (isDisplayed(header_bankDeposit)) {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page",
					"User successfully navigate to Bank Deposit by page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page", "User doesn't navigate to Bank Deposit by page",
					"fail");
		}

		click(btn_newBankDeposit);
		explicitWait(header_newBankDeposit, 30);
		if (isDisplayed(header_newBankDeposit)) {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page successfully displayed",
					"pass");
		} else {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page doesn't displayed", "fail");
		}

		selectText(drop_payMethodBankDeposi, payMethodCash);
		String selectedComboValue = getSelectedOptionInDropdown(drop_payMethodBankDeposi);
		if (selectedComboValue.equals(payMethodCash)) {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User successfully select any Pay Method from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User doesn't select any Pay Method from the drop down where the field will populate the selected value",
					"fail");
		}

		sendKeys(txt_descriptinArea, description);
		String descriptionStr = getAttribute(txt_descriptinArea, "value");
		if (descriptionStr.equals(description)) {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User successfully enter characters, numbers, alphanumeric and special characters to Description",
					"pass");
		} else {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User doesn't enter characters, numbers, alphanumeric and special characters to Description",
					"fail");
		}

		selectText(drop_bankNameBD, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bankNameBD);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_accountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_accountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_viewBankDeposit);
		explicitWait(chk_firstAdviceOnGridInBankDeposit, 30);
		click(chk_firstAdviceOnGridInBankDeposit);
		click(btn_draft);
		explicitWait(header_draftedBankDeposit, 30);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_draftedBankDeposit)) {
			writeTestResults("Verify user able to Draft the Bank Deposit created",
					"User should be able to Draft the Bank Deposit created",
					"User successfully Draft the Bank Deposit created", "pass");
		} else {
			writeTestResults("Verify user able to Draft the Bank Deposit created",
					"User should be able to Draft the Bank Deposit created",
					"User doesn't Draft the Bank Deposit created", "fail");
		}

		click(btn_action);
		explicitWait(btn_delete, 30);
		if (isDisplayed(btn_delete)) {
			writeTestResults("Verify action drop down menu is displayed", "Action drop down menu will be displayed",
					"Action drop down menu successfully displayed", "pass");
		} else {
			writeTestResults("Verify action drop down menu is displayed", "Action drop down menu will be displayed",
					"Action drop down menu doesn't displayed", "fail");
		}

		click(btn_delete);
		explicitWait(btn_yes, 30);
		if (isDisplayed(btn_yes)) {
			writeTestResults("Verify confirmation popup is displayed and user able to click on yes",
					"Confirmation popup should be displayed and user should be able to click on yes",
					"Confirmation popup successfully displayed and user able to click on yes", "pass");
		} else {
			writeTestResults("Verify confirmation popup is displayed and user able to click on yes",
					"Confirmation popup should be displayed and user should be able to click on yes",
					"Confirmation popup doesn't displayed and user not able to click on yes", "fail");
		}

		click(btn_yes);

		explicitWait(header_deletedBankDeposit, 40);
		if (isDisplayed(header_deletedBankDeposit)) {
			writeTestResults("Verify deleted Bank Deposit header is updated as Deleted",
					"Deleted Bank Deposit header should be updated as Deleted",
					"Deleted Bank Deposit header successfully updated as Deleted", "pass");
		} else {
			writeTestResults("Verify deleted Bank Deposit header is updated as Deleted",
					"Deleted Bank Deposit header should be updated as Deleted",
					"Deleted Bank Deposit header doesn't updated as Deleted", "fail");
		}
	}

	/* FIN_BD_18 */
	public void inboundPaymentAdvice1_FIN_BD_18() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_inboundPaymentAdvice, 30);
		if (isDisplayed(btn_inboundPaymentAdvice)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}
		click(btn_inboundPaymentAdvice);
		explicitWait(btn_newInboundPaymentAdvice, 30);
		if (isDisplayed(btn_newInboundPaymentAdvice)) {

			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User successfully navigate to Inbound payment advice by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User could'nt navigate to Inbound payment advice by-page", "fail");
		}

		click(btn_newInboundPaymentAdvice);
		if (isDisplayed(btn_CusAdvanceJourneyIPA)) {

			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User successfully view the list of journey",
					"pass");
		} else {
			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User could'nt view the list of journey",
					"fail");
		}

		click(btn_CusAdvanceJourneyIPA);
		explicitWait(lnl_postBusinessUnitIPA, 30);
		if (isDisplayed(lnl_postBusinessUnitIPA)) {

			writeTestResults("Verify user can navigate to new customer advance form",
					"User should navigate to new customer advance form",
					"User successfully navigate to new customer advance form", "pass");
		} else {
			writeTestResults("Verify user can navigate to new customer advance form",
					"User should navigate to new customer advance form",
					"User could'nt navigate to new customer advance form", "fail");
		}

		Thread.sleep(1000);
		click(lnl_postBusinessUnitIPA);
		Thread.sleep(700);
		selectText(drop_postBusinessUnit, postBusinessUnitSch);
		Thread.sleep(700);
		click(tab_subbaryIPA);
		Thread.sleep(2000);
		String get_post_business_unit = getText(lnl_postBusinessUnitIPA);
		System.out.println(get_post_business_unit);
		if (get_post_business_unit.equals(postBusinessUnitSch)) {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User successfully select post business unit from the drop down", "pass");
		} else {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User could'nt select post business unit from the drop down", "fail");
		}

		click(btn_searchCustomerIPA);
		if (isDisplayed(txt_cusAccountSearchIPA)) {
			writeTestResults("Verify user can click on Search button in Customer and Customer Lookup should pop up",
					"User should be click on Search button in Customer and Customer Lookup should pop up",
					"User successfully click on Search button in Customer and Customer Lookup should pop up", "pass");
		} else {

			writeTestResults("Verify user can click on Search button in Customer and Customer Lookup should pop up",
					"User should be click on Search button in Customer and Customer Lookup should pop up",
					"User could'nt click on Search button in Customer and Customer Lookup should pop up", "fail");
		}

		explicitWait(txt_cusAccountSearchIPA, 30);
		sendKeys(txt_cusAccountSearchIPA, customerAccount);
		pressEnter(txt_cusAccountSearchIPA);
		Thread.sleep(1500);
		explicitWait(lnk_resultCustomerAccountIPA, 30);
		if (isDisplayed(lnk_resultCustomerAccountIPA)) {
			writeTestResults("Verify user can search an existing customer by entering Customer name/code",
					"User should be able to search an existing customer by entering Customer name/code",
					"User successfully search an existing customer by entering Customer name/code", "pass");
		} else {

			writeTestResults("Verify user can search an existing customer by entering Customer name/code",
					"User should be able to search an existing customer by entering Customer name/code",
					"User could'nt search an existing customer by entering Customer name/code", "fail");
		}

		doubleClick(lnk_resultCustomerAccountIPA);

		sendKeys(txt_refNoIPA, testAllCharcterWordIPA);

		sendKeys(txt_descIPA, testAllCharcterWordIPA);
		Thread.sleep(2000);

		Select comboBox = new Select(driver.findElement(By.xpath(drop_cusrencyIPA)));
		selectText(drop_cusrencyIPA, currencyLKR);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		if (selectedComboValue.equals(currencyLKR)) {
			writeTestResults("Verify user can choose the currency from the drop down",
					"User should be able to choose the currency from the drop down",
					"User successfully choose the currency from the drop down", "pass");
		} else {

			writeTestResults("Verify user can choose the currency from the drop down",
					"User should be able to choose the currency from the drop down",
					"User could'nt choose the currency from the drop down", "fail");
		}

		sendKeys(txt_amountIPA, amountThousand);
		Thread.sleep(2000);
		String ckeckoutBefore = getText(btn_documentDataIPA);

		click(btn_checkout);
		Thread.sleep(3000);
		String ckeckoutAfter = getText(btn_documentDataIPA);
		if (!ckeckoutBefore.equals(ckeckoutAfter)) {
			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User successfully checkout Inbound Payment Advice", "pass");
		} else {

			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User doesn't checkout Inbound Payment Advice", "fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 40);
		if (isDisplayed(btn_reelese)) {

			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice",
					"User sucessfully draft Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice", "User could'nt draft Inboumd Payment Advice",
					"fail");
		}

		Thread.sleep(3000);
		click(btn_reelese);
		explicitWait(header_releasedInboundPaymentAdvice, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedInboundPaymentAdvice)) {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User sucessfully release Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User could'nt release Inboumd Payment Advice", "fail");
		}
	}

	public void inboundPayment1_FIN_BD_18() throws Exception {
		click(btn_actionCRV);

		click(btn_converToInboundPaymentCRV);
		click(btn_detailTabCRV);

		click(btn_summaryTab);

		selectText(drop_paidCurrencyCRV, currencyLKR);

		selectText(drop_paymentMethodCRV, payMethodCash);

		click(btn_detailTabCRV);

		click(btn_detailTabCRV);
		selectText(drop_filterCurrencyCRV, currencyLKR);

		sendKeys(txt_payingAmountCRV, amountThousandFiveHundred);

		click(chk_firstAdviceOnGridInboundPayment);

		click(btn_checkout);
		Thread.sleep(4000);
		String bannerTotalCRV = getText(btn_documentDataCRV);

		if (!bannerTotalCRV.equals("0")) {
			writeTestResults("Verify user can checkout Inbound Payment",
					"User should be able to checkout the Inbound Payment", "User successfully checkout Inbound Payment",
					"pass");
		} else {

			writeTestResults("Verify user can checkout Inbound Payment",
					"User should be able to checkout the Inbound Payment", "User doesn't checkout Inbound Payment",
					"fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 30);
		if (isDisplayed(btn_reelese)) {
			writeTestResults("Verify user can draft Inbound Payment", "User should be able to draft Inbound Payment",
					"User sucessfully draft Inbound Payment", "pass");
		} else {
			writeTestResults("Verify user can draft Inbound Payment", "User should be able to draft Inbound Payment",
					"User could'nt draft Inbound Payment", "fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedInboundPayment, 30);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedInboundPayment)) {
			writeTestResults("Verify user can release Inbound Payment",
					"User should be able to release Inbound Payment", "User sucessfully release Inbound Payment",
					"pass");
		} else {
			writeTestResults("Verify user can release Inbound Payment",
					"User should be able to release Inbound Payment", "User could'nt release Inbound Payment", "fail");
		}
	}

	public void inboundPaymentAdvice2_FIN_BD_18() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_inboundPaymentAdvice, 30);
		if (isDisplayed(btn_inboundPaymentAdvice)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}
		click(btn_inboundPaymentAdvice);
		explicitWait(btn_newInboundPaymentAdvice, 30);
		if (isDisplayed(btn_newInboundPaymentAdvice)) {

			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User successfully navigate to Inbound payment advice by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User could'nt navigate to Inbound payment advice by-page", "fail");
		}

		click(btn_newInboundPaymentAdvice);
		if (isDisplayed(btn_CusAdvanceJourneyIPA)) {

			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User successfully view the list of journey",
					"pass");
		} else {
			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User could'nt view the list of journey",
					"fail");
		}

		click(btn_CusAdvanceJourneyIPA);
		explicitWait(lnl_postBusinessUnitIPA, 30);
		if (isDisplayed(lnl_postBusinessUnitIPA)) {

			writeTestResults("Verify user can navigate to new customer advance form",
					"User should navigate to new customer advance form",
					"User successfully navigate to new customer advance form", "pass");
		} else {
			writeTestResults("Verify user can navigate to new customer advance form",
					"User should navigate to new customer advance form",
					"User could'nt navigate to new customer advance form", "fail");
		}

		Thread.sleep(1000);
		click(lnl_postBusinessUnitIPA);
		Thread.sleep(700);
		selectText(drop_postBusinessUnit, postBusinessUnitSch);
		Thread.sleep(700);
		click(tab_subbaryIPA);
		Thread.sleep(2000);
		String get_post_business_unit = getText(lnl_postBusinessUnitIPA);
		System.out.println(get_post_business_unit);
		if (get_post_business_unit.equals(postBusinessUnitSch)) {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User successfully select post business unit from the drop down", "pass");
		} else {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User could'nt select post business unit from the drop down", "fail");
		}

		click(btn_searchCustomerIPA);
		if (isDisplayed(txt_cusAccountSearchIPA)) {
			writeTestResults("Verify user can click on Search button in Customer and Customer Lookup should pop up",
					"User should be click on Search button in Customer and Customer Lookup should pop up",
					"User successfully click on Search button in Customer and Customer Lookup should pop up", "pass");
		} else {

			writeTestResults("Verify user can click on Search button in Customer and Customer Lookup should pop up",
					"User should be click on Search button in Customer and Customer Lookup should pop up",
					"User could'nt click on Search button in Customer and Customer Lookup should pop up", "fail");
		}

		explicitWait(txt_cusAccountSearchIPA, 30);
		sendKeys(txt_cusAccountSearchIPA, customerAccount);
		pressEnter(txt_cusAccountSearchIPA);
		Thread.sleep(1500);
		explicitWait(lnk_resultCustomerAccountIPA, 30);
		if (isDisplayed(lnk_resultCustomerAccountIPA)) {
			writeTestResults("Verify user can search an existing customer by entering Customer name/code",
					"User should be able to search an existing customer by entering Customer name/code",
					"User successfully search an existing customer by entering Customer name/code", "pass");
		} else {

			writeTestResults("Verify user can search an existing customer by entering Customer name/code",
					"User should be able to search an existing customer by entering Customer name/code",
					"User could'nt search an existing customer by entering Customer name/code", "fail");
		}

		doubleClick(lnk_resultCustomerAccountIPA);

		sendKeys(txt_refNoIPA, testAllCharcterWordIPA);

		sendKeys(txt_descIPA, testAllCharcterWordIPA);
		Thread.sleep(2000);

		Select comboBox = new Select(driver.findElement(By.xpath(drop_cusrencyIPA)));
		selectText(drop_cusrencyIPA, currencyLKR);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		if (selectedComboValue.equals(currencyLKR)) {
			writeTestResults("Verify user can choose the currency from the drop down",
					"User should be able to choose the currency from the drop down",
					"User successfully choose the currency from the drop down", "pass");
		} else {

			writeTestResults("Verify user can choose the currency from the drop down",
					"User should be able to choose the currency from the drop down",
					"User could'nt choose the currency from the drop down", "fail");
		}

		sendKeys(txt_amountIPA, amountThousand);
		Thread.sleep(2000);
		String ckeckoutBefore = getText(btn_documentDataIPA);

		click(btn_checkout);
		Thread.sleep(3000);
		String ckeckoutAfter = getText(btn_documentDataIPA);
		if (!ckeckoutBefore.equals(ckeckoutAfter)) {
			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User successfully checkout Inbound Payment Advice", "pass");
		} else {

			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User doesn't checkout Inbound Payment Advice", "fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 40);
		if (isDisplayed(btn_reelese)) {

			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice",
					"User sucessfully draft Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice", "User could'nt draft Inboumd Payment Advice",
					"fail");
		}

		Thread.sleep(3000);
		click(btn_reelese);
		explicitWait(header_releasedInboundPaymentAdvice, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedInboundPaymentAdvice)) {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User sucessfully release Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User could'nt release Inboumd Payment Advice", "fail");
		}
	}

	public void inboundPayment2_FIN_BD_18() throws Exception {
		click(btn_actionCRV);

		click(btn_converToInboundPaymentCRV);
		click(btn_detailTabCRV);

		click(btn_summaryTab);

		selectText(drop_paidCurrencyCRV, currencyLKR);

		selectText(drop_paymentMethodCRV, payMethodCash);

		click(btn_detailTabCRV);

		click(btn_detailTabCRV);
		selectText(drop_filterCurrencyCRV, currencyLKR);

		sendKeys(txt_payingAmountCRV, amountThousandFiveHundred);

		click(chk_firstAdviceOnGridInboundPayment);

		click(btn_checkout);
		Thread.sleep(4000);
		String bannerTotalCRV = getText(btn_documentDataCRV);

		if (!bannerTotalCRV.equals("0")) {
			writeTestResults("Verify user can checkout Inbound Payment",
					"User should be able to checkout the Inbound Payment", "User successfully checkout Inbound Payment",
					"pass");
		} else {

			writeTestResults("Verify user can checkout Inbound Payment",
					"User should be able to checkout the Inbound Payment", "User doesn't checkout Inbound Payment",
					"fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 30);
		if (isDisplayed(btn_reelese)) {
			writeTestResults("Verify user can draft Inbound Payment", "User should be able to draft Inbound Payment",
					"User sucessfully draft Inbound Payment", "pass");
		} else {
			writeTestResults("Verify user can draft Inbound Payment", "User should be able to draft Inbound Payment",
					"User could'nt draft Inbound Payment", "fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedInboundPayment, 30);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedInboundPayment)) {
			writeTestResults("Verify user can release Inbound Payment",
					"User should be able to release Inbound Payment", "User sucessfully release Inbound Payment",
					"pass");
		} else {
			writeTestResults("Verify user can release Inbound Payment",
					"User should be able to release Inbound Payment", "User could'nt release Inbound Payment", "fail");
		}
	}

	public void bankDeposit_FIN_BD_18() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_bankDeposit, 30);
		if (isDisplayed(btn_bankDeposit)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}

		click(btn_bankDeposit);
		explicitWait(header_bankDeposit, 30);
		if (isDisplayed(header_bankDeposit)) {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page",
					"User successfully navigate to Bank Deposit by page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page", "User doesn't navigate to Bank Deposit by page",
					"fail");
		}

		click(btn_newBankDeposit);
		explicitWait(header_newBankDeposit, 30);
		if (isDisplayed(header_newBankDeposit)) {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page successfully displayed",
					"pass");
		} else {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page doesn't displayed", "fail");
		}

		selectText(drop_payMethodBankDeposi, payMethodCash);
		String selectedComboValue = getSelectedOptionInDropdown(drop_payMethodBankDeposi);
		if (selectedComboValue.equals(payMethodCash)) {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User successfully select any Pay Method from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User doesn't select any Pay Method from the drop down where the field will populate the selected value",
					"fail");
		}

		String descriptionUnique = getInvObj().currentTimeAndDate().replace("/", "").replace(":", "");
		sendKeys(txt_descriptinArea, descriptionUnique);
		String descriptionStr = getAttribute(txt_descriptinArea, "value");
		if (descriptionStr.equals(descriptionUnique)) {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User successfully enter characters, numbers, alphanumeric and special characters to Description",
					"pass");
		} else {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User doesn't enter characters, numbers, alphanumeric and special characters to Description",
					"fail");
		}

		selectText(drop_bankNameBD, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bankNameBD);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_accountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_accountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_viewBankDeposit);
		explicitWait(chk_firstAdviceOnGridInBankDeposit, 30);
		click(chk_firstAdviceOnGridInBankDeposit);
		click(btn_draftAndNew);
		explicitWait(header_newBankDeposit, 30);
		if (isDisplayed(header_newBankDeposit)) {
			writeTestResults("Verify blank Bank Deposit - new page is displayed",
					"Blanl Bank Deposit - new page should be displayed",
					"Blan Bank Deposit - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify blank Bank Deposit - new page is displayed",
					"Blanl Bank Deposit - new page should be displayed",
					"Blan Bank Deposit - New page doesn't displayed", "fail");
		}

		((JavascriptExecutor) driver).executeScript("window.open('about:blank','_blank');");
		switchWindow();
		openPage(site_Url);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 30);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_bankDeposit, 30);
		if (isDisplayed(btn_bankDeposit)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}

		click(btn_bankDeposit);
		explicitWait(header_bankDeposit, 30);
		if (isDisplayed(header_bankDeposit)) {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page",
					"User successfully navigate to Bank Deposit by page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page", "User doesn't navigate to Bank Deposit by page",
					"fail");
		}

		click(label_recentlyDraftedBankDeposit);

		explicitWait(txt_descriptionDraftedBankDeposit, 30);

		String fondedDescription = getAttribute(txt_descriptionDraftedBankDeposit, "value");

		trackCode = getText(lbl_docNo);

		String firstBankDeposit = trackCode;
		if (fondedDescription.equals(descriptionUnique)) {
			writeTestResults(
					"Verify user able to temporarily saves the document and found the relevent drafted Bank Deposit",
					"User should be able to temporarily saves the document and found the relevent drafted Bank Deposit",
					"User successfully temporarily saves the document and found the relevent drafted Bank Deposit",
					"pass");
		} else {
			writeTestResults(
					"Verify user able to temporarily saves the document and found the relevent drafted Bank Deposit",
					"User should be able to temporarily saves the document and found the relevent drafted Bank Deposit",
					"User doesn't temporarily saves the document and/or not found the relevent drafted Bank Deposit",
					"fail");
		}

		switchWindow();

		/* Second Bank Deposit */
		selectText(drop_payMethodBankDeposi, payMethodCash);
		String selectedComboValueSecond = getSelectedOptionInDropdown(drop_payMethodBankDeposi);
		if (selectedComboValueSecond.equals(payMethodCash)) {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User successfully select any Pay Method from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User doesn't select any Pay Method from the drop down where the field will populate the selected value",
					"fail");
		}

		sendKeys(txt_descriptinArea, description);
		String descriptionStrSecond = getAttribute(txt_descriptinArea, "value");
		if (descriptionStrSecond.equals(description)) {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User successfully enter characters, numbers, alphanumeric and special characters to Description",
					"pass");
		} else {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User doesn't enter characters, numbers, alphanumeric and special characters to Description",
					"fail");
		}

		selectText(drop_bankNameBD, sampathBank);
		String selectedBankSecond = getSelectedOptionInDropdown(drop_bankNameBD);
		if (selectedBankSecond.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_accountNo, sampathCurrentAccount);
		String selectedAccountSecond = getSelectedOptionInDropdown(drop_accountNo);
		if (selectedAccountSecond.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_viewBankDeposit);
		explicitWait(chk_firstAdviceOnGridInBankDeposit, 30);
		click(chk_firstAdviceOnGridInBankDeposit);

		click(btn_draft);
		explicitWait(header_draftedBankDeposit, 30);
		trackCode = getText(lbl_docNo);
		String secondBankDeposit = trackCode;
		if (isDisplayed(header_draftedBankDeposit)) {
			writeTestResults("Verify user able to temporarily saves the document",
					"User should be able to temporarily saves the document",
					"User successfully temporarily saves the document", "pass");
		} else {
			writeTestResults("Verify user able to temporarily saves the document",
					"User should be able to temporarily saves the document",
					"User doesn't temporarily saves the document", "fail");
		}

		switchWindow();
		goBack();

		explicitWait(lnl_releventDraftedBandDepositDocReplace.replace("doc_no", firstBankDeposit), 40);

		if (isDisplayed(lnl_releventDraftedBandDepositDocReplace.replace("doc_no", firstBankDeposit))
				& isDisplayed(lnl_releventDraftedBandDepositDocReplace.replace("doc_no", secondBankDeposit))) {
			writeTestResults("Verify user able to find both Bank Deposit in the Bank Deposit by page",
					"User should be able to find both Bank Deposit in the Bank Deposit by page",
					"User successfully find both Bank Deposit in the Bank Deposit by page", "pass");
		} else {
			writeTestResults("Verify user able to find both Bank Deposit in the Bank Deposit by page",
					"User should be able to find both Bank Deposit in the Bank Deposit by page",
					"User doesn't find both Bank Deposit in the Bank Deposit by page", "fail");
		}
	}

	/* FIN_BD_19_20 */
	public void inboundPaymentAdvice_FIN_BD_19_20() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_inboundPaymentAdvice, 30);
		if (isDisplayed(btn_inboundPaymentAdvice)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}
		click(btn_inboundPaymentAdvice);
		explicitWait(btn_newInboundPaymentAdvice, 30);
		if (isDisplayed(btn_newInboundPaymentAdvice)) {

			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User successfully navigate to Inbound payment advice by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User could'nt navigate to Inbound payment advice by-page", "fail");
		}

		click(btn_newInboundPaymentAdvice);
		if (isDisplayed(btn_CusAdvanceJourneyIPA)) {

			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User successfully view the list of journey",
					"pass");
		} else {
			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User could'nt view the list of journey",
					"fail");
		}

		click(btn_CusAdvanceJourneyIPA);
		explicitWait(lnl_postBusinessUnitIPA, 30);
		if (isDisplayed(lnl_postBusinessUnitIPA)) {

			writeTestResults("Verify user can navigate to new customer advance form",
					"User should navigate to new customer advance form",
					"User successfully navigate to new customer advance form", "pass");
		} else {
			writeTestResults("Verify user can navigate to new customer advance form",
					"User should navigate to new customer advance form",
					"User could'nt navigate to new customer advance form", "fail");
		}

		Thread.sleep(1000);
		click(lnl_postBusinessUnitIPA);
		Thread.sleep(700);
		selectText(drop_postBusinessUnit, postBusinessUnitSch);
		Thread.sleep(700);
		click(tab_subbaryIPA);
		Thread.sleep(2000);
		String get_post_business_unit = getText(lnl_postBusinessUnitIPA);
		System.out.println(get_post_business_unit);
		if (get_post_business_unit.equals(postBusinessUnitSch)) {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User successfully select post business unit from the drop down", "pass");
		} else {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User could'nt select post business unit from the drop down", "fail");
		}

		click(btn_searchCustomerIPA);
		if (isDisplayed(txt_cusAccountSearchIPA)) {
			writeTestResults("Verify user can click on Search button in Customer and Customer Lookup should pop up",
					"User should be click on Search button in Customer and Customer Lookup should pop up",
					"User successfully click on Search button in Customer and Customer Lookup should pop up", "pass");
		} else {

			writeTestResults("Verify user can click on Search button in Customer and Customer Lookup should pop up",
					"User should be click on Search button in Customer and Customer Lookup should pop up",
					"User could'nt click on Search button in Customer and Customer Lookup should pop up", "fail");
		}

		explicitWait(txt_cusAccountSearchIPA, 30);
		sendKeys(txt_cusAccountSearchIPA, customerAccount);
		pressEnter(txt_cusAccountSearchIPA);
		Thread.sleep(1500);
		explicitWait(lnk_resultCustomerAccountIPA, 30);
		if (isDisplayed(lnk_resultCustomerAccountIPA)) {
			writeTestResults("Verify user can search an existing customer by entering Customer name/code",
					"User should be able to search an existing customer by entering Customer name/code",
					"User successfully search an existing customer by entering Customer name/code", "pass");
		} else {

			writeTestResults("Verify user can search an existing customer by entering Customer name/code",
					"User should be able to search an existing customer by entering Customer name/code",
					"User could'nt search an existing customer by entering Customer name/code", "fail");
		}

		doubleClick(lnk_resultCustomerAccountIPA);

		sendKeys(txt_refNoIPA, testAllCharcterWordIPA);

		sendKeys(txt_descIPA, testAllCharcterWordIPA);
		Thread.sleep(2000);

		Select comboBox = new Select(driver.findElement(By.xpath(drop_cusrencyIPA)));
		selectText(drop_cusrencyIPA, currencyLKR);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		if (selectedComboValue.equals(currencyLKR)) {
			writeTestResults("Verify user can choose the currency from the drop down",
					"User should be able to choose the currency from the drop down",
					"User successfully choose the currency from the drop down", "pass");
		} else {

			writeTestResults("Verify user can choose the currency from the drop down",
					"User should be able to choose the currency from the drop down",
					"User could'nt choose the currency from the drop down", "fail");
		}

		sendKeys(txt_amountIPA, amountThousand);
		Thread.sleep(2000);
		String ckeckoutBefore = getText(btn_documentDataIPA);

		click(btn_checkout);
		Thread.sleep(3000);
		String ckeckoutAfter = getText(btn_documentDataIPA);
		if (!ckeckoutBefore.equals(ckeckoutAfter)) {
			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User successfully checkout Inbound Payment Advice", "pass");
		} else {

			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User doesn't checkout Inbound Payment Advice", "fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 40);
		if (isDisplayed(btn_reelese)) {

			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice",
					"User sucessfully draft Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice", "User could'nt draft Inboumd Payment Advice",
					"fail");
		}

		Thread.sleep(3000);
		click(btn_reelese);
		explicitWait(header_releasedInboundPaymentAdvice, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedInboundPaymentAdvice)) {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User sucessfully release Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User could'nt release Inboumd Payment Advice", "fail");
		}
	}

	public void inboundPayment_FIN_BD_19_20() throws Exception {
		click(btn_actionCRV);

		click(btn_converToInboundPaymentCRV);
		click(btn_detailTabCRV);

		click(btn_summaryTab);

		selectText(drop_paidCurrencyCRV, currencyLKR);

		selectText(drop_paymentMethodCRV, payMethodCash);

		click(btn_detailTabCRV);

		click(btn_detailTabCRV);
		selectText(drop_filterCurrencyCRV, currencyLKR);

		sendKeys(txt_payingAmountCRV, amountThousandFiveHundred);

		click(chk_firstAdviceOnGridInboundPayment);

		click(btn_checkout);
		Thread.sleep(4000);
		String bannerTotalCRV = getText(btn_documentDataCRV);

		if (!bannerTotalCRV.equals("0")) {
			writeTestResults("Verify user can checkout Inbound Payment",
					"User should be able to checkout the Inbound Payment", "User successfully checkout Inbound Payment",
					"pass");
		} else {

			writeTestResults("Verify user can checkout Inbound Payment",
					"User should be able to checkout the Inbound Payment", "User doesn't checkout Inbound Payment",
					"fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 30);
		if (isDisplayed(btn_reelese)) {
			writeTestResults("Verify user can draft Inbound Payment", "User should be able to draft Inbound Payment",
					"User sucessfully draft Inbound Payment", "pass");
		} else {
			writeTestResults("Verify user can draft Inbound Payment", "User should be able to draft Inbound Payment",
					"User could'nt draft Inbound Payment", "fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedInboundPayment, 30);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedInboundPayment)) {
			writeTestResults("Verify user can release Inbound Payment",
					"User should be able to release Inbound Payment", "User sucessfully release Inbound Payment",
					"pass");
		} else {
			writeTestResults("Verify user can release Inbound Payment",
					"User should be able to release Inbound Payment", "User could'nt release Inbound Payment", "fail");
		}
	}

	public void bankDeposit_FIN_BD_19_20() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_bankDeposit, 30);
		if (isDisplayed(btn_bankDeposit)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}

		click(btn_bankDeposit);
		explicitWait(header_bankDeposit, 30);
		if (isDisplayed(header_bankDeposit)) {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page",
					"User successfully navigate to Bank Deposit by page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page", "User doesn't navigate to Bank Deposit by page",
					"fail");
		}

		click(btn_newBankDeposit);
		explicitWait(header_newBankDeposit, 30);
		if (isDisplayed(header_newBankDeposit)) {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page successfully displayed",
					"pass");
		} else {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page doesn't displayed", "fail");
		}

		selectText(drop_payMethodBankDeposi, payMethodCash);
		String selectedComboValue = getSelectedOptionInDropdown(drop_payMethodBankDeposi);
		if (selectedComboValue.equals(payMethodCash)) {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User successfully select any Pay Method from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User doesn't select any Pay Method from the drop down where the field will populate the selected value",
					"fail");
		}

		sendKeys(txt_descriptinArea, description);
		String descriptionStr = getAttribute(txt_descriptinArea, "value");
		if (descriptionStr.equals(description)) {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User successfully enter characters, numbers, alphanumeric and special characters to Description",
					"pass");
		} else {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User doesn't enter characters, numbers, alphanumeric and special characters to Description",
					"fail");
		}

		selectText(drop_bankNameBD, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bankNameBD);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_accountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_accountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_viewBankDeposit);
		explicitWait(chk_firstAdviceOnGridInBankDeposit, 30);
		click(chk_firstAdviceOnGridInBankDeposit);
		click(btn_draft);
		explicitWait(header_draftedBankDeposit, 30);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_draftedBankDeposit)) {
			writeTestResults("Verify user able to Draft the Bank Deposit created",
					"User should be able to Draft the Bank Deposit created",
					"User successfully Draft the Bank Deposit created", "pass");
		} else {
			writeTestResults("Verify user able to Draft the Bank Deposit created",
					"User should be able to Draft the Bank Deposit created",
					"User doesn't Draft the Bank Deposit created", "fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedBankDeposit, 30);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedBankDeposit)) {
			writeTestResults("Verify user can release(Permenently record the transaction) the Bank Deposit",
					"User should be able to release(Permenently record the transaction) the Bank Deposit",
					"User successfully release(Permenently record the transaction) the Bank Deposit", "pass");
		} else {
			writeTestResults("Verify user can release(Permenently record the transaction) the Bank Deposit",
					"User should be able to release(Permenently record the transaction) the Bank Deposit",
					"User doesn't release(Permenently record the transaction) the Bank Deposit", "fail");
		}

		click(btn_action);
		explicitWait(btn_reverse, 10);
		if (isDisplayed(btn_reverse)) {
			writeTestResults("Verify action menu is displayed", "Action menu should be displayed",
					"Action menu successfully displayed", "pass");
		} else {
			writeTestResults("Verify action menu is displayed", "Action menu should be displayed",
					"Action menu doesn't displayed", "fail");
		}

		click(btn_reverse);
		explicitWait(btn_reverseSecond, 10);
		click(btn_reverseSecond);
		explicitWait(btn_yes, 10);
		click(btn_yes);
		explicitWait(header_reversedBankDeposit, 10);
		if (isDisplayed(header_reversedBankDeposit)) {
			writeTestResults("Verify Bank Deposit header is changed as Reversed",
					"Bank Deposit header should be change as Reversed",
					"Bank Deposit header successfully change as Reversed", "pass");
		} else {
			writeTestResults("Verify Bank Deposit header is changed as Reversed",
					"Bank Deposit header should be change as Reversed",
					"Bank Deposit header doesn't change as Reversed", "fail");
		}
	}

	/* FIN_BD_21 */
	public void inboundPaymentAdvice_FIN_BD_21() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 30);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_curencyExchangeRate, 20);
		click(btn_curencyExchangeRate);
		explicitWait(txt_usdBuyingRate, 50);
		sendKeys(txt_usdBuyingRate, oneHundred);
		sendKeys(txt_usdSellingRate, oneHundred);
		click(btn_update2);
		Thread.sleep(2000);

		((JavascriptExecutor) driver).executeScript("window.open('about:blank','_blank');");
		switchWindow();
		openPage(site_Url);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 30);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_inboundPaymentAdvice, 30);
		if (isDisplayed(btn_inboundPaymentAdvice)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}
		click(btn_inboundPaymentAdvice);
		explicitWait(btn_newInboundPaymentAdvice, 30);
		if (isDisplayed(btn_newInboundPaymentAdvice)) {

			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User successfully navigate to Inbound payment advice by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User could'nt navigate to Inbound payment advice by-page", "fail");
		}

		click(btn_newInboundPaymentAdvice);
		explicitWait(btn_cusDebitMemo, 10);
		if (isDisplayed(btn_cusDebitMemo)) {
			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User successfully view the list of journey",
					"pass");
		} else {
			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User could'nt view the list of journey",
					"fail");
		}

		click(btn_cusDebitMemo);
		explicitWait(lnl_postBusinessUnitIPA, 30);
		if (isDisplayed(lnl_postBusinessUnitIPA)) {

			writeTestResults("Verify user able to navigate to Customer Debit Memo",
					"User should be able to navigate to Customer Debit Memo",
					"User successfully navigate to Customer Debit Memo", "pass");
		} else {
			writeTestResults("Verify user able to navigate to Customer Debit Memo",
					"User should be able to navigate to Customer Debit Memo",
					"User doesn't navigate to Customer Debit Memo", "fail");
		}

		Thread.sleep(1000);
		click(lnl_postBusinessUnitIPA);
		Thread.sleep(700);
		selectText(drop_postBusinessUnit, postBusinessUnitSch);
		Thread.sleep(700);
		click(tab_subbaryIPA);
		Thread.sleep(2000);
		String get_post_business_unit = getText(lnl_postBusinessUnitIPA);
		System.out.println(get_post_business_unit);
		if (get_post_business_unit.equals(postBusinessUnitSch)) {
			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User successfully select post business unit from the drop down", "pass");
		} else {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User could'nt select post business unit from the drop down", "fail");
		}

		click(btn_searchCustomerIPA);
		if (isDisplayed(txt_cusAccountSearchIPA)) {
			writeTestResults("Verify user can click on Search button in Customer and Customer Lookup should pop up",
					"User should be click on Search button in Customer and Customer Lookup should pop up",
					"User successfully click on Search button in Customer and Customer Lookup should pop up", "pass");
		} else {

			writeTestResults("Verify user can click on Search button in Customer and Customer Lookup should pop up",
					"User should be click on Search button in Customer and Customer Lookup should pop up",
					"User could'nt click on Search button in Customer and Customer Lookup should pop up", "fail");
		}

		explicitWait(txt_cusAccountSearchIPA, 30);
		sendKeys(txt_cusAccountSearchIPA, customerAccount);
		pressEnter(txt_cusAccountSearchIPA);
		Thread.sleep(1500);
		explicitWait(lnk_resultCustomerAccountIPA, 30);
		if (isDisplayed(lnk_resultCustomerAccountIPA)) {
			writeTestResults("Verify user can search an existing customer by entering Customer name/code",
					"User should be able to search an existing customer by entering Customer name/code",
					"User successfully search an existing customer by entering Customer name/code", "pass");
		} else {

			writeTestResults("Verify user can search an existing customer by entering Customer name/code",
					"User should be able to search an existing customer by entering Customer name/code",
					"User could'nt search an existing customer by entering Customer name/code", "fail");
		}

		doubleClick(lnk_resultCustomerAccountIPA);

		sendKeys(txt_refNoIPA, testAllCharcterWordIPA);

		sendKeys(txt_descIPA, testAllCharcterWordIPA);
		Thread.sleep(2000);

		Select comboBox = new Select(driver.findElement(By.xpath(drop_cusrencyIPA)));
		selectText(drop_cusrencyIPA, currencyUSD);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		if (selectedComboValue.equals(currencyUSD)) {
			writeTestResults("Verify user can choose the currency as USD from the drop down",
					"User should be able to choose the currency as USD from the drop down",
					"User successfully choose the currency as USD from the drop down", "pass");
		} else {

			writeTestResults("Verify user can choose the currency as USD from the drop down",
					"User should be able to choose the currency as USD from the drop down",
					"User could'nt choose the currency as USD from the drop down", "fail");
		}

		click(btn_glLookup);
		getInvObj().handeledSendKeys(txt_GL, glAccountMotorVehicles);
		pressEnter(txt_GL);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles), 30);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles));
		Thread.sleep(2000);

		sendKeys(txt_amountWithGL, amountThousand);

		String ckeckoutBefore = getText(btn_documentDataIPA);

		click(btn_checkout);
		Thread.sleep(3000);
		String ckeckoutAfter = getText(btn_documentDataIPA);
		if (!ckeckoutBefore.equals(ckeckoutAfter)) {
			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User successfully checkout Inbound Payment Advice", "pass");
		} else {

			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User doesn't checkout Inbound Payment Advice", "fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 40);
		if (isDisplayed(btn_reelese)) {

			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice",
					"User sucessfully draft Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice", "User could'nt draft Inboumd Payment Advice",
					"fail");
		}

		Thread.sleep(3000);
		click(btn_reelese);
		explicitWait(header_releasedInboundPaymentAdvice, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedInboundPaymentAdvice)) {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User sucessfully release Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User could'nt release Inboumd Payment Advice", "fail");
		}
	}

	public void inboundPayment_FIN_BD_21() throws Exception {
		switchWindow();
		sendKeys(txt_usdBuyingRate, oneHundredTwentyFive);
		sendKeys(txt_usdSellingRate, oneHundredTwentyFive);
		click(btn_update2);
		Thread.sleep(2000);

		switchWindow();
		click(btn_actionCRV);

		click(btn_converToInboundPaymentCRV);

		explicitWait(header_inboundPaymentNewCustomerRecipt, 30);
		if (isDisplayed(header_inboundPaymentNewCustomerRecipt)) {
			writeTestResults("Verify user can navigate to Inboumd Payment",
					"User should be able to navigate to Inboumd Payment",
					"User sucessfully navigate to Inboumd Payment", "pass");
		} else {
			writeTestResults("Verify user can navigate to Inboumd Payment",
					"User should be able to navigate to Inboumd Payment", "User could'nt navigate to Inboumd Payment",
					"fail");
		}

		String customerName = getAttribute(txt_customerInboundPaymentFrontPage, "value");
		if (customerName.equals(customerAccount)) {
			writeTestResults("Verify that Customer field auto filled", "Customer field should be auto filled",
					"Customer field successfully auto filled", "pass");
		} else {
			writeTestResults("Verify that Customer field auto filled", "Customer field should be auto filled",
					"Customer field successfully auto filled", "fail");
		}

		String customerAddress = getAttribute(txt_billingAddress, "value");
		if (customerAddress.equals(billingAddressCustomerAccount)) {
			writeTestResults("Verify that Address field auto filled", "Address field should be auto filled",
					"Address field successfully auto filled", "pass");
		} else {
			writeTestResults("Verify that Address field auto filled", "Address field should be auto filled",
					"Address field successfully auto filled", "fail");
		}

		String descriptionString = getAttribute(txt_descriptinArea, "value");
		if (descriptionString.equals(testAllCharcterWordIPA)) {
			writeTestResults("Verify that Description field auto filled", "Description field should be auto filled",
					"Description field successfully auto filled", "pass");
		} else {
			writeTestResults("Verify that Description field auto filled", "Description field should be auto filled",
					"Description field successfully auto filled", "fail");
		}

		selectText(drop_paymentMethodCRV, payMethodCheque);

		selectText(drop_bankNameInboundPayment, bank);

		sendKeys(txt_checkNoInboundPayment, getInvObj().currentTimeAndDate().replace("/", "").replace(":", ""));

		String paidCurrencySummaryTab = getSelectedOptionInDropdown(drop_currency);
		if (paidCurrencySummaryTab.equals(currencyUSD)) {
			writeTestResults("Verify that Currency dropdown auto filled as USD",
					"Currency dropdown should be auto filled as USD",
					"Currency dropdown successfully auto filled as USD", "pass");
		} else {
			writeTestResults("Verify that Currency dropdown auto filled as USD",
					"Currency dropdown should be auto filled as USD",
					"Currency dropdown successfully auto filled as USD", "fail");
		}

		click(btn_detailTabCRV);
		String filterCurrencyDetailTab = getSelectedOptionInDropdown(drop_filterCurrency);
		if (filterCurrencyDetailTab.equals(currencyUSD)) {
			writeTestResults("Verify that Filter Currency dropdown dispaly as USD on details tab",
					"Filter Currency dropdown should be display as USD on details tab",
					"Filter Currency dropdown successfully display as USD on details tab", "pass");
		} else {
			writeTestResults("Verify that Filter Currency dropdown dispaly as USD on details tab",
					"Filter Currency dropdown should be display as USD on details tab",
					"Filter Currency dropdown doesn't display as USD on details tab", "fail");
		}

		String paidCurrencyDetailTab = getSelectedOptionInDropdown(drop_paidCurrency);
		if (paidCurrencyDetailTab.equals(currencyUSD)) {
			writeTestResults("Verify that Paid Currency dropdown dispaly as USD on details tab",
					"Paid Currency dropdown should be display as USD on details tab",
					"Paid Currency dropdown successfully display as USD on details tab", "pass");
		} else {
			writeTestResults("Verify that Paid Currency dropdown dispaly as USD on details tab",
					"Paid Currency dropdown should be display as USD on details tab",
					"Paid Currency dropdown doesn't display as USD on details tab", "fail");
		}

		click(btn_detailTabCRV);

		click(chk_firstAdviceOnGridInboundPayment);

		click(btn_checkout);
		Thread.sleep(4000);
		String bannerTotalCRV = getText(btn_documentDataCRV);

		if (!bannerTotalCRV.equals("0")) {
			writeTestResults("Verify user can checkout Cheque Customer Receipt",
					"User should be able to checkout the Cheque Customer Receipt",
					"User successfully checkout Cheque Customer Receipt", "pass");
		} else {

			writeTestResults("Verify user can checkout Cheque Customer Receipt",
					"User should be able to checkout the Cheque Customer Receipt",
					"User doesn't checkout Cheque Customer Receipt", "fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 30);
		if (isDisplayed(btn_reelese)) {
			writeTestResults("Verify user can draft Cheque Customer Receipt",
					"User should be able to draft Cheque Customer Receipt",
					"User sucessfully draft Cheque Customer Receipt", "pass");
		} else {
			writeTestResults("Verify user can draft Cheque Customer Receipt",
					"User should be able to draft Cheque Customer Receipt",
					"User could'nt draft Cheque Customer Receipt", "fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedInboundPayment, 30);
		trackCode = getText(lbl_docNo);
		getFinObj().writeFinanceData("CustomerReciptCheque_FIN_BD_21", trackCode, 29);
		if (isDisplayed(header_releasedInboundPayment)) {
			writeTestResults("Verify user can release Cheque Customer Receipt",
					"User should be able to release Cheque Customer Receipt",
					"User sucessfully release Cheque Customer Receipt", "pass");
		} else {
			writeTestResults("Verify user can release Cheque Customer Receipt",
					"User should be able to release Cheque Customer Receipt",
					"User could'nt release Cheque Customer Receipt", "fail");
		}
	}

	public void bankDeposit_FIN_BD_21() throws Exception {
		switchWindow();
		sendKeys(txt_usdBuyingRate, oneHundredFifty);
		sendKeys(txt_usdSellingRate, oneHundredFifty);
		click(btn_update2);
		Thread.sleep(2000);

		switchWindow();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 20);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 20);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_bankDeposit, 30);
		if (isDisplayed(btn_bankDeposit)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}

		click(btn_bankDeposit);
		explicitWait(header_bankDeposit, 30);
		if (isDisplayed(header_bankDeposit)) {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page",
					"User successfully navigate to Bank Deposit by page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page", "User doesn't navigate to Bank Deposit by page",
					"fail");
		}

		click(btn_newBankDeposit);
		explicitWait(header_newBankDeposit, 30);
		if (isDisplayed(header_newBankDeposit)) {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page successfully displayed",
					"pass");
		} else {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page doesn't displayed", "fail");
		}

		selectText(drop_payMethodBankDeposi, payMethodCheque);
		String selectedComboValue = getSelectedOptionInDropdown(drop_payMethodBankDeposi);
		if (selectedComboValue.equals(payMethodCheque)) {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User successfully select any Pay Method from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User doesn't select any Pay Method from the drop down where the field will populate the selected value",
					"fail");
		}

		sendKeys(txt_descriptinArea, description);
		String descriptionStr = getAttribute(txt_descriptinArea, "value");
		if (descriptionStr.equals(description)) {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User successfully enter characters, numbers, alphanumeric and special characters to Description",
					"pass");
		} else {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User doesn't enter characters, numbers, alphanumeric and special characters to Description",
					"fail");
		}

		selectText(drop_bankNameBD, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bankNameBD);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_accountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_accountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_filterCurrencyBankDeposit, currencyUSD);

		click(btn_viewBankDeposit);

		explicitWait(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerReciptCheque_FIN_BD_21")), 30);
		if (isDisplayed(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerReciptCheque_FIN_BD_21")))) {
			writeTestResults("Verify Customer Receipt get loaded in the document list",
					"Customer Receipt should get loaded in the document list",
					"Customer Receipt successfully get loaded in the document list", "pass");
		} else {
			writeTestResults("Verify Customer Receipt get loaded in the document list",
					"Customer Receipt should get loaded in the document list",
					"Customer Receipt doesn't get loaded in the document list", "fail");
		}

		click(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerReciptCheque_FIN_BD_21")));
		if (isSelected(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerReciptCheque_FIN_BD_21")))) {
			writeTestResults("Verify user able to select 'Deposit'(Customer Recipt) check box",
					"User should be able to select 'Deposit'(Customer Recipt) check box",
					"User successfully select 'Deposit'(Customer Recipt) check box", "pass");
		} else {
			writeTestResults("Verify user able to select 'Deposit'(Customer Recipt) check box",
					"User should be able to select 'Deposit'(Customer Recipt) check box",
					"User doesn't select 'Deposit'(Customer Recipt) check box", "fail");
		}

		String depositAmountCustomerRecipt = getAttribute(txt_depositAmountBankDeposit.replace("doc_no",
				getFinObj().readFinanceData("CustomerReciptCheque_FIN_BD_21")), "value");

		if (depositAmountCustomerRecipt.equals("150000.000000")) {
			writeTestResults(
					"Verify that the full amount of the payment gets filled in the 'Deposit Amount'(Customer Recipt) field in the record",
					"Full amount of the payment should get filled in 'Deposit Amount'(Customer Recipt) field in the record",
					"Full amount of the payment successfully get filled in 'Deposit Amount'(Customer Recipt) field in the record",
					"pass");
		} else {
			writeTestResults(
					"Verify that the full amount of the payment gets filled in the 'Deposit Amount'(Customer Recipt) field in the record",
					"Full amount of the payment should get filled in 'Deposit Amount'(Customer Recipt) field in the record",
					"Full amount of the payment doesn't get filled in 'Deposit Amount'(Customer Recipt) field in the record",
					"fail");
		}

		String depositAmount = getAttribute(txt_depositAmount, "value");
		if (depositAmount.equals("150000.000000")) {
			writeTestResults("Verify Deposit Amount is display with the sum of payments selected",
					"Deposit Amount should be sum of payments selected",
					"Deposit Amount display with the sum of payments selected", "pass");
		} else {
			writeTestResults("Verify Deposit Amount is display with the sum of payments selected",
					"Deposit Amount should be sum of payments selected",
					"Deposit Amount doesn't display with the sum of payments selected", "fail");

		}

		click(btn_draft);
		explicitWait(header_draftedBankDeposit, 30);
		if (isDisplayed(header_draftedBankDeposit)) {
			writeTestResults("Verify user can draft the Bank Deposit", "User should be able to draft the Bank Deposit",
					"User successfully draft the Bank Deposit", "pass");
		} else {
			writeTestResults("Verify user can draft the Bank Deposit", "User should be able to draft the Bank Deposit",
					"User doesn't draft the Bank Deposit", "fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedBankDeposit, 30);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedBankDeposit)) {
			writeTestResults("Verify user can release the Bank Deposit",
					"User should be able to release the Bank Deposit", "User successfully release the Bank Deposit",
					"pass");
		} else {
			writeTestResults("Verify user can release the Bank Deposit",
					"User should be able to release the Bank Deposit", "User doesn't release the Bank Deposit", "fail");
		}

		click(btn_action);
		explicitWait(btn_journel, 10);
		click(btn_journel);
		explicitWait(header_journelEntry, 10);
		if (isDisplayed(header_journelEntry)) {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded",
					"Journal Entry details pop up successfully get loaded", "pass");
		} else {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded", "Journal Entry details pop up doesn't get loaded",
					"fail");
		}

		int idividualJECount = getCount(tbl_coundJournelEntries);
		if (idividualJECount == 1) {
			writeTestResults("Verify Journel Entry count is correct", "Journel Entry count should be correct",
					"Journel Entry count is correct", "pass");
		} else {
			writeTestResults("Verify Journel Entry count is correct", "Journel Entry count should be correct",
					"Journel Entry count is not correct", "fail");
		}

		/* Customer Receipt */
		String debitBalanceBankDeposit = getText(
				lbl_debitJournelEntryBankDepositGLReplace.replace("-je", "0").replace("gl", glBank));
		if (debitBalanceBankDeposit.equals("125000.000000")) {
			writeTestResults(
					"Verify 'Deposit Amount'(Customer Receipt) value is debited to the selected Bank Account [Exchange Rate during the Inbound Payment]",
					"'Deposit Amount'(Customer Receipt) value should get debited to the selected Bank Account [Exchange Rate during the Inbound Payment]",
					"'Deposit Amount'(Customer Receipt) value successfully get debited to the selected Bank Account [Exchange Rate during the Inbound Payment]",
					"pass");
		} else {
			writeTestResults(
					"Verify 'Deposit Amount'(Customer Receipt) value is debited to the selected Bank Account [Exchange Rate during the Inbound Payment]",
					"'Deposit Amount'(Customer Receipt) value should get debited to the selected Bank Account [Exchange Rate during the Inbound Payment]",
					"'Deposit Amount'(Customer Receipt) value doesn't get debited to the selected Bank Account [Exchange Rate during the Inbound Payment]",
					"fail");
		}

		String creditBalanceBankDeposit = getText(
				lbl_creditJournelEntryBankDepositGLReplace.replace("-je", "0").replace("gl", glTradeDebtors));

		if (creditBalanceBankDeposit.equals("100000.000000")) {
			writeTestResults(
					"Verify 'Deposit Amount'(Customer Receipt) value is credited from the Relevant Account [Exchange Rate during the Debit Memo]",
					"'Deposit Amount'(Customer Receipt) value should get credited from the Relevant Account [Exchange Rate during the Debit Memo]",
					"'Deposit Amount'(Customer Receipt) value successfully get credited from the Relevant Account [Exchange Rate during the Debit Memo]",
					"pass");
		} else {
			writeTestResults(
					"Verify 'Deposit Amount'(Customer Receipt) value is credited from the Relevant Account [Exchange Rate during the Debit Memo]",
					"'Deposit Amount'(Customer Receipt) value should get credited from the Relevant Account [Exchange Rate during the Debit Memo]",
					"'Deposit Amount'(Customer Receipt) value doesn't get credited from the Relevant Account [Exchange Rate during the Debit Memo]",
					"fail");
		}

		String creditBalanceCurencyGain = getText(
				lbl_creditJournelEntryBankDepositGLReplace.replace("-je", "0").replace("gl", glCurrencyGain));

		if (creditBalanceCurencyGain.equals("25000.000000")) {
			writeTestResults("Verify 'Gain Amount' value is credited from the Currency Gain account",
					"'Gain Amount' value should get credited from the Currency Gain account",
					"'Gain Amount' value successfully get credited from the Currency Gain account", "pass");
		} else {
			writeTestResults("Verify 'Gain Amount' value is credited from the Currency Gain account",
					"'Gain Amount' value should get credited from the Currency Gain account",
					"'Gain Amount' value doesn't get credited from the Currency Gain account", "fail");
		}

		String debitTotalBankDeposit = getText(lbl_debitTotalJournelEntryRecordReplace.replace("-Record", "0"));
		String credTotalBankDeposit = getText(lbl_creditTotalJournelEntryRecordReplace.replace("-Record", "0"));

		if (debitTotalBankDeposit.equals("125000.000000") && credTotalBankDeposit.equals(debitTotalBankDeposit)) {
			writeTestResults("Verify total debit and credit amounts (Bank Deposit) were balanced",
					"Total debit and credit amounts (Bank Deposit)  should balanced",
					"Total debit and credit amounts (Bank Deposit)  successfully balanced", "pass");
		} else {
			writeTestResults("Verify total debit and credit amounts (Bank Deposit)  were balanced",
					"Total debit and credit amounts (Bank Deposit)  should balanced",
					"Total debit and credit amounts (Bank Deposit) weren't balanced", "fail");
		}

		click(close_headerPopup);

		/* Inbound Payment - Customer Receipt */
		explicitWait(navigation_pane, 30);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_inboundPayment, 30);
		if (isDisplayed(btn_inboundPayment)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}
		click(btn_inboundPayment);
		explicitWait(txt_searchInboundPayment2, 30);
		if (isDisplayed(txt_searchInboundPayment2)) {

			writeTestResults("Verify user can navigate to Inbound payment by-page",
					"User should navigate to Inbound payment by-page",
					"User successfully navigate to Inbound payment by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Inbound payment by-page",
					"User should navigate to Inbound payment by-page",
					"User could'nt navigate to Inbound payment by-page", "fail");
		}

		/* Inbound Payment - Customer Receipt */
		explicitWait(lnl_releventInboundPaymentDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerReciptCheque_FIN_BD_21")), 40);
		if (!isDisplayedQuickCheck(lnl_releventInboundPaymentDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerReciptCheque_FIN_BD_21")))) {
			sendKeys(txt_searchInboundPayment2, getFinObj().readFinanceData("CustomerReciptCheque_FIN_BD_28"));
			pressEnter(txt_searchInboundPayment2);
			explicitWait(lnl_releventInboundPaymentDocReplace.replace("doc_no",
					getFinObj().readFinanceData("CustomerReciptCheque_FIN_BD_21")), 30);

		}

		click(lnl_releventInboundPaymentDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerReciptCheque_FIN_BD_21")));

		explicitWait(btn_action, 30);
		click(btn_action);
		explicitWait(btn_journel, 10);
		click(btn_journel);
		explicitWait(header_journelEntry, 10);
		if (isDisplayed(header_journelEntry)) {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded",
					"Journal Entry details pop up successfully get loaded", "pass");
		} else {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded", "Journal Entry details pop up doesn't get loaded",
					"fail");
		}

		String debitBalanceCustomerRecipt = getText(
				lbl_debitJournelEntryBankDepositGLReplace.replace("-je", "0").replace("gl", glBank));
		if (debitBalanceCustomerRecipt.equals("125000.000000")) {
			writeTestResults(
					"Verify 'Deposit Amount'(Customer Receipt) value is debited to the selected Bank Account [Exchange Rate during the Inbound Payment]",
					"'Deposit Amount'(Customer Receipt) value should get debited to the selected Bank Account [Exchange Rate during the Inbound Payment]",
					"'Deposit Amount'(Customer Receipt) value successfully get debited to the selected Bank Account [Exchange Rate during the Inbound Payment]",
					"pass");
		} else {
			writeTestResults(
					"Verify 'Deposit Amount'(Customer Receipt) value is debited to the selected Bank Account [Exchange Rate during the Inbound Payment]",
					"'Deposit Amount'(Customer Receipt) value should get debited to the selected Bank Account [Exchange Rate during the Inbound Payment]",
					"'Deposit Amount'(Customer Receipt) value doesn't get debited to the selected Bank Account [Exchange Rate during the Inbound Payment]",
					"fail");
		}

		String creditBalanceCustomerRecipt = getText(
				lbl_creditJournelEntryBankDepositGLReplace.replace("-je", "0").replace("gl", glTradeDebtors));

		if (creditBalanceCustomerRecipt.equals("100000.000000")) {
			writeTestResults(
					"Verify 'Deposit Amount'(Customer Receipt) value is credited from the Relevant Account [Exchange Rate during the Debit Memo]",
					"'Deposit Amount'(Customer Receipt) value should get credited from the Relevant Account [Exchange Rate during the Debit Memo]",
					"'Deposit Amount'(Customer Receipt) value successfully get credited from the Relevant Account [Exchange Rate during the Debit Memo]",
					"pass");
		} else {
			writeTestResults(
					"Verify 'Deposit Amount'(Customer Receipt) value is credited from the Relevant Account [Exchange Rate during the Debit Memo]",
					"'Deposit Amount'(Customer Receipt) value should get credited from the Relevant Account [Exchange Rate during the Debit Memo]",
					"'Deposit Amount'(Customer Receipt) value doesn't get credited from the Relevant Account [Exchange Rate during the Debit Memo]",
					"fail");
		}

		String creditBalanceCurencyGainCustomerRecipt = getText(
				lbl_creditJournelEntryBankDepositGLReplace.replace("-je", "0").replace("gl", glCurrencyGain));

		if (creditBalanceCurencyGainCustomerRecipt.equals("25000.000000")) {
			writeTestResults("Verify 'Gain Amount' value is credited from the Currency Gain account",
					"'Gain Amount' value should get credited from the Currency Gain account",
					"'Gain Amount' value successfully get credited from the Currency Gain account", "pass");
		} else {
			writeTestResults("Verify 'Gain Amount' value is credited from the Currency Gain account",
					"'Gain Amount' value should get credited from the Currency Gain account",
					"'Gain Amount' value doesn't get credited from the Currency Gain account", "fail");
		}

		String debitTotalCustomerRecipt = getText(lbl_debitTotalJournelEntryRecordReplace.replace("-Record", "0"));
		String credTotalCustomerRecipt = getText(lbl_creditTotalJournelEntryRecordReplace.replace("-Record", "0"));

		if (debitTotalCustomerRecipt.equals("125000.000000")
				&& credTotalCustomerRecipt.equals(debitTotalCustomerRecipt)) {
			writeTestResults("Verify total debit and credit amounts (Inbound Payment) were balanced",
					"Total debit and credit amounts (Inbound Payment)  should balanced",
					"Total debit and credit amounts (Inbound Payment)  successfully balanced", "pass");
		} else {
			writeTestResults("Verify total debit and credit amounts (Inbound Payment)  were balanced",
					"Total debit and credit amounts (Inbound Payment)  should balanced",
					"Total debit and credit amounts (Inbound Payment) weren't balanced", "fail");
		}

	}

	/* FIN_BD_22 */
	public void inboundPaymentAdvice_FIN_BD_22() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_inboundPaymentAdvice, 30);
		if (isDisplayed(btn_inboundPaymentAdvice)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}
		click(btn_inboundPaymentAdvice);
		explicitWait(btn_newInboundPaymentAdvice, 30);
		if (isDisplayed(btn_newInboundPaymentAdvice)) {

			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User successfully navigate to Inbound payment advice by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User could'nt navigate to Inbound payment advice by-page", "fail");
		}

		click(btn_newInboundPaymentAdvice);
		if (isDisplayed(btn_CusAdvanceJourneyIPA)) {

			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User successfully view the list of journey",
					"pass");
		} else {
			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User could'nt view the list of journey",
					"fail");
		}

		click(btn_CusAdvanceJourneyIPA);
		explicitWait(lnl_postBusinessUnitIPA, 30);
		if (isDisplayed(lnl_postBusinessUnitIPA)) {

			writeTestResults("Verify user able to navigate to Customer Advance/ Customer Debit Memo",
					"User should be able to navigate to Customer Advance/ Customer Debit Memo",
					"User successfully navigate to Customer Advance/ Customer Debit Memo", "pass");
		} else {
			writeTestResults("Verify user able to navigate to Customer Advance/ Customer Debit Memo",
					"User should be able to navigate to Customer Advance/ Customer Debit Memo",
					"User doesn't navigate to Customer Advance/ Customer Debit Memo", "fail");
		}

		Thread.sleep(1000);
		click(lnl_postBusinessUnitIPA);
		Thread.sleep(700);
		selectText(drop_postBusinessUnit, postBusinessUnitSch);
		Thread.sleep(700);
		click(tab_subbaryIPA);
		Thread.sleep(2000);
		String get_post_business_unit = getText(lnl_postBusinessUnitIPA);
		System.out.println(get_post_business_unit);
		if (get_post_business_unit.equals(postBusinessUnitSch)) {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User successfully select post business unit from the drop down", "pass");
		} else {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User could'nt select post business unit from the drop down", "fail");
		}

		click(btn_searchCustomerIPA);
		if (isDisplayed(txt_cusAccountSearchIPA)) {
			writeTestResults("Verify user can click on Search button in Customer and Customer Lookup should pop up",
					"User should be click on Search button in Customer and Customer Lookup should pop up",
					"User successfully click on Search button in Customer and Customer Lookup should pop up", "pass");
		} else {

			writeTestResults("Verify user can click on Search button in Customer and Customer Lookup should pop up",
					"User should be click on Search button in Customer and Customer Lookup should pop up",
					"User could'nt click on Search button in Customer and Customer Lookup should pop up", "fail");
		}

		explicitWait(txt_cusAccountSearchIPA, 30);
		sendKeys(txt_cusAccountSearchIPA, customerAccount);
		pressEnter(txt_cusAccountSearchIPA);
		Thread.sleep(1500);
		explicitWait(lnk_resultCustomerAccountIPA, 30);
		if (isDisplayed(lnk_resultCustomerAccountIPA)) {
			writeTestResults("Verify user can search an existing customer by entering Customer name/code",
					"User should be able to search an existing customer by entering Customer name/code",
					"User successfully search an existing customer by entering Customer name/code", "pass");
		} else {

			writeTestResults("Verify user can search an existing customer by entering Customer name/code",
					"User should be able to search an existing customer by entering Customer name/code",
					"User could'nt search an existing customer by entering Customer name/code", "fail");
		}

		doubleClick(lnk_resultCustomerAccountIPA);

		sendKeys(txt_refNoIPA, testAllCharcterWordIPA);

		sendKeys(txt_descIPA, testAllCharcterWordIPA);
		Thread.sleep(2000);

		Select comboBox = new Select(driver.findElement(By.xpath(drop_cusrencyIPA)));
		selectText(drop_cusrencyIPA, currencyLKR);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		if (selectedComboValue.equals(currencyLKR)) {
			writeTestResults("Verify user can choose the currency from the drop down",
					"User should be able to choose the currency from the drop down",
					"User successfully choose the currency from the drop down", "pass");
		} else {

			writeTestResults("Verify user can choose the currency from the drop down",
					"User should be able to choose the currency from the drop down",
					"User could'nt choose the currency from the drop down", "fail");
		}

		sendKeys(txt_amountIPA, amountTwoThousand);
		Thread.sleep(2000);
		String ckeckoutBefore = getText(btn_documentDataIPA);

		click(btn_checkout);
		Thread.sleep(3000);
		String ckeckoutAfter = getText(btn_documentDataIPA);
		if (!ckeckoutBefore.equals(ckeckoutAfter)) {
			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User successfully checkout Inbound Payment Advice", "pass");
		} else {

			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User doesn't checkout Inbound Payment Advice", "fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 40);
		if (isDisplayed(btn_reelese)) {

			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice",
					"User sucessfully draft Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice", "User could'nt draft Inboumd Payment Advice",
					"fail");
		}

		Thread.sleep(3000);
		click(btn_reelese);
		explicitWait(header_releasedInboundPaymentAdvice, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedInboundPaymentAdvice)) {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User sucessfully release Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User could'nt release Inboumd Payment Advice", "fail");
		}
	}

	public void inboundPayment_FIN_BD_22() throws Exception {
		click(btn_actionCRV);

		click(btn_converToInboundPaymentCRV);
		click(btn_detailTabCRV);

		click(btn_summaryTab);

		selectText(drop_paidCurrencyCRV, currencyLKR);

		selectText(drop_paymentMethodCRV, payMethodCash);

		selectText(drop_cashAccount, cashAccount);

		click(btn_detailTabCRV);

		click(btn_detailTabCRV);
		selectText(drop_filterCurrencyCRV, currencyLKR);

		click(chk_firstAdviceOnGridInboundPayment);

		click(btn_checkout);
		Thread.sleep(4000);
		String bannerTotalCRV = getText(btn_documentDataCRV);

		if (!bannerTotalCRV.equals("0")) {
			writeTestResults("Verify user can checkout Cash Customer Receipt",
					"User should be able to checkout the Cash Customer Receipt",
					"User successfully checkout Cash Customer Receipt", "pass");
		} else {

			writeTestResults("Verify user can checkout Cash Customer Receipt",
					"User should be able to checkout the Cash Customer Receipt",
					"User doesn't checkout Cash Customer Receipt", "fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 30);
		if (isDisplayed(btn_reelese)) {
			writeTestResults("Verify user can draft Cash Customer Receipt",
					"User should be able to draft Cash Customer Receipt",
					"User sucessfully draft Cash Customer Receipt", "pass");
		} else {
			writeTestResults("Verify user can draft Cash Customer Receipt",
					"User should be able to draft Cash Customer Receipt", "User could'nt draft Cash Customer Receipt",
					"fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedInboundPayment, 30);
		trackCode = getText(lbl_docNo);
		customerRecipt = trackCode;
		getFinObj().writeFinanceData("CustomerRecipt_FIN_BD_22", customerRecipt, 13);
		if (isDisplayed(header_releasedInboundPayment)) {
			writeTestResults("Verify user can release Cash Customer Receipt",
					"User should be able to release Cash Customer Receipt",
					"User sucessfully release Cash Customer Receipt", "pass");
		} else {
			writeTestResults("Verify user can release Cash Customer Receipt",
					"User should be able to release Cash Customer Receipt",
					"User could'nt release Cash Customer Receipt", "fail");
		}
	}

	public void bankDeposit_FIN_BD_22() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_bankDeposit, 30);
		if (isDisplayed(btn_bankDeposit)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}

		click(btn_bankDeposit);
		explicitWait(header_bankDeposit, 30);
		if (isDisplayed(header_bankDeposit)) {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page",
					"User successfully navigate to Bank Deposit by page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page", "User doesn't navigate to Bank Deposit by page",
					"fail");
		}

		click(btn_newBankDeposit);
		explicitWait(header_newBankDeposit, 30);
		if (isDisplayed(header_newBankDeposit)) {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page successfully displayed",
					"pass");
		} else {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page doesn't displayed", "fail");
		}

		selectText(drop_payMethodBankDeposi, payMethodCash);
		String selectedComboValue = getSelectedOptionInDropdown(drop_payMethodBankDeposi);
		if (selectedComboValue.equals(payMethodCash)) {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User successfully select any Pay Method from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User doesn't select any Pay Method from the drop down where the field will populate the selected value",
					"fail");
		}

		sendKeys(txt_descriptinArea, description);
		String descriptionStr = getAttribute(txt_descriptinArea, "value");
		if (descriptionStr.equals(description)) {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User successfully enter characters, numbers, alphanumeric and special characters to Description",
					"pass");
		} else {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User doesn't enter characters, numbers, alphanumeric and special characters to Description",
					"fail");
		}

		selectText(drop_bankNameBD, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bankNameBD);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_accountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_accountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_viewBankDeposit);
		explicitWait(div_paymentDocumentListBankDepositDocReplace.replace("doc_no", customerRecipt), 30);
		if (isDisplayed(div_paymentDocumentListBankDepositDocReplace.replace("doc_no", customerRecipt))) {
			writeTestResults("Verify Customer Receipt get loaded in the document list",
					"Customer Receipt should get loaded in the document list",
					"Customer Receipt successfully get loaded in the document list", "pass");
		} else {
			writeTestResults("Verify Customer Receipt get loaded in the document list",
					"Customer Receipt should get loaded in the document list",
					"Customer Receipt doesn't get loaded in the document list", "fail");
		}

		click(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no", customerRecipt));

		if (isSelected(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no", customerRecipt))) {
			writeTestResults("Verify user able to select the Customer Receipt to deposit",
					"User should be able to select the Customer Receipt to deposit",
					"User successfully select the Customer Receipt to deposit", "pass");
		} else {
			writeTestResults("Verify user able to select the Customer Receipt to deposit",
					"User should be able to select the Customer Receipt to deposit",
					"User doesn't select the Customer Receipt to deposit", "fail");
		}

		String depositAmount = getAttribute(txt_depositAmount, "value");
		if (depositAmount.equals("2000.000000")) {
			writeTestResults("Verify that Customer Receipt Amount (2000) get added to Deposit Amount",
					"Customer Receipt Amount (2000) should get added to Deposit Amount",
					"Customer Receipt Amount (2000) successfully get added to Deposit Amount", "pass");
		} else {
			writeTestResults("Verify that Customer Receipt Amount (2000) get added to Deposit Amount",
					"Customer Receipt Amount (2000) should get added to Deposit Amount",
					"Customer Receipt Amount (2000) doesn't get added to Deposit Amount", "fail");
		}
	}

	/* FIN_BD_23 */
	public void inboundPaymentAdvice_FIN_BD_23() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_inboundPaymentAdvice, 30);
		if (isDisplayed(btn_inboundPaymentAdvice)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}
		click(btn_inboundPaymentAdvice);
		explicitWait(btn_newInboundPaymentAdvice, 30);
		if (isDisplayed(btn_newInboundPaymentAdvice)) {

			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User successfully navigate to Inbound payment advice by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User could'nt navigate to Inbound payment advice by-page", "fail");
		}

		click(btn_newInboundPaymentAdvice);
		if (isDisplayed(btn_CusAdvanceJourneyIPA)) {

			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User successfully view the list of journey",
					"pass");
		} else {
			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User could'nt view the list of journey",
					"fail");
		}

		click(btn_journeyVendorRefund);
		explicitWait(lnl_postBusinessUnitIPA, 30);
		if (isDisplayed(lnl_postBusinessUnitIPA)) {
			writeTestResults("Verify user can navigate to Vendor Refund",
					"User should be able to navigate to Vendor Refund", "User successfully navigate to Vendor Refund",
					"pass");
		} else {
			writeTestResults("Verify user can navigate to Vendor Refund",
					"User should be able to navigate to Vendor Refund", "User doesn't navigate to Vendor Refund",
					"fail");
		}

		Thread.sleep(1000);
		click(lnl_postBusinessUnitIPA);
		Thread.sleep(700);
		selectText(drop_postBusinessUnit, postBusinessUnitSch);
		Thread.sleep(700);
		click(tab_subbaryIPA);
		Thread.sleep(2000);
		String get_post_business_unit = getText(lnl_postBusinessUnitIPA);
		System.out.println(get_post_business_unit);
		if (get_post_business_unit.equals(postBusinessUnitSch)) {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User successfully select post business unit from the drop down", "pass");
		} else {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User could'nt select post business unit from the drop down", "fail");
		}

		click(btn_vendorLoockup);
		explicitWait(txt_vendorSearch, 30);
		if (isDisplayed(txt_vendorSearch)) {
			writeTestResults("Verify user can click on Search button in Vendor and Vendor Lookup should pop up",
					"User should be click on Search button in Vendor and Vendor Lookup should pop up",
					"User successfully click on Search button in Vendor and Vendor Lookup should pop up", "pass");
		} else {

			writeTestResults("Verify user can click on Search button in Vendor and Vendor Lookup should pop up",
					"User should be click on Search button in Vendor and Vendor Lookup should pop up",
					"User could'nt click on Search button in Vendor and Vendor Lookup should pop up", "fail");
		}

		sendKeys(txt_vendorSearch, vendorAccount);
		pressEnter(txt_vendorSearch);
		Thread.sleep(1500);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", vendorAccount), 30);
		if (isDisplayed(lnk_inforOnLookupInformationReplace.replace("info", vendorAccount))) {
			writeTestResults("Verify user can search an existing vendor by entering Vendor name/code",
					"User should be able to search an existing vendor by entering Vendor name/code",
					"User successfully search an existing vendor by entering Vendor name/code", "pass");
		} else {

			writeTestResults("Verify user can search an existing vendor by entering Vendor name/code",
					"User should be able to search an existing vendor by entering Vendor name/code",
					"User could'nt search an existing vendor by entering Vendor name/code", "fail");
		}

		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", vendorAccount));

		sendKeys(txt_refNoIPA, testAllCharcterWordIPA);

		sendKeys(txt_descIPA, testAllCharcterWordIPA);
		Thread.sleep(2000);

		Select comboBox = new Select(driver.findElement(By.xpath(drop_cusrencyIPA)));
		selectText(drop_cusrencyIPA, currencyLKR);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		if (selectedComboValue.equals(currencyLKR)) {
			writeTestResults("Verify user can choose the currency from the drop down",
					"User should be able to choose the currency from the drop down",
					"User successfully choose the currency from the drop down", "pass");
		} else {

			writeTestResults("Verify user can choose the currency from the drop down",
					"User should be able to choose the currency from the drop down",
					"User could'nt choose the currency from the drop down", "fail");
		}

		click(btn_glLookupVendorRefund);
		getInvObj().handeledSendKeys(txt_GL, glAccountVendorRefund);
		pressEnter(txt_GL);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", glAccountVendorRefund), 30);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", glAccountVendorRefund));
		Thread.sleep(2000);

		sendKeys(txt_amountVendorRefund, fiftyThousand);

		Thread.sleep(2000);
		String ckeckoutBefore = getText(btn_documentDataIPA);

		click(btn_checkout);
		Thread.sleep(3000);
		String ckeckoutAfter = getText(btn_documentDataIPA);
		if (!ckeckoutBefore.equals(ckeckoutAfter)) {
			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User successfully checkout Inbound Payment Advice", "pass");
		} else {

			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User doesn't checkout Inbound Payment Advice", "fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 40);
		if (isDisplayed(btn_reelese)) {

			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice",
					"User sucessfully draft Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice", "User could'nt draft Inboumd Payment Advice",
					"fail");
		}

		Thread.sleep(3000);
		click(btn_reelese);
		explicitWait(header_releasedInboundPaymentAdvice, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedInboundPaymentAdvice)) {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User sucessfully release Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User could'nt release Inboumd Payment Advice", "fail");
		}
	}

	public void inboundPayment_FIN_BD_23() throws Exception {
		click(btn_actionCRV);

		click(btn_converToInboundPaymentCRV);
		click(btn_detailTabCRV);

		click(btn_summaryTab);

		selectText(drop_paidCurrencyCRV, currencyLKR);

		selectText(drop_paymentMethodCRV, payMethodCash);

		selectText(drop_cashAccount, cashAccount);

		click(btn_detailTabCRV);

		click(btn_detailTabCRV);
		selectText(drop_filterCurrencyCRV, currencyLKR);

		click(chk_firstAdviceOnGridInboundPayment);

		click(btn_checkout);
		Thread.sleep(4000);
		String bannerTotalCRV = getText(btn_documentDataCRV);

		if (!bannerTotalCRV.equals("0")) {
			writeTestResults("Verify user can checkout Cash Vendor Receipt",
					"User should be able to checkout the Cash Vendor Receipt",
					"User successfully checkout Cash Vendor Receipt", "pass");
		} else {

			writeTestResults("Verify user can checkout Cash Vendor Receipt",
					"User should be able to checkout the Cash Vendor Receipt",
					"User doesn't checkout Cash Vendor Receipt", "fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 30);
		if (isDisplayed(btn_reelese)) {
			writeTestResults("Verify user can draft Cash Vendor Receipt",
					"User should be able to draft Cash Vendor Receipt", "User sucessfully draft Cash Vendor Receipt",
					"pass");
		} else {
			writeTestResults("Verify user can draft Cash Vendor Receipt",
					"User should be able to draft Cash Vendor Receipt", "User could'nt draft Cash Vendor Receipt",
					"fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedInboundPayment, 30);
		trackCode = getText(lbl_docNo);
		vendorRecipt = trackCode;
		getFinObj().writeFinanceData("VendorRecipt_FIN_BD_023", vendorRecipt, 14);
		if (isDisplayed(header_releasedInboundPayment)) {
			writeTestResults("Verify user can release Cash Vendor Receipt",
					"User should be able to release Cash Vendor Receipt",
					"User sucessfully release Cash Vendor Receipt", "pass");
		} else {
			writeTestResults("Verify user can release Cash Vendor Receipt",
					"User should be able to release Cash Vendor Receipt", "User could'nt release Cash Vendor Receipt",
					"fail");
		}
	}

	public void bankDeposit_FIN_BD_23() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_bankDeposit, 30);
		if (isDisplayed(btn_bankDeposit)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}

		click(btn_bankDeposit);
		explicitWait(header_bankDeposit, 30);
		if (isDisplayed(header_bankDeposit)) {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page",
					"User successfully navigate to Bank Deposit by page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page", "User doesn't navigate to Bank Deposit by page",
					"fail");
		}

		click(btn_newBankDeposit);
		explicitWait(header_newBankDeposit, 30);
		if (isDisplayed(header_newBankDeposit)) {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page successfully displayed",
					"pass");
		} else {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page doesn't displayed", "fail");
		}

		selectText(drop_payMethodBankDeposi, payMethodCash);
		String selectedComboValue = getSelectedOptionInDropdown(drop_payMethodBankDeposi);
		if (selectedComboValue.equals(payMethodCash)) {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User successfully select any Pay Method from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User doesn't select any Pay Method from the drop down where the field will populate the selected value",
					"fail");
		}

		sendKeys(txt_descriptinArea, description);
		String descriptionStr = getAttribute(txt_descriptinArea, "value");
		if (descriptionStr.equals(description)) {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User successfully enter characters, numbers, alphanumeric and special characters to Description",
					"pass");
		} else {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User doesn't enter characters, numbers, alphanumeric and special characters to Description",
					"fail");
		}

		selectText(drop_bankNameBD, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bankNameBD);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_accountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_accountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_viewBankDeposit);
		explicitWait(div_paymentDocumentListBankDepositDocReplace.replace("doc_no", vendorRecipt), 30);
		if (isDisplayed(div_paymentDocumentListBankDepositDocReplace.replace("doc_no", vendorRecipt))) {
			writeTestResults("Verify Vendor Receipt get loaded in the document list",
					"Vendor Receipt should get loaded in the document list",
					"Vendor Receipt successfully get loaded in the document list", "pass");
		} else {
			writeTestResults("Verify Vendor Receipt get loaded in the document list",
					"Vendor Receipt should get loaded in the document list",
					"Vendor Receipt doesn't get loaded in the document list", "fail");
		}

		click(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no", vendorRecipt));

		if (isSelected(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no", vendorRecipt))) {
			writeTestResults("Verify user able to select the Vendor Receipt to deposit",
					"User should be able to select the Vendor Receipt to deposit",
					"User successfully select the Vendor Receipt to deposit", "pass");
		} else {
			writeTestResults("Verify user able to select the Vendor Receipt to deposit",
					"User should be able to select the Vendor Receipt to deposit",
					"User doesn't select the Vendor Receipt to deposit", "fail");
		}

		String depositAmount = getAttribute(txt_depositAmount, "value");
		if (depositAmount.equals("50000.000000")) {
			writeTestResults("Verify that Vendor Receipt Amount (50000) get added to Deposit Amount",
					"Vendor Receipt Amount (50000) should get added to Deposit Amount",
					"Vendor Receipt Amount (50000) successfully get added to Deposit Amount", "pass");
		} else {
			writeTestResults("Verify that Vendor Receipt Amount (50000) get added to Deposit Amount",
					"Vendor Receipt Amount (50000) should get added to Deposit Amount",
					"Vendor Receipt Amount (50000) doesn't get added to Deposit Amount", "fail");
		}
	}

	/* FIN_BD_24 */
	public void inboundPaymentAdvice_FIN_BD_24() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_inboundPaymentAdvice, 30);
		if (isDisplayed(btn_inboundPaymentAdvice)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}
		click(btn_inboundPaymentAdvice);
		explicitWait(btn_newInboundPaymentAdvice, 30);
		if (isDisplayed(btn_newInboundPaymentAdvice)) {

			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User successfully navigate to Inbound payment advice by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User could'nt navigate to Inbound payment advice by-page", "fail");
		}

		click(btn_newInboundPaymentAdvice);
		if (isDisplayed(btn_CusAdvanceJourneyIPA)) {

			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User successfully view the list of journey",
					"pass");
		} else {
			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User could'nt view the list of journey",
					"fail");
		}

		click(btn_journeyReciptVoucher);
		explicitWait(lnl_postBusinessUnitIPA, 30);
		if (isDisplayed(lnl_postBusinessUnitIPA)) {
			writeTestResults("Verify user can navigate to Recipt Voucher",
					"User should be able to navigate to Recipt Voucher", "User successfully navigate to Recipt Voucher",
					"pass");
		} else {
			writeTestResults("Verify user can navigate to Recipt Voucher",
					"User should be able to navigate to Recipt Voucher", "User doesn't navigate to Recipt Voucher",
					"fail");
		}

		Thread.sleep(1000);
		click(lnl_postBusinessUnitIPA);
		Thread.sleep(700);
		selectText(drop_postBusinessUnit, postBusinessUnitSch);
		Thread.sleep(700);
		click(tab_subbaryIPA);
		Thread.sleep(2000);
		String get_post_business_unit = getText(lnl_postBusinessUnitIPA);
		System.out.println(get_post_business_unit);
		if (get_post_business_unit.equals(postBusinessUnitSch)) {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User successfully select post business unit from the drop down", "pass");
		} else {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User could'nt select post business unit from the drop down", "fail");
		}

		sendKeys(txt_descIPA, testAllCharcterWordIPA);
		Thread.sleep(2000);

		Select comboBox = new Select(driver.findElement(By.xpath(drop_cusrencyIPA)));
		selectText(drop_cusrencyIPA, currencyLKR);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		if (selectedComboValue.equals(currencyLKR)) {
			writeTestResults("Verify user can choose the currency from the drop down",
					"User should be able to choose the currency from the drop down",
					"User successfully choose the currency from the drop down", "pass");
		} else {

			writeTestResults("Verify user can choose the currency from the drop down",
					"User should be able to choose the currency from the drop down",
					"User could'nt choose the currency from the drop down", "fail");
		}

		click(btn_glLookup);
		getInvObj().handeledSendKeys(txt_GL, glAccountMotorVehicles);
		pressEnter(txt_GL);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles), 30);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles));
		Thread.sleep(2000);

		sendKeys(txt_amountReciptVoucher, eightThousand);

		Thread.sleep(2000);
		String ckeckoutBefore = getText(btn_documentDataIPA);

		click(btn_checkout);
		Thread.sleep(3000);
		String ckeckoutAfter = getText(btn_documentDataIPA);
		if (!ckeckoutBefore.equals(ckeckoutAfter)) {
			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User successfully checkout Inbound Payment Advice", "pass");
		} else {

			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User doesn't checkout Inbound Payment Advice", "fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 40);
		if (isDisplayed(btn_reelese)) {

			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice",
					"User sucessfully draft Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice", "User could'nt draft Inboumd Payment Advice",
					"fail");
		}

		Thread.sleep(3000);
		click(btn_reelese);
		explicitWait(header_releasedInboundPaymentAdvice, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedInboundPaymentAdvice)) {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User sucessfully release Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User could'nt release Inboumd Payment Advice", "fail");
		}
	}

	public void inboundPayment_FIN_BD_24() throws Exception {
		click(btn_actionCRV);

		click(btn_converToInboundPaymentCRV);
		click(btn_detailTabCRV);

		click(btn_summaryTab);

		selectText(drop_paidCurrencyCRV, currencyLKR);

		selectText(drop_paymentMethodCRV, payMethodCash);

		selectText(drop_cashAccount, cashAccount);

		click(btn_detailTabCRV);

		click(btn_detailTabCRV);
		selectText(drop_filterCurrencyCRV, currencyLKR);

		click(chk_firstAdviceOnGridInboundPayment);

		click(btn_checkout);
		Thread.sleep(4000);
		String bannerTotalCRV = getText(btn_documentDataCRV);

		if (!bannerTotalCRV.equals("0")) {
			writeTestResults("Verify user can checkout Cash GL Recipt",
					"User should be able to checkout the Cash GL Recipt", "User successfully checkout Cash GL Recipt",
					"pass");
		} else {

			writeTestResults("Verify user can checkout Cash GL Recipt",
					"User should be able to checkout the Cash GL Recipt", "User doesn't checkout Cash GL Recipt",
					"fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 30);
		if (isDisplayed(btn_reelese)) {
			writeTestResults("Verify user can draft Cash GL Recipt", "User should be able to draft Cash GL Recipt",
					"User sucessfully draft Cash GL Recipt", "pass");
		} else {
			writeTestResults("Verify user can draft Cash GL Recipt", "User should be able to draft Cash GL Recipt",
					"User could'nt draft Cash GL Recipt", "fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedInboundPayment, 30);
		trackCode = getText(lbl_docNo);
		glRecipt = trackCode;
		getFinObj().writeFinanceData("GlRecipt_FIN_BD_024", glRecipt, 15);
		if (isDisplayed(header_releasedInboundPayment)) {
			writeTestResults("Verify user can release Cash GL Recipt", "User should be able to release Cash GL Recipt",
					"User sucessfully release Cash GL Recipt", "pass");
		} else {
			writeTestResults("Verify user can release Cash GL Recipt", "User should be able to release Cash GL Recipt",
					"User could'nt release Cash GL Recipt", "fail");
		}
	}

	public void bankDeposit_FIN_BD_24() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_bankDeposit, 30);
		if (isDisplayed(btn_bankDeposit)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}

		click(btn_bankDeposit);
		explicitWait(header_bankDeposit, 30);
		if (isDisplayed(header_bankDeposit)) {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page",
					"User successfully navigate to Bank Deposit by page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page", "User doesn't navigate to Bank Deposit by page",
					"fail");
		}

		click(btn_newBankDeposit);
		explicitWait(header_newBankDeposit, 30);
		if (isDisplayed(header_newBankDeposit)) {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page successfully displayed",
					"pass");
		} else {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page doesn't displayed", "fail");
		}

		selectText(drop_payMethodBankDeposi, payMethodCash);
		String selectedComboValue = getSelectedOptionInDropdown(drop_payMethodBankDeposi);
		if (selectedComboValue.equals(payMethodCash)) {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User successfully select any Pay Method from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User doesn't select any Pay Method from the drop down where the field will populate the selected value",
					"fail");
		}

		sendKeys(txt_descriptinArea, description);
		String descriptionStr = getAttribute(txt_descriptinArea, "value");
		if (descriptionStr.equals(description)) {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User successfully enter characters, numbers, alphanumeric and special characters to Description",
					"pass");
		} else {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User doesn't enter characters, numbers, alphanumeric and special characters to Description",
					"fail");
		}

		selectText(drop_bankNameBD, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bankNameBD);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_accountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_accountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_viewBankDeposit);
		explicitWait(div_paymentDocumentListBankDepositDocReplace.replace("doc_no", glRecipt), 30);
		if (isDisplayed(div_paymentDocumentListBankDepositDocReplace.replace("doc_no", glRecipt))) {
			writeTestResults("Verify GL Recipt get loaded in the document list",
					"GL Recipt should get loaded in the document list",
					"GL Recipt successfully get loaded in the document list", "pass");
		} else {
			writeTestResults("Verify GL Recipt get loaded in the document list",
					"GL Recipt should get loaded in the document list",
					"GL Recipt doesn't get loaded in the document list", "fail");
		}

		click(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no", glRecipt));

		if (isSelected(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no", glRecipt))) {
			writeTestResults("Verify user able to select the GL Recipt to deposit",
					"User should be able to select the GL Recipt to deposit",
					"User successfully select the GL Recipt to deposit", "pass");
		} else {
			writeTestResults("Verify user able to select the GL Recipt to deposit",
					"User should be able to select the GL Recipt to deposit",
					"User doesn't select the GL Recipt to deposit", "fail");
		}

		String depositAmount = getAttribute(txt_depositAmount, "value");
		if (depositAmount.equals("8000.000000")) {
			writeTestResults("Verify that GL Recipt Amount (8000) get added to Deposit Amount",
					"GL Recipt Amount (8000) should get added to Deposit Amount",
					"GL Recipt Amount (8000) successfully get added to Deposit Amount", "pass");
		} else {
			writeTestResults("Verify that GL Recipt Amount (8000) get added to Deposit Amount",
					"GL Recipt Amount (8000) should get added to Deposit Amount",
					"GL Recipt Amount (8000) doesn't get added to Deposit Amount", "fail");
		}
	}

	/* FIN_BD_25 */
	public void inboundPaymentAdvice_FIN_BD_25() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_inboundPaymentAdvice, 30);
		if (isDisplayed(btn_inboundPaymentAdvice)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}
		click(btn_inboundPaymentAdvice);
		explicitWait(btn_newInboundPaymentAdvice, 30);
		if (isDisplayed(btn_newInboundPaymentAdvice)) {

			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User successfully navigate to Inbound payment advice by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User could'nt navigate to Inbound payment advice by-page", "fail");
		}

		click(btn_newInboundPaymentAdvice);
		if (isDisplayed(btn_journeiesDows)) {

			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User successfully view the list of journey",
					"pass");
		} else {
			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User could'nt view the list of journey",
					"fail");
		}

		click(btn_journeiesDows);

		click(btn_journeyEmployeeReceiptAdvice);
		explicitWait(lnl_postBusinessUnitIPA, 30);
		if (isDisplayed(lnl_postBusinessUnitIPA)) {
			writeTestResults("Verify user can navigate to Recipt Voucher",
					"User should be able to navigate to Recipt Voucher", "User successfully navigate to Recipt Voucher",
					"pass");
		} else {
			writeTestResults("Verify user can navigate to Recipt Voucher",
					"User should be able to navigate to Recipt Voucher", "User doesn't navigate to Recipt Voucher",
					"fail");
		}

		Thread.sleep(1000);
		click(lnl_postBusinessUnitIPA);
		Thread.sleep(700);
		selectText(drop_postBusinessUnit, postBusinessUnitSch);
		Thread.sleep(700);
		click(tab_subbaryIPA);
		Thread.sleep(2000);
		String get_post_business_unit = getText(lnl_postBusinessUnitIPA);
		System.out.println(get_post_business_unit);
		if (get_post_business_unit.equals(postBusinessUnitSch)) {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User successfully select post business unit from the drop down", "pass");
		} else {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User could'nt select post business unit from the drop down", "fail");
		}

		click(btn_employeeLookup);
		getInvObj().handeledSendKeys(txt_searchEmployee, employeeMadhushan);
		pressEnter(txt_searchEmployee);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", employeeMadhushan), 50);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", employeeMadhushan));
		Thread.sleep(2000);

		sendKeys(txt_descIPA, testAllCharcterWordIPA);
		Thread.sleep(2000);

		Select comboBox = new Select(driver.findElement(By.xpath(drop_cusrencyIPA)));
		selectText(drop_cusrencyIPA, currencyLKR);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		if (selectedComboValue.equals(currencyLKR)) {
			writeTestResults("Verify user can choose the currency from the drop down",
					"User should be able to choose the currency from the drop down",
					"User successfully choose the currency from the drop down", "pass");
		} else {

			writeTestResults("Verify user can choose the currency from the drop down",
					"User should be able to choose the currency from the drop down",
					"User could'nt choose the currency from the drop down", "fail");
		}

		click(btn_glLookup);
		getInvObj().handeledSendKeys(txt_GL, glInvenstInShares);
		pressEnter(txt_GL);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", glInvenstInShares), 30);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", glInvenstInShares));
		Thread.sleep(2000);

		sendKeys(txt_amountEmployeeRecipt, twoThousandFiveHundred);

		Thread.sleep(2000);
		String ckeckoutBefore = getText(btn_documentDataIPA);

		click(btn_checkout);
		Thread.sleep(3000);
		String ckeckoutAfter = getText(btn_documentDataIPA);
		if (!ckeckoutBefore.equals(ckeckoutAfter)) {
			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User successfully checkout Inbound Payment Advice", "pass");
		} else {

			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User doesn't checkout Inbound Payment Advice", "fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 40);
		if (isDisplayed(btn_reelese)) {

			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice",
					"User sucessfully draft Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice", "User could'nt draft Inboumd Payment Advice",
					"fail");
		}

		Thread.sleep(3000);
		click(btn_reelese);
		explicitWait(header_releasedInboundPaymentAdvice, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedInboundPaymentAdvice)) {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User sucessfully release Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User could'nt release Inboumd Payment Advice", "fail");
		}
	}

	public void inboundPayment_FIN_BD_25() throws Exception {
		click(btn_actionCRV);

		click(btn_converToInboundPaymentCRV);
		click(btn_detailTabCRV);

		click(btn_summaryTab);

		selectText(drop_paidCurrencyCRV, currencyLKR);

		selectText(drop_paymentMethodCRV, payMethodCash);

		selectText(drop_cashAccount, cashAccount);

		click(btn_detailTabCRV);

		click(btn_detailTabCRV);
		selectText(drop_filterCurrencyCRV, currencyLKR);

		click(chk_firstAdviceOnGridInboundPayment);

		click(btn_checkout);
		Thread.sleep(4000);
		String bannerTotalCRV = getText(btn_documentDataCRV);

		if (!bannerTotalCRV.equals("0")) {
			writeTestResults("Verify user can checkout Cash Employee Receipt",
					"User should be able to checkout the Cash Employee Receipt",
					"User successfully checkout Cash Employee Receipt", "pass");
		} else {

			writeTestResults("Verify user can checkout Cash Employee Receipt",
					"User should be able to checkout the Cash Employee Receipt",
					"User doesn't checkout Cash Employee Receipt", "fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 30);
		if (isDisplayed(btn_reelese)) {
			writeTestResults("Verify user can draft Cash Employee Receipt",
					"User should be able to draft Cash Employee Receipt",
					"User sucessfully draft Cash Employee Receipt", "pass");
		} else {
			writeTestResults("Verify user can draft Cash Employee Receipt",
					"User should be able to draft Cash Employee Receipt", "User could'nt draft Cash Employee Receipt",
					"fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedInboundPayment, 30);
		trackCode = getText(lbl_docNo);
		employeeRecipt = trackCode;
		getFinObj().writeFinanceData("EmployeeRecipt_FIN_BD_025", employeeRecipt, 16);

		if (isDisplayed(header_releasedInboundPayment)) {
			writeTestResults("Verify user can release Cash Employee Receipt",
					"User should be able to release Cash Employee Receipt",
					"User sucessfully release Cash Employee Receipt", "pass");
		} else {
			writeTestResults("Verify user can release Cash Employee Receipt",
					"User should be able to release Cash Employee Receipt",
					"User could'nt release Cash Employee Receipt", "fail");
		}
	}

	public void bankDeposit_FIN_BD_25() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_bankDeposit, 30);
		if (isDisplayed(btn_bankDeposit)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}

		click(btn_bankDeposit);
		explicitWait(header_bankDeposit, 30);
		if (isDisplayed(header_bankDeposit)) {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page",
					"User successfully navigate to Bank Deposit by page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page", "User doesn't navigate to Bank Deposit by page",
					"fail");
		}

		click(btn_newBankDeposit);
		explicitWait(header_newBankDeposit, 30);
		if (isDisplayed(header_newBankDeposit)) {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page successfully displayed",
					"pass");
		} else {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page doesn't displayed", "fail");
		}

		selectText(drop_payMethodBankDeposi, payMethodCash);
		String selectedComboValue = getSelectedOptionInDropdown(drop_payMethodBankDeposi);
		if (selectedComboValue.equals(payMethodCash)) {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User successfully select any Pay Method from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User doesn't select any Pay Method from the drop down where the field will populate the selected value",
					"fail");
		}

		sendKeys(txt_descriptinArea, description);
		String descriptionStr = getAttribute(txt_descriptinArea, "value");
		if (descriptionStr.equals(description)) {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User successfully enter characters, numbers, alphanumeric and special characters to Description",
					"pass");
		} else {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User doesn't enter characters, numbers, alphanumeric and special characters to Description",
					"fail");
		}

		selectText(drop_bankNameBD, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bankNameBD);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_accountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_accountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_viewBankDeposit);
		explicitWait(div_paymentDocumentListBankDepositDocReplace.replace("doc_no", employeeRecipt), 30);
		if (isDisplayed(div_paymentDocumentListBankDepositDocReplace.replace("doc_no", employeeRecipt))) {
			writeTestResults("Verify Employee Receipt get loaded in the document list",
					"Employee Receipt should get loaded in the document list",
					"Employee Receipt successfully get loaded in the document list", "pass");
		} else {
			writeTestResults("Verify Employee Receipt get loaded in the document list",
					"Employee Receipt should get loaded in the document list",
					"Employee Receipt doesn't get loaded in the document list", "fail");
		}

		click(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no", employeeRecipt));

		if (isSelected(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no", employeeRecipt))) {
			writeTestResults("Verify user able to select the Employee Receipt to deposit",
					"User should be able to select the Employee Receipt to deposit",
					"User successfully select the Employee Receipt to deposit", "pass");
		} else {
			writeTestResults("Verify user able to select the Employee Receipt to deposit",
					"User should be able to select the Employee Receipt to deposit",
					"User doesn't select the Employee Receipt to deposit", "fail");
		}

		String depositAmount = getAttribute(txt_depositAmount, "value");
		if (depositAmount.equals("2500.000000")) {
			writeTestResults("Verify that Employee Receipt Amount (2500) get added to Deposit Amount",
					"Employee Receipt Amount (2500) should get added to Deposit Amount",
					"Employee Receipt Amount (2500) successfully get added to Deposit Amount", "pass");
		} else {
			writeTestResults("Verify that Employee Receipt Amount (2500) get added to Deposit Amount",
					"Employee Receipt Amount (2500) should get added to Deposit Amount",
					"Employee Receipt Amount (2500) doesn't get added to Deposit Amount", "fail");
		}
	}

	/* FIN_BD_26_27 */
	public void bankDeposit_FIN_BD_26_27() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 20);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 20);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_bankDeposit, 30);
		if (isDisplayed(btn_bankDeposit)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}

		click(btn_bankDeposit);
		explicitWait(header_bankDeposit, 30);
		if (isDisplayed(header_bankDeposit)) {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page",
					"User successfully navigate to Bank Deposit by page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page", "User doesn't navigate to Bank Deposit by page",
					"fail");
		}

		click(btn_newBankDeposit);
		explicitWait(header_newBankDeposit, 30);
		if (isDisplayed(header_newBankDeposit)) {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page successfully displayed",
					"pass");
		} else {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page doesn't displayed", "fail");
		}

		selectText(drop_payMethodBankDeposi, payMethodCash);
		String selectedComboValue = getSelectedOptionInDropdown(drop_payMethodBankDeposi);
		if (selectedComboValue.equals(payMethodCash)) {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User successfully select any Pay Method from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User doesn't select any Pay Method from the drop down where the field will populate the selected value",
					"fail");
		}

		sendKeys(txt_descriptinArea, description);
		String descriptionStr = getAttribute(txt_descriptinArea, "value");
		if (descriptionStr.equals(description)) {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User successfully enter characters, numbers, alphanumeric and special characters to Description",
					"pass");
		} else {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User doesn't enter characters, numbers, alphanumeric and special characters to Description",
					"fail");
		}

		selectText(drop_bankNameBD, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bankNameBD);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_accountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_accountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_viewBankDeposit);

		explicitWait(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerRecipt_FIN_BD_22")), 30);
		if (isDisplayed(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerRecipt_FIN_BD_22")))) {
			writeTestResults("Verify Customer Receipt get loaded in the document list",
					"Customer Receipt should get loaded in the document list",
					"Customer Receipt successfully get loaded in the document list", "pass");
		} else {
			writeTestResults("Verify Customer Receipt get loaded in the document list",
					"Customer Receipt should get loaded in the document list",
					"Customer Receipt doesn't get loaded in the document list", "fail");
		}

		explicitWait(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("VendorRecipt_FIN_BD_023")), 5);
		if (isDisplayed(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("VendorRecipt_FIN_BD_023")))) {
			writeTestResults("Verify Vendor Receipt get loaded in the document list",
					"Vendor Receipt should get loaded in the document list",
					"Vendor Receipt successfully get loaded in the document list", "pass");
		} else {
			writeTestResults("Verify Vendor Receipt get loaded in the document list",
					"Vendor Receipt should get loaded in the document list",
					"Vendor Receipt doesn't get loaded in the document list", "fail");
		}

		explicitWait(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("GlRecipt_FIN_BD_024")), 5);
		if (isDisplayed(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("GlRecipt_FIN_BD_024")))) {
			writeTestResults("Verify GL Recipt get loaded in the document list",
					"GL Recipt should get loaded in the document list",
					"GL Recipt successfully get loaded in the document list", "pass");
		} else {
			writeTestResults("Verify GL Recipt get loaded in the document list",
					"GL Recipt should get loaded in the document list",
					"GL Recipt doesn't get loaded in the document list", "fail");
		}

		explicitWait(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("EmployeeRecipt_FIN_BD_025")), 5);
		if (isDisplayed(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("EmployeeRecipt_FIN_BD_025")))) {
			writeTestResults("Verify Employee Receipt get loaded in the document list",
					"Employee Receipt should get loaded in the document list",
					"Employee Receipt successfully get loaded in the document list", "pass");
		} else {
			writeTestResults("Verify Employee Receipt get loaded in the document list",
					"Employee Receipt should get loaded in the document list",
					"Employee Receipt doesn't get loaded in the document list", "fail");
		}

		click(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerRecipt_FIN_BD_22")));
		click(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("VendorRecipt_FIN_BD_023")));
		click(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("GlRecipt_FIN_BD_024")));
		click(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("EmployeeRecipt_FIN_BD_025")));

		String depositAmount = getAttribute(txt_depositAmount, "value");
		if (depositAmount.equals("62500.000000")) {
			writeTestResults(
					"Verify Deposit Amount is display with the sum of all payment selected for deposit (2000 + 50000 + 8000+ 2500 = 62500)",
					"Deposit Amount should be sum of all payment selected for deposit (2000 + 50000 + 8000+ 2500 = 62500)",
					"Deposit Amount display with the sum of all payment selected for deposit (2000 + 50000 + 8000+ 2500 = 62500)",
					"pass");
		} else {
			writeTestResults(
					"Verify Deposit Amount is display with the sum of all payment selected for deposit (2000 + 50000 + 8000+ 2500 = 62500)",
					"Deposit Amount should be sum of all payment selected for deposit (2000 + 50000 + 8000+ 2500 = 62500)",
					"Deposit Amount doesn't display with the sum of all payment selected for deposit (2000 + 50000 + 8000+ 2500 = 62500)",
					"fail");

		}

		click(btn_draft);
		explicitWait(header_draftedBankDeposit, 30);
		if (isDisplayed(header_draftedBankDeposit)) {
			writeTestResults("Verify user can draft the Bank Deposit", "User should be able to draft the Bank Deposit",
					"User successfully draft the Bank Deposit", "pass");
		} else {
			writeTestResults("Verify user can draft the Bank Deposit", "User should be able to draft the Bank Deposit",
					"User doesn't draft the Bank Deposit", "fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedBankDeposit, 30);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedBankDeposit)) {
			writeTestResults("Verify user can release the Bank Deposit",
					"User should be able to release the Bank Deposit", "User successfully release the Bank Deposit",
					"pass");
		} else {
			writeTestResults("Verify user can release the Bank Deposit",
					"User should be able to release the Bank Deposit", "User doesn't release the Bank Deposit", "fail");
		}

		click(btn_action);
		explicitWait(btn_journel, 10);
		click(btn_journel);
		explicitWait(header_journelEntry, 10);
		if (isDisplayed(header_journelEntry)) {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded",
					"Journal Entry details pop up successfully get loaded", "pass");
		} else {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded", "Journal Entry details pop up doesn't get loaded",
					"fail");
		}

		String debitBalance = getText(lbl_debitJournelEntryBankDepositCash);

		if (debitBalance.equals("62500.000000")) {
			writeTestResults("Verify 'Deposit Amount' value is debited to the selected Bank Account",
					"'Deposit Amount' value should get debited to the selected Bank Account",
					"'Deposit Amount' value successfully get debited to the selected Bank Account", "pass");
		} else {
			writeTestResults("Verify 'Deposit Amount' value is debited to the selected Bank Account",
					"'Deposit Amount' value should get debited to the selected Bank Account",
					"'Deposit Amount' value doesn't get debited to the selected Bank Account", "fail");
		}

		String creditBalance = getText(lbl_creditJournelEntryBankDepositCash);

		if (creditBalance.equals("62500.000000")) {
			writeTestResults("Verify 'Deposit Amount' value is credited to the Cash in Hand Account",
					"'Deposit Amount' value should get credited to the Cash in Hand Account",
					"'Deposit Amount' value successfully get credited to the Cash in Hand Account", "pass");
		} else {
			writeTestResults("Verify 'Deposit Amount' value is credited to the Cash in Hand Account",
					"'Deposit Amount' value should get credited to the Cash in Hand Account",
					"'Deposit Amount' value doesn't get credited to the Cash in Hand Account", "fail");
		}

		String debTotal = getText(lbl_debitTotalJournelEntry);
		String credTotal = getText(lbl_creditTotalJournelEntry);

		if (debTotal.equals("62500.000000") && debTotal.equals(credTotal)) {
			writeTestResults("Verify total debit and credit amounts were balanced",
					"Total debit and credit amounts should balanced",
					"Total debit and credit amounts successfully balanced", "pass");
		} else {
			writeTestResults("Verify total debit and credit amounts were balanced",
					"Total debit and credit amounts should balanced", "Total debit and credit amounts weren't balanced",
					"fail");
		}
	}

	/* FIN_BD_28 */
	public void inboundPaymentAdvice_FIN_BD_28() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_inboundPaymentAdvice, 30);
		if (isDisplayed(btn_inboundPaymentAdvice)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}
		click(btn_inboundPaymentAdvice);
		explicitWait(btn_newInboundPaymentAdvice, 30);
		if (isDisplayed(btn_newInboundPaymentAdvice)) {

			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User successfully navigate to Inbound payment advice by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User could'nt navigate to Inbound payment advice by-page", "fail");
		}

		click(btn_newInboundPaymentAdvice);
		if (isDisplayed(btn_CusAdvanceJourneyIPA)) {

			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User successfully view the list of journey",
					"pass");
		} else {
			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User could'nt view the list of journey",
					"fail");
		}

		click(btn_CusAdvanceJourneyIPA);
		explicitWait(lnl_postBusinessUnitIPA, 30);
		if (isDisplayed(lnl_postBusinessUnitIPA)) {

			writeTestResults("Verify user able to navigate to Customer Advance/ Customer Debit Memo",
					"User should be able to navigate to Customer Advance/ Customer Debit Memo",
					"User successfully navigate to Customer Advance/ Customer Debit Memo", "pass");
		} else {
			writeTestResults("Verify user able to navigate to Customer Advance/ Customer Debit Memo",
					"User should be able to navigate to Customer Advance/ Customer Debit Memo",
					"User doesn't navigate to Customer Advance/ Customer Debit Memo", "fail");
		}

		Thread.sleep(1000);
		click(lnl_postBusinessUnitIPA);
		Thread.sleep(700);
		selectText(drop_postBusinessUnit, postBusinessUnitSch);
		Thread.sleep(700);
		click(tab_subbaryIPA);
		Thread.sleep(2000);
		String get_post_business_unit = getText(lnl_postBusinessUnitIPA);
		System.out.println(get_post_business_unit);
		if (get_post_business_unit.equals(postBusinessUnitSch)) {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User successfully select post business unit from the drop down", "pass");
		} else {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User could'nt select post business unit from the drop down", "fail");
		}

		click(btn_searchCustomerIPA);
		if (isDisplayed(txt_cusAccountSearchIPA)) {
			writeTestResults("Verify user can click on Search button in Customer and Customer Lookup should pop up",
					"User should be click on Search button in Customer and Customer Lookup should pop up",
					"User successfully click on Search button in Customer and Customer Lookup should pop up", "pass");
		} else {

			writeTestResults("Verify user can click on Search button in Customer and Customer Lookup should pop up",
					"User should be click on Search button in Customer and Customer Lookup should pop up",
					"User could'nt click on Search button in Customer and Customer Lookup should pop up", "fail");
		}

		explicitWait(txt_cusAccountSearchIPA, 30);
		sendKeys(txt_cusAccountSearchIPA, customerAccount);
		pressEnter(txt_cusAccountSearchIPA);
		Thread.sleep(1500);
		explicitWait(lnk_resultCustomerAccountIPA, 30);
		if (isDisplayed(lnk_resultCustomerAccountIPA)) {
			writeTestResults("Verify user can search an existing customer by entering Customer name/code",
					"User should be able to search an existing customer by entering Customer name/code",
					"User successfully search an existing customer by entering Customer name/code", "pass");
		} else {

			writeTestResults("Verify user can search an existing customer by entering Customer name/code",
					"User should be able to search an existing customer by entering Customer name/code",
					"User could'nt search an existing customer by entering Customer name/code", "fail");
		}

		doubleClick(lnk_resultCustomerAccountIPA);

		sendKeys(txt_refNoIPA, testAllCharcterWordIPA);

		sendKeys(txt_descIPA, testAllCharcterWordIPA);
		Thread.sleep(2000);

		Select comboBox = new Select(driver.findElement(By.xpath(drop_cusrencyIPA)));
		selectText(drop_cusrencyIPA, currencyLKR);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		if (selectedComboValue.equals(currencyLKR)) {
			writeTestResults("Verify user can choose the currency from the drop down",
					"User should be able to choose the currency from the drop down",
					"User successfully choose the currency from the drop down", "pass");
		} else {

			writeTestResults("Verify user can choose the currency from the drop down",
					"User should be able to choose the currency from the drop down",
					"User could'nt choose the currency from the drop down", "fail");
		}

		sendKeys(txt_amountIPA, fiveThousandEightHundred);
		Thread.sleep(2000);
		String ckeckoutBefore = getText(btn_documentDataIPA);

		click(btn_checkout);
		Thread.sleep(3000);
		String ckeckoutAfter = getText(btn_documentDataIPA);
		if (!ckeckoutBefore.equals(ckeckoutAfter)) {
			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User successfully checkout Inbound Payment Advice", "pass");
		} else {

			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User doesn't checkout Inbound Payment Advice", "fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 40);
		if (isDisplayed(btn_reelese)) {

			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice",
					"User sucessfully draft Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice", "User could'nt draft Inboumd Payment Advice",
					"fail");
		}

		Thread.sleep(3000);
		click(btn_reelese);
		explicitWait(header_releasedInboundPaymentAdvice, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedInboundPaymentAdvice)) {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User sucessfully release Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User could'nt release Inboumd Payment Advice", "fail");
		}
	}

	public void inboundPayment_FIN_BD_28() throws Exception {
		click(btn_actionCRV);

		click(btn_converToInboundPaymentCRV);
		click(btn_detailTabCRV);

		click(btn_summaryTab);

		selectText(drop_paidCurrencyCRV, currencyLKR);

		selectText(drop_paymentMethodCRV, payMethodCheque);

		selectText(drop_bankNameInboundPayment, bank);

		sendKeys(txt_checkNoInboundPayment, getInvObj().currentTimeAndDate().replace("/", "").replace(":", ""));

		click(btn_detailTabCRV);

		click(btn_detailTabCRV);
		selectText(drop_filterCurrencyCRV, currencyLKR);

		click(chk_firstAdviceOnGridInboundPayment);

		click(btn_checkout);
		Thread.sleep(4000);
		String bannerTotalCRV = getText(btn_documentDataCRV);

		if (!bannerTotalCRV.equals("0")) {
			writeTestResults("Verify user can checkout Cheque Customer Receipt",
					"User should be able to checkout the Cheque Customer Receipt",
					"User successfully checkout Cheque Customer Receipt", "pass");
		} else {

			writeTestResults("Verify user can checkout Cheque Customer Receipt",
					"User should be able to checkout the Cheque Customer Receipt",
					"User doesn't checkout Cheque Customer Receipt", "fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 30);
		if (isDisplayed(btn_reelese)) {
			writeTestResults("Verify user can draft Cheque Customer Receipt",
					"User should be able to draft Cheque Customer Receipt",
					"User sucessfully draft Cheque Customer Receipt", "pass");
		} else {
			writeTestResults("Verify user can draft Cheque Customer Receipt",
					"User should be able to draft Cheque Customer Receipt",
					"User could'nt draft Cheque Customer Receipt", "fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedInboundPayment, 30);
		trackCode = getText(lbl_docNo);
		getFinObj().writeFinanceData("CustomerReciptCheque_FIN_BD_28", trackCode, 17);
		if (isDisplayed(header_releasedInboundPayment)) {
			writeTestResults("Verify user can release Cheque Customer Receipt",
					"User should be able to release Cheque Customer Receipt",
					"User sucessfully release Cheque Customer Receipt", "pass");
		} else {
			writeTestResults("Verify user can release Cheque Customer Receipt",
					"User should be able to release Cheque Customer Receipt",
					"User could'nt release Cheque Customer Receipt", "fail");
		}
	}

	public void bankDeposit_FIN_BD_28() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_bankDeposit, 30);
		if (isDisplayed(btn_bankDeposit)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}

		click(btn_bankDeposit);
		explicitWait(header_bankDeposit, 30);
		if (isDisplayed(header_bankDeposit)) {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page",
					"User successfully navigate to Bank Deposit by page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page", "User doesn't navigate to Bank Deposit by page",
					"fail");
		}

		click(btn_newBankDeposit);
		explicitWait(header_newBankDeposit, 30);
		if (isDisplayed(header_newBankDeposit)) {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page successfully displayed",
					"pass");
		} else {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page doesn't displayed", "fail");
		}

		selectText(drop_payMethodBankDeposi, payMethodCheque);
		String selectedComboValue = getSelectedOptionInDropdown(drop_payMethodBankDeposi);
		if (selectedComboValue.equals(payMethodCheque)) {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User successfully select any Pay Method from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User doesn't select any Pay Method from the drop down where the field will populate the selected value",
					"fail");
		}

		sendKeys(txt_descriptinArea, description);
		String descriptionStr = getAttribute(txt_descriptinArea, "value");
		if (descriptionStr.equals(description)) {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User successfully enter characters, numbers, alphanumeric and special characters to Description",
					"pass");
		} else {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User doesn't enter characters, numbers, alphanumeric and special characters to Description",
					"fail");
		}

		selectText(drop_bankNameBD, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bankNameBD);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_accountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_accountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_viewBankDeposit);
		explicitWait(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerReciptCheque_FIN_BD_28")), 30);
		if (isDisplayed(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerReciptCheque_FIN_BD_28")))) {
			writeTestResults("Verify Customer Receipt get loaded in the document list",
					"Customer Receipt should get loaded in the document list",
					"Customer Receipt successfully get loaded in the document list", "pass");
		} else {
			writeTestResults("Verify Customer Receipt get loaded in the document list",
					"Customer Receipt should get loaded in the document list",
					"Customer Receipt doesn't get loaded in the document list", "fail");
		}

		click(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerReciptCheque_FIN_BD_28")));

		if (isSelected(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerReciptCheque_FIN_BD_28")))) {
			writeTestResults("Verify user able to select the Customer Receipt to deposit",
					"User should be able to select the Customer Receipt to deposit",
					"User successfully select the Customer Receipt to deposit", "pass");
		} else {
			writeTestResults("Verify user able to select the Customer Receipt to deposit",
					"User should be able to select the Customer Receipt to deposit",
					"User doesn't select the Customer Receipt to deposit", "fail");
		}

		String depositAmount = getAttribute(txt_depositAmount, "value");
		if (depositAmount.equals("5800.000000")) {
			writeTestResults("Verify that Customer Receipt Amount (5800) get added to Deposit Amount",
					"Customer Receipt Amount (5800) should get added to Deposit Amount",
					"Customer Receipt Amount (5800) successfully get added to Deposit Amount", "pass");
		} else {
			writeTestResults("Verify that Customer Receipt Amount (5800) get added to Deposit Amount",
					"Customer Receipt Amount (5800) should get added to Deposit Amount",
					"Customer Receipt Amount (5800) doesn't get added to Deposit Amount", "fail");
		}
	}

	/* FIN_BD_29 */
	public void inboundPaymentAdvice_FIN_BD_29() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_inboundPaymentAdvice, 30);
		if (isDisplayed(btn_inboundPaymentAdvice)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}
		click(btn_inboundPaymentAdvice);
		explicitWait(btn_newInboundPaymentAdvice, 30);
		if (isDisplayed(btn_newInboundPaymentAdvice)) {

			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User successfully navigate to Inbound payment advice by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User could'nt navigate to Inbound payment advice by-page", "fail");
		}

		click(btn_newInboundPaymentAdvice);
		if (isDisplayed(btn_CusAdvanceJourneyIPA)) {

			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User successfully view the list of journey",
					"pass");
		} else {
			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User could'nt view the list of journey",
					"fail");
		}

		click(btn_journeyVendorRefund);
		explicitWait(lnl_postBusinessUnitIPA, 30);
		if (isDisplayed(lnl_postBusinessUnitIPA)) {
			writeTestResults("Verify user can navigate to Vendor Refund",
					"User should be able to navigate to Vendor Refund", "User successfully navigate to Vendor Refund",
					"pass");
		} else {
			writeTestResults("Verify user can navigate to Vendor Refund",
					"User should be able to navigate to Vendor Refund", "User doesn't navigate to Vendor Refund",
					"fail");
		}

		Thread.sleep(1000);
		click(lnl_postBusinessUnitIPA);
		Thread.sleep(700);
		selectText(drop_postBusinessUnit, postBusinessUnitSch);
		Thread.sleep(700);
		click(tab_subbaryIPA);
		Thread.sleep(2000);
		String get_post_business_unit = getText(lnl_postBusinessUnitIPA);
		System.out.println(get_post_business_unit);
		if (get_post_business_unit.equals(postBusinessUnitSch)) {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User successfully select post business unit from the drop down", "pass");
		} else {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User could'nt select post business unit from the drop down", "fail");
		}

		click(btn_vendorLoockup);
		explicitWait(txt_vendorSearch, 30);
		if (isDisplayed(txt_vendorSearch)) {
			writeTestResults("Verify user can click on Search button in Vendor and Vendor Lookup should pop up",
					"User should be click on Search button in Vendor and Vendor Lookup should pop up",
					"User successfully click on Search button in Vendor and Vendor Lookup should pop up", "pass");
		} else {

			writeTestResults("Verify user can click on Search button in Vendor and Vendor Lookup should pop up",
					"User should be click on Search button in Vendor and Vendor Lookup should pop up",
					"User could'nt click on Search button in Vendor and Vendor Lookup should pop up", "fail");
		}

		sendKeys(txt_vendorSearch, vendorAccount);
		pressEnter(txt_vendorSearch);
		Thread.sleep(1500);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", vendorAccount), 30);
		if (isDisplayed(lnk_inforOnLookupInformationReplace.replace("info", vendorAccount))) {
			writeTestResults("Verify user can search an existing vendor by entering Vendor name/code",
					"User should be able to search an existing vendor by entering Vendor name/code",
					"User successfully search an existing vendor by entering Vendor name/code", "pass");
		} else {

			writeTestResults("Verify user can search an existing vendor by entering Vendor name/code",
					"User should be able to search an existing vendor by entering Vendor name/code",
					"User could'nt search an existing vendor by entering Vendor name/code", "fail");
		}

		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", vendorAccount));

		sendKeys(txt_refNoIPA, testAllCharcterWordIPA);

		sendKeys(txt_descIPA, testAllCharcterWordIPA);
		Thread.sleep(2000);

		Select comboBox = new Select(driver.findElement(By.xpath(drop_cusrencyIPA)));
		selectText(drop_cusrencyIPA, currencyLKR);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		if (selectedComboValue.equals(currencyLKR)) {
			writeTestResults("Verify user can choose the currency from the drop down",
					"User should be able to choose the currency from the drop down",
					"User successfully choose the currency from the drop down", "pass");
		} else {

			writeTestResults("Verify user can choose the currency from the drop down",
					"User should be able to choose the currency from the drop down",
					"User could'nt choose the currency from the drop down", "fail");
		}

		click(btn_glLookupVendorRefund);
		getInvObj().handeledSendKeys(txt_GL, glAccountVendorRefund);
		pressEnter(txt_GL);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", glAccountVendorRefund), 30);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", glAccountVendorRefund));
		Thread.sleep(2000);

		sendKeys(txt_amountVendorRefund, twoThousandTwoHundred);

		Thread.sleep(2000);
		String ckeckoutBefore = getText(btn_documentDataIPA);

		click(btn_checkout);
		Thread.sleep(3000);
		String ckeckoutAfter = getText(btn_documentDataIPA);
		if (!ckeckoutBefore.equals(ckeckoutAfter)) {
			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User successfully checkout Inbound Payment Advice", "pass");
		} else {

			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User doesn't checkout Inbound Payment Advice", "fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 40);
		if (isDisplayed(btn_reelese)) {

			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice",
					"User sucessfully draft Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice", "User could'nt draft Inboumd Payment Advice",
					"fail");
		}

		Thread.sleep(3000);
		click(btn_reelese);
		explicitWait(header_releasedInboundPaymentAdvice, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedInboundPaymentAdvice)) {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User sucessfully release Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User could'nt release Inboumd Payment Advice", "fail");
		}
	}

	public void inboundPayment_FIN_BD_29() throws Exception {
		click(btn_actionCRV);

		click(btn_converToInboundPaymentCRV);
		click(btn_detailTabCRV);

		click(btn_summaryTab);

		selectText(drop_paidCurrencyCRV, currencyLKR);

		selectText(drop_paymentMethodCRV, payMethodCheque);

		selectText(drop_bankNameInboundPayment, bank);

		sendKeys(txt_checkNoInboundPayment, getInvObj().currentTimeAndDate().replace("/", "").replace(":", ""));

		click(btn_detailTabCRV);

		click(btn_detailTabCRV);
		selectText(drop_filterCurrencyCRV, currencyLKR);

		click(chk_firstAdviceOnGridInboundPayment);

		click(btn_checkout);
		Thread.sleep(4000);
		String bannerTotalCRV = getText(btn_documentDataCRV);

		if (!bannerTotalCRV.equals("0")) {
			writeTestResults("Verify user can checkout Cheque Vendor Receipt",
					"User should be able to checkout the Cheque Vendor Receipt",
					"User successfully checkout Cheque Vendor Receipt", "pass");
		} else {

			writeTestResults("Verify user can checkout Cheque Vendor Receipt",
					"User should be able to checkout the Cheque Vendor Receipt",
					"User doesn't checkout Cheque Vendor Receipt", "fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 30);
		if (isDisplayed(btn_reelese)) {
			writeTestResults("Verify user can draft Cheque Vendor Receipt",
					"User should be able to draft Cheque Vendor Receipt",
					"User sucessfully draft Cheque Vendor Receipt", "pass");
		} else {
			writeTestResults("Verify user can draft Cheque Vendor Receipt",
					"User should be able to draft Cheque Vendor Receipt", "User could'nt draft Cheque Vendor Receipt",
					"fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedInboundPayment, 30);
		trackCode = getText(lbl_docNo);
		getFinObj().writeFinanceData("VendorReciptCheque_FIN_BD_29", trackCode, 18);
		if (isDisplayed(header_releasedInboundPayment)) {
			writeTestResults("Verify user can release Cheque Vendor Receipt",
					"User should be able to release Cheque Vendor Receipt",
					"User sucessfully release Cheque Vendor Receipt", "pass");
		} else {
			writeTestResults("Verify user can release Cheque Vendor Receipt",
					"User should be able to release Cheque Vendor Receipt",
					"User could'nt release Cheque Vendor Receipt", "fail");
		}
	}

	public void bankDeposit_FIN_BD_29() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_bankDeposit, 30);
		if (isDisplayed(btn_bankDeposit)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}

		click(btn_bankDeposit);
		explicitWait(header_bankDeposit, 30);
		if (isDisplayed(header_bankDeposit)) {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page",
					"User successfully navigate to Bank Deposit by page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page", "User doesn't navigate to Bank Deposit by page",
					"fail");
		}

		click(btn_newBankDeposit);
		explicitWait(header_newBankDeposit, 30);
		if (isDisplayed(header_newBankDeposit)) {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page successfully displayed",
					"pass");
		} else {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page doesn't displayed", "fail");
		}

		selectText(drop_payMethodBankDeposi, payMethodCheque);
		String selectedComboValue = getSelectedOptionInDropdown(drop_payMethodBankDeposi);
		if (selectedComboValue.equals(payMethodCheque)) {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User successfully select any Pay Method from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User doesn't select any Pay Method from the drop down where the field will populate the selected value",
					"fail");
		}

		sendKeys(txt_descriptinArea, description);
		String descriptionStr = getAttribute(txt_descriptinArea, "value");
		if (descriptionStr.equals(description)) {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User successfully enter characters, numbers, alphanumeric and special characters to Description",
					"pass");
		} else {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User doesn't enter characters, numbers, alphanumeric and special characters to Description",
					"fail");
		}

		selectText(drop_bankNameBD, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bankNameBD);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_accountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_accountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_viewBankDeposit);
		explicitWait(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("VendorReciptCheque_FIN_BD_29")), 30);
		if (isDisplayed(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("VendorReciptCheque_FIN_BD_29")))) {
			writeTestResults("Verify Vendor Receipt get loaded in the document list",
					"Vendor Receipt should get loaded in the document list",
					"Vendor Receipt successfully get loaded in the document list", "pass");
		} else {
			writeTestResults("Verify Vendor Receipt get loaded in the document list",
					"Vendor Receipt should get loaded in the document list",
					"Vendor Receipt doesn't get loaded in the document list", "fail");
		}

		click(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("VendorReciptCheque_FIN_BD_29")));

		if (isSelected(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("VendorReciptCheque_FIN_BD_29")))) {
			writeTestResults("Verify user able to select the Vendor Receipt to deposit",
					"User should be able to select the Vendor Receipt to deposit",
					"User successfully select the Vendor Receipt to deposit", "pass");
		} else {
			writeTestResults("Verify user able to select the Vendor Receipt to deposit",
					"User should be able to select the Vendor Receipt to deposit",
					"User doesn't select the Vendor Receipt to deposit", "fail");
		}

		String depositAmount = getAttribute(txt_depositAmount, "value");
		if (depositAmount.equals("2200.000000")) {
			writeTestResults("Verify that Vendor Receipt Amount (2200) get added to Deposit Amount",
					"Vendor Receipt Amount (2200) should get added to Deposit Amount",
					"Vendor Receipt Amount (2200) successfully get added to Deposit Amount", "pass");
		} else {
			writeTestResults("Verify that Vendor Receipt Amount (2200) get added to Deposit Amount",
					"Vendor Receipt Amount (2200) should get added to Deposit Amount",
					"Vendor Receipt Amount (2200) doesn't get added to Deposit Amount", "fail");
		}
	}

	/* FIN_BD_30 */
	public void inboundPaymentAdvice_FIN_BD_30() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_inboundPaymentAdvice, 30);
		if (isDisplayed(btn_inboundPaymentAdvice)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}
		click(btn_inboundPaymentAdvice);
		explicitWait(btn_newInboundPaymentAdvice, 30);
		if (isDisplayed(btn_newInboundPaymentAdvice)) {

			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User successfully navigate to Inbound payment advice by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User could'nt navigate to Inbound payment advice by-page", "fail");
		}

		click(btn_newInboundPaymentAdvice);
		if (isDisplayed(btn_CusAdvanceJourneyIPA)) {

			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User successfully view the list of journey",
					"pass");
		} else {
			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User could'nt view the list of journey",
					"fail");
		}

		click(btn_journeyReciptVoucher);
		explicitWait(lnl_postBusinessUnitIPA, 30);
		if (isDisplayed(lnl_postBusinessUnitIPA)) {
			writeTestResults("Verify user can navigate to Recipt Voucher",
					"User should be able to navigate to Recipt Voucher", "User successfully navigate to Recipt Voucher",
					"pass");
		} else {
			writeTestResults("Verify user can navigate to Recipt Voucher",
					"User should be able to navigate to Recipt Voucher", "User doesn't navigate to Recipt Voucher",
					"fail");
		}

		Thread.sleep(1000);
		click(lnl_postBusinessUnitIPA);
		Thread.sleep(700);
		selectText(drop_postBusinessUnit, postBusinessUnitSch);
		Thread.sleep(700);
		click(tab_subbaryIPA);
		Thread.sleep(2000);
		String get_post_business_unit = getText(lnl_postBusinessUnitIPA);
		System.out.println(get_post_business_unit);
		if (get_post_business_unit.equals(postBusinessUnitSch)) {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User successfully select post business unit from the drop down", "pass");
		} else {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User could'nt select post business unit from the drop down", "fail");
		}

		sendKeys(txt_descIPA, testAllCharcterWordIPA);
		Thread.sleep(2000);

		Select comboBox = new Select(driver.findElement(By.xpath(drop_cusrencyIPA)));
		selectText(drop_cusrencyIPA, currencyLKR);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		if (selectedComboValue.equals(currencyLKR)) {
			writeTestResults("Verify user can choose the currency from the drop down",
					"User should be able to choose the currency from the drop down",
					"User successfully choose the currency from the drop down", "pass");
		} else {

			writeTestResults("Verify user can choose the currency from the drop down",
					"User should be able to choose the currency from the drop down",
					"User could'nt choose the currency from the drop down", "fail");
		}

		click(btn_glLookup);
		getInvObj().handeledSendKeys(txt_GL, glAccountMotorVehicles);
		pressEnter(txt_GL);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles), 30);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles));
		Thread.sleep(2000);

		sendKeys(txt_amountReciptVoucher, twentyFiveThousand);

		Thread.sleep(2000);
		String ckeckoutBefore = getText(btn_documentDataIPA);

		click(btn_checkout);
		Thread.sleep(3000);
		String ckeckoutAfter = getText(btn_documentDataIPA);
		if (!ckeckoutBefore.equals(ckeckoutAfter)) {
			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User successfully checkout Inbound Payment Advice", "pass");
		} else {

			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User doesn't checkout Inbound Payment Advice", "fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 40);
		if (isDisplayed(btn_reelese)) {

			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice",
					"User sucessfully draft Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice", "User could'nt draft Inboumd Payment Advice",
					"fail");
		}

		Thread.sleep(3000);
		click(btn_reelese);
		explicitWait(header_releasedInboundPaymentAdvice, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedInboundPaymentAdvice)) {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User sucessfully release Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User could'nt release Inboumd Payment Advice", "fail");
		}
	}

	public void inboundPayment_FIN_BD_30() throws Exception {
		click(btn_actionCRV);

		click(btn_converToInboundPaymentCRV);
		click(btn_detailTabCRV);

		click(btn_summaryTab);

		selectText(drop_paidCurrencyCRV, currencyLKR);

		selectText(drop_paymentMethodCRV, payMethodCheque);

		selectText(drop_bankNameInboundPayment, bank);

		sendKeys(txt_checkNoInboundPayment, getInvObj().currentTimeAndDate().replace("/", "").replace(":", ""));

		click(btn_detailTabCRV);

		click(btn_detailTabCRV);
		selectText(drop_filterCurrencyCRV, currencyLKR);

		click(chk_firstAdviceOnGridInboundPayment);

		click(btn_checkout);
		Thread.sleep(4000);
		String bannerTotalCRV = getText(btn_documentDataCRV);

		if (!bannerTotalCRV.equals("0")) {
			writeTestResults("Verify user can checkout Cheque GL Receipt",
					"User should be able to checkout the Cheque GL Receipt",
					"User successfully checkout Cheque GL Receipt", "pass");
		} else {

			writeTestResults("Verify user can checkout Cheque GL Receipt",
					"User should be able to checkout the Cheque GL Receipt", "User doesn't checkout Cheque GL Receipt",
					"fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 30);
		if (isDisplayed(btn_reelese)) {
			writeTestResults("Verify user can draft Cheque GL Receipt",
					"User should be able to draft Cheque GL Receipt", "User sucessfully draft Cheque GL Receipt",
					"pass");
		} else {
			writeTestResults("Verify user can draft Cheque GL Receipt",
					"User should be able to draft Cheque GL Receipt", "User could'nt draft Cheque GL Receipt", "fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedInboundPayment, 30);
		trackCode = getText(lbl_docNo);
		getFinObj().writeFinanceData("GLReciptCheque_FIN_BD_30", trackCode, 19);
		if (isDisplayed(header_releasedInboundPayment)) {
			writeTestResults("Verify user can release Cheque GL Receipt",
					"User should be able to release Cheque GL Receipt", "User sucessfully release Cheque GL Receipt",
					"pass");
		} else {
			writeTestResults("Verify user can release Cheque GL Receipt",
					"User should be able to release Cheque GL Receipt", "User could'nt release Cheque GL Receipt",
					"fail");
		}
	}

	public void bankDeposit_FIN_BD_30() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_bankDeposit, 30);
		if (isDisplayed(btn_bankDeposit)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}

		click(btn_bankDeposit);
		explicitWait(header_bankDeposit, 30);
		if (isDisplayed(header_bankDeposit)) {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page",
					"User successfully navigate to Bank Deposit by page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page", "User doesn't navigate to Bank Deposit by page",
					"fail");
		}

		click(btn_newBankDeposit);
		explicitWait(header_newBankDeposit, 30);
		if (isDisplayed(header_newBankDeposit)) {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page successfully displayed",
					"pass");
		} else {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page doesn't displayed", "fail");
		}

		selectText(drop_payMethodBankDeposi, payMethodCheque);
		String selectedComboValue = getSelectedOptionInDropdown(drop_payMethodBankDeposi);
		if (selectedComboValue.equals(payMethodCheque)) {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User successfully select any Pay Method from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User doesn't select any Pay Method from the drop down where the field will populate the selected value",
					"fail");
		}

		sendKeys(txt_descriptinArea, description);
		String descriptionStr = getAttribute(txt_descriptinArea, "value");
		if (descriptionStr.equals(description)) {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User successfully enter characters, numbers, alphanumeric and special characters to Description",
					"pass");
		} else {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User doesn't enter characters, numbers, alphanumeric and special characters to Description",
					"fail");
		}

		selectText(drop_bankNameBD, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bankNameBD);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_accountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_accountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_viewBankDeposit);
		explicitWait(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("GLReciptCheque_FIN_BD_30")), 30);
		if (isDisplayed(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("GLReciptCheque_FIN_BD_30")))) {
			writeTestResults("Verify GL Receipt get loaded in the document list",
					"GL Receipt should get loaded in the document list",
					"GL Receipt successfully get loaded in the document list", "pass");
		} else {
			writeTestResults("Verify GL Receipt get loaded in the document list",
					"GL Receipt should get loaded in the document list",
					"GL Receipt doesn't get loaded in the document list", "fail");
		}

		click(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("GLReciptCheque_FIN_BD_30")));

		if (isSelected(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("GLReciptCheque_FIN_BD_30")))) {
			writeTestResults("Verify user able to select the GL Receipt to deposit",
					"User should be able to select the GL Receipt to deposit",
					"User successfully select the GL Receipt to deposit", "pass");
		} else {
			writeTestResults("Verify user able to select the GL Receipt to deposit",
					"User should be able to select the GL Receipt to deposit",
					"User doesn't select the GL Receipt to deposit", "fail");
		}

		String depositAmount = getAttribute(txt_depositAmount, "value");
		if (depositAmount.equals("25000.000000")) {
			writeTestResults("Verify that GL Receipt Amount (25000) get added to Deposit Amount",
					"GL Receipt Amount (25000) should get added to Deposit Amount",
					"GL Receipt Amount (25000) successfully get added to Deposit Amount", "pass");
		} else {
			writeTestResults("Verify that GL Receipt Amount (25000) get added to Deposit Amount",
					"GL Receipt Amount (25000) should get added to Deposit Amount",
					"GL Receipt Amount (25000) doesn't get added to Deposit Amount", "fail");
		}
	}

	/* FIN_BD_31 */
	public void inboundPaymentAdvice_FIN_BD_31() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_inboundPaymentAdvice, 30);
		if (isDisplayed(btn_inboundPaymentAdvice)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}
		click(btn_inboundPaymentAdvice);
		explicitWait(btn_newInboundPaymentAdvice, 30);
		if (isDisplayed(btn_newInboundPaymentAdvice)) {

			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User successfully navigate to Inbound payment advice by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User could'nt navigate to Inbound payment advice by-page", "fail");
		}

		click(btn_newInboundPaymentAdvice);
		if (isDisplayed(btn_journeiesDows)) {

			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User successfully view the list of journey",
					"pass");
		} else {
			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User could'nt view the list of journey",
					"fail");
		}

		click(btn_journeiesDows);

		click(btn_journeyEmployeeReceiptAdvice);
		explicitWait(lnl_postBusinessUnitIPA, 30);
		if (isDisplayed(lnl_postBusinessUnitIPA)) {
			writeTestResults("Verify user can navigate to Recipt Voucher",
					"User should be able to navigate to Recipt Voucher", "User successfully navigate to Recipt Voucher",
					"pass");
		} else {
			writeTestResults("Verify user can navigate to Recipt Voucher",
					"User should be able to navigate to Recipt Voucher", "User doesn't navigate to Recipt Voucher",
					"fail");
		}

		Thread.sleep(1000);
		click(lnl_postBusinessUnitIPA);
		Thread.sleep(700);
		selectText(drop_postBusinessUnit, postBusinessUnitSch);
		Thread.sleep(700);
		click(tab_subbaryIPA);
		Thread.sleep(2000);
		String get_post_business_unit = getText(lnl_postBusinessUnitIPA);
		System.out.println(get_post_business_unit);
		if (get_post_business_unit.equals(postBusinessUnitSch)) {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User successfully select post business unit from the drop down", "pass");
		} else {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User could'nt select post business unit from the drop down", "fail");
		}

		click(btn_employeeLookup);
		getInvObj().handeledSendKeys(txt_searchEmployee, employeeMadhushan);
		pressEnter(txt_searchEmployee);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", employeeMadhushan), 50);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", employeeMadhushan));
		Thread.sleep(2000);

		sendKeys(txt_descIPA, testAllCharcterWordIPA);
		Thread.sleep(2000);

		Select comboBox = new Select(driver.findElement(By.xpath(drop_cusrencyIPA)));
		selectText(drop_cusrencyIPA, currencyLKR);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		if (selectedComboValue.equals(currencyLKR)) {
			writeTestResults("Verify user can choose the currency from the drop down",
					"User should be able to choose the currency from the drop down",
					"User successfully choose the currency from the drop down", "pass");
		} else {

			writeTestResults("Verify user can choose the currency from the drop down",
					"User should be able to choose the currency from the drop down",
					"User could'nt choose the currency from the drop down", "fail");
		}

		click(btn_glLookup);
		getInvObj().handeledSendKeys(txt_GL, glInvenstInShares);
		pressEnter(txt_GL);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", glInvenstInShares), 30);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", glInvenstInShares));
		Thread.sleep(2000);

		sendKeys(txt_amountEmployeeRecipt, tenThousand);

		Thread.sleep(2000);
		String ckeckoutBefore = getText(btn_documentDataIPA);

		click(btn_checkout);
		Thread.sleep(3000);
		String ckeckoutAfter = getText(btn_documentDataIPA);
		if (!ckeckoutBefore.equals(ckeckoutAfter)) {
			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User successfully checkout Inbound Payment Advice", "pass");
		} else {

			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User doesn't checkout Inbound Payment Advice", "fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 40);
		if (isDisplayed(btn_reelese)) {

			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice",
					"User sucessfully draft Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice", "User could'nt draft Inboumd Payment Advice",
					"fail");
		}

		Thread.sleep(3000);
		click(btn_reelese);
		explicitWait(header_releasedInboundPaymentAdvice, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedInboundPaymentAdvice)) {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User sucessfully release Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User could'nt release Inboumd Payment Advice", "fail");
		}
	}

	public void inboundPayment_FIN_BD_31() throws Exception {
		click(btn_actionCRV);

		click(btn_converToInboundPaymentCRV);
		click(btn_detailTabCRV);

		click(btn_summaryTab);

		selectText(drop_paidCurrencyCRV, currencyLKR);

		selectText(drop_paymentMethodCRV, payMethodCheque);

		selectText(drop_bankNameInboundPayment, bank);

		sendKeys(txt_checkNoInboundPayment, getInvObj().currentTimeAndDate().replace("/", "").replace(":", ""));

		click(btn_detailTabCRV);

		click(btn_detailTabCRV);
		selectText(drop_filterCurrencyCRV, currencyLKR);

		click(chk_firstAdviceOnGridInboundPayment);

		click(btn_checkout);
		Thread.sleep(4000);
		String bannerTotalCRV = getText(btn_documentDataCRV);

		if (!bannerTotalCRV.equals("0")) {
			writeTestResults("Verify user can checkout Cheque Employee Receipt",
					"User should be able to checkout the Cheque Employee Receipt",
					"User successfully checkout Cheque Employee Receipt", "pass");
		} else {

			writeTestResults("Verify user can checkout Cheque Employee Receipt",
					"User should be able to checkout the Cheque Employee Receipt",
					"User doesn't checkout Cheque Employee Receipt", "fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 30);
		if (isDisplayed(btn_reelese)) {
			writeTestResults("Verify user can draft Cheque Employee Receipt",
					"User should be able to draft Cheque Employee Receipt",
					"User sucessfully draft Cheque Employee Receipt", "pass");
		} else {
			writeTestResults("Verify user can draft Cheque Employee Receipt",
					"User should be able to draft Cheque Employee Receipt",
					"User could'nt draft Cheque Employee Receipt", "fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedInboundPayment, 30);
		trackCode = getText(lbl_docNo);
		getFinObj().writeFinanceData("EmployeeReciptCheque_FIN_BD_31", trackCode, 20);
		if (isDisplayed(header_releasedInboundPayment)) {
			writeTestResults("Verify user can release Cheque Employee Receipt",
					"User should be able to release Cheque Employee Receipt",
					"User sucessfully release Cheque Employee Receipt", "pass");
		} else {
			writeTestResults("Verify user can release Cheque Employee Receipt",
					"User should be able to release Cheque Employee Receipt",
					"User could'nt release Cheque Employee Receipt", "fail");
		}
	}

	public void bankDeposit_FIN_BD_31() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_bankDeposit, 30);
		if (isDisplayed(btn_bankDeposit)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}

		click(btn_bankDeposit);
		explicitWait(header_bankDeposit, 30);
		if (isDisplayed(header_bankDeposit)) {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page",
					"User successfully navigate to Bank Deposit by page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page", "User doesn't navigate to Bank Deposit by page",
					"fail");
		}

		click(btn_newBankDeposit);
		explicitWait(header_newBankDeposit, 30);
		if (isDisplayed(header_newBankDeposit)) {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page successfully displayed",
					"pass");
		} else {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page doesn't displayed", "fail");
		}

		selectText(drop_payMethodBankDeposi, payMethodCheque);
		String selectedComboValue = getSelectedOptionInDropdown(drop_payMethodBankDeposi);
		if (selectedComboValue.equals(payMethodCheque)) {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User successfully select any Pay Method from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User doesn't select any Pay Method from the drop down where the field will populate the selected value",
					"fail");
		}

		sendKeys(txt_descriptinArea, description);
		String descriptionStr = getAttribute(txt_descriptinArea, "value");
		if (descriptionStr.equals(description)) {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User successfully enter characters, numbers, alphanumeric and special characters to Description",
					"pass");
		} else {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User doesn't enter characters, numbers, alphanumeric and special characters to Description",
					"fail");
		}

		selectText(drop_bankNameBD, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bankNameBD);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_accountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_accountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_viewBankDeposit);
		explicitWait(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("EmployeeReciptCheque_FIN_BD_31")), 30);
		if (isDisplayed(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("EmployeeReciptCheque_FIN_BD_31")))) {
			writeTestResults("Verify Employee Receipt get loaded in the document list",
					"Employee Receipt should get loaded in the document list",
					"Employee Receipt successfully get loaded in the document list", "pass");
		} else {
			writeTestResults("Verify Employee Receipt get loaded in the document list",
					"Employee Receipt should get loaded in the document list",
					"Employee Receipt doesn't get loaded in the document list", "fail");
		}

		click(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("EmployeeReciptCheque_FIN_BD_31")));

		if (isSelected(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("EmployeeReciptCheque_FIN_BD_31")))) {
			writeTestResults("Verify user able to select the Employee Receipt to deposit",
					"User should be able to select the Employee Receipt to deposit",
					"User successfully select the Employee Receipt to deposit", "pass");
		} else {
			writeTestResults("Verify user able to select the Employee Receipt to deposit",
					"User should be able to select the Employee Receipt to deposit",
					"User doesn't select the Employee Receipt to deposit", "fail");
		}

		String depositAmount = getAttribute(txt_depositAmount, "value");
		if (depositAmount.equals("10000.000000")) {
			writeTestResults("Verify that Employee Receipt Amount (10000) get added to Deposit Amount",
					"Employee Receipt Amount (10000) should get added to Deposit Amount",
					"Employee Receipt Amount (10000) successfully get added to Deposit Amount", "pass");
		} else {
			writeTestResults("Verify that Employee Receipt Amount (10000) get added to Deposit Amount",
					"Employee Receipt Amount (10000) should get added to Deposit Amount",
					"Employee Receipt Amount (10000) doesn't get added to Deposit Amount", "fail");
		}
	}

	/* FIN_BD_32_33_34 */
	public void bankDeposit_FIN_BD_32_33_34() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 20);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 20);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_bankDeposit, 30);
		if (isDisplayed(btn_bankDeposit)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}

		click(btn_bankDeposit);
		explicitWait(header_bankDeposit, 30);
		if (isDisplayed(header_bankDeposit)) {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page",
					"User successfully navigate to Bank Deposit by page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page", "User doesn't navigate to Bank Deposit by page",
					"fail");
		}

		click(btn_newBankDeposit);
		explicitWait(header_newBankDeposit, 30);
		if (isDisplayed(header_newBankDeposit)) {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page successfully displayed",
					"pass");
		} else {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page doesn't displayed", "fail");
		}

		selectText(drop_payMethodBankDeposi, payMethodCheque);
		String selectedComboValue = getSelectedOptionInDropdown(drop_payMethodBankDeposi);
		if (selectedComboValue.equals(payMethodCheque)) {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User successfully select any Pay Method from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User doesn't select any Pay Method from the drop down where the field will populate the selected value",
					"fail");
		}

		sendKeys(txt_descriptinArea, description);
		String descriptionStr = getAttribute(txt_descriptinArea, "value");
		if (descriptionStr.equals(description)) {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User successfully enter characters, numbers, alphanumeric and special characters to Description",
					"pass");
		} else {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User doesn't enter characters, numbers, alphanumeric and special characters to Description",
					"fail");
		}

		selectText(drop_bankNameBD, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bankNameBD);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_accountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_accountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_viewBankDeposit);

		explicitWait(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerReciptCheque_FIN_BD_28")), 30);
		if (isDisplayed(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerReciptCheque_FIN_BD_28")))) {
			writeTestResults("Verify Customer Receipt get loaded in the document list",
					"Customer Receipt should get loaded in the document list",
					"Customer Receipt successfully get loaded in the document list", "pass");
		} else {
			writeTestResults("Verify Customer Receipt get loaded in the document list",
					"Customer Receipt should get loaded in the document list",
					"Customer Receipt doesn't get loaded in the document list", "fail");
		}

		explicitWait(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("VendorReciptCheque_FIN_BD_29")), 5);
		if (isDisplayed(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("VendorReciptCheque_FIN_BD_29")))) {
			writeTestResults("Verify Vendor Receipt get loaded in the document list",
					"Vendor Receipt should get loaded in the document list",
					"Vendor Receipt successfully get loaded in the document list", "pass");
		} else {
			writeTestResults("Verify Vendor Receipt get loaded in the document list",
					"Vendor Receipt should get loaded in the document list",
					"Vendor Receipt doesn't get loaded in the document list", "fail");
		}

		explicitWait(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("GLReciptCheque_FIN_BD_30")), 5);
		if (isDisplayed(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("GLReciptCheque_FIN_BD_30")))) {
			writeTestResults("Verify GL Recipt get loaded in the document list",
					"GL Recipt should get loaded in the document list",
					"GL Recipt successfully get loaded in the document list", "pass");
		} else {
			writeTestResults("Verify GL Recipt get loaded in the document list",
					"GL Recipt should get loaded in the document list",
					"GL Recipt doesn't get loaded in the document list", "fail");
		}

		explicitWait(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("EmployeeReciptCheque_FIN_BD_31")), 5);
		if (isDisplayed(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("EmployeeReciptCheque_FIN_BD_31")))) {
			writeTestResults("Verify Employee Receipt get loaded in the document list",
					"Employee Receipt should get loaded in the document list",
					"Employee Receipt successfully get loaded in the document list", "pass");
		} else {
			writeTestResults("Verify Employee Receipt get loaded in the document list",
					"Employee Receipt should get loaded in the document list",
					"Employee Receipt doesn't get loaded in the document list", "fail");
		}

		click(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerReciptCheque_FIN_BD_28")));
		if (isSelected(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerReciptCheque_FIN_BD_28")))) {
			writeTestResults("Verify user able to select 'Deposit'(Customer Recipt) check box",
					"User should be able to select 'Deposit'(Customer Recipt) check box",
					"User successfully select 'Deposit'(Customer Recipt) check box", "pass");
		} else {
			writeTestResults("Verify user able to select 'Deposit'(Customer Recipt) check box",
					"User should be able to select 'Deposit'(Customer Recipt) check box",
					"User doesn't select 'Deposit'(Customer Recipt) check box", "fail");
		}

		String depositAmountCustomerRecipt = getAttribute(txt_depositAmountBankDeposit.replace("doc_no",
				getFinObj().readFinanceData("CustomerReciptCheque_FIN_BD_28")), "value");

		if (depositAmountCustomerRecipt.equals("5800.000000")) {
			writeTestResults(
					"Verify that the full amount of the payment gets filled in the 'Deposit Amount'(Customer Recipt) field in the record",
					"Full amount of the payment should get filled in 'Deposit Amount'(Customer Recipt) field in the record",
					"Full amount of the payment successfully get filled in 'Deposit Amount'(Customer Recipt) field in the record",
					"pass");
		} else {
			writeTestResults(
					"Verify that the full amount of the payment gets filled in the 'Deposit Amount'(Customer Recipt) field in the record",
					"Full amount of the payment should get filled in 'Deposit Amount'(Customer Recipt) field in the record",
					"Full amount of the payment doesn't get filled in 'Deposit Amount'(Customer Recipt) field in the record",
					"fail");
		}

		String diablePropertyCustomerRecipt = getAttribute(txt_depositAmountBankDeposit.replace("doc_no",
				getFinObj().readFinanceData("CustomerReciptCheque_FIN_BD_28")), "disabled");
		if (diablePropertyCustomerRecipt.equals("true")) {
			writeTestResults("Verify user not able to edit the 'Deposit Amount'(Customer Recipt) field",
					"User should not be able to edit the 'Deposit Amount'(Customer Recipt) field",
					"User not able to edit the 'Deposit Amount'(Customer Recipt) field", "pass");
		} else {
			writeTestResults("Verify user not able to edit the 'Deposit Amount'(Customer Recipt) field",
					"User should not be able to edit the 'Deposit Amount'(Customer Recipt) field",
					"User able to edit the 'Deposit Amount'(Customer Recipt) field", "fail");
		}

		click(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("VendorReciptCheque_FIN_BD_29")));
		if (isSelected(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("VendorReciptCheque_FIN_BD_29")))) {
			writeTestResults("Verify user able to select 'Deposit'(Vendor Recipt) check box",
					"User should be able to select 'Deposit'(Vendor Recipt) check box",
					"User successfully select 'Deposit'(Vendor Recipt) check box", "pass");
		} else {
			writeTestResults("Verify user able to select 'Deposit'(Vendor Recipt) check box",
					"User should be able to select 'Deposit'(Vendor Recipt) check box",
					"User doesn't select 'Deposit'(Vendor Recipt) check box", "fail");
		}

		String depositAmountVendorRecipt = getAttribute(txt_depositAmountBankDeposit.replace("doc_no",
				getFinObj().readFinanceData("VendorReciptCheque_FIN_BD_29")), "value");

		if (depositAmountVendorRecipt.equals("2200.000000")) {
			writeTestResults(
					"Verify that the full amount of the payment gets filled in the 'Deposit Amount'(Vendor Recipt) field in the record",
					"Full amount of the payment should get filled in 'Deposit Amount'(Vendor Recipt) field in the record",
					"Full amount of the payment successfully get filled in 'Deposit Amount'(Vendor Recipt) field in the record",
					"pass");
		} else {
			writeTestResults(
					"Verify that the full amount of the payment gets filled in the 'Deposit Amount'(Vendor Recipt) field in the record",
					"Full amount of the payment should get filled in 'Deposit Amount'(Vendor Recipt) field in the record",
					"Full amount of the payment doesn't get filled in 'Deposit Amount'(Vendor Recipt) field in the record",
					"fail");
		}

		String diablePropertyVendorRecipt = getAttribute(txt_depositAmountBankDeposit.replace("doc_no",
				getFinObj().readFinanceData("VendorReciptCheque_FIN_BD_29")), "disabled");
		if (diablePropertyVendorRecipt.equals("true")) {
			writeTestResults("Verify user not able to edit the 'Deposit Amount'(Vendor Recipt) field",
					"User should not be able to edit the 'Deposit Amount'(Vendor Recipt) field",
					"User not able to edit the 'Deposit Amount'(Vendor Recipt) field", "pass");
		} else {
			writeTestResults("Verify user not able to edit the 'Deposit Amount'(Vendor Recipt) field",
					"User should not be able to edit the 'Deposit Amount'(Vendor Recipt) field",
					"User able to edit the 'Deposit Amount'(Vendor Recipt) field", "fail");
		}

		click(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("GLReciptCheque_FIN_BD_30")));
		if (isSelected(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("GLReciptCheque_FIN_BD_30")))) {
			writeTestResults("Verify user able to select 'Deposit'(GL Recipt) check box",
					"User should be able to select 'Deposit'(GL Recipt) check box",
					"User successfully select 'Deposit'(GL Recipt) check box", "pass");
		} else {
			writeTestResults("Verify user able to select 'Deposit'(GL Recipt) check box",
					"User should be able to select 'Deposit'(GL Recipt) check box",
					"User doesn't select 'Deposit'(GL Recipt) check box", "fail");
		}

		String depositAmountGLRecipt = getAttribute(
				txt_depositAmountBankDeposit.replace("doc_no", getFinObj().readFinanceData("GLReciptCheque_FIN_BD_30")),
				"value");

		if (depositAmountGLRecipt.equals("25000.000000")) {
			writeTestResults(
					"Verify that the full amount of the payment gets filled in the 'Deposit Amount'(GL Recipt) field in the record",
					"Full amount of the payment should get filled in 'Deposit Amount'(GL Recipt) field in the record",
					"Full amount of the payment successfully get filled in 'Deposit Amount'(GL Recipt) field in the record",
					"pass");
		} else {
			writeTestResults(
					"Verify that the full amount of the payment gets filled in the 'Deposit Amount'(GL Recipt) field in the record",
					"Full amount of the payment should get filled in 'Deposit Amount'(GL Recipt) field in the record",
					"Full amount of the payment doesn't get filled in 'Deposit Amount'(GL Recipt) field in the record",
					"fail");
		}

		String diablePropertyGLRecipt = getAttribute(
				txt_depositAmountBankDeposit.replace("doc_no", getFinObj().readFinanceData("GLReciptCheque_FIN_BD_30")),
				"disabled");
		if (diablePropertyGLRecipt.equals("true")) {
			writeTestResults("Verify user not able to edit the 'Deposit Amount'(GL Recipt) field",
					"User should not be able to edit the 'Deposit Amount'(GL Recipt) field",
					"User not able to edit the 'Deposit Amount'(GL Recipt) field", "pass");
		} else {
			writeTestResults("Verify user not able to edit the 'Deposit Amount'(GL Recipt) field",
					"User should not be able to edit the 'Deposit Amount'(GL Recipt) field",
					"User able to edit the 'Deposit Amount'(GL Recipt) field", "fail");
		}

		click(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("EmployeeReciptCheque_FIN_BD_31")));

		if (isSelected(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("EmployeeReciptCheque_FIN_BD_31")))) {
			writeTestResults("Verify user able to select 'Deposit'(Employee Recipt) check box",
					"User should be able to select 'Deposit'(Employee Recipt) check box",
					"User successfully select 'Deposit'(Employee Recipt) check box", "pass");
		} else {
			writeTestResults("Verify user able to select 'Deposit'(Employee Recipt) check box",
					"User should be able to select 'Deposit'(Employee Recipt) check box",
					"User doesn't select 'Deposit'(Employee Recipt) check box", "fail");
		}

		String depositAmountEmployeeRecipt = getAttribute(txt_depositAmountBankDeposit.replace("doc_no",
				getFinObj().readFinanceData("EmployeeReciptCheque_FIN_BD_31")), "value");

		if (depositAmountEmployeeRecipt.equals("10000.000000")) {
			writeTestResults(
					"Verify that the full amount of the payment gets filled in the 'Deposit Amount'(Employee Recipt) field in the record",
					"Full amount of the payment should get filled in 'Deposit Amount'(Employee Recipt) field in the record",
					"Full amount of the payment successfully get filled in 'Deposit Amount'(Employee Recipt) field in the record",
					"pass");
		} else {
			writeTestResults(
					"Verify that the full amount of the payment gets filled in the 'Deposit Amount'(Employee Recipt) field in the record",
					"Full amount of the payment should get filled in 'Deposit Amount'(Employee Recipt) field in the record",
					"Full amount of the payment doesn't get filled in 'Deposit Amount'(Employee Recipt) field in the record",
					"fail");
		}

		String diablePropertyEmployeeRecipt = getAttribute(txt_depositAmountBankDeposit.replace("doc_no",
				getFinObj().readFinanceData("EmployeeReciptCheque_FIN_BD_31")), "disabled");
		if (diablePropertyEmployeeRecipt.equals("true")) {
			writeTestResults("Verify user not able to edit the 'Deposit Amount'(Employee Recipt) field",
					"User should not be able to edit the 'Deposit Amount'(Employee Recipt) field",
					"User not able to edit the 'Deposit Amount'(Employee Recipt) field", "pass");
		} else {
			writeTestResults("Verify user not able to edit the 'Deposit Amount'(Employee Recipt) field",
					"User should not be able to edit the 'Deposit Amount'(Employee Recipt) field",
					"User able to edit the 'Deposit Amount'(Employee Recipt) field", "fail");
		}

		String depositAmount = getAttribute(txt_depositAmount, "value");
		if (depositAmount.equals("43000.000000")) {
			writeTestResults(
					"Verify Deposit Amount is display with the sum of all payment selected for deposit (5800 + 2200 + 25000 + 10000 = 43000)",
					"Deposit Amount should be sum of all payment selected for deposit (5800 + 2200 + 25000 + 10000 = 43000)",
					"Deposit Amount display with the sum of all payment selected for deposit (5800 + 2200 + 25000 + 10000 = 43000)",
					"pass");
		} else {
			writeTestResults(
					"Verify Deposit Amount is display with the sum of all payment selected for deposit (5800 + 2200 + 25000 + 10000 = 43000)",
					"Deposit Amount should be sum of all payment selected for deposit (5800 + 2200 + 25000 + 10000 = 43000)",
					"Deposit Amount doesn't display with the sum of all payment selected for deposit (5800 + 2200 + 25000 + 10000 = 43000)",
					"fail");

		}

		click(btn_draft);
		explicitWait(header_draftedBankDeposit, 30);
		if (isDisplayed(header_draftedBankDeposit)) {
			writeTestResults("Verify user can draft the Bank Deposit", "User should be able to draft the Bank Deposit",
					"User successfully draft the Bank Deposit", "pass");
		} else {
			writeTestResults("Verify user can draft the Bank Deposit", "User should be able to draft the Bank Deposit",
					"User doesn't draft the Bank Deposit", "fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedBankDeposit, 30);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedBankDeposit)) {
			writeTestResults("Verify user can release the Bank Deposit",
					"User should be able to release the Bank Deposit", "User successfully release the Bank Deposit",
					"pass");
		} else {
			writeTestResults("Verify user can release the Bank Deposit",
					"User should be able to release the Bank Deposit", "User doesn't release the Bank Deposit", "fail");
		}

		click(btn_action);
		explicitWait(btn_journel, 10);
		click(btn_journel);
		explicitWait(header_journelEntry, 10);
		if (isDisplayed(header_journelEntry)) {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded",
					"Journal Entry details pop up successfully get loaded", "pass");
		} else {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded", "Journal Entry details pop up doesn't get loaded",
					"fail");
		}

		int idividualJECount = getCount(tbl_coundJournelEntries);
		if (idividualJECount == 4) {
			writeTestResults(
					"Veify Individual Journal Entries are created to each payment record selected in the Bank Deposit",
					"Individual Journal Entries should be created to each payment record selected in the Bank Deposit",
					"Individual Journal Entries successfully created to each payment record selected in the Bank Deposit",
					"pass");
		} else {
			writeTestResults(
					"Veify Individual Journal Entries are created to each payment record selected in the Bank Deposit",
					"Individual Journal Entries should be created to each payment record selected in the Bank Deposit",
					"Individual Journal Entries weren't created for each payment record selected in the Bank Deposit",
					"fail");
		}

		/* Customer Receipt */
		String debitBalanceCustomerRecipt = getText(
				lbl_debitJournelEntryBankDepositGLReplace.replace("-je", "0").replace("gl", glBank));
		if (debitBalanceCustomerRecipt.equals("5800.000000")) {
			writeTestResults("Verify 'Deposit Amount'(Customer Receipt) value is debited to the selected Bank Account",
					"'Deposit Amount'(Customer Receipt) value should get debited to the selected Bank Account",
					"'Deposit Amount'(Customer Receipt) value successfully get debited to the selected Bank Account",
					"pass");
		} else {
			writeTestResults("Verify 'Deposit Amount'(Customer Receipt) value is debited to the selected Bank Account",
					"'Deposit Amount'(Customer Receipt) value should get debited to the selected Bank Account",
					"'Deposit Amount'(Customer Receipt) value doesn't get debited to the selected Bank Account",
					"fail");
		}

		String creditBalanceCustomerRecipt = getText(
				lbl_creditJournelEntryBankDepositGLReplace.replace("-je", "0").replace("gl", glCustomerAdvance));

		if (creditBalanceCustomerRecipt.equals("5800.000000")) {
			writeTestResults("Verify 'Deposit Amount'(Customer Receipt) value is credited from the Relevant Account",
					"'Deposit Amount'(Customer Receipt) value should get credited from the Relevant Account",
					"'Deposit Amount'(Customer Receipt) value successfully get credited from the Relevant Account",
					"pass");
		} else {
			writeTestResults("Verify 'Deposit Amount'(Customer Receipt) value is credited from the Relevant Account",
					"'Deposit Amount'(Customer Receipt) value should get credited from the Relevant Account",
					"'Deposit Amount'(Customer Receipt) value doesn't get credited from the Relevant Account", "fail");
		}

		String debitTotalCustomerRecipt = getText(lbl_debitTotalJournelEntryRecordReplace.replace("-Record", "0"));
		String credTotalCustomerRecipt = getText(lbl_creditTotalJournelEntryRecordReplace.replace("-Record", "0"));

		if (debitTotalCustomerRecipt.equals("5800.000000")
				&& credTotalCustomerRecipt.equals(debitTotalCustomerRecipt)) {
			writeTestResults("Verify total debit and credit amounts (Customer Receipt) were balanced",
					"Total debit and credit amounts (Customer Receipt) should balanced",
					"Total debit and credit amounts (Customer Receipt) successfully balanced", "pass");
		} else {
			writeTestResults("Verify total debit and credit amounts (Customer Receipt) were balanced",
					"Total debit and credit amounts (Customer Receipt) should balanced",
					"Total debit and credit amounts (Customer Receipt) weren't balanced", "fail");
		}

		/* Vendor Receipt */
		String debitBalanceVendorRecipt = getText(
				lbl_debitJournelEntryBankDepositGLReplace.replace("-je", "1").replace("gl", glBank));
		if (debitBalanceVendorRecipt.equals("2200.000000")) {
			writeTestResults("Verify 'Deposit Amount'(Vendor Receipt) value is debited to the selected Bank Account",
					"'Deposit Amount'(Vendor Receipt) value should get debited to the selected Bank Account",
					"'Deposit Amount'(Vendor Receipt) value successfully get debited to the selected Bank Account",
					"pass");
		} else {
			writeTestResults("Verify 'Deposit Amount'(Vendor Receipt) value is debited to the selected Bank Account",
					"'Deposit Amount'(Vendor Receipt) value should get debited to the selected Bank Account",
					"'Deposit Amount'(Vendor Receipt) value doesn't get debited to the selected Bank Account", "fail");
		}

		String creditBalanceVendorRecipt = getText(
				lbl_creditJournelEntryBankDepositGLReplace.replace("-je", "1").replace("gl", glTradeCreditors));

		if (creditBalanceVendorRecipt.equals("2200.000000")) {
			writeTestResults("Verify 'Deposit Amount'(Vendor Receipt) value is credited to the Relevant Account",
					"'Deposit Amount'(Vendor Receipt) value should get credited to the Relevant Account",
					"'Deposit Amount'(Vendor Receipt) value successfully get credited to the Relevant Account", "pass");
		} else {
			writeTestResults("Verify 'Deposit Amount'(Vendor Receipt) value is credited to the Relevant Account",
					"'Deposit Amount'(Vendor Receipt) value should get credited to the Relevant Account",
					"'Deposit Amount'(Vendor Receipt) value doesn't get credited to the Relevant Account", "fail");
		}

		String debitTotalVendorRecipt = getText(lbl_debitTotalJournelEntryRecordReplace.replace("-Record", "1"));
		String credTotalVendorRecipt = getText(lbl_creditTotalJournelEntryRecordReplace.replace("-Record", "1"));

		if (debitTotalVendorRecipt.equals("2200.000000") && credTotalVendorRecipt.equals(debitTotalVendorRecipt)) {
			writeTestResults("Verify total debit and credit amounts (Vendor Receipt) were balanced",
					"Total debit and credit amounts (Vendor Receipt) should balanced",
					"Total debit and credit amounts (Vendor Receipt) successfully balanced", "pass");
		} else {
			writeTestResults("Verify total debit and credit amounts (Vendor Receipt) were balanced",
					"Total debit and credit amounts (Vendor Receipt) should balanced",
					"Total debit and credit amounts (Vendor Receipt) weren't balanced", "fail");
		}

		/* GL Receipt */
		String debitBalanceGLRecipt = getText(
				lbl_debitJournelEntryBankDepositGLReplace.replace("-je", "2").replace("gl", glBank));
		if (debitBalanceGLRecipt.equals("25000.000000")) {
			writeTestResults("Verify 'Deposit Amount'(GL Receipt) value is debited to the selected Bank Account",
					"'Deposit Amount'(GL Receipt) value should get debited to the selected Bank Account",
					"'Deposit Amount'(GL Receipt) value successfully get debited to the selected Bank Account", "pass");
		} else {
			writeTestResults("Verify 'Deposit Amount'(GL Receipt) value is debited to the selected Bank Account",
					"'Deposit Amount'(GL Receipt) value should get debited to the selected Bank Account",
					"'Deposit Amount'(GL Receipt) value doesn't get debited to the selected Bank Account", "fail");
		}

		String creditBalanceGLRecipt = getText(
				lbl_creditJournelEntryBankDepositGLReplace.replace("-je", "2").replace("gl", glAccountMotorVehicles));

		if (creditBalanceGLRecipt.equals("25000.000000")) {
			writeTestResults("Verify 'Deposit Amount'(GL Receipt) value is credited to the Relevant Account",
					"'Deposit Amount'(GL Receipt) value should get credited to the Relevant Account",
					"'Deposit Amount'(GL Receipt) value successfully get credited to the Relevant Account", "pass");
		} else {
			writeTestResults("Verify 'Deposit Amount'(GL Receipt) value is credited to the Relevant Account",
					"'Deposit Amount'(GL Receipt) value should get credited to the Relevant Account",
					"'Deposit Amount'(GL Receipt) value doesn't get credited to the Relevant Account", "fail");
		}

		String debitTotalGLRecipt = getText(lbl_debitTotalJournelEntryRecordReplace.replace("-Record", "2"));
		String credTotalGLRecipt = getText(lbl_creditTotalJournelEntryRecordReplace.replace("-Record", "2"));

		if (debitTotalGLRecipt.equals("25000.000000") && credTotalGLRecipt.equals(debitTotalGLRecipt)) {
			writeTestResults("Verify total debit and credit amounts (GL Receipt) were balanced",
					"Total debit and credit amounts (GL Receipt) should balanced",
					"Total debit and credit amounts (GL Receipt) successfully balanced", "pass");
		} else {
			writeTestResults("Verify total debit and credit amounts (GL Receipt) were balanced",
					"Total debit and credit amounts (GL Receipt) should balanced",
					"Total debit and credit amounts (GL Receipt) weren't balanced", "fail");
		}

		/* Employee Receipt */
		String debitBalanceEmployeeRecipt = getText(
				lbl_debitJournelEntryBankDepositGLReplace.replace("-je", "3").replace("gl", glBank));
		if (debitBalanceEmployeeRecipt.equals("10000.000000")) {
			writeTestResults("Verify 'Deposit Amount'(Employee Receipt) value is debited to the selected Bank Account",
					"'Deposit Amount'(Employee Receipt) value should get debited to the selected Bank Account",
					"'Deposit Amount'(Employee Receipt) value successfully get debited to the selected Bank Account",
					"pass");
		} else {
			writeTestResults("Verify 'Deposit Amount'(Employee Receipt) value is debited to the selected Bank Account",
					"'Deposit Amount'(Employee Receipt) value should get debited to the selected Bank Account",
					"'Deposit Amount'(Employee Receipt) value doesn't get debited to the selected Bank Account",
					"fail");
		}

		String creditBalanceEmployeeRecipt = getText(
				lbl_creditJournelEntryBankDepositGLReplace.replace("-je", "3").replace("gl", glInvenstInShares));

		if (creditBalanceEmployeeRecipt.equals("10000.000000")) {
			writeTestResults("Verify 'Deposit Amount'(Employee Receipt) value is credited to the Relevant Account",
					"'Deposit Amount'(Employee Receipt) value should get credited to the Relevant Account",
					"'Deposit Amount'(Employee Receipt) value successfully get credited to the Relevant Account",
					"pass");
		} else {
			writeTestResults("Verify 'Deposit Amount'(Employee Receipt) value is credited to the Relevant Account",
					"'Deposit Amount'(Employee Receipt) value should get credited to the Relevant Account",
					"'Deposit Amount'(Employee Receipt) value doesn't get credited to the Relevant Account", "fail");
		}

		String debitTotalEmployeeRecipt = getText(lbl_debitTotalJournelEntryRecordReplace.replace("-Record", "3"));
		String credTotalEmployeeRecipt = getText(lbl_creditTotalJournelEntryRecordReplace.replace("-Record", "3"));

		if (debitTotalEmployeeRecipt.equals("10000.000000")
				&& credTotalEmployeeRecipt.equals(debitTotalEmployeeRecipt)) {
			writeTestResults("Verify total debit and credit amounts (Employee Receipt) were balanced",
					"Total debit and credit amounts (Employee Receipt) should balanced",
					"Total debit and credit amounts (Employee Receipt) successfully balanced", "pass");
		} else {
			writeTestResults("Verify total debit and credit amounts (Employee Receipt) were balanced",
					"Total debit and credit amounts (Employee Receipt) should balanced",
					"Total debit and credit amounts (Employee Receipt) weren't balanced", "fail");
		}

		/* Compare GL Documents */
		String journelEntryCustomerRecipt = getText(lbl_journelEntryDocNoJEReplace.replace("-je", "0"));
		String journelEntryVendorRecipt = getText(lbl_journelEntryDocNoJEReplace.replace("-je", "1"));
		String journelEntryGLRecipt = getText(lbl_journelEntryDocNoJEReplace.replace("-je", "2"));
		String journelEntryEmployeeRecipt = getText(lbl_journelEntryDocNoJEReplace.replace("-je", "3"));
		click(close_headerPopup);

		/* Inbound Payment - Customer Receipt */
		explicitWait(navigation_pane, 30);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_inboundPayment, 30);
		if (isDisplayed(btn_inboundPayment)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}
		click(btn_inboundPayment);
		explicitWait(txt_searchInboundPayment2, 30);
		if (isDisplayed(txt_searchInboundPayment2)) {

			writeTestResults("Verify user can navigate to Inbound payment by-page",
					"User should navigate to Inbound payment by-page",
					"User successfully navigate to Inbound payment by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Inbound payment by-page",
					"User should navigate to Inbound payment by-page",
					"User could'nt navigate to Inbound payment by-page", "fail");
		}

		/* Inbound Payment - Customer Receipt */
		explicitWait(lnl_releventInboundPaymentDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerReciptCheque_FIN_BD_28")), 40);
		if (!isDisplayedQuickCheck(lnl_releventInboundPaymentDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerReciptCheque_FIN_BD_28")))) {
			sendKeys(txt_searchInboundPayment2, getFinObj().readFinanceData("CustomerReciptCheque_FIN_BD_28"));
			pressEnter(txt_searchInboundPayment2);
			explicitWait(lnl_releventInboundPaymentDocReplace.replace("doc_no",
					getFinObj().readFinanceData("CustomerReciptCheque_FIN_BD_28")), 30);

		}

		click(lnl_releventInboundPaymentDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerReciptCheque_FIN_BD_28")));

		explicitWait(btn_action, 30);
		click(btn_action);
		explicitWait(btn_journel, 10);
		click(btn_journel);
		explicitWait(header_journelEntry, 10);
		if (isDisplayed(header_journelEntry)) {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded",
					"Journal Entry details pop up successfully get loaded", "pass");
		} else {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded", "Journal Entry details pop up doesn't get loaded",
					"fail");
		}

		if (journelEntryCustomerRecipt.equals(getText(lbl_journelEntryDocNoSingleEntry))) {
			writeTestResults("Verify JE No in Bank Deposit and Inbound Payment (Customer Receipt) is the same",
					"JE No in Bank Deposit and Inbound Payment (Customer Receipt) should be the same",
					"JE No in Bank Deposit and Inbound Payment (Customer Receipt) is the same", "pass");
		} else {
			writeTestResults("Verify JE No in Bank Deposit and Inbound Payment (Customer Receipt) is the same",
					"JE No in Bank Deposit and Inbound Payment (Customer Receipt) should be the same",
					"JE No in Bank Deposit and Inbound Payment (Customer Receipt) is not the same", "fail");
		}

		/* Inbound Payment - Vendor Receipt */
		goBack();
		explicitWait(lnl_releventInboundPaymentDocReplace.replace("doc_no",
				getFinObj().readFinanceData("VendorReciptCheque_FIN_BD_29")), 40);
		if (!isDisplayedQuickCheck(lnl_releventInboundPaymentDocReplace.replace("doc_no",
				getFinObj().readFinanceData("VendorReciptCheque_FIN_BD_29")))) {
			sendKeys(txt_searchInboundPayment2, getFinObj().readFinanceData("VendorReciptCheque_FIN_BD_29"));
			pressEnter(txt_searchInboundPayment2);
			explicitWait(lnl_releventInboundPaymentDocReplace.replace("doc_no",
					getFinObj().readFinanceData("VendorReciptCheque_FIN_BD_29")), 30);

		}

		click(lnl_releventInboundPaymentDocReplace.replace("doc_no",
				getFinObj().readFinanceData("VendorReciptCheque_FIN_BD_29")));

		explicitWait(btn_action, 30);
		click(btn_action);
		explicitWait(btn_journel, 10);
		click(btn_journel);
		explicitWait(header_journelEntry, 10);
		if (isDisplayed(header_journelEntry)) {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded",
					"Journal Entry details pop up successfully get loaded", "pass");
		} else {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded", "Journal Entry details pop up doesn't get loaded",
					"fail");
		}

		if (journelEntryVendorRecipt.equals(getText(lbl_journelEntryDocNoSingleEntry))) {
			writeTestResults("Verify JE No in Bank Deposit and Inbound Payment (Vendor Receipt) is the same",
					"JE No in Bank Deposit and Inbound Payment (Vendor Receipt) should be the same",
					"JE No in Bank Deposit and Inbound Payment (Vendor Receipt) is the same", "pass");
		} else {
			writeTestResults("Verify JE No in Bank Deposit and Inbound Payment (Vendor Receipt) is the same",
					"JE No in Bank Deposit and Inbound Payment (Vendor Receipt) should be the same",
					"JE No in Bank Deposit and Inbound Payment (Vendor Receipt) is not the same", "fail");
		}

		/* Inbound Payment - GL Receipt */
		goBack();
		explicitWait(lnl_releventInboundPaymentDocReplace.replace("doc_no",
				getFinObj().readFinanceData("GLReciptCheque_FIN_BD_30")), 40);
		if (!isDisplayedQuickCheck(lnl_releventInboundPaymentDocReplace.replace("doc_no",
				getFinObj().readFinanceData("GLReciptCheque_FIN_BD_30")))) {
			sendKeys(txt_searchInboundPayment2, getFinObj().readFinanceData("GLReciptCheque_FIN_BD_30"));
			pressEnter(txt_searchInboundPayment2);
			explicitWait(lnl_releventInboundPaymentDocReplace.replace("doc_no",
					getFinObj().readFinanceData("GLReciptCheque_FIN_BD_30")), 30);

		}

		click(lnl_releventInboundPaymentDocReplace.replace("doc_no",
				getFinObj().readFinanceData("GLReciptCheque_FIN_BD_30")));

		explicitWait(btn_action, 30);
		click(btn_action);
		explicitWait(btn_journel, 10);
		click(btn_journel);
		explicitWait(header_journelEntry, 10);
		if (isDisplayed(header_journelEntry)) {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded",
					"Journal Entry details pop up successfully get loaded", "pass");
		} else {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded", "Journal Entry details pop up doesn't get loaded",
					"fail");
		}

		if (journelEntryGLRecipt.equals(getText(lbl_journelEntryDocNoSingleEntry))) {
			writeTestResults("Verify JE No in Bank Deposit and Inbound Payment (GL Receipt) is the same",
					"JE No in Bank Deposit and Inbound Payment (GL Receipt) should be the same",
					"JE No in Bank Deposit and Inbound Payment (GL Receipt) is the same", "pass");
		} else {
			writeTestResults("Verify JE No in Bank Deposit and Inbound Payment (GL Receipt) is the same",
					"JE No in Bank Deposit and Inbound Payment (GL Receipt) should be the same",
					"JE No in Bank Deposit and Inbound Payment (GL Receipt) is not the same", "fail");
		}

		/* Inbound Payment - Employee Receipt */
		goBack();
		explicitWait(lnl_releventInboundPaymentDocReplace.replace("doc_no",
				getFinObj().readFinanceData("EmployeeReciptCheque_FIN_BD_31")), 40);
		if (!isDisplayedQuickCheck(lnl_releventInboundPaymentDocReplace.replace("doc_no",
				getFinObj().readFinanceData("EmployeeReciptCheque_FIN_BD_31")))) {
			sendKeys(txt_searchInboundPayment2, getFinObj().readFinanceData("EmployeeReciptCheque_FIN_BD_31"));
			pressEnter(txt_searchInboundPayment2);
			explicitWait(lnl_releventInboundPaymentDocReplace.replace("doc_no",
					getFinObj().readFinanceData("EmployeeReciptCheque_FIN_BD_31")), 30);

		}

		click(lnl_releventInboundPaymentDocReplace.replace("doc_no",
				getFinObj().readFinanceData("EmployeeReciptCheque_FIN_BD_31")));

		explicitWait(btn_action, 30);
		click(btn_action);
		explicitWait(btn_journel, 10);
		click(btn_journel);
		explicitWait(header_journelEntry, 10);
		if (isDisplayed(header_journelEntry)) {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded",
					"Journal Entry details pop up successfully get loaded", "pass");
		} else {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded", "Journal Entry details pop up doesn't get loaded",
					"fail");
		}

		if (journelEntryEmployeeRecipt.equals(getText(lbl_journelEntryDocNoSingleEntry))) {
			writeTestResults("Verify JE No in Bank Deposit and Inbound Payment (Employee Receipt) is the same",
					"JE No in Bank Deposit and Inbound Payment (Employee Receipt) should be the same",
					"JE No in Bank Deposit and Inbound Payment (Employee Receipt) is the same", "pass");
		} else {
			writeTestResults("Verify JE No in Bank Deposit and Inbound Payment (Employee Receipt) is the same",
					"JE No in Bank Deposit and Inbound Payment (Employee Receipt) should be the same",
					"JE No in Bank Deposit and Inbound Payment (Employee Receipt) is not the same", "fail");
		}
	}

	/* FIN_BD_35_36_37 */
	public void inboundPaymentAdvice_FIN_BD_35_36_37() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_inboundPaymentAdvice, 30);
		if (isDisplayed(btn_inboundPaymentAdvice)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}
		click(btn_inboundPaymentAdvice);
		explicitWait(btn_newInboundPaymentAdvice, 30);
		if (isDisplayed(btn_newInboundPaymentAdvice)) {

			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User successfully navigate to Inbound payment advice by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User could'nt navigate to Inbound payment advice by-page", "fail");
		}

		click(btn_newInboundPaymentAdvice);
		if (isDisplayed(btn_CusAdvanceJourneyIPA)) {

			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User successfully view the list of journey",
					"pass");
		} else {
			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User could'nt view the list of journey",
					"fail");
		}

		click(btn_CusAdvanceJourneyIPA);
		explicitWait(lnl_postBusinessUnitIPA, 30);
		if (isDisplayed(lnl_postBusinessUnitIPA)) {

			writeTestResults("Verify user able to navigate to Customer Advance/ Customer Debit Memo",
					"User should be able to navigate to Customer Advance/ Customer Debit Memo",
					"User successfully navigate to Customer Advance/ Customer Debit Memo", "pass");
		} else {
			writeTestResults("Verify user able to navigate to Customer Advance/ Customer Debit Memo",
					"User should be able to navigate to Customer Advance/ Customer Debit Memo",
					"User doesn't navigate to Customer Advance/ Customer Debit Memo", "fail");
		}

		Thread.sleep(1000);
		click(lnl_postBusinessUnitIPA);
		Thread.sleep(700);
		selectText(drop_postBusinessUnit, postBusinessUnitSch);
		Thread.sleep(700);
		click(tab_subbaryIPA);
		Thread.sleep(2000);
		String get_post_business_unit = getText(lnl_postBusinessUnitIPA);
		System.out.println(get_post_business_unit);
		if (get_post_business_unit.equals(postBusinessUnitSch)) {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User successfully select post business unit from the drop down", "pass");
		} else {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User could'nt select post business unit from the drop down", "fail");
		}

		click(btn_searchCustomerIPA);
		if (isDisplayed(txt_cusAccountSearchIPA)) {
			writeTestResults("Verify user can click on Search button in Customer and Customer Lookup should pop up",
					"User should be click on Search button in Customer and Customer Lookup should pop up",
					"User successfully click on Search button in Customer and Customer Lookup should pop up", "pass");
		} else {

			writeTestResults("Verify user can click on Search button in Customer and Customer Lookup should pop up",
					"User should be click on Search button in Customer and Customer Lookup should pop up",
					"User could'nt click on Search button in Customer and Customer Lookup should pop up", "fail");
		}

		explicitWait(txt_cusAccountSearchIPA, 30);
		sendKeys(txt_cusAccountSearchIPA, customerAccount);
		pressEnter(txt_cusAccountSearchIPA);
		Thread.sleep(1500);
		explicitWait(lnk_resultCustomerAccountIPA, 30);
		if (isDisplayed(lnk_resultCustomerAccountIPA)) {
			writeTestResults("Verify user can search an existing customer by entering Customer name/code",
					"User should be able to search an existing customer by entering Customer name/code",
					"User successfully search an existing customer by entering Customer name/code", "pass");
		} else {

			writeTestResults("Verify user can search an existing customer by entering Customer name/code",
					"User should be able to search an existing customer by entering Customer name/code",
					"User could'nt search an existing customer by entering Customer name/code", "fail");
		}

		doubleClick(lnk_resultCustomerAccountIPA);

		sendKeys(txt_refNoIPA, testAllCharcterWordIPA);

		sendKeys(txt_descIPA, testAllCharcterWordIPA);
		Thread.sleep(2000);

		Select comboBox = new Select(driver.findElement(By.xpath(drop_cusrencyIPA)));
		selectText(drop_cusrencyIPA, currencyLKR);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		if (selectedComboValue.equals(currencyLKR)) {
			writeTestResults("Verify user can choose the currency from the drop down",
					"User should be able to choose the currency from the drop down",
					"User successfully choose the currency from the drop down", "pass");
		} else {

			writeTestResults("Verify user can choose the currency from the drop down",
					"User should be able to choose the currency from the drop down",
					"User could'nt choose the currency from the drop down", "fail");
		}

		sendKeys(txt_amountIPA, amountTwoThousand);
		Thread.sleep(2000);
		String ckeckoutBefore = getText(btn_documentDataIPA);

		click(btn_checkout);
		Thread.sleep(3000);
		String ckeckoutAfter = getText(btn_documentDataIPA);
		if (!ckeckoutBefore.equals(ckeckoutAfter)) {
			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User successfully checkout Inbound Payment Advice", "pass");
		} else {

			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User doesn't checkout Inbound Payment Advice", "fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 40);
		if (isDisplayed(btn_reelese)) {

			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice",
					"User sucessfully draft Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice", "User could'nt draft Inboumd Payment Advice",
					"fail");
		}

		Thread.sleep(3000);
		click(btn_reelese);
		explicitWait(header_releasedInboundPaymentAdvice, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedInboundPaymentAdvice)) {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User sucessfully release Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User could'nt release Inboumd Payment Advice", "fail");
		}
	}

	public void inboundPayment_FIN_BD_35_36_37() throws Exception {
		click(btn_actionCRV);

		click(btn_converToInboundPaymentCRV);
		click(btn_detailTabCRV);

		click(btn_summaryTab);

		selectText(drop_paidCurrencyCRV, currencyLKR);

		selectText(drop_paymentMethodCRV, payMethodCash);

		selectText(drop_cashAccount, cashAccount);

		click(btn_detailTabCRV);

		click(btn_detailTabCRV);
		selectText(drop_filterCurrencyCRV, currencyLKR);

		click(chk_firstAdviceOnGridInboundPayment);

		click(btn_checkout);
		Thread.sleep(4000);
		String bannerTotalCRV = getText(btn_documentDataCRV);

		if (!bannerTotalCRV.equals("0")) {
			writeTestResults("Verify user can checkout Cash Customer Receipt",
					"User should be able to checkout the Cash Customer Receipt",
					"User successfully checkout Cash Customer Receipt", "pass");
		} else {

			writeTestResults("Verify user can checkout Cash Customer Receipt",
					"User should be able to checkout the Cash Customer Receipt",
					"User doesn't checkout Cash Customer Receipt", "fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 30);
		if (isDisplayed(btn_reelese)) {
			writeTestResults("Verify user can draft Cash Customer Receipt",
					"User should be able to draft Cash Customer Receipt",
					"User sucessfully draft Cash Customer Receipt", "pass");
		} else {
			writeTestResults("Verify user can draft Cash Customer Receipt",
					"User should be able to draft Cash Customer Receipt", "User could'nt draft Cash Customer Receipt",
					"fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedInboundPayment, 30);
		trackCode = getText(lbl_docNo);

		getFinObj().writeFinanceData("CustomerRecipt_FIN_BD_35_36_37", trackCode, 21);
		if (isDisplayed(header_releasedInboundPayment)) {
			writeTestResults("Verify user can release Cash Customer Receipt",
					"User should be able to release Cash Customer Receipt",
					"User sucessfully release Cash Customer Receipt", "pass");
		} else {
			writeTestResults("Verify user can release Cash Customer Receipt",
					"User should be able to release Cash Customer Receipt",
					"User could'nt release Cash Customer Receipt", "fail");
		}
	}

	public void bankDeposit1_FIN_BD_35_36_37() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_bankDeposit, 30);
		if (isDisplayed(btn_bankDeposit)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}

		click(btn_bankDeposit);
		explicitWait(header_bankDeposit, 30);
		if (isDisplayed(header_bankDeposit)) {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page",
					"User successfully navigate to Bank Deposit by page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page", "User doesn't navigate to Bank Deposit by page",
					"fail");
		}

		click(btn_newBankDeposit);
		explicitWait(header_newBankDeposit, 30);
		if (isDisplayed(header_newBankDeposit)) {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page successfully displayed",
					"pass");
		} else {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page doesn't displayed", "fail");
		}

		selectText(drop_payMethodBankDeposi, payMethodCash);
		String selectedComboValue = getSelectedOptionInDropdown(drop_payMethodBankDeposi);
		if (selectedComboValue.equals(payMethodCash)) {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User successfully select any Pay Method from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User doesn't select any Pay Method from the drop down where the field will populate the selected value",
					"fail");
		}

		sendKeys(txt_descriptinArea, description);
		String descriptionStr = getAttribute(txt_descriptinArea, "value");
		if (descriptionStr.equals(description)) {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User successfully enter characters, numbers, alphanumeric and special characters to Description",
					"pass");
		} else {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User doesn't enter characters, numbers, alphanumeric and special characters to Description",
					"fail");
		}

		selectText(drop_bankNameBD, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bankNameBD);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_accountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_accountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_viewBankDeposit);
		explicitWait(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerRecipt_FIN_BD_35_36_37")), 30);
		if (isDisplayed(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerRecipt_FIN_BD_35_36_37")))) {
			writeTestResults("Verify Customer Receipt get loaded in the document list",
					"Customer Receipt should get loaded in the document list",
					"Customer Receipt successfully get loaded in the document list", "pass");
		} else {
			writeTestResults("Verify Customer Receipt get loaded in the document list",
					"Customer Receipt should get loaded in the document list",
					"Customer Receipt doesn't get loaded in the document list", "fail");
		}

		click(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerRecipt_FIN_BD_35_36_37")));

		if (isSelected(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerRecipt_FIN_BD_35_36_37")))) {
			writeTestResults("Verify user able to select 'Deposit' check box",
					"User should be able to select 'Deposit' check box", "User successfully select 'Deposit' check box",
					"pass");
		} else {
			writeTestResults("Verify user able to select 'Deposit' check box",
					"User should be able to select 'Deposit' check box", "User doesn't select 'Deposit' check box",
					"fail");
		}

		sendKeys(txt_depositAmountBankDeposit.replace("doc_no",
				getFinObj().readFinanceData("CustomerRecipt_FIN_BD_35_36_37")), amountThousandFiveHundred);

		String depositAmountModifiedValue = getAttribute(txt_depositAmountBankDeposit.replace("doc_no",
				getFinObj().readFinanceData("CustomerRecipt_FIN_BD_35_36_37")), "value");

		if (depositAmountModifiedValue.equals(amountThousandFiveHundred)) {
			writeTestResults("Verify user able to edit the 'Deposit Amount' field",
					"User should be able to edit the 'Deposit Amount' field",
					"User successfully edit the 'Deposit Amount' field", "pass");
		} else {
			writeTestResults("Verify user able to edit the 'Deposit Amount' field",
					"User should be able to edit the 'Deposit Amount' field",
					"User doesn't edit the 'Deposit Amount' field", "fail");
		}

		mouseMove(div_summaryTab);
		click(div_summaryTab);
		Thread.sleep(1000);
		String totDepositAmount = getAttribute(txt_depositAmount, "value");
		if (totDepositAmount.equals("1500.000000")) {
			writeTestResults("Verify that once modified, the total 'Deposit Amount' of the Bank Deposit is get updated",
					"Once modified, total 'Deposit Amount' of the Bank Deposit should get updated",
					"Once modified, total 'Deposit Amount' of the Bank Deposit successfully get updated", "pass");
		} else {
			writeTestResults("Verify that once modified, the total 'Deposit Amount' of the Bank Deposit is get updated",
					"Once modified, total 'Deposit Amount' of the Bank Deposit should get updated",
					"Once modified, total 'Deposit Amount' of the Bank Deposit doesn't get updated", "fail");
		}

		click(btn_draft);
		explicitWait(header_draftedBankDeposit, 30);
		if (isDisplayed(header_draftedBankDeposit)) {
			writeTestResults("Verify user can draft the Bank Deposit", "User should be able to draft the Bank Deposit",
					"User successfully draft the Bank Deposit", "pass");
		} else {
			writeTestResults("Verify user can draft the Bank Deposit", "User should be able to draft the Bank Deposit",
					"User doesn't draft the Bank Deposit", "fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedBankDeposit, 30);
		trackCode = getText(lbl_docNo);
		getFinObj().writeFinanceData("BankDeposit1_FIN_BD_35_36_37", trackCode, 22);
		if (isDisplayed(header_releasedBankDeposit)) {
			writeTestResults("Verify that Bank Deposit is get released with the partial deposit amount",
					"Bank Deposit should get released with the partial deposit amount",
					"Bank Deposit successfully get released with the partial deposit amount", "pass");
		} else {
			writeTestResults("Verify that Bank Deposit is get released with the partial deposit amount",
					"Bank Deposit should get released with the partial deposit amount",
					"Bank Deposit doesn't get released with the partial deposit amount", "fail");
		}

		click(btn_action);
		explicitWait(btn_journel, 10);
		click(btn_journel);
		explicitWait(header_journelEntry, 10);
		if (isDisplayed(header_journelEntry)) {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded",
					"Journal Entry details pop up successfully get loaded", "pass");
		} else {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded", "Journal Entry details pop up doesn't get loaded",
					"fail");
		}

		String debitBalance = getText(lbl_debitJournelEntryBankDepositCash);

		if (debitBalance.equals("1500.000000")) {
			writeTestResults("Verify modified 'Deposit Amount' value is debited to the selected Bank Account",
					"Modified 'Deposit Amount' value should get debited to the selected Bank Account",
					"Modified 'Deposit Amount' value successfully get debited to the selected Bank Account", "pass");
		} else {
			writeTestResults("Verify modified 'Deposit Amount' value is debited to the selected Bank Account",
					"Modified 'Deposit Amount' value should get debited to the selected Bank Account",
					"Modified 'Deposit Amount' value doesn't get debited to the selected Bank Account", "fail");
		}

		String creditBalance = getText(lbl_creditJournelEntryBankDepositCash);

		if (creditBalance.equals("1500.000000")) {
			writeTestResults("Verify modified 'Deposit Amount' value is credited to the Cash in Hand Account",
					"Modified 'Deposit Amount' value should get credited to the Cash in Hand Account",
					"Modified 'Deposit Amount' value successfully get credited to the Cash in Hand Account", "pass");
		} else {
			writeTestResults("Verify modified 'Deposit Amount' value is credited to the Cash in Hand Account",
					"Modified 'Deposit Amount' value should get credited to the Cash in Hand Account",
					"Modified 'Deposit Amount' value doesn't get credited to the Cash in Hand Account", "fail");
		}

		String debTotal = getText(lbl_debitTotalJournelEntry);
		String credTotal = getText(lbl_creditTotalJournelEntry);

		if (debTotal.equals("1500.000000") && debTotal.equals(credTotal)) {
			writeTestResults("Verify total debit and credit amounts were balanced",
					"Total debit and credit amounts should balanced",
					"Total debit and credit amounts successfully balanced", "pass");
		} else {
			writeTestResults("Verify total debit and credit amounts were balanced",
					"Total debit and credit amounts should balanced", "Total debit and credit amounts weren't balanced",
					"fail");
		}

		click(close_headerPopup);
	}

	public void bankDeposit2_FIN_BD_35_36_37() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_bankDeposit, 30);
		if (isDisplayed(btn_bankDeposit)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}

		click(btn_bankDeposit);
		explicitWait(header_bankDeposit, 30);
		if (isDisplayed(header_bankDeposit)) {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page",
					"User successfully navigate to Bank Deposit by page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page", "User doesn't navigate to Bank Deposit by page",
					"fail");
		}

		click(btn_newBankDeposit);
		explicitWait(header_newBankDeposit, 30);
		if (isDisplayed(header_newBankDeposit)) {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page successfully displayed",
					"pass");
		} else {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page doesn't displayed", "fail");
		}

		selectText(drop_payMethodBankDeposi, payMethodCash);
		String selectedComboValue = getSelectedOptionInDropdown(drop_payMethodBankDeposi);
		if (selectedComboValue.equals(payMethodCash)) {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User successfully select any Pay Method from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User doesn't select any Pay Method from the drop down where the field will populate the selected value",
					"fail");
		}

		sendKeys(txt_descriptinArea, description);
		String descriptionStr = getAttribute(txt_descriptinArea, "value");
		if (descriptionStr.equals(description)) {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User successfully enter characters, numbers, alphanumeric and special characters to Description",
					"pass");
		} else {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User doesn't enter characters, numbers, alphanumeric and special characters to Description",
					"fail");
		}

		selectText(drop_bankNameBD, commercialBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bankNameBD);
		if (selectedBank.equals(commercialBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_accountNo, commercialCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_accountNo);
		if (selectedAccount.equals(commercialCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_viewBankDeposit);
		explicitWait(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerRecipt_FIN_BD_35_36_37")), 30);
		if (isDisplayed(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerRecipt_FIN_BD_35_36_37")))) {
			writeTestResults("Verify Customer Receipt get loaded in the document list",
					"Customer Receipt should get loaded in the document list",
					"Customer Receipt successfully get loaded in the document list", "pass");
		} else {
			writeTestResults("Verify Customer Receipt get loaded in the document list",
					"Customer Receipt should get loaded in the document list",
					"Customer Receipt doesn't get loaded in the document list", "fail");
		}

		click(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerRecipt_FIN_BD_35_36_37")));

		if (isSelected(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerRecipt_FIN_BD_35_36_37")))) {
			writeTestResults("Verify user able to select 'Deposit' check box",
					"User should be able to select 'Deposit' check box", "User successfully select 'Deposit' check box",
					"pass");
		} else {
			writeTestResults("Verify user able to select 'Deposit' check box",
					"User should be able to select 'Deposit' check box", "User doesn't select 'Deposit' check box",
					"fail");
		}

		String totDepositAmount = getAttribute(txt_depositAmount, "value");
		if (totDepositAmount.equals("500.000000")) {
			writeTestResults(
					"Verify that due amount of the payment is get filled in 'Deposit Amount' field in the record",
					"Due amount of the payment should get filled in 'Deposit Amount' field in the record",
					"Due amount of the payment successfully get filled in 'Deposit Amount' field in the record",
					"pass");
		} else {
			writeTestResults(
					"Verify that due amount of the payment is get filled in 'Deposit Amount' field in the record",
					"Due amount of the payment should get filled in 'Deposit Amount' field in the record",
					"Due amount of the payment doesn't get filled in 'Deposit Amount' field in the record", "fail");
		}

		click(btn_draft);
		explicitWait(header_draftedBankDeposit, 30);
		if (isDisplayed(header_draftedBankDeposit)) {
			writeTestResults("Verify user can draft the Bank Deposit", "User should be able to draft the Bank Deposit",
					"User successfully draft the Bank Deposit", "pass");
		} else {
			writeTestResults("Verify user can draft the Bank Deposit", "User should be able to draft the Bank Deposit",
					"User doesn't draft the Bank Deposit", "fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedBankDeposit, 30);
		trackCode = getText(lbl_docNo);
		getFinObj().writeFinanceData("BankDeposit2_FIN_BD_35_36_37", trackCode, 23);
		if (isDisplayed(header_releasedBankDeposit)) {
			writeTestResults("Verify that Bank Deposit is get released with the deposit amount",
					"Bank Deposit should get released with the deposit amount",
					"Bank Deposit successfully get released with the deposit amount", "pass");
		} else {
			writeTestResults("Verify that Bank Deposit is get released with the deposit amount",
					"Bank Deposit should get released with the deposit amount",
					"Bank Deposit doesn't get released with the deposit amount", "fail");
		}

		click(btn_action);
		explicitWait(btn_journel, 10);
		click(btn_journel);
		explicitWait(header_journelEntry, 10);
		if (isDisplayed(header_journelEntry)) {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded",
					"Journal Entry details pop up successfully get loaded", "pass");
		} else {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded", "Journal Entry details pop up doesn't get loaded",
					"fail");
		}

		String debitBalance = getText(
				lbl_debitJournelEntryBankDepositGLReplace.replace("-je", "0").replace("gl", glBank2));

		if (debitBalance.equals("500.000000")) {
			writeTestResults("Verify 'Deposit Amount' value is debited to the selected Bank Account",
					"'Deposit Amount' value should get debited to the selected Bank Account",
					"'Deposit Amount' value successfully get debited to the selected Bank Account", "pass");
		} else {
			writeTestResults("Verify 'Deposit Amount' value is debited to the selected Bank Account",
					"'Deposit Amount' value should get debited to the selected Bank Account",
					"'Deposit Amount' value doesn't get debited to the selected Bank Account", "fail");
		}

		String creditBalance = getText(lbl_creditJournelEntryBankDepositCash);

		if (creditBalance.equals("500.000000")) {
			writeTestResults("Verify 'Deposit Amount' value is credited to the Cash in Hand Account",
					"'Deposit Amount' value should get credited to the Cash in Hand Account",
					"'Deposit Amount' value successfully get credited to the Cash in Hand Account", "pass");
		} else {
			writeTestResults("Verify 'Deposit Amount' value is credited to the Cash in Hand Account",
					"'Deposit Amount' value should get credited to the Cash in Hand Account",
					"'Deposit Amount' value doesn't get credited to the Cash in Hand Account", "fail");
		}

		String debTotal = getText(lbl_debitTotalJournelEntry);
		String credTotal = getText(lbl_creditTotalJournelEntry);

		if (debTotal.equals("500.000000") && debTotal.equals(credTotal)) {
			writeTestResults("Verify total debit and credit amounts were balanced",
					"Total debit and credit amounts should balanced",
					"Total debit and credit amounts successfully balanced", "pass");
		} else {
			writeTestResults("Verify total debit and credit amounts were balanced",
					"Total debit and credit amounts should balanced", "Total debit and credit amounts weren't balanced",
					"fail");
		}

		click(btn_closeWindow);
	}

	public void checkDocFlowInboundPayment_FIN_BD_35_36_37() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_bankDeposit, 30);
		if (isDisplayed(btn_bankDeposit)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}

		click(btn_inboundPayment);
		explicitWait(header_inboundPaymentPage, 30);
		if (isDisplayed(header_inboundPaymentPage)) {
			writeTestResults("Verify user can navigate to Inbound Payment by page",
					"User should be navigate to Inbound Payment by page",
					"User successfully navigate to Inbound Payment by page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Inbound Payment by page",
					"User should be navigate to Inbound Payment by page",
					"User doesn't navigate to Inbound Payment by page", "fail");
		}

		explicitWait(lnl_releventInboundPaymentDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerRecipt_FIN_BD_35_36_37")), 40);
		if (!isDisplayedQuickCheck(lnl_releventInboundPaymentDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerRecipt_FIN_BD_35_36_37")))) {
			sendKeys(txt_searchInboundPayment2, getFinObj().readFinanceData("CustomerRecipt_FIN_BD_35_36_37"));
			pressEnter(txt_searchInboundPayment2);
			explicitWait(lnl_releventInboundPaymentDocReplace.replace("doc_no",
					getFinObj().readFinanceData("CustomerRecipt_FIN_BD_35_36_37")), 30);

		}

		click(lnl_releventInboundPaymentDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerRecipt_FIN_BD_35_36_37")));

		explicitWait(btn_action, 30);
		click(btn_action);
		explicitWait(btn_docFlow, 30);
		click(btn_docFlow);

		JavascriptExecutor js = (JavascriptExecutor) driver;
		String headerBankDeposi1 = (String) js.executeScript(properties_docFlow.replace("item", "13"));
		String docNoBankDeposi1 = (String) js.executeScript(properties_docFlow.replace("item", "14"));
		String headerBankDeposi2 = (String) js.executeScript(properties_docFlow.replace("item", "19"));
		String docNoBankDeposi2 = (String) js.executeScript(properties_docFlow.replace("item", "20"));

		if (headerBankDeposi1.equals("Bank Deposit")
				& docNoBankDeposi1.equals(getFinObj().readFinanceData("BankDeposit1_FIN_BD_35_36_37"))
				& headerBankDeposi2.equals("Bank Deposit")
				& docNoBankDeposi2.equals(getFinObj().readFinanceData("BankDeposit2_FIN_BD_35_36_37"))) {
			writeTestResults("Verify both released Bank Deposits are available in the doc flow of the Inbound Payment",
					"Both released Bank Deposits should be available in the doc flow of the Inbound Payment",
					"Both released Bank Deposits successfully available in the doc flow of the Inbound Payment",
					"pass");
		} else {
			writeTestResults("Verify both released Bank Deposits are available in the doc flow of the Inbound Payment",
					"Both released Bank Deposits should be available in the doc flow of the Inbound Payment",
					"Both released Bank Deposits not available in the doc flow of the Inbound Payment", "fail");
		}
	}

	/* FIN_BD_38 */
	public void inboundPaymentAdvice_FIN_BD_38() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_inboundPaymentAdvice, 30);
		if (isDisplayed(btn_inboundPaymentAdvice)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}
		click(btn_inboundPaymentAdvice);
		explicitWait(btn_newInboundPaymentAdvice, 30);
		if (isDisplayed(btn_newInboundPaymentAdvice)) {

			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User successfully navigate to Inbound payment advice by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User could'nt navigate to Inbound payment advice by-page", "fail");
		}

		click(btn_newInboundPaymentAdvice);
		if (isDisplayed(btn_CusAdvanceJourneyIPA)) {

			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User successfully view the list of journey",
					"pass");
		} else {
			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User could'nt view the list of journey",
					"fail");
		}

		click(btn_CusAdvanceJourneyIPA);
		explicitWait(lnl_postBusinessUnitIPA, 30);
		if (isDisplayed(lnl_postBusinessUnitIPA)) {

			writeTestResults("Verify user able to navigate to Customer Advance/ Customer Debit Memo",
					"User should be able to navigate to Customer Advance/ Customer Debit Memo",
					"User successfully navigate to Customer Advance/ Customer Debit Memo", "pass");
		} else {
			writeTestResults("Verify user able to navigate to Customer Advance/ Customer Debit Memo",
					"User should be able to navigate to Customer Advance/ Customer Debit Memo",
					"User doesn't navigate to Customer Advance/ Customer Debit Memo", "fail");
		}

		Thread.sleep(1000);
		click(lnl_postBusinessUnitIPA);
		Thread.sleep(700);
		selectText(drop_postBusinessUnit, postBusinessUnitSch);
		Thread.sleep(700);
		click(tab_subbaryIPA);
		Thread.sleep(2000);
		String get_post_business_unit = getText(lnl_postBusinessUnitIPA);
		System.out.println(get_post_business_unit);
		if (get_post_business_unit.equals(postBusinessUnitSch)) {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User successfully select post business unit from the drop down", "pass");
		} else {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User could'nt select post business unit from the drop down", "fail");
		}

		click(btn_searchCustomerIPA);
		if (isDisplayed(txt_cusAccountSearchIPA)) {
			writeTestResults("Verify user can click on Search button in Customer and Customer Lookup should pop up",
					"User should be click on Search button in Customer and Customer Lookup should pop up",
					"User successfully click on Search button in Customer and Customer Lookup should pop up", "pass");
		} else {

			writeTestResults("Verify user can click on Search button in Customer and Customer Lookup should pop up",
					"User should be click on Search button in Customer and Customer Lookup should pop up",
					"User could'nt click on Search button in Customer and Customer Lookup should pop up", "fail");
		}

		explicitWait(txt_cusAccountSearchIPA, 30);
		sendKeys(txt_cusAccountSearchIPA, customerAccount);
		pressEnter(txt_cusAccountSearchIPA);
		Thread.sleep(1500);
		explicitWait(lnk_resultCustomerAccountIPA, 30);
		if (isDisplayed(lnk_resultCustomerAccountIPA)) {
			writeTestResults("Verify user can search an existing customer by entering Customer name/code",
					"User should be able to search an existing customer by entering Customer name/code",
					"User successfully search an existing customer by entering Customer name/code", "pass");
		} else {

			writeTestResults("Verify user can search an existing customer by entering Customer name/code",
					"User should be able to search an existing customer by entering Customer name/code",
					"User could'nt search an existing customer by entering Customer name/code", "fail");
		}

		doubleClick(lnk_resultCustomerAccountIPA);

		sendKeys(txt_refNoIPA, testAllCharcterWordIPA);

		sendKeys(txt_descIPA, testAllCharcterWordIPA);
		Thread.sleep(2000);

		Select comboBox = new Select(driver.findElement(By.xpath(drop_cusrencyIPA)));
		selectText(drop_cusrencyIPA, currencyLKR);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		if (selectedComboValue.equals(currencyLKR)) {
			writeTestResults("Verify user can choose the currency from the drop down",
					"User should be able to choose the currency from the drop down",
					"User successfully choose the currency from the drop down", "pass");
		} else {

			writeTestResults("Verify user can choose the currency from the drop down",
					"User should be able to choose the currency from the drop down",
					"User could'nt choose the currency from the drop down", "fail");
		}

		sendKeys(txt_amountIPA, amountTwoThousand);
		Thread.sleep(2000);
		String ckeckoutBefore = getText(btn_documentDataIPA);

		click(btn_checkout);
		Thread.sleep(3000);
		String ckeckoutAfter = getText(btn_documentDataIPA);
		if (!ckeckoutBefore.equals(ckeckoutAfter)) {
			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User successfully checkout Inbound Payment Advice", "pass");
		} else {

			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User doesn't checkout Inbound Payment Advice", "fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 40);
		if (isDisplayed(btn_reelese)) {

			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice",
					"User sucessfully draft Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice", "User could'nt draft Inboumd Payment Advice",
					"fail");
		}

		Thread.sleep(3000);
		click(btn_reelese);
		explicitWait(header_releasedInboundPaymentAdvice, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedInboundPaymentAdvice)) {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User sucessfully release Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User could'nt release Inboumd Payment Advice", "fail");
		}
	}

	public void inboundPayment_FIN_BD_38() throws Exception {
		click(btn_actionCRV);

		click(btn_converToInboundPaymentCRV);
		click(btn_detailTabCRV);

		click(btn_summaryTab);

		selectText(drop_paidCurrencyCRV, currencyLKR);

		selectText(drop_paymentMethodCRV, payMethodCash);

		selectText(drop_cashAccount, cashAccount);

		click(btn_detailTabCRV);

		click(btn_detailTabCRV);
		selectText(drop_filterCurrencyCRV, currencyLKR);

		click(chk_firstAdviceOnGridInboundPayment);

		click(btn_checkout);
		Thread.sleep(4000);
		String bannerTotalCRV = getText(btn_documentDataCRV);

		if (!bannerTotalCRV.equals("0")) {
			writeTestResults("Verify user can checkout Cash Customer Receipt",
					"User should be able to checkout the Cash Customer Receipt",
					"User successfully checkout Cash Customer Receipt", "pass");
		} else {

			writeTestResults("Verify user can checkout Cash Customer Receipt",
					"User should be able to checkout the Cash Customer Receipt",
					"User doesn't checkout Cash Customer Receipt", "fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 30);
		if (isDisplayed(btn_reelese)) {
			writeTestResults("Verify user can draft Cash Customer Receipt",
					"User should be able to draft Cash Customer Receipt",
					"User sucessfully draft Cash Customer Receipt", "pass");
		} else {
			writeTestResults("Verify user can draft Cash Customer Receipt",
					"User should be able to draft Cash Customer Receipt", "User could'nt draft Cash Customer Receipt",
					"fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedInboundPayment, 30);
		trackCode = getText(lbl_docNo);

		getFinObj().writeFinanceData("CustomerRecipt_FIN_BD_38", trackCode, 24);
		if (isDisplayed(header_releasedInboundPayment)) {
			writeTestResults("Verify user can release Cash Customer Receipt",
					"User should be able to release Cash Customer Receipt",
					"User sucessfully release Cash Customer Receipt", "pass");
		} else {
			writeTestResults("Verify user can release Cash Customer Receipt",
					"User should be able to release Cash Customer Receipt",
					"User could'nt release Cash Customer Receipt", "fail");
		}

		click(btn_action);
		explicitWait(btn_reverse, 10);
		if (isDisplayed(btn_reverse)) {
			writeTestResults("Verify 'Reverse' action is available in newly released Inbound Payments",
					"'Reverse' action should be available in newly released Inbound Payments",
					"'Reverse' action is available in newly released Inbound Payments", "pass");
		} else {
			writeTestResults("Verify 'Reverse' action is available in newly released Inbound Payments",
					"'Reverse' action should be available in newly released Inbound Payments",
					"'Reverse' action is not available in newly released Inbound Payments", "fail");
		}
	}

	public void bankDeposit_FIN_BD_38() throws Exception {
		((JavascriptExecutor) driver).executeScript("window.open('about:blank','_blank');");
		switchWindow();
		openPage(site_Url);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 30);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_bankDeposit, 30);
		if (isDisplayed(btn_bankDeposit)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}

		click(btn_bankDeposit);
		explicitWait(header_bankDeposit, 30);
		if (isDisplayed(header_bankDeposit)) {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page",
					"User successfully navigate to Bank Deposit by page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page", "User doesn't navigate to Bank Deposit by page",
					"fail");
		}

		click(btn_newBankDeposit);
		explicitWait(header_newBankDeposit, 30);
		if (isDisplayed(header_newBankDeposit)) {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page successfully displayed",
					"pass");
		} else {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page doesn't displayed", "fail");
		}

		selectText(drop_payMethodBankDeposi, payMethodCash);
		String selectedComboValue = getSelectedOptionInDropdown(drop_payMethodBankDeposi);
		if (selectedComboValue.equals(payMethodCash)) {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User successfully select any Pay Method from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User doesn't select any Pay Method from the drop down where the field will populate the selected value",
					"fail");
		}

		sendKeys(txt_descriptinArea, description);
		String descriptionStr = getAttribute(txt_descriptinArea, "value");
		if (descriptionStr.equals(description)) {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User successfully enter characters, numbers, alphanumeric and special characters to Description",
					"pass");
		} else {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User doesn't enter characters, numbers, alphanumeric and special characters to Description",
					"fail");
		}

		selectText(drop_bankNameBD, commercialBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bankNameBD);
		if (selectedBank.equals(commercialBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_accountNo, commercialCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_accountNo);
		if (selectedAccount.equals(commercialCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_viewBankDeposit);
		explicitWait(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerRecipt_FIN_BD_38")), 30);
		if (isDisplayed(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerRecipt_FIN_BD_38")))) {
			writeTestResults("Verify Customer Receipt get loaded in the document list",
					"Customer Receipt should get loaded in the document list",
					"Customer Receipt successfully get loaded in the document list", "pass");
		} else {
			writeTestResults("Verify Customer Receipt get loaded in the document list",
					"Customer Receipt should get loaded in the document list",
					"Customer Receipt doesn't get loaded in the document list", "fail");
		}

		click(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerRecipt_FIN_BD_38")));

		if (isSelected(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerRecipt_FIN_BD_38")))) {
			writeTestResults("Verify user able to select 'Deposit' check box",
					"User should be able to select 'Deposit' check box", "User successfully select 'Deposit' check box",
					"pass");
		} else {
			writeTestResults("Verify user able to select 'Deposit' check box",
					"User should be able to select 'Deposit' check box", "User doesn't select 'Deposit' check box",
					"fail");
		}

		click(btn_draft);
		explicitWait(header_draftedBankDeposit, 30);
		if (isDisplayed(header_draftedBankDeposit)) {
			writeTestResults("Verify user can draft the Bank Deposit", "User should be able to draft the Bank Deposit",
					"User successfully draft the Bank Deposit", "pass");
		} else {
			writeTestResults("Verify user can draft the Bank Deposit", "User should be able to draft the Bank Deposit",
					"User doesn't draft the Bank Deposit", "fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedBankDeposit, 30);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedBankDeposit)) {
			writeTestResults("Verify that Bank Deposit is get released with the deposit amount",
					"Bank Deposit should get released with the deposit amount",
					"Bank Deposit successfully get released with the deposit amount", "pass");
		} else {
			writeTestResults("Verify that Bank Deposit is get released with the deposit amount",
					"Bank Deposit should get released with the deposit amount",
					"Bank Deposit doesn't get released with the deposit amount", "fail");
		}

		switchWindow();

		click(btn_reverse);
		explicitWait(btn_reverseSecond, 10);
		click(btn_reverseSecond);
		explicitWait(btn_yes, 10);
		click(btn_yes);
		explicitWait(para_validatorInboundPaymentAlreadyUsed, 10);
		if (isDisplayed(para_validatorInboundPaymentAlreadyUsed)) {
			writeTestResults(
					"Veriyf that 'Inbound Payment is already used and it cannot be reversed' validation message is displayed when reversed after deposit",
					"'Inbound Payment is already used and it cannot be reversed' validation message should be displayed when reversed after deposit",
					"'Inbound Payment is already used and it cannot be reversed' validation message successfully displayed when reversed after deposit",
					"pass");
		} else {
			writeTestResults(
					"Veriyf that 'Inbound Payment is already used and it cannot be reversed' validation message is displayed when reversed after deposit",
					"'Inbound Payment is already used and it cannot be reversed' validation message should be displayed when reversed after deposit",
					"'Inbound Payment is already used and it cannot be reversed' validation message doesn't displayed when reversed after deposit",
					"fail");
		}
		pageRefersh();

		click(btn_action);
		explicitWait(btn_reverse, 10);
		if (!isDisplayedQuickCheck(btn_reverse)) {
			writeTestResults(
					"Verify that 'Reverse' not available in the 'Action' drop down after refreshing the Inbound Payment document.",
					"'Reverse' should not be available in the 'Action' drop down after refreshing the Inbound Payment document.",
					"'Reverse' not be available in the 'Action' drop down after refreshing the Inbound Payment document.",
					"pass");
		} else {
			writeTestResults(
					"Verify that 'Reverse' not available in the 'Action' drop down after refreshing the Inbound Payment document.",
					"'Reverse' should not be available in the 'Action' drop down after refreshing the Inbound Payment document.",
					"'Reverse' available in the 'Action' drop down after refreshing the Inbound Payment document.",
					"fail");
		}
	}

	/* FIN_BD_39 */
	public void inboundPaymentAdvice_FIN_BD_39() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_inboundPaymentAdvice, 30);
		if (isDisplayed(btn_inboundPaymentAdvice)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}
		click(btn_inboundPaymentAdvice);
		explicitWait(btn_newInboundPaymentAdvice, 30);
		if (isDisplayed(btn_newInboundPaymentAdvice)) {

			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User successfully navigate to Inbound payment advice by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User could'nt navigate to Inbound payment advice by-page", "fail");
		}

		click(btn_newInboundPaymentAdvice);
		if (isDisplayed(btn_CusAdvanceJourneyIPA)) {

			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User successfully view the list of journey",
					"pass");
		} else {
			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User could'nt view the list of journey",
					"fail");
		}

		click(btn_CusAdvanceJourneyIPA);
		explicitWait(lnl_postBusinessUnitIPA, 30);
		if (isDisplayed(lnl_postBusinessUnitIPA)) {

			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User successfully directed to by-page Bank Adjustment", "pass");
		} else {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User doesn't directed to by-page Bank Adjustment", "fail");
		}

		Thread.sleep(1000);
		click(lnl_postBusinessUnitIPA);
		Thread.sleep(700);
		selectText(drop_postBusinessUnit, postBusinessUnitSch);
		Thread.sleep(700);
		click(tab_subbaryIPA);
		Thread.sleep(2000);
		String get_post_business_unit = getText(lnl_postBusinessUnitIPA);
		System.out.println(get_post_business_unit);
		if (get_post_business_unit.equals(postBusinessUnitSch)) {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User successfully select post business unit from the drop down", "pass");
		} else {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User could'nt select post business unit from the drop down", "fail");
		}

		click(btn_searchCustomerIPA);
		if (isDisplayed(txt_cusAccountSearchIPA)) {
			writeTestResults("Verify user can click on Search button in Customer and Customer Lookup should pop up",
					"User should be click on Search button in Customer and Customer Lookup should pop up",
					"User successfully click on Search button in Customer and Customer Lookup should pop up", "pass");
		} else {

			writeTestResults("Verify user can click on Search button in Customer and Customer Lookup should pop up",
					"User should be click on Search button in Customer and Customer Lookup should pop up",
					"User could'nt click on Search button in Customer and Customer Lookup should pop up", "fail");
		}

		explicitWait(txt_cusAccountSearchIPA, 30);
		sendKeys(txt_cusAccountSearchIPA, customerAccount);
		pressEnter(txt_cusAccountSearchIPA);
		Thread.sleep(1500);
		explicitWait(lnk_resultCustomerAccountIPA, 30);
		if (isDisplayed(lnk_resultCustomerAccountIPA)) {
			writeTestResults("Verify user can search an existing customer by entering Customer name/code",
					"User should be able to search an existing customer by entering Customer name/code",
					"User successfully search an existing customer by entering Customer name/code", "pass");
		} else {

			writeTestResults("Verify user can search an existing customer by entering Customer name/code",
					"User should be able to search an existing customer by entering Customer name/code",
					"User could'nt search an existing customer by entering Customer name/code", "fail");
		}

		doubleClick(lnk_resultCustomerAccountIPA);

		sendKeys(txt_refNoIPA, testAllCharcterWordIPA);

		sendKeys(txt_descIPA, testAllCharcterWordIPA);
		Thread.sleep(2000);

		Select comboBox = new Select(driver.findElement(By.xpath(drop_cusrencyIPA)));
		selectText(drop_cusrencyIPA, currencyLKR);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		if (selectedComboValue.equals(currencyLKR)) {
			writeTestResults("Verify user can choose the currency from the drop down",
					"User should be able to choose the currency from the drop down",
					"User successfully choose the currency from the drop down", "pass");
		} else {

			writeTestResults("Verify user can choose the currency from the drop down",
					"User should be able to choose the currency from the drop down",
					"User could'nt choose the currency from the drop down", "fail");
		}

		sendKeys(txt_amountIPA, fiveThousandEightHundred);
		Thread.sleep(2000);
		String ckeckoutBefore = getText(btn_documentDataIPA);

		click(btn_checkout);
		Thread.sleep(3000);
		String ckeckoutAfter = getText(btn_documentDataIPA);
		if (!ckeckoutBefore.equals(ckeckoutAfter)) {
			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User successfully checkout Inbound Payment Advice", "pass");
		} else {

			writeTestResults("Verify user can checkout Inbound Payment Advice",
					"User should be able to checkout the Inbound Payment Advice",
					"User doesn't checkout Inbound Payment Advice", "fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 40);
		if (isDisplayed(btn_reelese)) {

			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice",
					"User sucessfully draft Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice", "User could'nt draft Inboumd Payment Advice",
					"fail");
		}

		Thread.sleep(3000);
		click(btn_reelese);
		explicitWait(header_releasedInboundPaymentAdvice, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedInboundPaymentAdvice)) {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User sucessfully release Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User could'nt release Inboumd Payment Advice", "fail");
		}
	}

	public void inboundPayment_FIN_BD_39() throws Exception {
		click(btn_actionCRV);

		click(btn_converToInboundPaymentCRV);
		click(btn_detailTabCRV);

		click(btn_summaryTab);

		selectText(drop_paidCurrencyCRV, currencyLKR);

		selectText(drop_paymentMethodCRV, payMethodCheque);

		selectText(drop_bankNameInboundPayment, bank);

		sendKeys(txt_checkNoInboundPayment, getInvObj().currentTimeAndDate().replace("/", "").replace(":", ""));

		click(btn_detailTabCRV);

		click(btn_detailTabCRV);
		selectText(drop_filterCurrencyCRV, currencyLKR);

		click(chk_firstAdviceOnGridInboundPayment);

		click(btn_checkout);
		Thread.sleep(4000);
		String bannerTotalCRV = getText(btn_documentDataCRV);

		if (!bannerTotalCRV.equals("0")) {
			writeTestResults("Verify user can checkout Cheque Customer Receipt",
					"User should be able to checkout the Cheque Customer Receipt",
					"User successfully checkout Cheque Customer Receipt", "pass");
		} else {

			writeTestResults("Verify user can checkout Cheque Customer Receipt",
					"User should be able to checkout the Cheque Customer Receipt",
					"User doesn't checkout Cheque Customer Receipt", "fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 30);
		if (isDisplayed(btn_reelese)) {
			writeTestResults("Verify user can draft Cheque Customer Receipt",
					"User should be able to draft Cheque Customer Receipt",
					"User sucessfully draft Cheque Customer Receipt", "pass");
		} else {
			writeTestResults("Verify user can draft Cheque Customer Receipt",
					"User should be able to draft Cheque Customer Receipt",
					"User could'nt draft Cheque Customer Receipt", "fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedInboundPayment, 30);
		trackCode = getText(lbl_docNo);
		getFinObj().writeFinanceData("CustomerReciptCheque_FIN_BD_39", trackCode, 25);
		if (isDisplayed(header_releasedInboundPayment)) {
			writeTestResults("Verify user can release Cheque Customer Receipt",
					"User should be able to release Cheque Customer Receipt",
					"User sucessfully release Cheque Customer Receipt", "pass");
		} else {
			writeTestResults("Verify user can release Cheque Customer Receipt",

					"User should be able to release Cheque Customer Receipt",
					"User could'nt release Cheque Customer Receipt", "fail");
		}
	}

	public void bankDeposit_FIN_BD_39() throws Exception {
		((JavascriptExecutor) driver).executeScript("window.open('about:blank','_blank');");
		switchWindow();
		openPage(site_Url);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 30);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_bankDeposit, 30);
		if (isDisplayed(btn_bankDeposit)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}

		click(btn_bankDeposit);
		explicitWait(header_bankDeposit, 30);
		if (isDisplayed(header_bankDeposit)) {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page",
					"User successfully navigate to Bank Deposit by page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Bank Deposit by page",
					"User should be navigate to Bank Deposit by page", "User doesn't navigate to Bank Deposit by page",
					"fail");
		}

		click(btn_newBankDeposit);
		explicitWait(header_newBankDeposit, 30);
		if (isDisplayed(header_newBankDeposit)) {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page successfully displayed",
					"pass");
		} else {
			writeTestResults("Verify Bank Deposit - New page is displayed",
					"Bank Deposit - New page should be displayed", "Bank Deposit - New page doesn't displayed", "fail");
		}

		selectText(drop_payMethodBankDeposi, payMethodCheque);
		String selectedComboValue = getSelectedOptionInDropdown(drop_payMethodBankDeposi);
		if (selectedComboValue.equals(payMethodCheque)) {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User successfully select any Pay Method from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Pay Method from the drop down where the field will populate the selected value",
					"User should be able to select any Pay Method from the drop down where the field will populate the selected value",
					"User doesn't select any Pay Method from the drop down where the field will populate the selected value",
					"fail");
		}

		sendKeys(txt_descriptinArea, description);
		String descriptionStr = getAttribute(txt_descriptinArea, "value");
		if (descriptionStr.equals(description)) {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User successfully enter characters, numbers, alphanumeric and special characters to Description",
					"pass");
		} else {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User doesn't enter characters, numbers, alphanumeric and special characters to Description",
					"fail");
		}

		selectText(drop_bankNameBD, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bankNameBD);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_accountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_accountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_viewBankDeposit);
		explicitWait(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerReciptCheque_FIN_BD_39")), 30);
		if (isDisplayed(div_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerReciptCheque_FIN_BD_39")))) {
			writeTestResults("Verify Customer Receipt get loaded in the document list",
					"Customer Receipt should get loaded in the document list",
					"Customer Receipt successfully get loaded in the document list", "pass");
		} else {
			writeTestResults("Verify Customer Receipt get loaded in the document list",
					"Customer Receipt should get loaded in the document list",
					"Customer Receipt doesn't get loaded in the document list", "fail");
		}

		click(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerReciptCheque_FIN_BD_39")));

		if (isSelected(chk_paymentDocumentListBankDepositDocReplace.replace("doc_no",
				getFinObj().readFinanceData("CustomerReciptCheque_FIN_BD_39")))) {
			writeTestResults("Verify user able to select 'Deposit' check box",
					"User should be able to select 'Deposit' check box", "User successfully select 'Deposit' check box",
					"pass");
		} else {
			writeTestResults("Verify user able to select 'Deposit' check box",
					"User should be able to select 'Deposit' check box", "User doesn't select 'Deposit' check box",
					"fail");
		}

		click(btn_draft);
		explicitWait(header_draftedBankDeposit, 30);
		if (isDisplayed(header_draftedBankDeposit)) {
			writeTestResults("Verify user can draft the Bank Deposit", "User should be able to draft the Bank Deposit",
					"User successfully draft the Bank Deposit", "pass");
		} else {
			writeTestResults("Verify user can draft the Bank Deposit", "User should be able to draft the Bank Deposit",
					"User doesn't draft the Bank Deposit", "fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedBankDeposit, 30);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedBankDeposit)) {
			writeTestResults("Verify that Bank Deposit is get released with the deposit amount",
					"Bank Deposit should get released with the deposit amount",
					"Bank Deposit successfully get released with the deposit amount", "pass");
		} else {
			writeTestResults("Verify that Bank Deposit is get released with the deposit amount",
					"Bank Deposit should get released with the deposit amount",
					"Bank Deposit doesn't get released with the deposit amount", "fail");
		}

		switchWindow();
		pageRefersh();
		explicitWait(btn_action, 40);
		click(btn_action);
		explicitWait(btn_chequeReturn, 20);
		if (isDisplayed(btn_chequeReturn)) {
			writeTestResults(
					"Verify that 'Cheque Return' option is available in the Inbound Payment after releasing the Bank Deposit",
					"'Cheque Return' option should be available in the Inbound Payment after releasing the Bank Deposit",
					"'Cheque Return' option successfully available in the Inbound Payment after releasing the Bank Deposit",
					"pass");
		} else {
			writeTestResults(
					"Verify that 'Cheque Return' option is available in the Inbound Payment after releasing the Bank Deposit",
					"'Cheque Return' option should be available in the Inbound Payment after releasing the Bank Deposit",
					"'Cheque Return' option doesn't available in the Inbound Payment after releasing the Bank Deposit",
					"fail");
		}

		click(btn_chequeReturn);
		explicitWait(header_checkReturnWindow, 40);
		if (isDisplayed(header_checkReturnWindow)) {
			writeTestResults("Verify cheque return window is appear", "Cheque return window should appear",
					"Cheque return window successfully appear", "pass");
		} else {
			writeTestResults("Verify cheque return window is appear", "Cheque return window should appear",
					"Cheque return window successfully appear", "pass");
		}

		click(txt_returnDate);
		Thread.sleep(1000);
		String basicDay = getInvObj().currentTimeAndDate().substring(8, 10);

		if (basicDay.startsWith("0")) {
			click(btn_day.replace("day", basicDay.substring(1)));
		} else {
			click(btn_day.replace("day", basicDay));
		}

		click(btn_return);

		explicitWait(lbl_docStatusChequeReturn, 50);
		if (isDisplayed(lbl_docStatusChequeReturn)) {
			writeTestResults("Verify user able to perform the Cheque Return",
					"User should be able to perform the Cheque Return", "User successfully perform the Cheque Return",
					"pass");
		} else {
			writeTestResults("Verify user able to perform the Cheque Return",
					"User should be able to perform the Cheque Return", "User doesn't perform the Cheque Return",
					"fail");
		}

		click(btn_action);
		explicitWait(btn_journel, 10);
		click(btn_journel);
		explicitWait(header_journelEntry, 10);
		if (isDisplayed(header_journelEntry)) {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded",
					"Journal Entry details pop up successfully get loaded", "pass");
		} else {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded", "Journal Entry details pop up doesn't get loaded",
					"fail");
		}

		String debitBalanceCustomerReciptIP = getText(
				lbl_debitJournelEntryBankDepositGLReplace.replace("-je", "0").replace("gl", glBank));

		if (debitBalanceCustomerReciptIP.equals("5800.000000")) {
			writeTestResults("Verify 'Return Amount' value is debited to the Relevant Account - Inbound Payment",
					"'Return Amount' value should get debited to the Relevant Account - Inbound Payment",
					"'Return Amount' value successfully get debited to the Relevant Account - Inbound Payment", "pass");
		} else {
			writeTestResults("Verify 'Return Amount' value is debited to the Relevant Account - Inbound Payment",
					"'Return Amount' value should get debited to the Relevant Account - Inbound Payment",
					"'Return Amount' value doesn't get debited to the Relevant Account - Inbound Payment", "fail");
		}

		String creditBalanceCustomerReciptIP = getText(
				lbl_creditJournelEntryBankDepositGLReplace.replace("-je", "0").replace("gl", glCustomerAdvance));
		if (creditBalanceCustomerReciptIP.equals("5800.000000")) {
			writeTestResults("Verify 'Return Amount'value is credited from the selected Bank Account - Inbound Payment",
					"'Return Amount'value should get credited from the selected Bank Account - Inbound Payment",
					"'Return Amount'value successfully get credited from the selected Bank Account - Inbound Payment",
					"pass");
		} else {
			writeTestResults("Verify 'Return Amount'value is credited from the selected Bank Account - Inbound Payment",
					"'Return Amount'value should get credited from the selected Bank Account - Inbound Payment",
					"'Return Amount'value doesn't get credited from the selected Bank Account - Inbound Payment",
					"fail");
		}

		String debitTotalCustomerReciptIP = getText(lbl_debitTotalJournelEntryRecordReplace.replace("-Record", "0"));
		String credTotalCustomerReciptIP = getText(lbl_creditTotalJournelEntryRecordReplace.replace("-Record", "0"));

		if (debitTotalCustomerReciptIP.equals("5800.000000")
				&& credTotalCustomerReciptIP.equals(debitTotalCustomerReciptIP)) {
			writeTestResults("Verify total debit and credit amounts were balanced - Inbound Payment",
					"Total debit and credit amounts should balanced - Inbound Payment",
					"Total debit and credit amounts successfully balanced - Inbound Payment", "pass");
		} else {
			writeTestResults("Verify total debit and credit amounts were balanced - Inbound Payment",
					"Total debit and credit amounts should balanced - Inbound Payment",
					"Total debit and credit amounts weren't balanced - Inbound Payment", "fail");
		}

		switchWindow();
		pageRefersh();
		explicitWait(btn_action, 50);
		click(btn_action);
		explicitWait(btn_journel, 10);
		click(btn_journel);
		explicitWait(header_journelEntry, 10);
		if (isDisplayed(header_journelEntry)) {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded",
					"Journal Entry details pop up successfully get loaded", "pass");
		} else {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded", "Journal Entry details pop up doesn't get loaded",
					"fail");
		}

		String debitBalanceCustomerReciptBD = getText(
				lbl_debitJournelEntryBankDepositGLReplace.replace("-je", "0").replace("gl", glBank));

		if (debitBalanceCustomerReciptBD.equals("5800.000000")) {
			writeTestResults("Verify 'Return Amount' value is debited to the Relevant Account - Bank Deposit",
					"'Return Amount' value should get debited to the Relevant Account - Bank Deposit",
					"'Return Amount' value successfully get debited to the Relevant Account - Bank Deposit", "pass");
		} else {
			writeTestResults("Verify 'Return Amount' value is debited to the Relevant Account - Bank Deposit",
					"'Return Amount' value should get debited to the Relevant Account - Bank Deposit",
					"'Return Amount' value doesn't get debited to the Relevant Account - Bank Deposit", "fail");
		}

		String creditBalanceCustomerRecBDtBD = getText(
				lbl_creditJournelEntryBankDepositGLReplace.replace("-je", "0").replace("gl", glCustomerAdvance));
		if (creditBalanceCustomerRecBDtBD.equals("5800.000000")) {
			writeTestResults("Verify 'Return Amount'value is credited from the selected Bank Account - Bank Deposit",
					"'Return Amount'value should get credited from the selected Bank Account - Bank Deposit",
					"'Return Amount'value successfully get credited from the selected Bank Account - Bank Deposit",
					"pass");
		} else {
			writeTestResults("Verify 'Return Amount'value is credited from the selected Bank Account - Bank Deposit",
					"'Return Amount'value should get credited from the selected Bank Account - Bank Deposit",
					"'Return Amount'value doesn't get credited from the selected Bank Account - Bank Deposit", "fail");
		}

		String debitTotalCustomerRecBDtBD = getText(lbl_debitTotalJournelEntryRecordReplace.replace("-Record", "0"));
		String credTotalCustomerRecBDtBD = getText(lbl_creditTotalJournelEntryRecordReplace.replace("-Record", "0"));

		if (debitTotalCustomerRecBDtBD.equals("5800.000000")
				&& credTotalCustomerRecBDtBD.equals(debitTotalCustomerRecBDtBD)) {
			writeTestResults("Verify total debit and credit amounts were balanced - Bank Deposit",
					"Total debit and credit amounts should balanced - Bank Deposit",
					"Total debit and credit amounts successfully balanced - Bank Deposit", "pass");
		} else {
			writeTestResults("Verify total debit and credit amounts were balanced - Bank Deposit",
					"Total debit and credit amounts should balanced - Bank Deposit",
					"Total debit and credit amounts weren't balanced - Bank Deposit", "fail");
		}

	}

	/* Bank Adjustment */
	/* FIN_BA_1_5_6 */
	public void bankAdjustmentNewPage_FIN_BA_1_5_6() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_bankaAdjustment, 20);
		if (isDisplayed(btn_bankaAdjustment)) {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option successfully available under finance module", "pass");
		} else {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option doesn't available under finance module", "fail");
		}

		if (isEnabled(btn_bankaAdjustment)) {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option", "User able to click on Bank Adjustment option",
					"pass");
		} else {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option",
					"User not able to click on Bank Adjustment option", "fail");
		}

		click(btn_bankaAdjustment);
		explicitWait(header_bankAdjustmentByPage, 35);
		if (isEnabled(header_bankAdjustmentByPage)) {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User successfully directed to by-page Bank Adjustment", "pass");
		} else {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User doesn't directed to by-page Bank Adjustment", "fail");
		}

		click(btn_newBankAdjustment);
		explicitWait(header_newBankAdjustment, 35);
		if (isDisplayed(header_newBankAdjustment)) {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed",
					"Bank Adjustment - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed", "Bank Adjustment - New page doesn't displayed",
					"fail");
		}

		click(btn_draft);
		Thread.sleep(2000);

		if (isDisplayed(lbl_mainValidation)) {
			writeTestResults("Verify main validation is appear", "Main validation should appear",
					"Main validation successfully appear", "pass");
		} else {
			writeTestResults("Verify main validation is appear", "Main validation should appear",
					"Main validation doesn't appear", "fail");
		}

		changeCSS(btn_closeMainValidation, "z-index:-9999");

		Thread.sleep(1000);

		if (isDisplayed(error_msgFieldDropdownBankBankAdjustment)) {
			writeTestResults("Verify manadatory fiald validation message is appear for Bank Field",
					"Manadatory fiald validation message should appear for Bank Field",
					"Manadatory fiald validation message successfully appear for Bank Field", "pass");
		} else {
			writeTestResults("Verify manadatory fiald validation message is appear for Bank Field",
					"Manadatory fiald validation message should appear for Bank Field",
					"Manadatory fiald validation message doesn't appear for Bank Field", "fail");
		}

		if (isDisplayed(error_borderBankAccountNoBankAdjustment)) {
			writeTestResults("Verify error border is appear for Bank Account Field",
					"Error border should appear for Bank Account Field",
					"Error border successfully appear for Bank Account Field", "pass");
		} else {
			writeTestResults("Verify error border is appear for Bank Account Field",
					"Error border should appear for Bank Account Field",
					"Error border doesn't appear for Bank Account Field", "fail");
		}

		if (isDisplayed(error_msgFieldDropdownBankAccountBankAdjustment)) {
			writeTestResults("Verify manadatory fiald validation message is appear for Bank Account Field",
					"Manadatory fiald validation message should appear for Bank Account Field",
					"Manadatory fiald validation message successfully appear for Bank Account Field", "pass");
		} else {
			writeTestResults("Verify manadatory fiald validation message is appear for Bank Account Field",
					"Manadatory fiald validation message should appear for Bank Account Field",
					"Manadatory fiald validation message doesn't appear for Bank Account Field", "fail");
		}

		if (isDisplayed(error_borderCurencyBankAdjustment)) {
			writeTestResults("Verify error border is appear for Currency Field",
					"Error border should appear for Currency Field",
					"Error border successfully appear for Currency Field", "pass");
		} else {
			writeTestResults("Verify error border is appear for Currency Field",
					"Error border should appear for Currency Field", "Error border doesn't appear for Currency Field",
					"fail");
		}

		if (isDisplayed(error_msgFieldCurrencyBankAdjustment)) {
			writeTestResults("Verify manadatory fiald validation message is appear for Currency Field",
					"Manadatory fiald validation message should appear for Currency Field",
					"Manadatory fiald validation message successfully appear for Currency Field", "pass");
		} else {
			writeTestResults("Verify manadatory fiald validation message is appear for Currency Field",
					"Manadatory fiald validation message should appear for Currency Field",
					"Manadatory fiald validation message doesn't appear for Currency Field", "fail");
		}

		if (isDisplayed(error_msgFieldGLBankAdjustment)) {
			writeTestResults("Verify manadatory fiald validation message is appear for GL Account Field",
					"Manadatory fiald validation message should appear for GL Account Field",
					"Manadatory fiald validation message successfully appear for GL Account Field", "pass");
		} else {
			writeTestResults("Verify manadatory fiald validation message is appear for GL Account Field",
					"Manadatory fiald validation message should appear for GL Account Field",
					"Manadatory fiald validation message doesn't appear for GL Account Field", "fail");
		}

		if (isDisplayed(error_borderAmountBankAdjustment)) {
			writeTestResults("Verify error border is appear for Amount Field",
					"Error border should appear for Amount Field", "Error border successfully appear for Amount Field",
					"pass");
		} else {
			writeTestResults("Verify error border is appear for Amount Field",
					"Error border should appear for Amount Field", "Error border doesn't appear for Amount Field",
					"fail");
		}

		if (isDisplayed(error_msgFieldAmountBankAdjustment)) {
			writeTestResults("Verify manadatory fiald validation message is appear for Amount Field",
					"Manadatory fiald validation message should appear for Amount Field",
					"Manadatory fiald validation message successfully appear for Amount Field", "pass");
		} else {
			writeTestResults("Verify manadatory fiald validation message is appear for Amount Field",
					"Manadatory fiald validation message should appear for Amount Field",
					"Manadatory fiald validation message doesn't appear for Amount Field", "fail");
		}

	}

	/* FIN_BA_2 */
	public void configureUserPermissionAndCheckBankAdjustment_FIN_BA_2() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 30);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_bankAdjustment, 10);
		if (isDisplayed(btn_bankAdjustment)) {
			writeTestResults("Verify Bank Adjustment option is displayed", "Bank Adjustment option should displayed",
					"Bank Adjustment option successfully displayed", "pass");
		} else {
			writeTestResults("Verify Bank Adjustment option is displayed", "Bank Adjustment option should displayed",
					"Bank Adjustment option doesn't displayed", "fail");
		}

		((JavascriptExecutor) driver).executeScript("window.open('about:blank','_blank');");
		switchWindow();
		openPage(site_Url);
		explicitWait(navigation_pane, 20);
		Thread.sleep(2000);
		explicitWait(navigation_pane, 20);
		click(navigation_pane);
		explicitWait(btn_adminModule, 20);
		click(btn_adminModule);
		explicitWait(btn_userPermission, 20);
		click(btn_userPermission);

		explicitWait(btn_userLookupUserPermission, 30);
		click(btn_userLookupUserPermission);
		getInvObj().handeledSendKeys(txt_userSearch, logUserEmail);
		pressEnter(txt_userSearch);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", logUserEmail), 30);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", logUserEmail));
		Thread.sleep(2000);

		selectText(drop_moduleNameUserPermission, moduleFinance);

		explicitWait(chk_reverseBankAdjustmentUserPermission, 40);
		if (isSelected(chk_reverseBankAdjustmentUserPermission)) {
			click(chk_reverseBankAdjustmentUserPermission);
		}
		if (isSelected(chk_releaseBankAdjustmentUserPermission)) {
			click(chk_releaseBankAdjustmentUserPermission);
		}
		if (isSelected(chk_viewBankAdjustmentUserPermission)) {
			click(chk_viewBankAdjustmentUserPermission);
		}
		if (isSelected(chk_allowTempBankAdjustmentUserPermission)) {
			click(chk_allowTempBankAdjustmentUserPermission);
		}

		click(btn_update);
		Thread.sleep(2500);

		switchWindow();
		click(btn_bankAdjustment);
		explicitWait(lbl_noPermissionValidation, 50);
		if (isDisplayed(lbl_noPermissionValidation)) {
			writeTestResults(
					"Verify user able to receive error message stating \"You have no permission to access this page. Please grant permission from administrator.\"",
					"User should be able to receive error message stating \"You have no permission to access this page. Please grant permission from administrator.\"",
					"User successfully receive error message stating \"You have no permission to access this page. Please grant permission from administrator.\"",
					"pass");
		} else {
			writeTestResults(
					"Verify user able to receive error message stating \"You have no permission to access this page. Please grant permission from administrator.\"",
					"User should be able to receive error message stating \"You have no permission to access this page. Please grant permission from administrator.\"",
					"User doesn't receive error message stating \"You have no permission to access this page. Please grant permission from administrator.\"",
					"fail");
		}

	}

	public void resetBankAdjustmentUserPermissions_FIN_BA_2() throws Exception {
		switchWindow();
		explicitWait(chk_reverseBankAdjustmentUserPermission, 40);
		if (isSelected(chk_viewBankAdjustmentUserPermission)) {
			click(chk_viewBankAdjustmentUserPermission);
		}
		if (!isSelected(chk_reverseBankAdjustmentUserPermission)) {
			click(chk_reverseBankAdjustmentUserPermission);
		}
		if (!isSelected(chk_releaseBankAdjustmentUserPermission)) {
			click(chk_releaseBankAdjustmentUserPermission);
		}
		if (!isSelected(chk_allowTempBankAdjustmentUserPermission)) {
			click(chk_allowTempBankAdjustmentUserPermission);
		}

		click(btn_update);
		Thread.sleep(2500);
	}

	/* FIN_BA_3 */
	public void checkAllowTemplateOptionUserPermission_FIN_BA_3() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		explicitWait(navigation_pane, 20);
		Thread.sleep(2000);
		explicitWait(navigation_pane, 20);
		click(navigation_pane);
		explicitWait(btn_adminModule, 20);
		click(btn_adminModule);
		explicitWait(btn_userPermission, 20);
		click(btn_userPermission);

		explicitWait(btn_userLookupUserPermission, 30);
		click(btn_userLookupUserPermission);
		getInvObj().handeledSendKeys(txt_userSearch, logUserEmail);
		pressEnter(txt_userSearch);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", logUserEmail), 30);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", logUserEmail));
		Thread.sleep(2000);

		selectText(drop_moduleNameUserPermission, moduleFinance);

		explicitWait(chk_reverseBankAdjustmentUserPermission, 40);

		if (isSelected(chk_allowTempBankAdjustmentUserPermission)) {
			click(chk_allowTempBankAdjustmentUserPermission);
		}
		if (isSelected(chk_viewBankAdjustmentUserPermission)) {
			click(chk_viewBankAdjustmentUserPermission);
		}
		if (!isSelected(chk_reverseBankAdjustmentUserPermission)) {
			click(chk_reverseBankAdjustmentUserPermission);
		}
		if (!isSelected(chk_releaseBankAdjustmentUserPermission)) {
			click(chk_releaseBankAdjustmentUserPermission);
		}

		click(btn_update);
		Thread.sleep(2500);

		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_bankAdjustment, 10);
		click(btn_bankAdjustment);
		explicitWait(drop_templateOnByPage, 50);

		List<String> bankAdjustmetTemplateDropdown = new ArrayList<String>();
		Select dropTemplate = new Select(driver.findElement(By.xpath(drop_templateOnByPage)));
		List<WebElement> op = dropTemplate.getOptions();
		int size = op.size();
		for (int i = 0; i < size; i++) {
			String options = op.get(i).getText();
			bankAdjustmetTemplateDropdown.add(options);
		}

		if (bankAdjustmetTemplateDropdown.contains(myTemplateOption)
				& !bankAdjustmetTemplateDropdown.contains(allOption)) {
			writeTestResults(
					"Verify user able to accessible to \"My Template\" only when \"Allow Template\" is unticked",
					"User should be able to accessible to \"My Template\" only when \"Allow Template\" is unticked",
					"User able to accessible to \"My Template\" only when \"Allow Template\" is unticked", "pass");
		} else {
			writeTestResults(
					"Verify user able to accessible to \"My Template\" only when \"Allow Template\" is unticked",
					"User should be able to accessible to \"My Template\" only when \"Allow Template\" is unticked",
					"User able to accessible to not only \"My Template\" when \"Allow Template\" is unticked", "fail");
		}

		explicitWait(navigation_pane, 20);
		Thread.sleep(2000);
		explicitWait(navigation_pane, 20);
		click(navigation_pane);
		explicitWait(btn_adminModule, 20);
		click(btn_adminModule);
		explicitWait(btn_userPermission, 20);
		click(btn_userPermission);

		explicitWait(btn_userLookupUserPermission, 30);
		click(btn_userLookupUserPermission);
		getInvObj().handeledSendKeys(txt_userSearch, logUserEmail);
		pressEnter(txt_userSearch);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", logUserEmail), 30);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", logUserEmail));
		Thread.sleep(2000);

		selectText(drop_moduleNameUserPermission, moduleFinance);

		explicitWait(chk_reverseBankAdjustmentUserPermission, 40);

		if (!isSelected(chk_allowTempBankAdjustmentUserPermission)) {
			click(chk_allowTempBankAdjustmentUserPermission);
		}

		click(btn_update);
		Thread.sleep(2500);

		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_bankAdjustment, 10);
		click(btn_bankAdjustment);

		explicitWait(drop_templateOnByPage, 50);

		List<String> bankAdjustmetTemplateDropdownRetick = new ArrayList<String>();
		Select dropTemplateRetick = new Select(driver.findElement(By.xpath(drop_templateOnByPage)));
		List<WebElement> opRetick = dropTemplateRetick.getOptions();
		int sizeRetick = opRetick.size();
		for (int i = 0; i < sizeRetick; i++) {
			String options = opRetick.get(i).getText();
			bankAdjustmetTemplateDropdownRetick.add(options);
		}

		if (bankAdjustmetTemplateDropdownRetick.contains(myTemplateOption)
				& bankAdjustmetTemplateDropdownRetick.contains(allOption)) {
			writeTestResults("Verify user able to accessible to \"My Template\" an All \"Allow Template\" is unticked",
					"User should be able to accessible to \"My Template\" an All \"Allow Template\" is ticked",
					"User able to accessible to \"My Template\" an All \"Allow Template\" is ticked", "pass");
		} else {
			writeTestResults("Verify user able to accessible to \"My Template\" an All \"Allow Template\" is ticked",
					"User should be able to accessible to \"My Template\" an All \"Allow Template\" is ticked",
					"User able to accessible to only \"My Template\" when \"Allow Template\" is ticked", "fail");
		}
	}

	/* FIN_BA_4_7_8_9 */
	public void draftReleaseBankAdjustment_FIN_BA_4_7_8_9() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_bankaAdjustment, 20);
		if (isDisplayed(btn_bankaAdjustment)) {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option successfully available under finance module", "pass");
		} else {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option doesn't available under finance module", "fail");
		}

		if (isEnabled(btn_bankaAdjustment)) {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option", "User able to click on Bank Adjustment option",
					"pass");
		} else {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option",
					"User not able to click on Bank Adjustment option", "fail");
		}

		click(btn_bankaAdjustment);
		explicitWait(header_bankAdjustmentByPage, 35);
		if (isEnabled(header_bankAdjustmentByPage)) {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User successfully directed to by-page Bank Adjustment", "pass");
		} else {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User doesn't directed to by-page Bank Adjustment", "fail");
		}

		click(btn_newBankAdjustment);
		explicitWait(header_newBankAdjustment, 35);
		if (isDisplayed(header_newBankAdjustment)) {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed",
					"Bank Adjustment - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed", "Bank Adjustment - New page doesn't displayed",
					"fail");
		}

		selectText(drop_bank, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bank);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_bankAccountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_bankAccountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_lookupGLBankAdjustment);
		getInvObj().handeledSendKeys(txt_GL, glAccountMotorVehicles);
		pressEnter(txt_GL);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles), 30);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles));
		Thread.sleep(2000);

		String gl_submitted = getAttribute(txt_glInFrontPageBankAdjustmentSmoke, "value");
		if (glAccountMotorVehicles.equals(gl_submitted)) {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User sucessfully set gl account fieald", "pass");
		} else {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User couldn't set gl account fieald", "fail");
		}

		sendKeys(txt_amount, tenThousand);

		click(btn_checkout);

		String amount = getAttribute(txt_amount, "value");
		if (amount.equals("10000.000000")) {
			writeTestResults("Verify user can enter the Amount", "User should be able to enter the Amount",
					"User sucessfully enter the Amount", "pass");
		} else {
			writeTestResults("Verify user can enter the Amount", "User should be able to enter the Amount",
					"User couldn't enter the Amount", "fail");
		}

		click(btn_draft);
		explicitWait(header_draftedBankAdjustment, 40);
		if (isDisplayed(header_draftedBankAdjustment)) {
			writeTestResults("Verify status of the Bank Adjustment document get changed to Draft",
					"Status of the Bank Adjustment document should get changed to Draft",
					"Status of the Bank Adjustment document successfully get changed to Draft", "pass");
		} else {
			writeTestResults("Verify status of the Bank Adjustment document get changed to Draft",
					"Status of the Bank Adjustment document should get changed to Draft",
					"Status of the Bank Adjustment document successfully get changed to Draft", "fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedBankAdjustment, 40);
		trackCode = getText(lbl_docNo);
		getFinObj().writeFinanceData("BankAdjustment_FIN_BA_4_7_8_9", trackCode, 28);
		releasedBankAdjustment_FIN_BA_4_7_8_9 = trackCode;
		if (isDisplayed(header_releasedBankAdjustment)) {
			writeTestResults("Verify status of the Bank Adjustment document get changed to Released",
					"Status of the Bank Adjustment document should get changed to Released",
					"Status of the Bank Adjustment document successfully get changed to Released", "pass");
		} else {
			writeTestResults("Verify status of the Bank Adjustment document get changed to Released",
					"Status of the Bank Adjustment document should get changed to Released",
					"Status of the Bank Adjustment document successfully get changed to Released", "fail");
		}

		explicitWait(btn_action, 50);
		click(btn_action);
		explicitWait(btn_journel, 10);
		click(btn_journel);
		explicitWait(header_journelEntry, 10);
		if (isDisplayed(header_journelEntry)) {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded",
					"Journal Entry details pop up successfully get loaded", "pass");
		} else {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded", "Journal Entry details pop up doesn't get loaded",
					"fail");
		}

		String debitBalanceCustomerReciptBD = getText(
				lbl_debitJournelEntryBankDepositGLReplace.replace("-je", "0").replace("gl", glBank));

		if (debitBalanceCustomerReciptBD.equals("10000.000000")) {
			writeTestResults(
					"Verify 'Adjustment Amount' value is debited to the GL account relevent to the selected Bank Account",
					"'Adjustment Amount' value should get debited to the GL account relevent to the selected Bank Account",
					"'Adjustment Amount' value successfully get debited to the GL account relevent to the selected Bank Account",
					"pass");
		} else {
			writeTestResults(
					"Verify 'Adjustment Amount' value is debited to the GL account relevent to the selected Bank Account",
					"'Adjustment Amount' value should get debited to the GL account relevent to the selected Bank Account",
					"'Adjustment Amount' value doesn't get debited to the GL account relevent to the selected Bank Account",
					"fail");
		}

		String creditBalanceCustomerRecBDtBD = getText(
				lbl_creditJournelEntryBankDepositGLReplace.replace("-je", "0").replace("gl", glAccountMotorVehicles));
		if (creditBalanceCustomerRecBDtBD.equals("10000.000000")) {
			writeTestResults("Verify 'Adjustment Amount'value is credited from the selected GL account ",
					"'Adjustment Amount'value should get credited from the selected GL account ",
					"'Adjustment Amount'value successfully get credited from the selected GL account ", "pass");
		} else {
			writeTestResults("Verify 'Adjustment Amount'value is credited from the selected GL account ",
					"'Adjustment Amount'value should get credited from the selected GL account ",
					"'Adjustment Amount'value doesn't get credited from the selected GL account ", "fail");
		}

		String debitTotalCustomerRecBDtBD = getText(lbl_debitTotalJournelEntryRecordReplace.replace("-Record", "0"));
		String credTotalCustomerRecBDtBD = getText(lbl_creditTotalJournelEntryRecordReplace.replace("-Record", "0"));

		if (debitTotalCustomerRecBDtBD.equals("10000.000000")
				&& credTotalCustomerRecBDtBD.equals(debitTotalCustomerRecBDtBD)) {
			writeTestResults("Verify total debit and credit amounts were balanced",
					"Total debit and credit amounts should balanced",
					"Total debit and credit amounts successfully balanced", "pass");
		} else {
			writeTestResults("Verify total debit and credit amounts were balanced",
					"Total debit and credit amounts should balanced", "Total debit and credit amounts weren't balanced",
					"fail");
		}
		click(btn_closeWindow);

		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_bankaAdjustment, 20);
		if (isDisplayed(btn_bankaAdjustment)) {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option successfully available under finance module", "pass");
		} else {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option doesn't available under finance module", "fail");
		}

		if (isEnabled(btn_bankaAdjustment)) {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option", "User able to click on Bank Adjustment option",
					"pass");
		} else {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option",
					"User not able to click on Bank Adjustment option", "fail");
		}

		click(btn_bankaAdjustment);
		explicitWait(header_bankAdjustmentByPage, 35);

		if (isEnabled(header_bankAdjustmentByPage)) {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User successfully directed to by-page Bank Adjustment", "pass");
		} else {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User doesn't directed to by-page Bank Adjustment", "fail");
		}

		sendKeys(txt_searchBankAdjustment, releasedBankAdjustment_FIN_BA_4_7_8_9);
		click(btn_searchOnByPage);
		Thread.sleep(2000);
		explicitWait(lnk_resultDocOnByPage.replace("doc_no", releasedBankAdjustment_FIN_BA_4_7_8_9), 40);
		click(lnk_firstRecordBankAdjustmentByPage);

		explicitWait(header_releasedBankAdjustmentDocReplace.replace("doc_no", releasedBankAdjustment_FIN_BA_4_7_8_9),
				40);
		if (isDisplayed(
				header_releasedBankAdjustmentDocReplace.replace("doc_no", releasedBankAdjustment_FIN_BA_4_7_8_9))) {
			writeTestResults(
					"Verify user able to view the existing list details which are contains specified key word only",
					"User should be able to view the existing list details which are contains specified key word only",
					"User successfully view the existing list details which are contains specified key word only",
					"pass");
		} else {
			writeTestResults(
					"Verify user able to view the existing list details which are contains specified key word only",
					"User should be able to view the existing list details which are contains specified key word only",
					"User doesn't view the existing list details which are contains specified key word only", "fail");
		}

	}

	/* FIN_BA_10 */
	public void bankAdjustment_FIN_BA_10() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_bankaAdjustment, 20);
		if (isDisplayed(btn_bankaAdjustment)) {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option successfully available under finance module", "pass");
		} else {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option doesn't available under finance module", "fail");
		}

		if (isEnabled(btn_bankaAdjustment)) {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option", "User able to click on Bank Adjustment option",
					"pass");
		} else {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option",
					"User not able to click on Bank Adjustment option", "fail");
		}

		click(btn_bankaAdjustment);
		explicitWait(header_bankAdjustmentByPage, 35);
		if (isEnabled(header_bankAdjustmentByPage)) {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User successfully directed to by-page Bank Adjustment", "pass");
		} else {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User doesn't directed to by-page Bank Adjustment", "fail");
		}

		click(btn_newBankAdjustment);
		explicitWait(header_newBankAdjustment, 35);
		if (isDisplayed(header_newBankAdjustment)) {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed",
					"Bank Adjustment - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed", "Bank Adjustment - New page doesn't displayed",
					"fail");
		}

		selectText(drop_bank, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bank);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_bankAccountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_bankAccountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_lookupGLBankAdjustment);
		getInvObj().handeledSendKeys(txt_GL, glAccountMotorVehicles);
		pressEnter(txt_GL);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles), 30);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles));
		Thread.sleep(2000);

		String gl_submitted = getAttribute(txt_glInFrontPageBankAdjustmentSmoke, "value");
		if (glAccountMotorVehicles.equals(gl_submitted)) {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User sucessfully set gl account fieald", "pass");
		} else {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User couldn't set gl account fieald", "fail");
		}

		click(chk_facilitySettlement);

		explicitWait(txt_bankFacilityFrontPageBankAdjustment, 10);
		if (isDisplayed(txt_bankFacilityFrontPageBankAdjustment)) {
			writeTestResults("Verify Bank Facility field is display", "Bank Facility field should display",
					"Bank Facility field successfully display", "pass");
		} else {
			writeTestResults("Verify Bank Facility field is display", "Bank Facility field should display",
					"Bank Facility field doesn't display", "fail");
		}

		if (isEnabled(drop_settlementTypeBankAdjustment)) {
			writeTestResults("Verify that settlement type field is enable", "Settlement type field should be enable",
					"Settlement type field successfully enable", "pass");
		} else {
			writeTestResults("Verify that settlement type field is enable", "Settlement type field should be enable",
					"Settlement type field is disabled", "fail");
		}

		click(btn_lookupBankFacilityOnBankAdjustment);
		getInvObj().handeledSendKeys(txt_bankFacilityAgreementSearch, bankFacilityOriginalDocNumber);
		pressEnter(txt_bankFacilityAgreementSearch);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", bankFacilityOriginalDocNumber), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", bankFacilityOriginalDocNumber));
		Thread.sleep(2000);

		String selectedFacility = getAttribute(txt_bankFacilityFrontPageBankAdjustment, "value");

		if (selectedFacility.equals("000001")) {
			writeTestResults("Verify user able to select a Bank Facility Agreement",
					"User should be able to select a Bank Facility Agreement",
					"User successfully select a Bank Facility Agreement", "pass");
		} else {
			writeTestResults("Verify user able to select a Bank Facility Agreement",
					"User should be able to select a Bank Facility Agreement",
					"User doesn't select a Bank Facility Agreement", "fail");
		}

		selectText(drop_settlementTypeBankAdjustment, setlementTypeBankFacilityOnBankAdjustment);

		sendKeys(txt_amount, tenThousand);

		click(btn_checkout);

		String amount = getAttribute(txt_amount, "value");
		if (amount.equals("10000.000000")) {
			writeTestResults("Verify user can enter the Amount", "User should be able to enter the Amount",
					"User sucessfully enter the Amount", "pass");
		} else {
			writeTestResults("Verify user can enter the Amount", "User should be able to enter the Amount",
					"User couldn't enter the Amount", "fail");
		}

		click(btn_draft);
		explicitWait(header_draftedBankAdjustment, 40);
		if (isDisplayed(header_draftedBankAdjustment)) {
			writeTestResults("Verify user can draft the Bank Adjustment",
					"User should be able to draft the Bank Adjustment", "User successfully draft the Bank Adjustment",
					"pass");
		} else {
			writeTestResults("Verify user can draft the Bank Adjustment",
					"User should be able to draft the Bank Adjustment", "User doesn't draft the Bank Adjustment",
					"fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedBankAdjustment, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedBankAdjustment)) {
			writeTestResults("Verify user can release the Bank Adjustment",
					"User should be able to release the Bank Adjustment",
					"User successfully release the Bank Adjustment", "pass");
		} else {
			writeTestResults("Verify user can release the Bank Adjustment",
					"User should be able to release the Bank Adjustment", "User doesn't release the Bank Adjustment",
					"fail");
		}

	}

	/* FIN_BA_11 */
	public void duplicateBankAdjustment_FIN_BA_11() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		explicitWait(navigation_pane, 30);
		Thread.sleep(2000);
		explicitWait(navigation_pane, 30);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_bankaAdjustment, 20);
		if (isDisplayed(btn_bankaAdjustment)) {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option successfully available under finance module", "pass");
		} else {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option doesn't available under finance module", "fail");
		}

		if (isEnabled(btn_bankaAdjustment)) {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option", "User able to click on Bank Adjustment option",
					"pass");
		} else {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option",
					"User not able to click on Bank Adjustment option", "fail");
		}

		click(btn_bankaAdjustment);
		explicitWait(header_bankAdjustmentByPage, 35);

		if (isEnabled(header_bankAdjustmentByPage)) {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User successfully directed to by-page Bank Adjustment", "pass");
		} else {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User doesn't directed to by-page Bank Adjustment", "fail");
		}

		sendKeys(txt_searchBankAdjustment, getFinObj().readFinanceData("BankAdjustment_FIN_BA_4_7_8_9"));
		click(btn_searchOnByPage);
		Thread.sleep(2000);
		explicitWait(
				lnk_resultDocOnByPage.replace("doc_no", getFinObj().readFinanceData("BankAdjustment_FIN_BA_4_7_8_9")),
				40);
		click(lnk_resultDocOnByPage.replace("doc_no", getFinObj().readFinanceData("BankAdjustment_FIN_BA_4_7_8_9")));

		explicitWait(header_releasedBankAdjustmentDocReplace.replace("doc_no",
				getFinObj().readFinanceData("BankAdjustment_FIN_BA_4_7_8_9")), 50);

		if (isDisplayed(header_releasedBankAdjustmentDocReplace.replace("doc_no",
				getFinObj().readFinanceData("BankAdjustment_FIN_BA_4_7_8_9")))) {
			writeTestResults("Verify able to click on existing Bank Adjustment and is it open",
					"Should be able to click on existing Bank Adjustment and it should open",
					"User successfully click on existing Bank Adjustment and it successfully open", "pass");
		} else {
			writeTestResults("Verify able to click on existing Bank Adjustment and is it open",
					"Should be able to click on existing Bank Adjustment and it should open",
					"User doesn't click on existing Bank Adjustment and/or it doesn't open", "fail");
		}

		if (isEnabled(btn_duplicate)) {
			writeTestResults("Verify user can click on Duplicate button",
					"User should be able to click on Duplicate button", "User able to click on Duplicate button",
					"pass");
		} else {
			writeTestResults("Verify user can click on Duplicate button",
					"User should be able to click on Duplicate button", "User not able to click on Duplicate button",
					"fail");
		}

		click(btn_duplicate);

		explicitWait(drop_bank, 40);
		Thread.sleep(1000);
		String selectedBank = getSelectedOptionInDropdown(drop_bank);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults("Verify selected Bank is remain same as original document",
					"Selected Bank should remain same as original document",
					"Selected Bank successfully remain same as original document", "pass");
		} else {
			writeTestResults("Verify selected Bank is remain same as original document",
					"Selected Bank should remain same as original document",
					"Selected Bank doesn't remain same as original document", "fail");
		}

		String selectedAccount = getSelectedOptionInDropdown(drop_bankAccountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults("Verify selected Account No. is remain same as original document",
					"Selected Account No. should remain same as original document",
					"Selected Account No. successfully remain same as original document", "pass");
		} else {
			writeTestResults("Verify selected Account No. is remain same as original document",
					"Selected Account No. should remain same as original document",
					"Selected Account No. doesn't remain same as original document", "fail");
		}

		String gl_submitted = getAttribute(txt_glInFrontPageBankAdjustmentSmoke, "value");
		if (glAccountMotorVehiclesDuplicatedBankAdjustment.equals(gl_submitted)) {
			writeTestResults("Verify selected GL Account is remain same as original document",
					"Selected GL Account should remain same as original document",
					"Selected GL Account successfully remain same as original document", "pass");
		} else {
			writeTestResults("Verify selected GL Account is remain same as original document",
					"Selected GL Account should remain same as original document",
					"Selected GL Account doesn't remain same as original document", "fail");
		}

		String amount = getAttribute(txt_amount, "value");
		if (amount.equals("10000.000000")) {
			writeTestResults("Verify entered Amount is remain same as original document",
					"entered Amount should remain same as original document",
					"entered Amount successfully remain same as original document", "pass");
		} else {
			writeTestResults("Verify entered Amount is remain same as original document",
					"entered Amount should remain same as original document",
					"entered Amount doesn't remain same as original document", "fail");
		}

		click(btn_draft);
		explicitWait(header_draftedBankAdjustment, 40);
		if (isDisplayed(header_draftedBankAdjustment)) {
			writeTestResults("Verify user can draft the Bank Adjustment",
					"User should be able to draft the Bank Adjustment", "User successfully draft the Bank Adjustment",
					"pass");
		} else {
			writeTestResults("Verify user can draft the Bank Adjustment",
					"User should be able to draft the Bank Adjustment", "User doesn't draft the Bank Adjustment",
					"fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedBankAdjustment, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedBankAdjustment)) {
			writeTestResults("Verify user can release the Bank Adjustment",
					"User should be able to release the Bank Adjustment",
					"User successfully release the Bank Adjustment", "pass");
		} else {
			writeTestResults("Verify user can release the Bank Adjustment",
					"User should be able to release the Bank Adjustment", "User doesn't release the Bank Adjustment",
					"fail");
		}
	}

	/* FIN_BA_12 */
	public void draftAndNewFunction_FIN_BA_12() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		explicitWait(navigation_pane, 30);
		Thread.sleep(2000);
		explicitWait(navigation_pane, 30);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_bankaAdjustment, 20);
		if (isDisplayed(btn_bankaAdjustment)) {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option successfully available under finance module", "pass");
		} else {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option doesn't available under finance module", "fail");
		}

		if (isEnabled(btn_bankaAdjustment)) {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option", "User able to click on Bank Adjustment option",
					"pass");
		} else {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option",
					"User not able to click on Bank Adjustment option", "fail");
		}

		click(btn_bankaAdjustment);
		explicitWait(header_bankAdjustmentByPage, 35);
		if (isEnabled(header_bankAdjustmentByPage)) {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User successfully directed to by-page Bank Adjustment", "pass");
		} else {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User doesn't directed to by-page Bank Adjustment", "fail");
		}

		click(btn_newBankAdjustment);
		explicitWait(header_newBankAdjustment, 35);
		if (isDisplayed(header_newBankAdjustment)) {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed",
					"Bank Adjustment - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed", "Bank Adjustment - New page doesn't displayed",
					"fail");
		}

		selectText(drop_bank, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bank);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_bankAccountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_bankAccountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		String description = getInvObj().currentTimeAndDate().replace("/", "").replace(":", "");
		sendKeys(txt_descriptinArea, description);

		click(btn_lookupGLBankAdjustment);
		getInvObj().handeledSendKeys(txt_GL, glAccountMotorVehicles);
		pressEnter(txt_GL);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles), 30);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles));
		Thread.sleep(2000);

		String gl_submitted = getAttribute(txt_glInFrontPageBankAdjustmentSmoke, "value");
		if (glAccountMotorVehicles.equals(gl_submitted)) {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User sucessfully set gl account fieald", "pass");
		} else {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User couldn't set gl account fieald", "fail");
		}

		sendKeys(txt_amount, tenThousand);

		click(btn_checkout);

		String amount = getAttribute(txt_amount, "value");
		if (amount.equals("10000.000000")) {
			writeTestResults("Verify user can enter the Amount", "User should be able to enter the Amount",
					"User sucessfully enter the Amount", "pass");
		} else {
			writeTestResults("Verify user can enter the Amount", "User should be able to enter the Amount",
					"User couldn't enter the Amount", "fail");
		}

		click(btn_draftAndNew);
		explicitWait(header_newBankAdjustment, 40);
		if (isDisplayed(header_newBankAdjustment)) {
			writeTestResults("Verify user able to navigate to new Bank Adjustment",
					"User should be able to navigate to new Bank Adjustment",
					"User successfully navigate to new Bank Adjustment", "pass");
		} else {
			writeTestResults("Verify user able to navigate to new Bank Adjustment",
					"User should be able to navigate to new Bank Adjustment",
					"User doesn't navigate to new Bank Adjustment", "fail");
		}

		((JavascriptExecutor) driver).executeScript("window.open('about:blank','_blank');");
		switchWindow();
		openPage(site_Url);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 30);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_bankAdjustment, 30);
		if (isDisplayed(btn_bankAdjustment)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}

		click(btn_bankAdjustment);
		explicitWait(header_bankAdjustmentByPage, 30);
		if (isDisplayed(header_bankAdjustmentByPage)) {
			writeTestResults("Verify user can navigate to Bank Adjustment by page",
					"User should be navigate to Bank Adjustment by page",
					"User successfully navigate to Bank Adjustment by page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Bank Adjustment by page",
					"User should be navigate to Bank Adjustment by page",
					"User doesn't navigate to Bank Adjustment by page", "fail");
		}

		click(lbl_recentlyDraftedBankAdjustment);

		explicitWait(txt_descriptionDraftedBankAdjustment, 30);

		String fondedDescription = getAttribute(txt_descriptionDraftedBankAdjustment, "value");

		trackCode = getText(lbl_docNo);

		String firstBankAdjustment = trackCode;
		if (fondedDescription.equals(description)) {
			writeTestResults(
					"Verify user able to temporarily saves the document and found the relevent drafted Bank Adjustment",
					"User should be able to temporarily saves the document and found the relevent drafted Bank Adjustment",
					"User successfully temporarily saves the document and found the relevent drafted Bank Adjustment",
					"pass");
		} else {
			writeTestResults(
					"Verify user able to temporarily saves the document and found the relevent drafted Bank Adjustment",
					"User should be able to temporarily saves the document and found the relevent drafted Bank Adjustment",
					"User doesn't temporarily saves the document and/or not found the relevent drafted Bank Adjustment",
					"fail");
		}

		switchWindow();
		selectText(drop_bank, sampathBank);
		String selectedBank2 = getSelectedOptionInDropdown(drop_bank);
		if (selectedBank2.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_bankAccountNo, sampathCurrentAccount);
		String selectedAccount2 = getSelectedOptionInDropdown(drop_bankAccountNo);
		if (selectedAccount2.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		String description2 = getInvObj().currentTimeAndDate().replace("/", "").replace(":", "");
		sendKeys(txt_descriptinArea, description2);

		click(btn_lookupGLBankAdjustment);
		getInvObj().handeledSendKeys(txt_GL, glAccountMotorVehicles);
		pressEnter(txt_GL);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles), 30);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles));
		Thread.sleep(2000);

		String gl_submitted2 = getAttribute(txt_glInFrontPageBankAdjustmentSmoke, "value");
		if (glAccountMotorVehicles.equals(gl_submitted2)) {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User sucessfully set gl account fieald", "pass");
		} else {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User couldn't set gl account fieald", "fail");
		}

		sendKeys(txt_amount, tenThousand);

		click(btn_checkout);

		String amount2 = getAttribute(txt_amount, "value");
		if (amount2.equals("10000.000000")) {
			writeTestResults("Verify user can enter the Amount", "User should be able to enter the Amount",
					"User sucessfully enter the Amount", "pass");
		} else {
			writeTestResults("Verify user can enter the Amount", "User should be able to enter the Amount",
					"User couldn't enter the Amount", "fail");
		}

		click(btn_draft);
		explicitWait(header_draftedBankAdjustment, 40);

		trackCode = getText(lbl_docNo);

		String secondBankAdjustment = trackCode;
		if (isDisplayed(header_draftedBankAdjustment)) {
			writeTestResults("Verify user can draft the Bank Adjustment",
					"User should be able to draft the Bank Adjustment", "User successfully draft the Bank Adjustment",
					"pass");
		} else {
			writeTestResults("Verify user can draft the Bank Adjustment",
					"User should be able to draft the Bank Adjustment", "User doesn't draft the Bank Adjustment",
					"fail");
		}

		switchWindow();
		goBack();

		explicitWait(txt_searchBankAdjustment, 30);
		sendKeys(txt_searchBankAdjustment, firstBankAdjustment);
		pressEnter(txt_searchBankAdjustment);
		Thread.sleep(2000);
		explicitWait(lnl_releventDraftedBankAdjustmentDocReplace.replace("doc_no", firstBankAdjustment), 40);

		if (isDisplayed(lnl_releventDraftedBankAdjustmentDocReplace.replace("doc_no", firstBankAdjustment))) {
			writeTestResults("Verify user able to find first Bank Adjustmentin the Bank Adjustment by page",
					"User should be able to find first Bank Adjustmentin the Bank Adjustment by page",
					"User successfully find first Bank Adjustmentin the Bank Adjustment by page", "pass");
		} else {
			writeTestResults("Verify user able to find first Bank Adjustmentin the Bank Adjustment by page",
					"User should be able to find first Bank Adjustmentin the Bank Adjustment by page",
					"User doesn't find first Bank Adjustmentin the Bank Adjustment by page", "fail");
		}

		sendKeys(txt_searchBankAdjustment, secondBankAdjustment);
		pressEnter(txt_searchBankAdjustment);
		Thread.sleep(2000);
		explicitWait(lnl_releventDraftedBankAdjustmentDocReplace.replace("doc_no", secondBankAdjustment), 40);

		if (isDisplayed(lnl_releventDraftedBankAdjustmentDocReplace.replace("doc_no", secondBankAdjustment))) {
			writeTestResults("Verify user able to find second Bank Adjustmentin the Bank Adjustment by page",
					"User should be able to find second Bank Adjustmentin the Bank Adjustment by page",
					"User successfully find second Bank Adjustmentin the Bank Adjustment by page", "pass");
		} else {
			writeTestResults("Verify user able to find second Bank Adjustmentin the Bank Adjustment by page",
					"User should be able to find second Bank Adjustmentin the Bank Adjustment by page",
					"User doesn't find second Bank Adjustmentin the Bank Adjustment by page", "fail");
		}
	}

	/* FIN_BA_13 */
	public void copyFromFunctionBankAdjustment_FIN_BA_13() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_bankaAdjustment, 20);
		if (isDisplayed(btn_bankaAdjustment)) {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option successfully available under finance module", "pass");
		} else {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option doesn't available under finance module", "fail");
		}

		if (isEnabled(btn_bankaAdjustment)) {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option", "User able to click on Bank Adjustment option",
					"pass");
		} else {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option",
					"User not able to click on Bank Adjustment option", "fail");
		}

		click(btn_bankaAdjustment);
		explicitWait(header_bankAdjustmentByPage, 35);
		if (isEnabled(header_bankAdjustmentByPage)) {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User successfully directed to by-page Bank Adjustment", "pass");
		} else {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User doesn't directed to by-page Bank Adjustment", "fail");
		}

		click(btn_newBankAdjustment);
		explicitWait(header_newBankAdjustment, 35);
		if (isDisplayed(header_newBankAdjustment)) {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed",
					"Bank Adjustment - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed", "Bank Adjustment - New page doesn't displayed",
					"fail");
		}

		click(btn_copyFrom);
		Thread.sleep(1000);
		explicitWait(header_loockupBankAdjustment, 40);
		if (isDisplayed(header_loockupBankAdjustment)) {
			writeTestResults("Verify user able to view Bank Adjustment lookup",
					"User should be able to view Bank Adjustment lookup",
					"User successfully view Bank Adjustment lookup", "pass");
		} else {
			writeTestResults("Verify user able to view Bank Adjustment lookup",
					"User should be able to view Bank Adjustment lookup", "User doesn't view Bank Adjustment lookup",
					"fail");
		}

		sendKeys(txt_searchBankAdjustment, getFinObj().readFinanceData("BankAdjustment_FIN_BA_4_7_8_9"));
		pressEnter(txt_searchBankAdjustment);

		explicitWait(lnk_inforOnLookupInformationReplace.replace("info",
				getFinObj().readFinanceData("BankAdjustment_FIN_BA_4_7_8_9")), 15);

		if (isDisplayed(lnk_inforOnLookupInformationReplace.replace("info",
				getFinObj().readFinanceData("BankAdjustment_FIN_BA_4_7_8_9")))) {
			writeTestResults(
					"Verify relevent existing Bank Adjustment was loaded and user allow to select the particular Bank Adjustment",
					"Relevent existing Bank Adjustment should be loaded and user should allow to select the particular Bank Adjustment",
					"Relevent existing Bank Adjustment successfully loaded and user allow to select the particular Bank Adjustment",
					"pass");
		} else {
			writeTestResults(
					"Verify relevent existing Bank Adjustment was loaded and user allow to select the particular Bank Adjustment",
					"Relevent existing Bank Adjustment should be loaded and user should allow to select the particular Bank Adjustment",
					"Relevent existing Bank Adjustment doesn't loaded and user not allow to select the particular Bank Adjustment",
					"fail");
		}

		doubleClick(lnk_inforOnLookupInformationReplace.replace("info",
				getFinObj().readFinanceData("BankAdjustment_FIN_BA_4_7_8_9")));

		explicitWait(drop_bank, 40);
		Thread.sleep(1000);
		String selectedBank = getSelectedOptionInDropdown(drop_bank);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults("Verify selected Bank is filled same as original document",
					"Selected Bank should filled same as original document",
					"Selected Bank successfully filled same as original document", "pass");
		} else {
			writeTestResults("Verify selected Bank is filled same as original document",
					"Selected Bank should filled same as original document",
					"Selected Bank doesn't filled same as original document", "fail");
		}

		String selectedAccount = getSelectedOptionInDropdown(drop_bankAccountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults("Verify selected Account No. is filled same as original document",
					"Selected Account No. should filled same as original document",
					"Selected Account No. successfully filled same as original document", "pass");
		} else {
			writeTestResults("Verify selected Account No. is filled same as original document",
					"Selected Account No. should filled same as original document",
					"Selected Account No. doesn't filled same as original document", "fail");
		}

		String gl_submitted = getAttribute(txt_glInFrontPageBankAdjustmentSmoke, "value");
		if (glAccountMotorVehiclesDuplicatedBankAdjustment.equals(gl_submitted)) {
			writeTestResults("Verify selected GL Account is filled same as original document",
					"Selected GL Account should filled same as original document",
					"Selected GL Account successfully filled same as original document", "pass");
		} else {
			writeTestResults("Verify selected GL Account is filled same as original document",
					"Selected GL Account should filled same as original document",
					"Selected GL Account doesn't filled same as original document", "fail");
		}

		String amount = getAttribute(txt_amount, "value");
		if (amount.equals("10000.000000")) {
			writeTestResults("Verify entered Amount is filled same as original document",
					"entered Amount should filled same as original document",
					"entered Amount successfully filled same as original document", "pass");
		} else {
			writeTestResults("Verify entered Amount is filled same as original document",
					"entered Amount should filled same as original document",
					"entered Amount doesn't filled same as original document", "fail");
		}
	}

	/* FIN_BA_14 */
	public void checkInactiveGLWithBankAdjustment_FIN_BA_14() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		explicitWait(navigation_pane, 30);
		Thread.sleep(2000);
		explicitWait(navigation_pane, 20);
		click(navigation_pane);

		explicitWait(btn_financeModule, 30);

		click(btn_financeModule);

		explicitWait(btn_chartOfAccount, 10);
		click(btn_chartOfAccount);

		explicitWait(btn_editChartOfAccount, 40);
		click(btn_edit);

		Thread.sleep(2000);
		explicitWait(btn_expandInventory, 30);
		click(btn_expandInventory);

		explicitWait(btn_expandPropertyPlantAndEquipment, 10);
		click(btn_expandPropertyPlantAndEquipment);

		explicitWait(btn_editLandGLAccount, 10);
		click(btn_editLandGLAccount);

		explicitWait(btn_accountStatusGL, 15);
		click(btn_accountStatusGL);

		explicitWait(drop_glAccountStatus, 15);
		selectText(drop_glAccountStatus, statusInactive);

		click(btn_applyGlAccountStatus);

		click(btn_updateLast);
		Thread.sleep(2000);

		((JavascriptExecutor) driver).executeScript("window.open('about:blank','_blank');");
		switchWindow();
		openPage(site_Url);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 30);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_bankAdjustment, 30);
		if (isDisplayed(btn_bankAdjustment)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}

		click(btn_bankAdjustment);
		explicitWait(header_bankAdjustmentByPage, 30);
		if (isDisplayed(header_bankAdjustmentByPage)) {
			writeTestResults("Verify user can navigate to Bank Adjustment by page",
					"User should be navigate to Bank Adjustment by page",
					"User successfully navigate to Bank Adjustment by page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Bank Adjustment by page",
					"User should be navigate to Bank Adjustment by page",
					"User doesn't navigate to Bank Adjustment by page", "fail");
		}

		click(btn_newBankAdjustment);
		explicitWait(header_newBankAdjustment, 35);
		if (isDisplayed(header_newBankAdjustment)) {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed",
					"Bank Adjustment - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed", "Bank Adjustment - New page doesn't displayed",
					"fail");
		}

		selectText(drop_bank, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bank);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_bankAccountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_bankAccountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_lookupGLBankAdjustment);
		explicitWait(txt_GL, 20);
		if (isDisplayed(txt_GL)) {
			writeTestResults("Verify that GL account lookup is displayed", "GL account lookup should be displayed",
					"GL account lookup successfully displayed", "pass");
		} else {
			writeTestResults("Verify that GL account lookup is displayed", "GL account lookup should be displayed",
					"GL account lookup doesn't displayed", "fail");
		}

		getInvObj().handeledSendKeys(txt_GL, glLand);
		pressEnter(txt_GL);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", glLand), 5);
		if (!isDisplayedQuickCheck(lnk_inforOnLookupInformationReplace.replace("info", glLand))) {
			writeTestResults("Verify that inactive GL Accounts were not get searched",
					"Inactive GL Accounts should not get searched",
					"Inactive GL Accounts successfully not get searched", "pass");
		} else {
			writeTestResults("Verify that inactive GL Accounts were not get searched",
					"Inactive GL Accounts should not get searched", "Inactive GL Accounts get searched", "fail");
		}

		switchWindow();
		explicitWait(btn_editLandGLAccount, 10);
		click(btn_editLandGLAccount);

		explicitWait(btn_accountStatusGL, 15);
		click(btn_accountStatusGL);

		explicitWait(drop_glAccountStatus, 15);
		selectText(drop_glAccountStatus, statusActive);

		click(btn_applyGlAccountStatus);

		click(btn_updateLast);
		Thread.sleep(2000);
	}

	/* FIN_BA_16 */
	public void reverseBankAdjustment_FIN_BA_16() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_bankaAdjustment, 20);
		if (isDisplayed(btn_bankaAdjustment)) {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option successfully available under finance module", "pass");
		} else {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option doesn't available under finance module", "fail");
		}

		if (isEnabled(btn_bankaAdjustment)) {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option", "User able to click on Bank Adjustment option",
					"pass");
		} else {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option",
					"User not able to click on Bank Adjustment option", "fail");
		}

		click(btn_bankaAdjustment);
		explicitWait(header_bankAdjustmentByPage, 35);
		if (isEnabled(header_bankAdjustmentByPage)) {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User successfully directed to by-page Bank Adjustment", "pass");
		} else {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User doesn't directed to by-page Bank Adjustment", "fail");
		}

		click(btn_newBankAdjustment);
		explicitWait(header_newBankAdjustment, 35);
		if (isDisplayed(header_newBankAdjustment)) {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed",
					"Bank Adjustment - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed", "Bank Adjustment - New page doesn't displayed",
					"fail");
		}

		selectText(drop_bank, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bank);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_bankAccountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_bankAccountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_lookupGLBankAdjustment);
		getInvObj().handeledSendKeys(txt_GL, glAccountMotorVehicles);
		pressEnter(txt_GL);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles), 30);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles));
		Thread.sleep(2000);

		String gl_submitted = getAttribute(txt_glInFrontPageBankAdjustmentSmoke, "value");
		if (glAccountMotorVehicles.equals(gl_submitted)) {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User sucessfully set gl account fieald", "pass");
		} else {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User couldn't set gl account fieald", "fail");
		}

		sendKeys(txt_amount, tenThousand);

		click(btn_checkout);

		String amount = getAttribute(txt_amount, "value");
		if (amount.equals("10000.000000")) {
			writeTestResults("Verify user can enter the Amount", "User should be able to enter the Amount",
					"User sucessfully enter the Amount", "pass");
		} else {
			writeTestResults("Verify user can enter the Amount", "User should be able to enter the Amount",
					"User couldn't enter the Amount", "fail");
		}

		click(btn_draft);
		explicitWait(header_draftedBankAdjustment, 40);
		if (isDisplayed(header_draftedBankAdjustment)) {
			writeTestResults("Verify status of the Bank Adjustment document get changed to Draft",
					"Status of the Bank Adjustment document should get changed to Draft",
					"Status of the Bank Adjustment document successfully get changed to Draft", "pass");
		} else {
			writeTestResults("Verify status of the Bank Adjustment document get changed to Draft",
					"Status of the Bank Adjustment document should get changed to Draft",
					"Status of the Bank Adjustment document successfully get changed to Draft", "fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedBankAdjustment, 40);
		trackCode = getText(lbl_docNo);
		getFinObj().writeFinanceData("BankAdjustment_FIN_BA_4_7_8_9", trackCode, 28);
		releasedBankAdjustment_FIN_BA_4_7_8_9 = trackCode;
		if (isDisplayed(header_releasedBankAdjustment)) {
			writeTestResults("Verify status of the Bank Adjustment document get changed to Released",
					"Status of the Bank Adjustment document should get changed to Released",
					"Status of the Bank Adjustment document successfully get changed to Released", "pass");
		} else {
			writeTestResults("Verify status of the Bank Adjustment document get changed to Released",
					"Status of the Bank Adjustment document should get changed to Released",
					"Status of the Bank Adjustment document successfully get changed to Released", "fail");
		}

		click(btn_action);
		explicitWait(btn_reverse, 10);
		if (isDisplayed(btn_reverse)) {
			writeTestResults("Verify action menu is displayed", "Action menu should be displayed",
					"Action menu successfully displayed", "pass");
		} else {
			writeTestResults("Verify action menu is displayed", "Action menu should be displayed",
					"Action menu doesn't displayed", "fail");
		}

		click(btn_reverse);
		explicitWait(header_reverseDatePopup, 10);
		if (isDisplayed(header_reverseDatePopup)) {
			writeTestResults("Verify Reverse Date pop up was opened", "Reverse Date pop up should get opened",
					"Reverse Date pop up successfully opened", "pass");
		} else {
			writeTestResults("Verify Reverse Date pop up was opened", "Reverse Date pop up should get opened",
					"Reverse Date pop up doesn't opened", "fail");
		}

		click(btn_reverseSecond);
		explicitWait(btn_yes, 10);
		click(btn_yes);
		explicitWait(header_reversedBankAdjustment, 10);
		if (isDisplayed(header_reversedBankAdjustment)) {
			writeTestResults("Verify Bank Adjustment header is changed as Reversed",
					"Bank Adjustment header should be change as Reversed",
					"Bank Adjustment header successfully change as Reversed", "pass");
		} else {
			writeTestResults("Verify Bank Adjustment header is changed as Reversed",
					"Bank Adjustment header should be change as Reversed",
					"Bank Adjustment header doesn't change as Reversed", "fail");
		}

	}

	/* FIN_BA_18_19 */
	public void plusAmountAndHighAmountValidationCapitalSettlement_FIN_BA_18_19() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_bankaAdjustment, 20);
		if (isDisplayed(btn_bankaAdjustment)) {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option successfully available under finance module", "pass");
		} else {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option doesn't available under finance module", "fail");
		}

		if (isEnabled(btn_bankaAdjustment)) {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option", "User able to click on Bank Adjustment option",
					"pass");
		} else {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option",
					"User not able to click on Bank Adjustment option", "fail");
		}

		click(btn_bankaAdjustment);
		explicitWait(header_bankAdjustmentByPage, 35);
		if (isEnabled(header_bankAdjustmentByPage)) {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User successfully directed to by-page Bank Adjustment", "pass");
		} else {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User doesn't directed to by-page Bank Adjustment", "fail");
		}

		click(btn_newBankAdjustment);
		explicitWait(header_newBankAdjustment, 35);
		if (isDisplayed(header_newBankAdjustment)) {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed",
					"Bank Adjustment - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed", "Bank Adjustment - New page doesn't displayed",
					"fail");
		}

		selectText(drop_bank, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bank);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_bankAccountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_bankAccountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_lookupGLBankAdjustment);
		getInvObj().handeledSendKeys(txt_GL, glAccountMotorVehicles);
		pressEnter(txt_GL);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles), 30);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles));
		Thread.sleep(2000);

		String gl_submitted = getAttribute(txt_glInFrontPageBankAdjustmentSmoke, "value");
		if (glAccountMotorVehicles.equals(gl_submitted)) {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User sucessfully set gl account fieald", "pass");
		} else {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User couldn't set gl account fieald", "fail");
		}

		click(chk_facilitySettlement);

		explicitWait(txt_bankFacilityFrontPageBankAdjustment, 10);
		if (isDisplayed(txt_bankFacilityFrontPageBankAdjustment)) {
			writeTestResults("Verify Bank Facility field is display", "Bank Facility field should display",
					"Bank Facility field successfully display", "pass");
		} else {
			writeTestResults("Verify Bank Facility field is display", "Bank Facility field should display",
					"Bank Facility field doesn't display", "fail");
		}

		if (isEnabled(drop_settlementTypeBankAdjustment)) {
			writeTestResults("Verify that settlement type field is enable", "Settlement type field should be enable",
					"Settlement type field successfully enable", "pass");
		} else {
			writeTestResults("Verify that settlement type field is enable", "Settlement type field should be enable",
					"Settlement type field is disabled", "fail");
		}

		click(btn_lookupBankFacilityOnBankAdjustment);
		getInvObj().handeledSendKeys(txt_bankFacilityAgreementSearch, bankFacilityOriginalDocNumber);
		pressEnter(txt_bankFacilityAgreementSearch);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", bankFacilityOriginalDocNumber), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", bankFacilityOriginalDocNumber));
		Thread.sleep(2000);

		String selectedFacility = getAttribute(txt_bankFacilityFrontPageBankAdjustment, "value");

		if (selectedFacility.equals("000001")) {
			writeTestResults("Verify user able to select a Bank Facility Agreement",
					"User should be able to select a Bank Facility Agreement",
					"User successfully select a Bank Facility Agreement", "pass");
		} else {
			writeTestResults("Verify user able to select a Bank Facility Agreement",
					"User should be able to select a Bank Facility Agreement",
					"User doesn't select a Bank Facility Agreement", "fail");
		}

		selectText(drop_settlementTypeBankAdjustment, setlementTypeCapital);
		String setlementType = getSelectedOptionInDropdown(drop_settlementTypeBankAdjustment);

		if (setlementType.equals(setlementTypeCapital)) {
			writeTestResults("Verify user able to select Capital as settlement type",
					"User should be able to select Capital as settlement type",
					"User successfully select Capital as settlement type", "pass");
		} else {
			writeTestResults("Verify user able to select Capital as settlement type",
					"User should be able to select Capital as settlement type",
					"User doesn't select Capital as settlement type", "fail");
		}

		sendKeys(txt_amount, tenThousand);

		click(drop_bank);

		Thread.sleep(1000);
		if (isDisplayed(txt_amountWithErrorBorderBankAdjustment)) {
			writeTestResults("Verify error border is appear for the Amount field",
					"Error border should appear for the Amount field",
					"Error border successfully appear for the Amount field", "pass");
		} else {
			writeTestResults("Verify error border is appear for the Amount field",
					"Error border should appear for the Amount field",
					"Error border doesn't appear for the Amount field", "fail");
		}

		if (isDisplayed(lbl_errorPlusAmountCapitalSettlementBankAdjustment)) {
			writeTestResults(
					"Verifhy when plus value entered \"Only minus values are allowed\" error message is appear",
					"When plus value entered \"Only minus values are allowed\" error message should appear",
					"When plus value entered \"Only minus values are allowed\" error message successfully appear",
					"pass");
		} else {
			writeTestResults(
					"Verifhy when plus value entered \"Only minus values are allowed\" error message is appear",
					"When plus value entered \"Only minus values are allowed\" error message should appear",
					"When plus value entered \"Only minus values are allowed\" error message doesn't appear", "fail");
		}

		String amountWhenEnterdPlusValue = getAttribute(txt_amount, "value");
		if (amountWhenEnterdPlusValue.equals("0.00000")) {
			writeTestResults("Verify enterd amount is disapear", "Enterd amount should disapear",
					"Enterd amount successfully disapear", "pass");
		} else {
			writeTestResults("Verify enterd amount is disapear", "Enterd amount should disapear",
					"Enterd amount doesn't disapear", "pass");
		}

		sendKeys(txt_amount, minusTenThousand);

		click(drop_bank);

		Thread.sleep(1000);
		if (!isDisplayedQuickCheck(txt_amountWithErrorBorderBankAdjustment)) {
			writeTestResults("Verify error border is not appear for the Amount field",
					"Error border should not appear for the Amount field",
					"Error border successfully not appear for the Amount field", "pass");
		} else {
			writeTestResults("Verify error border is not appear for the Amount field",
					"Error border should not appear for the Amount field",
					"Error border is appear for the Amount field", "fail");
		}

		if (!isDisplayedQuickCheck(lbl_errorPlusAmountCapitalSettlementBankAdjustment)) {
			writeTestResults("Verifhy when minus value entered no error message is not appear",
					"When minus value entered error message should not appear",
					"When minus value entered no error message is appear", "pass");
		} else {
			writeTestResults("Verifhy when minus value entered no error message is not appear",
					"When minus value entered error message should not appear",
					"When minus value entered error message is appear", "fail");
		}

		String amountWhenEnterdMinusValue = getAttribute(txt_amount, "value");
		if (amountWhenEnterdMinusValue.equals("-10000.00000")) {
			writeTestResults("Verify enterd minus amount is apear", "Enterd minus amount should apear",
					"Enterd amount successfully apear", "pass");
			writeTestResults("Verify enterd minus amount is apear", "Enterd minus amount should apear",
					"Enterd amount doesn't apear", "fail");
		}

	}

	/* FIN_BA_20_23 */
	public void bankAdjustmentAgainstBankFacilityAgreement_FIN_BA_20_23() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_bankaAdjustment, 20);
		if (isDisplayed(btn_bankaAdjustment)) {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option successfully available under finance module", "pass");
		} else {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option doesn't available under finance module", "fail");
		}

		if (isEnabled(btn_bankaAdjustment)) {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option", "User able to click on Bank Adjustment option",
					"pass");
		} else {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option",
					"User not able to click on Bank Adjustment option", "fail");
		}

		click(btn_bankaAdjustment);
		explicitWait(header_bankAdjustmentByPage, 35);
		if (isEnabled(header_bankAdjustmentByPage)) {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User successfully directed to by-page Bank Adjustment", "pass");
		} else {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User doesn't directed to by-page Bank Adjustment", "fail");
		}

		click(btn_newBankAdjustment);
		explicitWait(header_newBankAdjustment, 35);
		if (isDisplayed(header_newBankAdjustment)) {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed",
					"Bank Adjustment - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed", "Bank Adjustment - New page doesn't displayed",
					"fail");
		}

		selectText(drop_bank, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bank);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_bankAccountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_bankAccountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_lookupGLBankAdjustment);
		getInvObj().handeledSendKeys(txt_GL, glAccountMotorVehicles);
		pressEnter(txt_GL);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles), 30);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles));
		Thread.sleep(2000);

		String gl_submitted = getAttribute(txt_glInFrontPageBankAdjustmentSmoke, "value");
		if (glAccountMotorVehicles.equals(gl_submitted)) {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User sucessfully set gl account fieald", "pass");
		} else {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User couldn't set gl account fieald", "fail");
		}

		click(chk_facilitySettlement);

		explicitWait(txt_bankFacilityFrontPageBankAdjustment, 10);
		if (isDisplayed(txt_bankFacilityFrontPageBankAdjustment)) {
			writeTestResults("Verify Bank Facility field is display", "Bank Facility field should display",
					"Bank Facility field successfully display", "pass");
		} else {
			writeTestResults("Verify Bank Facility field is display", "Bank Facility field should display",
					"Bank Facility field doesn't display", "fail");
		}

		if (isEnabled(drop_settlementTypeBankAdjustment)) {
			writeTestResults("Verify that settlement type field is enable", "Settlement type field should be enable",
					"Settlement type field successfully enable", "pass");
		} else {
			writeTestResults("Verify that settlement type field is enable", "Settlement type field should be enable",
					"Settlement type field is disabled", "fail");
		}

		click(btn_lookupBankFacilityOnBankAdjustment);
		getInvObj().handeledSendKeys(txt_bankFacilityAgreementSearch, bankFacilityOriginalDocNumber);
		pressEnter(txt_bankFacilityAgreementSearch);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", bankFacilityOriginalDocNumber), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", bankFacilityOriginalDocNumber));
		Thread.sleep(2000);

		String selectedFacility = getAttribute(txt_bankFacilityFrontPageBankAdjustment, "value");

		if (selectedFacility.equals("000002")) {
			writeTestResults("Verify user able to select a Bank Facility Agreement",
					"User should be able to select a Bank Facility Agreement",
					"User successfully select a Bank Facility Agreement", "pass");
		} else {
			writeTestResults("Verify user able to select a Bank Facility Agreement",
					"User should be able to select a Bank Facility Agreement",
					"User doesn't select a Bank Facility Agreement", "fail");
		}

		selectText(drop_settlementTypeBankAdjustment, setlementTypeCapital);
		String setlementType = getSelectedOptionInDropdown(drop_settlementTypeBankAdjustment);

		if (setlementType.equals(setlementTypeCapital)) {
			writeTestResults("Verify user able to select Capital as settlement type",
					"User should be able to select Capital as settlement type",
					"User successfully select Capital as settlement type", "pass");
		} else {
			writeTestResults("Verify user able to select Capital as settlement type",
					"User should be able to select Capital as settlement type",
					"User doesn't select Capital as settlement type", "fail");
		}

		((JavascriptExecutor) driver).executeScript("window.open('about:blank','_blank');");
		switchWindow();
		openPage(site_Url);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 30);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_bankFacilityAgreement, 30);
		if (isDisplayed(btn_bankFacilityAgreement)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}

		click(btn_bankFacilityAgreement);
		explicitWait(txt_bankFacilityAgreementSearch, 40);
		if (!isDisplayedQuickCheck(lnk_bankFacilityAgreementDocReplace.replace("doc_no", "000002"))) {
			sendKeys(txt_bankFacilityAgreementSearch, bankFacilityOriginalDocNumber);
			pressEnter(txt_bankFacilityAgreementSearch);
			explicitWait(lnk_bankFacilityAgreementDocReplace.replace("doc_no", "000002"), 50);
			click(lnk_bankFacilityAgreementDocReplace.replace("doc_no", "000002"));
		}
		click(lnk_bankFacilityAgreementDocReplace.replace("doc_no", "000002"));
		explicitWait(div_balanceAmountBankFacilityAgreement, 40);
		Thread.sleep(2000);
		String beforeBalance = getText(div_balanceAmountBankFacilityAgreement);

		switchWindow();
		sendKeys(txt_amount, minusTenThousand);
		Thread.sleep(1000);
		String amount = getAttribute(txt_amount, "value");

		if (amount.equals(minusTenThousand)) {
			writeTestResults(
					"Verify user able to enter a negative amount which is lower than the Balance Amount of the Bank Facility Agreement",
					"User should be able to enter a negative amount which is lower than the Balance Amount of the Bank Facility Agreement",
					"User successfully enter a negative amount which is lower than the Balance Amount of the Bank Facility Agreement",
					"pass");
		} else {
			writeTestResults(
					"Verify user able to enter a negative amount which is lower than the Balance Amount of the Bank Facility Agreement",
					"User should be able to enter a negative amount which is lower than the Balance Amount of the Bank Facility Agreement",
					"User doesn't enter a negative amount which is lower than the Balance Amount of the Bank Facility Agreement",
					"fail");
		}

		click(btn_costAllocationBankAdjustment);

		explicitWait(drop_costAssignmentTypeBankAdjustment, 15);
		String disableStatus = getAttribute(drop_costAssignmentTypeBankAdjustment, "disabled");
		if (disableStatus.equals("true")) {
			writeTestResults(
					"Verify that Cost Assignment Type is disabled for capital settlement of Bank Facility Agreement",
					"Cost Assignment Type should be disabled for capital settlement of Bank Facility Agreement",
					"Cost Assignment Type successfully disabled for capital settlement of Bank Facility Agreement",
					"pass");
		} else {
			writeTestResults(
					"Verify that Cost Assignment Type is disabled for capital settlement of Bank Facility Agreement",
					"Cost Assignment Type should be disabled for capital settlement of Bank Facility Agreement",
					"Cost Assignment Type doesn't disabled for capital settlement of Bank Facility Agreement", "fail");
		}

		click(btn_closeCostAllocationWindow);
		Thread.sleep(1000);

		explicitWait(btn_checkout, 20);
		click(btn_checkout);

		String amountTotal = getAttribute(txt_amount, "value");
		if (amountTotal.equals("-10000.000000")) {
			writeTestResults("Verify user can checkout the Bank Adjsutment",
					"User should be able to checkout the Bank Adjsutment",
					"User successfully checkout the Bank Adjsutment", "pass");
		} else {
			writeTestResults("Verify user can checkout the Bank Adjsutment",
					"User should be able to checkout the Bank Adjsutment", "User doesn't checkout the Bank Adjsutment",
					"fail");
		}

		click(btn_draft);
		explicitWait(header_draftedBankAdjustment, 40);
		if (isDisplayed(header_draftedBankAdjustment)) {
			writeTestResults("Verify user can draft the Bank Adjsutment",
					"User should be able to draft the Bank Adjsutment", "User successfully draft the Bank Adjsutment",
					"pass");
		} else {
			writeTestResults("Verify user can draft the Bank Adjsutment",
					"User should be able to draft the Bank Adjsutment", "User doesn't draft the Bank Adjsutment",
					"fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedBankAdjustment, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedBankAdjustment)) {
			writeTestResults("Verify user can release the Bank Adjsutment",
					"User should be able to release the Bank Adjsutment",
					"User successfully release the Bank Adjsutment", "pass");
		} else {
			writeTestResults("Verify user can release the Bank Adjsutment",
					"User should be able to release the Bank Adjsutment", "User doesn't release the Bank Adjsutment",
					"fail");
		}

		switchWindow();
		pageRefersh();
		explicitWait(div_balanceAmountBankFacilityAgreement, 40);
		Thread.sleep(2000);
		String afterBalance = getText(div_balanceAmountBankFacilityAgreement);

		double beforeBalabceAmount = Double.valueOf(beforeBalance);
		double afterBalabceAmount = Double.valueOf(afterBalance);

		if (beforeBalabceAmount - afterBalabceAmount == 10000) {
			writeTestResults(
					"Verify that Bank Adjustment amount was get reduced from the Balance Amount of the Bank Facility Agreement",
					"Bank Adjustment amount should get reduced from the Balance Amount of the Bank Facility Agreement",
					"Bank Adjustment amount successfully reduced from the Balance Amount of the Bank Facility Agreement",
					"pass");
		} else {
			writeTestResults(
					"Verify that Bank Adjustment amount was get reduced from the Balance Amount of the Bank Facility Agreement",
					"Bank Adjustment amount should get reduced from the Balance Amount of the Bank Facility Agreement",
					"Bank Adjustment amount doesn't reduced from the Balance Amount of the Bank Facility Agreement",
					"fail");
		}

	}

	/* FIN_BA_21 */
	public void checkCompletedBankFacilityWithBankAdjustment_FIN_BA_21() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_bankaAdjustment, 20);
		if (isDisplayed(btn_bankaAdjustment)) {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option successfully available under finance module", "pass");
		} else {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option doesn't available under finance module", "fail");
		}

		if (isEnabled(btn_bankaAdjustment)) {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option", "User able to click on Bank Adjustment option",
					"pass");
		} else {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option",
					"User not able to click on Bank Adjustment option", "fail");
		}

		click(btn_bankaAdjustment);
		explicitWait(header_bankAdjustmentByPage, 35);
		if (isEnabled(header_bankAdjustmentByPage)) {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User successfully directed to by-page Bank Adjustment", "pass");
		} else {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User doesn't directed to by-page Bank Adjustment", "fail");
		}

		click(btn_newBankAdjustment);
		explicitWait(header_newBankAdjustment, 35);
		if (isDisplayed(header_newBankAdjustment)) {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed",
					"Bank Adjustment - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed", "Bank Adjustment - New page doesn't displayed",
					"fail");
		}

		selectText(drop_bank, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bank);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_bankAccountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_bankAccountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_lookupGLBankAdjustment);
		getInvObj().handeledSendKeys(txt_GL, glAccountMotorVehicles);
		pressEnter(txt_GL);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles), 30);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles));
		Thread.sleep(2000);

		String gl_submitted = getAttribute(txt_glInFrontPageBankAdjustmentSmoke, "value");
		if (glAccountMotorVehicles.equals(gl_submitted)) {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User sucessfully set gl account fieald", "pass");
		} else {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User couldn't set gl account fieald", "fail");
		}

		click(chk_facilitySettlement);

		explicitWait(txt_bankFacilityFrontPageBankAdjustment, 10);
		if (isDisplayed(txt_bankFacilityFrontPageBankAdjustment)) {
			writeTestResults("Verify Bank Facility field is display", "Bank Facility field should display",
					"Bank Facility field successfully display", "pass");
		} else {
			writeTestResults("Verify Bank Facility field is display", "Bank Facility field should display",
					"Bank Facility field doesn't display", "fail");
		}

		if (isEnabled(drop_settlementTypeBankAdjustment)) {
			writeTestResults("Verify that settlement type field is enable", "Settlement type field should be enable",
					"Settlement type field successfully enable", "pass");
		} else {
			writeTestResults("Verify that settlement type field is enable", "Settlement type field should be enable",
					"Settlement type field is disabled", "fail");
		}

		click(btn_lookupBankFacilityOnBankAdjustment);
		getInvObj().handeledSendKeys(txt_bankFacilityAgreementSearch, "000003");
		pressEnter(txt_bankFacilityAgreementSearch);
		explicitWait(lnk_resultBankFailityDocReplace.replace("doc_no", "000003"), 10);
		Thread.sleep(2000);

		if (!isDisplayedQuickCheck(lnk_resultBankFailityDocReplace.replace("doc_no", "000003"))) {
			writeTestResults("Verify user not able to view the completed Bank Facility Agreements loaded in the lookup",
					"User shouldn't able to view the completed Bank Facility Agreements loaded in the lookup",
					"User not able to view the completed Bank Facility Agreements loaded in the lookup", "pass");
		} else {
			writeTestResults("Verify user not able to view the completed Bank Facility Agreements loaded in the lookup",
					"User shouldn't able to view the completed Bank Facility Agreements loaded in the lookup",
					"User able to view the completed Bank Facility Agreements loaded in the lookup", "fail");
		}
	}

	/* FIN_BA_22 */
	public void checkBankFacilityAccordingToTheBank_FIN_BA_22() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_bankaAdjustment, 20);
		if (isDisplayed(btn_bankaAdjustment)) {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option successfully available under finance module", "pass");
		} else {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option doesn't available under finance module", "fail");
		}

		if (isEnabled(btn_bankaAdjustment)) {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option", "User able to click on Bank Adjustment option",
					"pass");
		} else {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option",
					"User not able to click on Bank Adjustment option", "fail");
		}

		click(btn_bankaAdjustment);
		explicitWait(header_bankAdjustmentByPage, 35);
		if (isEnabled(header_bankAdjustmentByPage)) {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User successfully directed to by-page Bank Adjustment", "pass");
		} else {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User doesn't directed to by-page Bank Adjustment", "fail");
		}

		click(btn_newBankAdjustment);
		explicitWait(header_newBankAdjustment, 35);
		if (isDisplayed(header_newBankAdjustment)) {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed",
					"Bank Adjustment - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed", "Bank Adjustment - New page doesn't displayed",
					"fail");
		}

		selectText(drop_bank, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bank);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_bankAccountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_bankAccountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_lookupGLBankAdjustment);
		getInvObj().handeledSendKeys(txt_GL, glAccountMotorVehicles);
		pressEnter(txt_GL);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles), 30);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles));
		Thread.sleep(2000);

		String gl_submitted = getAttribute(txt_glInFrontPageBankAdjustmentSmoke, "value");
		if (glAccountMotorVehicles.equals(gl_submitted)) {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User sucessfully set gl account fieald", "pass");
		} else {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User couldn't set gl account fieald", "fail");
		}

		click(chk_facilitySettlement);

		explicitWait(txt_bankFacilityFrontPageBankAdjustment, 10);
		if (isDisplayed(txt_bankFacilityFrontPageBankAdjustment)) {
			writeTestResults("Verify Bank Facility field is display", "Bank Facility field should display",
					"Bank Facility field successfully display", "pass");
		} else {
			writeTestResults("Verify Bank Facility field is display", "Bank Facility field should display",
					"Bank Facility field doesn't display", "fail");
		}

		if (isEnabled(drop_settlementTypeBankAdjustment)) {
			writeTestResults("Verify that settlement type field is enable", "Settlement type field should be enable",
					"Settlement type field successfully enable", "pass");
		} else {
			writeTestResults("Verify that settlement type field is enable", "Settlement type field should be enable",
					"Settlement type field is disabled", "fail");
		}

		click(btn_lookupBankFacilityOnBankAdjustment);

		getInvObj().handeledSendKeys(txt_bankFacilityAgreementSearch, "000002");
		pressEnter(txt_bankFacilityAgreementSearch);
		explicitWait(lnk_resultBankFailityDocReplace.replace("doc_no", "000002"), 10);
		Thread.sleep(2000);

		if (isDisplayedQuickCheck(lnk_resultBankFailityDocReplace.replace("doc_no", "000002"))) {
			writeTestResults("Verify that Bank Facility Agreement created for selected bank account was get searched",
					"Bank Facility Agreement created for selected bank account should get searched",
					"Bank Facility Agreement created for selected bank account successfully get searched", "pass");
		} else {
			writeTestResults("Verify that Bank Facility Agreement created for selected bank account was get searched",
					"Bank Facility Agreement created for selected bank account should get searched",
					"Bank Facility Agreement created for selected bank account doesn't get searched", "fail");
		}

		getInvObj().handeledSendKeys(txt_bankFacilityAgreementSearch, "000004");
		pressEnter(txt_bankFacilityAgreementSearch);
		explicitWait(lnk_resultBankFailityDocReplace.replace("doc_no", "000004"), 10);
		Thread.sleep(2000);

		if (!isDisplayedQuickCheck(lnk_resultBankFailityDocReplace.replace("doc_no", "000004"))) {
			writeTestResults(
					"Verify that Bank Facility Agreement created for different bank account was't get searched",
					"Bank Facility Agreement created for different bank account shouldn't get searched",
					"Bank Facility Agreement created for different bank account wasn't get searched", "pass");
		} else {
			writeTestResults(
					"Verify that Bank Facility Agreement created for different bank account was't get searched",
					"Bank Facility Agreement created for different bank account shouldn't get searched",
					"Bank Facility Agreement created for different bank account was get searched", "fail");
		}
	}

	/* FIN_BA_24 */
	public void bankAdjustmentAgainstBankFacilityAgreementNeagativeInterest_FIN_BA_24() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_bankaAdjustment, 20);
		if (isDisplayed(btn_bankaAdjustment)) {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option successfully available under finance module", "pass");
		} else {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option doesn't available under finance module", "fail");
		}

		if (isEnabled(btn_bankaAdjustment)) {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option", "User able to click on Bank Adjustment option",
					"pass");
		} else {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option",
					"User not able to click on Bank Adjustment option", "fail");
		}

		click(btn_bankaAdjustment);
		explicitWait(header_bankAdjustmentByPage, 35);
		if (isEnabled(header_bankAdjustmentByPage)) {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User successfully directed to by-page Bank Adjustment", "pass");
		} else {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User doesn't directed to by-page Bank Adjustment", "fail");
		}

		click(btn_newBankAdjustment);
		explicitWait(header_newBankAdjustment, 35);
		if (isDisplayed(header_newBankAdjustment)) {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed",
					"Bank Adjustment - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed", "Bank Adjustment - New page doesn't displayed",
					"fail");
		}

		selectText(drop_bank, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bank);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_bankAccountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_bankAccountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_lookupGLBankAdjustment);
		getInvObj().handeledSendKeys(txt_GL, glAccountMotorVehicles);
		pressEnter(txt_GL);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles), 30);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles));
		Thread.sleep(2000);

		String gl_submitted = getAttribute(txt_glInFrontPageBankAdjustmentSmoke, "value");
		if (glAccountMotorVehicles.equals(gl_submitted)) {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User sucessfully set gl account fieald", "pass");
		} else {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User couldn't set gl account fieald", "fail");
		}

		click(chk_facilitySettlement);

		explicitWait(txt_bankFacilityFrontPageBankAdjustment, 10);
		if (isDisplayed(txt_bankFacilityFrontPageBankAdjustment)) {
			writeTestResults("Verify Bank Facility field is display", "Bank Facility field should display",
					"Bank Facility field successfully display", "pass");
		} else {
			writeTestResults("Verify Bank Facility field is display", "Bank Facility field should display",
					"Bank Facility field doesn't display", "fail");
		}

		if (isEnabled(drop_settlementTypeBankAdjustment)) {
			writeTestResults("Verify that settlement type field is enable", "Settlement type field should be enable",
					"Settlement type field successfully enable", "pass");
		} else {
			writeTestResults("Verify that settlement type field is enable", "Settlement type field should be enable",
					"Settlement type field is disabled", "fail");
		}

		click(btn_lookupBankFacilityOnBankAdjustment);
		getInvObj().handeledSendKeys(txt_bankFacilityAgreementSearch, bankFacilityOriginalDocNumber);
		pressEnter(txt_bankFacilityAgreementSearch);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", bankFacilityOriginalDocNumber), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", bankFacilityOriginalDocNumber));
		Thread.sleep(2000);

		String selectedFacility = getAttribute(txt_bankFacilityFrontPageBankAdjustment, "value");

		if (selectedFacility.equals("000002")) {
			writeTestResults("Verify user able to select a Bank Facility Agreement",
					"User should be able to select a Bank Facility Agreement",
					"User successfully select a Bank Facility Agreement", "pass");
		} else {
			writeTestResults("Verify user able to select a Bank Facility Agreement",
					"User should be able to select a Bank Facility Agreement",
					"User doesn't select a Bank Facility Agreement", "fail");
		}

		selectText(drop_settlementTypeBankAdjustment, setlementTypeInterest);
		String setlementType = getSelectedOptionInDropdown(drop_settlementTypeBankAdjustment);

		if (setlementType.equals(setlementTypeInterest)) {
			writeTestResults("Verify user able to select Interest as settlement type",
					"User should be able to select Interest as settlement type",
					"User successfully select Interest as settlement type", "pass");
		} else {
			writeTestResults("Verify user able to select Interest as settlement type",
					"User should be able to select Interest as settlement type",
					"User doesn't select Interest as settlement type", "fail");
		}

		sendKeys(txt_amount, minusTwoThousand);
		Thread.sleep(1000);
		String amount = getAttribute(txt_amount, "value");

		if (amount.equals(minusTwoThousand)) {
			writeTestResults("Verify user able to enter a negative amount",
					"User should be able to enter a negative amount", "User successfully enter a negative amount",
					"pass");
		} else {
			writeTestResults("Verify user able to enter a negative amount",
					"User should be able to enter a negative amount", "User doesn't enter a negative amount", "fail");
		}

		explicitWait(btn_checkout, 20);
		click(btn_checkout);

		String amountTotal = getAttribute(txt_amount, "value");
		if (amountTotal.equals("-2000.000000")) {
			writeTestResults("Verify user can checkout the Bank Adjsutment",
					"User should be able to checkout the Bank Adjsutment",
					"User successfully checkout the Bank Adjsutment", "pass");
		} else {
			writeTestResults("Verify user can checkout the Bank Adjsutment",
					"User should be able to checkout the Bank Adjsutment", "User doesn't checkout the Bank Adjsutment",
					"fail");
		}

		click(btn_draft);
		explicitWait(header_draftedBankAdjustment, 40);
		if (isDisplayed(header_draftedBankAdjustment)) {
			writeTestResults("Verify user can draft the Bank Adjsutment",
					"User should be able to draft the Bank Adjsutment", "User successfully draft the Bank Adjsutment",
					"pass");
		} else {
			writeTestResults("Verify user can draft the Bank Adjsutment",
					"User should be able to draft the Bank Adjsutment", "User doesn't draft the Bank Adjsutment",
					"fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedBankAdjustment, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedBankAdjustment)) {
			writeTestResults("Verify user can release the Bank Adjsutment",
					"User should be able to release the Bank Adjsutment",
					"User successfully release the Bank Adjsutment", "pass");
		} else {
			writeTestResults("Verify user can release the Bank Adjsutment",
					"User should be able to release the Bank Adjsutment", "User doesn't release the Bank Adjsutment",
					"fail");
		}

	}

	/* FIN_BA_25 */
	public void bankAdjustmentAgainstBankFacilityAgreementPositiveInterest_FIN_BA_25() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_bankaAdjustment, 20);
		if (isDisplayed(btn_bankaAdjustment)) {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option successfully available under finance module", "pass");
		} else {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option doesn't available under finance module", "fail");
		}

		if (isEnabled(btn_bankaAdjustment)) {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option", "User able to click on Bank Adjustment option",
					"pass");
		} else {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option",
					"User not able to click on Bank Adjustment option", "fail");
		}

		click(btn_bankaAdjustment);
		explicitWait(header_bankAdjustmentByPage, 35);
		if (isEnabled(header_bankAdjustmentByPage)) {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User successfully directed to by-page Bank Adjustment", "pass");
		} else {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User doesn't directed to by-page Bank Adjustment", "fail");
		}

		click(btn_newBankAdjustment);
		explicitWait(header_newBankAdjustment, 35);
		if (isDisplayed(header_newBankAdjustment)) {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed",
					"Bank Adjustment - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed", "Bank Adjustment - New page doesn't displayed",
					"fail");
		}

		selectText(drop_bank, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bank);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_bankAccountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_bankAccountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_lookupGLBankAdjustment);
		getInvObj().handeledSendKeys(txt_GL, glAccountMotorVehicles);
		pressEnter(txt_GL);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles), 30);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles));
		Thread.sleep(2000);

		String gl_submitted = getAttribute(txt_glInFrontPageBankAdjustmentSmoke, "value");
		if (glAccountMotorVehicles.equals(gl_submitted)) {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User sucessfully set gl account fieald", "pass");
		} else {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User couldn't set gl account fieald", "fail");
		}

		click(chk_facilitySettlement);

		explicitWait(txt_bankFacilityFrontPageBankAdjustment, 10);
		if (isDisplayed(txt_bankFacilityFrontPageBankAdjustment)) {
			writeTestResults("Verify Bank Facility field is display", "Bank Facility field should display",
					"Bank Facility field successfully display", "pass");
		} else {
			writeTestResults("Verify Bank Facility field is display", "Bank Facility field should display",
					"Bank Facility field doesn't display", "fail");
		}

		if (isEnabled(drop_settlementTypeBankAdjustment)) {
			writeTestResults("Verify that settlement type field is enable", "Settlement type field should be enable",
					"Settlement type field successfully enable", "pass");
		} else {
			writeTestResults("Verify that settlement type field is enable", "Settlement type field should be enable",
					"Settlement type field is disabled", "fail");
		}

		click(btn_lookupBankFacilityOnBankAdjustment);
		getInvObj().handeledSendKeys(txt_bankFacilityAgreementSearch, bankFacilityOriginalDocNumber);
		pressEnter(txt_bankFacilityAgreementSearch);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", bankFacilityOriginalDocNumber), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", bankFacilityOriginalDocNumber));
		Thread.sleep(2000);

		String selectedFacility = getAttribute(txt_bankFacilityFrontPageBankAdjustment, "value");

		if (selectedFacility.equals("000002")) {
			writeTestResults("Verify user able to select a Bank Facility Agreement",
					"User should be able to select a Bank Facility Agreement",
					"User successfully select a Bank Facility Agreement", "pass");
		} else {
			writeTestResults("Verify user able to select a Bank Facility Agreement",
					"User should be able to select a Bank Facility Agreement",
					"User doesn't select a Bank Facility Agreement", "fail");
		}

		selectText(drop_settlementTypeBankAdjustment, setlementTypeInterest);
		String setlementType = getSelectedOptionInDropdown(drop_settlementTypeBankAdjustment);

		if (setlementType.equals(setlementTypeInterest)) {
			writeTestResults("Verify user able to select Interest as settlement type",
					"User should be able to select Interest as settlement type",
					"User successfully select Interest as settlement type", "pass");
		} else {
			writeTestResults("Verify user able to select Interest as settlement type",
					"User should be able to select Interest as settlement type",
					"User doesn't select Interest as settlement type", "fail");
		}

		sendKeys(txt_amount, amountTwoThousand);
		Thread.sleep(1000);
		String amount = getAttribute(txt_amount, "value");

		if (amount.equals(amountTwoThousand)) {
			writeTestResults("Verify user able to enter a positive amount",
					"User should be able to enter a positive amount", "User successfully enter a positive amount",
					"pass");
		} else {
			writeTestResults("Verify user able to enter a positive amount",
					"User should be able to enter a positive amount", "User doesn't enter a positive amount", "fail");
		}

		explicitWait(btn_checkout, 20);
		click(btn_checkout);

		String amountTotal = getAttribute(txt_amount, "value");
		if (amountTotal.equals("2000.000000")) {
			writeTestResults("Verify user can checkout the Bank Adjsutment",
					"User should be able to checkout the Bank Adjsutment",
					"User successfully checkout the Bank Adjsutment", "pass");
		} else {
			writeTestResults("Verify user can checkout the Bank Adjsutment",
					"User should be able to checkout the Bank Adjsutment", "User doesn't checkout the Bank Adjsutment",
					"fail");
		}

		click(btn_draft);
		explicitWait(header_draftedBankAdjustment, 40);
		if (isDisplayed(header_draftedBankAdjustment)) {
			writeTestResults("Verify user can draft the Bank Adjsutment",
					"User should be able to draft the Bank Adjsutment", "User successfully draft the Bank Adjsutment",
					"pass");
		} else {
			writeTestResults("Verify user can draft the Bank Adjsutment",
					"User should be able to draft the Bank Adjsutment", "User doesn't draft the Bank Adjsutment",
					"fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedBankAdjustment, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedBankAdjustment)) {
			writeTestResults("Verify user can release the Bank Adjsutment",
					"User should be able to release the Bank Adjsutment",
					"User successfully release the Bank Adjsutment", "pass");
		} else {
			writeTestResults("Verify user can release the Bank Adjsutment",
					"User should be able to release the Bank Adjsutment", "User doesn't release the Bank Adjsutment",
					"fail");
		}

	}

	/* FIN_BA_26 */
	public void bankAdjustmentAgainstBankFacilityAgreementNeagativePanaltyInterest_FIN_BA_26() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_bankaAdjustment, 20);
		if (isDisplayed(btn_bankaAdjustment)) {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option successfully available under finance module", "pass");
		} else {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option doesn't available under finance module", "fail");
		}

		if (isEnabled(btn_bankaAdjustment)) {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option", "User able to click on Bank Adjustment option",
					"pass");
		} else {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option",
					"User not able to click on Bank Adjustment option", "fail");
		}

		click(btn_bankaAdjustment);
		explicitWait(header_bankAdjustmentByPage, 35);
		if (isEnabled(header_bankAdjustmentByPage)) {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User successfully directed to by-page Bank Adjustment", "pass");
		} else {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User doesn't directed to by-page Bank Adjustment", "fail");
		}

		click(btn_newBankAdjustment);
		explicitWait(header_newBankAdjustment, 35);
		if (isDisplayed(header_newBankAdjustment)) {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed",
					"Bank Adjustment - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed", "Bank Adjustment - New page doesn't displayed",
					"fail");
		}

		selectText(drop_bank, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bank);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_bankAccountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_bankAccountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_lookupGLBankAdjustment);
		getInvObj().handeledSendKeys(txt_GL, glAccountMotorVehicles);
		pressEnter(txt_GL);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles), 30);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles));
		Thread.sleep(2000);

		String gl_submitted = getAttribute(txt_glInFrontPageBankAdjustmentSmoke, "value");
		if (glAccountMotorVehicles.equals(gl_submitted)) {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User sucessfully set gl account fieald", "pass");
		} else {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User couldn't set gl account fieald", "fail");
		}

		click(chk_facilitySettlement);

		explicitWait(txt_bankFacilityFrontPageBankAdjustment, 10);
		if (isDisplayed(txt_bankFacilityFrontPageBankAdjustment)) {
			writeTestResults("Verify Bank Facility field is display", "Bank Facility field should display",
					"Bank Facility field successfully display", "pass");
		} else {
			writeTestResults("Verify Bank Facility field is display", "Bank Facility field should display",
					"Bank Facility field doesn't display", "fail");
		}

		if (isEnabled(drop_settlementTypeBankAdjustment)) {
			writeTestResults("Verify that settlement type field is enable", "Settlement type field should be enable",
					"Settlement type field successfully enable", "pass");
		} else {
			writeTestResults("Verify that settlement type field is enable", "Settlement type field should be enable",
					"Settlement type field is disabled", "fail");
		}

		click(btn_lookupBankFacilityOnBankAdjustment);
		getInvObj().handeledSendKeys(txt_bankFacilityAgreementSearch, bankFacilityOriginalDocNumber);
		pressEnter(txt_bankFacilityAgreementSearch);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", bankFacilityOriginalDocNumber), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", bankFacilityOriginalDocNumber));
		Thread.sleep(2000);

		String selectedFacility = getAttribute(txt_bankFacilityFrontPageBankAdjustment, "value");

		if (selectedFacility.equals("000002")) {
			writeTestResults("Verify user able to select a Bank Facility Agreement",
					"User should be able to select a Bank Facility Agreement",
					"User successfully select a Bank Facility Agreement", "pass");
		} else {
			writeTestResults("Verify user able to select a Bank Facility Agreement",
					"User should be able to select a Bank Facility Agreement",
					"User doesn't select a Bank Facility Agreement", "fail");
		}

		selectText(drop_settlementTypeBankAdjustment, setlementTypePanaltyInterest);
		String setlementType = getSelectedOptionInDropdown(drop_settlementTypeBankAdjustment);

		if (setlementType.equals(setlementTypePanaltyInterest)) {
			writeTestResults("Verify user able to select Panlty Interest as settlement type",
					"User should be able to select Panlty Interest as settlement type",
					"User successfully select Panlty Interest as settlement type", "pass");
		} else {
			writeTestResults("Verify user able to select Panlty Interest as settlement type",
					"User should be able to select Panlty Interest as settlement type",
					"User doesn't select Panlty Interest as settlement type", "fail");
		}

		sendKeys(txt_amount, minusTwoThousand);
		Thread.sleep(1000);
		String amount = getAttribute(txt_amount, "value");

		if (amount.equals(minusTwoThousand)) {
			writeTestResults("Verify user able to enter a negative amount",
					"User should be able to enter a negative amount", "User successfully enter a negative amount",
					"pass");
		} else {
			writeTestResults("Verify user able to enter a negative amount",
					"User should be able to enter a negative amount", "User doesn't enter a negative amount", "fail");
		}

		explicitWait(btn_checkout, 20);
		click(btn_checkout);

		String amountTotal = getAttribute(txt_amount, "value");
		if (amountTotal.equals("-2000.000000")) {
			writeTestResults("Verify user can checkout the Bank Adjsutment",
					"User should be able to checkout the Bank Adjsutment",
					"User successfully checkout the Bank Adjsutment", "pass");
		} else {
			writeTestResults("Verify user can checkout the Bank Adjsutment",
					"User should be able to checkout the Bank Adjsutment", "User doesn't checkout the Bank Adjsutment",
					"fail");
		}

		click(btn_draft);
		explicitWait(header_draftedBankAdjustment, 40);
		if (isDisplayed(header_draftedBankAdjustment)) {
			writeTestResults("Verify user can draft the Bank Adjsutment",
					"User should be able to draft the Bank Adjsutment", "User successfully draft the Bank Adjsutment",
					"pass");
		} else {
			writeTestResults("Verify user can draft the Bank Adjsutment",
					"User should be able to draft the Bank Adjsutment", "User doesn't draft the Bank Adjsutment",
					"fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedBankAdjustment, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedBankAdjustment)) {
			writeTestResults("Verify user can release the Bank Adjsutment",
					"User should be able to release the Bank Adjsutment",
					"User successfully release the Bank Adjsutment", "pass");
		} else {
			writeTestResults("Verify user can release the Bank Adjsutment",
					"User should be able to release the Bank Adjsutment", "User doesn't release the Bank Adjsutment",
					"fail");
		}

	}

	/* FIN_BA_27 */
	public void bankAdjustmentAgainstBankFacilityAgreementPositivePanaltyInterest_FIN_BA_27() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_bankaAdjustment, 20);
		if (isDisplayed(btn_bankaAdjustment)) {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option successfully available under finance module", "pass");
		} else {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option doesn't available under finance module", "fail");
		}

		if (isEnabled(btn_bankaAdjustment)) {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option", "User able to click on Bank Adjustment option",
					"pass");
		} else {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option",
					"User not able to click on Bank Adjustment option", "fail");
		}

		click(btn_bankaAdjustment);
		explicitWait(header_bankAdjustmentByPage, 35);
		if (isEnabled(header_bankAdjustmentByPage)) {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User successfully directed to by-page Bank Adjustment", "pass");
		} else {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User doesn't directed to by-page Bank Adjustment", "fail");
		}

		click(btn_newBankAdjustment);
		explicitWait(header_newBankAdjustment, 35);
		if (isDisplayed(header_newBankAdjustment)) {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed",
					"Bank Adjustment - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed", "Bank Adjustment - New page doesn't displayed",
					"fail");
		}

		selectText(drop_bank, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bank);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_bankAccountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_bankAccountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_lookupGLBankAdjustment);
		getInvObj().handeledSendKeys(txt_GL, glAccountMotorVehicles);
		pressEnter(txt_GL);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles), 30);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles));
		Thread.sleep(2000);

		String gl_submitted = getAttribute(txt_glInFrontPageBankAdjustmentSmoke, "value");
		if (glAccountMotorVehicles.equals(gl_submitted)) {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User sucessfully set gl account fieald", "pass");
		} else {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User couldn't set gl account fieald", "fail");
		}

		click(chk_facilitySettlement);

		explicitWait(txt_bankFacilityFrontPageBankAdjustment, 10);
		if (isDisplayed(txt_bankFacilityFrontPageBankAdjustment)) {
			writeTestResults("Verify Bank Facility field is display", "Bank Facility field should display",
					"Bank Facility field successfully display", "pass");
		} else {
			writeTestResults("Verify Bank Facility field is display", "Bank Facility field should display",
					"Bank Facility field doesn't display", "fail");
		}

		if (isEnabled(drop_settlementTypeBankAdjustment)) {
			writeTestResults("Verify that settlement type field is enable", "Settlement type field should be enable",
					"Settlement type field successfully enable", "pass");
		} else {
			writeTestResults("Verify that settlement type field is enable", "Settlement type field should be enable",
					"Settlement type field is disabled", "fail");
		}

		click(btn_lookupBankFacilityOnBankAdjustment);
		getInvObj().handeledSendKeys(txt_bankFacilityAgreementSearch, bankFacilityOriginalDocNumber);
		pressEnter(txt_bankFacilityAgreementSearch);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", bankFacilityOriginalDocNumber), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", bankFacilityOriginalDocNumber));
		Thread.sleep(2000);

		String selectedFacility = getAttribute(txt_bankFacilityFrontPageBankAdjustment, "value");

		if (selectedFacility.equals("000002")) {
			writeTestResults("Verify user able to select a Bank Facility Agreement",
					"User should be able to select a Bank Facility Agreement",
					"User successfully select a Bank Facility Agreement", "pass");
		} else {
			writeTestResults("Verify user able to select a Bank Facility Agreement",
					"User should be able to select a Bank Facility Agreement",
					"User doesn't select a Bank Facility Agreement", "fail");
		}

		selectText(drop_settlementTypeBankAdjustment, setlementTypePanaltyInterest);
		String setlementType = getSelectedOptionInDropdown(drop_settlementTypeBankAdjustment);

		if (setlementType.equals(setlementTypePanaltyInterest)) {
			writeTestResults("Verify user able to select Panlty Interest as settlement type",
					"User should be able to select Panlty Interest as settlement type",
					"User successfully select Panlty Interest as settlement type", "pass");
		} else {
			writeTestResults("Verify user able to select Panlty Interest as settlement type",
					"User should be able to select Panlty Interest as settlement type",
					"User doesn't select Panlty Interest as settlement type", "fail");
		}

		sendKeys(txt_amount, amountTwoThousand);
		Thread.sleep(1000);
		String amount = getAttribute(txt_amount, "value");

		if (amount.equals(amountTwoThousand)) {
			writeTestResults("Verify user able to enter a positive amount",
					"User should be able to enter a positive amount", "User successfully enter a positive amount",
					"pass");
		} else {
			writeTestResults("Verify user able to enter a positive amount",
					"User should be able to enter a positive amount", "User doesn't enter a positive amount", "fail");
		}

		explicitWait(btn_checkout, 20);
		click(btn_checkout);

		String amountTotal = getAttribute(txt_amount, "value");
		if (amountTotal.equals("2000.000000")) {
			writeTestResults("Verify user can checkout the Bank Adjsutment",
					"User should be able to checkout the Bank Adjsutment",
					"User successfully checkout the Bank Adjsutment", "pass");
		} else {
			writeTestResults("Verify user can checkout the Bank Adjsutment",
					"User should be able to checkout the Bank Adjsutment", "User doesn't checkout the Bank Adjsutment",
					"fail");
		}

		click(btn_draft);
		explicitWait(header_draftedBankAdjustment, 40);
		if (isDisplayed(header_draftedBankAdjustment)) {
			writeTestResults("Verify user can draft the Bank Adjsutment",
					"User should be able to draft the Bank Adjsutment", "User successfully draft the Bank Adjsutment",
					"pass");
		} else {
			writeTestResults("Verify user can draft the Bank Adjsutment",
					"User should be able to draft the Bank Adjsutment", "User doesn't draft the Bank Adjsutment",
					"fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedBankAdjustment, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedBankAdjustment)) {
			writeTestResults("Verify user can release the Bank Adjsutment",
					"User should be able to release the Bank Adjsutment",
					"User successfully release the Bank Adjsutment", "pass");
		} else {
			writeTestResults("Verify user can release the Bank Adjsutment",
					"User should be able to release the Bank Adjsutment", "User doesn't release the Bank Adjsutment",
					"fail");
		}

	}

	/* FIN_BA_28 */
	public void bankAdjustmentAgainstBankFacilityAgreementNegativeLateFee_FIN_BA_28() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_bankaAdjustment, 20);
		if (isDisplayed(btn_bankaAdjustment)) {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option successfully available under finance module", "pass");
		} else {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option doesn't available under finance module", "fail");
		}

		if (isEnabled(btn_bankaAdjustment)) {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option", "User able to click on Bank Adjustment option",
					"pass");
		} else {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option",
					"User not able to click on Bank Adjustment option", "fail");
		}

		click(btn_bankaAdjustment);
		explicitWait(header_bankAdjustmentByPage, 35);
		if (isEnabled(header_bankAdjustmentByPage)) {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User successfully directed to by-page Bank Adjustment", "pass");
		} else {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User doesn't directed to by-page Bank Adjustment", "fail");
		}

		click(btn_newBankAdjustment);
		explicitWait(header_newBankAdjustment, 35);
		if (isDisplayed(header_newBankAdjustment)) {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed",
					"Bank Adjustment - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed", "Bank Adjustment - New page doesn't displayed",
					"fail");
		}

		selectText(drop_bank, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bank);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_bankAccountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_bankAccountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_lookupGLBankAdjustment);
		getInvObj().handeledSendKeys(txt_GL, glAccountMotorVehicles);
		pressEnter(txt_GL);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles), 30);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles));
		Thread.sleep(2000);

		String gl_submitted = getAttribute(txt_glInFrontPageBankAdjustmentSmoke, "value");
		if (glAccountMotorVehicles.equals(gl_submitted)) {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User sucessfully set gl account fieald", "pass");
		} else {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User couldn't set gl account fieald", "fail");
		}

		click(chk_facilitySettlement);

		explicitWait(txt_bankFacilityFrontPageBankAdjustment, 10);
		if (isDisplayed(txt_bankFacilityFrontPageBankAdjustment)) {
			writeTestResults("Verify Bank Facility field is display", "Bank Facility field should display",
					"Bank Facility field successfully display", "pass");
		} else {
			writeTestResults("Verify Bank Facility field is display", "Bank Facility field should display",
					"Bank Facility field doesn't display", "fail");
		}

		if (isEnabled(drop_settlementTypeBankAdjustment)) {
			writeTestResults("Verify that settlement type field is enable", "Settlement type field should be enable",
					"Settlement type field successfully enable", "pass");
		} else {
			writeTestResults("Verify that settlement type field is enable", "Settlement type field should be enable",
					"Settlement type field is disabled", "fail");
		}

		click(btn_lookupBankFacilityOnBankAdjustment);
		getInvObj().handeledSendKeys(txt_bankFacilityAgreementSearch, bankFacilityOriginalDocNumber);
		pressEnter(txt_bankFacilityAgreementSearch);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", bankFacilityOriginalDocNumber), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", bankFacilityOriginalDocNumber));
		Thread.sleep(2000);

		String selectedFacility = getAttribute(txt_bankFacilityFrontPageBankAdjustment, "value");

		if (selectedFacility.equals("000002")) {
			writeTestResults("Verify user able to select a Bank Facility Agreement",
					"User should be able to select a Bank Facility Agreement",
					"User successfully select a Bank Facility Agreement", "pass");
		} else {
			writeTestResults("Verify user able to select a Bank Facility Agreement",
					"User should be able to select a Bank Facility Agreement",
					"User doesn't select a Bank Facility Agreement", "fail");
		}

		selectText(drop_settlementTypeBankAdjustment, setlementTypeLateFee);
		String setlementType = getSelectedOptionInDropdown(drop_settlementTypeBankAdjustment);

		if (setlementType.equals(setlementTypeLateFee)) {
			writeTestResults("Verify user able to select Late Fee as settlement type",
					"User should be able to select Late Fee as settlement type",
					"User successfully select Late Fee as settlement type", "pass");
		} else {
			writeTestResults("Verify user able to select Late Fee as settlement type",
					"User should be able to select Late Fee as settlement type",
					"User doesn't select Late Fee as settlement type", "fail");
		}

		sendKeys(txt_amount, minusTwoThousand);
		Thread.sleep(1000);
		String amount = getAttribute(txt_amount, "value");

		if (amount.equals(minusTwoThousand)) {
			writeTestResults("Verify user able to enter a negative amount",
					"User should be able to enter a negative amount", "User successfully enter a negative amount",
					"pass");
		} else {
			writeTestResults("Verify user able to enter a negative amount",
					"User should be able to enter a negative amount", "User doesn't enter a negative amount", "fail");
		}

		explicitWait(btn_checkout, 20);
		click(btn_checkout);

		String amountTotal = getAttribute(txt_amount, "value");
		if (amountTotal.equals("-2000.000000")) {
			writeTestResults("Verify user can checkout the Bank Adjsutment",
					"User should be able to checkout the Bank Adjsutment",
					"User successfully checkout the Bank Adjsutment", "pass");
		} else {
			writeTestResults("Verify user can checkout the Bank Adjsutment",
					"User should be able to checkout the Bank Adjsutment", "User doesn't checkout the Bank Adjsutment",
					"fail");
		}

		click(btn_draft);
		explicitWait(header_draftedBankAdjustment, 40);
		if (isDisplayed(header_draftedBankAdjustment)) {
			writeTestResults("Verify user can draft the Bank Adjsutment",
					"User should be able to draft the Bank Adjsutment", "User successfully draft the Bank Adjsutment",
					"pass");
		} else {
			writeTestResults("Verify user can draft the Bank Adjsutment",
					"User should be able to draft the Bank Adjsutment", "User doesn't draft the Bank Adjsutment",
					"fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedBankAdjustment, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedBankAdjustment)) {
			writeTestResults("Verify user can release the Bank Adjsutment",
					"User should be able to release the Bank Adjsutment",
					"User successfully release the Bank Adjsutment", "pass");
		} else {
			writeTestResults("Verify user can release the Bank Adjsutment",
					"User should be able to release the Bank Adjsutment", "User doesn't release the Bank Adjsutment",
					"fail");
		}

	}

	/* FIN_BA_29 */
	public void bankAdjustmentAgainstBankFacilityAgreementPositiveLateFee_FIN_BA_29() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_bankaAdjustment, 20);
		if (isDisplayed(btn_bankaAdjustment)) {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option successfully available under finance module", "pass");
		} else {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option doesn't available under finance module", "fail");
		}

		if (isEnabled(btn_bankaAdjustment)) {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option", "User able to click on Bank Adjustment option",
					"pass");
		} else {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option",
					"User not able to click on Bank Adjustment option", "fail");
		}

		click(btn_bankaAdjustment);
		explicitWait(header_bankAdjustmentByPage, 35);
		if (isEnabled(header_bankAdjustmentByPage)) {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User successfully directed to by-page Bank Adjustment", "pass");
		} else {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User doesn't directed to by-page Bank Adjustment", "fail");
		}

		click(btn_newBankAdjustment);
		explicitWait(header_newBankAdjustment, 35);
		if (isDisplayed(header_newBankAdjustment)) {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed",
					"Bank Adjustment - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed", "Bank Adjustment - New page doesn't displayed",
					"fail");
		}

		selectText(drop_bank, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bank);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_bankAccountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_bankAccountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_lookupGLBankAdjustment);
		getInvObj().handeledSendKeys(txt_GL, glAccountMotorVehicles);
		pressEnter(txt_GL);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles), 30);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles));
		Thread.sleep(2000);

		String gl_submitted = getAttribute(txt_glInFrontPageBankAdjustmentSmoke, "value");
		if (glAccountMotorVehicles.equals(gl_submitted)) {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User sucessfully set gl account fieald", "pass");
		} else {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User couldn't set gl account fieald", "fail");
		}

		click(chk_facilitySettlement);

		explicitWait(txt_bankFacilityFrontPageBankAdjustment, 10);
		if (isDisplayed(txt_bankFacilityFrontPageBankAdjustment)) {
			writeTestResults("Verify Bank Facility field is display", "Bank Facility field should display",
					"Bank Facility field successfully display", "pass");
		} else {
			writeTestResults("Verify Bank Facility field is display", "Bank Facility field should display",
					"Bank Facility field doesn't display", "fail");
		}

		if (isEnabled(drop_settlementTypeBankAdjustment)) {
			writeTestResults("Verify that settlement type field is enable", "Settlement type field should be enable",
					"Settlement type field successfully enable", "pass");
		} else {
			writeTestResults("Verify that settlement type field is enable", "Settlement type field should be enable",
					"Settlement type field is disabled", "fail");
		}

		click(btn_lookupBankFacilityOnBankAdjustment);
		getInvObj().handeledSendKeys(txt_bankFacilityAgreementSearch, bankFacilityOriginalDocNumber);
		pressEnter(txt_bankFacilityAgreementSearch);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", bankFacilityOriginalDocNumber), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", bankFacilityOriginalDocNumber));
		Thread.sleep(2000);

		String selectedFacility = getAttribute(txt_bankFacilityFrontPageBankAdjustment, "value");

		if (selectedFacility.equals("000002")) {
			writeTestResults("Verify user able to select a Bank Facility Agreement",
					"User should be able to select a Bank Facility Agreement",
					"User successfully select a Bank Facility Agreement", "pass");
		} else {
			writeTestResults("Verify user able to select a Bank Facility Agreement",
					"User should be able to select a Bank Facility Agreement",
					"User doesn't select a Bank Facility Agreement", "fail");
		}

		selectText(drop_settlementTypeBankAdjustment, setlementTypeLateFee);
		String setlementType = getSelectedOptionInDropdown(drop_settlementTypeBankAdjustment);

		if (setlementType.equals(setlementTypeLateFee)) {
			writeTestResults("Verify user able to select Late Fee as settlement type",
					"User should be able to select Late Fee as settlement type",
					"User successfully select Late Fee as settlement type", "pass");
		} else {
			writeTestResults("Verify user able to select Late Fee as settlement type",
					"User should be able to select Late Fee as settlement type",
					"User doesn't select Late Fee as settlement type", "fail");
		}

		sendKeys(txt_amount, amountTwoThousand);
		Thread.sleep(1000);
		String amount = getAttribute(txt_amount, "value");

		if (amount.equals(amountTwoThousand)) {
			writeTestResults("Verify user able to enter a positive amount",
					"User should be able to enter a positive amount", "User successfully enter a positive amount",
					"pass");
		} else {
			writeTestResults("Verify user able to enter a positive amount",
					"User should be able to enter a positive amount", "User doesn't enter a positive amount", "fail");
		}

		explicitWait(btn_checkout, 20);
		click(btn_checkout);

		String amountTotal = getAttribute(txt_amount, "value");
		if (amountTotal.equals("2000.000000")) {
			writeTestResults("Verify user can checkout the Bank Adjsutment",
					"User should be able to checkout the Bank Adjsutment",
					"User successfully checkout the Bank Adjsutment", "pass");
		} else {
			writeTestResults("Verify user can checkout the Bank Adjsutment",
					"User should be able to checkout the Bank Adjsutment", "User doesn't checkout the Bank Adjsutment",
					"fail");
		}

		click(btn_draft);
		explicitWait(header_draftedBankAdjustment, 40);
		if (isDisplayed(header_draftedBankAdjustment)) {
			writeTestResults("Verify user can draft the Bank Adjsutment",
					"User should be able to draft the Bank Adjsutment", "User successfully draft the Bank Adjsutment",
					"pass");
		} else {
			writeTestResults("Verify user can draft the Bank Adjsutment",
					"User should be able to draft the Bank Adjsutment", "User doesn't draft the Bank Adjsutment",
					"fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedBankAdjustment, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedBankAdjustment)) {
			writeTestResults("Verify user can release the Bank Adjsutment",
					"User should be able to release the Bank Adjsutment",
					"User successfully release the Bank Adjsutment", "pass");
		} else {
			writeTestResults("Verify user can release the Bank Adjsutment",
					"User should be able to release the Bank Adjsutment", "User doesn't release the Bank Adjsutment",
					"fail");
		}

	}

	/* FIN_BA_30 */
	public void draftBankAdjustmentWithoutCheckout_FIN_BA_30() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_bankaAdjustment, 20);
		if (isDisplayed(btn_bankaAdjustment)) {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option successfully available under finance module", "pass");
		} else {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option doesn't available under finance module", "fail");
		}

		if (isEnabled(btn_bankaAdjustment)) {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option", "User able to click on Bank Adjustment option",
					"pass");
		} else {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option",
					"User not able to click on Bank Adjustment option", "fail");
		}

		click(btn_bankaAdjustment);
		explicitWait(header_bankAdjustmentByPage, 35);
		if (isEnabled(header_bankAdjustmentByPage)) {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User successfully directed to by-page Bank Adjustment", "pass");
		} else {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User doesn't directed to by-page Bank Adjustment", "fail");
		}

		click(btn_newBankAdjustment);
		explicitWait(header_newBankAdjustment, 35);
		if (isDisplayed(header_newBankAdjustment)) {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed",
					"Bank Adjustment - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed", "Bank Adjustment - New page doesn't displayed",
					"fail");
		}

		selectText(drop_bank, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bank);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_bankAccountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_bankAccountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_lookupGLBankAdjustment);
		getInvObj().handeledSendKeys(txt_GL, glAccountMotorVehicles);
		pressEnter(txt_GL);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles), 30);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles));
		Thread.sleep(2000);

		String gl_submitted = getAttribute(txt_glInFrontPageBankAdjustmentSmoke, "value");
		if (glAccountMotorVehicles.equals(gl_submitted)) {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User sucessfully set gl account fieald", "pass");
		} else {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User couldn't set gl account fieald", "fail");
		}

		click(chk_facilitySettlement);

		explicitWait(txt_bankFacilityFrontPageBankAdjustment, 10);
		if (isDisplayed(txt_bankFacilityFrontPageBankAdjustment)) {
			writeTestResults("Verify Bank Facility field is display", "Bank Facility field should display",
					"Bank Facility field successfully display", "pass");
		} else {
			writeTestResults("Verify Bank Facility field is display", "Bank Facility field should display",
					"Bank Facility field doesn't display", "fail");
		}

		if (isEnabled(drop_settlementTypeBankAdjustment)) {
			writeTestResults("Verify that settlement type field is enable", "Settlement type field should be enable",
					"Settlement type field successfully enable", "pass");
		} else {
			writeTestResults("Verify that settlement type field is enable", "Settlement type field should be enable",
					"Settlement type field is disabled", "fail");
		}

		click(btn_lookupBankFacilityOnBankAdjustment);
		getInvObj().handeledSendKeys(txt_bankFacilityAgreementSearch, bankFacilityOriginalDocNumber);
		pressEnter(txt_bankFacilityAgreementSearch);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", bankFacilityOriginalDocNumber), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", bankFacilityOriginalDocNumber));
		Thread.sleep(2000);

		String selectedFacility = getAttribute(txt_bankFacilityFrontPageBankAdjustment, "value");

		if (selectedFacility.equals("000002")) {
			writeTestResults("Verify user able to select a Bank Facility Agreement",
					"User should be able to select a Bank Facility Agreement",
					"User successfully select a Bank Facility Agreement", "pass");
		} else {
			writeTestResults("Verify user able to select a Bank Facility Agreement",
					"User should be able to select a Bank Facility Agreement",
					"User doesn't select a Bank Facility Agreement", "fail");
		}

		selectText(drop_settlementTypeBankAdjustment, setlementTypeInterest);
		String setlementType = getSelectedOptionInDropdown(drop_settlementTypeBankAdjustment);

		if (setlementType.equals(setlementTypeInterest)) {
			writeTestResults("Verify user able to select Interest as settlement type",
					"User should be able to select Interest as settlement type",
					"User successfully select Interest as settlement type", "pass");
		} else {
			writeTestResults("Verify user able to select Interest as settlement type",
					"User should be able to select Interest as settlement type",
					"User doesn't select Interest as settlement type", "fail");
		}

		sendKeys(txt_amount, minusTwoThousand);
		Thread.sleep(1000);
		String amount = getAttribute(txt_amount, "value");

		if (amount.equals(minusTwoThousand)) {
			writeTestResults("Verify user able to enter a negative amount",
					"User should be able to enter a negative amount", "User successfully enter a negative amount",
					"pass");
		} else {
			writeTestResults("Verify user able to enter a negative amount",
					"User should be able to enter a negative amount", "User doesn't enter a negative amount", "fail");
		}

		click(btn_draft);
		explicitWait(para_validationCheckoutBeforeDraft, 10);
		if (isDisplayed(para_validationCheckoutBeforeDraft)) {
			writeTestResults(
					"Verify that 'Checkout document before save data' error message is displayed when click on Draft",
					"'Checkout document before save data' error message should be displayed when click on Draft",
					"'Checkout document before save data' error message successfully displayed when click on Draft",
					"pass");
		} else {
			writeTestResults(
					"Verify that 'Checkout document before save data' error message is displayed when click on Draft",
					"'Checkout document before save data' error message should be displayed when click on Draft",
					"'Checkout document before save data' error message doesn't displayed when click on Draft", "fail");
		}

		explicitWait(btn_checkoutWithErrorBorder, 5);
		if (isDisplayed(para_validationCheckoutBeforeDraft)) {
			writeTestResults("Verify that error border is display with checkout button",
					"Error border should display with checkout button",
					"Error border successfully display with checkout button", "pass");
		} else {
			writeTestResults("Verify that error border is display with checkout button",
					"Error border should display with checkout button",
					"Error border doesn't display with checkout button", "fail");
		}
	}

	/* FIN_BA_31_32 */
	public void releaseBankAdjustmentWithTax_FIN_BA_31_32() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_bankaAdjustment, 20);
		if (isDisplayed(btn_bankaAdjustment)) {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option successfully available under finance module", "pass");
		} else {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option doesn't available under finance module", "fail");
		}

		if (isEnabled(btn_bankaAdjustment)) {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option", "User able to click on Bank Adjustment option",
					"pass");
		} else {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option",
					"User not able to click on Bank Adjustment option", "fail");
		}

		click(btn_bankaAdjustment);
		explicitWait(header_bankAdjustmentByPage, 35);
		if (isEnabled(header_bankAdjustmentByPage)) {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User successfully directed to by-page Bank Adjustment", "pass");
		} else {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User doesn't directed to by-page Bank Adjustment", "fail");
		}

		click(btn_newBankAdjustment);
		explicitWait(header_newBankAdjustment, 35);
		if (isDisplayed(header_newBankAdjustment)) {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed",
					"Bank Adjustment - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed", "Bank Adjustment - New page doesn't displayed",
					"fail");
		}

		selectText(drop_bank, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bank);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_bankAccountNo, sampathCurrentAccount);
		String selectedAccount = getSelectedOptionInDropdown(drop_bankAccountNo);
		if (selectedAccount.equals(sampathCurrentAccount)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_lookupGLBankAdjustment);
		getInvObj().handeledSendKeys(txt_GL, glAccountMotorVehicles);
		pressEnter(txt_GL);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles), 30);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles));
		Thread.sleep(2000);

		String gl_submitted = getAttribute(txt_glInFrontPageBankAdjustmentSmoke, "value");
		if (glAccountMotorVehicles.equals(gl_submitted)) {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User sucessfully set gl account fieald", "pass");
		} else {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User couldn't set gl account fieald", "fail");
		}

		sendKeys(txt_amount, minusTwoThousand);
		Thread.sleep(1000);
		String amount = getAttribute(txt_amount, "value");

		if (amount.equals(minusTwoThousand)) {
			writeTestResults("Verify user able to enter a negative amount",
					"User should be able to enter a negative amount", "User successfully enter a negative amount",
					"pass");
		} else {
			writeTestResults("Verify user able to enter a negative amount",
					"User should be able to enter a negative amount", "User doesn't enter a negative amount", "fail");
		}
		Thread.sleep(1000);
		click(drop_tax);
		selectText(drop_tax, taxGroup);
		Thread.sleep(1000);
		String selectedTaxGroup = getSelectedOptionInDropdown(drop_tax);
		if (selectedTaxGroup.equals(taxGroup)) {
			writeTestResults("Verify user able to select a Tax Group", "User should be able to select a Tax Group",
					"User successfully select a Tax Group", "pass");
		} else {
			writeTestResults("Verify user able to select a Tax Group", "User should be able to select a Tax Group",
					"User deosn't select a Tax Group", "fail");
		}

		explicitWait(btn_checkout, 20);
		click(btn_checkout);

		String amountTotal = getAttribute(txt_amount, "value");
		if (amountTotal.equals("-2000.000000")) {
			writeTestResults("Verify user can checkout the Bank Adjsutment",
					"User should be able to checkout the Bank Adjsutment",
					"User successfully checkout the Bank Adjsutment", "pass");
		} else {
			writeTestResults("Verify user can checkout the Bank Adjsutment",
					"User should be able to checkout the Bank Adjsutment", "User doesn't checkout the Bank Adjsutment",
					"fail");
		}

		String totalTax = getAttribute(txt_taxTotal, "value");
		if (totalTax.equals("200.000000")) {
			writeTestResults("Verify that when checkout Tax Total get calculated based on the tax group",
					"When checkout Tax Total should get calculated based on the tax group",
					"When checkout Tax Total successfully get calculated based on the tax group", "pass");
		} else {
			writeTestResults("Verify that when checkout Tax Total get calculated based on the tax group",
					"When checkout Tax Total should get calculated based on the tax group",
					"When checkout Tax Total doesn't get calculated based on the tax group", "fail");
		}

		click(btn_draft);
		explicitWait(header_draftedBankAdjustment, 40);
		if (isDisplayed(header_draftedBankAdjustment)) {
			writeTestResults("Verify user can draft the Bank Adjsutment",
					"User should be able to draft the Bank Adjsutment", "User successfully draft the Bank Adjsutment",
					"pass");
		} else {
			writeTestResults("Verify user can draft the Bank Adjsutment",
					"User should be able to draft the Bank Adjsutment", "User doesn't draft the Bank Adjsutment",
					"fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedBankAdjustment, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedBankAdjustment)) {
			writeTestResults("Verify user can release the Bank Adjsutment",
					"User should be able to release the Bank Adjsutment",
					"User successfully release the Bank Adjsutment", "pass");
		} else {
			writeTestResults("Verify user can release the Bank Adjsutment",
					"User should be able to release the Bank Adjsutment", "User doesn't release the Bank Adjsutment",
					"fail");
		}

		explicitWait(btn_action, 50);
		click(btn_action);
		explicitWait(btn_journel, 10);
		click(btn_journel);
		explicitWait(header_journelEntry, 10);
		if (isDisplayed(header_journelEntry)) {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded",
					"Journal Entry details pop up successfully get loaded", "pass");
		} else {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded", "Journal Entry details pop up doesn't get loaded",
					"fail");
		}

		String debitBalanceSelectedGL = getText(
				lbl_debitJournelEntryBankDepositGLReplace.replace("-je", "0").replace("gl", glAccountMotorVehicles));

		if (debitBalanceSelectedGL.equals("2000.000000")) {
			writeTestResults("Verify that amount was debited to the selected GL Account",
					"Amount should be debited to the selected GL Account",
					"Amount successfully debited to the selected GL Account", "pass");
		} else {
			writeTestResults("Verify that amount was debited to the selected GL Account",
					"Amount should be debited to the selected GL Account",
					"Amount doesn't debited to the selected GL Account", "fail");
		}

		String debitBalanceTaxReciveble = getText(
				lbl_debitJournelEntryBankDepositGLReplace.replace("-je", "0").replace("gl", glVATRecievable));
		if (debitBalanceTaxReciveble.equals("200.000000")) {
			writeTestResults("Verify that tax total was debited to the VAT Receivable Account",
					"Tax total should be debited to the VAT Receivable Account",
					"Tax total successfully debited to the VAT Receivable Account", "pass");
		} else {
			writeTestResults("Verify that tax total was debited to the VAT Receivable Account",
					"Tax total should be debited to the VAT Receivable Account",
					"Tax total doesn't debited to the VAT Receivable Account", "fail");
		}

		String creditBalanceBank = getText(
				lbl_creditJournelEntryBankDepositGLReplace.replace("-je", "0").replace("gl", glBank));
		if (creditBalanceBank.equals("2200.000000")) {
			writeTestResults(
					"Verify that  Amount + Tax total was credited to the default GL account for the selected Bank",
					"Amount + Tax total should be credited to the default GL account for the selected Bank",
					"Amount + Tax total successfully credited to the default GL account for the selected Bank", "pass");
		} else {
			writeTestResults(
					"Verify that Amount + Tax total was credited to the default GL account for the selected Bank",
					"Amount + Tax total should be credited to the default GL account for the selected Bank",
					"Amount + Tax total doesn't credited to the default GL account for the selected Bank", "fail");
		}

		String debitTotalCustomerRecBDtBD = getText(lbl_debitTotalJournelEntryRecordReplace.replace("-Record", "0"));
		String credTotalCustomerRecBDtBD = getText(lbl_creditTotalJournelEntryRecordReplace.replace("-Record", "0"));

		if (debitTotalCustomerRecBDtBD.equals("2200.000000")
				&& credTotalCustomerRecBDtBD.equals(debitTotalCustomerRecBDtBD)) {
			writeTestResults("Verify total debit and credit amounts were balanced",
					"Total debit and credit amounts should balanced",
					"Total debit and credit amounts successfully balanced", "pass");
		} else {
			writeTestResults("Verify total debit and credit amounts were balanced",
					"Total debit and credit amounts should balanced", "Total debit and credit amounts weren't balanced",
					"fail");
		}
	}

	/* FIN_BA_33 */
	public void releaseBankAdjustmentUSD_FIN_BA_33() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_bankaAdjustment, 20);
		if (isDisplayed(btn_bankaAdjustment)) {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option successfully available under finance module", "pass");
		} else {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option doesn't available under finance module", "fail");
		}

		if (isEnabled(btn_bankaAdjustment)) {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option", "User able to click on Bank Adjustment option",
					"pass");
		} else {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option",
					"User not able to click on Bank Adjustment option", "fail");
		}

		click(btn_bankaAdjustment);
		explicitWait(header_bankAdjustmentByPage, 35);
		if (isEnabled(header_bankAdjustmentByPage)) {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User successfully directed to by-page Bank Adjustment", "pass");
		} else {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User doesn't directed to by-page Bank Adjustment", "fail");
		}

		click(btn_newBankAdjustment);
		explicitWait(header_newBankAdjustment, 35);
		if (isDisplayed(header_newBankAdjustment)) {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed",
					"Bank Adjustment - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed", "Bank Adjustment - New page doesn't displayed",
					"fail");
		}

		selectText(drop_bank, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bank);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_bankAccountNo, sampathCurrentAccountUSD);
		String selectedAccount = getSelectedOptionInDropdown(drop_bankAccountNo);
		if (selectedAccount.equals(sampathCurrentAccountUSD)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		String currency = getAttribute(txt_currencyBankAdjustment, "value");
		if (currency.equals(currencyUSD)) {
			writeTestResults("Verify that 'Currency' field get updated as 'USD' when a USD Bank account is selected",
					"'Currency' field should get updated as 'USD' when a USD Bank account is selected",
					"'Currency' field successfully get updated as 'USD' when a USD Bank account is selected", "pass");
		} else {
			writeTestResults("Verify that 'Currency' field get updated as 'USD' when a USD Bank account is selected",
					"'Currency' field should get updated as 'USD' when a USD Bank account is selected",
					"'Currency' field doesn't get updated as 'USD' when a USD Bank account is selected", "fail");
		}

		click(btn_lookupGLBankAdjustment);
		getInvObj().handeledSendKeys(txt_GL, glAccountMotorVehicles);
		pressEnter(txt_GL);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles), 30);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles));
		Thread.sleep(2000);

		String gl_submitted = getAttribute(txt_glInFrontPageBankAdjustmentSmoke, "value");
		if (glAccountMotorVehicles.equals(gl_submitted)) {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User sucessfully set gl account fieald", "pass");
		} else {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User couldn't set gl account fieald", "fail");
		}

		sendKeys(txt_amount, amountTwoThousand);
		Thread.sleep(1000);
		String amount = getAttribute(txt_amount, "value");

		if (amount.equals(amountTwoThousand)) {
			writeTestResults("Verify user able to enter a amount", "User should be able to enter a amount",
					"User successfully enter a amount", "pass");
		} else {
			writeTestResults("Verify user able to enter a amount", "User should be able to enter a amount",
					"User doesn't enter a amount", "fail");
		}
		Thread.sleep(1000);

		explicitWait(btn_checkout, 20);
		click(btn_checkout);

		String amountTotal = getAttribute(txt_amount, "value");
		if (amountTotal.equals("2000.000000")) {
			writeTestResults("Verify user can checkout the Bank Adjsutment",
					"User should be able to checkout the Bank Adjsutment",
					"User successfully checkout the Bank Adjsutment", "pass");
		} else {
			writeTestResults("Verify user can checkout the Bank Adjsutment",
					"User should be able to checkout the Bank Adjsutment", "User doesn't checkout the Bank Adjsutment",
					"fail");
		}

		click(btn_draft);
		explicitWait(header_draftedBankAdjustment, 40);
		if (isDisplayed(header_draftedBankAdjustment)) {
			writeTestResults("Verify user can draft the Bank Adjsutment",
					"User should be able to draft the Bank Adjsutment", "User successfully draft the Bank Adjsutment",
					"pass");
		} else {
			writeTestResults("Verify user can draft the Bank Adjsutment",
					"User should be able to draft the Bank Adjsutment", "User doesn't draft the Bank Adjsutment",
					"fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedBankAdjustment, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedBankAdjustment)) {
			writeTestResults("Verify user can release the Bank Adjsutment",
					"User should be able to release the Bank Adjsutment",
					"User successfully release the Bank Adjsutment", "pass");
		} else {
			writeTestResults("Verify user can release the Bank Adjsutment",
					"User should be able to release the Bank Adjsutment", "User doesn't release the Bank Adjsutment",
					"fail");
		}
	}

	/* FIN_BA_34_35 */
	public void usdBankAdjustmentAgainstBankFacilityAgreement_FIN_BA_34() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_bankaAdjustment, 20);
		if (isDisplayed(btn_bankaAdjustment)) {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option successfully available under finance module", "pass");
		} else {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option doesn't available under finance module", "fail");
		}

		if (isEnabled(btn_bankaAdjustment)) {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option", "User able to click on Bank Adjustment option",
					"pass");
		} else {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option",
					"User not able to click on Bank Adjustment option", "fail");
		}

		click(btn_bankaAdjustment);
		explicitWait(header_bankAdjustmentByPage, 35);
		if (isEnabled(header_bankAdjustmentByPage)) {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User successfully directed to by-page Bank Adjustment", "pass");
		} else {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User doesn't directed to by-page Bank Adjustment", "fail");
		}

		click(btn_newBankAdjustment);
		explicitWait(header_newBankAdjustment, 35);
		if (isDisplayed(header_newBankAdjustment)) {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed",
					"Bank Adjustment - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed", "Bank Adjustment - New page doesn't displayed",
					"fail");
		}

		selectText(drop_bank, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bank);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		selectText(drop_bankAccountNo, sampathCurrentAccountUSD);
		String selectedAccount = getSelectedOptionInDropdown(drop_bankAccountNo);
		if (selectedAccount.equals(sampathCurrentAccountUSD)) {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User successfully select any Bank Account No from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User should be able to select any Bank Account No from the drop down where the field will populate the selected value",
					"User doesn't select any Bank Account No from the drop down where the field will populate the selected value",
					"fail");
		}

		click(btn_lookupGLBankAdjustment);
		getInvObj().handeledSendKeys(txt_GL, glAccountMotorVehicles);
		pressEnter(txt_GL);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles), 30);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", glAccountMotorVehicles));
		Thread.sleep(2000);

		String gl_submitted = getAttribute(txt_glInFrontPageBankAdjustmentSmoke, "value");
		if (glAccountMotorVehicles.equals(gl_submitted)) {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User sucessfully set gl account fieald", "pass");
		} else {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User couldn't set gl account fieald", "fail");
		}

		click(chk_facilitySettlement);

		explicitWait(txt_bankFacilityFrontPageBankAdjustment, 10);
		if (isDisplayed(txt_bankFacilityFrontPageBankAdjustment)) {
			writeTestResults("Verify Bank Facility field is display", "Bank Facility field should display",
					"Bank Facility field successfully display", "pass");
		} else {
			writeTestResults("Verify Bank Facility field is display", "Bank Facility field should display",
					"Bank Facility field doesn't display", "fail");
		}

		if (isEnabled(drop_settlementTypeBankAdjustment)) {
			writeTestResults("Verify that settlement type field is enable", "Settlement type field should be enable",
					"Settlement type field successfully enable", "pass");
		} else {
			writeTestResults("Verify that settlement type field is enable", "Settlement type field should be enable",
					"Settlement type field is disabled", "fail");
		}

		click(btn_lookupBankFacilityOnBankAdjustment);
		getInvObj().handeledSendKeys(txt_bankFacilityAgreementSearch, bankFacilityOriginalDocNumber);
		pressEnter(txt_bankFacilityAgreementSearch);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", bankFacilityOriginalDocNumber), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", bankFacilityOriginalDocNumber));
		Thread.sleep(2000);

		String selectedFacility = getAttribute(txt_bankFacilityFrontPageBankAdjustment, "value");

		if (selectedFacility.equals("000002")) {
			writeTestResults("Verify user able to select a Bank Facility Agreement",
					"User should be able to select a Bank Facility Agreement",
					"User successfully select a Bank Facility Agreement", "pass");
		} else {
			writeTestResults("Verify user able to select a Bank Facility Agreement",
					"User should be able to select a Bank Facility Agreement",
					"User doesn't select a Bank Facility Agreement", "fail");
		}

		selectText(drop_settlementTypeBankAdjustment, setlementTypeCapital);
		String setlementType = getSelectedOptionInDropdown(drop_settlementTypeBankAdjustment);

		if (setlementType.equals(setlementTypeCapital)) {
			writeTestResults("Verify user able to select Capital as settlement type",
					"User should be able to select Capital as settlement type",
					"User successfully select Capital as settlement type", "pass");
		} else {
			writeTestResults("Verify user able to select Capital as settlement type",
					"User should be able to select Capital as settlement type",
					"User doesn't select Capital as settlement type", "fail");
		}

		sendKeys(txt_amount, minusTenThousand);
		Thread.sleep(1000);
		String amount = getAttribute(txt_amount, "value");

		if (amount.equals(minusTenThousand)) {
			writeTestResults(
					"Verify user able to enter a negative amount which is lower than the Balance Amount of the Bank Facility Agreement",
					"User should be able to enter a negative amount which is lower than the Balance Amount of the Bank Facility Agreement",
					"User successfully enter a negative amount which is lower than the Balance Amount of the Bank Facility Agreement",
					"pass");
		} else {
			writeTestResults(
					"Verify user able to enter a negative amount which is lower than the Balance Amount of the Bank Facility Agreement",
					"User should be able to enter a negative amount which is lower than the Balance Amount of the Bank Facility Agreement",
					"User doesn't enter a negative amount which is lower than the Balance Amount of the Bank Facility Agreement",
					"fail");
		}

		explicitWait(btn_checkout, 20);
		click(btn_checkout);

		String amountTotal = getAttribute(txt_amount, "value");
		if (amountTotal.equals("-10000.000000")) {
			writeTestResults("Verify user can checkout the Bank Adjsutment",
					"User should be able to checkout the Bank Adjsutment",
					"User successfully checkout the Bank Adjsutment", "pass");
		} else {
			writeTestResults("Verify user can checkout the Bank Adjsutment",
					"User should be able to checkout the Bank Adjsutment", "User doesn't checkout the Bank Adjsutment",
					"fail");
		}

		click(btn_draft);
		explicitWait(header_draftedBankAdjustment, 40);
		if (isDisplayed(header_draftedBankAdjustment)) {
			writeTestResults("Verify user can draft the Bank Adjsutment",
					"User should be able to draft the Bank Adjsutment", "User successfully draft the Bank Adjsutment",
					"pass");
		} else {
			writeTestResults("Verify user can draft the Bank Adjsutment",
					"User should be able to draft the Bank Adjsutment", "User doesn't draft the Bank Adjsutment",
					"fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedBankAdjustment, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedBankAdjustment)) {
			writeTestResults("Verify user can release the Bank Adjsutment",
					"User should be able to release the Bank Adjsutment",
					"User successfully release the Bank Adjsutment", "pass");
		} else {
			writeTestResults("Verify user can release the Bank Adjsutment",
					"User should be able to release the Bank Adjsutment", "User doesn't release the Bank Adjsutment",
					"fail");
		}

	}

	/* FIN_BA_36 */
	public void checkBankAccountWithLeaseFacilityInBankAdjustmentForm_FIN_BA_36() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_bankaAdjustment, 20);
		if (isDisplayed(btn_bankaAdjustment)) {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option successfully available under finance module", "pass");
		} else {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option doesn't available under finance module", "fail");
		}

		if (isEnabled(btn_bankaAdjustment)) {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option", "User able to click on Bank Adjustment option",
					"pass");
		} else {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option",
					"User not able to click on Bank Adjustment option", "fail");
		}

		click(btn_bankaAdjustment);
		explicitWait(header_bankAdjustmentByPage, 35);
		if (isEnabled(header_bankAdjustmentByPage)) {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User successfully directed to by-page Bank Adjustment", "pass");
		} else {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User doesn't directed to by-page Bank Adjustment", "fail");
		}

		click(btn_newBankAdjustment);
		explicitWait(header_newBankAdjustment, 35);
		if (isDisplayed(header_newBankAdjustment)) {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed",
					"Bank Adjustment - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed", "Bank Adjustment - New page doesn't displayed",
					"fail");
		}

		selectText(drop_bank, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bank);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		List<String> bankAccountsList = new ArrayList<String>();
		Select dropBankAccounts = new Select(driver.findElement(By.xpath(drop_bankAccountNo)));
		List<WebElement> bankDropdown = dropBankAccounts.getOptions();
		int size = bankDropdown.size();
		for (int i = 0; i < size; i++) {
			String options = bankDropdown.get(i).getText();
			bankAccountsList.add(options);
		}

		if (!bankAccountsList.contains(sampathBankAccountWithLeaseFacilityAllow)) {
			writeTestResults("Verify that Bank Account with 'Lease Facility' selected not be loaded",
					"Bank Account with 'Lease Facility' selected should not be loaded",
					"Bank Account with 'Lease Facility' selected is not loaded", "pass");
		} else {
			writeTestResults("Verify that Bank Account with 'Lease Facility' selected not be loaded",
					"Bank Account with 'Lease Facility' selected should not be loaded",
					"Bank Account with 'Lease Facility' selected is loaded", "fail");
		}
	}

	/* Cash Bank Book */
	/* FIN_CBB_1_4_5_6_10 */
	public void cashBankBook_FIN_CBB_1_4_5_6_10() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_cashBankBook, 20);
		if (isDisplayed(btn_cashBankBook)) {
			writeTestResults("Verify that Cash/Bank Book option is available under Finance Module",
					"Cash/Bank Book option should be available under Finance Module",
					"Cash/Bank Book option successfully available under Finance Module", "pass");
		} else {
			writeTestResults("Verify that Cash/Bank Book option is available under Finance Module",
					"Cash/Bank Book option should be available under Finance Module",
					"Cash/Bank Book option successfully available under Finance Module", "fail");
		}

		if (isEnabled(btn_cashBankBook)) {
			writeTestResults("Verify user able to click on Cash/Bank Book option",
					"User should be able to click on Cash/Bank Book option",
					"User able to click on Cash/Bank Book option", "pass");
		} else {
			writeTestResults("Verify user able to click on Cash/Bank Book option",
					"User should be able to click on Cash/Bank Book option",
					"User not able to click on Cash/Bank Book option", "fail");
		}

		click(btn_cashBankBook);

		explicitWait(header_cashBankBookByPage, 40);
		if (isDisplayed(header_cashBankBookByPage)) {
			writeTestResults("Verify that user was directed to by-page Cash/Bank Book",
					"User should directed to by-page Cash/Bank Book",
					"User successfully directed to by-page Cash/Bank Book", "pass");
		} else {
			writeTestResults("Verify that user was directed to by-page Cash/Bank Book",
					"User should directed to by-page Cash/Bank Book", "User doesn't directed to by-page Cash/Bank Book",
					"fail");
		}

		click(btn_newCashBankBook);
		explicitWait(header_newCashBankBook, 40);
		if (isDisplayed(header_newCashBankBook)) {
			writeTestResults("Verify that Cash/Bank Book  - New page is displayed",
					"Cash/Bank Book  - New page should be displayed",
					"Cash/Bank Book  - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify that Cash/Bank Book  - New page is displayed",
					"Cash/Bank Book  - New page should be displayed", "Cash/Bank Book  - New page doesn't displayed",
					"fail");
		}

		selectText(drop_typeCashBankBook, cashBankBookTypeCash);
		String selectedBookType = getSelectedOptionInDropdown(drop_typeCashBankBook);
		if (selectedBookType.equals(cashBankBookTypeCash)) {
			writeTestResults("Verify that user able to select type as Cash",
					"User should be able to select type as Cash", "User successfully select type as Cash", "pass");
		} else {
			writeTestResults("Verify that user able to select type as Cash",
					"User should be able to select type as Cash", "User doesn't select type as Cash", "fail");
		}

		String accountNumber = getInvObj().currentTimeAndDate().replace("/", "").replace(":", "");
		sendKeys(txt_bankAccountNoCashBankBook, accountNumber);
		String setAccountNumber = getAttribute(txt_bankAccountNoCashBankBook, "value");
		if (setAccountNumber.equals(accountNumber)) {
			writeTestResults("Verify that user able to enter Account Number",
					"User should be able to enter Account Number", "User successfully enter Account Number", "pass");
		} else {
			writeTestResults("Verify that user able to enter Account Number",
					"User should be able to enter Account Number", "User doesn't enter Account Number", "fail");
		}

		selectText(drop_currency, currencyLKR);
		String selectedCurrency = getSelectedOptionInDropdown(drop_currency);
		if (selectedCurrency.equals(currencyLKR)) {
			writeTestResults("Verify that user able to select currency as LKR",
					"User should be able to select currency as LKR", "User successfully select currency as LKR",
					"pass");
		} else {
			writeTestResults("Verify that user able to select currency as LKR",
					"User should be able to select currency as LKR", "User doesn't select currency as LKR", "fail");
		}

		sendKeys(txt_floatCashBankBook, tenThousand);
		String enteredFloat = getAttribute(txt_floatCashBankBook, "value");
		if (enteredFloat.equals(tenThousand)) {
			writeTestResults("Verify that user able to enter Float", "User should be able to enter Float",
					"User successfully enter Float", "pass");
		} else {
			writeTestResults("Verify that user able to enter Float", "User should be able to enter Float",
					"User doesn't enter Float", "fail");
		}

		click(btn_permissionGroupLookupCashBankBook);
		getInvObj().handeledSendKeys(txt_searcUserGroup, permissionGroupCBB);
		pressEnter(txt_searcUserGroup);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", permissionGroupCBB), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", permissionGroupCBB));
		Thread.sleep(2000);
		String chousedPermissionGroup = getAttribute(txt_permissionGroupFrontPageCBB, "value");
		if (chousedPermissionGroup.equals(permissionGroupCBB)) {
			writeTestResults("Verify that user able to choose Permission Group",
					"User should be able to choose Permission Group", "User successfully choose Permission Group",
					"pass");
		} else {
			writeTestResults("Verify that user able to choose Permission Group",
					"User should be able to choose Permission Group", "User doesn't choose Permission Group", "fail");
		}

		if (!isEnabled(drop_bankCodeCBB)) {
			writeTestResults("Verify that Bank Code is disabled", "Bank Code should be disabled",
					"Bank Code successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Bank Code is disabled", "Bank Code should be disabled",
					"Bank Code doesn't diasabled", "fail");
		}

		if (!isEnabled(txt_companyStatementCBB)) {
			writeTestResults("Verify that Company Statement is disabled", "Company Statement should be disabled",
					"Company Statement successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Company Statement is disabled", "Company Statement should be disabled",
					"Company Statement doesn't diasabled", "fail");
		}

		if (!isEnabled(txt_bankBranchCBB)) {
			writeTestResults("Verify that Bank Branch is disabled", "Bank Branch should be disabled",
					"Bank Branch successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Bank Branch is disabled", "Bank Branch should be disabled",
					"Bank Branch doesn't diasabled", "fail");
		}

		if (!isEnabled(drop_accountTypeCBB)) {
			writeTestResults("Verify that Account Type is disabled", "Account Type should be disabled",
					"Account Type successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Account Type is disabled", "Account Type should be disabled",
					"Account Type doesn't diasabled", "fail");
		}

		if (!isEnabled(txt_telephoneNumberCBB)) {
			writeTestResults("Verify that Telephone Number is disabled", "Telephone Number should be disabled",
					"Telephone Number successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Telephone Number is disabled", "Telephone Number should be disabled",
					"Telephone Number doesn't diasabled", "fail");
		}

		if (!isEnabled(txt_faxCBB)) {
			writeTestResults("Verify that Fax Number is disabled", "Fax Number should be disabled",
					"Fax Number successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Fax Number is disabled", "Fax Number should be disabled",
					"Fax Number doesn't diasabled", "fail");
		}

		if (!isEnabled(txt_emailCBB)) {
			writeTestResults("Verify that Email is disabled", "Email should be disabled",
					"Email successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Email is disabled", "Email should be disabled", "Email doesn't diasabled",
					"fail");
		}

		if (!isEnabled(txt_webURLCBB)) {
			writeTestResults("Verify that Web URL is disabled", "Web URL should be disabled",
					"Web URL successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Web URL is disabled", "Web URL should be disabled",
					"Web URL doesn't diasabled", "fail");
		}

		if (!isEnabled(chk_moreCurrenciesCBB)) {
			writeTestResults("Verify that More Currencies check box is disabled",
					"More Currencies check box should be disabled", "More Currencies check box successfully diasabled",
					"pass");
		} else {
			writeTestResults("Verify that More Currencies check box is disabled",
					"More Currencies check box should be disabled", "More Currencies check box doesn't diasabled",
					"fail");
		}

		if (!isEnabled(txt_addressCBB)) {
			writeTestResults("Verify that Address is disabled", "Address should be disabled",
					"Address successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Address is disabled", "Address should be disabled",
					"Address doesn't diasabled", "fail");
		}

		if (!isEnabled(txt_discountingMaximumCBB)) {
			writeTestResults("Verify that Discounting Maximum is disabled", "Discounting Maximum should be disabled",
					"Discounting Maximum successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Discounting Maximum is disabled", "Discounting Maximum should be disabled",
					"Discounting Maximum doesn't diasabled", "fail");
		}

		if (!isEnabled(txt_activeFromCBB)) {
			writeTestResults("Verify that Active From is disabled", "Active From should be disabled",
					"Active From successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Active From is disabled", "Active From should be disabled",
					"Active From doesn't diasabled", "fail");
		}

		if (!isEnabled(txt_activeToCBB)) {
			writeTestResults("Verify that Active To is disabled", "Active To should be disabled",
					"Active To successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Active To is disabled", "Active To should be disabled",
					"Active To doesn't diasabled", "fail");
		}

		if (!isEnabled(txt_deawerCBB)) {
			writeTestResults("Verify that Drawer is disabled", "Drawer should be disabled",
					"Drawer successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Drawer is disabled", "Drawer should be disabled", "Drawer doesn't diasabled",
					"fail");
		}

		if (!isEnabled(txt_swiftCodeCBB)) {
			writeTestResults("Verify that Swift Code is disabled", "Swift Code should be disabled",
					"Swift Code successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Swift Code is disabled", "Swift Code should be disabled",
					"Swift Code doesn't diasabled", "fail");
		}

		if (!isEnabled(chk_iouCBB)) {
			writeTestResults("Verify that IOU Reimbursement check box is disabled",
					"IOU Reimbursement check box should be disabled",
					"IOU Reimbursement check box successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that IOU Reimbursement check box is disabled",
					"IOU Reimbursement check box should be disabled", "IOU Reimbursement check box doesn't diasabled",
					"fail");
		}

		if (!isEnabled(chk_leaseFacilityCBB)) {
			writeTestResults("Verify that Lease Facility check box is disabled",
					"Lease Facility check box should be disabled", "Lease Facility check box successfully diasabled",
					"pass");
		} else {
			writeTestResults("Verify that Lease Facility check box is disabled",
					"Lease Facility check box should be disabled", "Lease Facility check box doesn't diasabled",
					"fail");
		}

		if (!isEnabled(chk_leaseFacilityCBB)) {
			writeTestResults("Verify that Lease Facility check box is disabled",
					"Lease Facility check box should be disabled", "Lease Facility check box successfully diasabled",
					"pass");
		} else {
			writeTestResults("Verify that Lease Facility check box is disabled",
					"Lease Facility check box should be disabled", "Lease Facility check box doesn't diasabled",
					"fail");
		}

		click(btn_draft);
		explicitWait(header_draftedCashBankBook, 40);
		if (isDisplayed(header_draftedCashBankBook)) {
			writeTestResults("Verify user can draft the Cash /Bank Book",
					"User should be able to draft the Cash /Bank Book", "User successfully draft the Cash /Bank Book",
					"pass");
		} else {
			writeTestResults("Verify user can draft the Cash /Bank Book",
					"User should be able to draft the Cash /Bank Book", "User doesn't draft the Cash /Bank Book",
					"fail");
		}

		explicitWait(btn_reelese, 20);
		click(btn_reelese);
		explicitWait(header_releasedCashBankBook, 40);
		trackCode = getText(lbl_docNoCashBankBook);
		getFinObj().writeFinanceData("AccountNumber_FIN_CBB_1_4_5_6_10", trackCode, 30);
		releasedCashBankBook_FIN_CBB_1_4_5_6_10 = trackCode;
		if (isDisplayed(header_releasedCashBankBook)) {
			writeTestResults("Verify user can release the Cash /Bank Book",
					"User should be able to release the Cash /Bank Book",
					"User successfully release the Cash /Bank Book", "pass");
		} else {
			writeTestResults("Verify user can release the Cash /Bank Book",
					"User should be able to release the Cash /Bank Book", "User doesn't release the Cash /Bank Book",
					"fail");
		}

		getFinObj().writeFinanceData("ReleaseCashBookUrl_FIN_CBB_1_4_5_6_10", getCurrentUrl(), 32);

	}

	public void findReleasedCashBook_FIN_CBB_1_4_5_6_10() throws Exception {
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_cashBankBook, 20);
		if (isDisplayed(btn_cashBankBook)) {
			writeTestResults("Verify that Cash/Bank Book option is available under Finance Module",
					"Cash/Bank Book option should be available under Finance Module",
					"Cash/Bank Book option successfully available under Finance Module", "pass");
		} else {
			writeTestResults("Verify that Cash/Bank Book option is available under Finance Module",
					"Cash/Bank Book option should be available under Finance Module",
					"Cash/Bank Book option successfully available under Finance Module", "fail");
		}

		if (isEnabled(btn_cashBankBook)) {
			writeTestResults("Verify user able to click on Cash/Bank Book option",
					"User should be able to click on Cash/Bank Book option",
					"User able to click on Cash/Bank Book option", "pass");
		} else {
			writeTestResults("Verify user able to click on Cash/Bank Book option",
					"User should be able to click on Cash/Bank Book option",
					"User not able to click on Cash/Bank Book option", "fail");
		}

		click(btn_cashBankBook);

		explicitWait(header_cashBankBookByPage, 40);
		if (isDisplayed(header_cashBankBookByPage)) {
			writeTestResults("Verify that user was directed to by-page Cash/Bank Book",
					"User should directed to by-page Cash/Bank Book",
					"User successfully directed to by-page Cash/Bank Book", "pass");
		} else {
			writeTestResults("Verify that user was directed to by-page Cash/Bank Book",
					"User should directed to by-page Cash/Bank Book", "User doesn't directed to by-page Cash/Bank Book",
					"fail");
		}
		sendKeys(txt_searchCashBankBook, releasedCashBankBook_FIN_CBB_1_4_5_6_10);
		click(btn_searchOnByPage);
		Thread.sleep(2000);
		explicitWait(lnk_resultDocOnByPage.replace("doc_no", releasedCashBankBook_FIN_CBB_1_4_5_6_10), 40);

		int rowCountCashBankBook = getCount(count_cashBannkBookByPage);
		if (rowCountCashBankBook == 1) {
			writeTestResults("Verify that only relevent record is loaded", "Only relevent record shoulb be loaded",
					"Only relevent record successfully loaded", "pass");
		} else {
			writeTestResults("Verify that only relevent record is loaded", "Only relevent record shoulb be loaded",
					"Only relevent record dorsn't loaded", "fail");
		}

		click(lnk_resultDocOnByPage.replace("doc_no", releasedCashBankBook_FIN_CBB_1_4_5_6_10));

		explicitWait(header_releasedCashBankBookDocReplace.replace("doc_no", releasedCashBankBook_FIN_CBB_1_4_5_6_10),
				40);
		if (isDisplayed(
				header_releasedCashBankBookDocReplace.replace("doc_no", releasedCashBankBook_FIN_CBB_1_4_5_6_10))) {
			writeTestResults("Verify that Bank Books that matches with the entered number was get retrieved.",
					"Bank Books that matches with the entered number should get retrieved.",
					"Bank Books that matches with the entered number successfully get retrieved.", "pass");
		} else {
			writeTestResults("Verify that Bank Books that matches with the entered number was get retrieved.",
					"Bank Books that matches with the entered number should get retrieved.",
					"Bank Books that matches with the entered number doesn't get retrieved.", "fail");
		}
	}

	/* FIN_CBB_2 */
	public void configureUserPermissionAndCheckCashBankBook_FIN_CBB_2() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 30);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_cashBankBook, 10);
		if (isDisplayed(btn_cashBankBook)) {
			writeTestResults("Verify Cash Bank Book option is displayed", "Cash Bank Book option should displayed",
					"Cash Bank Book option successfully displayed", "pass");
		} else {
			writeTestResults("Verify Cash Bank Book option is displayed", "Cash Bank Book option should displayed",
					"Cash Bank Book option doesn't displayed", "fail");
		}

		((JavascriptExecutor) driver).executeScript("window.open('about:blank','_blank');");
		switchWindow();
		openPage(site_Url);
		explicitWait(navigation_pane, 20);
		Thread.sleep(2000);
		explicitWait(navigation_pane, 20);
		click(navigation_pane);
		explicitWait(btn_adminModule, 20);
		click(btn_adminModule);
		explicitWait(btn_userPermission, 20);
		click(btn_userPermission);

		explicitWait(btn_userLookupUserPermission, 30);
		click(btn_userLookupUserPermission);
		getInvObj().handeledSendKeys(txt_userSearch, logUserEmail);
		pressEnter(txt_userSearch);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", logUserEmail), 30);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", logUserEmail));
		Thread.sleep(2000);

		selectText(drop_moduleNameUserPermission, moduleFinance);

		explicitWait(chk_reverseCashBankBookUserPermission, 40);
		if (isSelected(chk_reverseCashBankBookUserPermission)) {
			click(chk_reverseCashBankBookUserPermission);
		}
		if (isSelected(chk_releaseCashBankBookUserPermission)) {
			click(chk_releaseCashBankBookUserPermission);
		}
		if (isSelected(chk_viewCashBankBookUserPermission)) {
			click(chk_viewCashBankBookUserPermission);
		}
		if (isSelected(chk_allowTempCashBankBookUserPermission)) {
			click(chk_allowTempCashBankBookUserPermission);
		}

		click(btn_update);
		Thread.sleep(2500);

		explicitWait(chk_reverseCashBankBookUserPermission, 20);
		if (!isSelected(chk_reverseCashBankBookUserPermission) & !isSelected(chk_releaseCashBankBookUserPermission)
				& !isSelected(chk_viewCashBankBookUserPermission)
				& !isSelected(chk_allowTempCashBankBookUserPermission)) {
			writeTestResults("Verify that all permissions were get removed for 'Cash/Bank Book'",
					"All permissions should get removed for 'Cash/Bank Book'",
					"All permissions successfully get removed for 'Cash/Bank Book'", "pass");
		} else {
			writeTestResults("Verify that all permissions were get removed for 'Cash/Bank Book'",
					"All permissions should get removed for 'Cash/Bank Book'",
					"All permissions not get removed for 'Cash/Bank Book'", "fail");
		}

		switchWindow();
		click(btn_cashBankBook);
		explicitWait(lbl_noPermissionValidation, 50);
		if (isDisplayed(lbl_noPermissionValidation)) {
			writeTestResults(
					"Verify user able to receive error message stating \"You have no permission to access this page. Please grant permission from administrator.\"",
					"User should be able to receive error message stating \"You have no permission to access this page. Please grant permission from administrator.\"",
					"User successfully receive error message stating \"You have no permission to access this page. Please grant permission from administrator.\"",
					"pass");
		} else {
			writeTestResults(
					"Verify user able to receive error message stating \"You have no permission to access this page. Please grant permission from administrator.\"",
					"User should be able to receive error message stating \"You have no permission to access this page. Please grant permission from administrator.\"",
					"User doesn't receive error message stating \"You have no permission to access this page. Please grant permission from administrator.\"",
					"fail");
		}

	}

	public void resetCashBankBookUserPermissions_FIN_CBB_2() throws Exception {
		switchWindow();
		explicitWait(chk_reverseCashBankBookUserPermission, 40);
		if (isSelected(chk_viewCashBankBookUserPermission)) {
			click(chk_viewCashBankBookUserPermission);
		}
		if (!isSelected(chk_reverseCashBankBookUserPermission)) {
			click(chk_reverseCashBankBookUserPermission);
		}
		if (!isSelected(chk_releaseCashBankBookUserPermission)) {
			click(chk_releaseCashBankBookUserPermission);
		}
		if (!isSelected(chk_allowTempCashBankBookUserPermission)) {
			click(chk_allowTempCashBankBookUserPermission);
		}

		click(btn_update);
		Thread.sleep(2500);
	}

	/* FIN_CBB_3 */
	public void checkAllowTemplateOptionUserPermission_FIN_CBB_3() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		explicitWait(navigation_pane, 20);
		Thread.sleep(2000);
		explicitWait(navigation_pane, 20);
		click(navigation_pane);
		explicitWait(btn_adminModule, 20);
		click(btn_adminModule);
		explicitWait(btn_userPermission, 20);
		click(btn_userPermission);

		explicitWait(btn_userLookupUserPermission, 30);
		click(btn_userLookupUserPermission);
		getInvObj().handeledSendKeys(txt_userSearch, logUserEmail);
		pressEnter(txt_userSearch);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", logUserEmail), 30);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", logUserEmail));
		Thread.sleep(2000);

		selectText(drop_moduleNameUserPermission, moduleFinance);

		explicitWait(chk_reverseCashBankBookUserPermission, 40);

		if (isSelected(chk_allowTempCashBankBookUserPermission)) {
			click(chk_allowTempCashBankBookUserPermission);
		}
		if (isSelected(chk_viewCashBankBookUserPermission)) {
			click(chk_viewCashBankBookUserPermission);
		}
		if (!isSelected(chk_reverseCashBankBookUserPermission)) {
			click(chk_reverseCashBankBookUserPermission);
		}
		if (!isSelected(chk_releaseCashBankBookUserPermission)) {
			click(chk_releaseCashBankBookUserPermission);
		}

		click(btn_update);
		Thread.sleep(2500);

		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_cashBankBook, 10);
		click(btn_cashBankBook);
		explicitWait(drop_templateOnByPage, 50);

		List<String> bankAdjustmetTemplateDropdown = new ArrayList<String>();
		Select dropTemplate = new Select(driver.findElement(By.xpath(drop_templateOnByPage)));
		List<WebElement> op = dropTemplate.getOptions();
		int size = op.size();
		for (int i = 0; i < size; i++) {
			String options = op.get(i).getText();
			bankAdjustmetTemplateDropdown.add(options);
		}

		if (bankAdjustmetTemplateDropdown.contains(myTemplateOption)
				& !bankAdjustmetTemplateDropdown.contains(allOption)) {
			writeTestResults(
					"Verify user able to accessible to \"My Template\" only when \"Allow Template\" is unticked",
					"User should be able to accessible to \"My Template\" only when \"Allow Template\" is unticked",
					"User able to accessible to \"My Template\" only when \"Allow Template\" is unticked", "pass");
		} else {
			writeTestResults(
					"Verify user able to accessible to \"My Template\" only when \"Allow Template\" is unticked",
					"User should be able to accessible to \"My Template\" only when \"Allow Template\" is unticked",
					"User able to accessible to not only \"My Template\" when \"Allow Template\" is unticked", "fail");
		}

		explicitWait(navigation_pane, 20);
		Thread.sleep(2000);
		explicitWait(navigation_pane, 20);
		click(navigation_pane);
		explicitWait(btn_adminModule, 20);
		click(btn_adminModule);
		explicitWait(btn_userPermission, 20);
		click(btn_userPermission);

		explicitWait(btn_userLookupUserPermission, 30);
		click(btn_userLookupUserPermission);
		getInvObj().handeledSendKeys(txt_userSearch, logUserEmail);
		pressEnter(txt_userSearch);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", logUserEmail), 30);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", logUserEmail));
		Thread.sleep(2000);

		selectText(drop_moduleNameUserPermission, moduleFinance);

		explicitWait(chk_reverseCashBankBookUserPermission, 40);

		if (!isSelected(chk_allowTempCashBankBookUserPermission)) {
			click(chk_allowTempCashBankBookUserPermission);
		}

		click(btn_update);
		Thread.sleep(2500);

		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_cashBankBook, 10);
		click(btn_cashBankBook);

		explicitWait(drop_templateOnByPage, 50);

		List<String> bankAdjustmetTemplateDropdownRetick = new ArrayList<String>();
		Select dropTemplateRetick = new Select(driver.findElement(By.xpath(drop_templateOnByPage)));
		List<WebElement> opRetick = dropTemplateRetick.getOptions();
		int sizeRetick = opRetick.size();
		for (int i = 0; i < sizeRetick; i++) {
			String options = opRetick.get(i).getText();
			bankAdjustmetTemplateDropdownRetick.add(options);
		}

		if (bankAdjustmetTemplateDropdownRetick.contains(myTemplateOption)
				& bankAdjustmetTemplateDropdownRetick.contains(allOption)) {
			writeTestResults("Verify user able to accessible to \"My Template\" an All \"Allow Template\" is unticked",
					"User should be able to accessible to \"My Template\" an All \"Allow Template\" is ticked",
					"User able to accessible to \"My Template\" an All \"Allow Template\" is ticked", "pass");
		} else {
			writeTestResults("Verify user able to accessible to \"My Template\" an All \"Allow Template\" is ticked",
					"User should be able to accessible to \"My Template\" an All \"Allow Template\" is ticked",
					"User able to accessible to only \"My Template\" when \"Allow Template\" is ticked", "fail");
		}
	}

	/* FIN_CBB_7 */
	public void bankBookWithReleventFields_FIN_CBB_7() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_cashBankBook, 20);
		if (isDisplayed(btn_cashBankBook)) {
			writeTestResults("Verify that Cash/Bank Book option is available under Finance Module",
					"Cash/Bank Book option should be available under Finance Module",
					"Cash/Bank Book option successfully available under Finance Module", "pass");
		} else {
			writeTestResults("Verify that Cash/Bank Book option is available under Finance Module",
					"Cash/Bank Book option should be available under Finance Module",
					"Cash/Bank Book option successfully available under Finance Module", "fail");
		}

		if (isEnabled(btn_cashBankBook)) {
			writeTestResults("Verify user able to click on Cash/Bank Book option",
					"User should be able to click on Cash/Bank Book option",
					"User able to click on Cash/Bank Book option", "pass");
		} else {
			writeTestResults("Verify user able to click on Cash/Bank Book option",
					"User should be able to click on Cash/Bank Book option",
					"User not able to click on Cash/Bank Book option", "fail");
		}

		click(btn_cashBankBook);

		explicitWait(header_cashBankBookByPage, 40);
		if (isDisplayed(header_cashBankBookByPage)) {
			writeTestResults("Verify that user was directed to by-page Cash/Bank Book",
					"User should directed to by-page Cash/Bank Book",
					"User successfully directed to by-page Cash/Bank Book", "pass");
		} else {
			writeTestResults("Verify that user was directed to by-page Cash/Bank Book",
					"User should directed to by-page Cash/Bank Book", "User doesn't directed to by-page Cash/Bank Book",
					"fail");
		}

		click(btn_newCashBankBook);
		explicitWait(header_newCashBankBook, 40);
		if (isDisplayed(header_newCashBankBook)) {
			writeTestResults("Verify that Cash/Bank Book  - New page is displayed",
					"Cash/Bank Book  - New page should be displayed",
					"Cash/Bank Book  - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify that Cash/Bank Book  - New page is displayed",
					"Cash/Bank Book  - New page should be displayed", "Cash/Bank Book  - New page doesn't displayed",
					"fail");
		}

		selectText(drop_typeCashBankBook, cashBankBookTypeBank);
		String selectedBookType = getSelectedOptionInDropdown(drop_typeCashBankBook);
		if (selectedBookType.equals(cashBankBookTypeBank)) {
			writeTestResults("Verify that user able to select type as Bank",
					"User should be able to select type as Bank", "User successfully select type as Bank", "pass");
		} else {
			writeTestResults("Verify that user able to select type as Bank",
					"User should be able to select type as Bank", "User doesn't select type as Bank", "fail");
		}

		selectText(drop_bankCodeCBB, sampathBank);
		String selectedBankCode = getSelectedOptionInDropdown(drop_bankCodeCBB);
		if (selectedBankCode.equals(sampathBank)) {
			writeTestResults("Verify that user able to select bank code as Sampath",
					"User should be able to select bank code as Sampath",
					"User successfully select bank code as Sampath", "pass");
		} else {
			writeTestResults("Verify that user able to select bank code as Sampath",
					"User should be able to select bank code as Sampath", "User doesn't select bank code as Sampath",
					"fail");
		}

		String accountNumber = getInvObj().currentTimeAndDate().replace("/", "").replace(":", "");
		sendKeys(txt_bankAccountNoCashBankBook, accountNumber);
		String setAccountNumber = getAttribute(txt_bankAccountNoCashBankBook, "value");
		if (setAccountNumber.equals(accountNumber)) {
			writeTestResults("Verify that user able to enter Account Number",
					"User should be able to enter Account Number", "User successfully enter Account Number", "pass");
		} else {
			writeTestResults("Verify that user able to enter Account Number",
					"User should be able to enter Account Number", "User doesn't enter Account Number", "fail");
		}

		sendKeys(txt_companyStatementCBB, companyStatement);
		String enteredCompanyStatement = getAttribute(txt_companyStatementCBB, "value");
		if (enteredCompanyStatement.equals(companyStatement)) {
			writeTestResults("Verify that user able to enter Company Statement",
					"User should be able to enter Company Statement", "User successfully enter Company Statement",
					"pass");
		} else {
			writeTestResults("Verify that user able to enter Company Statement",
					"User should be able to enter Company Statement", "User doesn't enter Company Statement", "fail");
		}

		sendKeys(txt_bankBranchCBB, bankBranchNegombo);
		String enteredBankBranch = getAttribute(txt_bankBranchCBB, "value");
		if (enteredBankBranch.equals(bankBranchNegombo)) {
			writeTestResults("Verify that user able to enter Bank Branch", "User should be able to enter Bank Branch",
					"User successfully enter Bank Branch", "pass");
		} else {
			writeTestResults("Verify that user able to enter Bank Branch", "User should be able to enter Bank Branch",
					"User doesn't enter Bank Branch", "fail");
		}

		selectText(drop_accountTypeCBB, accountTypeCBB);
		String selectedAccountType = getSelectedOptionInDropdown(drop_accountTypeCBB);
		if (selectedAccountType.equals(accountTypeCBB)) {
			writeTestResults("Verify that user able to select account type as Current Account",
					"User should be able to select account type as Current Account",
					"User successfully select account type as Current Account", "pass");
		} else {
			writeTestResults("Verify that user able to select account type as Current Account",
					"User should be able to select account type as Current Account",
					"User doesn't select account type as Current Account", "fail");
		}

		sendKeys(txt_telephoneNumberCBB, telephone);
		String enteredTelephone = getAttribute(txt_telephoneNumberCBB, "value");
		if (enteredTelephone.equals(telephone)) {
			writeTestResults("Verify that user able to enter Telephone Number",
					"User should be able to enter Telephone Number", "User successfully enter Telephone Number",
					"pass");
		} else {
			writeTestResults("Verify that user able to enter Telephone Number",
					"User should be able to enter Telephone Number", "User doesn't enter Telephone Number", "fail");
		}

		sendKeys(txt_faxCBB, fax);
		String enteredfax = getAttribute(txt_faxCBB, "value");
		if (enteredfax.equals(fax)) {
			writeTestResults("Verify that user able to enter Fax Number", "User should be able to enter Fax Number",
					"User successfully enter Fax Number", "pass");
		} else {
			writeTestResults("Verify that user able to enter Fax Number", "User should be able to enter Fax Number",
					"User doesn't enter Fax Number", "fail");
		}

		sendKeys(txt_emailCBB, email);
		String enteredMail = getAttribute(txt_emailCBB, "value");
		if (enteredMail.equals(email)) {
			writeTestResults("Verify that user able to enter Email", "User should be able to enter Email",
					"User successfully enter Email", "pass");
		} else {
			writeTestResults("Verify that user able to enter Email", "User should be able to enter Email",
					"User doesn't enter Email", "fail");
		}

		sendKeys(txt_webURLCBB, webUrl);
		String enteredWebUrl = getAttribute(txt_webURLCBB, "value");
		if (enteredWebUrl.equals(webUrl)) {
			writeTestResults("Verify that user able to enter Web URL", "User should be able to enter Web URL",
					"User successfully enter Web URL", "pass");
		} else {
			writeTestResults("Verify that user able to enter Web URL", "User should be able to enter Web URL",
					"User doesn't enter Web URL", "fail");
		}

		sendKeys(txt_addressCBB, address);
		String enteredAddress = getAttribute(txt_addressCBB, "value");
		if (enteredAddress.equals(address)) {
			writeTestResults("Verify that user able to enter Address", "User should be able to enter Address",
					"User successfully enter Address", "pass");
		} else {
			writeTestResults("Verify that user able to enter Address", "User should be able to enter Address",
					"User doesn't enter Address", "fail");
		}

		selectText(drop_currency, currencyLKR);
		String selectedCurrency = getSelectedOptionInDropdown(drop_currency);
		if (selectedCurrency.equals(currencyLKR)) {
			writeTestResults("Verify that user able to select currency as LKR",
					"User should be able to select currency as LKR", "User successfully select currency as LKR",
					"pass");
		} else {
			writeTestResults("Verify that user able to select currency as LKR",
					"User should be able to select currency as LKR", "User doesn't select currency as LKR", "fail");
		}

		sendKeys(txt_floatCashBankBook, tenThousand);
		String enteredFloat = getAttribute(txt_floatCashBankBook, "value");
		if (enteredFloat.equals(tenThousand)) {
			writeTestResults("Verify that user able to enter Float", "User should be able to enter Float",
					"User successfully enter Float", "pass");
		} else {
			writeTestResults("Verify that user able to enter Float", "User should be able to enter Float",
					"User doesn't enter Float", "fail");
		}

		sendKeys(txt_odLimit, amountFiftThousand);
		String enteredOdLimit = getAttribute(txt_odLimit, "value");
		if (enteredOdLimit.equals(amountFiftThousand)) {
			writeTestResults("Verify that user able to enter OD Limit", "User should be able to enter OD Limit",
					"User successfully enter OD Limit", "pass");
		} else {
			writeTestResults("Verify that user able to enter OD Limit", "User should be able to enter OD Limit",
					"User doesn't enter OD Limit", "fail");
		}

		sendKeys(txt_discountingMaximumCBB, amountFiftThousand);
		String enteredDiscountingMax = getAttribute(txt_discountingMaximumCBB, "value");
		if (enteredDiscountingMax.equals(amountFiftThousand)) {
			writeTestResults("Verify that user able to enter Discounting Maximum",
					"User should be able to enter Discounting Maximum", "User successfully enter Discounting Maximum",
					"pass");
		} else {
			writeTestResults("Verify that user able to enter Discounting Maximum",
					"User should be able to enter Discounting Maximum", "User doesn't enter Discounting Maximum",
					"fail");
		}

		sendKeys(txt_deawerCBB, drawer);
		String enteredDrawer = getAttribute(txt_deawerCBB, "value");
		if (enteredDrawer.equals(drawer)) {
			writeTestResults("Verify that user able to enter Drawer", "User should be able to enter Drawer",
					"User successfully enter Drawer", "pass");
		} else {
			writeTestResults("Verify that user able to enter Drawer", "User should be able to enter Drawer",
					"User doesn't enter Drawer", "fail");
		}

		sendKeys(txt_swiftCodeCBB, swiftCode);
		String enteredSwiftCode = getAttribute(txt_swiftCodeCBB, "value");
		if (enteredSwiftCode.equals(swiftCode)) {
			writeTestResults("Verify that user able to enter Swift Code", "User should be able to enter Swift Code",
					"User successfully enter Swift Code", "pass");
		} else {
			writeTestResults("Verify that user able to enter Swift Code", "User should be able to enter Swift Code",
					"User doesn't enter Swift Code", "fail");
		}

		click(btn_permissionGroupLookupCashBankBook);
		getInvObj().handeledSendKeys(txt_searcUserGroup, permissionGroupCBB);
		pressEnter(txt_searcUserGroup);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", permissionGroupCBB), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", permissionGroupCBB));
		Thread.sleep(2000);
		String chousedPermissionGroup = getAttribute(txt_permissionGroupFrontPageCBB, "value");
		if (chousedPermissionGroup.equals(permissionGroupCBB)) {
			writeTestResults("Verify that user able to choose Permission Group",
					"User should be able to choose Permission Group", "User successfully choose Permission Group",
					"pass");
		} else {
			writeTestResults("Verify that user able to choose Permission Group",
					"User should be able to choose Permission Group", "User doesn't choose Permission Group", "fail");
		}

		click(btn_draft);
		explicitWait(header_draftedCashBankBook, 40);
		if (isDisplayed(header_draftedCashBankBook)) {
			writeTestResults("Verify user can draft the Cash /Bank Book",
					"User should be able to draft the Cash /Bank Book", "User successfully draft the Cash /Bank Book",
					"pass");
		} else {
			writeTestResults("Verify user can draft the Cash /Bank Book",
					"User should be able to draft the Cash /Bank Book", "User doesn't draft the Cash /Bank Book",
					"fail");
		}

		explicitWait(btn_reelese, 20);
		click(btn_reelese);
		explicitWait(header_releasedCashBankBook, 40);
		trackCode = getText(lbl_docNoCashBankBook);
		if (isDisplayed(header_releasedCashBankBook)) {
			writeTestResults("Verify user can release the Cash /Bank Book",
					"User should be able to release the Cash /Bank Book",
					"User successfully release the Cash /Bank Book", "pass");
		} else {
			writeTestResults("Verify user can release the Cash /Bank Book",
					"User should be able to release the Cash /Bank Book", "User doesn't release the Cash /Bank Book",
					"fail");
		}

	}

	/* FIN_CBB_8 */
	public void cashBankBookPettyCash_FIN_CBB_8() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_cashBankBook, 20);
		if (isDisplayed(btn_cashBankBook)) {
			writeTestResults("Verify that Cash/Bank Book option is available under Finance Module",
					"Cash/Bank Book option should be available under Finance Module",
					"Cash/Bank Book option successfully available under Finance Module", "pass");
		} else {
			writeTestResults("Verify that Cash/Bank Book option is available under Finance Module",
					"Cash/Bank Book option should be available under Finance Module",
					"Cash/Bank Book option successfully available under Finance Module", "fail");
		}

		if (isEnabled(btn_cashBankBook)) {
			writeTestResults("Verify user able to click on Cash/Bank Book option",
					"User should be able to click on Cash/Bank Book option",
					"User able to click on Cash/Bank Book option", "pass");
		} else {
			writeTestResults("Verify user able to click on Cash/Bank Book option",
					"User should be able to click on Cash/Bank Book option",
					"User not able to click on Cash/Bank Book option", "fail");
		}

		click(btn_cashBankBook);

		explicitWait(header_cashBankBookByPage, 40);
		if (isDisplayed(header_cashBankBookByPage)) {
			writeTestResults("Verify that user was directed to by-page Cash/Bank Book",
					"User should directed to by-page Cash/Bank Book",
					"User successfully directed to by-page Cash/Bank Book", "pass");
		} else {
			writeTestResults("Verify that user was directed to by-page Cash/Bank Book",
					"User should directed to by-page Cash/Bank Book", "User doesn't directed to by-page Cash/Bank Book",
					"fail");
		}

		click(btn_newCashBankBook);
		explicitWait(header_newCashBankBook, 40);
		if (isDisplayed(header_newCashBankBook)) {
			writeTestResults("Verify that Cash/Bank Book  - New page is displayed",
					"Cash/Bank Book  - New page should be displayed",
					"Cash/Bank Book  - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify that Cash/Bank Book  - New page is displayed",
					"Cash/Bank Book  - New page should be displayed", "Cash/Bank Book  - New page doesn't displayed",
					"fail");
		}

		selectText(drop_typeCashBankBook, bookTypePettyCash);
		String selectedBookType = getSelectedOptionInDropdown(drop_typeCashBankBook);
		if (selectedBookType.equals(bookTypePettyCash)) {
			writeTestResults("Verify that user able to select type as Petty Cash",
					"User should be able to select type as Petty Cash", "User successfully select type as Petty Cash",
					"pass");
		} else {
			writeTestResults("Verify that user able to select type as Petty Cash",
					"User should be able to select type as Petty Cash", "User doesn't select type as Petty Cash",
					"fail");
		}

		String accountNumber = getInvObj().currentTimeAndDate().replace("/", "").replace(":", "");
		sendKeys(txt_bankAccountNoCashBankBook, accountNumber);
		String setAccountNumber = getAttribute(txt_bankAccountNoCashBankBook, "value");
		if (setAccountNumber.equals(accountNumber)) {
			writeTestResults("Verify that user able to enter Account Number",
					"User should be able to enter Account Number", "User successfully enter Account Number", "pass");
		} else {
			writeTestResults("Verify that user able to enter Account Number",
					"User should be able to enter Account Number", "User doesn't enter Account Number", "fail");
		}

		selectText(drop_currency, currencyLKR);
		String selectedCurrency = getSelectedOptionInDropdown(drop_currency);
		if (selectedCurrency.equals(currencyLKR)) {
			writeTestResults("Verify that user able to select currency as LKR",
					"User should be able to select currency as LKR", "User successfully select currency as LKR",
					"pass");
		} else {
			writeTestResults("Verify that user able to select currency as LKR",
					"User should be able to select currency as LKR", "User doesn't select currency as LKR", "fail");
		}

		sendKeys(txt_floatCashBankBook, tenThousand);
		String enteredFloat = getAttribute(txt_floatCashBankBook, "value");
		if (enteredFloat.equals(tenThousand)) {
			writeTestResults("Verify that user able to enter Float", "User should be able to enter Float",
					"User successfully enter Float", "pass");
		} else {
			writeTestResults("Verify that user able to enter Float", "User should be able to enter Float",
					"User doesn't enter Float", "fail");
		}

		click(btn_permissionGroupLookupCashBankBook);
		getInvObj().handeledSendKeys(txt_searcUserGroup, permissionGroupCBB);
		pressEnter(txt_searcUserGroup);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", permissionGroupCBB), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", permissionGroupCBB));
		Thread.sleep(2000);
		String chousedPermissionGroup = getAttribute(txt_permissionGroupFrontPageCBB, "value");
		if (chousedPermissionGroup.equals(permissionGroupCBB)) {
			writeTestResults("Verify that user able to choose Permission Group",
					"User should be able to choose Permission Group", "User successfully choose Permission Group",
					"pass");
		} else {
			writeTestResults("Verify that user able to choose Permission Group",
					"User should be able to choose Permission Group", "User doesn't choose Permission Group", "fail");
		}

		click(chk_iouCBB);
		if (isSelected(chk_iouCBB)) {
			writeTestResults("Verify that IOU Reimbursement check box is selected",
					"IOU Reimbursement check box should be selected",
					"IOU Reimbursement check box successfully selected", "pass");
		} else {
			writeTestResults("Verify that IOU Reimbursement check box is selected",
					"IOU Reimbursement check box should be selected", "IOU Reimbursement check box doesn't selected",
					"fail");
		}

		if (!isEnabled(drop_bankCodeCBB)) {
			writeTestResults("Verify that Bank Code is disabled", "Bank Code should be disabled",
					"Bank Code successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Bank Code is disabled", "Bank Code should be disabled",
					"Bank Code doesn't diasabled", "fail");
		}

		if (!isEnabled(txt_companyStatementCBB)) {
			writeTestResults("Verify that Company Statement is disabled", "Company Statement should be disabled",
					"Company Statement successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Company Statement is disabled", "Company Statement should be disabled",
					"Company Statement doesn't diasabled", "fail");
		}

		if (!isEnabled(txt_bankBranchCBB)) {
			writeTestResults("Verify that Bank Branch is disabled", "Bank Branch should be disabled",
					"Bank Branch successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Bank Branch is disabled", "Bank Branch should be disabled",
					"Bank Branch doesn't diasabled", "fail");
		}

		if (!isEnabled(drop_accountTypeCBB)) {
			writeTestResults("Verify that Account Type is disabled", "Account Type should be disabled",
					"Account Type successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Account Type is disabled", "Account Type should be disabled",
					"Account Type doesn't diasabled", "fail");
		}

		if (!isEnabled(txt_telephoneNumberCBB)) {
			writeTestResults("Verify that Telephone Number is disabled", "Telephone Number should be disabled",
					"Telephone Number successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Telephone Number is disabled", "Telephone Number should be disabled",
					"Telephone Number doesn't diasabled", "fail");
		}

		if (!isEnabled(txt_faxCBB)) {
			writeTestResults("Verify that Fax Number is disabled", "Fax Number should be disabled",
					"Fax Number successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Fax Number is disabled", "Fax Number should be disabled",
					"Fax Number doesn't diasabled", "fail");
		}

		if (!isEnabled(txt_emailCBB)) {
			writeTestResults("Verify that Email is disabled", "Email should be disabled",
					"Email successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Email is disabled", "Email should be disabled", "Email doesn't diasabled",
					"fail");
		}

		if (!isEnabled(txt_webURLCBB)) {
			writeTestResults("Verify that Web URL is disabled", "Web URL should be disabled",
					"Web URL successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Web URL is disabled", "Web URL should be disabled",
					"Web URL doesn't diasabled", "fail");
		}

		if (!isEnabled(chk_moreCurrenciesCBB)) {
			writeTestResults("Verify that More Currencies check box is disabled",
					"More Currencies check box should be disabled", "More Currencies check box successfully diasabled",
					"pass");
		} else {
			writeTestResults("Verify that More Currencies check box is disabled",
					"More Currencies check box should be disabled", "More Currencies check box doesn't diasabled",
					"fail");
		}

		if (!isEnabled(txt_addressCBB)) {
			writeTestResults("Verify that Address is disabled", "Address should be disabled",
					"Address successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Address is disabled", "Address should be disabled",
					"Address doesn't diasabled", "fail");
		}

		if (!isEnabled(txt_discountingMaximumCBB)) {
			writeTestResults("Verify that Discounting Maximum is disabled", "Discounting Maximum should be disabled",
					"Discounting Maximum successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Discounting Maximum is disabled", "Discounting Maximum should be disabled",
					"Discounting Maximum doesn't diasabled", "fail");
		}

		if (!isEnabled(txt_activeFromCBB)) {
			writeTestResults("Verify that Active From is disabled", "Active From should be disabled",
					"Active From successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Active From is disabled", "Active From should be disabled",
					"Active From doesn't diasabled", "fail");
		}

		if (!isEnabled(txt_activeToCBB)) {
			writeTestResults("Verify that Active To is disabled", "Active To should be disabled",
					"Active To successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Active To is disabled", "Active To should be disabled",
					"Active To doesn't diasabled", "fail");
		}

		if (!isEnabled(txt_deawerCBB)) {
			writeTestResults("Verify that Drawer is disabled", "Drawer should be disabled",
					"Drawer successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Drawer is disabled", "Drawer should be disabled", "Drawer doesn't diasabled",
					"fail");
		}

		if (!isEnabled(txt_swiftCodeCBB)) {
			writeTestResults("Verify that Swift Code is disabled", "Swift Code should be disabled",
					"Swift Code successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Swift Code is disabled", "Swift Code should be disabled",
					"Swift Code doesn't diasabled", "fail");
		}

		if (!isEnabled(chk_leaseFacilityCBB)) {
			writeTestResults("Verify that Lease Facility check box is disabled",
					"Lease Facility check box should be disabled", "Lease Facility check box successfully diasabled",
					"pass");
		} else {
			writeTestResults("Verify that Lease Facility check box is disabled",
					"Lease Facility check box should be disabled", "Lease Facility check box doesn't diasabled",
					"fail");
		}

		if (!isEnabled(chk_leaseFacilityCBB)) {
			writeTestResults("Verify that Lease Facility check box is disabled",
					"Lease Facility check box should be disabled", "Lease Facility check box successfully diasabled",
					"pass");
		} else {
			writeTestResults("Verify that Lease Facility check box is disabled",
					"Lease Facility check box should be disabled", "Lease Facility check box doesn't diasabled",
					"fail");
		}

		click(btn_draft);
		explicitWait(header_draftedCashBankBook, 40);
		if (isDisplayed(header_draftedCashBankBook)) {
			writeTestResults("Verify user can draft the Petty Cash Book",
					"User should be able to draft the Petty Cash Book", "User successfully draft the Petty Cash Book",
					"pass");
		} else {
			writeTestResults("Verify user can draft the Petty Cash Book",
					"User should be able to draft the Petty Cash Book", "User doesn't draft the Petty Cash Book",
					"fail");
		}

		explicitWait(btn_reelese, 20);
		click(btn_reelese);
		explicitWait(header_releasedCashBankBook, 40);
		trackCode = getText(lbl_docNoCashBankBook);
		releasedCashBankBook_FIN_CBB_1_4_5_6_10 = trackCode;
		if (isDisplayed(header_releasedCashBankBook)) {
			writeTestResults("Verify user can release the Petty Cash Book",
					"User should be able to release the Petty Cash Book",
					"User successfully release the Petty Cash Book", "pass");
		} else {
			writeTestResults("Verify user can release the Petty Cash Book",
					"User should be able to release the Petty Cash Book", "User doesn't release the Petty Cash Book",
					"fail");
		}

	}

	/* FIN_CBB_11 */
	public void dupliacteCashBankBook_FIN_CBB_11() throws InvalidFormatException, IOException, Exception {
		getInvObjReg2().loginBiletaGBV();
		openPage(getFinObj().readFinanceData("ReleaseCashBookUrl_FIN_CBB_1_4_5_6_10"));
		explicitWait(header_releasedCashBankBook, 50);
		if (isDisplayed(header_releasedCashBankBook)) {
			writeTestResults("Verify that existing Cash Book is open", "Existing Cash Book should be open",
					"Existing Cash Book successfully open", "pass");
		} else {
			writeTestResults("Verify that existing Cash Book is open", "Existing Cash Book should be open",
					"Existing Cash Book doen't open", "fail");
		}

		click(btn_duplicate);

		explicitWait(header_newCashBankBook, 40);
		if (isDisplayed(header_newCashBankBook)) {
			writeTestResults("Verify that user able to navigate to duplicated Cash Book",
					"User should be able to navigate to duplicated Cash Book",
					"User successfully navigate to duplicated Cash Book", "pass");
		} else {
			writeTestResults("Verify that user able to navigate to duplicated Cash Book",
					"User should be able to navigate to duplicated Cash Book",
					"User doesn't navigate to duplicated Cash Book", "fail");
		}

		String selectedBookType = getSelectedOptionInDropdown(drop_typeCashBankBook);
		if (selectedBookType.equals(cashBankBookTypeCash)) {
			writeTestResults("Verify that book type is remain same as original document",
					"Book type should be remain same as original document",
					"Book type successfully remain same as original document", "pass");
		} else {
			writeTestResults("Verify that book type is remain same as original document",
					"Book type should be remain same as original document",
					"Book type doesn't remain same as original document", "fail");
		}

		String selectedCurrency = getSelectedOptionInDropdown(drop_currency);
		if (selectedCurrency.equals(currencyLKR)) {
			writeTestResults("Verify that selected currency is remain same as original document",
					"Selected currency should be remain same as original document",
					"Selected currency successfully remain same as original document", "pass");
		} else {
			writeTestResults("Verify that selected currency is remain same as original document",
					"Selected currency should be remain same as original document",
					"Selected currency doesn't remain same as original document", "fail");
		}

		String enteredFloat = getAttribute(txt_floatCashBankBook, "value");
		if (enteredFloat.equals("10000.000000")) {
			writeTestResults("Verify that selected float amount is remain same as original document",
					"Selected float amount should be remain same as original document",
					"Selected float amount successfully remain same as original document", "pass");
		} else {
			writeTestResults("Verify that selected float amount is remain same as original document",
					"Selected float amount should be remain same as original document",
					"Selected float amount doesn't remain same as original document", "fail");
		}

		String chousedPermissionGroup = getAttribute(txt_permissionGroupFrontPageCBB, "value");
		if (chousedPermissionGroup.equals(permissionGroupCBB)) {
			writeTestResults("Verify that selected permission group is remain same as original document",
					"Selected permission group should be remain same as original document",
					"Selected permission group successfully remain same as original document", "pass");
		} else {
			writeTestResults("Verify that selected permission group is remain same as original document",
					"Selected permission group should be remain same as original document",
					"Selected permission group doesn't remain same as original document", "fail");
		}

		String accountNumber = getInvObj().currentTimeAndDate().replace("/", "").replace(":", "");
		sendKeys(txt_bankAccountNoCashBankBook, accountNumber);

		click(btn_draft);
		explicitWait(header_draftedCashBankBook, 40);
		if (isDisplayed(header_draftedCashBankBook)) {
			writeTestResults("Verify user can draft the Cash Book", "User should be able to draft the Cash Book",
					"User successfully draft the Cash Book", "pass");
		} else {
			writeTestResults("Verify user can draft the Cash Book", "User should be able to draft the Cash Book",
					"User doesn't draft the Cash Book", "fail");
		}

		explicitWait(btn_reelese, 20);
		click(btn_reelese);
		explicitWait(header_releasedCashBankBook, 40);
		trackCode = getText(lbl_docNoCashBankBook);
		if (isDisplayed(header_releasedCashBankBook)) {
			writeTestResults("Verify user can release the Cash Book", "User should be able to release the Cash Book",
					"User successfully release the Cash Book", "pass");
		} else {
			writeTestResults("Verify user can release the Cash Book", "User should be able to release the Cash Book",
					"User doesn't release the Cash Book", "fail");
		}
	}

	/* FIN_CBB_12 */
	public void checkAlreadyUsedAccountNumberAsAccountNumber_FIN_CBB_12() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_cashBankBook, 20);
		if (isDisplayed(btn_cashBankBook)) {
			writeTestResults("Verify that Cash/Bank Book option is available under Finance Module",
					"Cash/Bank Book option should be available under Finance Module",
					"Cash/Bank Book option successfully available under Finance Module", "pass");
		} else {
			writeTestResults("Verify that Cash/Bank Book option is available under Finance Module",
					"Cash/Bank Book option should be available under Finance Module",
					"Cash/Bank Book option successfully available under Finance Module", "fail");
		}

		if (isEnabled(btn_cashBankBook)) {
			writeTestResults("Verify user able to click on Cash/Bank Book option",
					"User should be able to click on Cash/Bank Book option",
					"User able to click on Cash/Bank Book option", "pass");
		} else {
			writeTestResults("Verify user able to click on Cash/Bank Book option",
					"User should be able to click on Cash/Bank Book option",
					"User not able to click on Cash/Bank Book option", "fail");
		}

		click(btn_cashBankBook);

		explicitWait(header_cashBankBookByPage, 40);
		if (isDisplayed(header_cashBankBookByPage)) {
			writeTestResults("Verify that user was directed to by-page Cash/Bank Book",
					"User should directed to by-page Cash/Bank Book",
					"User successfully directed to by-page Cash/Bank Book", "pass");
		} else {
			writeTestResults("Verify that user was directed to by-page Cash/Bank Book",
					"User should directed to by-page Cash/Bank Book", "User doesn't directed to by-page Cash/Bank Book",
					"fail");
		}

		click(btn_newCashBankBook);
		explicitWait(header_newCashBankBook, 40);
		if (isDisplayed(header_newCashBankBook)) {
			writeTestResults("Verify that Cash/Bank Book  - New page is displayed",
					"Cash/Bank Book  - New page should be displayed",
					"Cash/Bank Book  - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify that Cash/Bank Book  - New page is displayed",
					"Cash/Bank Book  - New page should be displayed", "Cash/Bank Book  - New page doesn't displayed",
					"fail");
		}

		selectText(drop_typeCashBankBook, cashBankBookTypeCash);
		String selectedBookType = getSelectedOptionInDropdown(drop_typeCashBankBook);
		if (selectedBookType.equals(cashBankBookTypeCash)) {
			writeTestResults("Verify that user able to select type as Cash",
					"User should be able to select type as Cash", "User successfully select type as Cash", "pass");
		} else {
			writeTestResults("Verify that user able to select type as Cash",
					"User should be able to select type as Cash", "User doesn't select type as Cash", "fail");
		}

		String accountNumber = getFinObj().readFinanceData("AccountNumber_FIN_CBB_1_4_5_6_10");
		sendKeys(txt_bankAccountNoCashBankBook, accountNumber);
		String setAccountNumber = getAttribute(txt_bankAccountNoCashBankBook, "value");
		if (setAccountNumber.equals(accountNumber)) {
			writeTestResults("Verify that user able to enter Account Number",
					"User should be able to enter Account Number", "User successfully enter Account Number", "pass");
		} else {
			writeTestResults("Verify that user able to enter Account Number",
					"User should be able to enter Account Number", "User doesn't enter Account Number", "fail");
		}

		selectText(drop_currency, currencyLKR);
		String selectedCurrency = getSelectedOptionInDropdown(drop_currency);
		if (selectedCurrency.equals(currencyLKR)) {
			writeTestResults("Verify that user able to select currency as LKR",
					"User should be able to select currency as LKR", "User successfully select currency as LKR",
					"pass");
		} else {
			writeTestResults("Verify that user able to select currency as LKR",
					"User should be able to select currency as LKR", "User doesn't select currency as LKR", "fail");
		}

		sendKeys(txt_floatCashBankBook, tenThousand);
		String enteredFloat = getAttribute(txt_floatCashBankBook, "value");
		if (enteredFloat.equals(tenThousand)) {
			writeTestResults("Verify that user able to enter Float", "User should be able to enter Float",
					"User successfully enter Float", "pass");
		} else {
			writeTestResults("Verify that user able to enter Float", "User should be able to enter Float",
					"User doesn't enter Float", "fail");
		}

		click(btn_permissionGroupLookupCashBankBook);
		getInvObj().handeledSendKeys(txt_searcUserGroup, permissionGroupCBB);
		pressEnter(txt_searcUserGroup);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", permissionGroupCBB), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", permissionGroupCBB));
		Thread.sleep(2000);
		String chousedPermissionGroup = getAttribute(txt_permissionGroupFrontPageCBB, "value");
		if (chousedPermissionGroup.equals(permissionGroupCBB)) {
			writeTestResults("Verify that user able to choose Permission Group",
					"User should be able to choose Permission Group", "User successfully choose Permission Group",
					"pass");
		} else {
			writeTestResults("Verify that user able to choose Permission Group",
					"User should be able to choose Permission Group", "User doesn't choose Permission Group", "fail");
		}

		click(btn_draft);
		explicitWait(para_validationDuplicateAccountNumber, 6);
		if (isDisplayed(para_validationDuplicateAccountNumber)) {
			writeTestResults("Verify that error message is give saying \"Bank account number cannot be duplicated\"",
					"Error message should give saying \"Bank account number cannot be duplicated\"",
					"Error message successfully give saying \"Bank account number cannot be duplicated\"", "pass");
		} else {
			writeTestResults("Verify that error message is give saying \"Bank account number cannot be duplicated\"",
					"Error message should give saying \"Bank account number cannot be duplicated\"",
					"Error message doesn't give saying \"Bank account number cannot be duplicated\"", "fail");
		}
	}

	/* FIN_CBB_13 */
	public void draftAndNewCashBook_FIN_CBB_13() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_cashBankBook, 20);
		if (isDisplayed(btn_cashBankBook)) {
			writeTestResults("Verify that Cash/Bank Book option is available under Finance Module",
					"Cash/Bank Book option should be available under Finance Module",
					"Cash/Bank Book option successfully available under Finance Module", "pass");
		} else {
			writeTestResults("Verify that Cash/Bank Book option is available under Finance Module",
					"Cash/Bank Book option should be available under Finance Module",
					"Cash/Bank Book option successfully available under Finance Module", "fail");
		}

		if (isEnabled(btn_cashBankBook)) {
			writeTestResults("Verify user able to click on Cash/Bank Book option",
					"User should be able to click on Cash/Bank Book option",
					"User able to click on Cash/Bank Book option", "pass");
		} else {
			writeTestResults("Verify user able to click on Cash/Bank Book option",
					"User should be able to click on Cash/Bank Book option",
					"User not able to click on Cash/Bank Book option", "fail");
		}

		click(btn_cashBankBook);

		explicitWait(header_cashBankBookByPage, 40);
		if (isDisplayed(header_cashBankBookByPage)) {
			writeTestResults("Verify that user was directed to by-page Cash/Bank Book",
					"User should directed to by-page Cash/Bank Book",
					"User successfully directed to by-page Cash/Bank Book", "pass");
		} else {
			writeTestResults("Verify that user was directed to by-page Cash/Bank Book",
					"User should directed to by-page Cash/Bank Book", "User doesn't directed to by-page Cash/Bank Book",
					"fail");
		}

		click(btn_newCashBankBook);
		explicitWait(header_newCashBankBook, 40);
		if (isDisplayed(header_newCashBankBook)) {
			writeTestResults("Verify that Cash/Bank Book  - New page is displayed",
					"Cash/Bank Book  - New page should be displayed",
					"Cash/Bank Book  - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify that Cash/Bank Book  - New page is displayed",
					"Cash/Bank Book  - New page should be displayed", "Cash/Bank Book  - New page doesn't displayed",
					"fail");
		}

		selectText(drop_typeCashBankBook, cashBankBookTypeCash);
		String selectedBookType = getSelectedOptionInDropdown(drop_typeCashBankBook);
		if (selectedBookType.equals(cashBankBookTypeCash)) {
			writeTestResults("Verify that user able to select type as Cash",
					"User should be able to select type as Cash", "User successfully select type as Cash", "pass");
		} else {
			writeTestResults("Verify that user able to select type as Cash",
					"User should be able to select type as Cash", "User doesn't select type as Cash", "fail");
		}

		String accountNumber = getInvObj().currentTimeAndDate().replace("/", "").replace(":", "");
		sendKeys(txt_bankAccountNoCashBankBook, accountNumber);
		String setAccountNumber = getAttribute(txt_bankAccountNoCashBankBook, "value");
		if (setAccountNumber.equals(accountNumber)) {
			writeTestResults("Verify that user able to enter Account Number",
					"User should be able to enter Account Number", "User successfully enter Account Number", "pass");
		} else {
			writeTestResults("Verify that user able to enter Account Number",
					"User should be able to enter Account Number", "User doesn't enter Account Number", "fail");
		}

		selectText(drop_currency, currencyLKR);
		String selectedCurrency = getSelectedOptionInDropdown(drop_currency);
		if (selectedCurrency.equals(currencyLKR)) {
			writeTestResults("Verify that user able to select currency as LKR",
					"User should be able to select currency as LKR", "User successfully select currency as LKR",
					"pass");
		} else {
			writeTestResults("Verify that user able to select currency as LKR",
					"User should be able to select currency as LKR", "User doesn't select currency as LKR", "fail");
		}

		sendKeys(txt_floatCashBankBook, tenThousand);
		String enteredFloat = getAttribute(txt_floatCashBankBook, "value");
		if (enteredFloat.equals(tenThousand)) {
			writeTestResults("Verify that user able to enter Float", "User should be able to enter Float",
					"User successfully enter Float", "pass");
		} else {
			writeTestResults("Verify that user able to enter Float", "User should be able to enter Float",
					"User doesn't enter Float", "fail");
		}

		click(btn_permissionGroupLookupCashBankBook);
		getInvObj().handeledSendKeys(txt_searcUserGroup, permissionGroupCBB);
		pressEnter(txt_searcUserGroup);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", permissionGroupCBB), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", permissionGroupCBB));
		Thread.sleep(2000);
		String chousedPermissionGroup = getAttribute(txt_permissionGroupFrontPageCBB, "value");
		if (chousedPermissionGroup.equals(permissionGroupCBB)) {
			writeTestResults("Verify that user able to choose Permission Group",
					"User should be able to choose Permission Group", "User successfully choose Permission Group",
					"pass");
		} else {
			writeTestResults("Verify that user able to choose Permission Group",
					"User should be able to choose Permission Group", "User doesn't choose Permission Group", "fail");
		}

		if (!isEnabled(drop_bankCodeCBB)) {
			writeTestResults("Verify that Bank Code is disabled", "Bank Code should be disabled",
					"Bank Code successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Bank Code is disabled", "Bank Code should be disabled",
					"Bank Code doesn't diasabled", "fail");
		}

		if (!isEnabled(txt_companyStatementCBB)) {
			writeTestResults("Verify that Company Statement is disabled", "Company Statement should be disabled",
					"Company Statement successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Company Statement is disabled", "Company Statement should be disabled",
					"Company Statement doesn't diasabled", "fail");
		}

		if (!isEnabled(txt_bankBranchCBB)) {
			writeTestResults("Verify that Bank Branch is disabled", "Bank Branch should be disabled",
					"Bank Branch successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Bank Branch is disabled", "Bank Branch should be disabled",
					"Bank Branch doesn't diasabled", "fail");
		}

		if (!isEnabled(drop_accountTypeCBB)) {
			writeTestResults("Verify that Account Type is disabled", "Account Type should be disabled",
					"Account Type successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Account Type is disabled", "Account Type should be disabled",
					"Account Type doesn't diasabled", "fail");
		}

		if (!isEnabled(txt_telephoneNumberCBB)) {
			writeTestResults("Verify that Telephone Number is disabled", "Telephone Number should be disabled",
					"Telephone Number successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Telephone Number is disabled", "Telephone Number should be disabled",
					"Telephone Number doesn't diasabled", "fail");
		}

		if (!isEnabled(txt_faxCBB)) {
			writeTestResults("Verify that Fax Number is disabled", "Fax Number should be disabled",
					"Fax Number successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Fax Number is disabled", "Fax Number should be disabled",
					"Fax Number doesn't diasabled", "fail");
		}

		if (!isEnabled(txt_emailCBB)) {
			writeTestResults("Verify that Email is disabled", "Email should be disabled",
					"Email successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Email is disabled", "Email should be disabled", "Email doesn't diasabled",
					"fail");
		}

		if (!isEnabled(txt_webURLCBB)) {
			writeTestResults("Verify that Web URL is disabled", "Web URL should be disabled",
					"Web URL successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Web URL is disabled", "Web URL should be disabled",
					"Web URL doesn't diasabled", "fail");
		}

		if (!isEnabled(chk_moreCurrenciesCBB)) {
			writeTestResults("Verify that More Currencies check box is disabled",
					"More Currencies check box should be disabled", "More Currencies check box successfully diasabled",
					"pass");
		} else {
			writeTestResults("Verify that More Currencies check box is disabled",
					"More Currencies check box should be disabled", "More Currencies check box doesn't diasabled",
					"fail");
		}

		if (!isEnabled(txt_addressCBB)) {
			writeTestResults("Verify that Address is disabled", "Address should be disabled",
					"Address successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Address is disabled", "Address should be disabled",
					"Address doesn't diasabled", "fail");
		}

		if (!isEnabled(txt_discountingMaximumCBB)) {
			writeTestResults("Verify that Discounting Maximum is disabled", "Discounting Maximum should be disabled",
					"Discounting Maximum successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Discounting Maximum is disabled", "Discounting Maximum should be disabled",
					"Discounting Maximum doesn't diasabled", "fail");
		}

		if (!isEnabled(txt_activeFromCBB)) {
			writeTestResults("Verify that Active From is disabled", "Active From should be disabled",
					"Active From successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Active From is disabled", "Active From should be disabled",
					"Active From doesn't diasabled", "fail");
		}

		if (!isEnabled(txt_activeToCBB)) {
			writeTestResults("Verify that Active To is disabled", "Active To should be disabled",
					"Active To successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Active To is disabled", "Active To should be disabled",
					"Active To doesn't diasabled", "fail");
		}

		if (!isEnabled(txt_deawerCBB)) {
			writeTestResults("Verify that Drawer is disabled", "Drawer should be disabled",
					"Drawer successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Drawer is disabled", "Drawer should be disabled", "Drawer doesn't diasabled",
					"fail");
		}

		if (!isEnabled(txt_swiftCodeCBB)) {
			writeTestResults("Verify that Swift Code is disabled", "Swift Code should be disabled",
					"Swift Code successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that Swift Code is disabled", "Swift Code should be disabled",
					"Swift Code doesn't diasabled", "fail");
		}

		if (!isEnabled(chk_iouCBB)) {
			writeTestResults("Verify that IOU Reimbursement check box is disabled",
					"IOU Reimbursement check box should be disabled",
					"IOU Reimbursement check box successfully diasabled", "pass");
		} else {
			writeTestResults("Verify that IOU Reimbursement check box is disabled",
					"IOU Reimbursement check box should be disabled", "IOU Reimbursement check box doesn't diasabled",
					"fail");
		}

		if (!isEnabled(chk_leaseFacilityCBB)) {
			writeTestResults("Verify that Lease Facility check box is disabled",
					"Lease Facility check box should be disabled", "Lease Facility check box successfully diasabled",
					"pass");
		} else {
			writeTestResults("Verify that Lease Facility check box is disabled",
					"Lease Facility check box should be disabled", "Lease Facility check box doesn't diasabled",
					"fail");
		}

		if (!isEnabled(chk_leaseFacilityCBB)) {
			writeTestResults("Verify that Lease Facility check box is disabled",
					"Lease Facility check box should be disabled", "Lease Facility check box successfully diasabled",
					"pass");
		} else {
			writeTestResults("Verify that Lease Facility check box is disabled",
					"Lease Facility check box should be disabled", "Lease Facility check box doesn't diasabled",
					"fail");
		}

		click(btn_draftAndNew);
		explicitWait(header_newCashBankBook, 40);
		if (isDisplayed(header_newCashBankBook)) {
			writeTestResults("Verify user able to navigate to new Cash Book",
					"User should be able to navigate to new Cash Book", "User successfully navigate to new Cash Book",
					"pass");
		} else {
			writeTestResults("Verify user able to navigate to new Cash Book",
					"User should be able to navigate to new Cash Book", "User doesn't navigate to new Cash Book",
					"fail");
		}

		((JavascriptExecutor) driver).executeScript("window.open('about:blank','_blank');");
		switchWindow();
		openPage(site_Url);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 30);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_cashBankBook, 30);
		if (isDisplayed(btn_cashBankBook)) {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Finance Module is clicked", "Finance should be clicked",
					"Finance module doesn't clicked successfully", "fail");
		}

		click(btn_cashBankBook);
		explicitWait(header_cashBankBookByPage, 30);
		if (isDisplayed(header_cashBankBookByPage)) {
			writeTestResults("Verify user can navigate to Cash/Bank Book by page",
					"User should be navigate to Cash/Bank Book by page",
					"User successfully navigate to Cash/Bank Book by page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Cash/Bank Book by page",
					"User should be navigate to Cash/Bank Book by page",
					"User doesn't navigate to Cash/Bank Book by page", "fail");
		}

		click(lbl_recentlyDraftedCashBAnkBook);

		explicitWait(txt_descriptionDraftedBankAdjustment, 30);

		String fondedAccountNo = getText(lbl_accountNoDrfatedCashBankBook);

		trackCode = getText(lbl_docNo);

		if (fondedAccountNo.equals(accountNumber)) {
			writeTestResults("Verify user should be able to draft the first document",
					"User should be able to draft the first document", "User successfully draft the first document",
					"pass");
		} else {
			writeTestResults("Verify user should be able to draft the first document",
					"User should be able to draft the first document", "User doesn't draft the first document", "fail");
		}
	}

	/* FIN_CBB_14_15 */
	public void copyFromCashBookEditDeatils_FIN_CBB_14_15() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_cashBankBook, 20);
		if (isDisplayed(btn_cashBankBook)) {
			writeTestResults("Verify that Cash/Bank Book option is available under Finance Module",
					"Cash/Bank Book option should be available under Finance Module",
					"Cash/Bank Book option successfully available under Finance Module", "pass");
		} else {
			writeTestResults("Verify that Cash/Bank Book option is available under Finance Module",
					"Cash/Bank Book option should be available under Finance Module",
					"Cash/Bank Book option successfully available under Finance Module", "fail");
		}

		if (isEnabled(btn_cashBankBook)) {
			writeTestResults("Verify user able to click on Cash/Bank Book option",
					"User should be able to click on Cash/Bank Book option",
					"User able to click on Cash/Bank Book option", "pass");
		} else {
			writeTestResults("Verify user able to click on Cash/Bank Book option",
					"User should be able to click on Cash/Bank Book option",
					"User not able to click on Cash/Bank Book option", "fail");
		}

		click(btn_cashBankBook);

		explicitWait(header_cashBankBookByPage, 40);
		if (isDisplayed(header_cashBankBookByPage)) {
			writeTestResults("Verify that user was directed to by-page Cash/Bank Book",
					"User should directed to by-page Cash/Bank Book",
					"User successfully directed to by-page Cash/Bank Book", "pass");
		} else {
			writeTestResults("Verify that user was directed to by-page Cash/Bank Book",
					"User should directed to by-page Cash/Bank Book", "User doesn't directed to by-page Cash/Bank Book",
					"fail");
		}

		click(btn_newCashBankBook);
		explicitWait(header_newCashBankBook, 40);
		if (isDisplayed(header_newCashBankBook)) {
			writeTestResults("Verify that Cash/Bank Book  - New page is displayed",
					"Cash/Bank Book  - New page should be displayed",
					"Cash/Bank Book  - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify that Cash/Bank Book  - New page is displayed",
					"Cash/Bank Book  - New page should be displayed", "Cash/Bank Book  - New page doesn't displayed",
					"fail");
		}

		click(btn_copyFrom);
		Thread.sleep(1000);
		explicitWait(header_lookupCashBankBook, 40);
		if (isDisplayed(header_lookupCashBankBook)) {
			writeTestResults("Verify user able to view Cash/Bank Book lookup",
					"User should be able to view Cash/Bank Book lookup", "User successfully view Cash/Bank Book lookup",
					"pass");
		} else {
			writeTestResults("Verify user able to view Cash/Bank Book lookup",
					"User should be able to view Cash/Bank Book lookup", "User doesn't view Cash/Bank Book lookup",
					"fail");
		}

		sendKeys(txt_searchCashBankBook, getFinObj().readFinanceData("AccountNumber_FIN_CBB_1_4_5_6_10"));
		pressEnter(txt_searchCashBankBook);

		explicitWait(lnk_inforOnLookupInformationReplace.replace("info",
				getFinObj().readFinanceData("AccountNumber_FIN_CBB_1_4_5_6_10")), 15);

		if (isDisplayed(lnk_inforOnLookupInformationReplace.replace("info",
				getFinObj().readFinanceData("AccountNumber_FIN_CBB_1_4_5_6_10")))) {
			writeTestResults(
					"Verify relevent existing Cash/Bank Book was loaded and user allow to select the particular Cash/Bank Book",
					"Relevent existing Cash/Bank Book should be loaded and user should allow to select the particular Cash/Bank Book",
					"Relevent existing Cash/Bank Book successfully loaded and user allow to select the particular Cash/Bank Book",
					"pass");
		} else {
			writeTestResults(
					"Verify relevent existing Cash/Bank Book was loaded and user allow to select the particular Cash/Bank Book",
					"Relevent existing Cash/Bank Book should be loaded and user should allow to select the particular Cash/Bank Book",
					"Relevent existing Cash/Bank Book doesn't loaded and user not allow to select the particular Cash/Bank Book",
					"fail");
		}

		doubleClick(lnk_inforOnLookupInformationReplace.replace("info",
				getFinObj().readFinanceData("AccountNumber_FIN_CBB_1_4_5_6_10")));

		Thread.sleep(2000);

		String selectedBookType = getSelectedOptionInDropdown(drop_typeCashBankBook);
		if (selectedBookType.equals(cashBankBookTypeCash)) {
			writeTestResults("Verify that book type is remain same as original document",
					"Book type should be remain same as original document",
					"Book type successfully remain same as original document", "pass");
		} else {
			writeTestResults("Verify that book type is remain same as original document",
					"Book type should be remain same as original document",
					"Book type doesn't remain same as original document", "fail");
		}

		String selectedCurrency = getSelectedOptionInDropdown(drop_currency);
		if (selectedCurrency.equals(currencyLKR)) {
			writeTestResults("Verify that selected currency is remain same as original document",
					"Selected currency should be remain same as original document",
					"Selected currency successfully remain same as original document", "pass");
		} else {
			writeTestResults("Verify that selected currency is remain same as original document",
					"Selected currency should be remain same as original document",
					"Selected currency doesn't remain same as original document", "fail");
		}

		String enteredFloat = getAttribute(txt_floatCashBankBook, "value");
		if (enteredFloat.equals("10000.000000")) {
			writeTestResults("Verify that selected float amount is remain same as original document",
					"Selected float amount should be remain same as original document",
					"Selected float amount successfully remain same as original document", "pass");
		} else {
			writeTestResults("Verify that selected float amount is remain same as original document",
					"Selected float amount should be remain same as original document",
					"Selected float amount doesn't remain same as original document", "fail");
		}

		String chousedPermissionGroup = getAttribute(txt_permissionGroupFrontPageCBB, "value");
		if (chousedPermissionGroup.equals(permissionGroupCBB)) {
			writeTestResults("Verify that selected permission group is remain same as original document",
					"Selected permission group should be remain same as original document",
					"Selected permission group successfully remain same as original document", "pass");
		} else {
			writeTestResults("Verify that selected permission group is remain same as original document",
					"Selected permission group should be remain same as original document",
					"Selected permission group doesn't remain same as original document", "fail");
		}

		String accountNumber = getInvObj().currentTimeAndDate().replace("/", "").replace(":", "");
		sendKeys(txt_bankAccountNoCashBankBook, accountNumber);

		selectText(drop_currency, currencyUSD);
		String changedCurrency = getSelectedOptionInDropdown(drop_currency);
		if (changedCurrency.equals(currencyUSD)) {
			writeTestResults("Verify that user able to change currency as USD",
					"User should be able to change currency as USD", "User successfully change currency as USD",
					"pass");
		} else {
			writeTestResults("Verify that user able to change currency as USD",
					"User should be able to change currency as USD", "User doesn't change currency as USD", "fail");
		}

		sendKeys(txt_floatCashBankBook, twoThousandFiveHundred);
		String changedFloat = getAttribute(txt_floatCashBankBook, "value");
		if (changedFloat.equals(twoThousandFiveHundred)) {
			writeTestResults("Verify that user able to change Float", "User should be able to change Float",
					"User successfully change Float", "pass");
		} else {
			writeTestResults("Verify that user able to change Float", "User should be able to change Float",
					"User doesn't change Float", "fail");
		}

		click(btn_removePermissionGroupCBB);
		Thread.sleep(2000);
		String changedPermissionGroup = getAttribute(txt_permissionGroupFrontPageCBB, "value");
		if (changedPermissionGroup.equals("")) {
			writeTestResults("Verify that user able to remove Permission Group",
					"User should be able to remove Permission Group", "User successfully remove Permission Group",
					"pass");
		} else {
			writeTestResults("Verify that user able to remove Permission Group",
					"User should be able to remove Permission Group", "User doesn't remove Permission Group", "fail");
		}

		click(btn_draft);
		explicitWait(header_draftedCashBankBook, 40);
		if (isDisplayed(header_draftedCashBankBook)) {
			writeTestResults("Verify user can draft the Cash Book", "User should be able to draft the Cash Book",
					"User successfully draft the Cash Book", "pass");
		} else {
			writeTestResults("Verify user can draft the Cash Book", "User should be able to draft the Cash Book",
					"User doesn't draft the Cash Book", "fail");
		}

		explicitWait(btn_reelese, 20);
		click(btn_reelese);
		explicitWait(header_releasedCashBankBook, 40);
		trackCode = getText(lbl_docNoCashBankBook);
		if (isDisplayed(header_releasedCashBankBook)) {
			writeTestResults("Verify user can release the Cash Book", "User should be able to release the Cash Book",
					"User successfully release the Cash Book", "pass");
		} else {
			writeTestResults("Verify user can release the Cash Book", "User should be able to release the Cash Book",
					"User doesn't release the Cash Book", "fail");
		}

	}

	/* FIN_CBB_16 */
	public void checkAccountNumberEditabilityOfReleaseCashBook_FIN_CBB_16() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		openPage(getFinObj().readFinanceData("ReleaseCashBookUrl_FIN_CBB_1_4_5_6_10"));
		explicitWait(header_releasedCashBankBook, 50);
		if (isDisplayed(header_releasedCashBankBook)) {
			writeTestResults("Verify that existing Cash Book is open", "Existing Cash Book should be open",
					"Existing Cash Book successfully open", "pass");
		} else {
			writeTestResults("Verify that existing Cash Book is open", "Existing Cash Book should be open",
					"Existing Cash Book doen't open", "fail");
		}

		click(btn_edit);
		explicitWait(btn_update, 40);
		if (isDisplayed(btn_update)) {
			writeTestResults("Verify user able to edit the selected Cash/Bank Book",
					"User should be able to edit the selected Cash/Bank Book",
					"User able to edit the selected Cash/Bank Book", "pass");
		} else {
			writeTestResults("Verify user able to edit the selected Cash/Bank Book",
					"User should be able to edit the selected Cash/Bank Book",
					"User not able to edit the selected Cash/Bank Book", "fail");
		}

		if (!isEnabled(txt_bankAccountNoCashBankBook)) {
			writeTestResults("Verify that Account Number field is editable",
					"Account Number field should not be editable", "Account Number field is not editable", "pass");
		} else {
			writeTestResults("Verify that Account Number field is editable",
					"Account Number field should not be editable", "Account Number field is editable", "fail");
		}
	}

	/* FIN_CBB_17 */
	public void checkCashBookWithPayBookGenaration_FIN_CBB_17() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg2().loginBiletaGBV();
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_payBookGenaration, 20);
		if (isDisplayed(btn_payBookGenaration)) {
			writeTestResults("Verify that Pay Book Genaration option is available under Finance Module",
					"Pay Book Genaration option should be available under Finance Module",
					"Pay Book Genaration option successfully available under Finance Module", "pass");
		} else {
			writeTestResults("Verify that Pay Book Genaration option is available under Finance Module",
					"Pay Book Genaration option should be available under Finance Module",
					"Pay Book Genaration option successfully available under Finance Module", "fail");
		}

		click(btn_payBookGenaration);

		explicitWait(btn_newPayBookGenaration, 40);
		click(btn_newPayBookGenaration);
		explicitWait(header_newPayBookGenaration, 40);
		if (isDisplayed(header_newPayBookGenaration)) {
			writeTestResults("Verify that Pay Book Genaration  - New page is displayed",
					"Pay Book Genaration  - New page should be displayed",
					"Pay Book Genaration  - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify that Pay Book Genaration  - New page is displayed",
					"Pay Book Genaration  - New page should be displayed",
					"Pay Book Genaration  - New page doesn't displayed", "fail");
		}
		
		List<String> accountDropodwnPayBookGenaration = new ArrayList<String>();
		Select dropAccount = new Select(driver.findElement(By.xpath(drop_templateOnByPage)));
		List<WebElement> opAccounts = dropAccount.getOptions();
		int sizeAccounts = opAccounts.size();
		for (int i = 0; i < sizeAccounts; i++) {
			String options = opAccounts.get(i).getText();
			accountDropodwnPayBookGenaration.add(options);
		}

		if (accountDropodwnPayBookGenaration.contains(getFinObj().readFinanceData("AccountNumber_FIN_CBB_1_4_5_6_10"))) {
			writeTestResults("Verify user able to view created Cash/Bank Book in the drop down",
					"User should be able to view created Cash/Bank Book in the drop down",
					"User successfully view created Cash/Bank Book in the drop down", "pass");
		} else {
			writeTestResults("Verify user able to view created Cash/Bank Book in the drop down",
					"User should be able to view created Cash/Bank Book in the drop down",
					"User doesn't view created Cash/Bank Book in the drop down", "fail");
		}
	}

	/* FIN_CBB_18 */
	public void inactiveCashBookAndCheckItWithPayBookGenaration_FIN_CBB_18() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		explicitWait(navigation_pane, 30);
		Thread.sleep(2000);
		explicitWait(navigation_pane, 30);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_cashBankBook);

		explicitWait(header_cashBankBookByPage, 40);
		if (isDisplayed(header_cashBankBookByPage)) {
			writeTestResults("Verify that user was directed to by-page Cash/Bank Book",
					"User should directed to by-page Cash/Bank Book",
					"User successfully directed to by-page Cash/Bank Book", "pass");
		} else {
			writeTestResults("Verify that user was directed to by-page Cash/Bank Book",
					"User should directed to by-page Cash/Bank Book", "User doesn't directed to by-page Cash/Bank Book",
					"fail");
		}
		sendKeys(txt_searchCashBankBook, getFinObj().readFinanceData("AccountNumber_FIN_CBB_1_4_5_6_10"));
		click(btn_searchOnByPage);
		Thread.sleep(2000);
		explicitWait(lnk_resultDocOnByPage.replace("doc_no",
				getFinObj().readFinanceData("AccountNumber_FIN_CBB_1_4_5_6_10")), 40);

		click(lnk_resultDocOnByPage.replace("doc_no", getFinObj().readFinanceData("AccountNumber_FIN_CBB_1_4_5_6_10")));

		explicitWait(header_releasedCashBankBookDocReplace.replace("doc_no",
				getFinObj().readFinanceData("AccountNumber_FIN_CBB_1_4_5_6_10")), 40);
		if (isDisplayed(header_releasedCashBankBookDocReplace.replace("doc_no",
				getFinObj().readFinanceData("AccountNumber_FIN_CBB_1_4_5_6_10")))) {
			writeTestResults("Verify that Bank Books that matches with the entered number was get retrieved.",
					"Bank Books that matches with the entered number should get retrieved.",
					"Bank Books that matches with the entered number successfully get retrieved.", "pass");
		} else {
			writeTestResults("Verify that Bank Books that matches with the entered number was get retrieved.",
					"Bank Books that matches with the entered number should get retrieved.",
					"Bank Books that matches with the entered number doesn't get retrieved.", "fail");
		}

		click(btn_edit);
		explicitWait(btn_statusAction, 15);
		click(btn_statusAction);
		explicitWait(drop_status, 9);
		selectText(drop_status, statusInactive);
		click(btn_applyStatus);
		Thread.sleep(2000);
		click(btn_update);
		Thread.sleep(2000);
		explicitWait(header_releasedCashBankBook, 20);

		String status = getText(lbl_status);
		if (status.equals(statusInactiveFrontPage)) {
			writeTestResults("Verify user able to change the status of the cash book to Inactive",
					"User should be able to change the status of the cash book to Inactive",
					"User successfully change the status of the cash book to Inactive", "pass");
		} else {
			writeTestResults("Verify user able to change the status of the cash book to Inactive",
					"User should be able to change the status of the cash book to Inactive",
					"User doesn't change the status of the cash book to Inactive", "fail");
		}

		explicitWait(navigation_pane, 30);
		Thread.sleep(2000);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_payBookGenaration, 20);
		click(btn_payBookGenaration);
		explicitWait(header_payBookGenerationByPage, 40);
		if (isDisplayed(header_payBookGenerationByPage)) {
			writeTestResults("Verify user able to navigate to Pay Book Generation by-page",
					"User should be able to navigate to Pay Book Generation by-page",
					"User successfully navigate to Pay Book Generation by-page", "pass");
		} else {
			writeTestResults("Verify user able to navigate to Pay Book Generation by-page",
					"User should be able to navigate to Pay Book Generation by-page",
					"User doesn't navigate to Pay Book Generation by-page", "fail");
		}

		click(btn_newPayBookGenaration);
		explicitWait(header_newPayBookGenaration, 40);
		if (isDisplayed(header_newPayBookGenaration)) {
			writeTestResults("Verify user able to navigate to Pay Book Generation new form",
					"User should be able to navigate to Pay Book Generation new form",
					"User successfully navigate to Pay Book Generation new form", "pass");
		} else {
			writeTestResults("Verify user able to navigate to Pay Book Generation new form",
					"User should be able to navigate to Pay Book Generation new form",
					"User doesn't navigate to Pay Book Generation new form", "fail");
		}

		selectText(drop_payMethod, payMethodCash);
		Thread.sleep(2000);

		List<String> cashAccountDropDown = new ArrayList<String>();
		Select dropdownCashAccount = new Select(driver.findElement(By.xpath(drop_payMethod)));
		List<WebElement> op = dropdownCashAccount.getOptions();
		int siz = op.size();
		for (int i = 0; i < siz; i++) {
			String options = op.get(i).getText();
			cashAccountDropDown.add(options);
		}

		if (!cashAccountDropDown.contains(getFinObj().readFinanceData("AccountNumber_FIN_CBB_1_4_5_6_10"))) {
			writeTestResults("Verify that the inactive cash account not contains in the dropdown",
					"The inactive cash account should not be contains in the dropdown",
					"The inactive cash account not contains in the dropdown", "pass");
		} else {
			writeTestResults("Verify that the inactive cash account not contains in the dropdown",
					"The inactive cash account should not be contains in the dropdown",
					"The inactive cash account contains in the dropdown", "fail");
		}
	}

	/* FIN_CBB_20 */
	public void createPettyCashAccount_FIN_CBB_20() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_cashBankBook, 20);
		if (isDisplayed(btn_cashBankBook)) {
			writeTestResults("Verify that Cash/Bank Book option is available under Finance Module",
					"Cash/Bank Book option should be available under Finance Module",
					"Cash/Bank Book option successfully available under Finance Module", "pass");
		} else {
			writeTestResults("Verify that Cash/Bank Book option is available under Finance Module",
					"Cash/Bank Book option should be available under Finance Module",
					"Cash/Bank Book option successfully available under Finance Module", "fail");
		}

		if (isEnabled(btn_cashBankBook)) {
			writeTestResults("Verify user able to click on Cash/Bank Book option",
					"User should be able to click on Cash/Bank Book option",
					"User able to click on Cash/Bank Book option", "pass");
		} else {
			writeTestResults("Verify user able to click on Cash/Bank Book option",
					"User should be able to click on Cash/Bank Book option",
					"User not able to click on Cash/Bank Book option", "fail");
		}

		click(btn_cashBankBook);

		explicitWait(header_cashBankBookByPage, 40);
		if (isDisplayed(header_cashBankBookByPage)) {
			writeTestResults("Verify that user was directed to by-page Cash/Bank Book",
					"User should directed to by-page Cash/Bank Book",
					"User successfully directed to by-page Cash/Bank Book", "pass");
		} else {
			writeTestResults("Verify that user was directed to by-page Cash/Bank Book",
					"User should directed to by-page Cash/Bank Book", "User doesn't directed to by-page Cash/Bank Book",
					"fail");
		}

		click(btn_newCashBankBook);
		explicitWait(header_newCashBankBook, 40);
		if (isDisplayed(header_newCashBankBook)) {
			writeTestResults("Verify that Cash/Bank Book  - New page is displayed",
					"Cash/Bank Book  - New page should be displayed",
					"Cash/Bank Book  - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify that Cash/Bank Book  - New page is displayed",
					"Cash/Bank Book  - New page should be displayed", "Cash/Bank Book  - New page doesn't displayed",
					"fail");
		}

		selectText(drop_typeCashBankBook, bookTypePettyCash);
		String selectedBookType = getSelectedOptionInDropdown(drop_typeCashBankBook);
		if (selectedBookType.equals(bookTypePettyCash)) {
			writeTestResults("Verify that user able to select type as Petty Cash",
					"User should be able to select type as Petty Cash", "User successfully select type as Petty Cash",
					"pass");
		} else {
			writeTestResults("Verify that user able to select type as Petty Cash",
					"User should be able to select type as Petty Cash", "User doesn't select type as Petty Cash",
					"fail");
		}

		String accountNumber = getInvObj().currentTimeAndDate().replace("/", "").replace(":", "");
		sendKeys(txt_bankAccountNoCashBankBook, accountNumber);
		String setAccountNumber = getAttribute(txt_bankAccountNoCashBankBook, "value");
		if (setAccountNumber.equals(accountNumber)) {
			writeTestResults("Verify that user able to enter Account Number",
					"User should be able to enter Account Number", "User successfully enter Account Number", "pass");
		} else {
			writeTestResults("Verify that user able to enter Account Number",
					"User should be able to enter Account Number", "User doesn't enter Account Number", "fail");
		}

		selectText(drop_currency, currencyLKR);
		String selectedCurrency = getSelectedOptionInDropdown(drop_currency);
		if (selectedCurrency.equals(currencyLKR)) {
			writeTestResults("Verify that user able to select currency as LKR",
					"User should be able to select currency as LKR", "User successfully select currency as LKR",
					"pass");
		} else {
			writeTestResults("Verify that user able to select currency as LKR",
					"User should be able to select currency as LKR", "User doesn't select currency as LKR", "fail");
		}

		sendKeys(txt_floatCashBankBook, amountThousand);
		String enteredFloat = getAttribute(txt_floatCashBankBook, "value");
		if (enteredFloat.equals(amountThousand)) {
			writeTestResults("Verify that user able to enter Float", "User should be able to enter Float",
					"User successfully enter Float", "pass");
		} else {
			writeTestResults("Verify that user able to enter Float", "User should be able to enter Float",
					"User doesn't enter Float", "fail");
		}

		click(btn_draft);
		explicitWait(header_draftedCashBankBook, 40);
		if (isDisplayed(header_draftedCashBankBook)) {
			writeTestResults("Verify user can draft the Petty Cash Book",
					"User should be able to draft the Petty Cash Book", "User successfully draft the Petty Cash Book",
					"pass");
		} else {
			writeTestResults("Verify user can draft the Petty Cash Book",
					"User should be able to draft the Petty Cash Book", "User doesn't draft the Petty Cash Book",
					"fail");
		}

		explicitWait(btn_reelese, 20);
		click(btn_reelese);
		explicitWait(header_releasedCashBankBook, 40);
		trackCode = getText(lbl_docNoCashBankBook);
		getFinObj().writeFinanceData("PettyCashBook_FIN_CBB_20", trackCode, 31);
		if (isDisplayed(header_releasedCashBankBook)) {
			writeTestResults("Verify user can release the Petty Cash Book",
					"User should be able to release the Petty Cash Book",
					"User successfully release the Petty Cash Book", "pass");
		} else {
			writeTestResults("Verify user can release the Petty Cash Book",
					"User should be able to release the Petty Cash Book", "User doesn't release the Petty Cash Book",
					"fail");
		}
	}

	public void pettyCashPart_FIN_CBB_20() throws Exception {
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_pettyCash, 20);
		if (isDisplayed(btn_pettyCash)) {
			writeTestResults("Verify that Petty Cash option is available under Finance Module",
					"Petty Cash option should be available under Finance Module",
					"Petty Cash option successfully available under Finance Module", "pass");
		} else {
			writeTestResults("Verify that Petty Cash option is available under Finance Module",
					"Petty Cash option should be available under Finance Module",
					"Petty Cash option successfully available under Finance Module", "fail");
		}

		click(btn_pettyCash);
		explicitWait(btn_newPettyCash, 40);
		click(btn_newPettyCash);

		explicitWait(header_newPettyCash, 40);
		if (isDisplayed(header_newPettyCash)) {
			writeTestResults("Verify user able to navigate to new Petty Cash Book",
					"User should be able to navigate to new Petty Cash Book",
					"User successfully navigate to new Petty Cash Book", "pass");
		} else {
			writeTestResults("Verify user able to navigate to new Petty Cash Book",
					"User should be able to navigate to new Petty Cash Book",
					"User doesn't navigate to new Petty Cash Book", "fail");
		}

		selectText(drop_pettyCashAccount, getFinObj().readFinanceData("PettyCashBook_FIN_CBB_20"));
		String selectedPettyCashAccount = getSelectedOptionInDropdown(drop_pettyCashAccount);
		if (selectedPettyCashAccount.equals(getFinObj().readFinanceData("PettyCashBook_FIN_CBB_20"))) {
			writeTestResults("Verify user able to select the Petty Cash Account",
					"User should be able to select the Petty Cash Account",
					"User successfully select the Petty Cash Account", "pass");
		} else {
			writeTestResults("Verify user able to select the Petty Cash Account",
					"User should be able to select the Petty Cash Account",
					"User doesn't select the Petty Cash Account", "fail");
		}

		sendKeys(txt_descPettyCash, descriptionPettyCashIOU);

		click(btn_employeeLookup);
		getInvObj().handeledSendKeys(txt_searchEmployee, employee);
		pressEnter(txt_searchEmployee);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", employee), 15);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", employee));

		selectText(drop_pettyCashType, pettyCashTypeIOU);
		String selectedType = getSelectedOptionInDropdown(drop_pettyCashType);
		if (selectedType.equals(pettyCashTypeIOU)) {
			writeTestResults("Verify user able to select IOU or One-off",
					"User should be able to select IOU or One-off", "User successfully select IOU or One-off", "pass");
		} else {
			writeTestResults("Verify user able to select IOU or One-off",
					"User should be able to select IOU or One-off", "User doesn't select IOU or One-off", "fail");
		}

		sendKeys(txt_amountPettyCash2, amountTwoThousand);

		click(btn_draft);
		Thread.sleep(2000);

		if (isDisplayed(lbl_mainValidation)) {
			writeTestResults("Verify main validation is appear", "Main validation should be appear",
					"Main validation successfully appear", "pass");
		} else {
			writeTestResults("Verify main validation is appear", "Main validation should be appear",
					"Main validation doesn't appear", "fail");
		}

		if (isDisplayed(error_msgFrontPageFloatAmountPettyCash)) {
			writeTestResults("Verify user receive error message stating \"Amount can't exceed Float\"(Front Page)",
					"User should receive error message stating \"Amount can't exceed Float\"(Front Page)",
					"User successfully receive error message stating \"Amount can't exceed Float\"(Front Page)",
					"pass");
		} else {
			writeTestResults("Verify user receive error message stating \"Amount can't exceed Float\"(Front Page)",
					"User should receive error message stating \"Amount can't exceed Float\"(Front Page)",
					"User doesn't receive error message stating \"Amount can't exceed Float\"(Front Page)", "fail");
		}

		if (isDisplayed(txt_amountWithErrorBorderFrontPagePettyCash)) {
			writeTestResults("Verify that error border (Total Amount) is visible for the user",
					"Error border (Total Amount) should visible for the user",
					"Error border (Total Amount) successfully visible for the user", "pass");
		} else {
			writeTestResults("Verify that error border (Total Amount) is visible for the user",
					"Error border (Total Amount) should visible for the user",
					"Error border (Total Amount) doesn't visible for the user", "fail");
		}

		if (isDisplayed(txt_amountWithErrorBorderPettyCash)) {
			writeTestResults("Verify that error border (Amount) is visible for the user",
					"Error border (Amount) should visible for the user",
					"Error border (Amount) successfully visible for the user", "pass");
		} else {
			writeTestResults("Verify that error border (Amount) is visible for the user",
					"Error border (Amount) should visible for the user",
					"Error border (Amount) doesn't visible for the user", "fail");
		}

		changeCSS(btn_closeMainValidation, "z-index:-9999");

		explicitWait(btn_rowErrorPettyCash, 5);
		click(btn_rowErrorPettyCash);
		explicitWait(error_gridFloatExceedPettyCash, 8);
		if (isDisplayed(error_gridFloatExceedPettyCash)) {
			writeTestResults("Verify user receive error message stating \"Amount can't exceed Float\"(Row Error)",
					"User should receive error message stating \"Amount can't exceed Float\"(Row Error)",
					"User successfully receive error message stating \"Amount can't exceed Float\"(Row Error)", "pass");
		} else {
			writeTestResults("Verify user receive error message stating \"Amount can't exceed Float\"(Row Error)",
					"User should receive error message stating \"Amount can't exceed Float\"(Row Error)",
					"User doesn't receive error message stating \"Amount can't exceed Float\"(Row Error)", "fail");
		}

		explicitWait(error_gridTotalAmountFloatExceedPettyCash, 8);
		if (isDisplayed(error_gridTotalAmountFloatExceedPettyCash)) {
			writeTestResults("Verify user receive error message stating \"Amount total can't exceed Float\"(Row Error)",
					"User should receive error message stating \"Amount total can't exceed Float\"(Row Error)",
					"User successfully receive error message stating \"Amount total can't exceed Float\"(Row Error)",
					"pass");
		} else {
			writeTestResults("Verify user receive error message stating \"Amount total can't exceed Float\"(Row Error)",
					"User should receive error message stating \"Amount total can't exceed Float\"(Row Error)",
					"User doesn't receive error message stating \"Amount total can't exceed Float\"(Row Error)",
					"fail");
		}

	}

	/* FIN_CBB_21 */
	public void cashBankBook_FIN_CBB_21() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_cashBankBook, 20);
		if (isDisplayed(btn_cashBankBook)) {
			writeTestResults("Verify that Cash/Bank Book option is available under Finance Module",
					"Cash/Bank Book option should be available under Finance Module",
					"Cash/Bank Book option successfully available under Finance Module", "pass");
		} else {
			writeTestResults("Verify that Cash/Bank Book option is available under Finance Module",
					"Cash/Bank Book option should be available under Finance Module",
					"Cash/Bank Book option successfully available under Finance Module", "fail");
		}

		if (isEnabled(btn_cashBankBook)) {
			writeTestResults("Verify user able to click on Cash/Bank Book option",
					"User should be able to click on Cash/Bank Book option",
					"User able to click on Cash/Bank Book option", "pass");
		} else {
			writeTestResults("Verify user able to click on Cash/Bank Book option",
					"User should be able to click on Cash/Bank Book option",
					"User not able to click on Cash/Bank Book option", "fail");
		}

		click(btn_cashBankBook);

		explicitWait(header_cashBankBookByPage, 40);
		if (isDisplayed(header_cashBankBookByPage)) {
			writeTestResults("Verify that user was directed to by-page Cash/Bank Book",
					"User should directed to by-page Cash/Bank Book",
					"User successfully directed to by-page Cash/Bank Book", "pass");
		} else {
			writeTestResults("Verify that user was directed to by-page Cash/Bank Book",
					"User should directed to by-page Cash/Bank Book", "User doesn't directed to by-page Cash/Bank Book",
					"fail");
		}

		click(btn_newCashBankBook);
		explicitWait(header_newCashBankBook, 40);
		if (isDisplayed(header_newCashBankBook)) {
			writeTestResults("Verify that Cash/Bank Book  - New page is displayed",
					"Cash/Bank Book  - New page should be displayed",
					"Cash/Bank Book  - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify that Cash/Bank Book  - New page is displayed",
					"Cash/Bank Book  - New page should be displayed", "Cash/Bank Book  - New page doesn't displayed",
					"fail");
		}

		selectText(drop_typeCashBankBook, cashBankBookTypeCash);
		String selectedBookType = getSelectedOptionInDropdown(drop_typeCashBankBook);
		if (selectedBookType.equals(cashBankBookTypeCash)) {
			writeTestResults("Verify that user able to select type as Cash",
					"User should be able to select type as Cash", "User successfully select type as Cash", "pass");
		} else {
			writeTestResults("Verify that user able to select type as Cash",
					"User should be able to select type as Cash", "User doesn't select type as Cash", "fail");
		}

		String accountNumber = getInvObj().currentTimeAndDate().replace("/", "").replace(":", "");
		sendKeys(txt_bankAccountNoCashBankBook, accountNumber);
		String setAccountNumber = getAttribute(txt_bankAccountNoCashBankBook, "value");
		if (setAccountNumber.equals(accountNumber)) {
			writeTestResults("Verify that user able to enter Account Number",
					"User should be able to enter Account Number", "User successfully enter Account Number", "pass");
		} else {
			writeTestResults("Verify that user able to enter Account Number",
					"User should be able to enter Account Number", "User doesn't enter Account Number", "fail");
		}

		selectText(drop_currency, currencyLKR);
		String selectedCurrency = getSelectedOptionInDropdown(drop_currency);
		if (selectedCurrency.equals(currencyLKR)) {
			writeTestResults("Verify that user able to select currency as LKR",
					"User should be able to select currency as LKR", "User successfully select currency as LKR",
					"pass");
		} else {
			writeTestResults("Verify that user able to select currency as LKR",
					"User should be able to select currency as LKR", "User doesn't select currency as LKR", "fail");
		}

		sendKeys(txt_floatCashBankBook, amountThousand);
		String enteredFloat = getAttribute(txt_floatCashBankBook, "value");
		if (enteredFloat.equals(amountThousand)) {
			writeTestResults("Verify that user able to enter Float", "User should be able to enter Float",
					"User successfully enter Float", "pass");
		} else {
			writeTestResults("Verify that user able to enter Float", "User should be able to enter Float",
					"User doesn't enter Float", "fail");
		}

		click(btn_draft);
		explicitWait(header_draftedCashBankBook, 40);
		if (isDisplayed(header_draftedCashBankBook)) {
			writeTestResults("Verify user can draft the Cash Book", "User should be able to draft the Cash Book",
					"User successfully draft the Cash Book", "pass");
		} else {
			writeTestResults("Verify user can draft the Cash Book", "User should be able to draft the Cash Book",
					"User doesn't draft the Cash Book", "fail");
		}

		explicitWait(btn_reelese, 20);
		click(btn_reelese);
		explicitWait(header_releasedCashBankBook, 40);
		trackCode = getText(lbl_docNoCashBankBook);
		if (isDisplayed(header_releasedCashBankBook)) {
			writeTestResults("Verify user can release the Cash Book", "User should be able to release the Cash Book",
					"User successfully release the Cash Book", "pass");
		} else {
			writeTestResults("Verify user can release the Cash Book", "User should be able to release the Cash Book",
					"User doesn't release the Cash Book", "fail");
		}

		explicitWait(navigation_pane, 30);
		Thread.sleep(2000);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_payBookGenaration, 20);
		click(btn_payBookGenaration);
		explicitWait(header_payBookGenerationByPage, 40);
		if (isDisplayed(header_payBookGenerationByPage)) {
			writeTestResults("Verify user able to navigate to Pay Book Generation by-page",
					"User should be able to navigate to Pay Book Generation by-page",
					"User successfully navigate to Pay Book Generation by-page", "pass");
		} else {
			writeTestResults("Verify user able to navigate to Pay Book Generation by-page",
					"User should be able to navigate to Pay Book Generation by-page",
					"User doesn't navigate to Pay Book Generation by-page", "fail");
		}

		click(btn_newPayBookGenaration);
		explicitWait(header_newPayBookGenaration, 40);
		if (isDisplayed(header_newPayBookGenaration)) {
			writeTestResults("Verify user able to navigate to Pay Book Generation new form",
					"User should be able to navigate to Pay Book Generation new form",
					"User successfully navigate to Pay Book Generation new form", "pass");
		} else {
			writeTestResults("Verify user able to navigate to Pay Book Generation new form",
					"User should be able to navigate to Pay Book Generation new form",
					"User doesn't navigate to Pay Book Generation new form", "fail");
		}

		String payBookNumber = getInvObj().currentTimeAndDate().replace("/", "").replace(":", "");
		sendKeys(txt_payCodePayBookGenaration, payBookNumber);

		selectText(drop_payMethod, payMethodCash);

		selectText(drop_accountNo, accountNumber);
		click(btn_draft);
		explicitWait(header_draftedPayBook, 40);
		if (isDisplayed(header_draftedPayBook)) {
			writeTestResults("Verify user can draft the Pay Book", "User should be able to draft the Pay Book",
					"User successfully draft the Pay Book", "pass");
		} else {
			writeTestResults("Verify user can draft the Pay Book", "User should be able to draft the Pay Book",
					"User doesn't draft the Pay Book", "fail");
		}

		explicitWait(btn_reelese, 20);
		click(btn_reelese);
		explicitWait(header_releasedPayBook, 40);
		trackCode = getText(lbl_docNoCashBankBook);
		if (isDisplayed(header_releasedPayBook)) {
			writeTestResults("Verify user can release the Pay Book", "User should be able to release the Pay Book",
					"User successfully release the Pay Book", "pass");
		} else {
			writeTestResults("Verify user can release the Pay Book", "User should be able to release the Pay Book",
					"User doesn't release the Pay Book", "fail");
		}

		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_outboundPaymentAdvice, 20);
		if (isDisplayed(btn_outboundPaymentAdvice)) {
			writeTestResults("Verify that Outbound Payment Advice option is available under Finance Module",
					"Outbound Payment Advice option should be available under Finance Module",
					"Outbound Payment Advice option successfully available under Finance Module", "pass");
		} else {
			writeTestResults("Verify that Outbound Payment Advice option is available under Finance Module",
					"Outbound Payment Advice option should be available under Finance Module",
					"Outbound Payment Advice option successfully available under Finance Module", "fail");
		}

		click(btn_outboundPaymentAdvice);

		if (isDisplayed(btn_newOutboundPaymentAdvice)) {

			writeTestResults("Verify user can navigate to Outbound Payment Advice by-page",
					"User should navigate to Outbound Payment Advice by-page",
					"User sucessfully navigate to Outbound Payment Advice by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Outbound Payment Advice by-page",
					"User should navigate to Outbound Payment Advice by-page",
					"User couldn't navigate to Outbound Payment Advice by-page", "fail");
		}

		click(btn_newOutboundPaymentAdvice);
		if (isDisplayed(btn_vendorAdvanceJourney)) {
			writeTestResults("Verify user can view the list of journey",
					"User should be able to view the list of journey", "User sucessfully view the list of journey",
					"pass");
		} else {
			writeTestResults("Verify user can view the list of journey", "User should view the list of journey",
					"User couldn't view the list of journey", "fail");
		}

		click(btn_vendorAdvanceJourney);
		if (isDisplayed(btn_searchVendorOPA)) {
			writeTestResults("Verify user can navigate to New Outbound Payment Advice page",
					"User should navigate to New Outbound Payment Advice page",
					"User sucessfully navigate to New Outbound Payment Advice page", "pass");
		} else {
			writeTestResults("Verify user can navigate to New Outbound Payment Advice page",
					"User should navigate to New Outbound Payment Advice page",
					"User couldn't navigate to New Outbound Payment Advice page", "fail");
		}
	}

	/* FIN_CBB_22 */
	public void cashBankBookForegnCurency_FIN_CBB_22() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_cashBankBook, 20);
		if (isDisplayed(btn_cashBankBook)) {
			writeTestResults("Verify that Cash/Bank Book option is available under Finance Module",
					"Cash/Bank Book option should be available under Finance Module",
					"Cash/Bank Book option successfully available under Finance Module", "pass");
		} else {
			writeTestResults("Verify that Cash/Bank Book option is available under Finance Module",
					"Cash/Bank Book option should be available under Finance Module",
					"Cash/Bank Book option successfully available under Finance Module", "fail");
		}

		if (isEnabled(btn_cashBankBook)) {
			writeTestResults("Verify user able to click on Cash/Bank Book option",
					"User should be able to click on Cash/Bank Book option",
					"User able to click on Cash/Bank Book option", "pass");
		} else {
			writeTestResults("Verify user able to click on Cash/Bank Book option",
					"User should be able to click on Cash/Bank Book option",
					"User not able to click on Cash/Bank Book option", "fail");
		}

		click(btn_cashBankBook);

		explicitWait(header_cashBankBookByPage, 40);
		if (isDisplayed(header_cashBankBookByPage)) {
			writeTestResults("Verify that user was directed to by-page Cash/Bank Book",
					"User should directed to by-page Cash/Bank Book",
					"User successfully directed to by-page Cash/Bank Book", "pass");
		} else {
			writeTestResults("Verify that user was directed to by-page Cash/Bank Book",
					"User should directed to by-page Cash/Bank Book", "User doesn't directed to by-page Cash/Bank Book",
					"fail");
		}

		click(btn_newCashBankBook);
		explicitWait(header_newCashBankBook, 40);
		if (isDisplayed(header_newCashBankBook)) {
			writeTestResults("Verify that Cash/Bank Book  - New page is displayed",
					"Cash/Bank Book  - New page should be displayed",
					"Cash/Bank Book  - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify that Cash/Bank Book  - New page is displayed",
					"Cash/Bank Book  - New page should be displayed", "Cash/Bank Book  - New page doesn't displayed",
					"fail");
		}

		selectText(drop_typeCashBankBook, cashBankBookTypeCash);
		String selectedBookType = getSelectedOptionInDropdown(drop_typeCashBankBook);
		if (selectedBookType.equals(cashBankBookTypeCash)) {
			writeTestResults("Verify that user able to select type as Cash",
					"User should be able to select type as Cash", "User successfully select type as Cash", "pass");
		} else {
			writeTestResults("Verify that user able to select type as Cash",
					"User should be able to select type as Cash", "User doesn't select type as Cash", "fail");
		}

		String accountNumber = getInvObj().currentTimeAndDate().replace("/", "").replace(":", "");
		sendKeys(txt_bankAccountNoCashBankBook, accountNumber);
		String setAccountNumber = getAttribute(txt_bankAccountNoCashBankBook, "value");
		if (setAccountNumber.equals(accountNumber)) {
			writeTestResults("Verify that user able to enter Account Number",
					"User should be able to enter Account Number", "User successfully enter Account Number", "pass");
		} else {
			writeTestResults("Verify that user able to enter Account Number",
					"User should be able to enter Account Number", "User doesn't enter Account Number", "fail");
		}

		selectText(drop_currency, currencyUSD);
		String selectedCurrency = getSelectedOptionInDropdown(drop_currency);
		if (selectedCurrency.equals(currencyUSD)) {
			writeTestResults("Verify that user able to select currency as USD",
					"User should be able to select currency as USD", "User successfully select currency as USD",
					"pass");
		} else {
			writeTestResults("Verify that user able to select currency as USD",
					"User should be able to select currency as USD", "User doesn't select currency as USD", "fail");
		}

		sendKeys(txt_floatCashBankBook, tenThousand);
		String enteredFloat = getAttribute(txt_floatCashBankBook, "value");
		if (enteredFloat.equals(tenThousand)) {
			writeTestResults("Verify that user able to enter Float", "User should be able to enter Float",
					"User successfully enter Float", "pass");
		} else {
			writeTestResults("Verify that user able to enter Float", "User should be able to enter Float",
					"User doesn't enter Float", "fail");
		}

		click(btn_permissionGroupLookupCashBankBook);
		getInvObj().handeledSendKeys(txt_searcUserGroup, permissionGroupCBB);
		pressEnter(txt_searcUserGroup);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", permissionGroupCBB), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", permissionGroupCBB));
		Thread.sleep(2000);
		String chousedPermissionGroup = getAttribute(txt_permissionGroupFrontPageCBB, "value");
		if (chousedPermissionGroup.equals(permissionGroupCBB)) {
			writeTestResults("Verify that user able to choose Permission Group",
					"User should be able to choose Permission Group", "User successfully choose Permission Group",
					"pass");
		} else {
			writeTestResults("Verify that user able to choose Permission Group",
					"User should be able to choose Permission Group", "User doesn't choose Permission Group", "fail");
		}

		click(btn_draft);
		explicitWait(header_draftedCashBankBook, 40);
		if (isDisplayed(header_draftedCashBankBook)) {
			writeTestResults("Verify user can draft the Cash /Bank Book",
					"User should be able to draft the Cash /Bank Book", "User successfully draft the Cash /Bank Book",
					"pass");
		} else {
			writeTestResults("Verify user can draft the Cash /Bank Book",
					"User should be able to draft the Cash /Bank Book", "User doesn't draft the Cash /Bank Book",
					"fail");
		}

		explicitWait(btn_reelese, 20);
		click(btn_reelese);
		explicitWait(header_releasedCashBankBook, 40);
		trackCode = getText(lbl_docNoCashBankBook);
		if (isDisplayed(header_releasedCashBankBook)) {
			writeTestResults("Verify user can release the Cash /Bank Book",
					"User should be able to release the Cash /Bank Book",
					"User successfully release the Cash /Bank Book", "pass");
		} else {
			writeTestResults("Verify user can release the Cash /Bank Book",
					"User should be able to release the Cash /Bank Book", "User doesn't release the Cash /Bank Book",
					"fail");
		}

	}

	/* FIN_CBB_23 */
	public void cashBankBookCurrencyRate_FIN_CBB_23() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 30);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);
		explicitWait(btn_curencyExchangeRate, 20);
		click(btn_curencyExchangeRate);
		explicitWait(txt_usdSellingRate, 50);
		String currencyRate = getAttribute(txt_usdSellingRate, "value");
		Thread.sleep(2000);

		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_cashBankBook, 20);
		if (isDisplayed(btn_cashBankBook)) {
			writeTestResults("Verify that Cash/Bank Book option is available under Finance Module",
					"Cash/Bank Book option should be available under Finance Module",
					"Cash/Bank Book option successfully available under Finance Module", "pass");
		} else {
			writeTestResults("Verify that Cash/Bank Book option is available under Finance Module",
					"Cash/Bank Book option should be available under Finance Module",
					"Cash/Bank Book option successfully available under Finance Module", "fail");
		}

		if (isEnabled(btn_cashBankBook)) {
			writeTestResults("Verify user able to click on Cash/Bank Book option",
					"User should be able to click on Cash/Bank Book option",
					"User able to click on Cash/Bank Book option", "pass");
		} else {
			writeTestResults("Verify user able to click on Cash/Bank Book option",
					"User should be able to click on Cash/Bank Book option",
					"User not able to click on Cash/Bank Book option", "fail");
		}

		click(btn_cashBankBook);

		explicitWait(header_cashBankBookByPage, 40);
		if (isDisplayed(header_cashBankBookByPage)) {
			writeTestResults("Verify that user was directed to by-page Cash/Bank Book",
					"User should directed to by-page Cash/Bank Book",
					"User successfully directed to by-page Cash/Bank Book", "pass");
		} else {
			writeTestResults("Verify that user was directed to by-page Cash/Bank Book",
					"User should directed to by-page Cash/Bank Book", "User doesn't directed to by-page Cash/Bank Book",
					"fail");
		}

		click(btn_newCashBankBook);
		explicitWait(header_newCashBankBook, 40);
		if (isDisplayed(header_newCashBankBook)) {
			writeTestResults("Verify that Cash/Bank Book  - New page is displayed",
					"Cash/Bank Book  - New page should be displayed",
					"Cash/Bank Book  - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify that Cash/Bank Book  - New page is displayed",
					"Cash/Bank Book  - New page should be displayed", "Cash/Bank Book  - New page doesn't displayed",
					"fail");
		}

		selectText(drop_currency, currencyUSD);
		String selectedCurrency = getSelectedOptionInDropdown(drop_currency);
		if (selectedCurrency.equals(currencyUSD)) {
			writeTestResults("Verify that user able to select currency as USD",
					"User should be able to select currency as USD", "User successfully select currency as USD",
					"pass");
		} else {
			writeTestResults("Verify that user able to select currency as USD",
					"User should be able to select currency as USD", "User doesn't select currency as USD", "fail");
		}

		click(btn_curencyWidgetCashBankBook);
		explicitWait(lbl_exchageRateCashBankBook, 40);

		String cyrencyRateAtCBB = getText(lbl_exchageRateCashBankBook);
		if (currencyRate.replace(".000000", "").equals(cyrencyRateAtCBB)) {
			writeTestResults("Verify that currency widget was loaded to display current exchange rate",
					"Currency widget should load to display current exchange rate",
					"Currency widget successfully load to display current exchange rate", "pass");
		} else {
			writeTestResults("Verify that currency widget was loaded to display current exchange rate",
					"Currency widget should load to display current exchange rate",
					"Currency widget doesn't load to display current exchange rate", "fail");
		}
	}

	/* FIN_CBB_24 */
	public void cashBankBookLeaseFacility_FIN_CBB_24() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 30);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);

		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_cashBankBook, 20);
		if (isDisplayed(btn_cashBankBook)) {
			writeTestResults("Verify that Cash/Bank Book option is available under Finance Module",
					"Cash/Bank Book option should be available under Finance Module",
					"Cash/Bank Book option successfully available under Finance Module", "pass");
		} else {
			writeTestResults("Verify that Cash/Bank Book option is available under Finance Module",
					"Cash/Bank Book option should be available under Finance Module",
					"Cash/Bank Book option successfully available under Finance Module", "fail");
		}

		if (isEnabled(btn_cashBankBook)) {
			writeTestResults("Verify user able to click on Cash/Bank Book option",
					"User should be able to click on Cash/Bank Book option",
					"User able to click on Cash/Bank Book option", "pass");
		} else {
			writeTestResults("Verify user able to click on Cash/Bank Book option",
					"User should be able to click on Cash/Bank Book option",
					"User not able to click on Cash/Bank Book option", "fail");
		}

		click(btn_cashBankBook);

		explicitWait(header_cashBankBookByPage, 40);
		if (isDisplayed(header_cashBankBookByPage)) {
			writeTestResults("Verify that user was directed to by-page Cash/Bank Book",
					"User should directed to by-page Cash/Bank Book",
					"User successfully directed to by-page Cash/Bank Book", "pass");
		} else {
			writeTestResults("Verify that user was directed to by-page Cash/Bank Book",
					"User should directed to by-page Cash/Bank Book", "User doesn't directed to by-page Cash/Bank Book",
					"fail");
		}

		click(btn_newCashBankBook);
		explicitWait(header_newCashBankBook, 40);
		if (isDisplayed(header_newCashBankBook)) {
			writeTestResults("Verify that Cash/Bank Book  - New page is displayed",
					"Cash/Bank Book  - New page should be displayed",
					"Cash/Bank Book  - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify that Cash/Bank Book  - New page is displayed",
					"Cash/Bank Book  - New page should be displayed", "Cash/Bank Book  - New page doesn't displayed",
					"fail");
		}

		selectText(drop_typeCashBankBook, cashBankBookTypeCash);
		String selectedBookType = getSelectedOptionInDropdown(drop_typeCashBankBook);
		if (selectedBookType.equals(cashBankBookTypeCash)) {
			writeTestResults("Verify that user able to select type as Cash",
					"User should be able to select type as Cash", "User successfully select type as Cash", "pass");
		} else {
			writeTestResults("Verify that user able to select type as Cash",
					"User should be able to select type as Cash", "User doesn't select type as Cash", "fail");
		}

		click(chk_leaseFacilityCBB);

		if (!isSelected(chk_leaseFacilityCBB)) {
			writeTestResults("Verify When 'Cash' is selected 'Lease Facility' check box is disabled",
					"When 'Cash' is selected 'Lease Facility' check box should be disabled",
					"When 'Cash' is selected 'Lease Facility' check box successfully disabled", "pass");
		} else {
			writeTestResults("Verify When 'Cash' is selected 'Lease Facility' check box is disabled",
					"When 'Cash' is selected 'Lease Facility' check box should be disabled",
					"When 'Cash' is selected 'Lease Facility' check box doesn't disabled", "fail");
		}

		selectText(drop_typeCashBankBook, cashBankBookTypeBank);
		String selectedBookTypeBank = getSelectedOptionInDropdown(drop_typeCashBankBook);
		if (selectedBookTypeBank.equals(cashBankBookTypeBank)) {
			writeTestResults("Verify that user able to select type as Cash",
					"User should be able to select type as Bank", "User successfully select type as Bank", "pass");
		} else {
			writeTestResults("Verify that user able to select type as Bank",
					"User should be able to select type as Bank", "User doesn't select type as Bank", "fail");
		}

		click(chk_leaseFacilityCBB);

		if (isSelected(chk_leaseFacilityCBB)) {
			writeTestResults(
					"Verify When 'Bank' is selected 'Lease Facility' check box is enabled and user can select checkbox",
					"When 'Bank' is selected 'Lease Facility' check box should be enabled and user can select checkbox",
					"When 'Bank' is selected 'Lease Facility' check box successfully enabled and user can select checkbox",
					"pass");
		} else {
			writeTestResults(
					"Verify When 'Bank' is selected 'Lease Facility' check box is enabled and user can select checkbox",
					"When 'Bank' is selected 'Lease Facility' check box should be enabled and user can select checkbox",
					"When 'Bank' is selected 'Lease Facility' check box doesn't enabled and user can select checkbox",
					"fail");
		}

		selectText(drop_typeCashBankBook, bookTypePettyCash);
		String selectedBookTypePettyCash = getSelectedOptionInDropdown(drop_typeCashBankBook);
		if (selectedBookTypePettyCash.equals(bookTypePettyCash)) {
			writeTestResults("Verify that user able to select type as Petty Cash",
					"User should be able to select type as Petty Cash", "User successfully select type as Petty Cash",
					"pass");
		} else {
			writeTestResults("Verify that user able to select type as Petty Cash",
					"User should be able to select type as Petty Cash", "User doesn't select type as Petty Cash",
					"fail");
		}

		click(chk_leaseFacilityCBB);

		if (!isSelected(chk_leaseFacilityCBB)) {
			writeTestResults("Verify When 'Petty Cash' is selected 'Lease Facility' check box is disabled",
					"When 'Petty Cash' is selected 'Lease Facility' check box should be disabled",
					"When 'Petty Cash' is selected 'Lease Facility' check box successfully disabled", "pass");
		} else {
			writeTestResults("Verify When 'Petty Cash' is selected 'Lease Facility' check box is disabled",
					"When 'Petty Cash' is selected 'Lease Facility' check box should be disabled",
					"When 'Petty Cash' is selected 'Lease Facility' check box doesn't disabled", "fail");
		}
	}

	/* FIN_CBB_25_26_27 */
	public void bankBookWithLeaseFacility_FIN_CBB_25_26_27() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_cashBankBook, 20);
		if (isDisplayed(btn_cashBankBook)) {
			writeTestResults("Verify that Cash/Bank Book option is available under Finance Module",
					"Cash/Bank Book option should be available under Finance Module",
					"Cash/Bank Book option successfully available under Finance Module", "pass");
		} else {
			writeTestResults("Verify that Cash/Bank Book option is available under Finance Module",
					"Cash/Bank Book option should be available under Finance Module",
					"Cash/Bank Book option successfully available under Finance Module", "fail");
		}

		if (isEnabled(btn_cashBankBook)) {
			writeTestResults("Verify user able to click on Cash/Bank Book option",
					"User should be able to click on Cash/Bank Book option",
					"User able to click on Cash/Bank Book option", "pass");
		} else {
			writeTestResults("Verify user able to click on Cash/Bank Book option",
					"User should be able to click on Cash/Bank Book option",
					"User not able to click on Cash/Bank Book option", "fail");
		}

		click(btn_cashBankBook);

		explicitWait(header_cashBankBookByPage, 40);
		if (isDisplayed(header_cashBankBookByPage)) {
			writeTestResults("Verify that user was directed to by-page Cash/Bank Book",
					"User should directed to by-page Cash/Bank Book",
					"User successfully directed to by-page Cash/Bank Book", "pass");
		} else {
			writeTestResults("Verify that user was directed to by-page Cash/Bank Book",
					"User should directed to by-page Cash/Bank Book", "User doesn't directed to by-page Cash/Bank Book",
					"fail");
		}

		click(btn_newCashBankBook);
		explicitWait(header_newCashBankBook, 40);
		if (isDisplayed(header_newCashBankBook)) {
			writeTestResults("Verify that Cash/Bank Book  - New page is displayed",
					"Cash/Bank Book  - New page should be displayed",
					"Cash/Bank Book  - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify that Cash/Bank Book  - New page is displayed",
					"Cash/Bank Book  - New page should be displayed", "Cash/Bank Book  - New page doesn't displayed",
					"fail");
		}

		selectText(drop_typeCashBankBook, cashBankBookTypeBank);
		String selectedBookType = getSelectedOptionInDropdown(drop_typeCashBankBook);
		if (selectedBookType.equals(cashBankBookTypeBank)) {
			writeTestResults("Verify that user able to select type as Bank",
					"User should be able to select type as Bank", "User successfully select type as Bank", "pass");
		} else {
			writeTestResults("Verify that user able to select type as Bank",
					"User should be able to select type as Bank", "User doesn't select type as Bank", "fail");
		}

		selectText(drop_bankCodeCBB, sampathBank);
		String selectedBankCode = getSelectedOptionInDropdown(drop_bankCodeCBB);
		if (selectedBankCode.equals(sampathBank)) {
			writeTestResults("Verify that user able to select bank code as Sampath",
					"User should be able to select bank code as Sampath",
					"User successfully select bank code as Sampath", "pass");
		} else {
			writeTestResults("Verify that user able to select bank code as Sampath",
					"User should be able to select bank code as Sampath", "User doesn't select bank code as Sampath",
					"fail");
		}

		String accountNumber = getInvObj().currentTimeAndDate().replace("/", "").replace(":", "");
		sendKeys(txt_bankAccountNoCashBankBook, accountNumber);
		String setAccountNumber = getAttribute(txt_bankAccountNoCashBankBook, "value");
		if (setAccountNumber.equals(accountNumber)) {
			writeTestResults("Verify that user able to enter Account Number",
					"User should be able to enter Account Number", "User successfully enter Account Number", "pass");
		} else {
			writeTestResults("Verify that user able to enter Account Number",
					"User should be able to enter Account Number", "User doesn't enter Account Number", "fail");
		}

		sendKeys(txt_companyStatementCBB, companyStatement);
		String enteredCompanyStatement = getAttribute(txt_companyStatementCBB, "value");
		if (enteredCompanyStatement.equals(companyStatement)) {
			writeTestResults("Verify that user able to enter Company Statement",
					"User should be able to enter Company Statement", "User successfully enter Company Statement",
					"pass");
		} else {
			writeTestResults("Verify that user able to enter Company Statement",
					"User should be able to enter Company Statement", "User doesn't enter Company Statement", "fail");
		}

		sendKeys(txt_bankBranchCBB, bankBranchNegombo);
		String enteredBankBranch = getAttribute(txt_bankBranchCBB, "value");
		if (enteredBankBranch.equals(bankBranchNegombo)) {
			writeTestResults("Verify that user able to enter Bank Branch", "User should be able to enter Bank Branch",
					"User successfully enter Bank Branch", "pass");
		} else {
			writeTestResults("Verify that user able to enter Bank Branch", "User should be able to enter Bank Branch",
					"User doesn't enter Bank Branch", "fail");
		}

		selectText(drop_accountTypeCBB, accountTypeCBB);
		String selectedAccountType = getSelectedOptionInDropdown(drop_accountTypeCBB);
		if (selectedAccountType.equals(accountTypeCBB)) {
			writeTestResults("Verify that user able to select account type as Current Account",
					"User should be able to select account type as Current Account",
					"User successfully select account type as Current Account", "pass");
		} else {
			writeTestResults("Verify that user able to select account type as Current Account",
					"User should be able to select account type as Current Account",
					"User doesn't select account type as Current Account", "fail");
		}

		sendKeys(txt_telephoneNumberCBB, telephone);
		String enteredTelephone = getAttribute(txt_telephoneNumberCBB, "value");
		if (enteredTelephone.equals(telephone)) {
			writeTestResults("Verify that user able to enter Telephone Number",
					"User should be able to enter Telephone Number", "User successfully enter Telephone Number",
					"pass");
		} else {
			writeTestResults("Verify that user able to enter Telephone Number",
					"User should be able to enter Telephone Number", "User doesn't enter Telephone Number", "fail");
		}

		sendKeys(txt_faxCBB, fax);
		String enteredfax = getAttribute(txt_faxCBB, "value");
		if (enteredfax.equals(fax)) {
			writeTestResults("Verify that user able to enter Fax Number", "User should be able to enter Fax Number",
					"User successfully enter Fax Number", "pass");
		} else {
			writeTestResults("Verify that user able to enter Fax Number", "User should be able to enter Fax Number",
					"User doesn't enter Fax Number", "fail");
		}

		sendKeys(txt_emailCBB, email);
		String enteredMail = getAttribute(txt_emailCBB, "value");
		if (enteredMail.equals(email)) {
			writeTestResults("Verify that user able to enter Email", "User should be able to enter Email",
					"User successfully enter Email", "pass");
		} else {
			writeTestResults("Verify that user able to enter Email", "User should be able to enter Email",
					"User doesn't enter Email", "fail");
		}

		sendKeys(txt_webURLCBB, webUrl);
		String enteredWebUrl = getAttribute(txt_webURLCBB, "value");
		if (enteredWebUrl.equals(webUrl)) {
			writeTestResults("Verify that user able to enter Web URL", "User should be able to enter Web URL",
					"User successfully enter Web URL", "pass");
		} else {
			writeTestResults("Verify that user able to enter Web URL", "User should be able to enter Web URL",
					"User doesn't enter Web URL", "fail");
		}

		sendKeys(txt_addressCBB, address);
		String enteredAddress = getAttribute(txt_addressCBB, "value");
		if (enteredAddress.equals(address)) {
			writeTestResults("Verify that user able to enter Address", "User should be able to enter Address",
					"User successfully enter Address", "pass");
		} else {
			writeTestResults("Verify that user able to enter Address", "User should be able to enter Address",
					"User doesn't enter Address", "fail");
		}

		selectText(drop_currency, currencyLKR);
		String selectedCurrency = getSelectedOptionInDropdown(drop_currency);
		if (selectedCurrency.equals(currencyLKR)) {
			writeTestResults("Verify that user able to select currency as LKR",
					"User should be able to select currency as LKR", "User successfully select currency as LKR",
					"pass");
		} else {
			writeTestResults("Verify that user able to select currency as LKR",
					"User should be able to select currency as LKR", "User doesn't select currency as LKR", "fail");
		}

		sendKeys(txt_floatCashBankBook, tenThousand);
		String enteredFloat = getAttribute(txt_floatCashBankBook, "value");
		if (enteredFloat.equals(tenThousand)) {
			writeTestResults("Verify that user able to enter Float", "User should be able to enter Float",
					"User successfully enter Float", "pass");
		} else {
			writeTestResults("Verify that user able to enter Float", "User should be able to enter Float",
					"User doesn't enter Float", "fail");
		}

		sendKeys(txt_odLimit, amountFiftThousand);
		String enteredOdLimit = getAttribute(txt_odLimit, "value");
		if (enteredOdLimit.equals(amountFiftThousand)) {
			writeTestResults("Verify that user able to enter OD Limit", "User should be able to enter OD Limit",
					"User successfully enter OD Limit", "pass");
		} else {
			writeTestResults("Verify that user able to enter OD Limit", "User should be able to enter OD Limit",
					"User doesn't enter OD Limit", "fail");
		}

		sendKeys(txt_discountingMaximumCBB, amountFiftThousand);
		String enteredDiscountingMax = getAttribute(txt_discountingMaximumCBB, "value");
		if (enteredDiscountingMax.equals(amountFiftThousand)) {
			writeTestResults("Verify that user able to enter Discounting Maximum",
					"User should be able to enter Discounting Maximum", "User successfully enter Discounting Maximum",
					"pass");
		} else {
			writeTestResults("Verify that user able to enter Discounting Maximum",
					"User should be able to enter Discounting Maximum", "User doesn't enter Discounting Maximum",
					"fail");
		}

		sendKeys(txt_deawerCBB, drawer);
		String enteredDrawer = getAttribute(txt_deawerCBB, "value");
		if (enteredDrawer.equals(drawer)) {
			writeTestResults("Verify that user able to enter Drawer", "User should be able to enter Drawer",
					"User successfully enter Drawer", "pass");
		} else {
			writeTestResults("Verify that user able to enter Drawer", "User should be able to enter Drawer",
					"User doesn't enter Drawer", "fail");
		}

		sendKeys(txt_swiftCodeCBB, swiftCode);
		String enteredSwiftCode = getAttribute(txt_swiftCodeCBB, "value");
		if (enteredSwiftCode.equals(swiftCode)) {
			writeTestResults("Verify that user able to enter Swift Code", "User should be able to enter Swift Code",
					"User successfully enter Swift Code", "pass");
		} else {
			writeTestResults("Verify that user able to enter Swift Code", "User should be able to enter Swift Code",
					"User doesn't enter Swift Code", "fail");
		}

		click(btn_permissionGroupLookupCashBankBook);
		getInvObj().handeledSendKeys(txt_searcUserGroup, permissionGroupCBB);
		pressEnter(txt_searcUserGroup);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", permissionGroupCBB), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", permissionGroupCBB));
		Thread.sleep(2000);
		String chousedPermissionGroup = getAttribute(txt_permissionGroupFrontPageCBB, "value");
		if (chousedPermissionGroup.equals(permissionGroupCBB)) {
			writeTestResults("Verify that user able to choose Permission Group",
					"User should be able to choose Permission Group", "User successfully choose Permission Group",
					"pass");
		} else {
			writeTestResults("Verify that user able to choose Permission Group",
					"User should be able to choose Permission Group", "User doesn't choose Permission Group", "fail");
		}

		click(chk_leaseFacilityCBB);

		if (isSelected(chk_leaseFacilityCBB)) {
			writeTestResults("Verify that user able to select 'Lease Facility' check box",
					"User should be able to select 'Lease Facility' check box",
					"User successfully select 'Lease Facility' check box", "pass");
		} else {
			writeTestResults("Verify that user able to select 'Lease Facility' check box",
					"User should be able to select 'Lease Facility' check box",
					"User doesn't select 'Lease Facility' check box", "fail");
		}

		click(btn_draft);
		explicitWait(header_draftedCashBankBook, 40);
		if (isDisplayed(header_draftedCashBankBook)) {
			writeTestResults("Verify user can draft the Cash /Bank Book",
					"User should be able to draft the Cash /Bank Book", "User successfully draft the Cash /Bank Book",
					"pass");
		} else {
			writeTestResults("Verify user can draft the Cash /Bank Book",
					"User should be able to draft the Cash /Bank Book", "User doesn't draft the Cash /Bank Book",
					"fail");
		}

		explicitWait(btn_reelese, 20);
		click(btn_reelese);
		explicitWait(header_releasedCashBankBook, 40);
		trackCode = getText(lbl_docNoCashBankBook);
		bankAccount_FIN_CBB_25_26_27 = trackCode;
		if (isDisplayed(header_releasedCashBankBook)) {
			writeTestResults("Verify user can release the Cash /Bank Book",
					"User should be able to release the Cash /Bank Book",
					"User successfully release the Cash /Bank Book", "pass");
		} else {
			writeTestResults("Verify user can release the Cash /Bank Book",
					"User should be able to release the Cash /Bank Book", "User doesn't release the Cash /Bank Book",
					"fail");
		}

	}

	public void checkWithBankAdjustmentAndReconcilation_FIN_CBB_25_26_27() throws Exception {
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_bankaAdjustment, 20);
		if (isDisplayed(btn_bankaAdjustment)) {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option successfully available under finance module", "pass");
		} else {
			writeTestResults("Verify that Bank Adjustment option is available under finance module",
					"Bank Adjustment option should be available under finance module",
					"Bank Adjustment option doesn't available under finance module", "fail");
		}

		if (isEnabled(btn_bankaAdjustment)) {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option", "User able to click on Bank Adjustment option",
					"pass");
		} else {
			writeTestResults("Verify user able to click on Bank Adjustment option",
					"Should be able to click on Bank Adjustment option",
					"User not able to click on Bank Adjustment option", "fail");
		}

		click(btn_bankaAdjustment);
		explicitWait(header_bankAdjustmentByPage, 35);
		if (isEnabled(header_bankAdjustmentByPage)) {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User successfully directed to by-page Bank Adjustment", "pass");
		} else {
			writeTestResults("Verify user directed to by-page Bank Adjustment",
					"User should directed to by-page Bank Adjustment",
					"User doesn't directed to by-page Bank Adjustment", "fail");
		}

		click(btn_newBankAdjustment);
		explicitWait(header_newBankAdjustment, 35);
		if (isDisplayed(header_newBankAdjustment)) {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed",
					"Bank Adjustment - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify Bank Adjustment - New page is displayed",
					"Bank Adjustment - New page should be displayed", "Bank Adjustment - New page doesn't displayed",
					"fail");
		}

		selectText(drop_bank, sampathBank);
		String selectedBank = getSelectedOptionInDropdown(drop_bank);
		if (selectedBank.equals(sampathBank)) {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User successfully select any Bank from the drop down where the field will populate the selected value",
					"pass");
		} else {
			writeTestResults(
					"Verify user able able to select any Bank from the drop down where the field will populate the selected value",
					"User should be able to select any Bank from the drop down where the field will populate the selected value",
					"User doesn't select any Bank from the drop down where the field will populate the selected value",
					"fail");
		}

		List<String> bankAccountDropDown = new ArrayList<String>();
		Select dropdownBankAccount = new Select(driver.findElement(By.xpath(drop_bankAccountNo)));
		List<WebElement> op = dropdownBankAccount.getOptions();
		int siz = op.size();
		for (int i = 0; i < siz; i++) {
			String options = op.get(i).getText();
			bankAccountDropDown.add(options);
		}

		if (!bankAccountDropDown.contains(bankAccount_FIN_CBB_25_26_27)) {
			writeTestResults("Verify that Lease Facility Bank Book is not loaded in the drop down",
					"Lease Facility Bank Book should not loaded in the drop down",
					"Lease Facility Bank Book not loaded in the drop down", "pass");
		} else {
			writeTestResults("Verify that Lease Facility Bank Book is not loaded in the drop down",
					"Lease Facility Bank Book should not loaded in the drop down",
					"Lease Facility Bank Book was loaded in the drop down", "fail");
		}

		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_bankReconcillation, 20);
		if (isDisplayed(btn_bankReconcillation)) {
			writeTestResults("Verify that Bank Reconcilation option is available under finance module",
					"Bank Reconcilation option should be available under finance module",
					"Bank Reconcilation option successfully available under finance module", "pass");
		} else {
			writeTestResults("Verify that Bank Reconcilation option is available under finance module",
					"Bank Reconcilation option should be available under finance module",
					"Bank Reconcilation option doesn't available under finance module", "fail");
		}

		click(btn_bankReconcillation);
		Thread.sleep(2000);
		String header = getText(header_page);
		if (header.equals("Bank Reconciliation")) {
			writeTestResults("Verify user can navigate to bank reconciliation by-page",
					"User should be able to navigate to bank reconciliation by-page",
					"User successfully navigate to bank reconciliation by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to bank reconciliation by-page",
					"User should be able to navigate to bank reconciliation by-page",
					"User could'nt navigate to bank reconciliation by-page", "fail");
		}

		click(btn_newBankReconcilation);
		explicitWait(header_newBankReconcilation, 140);

		if (isDisplayed(header_newBankReconcilation)) {
			writeTestResults("Verify user can navigate to new bank reconciliation page",
					"User should be able to navigate to new bank reconciliation page",
					"User successfully navigate to new bank reconciliation page", "pass");
		} else {
			writeTestResults("Verify user can navigate to new bank reconciliation page",
					"User should be able to navigate to new bank reconciliation page",
					"User could'nt navigate to new bank reconciliation page", "fail");
		}

		selectText(drop_bank, sampathBank);
		if (isDisplayed(drop_bank)) {
			writeTestResults("Verify user can select the bank", "User should be able to select the bank",
					"User successfully select the bank", "pass");
		} else {
			writeTestResults("Verify user can select the bank", "User should be able to select the bank",
					"User could'nt select the bank", "fail");
		}

		List<String> bankAccountDropDownBankReconcil = new ArrayList<String>();
		Select dropdownBankAccountBankReconcil = new Select(driver.findElement(By.xpath(drop_bankAccountNo)));
		List<WebElement> opBankReconcil = dropdownBankAccountBankReconcil.getOptions();
		int sizBankReconcil = opBankReconcil.size();
		for (int i = 0; i < sizBankReconcil; i++) {
			String options = opBankReconcil.get(i).getText();
			bankAccountDropDownBankReconcil.add(options);
		}

		if (!bankAccountDropDownBankReconcil.contains(bankAccount_FIN_CBB_25_26_27)) {
			writeTestResults("Verify that Lease Facility Bank Book is not loaded in the drop down",
					"Lease Facility Bank Book should not loaded in the drop down",
					"Lease Facility Bank Book not loaded in the drop down", "pass");
		} else {
			writeTestResults("Verify that Lease Facility Bank Book is not loaded in the drop down",
					"Lease Facility Bank Book should not loaded in the drop down",
					"Lease Facility Bank Book was loaded in the drop down", "fail");
		}
	}

	/* FIN_CBB_28 */
	public void cashBankBookIOURimbursement_FIN_CBB_28() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 30);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);

		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_cashBankBook, 20);
		if (isDisplayed(btn_cashBankBook)) {
			writeTestResults("Verify that Cash/Bank Book option is available under Finance Module",
					"Cash/Bank Book option should be available under Finance Module",
					"Cash/Bank Book option successfully available under Finance Module", "pass");
		} else {
			writeTestResults("Verify that Cash/Bank Book option is available under Finance Module",
					"Cash/Bank Book option should be available under Finance Module",
					"Cash/Bank Book option successfully available under Finance Module", "fail");
		}

		if (isEnabled(btn_cashBankBook)) {
			writeTestResults("Verify user able to click on Cash/Bank Book option",
					"User should be able to click on Cash/Bank Book option",
					"User able to click on Cash/Bank Book option", "pass");
		} else {
			writeTestResults("Verify user able to click on Cash/Bank Book option",
					"User should be able to click on Cash/Bank Book option",
					"User not able to click on Cash/Bank Book option", "fail");
		}

		click(btn_cashBankBook);

		explicitWait(header_cashBankBookByPage, 40);
		if (isDisplayed(header_cashBankBookByPage)) {
			writeTestResults("Verify that user was directed to by-page Cash/Bank Book",
					"User should directed to by-page Cash/Bank Book",
					"User successfully directed to by-page Cash/Bank Book", "pass");
		} else {
			writeTestResults("Verify that user was directed to by-page Cash/Bank Book",
					"User should directed to by-page Cash/Bank Book", "User doesn't directed to by-page Cash/Bank Book",
					"fail");
		}

		click(btn_newCashBankBook);
		explicitWait(header_newCashBankBook, 40);
		if (isDisplayed(header_newCashBankBook)) {
			writeTestResults("Verify that Cash/Bank Book  - New page is displayed",
					"Cash/Bank Book  - New page should be displayed",
					"Cash/Bank Book  - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify that Cash/Bank Book  - New page is displayed",
					"Cash/Bank Book  - New page should be displayed", "Cash/Bank Book  - New page doesn't displayed",
					"fail");
		}

		selectText(drop_typeCashBankBook, cashBankBookTypeCash);
		String selectedBookType = getSelectedOptionInDropdown(drop_typeCashBankBook);
		if (selectedBookType.equals(cashBankBookTypeCash)) {
			writeTestResults("Verify that user able to select type as Cash",
					"User should be able to select type as Cash", "User successfully select type as Cash", "pass");
		} else {
			writeTestResults("Verify that user able to select type as Cash",
					"User should be able to select type as Cash", "User doesn't select type as Cash", "fail");
		}

		click(chk_iouCBB);

		if (!isSelected(chk_iouCBB)) {
			writeTestResults("Verify When 'Cash' is selected 'IOU Reimbursement' check box is disabled",
					"When 'Cash' is selected 'IOU Reimbursement' check box should be disabled",
					"When 'Cash' is selected 'IOU Reimbursement' check box successfully disabled", "pass");
		} else {
			writeTestResults("Verify When 'Cash' is selected 'IOU Reimbursement' check box is disabled",
					"When 'Cash' is selected 'IOU Reimbursement' check box should be disabled",
					"When 'Cash' is selected 'IOU Reimbursement' check box doesn't disabled", "fail");
		}

		selectText(drop_typeCashBankBook, cashBankBookTypeBank);
		String selectedBookTypeBank = getSelectedOptionInDropdown(drop_typeCashBankBook);
		if (selectedBookTypeBank.equals(cashBankBookTypeBank)) {
			writeTestResults("Verify that user able to select type as Cash",
					"User should be able to select type as Bank", "User successfully select type as Bank", "pass");
		} else {
			writeTestResults("Verify that user able to select type as Bank",
					"User should be able to select type as Bank", "User doesn't select type as Bank", "fail");
		}

		click(chk_iouCBB);

		if (!isSelected(chk_iouCBB)) {
			writeTestResults("Verify When 'Bank' is selected 'IOU Reimbursement' check box is disabled",
					"When 'Bank' is selected 'IOU Reimbursement' check box should be disabled",
					"When 'Bank' is selected 'IOU Reimbursement' check box successfully disabled", "pass");
		} else {
			writeTestResults("Verify When 'Bank' is selected 'IOU Reimbursement' check box is disabled",
					"When 'Bank' is selected 'IOU Reimbursement' check box should be disabled",
					"When 'Bank' is selected 'IOU Reimbursement' check box doesn't disabled", "fail");
		}

		selectText(drop_typeCashBankBook, bookTypePettyCash);
		String selectedBookTypePettyCash = getSelectedOptionInDropdown(drop_typeCashBankBook);
		if (selectedBookTypePettyCash.equals(bookTypePettyCash)) {
			writeTestResults("Verify that user able to select type as Petty Cash",
					"User should be able to select type as Petty Cash", "User successfully select type as Petty Cash",
					"pass");
		} else {
			writeTestResults("Verify that user able to select type as Petty Cash",
					"User should be able to select type as Petty Cash", "User doesn't select type as Petty Cash",
					"fail");
		}

		click(chk_iouCBB);

		if (isSelected(chk_iouCBB)) {
			writeTestResults(
					"Verify When 'Petty Cash' is selected 'IOU Reimbursement' check box is enabled and user can select checkbox",
					"When 'Petty Cash' is selected 'IOU Reimbursement' check box should be enabled and user can select checkbox",
					"When 'Petty Cash' is selected 'IOU Reimbursement' check box successfully enabled and user can select checkbox",
					"pass");
		} else {
			writeTestResults(
					"Verify When 'Petty Cash' is selected 'IOU Reimbursement' check box is enabled and user can select checkbox",
					"When 'Petty Cash' is selected 'IOU Reimbursement' check box should be enabled and user can select checkbox",
					"When 'Petty Cash' is selected 'IOU Reimbursement' check box doesn't enabled and user can select checkbox",
					"fail");
		}
	}

	/* FIN_CBB_29 */
	public void pettyCashAccount_FIN_CBB_29() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		click(navigation_pane);
		explicitWait(btn_financeModule, 30);
		if (isDisplayed(btn_financeModule)) {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Finance Module is displayed", "Finance Module should be displayed",
					"Finance Module not displayed", "fail");
		}

		click(btn_financeModule);

		explicitWait(btn_cashBankBook, 20);
		if (isDisplayed(btn_cashBankBook)) {
			writeTestResults("Verify that Cash/Bank Book option is available under Finance Module",
					"Cash/Bank Book option should be available under Finance Module",
					"Cash/Bank Book option successfully available under Finance Module", "pass");
		} else {
			writeTestResults("Verify that Cash/Bank Book option is available under Finance Module",
					"Cash/Bank Book option should be available under Finance Module",
					"Cash/Bank Book option successfully available under Finance Module", "fail");
		}

		if (isEnabled(btn_cashBankBook)) {
			writeTestResults("Verify user able to click on Cash/Bank Book option",
					"User should be able to click on Cash/Bank Book option",
					"User able to click on Cash/Bank Book option", "pass");
		} else {
			writeTestResults("Verify user able to click on Cash/Bank Book option",
					"User should be able to click on Cash/Bank Book option",
					"User not able to click on Cash/Bank Book option", "fail");
		}

		click(btn_cashBankBook);

		explicitWait(header_cashBankBookByPage, 40);
		if (isDisplayed(header_cashBankBookByPage)) {
			writeTestResults("Verify that user was directed to by-page Cash/Bank Book",
					"User should directed to by-page Cash/Bank Book",
					"User successfully directed to by-page Cash/Bank Book", "pass");
		} else {
			writeTestResults("Verify that user was directed to by-page Cash/Bank Book",
					"User should directed to by-page Cash/Bank Book", "User doesn't directed to by-page Cash/Bank Book",
					"fail");
		}

		click(btn_newCashBankBook);
		explicitWait(header_newCashBankBook, 40);
		if (isDisplayed(header_newCashBankBook)) {
			writeTestResults("Verify that Cash/Bank Book  - New page is displayed",
					"Cash/Bank Book  - New page should be displayed",
					"Cash/Bank Book  - New page successfully displayed", "pass");
		} else {
			writeTestResults("Verify that Cash/Bank Book  - New page is displayed",
					"Cash/Bank Book  - New page should be displayed", "Cash/Bank Book  - New page doesn't displayed",
					"fail");
		}

		selectText(drop_typeCashBankBook, bookTypePettyCash);
		String selectedBookType = getSelectedOptionInDropdown(drop_typeCashBankBook);
		if (selectedBookType.equals(bookTypePettyCash)) {
			writeTestResults("Verify that user able to select type as Petty Cash",
					"User should be able to select type as Petty Cash", "User successfully select type as Petty Cash",
					"pass");
		} else {
			writeTestResults("Verify that user able to select type as Petty Cash",
					"User should be able to select type as Petty Cash", "User doesn't select type as Petty Cash",
					"fail");
		}

		String accountNumber = getInvObj().currentTimeAndDate().replace("/", "").replace(":", "");
		sendKeys(txt_bankAccountNoCashBankBook, accountNumber);
		String setAccountNumber = getAttribute(txt_bankAccountNoCashBankBook, "value");
		if (setAccountNumber.equals(accountNumber)) {
			writeTestResults("Verify that user able to enter Account Number",
					"User should be able to enter Account Number", "User successfully enter Account Number", "pass");
		} else {
			writeTestResults("Verify that user able to enter Account Number",
					"User should be able to enter Account Number", "User doesn't enter Account Number", "fail");
		}

		selectText(drop_currency, currencyLKR);
		String selectedCurrency = getSelectedOptionInDropdown(drop_currency);
		if (selectedCurrency.equals(currencyLKR)) {
			writeTestResults("Verify that user able to select currency as LKR",
					"User should be able to select currency as LKR", "User successfully select currency as LKR",
					"pass");
		} else {
			writeTestResults("Verify that user able to select currency as LKR",
					"User should be able to select currency as LKR", "User doesn't select currency as LKR", "fail");
		}

		click(chk_iouCBB);

		click(btn_draft);
		explicitWait(lbl_mainValidation, 40);
		if (isDisplayed(lbl_mainValidation)) {
			writeTestResults("Verify main validation is appear", "Main validation should be appear",
					"Main validation successfully appear", "pass");
		} else {
			writeTestResults("Verify main validation is appear", "Main validation should be appear",
					"Main validation doesn't appear", "fail");
		}

		if (isDisplayed(error_floatFieldMandatoryValidationCashBankBook)) {
			writeTestResults("Verify that 'You must enter a value.' validation message is displayed in 'Float' field",
					"'You must enter a value.' validation message should be displayed in 'Float' field",
					"'You must enter a value.' validation message successfully displayed in 'Float' field", "pass");
		} else {
			writeTestResults("Verify that 'You must enter a value.' validation message is displayed in 'Float' field",
					"'You must enter a value.' validation message should be displayed in 'Float' field",
					"'You must enter a value.' validation message doesn't displayed in 'Float' field", "fail");
		}

		if (isDisplayed(error_borderFloatCashBankBook)) {
			writeTestResults("Verify error border for float field is appear",
					"Error border for float field should be appear", "Error border for float field successfully appear",
					"pass");
		} else {
			writeTestResults("Verify error border for float field is appear",
					"Error border for float field should be appear", "Error border for float field doesn't appear",
					"fail");
		}
	}

}
