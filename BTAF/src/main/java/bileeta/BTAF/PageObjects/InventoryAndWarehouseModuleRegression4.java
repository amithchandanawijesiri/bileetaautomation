package bileeta.BTAF.PageObjects;

import java.io.IOException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import bileeta.BATF.Pages.InventoryAndWarehouseModuleData;

public class InventoryAndWarehouseModuleRegression4 extends InventoryAndWarehouseModuleData {

	/* common objects */
	private InventoryAndWarehouseModuleRegression2 invObjReg2 = new InventoryAndWarehouseModuleRegression2();
	private InventoryAndWarehouseModuleRegression invObjReg = new InventoryAndWarehouseModuleRegression();
	private InventoryAndWarehouseModule invObj = new InventoryAndWarehouseModule();

	/* Getters and Setters */
	public InventoryAndWarehouseModule getInvObj() {
		return invObj;
	}

	public InventoryAndWarehouseModuleRegression2 getInvObjReg2() {
		return invObjReg2;
	}

	public InventoryAndWarehouseModuleRegression getInvObjReg() {
		return invObjReg;
	}

	/* Test case variables */
	String costPrice;

	/* IN_PC_008 */
	public void checkAndStoreLastCostPriceOfTheProduct_IN_PC_008() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(inventory_module, 45);

		getInvObjReg().customizeLoadingDelay(btn_product_info, 45);
		click(btn_product_info);

		getInvObjReg().customizeLoadingDelay(txt_productSearch, 45);
		sendKeys(txt_productSearch, getInvObj().readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);

		getInvObjReg().customizeLoadingDelay(
				btn_productMasterWidget.replace("product", getInvObj().readTestCreation("BatchSpecific")), 45);

		click(btn_productMasterWidget.replace("product", getInvObj().readTestCreation("BatchSpecific")));

		getInvObjReg().customizeLoadingDelay(btn_productRelatedPriceOnWidgetMaster, 45);

		click(btn_productRelatedPriceOnWidgetMaster);

		getInvObjReg().customizeLoadingDelay(lbl_costPrice, 45);
		costPrice = getText(lbl_costPrice);

		click(span_closeButtonLast);

	}

	public void compareCostPriceWithGridLastCostPrice_IN_PC_008()
			throws InvalidFormatException, IOException, Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}
		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, productConversionWarehouseRegression);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify user allow to select warehouse", "User should allow to select warehouse",
					"User allow to select warehouse", "pass");
		} else {
			writeTestResults("Verify user allow to select warehouse", "User should allow to select warehouse",
					"User not allow to select warehouse", "fail");
		}
		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup successfully pop-up", "pass");
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		boolean flagVerifyProductAndLAstCostPrice = false;

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			flagVerifyProductAndLAstCostPrice = true;
		} else {
			flagVerifyProductAndLAstCostPrice = false;
		}

		String lastCostPriceGrid = getAttribute(td_lastCostPriceProductConversionGrid, "value");

		if (flagVerifyProductAndLAstCostPrice) {

			if (lastCostPriceGrid.equals(costPrice)) {
				flagVerifyProductAndLAstCostPrice = true;
			} else {
				flagVerifyProductAndLAstCostPrice = false;
			}

		}

		if (flagVerifyProductAndLAstCostPrice) {
			writeTestResults(
					"Verify selected product is added to the grid and last cost price for the selected product is display accordingly",
					"Selected product should be added to the grid and last cost price for the selected product should display accordingly",
					"Selected product successfully added to the grid and last cost price for the selected product display accordingly",
					"pass");
		} else {
			writeTestResults(
					"Verify selected product is added to the grid and last cost price for the selected product is display accordingly",
					"Selected product should be added to the grid and last cost price for the selected product should display accordingly",
					"Selected product doesn't add to the grid and/or the last cost price for the selected product doesn't display accordingly",
					"fail");
		}

	}

	/* IN_PC_009 */
	public void captureSerielBatchesOfFromProduct_IN_PC_009() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}
		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, productConversionWarehouseRegression);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify user allow to select warehouse", "User should allow to select warehouse",
					"User allow to select warehouse", "pass");
		} else {
			writeTestResults("Verify user allow to select warehouse", "User should allow to select warehouse",
					"User not allow to select warehouse", "fail");
		}

		/* BATCH SPECIFIC */
		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {

		} else {
			writeTestResults("Verify relevent product added to the grid",
					"Relevent product should be added to the grid", "Relevent product doesn't added to the grid",
					"fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"),
				qtyFromProductProductConversion);

		click(btn_serielBatchCaptureNosProductConversionRowReplace.replace("row", "1"));
		getInvObjReg().customizeLoadingDelayAndContinue(header_batchCaptureWindowProductConversion, 20);

		boolean flagSerielBatchCapturingWindow = false;

		if (isDisplayed(header_batchCaptureWindowProductConversion)) {
			flagSerielBatchCapturingWindow = true;
		} else {
			flagSerielBatchCapturingWindow = false;
		}

		click(btn_seraialBatchCaptureDocIconProductConversion);

		getInvObjReg().customizeLoadingDelay(chk_selectBatchForBatchSpecificProduct, 20);
		click(chk_selectBatchForBatchSpecificProduct);

		Thread.sleep(2000);

		boolean flagSerielBatchCapturing = false;

		String capturedQuantityBatchSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantityBatchSpecific.equals(capturedQuantityProductConversion)) {
			flagSerielBatchCapturing = true;
		} else {
			flagSerielBatchCapturing = false;
		}

		click(btn_appalySerielBatchesFromProductProductConversion);

		boolean getBackToProductConversionPage = false;
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 30);
		if (isDisplayed(header_newProductConversion)) {
			getBackToProductConversionPage = true;
		} else {
			getBackToProductConversionPage = false;
		}

		click(btn_addNewRowProductConversion);

		/* BATCH FIFO */
		click(btn_productLookupRowReplaceProductConversion.replace("row", "2"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchFifo")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchFifo")))) {

		} else {
			writeTestResults("Verify relevent product added to the grid",
					"Relevent product should be added to the grid", "Relevent product doesn't added to the grid",
					"fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "2"),
				qtyFromProductProductConversion);

		click(btn_serielBatchCaptureNosProductConversionRowReplace.replace("row", "2"));
		getInvObjReg().customizeLoadingDelayAndContinue(header_batchCaptureWindowProductConversion, 20);

		if (flagSerielBatchCapturingWindow) {
			if (isDisplayed(header_batchCaptureWindowProductConversion)) {
				flagSerielBatchCapturingWindow = true;
			} else {
				flagSerielBatchCapturingWindow = false;
			}
		}

		click(btn_seraialBatchCaptureDocIconProductConversion);

		getInvObjReg().customizeLoadingDelay(chk_selectBatchForBatchSpecificProduct, 20);
		click(chk_selectBatchForBatchSpecificProduct);

		Thread.sleep(2000);

		String capturedQuantityBatchFifo = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (flagSerielBatchCapturing) {
			if (capturedQuantityBatchFifo.equals(capturedQuantityProductConversion)) {
				flagSerielBatchCapturing = true;
			} else {
				flagSerielBatchCapturing = false;
			}
		}

		click(btn_appalySerielBatchesFromProductProductConversion);

		if (getBackToProductConversionPage) {
			getInvObjReg().customizeLoadingDelay(header_newProductConversion, 30);
			if (isDisplayed(header_newProductConversion)) {
				getBackToProductConversionPage = true;
			} else {
				getBackToProductConversionPage = false;
			}
		}

		getInvObjReg().customizeLoadingDelayAndContinue(btn_addNewRowProductConversion, 30);
		click(btn_addNewRowProductConversion);

		/* SERIEL SPECIFIC */
		click(btn_productLookupRowReplaceProductConversion.replace("row", "3"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")))) {

		} else {
			writeTestResults("Verify relevent product added to the grid",
					"Relevent product should be added to the grid", "Relevent product doesn't added to the grid",
					"fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "3"),
				qtyFromProductProductConversion);

		click(btn_serielBatchCaptureNosProductConversionRowReplace.replace("row", "3"));
		getInvObjReg().customizeLoadingDelayAndContinue(header_serileCaptureWindowProductConversion, 20);

		if (flagSerielBatchCapturingWindow) {
			if (isDisplayed(header_serileCaptureWindowProductConversion)) {
				flagSerielBatchCapturingWindow = true;
			} else {
				flagSerielBatchCapturingWindow = false;
			}
		}

		click(btn_seraialBatchCaptureDocIconProductConversion);

		getInvObjReg().customizeLoadingDelay(chk_serielRangeProductConversion, 20);
		Thread.sleep(1500);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, qtyFromProductProductConversion);
		sendKeys(txt_serielNo, getInvObj().getOngoinfSerirlSpecificSerielNumber(10));
		pressEnter(txt_serielNo);
		Thread.sleep(2000);

		String capturedQuantitySerielSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (flagSerielBatchCapturing) {
			if (capturedQuantitySerielSpecific.equals(capturedQuantityProductConversion)) {
				flagSerielBatchCapturing = true;
			} else {
				flagSerielBatchCapturing = false;
			}
		}

		click(btn_appalySerielBatchesFromProductProductConversion);

		if (getBackToProductConversionPage) {
			getInvObjReg().customizeLoadingDelay(header_newProductConversion, 30);
			if (isDisplayed(header_newProductConversion)) {
				getBackToProductConversionPage = true;
			} else {
				getBackToProductConversionPage = false;
			}
		}

		click(btn_addNewRowProductConversion);

		/* SERIEL BATCH SPECIFIC */
		click(btn_productLookupRowReplaceProductConversion.replace("row", "4"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielBatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielBatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielBatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielBatchSpecific")),
				10);

		if (isDisplayed(div_productOnGridProductConversation.replace("product",
				invObj.readTestCreation("SerielBatchSpecific")))) {

		} else {
			writeTestResults("Verify relevent product added to the grid",
					"Relevent product should be added to the grid", "Relevent product doesn't added to the grid",
					"fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "4"),
				qtyFromProductProductConversion);

		click(btn_serielBatchCaptureNosProductConversionRowReplace.replace("row", "4"));
		getInvObjReg().customizeLoadingDelayAndContinue(header_serileCaptureWindowProductConversion, 20);

		if (flagSerielBatchCapturingWindow) {
			if (isDisplayed(header_serileCaptureWindowProductConversion)) {
				flagSerielBatchCapturingWindow = true;
			} else {
				flagSerielBatchCapturingWindow = false;
			}
		}

		click(btn_seraialBatchCaptureDocIconProductConversion);

		getInvObjReg().customizeLoadingDelay(chk_serielRangeProductConversion, 20);
		Thread.sleep(1500);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, qtyFromProductProductConversion);
		sendKeys(txt_serielNo, getInvObj().getOngoingSerielBatchSpecificSerielNumber(10));
		pressEnter(txt_serielNo);
		Thread.sleep(2000);

		String capturedQuantitySerielBatchSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (flagSerielBatchCapturing) {
			if (capturedQuantitySerielBatchSpecific.equals(capturedQuantityProductConversion)) {
				flagSerielBatchCapturing = true;
			} else {
				flagSerielBatchCapturing = false;
			}
		}

		click(btn_appalySerielBatchesFromProductProductConversion);
		if (getBackToProductConversionPage) {
			getInvObjReg().customizeLoadingDelay(header_newProductConversion, 30);
			if (isDisplayed(header_newProductConversion)) {
				getBackToProductConversionPage = true;
			} else {
				getBackToProductConversionPage = false;
			}
		}

		if (flagSerielBatchCapturingWindow) {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window successfully pop-up",
					"pass");
		} else {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window doesn't pop-up",
					"fail");

		}

		if (getBackToProductConversionPage) {
			writeTestResults("Verify system is navigate to product conversion form",
					"System should navigate to product conversion form",
					"System successfully navigate to product conversion form", "pass");
		} else {
			writeTestResults("Verify system is navigate to product conversion form",
					"System should navigate to product conversion form",
					"System doesn't navigate to product conversion form", "fail");

		}

		if (flagSerielBatchCapturing) {
			writeTestResults("Verify user allow to capture serial/batches",
					"User should allow to capture serial/batches", "User allow to capture serial/batches", "pass");
		} else {
			writeTestResults("Verify user allow to capture serial/batches",
					"User should allow to capture serial/batches", "User not allow to capture serial/batches", "fail");

		}
	}

	/* IN_PC_010 */
	public void tryToSelectToProductWithoutSelectingFromProduct_IN_PC_010() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}
		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, productConversionWarehouseRegression);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify user allow to select warehouse", "User should allow to select warehouse",
					"User allow to select warehouse", "pass");
		} else {
			writeTestResults("Verify user allow to select warehouse", "User should allow to select warehouse",
					"User not allow to select warehouse", "fail");
		}

		click(btn_productLookupRowReplaceProductConversionToProduct);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 6);
		if (!getInvObj().isDisplayedQuickCheck(header_productLookupProductConversion)) {
			writeTestResults("Verify user not allow to select \"To product\" without selecting \"From product\"",
					"User should not allow to select \"To product\" without selecting \"From product\"",
					"User not allow to select \"To product\" without selecting \"From product\"", "pass");
		} else {
			writeTestResults("Verify user not allow to select \"To product\" without selecting \"From product\"",
					"User should not allow to select \"To product\" without selecting \"From product\"",
					"User allow to select \"To product\" without selecting \"From product\"", "fail");
		}
	}

	/* IN_PC_011 */
	public void createProductAndInactive_IN_PC_011() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObj().sleepCusomized(navigation_pane);
		getInvObj().waitImplicit(navigation_pane, 10);
		click(navigation_pane);
		getInvObj().sleepCusomized(inventory_module);
		click(inventory_module);
		getInvObj().customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		getInvObj().customizeLoadingDelay(btn_new_product, 8);

		Thread.sleep(1500);
		click(btn_new_product);

		// product code
		String product_code = (getInvObj().currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")
				+ "-BATCH-SPECIFIC");
		sendKeys(txt_product_code, product_code);

		// product description
		sendKeys(txt_product_desc, "DESC" + product_code);

		// product group
		selectText(dropdown_product_group, productionGroupBatchProductCreation);

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		getInvObj().sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		String menufacturur = getInvObj().readIWData(2);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturur);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", getInvObj().readIWData(2)));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);

		// product width
		getInvObj().customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		click(tab_detail);
		Thread.sleep(3000);
		getInvObj().customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);
		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		Thread.sleep(3000);
		click(chk_batchProductDispatchOnlyBatchProduct);
		Thread.sleep(3000);
		pageScrollDown();
		pageScrollUpToTop();
		selectText(dropdown_outbound_costing_method, outboundCostingMethod);
		Select comboBox = new Select(driver.findElement(By.xpath(dropdown_outbound_costing_method)));
		Thread.sleep(3000);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (!selectedComboValue.equals(outboundCostingMethod)) {
			selectText(dropdown_outbound_costing_method, outboundCostingMethod);
			Thread.sleep(2000);
		}

		click(checkbox_without_costing);
		click(chk_allocationBatchProduct);

		click(checkbox_validate_expiery);
		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		if (!isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		Thread.sleep(1000);
		if (!isSelected(checkbox_validate_expiery_date)) {
			click(checkbox_validate_expiery_date);
		}

		sendKeys(txt_minPriceTextProductCreation, minPriceCoommnonCreateProduct);
		Thread.sleep(1000);
		sendKeys(txt_maxPriceTextProductCreation, maxPriceCoommnonCreateProduct);
		Thread.sleep(1000);

		Thread.sleep(1000);

		click(btn_draft);

		getInvObj().customizeLoadingDelay(btn_releese2, 30);
		click(btn_releese2);
		getInvObj().customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 40);
		getInvObj().writeRegression01("BatchSpecific_IN_PC_011", product_code, 164);

		click(btn_editActionVerification);
		getInvObj().customizeLoadingDelay(btn_updateActionVerification, 40);
		click(btn_statusMenu);
		selectText(drop_docStatus, inactiveStatus);
		click(btn_applyStatusMenu);
		Thread.sleep(1000);
		click(btn_updateActionVerification);
		getInvObj().customizeLoadingDelay(btn_editActionVerification, 40);
	}

	public void checkToProductSelection_IN_PC_011() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}
		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, productConversionWarehouseRegression);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify user allow to select warehouse", "User should allow to select warehouse",
					"User allow to select warehouse", "pass");
		} else {
			writeTestResults("Verify user allow to select warehouse", "User should allow to select warehouse",
					"User not allow to select warehouse", "fail");
		}

		/* Active Product */
		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User allow to select \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User not allow to select \"From Product\"",
					"fail");
		}

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup successfully pop-up", "pass");
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchFifo")), 10);

		boolean flagActiveProduct = false;
		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("BatchFifo")))) {
			flagActiveProduct = true;

		} else {
			flagActiveProduct = false;
		}

		/* Inactive Product */
		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup successfully pop-up", "pass");
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readIWRegression01("BatchSpecific_IN_PC_011"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelayAndContinue(
				result_cpmmonProduct.replace("product", invObj.readIWRegression01("BatchSpecific_IN_PC_011")), 10);

		boolean flagInactiveProduct = false;
		if (!getInvObj().isDisplayedQuickCheck(
				result_cpmmonProduct.replace("product", invObj.readIWRegression01("BatchSpecific_IN_PC_011")))) {
			flagInactiveProduct = true;

		} else {
			flagInactiveProduct = false;
		}

		if (flagActiveProduct && flagInactiveProduct) {
			writeTestResults("Verify selected product is added to the grid",
					"Selected product should be added to the grid", "Selected product successfully added to the grid",
					"pass");
		} else {
			writeTestResults("Verify selected product is added to the grid",
					"Selected product should be added to the grid", "Selected product doesn't added to the grid",
					"fail");
		}
	}

	/* IN_PC_012 */
	public void checkDefoultUomToProduct_IN_PC_012() throws InvalidFormatException, IOException, Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}
		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, productConversionWarehouseRegression);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify user allow to select warehouse", "User should allow to select warehouse",
					"User allow to select warehouse", "pass");
		} else {
			writeTestResults("Verify user allow to select warehouse", "User should allow to select warehouse",
					"User not allow to select warehouse", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User allow to select \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User not allow to select \"From Product\"",
					"fail");
		}

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup successfully pop-up", "pass");
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchFifo")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("BatchFifo")))) {
			writeTestResults("Verify selected product is added to the grid",
					"Selected product should be added to the grid", "Selected product successfully added to the grid",
					"pass");
		} else {
			writeTestResults("Verify selected product is added to the grid",
					"Selected product should be added to the grid", "Selected product doesn't added to the grid",
					"fail");
		}

		String gridDefoultUom = getText(div_defoultUomToProductProductConversion);

		if (gridDefoultUom.equals(productDefoultUom)) {
			writeTestResults("Verify Default UOM for the selected product is display correctly",
					"Default UOM for the selected product should display correctly",
					"Default UOM for the selected product display correctly", "pass");
		} else {
			writeTestResults("Verify Default UOM for the selected product is display correctly",
					"Default UOM for the selected product should display correctly",
					"Default UOM for the selected product doesn't display correctly", "fail");
		}

	}

	/* IN_PC_013 */
	public void enterCostPriceForToProduct_IN_PC_013() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}
		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, productConversionWarehouseRegression);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify user allow to select warehouse", "User should allow to select warehouse",
					"User allow to select warehouse", "pass");
		} else {
			writeTestResults("Verify user allow to select warehouse", "User should allow to select warehouse",
					"User not allow to select warehouse", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User allow to select \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User not allow to select \"From Product\"",
					"fail");
		}

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchFifo")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("BatchFifo")))) {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User allow to select \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User not allow to select \"To Product\"", "fail");
		}

		sendKeys(txt_costPriceProductConversion, costPriceProductConversion);
		click(tab_summaryCommon);
		String gridCostPrice = getAttribute(txt_costPriceProductConversion, "value");

		if (gridCostPrice.equals(costPriceProductConversion)) {
			writeTestResults("Verify user allow to add cost price with decimal places",
					"User should allow to add cost price with decimal places",
					"User allow to add cost price with decimal places", "pass");
		} else {
			writeTestResults("Verify user allow to add cost price with decimal places",
					"User should allow to add cost price with decimal places",
					"User not allow to add cost price with decimal places", "fail");
		}
	}

	/* IN_PC_014 */
	public void checkQuantityWithConRate_IN_PC_014() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, productConversionWarehouseRegression);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User allow to fill summary details", "pass");
		} else {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User not allow to fill summary details", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User allow to select \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User not allow to select \"From Product\"",
					"fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"),
				qtyFromProductProductConversion);

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchFifo")), 10);

		String gridQuantityFromProduct = getAttribute(
				txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), "value");

		if (gridQuantityFromProduct.equals(qtyFromProductProductConversion)) {
			writeTestResults("Verify user allow to enter from product quantity",
					"User should allow to enter from product quantity", "User allow to enter from product quantity",
					"pass");
		} else {
			writeTestResults("Verify user allow to enter from product quantity",
					"User should allow to enter from product quantity", "User not allow to enter from product quantity",
					"fail");
		}

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("BatchFifo")))) {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User allow to select \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User not allow to select \"To Product\"", "fail");
		}

		sendKeys(txt_conRateProductConversion, conRateProductConversion);
		click(tab_summaryCommon);

		String conRateOnGrid = getAttribute(txt_conRateProductConversion, "value");

		if (conRateOnGrid.equals(conRateProductConversion + ".0000")) {
			writeTestResults("Verify user allow to add con rate", "User should allow to add con rate",
					"User allow to add con rate", "pass");
		} else {
			writeTestResults("Verify user allow to add con rate", "User should allow to add con rate",
					"User not allow to add con rate", "fail");
		}

		String quantityAccordingToConRateOnGrid = getAttribute(txt_quantityToProductProductConversion, "value");

		if (quantityAccordingToConRateOnGrid.equals(quantityAccordingToConRate)) {
			writeTestResults("Verify \"To product\" qty is change by multiplying qty of From product into Con Rate",
					"\"To product\" qty should change by multiplying qty of From product into Con Rate",
					"\"To product\" qty successfully change by multiplying qty of From product into Con Rate", "pass");
		} else {
			writeTestResults("Verify \"To product\" qty is change by multiplying qty of From product into Con Rate",
					"\"To product\" qty should change by multiplying qty of From product into Con Rate",
					"\"To product\" qty doesn't change by multiplying qty of From product into Con Rate", "fail");
		}
	}

	/* IN_PC_015 */
	public void checkConRateWithToProductQuantity_IN_PC_015() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, productConversionWarehouseRegression);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User allow to fill summary details", "pass");
		} else {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User not allow to fill summary details", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User allow to select \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User not allow to select \"From Product\"",
					"fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"),
				qtyFromProductProductConversion);

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchFifo")), 10);

		String gridQuantityFromProduct = getAttribute(
				txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), "value");

		if (gridQuantityFromProduct.equals(qtyFromProductProductConversion)) {
			writeTestResults("Verify user allow to enter from product quantity",
					"User should allow to enter from product quantity", "User allow to enter from product quantity",
					"pass");
		} else {
			writeTestResults("Verify user allow to enter from product quantity",
					"User should allow to enter from product quantity", "User not allow to enter from product quantity",
					"fail");
		}

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("BatchFifo")))) {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User allow to select \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User not allow to select \"To Product\"", "fail");
		}

		sendKeys(txt_quantityToProductProductConversion, toQuantityForCheckConRate);
		click(tab_summaryCommon);

		String toQuantityeOnGrid = getAttribute(txt_quantityToProductProductConversion, "value");

		if (toQuantityeOnGrid.equals(toQuantityForCheckConRate)) {
			writeTestResults("Verify user allow to add qty of \"To product\"",
					"User should allow to add qty of \"To product\"", "User allow to add qty of \"To product\"",
					"pass");
		} else {
			writeTestResults("Verify user allow to add qty of \"To product\"",
					"User should allow to add qty of \"To product\"", "User not allow to add qty of \"To product\"",
					"fail");
		}

		String conRateAccordingToToQuantityOnGrid = getAttribute(txt_conRateProductConversion, "value");

		if (conRateAccordingToToQuantityOnGrid.equals(conRateAccordingToToQuantity + ".0000")) {
			writeTestResults("Verify Con Rate is change by dividing \"To product\" Qty with \"From product\" Qty",
					"Con Rate should change by dividing \"To product\" Qty with \"From product\" Qty",
					"Con Rate successfully change by dividing \"To product\" Qty with \"From product\" Qty", "pass");
		} else {
			writeTestResults("Verify Con Rate is change by dividing \"To product\" Qty with \"From product\" Qty",
					"Con Rate should change by dividing \"To product\" Qty with \"From product\" Qty",
					"Con Rate doesn't change by dividing \"To product\" Qty with \"From product\" Qty", "fail");
		}
	}

	/* IN_PC_016 */
	public void createDecimalAllowLotProduct_IN_PC_016() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 30);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 30);

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_product_info, 30);
		Thread.sleep(1000);
		click(btn_product_info);
		getInvObjReg().customizeLoadingDelay(btn_new_product, 30);
		Thread.sleep(1500);
		click(btn_new_product);

		// product code
		String product_code_lot = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "-LOT");
		sendKeys(txt_product_code, product_code_lot);

		// product description
		sendKeys(txt_product_desc, productDesription);

		// product group
		selectText(dropdown_product_group, productionGroupBatchProductCreation);

		click(chk_allowDecimal);

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		getInvObjReg().customizeLoadingDelay(txt_menufacturerDispatchOnlyBatchProduct, 30);
		Thread.sleep(3000);
		String menufacturur = invObj.readIWData(2);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturur);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", invObj.readIWData(2)));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);
		// product width
		getInvObjReg().customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		click(tab_detail);
		getInvObjReg().customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);

		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		click(checkbox_without_costing);

		click(chk_allocationBatchProduct);

		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		Thread.sleep(3000);
		if (!isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		sendKeys(txt_minPriceTextProductCreation, minPriceCoommnonCreateProduct);
		Thread.sleep(1000);
		sendKeys(txt_maxPriceTextProductCreation, maxPriceCoommnonCreateProduct);
		Thread.sleep(1000);

		click(btn_draft);
		invObj.customizeLoadingDelay(btn_releese2, 40);

		click(btn_releese2);
		invObj.customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 40);
		invObj.writeRegression01("LotDecimalAllow_IN_PC_016", product_code_lot, 165);
	}

	public void createBatchSpecificProductAllowDecimal_IN_PC_016() throws Exception {
		getInvObj().sleepCusomized(navigation_pane);
		getInvObj().waitImplicit(navigation_pane, 10);
		click(navigation_pane);
		getInvObj().sleepCusomized(inventory_module);
		click(inventory_module);
		getInvObj().customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		getInvObj().customizeLoadingDelay(btn_new_product, 8);

		Thread.sleep(1500);
		click(btn_new_product);

		// product code
		String product_code = (getInvObj().currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")
				+ "-BATCH-SPECIFIC");
		sendKeys(txt_product_code, product_code);

		// product description
		sendKeys(txt_product_desc, "DESC" + product_code);

		// product group
		selectText(dropdown_product_group, productionGroupBatchProductCreation);

		click(chk_allowDecimal);

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		getInvObj().sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		String menufacturur = getInvObj().readIWData(2);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturur);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", getInvObj().readIWData(2)));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);

		// product width
		getInvObj().customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		click(tab_detail);
		Thread.sleep(3000);
		getInvObj().customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);
		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		Thread.sleep(3000);
		click(chk_batchProductDispatchOnlyBatchProduct);
		Thread.sleep(3000);
		pageScrollDown();
		pageScrollUpToTop();
		selectText(dropdown_outbound_costing_method, outboundCostingMethod);
		Select comboBox = new Select(driver.findElement(By.xpath(dropdown_outbound_costing_method)));
		Thread.sleep(3000);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (!selectedComboValue.equals(outboundCostingMethod)) {
			selectText(dropdown_outbound_costing_method, outboundCostingMethod);
			Thread.sleep(2000);
		}

		click(checkbox_without_costing);
		click(chk_allocationBatchProduct);

		click(checkbox_validate_expiery);
		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		if (!isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		Thread.sleep(1000);
		if (!isSelected(checkbox_validate_expiery_date)) {
			click(checkbox_validate_expiery_date);
		}

		sendKeys(txt_minPriceTextProductCreation, minPriceCoommnonCreateProduct);
		Thread.sleep(1000);
		sendKeys(txt_maxPriceTextProductCreation, maxPriceCoommnonCreateProduct);
		Thread.sleep(1000);

		Thread.sleep(1000);

		click(btn_draft);

		getInvObj().customizeLoadingDelay(btn_releese2, 30);
		click(btn_releese2);
		getInvObj().customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 40);
		getInvObj().writeRegression01("BatchSpecificAllowDecimal_IN_PC_016", product_code, 166);

	}

	public void checkDecimalQuantityWithDecimalAllowProduct_IN_PC_016() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, productConversionWarehouseRegression);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User allow to fill summary details", "pass");
		} else {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User not allow to fill summary details", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readIWRegression01("LotDecimalAllow_IN_PC_016"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readIWRegression01("LotDecimalAllow_IN_PC_016")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readIWRegression01("LotDecimalAllow_IN_PC_016")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(div_productOnGridProductConversation.replace("product",
				invObj.readIWRegression01("LotDecimalAllow_IN_PC_016")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("product",
				invObj.readIWRegression01("LotDecimalAllow_IN_PC_016")))) {
			writeTestResults("Verify user allow to select allow decimal enabled product as \"From Product\"",
					"User should allow to select allow decimal enabled product as \"From Product\"",
					"User allow to select allow decimal enabled product as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select allow decimal enabled product as \"From Product\"",
					"User should allow to select allow decimal enabled product as \"From Product\"",
					"User not allow to select allow decimal enabled product as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"),
				decimalQtyFromProductProductConversion);

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readIWRegression01("BatchSpecificAllowDecimal_IN_PC_016"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(result_cpmmonProduct.replace("product",
				invObj.readIWRegression01("BatchSpecificAllowDecimal_IN_PC_016")), 20);
		doubleClick(result_cpmmonProduct.replace("product",
				invObj.readIWRegression01("BatchSpecificAllowDecimal_IN_PC_016")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(div_productOnGridProductConversation.replace("product",
				invObj.readIWRegression01("BatchSpecificAllowDecimal_IN_PC_016")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readIWRegression01("BatchSpecificAllowDecimal_IN_PC_016")))) {
			writeTestResults("Verify user allow to select allow decimal enabled product as \"To Product\"",
					"User should allow to select allow decimal enabled product as \"To Product\"",
					"User allow to select allow decimal enabled product as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to allow decimal enabled product as select \"To Product\"",
					"User should allow to select allow decimal enabled product as \"To Product\"",
					"User not allow to select allow decimal enabled product as \"To Product\"", "fail");
		}

		sendKeys(txt_quantityToProductProductConversion, decimalQtyToProductProductConversion);
		click(tab_summaryCommon);

		String gridQuantityFromProduct = getAttribute(
				txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), "value");

		if (gridQuantityFromProduct.equals(decimalQtyFromProductProductConversion + "0")) {
			writeTestResults("Verify user allow to add decimal qty for \"From product\"",
					"User should allow to add decimal qty for \"From product\"",
					"User allow to add decimal qty for \"From product\"", "pass");
		} else {
			writeTestResults("Verify user allow to add decimal qty for \"From product\"",
					"User should allow to add decimal qty for \"From product\"",
					"User not allow to add decimal qty for \"From product\"", "fail");
		}

		String toQuantityeOnGrid = getAttribute(txt_quantityToProductProductConversion, "value");

		if (toQuantityeOnGrid.equals(decimalQtyToProductProductConversion + "0")) {
			writeTestResults("Verify user allow to add decimal qty for \"To product\"",
					"User should allow to add decimal qty for \"To product\"",
					"User allow to add decimal qty for \"To product\"", "pass");
		} else {
			writeTestResults("Verify user allow to add decimal qty for \"To product\"",
					"User should allow to add decimal qty for \"To product\"",
					"User not allow to add decimal qty for \"To product\"", "fail");
		}

	}

	/* IN_PC_017_018 */
	public void createLotProduct_IN_PC_017_018() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 30);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 30);

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_product_info, 30);
		Thread.sleep(1000);
		click(btn_product_info);
		getInvObjReg().customizeLoadingDelay(btn_new_product, 30);
		Thread.sleep(1500);
		click(btn_new_product);

		// product code
		String product_code_lot = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "-LOT");
		sendKeys(txt_product_code, product_code_lot);

		// product description
		sendKeys(txt_product_desc, productDesription);

		// product group
		selectText(dropdown_product_group, productionGroupBatchProductCreation);

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		getInvObjReg().customizeLoadingDelay(txt_menufacturerDispatchOnlyBatchProduct, 30);
		Thread.sleep(3000);
		String menufacturur = invObj.readIWData(2);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturur);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", invObj.readIWData(2)));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);
		// product width
		getInvObjReg().customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		click(tab_detail);
		getInvObjReg().customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);

		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		click(checkbox_without_costing);

		click(chk_allocationBatchProduct);

		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		Thread.sleep(3000);
		if (!isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		sendKeys(txt_minPriceTextProductCreation, minPriceCoommnonCreateProduct);
		Thread.sleep(1000);
		sendKeys(txt_maxPriceTextProductCreation, maxPriceCoommnonCreateProduct);
		Thread.sleep(1000);

		click(btn_draft);
		invObj.customizeLoadingDelay(btn_releese2, 40);

		click(btn_releese2);
		invObj.customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 40);
		invObj.writeRegression01("Lot_IN_PC_017_018", product_code_lot, 167);
	}

	public void createBatchSpecificProduct_IN_PC_017_018() throws Exception {
		getInvObj().sleepCusomized(navigation_pane);
		getInvObj().waitImplicit(navigation_pane, 10);
		click(navigation_pane);
		getInvObj().sleepCusomized(inventory_module);
		click(inventory_module);
		getInvObj().customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);
		getInvObj().customizeLoadingDelay(btn_new_product, 8);

		Thread.sleep(1500);
		click(btn_new_product);

		// product code
		String product_code = (getInvObj().currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")
				+ "-BATCH-SPECIFIC");
		sendKeys(txt_product_code, product_code);

		// product description
		sendKeys(txt_product_desc, "DESC" + product_code);

		// product group
		selectText(dropdown_product_group, productionGroupBatchProductCreation);

		// manufacturer
		click(btn_menufactururSearchBatchProduct);
		getInvObj().sleepCusomized(txt_menufacturerDispatchOnlyBatchProduct);
		String menufacturur = getInvObj().readIWData(2);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, menufacturur);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacBatchProduct.replace("menufac_code", getInvObj().readIWData(2)));

		// default uom group
		selectIndex(dropdown_defoult_uom_group, 1);

		// uom group
		selectIndex(dropdown_defoult_uom, 1);
		Thread.sleep(2000);

		// product width
		getInvObj().customizeLoadingDelayAndContinue(txt_width, 10);
		sendKeys(txt_width, productWidth);
		selectIndex(drop_widthUnitBtachProduct, 2);
		// product height
		sendKeys(txt_height, productHeight);
		selectIndex(drop_heightUnitBtachProduct, 2);
		// product length
		sendKeys(txt_length, productLength);
		selectIndex(drop_lengthUnitBtachProduct, 2);

		click(tab_detail);
		Thread.sleep(3000);
		getInvObj().customizeLoadingDelayAndContinue(header_warehouseInforProductGroupConfig, 10);
		if (!isDisplayed(header_warehouseInforProductGroupConfig)) {
			click(btn_inventoryActiveDispatchOnlyBatchProduct);
			click(btn_product_function_confirmation);
		}

		Thread.sleep(3000);
		click(chk_batchProductDispatchOnlyBatchProduct);
		Thread.sleep(3000);
		pageScrollDown();
		pageScrollUpToTop();
		selectText(dropdown_outbound_costing_method, outboundCostingMethod);
		Select comboBox = new Select(driver.findElement(By.xpath(dropdown_outbound_costing_method)));
		Thread.sleep(3000);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (!selectedComboValue.equals(outboundCostingMethod)) {
			selectText(dropdown_outbound_costing_method, outboundCostingMethod);
			Thread.sleep(2000);
		}

		click(checkbox_without_costing);
		click(chk_allocationBatchProduct);

		click(checkbox_validate_expiery);
		sendKeys(txt_tollerence_date, tollerenceDate);
		click(btn_roqBatchProduct);

		if (!isDisplayed(header_producInfoProductGroupConfig)) {
			click(btn_purchase_active);
			click(btn_purchaseActiveConfirmation);
		}

		if (!isSelected(checkbox_qc_acceptance)) {
			click(checkbox_qc_acceptance);
		}

		if (!isDisplayed(header_sellingDeatailsProductGroupConfig)) {
			click(btn_sales_active);
			click(btn_salesActiveConfirmation);
		}

		Thread.sleep(1000);
		if (!isSelected(checkbox_validate_expiery_date)) {
			click(checkbox_validate_expiery_date);
		}

		sendKeys(txt_minPriceTextProductCreation, minPriceCoommnonCreateProduct);
		Thread.sleep(1000);
		sendKeys(txt_maxPriceTextProductCreation, maxPriceCoommnonCreateProduct);
		Thread.sleep(1000);

		Thread.sleep(1000);

		click(btn_draft);

		getInvObj().customizeLoadingDelay(btn_releese2, 30);
		click(btn_releese2);
		getInvObj().customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 40);
		getInvObj().writeRegression01("BatchSpecific_IN_PC_017_018", product_code, 168);

	}

	public void checkDecimalQuantityWithDecimalAllowDisabledProduct_IN_PC_017_018() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, productConversionWarehouseRegression);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User allow to fill summary details", "pass");
		} else {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User not allow to fill summary details", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readIWRegression01("Lot_IN_PC_017_018"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readIWRegression01("Lot_IN_PC_017_018")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readIWRegression01("Lot_IN_PC_017_018")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readIWRegression01("Lot_IN_PC_017_018")),
				10);

		if (isDisplayed(div_productOnGridProductConversation.replace("product",
				invObj.readIWRegression01("Lot_IN_PC_017_018")))) {
			writeTestResults("Verify user allow to select allow decimal disabled product as \"From Product\"",
					"User should allow to select allow decimal disabled product as \"From Product\"",
					"User allow to select allow decimal disabled product as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select allow decimal disabled product as \"From Product\"",
					"User should allow to select allow decimal disabled product as \"From Product\"",
					"User not allow to select allow decimal disabled product as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"),
				decimalQtyFromProductProductConversion);

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readIWRegression01("BatchSpecific_IN_PC_017_018"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readIWRegression01("BatchSpecific_IN_PC_017_018")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readIWRegression01("BatchSpecific_IN_PC_017_018")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(div_productOnGridProductConversation.replace("product",
				invObj.readIWRegression01("BatchSpecific_IN_PC_017_018")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readIWRegression01("BatchSpecific_IN_PC_017_018")))) {
			writeTestResults("Verify user allow to select allow decimal disabled product as \"To Product\"",
					"User should allow to select allow decimal disabled product as \"To Product\"",
					"User allow to select allow decimal disabled product as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to allow decimal disabled product as select \"To Product\"",
					"User should allow to select allow decimal disabled product as \"To Product\"",
					"User not allow to select allow decimal disabled product as \"To Product\"", "fail");
		}

		sendKeys(txt_quantityToProductProductConversion, decimalQtyToProductProductConversion);
		click(tab_summaryCommon);

		String gridQuantityFromProduct = getAttribute(
				txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), "value");

		if (gridQuantityFromProduct.equals(decimalRemovedQuantityFromProductProductConversion)) {
			writeTestResults("Verify user allow to add decimal qty for \"From product\"",
					"User should allow to add decimal qty for \"From product\"",
					"User allow to add decimal qty for \"From product\"", "pass");
		} else {
			writeTestResults("Verify user allow to add decimal qty for \"From product\"",
					"User should allow to add decimal qty for \"From product\"",
					"User not allow to add decimal qty for \"From product\"", "fail");
		}

		String toQuantityeOnGrid = getAttribute(txt_quantityToProductProductConversion, "value");

		if (toQuantityeOnGrid.equals(decimalRemovedQuantityToProductProductConversion)) {
			writeTestResults("Verify user not allow to add decimal qty for \"To product\"",
					"User should not allow to add decimal qty for \"To product\"",
					"User not allow to add decimal qty for \"To product\"", "pass");
		} else {
			writeTestResults("Verify user not allow to add decimal qty for \"To product\"",
					"User should not allow to add decimal qty for \"To product\"",
					"User allow to add decimal qty for \"To product\"", "fail");
		}

	}

	public void changeReleventProductToAllowDecimal_IN_PC_017_018() throws Exception {
		click(navigation_pane);
		click(inventory_module);
		Thread.sleep(1500);
		click(btn_productInformation);
		getInvObjReg().customizeLoadingDelay(txt_productSearch, 8);
		sendKeys(txt_productSearch, invObj.readIWRegression01("Lot_IN_PC_017_018"));
		pressEnter(txt_productSearch);
		String prodct = btn_resultProductInSecrhByPage.replace("product",
				invObj.readIWRegression01("Lot_IN_PC_017_018"));
		getInvObjReg().customizeLoadingDelay(prodct, 20);
		click(prodct);
		getInvObjReg().customizeLoadingDelay(btn_editActionVerification, 15);
		click(btn_editActionVerification);
		getInvObjReg().customizeLoadingDelay(chk_allowDecimal, 15);
		click(chk_allowDecimal);
		if (isSelected(chk_allowDecimal)) {
			writeTestResults("Verify user can be able to change the from product as allow decimal enable product",
					"User should be able to change the from product as allow decimal enable product",
					"User successfully change the from product as allow decimal enable product", "pass");
		} else {
			writeTestResults("Verify user can be able to change the from product as allow decimal enable product",
					"User should be able to change the from product as allow decimal enable product",
					"User doesn't change the from product as allow decimal enable product", "fail");
		}
		click(btn_updateActionVerification);
		getInvObjReg().customizeLoadingDelay(btn_editActionVerification, 20);

		click(navigation_pane);
		click(inventory_module);
		Thread.sleep(1500);
		click(btn_productInformation);
		getInvObjReg().customizeLoadingDelay(txt_productSearch, 8);
		sendKeys(txt_productSearch, invObj.readIWRegression01("BatchSpecific_IN_PC_017_018"));
		pressEnter(txt_productSearch);
		String prodct2 = btn_resultProductInSecrhByPage.replace("product",
				invObj.readIWRegression01("BatchSpecific_IN_PC_017_018"));
		getInvObjReg().customizeLoadingDelay(prodct2, 20);
		click(prodct2);
		getInvObjReg().customizeLoadingDelay(btn_editActionVerification, 15);
		click(btn_editActionVerification);
		getInvObjReg().customizeLoadingDelay(chk_allowDecimal, 15);
		click(chk_allowDecimal);
		if (isSelected(chk_allowDecimal)) {
			writeTestResults("Verify user can be able to change the to product as allow decimal enable product",
					"User should be able to change the to product as allow decimal enable product",
					"User successfully change the to product as allow decimal enable product", "pass");
		} else {
			writeTestResults("Verify user can be able to change the to product as allow decimal enable product",
					"User should be able to change the to product as allow decimal enable product",
					"User doesn't change the to product as allow decimal enable product", "fail");
		}
		click(btn_updateActionVerification);
		getInvObjReg().customizeLoadingDelay(btn_editActionVerification, 20);
	}

	public void checkDecimalQuantityWithWhichEditAsDecimalAllowProduct_IN_PC_017_018() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, productConversionWarehouseRegression);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User allow to fill summary details", "pass");
		} else {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User not allow to fill summary details", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readIWRegression01("Lot_IN_PC_017_018"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readIWRegression01("Lot_IN_PC_017_018")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readIWRegression01("Lot_IN_PC_017_018")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readIWRegression01("Lot_IN_PC_017_018")),
				10);

		if (isDisplayed(div_productOnGridProductConversation.replace("product",
				invObj.readIWRegression01("Lot_IN_PC_017_018")))) {
			writeTestResults("Verify user allow to select allow decimal enabled product as \"From Product\"",
					"User should allow to select allow decimal enabled product as \"From Product\"",
					"User allow to select allow decimal enabled product as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select allow decimal enabled product as \"From Product\"",
					"User should allow to select allow decimal enabled product as \"From Product\"",
					"User not allow to select allow decimal enabled product as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"),
				decimalQtyFromProductProductConversion);

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readIWRegression01("BatchSpecific_IN_PC_017_018"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readIWRegression01("BatchSpecific_IN_PC_017_018")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readIWRegression01("BatchSpecific_IN_PC_017_018")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(div_productOnGridProductConversation.replace("product",
				invObj.readIWRegression01("BatchSpecific_IN_PC_017_018")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readIWRegression01("BatchSpecific_IN_PC_017_018")))) {
			writeTestResults("Verify user allow to select allow decimal enabled product as \"To Product\"",
					"User should allow to select allow decimal enabled product as \"To Product\"",
					"User allow to select allow decimal enabled product as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to allow decimal enabled product as select \"To Product\"",
					"User should allow to select allow decimal enabled product as \"To Product\"",
					"User not allow to select allow decimal enabled product as \"To Product\"", "fail");
		}

		sendKeys(txt_quantityToProductProductConversion, decimalQtyToProductProductConversion);
		click(tab_summaryCommon);

		String gridQuantityFromProduct = getAttribute(
				txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), "value");

		if (gridQuantityFromProduct.equals(decimalQtyFromProductProductConversion + "0")) {
			writeTestResults("Verify user allow to add decimal qty for \"From product\" which enable allow decimal",
					"User should allow to add decimal qty for \"From product\" which enable allow decimal",
					"User allow to add decimal qty for \"From product\" which enable allow decimal", "pass");
		} else {
			writeTestResults("Verify user allow to add decimal qty for \"From product\" which enable allow decimal",
					"User should allow to add decimal qty for \"From product\" which enable allow decimal",
					"User not allow to add decimal qty for \"From product\" which enable allow decimal", "fail");
		}

		String toQuantityeOnGrid = getAttribute(txt_quantityToProductProductConversion, "value");

		if (toQuantityeOnGrid.equals(decimalQtyToProductProductConversion + "0")) {
			writeTestResults("Verify user allow to add decimal qty for \"To product\" which enable allow decimal",
					"User should allow to add decimal qty for \"To product\" which enable allow decimal",
					"User allow to add decimal qty for \"To product\" which enable allow decimal", "pass");
		} else {
			writeTestResults("Verify user allow to add decimal qty for \"To product\" which enable allow decimal",
					"User should allow to add decimal qty for \"To product\" which enable allow decimal",
					"User not allow to add decimal qty for \"To product\" which enable allow decimal", "fail");
		}

	}

	/* IN_PC_019 */
	public void selectReleventProductsForProductConversion_IN_PC_019() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, productConversionWarehouseRegression);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User allow to fill summary details", "pass");
		} else {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User not allow to fill summary details", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User allow to select \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User not allow to select \"From Product\"",
					"fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), quantityFiveProductConversion);

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User allow to select \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User not allow to select \"To Product\"", "fail");
		}

		sendKeys(txt_quantityToProductProductConversion, quantityToProductProductConversion);
		click(tab_summaryCommon);

	}

	public void captureSerielsForToProduct_IN_PC_019() throws Exception {
		click(btn_serialNosFromProductConversionToProduc);
		getInvObjReg().customizeLoadingDelayAndContinue(header_serileCaptureWindowProductConversion, 20);

		if (isDisplayed(header_serileCaptureWindowProductConversion)) {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window successfully pop-up",
					"pass");
		} else {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window doesn't pop-up",
					"fail");
		}

		getInvObjReg().customizeLoadingDelay(chk_serielRangeProductConversion, 20);
		Thread.sleep(1500);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, quantityToProductProductConversion);

		String sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click(btn_date31ProductConversionToProduct);
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click(btn_date31ProductConversionToProduct);
		pressEnter(txt_lotNo);
		Thread.sleep(2000);

		String capturedQuantitySerielSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantitySerielSpecific.equals(quantityToProductProductConversion)) {
			writeTestResults("Verify user allwo to capture serails as new serails",
					"User should allwo to capture serails as new serails",
					"User allwo to capture serails as new serails", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serails as new serails",
					"User should allwo to capture serails as new serails",
					"User not allwo to capture serails as new serails", "fail");

		}

		click(btn_appalySerielBatchesFromProductProductConversion);

		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 30);
		if (isDisplayed(header_newProductConversion)) {
			writeTestResults("Verify user can navigate to Product Conversion form",
					"Verify user navigate to Product Conversion form",
					"User successfully navigate to Product Conversion form", "pass");
		} else {
			writeTestResults("Verify user can navigate to Product Conversion form",
					"Verify user navigate to Product Conversion form",
					"User doesn't navigate to Product Conversion form", "fail");

		}
	}

	/* IN_PC_020 */
	public void selectReleventProductsForProductConversionFormAndTryToCaptureFromProductWithNewSeriels_IN_PC_020()
			throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, productConversionWarehouseRegression);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User allow to fill summary details", "pass");
		} else {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User not allow to fill summary details", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User allow to select \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User not allow to select \"From Product\"",
					"fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), quantityFiveProductConversion);

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User allow to select \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User not allow to select \"To Product\"", "fail");
		}

		sendKeys(txt_quantityToProductProductConversion, quantityToProductProductConversion);

		click(btn_serielBatchCaptureNosProductConversionRowReplace.replace("row", "1"));
		getInvObjReg().customizeLoadingDelayAndContinue(header_serileCaptureWindowProductConversion, 20);

		sendKeys(txt_serielNo, invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		pressEnter(txt_serielNo);
		Thread.sleep(2000);
		if (isDisplayed(div_transparent) && isDisplayed(error_invalidSerielNo)) {
			writeTestResults("Verify user allow to capture new serials/batches for \"From product\"",
					"User should not allow to capture new serials/batches for \"From product\"",
					"User not allow to capture new serials/batches for \"From product\"", "pass");
		} else {
			writeTestResults("Verify user allow to capture new serials/batches for \"From product\"",
					"User should not allow to capture new serials/batches for \"From product\"",
					"User allow to capture new serials/batches for \"From product\"", "fail");
		}

	}

	/* IN_PC_021 */
	public void selectReleventProductsForProductConversion_IN_PC_021() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, productConversionWarehouseRegression);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User allow to fill summary details", "pass");
		} else {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User not allow to fill summary details", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User allow to select \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User not allow to select \"From Product\"",
					"fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), quantityFiveProductConversion);

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User allow to select \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User not allow to select \"To Product\"", "fail");
		}

		sendKeys(txt_quantityToProductProductConversion, quantityToProductProductConversion);
		click(tab_summaryCommon);

	}

	public void captureExistingSerielsForToProduct_IN_PC_021() throws Exception {
		click(btn_serialNosFromProductConversionToProduc);
		getInvObjReg().customizeLoadingDelayAndContinue(header_serileCaptureWindowProductConversion, 20);

		if (isDisplayed(header_serileCaptureWindowProductConversion)) {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window successfully pop-up",
					"pass");
		} else {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window doesn't pop-up",
					"fail");
		}

		getInvObjReg().customizeLoadingDelay(txt_lotNo, 20);
		Thread.sleep(1500);
		String sn = getInvObj().getOngoinfSerirlSpecificSerielNumber(1);
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click(btn_date31ProductConversionToProduct);
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click(btn_date31ProductConversionToProduct);
		pressEnter(txt_lotNo);
		Thread.sleep(2000);
		click(btn_appalySerielBatchesFromProductProductConversion);

		boolean flagExistingSeielCapture = false;
		getInvObjReg().customizeLoadingDelay(error_commonSerielNos, 20);
		if (isDisplayed(div_transparent) && isDisplayed(error_commonSerielNos)) {
			flagExistingSeielCapture = true;
		}

		changeCSS(btn_dismissError, "z-index:-9999");

		click(icon_rowErrorsSerielCaptureWindow);
		getInvObjReg().customizeLoadingDelay(div_rowErrorBaloonSerielCaptureWindow, 20);
		if (flagExistingSeielCapture) {
			if (isDisplayed(div_rowErrorBaloonSerielCaptureWindow)) {
				flagExistingSeielCapture = true;
			} else {
				flagExistingSeielCapture = false;
			}
		}

		if (flagExistingSeielCapture) {
			writeTestResults("Verify user not allow to capture existing serials/batches for \"To product\"",
					"User should not allow to capture existing serials/batches for \"To product\"",
					"User not allow to capture existing serials/batches for \"To product\"", "pass");
		} else {
			writeTestResults("Verify user not allow to capture existing serials/batches for \"To product\"",
					"User should not allow to capture existing serials/batches for \"To product\"",
					"User allow to capture existing serials/batches for \"To product\"", "fail");
		}
	}

	/* IN_PC_022 */
	public void draftProductConversionWithoutMandatoryFiealds() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		click(btn_draftActionVerification);
		getInvObjReg().customizeLoadingDelay(para_validatorErrorMessage, 10);

		boolean flagMandatoryFieldValidation = false;
		if (isDisplayed(para_validatorErrorMessage) && isDisplayed(div_transparent)) {
			flagMandatoryFieldValidation = true;
		}

		changeCSS(div_transparent, "z-index:-9999");

		if (flagMandatoryFieldValidation) {
			if (isDisplayed(error_warehouseValidation)) {
				flagMandatoryFieldValidation = true;
			} else {
				flagMandatoryFieldValidation = false;
			}
		}

		click(btn_gridErrosFirstRow);

		if (flagMandatoryFieldValidation) {
			if (isDisplayed(error_fromProductRequired) && isDisplayed(error_toProductRequired)) {
				flagMandatoryFieldValidation = true;
			} else {
				flagMandatoryFieldValidation = false;
			}
		}

		if (flagMandatoryFieldValidation) {
			writeTestResults(
					"Verify validation is appear when draft the Product Conversion without having mandatory fields",
					"Validation should appear when draft the Product Conversion without having mandatory fields",
					"Validation successfully appear when draft the Product Conversion without having mandatory fields",
					"pass");
		} else {
			writeTestResults(
					"Verify validation is appear when draft the Product Conversion without having mandatory fields",
					"Validation should appear when draft the Product Conversion without having mandatory fields",
					"Validation doesn't appear when draft the Product Conversion without having mandatory fields",
					"fail");

		}
	}

	/* IN_PC_023_025_029_030 */
	public void drfatAndReleaseProductConversion_IN_PC_023_025_029_030() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, productConversionWarehouseRegression);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User allow to fill summary details", "pass");
		} else {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User not allow to fill summary details", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User allow to select \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User not allow to select \"From Product\"",
					"fail");
		}
		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), quantityFiveProductConversion);

		click(btn_serielBatchCaptureNosProductConversionRowReplace.replace("row", "1"));
		getInvObjReg().customizeLoadingDelayAndContinue(header_batchCaptureWindowProductConversion, 20);

		click(btn_seraialBatchCaptureDocIconProductConversion);

		getInvObjReg().customizeLoadingDelay(chk_selectBatchForBatchSpecificProduct, 20);
		click(chk_selectBatchForBatchSpecificProduct);

		Thread.sleep(2000);

		String capturedQuantityBatchSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantityBatchSpecific.equals(quantityFiveProductConversion)) {
			writeTestResults("Verify user allwo to capture batches for From Product",
					"User should allwo to capture batches for From Product",
					"User allwo to capture batches for From Product", "pass");
		} else {
			writeTestResults("Verify user allwo to capture batches for From Product",
					"User should allwo to capture batches for From Product",
					"User not allwo to capture batches for From Product", "fail");

		}

		click(btn_appalySerielBatchesFromProductProductConversion);

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User allow to select \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User not allow to select \"To Product\"", "fail");
		}

		sendKeys(txt_quantityToProductProductConversion, quantityToProductProductConversion);

		click(btn_serialNosFromProductConversionToProduc);
		getInvObjReg().customizeLoadingDelayAndContinue(header_serileCaptureWindowProductConversion, 20);

		if (isDisplayed(header_serileCaptureWindowProductConversion)) {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window successfully pop-up",
					"pass");
		} else {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window doesn't pop-up",
					"fail");
		}

		getInvObjReg().customizeLoadingDelay(chk_serielRangeProductConversion, 20);
		Thread.sleep(1500);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, quantityToProductProductConversion);

		String sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click(btn_date31ProductConversionToProduct);
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click(btn_date31ProductConversionToProduct);
		pressEnter(txt_lotNo);
		Thread.sleep(2000);

		String capturedQuantitySerielSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantitySerielSpecific.equals(quantityToProductProductConversion)) {
			writeTestResults("Verify user allwo to capture serials for To Product",
					"User should allwo to capture serials for To Product",
					"User allwo to capture serials for To Product", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serials for To Product",
					"User should allwo to capture serials for To Product",
					"User not allwo to capture serials for To Product", "fail");

		}

		click(btn_appalySerielBatchesFromProductProductConversion);

		click(btn_draftActionVerification);

		getInvObjReg().customizeLoadingDelay(header_drftedProductConversion, 40);
		if (isDisplayed(header_drftedProductConversion)) {
			writeTestResults("Verify user allow to draft the product conversion with mandatory fields",
					"User should allow to draft the product conversion with mandatory fieldst",
					"User allow to draft the product conversion with mandatory fields", "pass");
		} else {
			writeTestResults("Verify user allow to draft the product conversion with mandatory fields",
					"User should allow to draft the product conversion with mandatory fieldst",
					"User not allow to draft the product conversion with mandatory fields", "fail");

		}

		click(btn_releaseCommon);

		getInvObjReg().customizeLoadingDelay(header_releasedProductConversion, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedProductConversion)) {
			writeTestResults("Verify Product Conversion is released", "Product conversion should be released",
					"Product conversion successfully released", "pass");
		} else {
			writeTestResults("Verify Product Conversion is released", "Product conversion should be released",
					"Product conversion doesn't released", "fail");

		}

		click(btn_serialNosFromProductConversion);

		if (isDisplayed(headers_serialBatchNosUptoExpiryDateProductConversion)) {

		} else {
			writeTestResults("Headers properly available", "Verify headers properly available",
					"Headers properly not available", "fail");

		}

		if (isDisplayed(txt_disabledExpiryDateReleasedProductConversion)) {
			writeTestResults("Verify expiry date is not enabled for From Product in a released Product Conversion",
					"Expiry date should not be enabled From Product in a released Product Conversion",
					"Expiry date not enabled From Product in a released Product Conversion", "pass");
		} else {
			writeTestResults("Verify expiry date is not enabled From Product in a released Product Conversion",
					"Expiry date should not be enabled From Product in a released Product Conversion",
					"Expiry date enabled From Product in a released Product Conversion", "fail");

		}

		click(btn_closeWindowPopup);

		getInvObjReg().customizeLoadingDelay(btn_serialNosFromProductConversionToProduc, 40);
		click(btn_serialNosFromProductConversionToProduc);

		if (isDisplayed(headers_serialBatchNosUptoExpiryDateProductConversion)) {

		} else {
			writeTestResults("Headers properly available", "Verify headers properly available",
					"Headers properly not available", "fail");

		}

		if (isDisplayed(txt_disabledExpiryDateReleasedProductConversion)) {
			writeTestResults("Verify expiry date is not enabled for To Product in a released Product Conversion",
					"Expiry date should not be enabled To Product in a released Product Conversion",
					"Expiry date not enabled To Product in a released Product Conversion", "pass");
		} else {
			writeTestResults("Verify expiry date is not enabled To Product in a released Product Conversion",
					"Expiry date should not be enabled To Product in a released Product Conversion",
					"Expiry date enabled To Product in a released Product Conversion", "fail");

		}

	}

	/* IN_PC_024 */
	public void checkWithSameProductForFromAndToProducts_IN_PC_024()
			throws InvalidFormatException, IOException, Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, productConversionWarehouseRegression);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify user able to select a warehouse", "User should be able to select a warehouse",
					"User able to select a warehouse", "pass");
		} else {
			writeTestResults("Verify user able to select a warehouse", "User should be able to select a warehouse",
					"User doesn't able to select a warehouse", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);

		if (isDisplayed(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify products were loaded in product lookup",
					"Products should be loaded in product lookup", "Products were loaded in product lookup", "pass");
		} else {
			writeTestResults("Verify products were loaded in product lookup",
					"Products should be loaded in product lookup", "Products weren't loaded in product lookup", "fail");
		}

		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User allow to select \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User not allow to select \"From Product\"",
					"fail");
		}

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);

		if (isDisplayed(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify products were loaded in product lookup",
					"Products should be loaded in product lookup", "Products were loaded in product lookup", "pass");
		} else {
			writeTestResults("Verify products were loaded in product lookup",
					"Products should be loaded in product lookup", "Products weren't loaded in product lookup", "fail");
		}

		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select same product as \"To Product\"",
					"User should allow to select same product as \"To Product\"",
					"User allow to select same product as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select same product as \"To Product\"",
					"User should allow to select same product as \"To Product\"",
					"User not allow to select same product as \"To Product\"", "fail");
		}

		click(btn_draftActionVerification);
		getInvObjReg().customizeLoadingDelay(para_validatorErrorMessage, 20);
		if (isDisplayed(para_validatorErrorMessage)) {
			writeTestResults("Verify main validation is appear", "Main validation should appear",
					"Main validation successfully appear", "pass");
		} else {
			writeTestResults("Verify main validation is appear", "Main validation should appear",
					"Main validation doesn't appear", "fail");
		}

		changeCSS(btn_dismissError, "z-index:-9999");

		if (isDisplayed(div_toProductWithErrorBorderProductConversion)) {
			writeTestResults("Verify error border is visible for To Product",
					"Error border should be visible for To Product", "Error border successfully visible for To Product",
					"pass");
		} else {
			writeTestResults("Verify error border is visible for To Product",
					"Error border should be visible for To Product", "Error border doesn't visible for To Product",
					"fail");
		}

		click(btn_gridErrosFirstRow);

		if (isDisplayed(div_visibleGridBaloon) && isDisplayed(div_gridErrorMessageDuplicateProductProductConversion)) {
			writeTestResults("Verify error message is pop-up as \"Duplicate Product\"",
					"Error message should be pop-up as \"Duplicate Product\"",
					"Error message successfully pop-up as \"Duplicate Product\"", "pass");
		} else {
			writeTestResults("Verify error message is pop-up as \"Duplicate Product\"",
					"Error message should be pop-up as \"Duplicate Product\"",
					"Error message doesn't pop-up as \"Duplicate Product\"", "fail");
		}
	}

	/* IN_PC_026 */
	public void checkProductConversionWithQuantiytGreaterThanOHQuantity_IN_PC_026() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, productConversionWarehouseRegression);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify user allow to fill mandatory fields", "User should allow to fill mandatory fields",
					"User allow to fill mandatory fields", "pass");
		} else {
			writeTestResults("Verify user allow to fill mandatory fields", "User should allow to fill mandatory fields",
					"User not allow to fill mandatory fields", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User allow to select \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User not allow to select \"From Product\"",
					"fail");
		}
		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), qtyFiveHundred);

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User allow to select \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User not allow to select \"To Product\"", "fail");
		}

		if (isDisplayed(txt_fromQuantityWithErrorBorder.replace("row", "1"))) {
			writeTestResults("Verify error border is available for From Quantity",
					"Error border should be available for From Quantity",
					"Error border successfully available for From Quantity", "pass");
		} else {
			writeTestResults("Verify error border is available for From Quantity",
					"Error border should be available for From Quantity",
					"Error border doesn't available for From Quantity", "fail");
		}

		if (isDisplayed(icon_rowErrorsSerielCaptureWindow)) {
			writeTestResults("Verify error button is available on grid", "Error button should be available on grid",
					"Error button successfully available on grid", "pass");
		} else {
			writeTestResults("Verify error button is available on grid", "Error button should be available on grid",
					"Error button doesn't available on grid", "fail");
		}
		click(icon_rowErrorsSerielCaptureWindow);

		explicitWait(error_cantExceedOH, 20);
		if (isDisplayed(error_cantExceedOH)) {
			writeTestResults("Verify error message is pop-up as \"Can't Exceed OH Quantity\"",
					"Error message should be pop-up as \"Can't Exceed OH Quantity\"",
					"Error message successfully pop-up as \"Can't Exceed OH Quantity\"", "pass");
		} else {
			writeTestResults("Verify error message is pop-up as \"Can't Exceed OH Quantity\"",
					"Error message should be pop-up as \"Can't Exceed OH Quantity\"",
					"Error message doesn't pop-up as \"Can't Exceed OH Quantity\"", "fail");
		}

	}

	/* IN_PC_027 */
	public void deleteDraftedProductConversion_IN_PC_027() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, productConversionWarehouseRegression);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify user allow to fill mandatory fields", "User should allow to fill mandatory fields",
					"User allow to fill mandatory fields", "pass");
		} else {
			writeTestResults("Verify user allow to fill mandatory fields", "User should allow to fill mandatory fields",
					"User not allow to fill mandatory fields", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User allow to select \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User not allow to select \"From Product\"",
					"fail");
		}
		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), quantityFiveProductConversion);

		click(btn_serielBatchCaptureNosProductConversionRowReplace.replace("row", "1"));
		getInvObjReg().customizeLoadingDelayAndContinue(header_batchCaptureWindowProductConversion, 20);

		click(btn_seraialBatchCaptureDocIconProductConversion);

		getInvObjReg().customizeLoadingDelay(chk_selectBatchForBatchSpecificProduct, 20);
		click(chk_selectBatchForBatchSpecificProduct);

		Thread.sleep(3000);

		String capturedQuantityBatchSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantityBatchSpecific.equals(quantityFiveProductConversion)) {
			writeTestResults("Verify user allwo to capture batches for From Product",
					"User should allwo to capture batches for From Product",
					"User allwo to capture batches for From Product", "pass");
		} else {
			writeTestResults("Verify user allwo to capture batches for From Product",
					"User should allwo to capture batches for From Product",
					"User not allwo to capture batches for From Product", "fail");

		}

		click(btn_applyLast);

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User allow to select \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User not allow to select \"To Product\"", "fail");
		}

		sendKeys(txt_quantityToProductProductConversion, quantityToProductProductConversion);

		click(btn_serialNosFromProductConversionToProduc);
		getInvObjReg().customizeLoadingDelayAndContinue(header_serileCaptureWindowProductConversion, 20);

		if (isDisplayed(header_serileCaptureWindowProductConversion)) {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window successfully pop-up",
					"pass");
		} else {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window doesn't pop-up",
					"fail");
		}

		getInvObjReg().customizeLoadingDelay(chk_serielRangeProductConversion, 20);
		Thread.sleep(1500);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, quantityToProductProductConversion);

		String sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click(btn_date31ProductConversionToProduct);
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click(btn_date31ProductConversionToProduct);
		pressEnter(txt_lotNo);
		Thread.sleep(3000);

		String capturedQuantitySerielSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantitySerielSpecific.equals(quantityToProductProductConversion)) {
			writeTestResults("Verify user allwo to capture serials for To Product",
					"User should allwo to capture serials for To Product",
					"User allwo to capture serials for To Product", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serials for To Product",
					"User should allwo to capture serials for To Product",
					"User not allwo to capture serials for To Product", "fail");

		}

		click(btn_applyLast);

		click(btn_draftActionVerification);

		click(btn_deleteActionVrification);

		getInvObjReg().customizeLoadingDelay(btn_action, 50);
		click(btn_action);
		Thread.sleep(1000);
		if (isDisplayed(btn_deleteActionVrification)) {
			writeTestResults("Verify delete option is display under actions",
					"Delete option should be display unde actions", "Delete option successfully display under actions",
					"pass");
		} else {
			writeTestResults("Verify delete option is display under actions",
					"Delete option should be display unde actions", "Delete option doesn't display under actions",
					"fail");
		}

		click(btn_deleteActionVrification);
		Thread.sleep(2000);
		if (isDisplayed(btn_deleteConfirationActionVerification)) {
			writeTestResults("Verify confirmation message is pop-up", "Confirmation message should pop-up",
					"Confirmation message successfully pop-up", "pass");
		} else {
			writeTestResults("Verify confirmation message is pop-up", "Confirmation message should pop-up",
					"Confirmation message doesn't pop-up", "fail");
		}

		click(btn_deleteConfirationActionVerification);
		Thread.sleep(3000);

		getInvObjReg().customizeLoadingDelay(lbl_statusConfirmation.replace("(status)", "(Deleted)"), 50);
		if (isDisplayed(lbl_statusConfirmation.replace("(status)", "(Deleted)"))) {
			writeTestResults("Verify drafted Product Conversion was deleted",
					"Drafted Product Conversion should be deleted", "Drafted Product Conversion successfully deleted",
					"pass");
		} else {
			writeTestResults("Verify drafted Product Conversion was deleted",
					"Drafted Product Conversion should be deleted", "Drafted Product Conversion doesn't deleted",
					"fail");
		}
	}

	/* IN_PC_028 */
	public void stockAdjustmentForNewWarehouse_IN_PC_028() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		invObj.customizeLoadingDelay(navigation_pane, 50);
		Thread.sleep(3000);
		invObj.customizeLoadingDelay(navigation_pane, 20);
		click(navigation_pane);
		invObj.customizeLoadingDelay(inventory_module, 40);
		click(inventory_module);
		invObj.customizeLoadingDelay(btn_stockAdjustment, 40);
		click(btn_stockAdjustment);
		invObj.customizeLoadingDelay(btn_newStockAdjustment, 40);
		click(btn_newStockAdjustment);

		sendKeys(txt_descStockAdj, descStockAdj);

		selectText(dropdown_warehouseStockAdj, getInvObj().readIWRegression01("NewlyCreatedWarehouse") + " [Negombo]");

		/* Product add to the grid */
		for (int i = 1; i < 3; i++) {
			String str_i = String.valueOf(i);
			click(btn_productLookupStockAdjForLoop.replace("row", str_i));
			Thread.sleep(3000);
			invObj.customizeLoadingDelay(txt_productSearch, 15);

			String product = "";

			if (i == 1) {
				product = "Lot";
			} else if (i == 2) {
				product = "BatchFifo";
			} else {
				System.out.println("Product type cannt identified.........");
			}

			invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation(product));
			pressEnter(txt_productSearch);
			String product_path = result_cpmmonProduct.replace("product", invObj.readTestCreation(product));
			invObj.customizeLoadingDelay(product_path, 30);
			doubleClick(product_path);
			Thread.sleep(1500);
			String txt_qty = txt_qtyProductGridStockAdjustment.replace("row", str_i);
			String txt_cost = txt_costProductGridStockAdjustment.replace("row", str_i);

			sendKeys(txt_qty, productQuantity1);

			sendKeys(txt_cost, productCost1);
			invObj.sleepCusomized(btn_avilabilityCheckWidget);

			invObj.sleepCusomized(btn_addNewRow);
			click(btn_addNewRow);
		}

		click(btn_deleteLastRowOfTheGrid);

		click(btn_draftActionVerification);
		invObj.customizeLoadingDelay(brn_serielBatchCapture, 40);
		click(brn_serielBatchCapture);

		/* Product 02 */
		Thread.sleep(3000);
		String item_xpath = btn_itemProductCapturePage.replace("_product_number", "2");
		click(item_xpath);

		sendKeys(txt_batchNumberCaptureILOOutboundShipment, invObj.genBatchFifoSerielNumber(200));
		sendKeys(txt_lotNumberStockAdjustment,
				invObj.genProductId() + invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_expiryDateStockAdjustmentProductCapture);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_lotNumberStockAdjustment);
		Thread.sleep(1000);
		invObj.customizeLoadingDelay(btn_updateCapture, 50);
		click(btn_updateCapture);
		invObj.customizeLoadingDelay((item_xpath + span_afterCaptureSerialGreen), 30);
		click(btn_productCaptureBackButton);
		Thread.sleep(1000);
		click(btn_relese);
		invObj.customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 60);
	}

	public void drftProductConversionWithThreeLines_IN_PC_028() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, productConversionWarehouseRegression);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User allow to fill summary details", "pass");
		} else {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User not allow to fill summary details", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("Lot"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(result_cpmmonProduct.replace("product", invObj.readTestCreation("Lot")),
				20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("Lot")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("Lot")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("product", invObj.readTestCreation("Lot")))) {
			writeTestResults("Verify user allow to select Lot product as \"From Product\"",
					"User should allow to select Lot product as \"From Product\"",
					"User allow to select Lot product as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select Lot product as \"From Product\"",
					"User should allow to select Lot product as \"From Product\"",
					"User not allow to select Lot product as \"From Product\"", "fail");
		}

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select batch specific product as \"To Product\"",
					"User should allow to select batch specific product as \"To Product\"",
					"User allow to select batch specific product as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select batch specific product as \"To Product\"",
					"User should allow to select batch specific product as \"To Product\"",
					"User not allow to select batch specific product as \"To Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), quantityFiveProductConversion);

		sendKeys(txt_quantityToProductProductConversion, quantityTenProductConversion);

		click(btn_addNewRowProductConversion);

		click(btn_productLookupRowReplaceProductConversion.replace("row", "2"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchFifo")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchFifo")))) {
			writeTestResults("Verify user allow to select batch FIFO as \"From Product\"",
					"User should allow to select batch FIFO as \"From Product\"",
					"User allow to select batch FIFO as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select batch FIFO as \"From Product\"",
					"User should allow to select batch FIFO as \"From Product\"",
					"User not allow to select batch FIFO as \"From Product\"", "fail");
		}

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "2"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify user allow to select seriel specific as \"To Product\"",
					"User should allow to select seriel specific as \"To Product\"",
					"User allow to select seriel specific as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select seriel specific as \"To Product\"",
					"User should allow to select seriel specific as \"To Product\"",
					"User not allow to select seriel specific as \"To Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "2"), quantityFiveProductConversion);

		sendKeys(txt_qtyToProductProductConcersionRowReplace.replace("row", "2"), quantityTenProductConversion);

		click(btn_addNewRowProductConversion);

		click(btn_productLookupRowReplaceProductConversion.replace("row", "3"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielBatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielBatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielBatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielBatchSpecific")),
				10);

		if (isDisplayed(div_productOnGridProductConversation.replace("product",
				invObj.readTestCreation("SerielBatchSpecific")))) {
			writeTestResults("Verify user allow to select serial batch specific as \"From Product\"",
					"User should allow to select serial batch specific as \"From Product\"",
					"User allow to select serial batch specific as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select serial batch specific as \"From Product\"",
					"User should allow to select serial batch specific as \"From Product\"",
					"User not allow to select serial batch specific as \"From Product\"", "fail");
		}

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "3"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerirlFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")))) {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO as \"To Product\"",
					"User allow to select seriel FIFO as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO batch as \"To Product\"",
					"User not allow to select seriel FIFO as \"To Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "3"), quantityFiveProductConversion);

		sendKeys(txt_qtyToProductProductConcersionRowReplace.replace("row", "3"), quantityTenProductConversion);

		/* Batch capturing batch specific to product */
		explicitWait(btn_serleNosProductConversionToProduct.replace("row", "1"), 10);
		click(btn_serleNosProductConversionToProduct.replace("row", "1"));
		explicitWait(txt_batchNoToProductProductConversion, 20);
		sendKeys(txt_batchNoToProductProductConversion,
				invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_expiryDateToProductProductConversion);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		sendKeys(txt_lotNoToProductProductConversion,
				invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_menufacDateToProductProductConversion);
		Thread.sleep(200);
		doubleClick(btn_calenderBack);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_lotNoToProductProductConversion);
		Thread.sleep(2000);
		click(btn_commoApplyLast);

		/* Batch capturing batch FIFO from product */
		click(btn_serleNosProductConversionFromProduct.replace("row", "2"));
		explicitWait(btn_seraialBatchCaptureDocIconProductConversion, 10);
		click(btn_seraialBatchCaptureDocIconProductConversion);
		explicitWait(chk_batchSelectionProductConversion, 10);
		click(chk_batchSelectionProductConversion);
		click(btn_commoApplyLast);

		/* Serial capturing serial specific to product */
		click(btn_serleNosProductConversionToProduct.replace("row", "2"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, quantityTenProductConversion);
		String sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		/* Serial capturing serial batch specific from product */
		click(btn_serleNosProductConversionFromProduct.replace("row", "3"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		explicitWait(txt_serielCountProductConversion, 20);
		sendKeys(txt_serielCountProductConversion, quantityFiveProductConversion);
		sendKeys(txt_serielNo, getInvObj().getOngoingSerielBatchSpecificSerielNumber(5));
		pressEnter(txt_serielNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		/* Serial capturing serial FIFO to product */
		click(btn_serleNosProductConversionToProduct.replace("row", "3"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, quantityTenProductConversion);
		sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		explicitWait(btn_draftActionVerification, 30);
		click(btn_draftActionVerification);

		getInvObjReg().customizeLoadingDelay(header_drftedProductConversion, 40);
		if (isDisplayed(header_drftedProductConversion)) {
			writeTestResults("Verify Product Conversion is drafted", "Product conversion should be drafted",
					"Product conversion successfully drafted", "pass");
		} else {
			writeTestResults("Verify Product Conversion is drafted", "Product conversion should be drafted",
					"Product conversion doesn't drafted", "fail");

		}
	}

	public void editDraftedProductConversion_IN_PC_028() throws Exception {
		click(btn_editActionVerification);
		explicitWait(btn_updateActionVerification, 25);
		if (isDisplayed(btn_updateActionVerification)) {
			writeTestResults("Verify Product Conversion is editable", "Product conversion should be editable",
					"Product conversion successfully editable", "pass");
		} else {
			writeTestResults("Verify Product Conversion is editable", "Product conversion should be editable",
					"Product conversion doesn't editable", "fail");

		}

		click(btn_deleteRowCommonRowReplace.replace("row", "1"));
		Thread.sleep(2000);
		int rowCount = getCount(tr_countProductConversion);
		if (rowCount == 2) {
			writeTestResults("Verify user can delete a product line", "User should be able to delete a product line",
					"User successfully delete a product line", "pass");
		} else {
			writeTestResults("Verify user can delete a product line", "User should be able to delete a product line",
					"User doesn't delete a product line", "fail");
		}

		/* Change warehouse */
		selectText(drop_wareProductCon, getInvObj().readIWRegression01("NewlyCreatedWarehouse") + " [Negombo]");
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(getInvObj().readIWRegression01("NewlyCreatedWarehouse") + " [Negombo]")) {
			writeTestResults("Verify user can change the warehouse", "User should be able to change the warehouse",
					"User successfully change the warehouse", "pass");
		} else {
			writeTestResults("Verify user can change the warehouse", "User should be able to change the warehouse",
					"User doesn't change the warehouse", "fail");
		}

		click(btn_addNewRowProductConversion);

		/* Add new line */
		click(btn_productLookupRowReplaceProductConversion.replace("row", "3"));
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("Lot"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(result_cpmmonProduct.replace("product", invObj.readTestCreation("Lot")),
				20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("Lot")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(div_productAccordingToRowProductConversionFromProduct
				.replace("row", "3").replace("product", invObj.readTestCreation("Lot")), 10);

		if (isDisplayed(div_productAccordingToRowProductConversionFromProduct.replace("row", "3").replace("product",
				invObj.readTestCreation("Lot")))) {
			writeTestResults("Verify user can add product line (From Product)",
					"User should be able to add product line (From Product)",
					"User successfully add product line (From Product)", "pass");
		} else {
			writeTestResults("Verify user can add product line (From Product)",
					"User should be able to add product line (From Product)",
					"User doesn't add product line (From Product)", "fail");
		}

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "3"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(div_productAccordingToRowProductConversionToProduct
				.replace("row", "3").replace("product", invObj.readTestCreation("SerielSpecific")), 10);

		if (isDisplayed(div_productAccordingToRowProductConversionToProduct.replace("row", "3").replace("product",
				invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify user can add product line (To Product)",
					"User should be able to add product line (To Product)",
					"User successfully add product line (To Product)", "pass");
		} else {
			writeTestResults("Verify user can add product line (To Product)",
					"User should be able to add product line (To Product)",
					"User doesn't add product line (To Product)", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "3"), quantityFiveProductConversion);

		sendKeys(txt_qtyToProductProductConcersionRowReplace.replace("row", "3"), quantityTenProductConversion);

		String addedFromQuantity = getAttribute(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "3"),
				"value");

		if (addedFromQuantity.equals(quantityFiveProductConversion)) {
			writeTestResults("Verify user can add product line (From Quantity)",
					"User should be able to add product line (From Quantity)",
					"User successfully add product line (From Quantity)", "pass");
		} else {
			writeTestResults("Verify user can add product line (From Quantity)",
					"User should be able to add product line (From Quantity)",
					"User doesn't add product line (From Quantity)", "fail");
		}

		String addedToQuantity = getAttribute(txt_qtyToProductProductConcersionRowReplace.replace("row", "3"), "value");

		if (addedToQuantity.equals(quantityTenProductConversion)) {
			writeTestResults("Verify user can add product line (To Quantity)",
					"User should be able to add product line (To Quantity)",
					"User successfully add product line (To Quantity)", "pass");
		} else {
			writeTestResults("Verify user can add product line (To Quantity)",
					"User should be able to add product line (To Quantity)",
					"User doesn't add product line (To Quantity)", "fail");
		}

		/* Change product line */
		click(btn_productLookupRowReplaceProductConversion.replace("row", "2"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("Lot"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(result_cpmmonProduct.replace("product", invObj.readTestCreation("Lot")),
				20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("Lot")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(div_productAccordingToRowProductConversionFromProduct
				.replace("row", "2").replace("product", invObj.readTestCreation("Lot")), 10);

		if (isDisplayed(div_productAccordingToRowProductConversionFromProduct.replace("row", "2").replace("product",
				invObj.readTestCreation("Lot")))) {
			writeTestResults("Verify user can change the product on grid",
					"User should be able to change product on grid", "User successfully change the product on grid",
					"pass");
		} else {
			writeTestResults("Verify user can change the product on grid",
					"User should be able to change product on grid", "User doesn't change the product on grid", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "2"), qtyFifteen);

		String changedFromQuantity = getAttribute(
				txt_quantityRowReplaceProductConversionFromProduct.replace("row", "2"), "value");

		if (changedFromQuantity.equals(qtyFifteen)) {
			writeTestResults("Verify user can change the From qty on grid",
					"User should be able to change From qty on grid", "User successfully change the From qty on grid",
					"pass");
		} else {
			writeTestResults("Verify user can change the From qty on grid",
					"User should be able to change From qty on grid", "User doesn't change the From qty on grid",
					"fail");
		}

		sendKeys(txt_qtyToProductProductConcersionRowReplace.replace("row", "2"), qtyFifteen);

		String changedToQuantity = getAttribute(txt_qtyToProductProductConcersionRowReplace.replace("row", "2"),
				"value");

		if (changedToQuantity.equals(qtyFifteen)) {
			writeTestResults("Verify user can change the To qty on grid",
					"User should be able to change to To qty on grid", "User successfully change the to To qty on grid",
					"pass");
		} else {
			writeTestResults("Verify user can change the To qty on grid",
					"User should be able to change To qty on grid", "User doesn't change the To qty on grid", "fail");
		}

		sendKeys(txt_costPriceProductConversionRowReplace.replace("row", "2"), costNinetyFive);

		String changedCostPrice = getAttribute(txt_costPriceProductConversionRowReplace.replace("row", "2"), "value");

		if (changedCostPrice.equals(costNinetyFive)) {
			writeTestResults("Verify user can change the to Cost Price on grid",
					"User should be able to change to Cost Price on grid",
					"User successfully change the to Cost Price on grid", "pass");
		} else {
			writeTestResults("Verify user can change the to Cost Price on grid",
					"User should be able to change to Cost Price on grid",
					"User doesn't change the to Cost Price on grid", "fail");
		}

		click(btn_updateActionVerification);
		explicitWait(btn_editActionVerification, 40);
	}

	public void checkUpdatedInformations_IN_PC_028() throws Exception {
		String warehouse = getText(lbl_warehouseProductConversion);
		if (warehouse.equals(getInvObj().readIWRegression01("NewlyCreatedWarehouse") + " [Negombo]")) {
			writeTestResults("Verify warehouse is updated", "Warehouse should be updated",
					"Warehouse successfully updated", "pass");
		} else {
			writeTestResults("Verify warehouse is updated", "Warehouse should be updated", "Warehouse doesn't updated",
					"fail");
		}

		if (isDisplayed(div_productAccordingToRowProductConversionFromProduct.replace("row", "3").replace("product",
				invObj.readTestCreation("Lot")))) {
			writeTestResults("Verify added line is updated(From Product)", "Added line should be updated(From Product)",
					"Added line successfully updated(From Product)", "pass");
		} else {
			writeTestResults("Verify added line is updated(From Product)", "Added line should be updated(From Product)",
					"Added line doesn't updated(From Product)", "fail");
		}

		String updatedFromQuantity = getAttribute(
				txt_quantityRowReplaceProductConversionFromProduct.replace("row", "3"), "value");

		if (updatedFromQuantity.equals(quantityFiveProductConversion)) {
			writeTestResults("Verify user can update product line (From Quantity)",
					"User should be able to update product line (From Quantity)",
					"User successfully update product line (From Quantity)", "pass");
		} else {
			writeTestResults("Verify user can update product line (From Quantity)",
					"User should be able to update product line (From Quantity)",
					"User doesn't update product line (From Quantity)", "fail");
		}

		String updatedToQuantity = getAttribute(txt_qtyToProductProductConcersionRowReplace.replace("row", "3"),
				"value");

		if (updatedToQuantity.equals(quantityTenProductConversion)) {
			writeTestResults("Verify user can update product line (To Quantity)",
					"User should be able to update product line (To Quantity)",
					"User successfully update product line (To Quantity)", "pass");
		} else {
			writeTestResults("Verify user can update product line (To Quantity)",
					"User should be able to update product line (To Quantity)",
					"User doesn't update product line (To Quantity)", "fail");
		}

		if (isDisplayed(div_productAccordingToRowProductConversionFromProduct.replace("row", "2").replace("product",
				invObj.readTestCreation("Lot")))) {
			writeTestResults("Verify changed product is updated on grid", "Changed product should be updated on grid",
					"Changed product successfully updated on grid", "pass");
		} else {
			writeTestResults("Verify changed product is updated on grid", "Changed product should be updated on grid",
					"Changed product doesn't updated on grid", "fail");
		}

		String changedFromQuantity = getAttribute(
				txt_quantityRowReplaceProductConversionFromProduct.replace("row", "2"), "value");

		if (changedFromQuantity.equals(qtyFifteen)) {
			writeTestResults("Verify changed From Quantity is updated on grid",
					"Changed From Quantity should be updated on grid",
					"Changed From Quantity successfully updated on grid", "pass");
		} else {
			writeTestResults("Verify changed From Quantity is updated on grid",
					"Changed From Quantity should be updated on grid", "Changed From Quantity doesn't updated on grid",
					"fail");
		}

		String changedToQuantity = getAttribute(txt_qtyToProductProductConcersionRowReplace.replace("row", "2"),
				"value");

		if (changedToQuantity.equals(qtyFifteen)) {
			writeTestResults("Verify changed To Quantity is updated on grid",
					"Changed To Quantity should be updated on grid", "Changed To Quantity successfully updated on grid",
					"pass");
		} else {
			writeTestResults("Verify changed To Quantity is updated on grid",
					"Changed To Quantity should be updated on grid", "Changed To Quantity doesn't updated on grid",
					"fail");
		}

		String changedCostPrice = getAttribute(txt_costPriceProductConversionRowReplace.replace("row", "2"), "value");

		if (changedCostPrice.equals(costNinetyFive)) {
			writeTestResults("Verify changed Cost Price is updated on grid",
					"Changed Cost Price should be updated on grid", "Changed Cost Price successfully updated on grid",
					"pass");
		} else {
			writeTestResults("Verify changed Cost Price is updated on grid",
					"Changed Cost Price should be updated on grid", "Changed Cost Price doesn't updated on grid",
					"fail");
		}

	}

	/* IN_PC_033 */
	public void pruductConversionWithSameSerielsForFromAndToProducts_IN_PC_033() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, productConversionWarehouseRegression);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify user allow to select warehouse", "User should allow to select warehouse",
					"User allow to select warehouse", "pass");
		} else {
			writeTestResults("Verify user allow to select warehouse", "User should allow to select warehouse",
					"User not allow to select warehouse", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify user allow to select Serial Specific product as \"From Product\"",
					"User should allow to select Serial Specific product as \"From Product\"",
					"User allow to select Serial Specific product as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select Serial Specific product as \"From Product\"",
					"User should allow to select Serial Specific product as \"From Product\"",
					"User not allow to select Serial Specific product as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerirlFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerirlFifo")))) {
			writeTestResults("Verify user allow to select serial specific product as \"To Product\"",
					"User should allow to select serial specific product as \"To Product\"",
					"User allow to select serial specific product as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select serial specific product as \"To Product\"",
					"User should allow to select serial specific product as \"To Product\"",
					"User not allow to select serial specific product as \"To Product\"", "fail");
		}

		sendKeys(txt_quantityToProductProductConversion, "5");

		click(btn_addNewRowProductConversion);

		click(btn_productLookupRowReplaceProductConversion.replace("row", "2"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielBatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielBatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielBatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielBatchSpecific")),
				10);

		if (isDisplayed(div_productOnGridProductConversation.replace("product",
				invObj.readTestCreation("SerielBatchSpecific")))) {
			writeTestResults("Verify user allow to select Serial Batch Specific as \"From Product\"",
					"User should allow to select Serial Batch Specific as \"From Product\"",
					"User allow to select Serial Batch Specific as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select Serial Batch Specific as \"From Product\"",
					"User should allow to select Serial Batch Specific as \"From Product\"",
					"User not allow to select Serial Batch Specific as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "2"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "2"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerirlFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerirlFifo")))) {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO as \"To Product\"",
					"User allow to select seriel FIFO as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO as \"To Product\"",
					"User not allow to select seriel FIFO as \"To Product\"", "fail");
		}

		sendKeys(txt_qtyToProductProductConcersionRowReplace.replace("row", "2"), "5");

		click(btn_addNewRowProductConversion);

		click(btn_productLookupRowReplaceProductConversion.replace("row", "3"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielBatchFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielBatchFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielBatchFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielBatchFifo")),
				10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielBatchFifo")))) {
			writeTestResults("Verify user allow to select serial batch FIFO as \"From Product\"",
					"User should allow to select serial batch FIFO as \"From Product\"",
					"User allow to select serial batch FIFO as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select serial batch FIFO as \"From Product\"",
					"User should allow to select serial batch FIFO as \"From Product\"",
					"User not allow to select serial batch FIFO as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "3"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "3"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerirlFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")))) {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO as \"To Product\"",
					"User allow to select seriel FIFO as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO batch as \"To Product\"",
					"User not allow to select seriel FIFO as \"To Product\"", "fail");
		}

		sendKeys(txt_qtyToProductProductConcersionRowReplace.replace("row", "3"), "5");

		/* capture row 01 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "1"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		explicitWait(txt_serielCountProductConversion, 20);
		sendKeys(txt_serielCountProductConversion, "10");
		String seirialSpecific = getInvObj().getOngoinfSerirlSpecificSerielNumber(10);
		sendKeys(txt_serielNo, seirialSpecific);
		pressEnter(txt_serielNo);
		Thread.sleep(3000);

		String capturedQuantitySerielSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantitySerielSpecific.equals("10")) {
			writeTestResults("Verify user allwo to capture serials for From Product(Serial Specific)",
					"User should allwo to capture serials for From Product(Serial Specific)",
					"User allwo to capture serials for From Product(Serial Specific)", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serials for From Product(Serial Specific)",
					"User should allwo to capture serials for From Product(Serial Specific)",
					"User not allwo to capture serials for From Product(Serial Specific)", "fail");

		}

		click(btn_commoApplyLast);

		click(btn_serleNosProductConversionToProduct.replace("row", "1"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		String sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, seirialSpecific);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);

		String capturedQuantitySerielFifo01 = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantitySerielFifo01.equals("5")) {
			writeTestResults(
					"Verify user allwo to capture serials for To Product(Serial FIFO) with same serials captured in From Product",
					"User should allwo to capture serials for To Product(Serial FIFO) with same serials captured in From Product",
					"User successfully allwo to capture serials for To Product(Serial FIFO) with same serials captured in From Product",
					"pass");
		} else {
			writeTestResults(
					"Verify user allwo to capture serials for To Product(Serial FIFO) with same serials captured in From Product",
					"User should allwo to capture serials for To Product(Serial FIFO) with same serials captured in From Product",
					"User not allwo to capture serials for To Product(Serial FIFO) with same serials captured in From Product",
					"fail");

		}

		click(btn_commoApplyLast);

		/* capture row 02 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "2"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		explicitWait(txt_serielCountProductConversion, 20);
		sendKeys(txt_serielCountProductConversion, "10");
		String seirialBatchSpecific = getInvObj().getOngoingSerielBatchSpecificSerielNumber(10);
		sendKeys(txt_serielNo, seirialBatchSpecific);
		pressEnter(txt_serielNo);
		Thread.sleep(3000);

		String capturedQuantitySerielBatchSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantitySerielBatchSpecific.equals("10")) {
			writeTestResults("Verify user allwo to capture serials for From Product(Serial Batch Specific)",
					"User should allwo to capture serials for From Product(Serial Batch Specific)",
					"User allwo to capture serials for From Product(Serial Batch Specific)", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serials for From Product(Serial Batch Specific)",
					"User should allwo to capture serials for From Product(Serial Batch Specific)",
					"User not allwo to capture serials for From Product(Serial Batch Specific)", "fail");

		}

		click(btn_commoApplyLast);

		click(btn_serleNosProductConversionToProduct.replace("row", "2"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		sendKeys(txt_serielNo, seirialBatchSpecific);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);

		String capturedQuantitySerielFifo02 = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantitySerielFifo02.equals("5")) {
			writeTestResults(
					"Verify user allwo to capture serials for To Product(Serial FIFO) with same serials captured in From Product",
					"User should allwo to capture serials for To Product(Serial FIFO) with same serials captured in From Product",
					"User successfully allwo to capture serials for To Product(Serial FIFO) with same serials captured in From Product",
					"pass");
		} else {
			writeTestResults(
					"Verify user allwo to capture serials for To Product(Serial FIFO) with same serials captured in From Product",
					"User should allwo to capture serials for To Product(Serial FIFO) with same serials captured in From Product",
					"User not allwo to capture serials for To Product(Serial FIFO) with same serials captured in From Product",
					"fail");

		}

		click(btn_commoApplyLast);

		/* capture row 03 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "3"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		explicitWait(txt_serielCountProductConversion, 20);
		sendKeys(txt_serielCountProductConversion, "10");
		String seirialBatchFifo = getInvObj().getOngoingSerielBatchFifoSerielNumber(10);
		sendKeys(txt_serielNo, seirialBatchFifo);
		pressEnter(txt_serielNo);
		Thread.sleep(3000);

		String capturedQuantitySerielBatchFifo = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantitySerielBatchFifo.equals("10")) {
			writeTestResults("Verify user allwo to capture serials for From Product(Serial Batch FIFO)",
					"User should allwo to capture serials for From Product(Serial Batch FIFO)",
					"User allwo to capture serials for From Product(Serial Batch FIFO)", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serials for From Product(Serial Batch FIFO)",
					"User should allwo to capture serials for From Product(Serial Batch FIFO)",
					"User not allwo to capture serials for From Product(Serial Batch FIFO)", "fail");

		}

		click(btn_commoApplyLast);

		click(btn_serleNosProductConversionToProduct.replace("row", "3"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);

		String capturedQuantitySerielFifo03 = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantitySerielFifo03.equals("5")) {
			writeTestResults(
					"Verify user allwo to capture serials for To Product(Serial FIFO) with same serials captured in From Product",
					"User should allwo to capture serials for To Product(Serial FIFO) with same serials captured in From Product",
					"User successfully allwo to capture serials for To Product(Serial FIFO) with same serials captured in From Product",
					"pass");
		} else {
			writeTestResults(
					"Verify user allwo to capture serials for To Product(Serial FIFO) with same serials captured in From Product",
					"User should allwo to capture serials for To Product(Serial FIFO) with same serials captured in From Product",
					"User not allwo to capture serials for To Product(Serial FIFO) with same serials captured in From Product",
					"fail");

		}

		click(btn_commoApplyLast);

		explicitWait(btn_draftActionVerification, 30);
		click(btn_draftActionVerification);

		getInvObjReg().customizeLoadingDelay(header_drftedProductConversion, 40);

		click(btn_releaseCommon);
		explicitWait(header_releasedProductConversion, 30);
		if (isDisplayed(header_releasedProductConversion)) {
			writeTestResults("Verify user able to complete Product Conversion",
					"User should be able to complete Product Conversion\"",
					"User successfully complete Product Conversion", "pass");
		} else {
			writeTestResults("Verify user able to complete Product Conversion",
					"User should be able to complete Product Conversion", "User doesn't complete Product Conversion",
					"fail");
		}
	}

	/* IN_PC_034 */
	public void productConversion_IN_PC_034() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, productConversionWarehouseRegression);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User allow to fill summary details", "pass");
		} else {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User not allow to fill summary details", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("Lot"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(result_cpmmonProduct.replace("product", invObj.readTestCreation("Lot")),
				20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("Lot")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("Lot")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("product", invObj.readTestCreation("Lot")))) {
			writeTestResults("Verify user allow to select Lot product as \"From Product\"",
					"User should allow to select Lot product as \"From Product\"",
					"User allow to select Lot product as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select Lot product as \"From Product\"",
					"User should allow to select Lot product as \"From Product\"",
					"User not allow to select Lot product as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerirlFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerirlFifo")))) {
			writeTestResults("Verify user allow to select serial specific product as \"To Product\"",
					"User should allow to select serial specific product as \"To Product\"",
					"User allow to select serial specific product as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select serial specific product as \"To Product\"",
					"User should allow to select serial specific product as \"To Product\"",
					"User not allow to select serial specific product as \"To Product\"", "fail");
		}

		sendKeys(txt_quantityToProductProductConversion, "5");

		click(btn_addNewRowProductConversion);

		click(btn_productLookupRowReplaceProductConversion.replace("row", "2"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchFifo")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchFifo")))) {
			writeTestResults("Verify user allow to select batch FIFO as \"From Product\"",
					"User should allow to select batch FIFO as \"From Product\"",
					"User allow to select batch FIFO as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select batch FIFO as \"From Product\"",
					"User should allow to select batch FIFO as \"From Product\"",
					"User not allow to select batch FIFO as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "2"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "2"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerirlFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerirlFifo")))) {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO as \"To Product\"",
					"User allow to select seriel FIFO as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO as \"To Product\"",
					"User not allow to select seriel FIFO as \"To Product\"", "fail");
		}

		sendKeys(txt_qtyToProductProductConcersionRowReplace.replace("row", "2"), "5");

		click(btn_addNewRowProductConversion);

		click(btn_productLookupRowReplaceProductConversion.replace("row", "3"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select batch specific as \"From Product\"",
					"User should allow to select batch specific as \"From Product\"",
					"User allow to select batch specific as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select batch specific as \"From Product\"",
					"User should allow to select batch specific as \"From Product\"",
					"User not allow to select batch specific as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "3"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "3"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerirlFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")))) {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO as \"To Product\"",
					"User allow to select seriel FIFO as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO batch as \"To Product\"",
					"User not allow to select seriel FIFO as \"To Product\"", "fail");
		}

		sendKeys(txt_qtyToProductProductConcersionRowReplace.replace("row", "3"), "5");

		click(btn_addNewRowProductConversion);

		click(btn_productLookupRowReplaceProductConversion.replace("row", "4"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify user allow to select serial specific as \"From Product\"",
					"User should allow to select serial specific as \"From Product\"",
					"User allow to select serial specific as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select serial specific as \"From Product\"",
					"User should allow to select serial specific as \"From Product\"",
					"User not allow to select serial specific as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "4"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "4"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerirlFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")))) {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO as \"To Product\"",
					"User allow to select seriel FIFO as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO batch as \"To Product\"",
					"User not allow to select seriel FIFO as \"To Product\"", "fail");
		}

		sendKeys(txt_qtyToProductProductConcersionRowReplace.replace("row", "4"), "5");

		/* capture row 01 */
		click(btn_serleNosProductConversionToProduct.replace("row", "1"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		String sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);

		String capturedQuantityLineOneToProduct = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantityLineOneToProduct.equals("5")) {
			writeTestResults("Verify user allwo to capture serials for To Product(Line 01)",
					"User should allwo to capture serials for To Product(Line 01)",
					"User successfully allwo to capture serials for To Product(Line 01)", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serials for To Product(Line 01)",
					"User should allwo to capture serials for To Product(Line 01)",
					"User not allwo to capture serials for To Product(Line 01)", "fail");

		}

		click(btn_commoApplyLast);

		/* capture row 02 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "2"));
		explicitWait(btn_seraialBatchCaptureDocIconProductConversion, 10);
		click(btn_seraialBatchCaptureDocIconProductConversion);
		explicitWait(chk_batchSelectionProductConversion, 10);
		click(chk_batchSelectionProductConversion);
		String capturedQuantityLineTwoFromProduct = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantityLineTwoFromProduct.equals("10")) {
			writeTestResults("Verify user allwo to capture serials for From Product(Line 02)",
					"User should allwo to capture serials for From Product(Line 02)",
					"User successfully allwo to capture serials for From Product(Line 02)", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serials for From Product(Line 02)",
					"User should allwo to capture serials for From Product(Line 02)",
					"User not allwo to capture serials for From Product(Line 02)", "fail");

		}

		click(btn_commoApplyLast);

		click(btn_serleNosProductConversionToProduct.replace("row", "2"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);

		String capturedQuantityLineTowToProduct = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantityLineTowToProduct.equals("5")) {
			writeTestResults("Verify user allwo to capture serials for To Product(Line 02)",
					"User should allwo to capture serials for To Product(Line 02)",
					"User successfully allwo to capture serials for To Product(Line 02)", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serials for To Product(Line 02)",
					"User should allwo to capture serials for To Product(Line 02)",
					"User not allwo to capture serials for To Product(Line 02)", "fail");

		}

		click(btn_commoApplyLast);

		/* capture row 03 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "3"));
		explicitWait(btn_seraialBatchCaptureDocIconProductConversion, 10);
		click(btn_seraialBatchCaptureDocIconProductConversion);
		explicitWait(chk_batchSelectionProductConversion, 10);
		click(chk_batchSelectionProductConversion);
		String capturedQuantityLineThreeFromProduct = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantityLineThreeFromProduct.equals("10")) {
			writeTestResults("Verify user allwo to capture serials for From Product(Line 03)",
					"User should allwo to capture serials for From Product(Line 03)",
					"User successfully allwo to capture serials for From Product(Line 03)", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serials for From Product(Line 03)",
					"User should allwo to capture serials for From Product(Line 03)",
					"User not allwo to capture serials for From Product(Line 03)", "fail");

		}
		click(btn_commoApplyLast);

		click(btn_serleNosProductConversionToProduct.replace("row", "3"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);

		String capturedQuantityLineThreeToProduct = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantityLineThreeToProduct.equals("5")) {
			writeTestResults("Verify user allwo to capture serials for To Product(Line 03)",
					"User should allwo to capture serials for To Product(Line 03)",
					"User successfully allwo to capture serials for To Product(Line 03)", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serials for To Product(Line 03)",
					"User should allwo to capture serials for To Product(Line 03)",
					"User not allwo to capture serials for To Product(Line 03)", "fail");

		}
		click(btn_commoApplyLast);

		/* capture row 04 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "4"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		explicitWait(txt_serielCountProductConversion, 20);
		sendKeys(txt_serielCountProductConversion, "10");
		sendKeys(txt_serielNo, getInvObj().getOngoinfSerirlSpecificSerielNumber(10));
		pressEnter(txt_serielNo);
		Thread.sleep(3000);
		String capturedQuantityLineFourFromProduct = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantityLineFourFromProduct.equals("10")) {
			writeTestResults("Verify user allwo to capture serials for From Product(Line 04)",
					"User should allwo to capture serials for From Product(Line 04)",
					"User successfully allwo to capture serials for From Product(Line 04)", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serials for From Product(Line 04)",
					"User should allwo to capture serials for From Product(Line 04)",
					"User not allwo to capture serials for From Product(Line 04)", "fail");

		}

		click(btn_commoApplyLast);

		click(btn_serleNosProductConversionToProduct.replace("row", "4"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);

		String capturedQuantityLineFourToProduct = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantityLineFourToProduct.equals("5")) {
			writeTestResults("Verify user allwo to capture serials for To Product(Line 04)",
					"User should allwo to capture serials for To Product(Line 04)",
					"User successfully allwo to capture serials for To Product(Line 04)", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serials for To Product(Line 04)",
					"User should allwo to capture serials for To Product(Line 04)",
					"User not allwo to capture serials for To Product(Line 04)", "fail");

		}

		click(btn_commoApplyLast);

		click(btn_deleteRowCommonRowReplace.replace("row", "1"));

		int rowCount = getCount(count_rowProductConversionGrid);

		if (rowCount == 3) {
			writeTestResults("Verify user able to delete the first raw", "User should be able to delete the first raw",
					"User successfully delete the first raw", "pass");
		} else {
			writeTestResults("Verify user able to delete the first raw", "User should be able to delete the first raw",
					"User doesn't delete the raw", "pass");
		}

		explicitWait(btn_draftActionVerification, 30);
		click(btn_draftActionVerification);

		getInvObjReg().customizeLoadingDelay(header_drftedProductConversion, 40);

		if (isDisplayed(header_drftedProductConversion)) {
			writeTestResults("Verify user can drfat the Product Conversion",
					"User should be able to drfat the Product Conversion",
					"User successfully drfat the Product Conversion", "pass");
		} else {
			writeTestResults("Verify user can drfat the Product Conversion",
					"User should be able to drfat the Product Conversion", "User doesn't drfat the Product Conversion",
					"fail");
		}

		/* Check serials after draft */
		/* Row 01 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "1"));

		String capturedQuantityLineOneFromProductAfterDraft = getAttribute(
				txt_capturedQuantityAfterDraftProductConversion, "value");

		if (capturedQuantityLineOneFromProductAfterDraft.equals("10")) {
			writeTestResults("Verify captured serials are displayed in the window for From Product(Line 01)",
					"Captured serials should be displayed in the window for From Product(Line 01)",
					"Captured serials successfully displayed in the window for From Product(Line 01)", "pass");
		} else {
			writeTestResults("Verify captured serials are displayed in the window for From Product(Line 01)",
					"Captured serials should be displayed in the window for From Product(Line 01)",
					"Captured serials not displayed in the window for From Product(Line 01)", "fail");

		}
		click(btn_closeWindowPopup);

		click(btn_serleNosProductConversionToProduct.replace("row", "1"));

		String capturedQuantityLineOneToProductAfterDraft = getAttribute(
				txt_capturedQuantityAfterDraftProductConversion, "value");

		if (capturedQuantityLineOneToProductAfterDraft.equals("5")) {
			writeTestResults("Verify captured serials are displayed in the window for To Product(Line 01)",
					"Captured serials should be displayed in the window for To Product(Line 01)",
					"Captured serials successfully displayed in the window for To Product(Line 01)", "pass");
		} else {
			writeTestResults("Verify captured serials are displayed in the window for To Product(Line 01)",
					"Captured serials should be displayed in the window for To Product(Line 01)",
					"Captured serials not displayed in the window for To Product(Line 01)", "fail");

		}

		click(btn_closeWindowPopup);

		/* Row 02 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "2"));

		String capturedQuantityLineTwoFromProductAfterDraft = getAttribute(
				txt_capturedQuantityAfterDraftProductConversion, "value");

		if (capturedQuantityLineTwoFromProductAfterDraft.equals("10")) {
			writeTestResults("Verify captured serials are displayed in the window for From Product(Line 02)",
					"Captured serials should be displayed in the window for From Product(Line 02)",
					"Captured serials successfully displayed in the window for From Product(Line 02)", "pass");
		} else {
			writeTestResults("Verify captured serials are displayed in the window for From Product(Line 02)",
					"Captured serials should be displayed in the window for From Product(Line 02)",
					"Captured serials not displayed in the window for From Product(Line 02)", "fail");

		}
		click(btn_closeWindowPopup);

		click(btn_serleNosProductConversionToProduct.replace("row", "2"));

		String capturedQuantityLineTwoToProductAfterDraft = getAttribute(
				txt_capturedQuantityAfterDraftProductConversion, "value");

		if (capturedQuantityLineTwoToProductAfterDraft.equals("5")) {
			writeTestResults("Verify captured serials are displayed in the window for To Product(Line 02)",
					"Captured serials should be displayed in the window for To Product(Line 02)",
					"Captured serials successfully displayed in the window for To Product(Line 02)", "pass");
		} else {
			writeTestResults("Verify captured serials are displayed in the window for To Product(Line 02)",
					"Captured serials should be displayed in the window for To Product(Line 02)",
					"Captured serials not displayed in the window for To Product(Line 02)", "fail");

		}

		click(btn_closeWindowPopup);

		/* Row 03 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "3"));

		String capturedQuantityLineThreeFromProductAfterDraft = getAttribute(
				txt_capturedQuantityAfterDraftProductConversion, "value");

		if (capturedQuantityLineThreeFromProductAfterDraft.equals("10")) {
			writeTestResults("Verify captured serials are displayed in the window for From Product(Line 03)",
					"Captured serials should be displayed in the window for From Product(Line 03)",
					"Captured serials successfully displayed in the window for From Product(Line 03)", "pass");
		} else {
			writeTestResults("Verify captured serials are displayed in the window for From Product(Line 03)",
					"Captured serials should be displayed in the window for From Product(Line 03)",
					"Captured serials not displayed in the window for From Product(Line 03)", "fail");

		}
		click(btn_closeWindowPopup);

		click(btn_serleNosProductConversionToProduct.replace("row", "3"));

		String capturedQuantityLineThreeToProductAfterDraft = getAttribute(
				txt_capturedQuantityAfterDraftProductConversion, "value");

		if (capturedQuantityLineThreeToProductAfterDraft.equals("5")) {
			writeTestResults("Verify captured serials are displayed in the window for To Product(Line 03)",
					"Captured serials should be displayed in the window for To Product(Line 03)",
					"Captured serials successfully displayed in the window for To Product(Line 03)", "pass");
		} else {
			writeTestResults("Verify captured serials are displayed in the window for To Product(Line 03)",
					"Captured serials should be displayed in the window for To Product(Line 03)",
					"Captured serials not displayed in the window for To Product(Line 03)", "fail");

		}

		click(btn_closeWindowPopup);

	}

	/* IN_PC_035 */
	public void productConversion_IN_PC_035() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, productConversionWarehouseRegression);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User allow to fill summary details", "pass");
		} else {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User not allow to fill summary details", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("Lot"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(result_cpmmonProduct.replace("product", invObj.readTestCreation("Lot")),
				20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("Lot")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("Lot")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("product", invObj.readTestCreation("Lot")))) {
			writeTestResults("Verify user allow to select Lot product as \"From Product\"",
					"User should allow to select Lot product as \"From Product\"",
					"User allow to select Lot product as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select Lot product as \"From Product\"",
					"User should allow to select Lot product as \"From Product\"",
					"User not allow to select Lot product as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerirlFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerirlFifo")))) {
			writeTestResults("Verify user allow to select serial specific product as \"To Product\"",
					"User should allow to select serial specific product as \"To Product\"",
					"User allow to select serial specific product as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select serial specific product as \"To Product\"",
					"User should allow to select serial specific product as \"To Product\"",
					"User not allow to select serial specific product as \"To Product\"", "fail");
		}

		sendKeys(txt_quantityToProductProductConversion, "5");

		click(btn_addNewRowProductConversion);

		click(btn_productLookupRowReplaceProductConversion.replace("row", "2"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchFifo")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchFifo")))) {
			writeTestResults("Verify user allow to select batch FIFO as \"From Product\"",
					"User should allow to select batch FIFO as \"From Product\"",
					"User allow to select batch FIFO as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select batch FIFO as \"From Product\"",
					"User should allow to select batch FIFO as \"From Product\"",
					"User not allow to select batch FIFO as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "2"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "2"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerirlFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerirlFifo")))) {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO as \"To Product\"",
					"User allow to select seriel FIFO as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO as \"To Product\"",
					"User not allow to select seriel FIFO as \"To Product\"", "fail");
		}

		sendKeys(txt_qtyToProductProductConcersionRowReplace.replace("row", "2"), "5");

		click(btn_addNewRowProductConversion);

		click(btn_productLookupRowReplaceProductConversion.replace("row", "3"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select batch specific as \"From Product\"",
					"User should allow to select batch specific as \"From Product\"",
					"User allow to select batch specific as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select batch specific as \"From Product\"",
					"User should allow to select batch specific as \"From Product\"",
					"User not allow to select batch specific as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "3"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "3"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerirlFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")))) {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO as \"To Product\"",
					"User allow to select seriel FIFO as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO batch as \"To Product\"",
					"User not allow to select seriel FIFO as \"To Product\"", "fail");
		}

		sendKeys(txt_qtyToProductProductConcersionRowReplace.replace("row", "3"), "5");

		click(btn_addNewRowProductConversion);

		click(btn_productLookupRowReplaceProductConversion.replace("row", "4"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify user allow to select serial specific as \"From Product\"",
					"User should allow to select serial specific as \"From Product\"",
					"User allow to select serial specific as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select serial specific as \"From Product\"",
					"User should allow to select serial specific as \"From Product\"",
					"User not allow to select serial specific as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "4"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "4"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerirlFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")))) {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO as \"To Product\"",
					"User allow to select seriel FIFO as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO batch as \"To Product\"",
					"User not allow to select seriel FIFO as \"To Product\"", "fail");
		}

		sendKeys(txt_qtyToProductProductConcersionRowReplace.replace("row", "4"), "5");

		/* capture row 01 */
		click(btn_serleNosProductConversionToProduct.replace("row", "1"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		String sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);

		String capturedQuantityLineOneToProduct = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantityLineOneToProduct.equals("5")) {
			writeTestResults("Verify user allwo to capture serials for To Product(Line 01)",
					"User should allwo to capture serials for To Product(Line 01)",
					"User successfully allwo to capture serials for To Product(Line 01)", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serials for To Product(Line 01)",
					"User should allwo to capture serials for To Product(Line 01)",
					"User not allwo to capture serials for To Product(Line 01)", "fail");

		}

		click(btn_commoApplyLast);

		/* capture row 02 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "2"));
		explicitWait(btn_seraialBatchCaptureDocIconProductConversion, 10);
		click(btn_seraialBatchCaptureDocIconProductConversion);
		explicitWait(chk_batchSelectionProductConversion, 10);
		click(chk_batchSelectionProductConversion);
		String capturedQuantityLineTwoFromProduct = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantityLineTwoFromProduct.equals("10")) {
			writeTestResults("Verify user allwo to capture serials for From Product(Line 02)",
					"User should allwo to capture serials for From Product(Line 02)",
					"User successfully allwo to capture serials for From Product(Line 02)", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serials for From Product(Line 02)",
					"User should allwo to capture serials for From Product(Line 02)",
					"User not allwo to capture serials for From Product(Line 02)", "fail");

		}

		click(btn_commoApplyLast);

		click(btn_serleNosProductConversionToProduct.replace("row", "2"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);

		String capturedQuantityLineTowToProduct = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantityLineTowToProduct.equals("5")) {
			writeTestResults("Verify user allwo to capture serials for To Product(Line 02)",
					"User should allwo to capture serials for To Product(Line 02)",
					"User successfully allwo to capture serials for To Product(Line 02)", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serials for To Product(Line 02)",
					"User should allwo to capture serials for To Product(Line 02)",
					"User not allwo to capture serials for To Product(Line 02)", "fail");

		}

		click(btn_commoApplyLast);

		/* capture row 03 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "3"));
		explicitWait(btn_seraialBatchCaptureDocIconProductConversion, 10);
		click(btn_seraialBatchCaptureDocIconProductConversion);
		explicitWait(chk_batchSelectionProductConversion, 10);
		click(chk_batchSelectionProductConversion);
		String capturedQuantityLineThreeFromProduct = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantityLineThreeFromProduct.equals("10")) {
			writeTestResults("Verify user allwo to capture serials for From Product(Line 03)",
					"User should allwo to capture serials for From Product(Line 03)",
					"User successfully allwo to capture serials for From Product(Line 03)", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serials for From Product(Line 03)",
					"User should allwo to capture serials for From Product(Line 03)",
					"User not allwo to capture serials for From Product(Line 03)", "fail");

		}
		click(btn_commoApplyLast);

		click(btn_serleNosProductConversionToProduct.replace("row", "3"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);

		String capturedQuantityLineThreeToProduct = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantityLineThreeToProduct.equals("5")) {
			writeTestResults("Verify user allwo to capture serials for To Product(Line 03)",
					"User should allwo to capture serials for To Product(Line 03)",
					"User successfully allwo to capture serials for To Product(Line 03)", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serials for To Product(Line 03)",
					"User should allwo to capture serials for To Product(Line 03)",
					"User not allwo to capture serials for To Product(Line 03)", "fail");

		}
		click(btn_commoApplyLast);

		/* capture row 04 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "4"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		explicitWait(txt_serielCountProductConversion, 20);
		sendKeys(txt_serielCountProductConversion, "10");
		sendKeys(txt_serielNo, getInvObj().getOngoinfSerirlSpecificSerielNumber(10));
		pressEnter(txt_serielNo);
		Thread.sleep(3000);
		String capturedQuantityLineFourFromProduct = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantityLineFourFromProduct.equals("10")) {
			writeTestResults("Verify user allwo to capture serials for From Product(Line 04)",
					"User should allwo to capture serials for From Product(Line 04)",
					"User successfully allwo to capture serials for From Product(Line 04)", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serials for From Product(Line 04)",
					"User should allwo to capture serials for From Product(Line 04)",
					"User not allwo to capture serials for From Product(Line 04)", "fail");

		}

		click(btn_commoApplyLast);

		click(btn_serleNosProductConversionToProduct.replace("row", "4"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);

		String capturedQuantityLineFourToProduct = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantityLineFourToProduct.equals("5")) {
			writeTestResults("Verify user allwo to capture serials for To Product(Line 04)",
					"User should allwo to capture serials for To Product(Line 04)",
					"User successfully allwo to capture serials for To Product(Line 04)", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serials for To Product(Line 04)",
					"User should allwo to capture serials for To Product(Line 04)",
					"User not allwo to capture serials for To Product(Line 04)", "fail");

		}

		click(btn_commoApplyLast);

		explicitWait(btn_draftActionVerification, 30);
		click(btn_draftActionVerification);

		getInvObjReg().customizeLoadingDelay(header_drftedProductConversion, 40);

		if (isDisplayed(header_drftedProductConversion)) {
			writeTestResults("Verify user can drfat the Product Conversion",
					"User should be able to drfat the Product Conversion",
					"User successfully drfat the Product Conversion", "pass");
		} else {
			writeTestResults("Verify user can drfat the Product Conversion",
					"User should be able to drfat the Product Conversion", "User doesn't drfat the Product Conversion",
					"fail");
		}

		click(btn_editActionVerification);
		explicitWait(btn_updateActionVerification, 30);

		if (isDisplayed(btn_updateActionVerification)) {
			writeTestResults("Verify user navigated to Edit mode", "User should be navigated to Edit mode",
					"User successfully navigated to Edit mode", "pass");
		} else {
			writeTestResults("Verify user navigated to Edit mode", "User should be navigated to Edit mode",
					"User doesn't navigated to Edit mode", "fail");
		}

		click(btn_deleteRowCommonRowReplace.replace("row", "1"));
		int rowCount = getCount(count_rowProductConversionGrid);

		if (rowCount == 3) {
			writeTestResults("Verify user able to delete the first raw", "User should be able to delete the first raw",
					"User successfully delete the first raw", "pass");
		} else {
			writeTestResults("Verify user able to delete the first raw", "User should be able to delete the first raw",
					"User doesn't delete the raw", "pass");
		}

		click(btn_updateActionVerification);
		explicitWait(btn_editActionVerification, 40);

		int rowCountAfterDraft = getCount(count_rowProductConversionGrid);

		if (rowCountAfterDraft == 3) {
			writeTestResults("Verify edited things were updated", "Edited things should be updated",
					"Edited things successfully updated", "pass");
		} else {
			writeTestResults("Verify edited things were updated", "Edited things should be updated",
					"Edited things doesn't updated", "fail");
		}

		/* Check serials after draft */
		/* Row 01 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "1"));

		String capturedQuantityLineOneFromProductAfterDraft = getAttribute(
				txt_capturedQuantityAfterDraftProductConversion, "value");

		if (capturedQuantityLineOneFromProductAfterDraft.equals("10")) {
			writeTestResults("Verify captured serials are displayed in the window for From Product(Line 01)",
					"Captured serials should be displayed in the window for From Product(Line 01)",
					"Captured serials successfully displayed in the window for From Product(Line 01)", "pass");
		} else {
			writeTestResults("Verify captured serials are displayed in the window for From Product(Line 01)",
					"Captured serials should be displayed in the window for From Product(Line 01)",
					"Captured serials not displayed in the window for From Product(Line 01)", "fail");

		}
		click(btn_closeWindowPopup);

		click(btn_serleNosProductConversionToProduct.replace("row", "1"));

		String capturedQuantityLineOneToProductAfterDraft = getAttribute(
				txt_capturedQuantityAfterDraftProductConversion, "value");

		if (capturedQuantityLineOneToProductAfterDraft.equals("5")) {
			writeTestResults("Verify captured serials are displayed in the window for To Product(Line 01)",
					"Captured serials should be displayed in the window for To Product(Line 01)",
					"Captured serials successfully displayed in the window for To Product(Line 01)", "pass");
		} else {
			writeTestResults("Verify captured serials are displayed in the window for To Product(Line 01)",
					"Captured serials should be displayed in the window for To Product(Line 01)",
					"Captured serials not displayed in the window for To Product(Line 01)", "fail");

		}

		click(btn_closeWindowPopup);

		/* Row 02 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "2"));

		String capturedQuantityLineTwoFromProductAfterDraft = getAttribute(
				txt_capturedQuantityAfterDraftProductConversion, "value");

		if (capturedQuantityLineTwoFromProductAfterDraft.equals("10")) {
			writeTestResults("Verify captured serials are displayed in the window for From Product(Line 02)",
					"Captured serials should be displayed in the window for From Product(Line 02)",
					"Captured serials successfully displayed in the window for From Product(Line 02)", "pass");
		} else {
			writeTestResults("Verify captured serials are displayed in the window for From Product(Line 02)",
					"Captured serials should be displayed in the window for From Product(Line 02)",
					"Captured serials not displayed in the window for From Product(Line 02)", "fail");

		}
		click(btn_closeWindowPopup);

		click(btn_serleNosProductConversionToProduct.replace("row", "2"));

		String capturedQuantityLineTwoToProductAfterDraft = getAttribute(
				txt_capturedQuantityAfterDraftProductConversion, "value");

		if (capturedQuantityLineTwoToProductAfterDraft.equals("5")) {
			writeTestResults("Verify captured serials are displayed in the window for To Product(Line 02)",
					"Captured serials should be displayed in the window for To Product(Line 02)",
					"Captured serials successfully displayed in the window for To Product(Line 02)", "pass");
		} else {
			writeTestResults("Verify captured serials are displayed in the window for To Product(Line 02)",
					"Captured serials should be displayed in the window for To Product(Line 02)",
					"Captured serials not displayed in the window for To Product(Line 02)", "fail");

		}

		click(btn_closeWindowPopup);

		/* Row 03 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "3"));

		String capturedQuantityLineThreeFromProductAfterDraft = getAttribute(
				txt_capturedQuantityAfterDraftProductConversion, "value");

		if (capturedQuantityLineThreeFromProductAfterDraft.equals("10")) {
			writeTestResults("Verify captured serials are displayed in the window for From Product(Line 03)",
					"Captured serials should be displayed in the window for From Product(Line 03)",
					"Captured serials successfully displayed in the window for From Product(Line 03)", "pass");
		} else {
			writeTestResults("Verify captured serials are displayed in the window for From Product(Line 03)",
					"Captured serials should be displayed in the window for From Product(Line 03)",
					"Captured serials not displayed in the window for From Product(Line 03)", "fail");

		}
		click(btn_closeWindowPopup);

		click(btn_serleNosProductConversionToProduct.replace("row", "3"));

		String capturedQuantityLineThreeToProductAfterDraft = getAttribute(
				txt_capturedQuantityAfterDraftProductConversion, "value");

		if (capturedQuantityLineThreeToProductAfterDraft.equals("5")) {
			writeTestResults("Verify captured serials are displayed in the window for To Product(Line 03)",
					"Captured serials should be displayed in the window for To Product(Line 03)",
					"Captured serials successfully displayed in the window for To Product(Line 03)", "pass");
		} else {
			writeTestResults("Verify captured serials are displayed in the window for To Product(Line 03)",
					"Captured serials should be displayed in the window for To Product(Line 03)",
					"Captured serials not displayed in the window for To Product(Line 03)", "fail");

		}

		click(btn_closeWindowPopup);

	}

	/* IN_PC_036 */
	public void backDateStockAdjustment_IN_PC_036() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		invObj.customizeLoadingDelay(navigation_pane, 50);
		Thread.sleep(3000);
		invObj.customizeLoadingDelay(navigation_pane, 20);
		click(navigation_pane);
		invObj.customizeLoadingDelay(inventory_module, 40);
		click(inventory_module);
		invObj.customizeLoadingDelay(btn_stockAdjustment, 40);
		click(btn_stockAdjustment);
		invObj.customizeLoadingDelay(btn_newStockAdjustment, 40);
		click(btn_newStockAdjustment);

		invObj.customizeLoadingDelay(btn_postDate, 30);
		click(btn_postDate);
		invObj.customizeLoadingDelay(btn_postDateInsitePostDatePopup, 10);
		click(btn_postDateInsitePostDatePopup);
		invObj.customizeLoadingDelay(btn_calenderBack, 10);
		click(btn_calenderBack);
		Thread.sleep(1000);
		click("15_Link");
		click(btn_commoApplyLast);
		Thread.sleep(2000);

		sendKeys(txt_descStockAdj, descStockAdj);

		selectText(dropdown_warehouseStockAdj, warehouseStockAdj);

		/* Product add to the grid */
		for (int i = 1; i < 2; i++) {
			String str_i = String.valueOf(i);
			click(btn_productLookupStockAdjForLoop.replace("row", str_i));
			Thread.sleep(3000);
			invObj.customizeLoadingDelay(txt_productSearch, 15);

			String product = "";

			if (i == 1) {
				product = "BatchSpecific";
			} else {
				System.out.println("Product type cannt identified.........");
			}

			invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation(product));
			pressEnter(txt_productSearch);
			String product_path = result_cpmmonProduct.replace("product", invObj.readTestCreation(product));
			invObj.customizeLoadingDelay(product_path, 30);
			doubleClick(product_path);
			Thread.sleep(1500);
			String txt_qty = txt_qtyProductGridStockAdjustment.replace("row", str_i);
			String txt_cost = txt_costProductGridStockAdjustment.replace("row", str_i);

			sendKeys(txt_qty, productQuantity1);

			sendKeys(txt_cost, productCost1);
			invObj.sleepCusomized(btn_avilabilityCheckWidget);

			invObj.sleepCusomized(btn_addNewRow);
			click(btn_addNewRow);
		}

		click(btn_deleteLastRowOfTheGrid);

		click(btn_draftActionVerification);
		invObj.customizeLoadingDelay(brn_serielBatchCapture, 40);
		click(brn_serielBatchCapture);

		/* Product 01 */
		invObj.customizeLoadingDelay(btn_serielBatchCaptureProduct1ILOOutboundShipment, 40);
		Thread.sleep(3000);
		String item_xpath;
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "1");
		click(item_xpath);
		String batchNumber = invObj.genBatchSpecificSerielNumber(200);
		sendKeys(txt_batchNumberCaptureILOOutboundShipment, batchNumber);

		String lotNo = invObj.genProductId() + invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "");
		sendKeys(txt_lotNumberStockAdjustment, lotNo);
		click(txt_expiryDateStockAdjustmentProductCapture);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_lotNumberStockAdjustment);
		Thread.sleep(1000);
		invObj.customizeLoadingDelay(btn_updateCapture, 50);
		click(btn_updateCapture);
		invObj.customizeLoadingDelay((item_xpath + span_afterCaptureSerialGreen), 30);

		Thread.sleep(3000);
		click(btn_productCaptureBackButton);
		Thread.sleep(1000);
		click(btn_relese);
		invObj.customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 60);
	}

	public void releaseProductConversionWithBackDate_IN_PC_036() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		invObj.customizeLoadingDelay(btn_postDate, 30);
		String post_date_before = getText(btn_postDate).replaceAll("/", "");
		click(btn_postDate);
		invObj.customizeLoadingDelay(btn_postDateInsitePostDatePopup, 10);
		click(btn_postDateInsitePostDatePopup);
		invObj.customizeLoadingDelay(btn_calenderBack, 10);
		click(btn_calenderBack);
		Thread.sleep(1000);
		click("15_Link");
		click(btn_commoApplyLast);
		Thread.sleep(2000);
		String post_date_after = getText(btn_postDate).replaceAll("/", "");

		int pst_date_before = Integer.parseInt(post_date_before);
		int pst_date_after = Integer.parseInt(post_date_after);

		if (pst_date_after < pst_date_before) {
			writeTestResults("Verify user allow to select past date as post date",
					"User should allow to select past date as post date", "User allow to select past date as post date",
					"pass");
		} else {
			writeTestResults("Verify user allow to select past date as post date",
					"User should allow to select past date as post date",
					"User not allow to select past date as post date", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, productConversionWarehouseRegression);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify user allow to fill mandatory fields", "User should allow to fill mandatory fields",
					"User allow to fill mandatory fields", "pass");
		} else {
			writeTestResults("Verify user allow to fill mandatory fields", "User should allow to fill mandatory fields",
					"User not allow to fill mandatory fields", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User allow to select \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User not allow to select \"From Product\"",
					"fail");
		}
		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), quantityFiveProductConversion);

		click(btn_serielBatchCaptureNosProductConversionRowReplace.replace("row", "1"));
		getInvObjReg().customizeLoadingDelayAndContinue(header_batchCaptureWindowProductConversion, 20);

		click(btn_seraialBatchCaptureDocIconProductConversion);

		getInvObjReg().customizeLoadingDelay(chk_selectBatchForBatchSpecificProduct, 20);
		click(chk_selectBatchForBatchSpecificProduct);

		Thread.sleep(3000);

		String capturedQuantityBatchSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantityBatchSpecific.equals(quantityFiveProductConversion)) {
			writeTestResults("Verify user allwo to capture batches for From Product",
					"User should allwo to capture batches for From Product",
					"User allwo to capture batches for From Product", "pass");
		} else {
			writeTestResults("Verify user allwo to capture batches for From Product",
					"User should allwo to capture batches for From Product",
					"User not allwo to capture batches for From Product", "fail");

		}

		click(btn_applyLast);

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User allow to select \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User not allow to select \"To Product\"", "fail");
		}

		sendKeys(txt_quantityToProductProductConversion, quantityToProductProductConversion);

		click(btn_serialNosFromProductConversionToProduc);
		getInvObjReg().customizeLoadingDelayAndContinue(header_serileCaptureWindowProductConversion, 20);

		if (isDisplayed(header_serileCaptureWindowProductConversion)) {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window successfully pop-up",
					"pass");
		} else {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window doesn't pop-up",
					"fail");
		}

		getInvObjReg().customizeLoadingDelay(chk_serielRangeProductConversion, 20);
		Thread.sleep(1500);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, quantityToProductProductConversion);

		String sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click(btn_date31ProductConversionToProduct);
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click(btn_date31ProductConversionToProduct);
		pressEnter(txt_lotNo);
		Thread.sleep(3000);

		String capturedQuantitySerielSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantitySerielSpecific.equals(quantityToProductProductConversion)) {
			writeTestResults("Verify user allwo to capture serials for To Product",
					"User should allwo to capture serials for To Product",
					"User allwo to capture serials for To Product", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serials for To Product",
					"User should allwo to capture serials for To Product",
					"User not allwo to capture serials for To Product", "fail");

		}

		click(btn_applyLast);

		click(btn_draftActionVerification);

		getInvObjReg().customizeLoadingDelay(header_drftedProductConversion, 40);
		if (isDisplayed(header_drftedProductConversion)) {
			writeTestResults("Verify user allow to draft the Product Conversion",
					"User should allow to draft the Product Conversiont", "User allow to draft the Product Conversion",
					"pass");
		} else {
			writeTestResults("Verify user allow to draft the Product Conversion",
					"User should allow to draft the Product Conversiont",
					"User not allow to draft the Product Conversion", "fail");

		}

		click(btn_releaseCommon);

		getInvObjReg().customizeLoadingDelay(header_releasedProductConversion, 40);
		trackCode = getText(lbl_docNo);

		if (isDisplayed(header_releasedProductConversion)) {
			writeTestResults("Verify Product Conversion is released", "Product conversion should be released",
					"Product conversion successfully released", "pass");
		} else {
			writeTestResults("Verify Product Conversion is released", "Product conversion should be released",
					"Product conversion doesn't released", "fail");

		}
	}

	/* IN_PC_037 */
	public void backDateStockAdjustment_IN_PC_037() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		invObj.customizeLoadingDelay(navigation_pane, 50);
		Thread.sleep(3000);
		invObj.customizeLoadingDelay(navigation_pane, 20);
		click(navigation_pane);
		invObj.customizeLoadingDelay(inventory_module, 40);
		click(inventory_module);
		invObj.customizeLoadingDelay(btn_stockAdjustment, 40);
		click(btn_stockAdjustment);
		invObj.customizeLoadingDelay(btn_newStockAdjustment, 40);
		click(btn_newStockAdjustment);
		invObj.customizeLoadingDelay(btn_postDate, 30);
		click(btn_postDate);
		invObj.customizeLoadingDelay(btn_postDateInsitePostDatePopup, 10);
		click(btn_postDateInsitePostDatePopup);
		invObj.customizeLoadingDelay(btn_calenderBack, 10);
		click(btn_calenderBack);
		Thread.sleep(1000);
		click("15_Link");
		click(btn_commoApplyLast);

		sendKeys(txt_descStockAdj, descStockAdj);

		selectText(dropdown_warehouseStockAdj, warehouseStockAdj);

		/* Product add to the grid */
		for (int i = 1; i < 3; i++) {
			String str_i = String.valueOf(i);
			click(btn_productLookupStockAdjForLoop.replace("row", str_i));
			Thread.sleep(3000);
			invObj.customizeLoadingDelay(txt_productSearch, 15);

			String product = "";

			if (i == 1) {
				product = "BatchSpecific";
			} else if (i == 2) {
				product = "BatchFifo";
			} else {
				System.out.println("Product type cannt identified.........");
			}

			invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation(product));
			pressEnter(txt_productSearch);
			String product_path = result_cpmmonProduct.replace("product", invObj.readTestCreation(product));
			invObj.customizeLoadingDelay(product_path, 30);
			doubleClick(product_path);
			Thread.sleep(1500);
			String txt_qty = txt_qtyProductGridStockAdjustment.replace("row", str_i);
			String txt_cost = txt_costProductGridStockAdjustment.replace("row", str_i);

			sendKeys(txt_qty, productQuantity1);

			sendKeys(txt_cost, productCost1);
			invObj.sleepCusomized(btn_avilabilityCheckWidget);

			invObj.sleepCusomized(btn_addNewRow);
			click(btn_addNewRow);
		}

		click(btn_deleteLastRowOfTheGrid);

		click(btn_draftActionVerification);
		invObj.customizeLoadingDelay(brn_serielBatchCapture, 40);
		click(brn_serielBatchCapture);

		/* Product 01 */
		invObj.customizeLoadingDelay(btn_serielBatchCaptureProduct1ILOOutboundShipment, 40);
		Thread.sleep(3000);
		String item_xpath;
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "1");
		click(item_xpath);
		String batchNumber = invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "-BD";
		sendKeys(txt_batchNumberCaptureILOOutboundShipment, batchNumber);
		invObj.writeRegression01("BatchNumberBatchSpecificBackDate_IN_PC_037", batchNumber, 169);

		String lotNo = invObj.genProductId() + invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "");
		sendKeys(txt_lotNumberStockAdjustment, lotNo);
		click(txt_expiryDateStockAdjustmentProductCapture);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_lotNumberStockAdjustment);
		Thread.sleep(1000);
		invObj.customizeLoadingDelay(btn_updateCapture, 50);
		click(btn_updateCapture);
		invObj.customizeLoadingDelay((item_xpath + span_afterCaptureSerialGreen), 30);

		/* Product 02 */
		Thread.sleep(3000);
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "2");
		click(item_xpath);

		String batchNumberBatchFifo = invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "-BD";
		sendKeys(txt_batchNumberCaptureILOOutboundShipment, batchNumberBatchFifo);
		invObj.writeRegression01("BatchNumberBatchFifoBackDate_IN_PC_037", batchNumberBatchFifo, 170);

		sendKeys(txt_lotNumberStockAdjustment,
				invObj.genProductId() + invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_expiryDateStockAdjustmentProductCapture);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_lotNumberStockAdjustment);
		Thread.sleep(1000);
		invObj.customizeLoadingDelay(btn_updateCapture, 50);
		click(btn_updateCapture);
		invObj.customizeLoadingDelay((item_xpath + span_afterCaptureSerialGreen), 30);

		Thread.sleep(3000);
		click(btn_productCaptureBackButton);
		Thread.sleep(1000);
		click(btn_relese);
		invObj.customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 60);
		if (isDisplayed(lbl_relesedConfirmation.replace("(Released)", "( Released )"))) {
			writeTestResults("Verify user can release back dated Stock Adjustment",
					"User should be able to release back dated Stock Adjustment",
					"User successfully release back dated Stock Adjustment", "pass");
		} else {
			writeTestResults("Verify user can release back dated Stock Adjustment",
					"User should be able to release back dated Stock Adjustment",
					"User doesn't release back dated Stock Adjustment", "fail");
		}
	}

	public void toDateStockAdjustment_IN_PC_037() throws Exception {
		invObj.customizeLoadingDelay(navigation_pane, 50);
		Thread.sleep(3000);
		invObj.customizeLoadingDelay(navigation_pane, 20);
		click(navigation_pane);
		invObj.customizeLoadingDelay(inventory_module, 40);
		click(inventory_module);
		invObj.customizeLoadingDelay(btn_stockAdjustment, 40);
		click(btn_stockAdjustment);
		invObj.customizeLoadingDelay(btn_newStockAdjustment, 40);
		click(btn_newStockAdjustment);

		sendKeys(txt_descStockAdj, descStockAdj);

		selectText(dropdown_warehouseStockAdj, warehouseStockAdj);

		/* Product add to the grid */
		for (int i = 1; i < 3; i++) {
			String str_i = String.valueOf(i);
			click(btn_productLookupStockAdjForLoop.replace("row", str_i));
			Thread.sleep(3000);
			invObj.customizeLoadingDelay(txt_productSearch, 15);

			String product = "";

			if (i == 1) {
				product = "BatchSpecific";
			} else if (i == 2) {
				product = "BatchFifo";
			} else {
				System.out.println("Product type cannt identified.........");
			}

			invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation(product));
			pressEnter(txt_productSearch);
			String product_path = result_cpmmonProduct.replace("product", invObj.readTestCreation(product));
			invObj.customizeLoadingDelay(product_path, 30);
			doubleClick(product_path);
			Thread.sleep(1500);
			String txt_qty = txt_qtyProductGridStockAdjustment.replace("row", str_i);
			String txt_cost = txt_costProductGridStockAdjustment.replace("row", str_i);

			sendKeys(txt_qty, productQuantity1);

			sendKeys(txt_cost, productCost1);
			invObj.sleepCusomized(btn_avilabilityCheckWidget);

			invObj.sleepCusomized(btn_addNewRow);
			click(btn_addNewRow);
		}

		click(btn_deleteLastRowOfTheGrid);

		click(btn_draftActionVerification);
		invObj.customizeLoadingDelay(brn_serielBatchCapture, 40);
		click(brn_serielBatchCapture);

		/* Product 01 */
		invObj.customizeLoadingDelay(btn_serielBatchCaptureProduct1ILOOutboundShipment, 40);
		Thread.sleep(3000);
		String item_xpath;
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "1");
		click(item_xpath);
		String batchNumber = invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "-TD";
		sendKeys(txt_batchNumberCaptureILOOutboundShipment, batchNumber);
		invObj.writeRegression01("BatchNumberBatchSpecificToDate_IN_PC_037", batchNumber, 171);

		String lotNo = invObj.genProductId() + invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "");
		sendKeys(txt_lotNumberStockAdjustment, lotNo);
		click(txt_expiryDateStockAdjustmentProductCapture);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_lotNumberStockAdjustment);
		Thread.sleep(1000);
		invObj.customizeLoadingDelay(btn_updateCapture, 50);
		click(btn_updateCapture);
		invObj.customizeLoadingDelay((item_xpath + span_afterCaptureSerialGreen), 30);

		/* Product 02 */
		Thread.sleep(3000);
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "2");
		click(item_xpath);

		String batchNumberBatchFifo = invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "-BD";
		sendKeys(txt_batchNumberCaptureILOOutboundShipment, batchNumberBatchFifo);
		invObj.writeRegression01("BatchNumberBatchFifoToDate_IN_PC_037", batchNumberBatchFifo, 172);

		sendKeys(txt_lotNumberStockAdjustment,
				invObj.genProductId() + invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_expiryDateStockAdjustmentProductCapture);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_lotNumberStockAdjustment);
		Thread.sleep(1000);
		invObj.customizeLoadingDelay(btn_updateCapture, 50);
		click(btn_updateCapture);
		invObj.customizeLoadingDelay((item_xpath + span_afterCaptureSerialGreen), 30);

		Thread.sleep(3000);
		click(btn_productCaptureBackButton);
		Thread.sleep(1000);
		click(btn_relese);
		invObj.customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 60);
		if (isDisplayed(lbl_relesedConfirmation.replace("(Released)", "( Released )"))) {
			writeTestResults("Verify user can release today dated Stock Adjustment",
					"User should be able to release today dated Stock Adjustment",
					"User successfully release back today Stock Adjustment", "pass");
		} else {
			writeTestResults("Verify user can release today dated Stock Adjustment",
					"User should be able to release today dated Stock Adjustment",
					"User doesn't release today dated Stock Adjustment", "fail");
		}
	}

	public void verifyBatchesWithDifferentPostDates_IN_PC_037() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		invObj.customizeLoadingDelay(btn_postDate, 30);
		click(btn_postDate);
		invObj.customizeLoadingDelay(btn_postDateInsitePostDatePopup, 10);
		click(btn_postDateInsitePostDatePopup);
		invObj.customizeLoadingDelay(btn_calenderBack, 10);
		click(btn_calenderBack);
		Thread.sleep(1000);
		click("15_Link");
		click(btn_commoApplyLast);

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, productConversionWarehouseRegression);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify user able to select a warehouse", "User should be able to select a warehouse",
					"User able to select a warehouse", "pass");
		} else {
			writeTestResults("Verify user able to select a warehouse", "User should be able to select a warehouse",
					"User doesn't able to select a warehouse", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);

		if (isDisplayed(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")))) {

		} else {
			writeTestResults("Verify products were loaded in product lookup",
					"Products should be loaded in product lookup", "Products weren't loaded in product lookup", "fail");
		}

		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select batch specific as \"From Product\"",
					"User should allow to select batch specific as \"From Product\"",
					"User allow to select batch specific as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select batch specific as \"From Product\"",
					"User should allow to select batch specific as \"From Product\"",
					"User not allow to select batch specific as \"From Product\"", "fail");
		}

		click(btn_addNewRowProductConversion);

		click(btn_productLookupRowReplaceProductConversion.replace("row", "2"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchFifo")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchFifo")))) {
			writeTestResults("Verify user allow to select batch FIFO as \"From Product\"",
					"User should allow to select batch FIFO as \"From Product\"",
					"User allow to select batch FIFO as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select batch FIFO as \"From Product\"",
					"User should allow to select batch FIFO as \"From Product\"",
					"User not allow to select batch FIFO as \"From Product\"", "fail");
		}

		click(btn_serielBatchCaptureNosProductConversionRowReplace.replace("row", "1"));
		getInvObjReg().customizeLoadingDelayAndContinue(header_batchCaptureWindowProductConversion, 20);
		click(btn_seraialBatchCaptureDocIconProductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(headers_existingSerialBatchNos, 20);

		if (isDisplayed(headers_existingSerialBatchNos)) {

		} else {
			writeTestResults("Verify headers properly positioned in Existing Serials Batch Nos",
					"Headers properly positioned in Existing Serials Batch Nos",
					"Headers doesn't properly positioned in Existing Serials Batch Nos", "fail");
		}

		if (isDisplayed(div_batchInExistingSerialBatchesBatchReplace.replace("batch_no",
				getInvObj().readIWRegression01("BatchNumberBatchSpecificBackDate_IN_PC_037")))) {
			writeTestResults("Verify existing batches of batch specific product for selected post date were loaded",
					"Existing batches of batch specific product for selected post date should be loaded",
					"Existing batches of batch specific product for selected post date successfully loaded", "pass");
		} else {
			writeTestResults("Verify existing batches of batch specific product for selected post date were loaded",
					"Existing batches of batch specific product for selected post date should be loaded",
					"Existing batches of batch specific product for selected post date doesn't loaded", "fail");
		}

		if (!getInvObj().isDisplayedQuickCheck(div_batchInExistingSerialBatchesBatchReplace.replace("batch_no",
				getInvObj().readIWRegression01("BatchNumberBatchSpecificToDate_IN_PC_037")))) {
			writeTestResults("Verify existing batches of batch specific product for today's date weren't loaded",
					"Existing batches of batch specific product for today's date shouldn't be loaded",
					"Existing batches of batch specific product for today's date weren't loaded", "pass");
		} else {
			writeTestResults("Verify existing batches of batch specific product for today's date weren't loaded",
					"Existing batches of batch specific product for today's date shouldn't be loaded",
					"Existing batches of batch specific product for today's date were loaded", "fail");
		}

		click(span_closeButtonLast);
		Thread.sleep(1000);
		click(btn_closeBatchNos);

		click(btn_serielBatchCaptureNosProductConversionRowReplace.replace("row", "2"));
		getInvObjReg().customizeLoadingDelayAndContinue(header_batchCaptureWindowProductConversion, 20);
		click(btn_seraialBatchCaptureDocIconProductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(headers_existingSerialBatchNos, 20);

		if (isDisplayed(headers_existingSerialBatchNos)) {

		} else {
			writeTestResults("Verify headers properly positioned in Existing Serials Batch Nos",
					"Headers properly positioned in Existing Serials Batch Nos",
					"Headers doesn't properly positioned in Existing Serials Batch Nos", "fail");
		}

		if (isDisplayed(div_batchInExistingSerialBatchesBatchReplace.replace("batch_no",
				getInvObj().readIWRegression01("BatchNumberBatchFifoBackDate_IN_PC_037")))) {
			writeTestResults("Verify existing batches of batch FIFO product for selected post date were loaded",
					"Existing batches of batch FIFO product for selected post date should be loaded",
					"Existing batches of batch FIFO product for selected post date successfully loaded", "pass");
		} else {
			writeTestResults("Verify existing batches of batch FIFO product for selected post date were loaded",
					"Existing batches of batch FIFO product for selected post date should be loaded",
					"Existing batches of batch FIFO product for selected post date doesn't loaded", "fail");
		}

		if (!getInvObj().isDisplayedQuickCheck(div_batchInExistingSerialBatchesBatchReplace.replace("batch_no",
				getInvObj().readIWRegression01("BatchNumberBatchFifoToDate_IN_PC_037")))) {
			writeTestResults("Verify existing batches of batch FIFO product for today's date weren't loaded",
					"Existing batches of batch FIFO product for today's date shouldn't be loaded",
					"Existing batches of batch FIFO product for today's date weren't loaded", "pass");
		} else {
			writeTestResults("Verify existing batches of batch FIFO product for today's date weren't loaded",
					"Existing batches of batch FIFO product for today's date shouldn't be loaded",
					"Existing batches of batch FIFO product for today's date were loaded", "fail");
		}

	}

	/* IN_PC_038 */
	public void verifyBatchesWithDifferentPostDatesInEditMode_IN_PC_038() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		invObj.customizeLoadingDelay(btn_postDate, 30);
		click(btn_postDate);
		invObj.customizeLoadingDelay(btn_postDateInsitePostDatePopup, 10);
		click(btn_postDateInsitePostDatePopup);
		invObj.customizeLoadingDelay(btn_calenderBack, 10);
		click(btn_calenderBack);
		Thread.sleep(1000);
		click("15_Link");
		click(btn_commoApplyLast);

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, productConversionWarehouseRegression);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify user able to select a warehouse", "User should be able to select a warehouse",
					"User able to select a warehouse", "pass");
		} else {
			writeTestResults("Verify user able to select a warehouse", "User should be able to select a warehouse",
					"User doesn't able to select a warehouse", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);

		if (isDisplayed(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")))) {

		} else {
			writeTestResults("Verify products were loaded in product lookup",
					"Products should be loaded in product lookup", "Products weren't loaded in product lookup", "fail");
		}

		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select batch specific as \"From Product\"",
					"User should allow to select batch specific as \"From Product\"",
					"User allow to select batch specific as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select batch specific as \"From Product\"",
					"User should allow to select batch specific as \"From Product\"",
					"User not allow to select batch specific as \"From Product\"", "fail");
		}

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify user allow to select \"To Product\" for first row",
					"User should allow to select \"To Product\" for first row",
					"User allow to select \"To Product\" for first row", "pass");
		} else {
			writeTestResults("Verify user allow to select \"To Product\" for first row",
					"User should allow to select \"To Product\" for first row",
					"User not allow to select \"To Product\" for first row", "fail");
		}

		click(btn_addNewRowProductConversion);

		click(btn_productLookupRowReplaceProductConversion.replace("row", "2"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchFifo")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchFifo")))) {
			writeTestResults("Verify user allow to select batch FIFO as \"From Product\"",
					"User should allow to select batch FIFO as \"From Product\"",
					"User allow to select batch FIFO as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select batch FIFO as \"From Product\"",
					"User should allow to select batch FIFO as \"From Product\"",
					"User not allow to select batch FIFO as \"From Product\"", "fail");
		}

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "2"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify user allow to select \"To Product\" for second row",
					"User should allow to select \"To Product\" for second row",
					"User allow to select \"To Product\" for second row", "pass");
		} else {
			writeTestResults("Verify user allow to select \"To Product\" for second row",
					"User should allow to select \"To Product\" for second row",
					"User not allow to select \"To Product\" for second row", "fail");
		}

		click(btn_draftActionVerification);

		getInvObjReg().customizeLoadingDelayAndContinue(btn_editActionVerification, 10);
		click(btn_editActionVerification);

		getInvObjReg().customizeLoadingDelayAndContinue(btn_updateActionVerification, 10);
		if (isDisplayed(btn_updateActionVerification)) {
			writeTestResults("Verify Product Conversion is in Edit Mode", "Product Conversion should be in Edit Mode",
					"Product Conversion is in Edit Mode", "pass");
		} else {
			writeTestResults("Verify Product Conversion is in Edit Mode", "Product Conversion should be in Edit Mode",
					"Product Conversion isn't in Edit Mode", "fail");
		}

		click(btn_serielBatchCaptureNosProductConversionRowReplace.replace("row", "1"));
		getInvObjReg().customizeLoadingDelayAndContinue(header_batchCaptureWindowProductConversion, 20);
		click(btn_seraialBatchCaptureDocIconProductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(headers_existingSerialBatchNos, 20);

		if (isDisplayed(headers_existingSerialBatchNos)) {

		} else {
			writeTestResults("Verify headers properly positioned in Existing Serials Batch Nos",
					"Headers properly positioned in Existing Serials Batch Nos",
					"Headers doesn't properly positioned in Existing Serials Batch Nos", "fail");
		}

		if (isDisplayed(div_batchInExistingSerialBatchesBatchReplace.replace("batch_no",
				getInvObj().readIWRegression01("BatchNumberBatchSpecificBackDate_IN_PC_037")))) {
			writeTestResults("Verify existing batches of batch specific product for selected post date were loaded",
					"Existing batches of batch specific product for selected post date should be loaded",
					"Existing batches of batch specific product for selected post date successfully loaded", "pass");
		} else {
			writeTestResults("Verify existing batches of batch specific product for selected post date were loaded",
					"Existing batches of batch specific product for selected post date should be loaded",
					"Existing batches of batch specific product for selected post date doesn't loaded", "fail");
		}

		if (!getInvObj().isDisplayedQuickCheck(div_batchInExistingSerialBatchesBatchReplace.replace("batch_no",
				getInvObj().readIWRegression01("BatchNumberBatchSpecificToDate_IN_PC_037")))) {
			writeTestResults("Verify existing batches of batch specific product for today's date weren't loaded",
					"Existing batches of batch specific product for today's date shouldn't be loaded",
					"Existing batches of batch specific product for today's date weren't loaded", "pass");
		} else {
			writeTestResults("Verify existing batches of batch specific product for today's date weren't loaded",
					"Existing batches of batch specific product for today's date shouldn't be loaded",
					"Existing batches of batch specific product for today's date were loaded", "fail");
		}

		click(span_closeButtonLast);
		Thread.sleep(1000);
		click(btn_closeBatchNos);

		click(btn_serielBatchCaptureNosProductConversionRowReplace.replace("row", "2"));
		getInvObjReg().customizeLoadingDelayAndContinue(header_batchCaptureWindowProductConversion, 20);
		click(btn_seraialBatchCaptureDocIconProductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(headers_existingSerialBatchNos, 20);

		if (isDisplayed(headers_existingSerialBatchNos)) {

		} else {
			writeTestResults("Verify headers properly positioned in Existing Serials Batch Nos",
					"Headers properly positioned in Existing Serials Batch Nos",
					"Headers doesn't properly positioned in Existing Serials Batch Nos", "fail");
		}

		if (isDisplayed(div_batchInExistingSerialBatchesBatchReplace.replace("batch_no",
				getInvObj().readIWRegression01("BatchNumberBatchFifoBackDate_IN_PC_037")))) {
			writeTestResults("Verify existing batches of batch FIFO product for selected post date were loaded",
					"Existing batches of batch FIFO product for selected post date should be loaded",
					"Existing batches of batch FIFO product for selected post date successfully loaded", "pass");
		} else {
			writeTestResults("Verify existing batches of batch FIFO product for selected post date were loaded",
					"Existing batches of batch FIFO product for selected post date should be loaded",
					"Existing batches of batch FIFO product for selected post date doesn't loaded", "fail");
		}

		if (!getInvObj().isDisplayedQuickCheck(div_batchInExistingSerialBatchesBatchReplace.replace("batch_no",
				getInvObj().readIWRegression01("BatchNumberBatchFifoToDate_IN_PC_037")))) {
			writeTestResults("Verify existing batches of batch FIFO product for today's date weren't loaded",
					"Existing batches of batch FIFO product for today's date shouldn't be loaded",
					"Existing batches of batch FIFO product for today's date weren't loaded", "pass");
		} else {
			writeTestResults("Verify existing batches of batch FIFO product for today's date weren't loaded",
					"Existing batches of batch FIFO product for today's date shouldn't be loaded",
					"Existing batches of batch FIFO product for today's date were loaded", "fail");
		}

	}

	/* IN_PC_039 */
	public void createLotAutoGenarateWarehouse_IN_PC_039() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}
		click(inventory_module);
		if (isDisplayed(lnk_product_group_configuration)) {
		} else {
			writeTestResults("Verify user can load Inventory module successfully",
					"user should be able load Inventory module successfully",
					"User couldn't load Inventory module successfully", "fail");
		}
		click(btn_warehouseInfo);
		if (isDisplayed(header_warehouseInformationPage)) {

		} else {
			writeTestResults("Verify user can navigate to the warehouse creation page",
					"User should be able to navigate the warehouse creation page",
					"User not navigate to the warehouse creation page", "Fail");
		}

		click(btn_newWarehouse);

		if (isDisplayed(header_warehouseCretionForm)) {

		} else {
			writeTestResults("Verify user can navigate to the warehouse creation form",
					"User should be able to navigate the warehouse creation form",
					"User not navigate to the warehouse creation form", "Fail");
		}

		String warehouse_create = getInvObj().currentTimeAndDate().replaceAll("/", "").replaceAll(":", "");
		getInvObj().writeRegression01("AutoLotWarehouse_IN_PC_039", warehouse_create, 173);
		explicitWait(txt_warehouseCode, 20);
		sendKeys(txt_warehouseCode, warehouse_create);

		sendKeys(txt_warehouseName, warehouseName);

		selectText(dropdown_businessUnit, businessUnit);

		if (isDisplayed(txt_warehouseCode) && isDisplayed(txt_warehouseName) && isDisplayed(dropdown_businessUnit)) {

		} else {
			writeTestResults("Verify user can fill mandatory fields", "User should be able to fill mandatory fields",
					"User couldn't fill mandatory fields", "fail");
		}

		if (!isSelected(checkbox_isAllocation)) {
			click(checkbox_isAllocation);
		}

		if (isSelected(checkbox_isAllocation)) {

		} else {
			writeTestResults("Verify user enable is 'Is Allocation Store' checkbox",
					"User should be able to enable is 'Is Allocation Store' checkbox",
					"User couldn't enable is 'Is Allocation Store' checkbox", "fail");
		}

		selectText(dropdown_qurantineWarehouse, qurantineWarehouse);
		if (isDisplayed(dropdown_qurantineWarehouse)) {

		} else {
			writeTestResults("Verify user select 'Qurantine Warehouse'",
					"User should be able to select 'Qurantine Warehouse'", "User couldn't select 'Qurantine Warehouse'",
					"fail");
		}

		if (!isSelected(checkbox_autoLot)) {
			click(checkbox_autoLot);
		}

		if (isSelected(checkbox_autoLot)) {

		} else {
			writeTestResults("Verify user enable is 'Auto Generate Lot No' checkbox",
					"User should be able to enable is 'Auto Generate Lot No' checkbox",
					"User couldn't enable is 'Auto Generate Lot No' checkbox", "fail");
		}

		selectText(dropdown_lotBook, lotBookWarehouse);
		if (isDisplayed(dropdown_lotBook)) {

		} else {
			writeTestResults("Verify user select 'Lot Book No'", "User should be able to select 'Lot Book No'",
					"User couldn't select 'Lot Book No'", "fail");
		}

		click(btn_draftActionVerification);

		explicitWait(btn_relese, 30);
		click(btn_relese);
		Thread.sleep(3000);
		trackCode = warehouse_create;
		if (isDisplayed(btn_releseConfirmation)) {

			writeTestResults("Verify user can releese Warehouse", "User should be able to releese Warehouse",
					"User sucessfully releese Warehouse", "pass");
		} else {
			writeTestResults("Verify user can releese Warehouse", "User should be able to releese Warehouse",
					"User couldn't releese Warehouse", "fail");
		}
	}

	public void stockAdjustmentForFromProduct_IN_PC_039() throws Exception {
		invObj.customizeLoadingDelay(navigation_pane, 50);
		Thread.sleep(3000);
		invObj.customizeLoadingDelay(navigation_pane, 20);
		click(navigation_pane);
		invObj.customizeLoadingDelay(inventory_module, 40);
		click(inventory_module);
		invObj.customizeLoadingDelay(btn_stockAdjustment, 40);
		Thread.sleep(1500);
		click(btn_stockAdjustment);
		invObj.customizeLoadingDelay(btn_newStockAdjustment, 40);
		click(btn_newStockAdjustment);

		sendKeys(txt_descStockAdj, descStockAdj);

		selectText(dropdown_warehouseStockAdj,
				getInvObj().readIWRegression01("AutoLotWarehouse_IN_PC_039") + " [Negombo]");

		/* Product add to the grid */
		for (int i = 1; i < 2; i++) {
			String str_i = String.valueOf(i);
			click(btn_productLookupStockAdjForLoop.replace("row", str_i));
			Thread.sleep(3000);
			invObj.customizeLoadingDelay(txt_productSearch, 15);

			String product = "";

			if (i == 1) {
				product = "Lot";
			} else {
				System.out.println("Product type cannt identified.........");
			}

			invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation(product));
			pressEnter(txt_productSearch);
			String product_path = result_cpmmonProduct.replace("product", invObj.readTestCreation(product));
			invObj.customizeLoadingDelay(product_path, 30);
			doubleClick(product_path);
			Thread.sleep(1500);
			String txt_qty = txt_qtyProductGridStockAdjustment.replace("row", str_i);
			String txt_cost = txt_costProductGridStockAdjustment.replace("row", str_i);

			sendKeys(txt_qty, productQuantity1);

			sendKeys(txt_cost, productCost1);
			invObj.sleepCusomized(btn_avilabilityCheckWidget);

			invObj.sleepCusomized(btn_addNewRow);
			click(btn_addNewRow);
		}

		click(btn_deleteLastRowOfTheGrid);

		click(btn_draftActionVerification);
		invObj.customizeLoadingDelay(btn_relese, 40);

		click(btn_relese);
		invObj.customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 60);
		if (isDisplayed(lbl_relesedConfirmation.replace("(Released)", "( Released )"))) {
			writeTestResults("Verify user can release  Stock Adjustment",
					"User should be able to release  Stock Adjustment",
					"User successfully release back today Stock Adjustment", "pass");
		} else {
			writeTestResults("Verify user can release  Stock Adjustment",
					"User should be able to release  Stock Adjustment", "User doesn't release  Stock Adjustment",
					"fail");
		}
	}

	public void checkAutoLotGenarateFunction_IN_PC_039() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, getInvObj().readIWRegression01("AutoLotWarehouse_IN_PC_039") + " [Negombo]");
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(getInvObj().readIWRegression01("AutoLotWarehouse_IN_PC_039") + " [Negombo]")) {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User allow to fill summary details", "pass");
		} else {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User not allow to fill summary details", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("Lot"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(result_cpmmonProduct.replace("product", invObj.readTestCreation("Lot")),
				20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("Lot")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("Lot")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("product", invObj.readTestCreation("Lot")))) {
			writeTestResults("Verify user allow to select Lot product as \"From Product\"",
					"User should allow to select Lot product as \"From Product\"",
					"User allow to select Lot product as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select Lot product as \"From Product\"",
					"User should allow to select Lot product as \"From Product\"",
					"User not allow to select Lot product as \"From Product\"", "fail");
		}

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielBatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielBatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielBatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielBatchSpecific")),
				10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerielBatchSpecific")))) {
			writeTestResults("Verify user allow to select serial batch specific product as \"To Product\"",
					"User should allow to select serial batch specific product as \"To Product\"",
					"User allow to select serial batch specific product as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select serial batch specific product as \"To Product\"",
					"User should allow to select serial batch specific product as \"To Product\"",
					"User not allow to select serial batch specific product as \"To Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), quantityFiveProductConversion);

		sendKeys(txt_quantityToProductProductConversion, quantityTenProductConversion);

		explicitWait(btn_serleNosProductConversionToProduct.replace("row", "1"), 10);
		click(btn_serleNosProductConversionToProduct.replace("row", "1"));

		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, quantityTenProductConversion);
		String sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);

		sendKeys(txt_batchNoProductConversionToProduct,
				(invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));

		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");

		Thread.sleep(3000);
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");

		pressEnter(txt_lotNo);

		Thread.sleep(2000);
		String capturedQuantity = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantity.equals(quantityTenProductConversion)) {
			writeTestResults("Verify user allwo to capture serials batches without lot",
					"User should allwo to capture serials batches without lot",
					"User allwo to capture serials batches without lot", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serials batches without lot",
					"User should allwo to capture serials batches without lot",
					"User not allwo to capture serials batches without lot", "fail");
		}

		click(btn_applyLast);

		explicitWait(btn_draftActionVerification, 20);
		click(btn_draftActionVerification);

		explicitWait(btn_releaseCommon, 20);
		click(btn_releaseCommon);

		explicitWait(header_releasedProductConversion, 10);
		click(btn_serleNosProductConversionToProduct.replace("row", "1"));

		explicitWait(headers_upTpToLotCapturedSerielBatchToProductProductConversion.replace("row", "1"), 10);
		if (isDisplayed(headers_upTpToLotCapturedSerielBatchToProductProductConversion)) {

		} else {
			writeTestResults("Verify headers are properly positioned", "Headers should be properly positioned",
					"Headers doesn't properly positioned", "fail");
		}

		boolean flagAutoGenLot = true;

		for (int i = 1; i <= 10; i++) {
			String capturedLot = getText(td_capturedLotOnSerielNos.replace("row", String.valueOf(i)));
			if (capturedLot.equals("N/A")) {
				flagAutoGenLot = false;
				break;
			} else if (capturedLot.length() < 1) {
				flagAutoGenLot = false;
				break;
			}
		}

		if (flagAutoGenLot) {
			writeTestResults("Verify Lot numbers are auto-generated", "Lot numbers should be auto-generated",
					"Lot numbers successfully auto-generated", "pass");
		} else {
			writeTestResults("Verify Lot numbers are auto-generated", "Lot numbers should be auto-generated",
					"Lot numbers doesn't auto-generated", "fail");

		}

	}

	/* IN_PC_040 */
	public void reverseProductConversion_IN_PC_040() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, productConversionWarehouseRegression);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User allow to fill summary details", "pass");
		} else {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User not allow to fill summary details", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("Lot"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(result_cpmmonProduct.replace("product", invObj.readTestCreation("Lot")),
				20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("Lot")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("Lot")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("product", invObj.readTestCreation("Lot")))) {
			writeTestResults("Verify user allow to select Lot product as \"From Product\"",
					"User should allow to select Lot product as \"From Product\"",
					"User allow to select Lot product as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select Lot product as \"From Product\"",
					"User should allow to select Lot product as \"From Product\"",
					"User not allow to select Lot product as \"From Product\"", "fail");
		}

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select batch specific product as \"To Product\"",
					"User should allow to select batch specific product as \"To Product\"",
					"User allow to select batch specific product as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select batch specific product as \"To Product\"",
					"User should allow to select batch specific product as \"To Product\"",
					"User not allow to select batch specific product as \"To Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), quantityFiveProductConversion);

		sendKeys(txt_quantityToProductProductConversion, quantityTenProductConversion);

		click(btn_addNewRowProductConversion);

		click(btn_productLookupRowReplaceProductConversion.replace("row", "2"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchFifo")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchFifo")))) {
			writeTestResults("Verify user allow to select batch FIFO as \"From Product\"",
					"User should allow to select batch FIFO as \"From Product\"",
					"User allow to select batch FIFO as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select batch FIFO as \"From Product\"",
					"User should allow to select batch FIFO as \"From Product\"",
					"User not allow to select batch FIFO as \"From Product\"", "fail");
		}

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "2"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify user allow to select seriel specific as \"To Product\"",
					"User should allow to select seriel specific as \"To Product\"",
					"User allow to select seriel specific as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select seriel specific as \"To Product\"",
					"User should allow to select seriel specific as \"To Product\"",
					"User not allow to select seriel specific as \"To Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "2"), quantityFiveProductConversion);

		sendKeys(txt_qtyToProductProductConcersionRowReplace.replace("row", "2"), quantityTenProductConversion);

		/* Batch capturing batch FIFO from product */
		click(btn_serleNosProductConversionFromProduct.replace("row", "2"));
		explicitWait(btn_seraialBatchCaptureDocIconProductConversion, 10);
		click(btn_seraialBatchCaptureDocIconProductConversion);
		explicitWait(chk_batchSelectionProductConversion, 10);
		click(chk_batchSelectionProductConversion);
		click(btn_commoApplyLast);

		/* Batch capturing batch specific to product */
		explicitWait(btn_serleNosProductConversionToProduct.replace("row", "1"), 10);
		click(btn_serleNosProductConversionToProduct.replace("row", "1"));
		explicitWait(txt_batchNoToProductProductConversion, 20);
		sendKeys(txt_batchNoToProductProductConversion,
				invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_expiryDateToProductProductConversion);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		sendKeys(txt_lotNoToProductProductConversion,
				invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_menufacDateToProductProductConversion);
		Thread.sleep(200);
		doubleClick(btn_calenderBack);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_lotNoToProductProductConversion);
		Thread.sleep(2000);
		click(btn_commoApplyLast);

		/* Serial capturing serial specific to product */
		click(btn_serleNosProductConversionToProduct.replace("row", "2"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, quantityTenProductConversion);
		String sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		explicitWait(btn_draftActionVerification, 30);
		click(btn_draftActionVerification);

		getInvObjReg().customizeLoadingDelay(header_drftedProductConversion, 40);
		if (isDisplayed(header_drftedProductConversion)) {
			writeTestResults("Verify Product Conversion is drafted", "Product conversion should be drafted",
					"Product conversion successfully drafted", "pass");
		} else {
			writeTestResults("Verify Product Conversion is drafted", "Product conversion should be drafted",
					"Product conversion doesn't drafted", "fail");

		}

		click(btn_releaseCommon);

		getInvObjReg().customizeLoadingDelay(header_releasedProductConversion, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedProductConversion)) {
			writeTestResults("Verify Product Conversion is released", "Product conversion should be released",
					"Product conversion successfully released", "pass");
		} else {
			writeTestResults("Verify Product Conversion is released", "Product conversion should be released",
					"Product conversion doesn't released", "fail");

		}

		click(btn_action);
		Thread.sleep(1000);
		if (isDisplayed(btn_reverseActionVrification)) {
			writeTestResults("Verify set of actions were loaded", "Set of actions should be loaded",
					"Set of actions successfully loaded", "pass");
		} else {
			writeTestResults("Verify set of actions were loaded", "Set of actions should be loaded",
					"Set of actions doesn't loaded", "fail");
		}
		click(btn_reverseActionVrification);
		JavascriptExecutor j7 = (JavascriptExecutor) driver;
		j7.executeScript("$(\"#txtrdcmnReverseDate\").datepicker(\"setDate\", new Date())");
		click(btn_reverseConfirmInboundShipmentActionVerification);
		if (isDisplayed(btn_reverseConfirationActionVerification)) {
			writeTestResults("Verify confirmation message is pop-up", "Confirmation message should pop-up",
					"Confirmation message successfully pop-up", "pass");
		} else {
			writeTestResults("Verify confirmation message is pop-up", "Confirmation message should pop-up",
					"Confirmation message doesn't pop-up", "fail");
		}
		click(btn_reverseConfirationActionVerification);
		Thread.sleep(3000);
		getInvObjReg().customizeLoadingDelay(header_reversedProductConversion, 50);
		if (isDisplayed(header_reversedProductConversion)) {
			writeTestResults("Verify user able to reverse Product Conversion",
					"User should be able to reverse Product Conversion", "User successfully reverse Product Conversion",
					"pass");
		} else {
			writeTestResults("Verify user able to reverse Product Conversion",
					"User should be able to reverse Product Conversion", "User doesn't reverse Product Conversion",
					"fail");

		}

	}

	/* IN_PC_042 */
	public void draftAndNewProductConversion_IN_PC_042() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, productConversionWarehouseRegression);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify user allow to fill mandatory fields", "User should allow to fill mandatory fields",
					"User allow to fill mandatory fields", "pass");
		} else {
			writeTestResults("Verify user allow to fill mandatory fields", "User should allow to fill mandatory fields",
					"User not allow to fill mandatory fields", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User allow to select \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User not allow to select \"From Product\"",
					"fail");
		}
		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), quantityFiveProductConversion);

		click(btn_serielBatchCaptureNosProductConversionRowReplace.replace("row", "1"));
		getInvObjReg().customizeLoadingDelayAndContinue(header_batchCaptureWindowProductConversion, 20);

		click(btn_seraialBatchCaptureDocIconProductConversion);

		getInvObjReg().customizeLoadingDelay(chk_selectBatchForBatchSpecificProduct, 20);
		click(chk_selectBatchForBatchSpecificProduct);

		Thread.sleep(2000);

		String capturedQuantityBatchSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantityBatchSpecific.equals(quantityFiveProductConversion)) {
			writeTestResults("Verify user allwo to capture batches for From Product",
					"User should allwo to capture batches for From Product",
					"User allwo to capture batches for From Product", "pass");
		} else {
			writeTestResults("Verify user allwo to capture batches for From Product",
					"User should allwo to capture batches for From Product",
					"User not allwo to capture batches for From Product", "fail");

		}

		click(btn_appalySerielBatchesFromProductProductConversion);

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User allow to select \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User not allow to select \"To Product\"", "fail");
		}

		sendKeys(txt_quantityToProductProductConversion, quantityToProductProductConversion);

		click(btn_serialNosFromProductConversionToProduc);
		getInvObjReg().customizeLoadingDelayAndContinue(header_serileCaptureWindowProductConversion, 20);

		if (isDisplayed(header_serileCaptureWindowProductConversion)) {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window successfully pop-up",
					"pass");
		} else {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window doesn't pop-up",
					"fail");
		}

		getInvObjReg().customizeLoadingDelay(chk_serielRangeProductConversion, 20);
		Thread.sleep(1500);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, quantityToProductProductConversion);

		String sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click(btn_date31ProductConversionToProduct);
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click(btn_date31ProductConversionToProduct);
		pressEnter(txt_lotNo);
		Thread.sleep(3000);

		String capturedQuantitySerielSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantitySerielSpecific.equals(quantityToProductProductConversion)) {
			writeTestResults("Verify user allwo to capture serials for To Product",
					"User should allwo to capture serials for To Product",
					"User allwo to capture serials for To Product", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serials for To Product",
					"User should allwo to capture serials for To Product",
					"User not allwo to capture serials for To Product", "fail");

		}

		click(btn_appalySerielBatchesFromProductProductConversion);

		String doc_date = getText(lbl_documentDate);
		getInvObj().writeRegression01("RecentlyDraftedProductConversionDate_IN_PC_042", doc_date, 174);

		click(btn_draftAndNewActionVerification);
		explicitWait(header_newProductConversion, 40);
		if (isDisplayed(header_newProductConversion)) {
			writeTestResults("Verify user can navigate to a new Product Conversion",
					"User should be navigate to a new Product Conversion",
					"User successfully navigate to a new Product Conversion", "pass");
		} else {
			writeTestResults("Verify user can navigate to a new Product Conversion",
					"User should be navigate to a new Product Conversion",
					"User doesn't navigate to a new Product Conversion", "fail");

		}
	}

	public void findRecentlyDraftedProductConversion_IN_PC_042() throws Exception {
		((JavascriptExecutor) driver).executeScript("window.open('about:blank','_blank');");
		switchWindow();
		openPage(siteURL);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}
		selectText(dropdown_templateFilter, myTemplate);
		click(btn_searchForms);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(lbl_recentlyDraftedProductConversion, 50);
		Thread.sleep(2000);
		String draftedProductConversion = getText(lbl_recentlyDraftedProductConversion);
		getInvObj().writeRegression01("RecentlyDraftedProductConversion_IN_PC_042", draftedProductConversion, 175);
	}

	public void draftAndReleseNewlyOpenProductConversion_IN_PC_042() throws Exception {
		switchWindow();
		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, productConversionWarehouseRegression);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify user allow to fill mandatory fields", "User should allow to fill mandatory fields",
					"User allow to fill mandatory fields", "pass");
		} else {
			writeTestResults("Verify user allow to fill mandatory fields", "User should allow to fill mandatory fields",
					"User not allow to fill mandatory fields", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User allow to select \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User not allow to select \"From Product\"",
					"fail");
		}
		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), quantityFiveProductConversion);

		click(btn_serielBatchCaptureNosProductConversionRowReplace.replace("row", "1"));
		getInvObjReg().customizeLoadingDelayAndContinue(header_batchCaptureWindowProductConversion, 20);

		click(btn_seraialBatchCaptureDocIconProductConversion);

		getInvObjReg().customizeLoadingDelay(chk_selectBatchForBatchSpecificProduct, 20);
		click(chk_selectBatchForBatchSpecificProduct);

		Thread.sleep(3000);

		String capturedQuantityBatchSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantityBatchSpecific.equals(quantityFiveProductConversion)) {
			writeTestResults("Verify user allwo to capture batches for From Product",
					"User should allwo to capture batches for From Product",
					"User allwo to capture batches for From Product", "pass");
		} else {
			writeTestResults("Verify user allwo to capture batches for From Product",
					"User should allwo to capture batches for From Product",
					"User not allwo to capture batches for From Product", "fail");

		}

		click(btn_applyLast);

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User allow to select \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User not allow to select \"To Product\"", "fail");
		}

		sendKeys(txt_quantityToProductProductConversion, quantityToProductProductConversion);

		click(btn_serialNosFromProductConversionToProduc);
		getInvObjReg().customizeLoadingDelayAndContinue(header_serileCaptureWindowProductConversion, 20);

		if (isDisplayed(header_serileCaptureWindowProductConversion)) {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window successfully pop-up",
					"pass");
		} else {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window doesn't pop-up",
					"fail");
		}

		getInvObjReg().customizeLoadingDelay(chk_serielRangeProductConversion, 20);
		Thread.sleep(1500);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, quantityToProductProductConversion);

		String sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click(btn_date31ProductConversionToProduct);
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click(btn_date31ProductConversionToProduct);
		pressEnter(txt_lotNo);
		Thread.sleep(3000);

		String capturedQuantitySerielSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantitySerielSpecific.equals(quantityToProductProductConversion)) {
			writeTestResults("Verify user allwo to capture serials for To Product",
					"User should allwo to capture serials for To Product",
					"User allwo to capture serials for To Product", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serials for To Product",
					"User should allwo to capture serials for To Product",
					"User not allwo to capture serials for To Product", "fail");

		}

		click(btn_applyLast);

		click(btn_draftActionVerification);

		getInvObjReg().customizeLoadingDelay(header_drftedProductConversion, 40);
		if (isDisplayed(header_drftedProductConversion)) {
			writeTestResults("Verify user allow to draft the Product Conversion",
					"User should allow to draft the Product Conversiont", "User allow to draft the Product Conversion",
					"pass");
		} else {
			writeTestResults("Verify user allow to draft the Product Conversion",
					"User should allow to draft the Product Conversiont",
					"User not allow to draft the Product Conversion", "fail");

		}

		click(btn_releaseCommon);

		getInvObjReg().customizeLoadingDelay(header_releasedProductConversion, 40);
		trackCode = getText(lbl_docNo);
		getInvObj().writeRegression01("ReleasedProductConversion_IN_PC_042", trackCode, 176);
		getInvObj().writeRegression01("ReleasedProductConversionURL_IN_PC_042", getCurrentUrl(), 177);

		if (isDisplayed(header_releasedProductConversion)) {
			writeTestResults("Verify Product Conversion is released", "Product conversion should be released",
					"Product conversion successfully released", "pass");
		} else {
			writeTestResults("Verify Product Conversion is released", "Product conversion should be released",
					"Product conversion doesn't released", "fail");

		}
	}

	public void findAnfReleaseDrftedProductConversion_IN_PC_042() throws Exception {
		switchWindow();
		sendKeys(txt_searchProductConversion,
				getInvObj().readIWRegression01("RecentlyDraftedProductConversion_IN_PC_042"));
		click(btn_searchForms);

		getInvObjReg().customizeLoadingDelayAndContinue(lnk_releventDocumentOnByPage.replace("doc_no",
				getInvObj().readIWRegression01("RecentlyDraftedProductConversion_IN_PC_042")), 50);
		click(lnk_releventDocumentOnByPage.replace("doc_no",
				getInvObj().readIWRegression01("RecentlyDraftedProductConversion_IN_PC_042")));

		getInvObjReg().customizeLoadingDelayAndContinue(header_draftedProductConversionReplaceDocNo.replace("doc_no",
				getInvObj().readIWRegression01("RecentlyDraftedProductConversion_IN_PC_042")), 50);

		boolean flagHeaderFirstDraftedProductConversion = false;
		boolean flagDateFirstDraftedProductConversion = false;
		if (isDisplayed(header_draftedProductConversionReplaceDocNo.replace("doc_no",
				getInvObj().readIWRegression01("RecentlyDraftedProductConversion_IN_PC_042")))) {
			flagHeaderFirstDraftedProductConversion = true;
		}

		String doc_date = getText(lbl_documentDate);
		if (doc_date.equals(getInvObj().readIWRegression01("RecentlyDraftedProductConversionDate_IN_PC_042"))) {
			flagDateFirstDraftedProductConversion = true;
		}

		if (flagHeaderFirstDraftedProductConversion && flagDateFirstDraftedProductConversion) {
			writeTestResults("Verify drafted Product Conversion is open", "Drafted Product Conversion should be open",
					"Drafted Product Conversion successfully open", "pass");
		} else {
			writeTestResults("Verify drafted Product Conversion is open", "Drafted Product Conversion should be open",
					"Drafted Product Conversion doesn't open", "fail");
		}

		click(btn_releaseCommon);
		getInvObjReg().customizeLoadingDelayAndContinue(header_documentReleasedStatus01, 30);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_documentReleasedStatus01)) {
			writeTestResults("Verify Product Conversion was released", "Product Conversion should be released",
					"Product Conversion successfully released", "pass");
		} else {
			writeTestResults("Verify Product Conversion was released", "Product Conversion should be released",
					"Product Conversion successfully released", "fail");
		}
	}

	/* IN_PC_043 */
	public void copyFromProductConversion_IN_PC_043() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		click(btn_copyFromCommon);
		Thread.sleep(1000);
		sendKeys(txt_searchProductConversion, getInvObj().readIWRegression01("ReleasedProductConversion_IN_PC_042"));
		pressEnter(txt_searchProductConversion);

		String result_pc_xpath = lnk_resultDocumentOnLookup.replace("doc_no",
				getInvObj().readIWRegression01("ReleasedProductConversion_IN_PC_042"));
		explicitWait(result_pc_xpath, 15);

		if (isDisplayed(result_pc_xpath)) {
			writeTestResults(
					"Verify relevent existing Product Conversion was loaded and user allow to select the particular Product Conversion",
					"Relevent existing Product Conversion should be loaded and user should allow to select the particular Product Conversion",
					"Relevent existing Product Conversion successfully loaded and user allow to select the particular Product Conversion",
					"pass");
		} else {
			writeTestResults(
					"Verify relevent existing Product Conversion was loaded and user allow to select the particular Product Conversion",
					"Relevent existing Product Conversion should be loaded and user should allow to select the particular Product Conversion",
					"Relevent existing Product Conversion doesn't loaded and user not allow to select the particular Product Conversion",
					"fail");
		}

		doubleClick(result_pc_xpath);

		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify warehouse is selected according to the copy from document",
					"Warehouse should be selected according to the copy from document",
					"Warehouse successfully selected according to the copy from document", "pass");
		} else {
			writeTestResults("Verify warehouse is selected according to the copy from document",
					"Warehouse should be selected according to the copy from document",
					"Warehouse doesn't selected according to the copy from document", "fail");
		}

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify from product is available according to the copy from document",
					"From product should be available according to the copy from document",
					"From product successfully available according to the copy from document", "pass");
		} else {
			writeTestResults("Verify from product is available according to the copy from document",
					"From product should be available according to the copy from document",
					"From product doesn't available according to the copy from document", "fail");

		}

		String fromQty = getAttribute(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), "value");
		if (fromQty.equals(quantityFiveProductConversion)) {
			writeTestResults("Verify from qty is available according to the copy from document",
					"From qty should be available according to the copy from document",
					"From qty successfully available according to the copy from document", "pass");
		} else {
			writeTestResults("Verify from qty is available according to the copy from document",
					"From qty should be available according to the copy from document",
					"From qty doesn't available according to the copy from document", "fail");

		}

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify to product is available according to the copy from document",
					"To product should be available according to the copy from document",
					"To product successfully available according to the copy from document", "pass");
		} else {
			writeTestResults("Verify to product is available according to the copy from document",
					"To product should be available according to the copy from document",
					"To product doesn't available according to the copy from document", "fail");

		}

		String toQty = getAttribute(txt_quantityToProductProductConversion.replace("row", "1"), "value");
		if (toQty.equals(quantityTenProductConversion)) {
			writeTestResults("Verify to qty is available according to the copy from document",
					"To qty should be available according to the copy from document",
					"To qty successfully available according to the copy from document", "pass");
		} else {
			writeTestResults("Verify to qty is available according to the copy from document",
					"To qty should be available according to the copy from document",
					"To qty doesn't available according to the copy from document", "fail");

		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), quantityTenProductConversion);

		sendKeys(txt_quantityToProductProductConversion, quantityFiveProductConversion);

		/* From product capture */
		click(btn_serielBatchCaptureNosProductConversionRowReplace.replace("row", "1"));
		getInvObjReg().customizeLoadingDelayAndContinue(header_batchCaptureWindowProductConversion, 20);

		click(btn_seraialBatchCaptureDocIconProductConversion);

		getInvObjReg().customizeLoadingDelay(chk_selectBatchForBatchSpecificProduct, 20);
		click(chk_selectBatchForBatchSpecificProduct);

		Thread.sleep(2000);

		String capturedQuantityBatchSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantityBatchSpecific.equals(quantityTenProductConversion)) {
			writeTestResults("Verify user allwo to capture batches for From Product",
					"User should allwo to capture batches for From Product",
					"User allwo to capture batches for From Product", "pass");
		} else {
			writeTestResults("Verify user allwo to capture batches for From Product",
					"User should allwo to capture batches for From Product",
					"User not allwo to capture batches for From Product", "fail");

		}

		click(btn_appalySerielBatchesFromProductProductConversion);

		/* To product capture */
		click(btn_serialNosFromProductConversionToProduc);
		getInvObjReg().customizeLoadingDelayAndContinue(header_serileCaptureWindowProductConversion, 20);

		if (isDisplayed(header_serileCaptureWindowProductConversion)) {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window successfully pop-up",
					"pass");
		} else {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window doesn't pop-up",
					"fail");
		}

		getInvObjReg().customizeLoadingDelay(chk_serielRangeProductConversion, 20);
		Thread.sleep(1500);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, quantityFiveProductConversion);

		String sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click(btn_date31ProductConversionToProduct);
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click(btn_date31ProductConversionToProduct);
		pressEnter(txt_lotNo);
		Thread.sleep(3000);

		String capturedQuantitySerielSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantitySerielSpecific.equals(quantityFiveProductConversion)) {
			writeTestResults("Verify user allwo to capture serials for To Product",
					"User should allwo to capture serials for To Product",
					"User allwo to capture serials for To Product", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serials for To Product",
					"User should allwo to capture serials for To Product",
					"User not allwo to capture serials for To Product", "fail");

		}

		click(btn_appalySerielBatchesFromProductProductConversion);

		click(btn_draftActionVerification);

		getInvObjReg().customizeLoadingDelay(header_drftedProductConversion, 40);
		if (isDisplayed(header_drftedProductConversion)) {
			writeTestResults("Verify user allow to draft the Product Conversion with changes",
					"User should allow to draft the Product Conversiont with changes",
					"User allow to draft the Product Conversion with changes", "pass");
		} else {
			writeTestResults("Verify user allow to draft the Product Conversion with changes",
					"User should allow to draft the Product Conversiont with changes",
					"User not allow to draft the Product Conversion with changes", "fail");

		}

		click(btn_releaseCommon);

		getInvObjReg().customizeLoadingDelay(header_releasedProductConversion, 40);
		trackCode = getText(lbl_docNo);

		if (isDisplayed(header_releasedProductConversion)) {
			writeTestResults("Verify Product Conversion is released with changes",
					"Product conversion should be released with changes",
					"Product conversion successfully released with changes", "pass");
		} else {
			writeTestResults("Verify Product Conversion is released with changes",
					"Product conversion should be released with changes",
					"Product conversion doesn't released with changes", "fail");

		}

	}

	/* IN_PC_044 */
	public void draftProductConversion_IN_PC_044() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, productConversionWarehouseRegression);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify user allow to fill mandatory fields", "User should allow to fill mandatory fields",
					"User allow to fill mandatory fields", "pass");
		} else {
			writeTestResults("Verify user allow to fill mandatory fields", "User should allow to fill mandatory fields",
					"User not allow to fill mandatory fields", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User allow to select \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User not allow to select \"From Product\"",
					"fail");
		}
		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), quantityFiveProductConversion);

		click(btn_serielBatchCaptureNosProductConversionRowReplace.replace("row", "1"));
		getInvObjReg().customizeLoadingDelayAndContinue(header_batchCaptureWindowProductConversion, 20);

		click(btn_seraialBatchCaptureDocIconProductConversion);

		getInvObjReg().customizeLoadingDelay(chk_selectBatchForBatchSpecificProduct, 20);
		click(chk_selectBatchForBatchSpecificProduct);

		Thread.sleep(3000);

		String capturedQuantityBatchSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantityBatchSpecific.equals(quantityFiveProductConversion)) {
			writeTestResults("Verify user allwo to capture batches for From Product",
					"User should allwo to capture batches for From Product",
					"User allwo to capture batches for From Product", "pass");
		} else {
			writeTestResults("Verify user allwo to capture batches for From Product",
					"User should allwo to capture batches for From Product",
					"User not allwo to capture batches for From Product", "fail");

		}

		click(btn_applyLast);

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User allow to select \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User not allow to select \"To Product\"", "fail");
		}

		sendKeys(txt_quantityToProductProductConversion, quantityToProductProductConversion);

		click(btn_serialNosFromProductConversionToProduc);
		getInvObjReg().customizeLoadingDelayAndContinue(header_serileCaptureWindowProductConversion, 20);

		if (isDisplayed(header_serileCaptureWindowProductConversion)) {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window successfully pop-up",
					"pass");
		} else {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window doesn't pop-up",
					"fail");
		}

		getInvObjReg().customizeLoadingDelay(chk_serielRangeProductConversion, 20);
		Thread.sleep(1500);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, quantityToProductProductConversion);

		String sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click(btn_date31ProductConversionToProduct);
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click(btn_date31ProductConversionToProduct);
		pressEnter(txt_lotNo);
		Thread.sleep(3000);

		String capturedQuantitySerielSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantitySerielSpecific.equals(quantityToProductProductConversion)) {
			writeTestResults("Verify user allwo to capture serials for To Product",
					"User should allwo to capture serials for To Product",
					"User allwo to capture serials for To Product", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serials for To Product",
					"User should allwo to capture serials for To Product",
					"User not allwo to capture serials for To Product", "fail");

		}

		click(btn_applyLast);

		click(btn_draftActionVerification);

	}

	public void duplicateDraftedProductConversion_IN_PC_044() throws Exception {
		explicitWait(btn_duplicateActionVerification, 40);
		click(btn_duplicateActionVerification);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify warehouse is selected according to the duplicated document",
					"Warehouse should be selected according to the duplicated document",
					"Warehouse successfully selected according to the duplicated document", "pass");
		} else {
			writeTestResults("Verify warehouse is selected according to the duplicated document",
					"Warehouse should be selected according to the duplicated document",
					"Warehouse doesn't selected according to the duplicated document", "fail");
		}

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify from product is available according to the duplicated document",
					"From product should be available according to the duplicated document",
					"From product successfully available according to the duplicated document", "pass");
		} else {
			writeTestResults("Verify from product is available according to the duplicated document",
					"From product should be available according to the duplicated document",
					"From product doesn't available according to the duplicated document", "fail");

		}

		String fromQty = getAttribute(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), "value");
		if (fromQty.equals(quantityFiveProductConversion)) {
			writeTestResults("Verify from qty is available according to the duplicated document",
					"From qty should be available according to the duplicated document",
					"From qty successfully available according to the duplicated document", "pass");
		} else {
			writeTestResults("Verify from qty is available according to the duplicated document",
					"From qty should be available according to the duplicated document",
					"From qty doesn't available according to the duplicated document", "fail");

		}

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify to product is available according to the duplicated document",
					"To product should be available according to the duplicated document",
					"To product successfully available according to the duplicated document", "pass");
		} else {
			writeTestResults("Verify to product is available according to the duplicated document",
					"To product should be available according to the duplicated document",
					"To product doesn't available according to the duplicated document", "fail");

		}

		String toQty = getAttribute(txt_quantityToProductProductConversion.replace("row", "1"), "value");
		if (toQty.equals(quantityTenProductConversion)) {
			writeTestResults("Verify to qty is available according to the duplicated document",
					"To qty should be available according to the duplicated document",
					"To qty successfully available according to the duplicated document", "pass");
		} else {
			writeTestResults("Verify to qty is available according to the duplicated document",
					"To qty should be available according to the duplicated document",
					"To qty doesn't available according to the duplicated document", "fail");

		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), quantityTenProductConversion);

		sendKeys(txt_quantityToProductProductConversion, quantityFiveProductConversion);

		/* From product capture */
		click(btn_serielBatchCaptureNosProductConversionRowReplace.replace("row", "1"));
		getInvObjReg().customizeLoadingDelayAndContinue(header_batchCaptureWindowProductConversion, 20);

		click(btn_seraialBatchCaptureDocIconProductConversion);

		getInvObjReg().customizeLoadingDelay(chk_selectBatchForBatchSpecificProduct, 20);
		click(chk_selectBatchForBatchSpecificProduct);

		Thread.sleep(2000);

		String capturedQuantityBatchSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantityBatchSpecific.equals(quantityTenProductConversion)) {
			writeTestResults("Verify user allwo to capture batches for From Product",
					"User should allwo to capture batches for From Product",
					"User allwo to capture batches for From Product", "pass");
		} else {
			writeTestResults("Verify user allwo to capture batches for From Product",
					"User should allwo to capture batches for From Product",
					"User not allwo to capture batches for From Product", "fail");

		}

		click(btn_appalySerielBatchesFromProductProductConversion);

		/* To product capture */
		click(btn_serialNosFromProductConversionToProduc);
		getInvObjReg().customizeLoadingDelayAndContinue(header_serileCaptureWindowProductConversion, 20);

		if (isDisplayed(header_serileCaptureWindowProductConversion)) {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window successfully pop-up",
					"pass");
		} else {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window doesn't pop-up",
					"fail");
		}

		getInvObjReg().customizeLoadingDelay(chk_serielRangeProductConversion, 20);
		Thread.sleep(1500);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, quantityFiveProductConversion);

		String sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click(btn_date31ProductConversionToProduct);
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click(btn_date31ProductConversionToProduct);
		pressEnter(txt_lotNo);
		Thread.sleep(3000);

		String capturedQuantitySerielSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantitySerielSpecific.equals(quantityFiveProductConversion)) {
			writeTestResults("Verify user allwo to capture serials for To Product",
					"User should allwo to capture serials for To Product",
					"User allwo to capture serials for To Product", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serials for To Product",
					"User should allwo to capture serials for To Product",
					"User not allwo to capture serials for To Product", "fail");

		}

		click(btn_appalySerielBatchesFromProductProductConversion);

		click(btn_draftActionVerification);

		getInvObjReg().customizeLoadingDelay(header_drftedProductConversion, 40);
		if (isDisplayed(header_drftedProductConversion)) {
			writeTestResults("Verify user allow to draft the Product Conversion",
					"User should allow to draft the Product Conversiont", "User allow to draft the Product Conversion",
					"pass");
		} else {
			writeTestResults("Verify user allow to draft the Product Conversion",
					"User should allow to draft the Product Conversiont",
					"User not allow to draft the Product Conversion", "fail");

		}

		click(btn_releaseCommon);

		getInvObjReg().customizeLoadingDelay(header_releasedProductConversion, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedProductConversion)) {
			writeTestResults("Verify Product Conversion is released", "Product conversion should be released",
					"Product conversion successfully released", "pass");
		} else {
			writeTestResults("Verify Product Conversion is released", "Product conversion should be released",
					"Product conversion doesn't released", "fail");

		}

	}

	/* IN_PC_045_046 */
	public void duplicateReleasedProductConversionAndCheckJournelEntry_IN_PC_045_046()
			throws InvalidFormatException, IOException, Exception {
		getInvObjReg2().loginBiletaGBV();
		openPage(getInvObj().readIWRegression01("ReleasedProductConversionURL_IN_PC_042"));
		explicitWait(btn_duplicateActionVerification, 40);
		click(btn_duplicateActionVerification);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify warehouse is selected according to the duplicated document",
					"Warehouse should be selected according to the duplicated document",
					"Warehouse successfully selected according to the duplicated document", "pass");
		} else {
			writeTestResults("Verify warehouse is selected according to the duplicated document",
					"Warehouse should be selected according to the duplicated document",
					"Warehouse doesn't selected according to the duplicated document", "fail");
		}

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify from product is available according to the duplicated document",
					"From product should be available according to the duplicated document",
					"From product successfully available according to the duplicated document", "pass");
		} else {
			writeTestResults("Verify from product is available according to the duplicated document",
					"From product should be available according to the duplicated document",
					"From product doesn't available according to the duplicated document", "fail");

		}

		String fromQty = getAttribute(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), "value");
		if (fromQty.equals(quantityFiveProductConversion)) {
			writeTestResults("Verify from qty is available according to the duplicated document",
					"From qty should be available according to the duplicated document",
					"From qty successfully available according to the duplicated document", "pass");
		} else {
			writeTestResults("Verify from qty is available according to the duplicated document",
					"From qty should be available according to the duplicated document",
					"From qty doesn't available according to the duplicated document", "fail");

		}

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify to product is available according to the duplicated document",
					"To product should be available according to the duplicated document",
					"To product successfully available according to the duplicated document", "pass");
		} else {
			writeTestResults("Verify to product is available according to the duplicated document",
					"To product should be available according to the duplicated document",
					"To product doesn't available according to the duplicated document", "fail");

		}

		String toQty = getAttribute(txt_quantityToProductProductConversion.replace("row", "1"), "value");
		if (toQty.equals(quantityTenProductConversion)) {
			writeTestResults("Verify to qty is available according to the duplicated document",
					"To qty should be available according to the duplicated document",
					"To qty successfully available according to the duplicated document", "pass");
		} else {
			writeTestResults("Verify to qty is available according to the duplicated document",
					"To qty should be available according to the duplicated document",
					"To qty doesn't available according to the duplicated document", "fail");

		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), quantityTenProductConversion);

		sendKeys(txt_quantityToProductProductConversion, quantityFiveProductConversion);

		/* From product capture */
		click(btn_serielBatchCaptureNosProductConversionRowReplace.replace("row", "1"));
		getInvObjReg().customizeLoadingDelayAndContinue(header_batchCaptureWindowProductConversion, 20);

		click(btn_seraialBatchCaptureDocIconProductConversion);

		getInvObjReg().customizeLoadingDelay(chk_selectBatchForBatchSpecificProduct, 20);
		click(chk_selectBatchForBatchSpecificProduct);

		Thread.sleep(2000);

		String capturedQuantityBatchSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantityBatchSpecific.equals(quantityTenProductConversion)) {
			writeTestResults("Verify user allwo to capture batches for From Product",
					"User should allwo to capture batches for From Product",
					"User allwo to capture batches for From Product", "pass");
		} else {
			writeTestResults("Verify user allwo to capture batches for From Product",
					"User should allwo to capture batches for From Product",
					"User not allwo to capture batches for From Product", "fail");

		}

		click(btn_appalySerielBatchesFromProductProductConversion);

		/* To product capture */
		click(btn_serialNosFromProductConversionToProduc);
		getInvObjReg().customizeLoadingDelayAndContinue(header_serileCaptureWindowProductConversion, 20);

		if (isDisplayed(header_serileCaptureWindowProductConversion)) {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window successfully pop-up",
					"pass");
		} else {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window doesn't pop-up",
					"fail");
		}

		getInvObjReg().customizeLoadingDelay(chk_serielRangeProductConversion, 20);
		Thread.sleep(1500);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, quantityFiveProductConversion);

		String sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click(btn_date31ProductConversionToProduct);
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click(btn_date31ProductConversionToProduct);
		pressEnter(txt_lotNo);
		Thread.sleep(3000);

		String capturedQuantitySerielSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantitySerielSpecific.equals(quantityFiveProductConversion)) {
			writeTestResults("Verify user allwo to capture serials for To Product",
					"User should allwo to capture serials for To Product",
					"User allwo to capture serials for To Product", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serials for To Product",
					"User should allwo to capture serials for To Product",
					"User not allwo to capture serials for To Product", "fail");

		}

		click(btn_appalySerielBatchesFromProductProductConversion);

		click(btn_draftActionVerification);

		getInvObjReg().customizeLoadingDelay(header_drftedProductConversion, 40);
		if (isDisplayed(header_drftedProductConversion)) {
			writeTestResults("Verify user allow to draft the Product Conversion",
					"User should allow to draft the Product Conversiont", "User allow to draft the Product Conversion",
					"pass");
		} else {
			writeTestResults("Verify user allow to draft the Product Conversion",
					"User should allow to draft the Product Conversiont",
					"User not allow to draft the Product Conversion", "fail");

		}

		click(btn_releaseCommon);

		getInvObjReg().customizeLoadingDelay(header_releasedProductConversion, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedProductConversion)) {
			writeTestResults("Verify Product Conversion is released", "Product conversion should be released",
					"Product conversion successfully released", "pass");
		} else {
			writeTestResults("Verify Product Conversion is released", "Product conversion should be released",
					"Product conversion doesn't released", "fail");

		}

		getInvObjReg().customizeLoadingDelay(btn_action, 40);
		click(btn_action);
		getInvObjReg().customizeLoadingDelay(btn_journelActionVerification, 10);
		if (isDisplayed(btn_journelActionVerification)) {
			writeTestResults("Verify set of actions are display", "Set of actions should display",
					"Set of actions successfully display", "pass");
		} else {
			writeTestResults("Verify set of actions are display", "Set of actions should display",
					"Set of actions doesn't display", "fail");

		}
		click(btn_journelActionVerification);
		getInvObjReg().customizeLoadingDelay(header_journelEntryDialogBox, 25);
		if (isDisplayed(header_journelEntryDialogBox)) {
			writeTestResults("Verify Journal Entry details window is pop-up",
					"Journal Entry details window should pop-up", "Journal Entry details window successfully pop-up",
					"pass");
		} else {
			writeTestResults("Verify Journal Entry details window is pop-up",
					"Journal Entry details window should pop-up", "Journal Entry details window doesn't pop-up",
					"fail");

		}
		String debitValue = getText(lbl_debitTotalJournelEntry);
		String creditValue = getText(lbl_creditTotalJournelEntry);
		if (debitValue.equals(creditValue)) {
			writeTestResults("Verify journals of the Product Conversion were corrected",
					"Journals of the Product Conversion should be corrected",
					"Journals of the Product Conversion were corrected", "pass");
		} else {
			writeTestResults("Verify journals of the Product Conversion were corrected",
					"Journals of the Product Conversion should be corrected",
					"Journals of the Product Conversion weren't corrected", "fail");

		}
	}

	/* IN_PC_047 */
	public void releaseProductConversionForVerifyHistory_IN_PC_047() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, productConversionWarehouseRegression);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify user allow to fill mandatory fields", "User should allow to fill mandatory fields",
					"User allow to fill mandatory fields", "pass");
		} else {
			writeTestResults("Verify user allow to fill mandatory fields", "User should allow to fill mandatory fields",
					"User not allow to fill mandatory fields", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User allow to select \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User not allow to select \"From Product\"",
					"fail");
		}
		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), quantityFiveProductConversion);

		click(btn_serielBatchCaptureNosProductConversionRowReplace.replace("row", "1"));
		getInvObjReg().customizeLoadingDelayAndContinue(header_batchCaptureWindowProductConversion, 20);

		click(btn_seraialBatchCaptureDocIconProductConversion);

		getInvObjReg().customizeLoadingDelay(chk_selectBatchForBatchSpecificProduct, 20);
		click(chk_selectBatchForBatchSpecificProduct);

		Thread.sleep(3000);

		String capturedQuantityBatchSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantityBatchSpecific.equals(quantityFiveProductConversion)) {
			writeTestResults("Verify user allwo to capture batches for From Product",
					"User should allwo to capture batches for From Product",
					"User allwo to capture batches for From Product", "pass");
		} else {
			writeTestResults("Verify user allwo to capture batches for From Product",
					"User should allwo to capture batches for From Product",
					"User not allwo to capture batches for From Product", "fail");

		}

		click(btn_applyLast);

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User allow to select \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User not allow to select \"To Product\"", "fail");
		}

		sendKeys(txt_quantityToProductProductConversion, quantityToProductProductConversion);

		click(btn_serialNosFromProductConversionToProduc);
		getInvObjReg().customizeLoadingDelayAndContinue(header_serileCaptureWindowProductConversion, 20);

		if (isDisplayed(header_serileCaptureWindowProductConversion)) {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window successfully pop-up",
					"pass");
		} else {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window doesn't pop-up",
					"fail");
		}

		getInvObjReg().customizeLoadingDelay(chk_serielRangeProductConversion, 20);
		Thread.sleep(1500);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, quantityToProductProductConversion);

		String sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click(btn_date31ProductConversionToProduct);
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click(btn_date31ProductConversionToProduct);
		pressEnter(txt_lotNo);
		Thread.sleep(3000);

		String capturedQuantitySerielSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantitySerielSpecific.equals(quantityToProductProductConversion)) {
			writeTestResults("Verify user allwo to capture serials for To Product",
					"User should allwo to capture serials for To Product",
					"User allwo to capture serials for To Product", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serials for To Product",
					"User should allwo to capture serials for To Product",
					"User not allwo to capture serials for To Product", "fail");

		}

		click(btn_applyLast);

		click(btn_draftActionVerification);
		explicitWait(btn_editActionVerification, 30);
		click(btn_editActionVerification);
		explicitWait(btn_updateActionVerification, 30);

		sendKeys(txt_quantityToProductProductConversion, "5");
		click(btn_serialNosFromProductConversionToProduc);
		getInvObjReg().customizeLoadingDelayAndContinue(header_serileCaptureWindowProductConversion, 20);

		click(btn_deleteLastRowOfTheGrid);
		click(btn_deleteLastRowOfTheGrid);
		click(btn_deleteLastRowOfTheGrid);
		click(btn_deleteLastRowOfTheGrid);
		click(btn_deleteLastRowOfTheGrid);

		click(btn_applyLast);
		click(btn_updateActionVerification);
		explicitWait(btn_releaseCommon, 30);
		click(btn_releaseCommon);
		explicitWait(header_releasedProductConversion, 40);

		if (isDisplayed(header_releasedProductConversion)) {
			writeTestResults("Verify Product Conversion is released", "Product Conversion should be released",
					"Product Conversion successfully released", "pass");
		} else {
			writeTestResults("Verify Product Conversion is released", "Product Conversion should be released",
					"Product Conversion successfully released", "fail");
		}

		getInvObjReg().customizeLoadingDelay(btn_action, 20);
		click(btn_action);
		if (isDisplayed(btn_historyActionVerification)) {
			writeTestResults("Verify history option was loaded under action",
					"History option should be loaded under action", "History option successfully loaded under action",
					"pass");
		} else {
			writeTestResults("Verify history option was loaded under action",
					"History option should be loaded under action", "History option successfully loaded under action",
					"fail");

		}

		click(btn_historyActionVerification);
		getInvObjReg().customizeLoadingDelay(div_historyTableActionVerification, 20);
		if (isDisplayed(row_relesedDetailsHistory) && isDisplayed(row_draftDetailsHistory)
				&& isDisplayed(row_updateDetailsHistory)) {
			writeTestResults("Verify history details are visible relevant to the Product Conversion",
					"History details should visible relevant to the Product Conversion",
					"History details successfully visible relevant to the Product Conversion", "pass");
		} else {
			writeTestResults("Verify history details are visible relevant to the Product Conversion",
					"History details should visible relevant to the Product Conversion",
					"History details are not visible relevant to the Product Conversion", "fail");
		}

	}

	/* IN_PC_048 */
	public void stockAdjustmentForNewWarehouse_IN_PC_048() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().createNewWarehouse();
		invObj.customizeLoadingDelay(navigation_pane, 50);
		Thread.sleep(3000);
		invObj.customizeLoadingDelay(navigation_pane, 20);
		click(navigation_pane);
		invObj.customizeLoadingDelay(inventory_module, 40);
		click(inventory_module);
		invObj.customizeLoadingDelay(btn_stockAdjustment, 40);
		click(btn_stockAdjustment);
		invObj.customizeLoadingDelay(btn_newStockAdjustment, 40);
		click(btn_newStockAdjustment);

		sendKeys(txt_descStockAdj, descStockAdj);
		Thread.sleep(1000);
		selectText(dropdown_warehouseStockAdj, getInvObj().readIWRegression01("NewlyCreatedWarehouse") + " [Negombo]");
		Thread.sleep(1500);

		/* Product add to the grid */
		for (int i = 1; i < 3; i++) {
			String str_i = String.valueOf(i);
			click(btn_productLookupStockAdjForLoop.replace("row", str_i));
			Thread.sleep(3000);
			invObj.customizeLoadingDelay(txt_productSearch, 15);

			String product = "";

			if (i == 1) {
				product = "Lot";
			} else if (i == 2) {
				product = "BatchFifo";
			} else {
				System.out.println("Product type cannt identified.........");
			}

			invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation(product));
			pressEnter(txt_productSearch);
			String product_path = result_cpmmonProduct.replace("product", invObj.readTestCreation(product));
			invObj.customizeLoadingDelay(product_path, 30);
			doubleClick(product_path);
			Thread.sleep(1500);
			String txt_qty = txt_qtyProductGridStockAdjustment.replace("row", str_i);
			String txt_cost = txt_costProductGridStockAdjustment.replace("row", str_i);

			sendKeys(txt_qty, productQuantity1);

			sendKeys(txt_cost, productCost1);
			invObj.sleepCusomized(btn_avilabilityCheckWidget);

			invObj.sleepCusomized(btn_addNewRow);
			click(btn_addNewRow);
		}

		click(btn_deleteLastRowOfTheGrid);

		click(btn_draftActionVerification);
		invObj.customizeLoadingDelay(brn_serielBatchCapture, 40);
		click(brn_serielBatchCapture);

		/* Product 02 */
		Thread.sleep(3000);
		String item_xpath = btn_itemProductCapturePage.replace("_product_number", "2");
		explicitWait(item_xpath, 15);
		click(item_xpath);

		sendKeys(txt_batchNumberCaptureILOOutboundShipment, invObj.genBatchFifoSerielNumber(200));
		sendKeys(txt_lotNumberStockAdjustment,
				invObj.genProductId() + invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_expiryDateStockAdjustmentProductCapture);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_lotNumberStockAdjustment);
		Thread.sleep(1000);
		invObj.customizeLoadingDelay(btn_updateCapture, 50);
		click(btn_updateCapture);
		invObj.customizeLoadingDelay((item_xpath + span_afterCaptureSerialGreen), 30);
		click(btn_productCaptureBackButton);
		Thread.sleep(1000);
		click(btn_relese);
		invObj.customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 60);
	}

	public void productConversion_IN_PC_048() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, getInvObj().readIWRegression01("NewlyCreatedWarehouse") + " [Negombo]");
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(getInvObj().readIWRegression01("NewlyCreatedWarehouse") + " [Negombo]")) {
			writeTestResults("Verify user can select warehouse only having converted serials",
					"User should be able to select warehouse only having converted serials",
					"User successfully select warehouse only having converted serials", "pass");
		} else {
			writeTestResults("Verify user can select warehouse only having converted serials",
					"User should be able to select warehouse only having converted serials",
					"User doesn't select warehouse only having converted serials", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("Lot"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(result_cpmmonProduct.replace("product", invObj.readTestCreation("Lot")),
				20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("Lot")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("Lot")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("product", invObj.readTestCreation("Lot")))) {
			writeTestResults("Verify user allow to select Lot product as \"From Product\"",
					"User should allow to select Lot product as \"From Product\"",
					"User allow to select Lot product as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select Lot product as \"From Product\"",
					"User should allow to select Lot product as \"From Product\"",
					"User not allow to select Lot product as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select Batch Specific product as \"To Product\"",
					"User should allow to select Batch Specific product as \"To Product\"",
					"User allow to select Batch Specific product as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select Batch Specific product as \"To Product\"",
					"User should allow to select Batch Specific product as \"To Product\"",
					"User not allow to select Batch Specific product as \"To Product\"", "fail");
		}

		sendKeys(txt_quantityToProductProductConversion, "5");

		click(btn_addNewRowProductConversion);

		click(btn_productLookupRowReplaceProductConversion.replace("row", "2"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchFifo")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchFifo")))) {
			writeTestResults("Verify user allow to select batch FIFO as \"From Product\"",
					"User should allow to select batch FIFO as \"From Product\"",
					"User allow to select batch FIFO as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select batch FIFO as \"From Product\"",
					"User should allow to select batch FIFO as \"From Product\"",
					"User not allow to select batch FIFO as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "2"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "2"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify user allow to select seriel specific as \"To Product\"",
					"User should allow to select seriel specific as \"To Product\"",
					"User allow to select seriel specific as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select seriel specific as \"To Product\"",
					"User should allow to select seriel specific as \"To Product\"",
					"User not allow to select seriel specific as \"To Product\"", "fail");
		}

		sendKeys(txt_qtyToProductProductConcersionRowReplace.replace("row", "2"), "5");

		/* capture row 01 */
		explicitWait(btn_serleNosProductConversionToProduct.replace("row", "1"), 10);
		click(btn_serleNosProductConversionToProduct.replace("row", "1"));
		explicitWait(txt_batchNoToProductProductConversion, 20);
		sendKeys(txt_batchNoToProductProductConversion,
				invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_expiryDateToProductProductConversion);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		sendKeys(txt_lotNoToProductProductConversion,
				invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_menufacDateToProductProductConversion);
		Thread.sleep(200);
		doubleClick(btn_calenderBack);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_lotNoToProductProductConversion);
		Thread.sleep(2000);
		click(btn_commoApplyLast);

		/* capture row 02 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "2"));
		explicitWait(btn_seraialBatchCaptureDocIconProductConversion, 10);
		click(btn_seraialBatchCaptureDocIconProductConversion);
		explicitWait(chk_batchSelectionProductConversion, 10);
		click(chk_batchSelectionProductConversion);
		click(btn_commoApplyLast);

		click(btn_serleNosProductConversionToProduct.replace("row", "2"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		String sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		getInvObj().writeRegression01("SN_IN_PC_048", sn, 189);
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		explicitWait(btn_draftActionVerification, 30);
		click(btn_draftActionVerification);

		getInvObjReg().customizeLoadingDelay(header_drftedProductConversion, 40);

		click(btn_releaseCommon);
		explicitWait(header_releasedProductConversion, 30);
		if (isDisplayed(header_releasedProductConversion)) {
			writeTestResults("Verify user can release Product Conversion",
					"User should be able to release Product Conversion", "User successfully release Product Conversion",
					"pass");
		} else {
			writeTestResults("Verify user can release Product Conversion",
					"User should be able to release Product Conversion", "User doesn't release Product Conversion",
					"fail");
		}
	}

	public void internalOrderAndInternalDispatchOrder_IN_PC_048() throws Exception {
		explicitWait(navigation_pane, 20);
		Thread.sleep(2000);
		explicitWait(navigation_pane, 20);
		click(navigation_pane);
		explicitWait(inventory_module, 10);
		click(inventory_module);
		click(btn_internelOrder);
		click(btn_newInternelOrder);
		explicitWait(btn_employeeRequestJourneyInternelOrder, 20);
		click(btn_employeeRequestJourneyInternelOrder);
		sendKeys(txt_tiltleInternelOrder, titleInternelOrder);

		click(btn_searchRequester);
		invObj.handeledSendKeys(txt_RequesterSearch, employee01);
		pressEnter(txt_RequesterSearch);
		explicitWait(lnk_commonResultEmployee.replace("emp_code", employee01), 30);
		doubleClick(lnk_commonResultEmployee.replace("emp_code", employee01));
		Thread.sleep(1300);
		/* Product add to the grid */
		for (int i = 1; i < 3; i++) {
			String str_i = String.valueOf(i);

			String lookup_path = btn_productLookupInterDeprtmentTransferOrderForLoop.replace("row", str_i)
					.replace("column", "10").replace("tblStockAdj", "tblInternalRequest");
			click(lookup_path);
			String product = "";

			if (i == 1) {
				product = "BatchSpecific";
			} else if (i == 2) {
				product = "SerielSpecific";
			} else {
				System.out.println("Product type cannot identified.........");
			}
			invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation(product));
			pressEnter(txt_productSearch);
			explicitWait(result_cpmmonProduct.replace("product", invObj.readTestCreation(product)), 20);
			Thread.sleep(1000);
			doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation(product)));
			Thread.sleep(1500);

			String txt_qty = txt_quantityInGrid.replace("row", str_i).replace("tblName", "tblInternalRequest");

			explicitWait(txt_qty, 7);
			sendKeys(txt_qty, "5");

			explicitWait(btn_addNewRow, 10);
			click(btn_addNewRow);
		}

		click(btn_deleteLastRowOfTheGrid);

		click(btn_detailTab);
		selectText(drop_warehouseInternelOrderEmployeeRequest,
				getInvObj().readIWRegression01("NewlyCreatedWarehouse") + " [Negombo]");

		click(btn_draftInternelOrder);
		Thread.sleep(3000);
		trackCode = getText(lbl_creationsDocNo);
		explicitWait(btn_relese, 15);
		click(btn_relese);
		explicitWait(btn_goToPageLink, 25);
		click(btn_goToPageLink);

		/* Internal Dispatch Order */
		switchWindow();
		explicitWait(btn_draftInternelDispatchOrder, 20);
		click(btn_draftInternelDispatchOrder);
		explicitWait(brn_serielBatchCapture, 20);
		click(brn_serielBatchCapture);

		/* Product 01 */
		explicitWait(btn_captureItem1, 20);
		click(btn_captureItem1);
		Thread.sleep(1000);

		click(btn_capturePageDocIcon);

		click(chk_availableProductsTransferOrderOutboundShipment.replace("row", "1"));

		String qty = getText(lbl_qtyBatchSpecifificCapturePage);
		String qty_capture = getText(lbl_availQuantityProductCapturuPage);

		int qty_int = Integer.parseInt(qty);
		int qty_capture_int = Integer.parseInt(qty_capture);

		int i = 2;
		while (qty_int > qty_capture_int) {
			String i_str = String.valueOf(i);
			click(chk_deleteItemCapturePage);
			click(btn_deleteRowProductCaptururePage);
			click(btn_capturePageDocIcon);
			click(chk_availableProductsTransferOrderOutboundShipment.replace("row", i_str));
			Thread.sleep(2000);
			qty_capture = getText(lbl_availQuantityProductCapturuPage);
			qty_capture_int = Integer.parseInt(qty_capture);
		}

		click(btn_updateCapture);

		/* Product 04 */
		String item_xpath = btn_itemProductCapturePage.replace("_product_number", "2");
		explicitWait(item_xpath, 20);

		click(item_xpath);
		click(chk_productCaptureRange);

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$('#attrSlide-txtSerialCount').val(" + "5" + ")");

		String lot4 = getInvObj().readIWRegression01("SN_IN_PC_048");
		String final_lot_script = "$('#attrSlide-txtSerialFrom').val(\"" + lot4 + "\")";
		jse.executeScript(final_lot_script);

		pressEnter(txt_seriealProductCapture);

		click(btn_updateCapture);

		explicitWait(btn_backButtonUpdateCapture, 20);
		click(btn_backButtonUpdateCapture);
		click(btn_relese);
		explicitWait(lbl_relesedConfirmation, 40);
		trackCode = getText(lbl_creationsDocNo);
		if (isDisplayed(lbl_relesedConfirmation)) {
			writeTestResults("Verify user able to dispatch above Serial/Batches",
					"User should be able to dispatch above Serial/Batches",
					"User successfully dispatch above Serial/Batches", "pass");
		} else {
			writeTestResults("Verify user able to dispatch Serial/Batches",
					"User should be able to dispatch Serial/Batches", "User doesn't dispatch Serial/Batches", "fail");
		}

	}

	/* IN_PC_049 */
	public void salesOrderOutbountShipmentToSalesInvoice_IN_PC_049() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 25);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(btn_salesModule, 10);
		click(btn_salesModule);
		getInvObjReg().customizeLoadingDelay(btn_salesOrder, 10);
		click(btn_salesOrder);
		getInvObjReg().customizeLoadingDelay(btn_newSalesOrder, 10);
		click(btn_newSalesOrder);
		getInvObjReg().customizeLoadingDelay(btn_salesOrderToSalesInvoiceJourney, 10);
		click(btn_salesOrderToSalesInvoiceJourney);

		getInvObjReg().customizeLoadingDelay(btn_lookupCustomerAccount, 25);
		click(btn_lookupCustomerAccount);
		getInvObjReg().customizeLoadingDelay(txt_searchCustomerSalesOrder, 10);
		sendKeys(txt_searchCustomerSalesOrder, commonCustomer);
		pressEnter(txt_searchCustomerSalesOrder);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(lnk_resultCustomerAccount, 10);
		doubleClick(lnk_resultCustomerAccount);

		click(btn_productLookupInGrid.replace("row", "1"));
		getInvObjReg().customizeLoadingDelay(txt_productSearch, 10);
		sendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		Thread.sleep(3000);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));

		selectText(drop_warehouseInGrid.replace("row", "1"), productConversionWarehouseRegression);

		sendKeys(txt_quantityInGrid.replace("row", "1").replace("tblName", "tblSalesOrder"),
				quantityTenProductConversion);

		sendKeys(txt_unitPriceInGrid.replace("row", "1").replace("tblName", "tblSalesOrder"), hundred);

		click(btn_checkout);
		Thread.sleep(2000);

		click(btn_draftActionVerification);
		getInvObjReg().customizeLoadingDelay(btn_releaseCommon, 20);
		getInvObjReg().customizeLoadingDelay(header_draft, 10);
		String header1 = getText(header_draft);
		if (!header1.equalsIgnoreCase("(Released)")) {
			Thread.sleep(6000);
			header1 = getText(header_draft);
		}

		trackCode = getText(lbl_docNo);
		invObj.writeRegression01("salesOrderNo_IN_PC_049", trackCode, 178);
		click(btn_releaseCommon);

		getInvObjReg().customizeLoadingDelay(btn_goToPageLink, 30);
		click(btn_goToPageLink);

		switchWindow();
		getInvObjReg().customizeLoadingDelay(btn_checkout, 30);
		click(btn_checkout);
		Thread.sleep(2000);

		click(btn_draftActionVerification);
		getInvObjReg().customizeLoadingDelay(btn_releaseCommon, 20);

		click(brn_serielBatchCapture);
		getInvObjReg().customizeLoadingDelay(btn_captureItem1, 10);
		click(btn_captureItem1);
		click(chk_productCaptureRange);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$('#attrSlide-txtSerialCount').val(10)");

		String sn = invObj.getOngoinfSerirlSpecificSerielNumber(10);
		invObj.writeRegression01("serielNo_IN_PC_049", sn, 179);
		sendKeys(txt_seriealProductCapture, sn);
		pressEnter(txt_seriealProductCapture);
		Thread.sleep(3000);
		getInvObjReg().customizeLoadingDelay(btn_updateCapture, 10);
		click(btn_updateCapture);
		Thread.sleep(3000);
		getInvObjReg().customizeLoadingDelay(btn_backButtonUpdateCapture, 10);
		click(btn_backButtonUpdateCapture);
		click(btn_releaseCommon);

		getInvObjReg().customizeLoadingDelay(btn_goToPageLink, 30);
		click(btn_goToPageLink);
		switchWindow();
		getInvObjReg().customizeLoadingDelayAndContinue(btn_checkout, 30);
		click(btn_checkout);
		Thread.sleep(2000);

		click(btn_draftActionVerification);
		getInvObjReg().customizeLoadingDelay(btn_releaseCommon, 20);
		click(btn_releaseCommon);
		getInvObjReg().customizeLoadingDelay(lbl_relesedConfirmation, 30);
		if (isDisplayed(lbl_relesedConfirmation)) {
			writeTestResults("Verify user can sell serials/batches via Sales Order journey",
					"User should be able to sell serials/batches via Sales Order journey",
					"User successfull sell serials/batches via Sales Order journey", "pass");
		} else {
			writeTestResults("Verify user can sell serials/batches via Sales Order journey",
					"User should be able to sell serials/batches via Sales Order journey",
					"User doesn't sell serials/batches via Sales Order journey", "fail");
		}

	}

	public void salesReturnOrderInboundShipmentToSalesInvoice_IN_PC_049() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 40);
		Thread.sleep(3000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 25);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(btn_salesModule, 10);
		click(btn_salesModule);
		getInvObjReg().customizeLoadingDelay(btn_salesReturnOrder, 10);
		click(btn_salesReturnOrder);
		getInvObjReg().customizeLoadingDelay(btn_newSalesReturnOrder, 10);
		click(btn_newSalesReturnOrder);
		getInvObjReg().customizeLoadingDelay(btn_journeySalesReturnOrderInboundShipmentSalesReturnInvoice, 10);
		click(btn_journeySalesReturnOrderInboundShipmentSalesReturnInvoice);
		getInvObjReg().customizeLoadingDelay(btn_customerAccountLookupCommon, 25);
		click(btn_customerAccountLookupCommon);
		getInvObjReg().customizeLoadingDelay(txt_searchCustomerSalesOrder, 10);
		sendKeys(txt_searchCustomerSalesOrder, commonCustomer);
		pressEnter(txt_searchCustomerSalesOrder);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(lnk_resultCustomerAccount, 20);
		click(lnk_resultCustomerAccount);
		doubleClick(lnk_resultCustomerAccount);
		getInvObjReg().customizeLoadingDelay(btn_soSelectorDocIcomIRO, 10);
		click(btn_soSelectorDocIcomIRO);
		getInvObjReg().customizeLoadingDelay(drop_searchByIRO, 10);
		selectText(drop_searchByIRO, salesOrderSearchByMethodSalesReturs);
		sendKeys(txt_docNomIROSerach, invObj.readIWRegression01("salesOrderNo_IN_PC_049"));
		click(btn_commonreturnSearch);
		Thread.sleep(4000);
		getInvObjReg().customizeLoadingDelay(chk_commonReturnDoc, 40);
		click(chk_commonReturnDoc);
		click(btn_commoApply);
		getInvObjReg().customizeLoadingDelay(drop_returnReason.replace("row", "1").replace("tblName", "tblReturnOrder"),
				10);
		selectText(drop_returnReason.replace("row", "1").replace("tblName", "tblReturnOrder"),
				returnReasonSalesReturnOrder);
		click(btn_checkout);
		Thread.sleep(2000);

		click(btn_draftActionVerification);
		getInvObjReg().customizeLoadingDelay(btn_releaseCommon, 20);
		click(btn_releaseCommon);

		getInvObjReg().customizeLoadingDelay(btn_goToPageLink, 30);
		click(btn_goToPageLink);

		switchWindow();
		getInvObjReg().customizeLoadingDelay(btn_checkout, 30);
		click(btn_checkout);
		Thread.sleep(2000);

		click(btn_draftActionVerification);
		getInvObjReg().customizeLoadingDelay(btn_releaseCommon, 20);
		click(brn_serielBatchCapture);
		getInvObjReg().customizeLoadingDelay(btn_captureItem1, 10);
		click(btn_captureItem1);
		getInvObjReg().customizeLoadingDelay(btn_updateCapture, 10);
		click(btn_updateCapture);
		Thread.sleep(3000);
		getInvObjReg().customizeLoadingDelay(btn_backButtonUpdateCapture, 10);
		click(btn_backButtonUpdateCapture);
		click(btn_releaseCommon);

		getInvObjReg().customizeLoadingDelay(btn_goToPageLink, 30);
		click(btn_goToPageLink);
		switchWindow();
		getInvObjReg().customizeLoadingDelayAndContinue(btn_checkout, 30);
		click(btn_checkout);
		Thread.sleep(2000);

		click(btn_draftActionVerification);
		getInvObjReg().customizeLoadingDelay(btn_releaseCommon, 20);
		click(btn_releaseCommon);
		if (isDisplayed(header_releasedSalesReturnOrder)) {
			writeTestResults("Verify user can return serials/batches", "User should be able to return serials/batches",
					"User successfull return serials/batches", "pass");
		} else {
			writeTestResults("Verify user can return serials/batches", "User should be able to return serials/batches",
					"User doesn't return serials/batches", "fail");
		}

	}

	public void productConversionWithReturnSeriels_IN_PC_049() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}
		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, productConversionWarehouseRegression);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify user allow to fill mandatory fields", "User should allow to fill mandatory fields",
					"User allow to fill mandatory fields", "pass");
		} else {
			writeTestResults("Verify user allow to fill mandatory fields", "User should allow to fill mandatory fields",
					"User not allow to fill mandatory fields", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User allow to select \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User not allow to select \"From Product\"",
					"fail");
		}
		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), quantityTenProductConversion);

		click(btn_serielBatchCaptureNosProductConversionRowReplace.replace("row", "1"));
		getInvObjReg().customizeLoadingDelayAndContinue(header_batchCaptureWindowProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, quantityTenProductConversion);
		sendKeys(txt_serielNo, getInvObj().readIWRegression01("serielNo_IN_PC_049"));
		pressEnter(txt_serielNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		String capturedQuantityBatchSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantityBatchSpecific.equals(quantityTenProductConversion)) {
			writeTestResults("Verify user allwo to capture batches for From Product",
					"User should allwo to capture batches for From Product",
					"User allwo to capture batches for From Product", "pass");
		} else {
			writeTestResults("Verify user allwo to capture batches for From Product",
					"User should allwo to capture batches for From Product",
					"User not allwo to capture batches for From Product", "fail");

		}

		click(btn_applyLast);

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User allow to select \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User not allow to select \"To Product\"", "fail");
		}

		sendKeys(txt_quantityToProductProductConversion, quantityToProductProductConversion);

		click(btn_serialNosFromProductConversionToProduc);
		getInvObjReg().customizeLoadingDelayAndContinue(header_batchCaptureWindowProductConversion, 20);

		if (isDisplayed(header_batchCaptureWindowProductConversion)) {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window successfully pop-up",
					"pass");
		} else {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window doesn't pop-up",
					"fail");
		}

		explicitWait(txt_batchNoToProductProductConversion, 20);
		sendKeys(txt_batchNoToProductProductConversion,
				invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_expiryDateToProductProductConversion);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		sendKeys(txt_lotNoToProductProductConversion,
				invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_menufacDateToProductProductConversion);
		Thread.sleep(200);
		doubleClick(btn_calenderBack);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_lotNoToProductProductConversion);
		Thread.sleep(3000);

		String capturedQuantitySerielSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantitySerielSpecific.equals(quantityToProductProductConversion)) {
			writeTestResults("Verify user allwo to capture serials for To Product",
					"User should allwo to capture serials for To Product",
					"User allwo to capture serials for To Product", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serials for To Product",
					"User should allwo to capture serials for To Product",
					"User not allwo to capture serials for To Product", "fail");

		}

		click(btn_applyLast);

		click(btn_draftActionVerification);

		getInvObjReg().customizeLoadingDelay(header_drftedProductConversion, 40);
		if (isDisplayed(header_drftedProductConversion)) {
			writeTestResults("Verify user allow to draft the Product Conversion",
					"User should allow to draft the Product Conversiont", "User allow to draft the Product Conversion",
					"pass");
		} else {
			writeTestResults("Verify user allow to draft the Product Conversion",
					"User should allow to draft the Product Conversiont",
					"User not allow to draft the Product Conversion", "fail");

		}

		click(btn_releaseCommon);

		getInvObjReg().customizeLoadingDelay(header_releasedProductConversion, 40);
		trackCode = getText(lbl_docNo);

		if (isDisplayed(header_releasedProductConversion)) {
			writeTestResults("Verify user able to convert the returned serial/batches into another product",
					"User should be able to convert the returned serial/batches into another product",
					"User able to convert the returned serial/batches into another product", "pass");
		} else {
			writeTestResults("Verify user able to convert the returned serial/batches into another product",
					"User should be able to convert the returned serial/batches into another product",
					"User not able to convert the returned serial/batches into another product", "fail");

		}
	}

	/* IN_PC_050 */
	public void internalOrderAndInternalDispatchOrder_IN_PC_050() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().createNewWarehouse();
		explicitWait(navigation_pane, 20);
		Thread.sleep(2000);
		explicitWait(navigation_pane, 20);
		click(navigation_pane);
		explicitWait(inventory_module, 10);
		click(inventory_module);
		click(btn_internelOrder);
		click(btn_newInternelOrder);
		explicitWait(btn_employeeRequestJourneyInternelOrder, 20);
		click(btn_employeeRequestJourneyInternelOrder);
		sendKeys(txt_tiltleInternelOrder, titleInternelOrder);

		click(btn_searchRequester);
		invObj.handeledSendKeys(txt_RequesterSearch, employee01);
		pressEnter(txt_RequesterSearch);
		explicitWait(lnk_commonResultEmployee.replace("emp_code", employee01), 30);
		doubleClick(lnk_commonResultEmployee.replace("emp_code", employee01));
		Thread.sleep(1300);
		/* Product add to the grid */
		for (int i = 1; i < 5; i++) {
			String str_i = String.valueOf(i);

			String lookup_path = btn_productLookupInterDeprtmentTransferOrderForLoop.replace("row", str_i)
					.replace("column", "10").replace("tblStockAdj", "tblInternalRequest");
			click(lookup_path);
			String product = "";

			if (i == 1) {
				product = "BatchSpecific";
			} else if (i == 2) {
				product = "BatchFifo";
			} else if (i == 3) {
				product = "Lot";
			} else if (i == 4) {
				product = "SerielSpecific";
			} else {
				System.out.println("Product type cannot identified.........");
			}
			invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation(product));
			pressEnter(txt_productSearch);
			explicitWait(result_cpmmonProduct.replace("product", invObj.readTestCreation(product)), 20);
			Thread.sleep(1000);
			doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation(product)));
			Thread.sleep(1500);

			String txt_qty = txt_quantityInGrid.replace("row", str_i).replace("tblName", "tblInternalRequest");

			explicitWait(txt_qty, 7);
			sendKeys(txt_qty, "10");

			explicitWait(btn_addNewRow, 10);
			click(btn_addNewRow);
		}

		click(btn_deleteLastRowOfTheGrid);

		click(btn_detailTab);
		selectText(drop_warehouseInternelOrderEmployeeRequest, warehouseInternelOrderEmployeeRequest);

		click(btn_draftInternelOrder);
		Thread.sleep(3000);
		trackCode = getText(lbl_creationsDocNo);
		explicitWait(btn_relese, 15);
		click(btn_relese);
		explicitWait(btn_goToPageLink, 25);
		click(btn_goToPageLink);

		/* Internal Dispatch Order */
		switchWindow();
		explicitWait(btn_draftInternelDispatchOrder, 20);
		click(btn_draftInternelDispatchOrder);
		explicitWait(brn_serielBatchCapture, 20);
		click(brn_serielBatchCapture);

		/* Product 01 */
		explicitWait(btn_captureItem1, 20);
		click(btn_captureItem1);
		Thread.sleep(1000);

		click(btn_capturePageDocIcon);

		click(chk_availableProductsTransferOrderOutboundShipment.replace("row", "1"));

		String qty = getText(lbl_qtyBatchSpecifificCapturePage);
		String qty_capture = getText(lbl_availQuantityProductCapturuPage);

		int qty_int = Integer.parseInt(qty);
		int qty_capture_int = Integer.parseInt(qty_capture);

		int i = 2;
		while (qty_int > qty_capture_int) {
			String i_str = String.valueOf(i);
			click(chk_deleteItemCapturePage);
			click(btn_deleteRowProductCaptururePage);
			click(btn_capturePageDocIcon);
			click(chk_availableProductsTransferOrderOutboundShipment.replace("row", i_str));
			Thread.sleep(2000);
			qty_capture = getText(lbl_availQuantityProductCapturuPage);
			qty_capture_int = Integer.parseInt(qty_capture);
		}

		click(btn_updateCapture);

		/* Product 04 */
		String item_xpath = btn_itemProductCapturePage.replace("_product_number", "4");
		explicitWait(item_xpath, 20);

		click(item_xpath);
		click(chk_productCaptureRange);

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$('#attrSlide-txtSerialCount').val(" + qtyTransferOrder + ")");

		String lot4 = invObj.getOngoinfSerirlSpecificSerielNumber(10);
		invObj.writeRegression01("SerielSpecific_IN_PC_050", lot4, 180);
		String final_lot_script = "$('#attrSlide-txtSerialFrom').val(\"" + lot4 + "\")";
		jse.executeScript(final_lot_script);

		pressEnter(txt_seriealProductCapture);

		click(btn_updateCapture);

		explicitWait(btn_backButtonUpdateCapture, 20);
		click(btn_backButtonUpdateCapture);
		click(btn_relese);
		explicitWait(lbl_relesedConfirmation, 40);
		trackCode = getText(lbl_creationsDocNo);
		invObj.writeRegression01("IDO_IN_PC_050", trackCode, 181);
		if (isDisplayed(lbl_relesedConfirmation)) {
			writeTestResults("Verify user able to dispatch Serial/Batches via Internal Order",
					"User should be able to dispatch Serial/Batches via Internal Order",
					"User successfully dispatch Serial/Batches via Internal Order", "pass");
		} else {
			writeTestResults("Verify user able to dispatch Serial/Batches via Internal Order",
					"User should be able to dispatch Serial/Batches via Internal Order",
					"User doesn't dispatch Serial/Batches via Internal Order", "fail");
		}

	}

	public void internalRetunOrderAndInternalRecipt_IN_PC_050() throws Exception {
		explicitWait(navigation_pane, 20);
		Thread.sleep(2000);
		click(navigation_pane);
		explicitWait(inventory_module, 10);
		click(inventory_module);
		explicitWait(btn_internelReturnOrder, 10);
		click(btn_internelReturnOrder);

		click(btn_newInternelReturnOrder);

		click(btn_employeeReturnJourneyIRO);

		click(btn_searchIconEmployeeIRO);
		invObj.handeledSendKeys(txt_employeeSearchRequesterWindowIRO, employee01);
		pressEnter(txt_employeeSearchRequesterWindowIRO);
		explicitWait(lnk_commonResultEmployee.replace("emp_code", employee01), 30);
		doubleClick(lnk_commonResultEmployee.replace("emp_code", employee01));

		selectText(dropdown_toWarehouseIRO, getInvObj().readIWRegression01("NewlyCreatedWarehouse") + " [Negombo]");
		click(btn_docIconSelectInternelOrderIRO);

		sendKeys(txt_internelOrderSearchIRO, invObj.readIWRegression01("IDO_IN_PC_050"));

		click(btn_serchDocInternelReturnOrder);
		explicitWait(
				"(" + td_countProductReleventToIDO.replace("ido", invObj.readIWRegression01("IDO_IN_PC_050")) + ")[1]",
				15);
		click(chk_productsReleventToIDO.replace("row", "1"));
		click(chk_productsReleventToIDO.replace("row", "2"));
		click(chk_productsReleventToIDO.replace("row", "3"));
		click(chk_productsReleventToIDO.replace("row", "4"));

		click(btn_commoApply);
		click(btn_draft2);
		explicitWait(header_andHeaderStatusDarftedIntrnalReturOrder, 30);
		click(btn_relese);
		explicitWait(btn_goToPageLink, 30);

		click(btn_goToPageLink);
		switchWindow();
		explicitWait(btn_draftActionVerification, 30);
		click(btn_draftActionVerification);

		explicitWait(brn_serielBatchCapture, 50);
		click(brn_serielBatchCapture);
		explicitWait(btn_itemProductCapturePage.replace("_product_number", "1"), 20);
		for (int i = 1; i < 5; i++) {
			Actions action1 = new Actions(driver);
			WebElement we1 = driver
					.findElement(By.xpath(btn_itemProductCapturePage.replace("_product_number", String.valueOf(i))));
			action1.moveToElement(we1).build().perform();
			click(btn_itemProductCapturePage.replace("_product_number", String.valueOf(i)));
			click(chk_selectAllCaptureItem);
			click(btn_updateCapture);
			Thread.sleep(2000);
		}

		click(btn_backButtonUpdateCapture);

		click(btn_releese2);
		if (isDisplayed(lbl_relesedConfirmation)) {
			writeTestResults("Verify user able to return Serial/Batches via Internal Return Order",
					"User should be able to return Serial/Batches via Internal Return Order",
					"User successfully return Serial/Batches via Internal Return Order", "pass");
		} else {
			writeTestResults("Verify user able to return Serial/Batches via Internal Return Order",
					"User should be able to return Serial/Batches via Internal Return Order",
					"User doesn't return Serial/Batches via Internal Return Order", "fail");
		}

	}

	public void productConversion_IN_PC_050() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, getInvObj().readIWRegression01("NewlyCreatedWarehouse") + " [Negombo]");
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(getInvObj().readIWRegression01("NewlyCreatedWarehouse") + " [Negombo]")) {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User allow to fill summary details", "pass");
		} else {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User not allow to fill summary details", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("Lot"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(result_cpmmonProduct.replace("product", invObj.readTestCreation("Lot")),
				20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("Lot")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("Lot")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("product", invObj.readTestCreation("Lot")))) {
			writeTestResults("Verify user allow to select Lot product as \"From Product\"",
					"User should allow to select Lot product as \"From Product\"",
					"User allow to select Lot product as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select Lot product as \"From Product\"",
					"User should allow to select Lot product as \"From Product\"",
					"User not allow to select Lot product as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerirlFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerirlFifo")))) {
			writeTestResults("Verify user allow to select serial specific product as \"To Product\"",
					"User should allow to select serial specific product as \"To Product\"",
					"User allow to select serial specific product as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select serial specific product as \"To Product\"",
					"User should allow to select serial specific product as \"To Product\"",
					"User not allow to select serial specific product as \"To Product\"", "fail");
		}

		sendKeys(txt_quantityToProductProductConversion, "5");

		click(btn_addNewRowProductConversion);

		click(btn_productLookupRowReplaceProductConversion.replace("row", "2"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchFifo")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchFifo")))) {
			writeTestResults("Verify user allow to select batch FIFO as \"From Product\"",
					"User should allow to select batch FIFO as \"From Product\"",
					"User allow to select batch FIFO as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select batch FIFO as \"From Product\"",
					"User should allow to select batch FIFO as \"From Product\"",
					"User not allow to select batch FIFO as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "2"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "2"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerirlFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerirlFifo")))) {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO as \"To Product\"",
					"User allow to select seriel FIFO as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO as \"To Product\"",
					"User not allow to select seriel FIFO as \"To Product\"", "fail");
		}

		sendKeys(txt_qtyToProductProductConcersionRowReplace.replace("row", "2"), "5");

		click(btn_addNewRowProductConversion);

		click(btn_productLookupRowReplaceProductConversion.replace("row", "3"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select batch specific as \"From Product\"",
					"User should allow to select batch specific as \"From Product\"",
					"User allow to select batch specific as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select batch specific as \"From Product\"",
					"User should allow to select batch specific as \"From Product\"",
					"User not allow to select batch specific as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "3"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "3"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerirlFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")))) {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO as \"To Product\"",
					"User allow to select seriel FIFO as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO batch as \"To Product\"",
					"User not allow to select seriel FIFO as \"To Product\"", "fail");
		}

		sendKeys(txt_qtyToProductProductConcersionRowReplace.replace("row", "3"), "5");

		click(btn_addNewRowProductConversion);

		click(btn_productLookupRowReplaceProductConversion.replace("row", "4"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify user allow to select serial specific as \"From Product\"",
					"User should allow to select serial specific as \"From Product\"",
					"User allow to select serial specific as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select serial specific as \"From Product\"",
					"User should allow to select serial specific as \"From Product\"",
					"User not allow to select serial specific as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "4"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "4"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerirlFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")))) {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO as \"To Product\"",
					"User allow to select seriel FIFO as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO batch as \"To Product\"",
					"User not allow to select seriel FIFO as \"To Product\"", "fail");
		}

		sendKeys(txt_qtyToProductProductConcersionRowReplace.replace("row", "4"), "5");

		/* capture row 01 */
		click(btn_serleNosProductConversionToProduct.replace("row", "1"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		String sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		/* capture row 02 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "2"));
		explicitWait(btn_seraialBatchCaptureDocIconProductConversion, 10);
		click(btn_seraialBatchCaptureDocIconProductConversion);
		explicitWait(chk_batchSelectionProductConversion, 10);
		click(chk_batchSelectionProductConversion);
		click(btn_commoApplyLast);

		click(btn_serleNosProductConversionToProduct.replace("row", "2"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		/* capture row 03 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "3"));
		explicitWait(btn_seraialBatchCaptureDocIconProductConversion, 10);
		click(btn_seraialBatchCaptureDocIconProductConversion);
		explicitWait(chk_batchSelectionProductConversion, 10);
		click(chk_batchSelectionProductConversion);
		click(btn_commoApplyLast);

		click(btn_serleNosProductConversionToProduct.replace("row", "3"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		/* capture row 04 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "4"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		explicitWait(txt_serielCountProductConversion, 20);
		sendKeys(txt_serielCountProductConversion, "10");
		sendKeys(txt_serielNo, getInvObj().readIWRegression01("SerielSpecific_IN_PC_050"));
		pressEnter(txt_serielNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		click(btn_serleNosProductConversionToProduct.replace("row", "4"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		explicitWait(btn_draftActionVerification, 30);
		click(btn_draftActionVerification);

		getInvObjReg().customizeLoadingDelay(header_drftedProductConversion, 40);

		click(btn_releaseCommon);
		explicitWait(header_releasedProductConversion, 30);
		if (isDisplayed(header_releasedProductConversion)) {
			writeTestResults("Verify user able to convert the returned Serial/Batches to another product",
					"User should be able to convert the returned Serial/Batches to another product",
					"User successfully convert the returned Serial/Batches to another product", "pass");
		} else {
			writeTestResults("Verify user able to convert the returned Serial/Batches to another product",
					"User should be able to convert the returned Serial/Batches to another product",
					"User doesn't convert the returned Serial/Batches to another product", "fail");

		}
	}

	/* IN_PC_041_051 */
	public void stockAdjustmentForNewWarehouse_IN_PC_041_051() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().createNewWarehouse();
		invObj.customizeLoadingDelay(navigation_pane, 50);
		Thread.sleep(3000);
		invObj.customizeLoadingDelay(navigation_pane, 20);
		click(navigation_pane);
		invObj.customizeLoadingDelay(inventory_module, 40);
		click(inventory_module);
		invObj.customizeLoadingDelay(btn_stockAdjustment, 40);
		click(btn_stockAdjustment);
		invObj.customizeLoadingDelay(btn_newStockAdjustment, 40);
		click(btn_newStockAdjustment);

		sendKeys(txt_descStockAdj, descStockAdj);
		Thread.sleep(1000);
		selectText(dropdown_warehouseStockAdj, getInvObj().readIWRegression01("NewlyCreatedWarehouse") + " [Negombo]");
		Thread.sleep(1500);

		/* Product add to the grid */
		for (int i = 1; i < 3; i++) {
			String str_i = String.valueOf(i);
			click(btn_productLookupStockAdjForLoop.replace("row", str_i));
			Thread.sleep(3000);
			invObj.customizeLoadingDelay(txt_productSearch, 15);

			String product = "";

			if (i == 1) {
				product = "Lot";
			} else if (i == 2) {
				product = "BatchFifo";
			} else {
				System.out.println("Product type cannt identified.........");
			}

			invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation(product));
			pressEnter(txt_productSearch);
			String product_path = result_cpmmonProduct.replace("product", invObj.readTestCreation(product));
			invObj.customizeLoadingDelay(product_path, 30);
			doubleClick(product_path);
			Thread.sleep(1500);
			String txt_qty = txt_qtyProductGridStockAdjustment.replace("row", str_i);
			String txt_cost = txt_costProductGridStockAdjustment.replace("row", str_i);

			sendKeys(txt_qty, productQuantity1);

			sendKeys(txt_cost, productCost1);
			invObj.sleepCusomized(btn_avilabilityCheckWidget);

			invObj.sleepCusomized(btn_addNewRow);
			click(btn_addNewRow);
		}

		click(btn_deleteLastRowOfTheGrid);

		click(btn_draftActionVerification);
		invObj.customizeLoadingDelay(brn_serielBatchCapture, 40);
		click(brn_serielBatchCapture);

		/* Product 02 */
		Thread.sleep(3000);
		String item_xpath = btn_itemProductCapturePage.replace("_product_number", "2");
		explicitWait(item_xpath, 15);
		click(item_xpath);

		sendKeys(txt_batchNumberCaptureILOOutboundShipment, invObj.genBatchFifoSerielNumber(200));
		sendKeys(txt_lotNumberStockAdjustment,
				invObj.genProductId() + invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_expiryDateStockAdjustmentProductCapture);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_lotNumberStockAdjustment);
		Thread.sleep(1000);
		invObj.customizeLoadingDelay(btn_updateCapture, 50);
		click(btn_updateCapture);
		invObj.customizeLoadingDelay((item_xpath + span_afterCaptureSerialGreen), 30);
		click(btn_productCaptureBackButton);
		Thread.sleep(1000);
		click(btn_relese);
		invObj.customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 60);
	}

	public void productConversion_IN_PC_041_051() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, getInvObj().readIWRegression01("NewlyCreatedWarehouse") + " [Negombo]");
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(getInvObj().readIWRegression01("NewlyCreatedWarehouse") + " [Negombo]")) {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User allow to fill summary details", "pass");
		} else {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User not allow to fill summary details", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("Lot"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(result_cpmmonProduct.replace("product", invObj.readTestCreation("Lot")),
				20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("Lot")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("Lot")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("product", invObj.readTestCreation("Lot")))) {
			writeTestResults("Verify user allow to select Lot product as \"From Product\"",
					"User should allow to select Lot product as \"From Product\"",
					"User allow to select Lot product as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select Lot product as \"From Product\"",
					"User should allow to select Lot product as \"From Product\"",
					"User not allow to select Lot product as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select Batch Specific product as \"To Product\"",
					"User should allow to select Batch Specific product as \"To Product\"",
					"User allow to select Batch Specific product as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select Batch Specific product as \"To Product\"",
					"User should allow to select Batch Specific product as \"To Product\"",
					"User not allow to select Batch Specific product as \"To Product\"", "fail");
		}

		sendKeys(txt_quantityToProductProductConversion, "5");

		click(btn_addNewRowProductConversion);

		click(btn_productLookupRowReplaceProductConversion.replace("row", "2"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchFifo")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchFifo")))) {
			writeTestResults("Verify user allow to select batch FIFO as \"From Product\"",
					"User should allow to select batch FIFO as \"From Product\"",
					"User allow to select batch FIFO as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select batch FIFO as \"From Product\"",
					"User should allow to select batch FIFO as \"From Product\"",
					"User not allow to select batch FIFO as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "2"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "2"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify user allow to select seriel specific as \"To Product\"",
					"User should allow to select seriel specific as \"To Product\"",
					"User allow to select seriel specific as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select seriel specific as \"To Product\"",
					"User should allow to select seriel specific as \"To Product\"",
					"User not allow to select seriel specific as \"To Product\"", "fail");
		}

		sendKeys(txt_qtyToProductProductConcersionRowReplace.replace("row", "2"), "5");

		/* capture row 01 */
		explicitWait(btn_serleNosProductConversionToProduct.replace("row", "1"), 10);
		click(btn_serleNosProductConversionToProduct.replace("row", "1"));
		explicitWait(txt_batchNoToProductProductConversion, 20);
		sendKeys(txt_batchNoToProductProductConversion,
				invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_expiryDateToProductProductConversion);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		sendKeys(txt_lotNoToProductProductConversion,
				invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_menufacDateToProductProductConversion);
		Thread.sleep(200);
		doubleClick(btn_calenderBack);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_lotNoToProductProductConversion);
		Thread.sleep(2000);
		click(btn_commoApplyLast);

		/* capture row 02 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "2"));
		explicitWait(btn_seraialBatchCaptureDocIconProductConversion, 10);
		click(btn_seraialBatchCaptureDocIconProductConversion);
		explicitWait(chk_batchSelectionProductConversion, 10);
		click(chk_batchSelectionProductConversion);
		click(btn_commoApplyLast);

		click(btn_serleNosProductConversionToProduct.replace("row", "2"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		String sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		getInvObj().writeRegression01("SN_IN_PC_041_051", sn, 182);
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		explicitWait(btn_draftActionVerification, 30);
		click(btn_draftActionVerification);

		getInvObjReg().customizeLoadingDelay(header_drftedProductConversion, 40);

		click(btn_releaseCommon);
		explicitWait(header_releasedProductConversion, 30);
		if (isDisplayed(header_releasedProductConversion)) {
			writeTestResults("Verify user can convert product via Product Conversion",
					"User should be able to convert product via Product Conversion",
					"User successfully convert product via Product Conversion", "pass");
		} else {
			writeTestResults("Verify user can convert product via Product Conversion",
					"User should be able to convert product via Product Conversion",
					"User doesn't convert product via Product Conversion", "fail");
		}
	}

	public void productConversionWithConvertedSeriels_IN_PC_041_051() throws Exception {
		((JavascriptExecutor) driver).executeScript("window.open('about:blank','_blank');");
		switchWindow();
		openPage(siteURL);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, getInvObj().readIWRegression01("NewlyCreatedWarehouse") + " [Negombo]");
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(getInvObj().readIWRegression01("NewlyCreatedWarehouse") + " [Negombo]")) {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User allow to fill summary details", "pass");
		} else {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User not allow to fill summary details", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select batch specific product as \"From Product\"",
					"User should allow to select batch specific product as \"From Product\"",
					"User allow to select batch specific product as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select batch specific product as \"From Product\"",
					"User should allow to select batch specific product as \"From Product\"",
					"User not allow to select batch specific product as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), "5");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("Lot"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(result_cpmmonProduct.replace("product", invObj.readTestCreation("Lot")),
				20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("Lot")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("Lot")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("Lot")))) {
			writeTestResults("Verify user allow to select Lot product as \"To Product\"",
					"User should allow to select Lot product as \"To Product\"",
					"User allow to select Lot product as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select Lot product as \"To Product\"",
					"User should allow to select Lot product as \"To Product\"",
					"User not allow to select Lot product as \"To Product\"", "fail");
		}

		sendKeys(txt_quantityToProductProductConversion, "10");

		click(btn_addNewRowProductConversion);

		click(btn_productLookupRowReplaceProductConversion.replace("row", "2"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify user allow to select serial specific as \"From Product\"",
					"User should allow to select serial specific as \"From Product\"",
					"User allow to select serial specific as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select serial specific as \"From Product\"",
					"User should allow to select serial specific as \"From Product\"",
					"User not allow to select serial specific as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "2"), "5");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "2"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchFifo")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("BatchFifo")))) {
			writeTestResults("Verify user allow to select batch FIFO as \"To Product\"",
					"User should allow to select batch FIFO as \"To Product\"",
					"User allow to select batch FIFO as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select batch FIFO as \"To Product\"",
					"User should allow to select batch FIFO as \"To Product\"",
					"User not allow to select batch FIFO as \"To Product\"", "fail");
		}

		sendKeys(txt_qtyToProductProductConcersionRowReplace.replace("row", "2"), "10");

		/* capture row 01 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "1"));
		explicitWait(btn_seraialBatchCaptureDocIconProductConversion, 10);
		click(btn_seraialBatchCaptureDocIconProductConversion);
		explicitWait(chk_batchSelectionProductConversion, 10);
		click(chk_batchSelectionProductConversion);
		click(btn_commoApplyLast);

		click(btn_serleNosProductConversionToProduct.replace("row", "1"));
		explicitWait(txt_lotNumber, 20);
		sendKeys(txt_lotNumber, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		pressEnter(txt_lotNumber);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		/* capture row 02 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "2"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		explicitWait(txt_serielCountProductConversion, 20);
		sendKeys(txt_serielCountProductConversion, quantityFiveProductConversion);
		sendKeys(txt_serielNo, getInvObj().readIWRegression01("SN_IN_PC_041_051"));
		pressEnter(txt_serielNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		explicitWait(btn_serleNosProductConversionToProduct.replace("row", "2"), 10);
		click(btn_serleNosProductConversionToProduct.replace("row", "2"));
		explicitWait(txt_batchNoToProductProductConversion, 20);
		sendKeys(txt_batchNoToProductProductConversion,
				invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_expiryDateToProductProductConversion);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		sendKeys(txt_lotNoToProductProductConversion,
				invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_menufacDateToProductProductConversion);
		Thread.sleep(200);
		doubleClick(btn_calenderBack);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_lotNoToProductProductConversion);
		Thread.sleep(2000);
		click(btn_commoApplyLast);

		explicitWait(btn_draftActionVerification, 30);
		click(btn_draftActionVerification);

		getInvObjReg().customizeLoadingDelay(header_drftedProductConversion, 40);

		click(btn_releaseCommon);
		explicitWait(header_releasedProductConversion, 30);
		if (isDisplayed(header_releasedProductConversion)) {
			writeTestResults("Verify user able to convert the above converted serial/batches",
					"User should be able to convert the above converted serial/batches",
					"User successfully convert the above converted serial/batches", "pass");
		} else {
			writeTestResults("Verify user able to convert the above converted serial/batches",
					"User should be able to convert the above converted serial/batches",
					"User doesn't convert the above converted serial/batches", "fail");
		}
	}

	public void reverseUsedProductConversion_IN_PC_041_051() throws Exception {
		switchWindow();
		click(btn_action);
		Thread.sleep(1000);
		if (isDisplayed(btn_reverseActionVrification)) {

		} else {
			writeTestResults("Verify set of actions were loaded including reverse",
					"Set of actions should be loaded including reverse",
					"Set of actions doesn't loaded including reverse", "fail");
		}
		click(btn_reverseActionVrification);
		JavascriptExecutor j7 = (JavascriptExecutor) driver;
		j7.executeScript("$(\"#txtrdcmnReverseDate\").datepicker(\"setDate\", new Date())");
		click(btn_reverseConfirmInboundShipmentActionVerification);
		if (isDisplayed(btn_reverseConfirationActionVerification)) {

		} else {
			writeTestResults("Verify confirmation message is pop-up", "Confirmation message should pop-up",
					"Confirmation message doesn't pop-up", "fail");
		}
		click(btn_reverseConfirationActionVerification);
		Thread.sleep(4000);
		String message = getText(para_validator);
		if (!message.contains("This document related stock has been already used...!")) {
			Thread.sleep(4000);
			message = getText(para_validator);
		}
		if (message.contains("This document related stock has been already used...!")) {
			writeTestResults("Verify user not allow to reverse the Product Conversion due to usage of stock",
					"User should not allow to reverse the Product Conversion due to usage of stock",
					"User not allow to reverse the Product Conversion due to usage of stock", "pass");
		} else {
			writeTestResults("Verify user not allow to reverse the Product Conversion due to usage of stock",
					"User should not allow to reverse the Product Conversion due to usage of stock",
					"The user allows reversing the Product Conversion even when used the stock", "fail");
		}
	}

	/* IN_PC_052 */
	public void transferOrder_IN_PC_052() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().createNewWarehouse();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 40);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 40);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 20);
		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_transferOrder, 30);
		click(btn_transferOrder);
		getInvObjReg().customizeLoadingDelay(btn_newTransferOrder, 50);
		click(btn_newTransferOrder);
		getInvObjReg().customizeLoadingDelay(txt_fromWareTO, 50);
		getInvObjReg().partSelectText(txt_fromWareTO, commonUseWarehouseTransferOrder);
		Select comboBoxWare = new Select(driver.findElement(By.xpath(txt_fromWareTO)));
		String selectedComboValueWare = comboBoxWare.getFirstSelectedOption().getText();

		boolean flag_allocation_ware = false;
		if (selectedComboValueWare.contains(commonUseWarehouseTransferOrder)) {
			flag_allocation_ware = true;
		}

		Select comboBoxToWare = new Select(driver.findElement(By.xpath(txt_toWareTO)));

		selectText(txt_toWareTO, invObj.readIWRegression01("NewlyCreatedWarehouse") + " [Negombo]");
		Thread.sleep(2000);
		String selectedComboValueToWare = comboBoxToWare.getFirstSelectedOption().getText();
		boolean flag_allocation_to_ware = false;
		if (selectedComboValueToWare.contains(invObj.readIWRegression01("NewlyCreatedWarehouse") + " [Negombo]")) {
			flag_allocation_to_ware = true;
		}

		if (isDisplayed(txt_fromWareTO)) {
		} else {
			writeTestResults("Verify warehouses were loaded in the dropdown",
					"Warehouses should be loaded in the dropdown", "Warehouses doesn't loaded in the dropdown", "fail");
		}

		if (flag_allocation_ware && flag_allocation_to_ware) {
		} else {
			writeTestResults("Verify selected warehouse was added to the grid",
					"Selected warehouse should be added to the grid", "Selected warehouse doesn't added to the grid",
					"fail");
		}

		/* Product add to the grid */
		for (int i = 1; i < 5; i++) {
			String str_i = String.valueOf(i);

			String lookup_path = btn_productLookupInGrid.replace("row", str_i);
			click(lookup_path);
			String product = "";

			if (i == 1) {
				product = "BatchSpecific";
			} else if (i == 2) {
				product = "BatchFifo";
			} else if (i == 3) {
				product = "Lot";
			} else if (i == 4) {
				product = "SerielSpecific";
			} else {
				System.out.println("Product type cannot identified.........");
			}
			Thread.sleep(2000);
			invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation(product));
			Thread.sleep(1000);
			pressEnter(txt_productSearch);
			getInvObjReg().customizeLoadingDelay(
					result_cpmmonProduct.replace("product", invObj.readTestCreation(product)), 40);

			if (isDisplayed(result_cpmmonProduct.replace("product", invObj.readTestCreation(product)))) {
			} else {
				writeTestResults("Verify user allow to select particular product in product lookup",
						"User should allow to select particular product in product lookup",
						"User not allow to select particular product in product lookup", "fail");
			}
			doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation(product)));
			Thread.sleep(2000);
			String grid_product = getText(txt_addedProductInGridMultipleProduct.replace("row", String.valueOf(i)));
			if (grid_product.contains(invObj.readTestCreation(product).substring(0, 14))) {
			} else {
				writeTestResults("Verify product should be added to the grid", "Product should be added to the grid",
						"Product doesn't added to the grid", "fail");
			}

			String txt_qty = txt_quantityInGrid.replace("row", str_i).replace("tblName", "tblTransferOrder");

			getInvObjReg().customizeLoadingDelay(txt_qty, 7);
			sendKeys(txt_qty, qtyTransferOrderRegression);
			Thread.sleep(2000);
			String grid_product_quantity_updating = getAttribute(txt_qtyTransferOrder.replace("row", "1"), "value");
			if (grid_product_quantity_updating.contains(qtyTransferOrderRegression)) {
			} else {
				writeTestResults("Verify product qty is added to the grid", "Product qty should be added to the grid",
						"Product qty doesn't added to the grid", "fail");
			}

			sendKeys(txt_transferCostOnTransferOrderGrid.replace("row", str_i), transferCost_IN_TO_036_038);

			if (getAttribute(txt_transferCostOnTransferOrderGrid.replace("row", str_i), "value")
					.equals(transferCost_IN_TO_036_038)) {

			} else {
				writeTestResults("Verify user allow to add transfer cost", "User should allow to add transfer cost",
						"User not allow to add transfer cost", "fail");
			}

			getInvObjReg().customizeLoadingDelay(btn_addNewRow, 10);
			click(btn_addNewRow);
		}

		click(btn_deleteRowCommonRowReplace.replace("row", "5"));

		click(btn_checkout);
		Thread.sleep(2000);
		click(btn_draftActionVerification);
		invObj.customizeLoadingDelay(header_andHeaderStatusDarftedTransferOrder, 40);
		if (isDisplayed(header_andHeaderStatusDarftedTransferOrder)) {
		} else {
			writeTestResults("Verify Transfer Order is drafted successfully",
					"Transfer Order should be drafted successfully", "Transfer Order doesn't drafted successfully",
					"fail");
		}
		trackCode = getText(lbl_docNo);
		click(btn_releaseCommon);
		getInvObjReg().customizeLoadingDelayAndContinue(btn_goToPageLink, 30);
		if (isDisplayed(btn_goToPageLink)) {
		} else {
			writeTestResults("User should allow to release the Transfer Order",
					"User should allow to release the Transfer Order", "User not allow to release the Transfer Order",
					"fail");
		}
	}

	public void outboundShipment_IN_PC_052() throws Exception {
		click(btn_goToPageLink);
		Thread.sleep(1500);
		pageRefersh();
		getInvObjReg().customizeLoadingDelay(header_releaseTransferOrder, 50);
		trackCode = getText(lbl_docNo);
		getInvObjReg2().documentNumber = trackCode;
		switchWindow();
		invObj.customizeLoadingDelay(header_newOutboundShiment, 50);
		if (isDisplayed(header_newOutboundShiment)) {
		} else {
			writeTestResults("Verify Outbound Shipment is loaded", "Outbound Shipment should be loaded",
					"Outbound Shipment doesn't loaded", "fail");
		}

		invObj.customizeLoadingDelay(btn_draftActionVerification, 10);
		click(btn_draftActionVerification);
		boolean flagDraftOutboundShipment = false;
		invObj.customizeLoadingDelay(lbl_statusConfirmation.replace("(status)", "(Draft)"), 50);
		if (isDisplayed(lbl_statusConfirmation.replace("(status)", "(Draft)"))) {
			flagDraftOutboundShipment = true;
		}
		getInvObjReg().customizeLoadingDelay(brn_serielBatchCapture, 20);
		click(brn_serielBatchCapture);

		/* Product 01 */
		getInvObjReg().customizeLoadingDelay(btn_captureItem1, 20);
		click(btn_captureItem1);
		Thread.sleep(1000);

		click(btn_capturePageDocIcon);

		click(chk_availableProductsTransferOrderOutboundShipment.replace("row", "1"));

		String qty = getText(lbl_qtyBatchSpecifificCapturePage);
		String qty_capture = getText(lbl_availQuantityProductCapturuPage);

		int qty_int = Integer.parseInt(qty);
		int qty_capture_int = Integer.parseInt(qty_capture);

		int i = 2;
		while (qty_int > qty_capture_int) {
			String i_str = String.valueOf(i);
			click(chk_deleteItemCapturePage);
			click(btn_deleteRowProductCaptururePage);
			click(btn_capturePageDocIcon);
			click(chk_availableProductsTransferOrderOutboundShipment.replace("row", i_str));
			Thread.sleep(2000);
			qty_capture = getText(lbl_availQuantityProductCapturuPage);
			qty_capture_int = Integer.parseInt(qty_capture);
		}

		click(btn_updateCapture);

		/* Product 04 */
		String item_xpath = btn_itemProductCapturePage.replace("_product_number", "4");
		getInvObjReg().customizeLoadingDelay(item_xpath, 20);

		click(item_xpath);
		click(chk_productCaptureRange);

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$('#attrSlide-txtSerialCount').val(" + qtyTransferOrderRegression + ")");

		String lot4 = invObj.getOngoinfSerirlSpecificSerielNumber(10);
		getInvObj().writeRegression01("SN_IN_PC_052", lot4, 183);
		String final_lot_script = "$('#attrSlide-txtSerialFrom').val(\"" + lot4 + "\")";
		jse.executeScript(final_lot_script);

		pressEnter(txt_seriealProductCapture);

		click(btn_updateCapture);

		getInvObjReg().customizeLoadingDelay(btn_backButtonUpdateCapture, 20);
		click(btn_backButtonUpdateCapture);
		click(btn_releaseCommon);
		invObj.customizeLoadingDelay(btn_goToPageLink, 40);
		click(btn_goToPageLink);
		Thread.sleep(1500);
		pageRefersh();
		boolean flagReleseOutboundShipment = false;
		invObj.customizeLoadingDelay(lbl_statusConfirmation.replace("(status)", "(Released)"), 50);
		if (isDisplayed(lbl_statusConfirmation.replace("(status)", "(Released)"))) {
			flagReleseOutboundShipment = true;
		}

		if (flagDraftOutboundShipment && flagReleseOutboundShipment) {

		} else {
			writeTestResults("Verify user allow to release the Outbound Shipment",
					"User should allow to release the Outbound Shipment",
					"User not allow to release the Outbound Shipment", "fail");
		}
	}

	public void inboundShipment_IN_PC_052() throws Exception {
		switchWindow();
		invObj.customizeLoadingDelay(header_newInboundShiment, 50);
		if (isDisplayed(header_newInboundShiment)) {
		} else {
			writeTestResults("Verify Inbound Shipment is loaded", "Inbound Shipment should be loaded",
					"Inbound Shipment doesn't loaded", "fail");
		}

		click(btn_checkout);
		Thread.sleep(2000);
		invObj.customizeLoadingDelay(btn_draftActionVerification, 40);
		click(btn_draftActionVerification);

		invObj.customizeLoadingDelay(btn_releaseCommon, 40);
		click(btn_releaseCommon);

		invObj.customizeLoadingDelay(lbl_statusConfirmation.replace("(status)", "(Released)"), 50);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(lbl_statusConfirmation.replace("(status)", "(Released)"))) {
			writeTestResults("Verify user able to transfer Serial/Batches via Trasnfer Order",
					"User should be able to transfer Serial/Batches via Trasnfer Order",
					"User successfully transfer Serial/Batches via Trasnfer Order", "pass");
		} else {
			writeTestResults("Verify user able to transfer Serial/Batches via Trasnfer Order",
					"User should be able to transfer Serial/Batches via Trasnfer Order",
					"User doesn't transfer Serial/Batches via Trasnfer Order", "fail");
		}
	}

	public void productConversion_IN_PC_052() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, getInvObj().readIWRegression01("NewlyCreatedWarehouse") + " [Negombo]");
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(getInvObj().readIWRegression01("NewlyCreatedWarehouse") + " [Negombo]")) {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User allow to fill summary details", "pass");
		} else {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User not allow to fill summary details", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("Lot"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(result_cpmmonProduct.replace("product", invObj.readTestCreation("Lot")),
				20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("Lot")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("Lot")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("product", invObj.readTestCreation("Lot")))) {
			writeTestResults("Verify user allow to select Lot product as \"From Product\"",
					"User should allow to select Lot product as \"From Product\"",
					"User allow to select Lot product as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select Lot product as \"From Product\"",
					"User should allow to select Lot product as \"From Product\"",
					"User not allow to select Lot product as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerirlFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerirlFifo")))) {
			writeTestResults("Verify user allow to select serial specific product as \"To Product\"",
					"User should allow to select serial specific product as \"To Product\"",
					"User allow to select serial specific product as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select serial specific product as \"To Product\"",
					"User should allow to select serial specific product as \"To Product\"",
					"User not allow to select serial specific product as \"To Product\"", "fail");
		}

		sendKeys(txt_quantityToProductProductConversion, "5");

		click(btn_addNewRowProductConversion);

		click(btn_productLookupRowReplaceProductConversion.replace("row", "2"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchFifo")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchFifo")))) {
			writeTestResults("Verify user allow to select batch FIFO as \"From Product\"",
					"User should allow to select batch FIFO as \"From Product\"",
					"User allow to select batch FIFO as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select batch FIFO as \"From Product\"",
					"User should allow to select batch FIFO as \"From Product\"",
					"User not allow to select batch FIFO as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "2"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "2"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerirlFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerirlFifo")))) {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO as \"To Product\"",
					"User allow to select seriel FIFO as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO as \"To Product\"",
					"User not allow to select seriel FIFO as \"To Product\"", "fail");
		}

		sendKeys(txt_qtyToProductProductConcersionRowReplace.replace("row", "2"), "5");

		click(btn_addNewRowProductConversion);

		click(btn_productLookupRowReplaceProductConversion.replace("row", "3"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select batch specific as \"From Product\"",
					"User should allow to select batch specific as \"From Product\"",
					"User allow to select batch specific as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select batch specific as \"From Product\"",
					"User should allow to select batch specific as \"From Product\"",
					"User not allow to select batch specific as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "3"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "3"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerirlFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")))) {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO as \"To Product\"",
					"User allow to select seriel FIFO as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO batch as \"To Product\"",
					"User not allow to select seriel FIFO as \"To Product\"", "fail");
		}

		sendKeys(txt_qtyToProductProductConcersionRowReplace.replace("row", "3"), "5");

		click(btn_addNewRowProductConversion);

		click(btn_productLookupRowReplaceProductConversion.replace("row", "4"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify user allow to select serial specific as \"From Product\"",
					"User should allow to select serial specific as \"From Product\"",
					"User allow to select serial specific as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select serial specific as \"From Product\"",
					"User should allow to select serial specific as \"From Product\"",
					"User not allow to select serial specific as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "4"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "4"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerirlFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")))) {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO as \"To Product\"",
					"User allow to select seriel FIFO as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO batch as \"To Product\"",
					"User not allow to select seriel FIFO as \"To Product\"", "fail");
		}

		sendKeys(txt_qtyToProductProductConcersionRowReplace.replace("row", "4"), "5");

		/* capture row 01 */
		click(btn_serleNosProductConversionToProduct.replace("row", "1"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		String sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		/* capture row 02 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "2"));
		explicitWait(btn_seraialBatchCaptureDocIconProductConversion, 10);
		click(btn_seraialBatchCaptureDocIconProductConversion);
		explicitWait(chk_batchSelectionProductConversion, 10);
		click(chk_batchSelectionProductConversion);
		click(btn_commoApplyLast);

		click(btn_serleNosProductConversionToProduct.replace("row", "2"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		/* capture row 03 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "3"));
		explicitWait(btn_seraialBatchCaptureDocIconProductConversion, 10);
		click(btn_seraialBatchCaptureDocIconProductConversion);
		explicitWait(chk_batchSelectionProductConversion, 10);
		click(chk_batchSelectionProductConversion);
		click(btn_commoApplyLast);

		click(btn_serleNosProductConversionToProduct.replace("row", "3"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		/* capture row 04 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "4"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		explicitWait(txt_serielCountProductConversion, 20);
		sendKeys(txt_serielCountProductConversion, "10");
		sendKeys(txt_serielNo, getInvObj().readIWRegression01("SN_IN_PC_052"));
		pressEnter(txt_serielNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		click(btn_serleNosProductConversionToProduct.replace("row", "4"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		explicitWait(btn_draftActionVerification, 30);
		click(btn_draftActionVerification);

		getInvObjReg().customizeLoadingDelay(header_drftedProductConversion, 40);

		click(btn_releaseCommon);
		explicitWait(header_releasedProductConversion, 30);
		if (isDisplayed(header_releasedProductConversion)) {
			writeTestResults("Verify user able convert the Serial/Batches which transferred by Transfer Order",
					"User should be able convert the Serial/Batches which transferred by Transfer Order",
					"User successfully convert the Serial/Batches which transferred by Transfer Order", "pass");
		} else {
			writeTestResults("Verify user able convert the Serial/Batches which transferred by Transfer Order",
					"User should be able convert the Serial/Batches which transferred by Transfer Order",
					"User doesn't convert the Serial/Batches which transferred by Transfer Order", "fail");

		}
	}

	/* IN_PC_053 */
	public void interdepartmentalTransferOrder_IN_PC_053() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().createNewWarehouse();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);
		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_InterDepartmentTransferOrder, 10);
		click(btn_InterDepartmentTransferOrder);
		if (isDisplayed(btn_verifyInternelOrderPage)) {

		} else {
			writeTestResults("Verify user able to navigate Inter Department Transfer Order page",
					"User should be able to navigateInter Department Transfer Order page",
					"User does'nt navigate Inter Department Transfer Order page", "Fail");
		}

		click(btn_newInternelReturnOrder);
		if (isDisplayed(btn_draft2)) {
		} else {
			writeTestResults("Verify user able to navigate  New Inter Department Transfer Order form",
					"User should be able to navigate New Inter Department Transfer Order form",
					"User does'nt navigate  New Inter Department Transfer Order form", "Fail");
		}

		// From ware
		selectText(txt_fromWareTO, fromWareInterDepartmentlTransferOrder);

		// To ware
		selectText(txt_toWareTO, invObj.readIWRegression01("NewlyCreatedWarehouse") + " [Negombo]");

		if (isDisplayed(txt_fromWareTO) && isDisplayed(txt_toWareTO)) {

		} else {
			writeTestResults(
					"Verify user can select from and destination warehouses and addresses of relevant warehouses are loaded",
					"User should be able to select from and destination warehouses and addresses of relevant warehouses should be loaded",
					"User couldn't select from and destination warehouses and addresses of relevant warehouses successfully loaded",
					"fail");
		}

		selectText(drop_salesPriceModel, salesPriceModelInterDepartmentalTransferOrder);
		click(btn_salesPriceModelConfirmation);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_salesPriceModel)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		if (selectedComboValue.equals(salesPriceModelInterDepartmentalTransferOrder)) {

		} else {
			writeTestResults("Verify user can select Sales Price Model",
					"User should be able to select select Sales Price Model",
					"User couldn't select select Sales Price Model", "fail");
		}

		/* Product add to the grid */
		int j = 0;
		for (int i = 1; i < 5; i++) {
			String str_i = String.valueOf(i);
			click(btn_productLookupStockAdjForLoop.replace("row", str_i).replace("tblStockAdj", "tblTransferOrder"));
			if (isDisplayed(txt_productSearch)) {

			} else {
				writeTestResults("Verify Product Lookup is pop-up", "Product Lookup should be pop-up",
						"Product Lookup doesn't pop-up", "fail");
			}

			String product = "";

			if (i == 1) {
				product = "BatchSpecific";
			} else if (i == 2) {
				product = "BatchFifo";
			} else if (i == 3) {
				product = "Lot";
			} else if (i == 4) {
				product = "SerielSpecific";
			} else {
				System.out.println("Product type cannot identified.........");
			}
			invObj.sleepCusomized(txt_productSearch);
			sendKeys(txt_productSearch, invObj.readTestCreation(product));
			pressEnter(txt_productSearch);
			Thread.sleep(3000);

			invObj.sleepCusomized(btn_resultProduct);
			if (isDisplayed(btn_resultProduct)) {

			} else {
				writeTestResults("Verify searched products was loaded", "Searched products should be loaded",
						"Searched products doesn't loaded", "fail");
			}
			invObj.sleepCusomized(btn_resultProduct);
			doubleClick(btn_resultProduct);

			String txt_qty = txt_qtyProductGridInterDepartmentTransferOrder.replace("row", str_i);

			if (isDisplayed(btn_avilabilityCheckWidget)) {

			} else {
				writeTestResults(
						"Verify selected products should be added to the Grid and OnHand Quantity [From][To], Default UOM were loaded for selected product",
						"Selected products should be added to the Grid and OnHand Quantity [From][To], Default UOM should be loaded for selected product",
						"Selected products doesn't added to the Grid and OnHand Quantity [From][To], Default UOM successfully loaded for selected product",
						"fail");
			}

			sendKeys(txt_qty, "10");
			if (isDisplayed(txt_qty)) {
			} else {
				writeTestResults("Verify selected products should be added to the grid",
						"Selected products should be added to the grid", "Selected products doesn't added to the grid",
						"pass");
			}

			invObj.sleepCusomized(btn_addNewRow);
			click(btn_addNewRow);
		}

		click(btn_deleteLastRowOfTheGrid);

		Thread.sleep(3000);
		click(btn_checkout2);
		Thread.sleep(4000);

		click(btn_draftActionVerification);
		invObj.customizeLoadingDelay(header_draftedInterdepartmentalTransferOrder, 40);
		if (isDisplayed(header_draftedInterdepartmentalTransferOrder)) {
		} else {
			writeTestResults("Verify Interdepartmental Transfer Order is drafted successfully",
					"Interdepartmental Transfer Order should be drafted successfully",
					"Interdepartmental Transfer Order doesn't drafted successfully", "fail");
		}
		click(btn_releaseCommon);
		getInvObjReg().customizeLoadingDelayAndContinue(btn_goToPageLink, 30);
		if (isDisplayed(btn_goToPageLink)) {
		} else {
			writeTestResults("User should allow to release the Interdepartmental Transfer Order",
					"User should allow to release the Interdepartmental Transfer Order",
					"User not allow to release the Interdepartmental Transfer Order", "fail");
		}
	}

	public void outboundShipmentInterdepartmentalTransferOrder_IN_PC_053() throws Exception {
		click(btn_goToPageLink);
		switchWindow();
		getInvObjReg().customizeLoadingDelay(btn_checkout2, 20);
		click(btn_checkout2);
		Thread.sleep(3000);
		if (isDisplayed(btn_checkout2)) {

		} else {
			writeTestResults("Verify user can checkout the 'Outbound Shipment'",
					"User should be able to checkout the 'Outbound Shipment",
					"User couldn't checkout the 'Outbound Shipment", "fail");
		}

		click(btn_draftActionVerification);
		invObj.customizeLoadingDelay(header_draftedOutboundShipment, 40);
		if (isDisplayed(header_draftedOutboundShipment)) {
		} else {
			writeTestResults("Verify Outbound Shipment is drafted successfully",
					"Outbound Shipment should be drafted successfully",
					"Outbound Shipment doesn't drafted successfully", "fail");
		}

		invObj.customizeLoadingDelay(brn_serielBatchCapture, 10);
		click(brn_serielBatchCapture);

		/* Product 01 */
		if (isDisplayed(btn_captureItem1)) {

		} else {
			writeTestResults(
					"Verify user can navigated Serial/Batch window and products which going to Outbound should be loaded for item 1",
					"User should be navigated Serial/Batch window and products which going to Outbound should be loaded for item 1",
					"User couldn't navigated Serial/Batch window and products which going to Outbound doesn't loaded for item 1",
					"fail");
		}

		click(btn_captureItem1);
		if (isDisplayed(btn_capturePageDocIcon)) {

		} else {
			writeTestResults("Verify Serial/Batch capturing details are enabled",
					"Serial/Batch capturing details should be enabled",
					"Serial/Batch capturing details doesn't enabled", "fail");
		}

		click(btn_capturePageDocIcon);
		if (isDisplayed(chk_availableProductsTransferOrderOutboundShipment.replace("row", "1"))) {

		} else {
			writeTestResults("Verify existing batches are loaded as pop-up",
					"Existing batches should be loaded as pop-up", "Existing batches doesn't loaded as pop-up", "fail");
		}

		click(chk_availableProductsTransferOrderOutboundShipment.replace("row", "1"));

		String qty = getText(lbl_qtyBatchSpecifificCapturePage);
		String qty_capture = getText(lbl_availQuantityProductCapturuPage);

		int qty_int = Integer.parseInt(qty);
		int qty_capture_int = Integer.parseInt(qty_capture);

		int i = 2;
		while (qty_int >= qty_capture_int) {
			String i_str = String.valueOf(i);
			click(chk_deleteItemCapturePage);
			click(btn_deleteRowProductCaptururePage);
			click(btn_capturePageDocIcon);
			click(chk_availableProductsTransferOrderOutboundShipment.replace("row", i_str));
			Thread.sleep(2000);
			qty_capture = getText(lbl_availQuantityProductCapturuPage);
			qty_capture_int = Integer.parseInt(qty_capture);
		}

		if (qty_int <= qty_capture_int) {

		} else {
			writeTestResults("Verify selected batches are added to the grid",
					"Selected batches should be added to the grid", "Selected batches doesn't added to the grid",
					"fail");
		}

		click(btn_updateCapture);

		String pro = getAttribute(btn_captureItem1, "class");

		if (pro.equals("fli-number green-vackground-fli-number")
				&& pro.equals("fli-number green-vackground-fli-number")) {

		} else {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User couldn't capture 'Seriel/Batch'", "fail");
		}

		/* Product 04 */
		String item_xpath = btn_itemProductCapturePage.replace("_product_number", "4");
		invObj.sleepCusomized(item_xpath);

		if (isDisplayed(item_xpath)) {

		} else {
			writeTestResults(
					"Verify user can navigated Serial/Batch window and products which going to Outbound should be loaded for item 4",
					"User should be navigated Serial/Batch window and products which going to Outbound should be loaded for item 4",
					"User couldn't navigated Serial/Batch window and products which going to Outbound doesn't loaded for item 4",
					"fail");
		}

		click(item_xpath);
		if (isDisplayed(chk_productCaptureRange)) {

		} else {
			writeTestResults("Verify Serial/Batch capturing details are enabled",
					"Serial/Batch capturing details should be enabled",
					"Serial/Batch capturing details doesn't enabled", "fail");
		}
		click(chk_productCaptureRange);

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$('#attrSlide-txtSerialCount').val(" + qtyTransferOrder + ")");

		String lot4 = invObj.getOngoinfSerirlSpecificSerielNumber(10);
		invObj.writeRegression01("SN_IN_PC_053", lot4, 188);
		String final_lot_script = "$('#attrSlide-txtSerialFrom').val(\"" + lot4 + "\")";
		jse.executeScript(final_lot_script);

		Thread.sleep(2000);
		pressEnter(txt_seriealProductCapture);

		if (qty_int <= qty_capture_int) {

		} else {
			writeTestResults("Verify selected batches are added to the grid",
					"Selected batches should be added to the grid", "Selected batches doesn't added to the grid",
					"fail");
		}
		Thread.sleep(2500);
		click(btn_updateCapture);
		Thread.sleep(2000);
		String pro1 = getAttribute(item_xpath, "class");
		if (!pro1.equals("fli-number green-vackground-fli-number")) {
			Thread.sleep(6000);
			pro1 = getAttribute(item_xpath, "class");
		}
		if (pro1.equals("fli-number green-vackground-fli-number")) {

		} else {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User couldn't capture 'Seriel/Batch'", "fail");
		}

		invObj.sleepCusomized(btn_backButtonUpdateCapture);
		click(btn_backButtonUpdateCapture);

		invObj.sleepCusomized(btn_relese);
		if (isDisplayed(btn_relese)) {

		} else {
			writeTestResults("Verify user can navigated to Outbound Shipment Form",
					"User should be able to navigated to Outbound Shipment Form",
					"User couldn't navigated to Outbound Shipment Form", "fail");
		}
		Thread.sleep(3000);
		trackCode = getText(lbl_creationsDocNo);
		click(btn_relese);
		invObj.sleepCusomized(btn_goToPageLink);
		if (isDisplayed(btn_goToPageLink)) {

		} else {
			writeTestResults("Verify user can releese Outbound Shipment",
					"User should be able to releese Outbound Shipment", "User couldn't releese Outbound Shipment",
					"fail");
		}

		invObj.sleepCusomized(btn_goToPageLink);
		if (isDisplayed(btn_goToPageLink)) {

		} else {
			writeTestResults("Verify user can complete Outbound Shipment",
					"User can be able to complete Outbound Shipment", "User not complete Outbound Shipment", "Fail");
		}
	}

	public void inboundShipmentInterdepartmentalTransferOrder_IN_PC_053() throws Exception {
		click(btn_goToPageLink);
		switchWindow();
		click(btn_checkout);
		if (isDisplayed(btn_checkout)) {

		} else {
			writeTestResults("Verify user can navigated to Inbound Shipment",
					"User should be able to navigated to Inbound Shipment",
					"User couldn't navigated to Inbound Shipment", "fail");
		}
		Thread.sleep(2000);
		click(btn_draft);
		invObj.customizeLoadingDelay(btn_draft, 40);
		if (isDisplayed(btn_relese)) {

		} else {
			writeTestResults("Verify user can draft \'Inbound Shipment\'",
					"User should be able to draft \'Inbound Shipment\'", "User couldn't draft \'Inbound Shipment\'",
					"fail");
		}

		click(btn_relese);
		invObj.customizeLoadingDelay(header_releasedInboundShipment, 40);
		trackCode = getText(lbl_creationsDocNo);
		if (isDisplayed(header_releasedInboundShipment)) {
			writeTestResults("Verify user able to transfer serial/batches via Interdepartmental Transfer Order",
					"User should be able to transfer serial/batches via Interdepartmental Transfer Order",
					"User successfully transfer serial/batches via Interdepartmental Transfer Order", "pass");
		} else {
			writeTestResults("Verify user able to transfer serial/batches via Interdepartmental Transfer Order",
					"User should be able to transfer serial/batches via Interdepartmental Transfer Order",
					"User doesn't transfer serial/batches via Interdepartmental Transfer Order", "fail");
		}

	}

	public void productConversion_IN_PC_053() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, getInvObj().readIWRegression01("NewlyCreatedWarehouse") + " [Negombo]");
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(getInvObj().readIWRegression01("NewlyCreatedWarehouse") + " [Negombo]")) {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User allow to fill summary details", "pass");
		} else {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User not allow to fill summary details", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("Lot"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(result_cpmmonProduct.replace("product", invObj.readTestCreation("Lot")),
				20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("Lot")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("Lot")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("product", invObj.readTestCreation("Lot")))) {
			writeTestResults("Verify user allow to select Lot product as \"From Product\"",
					"User should allow to select Lot product as \"From Product\"",
					"User allow to select Lot product as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select Lot product as \"From Product\"",
					"User should allow to select Lot product as \"From Product\"",
					"User not allow to select Lot product as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerirlFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerirlFifo")))) {
			writeTestResults("Verify user allow to select serial specific product as \"To Product\"",
					"User should allow to select serial specific product as \"To Product\"",
					"User allow to select serial specific product as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select serial specific product as \"To Product\"",
					"User should allow to select serial specific product as \"To Product\"",
					"User not allow to select serial specific product as \"To Product\"", "fail");
		}

		sendKeys(txt_quantityToProductProductConversion, "5");

		click(btn_addNewRowProductConversion);

		click(btn_productLookupRowReplaceProductConversion.replace("row", "2"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchFifo")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchFifo")))) {
			writeTestResults("Verify user allow to select batch FIFO as \"From Product\"",
					"User should allow to select batch FIFO as \"From Product\"",
					"User allow to select batch FIFO as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select batch FIFO as \"From Product\"",
					"User should allow to select batch FIFO as \"From Product\"",
					"User not allow to select batch FIFO as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "2"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "2"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerirlFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerirlFifo")))) {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO as \"To Product\"",
					"User allow to select seriel FIFO as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO as \"To Product\"",
					"User not allow to select seriel FIFO as \"To Product\"", "fail");
		}

		sendKeys(txt_qtyToProductProductConcersionRowReplace.replace("row", "2"), "5");

		click(btn_addNewRowProductConversion);

		click(btn_productLookupRowReplaceProductConversion.replace("row", "3"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select batch specific as \"From Product\"",
					"User should allow to select batch specific as \"From Product\"",
					"User allow to select batch specific as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select batch specific as \"From Product\"",
					"User should allow to select batch specific as \"From Product\"",
					"User not allow to select batch specific as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "3"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "3"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerirlFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")))) {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO as \"To Product\"",
					"User allow to select seriel FIFO as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO batch as \"To Product\"",
					"User not allow to select seriel FIFO as \"To Product\"", "fail");
		}

		sendKeys(txt_qtyToProductProductConcersionRowReplace.replace("row", "3"), "5");

		click(btn_addNewRowProductConversion);

		click(btn_productLookupRowReplaceProductConversion.replace("row", "4"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify user allow to select serial specific as \"From Product\"",
					"User should allow to select serial specific as \"From Product\"",
					"User allow to select serial specific as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select serial specific as \"From Product\"",
					"User should allow to select serial specific as \"From Product\"",
					"User not allow to select serial specific as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "4"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "4"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerirlFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")))) {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO as \"To Product\"",
					"User allow to select seriel FIFO as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO batch as \"To Product\"",
					"User not allow to select seriel FIFO as \"To Product\"", "fail");
		}

		sendKeys(txt_qtyToProductProductConcersionRowReplace.replace("row", "4"), "5");

		/* capture row 01 */
		click(btn_serleNosProductConversionToProduct.replace("row", "1"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		String sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		/* capture row 02 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "2"));
		explicitWait(btn_seraialBatchCaptureDocIconProductConversion, 10);
		click(btn_seraialBatchCaptureDocIconProductConversion);
		explicitWait(chk_batchSelectionProductConversion, 10);
		click(chk_batchSelectionProductConversion);
		click(btn_commoApplyLast);

		click(btn_serleNosProductConversionToProduct.replace("row", "2"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		/* capture row 03 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "3"));
		explicitWait(btn_seraialBatchCaptureDocIconProductConversion, 10);
		click(btn_seraialBatchCaptureDocIconProductConversion);
		explicitWait(chk_batchSelectionProductConversion, 10);
		click(chk_batchSelectionProductConversion);
		click(btn_commoApplyLast);

		click(btn_serleNosProductConversionToProduct.replace("row", "3"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		/* capture row 04 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "4"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		explicitWait(txt_serielCountProductConversion, 20);
		sendKeys(txt_serielCountProductConversion, "10");
		sendKeys(txt_serielNo, getInvObj().readIWRegression01("SN_IN_PC_053"));
		pressEnter(txt_serielNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		click(btn_serleNosProductConversionToProduct.replace("row", "4"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		explicitWait(btn_draftActionVerification, 30);
		click(btn_draftActionVerification);

		getInvObjReg().customizeLoadingDelay(header_drftedProductConversion, 40);

		click(btn_releaseCommon);
		explicitWait(header_releasedProductConversion, 30);
		if (isDisplayed(header_releasedProductConversion)) {
			writeTestResults("Verify user able to convert above Serial/Batches",
					"User should be able to convert above Serial/Batches",
					"User successfully convert above Serial/Batches", "pass");
		} else {
			writeTestResults("Verify user able to convert above Serial/Batches",
					"User should be able to convert above Serial/Batches", "User doesn't convert above Serial/Batches",
					"fail");

		}
	}

	/* IN_PC_054 */
	public void createBatchProductDispatchOnly_IN_PC_054() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 20);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 20);
		click(inventory_module);

		getInvObjReg().customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);

		InventoryAndWarehouseModuleRegression obj1 = new InventoryAndWarehouseModuleRegression();
		obj1.customizeLoadingDelay(btn_new_product, 8);
		Thread.sleep(1500);
		click(btn_new_product);

		String product_code = invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")
				+ "-Madhushan_DispatchOnly";

		sendKeys(txt_product_code, product_code);

		sendKeys(txt_product_desc, productDesription);

		selectText(dropdown_productGroupDispatchOnlyBatchProduct, productGroup);

		click(btn_menufacturerSearchDispatchOnlyBatchProduct);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, manufacturer);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		invObj.waitImplicit(lnk_resultMenufacturerDispatchOnlyBatchProduct, 6);
		invObj.sleepCusomized(lnk_resultMenufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacturerDispatchOnlyBatchProduct);

		selectIndex(dropdownDefoultUOMGroupDispatchOnlyBatchProduct, 1);

		selectIndex(dropdownDefoultUOMDispatchOnlyBatchProduct, 1);

		click(tab_deatilsDispatchOnlyBatchProduct);

		selectText(dropdown_outbound_costing_method, "FIFO");

		if (!isSelected(chk_batchProductDispatchOnlyBatchProduct)) {
			click(chk_batchProductDispatchOnlyBatchProduct);
		}
		selectIndex(dropdown_dispatchSelectionDispatchOnlyBatchProduct, 1);

		click(btn_draft2);

		click(btn_releese2);
		invObj.writeRegression01("DispatchOnlyBatchProduct_IN_PC_054", product_code, 196);
		Thread.sleep(3500);

	}

	public void createSerielProductDispatchOnly_IN_PC_054() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 20);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 20);
		click(inventory_module);

		getInvObjReg().customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);

		InventoryAndWarehouseModuleRegression obj1 = new InventoryAndWarehouseModuleRegression();
		obj1.customizeLoadingDelay(btn_new_product, 8);
		Thread.sleep(1500);
		click(btn_new_product);

		String product_code = invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")
				+ "-Madhushan_DispatchOnly";

		sendKeys(txt_product_code, product_code);

		sendKeys(txt_product_desc, productDesription);

		selectText(dropdown_productGroupDispatchOnlyBatchProduct, productGroup);

		click(btn_menufacturerSearchDispatchOnlyBatchProduct);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, manufacturer);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		invObj.waitImplicit(lnk_resultMenufacturerDispatchOnlyBatchProduct, 6);
		invObj.sleepCusomized(lnk_resultMenufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacturerDispatchOnlyBatchProduct);

		selectIndex(dropdownDefoultUOMGroupDispatchOnlyBatchProduct, 1);

		selectIndex(dropdownDefoultUOMDispatchOnlyBatchProduct, 1);

		click(tab_deatilsDispatchOnlyBatchProduct);
		selectText(dropdown_outbound_costing_method, "FIFO");
		if (!isSelected(btn_chkIsSeriel)) {
			click(btn_chkIsSeriel);
		}
		selectIndex(drop_dispatchOnlySelectionSeriel, 1);

		click(btn_draft2);

		click(btn_releese2);
		invObj.writeRegression01("DispatchOnlySerielProduct_IN_PC_054", product_code, 197);
		Thread.sleep(3500);

	}

	public void createSerielBatchProductDispatchOnly_IN_PC_054() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 20);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 20);
		click(inventory_module);

		getInvObjReg().customizeLoadingDelay(btn_product_info, 8);
		Thread.sleep(1000);
		click(btn_product_info);

		InventoryAndWarehouseModuleRegression obj1 = new InventoryAndWarehouseModuleRegression();
		obj1.customizeLoadingDelay(btn_new_product, 8);
		Thread.sleep(1500);
		click(btn_new_product);

		String product_code = invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")
				+ "-Madhushan_DispatchOnly";

		sendKeys(txt_product_code, product_code);

		sendKeys(txt_product_desc, productDesription);

		selectText(dropdown_productGroupDispatchOnlyBatchProduct, productGroup);

		click(btn_menufacturerSearchDispatchOnlyBatchProduct);
		sendKeys(txt_menufacturerDispatchOnlyBatchProduct, manufacturer);
		pressEnter(txt_menufacturerDispatchOnlyBatchProduct);
		invObj.waitImplicit(lnk_resultMenufacturerDispatchOnlyBatchProduct, 6);
		invObj.sleepCusomized(lnk_resultMenufacturerDispatchOnlyBatchProduct);
		doubleClick(lnk_resultMenufacturerDispatchOnlyBatchProduct);

		selectIndex(dropdownDefoultUOMGroupDispatchOnlyBatchProduct, 1);

		selectIndex(dropdownDefoultUOMDispatchOnlyBatchProduct, 1);

		click(tab_deatilsDispatchOnlyBatchProduct);
		selectText(dropdown_outbound_costing_method, "FIFO");
		if (!isSelected(chk_batchProductDispatchOnlyBatchProduct)) {
			click(chk_batchProductDispatchOnlyBatchProduct);
		}

		selectIndex(dropdown_dispatchSelectionDispatchOnlyBatchProduct, 1);

		if (!isSelected(btn_chkIsSeriel)) {
			click(btn_chkIsSeriel);
		}

		selectIndex(drop_dispatchOnlySelectionSeriel, 1);

		click(btn_draft2);

		click(btn_releese2);
		invObj.writeRegression01("DispatchOnlySerielBatchProduct_IN_PC_054", product_code, 198);
		Thread.sleep(3500);

	}

	public void purchaseOrderToInboundShipmentPurchaseInvoice_IN_PC_054() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 15);
		click(navigation_pane);

		getInvObjReg().customizeLoadingDelay(btn_procumentModule, 15);
		click(btn_procumentModule);

		getInvObjReg().customizeLoadingDelay(btn_purchaseOrder, 15);
		click(btn_purchaseOrder);

		click(btn_newPrurchaseOrder);

		click(btn_purchaseOrderToInboundShipmentToPurchaseInvoiceJourney);

		getInvObjReg().customizeLoadingDelay(btn_vendorSearchPurchaseOredr, 10);
		click(btn_vendorSearchPurchaseOredr);

		getInvObjReg().customizeLoadingDelay(txt_vendorSearch23, 10);
		sendKeys(txt_vendorSearch23, vendorCommonAutomationTenant);
		pressEnter(txt_vendorSearch23);
		Thread.sleep(2500);
		getInvObjReg().customizeLoadingDelay(lnk_resultVendorSeacrh23, 10);
		doubleClick(lnk_resultVendorSeacrh23);

		for (int i = 1; i < 4; i++) {
			String str_i = String.valueOf(i);

			String lookup_path = btn_lookupProductsMultipleProductGrids.replace("row", str_i);
			click(lookup_path);
			String product = "";

			if (i == 1) {
				product = "DispatchOnlyBatchProduct_IN_PC_054";
			} else if (i == 2) {
				product = "DispatchOnlySerielProduct_IN_PC_054";
			} else if (i == 3) {
				product = "DispatchOnlySerielBatchProduct_IN_PC_054";
			} else {
				System.out.println("Product row cannot identified.........");
			}

			Thread.sleep(2000);
			WebDriverWait wait = new WebDriverWait(driver, 20);
			WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(txt_productSearch)));
			Thread.sleep(2000);
			sendKeys(txt_productSearch, invObj.readIWRegression01(product));
			Thread.sleep(1000);
			pressEnter(txt_productSearch);
			Thread.sleep(3000);
			getInvObjReg().customizeLoadingDelay(
					result_cpmmonProduct.replace("product", invObj.readIWRegression01(product)), 20);
			doubleClick(result_cpmmonProduct.replace("product", invObj.readIWRegression01(product)));
			Thread.sleep(1500);

			String drop_warehouse = drop_warehouseMultipleProductGrid.replace("row", str_i).replace("tblName",
					"tblPurchaseOrder");
			String txt_qty = txt_quantityInGrid.replace("row", str_i).replace("tblName", "tblPurchaseOrder");
			String txt_cost = txt_unitPriceInGrid.replace("row", str_i).replace("tblName", "tblPurchaseOrder");

			Thread.sleep(2000);
			selectText(drop_warehouse, invObj.readIWRegression01("NewlyCreatedWarehouse") + " [Negombo]");
			sendKeys(txt_qty, quantity_IN_IO_072);
			sendKeys(txt_cost, unitPrice_IN_IO_072);

			getInvObjReg().customizeLoadingDelay(btn_addNewRow, 10);
			click(btn_addNewRow);
		}

		click(btn_deleteEmotyRowStockAdjustment.replace("tblStockAdj", "tblPurchaseOrder").replace("8", "4"));

		click(btn_checkoutPurchaaseOrdeer);

		click(btn_draft2);

		getInvObjReg().customizeLoadingDelay(btn_releese2, 10);
		click(btn_releese2);
		getInvObjReg().customizeLoadingDelay(btn_goToPageLink, 10);
		click(btn_goToPageLink);
		switchWindow();
		getInvObjReg().customizeLoadingDelay(btn_checkout2, 15);
		Thread.sleep(2000);
		click(btn_checkout2);

		Thread.sleep(300);
		click(btn_draft2);
		Thread.sleep(4000);
		getInvObjReg().customizeLoadingDelay(btn_releese2, 20);
		String inbound_shipment_doc = getAttribute(lbl_relesedInboundShipmentNoBarcodeGeneratorPage, "orderid");
		invObj.writeRegression01("inbound_shipment_IN_PC_054", inbound_shipment_doc, 199);
		click(btn_releese2);
		Thread.sleep(4000);

		getInvObjReg().customizeLoadingDelay(btn_goToPageLink, 20);
		click(btn_goToPageLink);
		switchWindow();
		getInvObjReg().customizeLoadingDelay(btn_checkout2, 15);
		Thread.sleep(2000);
		click(btn_checkout2);

		Thread.sleep(300);
		click(btn_draft2);

		getInvObjReg().customizeLoadingDelay(btn_releese2, 15);
		click(btn_releese2);
		getInvObjReg().customizeLoadingDelay(header_releasedPurchaseInvoice, 40);
		trackCode = getText(lbl_creationsDocNo);
		if (isDisplayed(header_releasedPurchaseInvoice)) {
			writeTestResults("Verify user able to complete purchase order journey with dispatch only products",
					"User should be able to complete purchase order journey with dispatch only products",
					"User successfully complete purchase order journey with dispatch only products", "pass");
		} else {
			writeTestResults("Verify user able to complete purchase order journey with dispatch only products",
					"User should be able to complete purchase order journey with dispatch only products",
					"User doesn't complete purchase order journey with dispatch only products", "fail");
		}

	}

	public void genarateBarcode_IN_PC_054() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().createNewWarehouse();
		createBatchProductDispatchOnly_IN_PC_054();
		createSerielProductDispatchOnly_IN_PC_054();
		createSerielBatchProductDispatchOnly_IN_PC_054();
		invObj.removeShipmentAcceptance();
		purchaseOrderToInboundShipmentPurchaseInvoice_IN_PC_054();

		getInvObjReg().customizeLoadingDelay(navigation_pane, 40);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 20);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 20);
		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_barcodeGenerator, 20);
		click(btn_barcodeGenerator);
		getInvObjReg().customizeLoadingDelay(dropdown_warehouseBarcodeGeneratorPage, 20);
		Actions action = new Actions(driver);
		WebElement we = driver.findElement(By.xpath(icon_inboundShipmentBarcodeGeneratorPage));
		action.moveToElement(we).build().perform();
		click(icon_inboundShipmentBarcodeGeneratorPage);

		selectText(dropdown_warehouseBarcodeGeneratorPage,
				invObj.readIWRegression01("NewlyCreatedWarehouse") + " [Negombo]");

		click(span_dateRangeBarcodeGeneratorPage);

		click(btn_todayDateSetBarcodeGeneratorPage);

		sendKeys(txt_searchBarcodeGeneratorPage, getInvObj().readIWRegression01("inbound_shipment_IN_PC_054"));

		click(btn_docSaecrhBarcodeGeneratorPage);

		getInvObjReg().customizeLoadingDelay(btn_genarateProductBarcode.replace("product",
				getInvObj().readIWRegression01("DispatchOnlyBatchProduct_IN_PC_054")), 40);
		click(btn_genarateProductBarcode.replace("product",
				getInvObj().readIWRegression01("DispatchOnlyBatchProduct_IN_PC_054")));

		selectText(dropdown_batchBookGenerateBarcodeGeneratorPage, "BG");

		sendKeys(txt_lotGenerateBarcodeGeneratorPage,
				invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));

		click(btn_expiryDateBArcodeGenarate);

		Thread.sleep(2000);
		click(btn_dateNextBArcodeGenarator);
		Thread.sleep(2000);
		click(btn_day28barcodeGenarator);

		click(btn_generateGenerateWindowBarcodeGeneratorPage);
		getInvObjReg().customizeLoadingDelay(btn_updateGenerateWindowBarcodeGeneratorPage, 10);
		click(btn_updateGenerateWindowBarcodeGeneratorPage);

		getInvObjReg().customizeLoadingDelay(btn_viewProductBarcode.replace("product",
				getInvObj().readIWRegression01("DispatchOnlyBatchProduct_IN_PC_054")), 12);
		click(btn_viewProductBarcode.replace("product",
				getInvObj().readIWRegression01("DispatchOnlyBatchProduct_IN_PC_054")));

		boolean flag_barcode_gen_pro1 = false;
		boolean flag_barcode_gen_pro2 = false;
		boolean flag_barcode_gen_pro3 = false;

		if (isDisplayed(btn_printBarcodeGenarate)) {
			flag_barcode_gen_pro1 = true;
		}

		click(btn_backBarcodeGenSerielBatchCaptureWindow);

		getInvObjReg().customizeLoadingDelay(btn_genarateProductBarcode.replace("product",
				getInvObj().readIWRegression01("DispatchOnlySerielProduct_IN_PC_054")), 40);
		click(btn_genarateProductBarcode.replace("product",
				getInvObj().readIWRegression01("DispatchOnlySerielProduct_IN_PC_054")));

		selectText(select_serelBookBarcodeGenarate, "BG");

		sendKeys(txt_lotGenerateBarcodeGeneratorPage,
				invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));

		click(btn_expiryDateBArcodeGenarate);

		Thread.sleep(2000);
		click(btn_dateNextBArcodeGenarator);
		Thread.sleep(2000);
		click(btn_day28barcodeGenarator);

		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(btn_generateGenerateWindowBarcodeGeneratorPage, 20);
		click(btn_generateGenerateWindowBarcodeGeneratorPage);
		getInvObjReg().customizeLoadingDelay(btn_updateGenerateWindowBarcodeGeneratorPage, 10);
		click(btn_updateGenerateWindowBarcodeGeneratorPage);

		getInvObjReg().customizeLoadingDelay(btn_viewProductBarcode.replace("product",
				getInvObj().readIWRegression01("DispatchOnlySerielProduct_IN_PC_054")), 40);
		click(btn_viewProductBarcode.replace("product",
				getInvObj().readIWRegression01("DispatchOnlySerielProduct_IN_PC_054")));
		getInvObjReg().customizeLoadingDelay(btn_printBarcodeGenarate, 12);
		if (isDisplayed(btn_printBarcodeGenarate)) {
			flag_barcode_gen_pro2 = true;
		}

		String serialNoSerialSpecific = getText(div_firstSerialBarcodeGeanarateView);
		getInvObj().writeRegression01("SN_SerialSpecificDispatchOnly_IN_PC_054", serialNoSerialSpecific, 200);

		click(btn_backBarcodeGenSerielBatchCaptureWindow);
		getInvObjReg().customizeLoadingDelay(btn_genarateProductBarcode.replace("product",
				getInvObj().readIWRegression01("DispatchOnlySerielBatchProduct_IN_PC_054")), 15);
		Thread.sleep(3000);
		click(btn_genarateProductBarcode.replace("product",
				getInvObj().readIWRegression01("DispatchOnlySerielBatchProduct_IN_PC_054")));

		Thread.sleep(1000);
		getInvObjReg().customizeLoadingDelay(select_serelBookBarcodeGenarate, 15);
		selectText(select_serelBookBarcodeGenarate, "BG");

		selectText(dropdown_batchBookGenerateBarcodeGeneratorPage, "BG");

		sendKeys(txt_lotGenerateBarcodeGeneratorPage,
				invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));

		click(btn_expiryDateBArcodeGenarate);

		Thread.sleep(2000);
		click(btn_dateNextBArcodeGenarator);

		Thread.sleep(2000);
		click(btn_day28barcodeGenarator);

		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(btn_generateGenerateWindowBarcodeGeneratorPage, 20);
		click(btn_generateGenerateWindowBarcodeGeneratorPage);
		getInvObjReg().customizeLoadingDelay(btn_updateGenerateWindowBarcodeGeneratorPage, 10);
		click(btn_updateGenerateWindowBarcodeGeneratorPage);

		getInvObjReg().customizeLoadingDelay(btn_viewProductBarcode.replace("product",
				getInvObj().readIWRegression01("DispatchOnlySerielBatchProduct_IN_PC_054")), 12);
		click(btn_viewProductBarcode.replace("product",
				getInvObj().readIWRegression01("DispatchOnlySerielBatchProduct_IN_PC_054")));

		if (isDisplayed(btn_printBarcodeGenarate)) {
			flag_barcode_gen_pro3 = true;
		}

		String serialNoSerialBatchSpecific = getText(div_firstSerialBarcodeGeanarateView);
		getInvObj().writeRegression01("SN_SerialBatchSpecificDispatchOnly_IN_PC_054", serialNoSerialBatchSpecific, 201);

		click(btn_backBarcodeGenSerielBatchCaptureWindow);

		if (flag_barcode_gen_pro1 && flag_barcode_gen_pro2 && flag_barcode_gen_pro3) {
			writeTestResults("Verify user able to generate Serial/Batches via Barcode Generator",
					"User should be able to generate Serial/Batches via Barcode Generator",
					"User successfully generate Serial/Batches via Barcode Generator", "pass");
		} else {
			writeTestResults("Verify user able to generate Serial/Batches via Barcode Generator",
					"User should be able to generate Serial/Batches via Barcode Generator",
					"User doesn't generate Serial/Batches via Barcode Generator", "fail");
		}

	}

	public void productConversion_IN_PC_054() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, getInvObj().readIWRegression01("NewlyCreatedWarehouse") + " [Negombo]");
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(getInvObj().readIWRegression01("NewlyCreatedWarehouse") + " [Negombo]")) {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User allow to fill summary details", "pass");
		} else {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User not allow to fill summary details", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch,
				getInvObj().readIWRegression01("DispatchOnlyBatchProduct_IN_PC_054"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(result_cpmmonProduct.replace("product",
				getInvObj().readIWRegression01("DispatchOnlyBatchProduct_IN_PC_054")), 20);
		doubleClick(result_cpmmonProduct.replace("product",
				getInvObj().readIWRegression01("DispatchOnlyBatchProduct_IN_PC_054")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(div_productOnGridProductConversation.replace("product",
				getInvObj().readIWRegression01("DispatchOnlyBatchProduct_IN_PC_054")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("product",
				getInvObj().readIWRegression01("DispatchOnlyBatchProduct_IN_PC_054")))) {
			writeTestResults("Verify user allow to select Dispacth Only Batch Product product as \"From Product\"",
					"User should allow to select Dispacth Only Batch Product product as \"From Product\"",
					"User allow to select Dispacth Only Batch Product product as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select Dispacth Only Batch Product product as \"From Product\"",
					"User should allow to select Dispacth Only Batch Product product as \"From Product\"",
					"User not allow to select Dispacth Only Batch Product product as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerirlFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerirlFifo")))) {
			writeTestResults("Verify user allow to select serial specific product as \"To Product\"",
					"User should allow to select serial specific product as \"To Product\"",
					"User allow to select serial specific product as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select serial specific product as \"To Product\"",
					"User should allow to select serial specific product as \"To Product\"",
					"User not allow to select serial specific product as \"To Product\"", "fail");
		}

		sendKeys(txt_quantityToProductProductConversion, "5");

		click(btn_addNewRowProductConversion);

		click(btn_productLookupRowReplaceProductConversion.replace("row", "2"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch,
				getInvObj().readIWRegression01("DispatchOnlySerielProduct_IN_PC_054"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(result_cpmmonProduct.replace("product",
				getInvObj().readIWRegression01("DispatchOnlySerielProduct_IN_PC_054")), 20);
		doubleClick(result_cpmmonProduct.replace("product",
				getInvObj().readIWRegression01("DispatchOnlySerielProduct_IN_PC_054")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(div_productOnGridProductConversation.replace("product",
				getInvObj().readIWRegression01("DispatchOnlySerielProduct_IN_PC_054")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("product",
				getInvObj().readIWRegression01("DispatchOnlySerielProduct_IN_PC_054")))) {
			writeTestResults("Verify user allow to select Dispatch Only Serial Product as \"From Product\"",
					"User should allow to select Dispatch Only Serial Product as \"From Product\"",
					"User allow to select Dispatch Only Serial Product as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select Dispatch Only Serial Product as \"From Product\"",
					"User should allow to select Dispatch Only Serial Product as \"From Product\"",
					"User not allow to select Dispatch Only Serial Product as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "2"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "2"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerirlFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerirlFifo")))) {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO as \"To Product\"",
					"User allow to select seriel FIFO as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO as \"To Product\"",
					"User not allow to select seriel FIFO as \"To Product\"", "fail");
		}

		sendKeys(txt_qtyToProductProductConcersionRowReplace.replace("row", "2"), "5");

		click(btn_addNewRowProductConversion);

		click(btn_productLookupRowReplaceProductConversion.replace("row", "3"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch,
				getInvObj().readIWRegression01("DispatchOnlySerielBatchProduct_IN_PC_054"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(result_cpmmonProduct.replace("product",
				getInvObj().readIWRegression01("DispatchOnlySerielBatchProduct_IN_PC_054")), 20);
		doubleClick(result_cpmmonProduct.replace("product",
				getInvObj().readIWRegression01("DispatchOnlySerielBatchProduct_IN_PC_054")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(div_productOnGridProductConversation.replace("product",
				getInvObj().readIWRegression01("DispatchOnlySerielBatchProduct_IN_PC_054")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("product",
				getInvObj().readIWRegression01("DispatchOnlySerielBatchProduct_IN_PC_054")))) {
			writeTestResults("Verify user allow to select Dispatch Only Serial Batch Product as \"From Product\"",
					"User should allow to select Dispatch Only Serial Batch Product as \"From Product\"",
					"User allow to select Dispatch Only Serial Batch Product as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select Dispatch Only Serial Batch Product as \"From Product\"",
					"User should allow to select Dispatch Only Serial Batch Product as \"From Product\"",
					"User not allow to select Dispatch Only Serial Batch Product as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "3"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "3"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerirlFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")))) {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO as \"To Product\"",
					"User allow to select seriel FIFO as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO batch as \"To Product\"",
					"User not allow to select seriel FIFO as \"To Product\"", "fail");
		}

		sendKeys(txt_qtyToProductProductConcersionRowReplace.replace("row", "3"), "5");

		/* capture row 01 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "1"));
		explicitWait(btn_seraialBatchCaptureDocIconProductConversion, 10);
		click(btn_seraialBatchCaptureDocIconProductConversion);
		explicitWait(chk_batchSelectionProductConversion, 10);
		click(chk_batchSelectionProductConversion);
		click(btn_commoApplyLast);

		click(btn_serleNosProductConversionToProduct.replace("row", "1"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		String sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		/* capture row 02 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "2"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		explicitWait(txt_serielCountProductConversion, 20);
		sendKeys(txt_serielCountProductConversion, "10");
		sendKeys(txt_serielNo, getInvObj().readIWRegression01("SN_SerialSpecificDispatchOnly_IN_PC_054"));
		pressEnter(txt_serielNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		click(btn_serleNosProductConversionToProduct.replace("row", "2"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		/* capture row 03 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "3"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		explicitWait(txt_serielCountProductConversion, 20);
		sendKeys(txt_serielCountProductConversion, "10");
		sendKeys(txt_serielNo, getInvObj().readIWRegression01("SN_SerialBatchSpecificDispatchOnly_IN_PC_054"));
		pressEnter(txt_serielNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		click(btn_serleNosProductConversionToProduct.replace("row", "3"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		explicitWait(btn_draftActionVerification, 30);
		click(btn_draftActionVerification);

		getInvObjReg().customizeLoadingDelay(header_drftedProductConversion, 40);

		click(btn_releaseCommon);
		explicitWait(header_releasedProductConversion, 30);
		trackCode = getText(lbl_creationsDocNo);

		if (isDisplayed(header_releasedProductConversion)) {
			writeTestResults("Verify user able to convert the Serials/Batches which generated via Barcode Generator",
					"User should be able to convert the Serials/Batches which generated via Barcode Generator",
					"User successfully convert the Serials/Batches which generated via Barcode Generator", "pass");
		} else {
			writeTestResults("Verify user able to convert the Serials/Batches which generated via Barcode Generator",
					"User should be able to convert the Serials/Batches which generated via Barcode Generator",
					"User doesn't convert the Serials/Batches which generated via Barcode Generator", "fail");

		}
	}

	/* IN_PC_055 */
	public void outboundLoanOrder_IN_PC_055() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().createNewWarehouse();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 20);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 20);
		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_outboundLoanOrder, 20);
		click(btn_outboundLoanOrder);
		getInvObjReg().customizeLoadingDelay(btn_newOutboundLoanOrder, 20);
		click(btn_newOutboundLoanOrder);
		getInvObjReg().customizeLoadingDelay(btn_outboundLoanOrderToOutboundShipmentJourney, 20);
		click(btn_outboundLoanOrderToOutboundShipmentJourney);
		getInvObjReg().customizeLoadingDelay(btn_customerAccountSearchLookupILO, 20);
		click(btn_customerAccountSearchLookupILO);
		getInvObjReg().customizeLoadingDelay(txt_accountSearchILO, 20);
		sendKeys(txt_accountSearchILO, customerAccount_IN_TO_070);
		pressEnter(txt_accountSearchILO);
		Thread.sleep(3000);
		getInvObjReg().customizeLoadingDelay(lnk_resultCustomerAccount, 20);
		doubleClick(lnk_resultCustomerAccount);
		getInvObjReg().customizeLoadingDelay(btn_billingAddressILO, 20);
		click(btn_billingAddressILO);
		sendKeys(txt_billingAddressILO, address1ILO);
		sendKeys(txt_shippingAddressILO, address2ILO);
		click(btn_applyAddressILO);
		selectIndex(dropdown_salesUnitILO, 1);

		/* Product add to the grid */
		int j = 0;
		for (int i = 1; i < 5; i++) {
			String str_i = String.valueOf(i);

			String lookup_path = btn_productLookupInterDeprtmentTransferOrderForLoop.replace("row", str_i)
					.replace("column", "8").replace("tblStockAdj", "tblLoanOrder");
			getInvObjReg().customizeLoadingDelay(lookup_path, 15);
			click(lookup_path);

			String product = "";

			if (i == 1) {
				product = "BatchSpecific";
			} else if (i == 2) {
				product = "BatchFifo";
			} else if (i == 3) {
				product = "Lot";
			} else if (i == 4) {
				product = "SerielSpecific";
			} else {
				System.out.println("Product type cannot identified.........");
			}
			Thread.sleep(2000);
			getInvObjReg().customizeLoadingDelay(txt_productSearch, 20);
			sendKeys(txt_productSearch, invObj.readTestCreation(product));
			pressEnter(txt_productSearch);
			Thread.sleep(4000);
			getInvObjReg().customizeLoadingDelay(btn_resultProduct, 20);
			doubleClick(btn_resultProduct);
			Thread.sleep(1500);
			String txt_qty = txt_qtyProductGridInboundLoanOrder.replace("row", str_i);

			sendKeys(txt_qty, qtyInterDepartmentTransferOrder);

			getInvObjReg().customizeLoadingDelay(btn_addNewRow, 20);
			click(btn_addNewRow);
		}

		click(btn_deleteLastRowOfTheGrid);

		click(btn_tabInboundLoanOrder);
		selectText(drop_warehouseInternelOrderEmployeeRequest, warehouseInternelOrderEmployeeRequest);

		click(btn_detailsTabILO);

		click(chkbox_underDeliveryILO);
		click(chkbox_partielDeliveryILO);

		click(btn_reqSlipDateILO);
		click(btn_reqSlipDate17ILO);

		click(btn_conSlipDateILO);
		click(btn_conSlipDate18ILO);

		click(btn_reqReciptDateILO);
		click(btn_reqReciptDate19ILO);

		click(btn_conReciptDateILO);
		click(btn_conReciptDate20ILO);

		click(btn_draftActionVerification);
		getInvObjReg().customizeLoadingDelay(btn_releseILO, 20);
		click(btn_releseILO);

	}

	public void outboundShipmentAndInboundShipment_IN_PC_055() throws Exception {
		getInvObjReg().customizeLoadingDelay(btn_goToNextTaskILO, 20);
		click(btn_goToNextTaskILO);
		switchWindow();
		getInvObjReg().customizeLoadingDelay(btn_draft, 20);
		click(btn_draft);

		getInvObjReg().customizeLoadingDelay(brn_serielBatchCapture, 20);
		click(brn_serielBatchCapture);

		/* Product 01 */
		getInvObjReg().customizeLoadingDelay(btn_captureItem1, 20);
		click(btn_captureItem1);

		click(btn_capturePageDocIcon);

		click(chk_availableProductsTransferOrderOutboundShipment.replace("row", "1"));

		String qty = getText(lbl_qtyBatchSpecifificCapturePage);
		String qty_capture = getText(lbl_availQuantityProductCapturuPage);

		int qty_int = Integer.parseInt(qty);
		int qty_capture_int = Integer.parseInt(qty_capture);

		int i = 2;
		while (qty_int > qty_capture_int) {
			String i_str = String.valueOf(i);
			click(chk_deleteItemCapturePage);
			click(btn_deleteRowProductCaptururePage);
			click(btn_capturePageDocIcon);
			click(chk_availableProductsTransferOrderOutboundShipment.replace("row", i_str));
			Thread.sleep(2000);
			qty_capture = getText(lbl_availQuantityProductCapturuPage);
			qty_capture_int = Integer.parseInt(qty_capture);
		}

		click(btn_updateCapture);

		/* Product 04 */
		String item_xpath = btn_itemProductCapturePage.replace("_product_number", "4");
		getInvObjReg().customizeLoadingDelay(item_xpath, 20);
		click(item_xpath);
		click(chk_productCaptureRange);

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$('#attrSlide-txtSerialCount').val(" + qtyTransferOrder + ")");

		String lot4 = invObj.getOngoinfSerirlSpecificSerielNumber(10);
		invObj.writeRegression01("SN_IN_PC_055", lot4, 107);
		String final_lot_script = "$('#attrSlide-txtSerialFrom').val(\"" + lot4 + "\")";
		jse.executeScript(final_lot_script);

		pressEnter(txt_seriealProductCapture);

		click(btn_updateCapture);

		Thread.sleep(2000);
		String pro1 = getAttribute(item_xpath, "class");
		if (!pro1.equals("fli-number green-vackground-fli-number")) {
			Thread.sleep(6000);
			pro1 = getAttribute(item_xpath, "class");
		}
		if (pro1.equals("fli-number green-vackground-fli-number")) {

		} else {
			writeTestResults("Verify user can capture 'Seriel/Batch'", "User should be able to capture 'Seriel/Batch'",
					"User couldn't capture 'Seriel/Batch'", "fail");
		}
		getInvObjReg().customizeLoadingDelay(btn_backButtonUpdateCapture, 15);
		click(btn_backButtonUpdateCapture);
		getInvObjReg().customizeLoadingDelay(btn_releese2, 20);
		click(btn_releese2);
		getInvObjReg().customizeLoadingDelay(lbl_relesedConfirmation, 25);
		if (isDisplayed(lbl_relesedConfirmation)) {
			writeTestResults("Verify user able to outbound serial/batches via Outbound Loan Order",
					"User should be able to outbound serial/batches via Outbound Loan Order",
					"User successfully outbound serial/batches via Outbound Loan Order", "pass");
		} else {
			writeTestResults("Verify user able to outbound serial/batches via Outbound Loan Order",
					"User should be able to outbound serial/batches via Outbound Loan Order",
					"User doesn't outbound serial/batches via Outbound Loan Order", "fail");
		}

		switchWindow();
		pageRefersh();
		getInvObjReg().customizeLoadingDelay(btn_action, 30);
		click(btn_action);
		getInvObjReg().customizeLoadingDelay(btn_convertToInboundShipment, 20);
		click(btn_convertToInboundShipment);
		getInvObjReg().customizeLoadingDelay(
				drop_warehouseMultipleProductGrid.replace("tblName", "tblShipmentDetails").replace("row", "1"), 25);

		for (int j = 1; j < 5; j++) {
			String str_j = String.valueOf(j);
			selectText(drop_warehouseMultipleProductGrid.replace("tblName", "tblShipmentDetails").replace("row", str_j),
					invObj.readIWRegression01("NewlyCreatedWarehouse") + " [Negombo]");
		}
		click(btn_checkout);
		Thread.sleep(2000);
		click(btn_draftActionVerification);
		getInvObjReg().customizeLoadingDelay(brn_serielBatchCapture, 20);
		click(brn_serielBatchCapture);
		getInvObjReg().customizeLoadingDelay(btn_itemProductCapturePage.replace("_product_number", "1"), 20);
		for (int j = 1; j < 5; j++) {
			String str_j = String.valueOf(j);
			String item_xpath_inbound = btn_itemProductCapturePage.replace("_product_number", str_j);
			Actions action1 = new Actions(driver);
			WebElement we1 = driver.findElement(By.xpath(item_xpath_inbound));
			action1.moveToElement(we1).build().perform();
			click(item_xpath_inbound);

			click(btn_updateCapture);
		}
		getInvObjReg().customizeLoadingDelay(btn_backButtonUpdateCapture, 15);
		click(btn_backButtonUpdateCapture);
		getInvObjReg().customizeLoadingDelay(btn_releese2, 20);
		click(btn_releese2);
		getInvObjReg().customizeLoadingDelay(lbl_relesedConfirmation, 25);
		if (isDisplayed(lbl_relesedConfirmation)) {
			writeTestResults("Verify user able to inbound same Serials/Batches outbound via Outbound Loan Order",
					"User should be able to inbound same Serials/Batches outbound via Outbound Loan Order",
					"User successfully inbound same Serials/Batches outbound via Outbound Loan Order", "pass");
		} else {
			writeTestResults("Verify user able to inbound same Serials/Batches outbound via Outbound Loan Order",
					"User should be able to inbound same Serials/Batches outbound via Outbound Loan Order",
					"User doesn't inbound Serials/Batches outbound via Outbound Loan Order", "fail");
		}

	}

	public void productConversion_IN_PC_055() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, getInvObj().readIWRegression01("NewlyCreatedWarehouse") + " [Negombo]");
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(getInvObj().readIWRegression01("NewlyCreatedWarehouse") + " [Negombo]")) {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User allow to fill summary details", "pass");
		} else {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User not allow to fill summary details", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("Lot"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(result_cpmmonProduct.replace("product", invObj.readTestCreation("Lot")),
				20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("Lot")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("Lot")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("product", invObj.readTestCreation("Lot")))) {
			writeTestResults("Verify user allow to select Lot product as \"From Product\"",
					"User should allow to select Lot product as \"From Product\"",
					"User allow to select Lot product as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select Lot product as \"From Product\"",
					"User should allow to select Lot product as \"From Product\"",
					"User not allow to select Lot product as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerirlFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerirlFifo")))) {
			writeTestResults("Verify user allow to select serial specific product as \"To Product\"",
					"User should allow to select serial specific product as \"To Product\"",
					"User allow to select serial specific product as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select serial specific product as \"To Product\"",
					"User should allow to select serial specific product as \"To Product\"",
					"User not allow to select serial specific product as \"To Product\"", "fail");
		}

		sendKeys(txt_quantityToProductProductConversion, "5");

		click(btn_addNewRowProductConversion);

		click(btn_productLookupRowReplaceProductConversion.replace("row", "2"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchFifo")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchFifo")))) {
			writeTestResults("Verify user allow to select batch FIFO as \"From Product\"",
					"User should allow to select batch FIFO as \"From Product\"",
					"User allow to select batch FIFO as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select batch FIFO as \"From Product\"",
					"User should allow to select batch FIFO as \"From Product\"",
					"User not allow to select batch FIFO as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "2"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "2"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerirlFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerirlFifo")))) {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO as \"To Product\"",
					"User allow to select seriel FIFO as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO as \"To Product\"",
					"User not allow to select seriel FIFO as \"To Product\"", "fail");
		}

		sendKeys(txt_qtyToProductProductConcersionRowReplace.replace("row", "2"), "5");

		click(btn_addNewRowProductConversion);

		click(btn_productLookupRowReplaceProductConversion.replace("row", "3"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select batch specific as \"From Product\"",
					"User should allow to select batch specific as \"From Product\"",
					"User allow to select batch specific as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select batch specific as \"From Product\"",
					"User should allow to select batch specific as \"From Product\"",
					"User not allow to select batch specific as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "3"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "3"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerirlFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")))) {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO as \"To Product\"",
					"User allow to select seriel FIFO as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO batch as \"To Product\"",
					"User not allow to select seriel FIFO as \"To Product\"", "fail");
		}

		sendKeys(txt_qtyToProductProductConcersionRowReplace.replace("row", "3"), "5");

		click(btn_addNewRowProductConversion);

		click(btn_productLookupRowReplaceProductConversion.replace("row", "4"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify user allow to select serial specific as \"From Product\"",
					"User should allow to select serial specific as \"From Product\"",
					"User allow to select serial specific as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select serial specific as \"From Product\"",
					"User should allow to select serial specific as \"From Product\"",
					"User not allow to select serial specific as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "4"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "4"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerirlFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")))) {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO as \"To Product\"",
					"User allow to select seriel FIFO as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO batch as \"To Product\"",
					"User not allow to select seriel FIFO as \"To Product\"", "fail");
		}

		sendKeys(txt_qtyToProductProductConcersionRowReplace.replace("row", "4"), "5");

		/* capture row 01 */
		click(btn_serleNosProductConversionToProduct.replace("row", "1"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		String sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		/* capture row 02 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "2"));
		explicitWait(btn_seraialBatchCaptureDocIconProductConversion, 10);
		click(btn_seraialBatchCaptureDocIconProductConversion);
		explicitWait(chk_batchSelectionProductConversion, 10);
		click(chk_batchSelectionProductConversion);
		click(btn_commoApplyLast);

		click(btn_serleNosProductConversionToProduct.replace("row", "2"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		/* capture row 03 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "3"));
		explicitWait(btn_seraialBatchCaptureDocIconProductConversion, 10);
		click(btn_seraialBatchCaptureDocIconProductConversion);
		explicitWait(chk_batchSelectionProductConversion, 10);
		click(chk_batchSelectionProductConversion);
		click(btn_commoApplyLast);

		click(btn_serleNosProductConversionToProduct.replace("row", "3"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		/* capture row 04 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "4"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		explicitWait(txt_serielCountProductConversion, 20);
		sendKeys(txt_serielCountProductConversion, "10");
		sendKeys(txt_serielNo, getInvObj().readIWRegression01("SN_IN_PC_055"));
		pressEnter(txt_serielNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		click(btn_serleNosProductConversionToProduct.replace("row", "4"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		explicitWait(btn_draftActionVerification, 30);
		click(btn_draftActionVerification);

		getInvObjReg().customizeLoadingDelay(header_drftedProductConversion, 40);

		click(btn_releaseCommon);
		explicitWait(header_releasedProductConversion, 30);
		if (isDisplayed(header_releasedProductConversion)) {
			writeTestResults("Verify user able to convert above Serial/Batches",
					"User should be able to convert above Serial/Batches",
					"User successfully convert above Serial/Batches", "pass");
		} else {
			writeTestResults("Verify user able to convert above Serial/Batches",
					"User should be able to convert above Serial/Batches", "User doesn't convert above Serial/Batches",
					"fail");

		}
	}

	/* IN_PC_056 */
	public void warehouseCreationWithRacks_IN_PC_056() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 10);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_warehouseInfo, 10);
		click(btn_warehouseInfo);
		getInvObjReg().customizeLoadingDelay(btn_newWarehouse, 10);
		click(btn_newWarehouse);
		String warehouse_create = invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "Ware";
		getInvObjReg().customizeLoadingDelay(txt_warehouseCode, 10);
		sendKeys(txt_warehouseCode, warehouse_create);
		sendKeys(txt_warehouseName, warehouseName);

		selectText(dropdown_businessUnit, businessUnit);

		if (!isSelected(checkbox_isAllocation)) {
			click(checkbox_isAllocation);
		}

		selectText(dropdown_qurantineWarehouse, qurantineWarehouse);

		if (!isSelected(checkbox_autoLot)) {
			click(checkbox_autoLot);
		}

		selectText(dropdown_lotBook, lotBookWarehouse);
		click(btn_relese);
		Thread.sleep(3000);
		trackCode = warehouse_create;
		getInvObjReg().customizeLoadingDelay(btn_draft, 15);
		click(btn_draft);
		Thread.sleep(3000);
		getInvObjReg().customizeLoadingDelay(btn_releaseCommon, 15);
		click(btn_releaseCommon);

		String rack01 = invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "").substring(8) + "-Rack-01";
		String rack02 = invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "").substring(8) + "-Rack-02";
		getInvObjReg().customizeLoadingDelay(txt_rackName, 15);
		Thread.sleep(1500);
		sendKeys(txt_rackName, rack01);
		click(btn_addRackPlusMark);
		Thread.sleep(1500);
		pageScrollDown();
		sendKeys(txt_rackName, rack02);
		click(btn_updateRacks);
		Thread.sleep(5000);
		invObj.writeRegression01("NewlyCreatedWarehouse_IN_PC_056", warehouse_create, 184);
		invObj.writeRegression01("Rack01_IN_PC_056", rack01, 185);
		invObj.writeRegression01("Rack02_IN_PC_056", rack02, 186);
		pageRefersh();
	}

	public void stockAdjustmentForRack_IN_PC_056() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);
		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_stockAdjustment, 10);
		click(btn_stockAdjustment);
		getInvObjReg().customizeLoadingDelay(btn_newStockAdjustment, 30);
		click(btn_newStockAdjustment);
		getInvObjReg().customizeLoadingDelay(txt_descStockAdj, 30);
		sendKeys(txt_descStockAdj, descStockAdj);

		getInvObjReg().partSelectText(dropdown_warehouseStockAdj,
				invObj.readIWRegression01("NewlyCreatedWarehouse_IN_PC_056"));

		/* Product add to the grid */
		for (int i = 1; i < 5; i++) {
			String str_i = String.valueOf(i);
			click(btn_productLookupStockAdjForLoop.replace("row", str_i));
			String product = "";

			if (i == 1) {
				product = "BatchSpecific";
			} else if (i == 2) {
				product = "BatchFifo";
			} else if (i == 3) {
				product = "Lot";
			} else if (i == 4) {
				product = "SerielSpecific";
			} else {
				System.out.println("Product type cannot identified.........");
			}

			invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation(product));
			pressEnter(txt_productSearch);
			getInvObjReg().customizeLoadingDelay(
					result_cpmmonProduct.replace("product", invObj.readTestCreation(product)), 20);
			doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation(product)));
			Thread.sleep(1500);

			String txt_qty = txt_qtyProductGridStockAdjustment.replace("row", str_i);
			String txt_cost = txt_costProductGridStockAdjustment.replace("row", str_i);
			Thread.sleep(1500);
			sendKeys(txt_qty, productQuantity_IN_IO_074);
			selectText(drop_rackSelectStockAdjustment, invObj.readIWRegression01("Rack01_IN_PC_056"));
			sendKeys(txt_cost, productCost1);

			getInvObjReg().customizeLoadingDelay(btn_addNewRow, 10);
			click(btn_addNewRow);
		}

		click(btn_deleteLastRowOfTheGrid);

		getInvObjReg().customizeLoadingDelay(btn_draftActionVerification, 20);
		click(btn_draftActionVerification);
		getInvObjReg().customizeLoadingDelay(brn_serielBatchCapture, 20);
		click(brn_serielBatchCapture);

		/* Product 01 */
		String item_xpath;
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "1");
		getInvObjReg().customizeLoadingDelay(item_xpath, 10);
		click(item_xpath);
		sendKeys(txt_batchNumberCaptureILOOutboundShipment,
				invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_lotNumberStockAdjustment, invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_expiryDateStockAdjustmentProductCapture);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_lotNumberStockAdjustment);
		Thread.sleep(1000);
		getInvObjReg().customizeLoadingDelay(btn_updateCapture, 50);
		click(btn_updateCapture);

		/* Product 02 */
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "2");
		getInvObjReg().customizeLoadingDelay(item_xpath, 10);
		click(item_xpath);

		sendKeys(txt_batchNumberCaptureILOOutboundShipment,
				invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_lotNumberStockAdjustment, invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_expiryDateStockAdjustmentProductCapture);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_lotNumberStockAdjustment);
		Thread.sleep(1000);
		getInvObjReg().customizeLoadingDelay(btn_updateCapture, 50);
		click(btn_updateCapture);

		/* Product 04 */
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "4");
		getInvObjReg().customizeLoadingDelay(item_xpath, 10);
		click(item_xpath);

		click(chk_productCaptureRange);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$('#attrSlide-txtSerialCount').val(10)");

		String lot4 = invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "");
		invObj.writeRegression01("SN_IN_PC_056", lot4, 187);
		String final_lot_script = "$('#attrSlide-txtSerialFrom').val(\"" + lot4 + "\")";
		jse.executeScript(final_lot_script);
		Thread.sleep(1000);
		sendKeys(txt_searielLotNo, invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_dateSerielProductProductCapturePage);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_dateSerielProductProductCapturePage);
		Thread.sleep(1000);
		getInvObjReg().customizeLoadingDelay(btn_updateCapture, 50);
		click(btn_updateCapture);

		Thread.sleep(3000);
		getInvObjReg().customizeLoadingDelay(btn_productCaptureBackButton, 20);
		click(btn_productCaptureBackButton);
		Thread.sleep(1000);
		click(btn_relese);
		getInvObjReg().customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 30);

		if (isDisplayed(lbl_relesedConfirmation.replace("(Released)", "( Released )"))) {
			writeTestResults("Verify user able to inbound serial/batches for racks",
					"User should be able to inbound serial/batches for racks",
					"User successfully inbound serial/batches for racks", "pass");
		} else {
			writeTestResults("Verify user able to inbound serial/batches for racks",
					"User should be able to inbound serial/batches for racks",
					"User doesn't inbound serial/batches for racks", "fail");
		}
	}

	public void rackTransfer_IN_PC_056() throws Exception {
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);
		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_rackTransfer, 10);
		click(btn_rackTransfer);
		getInvObjReg().customizeLoadingDelay(btn_newRackTransfer, 30);
		click(btn_newRackTransfer);
		getInvObjReg().customizeLoadingDelay(drop_warehouseRackTransfer, 40);
		getInvObjReg().partSelectText(drop_warehouseRackTransfer,
				invObj.readIWRegression01("NewlyCreatedWarehouse_IN_PC_056"));
		getInvObjReg().partSelectText(drop_filterBy, filterByRackTransfer);
		getInvObjReg().partSelectText(drop_rackRackTransfer, invObj.readIWRegression01("Rack01_IN_PC_056"));
		Thread.sleep(3000);
		click(btn_productDocIconRackTransfer);
		getInvObjReg().customizeLoadingDelayAndContinue(chk_allProductsRackTransfer, 50);
		Thread.sleep(4000);
		click(chk_allProductsRackTransfer);
		Thread.sleep(2000);
		click(btn_commoApplyLast);
		for (int i = 1; i < 5; i++) {
			getInvObjReg().customizeLoadingDelay(btn_destityRackSelectionMenuLastRow.replace("row", String.valueOf(i)),
					60);
			click(btn_destityRackSelectionMenuLastRow.replace("row", String.valueOf(i)));
			getInvObjReg().customizeLoadingDelay(drop_destinationRackRackTransfer, 15);
			getInvObjReg().partSelectText(drop_destinationRackRackTransfer,
					invObj.readIWRegression01("Rack02_IN_PC_056"));
			sendKeys(input_rackTarnsferQunatity, productQuantity_IN_IO_074);
			click(btn_applyDestinationRack);
		}

		explicitWait(btn_draftActionVerification, 30);
		click(btn_draftActionVerification);
		getInvObjReg().customizeLoadingDelay(brn_serielBatchCapture, 40);
		click(brn_serielBatchCapture);
		getInvObjReg().customizeLoadingDelay(btn_itemProductCapturePage.replace("_product_number", "1"), 20);
		for (int i = 1; i < 5; i++) {
			if (i == 3) {
				continue;
			}
			Actions action1 = new Actions(driver);
			WebElement we1 = driver
					.findElement(By.xpath(btn_itemProductCapturePage.replace("_product_number", String.valueOf(i))));
			action1.moveToElement(we1).build().perform();
			click(btn_itemProductCapturePage.replace("_product_number", String.valueOf(i)));
			click(chk_selectAllCaptureItem);
			click(btn_updateCapture);
			Thread.sleep(2000);
		}

		click(btn_productCaptureBackButton);
		click(btn_relese);
		getInvObjReg().customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 30);

		if (isDisplayed(lbl_relesedConfirmation.replace("(Released)", "( Released )"))) {
			writeTestResults("Verify user can transfer serial/batches form R1 to R2",
					"User should be able to transfer serial/batches form R1 to R2",
					"User successfully transfer serial/batches form R1 to R2", "pass");
		} else {
			writeTestResults("Verify user can transfer serial/batches form R1 to R2",
					"User should be able to transfer serial/batches form R1 to R2",
					"User doesn't transfer serial/batches form R1 to R2", "fail");
		}

	}

	public void productConversion_IN_PC_056() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}

		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon,
				getInvObj().readIWRegression01("NewlyCreatedWarehouse_IN_PC_056") + " [Negombo]");
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue
				.equals(getInvObj().readIWRegression01("NewlyCreatedWarehouse_IN_PC_056") + " [Negombo]")) {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User allow to fill summary details", "pass");
		} else {
			writeTestResults("Verify user allow to fill summary details", "User should allow to fill summary details",
					"User not allow to fill summary details", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("Lot"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(result_cpmmonProduct.replace("product", invObj.readTestCreation("Lot")),
				20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("Lot")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("Lot")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("product", invObj.readTestCreation("Lot")))) {
			writeTestResults("Verify user allow to select Lot product as \"From Product\"",
					"User should allow to select Lot product as \"From Product\"",
					"User allow to select Lot product as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select Lot product as \"From Product\"",
					"User should allow to select Lot product as \"From Product\"",
					"User not allow to select Lot product as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerirlFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerirlFifo")))) {
			writeTestResults("Verify user allow to select serial specific product as \"To Product\"",
					"User should allow to select serial specific product as \"To Product\"",
					"User allow to select serial specific product as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select serial specific product as \"To Product\"",
					"User should allow to select serial specific product as \"To Product\"",
					"User not allow to select serial specific product as \"To Product\"", "fail");
		}

		sendKeys(txt_quantityToProductProductConversion, "5");

		click(btn_addNewRowProductConversion);

		click(btn_productLookupRowReplaceProductConversion.replace("row", "2"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchFifo")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchFifo")))) {
			writeTestResults("Verify user allow to select batch FIFO as \"From Product\"",
					"User should allow to select batch FIFO as \"From Product\"",
					"User allow to select batch FIFO as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select batch FIFO as \"From Product\"",
					"User should allow to select batch FIFO as \"From Product\"",
					"User not allow to select batch FIFO as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "2"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "2"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerirlFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerirlFifo")))) {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO as \"To Product\"",
					"User allow to select seriel FIFO as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO as \"To Product\"",
					"User not allow to select seriel FIFO as \"To Product\"", "fail");
		}

		sendKeys(txt_qtyToProductProductConcersionRowReplace.replace("row", "2"), "5");

		click(btn_addNewRowProductConversion);

		click(btn_productLookupRowReplaceProductConversion.replace("row", "3"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select batch specific as \"From Product\"",
					"User should allow to select batch specific as \"From Product\"",
					"User allow to select batch specific as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select batch specific as \"From Product\"",
					"User should allow to select batch specific as \"From Product\"",
					"User not allow to select batch specific as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "3"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "3"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerirlFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")))) {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO as \"To Product\"",
					"User allow to select seriel FIFO as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO batch as \"To Product\"",
					"User not allow to select seriel FIFO as \"To Product\"", "fail");
		}

		sendKeys(txt_qtyToProductProductConcersionRowReplace.replace("row", "3"), "5");

		click(btn_addNewRowProductConversion);

		click(btn_productLookupRowReplaceProductConversion.replace("row", "4"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify user allow to select serial specific as \"From Product\"",
					"User should allow to select serial specific as \"From Product\"",
					"User allow to select serial specific as \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select serial specific as \"From Product\"",
					"User should allow to select serial specific as \"From Product\"",
					"User not allow to select serial specific as \"From Product\"", "fail");
		}

		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "4"), "10");

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "4"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerirlFifo"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerirlFifo")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerirlFifo")))) {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO as \"To Product\"",
					"User allow to select seriel FIFO as \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select seriel FIFO as \"To Product\"",
					"User should allow to select seriel FIFO batch as \"To Product\"",
					"User not allow to select seriel FIFO as \"To Product\"", "fail");
		}

		sendKeys(txt_qtyToProductProductConcersionRowReplace.replace("row", "4"), "5");

		/* capture row 01 */
		click(btn_serleNosProductConversionToProduct.replace("row", "1"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		String sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		/* capture row 02 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "2"));
		explicitWait(btn_seraialBatchCaptureDocIconProductConversion, 10);
		click(btn_seraialBatchCaptureDocIconProductConversion);
		explicitWait(chk_batchSelectionProductConversion, 10);
		click(chk_batchSelectionProductConversion);
		click(btn_commoApplyLast);

		click(btn_serleNosProductConversionToProduct.replace("row", "2"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		/* capture row 03 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "3"));
		explicitWait(btn_seraialBatchCaptureDocIconProductConversion, 10);
		click(btn_seraialBatchCaptureDocIconProductConversion);
		explicitWait(chk_batchSelectionProductConversion, 10);
		click(chk_batchSelectionProductConversion);
		click(btn_commoApplyLast);

		click(btn_serleNosProductConversionToProduct.replace("row", "3"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		/* capture row 04 */
		click(btn_serleNosProductConversionFromProduct.replace("row", "4"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		explicitWait(txt_serielCountProductConversion, 20);
		sendKeys(txt_serielCountProductConversion, "10");
		sendKeys(txt_serielNo, getInvObj().readIWRegression01("SN_IN_PC_056"));
		pressEnter(txt_serielNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		click(btn_serleNosProductConversionToProduct.replace("row", "4"));
		explicitWait(chk_serielRangeProductConversion, 20);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, "5");
		sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click("28_Link");
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click("28_Link");
		pressEnter(txt_lotNo);
		Thread.sleep(3000);
		click(btn_commoApplyLast);

		explicitWait(btn_draftActionVerification, 30);
		click(btn_draftActionVerification);

		getInvObjReg().customizeLoadingDelay(header_drftedProductConversion, 40);

		click(btn_releaseCommon);
		explicitWait(header_releasedProductConversion, 30);
		if (isDisplayed(header_releasedProductConversion)) {
			writeTestResults("Verify user able to convert above Serial/Batches",
					"User should be able to convert above Serial/Batches",
					"User successfully convert above Serial/Batches", "pass");
		} else {
			writeTestResults("Verify user able to convert above Serial/Batches",
					"User should be able to convert above Serial/Batches", "User doesn't convert above Serial/Batches",
					"fail");

		}
	}

	/* IN_PC_057 */
	public void checkProductConversionWithProductCodeOnly_IN_PC_057() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		invObj.balancingLevelConfigProductInformationRegression01("Code Only");
		pageRefersh();

		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}
		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, productConversionWarehouseRegression);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify user allow to fill mandatory fields", "User should allow to fill mandatory fields",
					"User allow to fill mandatory fields", "pass");
		} else {
			writeTestResults("Verify user allow to fill mandatory fields", "User should allow to fill mandatory fields",
					"User not allow to fill mandatory fields", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User allow to select \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User not allow to select \"From Product\"",
					"fail");
		}
		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), quantityFiveProductConversion);

		click(btn_serielBatchCaptureNosProductConversionRowReplace.replace("row", "1"));
		getInvObjReg().customizeLoadingDelayAndContinue(header_batchCaptureWindowProductConversion, 20);

		click(btn_seraialBatchCaptureDocIconProductConversion);

		getInvObjReg().customizeLoadingDelay(chk_selectBatchForBatchSpecificProduct, 20);
		click(chk_selectBatchForBatchSpecificProduct);

		Thread.sleep(3000);

		String capturedQuantityBatchSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantityBatchSpecific.equals(quantityFiveProductConversion)) {
			writeTestResults("Verify user allwo to capture batches for From Product",
					"User should allwo to capture batches for From Product",
					"User allwo to capture batches for From Product", "pass");
		} else {
			writeTestResults("Verify user allwo to capture batches for From Product",
					"User should allwo to capture batches for From Product",
					"User not allwo to capture batches for From Product", "fail");

		}

		click(btn_applyLast);

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User allow to select \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User not allow to select \"To Product\"", "fail");
		}

		sendKeys(txt_quantityToProductProductConversion, quantityToProductProductConversion);

		click(btn_serialNosFromProductConversionToProduc);
		getInvObjReg().customizeLoadingDelayAndContinue(header_serileCaptureWindowProductConversion, 20);

		if (isDisplayed(header_serileCaptureWindowProductConversion)) {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window successfully pop-up",
					"pass");
		} else {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window doesn't pop-up",
					"fail");
		}

		getInvObjReg().customizeLoadingDelay(chk_serielRangeProductConversion, 20);
		Thread.sleep(1500);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, quantityToProductProductConversion);

		String sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click(btn_date31ProductConversionToProduct);
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click(btn_date31ProductConversionToProduct);
		pressEnter(txt_lotNo);
		Thread.sleep(3000);

		String capturedQuantitySerielSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantitySerielSpecific.equals(quantityToProductProductConversion)) {
			writeTestResults("Verify user allwo to capture serials for To Product",
					"User should allwo to capture serials for To Product",
					"User allwo to capture serials for To Product", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serials for To Product",
					"User should allwo to capture serials for To Product",
					"User not allwo to capture serials for To Product", "fail");

		}

		click(btn_applyLast);

		click(btn_draftActionVerification);

		getInvObjReg().customizeLoadingDelay(header_drftedProductConversion, 40);
		if (isDisplayed(header_drftedProductConversion)) {
			writeTestResults("Verify user allow to draft the Product Conversion",
					"User should allow to draft the Product Conversiont", "User allow to draft the Product Conversion",
					"pass");
		} else {
			writeTestResults("Verify user allow to draft the Product Conversion",
					"User should allow to draft the Product Conversiont",
					"User not allow to draft the Product Conversion", "fail");

		}

		click(btn_releaseCommon);

		getInvObjReg().customizeLoadingDelay(header_releasedProductConversion, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedProductConversion)) {
			writeTestResults("Verify user allow to release the Product Conversion",
					"User should allow to release the Product Conversiont",
					"User allow to release the Product Conversion", "pass");
		} else {
			writeTestResults("Verify user allow to release the Product Conversion",
					"User should allow to release the Product Conversiont",
					"User not allow to release the Product Conversion", "fail");

		}

		String fromProductOnGrid = getText(
				div_productAccordingToRowRelesedProductConversionFromProduct.replace("row", "1"));
		if (fromProductOnGrid.equals(invObj.readTestCreation("BatchSpecific"))) {
			writeTestResults("Verify only product code is display in Product Conversion(From Product)",
					"Only product code should display in Product Conversion(From Product)",
					"Only product code successfully display in Product Conversion(From Product)", "pass");
		} else {
			writeTestResults("Verify only product code is display in Product Conversion(From Product)",
					"Only product code should display in Product Conversion(From Product)",
					"Only product code is not display in Product Conversion(From Product)", "fail");

		}

		String toProductOnGrid = getText(
				div_productAccordingToRowRelesedProductConversionToProduct.replace("row", "1"));
		if (toProductOnGrid.equals(invObj.readTestCreation("SerielSpecific"))) {
			writeTestResults("Verify only product code is display in Product Conversion(To Product)",
					"Only product code should display in Product Conversion(To Product)",
					"Only product code successfully display in Product Conversion(To Product)", "pass");
		} else {
			writeTestResults("Verify only product code is display in Product Conversion(To Product)",
					"Only product code should display in Product Conversion(To Product)",
					"Only product code is not display in Product Conversion(To Product)", "fail");
		}
	}

	/* IN_PC_058 */
	public void checkProductConversionWithProductDescriptionOnly_IN_PC_058() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		invObj.balancingLevelConfigProductInformationRegression01("Name Only");
		pageRefersh();

		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}
		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, productConversionWarehouseRegression);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify user allow to fill mandatory fields", "User should allow to fill mandatory fields",
					"User allow to fill mandatory fields", "pass");
		} else {
			writeTestResults("Verify user allow to fill mandatory fields", "User should allow to fill mandatory fields",
					"User not allow to fill mandatory fields", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, "DESC" + invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User allow to select \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User not allow to select \"From Product\"",
					"fail");
		}
		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), quantityFiveProductConversion);

		click(btn_serielBatchCaptureNosProductConversionRowReplace.replace("row", "1"));
		getInvObjReg().customizeLoadingDelayAndContinue(header_batchCaptureWindowProductConversion, 20);

		click(btn_seraialBatchCaptureDocIconProductConversion);

		getInvObjReg().customizeLoadingDelay(chk_selectBatchForBatchSpecificProduct, 20);
		click(chk_selectBatchForBatchSpecificProduct);

		Thread.sleep(3000);

		String capturedQuantityBatchSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantityBatchSpecific.equals(quantityFiveProductConversion)) {
			writeTestResults("Verify user allwo to capture batches for From Product",
					"User should allwo to capture batches for From Product",
					"User allwo to capture batches for From Product", "pass");
		} else {
			writeTestResults("Verify user allwo to capture batches for From Product",
					"User should allwo to capture batches for From Product",
					"User not allwo to capture batches for From Product", "fail");

		}

		click(btn_applyLast);

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User allow to select \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User not allow to select \"To Product\"", "fail");
		}

		sendKeys(txt_quantityToProductProductConversion, quantityToProductProductConversion);

		click(btn_serialNosFromProductConversionToProduc);
		getInvObjReg().customizeLoadingDelayAndContinue(header_serileCaptureWindowProductConversion, 20);

		if (isDisplayed(header_serileCaptureWindowProductConversion)) {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window successfully pop-up",
					"pass");
		} else {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window doesn't pop-up",
					"fail");
		}

		getInvObjReg().customizeLoadingDelay(chk_serielRangeProductConversion, 20);
		Thread.sleep(1500);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, quantityToProductProductConversion);

		String sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click(btn_date31ProductConversionToProduct);
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click(btn_date31ProductConversionToProduct);
		pressEnter(txt_lotNo);
		Thread.sleep(3000);

		String capturedQuantitySerielSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantitySerielSpecific.equals(quantityToProductProductConversion)) {
			writeTestResults("Verify user allwo to capture serials for To Product",
					"User should allwo to capture serials for To Product",
					"User allwo to capture serials for To Product", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serials for To Product",
					"User should allwo to capture serials for To Product",
					"User not allwo to capture serials for To Product", "fail");

		}

		click(btn_applyLast);

		click(btn_draftActionVerification);

		getInvObjReg().customizeLoadingDelay(header_drftedProductConversion, 40);
		if (isDisplayed(header_drftedProductConversion)) {
			writeTestResults("Verify user allow to draft the Product Conversion",
					"User should allow to draft the Product Conversiont", "User allow to draft the Product Conversion",
					"pass");
		} else {
			writeTestResults("Verify user allow to draft the Product Conversion",
					"User should allow to draft the Product Conversiont",
					"User not allow to draft the Product Conversion", "fail");

		}

		click(btn_releaseCommon);

		getInvObjReg().customizeLoadingDelay(header_releasedProductConversion, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedProductConversion)) {
			writeTestResults("Verify user allow to release the Product Conversion",
					"User should allow to release the Product Conversiont",
					"User allow to release the Product Conversion", "pass");
		} else {
			writeTestResults("Verify user allow to release the Product Conversion",
					"User should allow to release the Product Conversiont",
					"User not allow to release the Product Conversion", "fail");

		}

		String fromProductOnGrid = getText(
				div_productAccordingToRowRelesedProductConversionFromProduct.replace("row", "1"));
		if (fromProductOnGrid.equals("DESC" + invObj.readTestCreation("BatchSpecific"))) {
			writeTestResults("Verify only product description is display in Product Conversion(From Product)",
					"Only product description should display in Product Conversion(From Product)",
					"Only product description successfully display in Product Conversion(From Product)", "pass");
		} else {
			writeTestResults("Verify only product description is display in Product Conversion(From Product)",
					"Only product description should display in Product Conversion(From Product)",
					"Only product description is not display in Product Conversion(From Product)", "fail");

		}

		String toProductOnGrid = getText(
				div_productAccordingToRowRelesedProductConversionToProduct.replace("row", "1"));
		if (toProductOnGrid.equals("DESC" + invObj.readTestCreation("SerielSpecific"))) {
			writeTestResults("Verify only product description is display in Product Conversion(To Product)",
					"Only product description should display in Product Conversion(To Product)",
					"Only product description successfully display in Product Conversion(To Product)", "pass");
		} else {
			writeTestResults("Verify only product description is display in Product Conversion(To Product)",
					"Only product description should display in Product Conversion(To Product)",
					"Only product description is not display in Product Conversion(To Product)", "fail");
		}
	}

	/* IN_PC_059 */
	public void checkProductConversionWithProductCodeAndDescription_IN_PC_059() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		invObj.balancingLevelConfigProductInformationRegression01("Code and Description");
		pageRefersh();

		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		getInvObjReg().customizeLoadingDelay(header_newProductConversion, 45);
		if (isDisplayed(header_newProductConversion)) {

		} else {
			writeTestResults("Verify user can navigated to product conversion new form",
					"User should be navigated to product conversion new form",
					"User doesn't navigated to product conversion new form", "fail");
		}
		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, productConversionWarehouseRegression);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(productConversionWarehouseRegression)) {
			writeTestResults("Verify user allow to fill mandatory fields", "User should allow to fill mandatory fields",
					"User allow to fill mandatory fields", "pass");
		} else {
			writeTestResults("Verify user allow to fill mandatory fields", "User should allow to fill mandatory fields",
					"User not allow to fill mandatory fields", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User allow to select \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User not allow to select \"From Product\"",
					"fail");
		}
		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), quantityFiveProductConversion);

		click(btn_serielBatchCaptureNosProductConversionRowReplace.replace("row", "1"));
		getInvObjReg().customizeLoadingDelayAndContinue(header_batchCaptureWindowProductConversion, 20);

		click(btn_seraialBatchCaptureDocIconProductConversion);

		getInvObjReg().customizeLoadingDelay(chk_selectBatchForBatchSpecificProduct, 20);
		click(chk_selectBatchForBatchSpecificProduct);

		Thread.sleep(3000);

		String capturedQuantityBatchSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantityBatchSpecific.equals(quantityFiveProductConversion)) {
			writeTestResults("Verify user allwo to capture batches for From Product",
					"User should allwo to capture batches for From Product",
					"User allwo to capture batches for From Product", "pass");
		} else {
			writeTestResults("Verify user allwo to capture batches for From Product",
					"User should allwo to capture batches for From Product",
					"User not allwo to capture batches for From Product", "fail");

		}

		click(btn_applyLast);

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User allow to select \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User not allow to select \"To Product\"", "fail");
		}

		sendKeys(txt_quantityToProductProductConversion, quantityToProductProductConversion);

		click(btn_serialNosFromProductConversionToProduc);
		getInvObjReg().customizeLoadingDelayAndContinue(header_serileCaptureWindowProductConversion, 20);

		if (isDisplayed(header_serileCaptureWindowProductConversion)) {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window successfully pop-up",
					"pass");
		} else {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window doesn't pop-up",
					"fail");
		}

		getInvObjReg().customizeLoadingDelay(chk_serielRangeProductConversion, 20);
		Thread.sleep(1500);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, quantityToProductProductConversion);

		String sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click(btn_date31ProductConversionToProduct);
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click(btn_date31ProductConversionToProduct);
		pressEnter(txt_lotNo);
		Thread.sleep(3000);

		String capturedQuantitySerielSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantitySerielSpecific.equals(quantityToProductProductConversion)) {
			writeTestResults("Verify user allwo to capture serials for To Product",
					"User should allwo to capture serials for To Product",
					"User allwo to capture serials for To Product", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serials for To Product",
					"User should allwo to capture serials for To Product",
					"User not allwo to capture serials for To Product", "fail");

		}

		click(btn_applyLast);

		click(btn_draftActionVerification);

		getInvObjReg().customizeLoadingDelay(header_drftedProductConversion, 40);
		if (isDisplayed(header_drftedProductConversion)) {
			writeTestResults("Verify user allow to draft the Product Conversion",
					"User should allow to draft the Product Conversiont", "User allow to draft the Product Conversion",
					"pass");
		} else {
			writeTestResults("Verify user allow to draft the Product Conversion",
					"User should allow to draft the Product Conversiont",
					"User not allow to draft the Product Conversion", "fail");

		}

		click(btn_releaseCommon);

		getInvObjReg().customizeLoadingDelay(header_releasedProductConversion, 40);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(header_releasedProductConversion)) {
			writeTestResults("Verify user allow to release the Product Conversion",
					"User should allow to release the Product Conversiont",
					"User allow to release the Product Conversion", "pass");
		} else {
			writeTestResults("Verify user allow to release the Product Conversion",
					"User should allow to release the Product Conversiont",
					"User not allow to release the Product Conversion", "fail");

		}

		String fromProductOnGrid = getText(
				div_productAccordingToRowRelesedProductConversionFromProduct.replace("row", "1"));
		if (fromProductOnGrid.equals(invObj.readTestCreation("BatchSpecific") + " [ DESC"
				+ invObj.readTestCreation("BatchSpecific") + " ]")) {
			writeTestResults("Verify product code and description is displayed in Product Conversion(From Product)",
					"Product code and description should display in Product Conversion(From Product)",
					"Product code and description successfully display in Product Conversion(From Product)", "pass");
		} else {
			writeTestResults("Verify product code and description is display in Product Conversion(From Product)",
					"Product code and description should display in Product Conversion(From Product)",
					"Product code and description is not displayed in Product Conversion(From Product)", "fail");
		}

		String toProductOnGrid = getText(
				div_productAccordingToRowRelesedProductConversionToProduct.replace("row", "1"));
		if (toProductOnGrid.equals(invObj.readTestCreation("SerielSpecific") + " [ DESC"
				+ invObj.readTestCreation("SerielSpecific") + " ]")) {
			writeTestResults("Verify product code and description is displayed in Product Conversion(To Product)",
					"Product code and description should display in Product Conversion(To Product)",
					"Product code and description successfully display in Product Conversion(To Product)", "pass");
		} else {
			writeTestResults("Verify product code and description is display in Product Conversion(To Product)",
					"Product code and description should display in Product Conversion(To Product)",
					"Product code and description is not displayed in Product Conversion(To Product)", "fail");
		}
	}

	/* IN_PC_060 */
	public void configureWarehouseCodeOnly_IN_PC_060() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		Thread.sleep(1000);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(btn_adminModule, 15);
		click(btn_adminModule);
		getInvObjReg().customizeLoadingDelay(btn_balancingLevelAdminModule, 15);
		click(btn_balancingLevelAdminModule);
		getInvObjReg().customizeLoadingDelay(btn_inentoryBalancingLevelSetting, 15);
		click(btn_genarelBalancingLevelSetting);
		Thread.sleep(2000);
		selectText(drop_warehouseConfigrationBalancingLavel, "Code Only");
		Thread.sleep(1000);
		click(btn_updateGenaralTabBalancingLavelSetting);
		Thread.sleep(2000);
		pageRefersh();
	}

	public void releaseProductConversionAndCheckWarehouseCodeOnly_IN_PC_060() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, commonWarehouseCodeOnly);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(commonWarehouseCodeOnly)) {
			writeTestResults("Verify user allow to fill mandatory fields", "User should allow to fill mandatory fields",
					"User allow to fill mandatory fields", "pass");
		} else {
			writeTestResults("Verify user allow to fill mandatory fields", "User should allow to fill mandatory fields",
					"User not allow to fill mandatory fields", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User allow to select \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User not allow to select \"From Product\"",
					"fail");
		}
		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), quantityFiveProductConversion);

		click(btn_serielBatchCaptureNosProductConversionRowReplace.replace("row", "1"));
		getInvObjReg().customizeLoadingDelayAndContinue(header_batchCaptureWindowProductConversion, 20);

		click(btn_seraialBatchCaptureDocIconProductConversion);

		getInvObjReg().customizeLoadingDelay(chk_selectBatchForBatchSpecificProduct, 20);
		click(chk_selectBatchForBatchSpecificProduct);

		Thread.sleep(3000);

		String capturedQuantityBatchSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantityBatchSpecific.equals(quantityFiveProductConversion)) {
			writeTestResults("Verify user allwo to capture batches for From Product",
					"User should allwo to capture batches for From Product",
					"User allwo to capture batches for From Product", "pass");
		} else {
			writeTestResults("Verify user allwo to capture batches for From Product",
					"User should allwo to capture batches for From Product",
					"User not allwo to capture batches for From Product", "fail");

		}

		click(btn_applyLast);

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User allow to select \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User not allow to select \"To Product\"", "fail");
		}

		sendKeys(txt_quantityToProductProductConversion, quantityToProductProductConversion);

		click(btn_serialNosFromProductConversionToProduc);
		getInvObjReg().customizeLoadingDelayAndContinue(header_serileCaptureWindowProductConversion, 20);

		if (isDisplayed(header_serileCaptureWindowProductConversion)) {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window successfully pop-up",
					"pass");
		} else {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window doesn't pop-up",
					"fail");
		}

		getInvObjReg().customizeLoadingDelay(chk_serielRangeProductConversion, 20);
		Thread.sleep(1500);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, quantityToProductProductConversion);

		String sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click(btn_date31ProductConversionToProduct);
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click(btn_date31ProductConversionToProduct);
		pressEnter(txt_lotNo);
		Thread.sleep(3000);

		String capturedQuantitySerielSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantitySerielSpecific.equals(quantityToProductProductConversion)) {
			writeTestResults("Verify user allwo to capture serials for To Product",
					"User should allwo to capture serials for To Product",
					"User allwo to capture serials for To Product", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serials for To Product",
					"User should allwo to capture serials for To Product",
					"User not allwo to capture serials for To Product", "fail");

		}

		click(btn_applyLast);

		click(btn_draftActionVerification);

		getInvObjReg().customizeLoadingDelay(header_drftedProductConversion, 40);
		if (isDisplayed(header_drftedProductConversion)) {
			writeTestResults("Verify user allow to draft the Product Conversion",
					"User should allow to draft the Product Conversiont", "User allow to draft the Product Conversion",
					"pass");
		} else {
			writeTestResults("Verify user allow to draft the Product Conversion",
					"User should allow to draft the Product Conversiont",
					"User not allow to draft the Product Conversion", "fail");

		}

		click(btn_releaseCommon);

		getInvObjReg().customizeLoadingDelay(header_releasedProductConversion, 40);
		trackCode = getText(lbl_docNo);
		getInvObj().writeRegression01("ReleasedProductConversion_IN_PC_042", trackCode, 176);
		getInvObj().writeRegression01("ReleasedProductConversionURL_IN_PC_042", getCurrentUrl(), 177);

		if (isDisplayed(header_releasedProductConversion)) {
			writeTestResults("Verify Product Conversion is released", "Product conversion should be released",
					"Product conversion successfully released", "pass");
		} else {
			writeTestResults("Verify Product Conversion is released", "Product conversion should be released",
					"Product conversion doesn't released", "fail");

		}

		String warehouse = getText(lbl_warehouseProductConversion);
		if (warehouse.equals(getInvObj().readIWRegression01("NewlyCreatedWarehouseWithARack"))) {
			writeTestResults("Verify only warehouse code is display in Product Conversion",
					"Only warehouse code should display in Product Conversion",
					"Only warehouse code successfully display in Product Conversion", "pass");
		} else {
			writeTestResults("Verify only warehouse code is display in Product Conversion",
					"Only warehouse code should display in Product Conversion",
					"Not only warehouse code is display in Product Conversion", "fail");
		}

	}

	/* IN_PC_061 */
	public void configureWarehouseNameOnly_IN_PC_061() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		Thread.sleep(1000);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(btn_adminModule, 15);
		click(btn_adminModule);
		getInvObjReg().customizeLoadingDelay(btn_balancingLevelAdminModule, 15);
		click(btn_balancingLevelAdminModule);
		getInvObjReg().customizeLoadingDelay(btn_inentoryBalancingLevelSetting, 15);
		click(btn_genarelBalancingLevelSetting);
		Thread.sleep(2000);
		selectText(drop_warehouseConfigrationBalancingLavel, "Name Only");
		Thread.sleep(1000);
		click(btn_updateGenaralTabBalancingLavelSetting);
		Thread.sleep(2000);
		pageRefersh();
	}

	public void warehouseCreation_IN_PC_061() throws Exception {

		getInvObjReg().createNewWarehouseWithNameUnique01();

	}

	public void stockAdjustment_IN_PC_061() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);
		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_stockAdjustment, 10);
		click(btn_stockAdjustment);
		getInvObjReg().customizeLoadingDelay(btn_newStockAdjustment, 30);
		click(btn_newStockAdjustment);
		getInvObjReg().customizeLoadingDelay(txt_descStockAdj, 30);

		sendKeys(txt_descStockAdj, descStockAdj);

		selectText(dropdown_warehouseStockAdj, invObj.readIWRegression01("NewlyCreatedWarehouseUniqueName_name01"));

		/* Product add to the grid */
		for (int i = 1; i < 2; i++) {
			String str_i = String.valueOf(i);
			click(btn_productLookupStockAdjForLoop.replace("row", str_i));
			String product = "";

			if (i == 1) {
				product = invObj.readTestCreation("BatchSpecific");
			} else {
				System.out.println("Product type cannot identified.........");
			}

			invObj.handeledSendKeys(txt_productSearch, product);
			pressEnter(txt_productSearch);
			getInvObjReg().customizeLoadingDelay(result_cpmmonProduct.replace("product", product), 20);
			doubleClick(result_cpmmonProduct.replace("product", product));
			Thread.sleep(1500);

			String txt_qty = txt_quantityInGrid.replace("tblName", "tblStockAdj").replace("row", str_i);
			String txt_cost = txt_costProductGridStockAdjustment.replace("row", str_i);
			Thread.sleep(1500);
			getInvObjReg().customizeLoadingDelay(txt_qty, 15);
			sendKeys(txt_qty, commonQuantity);
			sendKeys(txt_cost, commonUnitCost);

			getInvObjReg().customizeLoadingDelay(btn_addNewRow, 10);
			click(btn_addNewRow);
		}

		click(btn_deleteRowCommonRowReplace.replace("row", "2"));

		getInvObjReg().customizeLoadingDelay(btn_draftActionVerification, 20);
		click(btn_draftActionVerification);
		getInvObjReg().customizeLoadingDelay(brn_serielBatchCapture, 20);
		click(brn_serielBatchCapture);

		/* Product 01 */
		Thread.sleep(2000);
		String item_xpath;
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "1");
		getInvObjReg().customizeLoadingDelay(item_xpath, 20);
		click(item_xpath);
		getInvObjReg().customizeLoadingDelay(txt_batchNumberCaptureILOOutboundShipment, 30);
		sendKeys(txt_batchNumberCaptureILOOutboundShipment,
				invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_lotNumberStockAdjustment, getInvObjReg2().genarateUniqueSeriels());
		click(txt_expiryDateStockAdjustmentProductCapture);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_lotNumberStockAdjustment);
		Thread.sleep(1000);
		getInvObjReg().customizeLoadingDelay(btn_updateCapture, 50);
		click(btn_updateCapture);
		getInvObjReg().customizeLoadingDelay((item_xpath + span_afterCaptureSerialGreen), 30);
		Thread.sleep(3000);
		getInvObjReg().customizeLoadingDelay(btn_productCaptureBackButton, 20);
		click(btn_productCaptureBackButton);
		Thread.sleep(1000);
		click(btn_relese);
		getInvObjReg().customizeLoadingDelay(lbl_relesedConfirmation.replace("(Released)", "( Released )"), 30);
	}

	public void releaseProductConversionAndCheckWarehouseNameOnly_IN_PC_061() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, invObj.readIWRegression01("NewlyCreatedWarehouseUniqueName_name01"));
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(invObj.readIWRegression01("NewlyCreatedWarehouseUniqueName_name01"))) {
			writeTestResults("Verify user allow to fill mandatory fields", "User should allow to fill mandatory fields",
					"User allow to fill mandatory fields", "pass");
		} else {
			writeTestResults("Verify user allow to fill mandatory fields", "User should allow to fill mandatory fields",
					"User not allow to fill mandatory fields", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User allow to select \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User not allow to select \"From Product\"",
					"fail");
		}
		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), quantityFiveProductConversion);

		click(btn_serielBatchCaptureNosProductConversionRowReplace.replace("row", "1"));
		getInvObjReg().customizeLoadingDelayAndContinue(header_batchCaptureWindowProductConversion, 20);

		click(btn_seraialBatchCaptureDocIconProductConversion);

		getInvObjReg().customizeLoadingDelay(chk_selectBatchForBatchSpecificProduct, 20);
		click(chk_selectBatchForBatchSpecificProduct);

		Thread.sleep(3000);

		String capturedQuantityBatchSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantityBatchSpecific.equals(quantityFiveProductConversion)) {
			writeTestResults("Verify user allwo to capture batches for From Product",
					"User should allwo to capture batches for From Product",
					"User allwo to capture batches for From Product", "pass");
		} else {
			writeTestResults("Verify user allwo to capture batches for From Product",
					"User should allwo to capture batches for From Product",
					"User not allwo to capture batches for From Product", "fail");

		}

		click(btn_applyLast);

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User allow to select \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User not allow to select \"To Product\"", "fail");
		}

		sendKeys(txt_quantityToProductProductConversion, quantityToProductProductConversion);

		click(btn_serialNosFromProductConversionToProduc);
		getInvObjReg().customizeLoadingDelayAndContinue(header_serileCaptureWindowProductConversion, 20);

		if (isDisplayed(header_serileCaptureWindowProductConversion)) {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window successfully pop-up",
					"pass");
		} else {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window doesn't pop-up",
					"fail");
		}

		getInvObjReg().customizeLoadingDelay(chk_serielRangeProductConversion, 20);
		Thread.sleep(1500);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, quantityToProductProductConversion);

		String sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click(btn_date31ProductConversionToProduct);
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click(btn_date31ProductConversionToProduct);
		pressEnter(txt_lotNo);
		Thread.sleep(3000);

		String capturedQuantitySerielSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantitySerielSpecific.equals(quantityToProductProductConversion)) {
			writeTestResults("Verify user allwo to capture serials for To Product",
					"User should allwo to capture serials for To Product",
					"User allwo to capture serials for To Product", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serials for To Product",
					"User should allwo to capture serials for To Product",
					"User not allwo to capture serials for To Product", "fail");

		}

		click(btn_applyLast);

		click(btn_draftActionVerification);

		getInvObjReg().customizeLoadingDelay(header_drftedProductConversion, 40);
		if (isDisplayed(header_drftedProductConversion)) {
			writeTestResults("Verify user allow to draft the Product Conversion",
					"User should allow to draft the Product Conversiont", "User allow to draft the Product Conversion",
					"pass");
		} else {
			writeTestResults("Verify user allow to draft the Product Conversion",
					"User should allow to draft the Product Conversiont",
					"User not allow to draft the Product Conversion", "fail");

		}

		click(btn_releaseCommon);

		getInvObjReg().customizeLoadingDelay(header_releasedProductConversion, 40);
		trackCode = getText(lbl_docNo);
		getInvObj().writeRegression01("ReleasedProductConversion_IN_PC_042", trackCode, 176);
		getInvObj().writeRegression01("ReleasedProductConversionURL_IN_PC_042", getCurrentUrl(), 177);

		if (isDisplayed(header_releasedProductConversion)) {
			writeTestResults("Verify Product Conversion is released", "Product conversion should be released",
					"Product conversion successfully released", "pass");
		} else {
			writeTestResults("Verify Product Conversion is released", "Product conversion should be released",
					"Product conversion doesn't released", "fail");

		}

		String warehouse = getText(lbl_warehouseProductConversion);
		if (warehouse.equals(invObj.readIWRegression01("NewlyCreatedWarehouseUniqueName_name01"))) {
			writeTestResults("Verify only warehouse description is display in Product Conversion",
					"Only warehouse description should display in Product Conversion",
					"Only warehouse description successfully display in Product Conversion", "pass");
		} else {
			writeTestResults("Verify only warehouse description is display in Product Conversion",
					"Only warehouse description should display in Product Conversion",
					"Not only warehouse description is display in Product Conversion", "fail");
		}

	}

	/* IN_PC_062 */
	public void configureWarehouseCodeAndDescription_IN_PC_062() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		Thread.sleep(1000);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(btn_adminModule, 15);
		click(btn_adminModule);
		getInvObjReg().customizeLoadingDelay(btn_balancingLevelAdminModule, 15);
		click(btn_balancingLevelAdminModule);
		getInvObjReg().customizeLoadingDelay(btn_inentoryBalancingLevelSetting, 15);
		click(btn_genarelBalancingLevelSetting);
		Thread.sleep(2000);
		selectText(drop_warehouseConfigrationBalancingLavel, "Code and Description");
		Thread.sleep(1000);
		click(btn_updateGenaralTabBalancingLavelSetting);
		Thread.sleep(2000);
		pageRefersh();
	}

	public void releaseProductConversionAndCheckWarehouseCodeAndDescription_IN_PC_062() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		if (isDisplayed(headers_productConversionGrid)) {

		} else {
			writeTestResults("Verify grid headers", "Grid headers should be properly set",
					"Grid headers not properly set", "fail");
		}

		selectText(drop_wareProductCon, commonWarehouseCodeAndDescription);
		Select comboBox = new Select(driver.findElement(By.xpath(drop_wareProductCon)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		if (selectedComboValue.equals(commonWarehouseCodeAndDescription)) {
			writeTestResults("Verify user allow to fill mandatory fields", "User should allow to fill mandatory fields",
					"User allow to fill mandatory fields", "pass");
		} else {
			writeTestResults("Verify user allow to fill mandatory fields", "User should allow to fill mandatory fields",
					"User not allow to fill mandatory fields", "fail");
		}

		click(btn_productLookupOroductConversion);
		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);
		if (isDisplayed(header_productLookupProductConversion)) {
		} else {
			writeTestResults("Verify product lookup is pop-up", "Product lookup should pop-up",
					"Product lookup doesn't pop-up", "fail");
		}

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("BatchSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("BatchSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")), 10);

		if (isDisplayed(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("BatchSpecific")))) {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User allow to select \"From Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"From Product\"",
					"User should allow to select \"From Product\"", "User not allow to select \"From Product\"",
					"fail");
		}
		sendKeys(txt_quantityRowReplaceProductConversionFromProduct.replace("row", "1"), quantityFiveProductConversion);

		click(btn_serielBatchCaptureNosProductConversionRowReplace.replace("row", "1"));
		getInvObjReg().customizeLoadingDelayAndContinue(header_batchCaptureWindowProductConversion, 20);

		click(btn_seraialBatchCaptureDocIconProductConversion);

		getInvObjReg().customizeLoadingDelay(chk_selectBatchForBatchSpecificProduct, 20);
		click(chk_selectBatchForBatchSpecificProduct);

		Thread.sleep(3000);

		String capturedQuantityBatchSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantityBatchSpecific.equals(quantityFiveProductConversion)) {
			writeTestResults("Verify user allwo to capture batches for From Product",
					"User should allwo to capture batches for From Product",
					"User allwo to capture batches for From Product", "pass");
		} else {
			writeTestResults("Verify user allwo to capture batches for From Product",
					"User should allwo to capture batches for From Product",
					"User not allwo to capture batches for From Product", "fail");

		}

		click(btn_applyLast);

		click(btn_productLookupRowReplaceProductConversionToProduct.replace("row", "1"));

		getInvObjReg().customizeLoadingDelayAndContinue(header_productLookupProductConversion, 5);

		invObj.handeledSendKeys(txt_productSearch, invObj.readTestCreation("SerielSpecific"));
		pressEnter(txt_productSearch);
		getInvObjReg().customizeLoadingDelay(
				result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")), 20);
		doubleClick(result_cpmmonProduct.replace("product", invObj.readTestCreation("SerielSpecific")));
		Thread.sleep(1500);

		getInvObjReg().customizeLoadingDelayAndContinue(
				div_productOnGridProductConversation.replace("product", invObj.readTestCreation("SerielSpecific")), 10);

		if (isDisplayed(div_productOnGridProductConversation.replace("row", "1").replace("product",
				invObj.readTestCreation("SerielSpecific")))) {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User allow to select \"To Product\"", "pass");
		} else {
			writeTestResults("Verify user allow to select \"To Product\"", "User should allow to select \"To Product\"",
					"User not allow to select \"To Product\"", "fail");
		}

		sendKeys(txt_quantityToProductProductConversion, quantityToProductProductConversion);

		click(btn_serialNosFromProductConversionToProduc);
		getInvObjReg().customizeLoadingDelayAndContinue(header_serileCaptureWindowProductConversion, 20);

		if (isDisplayed(header_serileCaptureWindowProductConversion)) {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window successfully pop-up",
					"pass");
		} else {
			writeTestResults("Verify serial batch capturing window is pop-up",
					"Serial batch capturing window should pop-up", "Serial batch capturing window doesn't pop-up",
					"fail");
		}

		getInvObjReg().customizeLoadingDelay(chk_serielRangeProductConversion, 20);
		Thread.sleep(1500);
		click(chk_serielRangeProductConversion);
		Thread.sleep(2500);
		getInvObj().handeledSendKeys(txt_serielCountProductConversion, quantityToProductProductConversion);

		String sn = (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_serielNo, sn);
		Thread.sleep(2000);
		sendKeys(txt_lotNo, (invObj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "")));
		click(calender_serelSpecificCapturuePage);
		click(btn_clanderFw);
		click(btn_date31ProductConversionToProduct);
		click(txt_menufactureDate);
		click(btn_calenderBack);
		click(btn_date31ProductConversionToProduct);
		pressEnter(txt_lotNo);
		Thread.sleep(3000);

		String capturedQuantitySerielSpecific = getAttribute(txt_capturedQuantityProductConversion, "value");

		if (capturedQuantitySerielSpecific.equals(quantityToProductProductConversion)) {
			writeTestResults("Verify user allwo to capture serials for To Product",
					"User should allwo to capture serials for To Product",
					"User allwo to capture serials for To Product", "pass");
		} else {
			writeTestResults("Verify user allwo to capture serials for To Product",
					"User should allwo to capture serials for To Product",
					"User not allwo to capture serials for To Product", "fail");

		}

		click(btn_applyLast);

		click(btn_draftActionVerification);

		getInvObjReg().customizeLoadingDelay(header_drftedProductConversion, 40);
		if (isDisplayed(header_drftedProductConversion)) {
			writeTestResults("Verify user allow to draft the Product Conversion",
					"User should allow to draft the Product Conversiont", "User allow to draft the Product Conversion",
					"pass");
		} else {
			writeTestResults("Verify user allow to draft the Product Conversion",
					"User should allow to draft the Product Conversiont",
					"User not allow to draft the Product Conversion", "fail");

		}

		click(btn_releaseCommon);

		getInvObjReg().customizeLoadingDelay(header_releasedProductConversion, 40);
		trackCode = getText(lbl_docNo);
		getInvObj().writeRegression01("ReleasedProductConversion_IN_PC_042", trackCode, 176);
		getInvObj().writeRegression01("ReleasedProductConversionURL_IN_PC_042", getCurrentUrl(), 177);

		if (isDisplayed(header_releasedProductConversion)) {
			writeTestResults("Verify Product Conversion is released", "Product conversion should be released",
					"Product conversion successfully released", "pass");
		} else {
			writeTestResults("Verify Product Conversion is released", "Product conversion should be released",
					"Product conversion doesn't released", "fail");

		}

		String warehouse = getText(lbl_warehouseProductConversion);
		if (warehouse.equals(commonWarehouseCodeAndDescription)) {
			writeTestResults("Verify warehouse code and description is display in Product Conversion",
					"warehouse code and description should display in Product Conversion",
					"warehouse code and description successfully display in Product Conversion", "pass");
		} else {
			writeTestResults("Verify warehouse code and description is display in Product Conversion",
					"warehouse code and description should display in Product Conversion",
					"Not warehouse code and description is display in Product Conversion", "fail");
		}

	}

	/* IN_PC_063 */
	public void inactiveProductConversionJouerney_IN_PC_063() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 40);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 40);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(btn_adminModule, 20);
		click(btn_adminModule);
		click(btn_journeyConfiguration);
		getInvObjReg().customizeLoadingDelay(header_journeyConfiguration, 40);
		ExecuteJS(btn_journeyConfigProductConversion);
		Thread.sleep(2500);
		if (invObj.isDisplayedQuickCheck(btn_inactiveJourney)) {
			click(btn_inactiveJourney);
			if (isDisplayed(btn_yesConfirmationJourney)) {
				writeTestResults("Verify confirmation message is pop-up", "Confirmation message should pop-up",
						"Confirmation message successfully pop-up", "pass");
			} else {
				writeTestResults("Verify confirmation message is pop-up", "Confirmation message should pop-up",
						"Confirmation message doesn't pop-up", "fail");
			}
			click(btn_yesConfirmationJourney);

		}

		ExecuteJS(btn_journeyConfigProductConversion);
		if (invObj.isDisplayedQuickCheck(btn_activeJourney)) {
			writeTestResults("Verify Product Conversion journey is inactivated",
					"Product Conversion journey should be inactivated",
					"Product Conversion journey successfully inactivated", "pass");
		} else {
			writeTestResults("Verify Product Conversion journey is inactivated",
					"Product Conversion journey should be inactivated",
					"Product Conversion journey doesn't inactivated", "fail");
		}
	}

	public void checkProductConversionJourneyInactivation_IN_PC_063() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		Thread.sleep(2000);
		if (isDisplayed(validator_noPermissionForTheJourney)) {
			writeTestResults("Verify user not allow to do Product Conversion",
					"User should not allow to do Product Conversion", "User not allow to do Product Conversion",
					"pass");
		} else {
			writeTestResults("Verify user not allow to do Product Conversion",
					"User should not allow to do Product Conversion", "User allow to do Product Conversion",
					"fail");
		}

	}

	/* IN_PC_064 */
	public void activeProductConversionJouerney_IN_PC_064() throws Exception {
		getInvObjReg2().loginBiletaGBV();
		getInvObjReg().customizeLoadingDelay(navigation_pane, 40);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 40);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(btn_adminModule, 20);
		click(btn_adminModule);
		click(btn_journeyConfiguration);
		getInvObjReg().customizeLoadingDelay(header_journeyConfiguration, 40);
		ExecuteJS(btn_journeyConfigProductConversion);
		Thread.sleep(2500);
		if (invObj.isDisplayedQuickCheck(btn_activeJourney)) {
			click(btn_activeJourney);
			if (isDisplayed(btn_yesConfirmationJourney)) {
				writeTestResults("Verify confirmation message is pop-up", "Confirmation message should pop-up",
						"Confirmation message successfully pop-up", "pass");
			} else {
				writeTestResults("Verify confirmation message is pop-up", "Confirmation message should pop-up",
						"Confirmation message doesn't pop-up", "fail");
			}
			click(btn_yesConfirmationJourney);
		}
		
		ExecuteJS(btn_journeyConfigProductConversion);
		if (invObj.isDisplayedQuickCheck(btn_inactiveJourney)) {
			writeTestResults("Verify Product Conversion journey is activated",
					"Product Conversion journey should be activated",
					"Product Conversion journey successfully activated", "pass");
		} else {
			writeTestResults("Verify Product Conversion journey is activated",
					"Product Conversion journey should be activated",
					"Product Conversion journey doesn't activated", "fail");
		}

	}

	public void checkProductConversionJourneyActivation_IN_PC_064() throws Exception {
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		Thread.sleep(2000);
		getInvObjReg().customizeLoadingDelay(navigation_pane, 45);
		click(navigation_pane);
		getInvObjReg().customizeLoadingDelay(inventory_module, 10);

		if (isDisplayed(inventory_module)) {
		} else {
			writeTestResults("Verify set of modules were loaded", "Set of modules should be loaded",
					"Set of modules doesn't loaded", "fail");
		}

		click(inventory_module);
		getInvObjReg().customizeLoadingDelay(btn_productConversion, 45);
		if (isDisplayed(btn_productConversion)) {
		} else {
			writeTestResults("Verify set of forms related to the inventory module were loaded",
					"Set of forms related to the inventory module should be loaded",
					"Set of forms related to the inventory module doesn't loaded", "fail");
		}

		click(btn_productConversion);
		getInvObjReg().customizeLoadingDelay(header_productConversionByPage, 45);
		if (isDisplayed(header_productConversionByPage)) {
		} else {
			writeTestResults("Verify user can navigated to Product Conversion by page",
					"User should be navigated to Product Conversion by page",
					"User doesn't navigated to Product Conversion by page", "fail");
		}

		click(bnt_newProductConversion);
		explicitWait(header_newProductConversion, 40);
		if (isDisplayed(header_newProductConversion)) {
			writeTestResults("Verify user allow to do Product Conversion",
					"User should allow to do Product Conversion", "User allow to do Product Conversion",
					"pass");
		} else {
			writeTestResults("Verify user allow to do Product Conversion",
					"User should allow to do Product Conversion", "User not allow to do Product Conversion",
					"fail");
		}

	}

}
