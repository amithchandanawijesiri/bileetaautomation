package bileeta.BTAF.PageObjects;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import bileeta.BATF.Pages.ProjectModuleData;




public class ProjectModule extends ProjectModuleData{

	public static String actualcost,number,number1,number2,number3,number4,number8,number9,number10,TimeInsel,TimeOutsel,TimeActual,ReleasedA, serviceJobOrderCodeA,InterDepartmentActualvalueA,z,OrderDateA,OrderNoA,InvoiceDateA,InvoiceNoA,ProductA,EstimatedAmountA,ActualAmountA,date,PONo,INo,RateProfileValue;
	public static int number5,number6,number7,number11,TimeIn,TimeOut,ActualWork,x,y;
	InventoryAndWarehouseModule pro7=new InventoryAndWarehouseModule();

	public void navigateToTheLoginPage() throws Exception {
		openPage(siteURL);

		writeTestResults("Verify that 'Entution' header is available on the page",
				"user should be able to see the logo", "logo is dislayed", "pass");

	}

	public void verifyTheLogo() throws Exception {
		if (isDisplayed(siteLogo)) {

			writeTestResults("Verify that 'Entution' header is available on the page","user should be able to see the logo", "logo is dislayed", "pass");
		} else {
			writeTestResults("Verify that 'Entution' header is available on the page","user should be able to see the logo", "logo is not dislayed", "fail");

		}
	}

	public void userLogin() throws Exception {
		Thread.sleep(4000);
		sendKeys(txt_username, userNameData);

		sendKeys(txt_password, passwordData);
		Thread.sleep(4000);
		click(btn_login);

		if (isDisplayed(lnk_home)) {

			writeTestResults("User login", "User should navigate to the Home page of Entution",
					"user navigated to the home page", "pass");
		}

		else {
			writeTestResults("User login", "User should navigate to the Home page of Entution",
					"user cant navigate to the home page", "fail");

		}
	}

	public void clickNavigation() throws Exception {
	
		click(btn_nav);
		
		if(isDisplayed(sideMenu)) {
			writeTestResults("verify sidemenu","view sidemenu", "sidemenu is display", "pass");
		}else {
			writeTestResults("verify sidemenu","view sidemenu", "sidemenu is not display", "fail");
		} 
	}
	
public void clickProjectbutton() throws Exception {
		
		click(btn_projbutton);
		if(isDisplayed(subsideMenu)) {
			writeTestResults("verify subsideMenu","view subsideMenu", "subsideMenu is display", "pass");
		}else {
			writeTestResults("verify subsideMenu","view subsideMenu", "subsideMenu is not display", "fail");
		}
	}

public void createProject() throws Exception {
	
		WaitClick(btn_projectbutton);
		click(btn_projectbutton);
		
		WaitElement(projectpage);
		if(isDisplayed(projectpage)) {
			writeTestResults("verify project page","view project page", "project page is display", "pass");
		}else {
			writeTestResults("verify project page","view project page", "project page is not display", "fail");
		}
		
		WaitClick(btn_newprojectbutton);
		click(btn_newprojectbutton);
		
		WaitElement(newprojectpage);
		if(isDisplayed(newprojectpage)) {
			writeTestResults("verify new project page","view new project page", "new project page is display", "pass");
		}else {
			writeTestResults("verify new project page","view new project page", "new project page is not display", "fail");
		}
		
		WaitElement(txt_ProjectCode);
		LocalTime myObj = LocalTime.now();
		sendKeys(txt_ProjectCode, ProjectCode+myObj);
		
		Thread.sleep(4000);
		number1=ProjectCode+myObj;
		WaitElement(txt_Title);
		sendKeys(txt_Title, Title+myObj);
		WaitElement(txt_ProjectGroup);
		selectIndex(txt_ProjectGroup, 1);
		WaitClick(btn_PricingProfilebutton);
		click(btn_PricingProfilebutton);
		WaitElement(txt_PricingProfiletxt);
		sendKeys(txt_PricingProfiletxt,PricingProfiletxt);
		Thread.sleep(4000);
		pressEnter(txt_PricingProfiletxt);
		Thread.sleep(4000);
		doubleClick(sel_PricingProfilesel);
		
		WaitClick(btn_ResponsiblePersonbutton);
		click(btn_ResponsiblePersonbutton);
		WaitElement(txt_ResponsiblePersontxt);
		sendKeys(txt_ResponsiblePersontxt, ResponsiblePersontxt);
		pressEnter(txt_ResponsiblePersontxt);
		Thread.sleep(4000);
		doubleClick(sel_ResponsiblePersonsel);
		WaitElement(txt_AssignedBusinessUnits);
		selectIndex(txt_AssignedBusinessUnits, 1);
		
		WaitClick(btn_CustomerAccountbutton);
		click(btn_CustomerAccountbutton);
		WaitElement(txt_CustomerAccounttxt);
		sendKeys(txt_CustomerAccounttxt, CustomerAccounttxt);
		pressEnter(txt_CustomerAccounttxt);
		Thread.sleep(4000);
		doubleClick(sel_CustomerAccountsel);
		
		WaitClick(btn_ProjectLocationbutton);
		click(btn_ProjectLocationbutton);
		WaitElement(txt_ProjectLocationtxt);
		sendKeys(txt_ProjectLocationtxt, ProjectLocationtxt);
		pressEnter(txt_ProjectLocationtxt);
		Thread.sleep(4000);
		doubleClick(sel_ProjectLocationsel);
		
		WaitClick(txt_RequestedStartDate);
		click(txt_RequestedStartDate);
		WaitElement(sel_RequestedStartDatesel);
		click(sel_RequestedStartDatesel);
		
		WaitClick(btn_draft);
		click(btn_draft);
		WaitElement(pageDraft);
		if (isDisplayed(pageDraft)){

			writeTestResults("verify Project Document Draft","Project Document Draft", "Project Document is Draft", "pass");
		} else {
			writeTestResults("verify Project Document Draft","Project Document Draft", "Project Document is not Draft", "fail");

		}
		
		if (isDisplayed(txt_ActivitiesTabA)) {
			pageRefersh();
		}
		
		WaitClick(btn_release);
		click(btn_release);
		
		Thread.sleep(4000);
		trackCode= getText(Header);
		WaitElement(pageRelease);
		if (isDisplayed(pageRelease)){

			writeTestResults("verify Project Document Released","Project Document Released", "Project Document is Released", "pass");
		} else {
			writeTestResults("verify Project Document Released","Project Document Released", "Project Document is not Released", "fail");

		}
}

public void addingAprojectTask() throws Exception {
	
	WaitClick(btn_edit);
	click(btn_edit);
	WaitClick(btn_WorkBreakdown);
	click(btn_WorkBreakdown);
	WaitClick(btn_AddTask);
	click(btn_AddTask);
	WaitElement(txt_TaskName);
	LocalTime myObj = LocalTime.now();
	sendKeys(txt_TaskName,TaskName+myObj);
	WaitElement(txt_TaskType);
	selectIndex(txt_TaskType, 2);
	
	WaitClick(btn_InputProducts);
	click(btn_InputProducts);
	WaitClick(btn_productbutton);
	click(btn_productbutton);
	Thread.sleep(4000);
	sendKeys(txt_producttxt, producttxt);
	Thread.sleep(4000);
	pressEnter(txt_producttxt);
	Thread.sleep(4000);
	doubleClick(sel_productsel);
	WaitElement(txt_EstimatedQty);
	sendKeys(txt_EstimatedQty, EstimatedQty);
	JavascriptExecutor js1 = (JavascriptExecutor)driver;
	js1.executeScript("$(\"#tblSADProduct\").children().find(\".unchecked\").click()");
	
	WaitClick(btn_Labour);
	click(btn_Labour);
	WaitClick(btn_ServiceProductbutton);
	click(btn_ServiceProductbutton);
	Thread.sleep(4000);
	sendKeys(txt_ServiceProducttxt, ServiceProducttxt);
	Thread.sleep(4000);
	pressEnter(txt_ServiceProducttxt);
	Thread.sleep(4000);
	doubleClick(sel_ServiceProductsel);
	WaitClick(txt_EstimatedWork);
	click(txt_EstimatedWork);
	Thread.sleep(4000);
	number4=getText(sel_EstimatedWorksel);
	Thread.sleep(4000);
	number6=Integer.parseInt(number4);
	click(sel_EstimatedWorksel);
	click(btn_EstimatedWorkhourok);
	js1.executeScript("$(\"#tblSADService\").children().find(\".unchecked\").click()");
	
	WaitClick(btn_Resource);
	click(btn_Resource);
	WaitClick(btn_Resourcebutton);
	click(btn_Resourcebutton);
	Thread.sleep(4000);
	sendKeys(txt_Resourcetxt, Resourcetxt);
	Thread.sleep(4000);
	pressEnter(txt_Resourcetxt);
	Thread.sleep(4000);
	doubleClick(sel_Resourcesel);
	WaitClick(btn_ResourceServiceProductbutton);
	click(btn_ResourceServiceProductbutton);
	Thread.sleep(4000);
	sendKeys(txt_ServiceProducttxt, ServiceProducttxt);
	Thread.sleep(4000);
	pressEnter(txt_ServiceProducttxt);
	Thread.sleep(4000);
	doubleClick(sel_ServiceProductsel);
	WaitClick(txt_ResourceEstimatedWork);
	click(txt_ResourceEstimatedWork);
	Thread.sleep(4000);
	number10=getText(sel_ResourceEstimatedWorksel);
	Thread.sleep(4000);
	number11=Integer.parseInt(number10);
	Thread.sleep(4000);
	click(sel_ResourceEstimatedWorksel);
	js1.executeScript("$(\"#tblSADResource\").children().find(\".unchecked\").click()");
	
	WaitClick(btn_Expense);
	click(btn_Expense);
	WaitElement(txt_AnalysisCode);
	selectIndex(txt_AnalysisCode, 2);
	WaitClick(btn_ExpenseServiceProductbutton);
	click(btn_ExpenseServiceProductbutton);
	Thread.sleep(4000);
	sendKeys(txt_ServiceProducttxt, ServiceProducttxt);
	Thread.sleep(4000);
	pressEnter(txt_ServiceProducttxt);
	Thread.sleep(4000);
	doubleClick(sel_ServiceProductsel);
	WaitElement(txt_EstimatedCost);
	sendKeys(txt_EstimatedCost, EstimatedCost);
	js1.executeScript("$(\"#tblSADExpence\").children().find(\".unchecked\").click()");
	
	WaitClick(btn_TaskUpdate);
	click(btn_TaskUpdate);
	
	pageRefersh();
	Thread.sleep(3000);
	click(btn_WorkBreakdown);
	WaitClick(btn_releaseTaskdropdown);
	click(btn_releaseTaskdropdown);
	WaitClick(btn_releaseTask);
	click(btn_releaseTask);
	
	pageRefersh();
	Thread.sleep(3000);
	WaitClick(btn_update);
	click(btn_update);
}

public void  functionOfRecordingActualHours() throws Exception {

	clickNavigation();
	click(btn_serbutton);
	click(btn_DailyWorkSheetbutton);
	if(isDisplayed(txt_DailyWorkSheetpage)) {
		writeTestResults("verify Daily WorkSheet page","view Daily WorkSheet page", "Daily WorkSheet page is display", "pass");
	}else {
		writeTestResults("verify Daily WorkSheet page","view Daily WorkSheet page", "Daily WorkSheet page is not display", "fail");
	}
	click(btn_newDailyWorkSheetbutton);
	Thread.sleep(4000);
	click(btn_DailyWorkSheetProjectbutton);
	if(isDisplayed(txt_newDailyWorkSheetpage)) {
		writeTestResults("verify New Daily WorkSheet page","view New Daily WorkSheet page", "New Daily WorkSheet page is display", "pass");
	}else {
		writeTestResults("verify New Daily WorkSheet page","view New Daily WorkSheet page", "New Daily WorkSheet page is not display", "fail");
	}
	Thread.sleep(4000);
	click(btn_AddNewRecord);
	Thread.sleep(4000);
	click(btn_JobNobutton);
	Thread.sleep(4000);
	sendKeys(txt_JobNotxt, number1);
	pressEnter(txt_JobNotxt);
	Thread.sleep(4000);
	doubleClick(sel_JobNosel);
	Thread.sleep(4000);
	click(btn_Employeebutton);
	Thread.sleep(4000);
	sendKeys(txt_Employeetxt, ResponsiblePersontxt);
	pressEnter(txt_Employeetxt);
	Thread.sleep(4000);
	doubleClick(sel_Employeesel);
	
	
	Thread.sleep(4000);
	click(txt_TimeIn);
	Thread.sleep(4000);
	TimeInsel=getText(sel_TimeInsel);
	Thread.sleep(4000);
	TimeIn=Integer.parseInt(TimeInsel);
	click(sel_TimeInsel);
	Thread.sleep(4000);
	click(btn_TimeInOK);
	Thread.sleep(4000);
	click(txt_TimeOut);
	Thread.sleep(4000);
	TimeOutsel=getText(sel_TimeOutsel);
	Thread.sleep(4000);
	TimeOut=Integer.parseInt(TimeOutsel);
	ActualWork= TimeOut-TimeIn;
	System.out.println("ActualWork:"+ActualWork);
	click(sel_TimeOutsel);
	Thread.sleep(4000);
	click(btn_TimeOutOK);
	Thread.sleep(4000);
	click(btn_AddNewJobupdate);
	Thread.sleep(4000);
	click(btn_draft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Daily WorkSheet Document Draft","Daily WorkSheet Document Draft", "Daily WorkSheet Document is Draft", "pass");
	} else {
		writeTestResults("verify Daily WorkSheet Document Draft","Daily WorkSheet Document Draft", "Daily WorkSheet Document is not Draft", "fail");

	}
	
	Thread.sleep(4000);
	click(btn_release);
	Thread.sleep(4000);
	trackCode= getText(Header);
	Thread.sleep(5000);
	if (isDisplayed(pageRelease)){

		writeTestResults("verify Daily WorkSheet Document Released","Daily WorkSheet Document Released", "Daily WorkSheet Document is Released", "pass");
	} else {
		writeTestResults("verify Daily WorkSheet Document Released","Daily WorkSheet Document Released", "Daily WorkSheet Document is not Released", "fail");

	}
}

public void   actualUpdateFunction() throws Exception {
	
	Thread.sleep(4000);
	click(btn_Action);
	click(btn_ActualUpdate);
	click(btn_ActionResource);
	Thread.sleep(4000);
	click(btn_ActualWork);
	Thread.sleep(4000);
	TimeActual=getText(btn_ActualWorksel);
	Thread.sleep(4000);
	click(btn_ActualWorksel);
	Thread.sleep(4000);
	click(btn_ActualWorkOK);
	
//	Thread.sleep(4000);
//	click(btn_ActionUpdate);
	
	Thread.sleep(3000);
	JavascriptExecutor j1 = (JavascriptExecutor)driver;
	j1.executeScript("$(\"#divActualUpdate\").parent().find('.dialogbox-buttonarea > .button').click()");
	
	Thread.sleep(4000);
	click(btn_ActualUpdateclose);
	pageRefersh();
	
}

public void   VerifyThatEstimatedCost() throws Exception {
	
	WaitClick(btn_CostingSummary);
	click(btn_CostingSummary);
	WaitClick(btn_Material);
	click(btn_Material);
	WaitElement(txt_Materialvalue);
	number= getText(txt_Materialvalue).replace(".00000", "");
	//System.out.println(number1);
	
	WaitClick(btn_WorkBreakdown);
	click(btn_WorkBreakdown);
	WaitClick(btn_releaseTaskdropdown);
	click(btn_releaseTaskdropdown);
	WaitClick(btn_editTask);
	click(btn_editTask);
	
	WaitClick(btn_InputProducts);
	click(btn_InputProducts);
	Thread.sleep(6000);
	click(btn_productinfo);
	WaitClick(btn_ProductRelatedPrice);
	click(btn_ProductRelatedPrice);
	WaitElement(txt_LastPurchasePrice);
	number2=getText(txt_LastPurchasePrice).replace(".00000", "");
	x= Integer.parseInt(EstimatedQty);
	System.out.println(x*Integer.parseInt(number2));
	z=String.valueOf(x*Integer.parseInt(number2));
	
	if(number.equals(z)) {
		System.out.println("correct estimated cost is updated in 'allocated' column against material");
		writeTestResults("Verify that estimated cost for input products is updated in costing summary tab","correct estimated cost for input products is updated", "correct estimated cost is updated in 'allocated' column against material", "pass");
	}else {
		System.out.println("correct estimated cost is not updated in 'allocated' column against material");
		writeTestResults("Verify that estimated cost for input products is updated in costing summary tab","correct estimated cost for input products is updated", "correct estimated cost is not updated in 'allocated' column against material", "fail");
	}
}

public void   VerifyThatEstimatedCostForLabour() throws Exception {
	
	WaitClick(btn_CostingSummary);
	click(btn_CostingSummary);
	WaitClick(btn_Hours);
	click(btn_Hours);
	WaitElement(txt_Hoursvalue);
	number2= getText(txt_Hoursvalue);
	number9 = number2.replaceAll(",", "");
	
	clickNavigation();
	WaitClick(btn_Orgabutton);
	click(btn_Orgabutton);
	WaitClick(btn_EmployeeInformation);
	click(btn_EmployeeInformation);
	WaitElement(txt_EmployeeInformationTemplate);
	selectIndex(txt_EmployeeInformationTemplate, 0);
	WaitElement(txt_EmployeeInformationtxt);
	sendKeys(txt_EmployeeInformationtxt, ResponsiblePersontxt);
	Thread.sleep(4000);
	pressEnter(txt_EmployeeInformationtxt);
	Thread.sleep(4000);
	doubleClick(sel_EmployeeInformationsel);
	WaitElement(txt_RateProfiletxt);
	number3= getText(txt_RateProfiletxt);

	clickNavigation();
	WaitClick(btn_Orgabutton);
	click(btn_Orgabutton);
	WaitClick(btn_RateProfile);
	click(btn_RateProfile);
	WaitElement(txt_RateProfileserch);
	sendKeys(txt_RateProfileserch, number3);
	Thread.sleep(4000);
	pressEnter(txt_RateProfileserch);
	Thread.sleep(4000);
	driver.findElement(getLocator(txt_RateProfileserch)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(txt_RateProfileserch);
	WaitElement(txt_ExternalHourlyRate);
	RateProfileValue=getText(txt_ExternalHourlyRate).replace(".00000", "");
	
	
	
//	Thread.sleep(4000);
	number5=Integer.parseInt(RateProfileValue);
//	Thread.sleep(4000);
	number7= (number5)*(number6);
	number8= String.valueOf(number7+".00000");
	if(number8.equals(number9)) {
		System.out.println("correct estimated cost is updated in 'allocated' column against Hours");
		writeTestResults("Verify that estimated cost for labour is updated in costing summary tab","correct estimated cost for labour is updated", "correct estimated cost is updated in 'allocated' column against Hours", "pass");
	}else {
		System.out.println("correct estimated cost is not updated in 'allocated' column against Hours");
		writeTestResults("Verify that estimated cost for labour is updated in costing summary tab","correct estimated cost for labour is updated", "correct estimated cost is not updated in 'allocated' column against Hours", "fail");
	}
}

public void   VerifyThatEstimatedCostForResource() throws Exception {
	
	WaitClick(btn_CostingSummary);
	click(btn_CostingSummary);
	WaitClick(btn_ResourceCost);
	click(btn_ResourceCost);
	WaitElement(txt_ResourceCostvalue);
	number2= getText(txt_ResourceCostvalue);
	number9 = number2.replaceAll(",", "");
	
	clickNavigation();
	WaitClick(btn_Orgabutton);
	click(btn_Orgabutton);
	WaitClick(btn_ResourceInformation);
	click(btn_ResourceInformation);
	WaitElement(txt_EmployeeInformationTemplate);
	selectIndex(txt_EmployeeInformationTemplate, 0);
	WaitElement(txt_ResourceInformationtxt);
	sendKeys(txt_ResourceInformationtxt, Resourcetxt);
	Thread.sleep(4000);
	pressEnter(txt_ResourceInformationtxt);
	Thread.sleep(4000);
	doubleClick(sel_ResourceInformationsel);
	WaitElement(txt_RateProfiletxt);
	number3= getText(txt_RateProfiletxt);
	
	clickNavigation();
	WaitClick(btn_Orgabutton);
	click(btn_Orgabutton);
	WaitClick(btn_RateProfile);
	click(btn_RateProfile);
	WaitElement(txt_RateProfileserch);
	sendKeys(txt_RateProfileserch, number3);
	Thread.sleep(4000);
	driver.findElement(getLocator(txt_RateProfileserch)).sendKeys(Keys.ARROW_DOWN);
	pressEnter(txt_RateProfileserch);
	WaitElement(txt_ExternalHourlyRate);
	RateProfileValue=getText(txt_ExternalHourlyRate).replace(".00000", "");
	
//	Thread.sleep(4000);
	number5=Integer.parseInt(RateProfileValue);
	
//	Thread.sleep(4000);
	number7= (number5)*(number11);
	number8= String.valueOf(number7+".00000");
	if(number8.equals(number9)) {
		System.out.println("correct estimated cost is updated in 'allocated' column against Resource");
		writeTestResults("Verify that estimated cost for resource is updated in costing summary tab","correct estimated cost for resource is updated", "correct estimated cost is updated in 'allocated' column against Resource", "pass");
	}else {
		System.out.println("correct estimated cost is not updated in 'allocated' column against Resource");
		writeTestResults("Verify that estimated cost for resource is updated in costing summary tab","correct estimated cost for resource is updated", "correct estimated cost is not updated in 'allocated' column against Resource", "fail");
	}
}

public void  VerifyThatEstimatedCostForExpense() throws Exception {
	
	WaitClick(btn_CostingSummary);
	click(btn_CostingSummary);
	WaitClick(btn_Expensearrow);
	click(btn_Expensearrow);
	
	WaitElement(txt_Expensevalue);
	number1= getText(txt_Expensevalue).replace(",", "");
	WaitClick(btn_WorkBreakdown);
	click(btn_WorkBreakdown);
	WaitClick(btn_releaseTaskdropdown);
	click(btn_releaseTaskdropdown);
	WaitClick(btn_editTask);
	click(btn_editTask);
	WaitClick(btn_Expense);
	click(btn_Expense);
	
	WaitElement(txt_EstimatedCostAfter);
	number2= getText(txt_EstimatedCostAfter);
	if(number1.equals(number2)) {
		System.out.println("correct estimated cost is updated in 'allocated' column against Expense");
		writeTestResults("Verify that estimated cost for expense is updated in costing summary tab","correct estimated cost for expense is updated", "correct estimated cost is updated in 'allocated' column against Expense", "pass");
	}else {
		System.out.println("correct estimated cost is not updated in 'allocated' column against Expense");
		writeTestResults("Verify that estimated cost for expense is updated in costing summary tab","correct estimated cost for expense is updated", "correct estimated cost is not updated in 'allocated' column against Expense", "fail");
	}
	
}

public void  VerifyTheAbilityOfCreatingInternalOrders() throws Exception {
	
	Thread.sleep(4000);
	click(btn_Action);
	click(btn_CreateInternalOrder);
	selectIndex(btn_Taskdropdown, 2);
	Thread.sleep(4000);
	JavascriptExecutor jse = (JavascriptExecutor)driver;
	jse.executeScript("$(\'#comOrderTbl tbody\').find(\".unchecked\").click();");
	Thread.sleep(4000);
	click(btn_Apply);
	
	switchWindow();
	Thread.sleep(4000);
	click(btn_draft);
	Thread.sleep(4000);
	click(btn_release);
	Thread.sleep(4000);
	click(btn_Gotopage);
	
	
	switchWindow();
	Thread.sleep(4000);
	click(btn_draft);
	Thread.sleep(4000);
	click(btn_release);
}


public void  VerifyTheAbilityOfUpdatingTheActual() throws Exception {
	
	Thread.sleep(4000);
	click(btn_Action);
	click(btn_CreateInternalOrder);
	selectIndex(btn_Taskdropdown, 2);
	Thread.sleep(4000);
	JavascriptExecutor jse = (JavascriptExecutor)driver;
	jse.executeScript("$(\'#comOrderTbl tbody\').find(\".unchecked\").click();");
	Thread.sleep(4000);
	click(btn_Apply);
	
	switchWindow();
	Thread.sleep(4000);
	click(btn_draft);
	Thread.sleep(4000);
	click(btn_release);
	Thread.sleep(4000);
	click(btn_Gotopage);
	
	Thread.sleep(4000);
	switchWindow();
	Thread.sleep(4000);
	click(btn_draft);
	Thread.sleep(4000);
	click(btn_release);
	Thread.sleep(4000);
	number2= getText(txt_InternalDispatchOrdercode);

	
	Thread.sleep(5000);
	clickNavigation();
	Thread.sleep(4000);
	click(btn_projbutton);
	Thread.sleep(4000);
	click(btn_projectbutton);
	Thread.sleep(4000);
	sendKeys(txt_projectcodeserch, number1);
	Thread.sleep(4000);
	pressEnter(txt_projectcodeserch);
	Thread.sleep(4000);
	doubleClick(sel_projectcodeserchsel);
	Thread.sleep(4000);
	click(btn_Action);
	Thread.sleep(4000);
	click(btn_ActualUpdate);
	sendKeys(txt_ActualQty, ActualQty);
	Thread.sleep(4000);
	click(btn_ActionUpdate);
	Thread.sleep(4000);
	click(btn_ActualUpdateclose);
}

public void  VerifyTheAccuracyOfLabourRatesCalculation() throws Exception {
	clickNavigation();
	click(btn_serbutton);
	click(btn_DailyWorkSheetbutton);
	if(isDisplayed(txt_DailyWorkSheetpage)) {
		writeTestResults("verify Daily WorkSheet page","view Daily WorkSheet page", "Daily WorkSheet page is display", "pass");
	}else {
		writeTestResults("verify Daily WorkSheet page","view Daily WorkSheet page", "Daily WorkSheet page is not display", "fail");
	}
	click(btn_newDailyWorkSheetbutton);
	Thread.sleep(4000);
	click(btn_DailyWorkSheetProjectbutton);
	if(isDisplayed(txt_newDailyWorkSheetpage)) {
		writeTestResults("verify New Daily WorkSheet page","view New Daily WorkSheet page", "New Daily WorkSheet page is display", "pass");
	}else {
		writeTestResults("verify New Daily WorkSheet page","view New Daily WorkSheet page", "New Daily WorkSheet page is not display", "fail");
	}
	Thread.sleep(4000);
	click(btn_AddNewRecord);
	Thread.sleep(4000);
	click(btn_JobNobutton);
	Thread.sleep(4000);
	sendKeys(txt_JobNotxt, number1);
	Thread.sleep(4000);
	pressEnter(txt_JobNotxt);
	Thread.sleep(4000);
	doubleClick(sel_JobNosel);
	Thread.sleep(4000);
	click(btn_Employeebutton);
	Thread.sleep(4000);
	sendKeys(txt_Employeetxt, ResponsiblePersontxt);
	Thread.sleep(4000);
	pressEnter(txt_Employeetxt);
	Thread.sleep(4000);
	doubleClick(sel_Employeesel);
	
	
	Thread.sleep(4000);
	click(txt_TimeIn);
	Thread.sleep(4000);
	TimeInsel=getText(sel_TimeInsel);
	TimeIn=Integer.parseInt(TimeInsel);
	click(sel_TimeInsel);
	Thread.sleep(4000);
	click(btn_TimeInOK);
	Thread.sleep(4000);
	click(txt_TimeOut);
	Thread.sleep(4000);
	TimeOutsel=getText(sel_TimeOutsel);
	Thread.sleep(4000);
	TimeOut=Integer.parseInt(TimeOutsel);
	ActualWork= TimeOut-TimeIn;
	z=String.valueOf(ActualWork);
	//System.out.println("ActualWork:"+ActualWork);
	click(sel_TimeOutsel);
	Thread.sleep(4000);
	click(btn_TimeOutOK);
	Thread.sleep(4000);
	click(btn_AddNewJobupdate);
	Thread.sleep(4000);
	click(btn_draft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraftNew)){

		writeTestResults("verify Daily WorkSheet Document Draft","Daily WorkSheet Document Draft", "Daily WorkSheet Document is Draft", "pass");
	} else {
		writeTestResults("verify Daily WorkSheet Document Draft","Daily WorkSheet Document Draft", "Daily WorkSheet Document is not Draft", "fail");

	}
	Thread.sleep(4000);
	click(btn_release);
	Thread.sleep(4000);
	trackCode= getText(Header);
	Thread.sleep(5000);
	if (isDisplayed(pageReleaseNew)){

		writeTestResults("verify Daily WorkSheet Document Released","Daily WorkSheet Document Released", "Daily WorkSheet Document is Released", "pass");
	} else {
		writeTestResults("verify Daily WorkSheet Document Released","Daily WorkSheet Document Released", "Daily WorkSheet Document is not Released", "fail");

	}
	
	
	Thread.sleep(4000);
	clickNavigation();
	Thread.sleep(4000);
	click(btn_projbutton);
	Thread.sleep(4000);
	click(btn_projectbutton);
	Thread.sleep(4000);
	sendKeys(txt_projectcodeserch, number1);
	Thread.sleep(4000);
	pressEnter(txt_projectcodeserch);
	Thread.sleep(4000);
	doubleClick(sel_projectcodeserchsel);
	
	click(btn_CostingSummary);
	click(btn_Hours);
	Thread.sleep(4000);
	number2= getText(txt_Hoursvalue);
	Thread.sleep(4000);
	number9 = number2.replaceAll(",", "");
	Thread.sleep(4000);
	clickNavigation();
	Thread.sleep(4000);
	click(btn_Orgabutton);
	Thread.sleep(4000);
	click(btn_EmployeeInformation);
	selectIndex(txt_EmployeeInformationTemplate, 0);
	sendKeys(txt_EmployeeInformationtxt, ResponsiblePersontxt);
	Thread.sleep(4000);
	pressEnter(txt_EmployeeInformationtxt);
	Thread.sleep(4000);
	doubleClick(sel_EmployeeInformationsel);
	Thread.sleep(4000);
	number3= getText(txt_RateProfiletxt);
	
	Thread.sleep(4000);
	clickNavigation();
	Thread.sleep(4000);
	click(btn_Orgabutton);
	Thread.sleep(4000);
	click(btn_RateProfile);
	Thread.sleep(4000);
	sendKeys(txt_RateProfileserch, number3);
	Thread.sleep(4000);
	pressEnter(txt_RateProfileserch);
	Thread.sleep(4000);
	driver.findElement(getLocator(txt_RateProfileserch)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(txt_RateProfileserch);
	Thread.sleep(4000);
	RateProfileValue=getText(txt_ExternalHourlyRate).replace(".00000", "");
	
	
	
	Thread.sleep(4000);
	number5=Integer.parseInt(RateProfileValue);
	Thread.sleep(4000);
	number7= (number5)*(number6);
	Thread.sleep(4000);
	number8= String.valueOf(number7+".00000");
	System.out.println(number8);
	System.out.println(number9);
	if(number8.equals(number9)) {
		System.out.println("correct estimated cost is updated in 'allocated' column");
		writeTestResults("verify correct estimated cost is updated in 'allocated' column","correct estimated cost is updated", "correct estimated cost is updated in 'allocated' column", "pass");
	}else {
		System.out.println("incorrect estimated cost is updated in 'allocated' column");
		writeTestResults("verify correct estimated cost is updated in 'allocated' column","correct estimated cost is updated", "correct estimated cost is not updated in 'allocated' column", "fail");
	}
	
	
	Thread.sleep(4000);
	clickNavigation();
	Thread.sleep(4000);
	click(btn_projbutton);
	Thread.sleep(4000);
	click(btn_projectbutton);
	Thread.sleep(4000);
	sendKeys(txt_projectcodeserch, number1);
	Thread.sleep(4000);
	pressEnter(txt_projectcodeserch);
	Thread.sleep(4000);
	doubleClick(sel_projectcodeserchsel);
	
	Thread.sleep(4000);
	click(btn_Overview);
	click(btn_Labourtab);
	Thread.sleep(4000);
	WebElement el1=driver.findElement(By.xpath("//table[@id='lblProjectLabour']//td[12]/div/input"));
	String value1 = el1.getAttribute("value");
	Thread.sleep(4000);
	WebElement el2=driver.findElement(By.xpath("//table[@id='lblProjectLabour']//td[13]/div/input"));
	String value2 = el2.getAttribute("value");
	
//	System.out.println("estimate"+number4);
//	System.out.println("estimate"+value1);
	
	if(value1.equals(number4)) {
		System.out.println("estimated labour hours are correct updated");
		writeTestResults("Verify whether estimated labour hours are correct","estimated labour hours are correct", "estimated labour hours are correct updated", "pass");
	}else {
		System.out.println("estimated labour hours are incorrect updated");
		writeTestResults("Verify whether estimated labour hours are correct","estimated labour hours are correct", "estimated labour hours are incorrect updated", "fail");
	}
	
	//System.out.println("actual"+value2);
	
	if(z.equals(value2.replaceAll("0", ""))) {
		System.out.println("actual labour hours are correct updated");
		writeTestResults("Verify whether actual labour hours are correct","actual labour hours are correct", "actual labour hours are correct updated", "pass");
	}else {
		System.out.println("actual labour hours are incorrect updated");
		writeTestResults("Verify whether actual labour hours are correct","actual labour hours are correct", "actual labour hours are incorrect updated", "fail");
	}
}

public void  VerifyTheReflectionOfActualUtilization() throws Exception {
	
	Thread.sleep(4000);
	click(btn_Action);
	WaitClick(btn_CreateInternalOrder);
	click(btn_CreateInternalOrder);
	WaitElement(btn_Taskdropdown);
	selectIndex(btn_Taskdropdown, 2);
	WaitElement("//table[@id=\"comOrderTbl\"]//input[@class='unchecked el-resize1']");
	JavascriptExecutor jse = (JavascriptExecutor)driver;
	jse.executeScript("$(\'#comOrderTbl tbody\').find(\".unchecked\").click();");
	WaitClick(btn_Apply);
	click(btn_Apply);
	
	switchWindow();
	
	Thread.sleep(4000);
	WaitClick(btn_draft);
	click(btn_draft);
	WaitClick(btn_release);
	click(btn_release);
	WaitClick(btn_Gotopage);
	click(btn_Gotopage);
	
	switchWindow();
	Thread.sleep(4000);
	click(btn_draft);
	
	WaitClick(createGridMasterInfo1A);
	click(createGridMasterInfo1A);
	WaitClick(ProductBatchSerialWiseCostA);
	click(ProductBatchSerialWiseCostA);
	WaitElement(ProductBatchSelA);
	String val1=getText(ProductBatchSelA);
	WaitClick(headercloseA);
	click(headercloseA);
	
	WaitClick(createGridMasterInfo4A);
	click(createGridMasterInfo4A);
	WaitClick(ProductBatchSerialWiseCostA);
	click(ProductBatchSerialWiseCostA);
	WaitElement(ProductBatchSerialWiseCostSelA);
	String val4=getText(ProductBatchSerialWiseCostSelA);
	WaitClick(headercloseA);
	click(headercloseA);
	
	WaitClick(createGridMasterInfo5A);
	click(createGridMasterInfo5A);
	WaitClick(ProductBatchSerialWiseCostA);
	click(ProductBatchSerialWiseCostA);
	WaitElement(ProductBatchSerialWiseCostSelA);
	String val5=getText(ProductBatchSerialWiseCostSelA);
	WaitClick(headercloseA);
	click(headercloseA);
	
	Thread.sleep(4000);
	click(SerialBatchA);
	
	Thread.sleep(4000);
	click(Cap1A);
	WaitElement(BatchFromNuA);
	sendKeys(BatchFromNuA, val1);
	pressEnter(BatchFromNuA);
	Thread.sleep(4000);
	click(updateSerialA);
	
	Thread.sleep(4000);
	click(Cap4A);
	WaitClick(SerialRangeA);
	click(SerialRangeA);
	Thread.sleep(3000);
	JavascriptExecutor j1 = (JavascriptExecutor)driver;
	j1.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",1)");
	WaitElement(SerialFromNuA);
	sendKeys(SerialFromNuA, val4);
	pressEnter(SerialFromNuA);
	Thread.sleep(4000);
	click(updateSerialA);
	
	Thread.sleep(4000);
	click(Cap5A);
	WaitClick(SerialRangeA);
	click(SerialRangeA);
	Thread.sleep(3000);
	JavascriptExecutor j2 = (JavascriptExecutor)driver;
	j2.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",1)");
	WaitElement(SerialFromNuA);
	sendKeys(SerialFromNuA, val5);
	pressEnter(SerialFromNuA);
	Thread.sleep(4000);
	click(updateSerialA);
	
	Thread.sleep(4000);
	click(closeProductListA);
	WaitClick(btn_release);
	click(btn_release);
	
	Thread.sleep(8000);
	clickNavigation();
	WaitClick(btn_projbutton);
	click(btn_projbutton);
	WaitClick(btn_projectbutton);
	click(btn_projectbutton);
	
	WaitElement(txt_projectcodeserch);
	sendKeys(txt_projectcodeserch, number1);
	pressEnter(txt_projectcodeserch);
	Thread.sleep(4000);
	doubleClick(sel_projectcodeserchsel);
	
	Thread.sleep(4000);
	click(btn_Action);
	WaitClick(btn_ActualUpdate);
	click(btn_ActualUpdate);
//	sendKeys(txt_ActualQty, ActualQty);
	Thread.sleep(6000);
	number3="1";
	x=Integer.parseInt(number3);
	//System.out.println("x:"+x);
	
	WaitClick(openSerialNoSelection1A);
	click(openSerialNoSelection1A);
	WaitClick(captureQtyA);
	click(captureQtyA);
	WaitClick(ActualSerialUpdate);
	click(ActualSerialUpdate);
	WaitClick(ActualSerialYesA);
	click(ActualSerialYesA);
	
	WaitClick(openSerialNoSelection4A);
	click(openSerialNoSelection4A);
	WaitClick(captureQtyA);
	click(captureQtyA);
	WaitClick(ActualSerialUpdate);
	click(ActualSerialUpdate);
	WaitClick(ActualSerialYesA);
	click(ActualSerialYesA);
	
	WaitClick(openSerialNoSelection5A);
	click(openSerialNoSelection5A);
	WaitClick(captureQtyA);
	click(captureQtyA);
	WaitClick(ActualSerialUpdate);
	click(ActualSerialUpdate);
	WaitClick(ActualSerialYesA);
	click(ActualSerialYesA);
	
	
	WaitClick(btn_ActionUpdate);
	click(btn_ActionUpdate);
	
	pageRefersh();
	Thread.sleep(2000);
	WaitClick(btn_WorkBreakdown);
	click(btn_WorkBreakdown);
	WaitClick(btn_releaseTaskdropdown);
	click(btn_releaseTaskdropdown);
	WaitClick(btn_editTask);
	click(btn_editTask);
	
	WaitClick(btn_InputProducts);
	click(btn_InputProducts);
	Thread.sleep(4000);
	number10=getText(txt_EstimatedQtyfiled);
	
	WaitClick(btn_productinfo);
	click(btn_productinfo);
	WaitClick(btn_ProductRelatedPrice);
	click(btn_ProductRelatedPrice);
	Thread.sleep(4000);
	number2=getText(txt_LastPurchasePrice).replace(".00000", "");
	y=Integer.parseInt(number2);
	//System.out.println(y);
	
	z= String.valueOf(x*y*7);
	
	pageRefersh();
	
	click(btn_CostingSummary);
	click(btn_Material);
	Thread.sleep(4000);
	number4= getText(txt_MaterialActualvalue).replace(".00000", "");
	System.out.println(number4);


	if(number4.equals(z)) {
		System.out.println("actual cost of input materials is updated in 'Actual' column correct");
		writeTestResults("Verify whether actual cost of input materials is updated in 'Actual' column against material","actual cost of input materials is updated in 'Actual' column", "actual cost of input materials is updated in 'Actual' column correct", "pass");
	}else {
		System.out.println("actual cost of input materials is updated in 'Actual' column incorrect");
		writeTestResults("Verify whether actual cost of input materials is updated in 'Actual' column against material","actual cost of input materials is updated in 'Actual' column", "actual cost of input materials is updated in 'Actual' column incorrect", "fail");
	}
	
	click(btn_Overview);
	click(btn_InputProductstab);
	
	Thread.sleep(4000);
	number8=getText(txt_ActualQtybox);
	
	
	if(number3.equals(number8)) {
		System.out.println("same value updated for Actual Qty box");
		writeTestResults("verify same value updated for Actual Qty box","same value updated", "same value updated for Actual Qty box", "pass");
	}else {
		System.out.println("incorrect value updated for Actual Qty box");
		writeTestResults("verify same value updated for Actual Qty box","same value updated", "incorrect value updated for Actual Qty box", "fail");
	}
	
	Thread.sleep(4000);
	number9= getText(txt_EstimatedQtybox);
	
	
	if(number10.equals(number9)) {
		System.out.println("same value updated for Estimated Qty box");
		writeTestResults("verify same value updated for Estimated Qty box","same value updated", "same value updated for Estimated Qty box", "pass");
	}else {
		System.out.println("incorrect value updated for Estimated Qty box");
		writeTestResults("verify same value updated for Estimated Qty box","same value updated", "incorrect value updated for Estimated Qty box", "fail");
	}
}


public void  VerifyTheAbilityOfCreatingInternalReturnOrders() throws Exception {
	
	Thread.sleep(4000);
	click(btn_Action);
	click(btn_CreateInternalOrder);
	selectIndex(btn_Taskdropdown, 2);
	Thread.sleep(4000);
	JavascriptExecutor jse = (JavascriptExecutor)driver;
	jse.executeScript("$(\'#comOrderTbl tbody\').find(\".unchecked\").click();");
	Thread.sleep(4000);
	click(btn_Apply);
	
	switchWindow();
	Thread.sleep(4000);
	click(btn_draft);
	Thread.sleep(4000);
	click(btn_release);
	Thread.sleep(4000);
	click(btn_Gotopage);
	
	switchWindow();
	Thread.sleep(4000);
	click(btn_draft);
	
	Thread.sleep(4000);
	click(createGridMasterInfo1A);
	Thread.sleep(4000);
	click(ProductBatchSerialWiseCostA);
	Thread.sleep(4000);
	String val1=getText(ProductBatchSelA);
	Thread.sleep(4000);
	click(headercloseA);
	
	Thread.sleep(4000);
	click(createGridMasterInfo4A);
	Thread.sleep(4000);
	click(ProductBatchSerialWiseCostA);
	Thread.sleep(4000);
	String val4=getText(ProductBatchSerialWiseCostSelA);
	String val4_2=getText(ProductBatchSerialWiseCostSel2A);
	Thread.sleep(4000);
	click(headercloseA);
	
	Thread.sleep(4000);
	click(createGridMasterInfo5A);
	Thread.sleep(4000);
	click(ProductBatchSerialWiseCostA);
	Thread.sleep(4000);
	String val5=getText(ProductBatchSerialWiseCostSelA);
	String val5_2=getText(ProductBatchSerialWiseCostSel2A);
	Thread.sleep(4000);
	click(headercloseA);
	
	Thread.sleep(4000);
	click(SerialBatchA);
	
	Thread.sleep(4000);
	click(Cap1A);
	Thread.sleep(4000);
	sendKeys(BatchFromNuA, val1);
	Thread.sleep(4000);
	pressEnter(BatchFromNuA);
	Thread.sleep(4000);
	click(updateSerialA);
	
	Thread.sleep(4000);
	click(Cap4A);
	Thread.sleep(4000);
	sendKeys(SerialFromNuA, val4);
	Thread.sleep(4000);
	pressEnter(SerialFromNuA);
	Thread.sleep(4000);
	sendKeys(SerialFromNuA, val4_2);
	Thread.sleep(4000);
	pressEnter(SerialFromNuA);
	Thread.sleep(4000);
	click(updateSerialA);
	
	Thread.sleep(4000);
	click(Cap5A);
	Thread.sleep(4000);
	sendKeys(SerialFromNuA, val5);
	Thread.sleep(4000);
	pressEnter(SerialFromNuA);
	Thread.sleep(4000);
	sendKeys(SerialFromNuA, val5_2);
	Thread.sleep(4000);
	pressEnter(SerialFromNuA);
	Thread.sleep(4000);
	click(updateSerialA);
	
	Thread.sleep(4000);
	click(closeProductListA);
	Thread.sleep(4000);
	click(btn_release);
	Thread.sleep(4000);
	number2= getText(txt_InternalDispatchOrdercode);
	System.out.println(number2);
	
	Thread.sleep(5000);
	clickNavigation();
	Thread.sleep(4000);
	click(btn_projbutton);
	Thread.sleep(4000);
	click(btn_projectbutton);
	Thread.sleep(4000);
	sendKeys(txt_projectcodeserch, number1);
	Thread.sleep(4000);
	pressEnter(txt_projectcodeserch);
	Thread.sleep(4000);
	doubleClick(sel_projectcodeserchsel);
	
	Thread.sleep(4000);
	click(btn_Action);
	Thread.sleep(4000);
	click(btn_ActualUpdate);
//	sendKeys(txt_ActualQty, ActualQty);
	Thread.sleep(4000);
	number3="1";
	x=Integer.parseInt(number3);
	//System.out.println("x:"+x);
	

	Thread.sleep(4000);
	sendKeys(ActualQty1, "1");
	Thread.sleep(4000);
	sendKeys(ActualQty2, "1");
	Thread.sleep(4000);
	sendKeys(ActualQty3, "1");
	
	Thread.sleep(4000);
	click(openSerialNoSelection4A);
	Thread.sleep(4000);
	click(captureQtyA);
	Thread.sleep(4000);
	click(ActualSerialUpdate);
	Thread.sleep(4000);
	click(ActualSerialYesA);
	
	Thread.sleep(4000);
	click(openSerialNoSelection5A);
	Thread.sleep(4000);
	click(captureQtyA);
	Thread.sleep(4000);
	click(ActualSerialUpdate);
	Thread.sleep(4000);
	click(ActualSerialYesA);
	
	Thread.sleep(4000);
	sendKeys(ActualQty6, "1");
	Thread.sleep(4000);
	sendKeys(ActualQty7, "1");
	
	
	Thread.sleep(4000);
	click(btn_ActionUpdate);
	Thread.sleep(4000);
	click(btn_ActualUpdateclose);
	
	Thread.sleep(4000);
	clickNavigation();
	click(btn_InventoryWarehousing);
	click(btn_InternalReturnOrder);
	click(btn_newInternalReturnOrder);
	Thread.sleep(4000);
	click(btn_WIPReturns);
	
	selectIndex(txt_ToWarehouse, 7);
	Thread.sleep(4000);
	click(btn_documentMark);
	Thread.sleep(4000);
	sendKeys(txt_CommonSearch, number2);
	Thread.sleep(5000);
	click(btn_Search);
	Thread.sleep(4000);
	JavascriptExecutor js = (JavascriptExecutor)driver;
	js.executeScript("$(\"#tblDocList tbody\").find(\".el-resize1\").click()");
	click(btn_ApplyIRO);
	Thread.sleep(4000);
	click(btn_draft);
	Thread.sleep(4000);
	click(btn_release);
	Thread.sleep(4000);
	click(btn_Gotopage);
	
	switchWindow();
	Thread.sleep(4000);
	click(btn_draftIR);
	
	Thread.sleep(4000);
	click(btn_SerialBatchIR);
	
	

	Thread.sleep(5000);
	click(Cap1A);
	Thread.sleep(4000);
	JavascriptExecutor j1 = (JavascriptExecutor)driver;
	j1.executeScript("$(\"#attr-divSerialBatchNos\").jqxGrid('setcellvalue', 0, \"Qty\", \"1\");");
	Thread.sleep(4000);
	click(updateSerialA);
	
	Thread.sleep(5000);
	click(Cap2A);
	Thread.sleep(4000);
	click(updateSerialA);
	
	Thread.sleep(5000);
	click(Cap3A);
	Thread.sleep(4000);
	click(updateSerialA);
	
	Thread.sleep(5000);
	click(Cap4A);
	Thread.sleep(4000);
	click(updateSerialA);
	
	Thread.sleep(5000);
	click(Cap5A);
	Thread.sleep(4000);
	click(updateSerialA);
	
	Thread.sleep(5000);
	click(Cap7A);
	Thread.sleep(4000);
	JavascriptExecutor j7 = (JavascriptExecutor)driver;
	j7.executeScript("$(\"#attr-divSerialBatchNos\").jqxGrid('setcellvalue', 0, \"Qty\", \"1\");");
	Thread.sleep(4000);
	click(updateSerialA);

	Thread.sleep(4000);
	click(btn_SerialBatchback);
	Thread.sleep(4000);
	click(btn_releaseIR);
}


public void  VerifyTheAbilityOfUpdatingTaskPOC() throws Exception {
	
	navigateToTheLoginPage();
	verifyTheLogo();
	userLogin();
	clickNavigation();
	clickProjectbutton();
	createProject();
	ProjectProduct7_2();
	
	//functionOfRecordingActualHours();
	VerifyTheAccuracyOfLabourRatesCalculation() ;

	Thread.sleep(8000);
	clickNavigation();
	Thread.sleep(4000);
	click(btn_projbutton);
	Thread.sleep(4000);
	click(btn_projectbutton);
	Thread.sleep(4000);
	sendKeys(txt_projectcodeserch, number1);
	Thread.sleep(4000);
	pressEnter(txt_projectcodeserch);
	Thread.sleep(4000);
	doubleClick(sel_projectcodeserchsel);
	
	actualUpdateFunction();
	
	
	//VerifyTheAbilityOfUpdatingTheActual();
	Thread.sleep(4000);
	click(btn_Action);
	click(btn_CreateInternalOrder);
	selectIndex(btn_Taskdropdown, 2);
	Thread.sleep(4000);
	JavascriptExecutor jse = (JavascriptExecutor)driver;
	jse.executeScript("$(\'#comOrderTbl tbody\').find(\".unchecked\").click();");
	Thread.sleep(4000);
	click(btn_Apply);
	
	switchWindow();
	Thread.sleep(4000);
	click(btn_draft);
	Thread.sleep(4000);
	click(btn_release);
	Thread.sleep(4000);
	click(btn_Gotopage);
	
	switchWindow();
	Thread.sleep(4000);
	click(btn_draft);
	
	Thread.sleep(4000);
	click(createGridMasterInfo1A);
	Thread.sleep(4000);
	click(ProductBatchSerialWiseCostA);
	Thread.sleep(4000);
	String val1=getText(ProductBatchSelA);
	Thread.sleep(4000);
	click(headercloseA);
	
	Thread.sleep(4000);
	click(createGridMasterInfo4A);
	Thread.sleep(4000);
	click(ProductBatchSerialWiseCostA);
	Thread.sleep(4000);
	String val4=getText(ProductBatchSerialWiseCostSelA);
	String val4_2=getText(ProductBatchSerialWiseCostSel2A);
	Thread.sleep(4000);
	click(headercloseA);
	
	Thread.sleep(4000);
	click(createGridMasterInfo5A);
	Thread.sleep(4000);
	click(ProductBatchSerialWiseCostA);
	Thread.sleep(4000);
	String val5=getText(ProductBatchSerialWiseCostSelA);
	String val5_2=getText(ProductBatchSerialWiseCostSel2A);
	Thread.sleep(4000);
	click(headercloseA);
	
	Thread.sleep(4000);
	click(SerialBatchA);
	
	Thread.sleep(4000);
	click(Cap1A);
	Thread.sleep(4000);
	sendKeys(BatchFromNuA, val1);
	Thread.sleep(4000);
	pressEnter(BatchFromNuA);
	Thread.sleep(4000);
	click(updateSerialA);
	
	Thread.sleep(4000);
	click(Cap4A);
	Thread.sleep(4000);
	sendKeys(SerialFromNuA, val4);
	Thread.sleep(4000);
	pressEnter(SerialFromNuA);
	Thread.sleep(4000);
	sendKeys(SerialFromNuA, val4_2);
	Thread.sleep(4000);
	pressEnter(SerialFromNuA);
	Thread.sleep(4000);
	click(updateSerialA);
	
	Thread.sleep(4000);
	click(Cap5A);
	Thread.sleep(4000);
	sendKeys(SerialFromNuA, val5);
	Thread.sleep(4000);
	pressEnter(SerialFromNuA);
	Thread.sleep(4000);
	sendKeys(SerialFromNuA, val5_2);
	Thread.sleep(4000);
	pressEnter(SerialFromNuA);
	Thread.sleep(4000);
	click(updateSerialA);
	
	Thread.sleep(4000);
	click(closeProductListA);
	Thread.sleep(4000);
	click(btn_release);
	Thread.sleep(4000);
	number2= getText(txt_InternalDispatchOrdercode);
	System.out.println(number2);
	
	Thread.sleep(8000);
	clickNavigation();
	Thread.sleep(4000);
	click(btn_projbutton);
	Thread.sleep(4000);
	click(btn_projectbutton);
	Thread.sleep(4000);
	sendKeys(txt_projectcodeserch, number1);
	Thread.sleep(4000);
	pressEnter(txt_projectcodeserch);
	Thread.sleep(4000);
	doubleClick(sel_projectcodeserchsel);
	
	Thread.sleep(4000);
	click(btn_Action);
	Thread.sleep(4000);
	click(btn_ActualUpdate);
//	sendKeys(txt_ActualQty, ActualQty);
	Thread.sleep(4000);
	number3="1";
	x=Integer.parseInt(number3);
	//System.out.println("x:"+x);
	
//	Thread.sleep(4000);
//	click(openSerialNoSelection1A);
//	Thread.sleep(4000);
//	click(captureQtyA);
//	Thread.sleep(4000);
//	sendKeys("//input[@class='el-resize7']", "1");
//	Thread.sleep(4000);
//	click(ActualSerialUpdate);
//	Thread.sleep(4000);
//	click(ActualSerialYesA);
	
	Thread.sleep(4000);
	sendKeys(ActualQty1, "1");
	Thread.sleep(4000);
	sendKeys(ActualQty2, "1");
	Thread.sleep(4000);
	sendKeys(ActualQty3, "1");
	
	Thread.sleep(4000);
	click(openSerialNoSelection4A);
	Thread.sleep(4000);
	click(captureQtyA);
	Thread.sleep(4000);
	click(ActualSerialUpdate);
	Thread.sleep(4000);
	click(ActualSerialYesA);
	
	Thread.sleep(4000);
	click(openSerialNoSelection5A);
	Thread.sleep(4000);
	click(captureQtyA);
	Thread.sleep(4000);
	click(ActualSerialUpdate);
	Thread.sleep(4000);
	click(ActualSerialYesA);
	
	Thread.sleep(4000);
	sendKeys(ActualQty6, "1");
	Thread.sleep(4000);
	sendKeys(ActualQty7, "1");
	
	
	Thread.sleep(4000);
	click(btn_ActionUpdate);
	Thread.sleep(4000);
	click(btn_ActualUpdateclose);
	
	Thread.sleep(6000);
	clickNavigation();
	click(btn_InventoryWarehousing);
	Thread.sleep(4000);
	click(btn_InternalReturnOrder);
	Thread.sleep(4000);
	click(btn_newInternalReturnOrder);
	Thread.sleep(4000);
	click(btn_WIPReturns);
	
	selectText(txt_ToWarehouse, "WH-PARTS [Main Warehouse - Parts]");
	Thread.sleep(4000);
	click(btn_documentMark);
	Thread.sleep(4000);
	sendKeys(txt_CommonSearch, number2);
	Thread.sleep(6000);
	click(btn_Search);
	Thread.sleep(8000);
	JavascriptExecutor js = (JavascriptExecutor)driver;
	js.executeScript("$(\"#tblDocList tbody\").find(\".el-resize1\").click()");
	Thread.sleep(3000);
	js.executeScript("$(\"#divDocList\").parent().find('.dialogbox-buttonarea > .button').click()");
	
	Thread.sleep(4000);
	click(btn_draft);
	Thread.sleep(4000);
	click(btn_release);
	Thread.sleep(8000);
	click(btn_Gotopage);
	
	switchWindow();
	Thread.sleep(4000);
	click(btn_draftIR);
	
	Thread.sleep(4000);
	click(btn_SerialBatchIR);
	
	Thread.sleep(4000);
	Actions action1 = new Actions(driver);
	WebElement c1 = driver.findElement(By.xpath(Cap1A));
	action1.moveToElement(c1).build().perform();
	click(Cap1A);
	Thread.sleep(4000);
	JavascriptExecutor j1 = (JavascriptExecutor)driver;
	j1.executeScript("$(\"#attr-divSerialBatchNos\").jqxGrid('setcellvalue', 0, \"Qty\", \"1\");");
	Thread.sleep(4000);
	click(updateSerialA);
	
	Thread.sleep(4000);
	Actions action2 = new Actions(driver);
	WebElement c2 = driver.findElement(By.xpath(Cap2A));
	action2.moveToElement(c2).build().perform();
	click(Cap2A);
	Thread.sleep(4000);
	click(updateSerialA);
	
	Thread.sleep(4000);
	Actions action3 = new Actions(driver);
	WebElement c3 = driver.findElement(By.xpath(Cap3A));
	action3.moveToElement(c3).build().perform();
	click(Cap3A);
	Thread.sleep(4000);
	click(updateSerialA);
	
	Thread.sleep(4000);
	Actions action4 = new Actions(driver);
	WebElement c4 = driver.findElement(By.xpath(Cap4A));
	action4.moveToElement(c4).build().perform();
	click(Cap4A);
	Thread.sleep(4000);
	click(updateSerialA);
	
	Thread.sleep(4000);
	Actions action5 = new Actions(driver);
	WebElement c5 = driver.findElement(By.xpath(Cap5A));
	action5.moveToElement(c5).build().perform();
	click(Cap5A);
	Thread.sleep(4000);
	click(updateSerialA);
	
	Thread.sleep(4000);
	Actions action7 = new Actions(driver);
	WebElement c7 = driver.findElement(By.xpath(Cap7A));
	action7.moveToElement(c7).build().perform();
	click(Cap7A);
	Thread.sleep(4000);
	JavascriptExecutor j7 = (JavascriptExecutor)driver;
	j7.executeScript("$(\"#attr-divSerialBatchNos\").jqxGrid('setcellvalue', 0, \"Qty\", \"1\");");
	Thread.sleep(4000);
	click(updateSerialA);

	Thread.sleep(4000);
	click(btn_SerialBatchback);
	Thread.sleep(4000);
	click(btn_releaseIR);
	
	Thread.sleep(8000);
	clickNavigation();
	Thread.sleep(4000);
	click(btn_projbutton);
	Thread.sleep(4000);
	click(btn_projectbutton);
	Thread.sleep(4000);
	sendKeys(txt_projectcodeserch, number1);
	Thread.sleep(4000);
	pressEnter(txt_projectcodeserch);
	Thread.sleep(4000);
	doubleClick(sel_projectcodeserchsel);

	Thread.sleep(4000);
	click(btn_Action);
	Thread.sleep(4000);
	click(btn_UpdateTaskPOC);
	selectIndex(txt_cboxTaskPOC, 1);
	sendKeys(txt_TskPOC, TskPOC);
	Thread.sleep(4000);
	click(btn_POCApply);
}

public void  VerifyTheAbilityOfClosingATask() throws Exception {
	
	VerifyTheAbilityOfUpdatingTaskPOC();
	
	Thread.sleep(4000);
	click(btn_Action);
	Thread.sleep(4000);
	click(btn_CompleteA);
	Thread.sleep(4000);
	click(btn_YesA);

	Thread.sleep(4000);
	JavascriptExecutor j7 = (JavascriptExecutor)driver;
	j7.executeScript("$(\"#txtTaskCompPostDate\").datepicker(\"setDate\", new Date())");

	Thread.sleep(4000);
	click(btn_checkboxA);
	Thread.sleep(4000);
	click(btn_Apply);
}

public void  VerifyTheAbilityOfAddingSubContract() throws Exception {
	
	navigateToTheLoginPage();
	verifyTheLogo();
	userLogin();
	clickNavigation();
	clickProjectbutton();
	createProject();
	Thread.sleep(4000);
	click(btn_edit);
	click(btn_WorkBreakdown);
	click(btn_AddTask);
	LocalTime myObj = LocalTime.now();
	sendKeys(txt_TaskName,TaskName+myObj);
	selectIndex(txt_TaskType, 2);
	selectIndex(txt_ContractTypeA, 1);
	click(btn_SubContractServicesA);
	
	click(btn_SubproductbuttonA);
	Thread.sleep(4000);
	sendKeys(txt_ServiceProducttxt, ServiceProducttxt);
	Thread.sleep(4000);
	pressEnter(txt_ServiceProducttxt);
	Thread.sleep(4000);
	doubleClick(sel_ServiceProductsel);
//	Thread.sleep(4000);
	sendKeys(txt_SubEstimatedCostA, EstimatedCost);
	Thread.sleep(4000);
	number3=EstimatedCost+".00000";
	Thread.sleep(4000);
	pressEnter(txt_SubEstimatedCostA);
//	Thread.sleep(4000);
//	click(btn_BillableA);
	JavascriptExecutor js1 = (JavascriptExecutor)driver;
	js1.executeScript("$(\"#tblSADSVProduct\").children().find(\".unchecked\").click()");
	
	click(btn_TaskUpdate);
	pageRefersh();
	Thread.sleep(3000);
	click(btn_WorkBreakdown);
	Thread.sleep(6000);
	click(btn_releaseTaskdropdown);
	Thread.sleep(4000);
	click(btn_releaseTask);
	pageRefersh();
	Thread.sleep(6000);
	click(btn_update);
}

public void  VerifyThatEstimatedCostForSubContract() throws Exception {
	
	VerifyTheAbilityOfAddingSubContract();
	click(btn_CostingSummary);
	click(btn_SubContractA);
	Thread.sleep(4000);
	number2=getText(txt_SubContractvalueA);
	number4 = number2.replaceAll(",", "");
	
	if(number4.equals(number3)) {
		System.out.println("correct estimated cost is updated in 'allocated' column");
		writeTestResults("verify correct estimated cost is updated in 'allocated' column","correct estimated cost is updated", "correct estimated cost is updated in 'allocated' column", "pass");
	}else {
		System.out.println("incorrect estimated cost is updated in 'allocated' column");
		writeTestResults("verify correct estimated cost is updated in 'allocated' column","correct estimated cost is updated", "correct estimated cost is not updated in 'allocated' column", "fail");
	}
}

public void  VerifyThatActualCostForResourceIsUpdated() throws Exception {
	
	navigateToTheLoginPage();
	verifyTheLogo();
	userLogin();
	clickNavigation();
	clickProjectbutton();
	createProject();
	addingAprojectTask();
	actualUpdateFunction();
	VerifyThatEstimatedCostForResource();
	System.out.println("Estimated:"+number10);
	System.out.println("actual:"+TimeActual);
	
	Thread.sleep(4000);
	clickNavigation();
	Thread.sleep(4000);
	click(btn_projbutton);
	Thread.sleep(4000);
	click(btn_projectbutton);
	Thread.sleep(4000);
	sendKeys(txt_projectcodeserch, number1);
	Thread.sleep(4000);
	pressEnter(txt_projectcodeserch);
	Thread.sleep(4000);
	doubleClick(sel_projectcodeserchsel);
	
	click(btn_Overview);
	click(btn_Resourcebutton);
	click(btn_ResourcestabA);
	
	
	Thread.sleep(4000);
	WebElement el1=driver.findElement(By.xpath("//table[@id='tblProjectResources']//td[8]/div/input[1]"));
	String value1 = el1.getAttribute("value");
	Thread.sleep(4000);
	WebElement el2=driver.findElement(By.xpath("//table[@id='tblProjectResources']//td[9]/div/input[1]"));
	String value2 = el2.getAttribute("value");
	
	System.out.println("estimate"+value1);
	System.out.println("actual"+value2);
	
	
	if(number10.equals(value1)) {
		System.out.println("estimated resource hours are correct updated");
		writeTestResults("Verify whether estimated resource hours are correct","estimated resource hours are correct", "estimated resource hours are correct updated", "pass");
	}else {
		System.out.println("estimated labour hours are incorrect updated");
		writeTestResults("Verify whether estimated resource hours are correct","estimated resource hours are correct", "estimated resource hours are incorrect updated", "fail");
	}
	
	if(TimeActual.equals(value2)) {
		System.out.println("actual labour hours are correct updated");
		writeTestResults("Verify whether actual labour hours are correct","actual labour hours are correct", "actual labour hours are correct updated", "pass");
	}else {
		System.out.println("actual labour hours are incorrect updated");
		writeTestResults("Verify whether actual labour hours are correct","actual labour hours are correct", "actual labour hours are incorrect updated", "fail");
	}
}


public void  VerifyThatTheSubContractTypeTaskCanBeConverted() throws Exception {
	
	VerifyTheAbilityOfAddingSubContract();
	Thread.sleep(4000);
	click(btn_Action);
	click(btn_SubContractOrderA);
	selectIndex(txt_subTaskA, 2);
	Thread.sleep(4000);
	click(btn_subVendorbuttonA);
	Thread.sleep(4000);
	sendKeys(txt_subVendortxtA, subVendortxtA);
	Thread.sleep(4000);
	pressEnter(txt_subVendortxtA);
	Thread.sleep(4000);
	doubleClick(sel_subVendorselA);
	Thread.sleep(4000);
	click(btn_subcheckboxA);
	Thread.sleep(4000);
	click(btn_Apply);
	
	switchWindow();
	Thread.sleep(4000);
	if (isDisplayed(PurchaseOrderpageA)) {

		writeTestResults("verify Purchase Order page","view Purchase Order page", "Purchase Order page is display", "pass");
	} else {
		writeTestResults("verify Purchase Order page","view Purchase Order page", "Purchase Order page is not dislayed", "fail");

	}
	LocalTime myObj = LocalTime.now();
	sendKeys(txt_VendorReferenceNoA, VendorReferenceNotxtA+myObj);
	click(btn_CheckoutA);
	Thread.sleep(4000);
	click(btn_draft);
	Thread.sleep(4000);
	click(btn_release);	
	Thread.sleep(4000);
	trackCode= getText(Header);
	PONo=trackCode;
	actualcost=getText("//h1[@id='bannerTotal']");
	//System.out.println("actualcost:"+actualcost);
	
	if (isDisplayed(btn_Gotopage)){

		writeTestResults("verify Purchase Order Document Released","Purchase Order Document Released", "Purchase Order Document is Released", "pass");
	} else {
		writeTestResults("verify Purchase Order Document Released","Purchase Order Document Released", "Purchase Order Document is not Released", "fail");

	}
	Thread.sleep(4000);
	click(btn_Gotopage);
	
	
	switchWindow();
	Thread.sleep(4000);
	if (isDisplayed(PurchaseInvoicepageA)) {

		writeTestResults("verify Purchase Invoice page","view Purchase Invoice page", "Purchase Invoice page is display", "pass");
	} else {
		writeTestResults("verify Purchase Invoice page","view Purchase Invoice page", "Purchase Invoice page is not dislayed", "fail");

	}
	click(btn_CheckoutA);
	Thread.sleep(4000);
	click(btn_draft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Invoice Document Draft","Purchase Invoice Document Draft", "Purchase Invoice Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Invoice Document Draft","Purchase Invoice Document Draft", "Purchase Invoice Document is not Draft", "fail");

	}
	
	Thread.sleep(4000);
	click(btn_release);
	Thread.sleep(4000);
	trackCode= getText(Header);
	Thread.sleep(4000);
	INo=trackCode;
	
	Thread.sleep(5000);
	if (isDisplayed(pageRelease)){

		writeTestResults("verify Purchase Invoice Document Released","Purchase Invoice Document Released", "Purchase Invoice Document is Released", "pass");
	} else {
		writeTestResults("verify Purchase Invoice Document Released","Purchase Invoice Document Released", "Purchase Invoice Document is not Released", "fail");

	}
}

public void  VerifyTheAccuracyOfReflectingSubContractExpenses_InCostingSummary() throws Exception {
	
	VerifyThatTheSubContractTypeTaskCanBeConverted();
	
	Thread.sleep(4000);
	clickNavigation();
	Thread.sleep(4000);
	click(btn_projbutton);
	Thread.sleep(4000);
	click(btn_projectbutton);
	Thread.sleep(4000);
	sendKeys(txt_projectcodeserch, number1);
	Thread.sleep(4000);
	pressEnter(txt_projectcodeserch);
	Thread.sleep(4000);
	doubleClick(sel_projectcodeserchsel);
	

	click(btn_CostingSummary);
	click(btn_SubContractA);
	Thread.sleep(4000);
	number2=getText(txt_SubContractvalueActualA);
	number4 = number2.replaceAll(",", "");
	
	if(number4.contentEquals(actualcost)) {
		System.out.println("correct actual cost is updated in 'actual' column");
		writeTestResults("verify correct actual cost is updated in 'actual' column","correct actual cost is updated", "correct actual cost is updated in 'actual' column", "pass");
	}else {
		System.out.println("incorrect actual cost is updated in 'actual' column");
		writeTestResults("verify correct actual cost is updated in 'actual' column","correct actual cost is updated", "correct actual cost is not updated in 'actual' column", "fail");
	}
}


public void  VerifyTheAccuracyOfReflectingSubContractExpenses_InOverview() throws Exception {
	
	VerifyThatTheSubContractTypeTaskCanBeConverted();
	
	Thread.sleep(5000);
	clickNavigation();
	Thread.sleep(4000);
	click(btn_projbutton);
	Thread.sleep(4000);
	click(btn_projectbutton);
	Thread.sleep(4000);
	sendKeys(txt_projectcodeserch, number1);
	Thread.sleep(4000);
	pressEnter(txt_projectcodeserch);
	Thread.sleep(4000);
	doubleClick(sel_projectcodeserchsel);
	
	click(btn_Overview);
	click(btn_SubContractServicestabA);
	
	Thread.sleep(4000);
	OrderDateA=getText(txt_OrderDateA);
	Thread.sleep(4000);
	OrderNoA=getText(txt_OrderNoA);
	Thread.sleep(4000);
	InvoiceDateA=getText(txt_InvoiceDateA);
	Thread.sleep(4000);
	InvoiceNoA=getText(txt_InvoiceNoA);
	Thread.sleep(4000);
	ProductA=getText(txt_ProductA);
	Thread.sleep(4000);
	EstimatedAmountA=getText(txt_EstimatedAmountA);
	Thread.sleep(4000);
	ActualAmountA=getText(txt_ActualAmountA);
	
	   DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy");  
	   LocalDateTime now = LocalDateTime.now(); 
	   date=dtf.format(now);
	   System.out.println(date); 
	
	//System.out.println("OrderDateA"+OrderDateA);
	Thread.sleep(4000);
	if(date.equals(OrderDateA)) {
		System.out.println("correct Order Date is updated in Table");
		writeTestResults("verify correct Order Date is updated in Table","correct Order Date is updated", "correct Order Date is updated in Table", "pass");
	}else {
		System.out.println("incorrect Order Date is updated in Table");
		writeTestResults("verify correct Order Date is updated in Table","correct Order Date is updated", "incorrect Order Date is updated in Table", "fail");
	}
	
	//System.out.println("OrderNoA"+OrderNoA);
	Thread.sleep(4000);
	if(PONo.equals(OrderNoA)) {
		System.out.println("correct Order No is updated in Table");
		writeTestResults("verify correct Order No Date is updated in Table","correct Order No is updated", "correct Order No is updated in Table", "pass");
	}else {
		System.out.println("incorrect Order No is updated in Table");
		writeTestResults("verify correct Order No is updated in Table","correct Order No is updated", "incorrect Order No is updated in Table", "fail");
	}
	
	//System.out.println("InvoiceDateA"+InvoiceDateA);
	Thread.sleep(4000);
	if(date.equals(InvoiceDateA)) {
		System.out.println("correct Invoice Date is updated in Table");
		writeTestResults("verify correct Invoice Date is updated in Table","correct Invoice Date is updated", "correct Invoice Date is updated in Table", "pass");
	}else {
		System.out.println("incorrect Invoice Date is updated in Table");
		writeTestResults("verify correct Invoice Date is updated in Table","correct Invoice Date is updated", "incorrect Invoice Date is updated in Table", "fail");
	}
	
	//System.out.println("InvoiceNoA"+InvoiceNoA);
//	Thread.sleep(4000);
//	if(INo.equals(InvoiceNoA)) {
//		System.out.println("correct Invoice No is updated in Table");
//		writeTestResults("verify correct Invoice No is updated in Table","correct Invoice No is updated", "correct Invoice No is updated in Table", "pass");
//	}else {
//		System.out.println("incorrect Invoice No is updated in Table");
//		writeTestResults("verify correct Invoice No is updated in Table","correct Invoice No is updated", "incorrect Invoice No is updated in Table", "fail");
//	}
	//System.out.println("ProductA"+ProductA);
	Thread.sleep(4000);
	if(ServiceProducttxt.equals(ProductA.replace(" [service ]", ""))) {
		System.out.println("correct Product is updated in Table");
		writeTestResults("verify correct Product is updated in Table","correct Product is updated", "correct Product is updated in Table", "pass");
	}else {
		System.out.println("incorrect Product is updated in Table");
		writeTestResults("verify correct Product is updated in Table","correct Product is updated", "incorrect Product is updated in Table", "fail");
	}
	
	//System.out.println("EstimatedAmountA"+EstimatedAmountA);
	
	Thread.sleep(4000);
	if((EstimatedCost+".00").equals(EstimatedAmountA.replace(",", ""))) {
		System.out.println("correct Estimated Amount is updated in Table");
		writeTestResults("verify correct Estimated Amount is updated in Table","correct Estimated Amount is updated", "correct Estimated Amount is updated in Table", "pass");
	}else {
		System.out.println("incorrect Estimated Amount is updated in Table");
		writeTestResults("verify correct Estimated Amount is updated in Table","correct Estimated Amount is updated", "incorrect Estimated Amount is updated in Table", "fail");
	}
	
	//System.out.println("ActualAmountA"+ActualAmountA);
	
	Thread.sleep(4000);
	if((EstimatedCost+".00").equals(ActualAmountA.replace(",", ""))) {
		System.out.println("correct Actual Amount is updated in Table");
		writeTestResults("verify correct Actual Amount is updated in Table","correct Actual Amount is updated", "correct Actual Amount is updated in Table", "pass");
	}else {
		System.out.println("incorrect Actual Amount is updated in Table");
		writeTestResults("verify correct Actual Amount is updated in Table","correct Actual Amount is updated", "incorrect Actual Amount is updated in Table", "fail");
	}
	
}

public void  VerifyTheAbilityOfAddingInterDepartmentTask() throws Exception {
	
	navigateToTheLoginPage();
	verifyTheLogo();
	userLogin();
	clickNavigation();
	clickProjectbutton();
	createProject();
	
	Thread.sleep(4000);
	click(btn_edit);
	click(btn_WorkBreakdown);
	click(btn_AddTask);
	LocalTime myObj = LocalTime.now();
	sendKeys(txt_TaskName,TaskName+myObj);
	selectIndex(txt_TaskType, 2);
	selectIndex(txt_ContractTypeA, 2);
	click(btn_InterDepartmentA);
	click(btn_OwnerbuttonA);
	sendKeys(txt_OwnertxtA, OwnertxtA);
	Thread.sleep(4000);
	pressEnter(txt_OwnertxtA);
	Thread.sleep(4000);
	doubleClick(sel_OwnerselA);
	sendKeys(txt_EstimatedCostA, EstimatedCost);
	Thread.sleep(4000);
	click(txt_EstimatedDateA);
	Thread.sleep(4000);
	click(sel_EstimatedDateselA);
	
	click(btn_TaskUpdate);
	pageRefersh();
	Thread.sleep(3000);
	click(btn_WorkBreakdown);
	Thread.sleep(6000);
	click(btn_releaseTaskdropdown);
	Thread.sleep(4000);
	click(btn_releaseTask);
	pageRefersh();
	Thread.sleep(6000);
	click(btn_update);
}

public void  VerifyThatIfAnInterDepartmentTypeOfTask() throws Exception {
	
	VerifyTheAbilityOfAddingInterDepartmentTask();
	Thread.sleep(4000);
	clickNavigation();
	click(btn_serbutton);
	click(btn_ServiceCalendarA);
	
	
	if(isDisplayed(txt_interdepartmentjobA.replace("number", number1))) {
		System.out.println("inter department job is showing on the estimated date");
		writeTestResults("Verify that inter department job is showing on the estimated date","inter department job is showing", "inter department job is showing on the estimated date", "pass");
	}else {
		System.out.println("inter department job is not showing on the estimated date");
		writeTestResults("Verify that inter department job is showing on the estimated date","inter department job is showing", "inter department job is not showing on the estimated date", "fail");
	}
}

public void  VerifyTheAbilityOfCreatingServiceJobOrder() throws Exception {
	VerifyThatIfAnInterDepartmentTypeOfTask();
	Thread.sleep(4000);
	click(txt_interdepartmentjobA.replace("number", number1));
	
	Thread.sleep(4000);
	switchWindow();
	Thread.sleep(4000);
	if(isDisplayed(txt_ServiceJobOrderpageA)) {
		System.out.println("user is navigated to a new service job order form");
		writeTestResults("Verify whether user is navigated to a new service job order form","navigated to a new service job order form", "user is navigated to a new service job order form", "pass");
	}else {
		System.out.println("user is not navigated to a new service job order form");
		writeTestResults("Verify whether user is navigated to a new service job order form","navigated to a new service job order form", "user is not navigated to a new service job order form", "fail");
	}
}

public void  VerifyTheAbilityOfDraftReleaseOfServiceJobOrder() throws Exception {
	
	
	VerifyTheAbilityOfCreatingServiceJobOrder();
	
	selectText(txt_ServiceGroupA, "SEP-SCHINDLER ESCALATOR PAID SERVICE");
	
//	click(btn_ServicePerformerbuttonA);
//	Thread.sleep(4000);
//	sendKeys(txt_ServicePerformertxtA, ServicePerformertxtA);
//	Thread.sleep(4000);
//	pressEnter(txt_ServicePerformertxtA);
//	Thread.sleep(4000);
//	doubleClick(sel_ServicePerformerselA);
	
	selectIndex(txt_PriorityA, 3);
	
	click(btn_PricingProfilebuttonA);
	Thread.sleep(4000);
	sendKeys(txt_PricingProfiletxtA,PricingProfiletxtA);
	Thread.sleep(4000);
	pressEnter(txt_PricingProfiletxtA);
	Thread.sleep(4000);
	doubleClick(sel_PricingProfileselA);
	
	click(btn_ServiceLocationbuttonA);
	Thread.sleep(4000);
	sendKeys(txt_ServiceLocationtxtA, ProjectLocationtxt);
	Thread.sleep(4000);
	pressEnter(txt_ServiceLocationtxtA);
	Thread.sleep(4000);
	doubleClick(sel_ServiceLocationselA);
	
	sendKeys(txt_OrderDescriptionA, OrderDescriptionA);

	
	Thread.sleep(4000);
	click(btn_draft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Service Job Order form Document Released","Service Job Order form Document Released", "Service Job Order form Document is Released", "pass");
	} else {
		writeTestResults("verify Service Job Order form Document Released","Service Job Order form Document Released", "Service Job Order form Document is not Released", "fail");

	}
	
	Thread.sleep(4000);
	click(btn_release);
	Thread.sleep(4000);
	trackCode= getText(Header);
	Thread.sleep(4000);
	serviceJobOrderCodeA= getText(Header);
	Thread.sleep(5000);
	if (isDisplayed(pageRelease)){

		writeTestResults("verify Service Job Order form Document Released","Service Job Order form Document Released", "Service Job Order form Document is Released", "pass");
	} else {
		writeTestResults("verify Service Job Order form Document Released","Service Job Order form Document Released", "Service Job Order form Document is not Released", "fail");

	}
}

public void  VerifyThatEstimatedCostForInterDepartment() throws Exception {
	
	VerifyTheAbilityOfDraftReleaseOfServiceJobOrder();	
	
	Thread.sleep(8000);
	clickNavigation();
	Thread.sleep(4000);
	click(btn_projbutton);
	Thread.sleep(4000);
	click(btn_projectbutton);
	Thread.sleep(4000);
	sendKeys(txt_projectcodeserch, number1);
	Thread.sleep(4000);
	pressEnter(txt_projectcodeserch);
	Thread.sleep(4000);
	doubleClick(sel_projectcodeserchsel);
	
	click(btn_CostingSummary);
	click(btn_InterDepartmenttabA);
	Thread.sleep(4000);
	number10 = getText(txt_InterDepartmentestimatedcostA).replaceAll(",", "");
	
//	click(btn_WorkBreakdown);
//	Thread.sleep(4000);
//	click(btn_releaseTaskdropdown);
//	Thread.sleep(4000);
//	click(btn_editTask);
//	click(btn_InterDepartmentA);
	
	if(number10.equals(EstimatedCost+".00000")) {
		System.out.println("correct estimated cost is updated in 'allocated' column");
		writeTestResults("verify correct estimated cost is updated in 'allocated' column","correct estimated cost is updated", "correct estimated cost is updated in 'allocated' column", "pass");
	}else {
		System.out.println("incorrect estimated cost is updated in 'allocated' column");
		writeTestResults("verify correct estimated cost is updated in 'allocated' column","correct estimated cost is updated", "correct estimated cost is not updated in 'allocated' column", "fail");
	}
}

public void  VerifyTheAbilityToDoCostAllocationForServiceJobOrder() throws Exception {
	
	VerifyThatEstimatedCostForInterDepartment();
	
//	Thread.sleep(4000);
//	click(btn_TaskcloseA);
	
	Thread.sleep(4000);
	clickNavigation();
	Thread.sleep(4000);
	click(btn_FinanceA);
	Thread.sleep(4000);
	click(btn_OutboundPaymentAdviceA);
	
	if(isDisplayed(txt_OutboundPaymentAdvicepageA)) {
		writeTestResults("verify Outbound Payment Advice page","view Outbound Payment Advice page", "Outbound Payment Advice page is display", "pass");
	}else {
		writeTestResults("verify Outbound Payment Advice page","view Outbound Payment Advice page", "Outbound Payment Advice page is not display", "fail");
	}
	
	click(btn_NewOutboundPaymentAdviceA);
	Thread.sleep(4000);
	click(btn_AccrualVoucherA);
	
	Thread.sleep(4000);
	click(btn_AccrualVoucherVendorA);
	Thread.sleep(4000);
	sendKeys(txt_AccrualVoucherVendortxtA, subVendortxtA);
	Thread.sleep(4000);
	pressEnter(txt_AccrualVoucherVendortxtA);
	Thread.sleep(4000);
	doubleClick(sel_AccrualVoucherVendorselA);
	
	LocalTime myObj = LocalTime.now();
	sendKeys(txt_AccrualVoucherVendorReferenceNoA, AccrualVoucherVendorReferenceNoA+myObj);
	selectIndex(txt_AnalysisCodeA, 4);
	sendKeys(txt_AmountA, AmountA);
	
	click(btn_costallocationA);
	sendKeys(txt_PercentageA, PercentageA);
	selectIndex(txt_CostAssignmentTypeA, 3);
	
	Thread.sleep(4000);
	click(btn_CostObjectbuttonA);
	Thread.sleep(4000);
	sendKeys(txt_CostObjecttxtA, serviceJobOrderCodeA);
	Thread.sleep(4000);
	pressEnter(txt_CostObjecttxtA);
	Thread.sleep(4000);
	doubleClick(sel_CostObjectselA);
	
	Thread.sleep(4000);
	click(btn_AddRecordA);
	Thread.sleep(4000);
	click(btn_UpdateA);
	
	click(btn_CheckoutA);
	Thread.sleep(4000);
	click(btn_draft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraftNew)){

		writeTestResults("verify  Accrual Voucher Document Draft"," Accrual Voucher Document Draft", " Accrual Voucher Document is Draft", "pass");
	} else {
		writeTestResults("verify  Accrual Voucher Document Draft"," Accrual Voucher Document Draft", " Accrual Voucher Document is not Draft", "fail");

	}
	
	Thread.sleep(4000);
	click(btn_release);
	Thread.sleep(4000);
	trackCode= getText(Header);
	Thread.sleep(5000);
	if (isDisplayed(pageReleaseNew)){

		writeTestResults("verify  Accrual Voucher Document Released"," Accrual Voucher Document Released", " Accrual Voucher Document is Released", "pass");
	} else {
		writeTestResults("verify  Accrual Voucher Document Released"," Accrual Voucher Document Released", " Accrual Voucher Document is not Released", "fail");

	}
	
}


public void  VerifyThatExpensesRecordedFromTheServiceJobOrder() throws Exception {
	
	VerifyTheAbilityToDoCostAllocationForServiceJobOrder();
	
	Thread.sleep(8000);
	clickNavigation();
	Thread.sleep(4000);
	click(btn_projbutton);
	Thread.sleep(4000);
	click(btn_projectbutton);
	Thread.sleep(4000);
	sendKeys(txt_projectcodeserch, number1);
	Thread.sleep(4000);
	pressEnter(txt_projectcodeserch);
	Thread.sleep(4000);
	doubleClick(sel_projectcodeserchsel);
	
	click(btn_CostingSummary);
	click(btn_InterDepartmenttabA);
	Thread.sleep(4000);
	InterDepartmentActualvalueA=getText(txt_InterDepartmentActualvalueA);
	
	if(InterDepartmentActualvalueA.equals(AmountA+".00000")) {
		System.out.println("correct estimated cost is updated in 'Actual' column");
		writeTestResults("verify correct estimated cost is updated in 'Actual' column","correct actual cost is updated", "correct actual cost is updated in 'actual' column", "pass");
	}else {
		System.out.println("incorrect estimated cost is updated in 'Actual' column");
		writeTestResults("verify correct estimated cost is updated in 'Actual' column","correct actual cost is updated", "correct actual cost is not updated in 'actual' column", "fail");
	}
	
}


public void  VerifyWhetherPOCOfTheInterDepartmentTask() throws Exception {
	
	VerifyThatExpensesRecordedFromTheServiceJobOrder();
	
	Thread.sleep(4000);
	clickNavigation();
	Thread.sleep(4000);
	click(btn_serbutton);
	
	click(btn_ServiceJobOrderA);
	sendKeys(txt_ServiceJobOrderserchA, serviceJobOrderCodeA);
	Thread.sleep(4000);
	pressEnter(txt_ServiceJobOrderserchA);
	Thread.sleep(4000);
	doubleClick(sel_ServiceJobOrderselA);
	
	Thread.sleep(4000);
	click(btn_Action);
	Thread.sleep(4000);
	click(btn_AddProductTagNoA);
	click(btn_SpnSeriaA);
	Thread.sleep(4000);
	click(btn_SerialNosA);
	click(btn_AddProductTagNoapplyA);
	
	Thread.sleep(4000);
	click(btn_Action);
	click(btn_StopA);
	click(btn_CompleteA);
	
	Thread.sleep(8000);
	clickNavigation();
	Thread.sleep(4000);
	click(btn_projbutton);
	Thread.sleep(4000);
	click(btn_projectbutton);
	Thread.sleep(4000);
	sendKeys(txt_projectcodeserch, number1);
	Thread.sleep(4000);
	pressEnter(txt_projectcodeserch);
	Thread.sleep(4000);
	doubleClick(sel_projectcodeserchsel);
	
	click(btn_WorkBreakdown);
	
	Thread.sleep(4000);
	if(getText(txt_POCA).equals("100")) {
		System.out.println("correct value updated POC of the inter department task 100%");
		writeTestResults("Verify whether POC of the inter department task gets 100%","POC of the inter department task gets 100%", "correct value updated POC of the inter department task 100%", "pass");
	}else {
		System.out.println("incorrect value updated POC of the inter department task 100%");
		writeTestResults("Verify whether POC of the inter department task gets 100%","POC of the inter department task gets 100%", "incorrect value updated POC of the inter department task 100%", "fail");
	}
	
}

public void  ProjectThreeToSix() throws Exception {
	
	navigateToTheLoginPage();
	verifyTheLogo();
	userLogin();
	clickNavigation();
	clickProjectbutton();
	createProject();
	addingAprojectTask();
	
	VerifyThatEstimatedCost();
	
	WaitClick(btn_CloseProductRelatedPriceA);
	click(btn_CloseProductRelatedPriceA);
	WaitClick(headercloseA);
	click(headercloseA);
	
	VerifyThatEstimatedCostForLabour();
	
	
	clickNavigation();
	WaitClick(btn_projbutton);
	click(btn_projbutton);
	WaitClick(btn_projectbutton);
	click(btn_projectbutton);
	WaitElement(txt_projectcodeserch);
	sendKeys(txt_projectcodeserch, number1);
	Thread.sleep(4000);
	pressEnter(txt_projectcodeserch);
	Thread.sleep(4000);
	doubleClick(sel_projectcodeserchsel);
	
	VerifyThatEstimatedCostForResource();
	
	clickNavigation();
	WaitClick(btn_projbutton);
	click(btn_projbutton);
	WaitClick(btn_projectbutton);
	click(btn_projectbutton);
	WaitElement(txt_projectcodeserch);
	sendKeys(txt_projectcodeserch, number1);
	Thread.sleep(4000);
	pressEnter(txt_projectcodeserch);
	Thread.sleep(4000);
	doubleClick(sel_projectcodeserchsel);
	
	VerifyThatEstimatedCostForExpense();
	
	
}

public void  ProjectNinteentoTwowentyOne() throws Exception {
	
	VerifyTheAccuracyOfReflectingSubContractExpenses_InCostingSummary();
	
	click(btn_Overview);
	click(btn_SubContractServicestabA);
	
	Thread.sleep(4000);
	OrderDateA=getText(txt_OrderDateA);
	Thread.sleep(4000);
	OrderNoA=getText(txt_OrderNoA);
	Thread.sleep(4000);
	InvoiceDateA=getText(txt_InvoiceDateA);
	Thread.sleep(4000);
	InvoiceNoA=getText(txt_InvoiceNoA);
	Thread.sleep(4000);
	ProductA=getText(txt_ProductA);
	Thread.sleep(4000);
	EstimatedAmountA=getText(txt_EstimatedAmountA);
	Thread.sleep(4000);
	ActualAmountA=getText(txt_ActualAmountA);
	
	   DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");  
	   LocalDateTime now = LocalDateTime.now(); 
	   date=dtf.format(now);
	   System.out.println(date); 
	
	//System.out.println("OrderDateA"+OrderDateA);
	Thread.sleep(4000);
	if(date.equals(OrderDateA)) {
		System.out.println("correct Order Date is updated in Table");
		writeTestResults("verify correct Order Date is updated in Table","correct Order Date is updated", "correct Order Date is updated in Table", "pass");
	}else {
		System.out.println("incorrect Order Date is updated in Table");
		writeTestResults("verify correct Order Date is updated in Table","correct Order Date is updated", "incorrect Order Date is updated in Table", "fail");
	}
	
	//System.out.println("OrderNoA"+OrderNoA);
	Thread.sleep(4000);
	if(PONo.equals(OrderNoA)) {
		System.out.println("correct Order No is updated in Table");
		writeTestResults("verify correct Order No Date is updated in Table","correct Order No is updated", "correct Order No is updated in Table", "pass");
	}else {
		System.out.println("incorrect Order No is updated in Table");
		writeTestResults("verify correct Order No is updated in Table","correct Order No is updated", "incorrect Order No is updated in Table", "fail");
	}
	
	//System.out.println("InvoiceDateA"+InvoiceDateA);
	Thread.sleep(4000);
	if(date.equals(InvoiceDateA)) {
		System.out.println("correct Invoice Date is updated in Table");
		writeTestResults("verify correct Invoice Date is updated in Table","correct Invoice Date is updated", "correct Invoice Date is updated in Table", "pass");
	}else {
		System.out.println("incorrect Invoice Date is updated in Table");
		writeTestResults("verify correct Invoice Date is updated in Table","correct Invoice Date is updated", "incorrect Invoice Date is updated in Table", "fail");
	}
	
	//System.out.println("InvoiceNoA"+InvoiceNoA);
//	Thread.sleep(4000);
//	if(INo.equals(InvoiceNoA)) {
//		System.out.println("correct Invoice No is updated in Table");
//		writeTestResults("verify correct Invoice No is updated in Table","correct Invoice No is updated", "correct Invoice No is updated in Table", "pass");
//	}else {
//		System.out.println("incorrect Invoice No is updated in Table");
//		writeTestResults("verify correct Invoice No is updated in Table","correct Invoice No is updated", "incorrect Invoice No is updated in Table", "fail");
//	}
	//System.out.println("ProductA"+ProductA);
	Thread.sleep(4000);
	if(ServiceProducttxt.equals(ProductA.replace(" [service ]", ""))) {
		System.out.println("correct Product is updated in Table");
		writeTestResults("verify correct Product is updated in Table","correct Product is updated", "correct Product is updated in Table", "pass");
	}else {
		System.out.println("incorrect Product is updated in Table");
		writeTestResults("verify correct Product is updated in Table","correct Product is updated", "incorrect Product is updated in Table", "fail");
	}
	
	//System.out.println("EstimatedAmountA"+EstimatedAmountA);
	
	Thread.sleep(4000);
	if((EstimatedCost+".00").equals(EstimatedAmountA.replace(",", ""))) {
		System.out.println("correct Estimated Amount is updated in Table");
		writeTestResults("verify correct Estimated Amount is updated in Table","correct Estimated Amount is updated", "correct Estimated Amount is updated in Table", "pass");
	}else {
		System.out.println("incorrect Estimated Amount is updated in Table");
		writeTestResults("verify correct Estimated Amount is updated in Table","correct Estimated Amount is updated", "incorrect Estimated Amount is updated in Table", "fail");
	}
	
	//System.out.println("ActualAmountA"+ActualAmountA);
	
	Thread.sleep(4000);
	if((EstimatedCost+".00").equals(ActualAmountA.replace(",", ""))) {
		System.out.println("correct Actual Amount is updated in Table");
		writeTestResults("verify correct Actual Amount is updated in Table","correct Actual Amount is updated", "correct Actual Amount is updated in Table", "pass");
	}else {
		System.out.println("incorrect Actual Amount is updated in Table");
		writeTestResults("verify correct Actual Amount is updated in Table","correct Actual Amount is updated", "incorrect Actual Amount is updated in Table", "fail");
	}
	
}

public void ProjectProduct7() throws Exception {
	
	WaitClick(btn_edit);
	click(btn_edit);
	WaitClick(btn_WorkBreakdown);
	WaitClick(btn_WorkBreakdown);
	click(btn_WorkBreakdown);
	WaitClick(btn_AddTask);
	click(btn_AddTask);
	WaitElement(txt_TaskName);
	LocalTime myObj = LocalTime.now();
	sendKeys(txt_TaskName,TaskName+myObj);
	WaitElement(txt_TaskType);
	selectIndex(txt_TaskType, 2);
	
	WaitClick(btn_InputProducts);
	click(btn_InputProducts);
	
	Thread.sleep(4000);
	click(pro1A);
	Thread.sleep(4000);
	sendKeys(txt_producttxt, pro7.readTestCreation("BatchSpecific"));
	Thread.sleep(4000);
	pressEnter(txt_producttxt);
	Thread.sleep(4000);
	doubleClick(sel_productsel);
	Thread.sleep(4000);
	sendKeys(txt_EstimatedQty, "1");
	JavascriptExecutor js1 = (JavascriptExecutor)driver;
	js1.executeScript("$(\"#tblSADProduct\").children().find(\".unchecked\").click()");
	WaitClick(plus1A);
	click(plus1A);
	
	click(pro2A);
	Thread.sleep(4000);
	sendKeys(txt_producttxt, pro7.readTestCreation("BatchFifo"));
	Thread.sleep(4000);
	pressEnter(txt_producttxt);
	Thread.sleep(4000);
	doubleClick(sel_productsel);
	sendKeys(EstimatedQty2A, "1");
	JavascriptExecutor js2 = (JavascriptExecutor)driver;
	js2.executeScript("$(\"#tblSADProduct\").children().find(\".unchecked\").click()");
	WaitClick(plus2A);
	click(plus2A);
	
	click(pro3A);
	Thread.sleep(4000);
	sendKeys(txt_producttxt, pro7.readTestCreation("Lot"));
	Thread.sleep(4000);
	pressEnter(txt_producttxt);
	Thread.sleep(4000);
	doubleClick(sel_productsel);
	sendKeys(EstimatedQty3A, "1");
	JavascriptExecutor js3 = (JavascriptExecutor)driver;
	js3.executeScript("$(\"#tblSADProduct\").children().find(\".unchecked\").click()");
	WaitClick(plus3A);
	click(plus3A);
	
	click(pro4A);
	Thread.sleep(4000);
	sendKeys(txt_producttxt, pro7.readTestCreation("SerielSpecific"));
	Thread.sleep(4000);
	pressEnter(txt_producttxt);
	Thread.sleep(4000);
	doubleClick(sel_productsel);
	sendKeys(EstimatedQty4A, "1");
	JavascriptExecutor js4 = (JavascriptExecutor)driver;
	js4.executeScript("$(\"#tblSADProduct\").children().find(\".unchecked\").click()");
	WaitClick(plus4A);
	click(plus4A);
	
	click(pro5A);
	Thread.sleep(4000);
	sendKeys(txt_producttxt, pro7.readTestCreation("SerielBatchSpecific"));
	Thread.sleep(4000);
	pressEnter(txt_producttxt);
	Thread.sleep(4000);
	doubleClick(sel_productsel);
	sendKeys(EstimatedQty5A, "1");
	JavascriptExecutor js5 = (JavascriptExecutor)driver;
	js5.executeScript("$(\"#tblSADProduct\").children().find(\".unchecked\").click()");
	WaitClick(plus5A);
	click(plus5A);
	
	click(pro6A);
	Thread.sleep(4000);
	sendKeys(txt_producttxt, pro7.readTestCreation("SerielBatchFifo"));
	Thread.sleep(4000);
	pressEnter(txt_producttxt);
	Thread.sleep(4000);
	doubleClick(sel_productsel);
	sendKeys(EstimatedQty6A, "1");
	JavascriptExecutor js6 = (JavascriptExecutor)driver;
	js6.executeScript("$(\"#tblSADProduct\").children().find(\".unchecked\").click()");
	WaitClick(plus6A);
	click(plus6A);

	click(pro7A);
	Thread.sleep(4000);
	sendKeys(txt_producttxt, pro7.readTestCreation("SerirlFifo"));
	Thread.sleep(4000);
	pressEnter(txt_producttxt);
	Thread.sleep(4000);
	doubleClick(sel_productsel);
	sendKeys(EstimatedQty7A, "1");
	JavascriptExecutor js7 = (JavascriptExecutor)driver;
	js7.executeScript("$(\"#tblSADProduct\").children().find(\".unchecked\").click()");
	
	WaitClick(btn_Labour);
	click(btn_Labour);
	WaitClick(btn_ServiceProductbutton);
	click(btn_ServiceProductbutton);
	Thread.sleep(4000);
	sendKeys(txt_ServiceProducttxt, ServiceProducttxt);
	Thread.sleep(4000);
	pressEnter(txt_ServiceProducttxt);
	Thread.sleep(4000);
	doubleClick(sel_ServiceProductsel);
	click(txt_EstimatedWork);
	Thread.sleep(4000);
	number4=getText(sel_EstimatedWorksel);
	Thread.sleep(4000);
	number6=Integer.parseInt(number4);
	WaitClick(sel_EstimatedWorksel);
	click(sel_EstimatedWorksel);
	WaitClick(btn_EstimatedWorkhourok);
	click(btn_EstimatedWorkhourok);
	js1.executeScript("$(\"#tblSADService\").children().find(\".unchecked\").click()");
	
	WaitClick(btn_Resource);
	click(btn_Resource);
	WaitClick(btn_Resourcebutton);
	click(btn_Resourcebutton);
	Thread.sleep(4000);
	sendKeys(txt_Resourcetxt, Resourcetxt);
	Thread.sleep(4000);
	pressEnter(txt_Resourcetxt);
	Thread.sleep(4000);
	doubleClick(sel_Resourcesel);
	Thread.sleep(4000);
	click(btn_ResourceServiceProductbutton);
	Thread.sleep(4000);
	sendKeys(txt_ServiceProducttxt, ServiceProducttxt);
	Thread.sleep(4000);
	pressEnter(txt_ServiceProducttxt);
	Thread.sleep(4000);
	doubleClick(sel_ServiceProductsel);
	WaitClick(txt_ResourceEstimatedWork);
	click(txt_ResourceEstimatedWork);
	Thread.sleep(4000);
	number10=getText(sel_ResourceEstimatedWorksel);
	Thread.sleep(4000);
	number11=Integer.parseInt(number10);
	Thread.sleep(4000);
	click(sel_ResourceEstimatedWorksel);
	js1.executeScript("$(\"#tblSADResource\").children().find(\".unchecked\").click()");
	
	WaitClick(btn_Expense);
	click(btn_Expense);
	WaitElement(txt_AnalysisCode);
	selectIndex(txt_AnalysisCode, 2);
	Thread.sleep(4000);
	click(btn_ExpenseServiceProductbutton);
	Thread.sleep(4000);
	sendKeys(txt_ServiceProducttxt, ServiceProducttxt);
	Thread.sleep(4000);
	pressEnter(txt_ServiceProducttxt);
	Thread.sleep(4000);
	doubleClick(sel_ServiceProductsel);
	WaitElement(txt_EstimatedCost);
	sendKeys(txt_EstimatedCost, EstimatedCost);
	js1.executeScript("$(\"#tblSADExpence\").children().find(\".unchecked\").click()");
	
	WaitClick(btn_TaskUpdate);
	click(btn_TaskUpdate);
	pageRefersh();
	Thread.sleep(3000);
	click(btn_WorkBreakdown);
	WaitClick(btn_releaseTaskdropdown);
	click(btn_releaseTaskdropdown);
	WaitClick(btn_releaseTask);
	click(btn_releaseTask);
	
	pageRefersh();
	Thread.sleep(3000);
	WaitClick(btn_update);
	click(btn_update);
}

public void ProjectProduct7_2() throws Exception {
	
	WaitClick(btn_edit);
	click(btn_edit);
	WaitClick(btn_WorkBreakdown);
	click(btn_WorkBreakdown);
	WaitClick(btn_AddTask);
	click(btn_AddTask);
	LocalTime myObj = LocalTime.now();
	WaitElement(txt_TaskName);
	sendKeys(txt_TaskName,TaskName+myObj);
	WaitElement(txt_TaskType);
	selectIndex(txt_TaskType, 2);
	
	WaitClick(btn_InputProducts);
	click(btn_InputProducts);
	
	Thread.sleep(4000);
	click(pro1A);
	Thread.sleep(4000);
	sendKeys(txt_producttxt, pro7.readTestCreation("BatchSpecific"));
	Thread.sleep(4000);
	pressEnter(txt_producttxt);
	Thread.sleep(4000);
	doubleClick(sel_productsel);
	Thread.sleep(4000);
	sendKeys(txt_EstimatedQty, "2");
	JavascriptExecutor js1 = (JavascriptExecutor)driver;
	js1.executeScript("$(\"#tblSADProduct\").children().find(\".unchecked\").click()");
	WaitClick(plus1A);
	click(plus1A);
	
	Thread.sleep(4000);
	click(pro2A);
	Thread.sleep(4000);
	sendKeys(txt_producttxt, pro7.readTestCreation("BatchFifo"));
	Thread.sleep(4000);
	pressEnter(txt_producttxt);
	Thread.sleep(4000);
	doubleClick(sel_productsel);
	Thread.sleep(4000);
	sendKeys(EstimatedQty2A, "2");
	JavascriptExecutor js2 = (JavascriptExecutor)driver;
	js2.executeScript("$(\"#tblSADProduct\").children().find(\".unchecked\").click()");
	WaitClick(plus2A);
	click(plus2A);
	
	Thread.sleep(4000);
	click(pro3A);
	Thread.sleep(4000);
	sendKeys(txt_producttxt, pro7.readTestCreation("Lot"));
	Thread.sleep(4000);
	pressEnter(txt_producttxt);
	Thread.sleep(4000);
	doubleClick(sel_productsel);
	Thread.sleep(4000);
	sendKeys(EstimatedQty3A, "2");
	JavascriptExecutor js3 = (JavascriptExecutor)driver;
	js3.executeScript("$(\"#tblSADProduct\").children().find(\".unchecked\").click()");
	WaitClick(plus3A);
	click(plus3A);
	
	Thread.sleep(4000);
	click(pro4A);
	Thread.sleep(4000);
	sendKeys(txt_producttxt, pro7.readTestCreation("SerielSpecific"));
	Thread.sleep(4000);
	pressEnter(txt_producttxt);
	Thread.sleep(4000);
	doubleClick(sel_productsel);
	Thread.sleep(4000);
	sendKeys(EstimatedQty4A, "2");
	JavascriptExecutor js4 = (JavascriptExecutor)driver;
	js4.executeScript("$(\"#tblSADProduct\").children().find(\".unchecked\").click()");
	WaitClick(plus4A);
	click(plus4A);
	
	Thread.sleep(4000);
	click(pro5A);
	Thread.sleep(4000);
	sendKeys(txt_producttxt, pro7.readTestCreation("SerielBatchSpecific"));
	Thread.sleep(4000);
	pressEnter(txt_producttxt);
	Thread.sleep(4000);
	doubleClick(sel_productsel);
	Thread.sleep(4000);
	sendKeys(EstimatedQty5A, "2");
	JavascriptExecutor js5 = (JavascriptExecutor)driver;
	js5.executeScript("$(\"#tblSADProduct\").children().find(\".unchecked\").click()");
	WaitClick(plus5A);
	click(plus5A);
	
	Thread.sleep(4000);
	click(pro6A);
	Thread.sleep(4000);
	sendKeys(txt_producttxt, pro7.readTestCreation("SerielBatchFifo"));
	Thread.sleep(4000);
	pressEnter(txt_producttxt);
	Thread.sleep(4000);
	doubleClick(sel_productsel);
	Thread.sleep(4000);
	sendKeys(EstimatedQty6A, "2");
	JavascriptExecutor js6 = (JavascriptExecutor)driver;
	js6.executeScript("$(\"#tblSADProduct\").children().find(\".unchecked\").click()");
	WaitClick(plus6A);
	click(plus6A);

	Thread.sleep(4000);
	click(pro7A);
	Thread.sleep(4000);
	sendKeys(txt_producttxt, pro7.readTestCreation("SerirlFifo"));
	Thread.sleep(4000);
	pressEnter(txt_producttxt);
	Thread.sleep(4000);
	doubleClick(sel_productsel);
	Thread.sleep(4000);
	sendKeys(EstimatedQty7A, "2");
	JavascriptExecutor js7 = (JavascriptExecutor)driver;
	js7.executeScript("$(\"#tblSADProduct\").children().find(\".unchecked\").click()");
	
	WaitClick(btn_Labour);
	click(btn_Labour);
	Thread.sleep(4000);
	click(btn_ServiceProductbutton);
	Thread.sleep(4000);
	sendKeys(txt_ServiceProducttxt, ServiceProducttxt);
	Thread.sleep(4000);
	pressEnter(txt_ServiceProducttxt);
	Thread.sleep(4000);
	doubleClick(sel_ServiceProductsel);
	click(txt_EstimatedWork);
	Thread.sleep(4000);
	number4=getText(sel_EstimatedWorksel);
	Thread.sleep(4000);
	number6=Integer.parseInt(number4);
	click(sel_EstimatedWorksel);
	click(btn_EstimatedWorkhourok);
	js1.executeScript("$(\"#tblSADService\").children().find(\".unchecked\").click()");
	
	WaitClick(btn_Resource);
	click(btn_Resource);
	Thread.sleep(4000);
	click(btn_Resourcebutton);
	Thread.sleep(4000);
	sendKeys(txt_Resourcetxt, Resourcetxt);
	Thread.sleep(4000);
	pressEnter(txt_Resourcetxt);
	Thread.sleep(4000);
	doubleClick(sel_Resourcesel);
	Thread.sleep(4000);
	click(btn_ResourceServiceProductbutton);
	Thread.sleep(4000);
	sendKeys(txt_ServiceProducttxt, ServiceProducttxt);
	Thread.sleep(4000);
	pressEnter(txt_ServiceProducttxt);
	Thread.sleep(4000);
	doubleClick(sel_ServiceProductsel);
	click(txt_ResourceEstimatedWork);
	Thread.sleep(4000);
	number10=getText(sel_ResourceEstimatedWorksel);
	Thread.sleep(4000);
	number11=Integer.parseInt(number10);
	Thread.sleep(4000);
	click(sel_ResourceEstimatedWorksel);
	js1.executeScript("$(\"#tblSADResource\").children().find(\".unchecked\").click()");
	
	WaitClick(btn_Expense);
	click(btn_Expense);
	selectIndex(txt_AnalysisCode, 2);
	Thread.sleep(4000);
	click(btn_ExpenseServiceProductbutton);
	Thread.sleep(4000);
	sendKeys(txt_ServiceProducttxt, ServiceProducttxt);
	Thread.sleep(4000);
	pressEnter(txt_ServiceProducttxt);
	Thread.sleep(4000);
	doubleClick(sel_ServiceProductsel);
	sendKeys(txt_EstimatedCost, EstimatedCost);
	js1.executeScript("$(\"#tblSADExpence\").children().find(\".unchecked\").click()");
	
	WaitClick(btn_TaskUpdate);
	click(btn_TaskUpdate);
	
	pageRefersh();
	Thread.sleep(3000);
	
	WaitClick(btn_WorkBreakdown);
	click(btn_WorkBreakdown);
	WaitClick(btn_releaseTaskdropdown);
	click(btn_releaseTaskdropdown);
	WaitClick(btn_releaseTask);
	click(btn_releaseTask);
	
	pageRefersh();
	Thread.sleep(3000);
	WaitClick(btn_update);
	click(btn_update);
}


/*
 * =============================================================================
 * =================
 */

public static String quotationIDVal;
public static String s;
public static String sNew;

public static String paymentHeader;
public static String paymentHeader1;
public static String paymentHeader2;
public static String vendorRefVal = Integer.toString(generateRandomIntIntRange(1, 10000));

//s5
public static String modelCode = Integer.toString(generateRandomIntIntRange(1, 100));

// 31
public static String expenseExistingValBef;
public static String expenseExistingValAft;

//32
public static String accrualHeaderVal;

//33
public static String customerRefundHeaderVal;

//41
public static String customerDebitHeaderVal;

//44
public static String vendorRefundHeaderVal;

// 45
public static String pettyBeforeVal;
public static String pettyAfterVal;

//46
public static String expenseExistingValInv3;
public static String expenseExistingValInv4;

//47
public static String pettyCashHeaderVal;

//48
public static String amount1;
public static String bankAdjHeaderVal;

//51
public static String bannerTot;
public static String InvoiceHeaderVal;

//52
public static String expenseExistingValInv1;
public static String expenseExistingValInv2;

//57
public static String quotationID;
public static String projectCodeVal = Integer.toString(generateRandomIntIntRange(1, 1000));

//Smoke 3

public void checkProjectModule() throws Exception {

	if (isDisplayed(projectModule)) {
		writeTestResults("Verify that Project module is available", "User should be able to see the 'Project'",
				"Project module is dislayed", "pass");
	} else {
		writeTestResults("Verify that Project module is available", "User should be able to see the 'Project'",
				"Project module is not dislayed", "Fail");
	}
}

public void checkFinanceModule() throws Exception {

	if (isDisplayed(financeModule)) {
		writeTestResults("Verify that finance module is available", "User should be able to see the 'Finance'",
				"Finance module is dislayed", "Pass");
	} else {
		writeTestResults("Verify that finance module is available", "User should be able to see the 'Finance'",
				"Finance module is not dislayed", "Fail");
	}
}

public void clickOnProjectModule() throws Exception {

	click(projectModule);
	Thread.sleep(2000);
	if (isDisplayed(searchBar)) {
		writeTestResults("Verify that user can navigate to project menu",
				"User should be able to see the project menu", "Project menu is dislayed", "Pass");
	} else {
		writeTestResults("Verify that user can navigate to project menu",
				"User should be able to see the project menu", "Project menu is not dislayed", "Fail");
	}
}

public void clickOnFinance() throws Exception {

	click(financeModule);

	if (isDisplayed(searchBar)) {
		writeTestResults("Verify that user can navigate to finance menu",
				"User should be able to see the finance menu", "Finance menu is dislayed", "Pass");
	} else {
		writeTestResults("Verify that user can navigate to finance menu",
				"User should be able to see the finance menu", "Finance menu is not dislayed", "Fail");
	}
}

//Verify the ability to do cost allocation for project tasks using accrual
//voucher--30

public void costAllocation() throws Exception {

	click(outboundPaymentAdvice);
	Thread.sleep(2000);
	click(newOutboundPaymentAdvice);
	Thread.sleep(2000);
	click(accuralVoucher);
	Thread.sleep(2000);
	click(vendorSearch);
	Thread.sleep(3000);
	sendKeys(vendorText, vendorVal);
	Thread.sleep(2000);
	pressEnter(vendorText);
	Thread.sleep(2000);
	doubleClick(vendorSelectedVal);
	Thread.sleep(2000);
	selectText(analysisCodeDrop, analysisCodeVal);
	Thread.sleep(2000);
	sendKeys(amountText, amountVal);
	Thread.sleep(2000);
	click(costAllocationBtn);
	Thread.sleep(2000);
	sendKeys(precentageText, percentageVal);
	Thread.sleep(2000);
	selectIndex(costAssignmentTypeNad, 1);
	Thread.sleep(2000);
	click(costObjectSearch);
	Thread.sleep(2000);
	sendKeys(costObjectText, costObjectVal);
	Thread.sleep(2000);
	pressEnter(costObjectText);
	Thread.sleep(2000);
	doubleClick(costObjectSelected);
	Thread.sleep(2000);
	click(addRecord);
	Thread.sleep(2000);
	click(updateCostAllocation);
	Thread.sleep(2000);

	sendKeys(vendorRef, vendorRefVal);
	Thread.sleep(2000);
	click(checkout);
	Thread.sleep(5000);

	if (isDisplayed(draftNad)) {
		writeTestResults("Verify that draft button is available on the page",
				"User should be able to see the draft button", "Draft button is available", "Pass");
		Thread.sleep(4000);
		click(draftNad);
		
	} else {
		writeTestResults("Verify that draft button is available on the page",
				"User should be able to see the draft button", "Draft button is not available", "Fail");
	}
	Thread.sleep(3000);
	accrualHeaderVal = getText(accrualHeader);
	 //System.out.println(accrualHeaderVal);
	Thread.sleep(3000);
	click(releaseNad);
	Thread.sleep(3000);

	String header = getText(releaseInHeader);
	// System.out.println(header);

	// System.out.println(getText(documentVal));
	trackCode = getText(documentVal);
	Thread.sleep(5000);
	if (header.equals("( Released )")) {
		writeTestResults("User should be able to release the accrual voucher", "accrual voucher should be released",
				"Accrual voucher is released", "Pass");
	} else {
		writeTestResults("User should be able to release the accrual voucher", "accrual voucher should be released",
				"Accrual voucher is not released", "Fail");
	}
}

public void getValueExpenseBefore() throws Exception {

	click(project);
	Thread.sleep(2000);
	sendKeys(projectSearchText, projectVal);
	Thread.sleep(2000);
	pressEnter(projectSearchText);
	Thread.sleep(2000);
	click(projectSelectedVal);
	Thread.sleep(2000);
	click(costingSummaryTab);
	Thread.sleep(2000);
	click(expenseArrow);

	expenseExistingValBef = getText(existingVal);
	System.out.println(expenseExistingValBef);

}

public static int generateRandomIntIntRange(int min, int max) {
	Random r = new Random();
	return r.nextInt((max - min) + 1) + min;
}

//Verify that expenses recorded from the accrual voucher shows in project
//costing summary -- 31

public void getValueExpenseAfter() throws Exception {

	click(project);
	Thread.sleep(2000);
	sendKeys(projectSearchText, projectVal);
	Thread.sleep(2000);
	pressEnter(projectSearchText);
	Thread.sleep(2000);
	click(projectSelectedVal);
	Thread.sleep(2000);
	click(costingSummaryTab);
	Thread.sleep(2000);
	click(expenseArrow);

	expenseExistingValAft = getText(existingVal);
	System.out.println(expenseExistingValAft);

}

public void checkExpensesAccrual() throws Exception {

	getValueExpenseAfter();
	Thread.sleep(2000);
	String expenBefore = expenseExistingValBef;
	String expenseBefore1 = expenBefore.replace(",", "");
	// System.out.println(val2);
	String expenseBefore2 = expenseBefore1.substring(0, expenseBefore1.length() - 6);
	// System.out.println(val3);

	int expenseBefore3 = Integer.parseInt(expenseBefore2);
	int amount = Integer.parseInt(amountVal);

	String expenseAft = expenseExistingValAft;
	String expenseAft1 = expenseAft.replace(",", "");
	// System.out.println(val22);
	String expenseAft2 = expenseAft1.substring(0, expenseAft1.length() - 6);
	System.out.println(expenseAft2);

	int expenseAft3 = Integer.parseInt(expenseAft2);
	// System.out.println(val44);

	System.out.println(expenseBefore3 + amount);

	if (expenseAft3 == expenseBefore3 + amount) {
		writeTestResults("Verify that actual cost updated in costing summary according to accrual voucher",
				"Expenses recorded from the accrual voucher should shows in project overview",
				"Values updated accordingly", "Pass");
	} else {
		writeTestResults("Verify that actual cost updated in costing summary according to accrual voucher",
				"Expenses recorded from the accrual voucher should shows in project overview",
				"Values not updated accordingly", "Fail");
	}
}

//Verify that expenses recorded from the accrual voucher shows in project
//overview -- 32
public void checkOverViewAccrual() throws Exception {

	click(project);
	Thread.sleep(3000);
	sendKeys(projectSearchText, projectVal);
	Thread.sleep(2000);
	pressEnter(projectSearchText);
	Thread.sleep(2000);
	click(projectSelectedVal);
	Thread.sleep(2000);
	click(overviewNad);
	Thread.sleep(2000);
	click(expenses);
	Thread.sleep(3000);
	String remove = accrualHeaderVal.replace(" ", "");
	 //System.out.println(remove);
	String remove1 = remove.replace("AccrualVoucher", "");
	 //System.out.println(remove1);
	 Thread.sleep(3000);
	String accrual = valPre.replace("OPA/27571", remove1);
	 //System.out.println(accrual);
	Thread.sleep(2000);
	String cus = getText(accrual);
	// System.out.println(cus);
	String cus1 = cus.substring(0, cus.length() - 6);
	System.out.println(cus1);
	System.out.println(amountVal);

	// String cus1 = cus.replace(",", "");

	if (cus1.equals(amountVal)) {
		writeTestResults("Verify that actual cost updated in overview according to accrual voucher",
				"Actual cost should be updated for expenses", "Values are equal", "Pass");
	} else {
		writeTestResults("Verify that actual cost updated in overview according to accrual voucher",
				"Actual cost should be updated for expenses", "Values are not equal", "Fail");
	}
}

//Verify the ability to do cost allocation for project tasks using customer
//refund --33
public void customerRefund() throws Exception {

	click(financeModule);
	Thread.sleep(2000);
	click(outboundPaymentAdvice);
	Thread.sleep(2000);
	click(newOutboundPaymentAdvice);
	Thread.sleep(2000);
	click(downArrow);
	Thread.sleep(2000);
	click(customerRefund);
	
	Thread.sleep(2000);
	click(customerSearch);
	Thread.sleep(3000);
	sendKeys(customerText, customerVal);
	Thread.sleep(3000);
	pressEnter(customerText);
	Thread.sleep(3000);
	doubleClick(customerSelectedVal);
	
	Thread.sleep(2000);
	selectIndex(analysisCodeDrop, 1);
	Thread.sleep(2000);
	sendKeys(amountText, cusRefundAmountVal);
	Thread.sleep(2000);
	click(costAllocationBtn);
	Thread.sleep(2000);
	sendKeys(precentageText, percentageVal);
	Thread.sleep(2000);
	selectText(costAssignmentTypeNad, "Project Task");
	Thread.sleep(2000);
	click(costObjectSearch);
	Thread.sleep(2000);
	sendKeys(costObjectText, costObjectVal);
	Thread.sleep(2000);
	pressEnter(costObjectText);
	Thread.sleep(2000);
	doubleClick(costObjectSelected);
	Thread.sleep(2000);
	click(addRecord);
	Thread.sleep(2000);
	click(updateCostAllocation);
	Thread.sleep(2000);
	sendKeys(vendorRef, vendorRefVal);
	click(checkout);
	Thread.sleep(2000);
	click(draftNad);
	Thread.sleep(3000);
	customerRefundHeaderVal = getText(customerRefundHeader);
	// System.out.println(customerRefundHeaderVal);
	click(releaseNad);
	Thread.sleep(4000);
	String header = getText(releaseInHeader);
	// System.out.println(header);

	// System.out.println(getText(documentVal));
	trackCode = getText(documentVal);
	Thread.sleep(5000);
	if (header.equals("( Released )")) {
		writeTestResults("User should be able to release the customer refund", "Customer refund should be released",
				"Customer refund is released", "Pass");

	} else {
		writeTestResults("User should be able to release the customer refund", "Customer refund should be released",
				"Customer refund is not released", "Fail");
	}

}

//Verify that expenses recorded from the customer refund shows in project
//costing summary -- 34
public void getValueCustomerRefund() throws Exception {

	click(project);
	Thread.sleep(2000);
	sendKeys(projectSearchText, projectVal);
	Thread.sleep(2000);
	pressEnter(projectSearchText);
	Thread.sleep(2000);
	click(projectSelectedVal);
	Thread.sleep(2000);
	click(costingSummaryTab);
	Thread.sleep(2000);
	click(adjustment);

	String remove = customerRefundHeaderVal.replace(" ", "");
	 System.out.println(remove);
	String remove1 = remove.replace("CustomerRefund", "");
	 System.out.println(remove1);
	String cusRefund = cusRefundCostingSum.replace("OPA/27598", remove1);
	String cus = getText(cusRefund);

	String cus1 = cus.substring(0, cus.length() - 6);

	String cus2 = cus1.replace(",", "");
	Thread.sleep(2000);
	 System.out.println(cus2);
	System.out.println(cusRefundAmountVal);

	if (cus2.equals("-" + cusRefundAmountVal)) {
		writeTestResults("User should be able to see the recorded customer refund amount",
				"Verify that expenses recorded from the customer refund shows in project costing summary",
				"Values are equal", "Pass");
	} else {
		writeTestResults("User should be able to see the recorded customer refund amount",
				"Verify that expenses recorded from the customer refund shows in project costing summary",
				"Values are not equal", "Fail");
	}
}

//Verify that expenses recorded from the customer refund shows in project
//overview -- 35
public void chekOverviewCustomerRefund() throws Exception {
	click(project);
	Thread.sleep(3000);
	sendKeys(projectSearchText, projectVal);
	Thread.sleep(2000);
	pressEnter(projectSearchText);
	Thread.sleep(2000);
	click(projectSelectedVal);
	Thread.sleep(2000);
	click(overviewNad);
	Thread.sleep(2000);
	click(revenueAdjustment);

	String remove = customerRefundHeaderVal.replace(" ", "");
//	 System.out.println(remove);
	String remove1 = remove.replace("CustomerRefund", "");
//	 System.out.println(remove1);
	String cusRefund = valOverviewCusRefund.replace("OPA/27598", remove1);

	String cus = getText(cusRefund);
	cus = cus.substring(0, Math.min(cus.length(), 5));

	String cus1 = cus.replace(",", "");
//	Thread.sleep(2000);
//	 System.out.println(cus1);
//	 System.out.println(cusRefundAmountVal);

	if (cus1.equals(cusRefundAmountVal)) {
		writeTestResults("User should be able to see the recorded customer refund amount",
				"Verify that expenses recorded from the customer refund shows in project overview",
				"Values are equal", "Pass");
	} else {
		writeTestResults("User should be able to see the recorded customer refund amount",
				"Verify that expenses recorded from the customer refund shows in project overview",
				"Values are equal", "Fail");
	}

}

// Verify the ability to do cost allocation for project tasks using payment
// voucher -- 36

public void getValueExpenseBeforePaymentVoucher() throws Exception {

	click(project);
	Thread.sleep(2000);
	sendKeys(projectSearchText, projectVal);
	Thread.sleep(2000);
	pressEnter(projectSearchText);
	Thread.sleep(2000);
	click(projectSelectedVal);
	Thread.sleep(2000);
	click(costingSummaryTab);
	Thread.sleep(2000);
	click(expenseArrow);

	expenseExistingValBef = getText(projectInExpenseVal);
	// System.out.println(expenseExistingValBef);

}

public void getValueExpenseAfterPaymentVoucher() throws Exception {

	click(project);
	Thread.sleep(2000);
	sendKeys(projectSearchText, projectVal);
	Thread.sleep(2000);
	pressEnter(projectSearchText);
	Thread.sleep(2000);
	click(projectSelectedVal);
	Thread.sleep(2000);
	click(costingSummaryTab);
	Thread.sleep(2000);
	click(expenseArrow);

	expenseExistingValAft = getText(projectInExpenseVal);
	// System.out.println(expenseExistingValAft);

}

public void paymentVoucher() throws Exception {
	click(financeModule);
	Thread.sleep(2000);
	click(outboundPaymentAdvice);
	Thread.sleep(2000);
	click(newOutboundPaymentAdvice);
	Thread.sleep(2000);
	click(downArrow);
	Thread.sleep(2000);
	click(paymentVoucher);
	Thread.sleep(2000);
	selectText(analysisCodeDrop, analysisCodePayVouVal);
	Thread.sleep(2000);
	sendKeys(amountText, amountVal);
	Thread.sleep(2000);
	click(costAllocationBtn);
	Thread.sleep(2000);
	sendKeys(precentageText, percentageVal);
	Thread.sleep(2000);
	selectText(costAssignmentTypeNad, "Project Task");
	Thread.sleep(2000);
	click(costObjectSearch);
	Thread.sleep(2000);
	sendKeys(costObjectText, costObjectVal);
	Thread.sleep(2000);
	pressEnter(costObjectText);
	Thread.sleep(2000);
	doubleClick(costObjectSelected);
	Thread.sleep(2000);
	click(addRecord);
	Thread.sleep(2000);
	click(updateCostAllocation);
	Thread.sleep(2000);
	click(checkout);
	InventoryAndWarehouseModule obj = new InventoryAndWarehouseModule();
	obj.customizeLoadingDelay(draftNad, 30);
	click(draftNad);
	Thread.sleep(2000);
	paymentHeader = getText(paymentVoucherHeader);
	paymentHeader1 = paymentHeader.replace(" ", "");
	paymentHeader2 = paymentHeader1.replace("PaymentVoucher", "");
	System.out.println(paymentHeader2);
	// System.out.println(paymentHeader);
	obj.customizeLoadingDelay(releaseNad, 30);
	click(releaseNad);
	Thread.sleep(3000);

	String header = getText(releaseInHeader);

	trackCode = getText(documentVal);
	Thread.sleep(5000);
	if (header.equals("( Released )")) {
		writeTestResults("User should be able to release the payment voucher", "Payment voucher should be released",
				"Payment voucher is released", "Pass");

	} else {
		writeTestResults("User should be able to release the payment voucher", "Payment voucher should be released",
				"Payment voucher is not released", "Fail");
	}
	click(actionMenu);
	Thread.sleep(2000);
	click(convertToOutboundPayment);
	Thread.sleep(2000);
	sendKeys(payeeText, payeeVal);
	Thread.sleep(2000);
	click(detailsTab);
	Thread.sleep(2000);
	click(checkout);
	Thread.sleep(2000);
	click(draftNad);
	Thread.sleep(2000);
	click(releaseNad);
	Thread.sleep(4000);
	String header1 = getText(releaseInHeader);

	trackCode = getText(documentVal);
	Thread.sleep(5000);
	if (header1.equals("( Released )")) {
		writeTestResults("User should be able to release the outbound payment",
				"Outbound payment should be released", "Outbound payment is released", "Pass");

	} else {
		writeTestResults("User should be able to release the outbound payment",
				"Outbound payment should be released", "Outbound payment is not released", "Fail");
	}
}

// Verify that expenses recorded from the payment voucher shows in project
// costing summary -- 37
public void verifyPaymentVoucher() throws Exception {

	getValueExpenseAfterPaymentVoucher();
	Thread.sleep(2000);
	String val1 = expenseExistingValBef;
	String val2 = val1.replace(",", "");
	// System.out.println(val2);
	String val3 = val2.substring(0, val2.length() - 6);
	// System.out.println(val3);

	int val4 = Integer.parseInt(val3);
	int amount = Integer.parseInt(amountVal);

	String val11 = expenseExistingValAft;
	String val22 = val11.replace(",", "");
	// System.out.println(val22);
	String val33 = val22.substring(0, val22.length() - 6);
	// System.out.println(val33);

	int val44 = Integer.parseInt(val33);
	// System.out.println(val44);

	// System.out.println(val4 + amount);

	if (val44 == val4 + amount) {
		writeTestResults("Verify that expenses recorded from the payment voucher shows in project costing summary",
				"Expenses recorded from the payment voucher should shows in project overview",
				"Values updated accordingly", "Pass");
	} else {
		writeTestResults("Verify that actual cost updated in costing summary according to accrual voucher",
				"Expenses recorded from the payment voucher should shows in project overview",
				"Values not updated accordingly", "Fail");
	}
}

//Verify that expenses recorded from the payment voucher shows in project
//overview -- 38

public void paymentVoucherInOverview() throws Exception {

	click(project);
	Thread.sleep(2000);
	sendKeys(projectSearchText, projectVal);
	Thread.sleep(2000);
	pressEnter(projectSearchText);
	Thread.sleep(2000);
	click(projectSelectedVal);
	Thread.sleep(2000);
	click(overviewNad);
	Thread.sleep(2000);
	click(expenses);

	Thread.sleep(3000);
	String myStr = val;
	String mystr2 = myStr.replace("OPA/27383", paymentHeader2);

	val = mystr2;
	// System.out.println(myStr);
	// System.out.println(mystr2);
	// System.out.println(getText(val));
	mouseMove(val);
	String removed = getText(val);
	// System.out.println(val);

	String strNew = removed.substring(0, removed.length() - 6);
	// System.out.println(strNew);
	// System.out.println(amountVal);

	if (strNew.equals(amountVal)) {
		writeTestResults("Verify that expenses recorded from the payment voucher shows in project overview",
				"Actual cost should updated for expenses", "Actual cost updated for expenses", "Pass");
	} else {
		writeTestResults("Verify that expenses recorded from the payment voucher shows in project overview",
				"Actual cost should updated for expenses", "Actual cost not updated for expenses", "Fail");
	}
}

//Verify the ability to do cost allocation for project tasks using customer debit memo --39
public void customerDebitMemo() throws Exception {

	click(financeModule);
	Thread.sleep(2000);
	click(inboundPaymentAdvice);
	Thread.sleep(2000);
	click(newInboundPaymentAdvice);

	Thread.sleep(2000);
	click(customerDebitMemo);
	Thread.sleep(3000);
	click(customerSearchDebit);
	Thread.sleep(3000);
	sendKeys(customerText, customerVal);
	Thread.sleep(2000);
	pressEnter(customerText);
	Thread.sleep(2000);
	doubleClick(customerSelectedVal);
	Thread.sleep(2000);
	selectIndex(analysisCodeDrop, 1);
	Thread.sleep(2000);
	sendKeys(amountDebitText, amountDebitVal);
	Thread.sleep(2000);
	click(costAllocation);
	Thread.sleep(2000);
	sendKeys(precentageText, percentageVal);
	Thread.sleep(2000);
	selectText(costAssignmentTypeNad, "Project Task");
	Thread.sleep(2000);
	click(costObjectSearch);
	Thread.sleep(2000);
	sendKeys(costObjectText, costObjectVal);
	Thread.sleep(2000);
	pressEnter(costObjectText);
	Thread.sleep(2000);
	doubleClick(costObjectSelected);
	Thread.sleep(3000);
	click(addRecord);
	Thread.sleep(3000);
	click(updateCostAllocation);
	Thread.sleep(2000);
	// sendKeys(vendorRef, vendorRefVal);
	click(checkout);
	Thread.sleep(3000);
	click(draftNad);
	Thread.sleep(3000);
	customerDebitHeaderVal = getText(customerDebitHeader);
	Thread.sleep(3000);
	click(releaseNad);
	Thread.sleep(4000);
	String header = getText(releaseInHeader);

	trackCode = getText(documentVal);
	Thread.sleep(5000);
	if (header.equals("( Released )")) {
		writeTestResults("User should be able to release the Customer debit memo",
				"Customer debit memo should be released", "Customer debit memo is released", "Pass");

	} else {
		writeTestResults("User should be able to release the Customer debit memo",
				"Customer debit memo should be released", "Customer debit memo is not released", "Fail");
	}

}

//Verify that expenses recorded from the customer debit memo shows in project
//costing summary -- 40
public void getValueCustomerDebitMemo() throws Exception {

	click(project);
	Thread.sleep(2000);
	sendKeys(projectSearchText, projectVal);
	Thread.sleep(2000);
	pressEnter(projectSearchText);
	Thread.sleep(2000);
	click(projectSelectedVal);
	Thread.sleep(2000);
	click(costingSummaryTab);
	Thread.sleep(2000);
	click(adjustment);

	String remove = customerDebitHeaderVal.replace(" ", "");
	// System.out.println(remove);
	String remove1 = remove.replace("CustomerDebitMemo", "");
	// System.out.println(remove1);
	String cusRefund = cusDebitMemoSum.replace("IPA/18218", remove1);
	Thread.sleep(4000);
	if (isElementPresent(cusRefund)) {
		writeTestResults("Verify that customer debit memo record shows in project costing summary",
				"Customer debit memo record should be avilable", "Customer debit memo record is avilable", "Pass");

	} else {

		writeTestResults("Verify that customer debit memo record shows in project costing summary",
				"Customer debit memo record should be avilable", "Customer debit memo record is not avilable",
				"Pass");
	}

	String cus = getText(cusRefund);

	String cus1 = cus.substring(0, cus.length() - 6);

	String cus2 = cus1.replace(",", "");
	Thread.sleep(2000);
	// System.out.println(cus2);
	// System.out.println(amountDebitVal);

	if (cus2.equals(amountDebitVal)) {
		writeTestResults("User should be able to see the released customer debit memo",
				"Values should be equal as in released customer debit memo", "Values are equal", "Pass");
	} else {
		writeTestResults("User should be able to see the released customer debit memo",
				"Values should be equal as in released customer debit memo", "Values are not equal", "Fail");
	}
}

//Verify that expenses recorded from the customer debit memo shows in project
//overview -- 41
public void chekOverviewCustomerDebit() throws Exception {

	click(project);
	Thread.sleep(3000);
	sendKeys(projectSearchText, projectVal);
	Thread.sleep(2000);
	pressEnter(projectSearchText);
	Thread.sleep(2000);
	click(projectSelectedVal);
	Thread.sleep(2000);
	click(overviewNad);
	Thread.sleep(2000);
	click(revenueAdjustment);

	String remove = customerDebitHeaderVal.replace(" ", "");
	// System.out.println(remove);
	String remove1 = remove.replace("CustomerDebitMemo", "");
	// System.out.println(remove1);
	String cusRefund = valOverviewCusDebit.replace("IPA/18082", remove1);
	Thread.sleep(4000);
	if (isElementPresent(cusRefund)) {
		writeTestResults("Verify that customer debit memo record shows in project overview",
				"Customer debit memo record should be avilable in project overview",
				"Customer debit memo record is avilable", "Pass");
	} else {
		writeTestResults("Verify that customer debit memo record shows in project overview",
				"Customer debit memo record should be avilable in project overview",
				"Customer debit memo record is avilable", "Pass");
	}

	String cus = getText(cusRefund);
	cus = cus.substring(0, Math.min(cus.length(), 5));

	String cus1 = cus.replace(",", "");
	Thread.sleep(2000);
	// System.out.println(cus1);
	// System.out.println(amountDebitVal);

	if (cus1.equals(amountDebitVal)) {
		writeTestResults("Verify that the released customer debit value is e",
				"Values should be equal as in released customer debit memo", "Values are equal", "Pass");
	} else {
		writeTestResults("User should be able to see the released customer debit memo in project overview",
				"Values should be equal", "Values are not equal", "Fail");
	}

}

//Verify the ability to do cost allocation for project tasks using vendor
//refund --- 42
public void vendorRefund() throws Exception {

	click(financeModule);
	Thread.sleep(2000);
	click(inboundPaymentAdvice);
	Thread.sleep(2000);
	click(newInboundPaymentAdvice);
	Thread.sleep(2000);
	click(vendorRefund);
	Thread.sleep(2000);
	click(customerSearchDebit);
	Thread.sleep(3000);
	sendKeys(vendorText, vendorVal);
	Thread.sleep(2000);
	pressEnter(vendorText);
	Thread.sleep(2000);
	doubleClick(vendorSelectedVal);
	Thread.sleep(2000);
	selectIndex(analysisCodeDrop, 1);
	Thread.sleep(2000);
	sendKeys(amountDebitText, amountVal);
	Thread.sleep(2000);
	click(costAllocationBtnVendor);
	Thread.sleep(3000);
	sendKeys(precentageTextVendor, percentageVal);
	Thread.sleep(2000);
	selectText(costAssignmentTypeNad, "Project Task");
	Thread.sleep(2000);
	click(costObjectSearch);
	Thread.sleep(2000);
	sendKeys(costObjectText, costObjectVal);
	Thread.sleep(2000);
	pressEnter(costObjectText);
	Thread.sleep(2000);
	doubleClick(costObjectSelected);
	Thread.sleep(2000);
	click(addRecord);
	Thread.sleep(2000);
	click(updateCostAllocation);
	Thread.sleep(2000);
	sendKeys(vendorRef, vendorRefVal);
	click(checkout);
	Thread.sleep(2000);
	click(draftNad);
	Thread.sleep(3000);
	vendorRefundHeaderVal = getText(vendorRefundHeader);
	Thread.sleep(2000);
	click(releaseNad);

	Thread.sleep(4000);
	String header = getText(releaseInHeader);

	trackCode = getText(documentVal);
	Thread.sleep(5000);
	if (header.equals("( Released )")) {
		writeTestResults("Verify the ability to do cost allocation for project tasks using vendor refund",
				"vendor refund should be released", "vendor refund is released", "Pass");
	} else {
		writeTestResults("User should be able to release the sales invoice", "vendor refund should be released",
				"vendor refund is not released", "Fail");
	}
}

//43
public void vendorRefundCostingSummary() throws Exception {

	click(project);
	Thread.sleep(2000);
	sendKeys(projectSearchText, projectVal);
	Thread.sleep(2000);
	pressEnter(projectSearchText);
	Thread.sleep(2000);
	click(projectSelectedVal);
	Thread.sleep(2000);
	click(costingSummaryTab);
	Thread.sleep(2000);
	click(costsAdjustment);

	String remove = vendorRefundHeaderVal.replace(" ", "");
	System.out.println(remove);
	String remove1 = remove.replace("VendorRefund", "");
	System.out.println(remove1);
	String cusRefund = vendorRefundCosting.replace("IPA/18699", remove1);
	System.out.println(cusRefund);
	String cus = getText(cusRefund);
	System.out.println(cus);

	String cus1 = cus.substring(0, cus.length() - 6);

	String cus2 = cus1.replace(",", "");
	Thread.sleep(2000);
	System.out.println(cus2);
	System.out.println(amountVal);

	if (cus2.equals("-" + amountVal)) {
		writeTestResults("User should be", "Values should be equal", "Values are equal", "Pass");
	} else {
		writeTestResults("User should be", "Values should be equal", "Values are not equal", "Fail");
	}

}

//Verify that expenses recorded from the vendor refund shows in project
//overview -- 44

public void checkOverviewCustomerRefund() throws Exception {

	click(project);
	Thread.sleep(3000);
	sendKeys(projectSearchText, projectVal);
	Thread.sleep(2000);
	pressEnter(projectSearchText);
	Thread.sleep(2000);
	click(projectSelectedVal);
	Thread.sleep(2000);
	click(overviewNad);
	Thread.sleep(2000);
	click(costAdjustment);

	String remove = vendorRefundHeaderVal.replace(" ", "");
	System.out.println(remove);
	String remove1 = remove.replace("VendorRefund", "");
	System.out.println(remove1);
	String cusRefund = valOverviewVendorRefund.replace("IPA/18090", remove1);

	String cus = getText(cusRefund);
	cus = cus.substring(0, Math.min(cus.length(), 4));

	String cus1 = cus.replace(",", "");
	Thread.sleep(2000);
	System.out.println(cus1);
	System.out.println(amountVal);

	if (cus1.equals("-" + amountVal)) {
		writeTestResults("Verify that expenses recorded from the vendor refund shows in project overview",
				"Values should be equal", "Values are equal", "Pass");
	} else {
		writeTestResults("Verify that expenses recorded from the vendor refund shows in project overview",
				"Values should be equal as vedor refund", "Values are not equal", "Fail");
	}
}

//Verify the ability to do cost allocation for project tasks using petty cash
//-- 45

public void pettyCash() throws Exception {

	click(financeModule);
	Thread.sleep(2000);
	click(pettyCash);
	Thread.sleep(2000);
	click(newPettyCash);
	Thread.sleep(2000);
	selectIndex(pettyCashAccount, 1);
	Thread.sleep(2000);
	click(customerSearchDebit);
	Thread.sleep(3000);
	sendKeys(descriptionPetty, desVal);
	Thread.sleep(2000);
	click(paidtoSearch);
	Thread.sleep(3000);
	sendKeys(employeeSearchText, employeeVal);
	Thread.sleep(3000);
	pressEnter(employeeSearchText);
	Thread.sleep(2000);
	doubleClick(empSelectedVal);
	Thread.sleep(2000);
	selectIndex(type, 2);
//	Thread.sleep(2000);
//	click(IOUNoSearch);

	Thread.sleep(2000);
	selectText(analysisCodeDrop, analysisCodePettyCash);
	Thread.sleep(2000);
	sendKeys(narration, narrationVal);
	Thread.sleep(2000);
	sendKeys(amountTextPetty, amountPettyVal);
	Thread.sleep(2000);
	click(costAllocationPetty);
	Thread.sleep(2000);
	sendKeys(precentageText, percentageVal);
	Thread.sleep(2000);
	selectIndex(costAssignmentTypeNad, 1);
	Thread.sleep(2000);
	click(costObjectSearch);
	Thread.sleep(2000);
	sendKeys(costObjectText, costObjectVal);
	Thread.sleep(2000);
	pressEnter(costObjectText);
	Thread.sleep(2000);
	doubleClick(costObjectSelected);
	Thread.sleep(2000);
	click(addRecord);
	Thread.sleep(2000);
	click(updateCostAllocation);
	Thread.sleep(2000);
	click(draftNad);
	Thread.sleep(3000);
	pettyCashHeaderVal = getText(pettyCashHeader);
	System.out.println(pettyCashHeaderVal);
	Thread.sleep(2000);
	click(releaseNad);

	Thread.sleep(4000);
	String header = getText(releaseInHeader);

	trackCode = getText(documentVal);
	Thread.sleep(5000);
	if (header.equals("(Released)")) {
		writeTestResults("Verify the ability to do cost allocation for project tasks using petty cash",
				"Petty cash should be released", "Petty cash is released", "Pass");

	} else {
		writeTestResults("Verify the ability to do cost allocation for project tasks using petty cash",
				"Petty cash should be released", "Petty cash is not released", "Fail");
	}

}

//Verify that expenses recorded from the petty cash shows in project costing
//summary -- 46

public void getValueExpensePettyBefore() throws Exception {

	click(projectModule);
	Thread.sleep(2000);
	click(project);
	Thread.sleep(2000);
	sendKeys(projectSearchText, projectVal);
	Thread.sleep(2000);
	pressEnter(projectSearchText);
	Thread.sleep(2000);
	click(projectSelectedVal);
	Thread.sleep(2000);
	click(costingSummaryTab);
	Thread.sleep(2000);
	click(expenseArrow);

	pettyBeforeVal = getText(existingVal);
	// System.out.println(pettyBeforeVal);

}

public void getValueExpensePettyAfter() throws Exception {

	click(project);
	Thread.sleep(2000);
	sendKeys(projectSearchText, projectVal);
	Thread.sleep(2000);
	pressEnter(projectSearchText);
	Thread.sleep(2000);
	click(projectSelectedVal);
	Thread.sleep(2000);
	click(costingSummaryTab);
	Thread.sleep(2000);
	click(expenseArrow);
	Thread.sleep(2000);
	pettyAfterVal = getText(existingVal);
	// System.out.println(pettyAfterVal);

}

public void checkValuePettyCashCostingSummary() throws Exception {

	System.out.println(pettyBeforeVal);
	System.out.println(pettyAfterVal);

	String val1 = pettyBeforeVal;
	// System.out.println(val1);

	String val3 = val1.replace(",", "");
	// System.out.println(val3);
	String val4 = val3.substring(0, val3.length() - 6);
	// System.out.println(val4);

	int val5 = Integer.parseInt(val4);
	// System.out.println(val5);

	String val22 = pettyAfterVal;
	// System.out.println(val22);
	String val33 = val22.replace(",", "");
	// System.out.println(val33);

	String val44 = val33.substring(0, val33.length() - 6);
	// System.out.println(val44);

	int val55 = Integer.parseInt(val44);
	// System.out.println(val55);

	int amountPetty = Integer.parseInt(amountPettyVal);

	if (val55 == val5 + amountPetty) {
		writeTestResults("Verify that expenses recorded from the petty cash shows in project costing summary",
				"Actual cost should be update according to recorded value", "Values are equal", "Pass");
	} else {
		writeTestResults("Verify that expenses recorded from the petty cash shows in project costing summary",
				"Actual cost should be update according to recorded value", "Values are not equal", "Fail");
	}
}

//Verify that expenses recorded from the petty cash shows in project overview
//-- 47
public void checkOverviewPetty() throws Exception {

	click(project);
	Thread.sleep(3000);
	sendKeys(projectSearchText, projectVal);
	Thread.sleep(2000);
	pressEnter(projectSearchText);
	Thread.sleep(2000);
	doubleClick(projectSelectedVal);
	Thread.sleep(2000);
	click(overviewNad);
	Thread.sleep(2000);
	click(expenses);
	Thread.sleep(3000);

	String cusRefund = valOverviewPetty.replace("PC/19/003505", pettyCashHeaderVal);
	// System.out.println(cusRefund);

	String cus = getText(cusRefund);
	String cus1 = cus.substring(0, cus.length() - 6);

	String cus2 = cus1.replace(",", "");
	Thread.sleep(2000);
	// System.out.println(cus2);
	// System.out.println(amountPettyVal);

	if (cus2.equals(amountPettyVal)) {
		writeTestResults("Verify that expenses recorded from the petty cash shows in project overview",
				"Values should be equal", "Actual cost updated for recorded petty cash", "Pass");
	} else {
		writeTestResults("Verify that expenses recorded from the petty cash shows in project overview",
				"Values should be equal", "Actual cost not updated for recorded petty cash", "Fail");
	}
}

// Verify the ability to do cost allocation for project tasks using bank
// adjustment -- 48
public void bankAdjustment() throws Exception {

	click(financeModule);
	Thread.sleep(2000);
	click(bankAdjustment);
	Thread.sleep(2000);
	click(newBankAdjustment);
	Thread.sleep(2000);
	selectIndex(selectBank, 1);
	Thread.sleep(2000);
	selectIndex(bankAccountNo, 2);
	Thread.sleep(3000);
	selectText(analysisCode, analysisCodeBankAdj);
	Thread.sleep(1000);
//		click(GLAccountSearch);
//		Thread.sleep(2000);
//		sendKeys(GLAccountText, GLAccountVal);
//		Thread.sleep(2000);
//		pressEnter(GLAccountText);
//		Thread.sleep(2000);
//		doubleClick(GLAccountSelected);
	Thread.sleep(2000);
	sendKeys(amountTextt, minusAmountVal);
	Thread.sleep(2000);
	click(costAllocate);
	Thread.sleep(2000);
	sendKeys(precentageText, percentageVal);
	Thread.sleep(2000);
	selectIndex(costAssignmentTypeNad, 1);
	Thread.sleep(2000);
	click(costObjectSearch);
	Thread.sleep(2000);
	sendKeys(costObjectText, costObjectVal);
	Thread.sleep(2000);
	pressEnter(costObjectText);
	Thread.sleep(2000);
	doubleClick(costObjectSelected);
	Thread.sleep(2000);
	click(addRecord);
	Thread.sleep(2000);
	click(updateCostAllocation);
	Thread.sleep(2000);
	click(checkout);
	Thread.sleep(2000);
	click(draftNad);
	Thread.sleep(2000);

	bankAdjHeaderVal = getText(pettyCashHeader);
	System.out.println(bankAdjHeaderVal);

	click(releaseNad);

	Thread.sleep(4000);
	String header = getText(releaseInHeader);

	trackCode = getText(documentVal);
	System.out.println(trackCode);
	Thread.sleep(5000);
	if (header.equals("( Released )")) {
		writeTestResults("Verify the ability to do cost allocation for project tasks using bank adjustment",
				"User should be able to release the bank adjustment", "Bank adjustment is released", "Pass");
	} else {
		writeTestResults("Verify the ability to do cost allocation for project tasks using bank adjustment",
				"User should be able to release the bank adjustment", "Bank adjustment is not released", "Fail");
	}
}

// 49
public void getValueExpenseBankAdjBefore() throws Exception {

	click(project);
	Thread.sleep(2000);
	sendKeys(projectSearchText, projectVal);
	Thread.sleep(2000);
	pressEnter(projectSearchText);
	Thread.sleep(2000);
	doubleClick(projectSelectedVal);
	Thread.sleep(2000);
	click(costingSummaryTab);
	Thread.sleep(2000);
	click(expenseArrow);

	expenseExistingValInv1 = getText(existingVal);
	// System.out.println(expenseExistingValInv1);

}

public void getValueExpenseBankAdjAfter() throws Exception {

	click(project);
	Thread.sleep(2000);
	sendKeys(projectSearchText, projectVal);
	Thread.sleep(2000);
	pressEnter(projectSearchText);
	Thread.sleep(3000);
	doubleClick(projectSelectedVal);
	Thread.sleep(2000);
	click(costingSummaryTab);
	Thread.sleep(2000);
	click(expenseArrow);
	Thread.sleep(2000);
	expenseExistingValInv2 = getText(existingVal);
	// System.out.println(expenseExistingValInv2);

}

public void checkCostingBankAdj() throws Exception {

	getValueExpenseBankAdjAfter();

	Thread.sleep(2000);
	String bankAdj = expenseExistingValInv1;
	// System.out.println(bankAdj);

	String bankAdj1 = bankAdj.replace(",", "");

	// System.out.println(expenseExistingValInv2);

	String bankAdj2 = bankAdj1.substring(0, bankAdj1.length() - 6);
	// System.out.println(bankAdj2);

	int bankAdj3 = Integer.parseInt(bankAdj2);
	// System.out.println(bankAdj3);

	String bank = expenseExistingValInv2;
	// System.out.println(val22);
	String bank1 = bank.replace(",", "");
	// System.out.println(val33);

	String bank2 = bank1.substring(0, bank1.length() - 6);
	// System.out.println(val44);

	int bank3 = Integer.parseInt(bank2);

	// banner Total
	String amount = minusAmountVal;
	amount1 = amount.replace("-", "");
	// System.out.println(amount1);

	int ban2 = Integer.parseInt(amount1);

	System.out.println(bank3);
	System.out.println(bankAdj3);
	System.out.println(ban2);

	if (bank3 == (bankAdj3 + ban2)) {
		writeTestResults("Verify that expenses recorded from the bank adjustment shows in project costing summary",
				"User should be able to see the actual cost updated under costing summary according to bank adjustment",
				"Actual cost updated for bank adjustment", "Pass");
	} else {
		writeTestResults("Verify that expenses recorded from the bank adjustment shows in project costing summary",
				"User should be able to see the actual cost updated under costing summary according to bank adjustment",
				"Actual cost not updated for bank adjustment", "Fail");
	}
}

// Verify that expenses recorded from the bank adjustment shows in project
// overview -- 50
public void checkBankCostingSummary() throws Exception {

	click(project);
	Thread.sleep(2000);
	sendKeys(projectSearchText, projectVal);
	Thread.sleep(2000);
	pressEnter(projectSearchText);
	Thread.sleep(2000);
	click(projectSelectedVal);
	Thread.sleep(2000);

	click(overviewNad);
	Thread.sleep(2000);
	click(expenses);
	Thread.sleep(2000);
	// click(valueNad);
	Thread.sleep(2000);
	String cusRefund = valOverviewBA.replace("BA/05710", bankAdjHeaderVal);
	System.out.println(cusRefund);

	String cus = getText(cusRefund);
	String cus1 = cus.substring(0, cus.length() - 6);

	String cus2 = cus1.replace(",", "");
	Thread.sleep(2000);
	System.out.println(cus2);
	System.out.println(amount1);

	if (cus2.equals(amount1)) {
		writeTestResults("Verify that expenses recorded from the bank adjustment shows in project overview",
				"User should be able to see the released bank adjustment details under expenses",
				"Released bank adjustment record not available in the overview with correct values", "Pass");
	} else {
		writeTestResults("Verify that expenses recorded from the bank adjustment shows in project overview",
				"User should be able to see the released bank adjustment details under expenses",
				"Released bank adjustment record not available in the overview with correct values", "Fail");
	}
}

//Verify the ability to do cost allocation for project tasks using purchase
//invoice -- 51
public void costAllocationPurchaseInvoice() throws Exception {

	click(procument);
	Thread.sleep(2000);
	click(purchaseInvoice);
	Thread.sleep(2000);
	click(newPurchaseInvoice);
	Thread.sleep(2000);
	click(purchaseInvoiceService);
	Thread.sleep(3000);
	click(vendorSearchBtn);
	Thread.sleep(2000);
	sendKeys(vendorText, vendorVal);
	Thread.sleep(2000);
	pressEnter(vendorText);
	Thread.sleep(2000);
	doubleClick(vendorSelectedVal);
	Thread.sleep(2000);
	click(productSearchBtn);
	Thread.sleep(2000);
	sendKeys(productSearchText, productSerVal);
	Thread.sleep(2000);
	pressEnter(productSearchText);
	Thread.sleep(4000);
	doubleClick(serviceSelected);
	click(checkout);
	Thread.sleep(2000);
	click(costAllocationInv);
	Thread.sleep(2000);

	click(removeFirstRow);
	Thread.sleep(2000);
	sendKeys(precentageText, percentageVal);
	Thread.sleep(2000);
	selectIndex(costAssignmentTypeNad, 1);
	Thread.sleep(2000);
	click(costObjectSearch);
	Thread.sleep(2000);
	sendKeys(costObjectText, costObjectVal);
	Thread.sleep(2000);
	pressEnter(costObjectText);
	Thread.sleep(2000);
	doubleClick(costObjectSelected);
	Thread.sleep(2000);
	click(addRecord);
	Thread.sleep(2000);
	click(updateCostAllocation);
	Thread.sleep(2000);
	click(checkout);
	Thread.sleep(2000);
	click(draftNad);
	Thread.sleep(5000);
	click(releaseNad);
	Thread.sleep(6000);
	bannerTot = getText(bannerTotInvoice);

	InvoiceHeaderVal = getText(accrualHeader);
	System.out.println(InvoiceHeaderVal);

	Thread.sleep(4000);
	String header = getText(releaseInHeader);
	
	trackCode = getText(documentVal);
	Thread.sleep(5000);
	if (header.equals("(Released)")) {
		writeTestResults("User should be able to release the purchase invoice",
				"Purchase invoice should be released", "Purchase invoice is released", "Pass");

	} else {
		writeTestResults("User should be able to release the purchase invoice",
				"Purchase invoice should be released", "Purchase invoice is not released", "Fail");
	}

}

//Verify that expenses recorded from the purchase invoice shows in project
//costing summary -- 52

public void getValueExpenseInvoiceBefore() throws Exception {

	click(project);
	Thread.sleep(2000);
	sendKeys(projectSearchText, projectVal);
	Thread.sleep(2000);
	pressEnter(projectSearchText);
	Thread.sleep(2000);
	click(projectSelectedVal);
	Thread.sleep(2000);
	click(costingSummaryTab);
	Thread.sleep(2000);
	click(expenseArrow);

	expenseExistingValInv1 = getText(expenseExisting);
	System.out.println(expenseExistingValInv1);

}

public void getValueExpenseInvoiceAfter() throws Exception {

	click(project);
	Thread.sleep(2000);
	sendKeys(projectSearchText, projectVal);
	Thread.sleep(2000);
	pressEnter(projectSearchText);
	Thread.sleep(2000);
	click(projectSelectedVal);
	Thread.sleep(2000);
	click(costingSummaryTab);
	Thread.sleep(2000);
	click(expenseArrow);

	expenseExistingValInv2 = getText(expenseExisting);
	System.out.println(expenseExistingValInv2);

}

public void checkCostingPurchaseInvoice() throws Exception {

	getValueExpenseInvoiceAfter();
	Thread.sleep(2000);
	String val1 = expenseExistingValInv1;
	// System.out.println(val1);
	System.out.println(expenseExistingValInv2);

	String val3 = val1.replace(",", "");
	// System.out.println(val3);
	String val4 = val3.substring(0, val3.length() - 6);
	// System.out.println(val4);

	int val5 = Integer.parseInt(val4);
	System.out.println(val5);

	String val22 = expenseExistingValInv2;
	// System.out.println(val22);
	String val33 = val22.replace(",", "");
	// System.out.println(val33);

	String val44 = val33.substring(0, val33.length() - 6);
	// System.out.println(val44);

	int val55 = Integer.parseInt(val44);

	// banner Total
	String banner = bannerTot;
	String banner1 = banner.replace(",", "");
	String banner2 = banner1.substring(0, banner1.length() - 6);
	// System.out.println(banner2);

	int ban2 = Integer.parseInt(banner2);

	int tot = val5 + ban2;
	// System.out.println(tot);

	if (tot == (val55)) {
		writeTestResults("Verify that expenses recorded from the purchase invoice shows in project costing summary",
				"User should be able to see the updated values in project overview",
				"Values are updated correctly in overview", "Pass");
	} else {
		writeTestResults("Verify that expenses recorded from the purchase invoice shows in project costing summary",
				"User should be able to see the updated values in project overview",
				"Values are not updated correctly in overview", "Fail");
	}
}

//Verify that expenses recorded from the purchase invoice shows in project
//overview -- 53
public void checkOverviewInvoice() throws Exception {

	click(project);
	Thread.sleep(3000);
	sendKeys(projectSearchText, projectVal);
	Thread.sleep(2000);
	pressEnter(projectSearchText);
	Thread.sleep(2000);
	click(projectSelectedVal);
	Thread.sleep(2000);
	click(overviewNad);
	Thread.sleep(2000);
	click(expenses);
	Thread.sleep(2000);
	// System.out.println(invoiceValue);
	// System.out.println(InvoiceHeaderVal);

	String invoice = invoiceValue.replace("LCP/001934", InvoiceHeaderVal);
	// System.out.println(invoice);

	String cus = getText(invoice);
	// System.out.println(cus);

	if (cus.equals(bannerTot)) {
		writeTestResults("Verify that expenses recorded from the purchase invoice shows in project overview",
				"User should be able to see the released purchase invoice details under expenses",
				"Released purchase invoice record available in the overview with correct values", "Pass");
	} else {
		writeTestResults("Verify that expenses recorded from the purchase invoice shows in project overview",
				"User should be able to see the released purchase invoice details under expenses",
				"Released purchase invoice record not available in the overview with correct values", "Fail");
	}
}

//Verify the ability to draft and release invoice proposal, service invoice to
//a project -- 54

public void invoiceProposal() throws Exception {

//	click(project);
//	Thread.sleep(2000);
//	sendKeys(projectSearchText, projectVal);
//	Thread.sleep(2000);
//	pressEnter(projectSearchText);
//	Thread.sleep(2000);
//	click(projectSelectedVal);
//	Thread.sleep(3000);
	click(editNa);
	Thread.sleep(2000);
	click(workBreakdown);
	Thread.sleep(2000);
	click(addTask);
	sendKeys(taskName, taskNameVal);
	Thread.sleep(1000);
	click(inputProducts);
	Thread.sleep(2000);
	click(searchProduct);
	Thread.sleep(2000);
	sendKeys(productText, productVal);
	Thread.sleep(2000);
	pressEnter(productText);
	Thread.sleep(3000);
	doubleClick(selectedVal);
	Thread.sleep(2000);
	// sendKeys(estimatedQty, value);
	click(billableCheck);
	Thread.sleep(2000);
	click(labourNa);
	Thread.sleep(2000);
	click(serviceProSearch);
	Thread.sleep(2000);
	sendKeys(serviceProText, serviceProTextVal);
	Thread.sleep(2000);
	pressEnter(serviceProText);
	Thread.sleep(3000);
	doubleClick(serviceSelected);
	Thread.sleep(3000);
	click(EstimatedWorkNew);
//	Thread.sleep(4000);
//	click(hoursResource);
//	Thread.sleep(4000);
//	click(minutesResource);
//	Thread.sleep(4000);
//	click(okBtn);
	Thread.sleep(4000);
	click(sel_EstimatedWorksel);
	click(btn_EstimatedWorkhourok);
	
	Thread.sleep(2000);
	click(billableResource);
	Thread.sleep(2000);
	click(resourceNad);
	Thread.sleep(2000);
	click(resourceSearch);
	Thread.sleep(2000);
	sendKeys(resourceText, resourceVal);
	Thread.sleep(2000);
	pressEnter(resourceText);
	Thread.sleep(3000);
	doubleClick(resourceSelected);
	Thread.sleep(2000);
	click(serviceSearchResource);
	Thread.sleep(2000);
	sendKeys(serviceTextResource, serviceTextResourceVal);
	Thread.sleep(2000);
	pressEnter(serviceTextResource);
	Thread.sleep(3000);
	doubleClick(serviceSelectedResource);
	Thread.sleep(2000);
	click(estimatedResouce);
	Thread.sleep(4000);
//	click(hoursResource);
//	Thread.sleep(4000);
//	click(minutesResource);
//	Thread.sleep(4000);
//	click(okResource);
	Thread.sleep(4000);
	click(sel_ResourceEstimatedWorksel);
	
	Thread.sleep(2000);
	click(billableResour);
	Thread.sleep(2000);
	click(updateNad);
	Thread.sleep(2000);
	click(expandArrow);
	Thread.sleep(2000);
	click(releaseTask);
	Thread.sleep(3000);
	click(updateHeader);
	
	Thread.sleep(3000);
	click(actionMenu);
	Thread.sleep(2000);
	click(invoiceProposal);
	Thread.sleep(4000);
	click(checkBoxNad);
	Thread.sleep(2000);
	click(checkoutInvoice);
	Thread.sleep(2000);
	click(applyNad);
	Thread.sleep(4000);
	switchWindow();
	click(close);
	Thread.sleep(2000);
	mouseMove(checkout);
	click(checkout);
	Thread.sleep(2000);
	click(draftNad);
	Thread.sleep(5000);

	String header = getText(releaseInHeader);
	trackCode = getText(documentVal);

	System.out.println(header);
	Thread.sleep(5000);

	if (isDisplayed(pageDraft)) {
		writeTestResults("User should be able to draft the invoice proposal", "Invoice proposal should be drafted",
				"Invoice proposal drafted", "Pass");

	} else {
		writeTestResults("User should be able to draft the invoice proposal", "Invoice proposal should be drafted",
				"Invoice proposal not drafted", "Fail");
	}
	Thread.sleep(3000);
	click(releaseNad);
	Thread.sleep(6000);

	if (isDisplayed(link)) {
		writeTestResults("User should be able to release the invoice proposal",
				"Invoice proposal should be released", "Invoice proposal is released", "Pass");
		click(link);
		Thread.sleep(5000);

	} else {
		writeTestResults("User should be able to release the invoice proposal",
				"Invoice proposal should be released", "Invoice proposal is not released", "Fail");
	}

	switchWindow();

	click(serviceDetails);
	Thread.sleep(2000);
	click(checkout);
	Thread.sleep(3000);
	click(draftNad);
	Thread.sleep(5000);
	click(releaseNad);
	Thread.sleep(8000);

	String header2 = getText(releaseInHeader);
	System.out.println(header2);
	trackCode = getText(documentVal);
	Thread.sleep(5000);
	if (header2.contentEquals("(Released)")) {
		writeTestResults("User should be able to release the service invoice", "Service invoice should be released",
				"Service invoice is released", "Pass");

	} else {
		writeTestResults("User should be able to release the service invoice", "Service invoice should be released",
				"Service invoice is not released", "Fail");
	}
}

//Verify the ability of drafting and releasing a project quotation with
//mandatory data -- 55

public void projectQuotation() throws Exception {

	click(projectQuatation);
	Thread.sleep(2000);
	click(newProjectQuatation);
	Thread.sleep(2000);
	click(searchCus);
	Thread.sleep(2000);
	sendKeys(customerTextQuat, customerTextQuatVal);
	Thread.sleep(2000);
	pressEnter(customerTextQuat);
	Thread.sleep(2000);
	doubleClick(selectedValCus);
	Thread.sleep(2000);
	selectIndex(salesUnit, 2);
	Thread.sleep(2000);
	click(productSearch);
	Thread.sleep(3000);
	sendKeys(productSearchText, productSearchTextVal);
	Thread.sleep(2000);
	pressEnter(productSearchText);
	Thread.sleep(3000);
	doubleClick(productSelectedVal);
	Thread.sleep(2000);
	click(addNewRecord);
	Thread.sleep(2000);
	selectIndex(productTypeDrop, 1);
	Thread.sleep(2000);
	click(referenceSearch);
	Thread.sleep(2000);
	sendKeys(referenceText, referenceTextVal);
	Thread.sleep(2000);
	pressEnter(referenceText);
	Thread.sleep(3000);
	doubleClick(referenceSelected);
	Thread.sleep(2000);
	click(productSearchLabour);
	Thread.sleep(2000);
	sendKeys(productTextLabour, productTextLabourVal);
	Thread.sleep(2000);
	pressEnter(productTextLabour);
	Thread.sleep(3000);
	doubleClick(serviceSelectedLabour);
	Thread.sleep(2000);
	click(addNewRecord2);
	Thread.sleep(2000);
	selectIndex(typeDrop, 2);
	Thread.sleep(2000);
	click(referenceSearchResource);
	Thread.sleep(2000);
	sendKeys(referenceTextResource, referenceTextResourceVal);
	Thread.sleep(2000);
	doubleClick(referenceResourceSelected);
	Thread.sleep(2000);
	click(resourceServiceSearch);
	Thread.sleep(2000);
	sendKeys(resourceServiceText, resourceServiceTextVal);
	Thread.sleep(2000);
	doubleClick(resourceServiceSelected);
	Thread.sleep(2000);
	click(checkout);
	Thread.sleep(2000);
	click(draftNad);
	Thread.sleep(5000);
	click(releaseNad);
	Thread.sleep(6000);
	quotationIDVal = getText(quotationID);
	// System.out.println(quotationIDVal);
	Thread.sleep(5000);
	String header = getText(releaseInHeader);
	trackCode = getText(documentVal);
	Thread.sleep(5000);
	if (header.equals("(Released)")) {
		writeTestResults("User should be able to release the project quotation",
				"Project quotation should be released", "Project quotation is released", "Pass");
	} else {
		writeTestResults("User should be able to release the project quotation",
				"Project quotation should be released", "Project quotation is not released", "Fail");
	}
}

//Verify the confirm action for a released project quotation -- 56
public void checkConfirmAction() throws Exception {

	click(projectQuatation);
	Thread.sleep(3000);
	// Thread.sleep(2000);
	// sendKeys(quotationSearchText, quotationIDVal);
	// Thread.sleep(2000);
	// pressEnter(quotationSearchText);
	// Thread.sleep(2000);
	doubleClick(selectedValquot);
	Thread.sleep(2000);
	click(version);
	Thread.sleep(2000);
	mouseMove(colorSelect);
	Thread.sleep(2000);
	click(tickVersion);
	Thread.sleep(2000);
	click(confirm);
	Thread.sleep(3000);
	quotationID = getText(quotationHeaderID);
	// System.out.println(quotationID);
}

//Verify that the confirmed project quotation can be converted into a project
//-- 57

public void convertToProject() throws Exception {

	click(projectQuatation);
	Thread.sleep(2000);
	sendKeys(quotationSearchText, quotationID);
	Thread.sleep(2000);
	pressEnter(quotationSearchText);
	Thread.sleep(2000);
	doubleClick(selectedValquot);
	Thread.sleep(2000);
	click(actionMenu);
	Thread.sleep(2000);
	click(convertToProject);
	Thread.sleep(6000);
	switchWindow();

	sendKeys(ProjectCodeNew, projectCodeVal);
	Thread.sleep(2000);
	sendKeys(titleText, titleVal);
	Thread.sleep(2000);
	selectText(projectGrp, "GSC-GENERATOR SALES");
	Thread.sleep(2000);
	click(searchResponsible);
	Thread.sleep(2000);
	sendKeys(responsiblePersonText, responsiblePersonVal);
	Thread.sleep(2000);
	pressEnter(responsiblePersonText);
	Thread.sleep(2000);
	doubleClick(responsiblePersonSelected);
	Thread.sleep(2000);
	click(searchPricing);
	Thread.sleep(3000);
	sendKeys(searchPricingText, pricingVal);
	Thread.sleep(2000);
	pressEnter(searchPricingText);
	Thread.sleep(2000);
	doubleClick(pricingSelected);
	Thread.sleep(2000);

	click(requestedStartDate);
	Thread.sleep(2000);
	click(requestedSDate);
	Thread.sleep(2000);
	click(requestedEndDate);
	Thread.sleep(2000);
	click(requestedEDate);
	Thread.sleep(2000);
	click(projectLocationSearch);
	Thread.sleep(2000);
	sendKeys(projectLocationText, locationVal);
	Thread.sleep(2000);
	pressEnter(projectLocationText);
	Thread.sleep(2000);
	doubleClick(projectLocationSelected);
	Thread.sleep(2000);
	selectText(inputWarehouse, "WH010 [Kegalle]");
	Thread.sleep(2000);
	selectText(wipWarehouse, "Soap-WIP [WIP warehouse]");
	Thread.sleep(2000);
	click(draftNad);
	Thread.sleep(4000);
	click(releaseNad);
	Thread.sleep(6000);

	String header = getText(releaseInHeader);
	trackCode = getText(documentVal);
	Thread.sleep(5000);
	if (header.equals("(Released)")) {
		writeTestResults("Verify that project quotation can be convert to project",
				"User should be able to convert created quotation to a project", "Quotation converted to a project",
				"Pass");
	} else {
		writeTestResults("Verify that project quotation can be convert to project",
				"User should be able to convert created quotation to a project", "Quotation converted to a project",
				"Fail");
	}
}

//smoke pro 3

public void addingTasksToProject() throws Exception {

	click(project);
	Thread.sleep(3000);
	sendKeys(projectSearchText, projectValTask);
	Thread.sleep(2000);
	pressEnter(projectSearchText);
	Thread.sleep(2000);
	click(projectSelectedVal);
	Thread.sleep(2000);

	click(editNa);
	Thread.sleep(2000);
	selectIndex(accountOwner, 1);
	click(workBreakdown);
	Thread.sleep(2000);
	click(addTask);
	Thread.sleep(2000);
	String taskNameInt = Integer.toString(generateRandomIntIntRange(1, 100));
	String taskNameVal = taskNameValue + taskNameInt;
	System.out.println(taskNameVal);

	sendKeys(taskName, taskNameVal);
	Thread.sleep(2000);
	click(inputProducts);
	Thread.sleep(2000);
	click(searchProduct);
	Thread.sleep(3000);
	sendKeys(productText, productVal);
	Thread.sleep(2000);
	pressEnter(productText);
	Thread.sleep(3000);
	doubleClick(selectedVal);
	// sendKeys(estimatedQty, value);
	Thread.sleep(2000);
	click(billableCheck);
	Thread.sleep(2000);
	click(updateNad);
	Thread.sleep(2000);
	click(expandArrow);
	Thread.sleep(2000);

	String draft = draftCheck.replace("repair elevator", taskNameVal);

	if (isElementPresent(draft)) {
		writeTestResults("Veify that add task loading", "Added task shold be available", "Task is displayed",
				"Pass");
	} else {
		writeTestResults("Veify that add task loading", "Added task shold be available", "Task is not displayed",
				"Fail");
	}

	String release = releaseRelavantTask.replace("abcd", taskNameVal);
	click(release);
	Thread.sleep(2000);
	click(updateHeader);
	Thread.sleep(2000);
	click(workBreakdown);
	Thread.sleep(2000);
	click(expandArrow);
	Thread.sleep(2000);
	String val = taskStatus.replace("repair elevator", taskNameVal);
	String val1 = getText(val);
	System.out.println(val1);

	if (val1.equals("Released")) {
		writeTestResults("Verify the ability of completing tasks", "User should be able to release the tasks",
				"Task completed", "Pass");

	} else {
		writeTestResults("Verify the ability of adding tasks to a project and completing tasks",
				"User should be able to release the tasks", "Task not completed", "Fail");
	}
}

//smoke pro 4
public void createcostEstimation() throws Exception {

	click(projectCostEstimation);
	Thread.sleep(3000);
	click(newProjectCostEstimation);
	Thread.sleep(3000);
	sendKeys(descriptionCostEsti, descriptionValCostEsti);
	Thread.sleep(2000);
	click(pricingProfileSearch);
	Thread.sleep(2000);
	sendKeys(pricingProText, "PROFILE1");
	Thread.sleep(2000);
	pressEnter(pricingProText);
	Thread.sleep(2000);
	doubleClick(pricingProSelected);
	Thread.sleep(2000);

	click(estimationDetails);
	Thread.sleep(2000);
	click(inputProductSearch);
	Thread.sleep(2000);
	sendKeys(productSearchText, productSearchTextVal);
	Thread.sleep(2000);
	pressEnter(productSearchText);
	Thread.sleep(3000);
	doubleClick(productSelectedVal);
	Thread.sleep(2000);
	// System.out.println(getText(inputProductText));
	click(inputProductMenu);
	Thread.sleep(2000);
	click(productRelatedPrice);
	String productVal = getText(lastPurchasePrice);
	System.out.println(productVal);
	click(closeWindow);
	Thread.sleep(2000);
	click(billableInputProduct);

	click(addNewLabour);
	Thread.sleep(2000);
	selectIndex(typeDropdownLabour, 1);
	Thread.sleep(2000);
	click(LabourProductSearch);
	Thread.sleep(2000);
	sendKeys(resourceServiceText, resourceServiceTextVal);
	Thread.sleep(2000);
	pressEnter(resourceServiceText);
	Thread.sleep(3000);
	doubleClick(resourceServiceSelected);
	Thread.sleep(2000);
	click(billableLabour);
	Thread.sleep(2000);

	click(addNewResource);
	Thread.sleep(2000);
	selectIndex(resourceDropdown, 2);
	Thread.sleep(2000);
	click(resourceProductSearch);
	Thread.sleep(2000);
	sendKeys(resourceServiceText, resourceServiceTextVal);
	Thread.sleep(2000);
	pressEnter(resourceServiceText);
	Thread.sleep(3000);
	doubleClick(resourceServiceSelected);
	Thread.sleep(2000);
	click(resourceBillable);
	Thread.sleep(2000);

	click(addNewExpense);
	Thread.sleep(2000);
	selectIndex(dropDownExpense, 3);
	Thread.sleep(2000);
	click(expenseProductSearch);
	Thread.sleep(2000);
	sendKeys(resourceServiceText, resourceServiceTextVal);
	Thread.sleep(2000);
	pressEnter(resourceServiceText);
	Thread.sleep(3000);
	doubleClick(resourceServiceSelected);
	Thread.sleep(2000);
	sendKeys(expenseCostText, expenseVal);
	Thread.sleep(2000);
	click(billableExpense);
	Thread.sleep(2000);

	click(addNewService);
	Thread.sleep(2000);
	selectIndex(serviceDropdown, 4);
	Thread.sleep(2000);
	click(serviceProductSearch);
	Thread.sleep(2000);
	sendKeys(resourceServiceText, resourceServiceTextVal);
	Thread.sleep(2000);
	pressEnter(resourceServiceText);
	Thread.sleep(3000);
	doubleClick(resourceServiceSelected);
	Thread.sleep(2000);
	sendKeys(serviceCostText, serviceVal);
	click(billableService);
	Thread.sleep(2000);
	click(summaryTab);
	Thread.sleep(2000);

	click(checkout);
	Thread.sleep(2000);

	String inputPro = getText(inputProductEstimatedCost);
	String inputPro1 = inputPro.substring(0, inputPro.length() - 6);
	int inputPro2 = Integer.parseInt(inputPro1);
	// System.out.println(inputPro2);

	String productVal3 = productVal.replace("Last Purchase Price (LKR)", "");
	String productVal2 = productVal3.substring(0, productVal3.length() - 6);
	int productVal1 = Integer.parseInt(productVal2);

	String expense = getText(expenseEstimatedCost);
	String expense1 = expense.substring(0, expense.length() - 6);
	String expense2 = expense1.replace(",", "");
	int expense3 = Integer.parseInt(expense2);
	int expense4 = Integer.parseInt(expenseVal);
	// System.out.println(expense4);

	String service = getText(serviceProduct);
	String service1 = service.substring(0, service.length() - 6);
	int service2 = Integer.parseInt(service1);
	int service3 = Integer.parseInt(serviceVal);
	// System.out.println(service3);

	if (inputPro2 == productVal1 && expense3 == expense4 && service2 == service3) {
		writeTestResults("Verify the costing summary updating", "Costing summary should update accordingly",
				"Costing summary updated", "Pass");

	} else {
		writeTestResults("Verify the costing summary updating", "Costing summary should update accordingly",
				"Costing summary not updated", "Fail");
	}

	click(draftNad);
	Thread.sleep(4000);
	click(releaseNad);
	Thread.sleep(4000);

	String header = getText(releaseInHeader);
	trackCode = getText(documentVal);

	if (header.equals("(Released)")) {
		writeTestResults("User should be able to release the project cost estimation",
				"Project cost estimation should be released", "Project cost estimation is released", "Pass");

	} else {
		writeTestResults("User should be able to release the project cost estimation",
				"Project cost estimation should be released", "Project cost estimation is not released", "Fail");
	}
}

//smoke pro 5
public void createProjectModel() throws Exception {

	click(projectModel);
	Thread.sleep(3000);
	click(newProjectModel);
	Thread.sleep(3000);

	LocalTime myObj = LocalTime.now();
	String modelCode1 = "M" + myObj;

	sendKeys(proModelCode, modelCode1);
	sendKeys(proModelName, "One");

	selectText(warehouseInput, "WH010 [Kegalle]");
	selectText(warehouseWIP, "WIP-Warehouse-180 [WIP-Warehouse-180]");

	click(draftNad);
	Thread.sleep(4000);
	click(releaseNad);
	Thread.sleep(5000);

	String header = getText(releaseInHeader);
	trackCode = getText(documentVal);

	if (header.equals("(Released)")) {
		writeTestResults("User should be able to release the project model", "Project model should be released",
				"Project model is released", "Pass");

	} else {
		writeTestResults("User should be able to release the project model", "Project model should be released",
				"Project model is not released", "Fail");
	}

}

// smoke 6

public void createProjectSmoke6() throws Exception {
	
	click(btn_projectbutton);
	if (isDisplayed(projectpage)) {
		writeTestResults("verify project page", "view project page", "project page is display", "pass");
	} else {
		writeTestResults("verify project page", "view project page", "project page is not display", "fail");
	}
	click(btn_newprojectbutton);
	if (isDisplayed(newprojectpage)) {
		writeTestResults("verify new project page", "view new project page", "new project page is display", "pass");
	} else {
		writeTestResults("verify new project page", "view new project page", "new project page is not display",
				"fail");
	}
	LocalTime myObj = LocalTime.now();
	sendKeys(txt_ProjectCode, ProjectCode + myObj);
	Thread.sleep(2000);
	number1 = ProjectCode + myObj;
	System.out.println(number1);
	sendKeys(txt_Title, Title + myObj);
	selectIndex(txt_ProjectGroup, 1);
	click(btn_PricingProfilebutton);
	sendKeys(txt_PricingProfiletxt, PricingProfiletxt);
	Thread.sleep(3000);
	pressEnter(txt_PricingProfiletxt);
	Thread.sleep(3000);
	doubleClick(sel_PricingProfilesel);

	click(btn_ResponsiblePersonbutton);
	sendKeys(txt_ResponsiblePersontxt, ResponsiblePersontxt);
	pressEnter(txt_ResponsiblePersontxt);
	Thread.sleep(3000);
	doubleClick(sel_ResponsiblePersonsel);
	selectIndex(txt_AssignedBusinessUnits, 1);

	click(btn_CustomerAccountbutton);
	sendKeys(txt_CustomerAccounttxt, CustomerAccounttxt);
	pressEnter(txt_CustomerAccounttxt);
	Thread.sleep(3000);
	doubleClick(sel_CustomerAccountsel);
	click(btn_ProjectLocationbutton);
	sendKeys(txt_ProjectLocationtxt, ProjectLocationtxt);
	pressEnter(txt_ProjectLocationtxt);
	Thread.sleep(3000);
	doubleClick(sel_ProjectLocationsel);
	Thread.sleep(2000);
	click(txt_RequestedStartDate);
	Thread.sleep(2000);
	click(sel_RequestedStartDatesel);
	Thread.sleep(3000);
	click(btn_draft);
	Thread.sleep(3000);
	click(btn_release);

	Thread.sleep(3000);
	trackCode = getText("//label[@id='lblTemplateFormHeader']");
	if (isDisplayed("//label[@id='lbldocstatus']")) {

		writeTestResults("verify Project Document Released", "Project Document Released",
				"Project Document is Released", "pass");
	} else {
		writeTestResults("verify Project Document Released", "Project Document Released",
				"Project Document is not Released", "fail");

	}
}

public void addingAprojectTaskSmoke6() throws Exception {
	Thread.sleep(4000);
	click(btn_edit);
	click(btn_WorkBreakdown);
	click(btn_AddTask);
	LocalTime myObj = LocalTime.now();
	sendKeys(txt_TaskName, TaskName + myObj);
	selectIndex(txt_TaskType, 2);
	click(btn_InputProducts);
	Thread.sleep(2000);
	click(btn_productbutton);
	Thread.sleep(3000);
	sendKeys(txt_producttxt, producttxt);
	Thread.sleep(3000);
	pressEnter(txt_producttxt);
	Thread.sleep(3000);
	doubleClick(sel_productsel);
	Thread.sleep(2000);
	sendKeys(txt_EstimatedQty, EstimatedQty);

	click(btn_Labour);
	Thread.sleep(3000);
	click(btn_ServiceProductbutton);
	Thread.sleep(3000);
	sendKeys(txt_ServiceProducttxt, ServiceProducttxt);
	Thread.sleep(3000);
	pressEnter(txt_ServiceProducttxt);
	Thread.sleep(3000);
	doubleClick(sel_ServiceProductsel);
	click(txt_EstimatedWork);
	Thread.sleep(3000);
	number4 = getText(sel_EstimatedWorksel);
	Thread.sleep(3000);
	number6 = Integer.parseInt(number4);
	click(sel_EstimatedWorksel);
	click(btn_EstimatedWorkhourok);

	click(btn_Resource);
	Thread.sleep(3000);
	click(btn_Resourcebutton);
	Thread.sleep(3000);
	sendKeys(txt_Resourcetxt, Resourcetxt);
	Thread.sleep(3000);
	pressEnter(txt_Resourcetxt);
	Thread.sleep(3000);
	doubleClick(sel_Resourcesel);
	Thread.sleep(3000);
	click(btn_ResourceServiceProductbutton);
	Thread.sleep(3000);
	sendKeys(txt_ServiceProducttxt, ServiceProducttxt);
	Thread.sleep(3000);
	pressEnter(txt_ServiceProducttxt);
	Thread.sleep(3000);
	doubleClick(sel_ServiceProductsel);
	click(txt_ResourceEstimatedWork);
	Thread.sleep(3000);
	number10 = getText(sel_ResourceEstimatedWorksel);
	Thread.sleep(3000);
	number11 = Integer.parseInt(number10);
	Thread.sleep(3000);
	click(sel_ResourceEstimatedWorksel);

	click(btn_Expense);
	Thread.sleep(1000);
	selectIndex(txt_AnalysisCode, 2);
	Thread.sleep(3000);
	click(btn_ExpenseServiceProductbutton);
	Thread.sleep(3000);
	sendKeys(txt_ServiceProducttxt, ServiceProducttxt);
	Thread.sleep(3000);
	pressEnter(txt_ServiceProducttxt);
	Thread.sleep(3000);
	doubleClick(sel_ServiceProductsel);
	Thread.sleep(2000);
	sendKeys(txt_EstimatedCost, EstimatedCost);
	Thread.sleep(1000);
	click(btn_TaskUpdate);
//	Thread.sleep(3000);
//	click(btn_releaseTaskdropdown);
//	Thread.sleep(3000);
//	click(btn_releaseTask);
//	Thread.sleep(3000);
//	click(btn_update);
	pageRefersh();
	Thread.sleep(3000);
	click(btn_WorkBreakdown);
	Thread.sleep(6000);
	click(btn_releaseTaskdropdown);
	Thread.sleep(4000);
	click(btn_releaseTask);
	pageRefersh();
	Thread.sleep(6000);
	click(btn_update);
	Thread.sleep(3000);
}

public void addingSubContractTypeSmoke6() throws Exception {

	click(btn_edit);
	Thread.sleep(2000);
	click(btn_WorkBreakdown);
	Thread.sleep(1000);
	click(btn_AddTask);
	Thread.sleep(1000);
	LocalTime myObj = LocalTime.now();
	sendKeys(txt_TaskName, TaskName + myObj);
	selectIndex(txt_TaskType, 2);
	Thread.sleep(1000);
	selectIndex(txt_ContractTypeA, 1);
	Thread.sleep(1000);
	click(btn_SubContractServicesA);
	Thread.sleep(1000);
	click(btn_SubproductbuttonA);
	Thread.sleep(2000);
	sendKeys(txt_ServiceProducttxt, ServiceProducttxt);
	Thread.sleep(2000);
	pressEnter(txt_ServiceProducttxt);
	Thread.sleep(2000);
	doubleClick(sel_ServiceProductsel);
	Thread.sleep(2000);
	sendKeys(txt_SubEstimatedCostA, EstimatedCost);
	Thread.sleep(2000);
	number3 = EstimatedCost + ".00000";
	Thread.sleep(3000);
	pressEnter(txt_SubEstimatedCostA);
	Thread.sleep(3000);
	click(btn_BillableA);
	JavascriptExecutor js1 = (JavascriptExecutor) driver;
	js1.executeScript("$(\"#tblSADSVProduct\").children().find(\".unchecked\").click()");

	Thread.sleep(2000);
	click(btn_TaskUpdate);
//	Thread.sleep(2000);
//	click(btn_releaseTaskdropdown);
	pageRefersh();
	Thread.sleep(3000);
	click(btn_WorkBreakdown);
	Thread.sleep(6000);
	click(btn_releaseTaskdropdown);
	Thread.sleep(2000);
	click(btn_releaseTaskSubcontract);
//	Thread.sleep(3000);
//	click(btn_update);
//	Thread.sleep(3000);
	pageRefersh();
	Thread.sleep(6000);
	click(btn_update);
	Thread.sleep(3000);
}

public void addInterdepartmentTypeSmoke6() throws Exception {

	Thread.sleep(4000);
	click(btn_edit);
	Thread.sleep(2000);
	click(btn_WorkBreakdown);
	Thread.sleep(2000);
	click(btn_AddTask);
	LocalTime myObj = LocalTime.now();
	sendKeys(txt_TaskName, TaskName + myObj);
	selectIndex(txt_TaskType, 2);
	Thread.sleep(1000);
	selectIndex(txt_ContractTypeA, 2);
	Thread.sleep(1000);
	click(btn_InterDepartmentA);
	Thread.sleep(1000);
	click(btn_OwnerbuttonA);
	Thread.sleep(1000);
	sendKeys(txt_OwnertxtA, OwnertxtA);
	Thread.sleep(3000);
	pressEnter(txt_OwnertxtA);
	Thread.sleep(3000);
	doubleClick(sel_OwnerselA);
	Thread.sleep(1000);
	sendKeys(txt_EstimatedCostA, EstimatedCost);
	Thread.sleep(3000);
	click(txt_EstimatedDateA);
	Thread.sleep(2000);
	click(sel_EstimatedDateselA);
	Thread.sleep(2000);
	click(btn_TaskUpdate);
//	Thread.sleep(3000);
//	click(btn_releaseTaskdropdown);
	pageRefersh();
	Thread.sleep(3000);
	click(btn_WorkBreakdown);
	Thread.sleep(6000);
	click(btn_releaseTaskdropdown);
	Thread.sleep(2000);
	click(btn_releaseTaskInterDepartment);
//	Thread.sleep(4000);
//	click(btn_update);
//	Thread.sleep(3000);
	pageRefersh();
	Thread.sleep(6000);
	click(btn_update);
	Thread.sleep(3000);
}

public void internalOrderSmoke6() throws Exception {

	click(btn_Action);
	Thread.sleep(2000);
	click(btn_CreateInternalOrder);
	Thread.sleep(2000);
	selectIndex(btn_Taskdropdown, 2);
	Thread.sleep(2000);
	JavascriptExecutor jse = (JavascriptExecutor) driver;
	jse.executeScript("$('#comOrderTbl tbody').find('.unchecked').click()");
	Thread.sleep(2000);
	click(btn_Apply);
	Thread.sleep(3000);
	switchWindow();
	Thread.sleep(3000);
	click(btn_draft);
	Thread.sleep(4000);
	click(btn_release);
	Thread.sleep(4000);
	click(btn_Gotopage);
	Thread.sleep(4000);
	switchWindow();
	Thread.sleep(3000);
	click(btn_draft);
	Thread.sleep(4000);
	click(btn_release);
	Thread.sleep(4000);
	closeWindow();
	Thread.sleep(2000);
	switchWindow();
}

public void subContractOrderSmoke6() throws Exception {

	Thread.sleep(3000);
	click(btn_Action);
	Thread.sleep(2000);
	click(btn_SubContractOrderA);
	Thread.sleep(4000);
	selectIndex(txt_subTaskA, 1);
	Thread.sleep(8000);
	//click(btn_subVendorbuttonA);
	JavascriptExecutor jse = (JavascriptExecutor) driver;
	jse.executeScript("$('#txtVenSub').nextAll().find('.pic16-search').click()");
	Thread.sleep(2000);
	sendKeys(txt_subVendortxtA, subVendortxtA);
	Thread.sleep(2000);
	pressEnter(txt_subVendortxtA);
	Thread.sleep(2000);
	doubleClick(sel_subVendorselA);
	Thread.sleep(2000);
	click(btn_subcheckboxA);
	Thread.sleep(2000);
	click(btn_Apply);
	Thread.sleep(3000);
	switchWindow();
	Thread.sleep(2000);

	if (isDisplayed(PurchaseOrderpageA)) {

		writeTestResults("verify Purchase Order page", "view Purchase Order page", "Purchase Order page is display",
				"pass");
	} else {
		writeTestResults("verify Purchase Order page", "view Purchase Order page",
				"Purchase Order page is not dislayed", "fail");

	}
	LocalTime myObj = LocalTime.now();
	sendKeys(txt_VendorReferenceNoA, VendorReferenceNotxtA + myObj);
	click(btn_CheckoutA);
	Thread.sleep(3000);
	click(btn_draft);
	Thread.sleep(4000);
	click(btn_release);
	Thread.sleep(4000);

	trackCode = getText("//label[@id='lblTemplateFormHeader']");
	PONo = trackCode;

	if (isDisplayed(btn_Gotopage)) {

		writeTestResults("verify Purchase Order Document Released", "Purchase Order Document Released",
				"Purchase Order Document is Released", "pass");
	} else {

		writeTestResults("verify Purchase Order Document Released", "Purchase Order Document Released",
				"Purchase Order Document is not Released", "fail");

	}

	click(btn_Gotopage);
	Thread.sleep(3000);

	switchWindow();
	Thread.sleep(3000);
	if (isDisplayed(PurchaseInvoicepageA)) {

		writeTestResults("verify Purchase Invoice page", "view Purchase Invoice page",
				"Purchase Invoice page is display", "pass");
	} else {
		writeTestResults("verify Purchase Invoice page", "view Purchase Invoice page",
				"Purchase Invoice page is not dislayed", "fail");

	}

	click(btn_CheckoutA);
	Thread.sleep(3000);
	click(btn_draft);
	Thread.sleep(4000);
	click(btn_release);
	Thread.sleep(4000);
	trackCode = getText("//label[@id='lblTemplateFormHeader']");
	Thread.sleep(3000);
	INo = trackCode;

	if (isDisplayed("//label[@id='lbldocstatus']")) {

		writeTestResults("verify Purchase Invoice Document Released", "Purchase Invoice Document Released",
				"Purchase Invoice Document is Released", "pass");
	} else {
		writeTestResults("verify Purchase Invoice Document Released", "Purchase Invoice Document Released",
				"Purchase Invoice Document is not Released", "fail");

	}

	Thread.sleep(2000);
	closeWindow();
	Thread.sleep(2000);
	switchWindow();
	Thread.sleep(2000);
	closeWindow();
	Thread.sleep(2000);
	switchWindow();
	Thread.sleep(2000);

}

public void actualUpdateSmoke6() throws Exception {

	click(btn_Action);
	Thread.sleep(2000);
	click(btn_ActualUpdate);
	Thread.sleep(2000);
	click(btn_ActionResource);
	Thread.sleep(2000);
	click(btn_ActualWork);
	Thread.sleep(2000);
	TimeActual = getText(btn_ActualWorksel);
	Thread.sleep(2000);
	click(btn_ActualWorksel);
	Thread.sleep(2000);
	click(btn_ActualWorkOK);
	Thread.sleep(2000);
	click(btn_ActionUpdate);
//	Thread.sleep(3000);
//	click(btn_ActualUpdateclose);
	pageRefersh();
	Thread.sleep(4000);

}
// Verify the ability to do cost allocation for project tasks using accrual
// voucher--30

public void costAllocationSmoke() throws Exception {

	click("//a[@class='close']");
	Thread.sleep(2000);
	click(outboundPaymentAdvice);
	Thread.sleep(2000);
	click(newOutboundPaymentAdvice);
	Thread.sleep(2000);
	click(accuralVoucher);
	Thread.sleep(2000);
	click(vendorSearch);
	Thread.sleep(3000);
	sendKeys(vendorText, vendorVal);
	Thread.sleep(2000);
	pressEnter(vendorText);
	Thread.sleep(2000);
	doubleClick(vendorSelectedVal);
	Thread.sleep(2000);
	selectText(analysisCodeDrop, analysisCodeVal);
	Thread.sleep(2000);
	sendKeys(amountText, amountVal);
	Thread.sleep(2000);
	click(costAllocationBtn);
	Thread.sleep(2000);
	sendKeys(precentageText, percentageVal);
	Thread.sleep(2000);
	selectIndex(costAssignmentTypeNad, 1);
	Thread.sleep(2000);
	click(costObjectSearch);
	Thread.sleep(2000);
	sendKeys(costObjectText, number1);
	Thread.sleep(2000);
	pressEnter(costObjectText);
	Thread.sleep(2000);
	doubleClick(costObjectSelected);
	Thread.sleep(2000);
	click(addRecord);
	Thread.sleep(2000);
	click(updateCostAllocation);
	Thread.sleep(2000);

	sendKeys(vendorRef, vendorRefVal);
	Thread.sleep(2000);
	click(checkout);
	Thread.sleep(2000);

	if (isDisplayed(draftNad)) {
		writeTestResults("Verify that draft button is available on the page",
				"User should be able to see the draft button", "Draft button is available", "Pass");
		click(draftNad);
		Thread.sleep(4000);
	} else {
		writeTestResults("Verify that draft button is available on the page",
				"User should be able to see the draft button", "Draft button is not available", "Fail");
	}

	// click(draftNad);
	Thread.sleep(3000);
	accrualHeaderVal = getText(accrualHeader);
	 System.out.println(accrualHeaderVal);
	Thread.sleep(3000);
	click(releaseNad);
	Thread.sleep(5000);

	String header = getText(releaseInHeader);
	// System.out.println(header);

	// System.out.println(getText(documentVal));
	trackCode = getText(documentVal);
	Thread.sleep(5000);
	if (header.equals("( Released )")) {
		writeTestResults("User should be able to release the accrual voucher", "accrual voucher should be released",
				"Accrual voucher is released", "Pass");
	} else {
		writeTestResults("User should be able to release the accrual voucher", "accrual voucher should be released",
				"Accrual voucher is not released", "Fail");
	}
}

public void getValueExpenseBeforeSmoke() throws Exception {

	click(project);
	Thread.sleep(2000);
	sendKeys(projectSearchText, number1);
	Thread.sleep(2000);
	pressEnter(projectSearchText);
	Thread.sleep(2000);
	click(projectSelectedVal);
	Thread.sleep(2000);
	click(costingSummaryTab);
	Thread.sleep(2000);
	click(expenseArrow);

	expenseExistingValBef = getText(existingVal);
	System.out.println(expenseExistingValBef);

}

public static int generateRandomIntIntRangeSmoke(int min, int max) {
	Random r = new Random();
	return r.nextInt((max - min) + 1) + min;
}

// Verify that expenses recorded from the accrual voucher shows in project
// costing summary -- 31

public void getValueExpenseAfterSmoke() throws Exception {

	click(project);
	Thread.sleep(2000);
	sendKeys(projectSearchText, number1);
	Thread.sleep(2000);
	pressEnter(projectSearchText);
	Thread.sleep(2000);
	click(projectSelectedVal);
	Thread.sleep(2000);
	click(costingSummaryTab);
	Thread.sleep(2000);
	click(expenseArrow);

	expenseExistingValAft = getText(existingVal);
	System.out.println(expenseExistingValAft);

}

public void checkExpensesAccrualSmoke() throws Exception {

	getValueExpenseAfterSmoke();
	Thread.sleep(2000);
	// String expenBefore = expenseExistingValBef;
	// String expenseBefore1 = expenBefore.replace(",", "");
	// System.out.println(val2);
	// String expenseBefore2 = expenseBefore1.substring(0, expenseBefore1.length() -
	// 6);
	// System.out.println(val3);

	// int expenseBefore3 = Integer.parseInt(expenseBefore2);
	int amount = Integer.parseInt(amountVal);

	String expenseAft = expenseExistingValAft;
	String expenseAft1 = expenseAft.replace(",", "");
	// System.out.println(val22);
	String expenseAft2 = expenseAft1.substring(0, expenseAft1.length() - 6);
	System.out.println(expenseAft2);

	int expenseAft3 = Integer.parseInt(expenseAft2);
	// System.out.println(val44);

	// System.out.println(expenseBefore3 + amount);

	if (expenseAft3 == amount) {
		writeTestResults("Verify that actual cost updated in costing summary according to accrual voucher",
				"Expenses recorded from the accrual voucher should shows in project overview",
				"Values updated accordingly", "Pass");
	} else {
		writeTestResults("Verify that actual cost updated in costing summary according to accrual voucher",
				"Expenses recorded from the accrual voucher should shows in project overview",
				"Values not updated accordingly", "Fail");
	}
}

// Verify that expenses recorded from the accrual voucher shows in project
// overview -- 32
public void checkOverViewAccrualSmoke() throws Exception {

	click(project);
	Thread.sleep(3000);
	sendKeys(projectSearchText, number1);
	Thread.sleep(2000);
	pressEnter(projectSearchText);
	Thread.sleep(2000);
	click(projectSelectedVal);
	Thread.sleep(2000);
	click(overviewNad);
	Thread.sleep(2000);
	click(expenses);
	Thread.sleep(2000);
	String remove = accrualHeaderVal.replace(" ", "");
	// System.out.println(remove);
	String remove1 = remove.replace("AccrualVoucher", "");
	// System.out.println(remove1);
	String accrual = valPre.replace("OPA/27571", remove1);
	// System.out.println(accrual);
	Thread.sleep(2000);
	String cus = getText(accrual);
	// System.out.println(cus);
	String cus1 = cus.substring(0, cus.length() - 6);
	System.out.println(cus1);
	System.out.println(amountVal);

	// String cus1 = cus.replace(",", "");

	if (cus1.equals(amountVal)) {
		writeTestResults("Verify that actual cost updated in overview according to accrual voucher",
				"Actual cost should be updated for expenses", "Values are equal", "Pass");
	} else {
		writeTestResults("Verify that actual cost updated in overview according to accrual voucher",
				"Actual cost should be updated for expenses", "Values are not equal", "Fail");
	}
}

public void VerifyThatIfAnInterDepartmentTypeOfTaskSmoke6() throws Exception {

	// VerifyTheAbilityOfAddingInterDepartmentTask();
	Thread.sleep(2000);
	clickNavigation();
	click(btn_serbutton);
	Thread.sleep(2000);
	click(btn_ServiceCalendarA);
	Thread.sleep(2000);
	
	if (isDisplayed(txt_interdepartmentjobA.replace("number", number1))) {
		System.out.println("inter department job is showing on the estimated date");
		writeTestResults("Verify that inter department job is showing on the estimated date",
				"inter department job is showing", "inter department job is showing on the estimated date", "pass");
	} else {
		System.out.println("inter department job is not showing on the estimated date");
		writeTestResults("Verify that inter department job is showing on the estimated date",
				"inter department job is showing", "inter department job is not showing on the estimated date",
				"fail");
	}
	Thread.sleep(2000);
}

public void VerifyTheAbilityOfDraftReleaseOfServiceJobOrderSmoke6() throws Exception {

	// VerifyTheAbilityOfCreatingServiceJobOrder();
	String serviceJob = txt_interdepartmentjobA.replace("number", number1);
	click(serviceJob);

	Thread.sleep(3000);
	selectText(txt_ServiceGroupA, "SEP-SCHINDLER ESCALATOR PAID SERVICE");
	Thread.sleep(2000);
//	click(btn_ServicePerformerbuttonA);
//	Thread.sleep(2000);
//	sendKeys(txt_ServicePerformertxtA, ServicePerformertxtA);
//	Thread.sleep(2000);
//	pressEnter(txt_ServicePerformertxtA);
//	Thread.sleep(2000);
//	doubleClick(sel_ServicePerformerselA);
	Thread.sleep(2000);
	selectIndex(txt_PriorityA, 3);
	Thread.sleep(2000);
	click(btn_PricingProfilebuttonA);
	Thread.sleep(2000);
	sendKeys(txt_PricingProfiletxtA, PricingProfiletxtA);
	Thread.sleep(2000);
	pressEnter(txt_PricingProfiletxtA);
	Thread.sleep(2000);
	doubleClick(sel_PricingProfileselA);
	Thread.sleep(2000);
	click(btn_ServiceLocationbuttonA);
	Thread.sleep(2000);
	sendKeys(txt_ServiceLocationtxtA, ProjectLocationtxt);
	Thread.sleep(2000);
	pressEnter(txt_ServiceLocationtxtA);
	Thread.sleep(2000);
	doubleClick(sel_ServiceLocationselA);
	Thread.sleep(2000);
	sendKeys(txt_OrderDescriptionA, OrderDescriptionA);
	Thread.sleep(2000);
	click(btn_draft);
	Thread.sleep(5000);
	click(btn_release);
	Thread.sleep(6000);
	trackCode = getText("//label[@id='lblTemplateFormHeader']");
	Thread.sleep(2000);
	serviceJobOrderCodeA = getText("//label[@id='lblTemplateFormHeader']");
	Thread.sleep(5000);
	if (isDisplayed("//label[@id='lbldocstatus']")) {
		writeTestResults("verify Service Job Order form Document Released",
				"Service Job Order form Document Released", "Service Job Order form Document is Released", "pass");
	} else {
		writeTestResults("verify Service Job Order form Document Released",
				"Service Job Order form Document Released", "Service Job Order form Document is not Released",
				"fail");
	}
}

public void VerifyTheAbilityOfCreatingServiceJobOrderSmoke6() throws Exception {
	// VerifyThatIfAnInterDepartmentTypeOfTask();
	Thread.sleep(2000);
	click(txt_interdepartmentjobA.replace("number", number1));
	Thread.sleep(2000);
	switchWindow();
	Thread.sleep(3000);
	
	if (isDisplayed(txt_ServiceJobOrderpageA)) {
		System.out.println("user is navigated to a new service job order form");
		writeTestResults("Verify whether user is navigated to a new service job order form",
				"navigated to a new service job order form", "user is navigated to a new service job order form",
				"pass");
	} else {
		System.out.println("user is not navigated to a new service job order form");
		writeTestResults("Verify whether user is navigated to a new service job order form",
				"navigated to a new service job order form",
				"user is not navigated to a new service job order form", "fail");
	}
}

public void VerifyWhetherPOCOfTheInterDepartmentTaskSmoke6() throws Exception {

	// VerifyThatExpensesRecordedFromTheServiceJobOrder();

	Thread.sleep(3000);
	clickNavigation();
	Thread.sleep(3000);
	click(btn_serbutton);
	Thread.sleep(2000);

	click(btn_ServiceJobOrderA);
	Thread.sleep(2000);
	sendKeys(txt_ServiceJobOrderserchA, serviceJobOrderCodeA);
	Thread.sleep(2000);
	pressEnter(txt_ServiceJobOrderserchA);
	Thread.sleep(2000);
	doubleClick(sel_ServiceJobOrderselA);

	Thread.sleep(2000);
	click(btn_Action);
	Thread.sleep(2000);
	click(btn_AddProductTagNoA);
	Thread.sleep(2000);
	click(btn_SpnSeriaA);
	Thread.sleep(3000);
	click(btn_SerialNosA);
	Thread.sleep(2000);
	click(btn_AddProductTagNoapplyA);

	Thread.sleep(2000);
	click(btn_Action);
	Thread.sleep(2000);
	click(btn_StopA);
	Thread.sleep(2000);
	click(btn_CompleteA);

	Thread.sleep(3000);
	clickNavigation();
	Thread.sleep(2000);
	click(btn_projbutton);
	Thread.sleep(2000);
	click(btn_projectbutton);
	Thread.sleep(2000);
	sendKeys(txt_projectcodeserch, number1);
	Thread.sleep(2000);
	pressEnter(txt_projectcodeserch);
	Thread.sleep(2000);
	doubleClick(sel_projectcodeserchsel);
//	Thread.sleep(3000);
//	click(btn_WorkBreakdown);
//	
//	Thread.sleep(3000);
//	click(btn_releaseTaskdropdown);
	pageRefersh();
	Thread.sleep(3000);
	click(btn_WorkBreakdown);
	Thread.sleep(6000);
	click(btn_releaseTaskdropdown);
	

	Thread.sleep(2000);
	if (getText(txt_POCA2).equals("100")) {
		System.out.println("correct value updated POC of the inter department task 100%");
		writeTestResults("Verify whether POC of the inter department task gets 100%",
				"POC of the inter department task gets 100%",
				"correct value updated POC of the inter department task 100%", "pass");
	} else {
		System.out.println("incorrect value updated POC of the inter department task 100%");
		writeTestResults("Verify whether POC of the inter department task gets 100%",
				"POC of the inter department task gets 100%",
				"incorrect value updated POC of the inter department task 100%", "fail");
	}

}


public void Master() throws Exception {

	navigateToTheLoginPage();
	verifyTheLogo();
	userLogin();
	clickNavigation();
	clickProjectbutton();
	
	Thread.sleep(3000);
	click(btn_ProjectParametersA);
	Thread.sleep(3000);
	click(btn_SaveA);
	Thread.sleep(3000);
	if(isDisplayed(txt_validatorA)) {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is Working", "pass");
	}else {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is not Working", "fail");
	}
}

public void SetUp1() throws Exception {
	navigateToTheLoginPage();
	verifyTheLogo();
	userLogin();
	clickNavigation();
	clickProjectbutton();
	
	Thread.sleep(3000);
	click(btn_ProjectGroupConfigurationA);
	Thread.sleep(3000);
	click(btn_NewConfigurationA);
	LocalTime myObj = LocalTime.now();
	Thread.sleep(3000);
	sendKeys(txt_ConfigCodeA, "ConfigCode"+myObj);
	
	
	
	Thread.sleep(3000);
	click(btn_AddProGroupA);
	Thread.sleep(3000);
	click(btn_AddNewProGroupA);
	Thread.sleep(3000);
	sendKeys(txt_AddNewProGrouptxtA, "ProGroup"+myObj);
	Thread.sleep(3000);
	click(btn_ProGroupUpdateA);
	Thread.sleep(3000);
	selectText(txt_ProGroupA, "ProGroup"+myObj);
	Thread.sleep(3000);
	click(btn_ProjectGroupConfigurationDraftA);
	
	Thread.sleep(4000);
	sendKeys(txt_ProjectGroupConfigurationSerchA, "ConfigCode"+myObj);
	Thread.sleep(4000);
	pressEnter(txt_ProjectGroupConfigurationSerchA);
	Thread.sleep(4000);
	driver.findElement(getLocator(txt_ProjectGroupConfigurationSerchA)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(txt_ProjectGroupConfigurationSerchA);
	
	Thread.sleep(3000);
	click(btn_ProjectGroupConfigurationEditA);
	Thread.sleep(3000);
	click(btn_ProjectGroupConfigurationUpdateA);
}

public void Transaction3() throws Exception {
	
	navigateToTheLoginPage();
	verifyTheLogo();
	userLogin();
	clickNavigation();
	clickProjectbutton();
	
	click(btn_projectbutton);
	if(isDisplayed(projectpage)) {
		writeTestResults("verify project page","view project page", "project page is display", "pass");
	}else {
		writeTestResults("verify project page","view project page", "project page is not display", "fail");
	}
	click(btn_newprojectbutton);
	if(isDisplayed(newprojectpage)) {
		writeTestResults("verify new project page","view new project page", "new project page is display", "pass");
	}else {
		writeTestResults("verify new project page","view new project page", "new project page is not display", "fail");
	}
	//================================================================================================================
	Thread.sleep(3000);
	click(btn_CopyFromA);
	Thread.sleep(3000);
	if(isDisplayed(txt_CopyFromTextA)) {
		writeTestResults("verify Copy From Button","Working Copy From Button", "Copy From Button is Working", "pass");
	}else {
		writeTestResults("verify Copy From Button","Working Copy From Button", "Copy From Button is not Working", "fail");
	}
	Thread.sleep(3000);
	click(btn_headercloseA);
	
	LocalTime myObj1 = LocalTime.now();
	sendKeys(txt_ProjectCode, ProjectCode+myObj1);
	Thread.sleep(4000);
	number1=ProjectCode+myObj1;
	sendKeys(txt_Title, Title+myObj1);
	selectIndex(txt_ProjectGroup, 1);
	click(btn_PricingProfilebutton);
	sendKeys(txt_PricingProfiletxt,PricingProfiletxt);
	Thread.sleep(4000);
	pressEnter(txt_PricingProfiletxt);
	Thread.sleep(4000);
	doubleClick(sel_PricingProfilesel);
	
	click(btn_ResponsiblePersonbutton);
	sendKeys(txt_ResponsiblePersontxt, ResponsiblePersontxt);
	pressEnter(txt_ResponsiblePersontxt);
	Thread.sleep(4000);
	doubleClick(sel_ResponsiblePersonsel);
	selectIndex(txt_AssignedBusinessUnits, 1);
	
	click(btn_CustomerAccountbutton);
	sendKeys(txt_CustomerAccounttxt, CustomerAccounttxt);
	pressEnter(txt_CustomerAccounttxt);
	Thread.sleep(4000);
	doubleClick(sel_CustomerAccountsel);
	click(btn_ProjectLocationbutton);
	sendKeys(txt_ProjectLocationtxt, ProjectLocationtxt);
	pressEnter(txt_ProjectLocationtxt);
	Thread.sleep(4000);
	doubleClick(sel_ProjectLocationsel);
	Thread.sleep(4000);
	click(txt_RequestedStartDate);
	Thread.sleep(4000);
	click(sel_RequestedStartDatesel);
	Thread.sleep(4000);
	click(btn_draft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Project Document Draft","Project Document Draft", "Project Document is Draft", "pass");
	} else {
		writeTestResults("verify Project Document Draft","Project Document Draft", "Project Document is not Draft", "fail");

	}
	
	
	Thread.sleep(3000);
	click(btn_DuplicateA);
	Thread.sleep(3000);
	if(getText(Status).contentEquals("New")) 
	{
		writeTestResults("verify Duplicate Button","Working Duplicate Button", "Duplicate Button is Working", "pass");
	}else {
		writeTestResults("verify Duplicate Button","Working Duplicate Button", "Duplicate Button is not Working", "fail");
	}
	
	//================================================================================================================
	
	LocalTime myObj2 = LocalTime.now();
	sendKeys(txt_ProjectCode, ProjectCode+myObj2);

	Thread.sleep(4000);
	click(txt_RequestedStartDate);
	Thread.sleep(4000);
	click(sel_RequestedStartDatesel);
	Thread.sleep(4000);
	click(btn_DraftAndNewA);
	Thread.sleep(3000);
	if(isDisplayed(btn_draft)) {
		writeTestResults("verify Draft And New Button","Working Draft And New Button", "Draft And New Button is Working", "pass");
	}else {
		writeTestResults("verify Draft And New Button","Working Draft And New Button", "Draft And New Button is not Working", "fail");
	}
	
	//===============================================================================================================
	Thread.sleep(4000);
	LocalTime myObj3 = LocalTime.now();
	sendKeys(txt_ProjectCode, ProjectCode+myObj3);
	Thread.sleep(4000);
	number1=ProjectCode+myObj3;
	sendKeys(txt_Title, Title+myObj3);
	selectIndex(txt_ProjectGroup, 1);
	click(btn_PricingProfilebutton);
	sendKeys(txt_PricingProfiletxt,PricingProfiletxt);
	Thread.sleep(4000);
	pressEnter(txt_PricingProfiletxt);
	Thread.sleep(4000);
	doubleClick(sel_PricingProfilesel);
	
	click(btn_ResponsiblePersonbutton);
	sendKeys(txt_ResponsiblePersontxt, ResponsiblePersontxt);
	pressEnter(txt_ResponsiblePersontxt);
	Thread.sleep(4000);
	doubleClick(sel_ResponsiblePersonsel);
	selectIndex(txt_AssignedBusinessUnits, 1);
	
	click(btn_CustomerAccountbutton);
	sendKeys(txt_CustomerAccounttxt, CustomerAccounttxt);
	pressEnter(txt_CustomerAccounttxt);
	Thread.sleep(4000);
	doubleClick(sel_CustomerAccountsel);
	click(btn_ProjectLocationbutton);
	sendKeys(txt_ProjectLocationtxt, ProjectLocationtxt);
	pressEnter(txt_ProjectLocationtxt);
	Thread.sleep(4000);
	doubleClick(sel_ProjectLocationsel);
	Thread.sleep(4000);
	click(txt_RequestedStartDate);
	Thread.sleep(4000);
	click(sel_RequestedStartDatesel);
	Thread.sleep(4000);
	click(btn_draft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Project Document Draft","Project Document Draft", "Project Document is Draft", "pass");
	} else {
		writeTestResults("verify Project Document Draft","Project Document Draft", "Project Document is not Draft", "fail");

	}
	
	Thread.sleep(3000);
	click(btn_Action);
	Thread.sleep(3000);
	click(btn_DeleteA);
	Thread.sleep(3000);
	if(click(btn_NoA)==true) {
		writeTestResults("verify Delete Button","Working Delete Button", "Delete Button is Working", "pass");
	}else {
		writeTestResults("verify Delete Button","Working Delete Button", "Delete Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_edit);
	Thread.sleep(3000);
	if(isDisplayed(btn_update)) {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is Working", "pass");
	}else {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_UpdateAndNewA);
	Thread.sleep(3000);
	if(isDisplayed(btn_draft)) {
		writeTestResults("verify Update And New Button","Working Update And New Button", "Update And New Button is Working", "pass");
	}else {
		writeTestResults("verify Update And New Button","Working Update And New Button", "Update And New Button is not Working", "fail");
	}
	
	//================================================================================================================
	
	LocalTime myObj4 = LocalTime.now();
	sendKeys(txt_ProjectCode, ProjectCode+myObj4);
	Thread.sleep(4000);
	number1=ProjectCode+myObj4;
	sendKeys(txt_Title, Title+myObj4);
	selectIndex(txt_ProjectGroup, 1);
	click(btn_PricingProfilebutton);
	sendKeys(txt_PricingProfiletxt,PricingProfiletxt);
	Thread.sleep(4000);
	pressEnter(txt_PricingProfiletxt);
	Thread.sleep(4000);
	doubleClick(sel_PricingProfilesel);
	
	click(btn_ResponsiblePersonbutton);
	sendKeys(txt_ResponsiblePersontxt, ResponsiblePersontxt);
	pressEnter(txt_ResponsiblePersontxt);
	Thread.sleep(4000);
	doubleClick(sel_ResponsiblePersonsel);
	selectIndex(txt_AssignedBusinessUnits, 1);
	
	click(btn_CustomerAccountbutton);
	sendKeys(txt_CustomerAccounttxt, CustomerAccounttxt);
	pressEnter(txt_CustomerAccounttxt);
	Thread.sleep(4000);
	doubleClick(sel_CustomerAccountsel);
	click(btn_ProjectLocationbutton);
	sendKeys(txt_ProjectLocationtxt, ProjectLocationtxt);
	pressEnter(txt_ProjectLocationtxt);
	Thread.sleep(4000);
	doubleClick(sel_ProjectLocationsel);
	Thread.sleep(4000);
	click(txt_RequestedStartDate);
	Thread.sleep(4000);
	click(sel_RequestedStartDatesel);
	Thread.sleep(4000);
	click(btn_draft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Project Document Draft","Project Document Draft", "Project Document is Draft", "pass");
	} else {
		writeTestResults("verify Project Document Draft","Project Document Draft", "Project Document is not Draft", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_edit);
	Thread.sleep(3000);
	if(isDisplayed(btn_update)) {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is Working", "pass");
	}else {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_update);
	Thread.sleep(3000);
	if(isDisplayed(btn_release)) {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is Working", "pass");
	}else {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is not Working", "fail");
	}
	
	Thread.sleep(4000);
	click(btn_release);
	
	Thread.sleep(4000);
	trackCode= getText(Header);
	Thread.sleep(5000);
	if (isDisplayed(pageRelease)){

		writeTestResults("verify Project Document Released","Project Document Released", "Project Document is Released", "pass");
	} else {
		writeTestResults("verify Project Document Released","Project Document Released", "Project Document is not Released", "fail");

	}
	Thread.sleep(3000);
	click(btn_Action);
	Thread.sleep(3000);
	click(btn_ActualUpdate);
	Thread.sleep(3000);
	if(isDisplayed("//label[@id='shopflorComlabl']")) {
		writeTestResults("verify Actual Update Button","Working Actual Update Button", "Actual Update Button is Working", "pass");
	}else {
		writeTestResults("verify Actual Update Button","Working Actual Update Button", "Actual Update Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	pageRefersh();
	
	Thread.sleep(3000);
	click(btn_Action);
	Thread.sleep(3000);
	click(btn_CreateInternalOrder);
	Thread.sleep(3000);
	if(isDisplayed(txt_CreateInternalOrdertxtA)) {
		writeTestResults("verify Create Internal Order Button","Working Create Internal Order Button", "Create Internal Order Button is Working", "pass");
	}else {
		writeTestResults("verify Create Internal Order Button","Working Create Internal Order Button", "Create Internal Order Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	pageRefersh();
	
	Thread.sleep(3000);
	click(btn_Action);
	Thread.sleep(3000);
	click(btn_ChangeRetentionA);
	Thread.sleep(3000);
	if(isDisplayed(txt_ChangeRetentiontxtA)) {
		writeTestResults("verify Change Retention Button","Working Change Retention Button", "Change Retention Button is Working", "pass");
	}else {
		writeTestResults("verify Change Retention Button","Working Change Retention Button", "Change Retention Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	pageRefersh();
	
	Thread.sleep(3000);
	click(btn_Action);
	Thread.sleep(3000);
	click(btn_GenerateCustomerAdvanceA);
	Thread.sleep(3000);
	if(isDisplayed(txt_GenerateCustomerAdvancetxtA)) {
		writeTestResults("verify Generate Customer Advance Button","Working Generate Customer Advance Button", "Generate Customer Advance Button is Working", "pass");
	}else {
		writeTestResults("verify Generate Customer Advance Button","Working Generate Customer Advance Button", "Generate Customer Advance Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	pageRefersh();
	
	Thread.sleep(3000);
	click(btn_Action);
	Thread.sleep(3000);
	click(btn_PendingApprovalA);
	Thread.sleep(3000);
	if(isDisplayed(txt_PendingApprovaltxtA)) {
		writeTestResults("verify Pending Approval Button","Working Pending Approval Button", "Pending Approval Button is Working", "pass");
	}else {
		writeTestResults("verify Pending Approval Button","Working Pending Approval Button", "Pending Approval Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	pageRefersh();
	
	Thread.sleep(3000);
	click(btn_Action);
	Thread.sleep(3000);
	click(btn_ProductRegistrationA);
	Thread.sleep(3000);
	if(isDisplayed(txt_ProductRegistrationtxtA)) {
		writeTestResults("verify Product Registration Button","Working Product Registration Button", "Product Registration Button is Working", "pass");
	}else {
		writeTestResults("verify Product Registration Button","Working Product Registration Button", "Product Registration Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	pageRefersh();
	
	Thread.sleep(3000);
	click(btn_Action);
	Thread.sleep(3000);
	click(btn_SubContractOrderA);
	Thread.sleep(3000);
	if(isDisplayed(txt_SubContractOrdertxtA)) {
		writeTestResults("verify Sub Contract Order Button","Working Sub Contract Order Button", "Sub Contract Order Button is Working", "pass");
	}else {
		writeTestResults("verify Sub Contract Order Button","Working Sub Contract Order Button", "Sub Contract Order Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	pageRefersh();
	
	Thread.sleep(3000);
	click(btn_Action);
	Thread.sleep(3000);
	click(btn_TaskDetailAllocationA);
	Thread.sleep(3000);
	if(isDisplayed(txt_TaskDetailAllocationtxtA)) {
		writeTestResults("verify Task Detail Allocation Button","Working Task Detail Allocation Button", "Task Detail Allocation Button is Working", "pass");
	}else {
		writeTestResults("verify Task Detail Allocation Button","Working Task Detail Allocation Button", "Task Detail Allocation Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	pageRefersh();
	
	Thread.sleep(3000);
	click(btn_Action);
	Thread.sleep(3000);
	click(btn_UpdateTaskDetailsA);
	Thread.sleep(3000);
	if(isDisplayed(txt_UpdateTaskDetailstxtA)) {
		writeTestResults("verify Update Task Details Button","Working Update Task Details Button", "Update Task Details Button is Working", "pass");
	}else {
		writeTestResults("verify Update Task Details Button","Working Update Task Details Button", "Update Task Details Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	pageRefersh();
	
	Thread.sleep(3000);
	click(btn_Action);
	Thread.sleep(3000);
	click(btn_UpdateTaskPOC);
	Thread.sleep(3000);
	if(isDisplayed(txt_UpdateTaskPOCtxtA)) {
		writeTestResults("verify Update Task POC Button","Working Update Task POC Button", "Update Task POC Button is Working", "pass");
	}else {
		writeTestResults("verify Update Task POC Button","Working Update Task POC Button", "Update Task POC Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	pageRefersh();
	
	Thread.sleep(3000);
	click(btn_Action);
	Thread.sleep(3000);
	click(btn_VendorAdvanceA);
	Thread.sleep(3000);
	if(isDisplayed(txt_VendorAdvancetxtA)) {
		writeTestResults("verify Vendor Advance Button","Working Vendor Advance Button", "Vendor Advance Button is Working", "pass");
	}else {
		writeTestResults("verify Vendor Advance Button","Working Vendor Advance Button", "Vendor Advance Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_ActualUpdateclose);
	
	Thread.sleep(3000);
	click(btn_Action);
	Thread.sleep(4000);
	click(btn_ReverseA);

	Thread.sleep(4000);
	JavascriptExecutor j7 = (JavascriptExecutor)driver;
	j7.executeScript("$(\"#txtrdcmnReverseDate\").datepicker(\"setDate\", new Date())");
	
	Thread.sleep(3000);
	click(btn_ReversebuttonA);
	Thread.sleep(3000);
	if(click(btn_YesA)==true) {
		writeTestResults("verify Reverse Button","Working Reverse Button", "Reverse Button is Working", "pass");
	}else {
		writeTestResults("verify Reverse Button","Working Reverse Button", "Reverse Button is not Working", "fail");
	}
}

public void SetUp2() throws Exception {
	
	navigateToTheLoginPage();
	verifyTheLogo();
	userLogin();
	clickNavigation();
	clickProjectbutton();
	
	click(projectModel);
	click(newProjectModel);
	
	//==============================================================================================================
	
	Thread.sleep(3000);
	click(btn_CopyFromA);
	Thread.sleep(3000);
	if(isDisplayed(txt_CopyFromTextPMA)) {
		writeTestResults("verify Copy From Button","Working Copy From Button", "Copy From Button is Working", "pass");
	}else {
		writeTestResults("verify Copy From Button","Working Copy From Button", "Copy From Button is not Working", "fail");
	}
	Thread.sleep(3000);
	click(btn_headercloseA);
	
	LocalTime myObj1 = LocalTime.now();
	Thread.sleep(3000);
	sendKeys(ProjectCodeNew, "PM001"+myObj1);
	Thread.sleep(3000);
	sendKeys(proModelName, "PMN001"+myObj1);
	
	Thread.sleep(3000);
	selectIndex(inputWarehouse, 1);
	Thread.sleep(3000);
	selectIndex(wipWarehouse, 2);
	
	Thread.sleep(3000);
	click(btn_DraftAndNewA);
	Thread.sleep(3000);
	if(isDisplayed(btn_draft)) {
		writeTestResults("verify Draft And New Button","Working Draft And New Button", "Draft And New Button is Working", "pass");
	}else {
		writeTestResults("verify Draft And New Button","Working Draft And New Button", "Draft And New Button is not Working", "fail");
	}
	//==============================================================================================================
	
	LocalTime myObj2 = LocalTime.now();
	Thread.sleep(3000);
	sendKeys(ProjectCodeNew, "PM001"+myObj2);
	Thread.sleep(3000);
	sendKeys(proModelName, "PMN001"+myObj2);
	
	Thread.sleep(3000);
	selectIndex(inputWarehouse, 1);
	Thread.sleep(3000);
	selectIndex(wipWarehouse, 2);
	
	Thread.sleep(4000);
	click(btn_draft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Project Model Document Draft","Project Model Document Draft", "Project Model Document is Draft", "pass");
	} else {
		writeTestResults("verify Project Model Document Draft","Project Model Document Draft", "Project Model Document is not Draft", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_edit);
	Thread.sleep(3000);
	if(isDisplayed(btn_update)) {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is Working", "pass");
	}else {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_UpdateAndNewA);
	Thread.sleep(3000);
	if(isDisplayed(btn_draft)) {
		writeTestResults("verify Update And New Button","Working Update And New Button", "Update And New Button is Working", "pass");
	}else {
		writeTestResults("verify Update And New Button","Working Update And New Button", "Update And New Button is not Working", "fail");
	}
	//==============================================================================================================
	
	LocalTime myObj3 = LocalTime.now();
	Thread.sleep(3000);
	sendKeys(ProjectCodeNew, "PM001"+myObj3);
	Thread.sleep(3000);
	sendKeys(proModelName, "PMN001"+myObj3);
	
	Thread.sleep(3000);
	selectIndex(inputWarehouse, 1);
	Thread.sleep(3000);
	selectIndex(wipWarehouse, 2);
	
	Thread.sleep(4000);
	click(btn_draft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Project Model Document Draft","Project Model Document Draft", "Project Model Document is Draft", "pass");
	} else {
		writeTestResults("verify Project Model Document Draft","Project Model Document Draft", "Project Model Document is not Draft", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_Action);
	Thread.sleep(3000);
	click(btn_DeleteA);
	Thread.sleep(3000);
	if(click(btn_NoA)==true) {
		writeTestResults("verify Delete Button","Working Delete Button", "Delete Button is Working", "pass");
	}else {
		writeTestResults("verify Delete Button","Working Delete Button", "Delete Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_edit);
	Thread.sleep(3000);
	if(isDisplayed(btn_update)) {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is Working", "pass");
	}else {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_update);
	Thread.sleep(3000);
	if(isDisplayed(btn_edit)) {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is Working", "pass");
	}else {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is not Working", "fail");
	}
	
	Thread.sleep(4000);
	click(btn_release);
	Thread.sleep(5000);
	if (isDisplayed(pageRelease)){

		writeTestResults("verify Project Model Document Release","Project Model Document Release", "Project Model Document is Release", "pass");
	} else {
		writeTestResults("verify Project Model Document Release","Project Model Document Release", "Project Model Document is not Release", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_Action);
	Thread.sleep(3000);
	click(btn_ReverseA);
	Thread.sleep(3000);
	click(btn_YesA);
	Thread.sleep(5000);
	if(getText(Status).contentEquals("(Reversed)")) {
		writeTestResults("verify Reverse Button","Working Reverse Button", "Reverse Button is Working", "pass");
	}else {
		writeTestResults("verify Reverse Button","Working Reverse Button", "Reverse Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_DuplicateA);
	Thread.sleep(3000);
	if(getText(Status).contentEquals("New")) 
	{
		writeTestResults("verify Duplicate Button","Working Duplicate Button", "Duplicate Button is Working", "pass");
	}else {
		writeTestResults("verify Duplicate Button","Working Duplicate Button", "Duplicate Button is not Working", "fail");
	}
	
}

public void Transaction1() throws Exception {
	
	navigateToTheLoginPage();
	verifyTheLogo();
	userLogin();
	clickNavigation();
	clickProjectbutton();
	
	click(projectCostEstimation);
	click(newProjectCostEstimation);
	
	//============================================================================================================
	Thread.sleep(3000);
	click(btn_CopyFromA);
	Thread.sleep(3000);
	if(isDisplayed(txt_CopyFromTextPCE)) {
		writeTestResults("verify Copy From Button","Working Copy From Button", "Copy From Button is Working", "pass");
	}else {
		writeTestResults("verify Copy From Button","Working Copy From Button", "Copy From Button is not Working", "fail");
	}
	Thread.sleep(3000);
	click(btn_headercloseA);
	
	//============================================================================================================
	Thread.sleep(3000);
	sendKeys(descriptionCostEsti, txt_OrderDescriptionA);
	
	Thread.sleep(3000);
	click(pricingProfileSearch);
	Thread.sleep(3000);
	sendKeys(pricingProText, PricingProfiletxt);
	Thread.sleep(3000);
	pressEnter(pricingProText);
	Thread.sleep(3000);
	doubleClick(pricingProSelected);
	
	click(btn_CheckoutA);
	
	Thread.sleep(3000);
	click(btn_DraftAndNewA);
	Thread.sleep(3000);
	if(isDisplayed(btn_draft)) {
		writeTestResults("verify Update And New Button","Working Update And New Button", "Update And New Button is Working", "pass");
	}else {
		writeTestResults("verify Update And New Button","Working Update And New Button", "Update And New Button is not Working", "fail");
	}
	//============================================================================================================
	Thread.sleep(3000);
	sendKeys(descriptionCostEsti, txt_OrderDescriptionA);
	
	Thread.sleep(3000);
	click(pricingProfileSearch);
	Thread.sleep(3000);
	sendKeys(pricingProText, PricingProfiletxt);
	Thread.sleep(3000);
	pressEnter(pricingProText);
	Thread.sleep(3000);
	doubleClick(pricingProSelected);
	
	click(btn_CheckoutA);
	
	Thread.sleep(4000);
	click(btn_draft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Project Cost Estimation Document Draft","Project Cost Estimation Document Draft", "Project Cost Estimation Document is Draft", "pass");
	} else {
		writeTestResults("verify Project Cost Estimation Document Draft","Project Cost Estimation Document Draft", "Project Cost Estimation Document is not Draft", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_Action);
	Thread.sleep(3000);
	click(btn_DeleteA);
	Thread.sleep(3000);
	if(click(btn_NoA)==true) {
		writeTestResults("verify Delete Button","Working Delete Button", "Delete Button is Working", "pass");
	}else {
		writeTestResults("verify Delete Button","Working Delete Button", "Delete Button is not Working", "fail");
	}
	

	Thread.sleep(3000);
	click(btn_edit);
	Thread.sleep(3000);
	if(isDisplayed(btn_update)) {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is Working", "pass");
	}else {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is not Working", "fail");
	}
	
	click(btn_CheckoutA);
	
	Thread.sleep(3000);
	click(btn_UpdateAndNewA);
	Thread.sleep(3000);
	if(isDisplayed(btn_draft)) {
		writeTestResults("verify Update And New Button","Working Update And New Button", "Update And New Button is Working", "pass");
	}else {
		writeTestResults("verify Update And New Button","Working Update And New Button", "Update And New Button is not Working", "fail");
	}
	
	//============================================================================================================
	
	Thread.sleep(3000);
	sendKeys(descriptionCostEsti, txt_OrderDescriptionA);
	
	Thread.sleep(3000);
	click(pricingProfileSearch);
	Thread.sleep(3000);
	sendKeys(pricingProText, PricingProfiletxt);
	Thread.sleep(3000);
	pressEnter(pricingProText);
	Thread.sleep(3000);
	doubleClick(pricingProSelected);
	
	click(btn_CheckoutA);
	
	Thread.sleep(4000);
	click(btn_draft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Project Cost Estimation Document Draft","Project Cost Estimation Document Draft", "Project Cost Estimation Document is Draft", "pass");
	} else {
		writeTestResults("verify Project Cost Estimation Document Draft","Project Cost Estimation Document Draft", "Project Cost Estimation Document is not Draft", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_edit);
	Thread.sleep(3000);
	if(isDisplayed(btn_update)) {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is Working", "pass");
	}else {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is not Working", "fail");
	}
	
	click(btn_CheckoutA);
	
	Thread.sleep(3000);
	click(btn_update);
	Thread.sleep(3000);
	if(isDisplayed(btn_edit)) {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is Working", "pass");
	}else {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is not Working", "fail");
	}
	
	Thread.sleep(4000);
	click(btn_release);
	Thread.sleep(5000);
	if (isDisplayed(pageRelease)){

		writeTestResults("verify Project Cost Estimation Document Release","Project Cost Estimation Document Release", "Project Cost Estimation Document is Release", "pass");
	} else {
		writeTestResults("verify Project Cost Estimation Document Release","Project Cost Estimation Document Release", "Project Cost Estimation Document is not Release", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_Action);
	Thread.sleep(3000);
	click(btn_ReverseA);
	Thread.sleep(3000);
	click(btn_YesA);
	Thread.sleep(5000);
	if(getText(Status).contentEquals("(Reversed)")) {
		writeTestResults("verify Reverse Button","Working Reverse Button", "Reverse Button is Working", "pass");
	}else {
		writeTestResults("verify Reverse Button","Working Reverse Button", "Reverse Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_DuplicateA);
	Thread.sleep(3000);
	if(getText(Status).contentEquals("New")) 
	{
		writeTestResults("verify Duplicate Button","Working Duplicate Button", "Duplicate Button is Working", "pass");
	}else {
		writeTestResults("verify Duplicate Button","Working Duplicate Button", "Duplicate Button is not Working", "fail");
	}
}

public void Transaction2() throws Exception {
	
	navigateToTheLoginPage();
	verifyTheLogo();
	userLogin();
	clickNavigation();
	clickProjectbutton();
	
	click(projectQuatation);
	click(newProjectQuatation);
	
	//============================================================================================================
	Thread.sleep(3000);
	click(btn_CopyFromA);
	Thread.sleep(3000);
	if(isDisplayed(txt_CopyFromTextPQ)) {
		writeTestResults("verify Copy From Button","Working Copy From Button", "Copy From Button is Working", "pass");
	}else {
		writeTestResults("verify Copy From Button","Working Copy From Button", "Copy From Button is not Working", "fail");
	}
	Thread.sleep(3000);
	click(btn_headercloseA);
	
	//============================================================================================================
	
	Thread.sleep(3000);
	click(searchCus);
	Thread.sleep(3000);
	sendKeys(customerTextQuat, customerTextQuatVal);
	Thread.sleep(3000);
	pressEnter(customerTextQuat);
	Thread.sleep(3000);
	doubleClick(selectedValCus);
	
	Thread.sleep(3000);
	selectIndex(salesUnit, 5);
	
	Thread.sleep(3000);
	click(productSearch);
	Thread.sleep(3000);
	sendKeys(productSearchText, producttxt);
	Thread.sleep(3000);
	pressEnter(productSearchText);
	Thread.sleep(3000);
	doubleClick(sel_productsel);
	
	click(btn_CheckoutA);
	
	Thread.sleep(3000);
	click(btn_DraftAndNewA);
	Thread.sleep(3000);
	if(isDisplayed(btn_draft)) {
		writeTestResults("verify Update And New Button","Working Update And New Button", "Update And New Button is Working", "pass");
	}else {
		writeTestResults("verify Update And New Button","Working Update And New Button", "Update And New Button is not Working", "fail");
	}
	//============================================================================================================
	
	Thread.sleep(3000);
	click(searchCus);
	Thread.sleep(3000);
	sendKeys(customerTextQuat, customerTextQuatVal);
	Thread.sleep(3000);
	pressEnter(customerTextQuat);
	Thread.sleep(3000);
	doubleClick(selectedValCus);
	
	Thread.sleep(3000);
	selectIndex(salesUnit, 5);
	
	Thread.sleep(3000);
	click(productSearch);
	Thread.sleep(3000);
	sendKeys(productSearchText, producttxt);
	Thread.sleep(3000);
	pressEnter(productSearchText);
	Thread.sleep(3000);
	doubleClick(sel_productsel);
	
	click(btn_CheckoutA);
	
	Thread.sleep(4000);
	click(btn_draft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Project Cost Estimation Document Draft","Project Cost Estimation Document Draft", "Project Cost Estimation Document is Draft", "pass");
	} else {
		writeTestResults("verify Project Cost Estimation Document Draft","Project Cost Estimation Document Draft", "Project Cost Estimation Document is not Draft", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_Action);
	Thread.sleep(3000);
	click(btn_DeleteA);
	Thread.sleep(3000);
	if(click(btn_NoA)==true) {
		writeTestResults("verify Delete Button","Working Delete Button", "Delete Button is Working", "pass");
	}else {
		writeTestResults("verify Delete Button","Working Delete Button", "Delete Button is not Working", "fail");
	}
	

	Thread.sleep(3000);
	click(btn_edit);
	Thread.sleep(3000);
	if(isDisplayed(btn_update)) {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is Working", "pass");
	}else {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is not Working", "fail");
	}
	
	click(btn_CheckoutA);
	
	Thread.sleep(3000);
	click(btn_UpdateAndNewA);
	Thread.sleep(3000);
	if(isDisplayed(btn_draft)) {
		writeTestResults("verify Update And New Button","Working Update And New Button", "Update And New Button is Working", "pass");
	}else {
		writeTestResults("verify Update And New Button","Working Update And New Button", "Update And New Button is not Working", "fail");
	}
	
	//============================================================================================================
	
	
	Thread.sleep(3000);
	click(searchCus);
	Thread.sleep(3000);
	sendKeys(customerTextQuat, customerTextQuatVal);
	Thread.sleep(3000);
	pressEnter(customerTextQuat);
	Thread.sleep(3000);
	doubleClick(selectedValCus);
	
	Thread.sleep(3000);
	selectIndex(salesUnit, 5);
	
	Thread.sleep(3000);
	click(productSearch);
	Thread.sleep(3000);
	sendKeys(productSearchText, producttxt);
	Thread.sleep(3000);
	pressEnter(productSearchText);
	Thread.sleep(3000);
	doubleClick(sel_productsel);
	
	click(btn_CheckoutA);
	
	Thread.sleep(4000);
	click(btn_draft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Project Cost Estimation Document Draft","Project Cost Estimation Document Draft", "Project Cost Estimation Document is Draft", "pass");
	} else {
		writeTestResults("verify Project Cost Estimation Document Draft","Project Cost Estimation Document Draft", "Project Cost Estimation Document is not Draft", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_edit);
	Thread.sleep(3000);
	if(isDisplayed(btn_update)) {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is Working", "pass");
	}else {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is not Working", "fail");
	}
	
	click(btn_CheckoutA);
	
	Thread.sleep(3000);
	click(btn_update);
	Thread.sleep(3000);
	if(isDisplayed(btn_edit)) {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is Working", "pass");
	}else {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is not Working", "fail");
	}
	
	Thread.sleep(4000);
	click(btn_release);
	Thread.sleep(5000);
	if (isDisplayed(pageRelease)){

		writeTestResults("verify Project Cost Estimation Document Release","Project Cost Estimation Document Release", "Project Cost Estimation Document is Release", "pass");
	} else {
		writeTestResults("verify Project Cost Estimation Document Release","Project Cost Estimation Document Release", "Project Cost Estimation Document is not Release", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_Action);
	Thread.sleep(3000);
	click(btn_ReverseA);
	Thread.sleep(3000);
	click(btn_YesA);
	Thread.sleep(5000);
	if(getText(Status).contentEquals("(Reversed)")) {
		writeTestResults("verify Reverse Button","Working Reverse Button", "Reverse Button is Working", "pass");
	}else {
		writeTestResults("verify Reverse Button","Working Reverse Button", "Reverse Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_DuplicateA);
	Thread.sleep(4000);
	if(getText(Header).contentEquals("New")) 
	{
		writeTestResults("verify Duplicate Button","Working Duplicate Button", "Duplicate Button is Working", "pass");
	}else {
		writeTestResults("verify Duplicate Button","Working Duplicate Button", "Duplicate Button is not Working", "fail");
	}
	
	click(btn_CheckoutA);
	
	Thread.sleep(4000);
	click(btn_draft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Project Cost Estimation Document Draft","Project Cost Estimation Document Draft", "Project Cost Estimation Document is Draft", "pass");
	} else {
		writeTestResults("verify Project Cost Estimation Document Draft","Project Cost Estimation Document Draft", "Project Cost Estimation Document is not Draft", "fail");
	}
	
	Thread.sleep(4000);
	click(btn_release);
	Thread.sleep(5000);
	if (isDisplayed(pageRelease)){

		writeTestResults("verify Project Cost Estimation Document Release","Project Cost Estimation Document Release", "Project Cost Estimation Document is Release", "pass");
	} else {
		writeTestResults("verify Project Cost Estimation Document Release","Project Cost Estimation Document Release", "Project Cost Estimation Document is not Release", "fail");
	}
	
	Thread.sleep(4000);
	click(version);
	Thread.sleep(4000);
	mouseMove("//div[@class='color-selected']");
	Thread.sleep(4000);
	click(tickVersion);
	Thread.sleep(4000);
	if(click(confirm)==true){
		writeTestResults("verify confirm Button","Working confirm Button", "confirm Button is Working", "pass");
	}else {
		writeTestResults("verify confirm Button","Working confirm Button", "confirm Button is not Working", "fail");
	}
	
	Thread.sleep(4000);
	click(btn_Action);
	Thread.sleep(4000);
	click(convertToProject);
	
	switchWindow();
	Thread.sleep(4000);
	if(isDisplayed(newprojectpage)){
		writeTestResults("verify convert To Project Button","Working convert To Project Button", "convert To Project Button is Working", "pass");
	}else {
		writeTestResults("verify convert To Project Button","Working convert To Project Button", "convert To Project Button is not Working", "fail");
	}
}

//scrollDown
public void ScrollDownAruna() throws Exception {
	JavascriptExecutor jse = (JavascriptExecutor)driver;
	jse.executeScript("window.scrollBy(0,400)");
}

//WaitClick
public void WaitClick(String Locator)  throws Exception {
	
	WebDriverWait wait = new WebDriverWait(driver,60);
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Locator)));
}

//WaitElement
public void WaitElement(String Locator)  throws Exception {
	
	WebDriverWait wait = new WebDriverWait(driver,60);
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator)));
}
}
