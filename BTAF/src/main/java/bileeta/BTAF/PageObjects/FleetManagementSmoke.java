package bileeta.BTAF.PageObjects;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import bileeta.BATF.Pages.FixedAssetDataSmoke;
import bileeta.BATF.Pages.FleetManagementModuleSmokeData;

import java.text.DateFormat;
import java.util.Date;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.time.LocalDate;

public class FleetManagementSmoke extends FleetManagementModuleSmokeData {
	private static Workbook wb;
	private static Sheet sh;
	private static FileInputStream fis;
	private static FileOutputStream fos;
	private static Row row;
	private static Cell cell;
	private static Cell cell1;
	private static Cell cell2;

	/* common method */
	public void writeFleetData(String name, String variable, int data_row) throws InvalidFormatException, IOException {
		genarateInventoryAndWarehouseInformation obj = new genarateInventoryAndWarehouseInformation();
		String path = obj.getProjectPath("*src*test*java*fleetModule*fleetData.xlsx");
		fis = new FileInputStream(path);
		wb = WorkbookFactory.create(fis);
		sh = wb.getSheet("Sheet1");
		/* int noOfRows = sh.getLastRowNum(); */
		/* System.out.println(noOfRows); */
		row = sh.createRow(data_row);
		cell = row.createCell(1);
		cell1 = row.createCell(0);
		cell.setCellValue(variable);
		cell1.setCellValue(name);
		System.out.println(cell.getStringCellValue());
		fos = new FileOutputStream(path);
		wb.write(fos);
		fos.flush();
		fos.close();
		/* System.out.println("Done"); */
	}

	public String readFleetData(int data_row) throws IOException, InvalidFormatException {
		genarateInventoryAndWarehouseInformation obj = new genarateInventoryAndWarehouseInformation();
		String path = obj.getProjectPath("*src*test*java*fleetModule*fleetData.xlsx");
		fis = new FileInputStream(path);
		wb = WorkbookFactory.create(fis);
		sh = wb.getSheet("Sheet1");
		row = sh.getRow(data_row);
		cell = row.getCell(1);

		String employee_readed = cell.getStringCellValue();
		return employee_readed;
	}

	public void sugestionKeyDown(String locator) throws Exception {
		driver.findElement(getLocator(locator)).sendKeys(Keys.ARROW_DOWN);
	}

	String vehicle_make;
	String vm_Description;
	String packCode;
	String vehicle_Model_1;
	String vehicle_Model_2;
	String vehicle_No;

	// --------------START LOGIN------------------

	public void navigateToTheLoginPage() throws Exception {
		openPage(siteURL);

	}

	public void verifyTheLogo() throws Exception {
		if (isDisplayed(siteLogo)) {

			writeTestResults("Verify that 'Entution' header is available on the page",
					"user should be able to see the logo", "Logo is displayed sucessfully", "pass");
		} else {
			writeTestResults("Verify that 'Entution' header is available on the page",
					"user should be able to see the logo", "Logo doesn't displayed", "fail");
		}
	}

	public void userLogin() throws Exception {

		sendKeys(txt_username, userNameData);

		if (isDisplayed(txt_username)) {
			writeTestResults("Verify user can enter a Username ", "user should be able to enter a Username",
					"User successfully enter a Username", "pass");
		} else {
			writeTestResults("Verify user can enter a Username ", "user should be able to enter a Username",
					"User couldn't enter a Username", "fail");
		}

		sendKeys(txt_password, passwordData);
		if (isDisplayed(txt_username)) {
			writeTestResults("Verify user can enter a Password ", "user should be able to enter a Password",
					"User successfully enter a Password", "pass");
		} else {
			writeTestResults("Verify user can enter a Password ", "user should be able to enter a Password",
					"User couldn't enter a Password", "fail");
		}

		click(btn_login);

		if (isDisplayed(lnk_home)) {

			writeTestResults("Verify that user can login to the 'Entution' ", "User should be login to the 'Entution'",
					"User login to the 'Entution' sucessfully", "pass");
		} else {
			writeTestResults("Verify that user can login to the 'Entution' ", "User should be login to the 'Entution'",
					"User does'nt login to the 'Entution'", "fail");

		}

	}

	public void navigationmenu() throws Exception {

		click(btn_navigationmenu);

		if (isDisplayed(sidemenu)) {
			writeTestResults("Click on the top navigation menu", "System should be loaded the menu item successfully",
					"Menu item is displayed", "pass");
		} else {
			writeTestResults("Click on the top navigation menu", "System should not  loaded the menu item successfully",
					"Menu item is not displayed", "fail");
		}
	}

	public void navigate_fleet_management() throws Exception {

		Thread.sleep(3000);
		click(btn_fleetmodule);
		Thread.sleep(2000);

		if (isDisplayed(fleetParameterMenu)) {

			writeTestResults("Click on the Fleet module", "System should be loaded the Fleet module forms successfully",
					"Forms of Fleet module is displayed", "pass");
		} else {

			writeTestResults("Click on the Fleet module", "System should not loaded the Fleet module successfully",
					"Forms of Fleet module is not displayed", "fail");
		}
	}

	public void navigate_fleet_parameter() throws Exception {

		Thread.sleep(1000);
		click(fleetParameterMenu);
		Thread.sleep(1000);

		if (isDisplayed(fleetParameterHeader)) {

			writeTestResults("Click on the Fleet Parameter", "System should navigate to Fleet Parameter page",
					"User navigate to Fleet Parameter page and Fleet Parameters header is displayed", "pass");
		} else {

			writeTestResults("Click on the Fleet Parameter", "System should navigate to Fleet Parameter page",
					"User not navigate to Fleet Parameter page and Fleet Parameters header is not displayed", "fail");
		}
	}

	public void VerifyThatAbleToAddVehicleMakeParameters() throws Exception {

		click(vehicleMakeTab);
		if (isDisplayed(vehicleMakeHeader)) {
			writeTestResults("Click on Vehicle make tab", "Vehicle Make Header should dispaly",
					"Vehicle Make Header is displayed", "pass");
		} else {
			writeTestResults("Click on Vehicle make tab", "Vehicle Make Header should dispaly",
					"Vehicle Make Header is not displayed", "fail");
		}

		click(vehicleMakeAddBtn);
		Thread.sleep(1000);
		click(vehicleMakeTxtBox);
		LocalTime Obj1 = LocalTime.now();
		vehicle_make = vehicleMakeTxtVal + Obj1;
		writeFleetData("vehicaleMake", vehicle_make, 0);
		sendKeys(vehicleMakeTxtBox, vehicle_make);
		Thread.sleep(1000);
		click(updateVehicleMakeParameter);
		driver.navigate().refresh();

	}

	public void navigatetoVehicleModelPage() throws Exception {

		Thread.sleep(1000);
		click(vehicleModelMenu);
		Thread.sleep(1000);

		if (isDisplayed(vehicleModelConfigurationHeader)) {

			writeTestResults("Click on the Vehicle Model", "System should navigate to Vehicle Model page",
					"User navigate to Vehicle Model page and Vehicle Model configuration header is displayed", "pass");
		} else {

			writeTestResults("Click on the Vehicle Model", "System should navigate toVehicle Model page",
					"User not navigate to Vehicle Model page and Vehicle Model configuration header is not displayed",
					"fail");
		}
	}

	public void createNewVehicleModel() throws Exception {

		Thread.sleep(1000);
		click(newVehicleModelBtn);
		if (isDisplayed(vehicleModelHeader)) {
			writeTestResults("Click on New Vehicle Model", "System Should navigate to New vehicle model window",
					"User navigate to new Vheicle model window and New Vehicle Model is displayed", "Pass");
		} else {
			writeTestResults("Click on New Vehicle Model", "System Should navigate to New vehicle model window",
					"User navigate to new Vheicle model window and New Vehicle Model is not displayed", "fail");
		}

		click(vehicleModelDropDown);
		selectText(vehicleModelDropDown, vehicle_make);
		click(vehicleModelDescription);
		LocalTime Obj2 = LocalTime.now();
		vm_Description = vehicleModelDescriptionVal + Obj2;
		sendKeys(vehicleModelDescription, vm_Description);
		Thread.sleep(1000);
		click(vehicleModel1);
		vehicle_Model_1 = vehicleModelVal1 + Obj2;
		writeFleetData("vehicleModel1", vehicle_Model_1, 1);
		sendKeys(vehicleModel1, vehicle_Model_1);
		click(vehicleType1);
		selectText(vehicleType1, vehicleTypeVal1);
		Thread.sleep(1000);
		click(damageUpload);
		Thread.sleep(1000);
		sendKeys(damageUpload, "C:\\Users\\sithara\\Pictures\\cargo.png");

		Thread.sleep(1000);
		click(addVehicleModelBtn);
		click(vehicleModel2);
		LocalTime Obj3 = LocalTime.now();
		vehicle_Model_2 = vehicleModelVal2 + Obj3;
		sendKeys(vehicleModel2, vehicle_Model_2);
		Thread.sleep(1000);
		click(vehicleType2);
		selectText(vehicleType2, vehicleTypeVal2);
		Thread.sleep(1000);
		click(updateVehicleModel);
		Thread.sleep(1000);
		driver.navigate().refresh();
		explicitWait(searchVehicleModel, 30);
		Thread.sleep(2000);
		click(searchVehicleModel);
		sendKeys(searchVehicleModel, readFleetData(0));
		Thread.sleep(3000);
		sugestionKeyDown(searchVehicleModel);
		pressEnter(searchVehicleModel);
		Thread.sleep(2000);
		click(activateVehicleModel);

		if (isDisplayed(confirmationHeader)) {
			writeTestResults("Click on activate vehicle model", "Confirmation message should pop-up",
					"confirmatin message is display with having heading", "Pass");
		} else {
			writeTestResults("Click on activate vehicle model", "Confirmation message should pop-up",
					"confirmation message is not display and confirmation header is not display", "fail");
		}
		click(confirmationYes);

	}

	public void NavigationPackageInformationByPage() throws Exception {

		Thread.sleep(1000);
		click(packageInformationMenu);
		Thread.sleep(1000);

		if (isDisplayed(packageInformationHeader)) {

			writeTestResults("Click on the Package Information", "System should navigate to Package Information Bypage",
					"User navigate to Package Information By page and Package Information header is displayed", "pass");
		} else {

			writeTestResults("Click on the Package Information", "System should navigate to Package Information Bypage",
					"User not navigate Package Information By page and Package Information header is not displayed",
					"fail");
		}
	}

	public void NavigationPackageInformationNewPage() throws Exception {

		Thread.sleep(1000);
		click(newPackageBtn);
		Thread.sleep(1000);

		if (isDisplayed(newpackageHeader)) {

			writeTestResults("Click on the New Package Information",
					"System should navigate to Package Information New page",
					"User navigate to Package Information New page and New Package Information header is displayed",
					"pass");
		} else {

			writeTestResults("Click on the New Package Information",
					"System should navigate to Package Information New page",
					"User not navigate to Package Information new page and new Package Information header is not displayed",
					"fail");
		}
	}

	public void VerifyThatAbleToCreatePackage() throws Exception {

		click(packageCode);
		LocalTime Obj3 = LocalTime.now();
		packCode = packageCodeVal + Obj3;
		writeFleetData("packageCode", packCode, 2);
		sendKeys(packageCode, packCode);
		Thread.sleep(1000);
		click(packageDes);
		LocalTime Obj4 = LocalTime.now();
		String packDes = packageDesVal + Obj4;
		sendKeys(packageDes, packDes);
		click(accountGroup);
		selectText(accountGroup, accountGroupVal);
		click(rateType);
		selectText(rateType, rateTypeVal);
		click(rate);
		sendKeys(rate, rateVal);

		click(invoicingProductLookup);
		if (isDisplayed(invoicingProductHeader)) {

			writeTestResults("Click on the invoicing product lookup", "Product Lookup should pop-up",
					"Product header display in product window", "pass");
		} else {

			writeTestResults("Click on the invoicing product lookup", "Product Lookup should pop-up",
					"Product header is not display in product window", "fail");
		}

		WaitClick(invoicingProductTxtBox);
		click(invoicingProductTxtBox);
		sendKeys(invoicingProductTxtBox, invoicingProductVal);
		click(invoicingProductSearchBtn);
		Thread.sleep(3000);
		pressEnter(invoicingProductSearchBtn);
//		WaitClick(selectInvoicingProduct);
		Thread.sleep(3000);
		doubleClick(selectInvoicingProduct);
		click(allottedMileage);
		Thread.sleep(1000);
		sendKeys(allottedMileage, allottedMileageVal);
		click(extraMileageRate);
		Thread.sleep(1000);
		sendKeys(extraMileageRate, extraMileageRateVal);

		WaitClick(extraMileageProductLookup);
		click(extraMileageProductLookup);
		if (isDisplayed(extraMileageProductHeader)) {

			writeTestResults("Click on the extra mileage product lookup", "Product Lookup should pop-up",
					"Product header display in product window", "pass");
		} else {

			writeTestResults("Click on the extra mileage  product lookup", "Product Lookup should pop-up",
					"Product header is not display in product window", "fail");
		}

//		WaitClick(extraMileageProductTxtBox);
//		click(extraMileageProductTxtBox);
//		Thread.sleep(1000);
//		WaitElement(extraMileageProductTxtBox);
		sendKeys(extraMileageProductTxtBox, extraMileageProductVal);
		Thread.sleep(3000);
		pressEnter(extraMileageProductTxtBox);
//		WaitClick(selectextraMileageProduct);
		Thread.sleep(3000);
		doubleClick(selectextraMileageProduct);
		Thread.sleep(1000);
		click(allottedTime);
		sendKeys(allottedTime, allottedTimeVal);
		click(extraTimeRate);
		sendKeys(extraTimeRate, extraTimeRateval);

		WaitClick(extraTimeProductLookup);
		click(extraTimeProductLookup);
		if (isDisplayed(extraTimeProductHeader)) {

			writeTestResults("Click on the extra Time product lookup", "Product Lookup should pop-up",
					"Product header display in product window", "pass");
		} else {

			writeTestResults("Click on the extra Time product lookup", "Product Lookup should pop-up",
					"Product header is not display in product window", "fail");
		}

//		click(extraTimeProductTxtBox);
//		Thread.sleep(1000);
		WaitElement(extraTimeProductTxtBox);
		sendKeys(extraTimeProductTxtBox, extraTimeProductVal);
		Thread.sleep(3000);
		pressEnter(extraTimeProductTxtBox);
		Thread.sleep(3000);
//		WaitClick(selectextraTimeProduct);
		doubleClick(selectextraTimeProduct);
		Thread.sleep(1000);
		click(withDriverFlag);
		// Thread.sleep(1000);

		WaitClick(labourProductLookup);
		click(labourProductLookup);
		if (isDisplayed(labourProductHeader)) {

			writeTestResults("Click on the labour product lookup", "Product Lookup should pop-up",
					"Product header display in product window", "pass");
		} else {

			writeTestResults("Click on the labour product lookup", "Product Lookup should pop-up",
					"Product header is not display in product window", "fail");
		}

//		click(labourProductTxtBox);
//		Thread.sleep(1000);
		WaitElement(labourProductTxtBox);
		sendKeys(labourProductTxtBox, labourProductVal);
		WaitClick(labourProductSearchBtn);
		click(labourProductSearchBtn);
		WaitClick(selectlabourProduct);
		doubleClick(selectlabourProduct);

		WaitClick(packageDraft);
		click(packageDraft);
		if (isDisplayed(packageDraftHeader)) {

			writeTestResults("Click on Draft Button", "package should be drafted",
					"Package is drafted and status dispay as Draft", "pass");
		} else {

			writeTestResults("Click on Draft Button", "package should be drafted",
					"Package is not drafted and status is not dispay as Draft", "fail");

		}

		WaitClick(packageRelease);
		click(packageRelease);
		Thread.sleep(1000);
		if (isDisplayed(packageReleaseHeader)) {

			writeTestResults("Click on Release Button", "package should be Released",
					"Package is Released and status dispay as Released", "pass");
		} else {

			writeTestResults("Click on Release Button", "package should be Released",
					"Package is not Released and status is not dispay as Released", "fail");
		}

		trackCode = getText(packageId);

	}

	public void NavigationVehicleInformationByPage() throws Exception {

		Thread.sleep(1000);
		click(OrgMgtModule);
		Thread.sleep(1000);

		if (isDisplayed(VehicleInformationMenu)) {

			writeTestResults("Click on the Organiztion Management module",
					"System should display Vehicle Information Menu", "System display Vehicle Information Menu",
					"pass");
		} else {

			writeTestResults("Click on the Organiztion Management module",
					"System should display Vehicle Information Menu", "System is not display Vehicle Information Menu",
					"fail");
		}
		click(VehicleInformationMenu);
		if (isDisplayed(VIBypageHeader)) {

			writeTestResults("Click on the Vehicle Information", "System should navigate to Vehicle Information Bypage",
					"User navigate to Vehicle Information By page and Vehicle Information header is displayed", "pass");
		} else {

			writeTestResults("Click on the Vehicle Information", "System should navigate to Vehicle Information Bypage",
					"User not navigate Vehicle Information By page and Vehicle Information header is not displayed",
					"fail");
		}
	}

	public void NavigationVehicleInformationNewPage() throws Exception {

		Thread.sleep(1000);
		click(NewVehicleBtn);
		Thread.sleep(1000);

		if (isDisplayed(newVehicleHeader)) {

			writeTestResults("Click on the New Vehicle Information",
					"System should navigate to Vehicle Information New page",
					"User navigate to Vehicle Information New page and New Vehicle Information header is displayed",
					"pass");
		} else {

			writeTestResults("Click on the New Vehicle Information",
					"System should navigate to Vehicle Information New page",
					"User not navigate to Vehicle Information new page and new Vehicle Information header is not displayed",
					"fail");
		}
	}

	public void VerifyThatAbleToCreateVehicle() throws Exception {

		click(vehicleNo);
		LocalTime Obj1 = LocalTime.now();
		vehicle_No = vehicleNoVal + Obj1;
		writeFleetData("vehicleNo", vehicle_No, 3);
		sendKeys(vehicleNo, vehicle_No);
		click(vehicleDescription);
		LocalTime Obj2 = LocalTime.now();
		String vehicle_Des = vehicleDescriptionVal + Obj2;
		sendKeys(vehicleDescription, vehicle_Des);
		click(vehicleGroup);
		selectText(vehicleGroup, vehicleGroupVal);
		click(vehicleLocation);
		selectText(vehicleLocation, vehicleLocationVal);
		click(fixedAssetLookup);
		Thread.sleep(1000);
		if (isDisplayed(fixedAssetHeader)) {

			writeTestResults("Click on the fixed Asset lookup", "fixed Asset Lookup should pop-up",
					"fixed Asset header display in fixed Asset window", "pass");
		} else {

			writeTestResults("Click on the fixed Asset lookup", "fixed Asset Lookup should pop-up",
					"fixed Asset header is not display in fixed Asset window", "fail");
		}

		click(fixedAssetSearch);
		sendKeys(fixedAssetSearch, fixedAssetSearchVal);
		Thread.sleep(1000);
		pressEnter(fixedAssetSearch);
		Thread.sleep(2000);
		doubleClick(selectFixedAsset);
		click(selectVehicleType);
		selectText(selectVehicleType, vehicleTypeVal);
		Thread.sleep(1000);
		click(selectVehicleMake);
		selectText(selectVehicleMake, readFleetData(0));
		Thread.sleep(1000);
		click(selectVehicleModel);
		selectText(selectVehicleModel, readFleetData(1));
		Thread.sleep(1000);
		click(selectVehicleTransmission);
		selectText(selectVehicleTransmission, selectVehicleTransmissionVal);
		Thread.sleep(1000);
		click(selectFuelType);
		selectText(selectFuelType, selectFuelTypeVal);
		Thread.sleep(1000);
		click(selectColor);
		selectText(selectColor, selectColorVal);
		Thread.sleep(1000);
		click(draftVehicle);

		if (isDisplayed(draftVehicleHeader)) {

			writeTestResults("Click on the Draft",
					"Vehicle Information should be drafted and Status should be displayed as Draft",
					"Vehicle is Drafted and status display as Draft", "pass");
		} else {

			writeTestResults("Click on the Draft",
					"Vehicle Information should be drafted and Status should be displayed as Draft",
					"Vehicle is not Drafted and status  is not display as Draft", "fail");
		}

		click(releaseVehicle);

		if (isDisplayed(releaseVehicleHeader)) {

			writeTestResults("Click on the Release",
					"Vehicle Information should be Release and Status should be displayed as Released",
					"Vehicle is Drafted and status display as Draft", "pass");
		} else {

			writeTestResults("Click on the Release",
					"Vehicle Information should be Release and Status should be displayed as Released",
					"Vehicle is not Released and status  is not display as Released", "fail");
		}

	}

	public void NavigationGatePassByPage() throws Exception {

		Thread.sleep(1000);
		click(btn_fleetmodule);
		Thread.sleep(1000);

		if (isDisplayed(gatePassMenu)) {

			writeTestResults("Click on the Fleet Management module", "System should display Gate Pass Menu",
					"System display Gate Pass Menu", "pass");
		} else {

			writeTestResults("Click on the Fleet Management module", "System should display Gate Pass Menu",
					"System is not display Gate Pass Menu", "fail");
		}
		click(gatePassMenu);
		if (isDisplayed(gatePassByPageHeader)) {

			writeTestResults("Click on the Gate Pass Menu", "System should navigate to Gate Pass Bypage",
					"User navigate to Gate Pass By page and Gate Pass header is displayed", "pass");
		} else {

			writeTestResults("Click on the Gate Pass Menu", "System should navigate to Gate Pass Bypage",
					"User not navigate to Gate Pass By page and Gate Pass header is not displayed", "fail");
		}

	}

	public void NavigationGatePassNewPage() throws Exception {

		Thread.sleep(1000);
		click(newGatePassBtn);
		Thread.sleep(1000);

		if (isDisplayed(newGatePassHeader)) {

			writeTestResults("Click on the New Gate Pass", "System should navigate to Gate Pass New page",
					"User navigate to Gate Pass New page and New Gate Pass header is displayed", "pass");
		} else {

			writeTestResults("Click on the New Gate Pass", "System should navigate to Gate Pass New page",
					"User not navigate to Gate Pass new page and new Gate Pass header is not displayed", "fail");
		}

	}

	public void VerifyThatAbleToCreateVehicleGatePass() throws Exception {

		click(vgpPostDate);
		if (isDisplayed(vgpPostDateHeader)) {

			writeTestResults("Click on Post Date", "Post Date window should pop-up",
					"Post Date header display in Post Date window", "pass");
		} else {

			writeTestResults("Click on post Date", "Post Date window should pop-up",
					"Post Date header is not display in Post Date  window", "fail");
		}
		click(vgpPostDateTxt);
		doubleClick(vgpPostDateSelection.replace("Date", "1"));
		click(postDateApply);

		click(purposeTxt);
		LocalTime Obj5 = LocalTime.now();
		String purpose_Txt = purposeTxtVal + Obj5;
		sendKeys(purposeTxt, purpose_Txt);
		click(DesTxt);
		String Des_Txt = desTxtVal + Obj5;
		sendKeys(DesTxt, Des_Txt);
		click(vehicleLookup);
		Thread.sleep(1000);
		if (isDisplayed(vehicleHeader)) {

			writeTestResults("Click on Vehicle lookup", "Vehicle Lookup should pop-up",
					"Vehicle header display in Vehicle window", "pass");
		} else {

			writeTestResults("Click on the Vehicle lookup", "Vehicle Lookup should pop-up",
					"Vehicle header is not display in Vehicle window", "fail");
		}
		click(vehicleSearch);
		sendKeys(vehicleSearch, readFleetData(3));
		Thread.sleep(1000);
		pressEnter(vehicleSearch);
		Thread.sleep(2000);
		doubleClick(selectVehicle);

		click(driverLookup);
		if (isDisplayed(driverHeader)) {

			writeTestResults("Click on Driver lookup", "Driver Lookup should pop-up",
					"Driver header display in Driver window", "pass");
		} else {

			writeTestResults("Click on the Driver lookup", "Driver Lookup should pop-up",
					"Driver header is not display in Driver window", "fail");
		}

		click(driverSearch);
		sendKeys(driverSearch, driverSearchVal);
		Thread.sleep(2000);
		pressEnter(driverSearch);
		Thread.sleep(2000);
		doubleClick(selectDriver);
		click(vgpEstimatedCheckoutDate);
		doubleClick(vgpEstimatedCheckoutDateSelection.replace("Date", "1"));
		click(vgpEstimatedCheckoutTimeTxt);
		click(vgpEstimatedCheckoutTime.replace("Time", "08"));
		click(vgpEstimatedCheckoutTimeOk);

		click(vgpEstimatedCheckinDate);
		doubleClick(vgpEstimatedCheckinDateSelection.replace("Date", "2"));
		click(vgpEstimatedCheckinTimeTxt);

		click(vgpEstimatedCheckinTime);
		click(vgpEstimatedCheckinTimeOk.replace("Time", "15"));
		click(dateOK);

		click(draftGatePass);

		if (isDisplayed(draftGatepassStatus)) {

			writeTestResults("Click on the Draft",
					"Vehicle Gate Pass should be drafted and Status should be displayed as Draft",
					"Vehicle Gate Pass is Drafted and status display as Draft", "pass");
		} else {

			writeTestResults("Click on the Draft",
					"Vehicle Gate Pass should be drafted and Status should be displayed as Draft",
					"Vehicle Gate Pass is not Drafted and status  is not display as Draft", "fail");
		}

		click(releaseGatePass);

		if (isDisplayed(releaseGatePassStatus)) {

			writeTestResults("Click on the Release",
					"Vehicle Gate Pass should be Release and Status should be displayed as Released",
					"Vehicle Gate Pass is Drafted and status display as Draft", "pass");
		} else {

			writeTestResults("Click on the Release",
					"Vehicle Gate Pass should be Release and Status should be displayed as Released",
					"Vehicle Gate Pass is not Released and status  is not display as Released", "fail");
		}

		driver.navigate().refresh();

	}

	public void VerifyGatePassCheckout() throws Exception {

		click(gatePassAction);
		Thread.sleep(1000);
		if (isDisplayed(actionCheckout)) {

			writeTestResults("Click on Gate Pass Action", "Vehicle Gate Pass Checkout action should display",
					"Vehicle Gate Pass Checkout action is display", "pass");
		} else {

			writeTestResults("Click on Gate Pass Action", "Vehicle Gate Pass Checkout action should display",
					"Vehicle Gate Pass Checkout action is not display", "fail");
		}

		click(actionCheckout);
		if (isDisplayed(gatePassChekoutHeader)) {

			writeTestResults("Click on Gate Pass checkout Action", "Vehicle Gate Pass Checkout Header should display",
					"Vehicle Gate Pass Checkout Header is display", "pass");
		} else {

			writeTestResults("Click on Gate Pass checkout Action", "Vehicle Gate Pass Checkout Header should display",
					"Vehicle Gate Pass Checkout Header is not display", "fail");
		}

		click(gatePassCheckoutDate);
		doubleClick(vgpEstimatedCheckoutDateSelection.replace("Date", "1"));
		// JavascriptExecutor j7 = (JavascriptExecutor) driver;
		// j7.executeScript("$(\"#txtChkOutDte\").datepicker(\"setDate\", new Date())");
		click(gatePassCheckoutTime);
		sendKeys(gatePassCheckoutTime, "8");
		click(gatePassCheckoutMileage);
		sendKeys(gatePassCheckoutMileage, gatePassCheckoutMileageVal);
		click(gatePassCheckoutFuelLevel);
		JavascriptExecutor js1 = (JavascriptExecutor) driver;
		js1.executeScript("$('#txtChkOutFlLvl').val(30)");
		click(gatePassCheckoutLocation);
		selectIndex(gatePassCheckoutLocation, 1);
		click(updateGatePassCheckout);
		Thread.sleep(1000);

	}

	public void VerifyGatePassCheckIn() throws Exception {

		click(gatePassAction);
		Thread.sleep(1000);
		if (isDisplayed(actionCheckin)) {

			writeTestResults("Click on Gate Pass Action", "Vehicle Gate Pass Check-In action should display",
					"Vehicle Gate Pass Check-In action is display", "pass");
		} else {

			writeTestResults("Click on Gate Pass Action", "Vehicle Gate Pass Check-In action should display",
					"Vehicle Gate Pass Check-In action is not display", "fail");
		}

		click(actionCheckin);
		if (isDisplayed(gatePassChekInHeader)) {

			writeTestResults("Click on Gate Pass Check-In Action", "Vehicle Gate Pass Check-In Header should display",
					"Vehicle Gate Pass Check-In Header is display", "pass");
		} else {

			writeTestResults("Click on Gate Pass Check-In Action", "Vehicle Gate Pass Check-In Header should display",
					"Vehicle Gate Pass Check-In Header is not display", "fail");
		}

		click(gatePassCheckInDate);
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date currentDate = new Date();

		// convert date to calendar
		Calendar cal = Calendar.getInstance();
		cal.setTime(currentDate);

		// manipulate date
		cal.add(Calendar.DATE, 3);

		// convert calendar to date
		Date currentDatePlusOne = cal.getTime();

		// get Future date to String
		String GpFutureDate = dateFormat.format(currentDatePlusOne);
		sendKeys(gatePassCheckInDate, GpFutureDate);

		click(gatePassCheckInTime);
		sendKeys(gatePassCheckInTime, "10");
		click(gatePassCheckInMileage);
		sendKeys(gatePassCheckInMileage, gatePassCheckInMileageVal);
		click(gatePassCheckInFuelLevel);
		JavascriptExecutor js2 = (JavascriptExecutor) driver;
		js2.executeScript("$('#txtChkInFlLvl').val(30)");
		Thread.sleep(1000);
		click(gatePassCheckInLocation);
		selectIndex(gatePassCheckInLocation, 1);
		click(updateGatePassCheckIn);
		Thread.sleep(1000);
		driver.navigate().refresh();

	}

	public void NavigationRentAgreementByPage() throws Exception {

		click(btn_fleetmodule);

		if (isDisplayed(rentAgreementMenu)) {

			writeTestResults("Click on the Fleet Management module", "System should display Rent Agreement Menu",
					"System display Rent Agreement Menu", "pass");
		} else {

			writeTestResults("Click on the Fleet Management module", "System should display Rent Agreement Menu",
					"System is not display Rent Agreement", "fail");
		}

		click(rentAgreementMenu);

		if (isDisplayed(rentAgreementByPageHeader)) {

			writeTestResults("Click on the Rent Agreement Menu", "System should navigate to Rent Agreement Bypage",
					"User navigate to Rent Agreement By page and Rent Agreement header is displayed", "pass");
		} else {

			writeTestResults("Click on the Rent Agreement Menu", "System should navigate to Rent Agreement Bypage",
					"User not navigate to Rent Agreement By page and Rent Agreement header is not displayed", "fail");
		}

	}

	public void NavigationRentAgreementNewPage() throws Exception {

		WaitClick(rentAgreementBtn);
		click(rentAgreementBtn);

		if (isDisplayed(newRentAgreementHeader)) {

			writeTestResults("Click on the New Rent Agreement", "System should navigate to Rent Agreement New page",
					"User navigate to Rent Agreement New page and New Rent Agreement header is displayed", "pass");
		} else {

			writeTestResults("Click on the New Rent Agreement", "System should navigate to Rent Agreement New page",
					"User not navigate to Rent Agreement new page and New Rent Agreement header is not displayed",
					"fail");
		}

	}

	public void VerifyThatAbleToCreateRentAgreement() throws Exception {
		click(accountLookup);
		if (isDisplayed(accountHeader)) {

			writeTestResults("Click on the Account Lookup",
					"System should display Account Lookup and Account Header should display",
					"Account lookup is display with Account Header", "pass");
		} else {

			writeTestResults("Click on the Account Lookup",
					"System should display Account Lookup and Account Header should display",
					"Account lookup is not display with Account Header", "fail");
		}
		WaitClick(accountSearch);
		click(accountSearch);
		sendKeys(accountSearch, accountSearchVal);
		pressEnter(accountSearch);
		Thread.sleep(2000);
		doubleClick(selectAccount);
		click(selectContactPerson);
		selectIndex(selectContactPerson, 1);

		click(raVehicleLookup);
		if (isDisplayed(raVehicleHeader)) {

			writeTestResults("Click on the Vehicle Lookup",
					"System should display Vehicle Lookup and Vehicle Header should display",
					"Vehicle lookup is display with Vehicle Header", "pass");
		} else {

			writeTestResults("Click on the Vehicle Lookup",
					"System should display Vehicle Lookup and Vehicle Header should display",
					"Vehicle lookup is not display with Vehicle Header", "fail");
		}

		WaitClick(raVehicleSearch);
		click(raVehicleSearch);
		sendKeys(raVehicleSearch, readFleetData(3));
		pressEnter(raVehicleSearch);
		Thread.sleep(2000);
		doubleClick(raSelectVehicle);
		Thread.sleep(1000);
		click(raDes);
		LocalTime Obj11 = LocalTime.now();
		String raDes_Txt = raDesval + Obj11;
		sendKeys(raDes, raDes_Txt);
		click(raCheckoutDate);
		JavascriptExecutor j8 = (JavascriptExecutor) driver;
		j8.executeScript("$(\"#txtChkOutDte\").datepicker(\"setDate\", new Date())");
		Thread.sleep(1000);
		// click(raCheckoutTime);
		sendKeys(raCheckoutTime, "8");
		// click(raCheckOutTimeOk);

		// click(raCheckInDate);
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date currentDate = new Date();

		// convert date to calendar
		Calendar cal = Calendar.getInstance();
		cal.setTime(currentDate);

		// manipulate date
		cal.add(Calendar.DATE, 2);

		// convert calendar to date
		Date currentDatePlusTwo = cal.getTime();

		// get Future date to String
		String RaFutureDate = dateFormat.format(currentDatePlusTwo);
		sendKeys(raCheckInDate, RaFutureDate);
		System.out.println(RaFutureDate);

		// sendKeys(raCheckInDate, RaFutureDate);

		// j8.executeScript("$('#txtChkin').datepicker('setDate', 'RaFutureDate')");

		click(raCheckInTime);
		sendKeys(raCheckoutTime, "10");
		Thread.sleep(1000);
		click(raCheckInTimeOk);
		click(raSelectBillingType);
		selectIndex(raSelectBillingType, 1);
		click(raSelectRateType);
		selectIndex(raSelectRateType, 2);
		click(raWithDriver);
		click(raPackageLookup);
		if (isDisplayed(raPackageHeader)) {

			writeTestResults("Click on the package Lookup",
					"System should display package Lookup and package Header should display",
					"package lookup is display with package Header", "pass");
		} else {

			writeTestResults("Click on the package Lookup",
					"System should display package Lookup and package Header should display",
					"package lookup is not display with package Header", "fail");
		}

		click(raPackageSearch);
		sendKeys(raPackageSearch, readFleetData(2));
		pressEnter(raPackageSearch);
		Thread.sleep(2000);
		doubleClick(raSelectPackage);
		Thread.sleep(1000);
		click(raEstimationTab);
		click(raMainDiscountPercentage);
		sendKeys(raMainDiscountPercentage, "10");
		click(raMainTaxGroup);
		selectText(raMainTaxGroup, raTaxGroupVal);
		click(raChargesAnalzeCode1);
		selectIndex(raChargesAnalzeCode1, 2);

		click(raChargesProductLookup1);
		Thread.sleep(1000);
		if (isDisplayed(raChargesProductLookupHeader1)) {

			writeTestResults("Click on the product lookup", "Product Lookup should pop-up",
					"Product header display in product window", "pass");
		} else {

			writeTestResults("Click on the product lookup", "Product Lookup should pop-up",
					"Product header is not display in product window", "fail");
		}

		click(raChargesProductSearch1);
		Thread.sleep(1000);
		sendKeys(raChargesProductSearch1, raChargesProductSearchVal1);
		pressEnter(raChargesProductSearch1);
		Thread.sleep(1000);
		doubleClick(raChargesSelectProduct1);
		click(raChargesFromDate1);
		j8.executeScript("$(\"#dp1598782366498\").datepicker(\"setDate\", new Date())");
		click(raChargesToDate1);
		sendKeys(raChargesToDate1, RaFutureDate);
		click(raChargesRate1);
		sendKeys(raChargesRate1, "2000");
		click(raChargesDiscountPercentage1);
		sendKeys(raChargesDiscountPercentage1, "20");
		click(raChargesTaxGroup1);
		selectText(raChargesTaxGroup1, raTaxGroupVal);
		click(raAddChargeLine);

		click(raChargesAnalzeCode2);
		selectIndex(raChargesAnalzeCode2, 2);

		click(raChargesProductLookup2);
		Thread.sleep(1000);
		if (isDisplayed(raChargesProductLookupHeader2)) {

			writeTestResults("Click on the product lookup", "Product Lookup should pop-up",
					"Product header display in product window", "pass");
		} else {

			writeTestResults("Click on the product lookup", "Product Lookup should pop-up",
					"Product header is not display in product window", "fail");
		}

		click(raChargesProductSearch2);
		Thread.sleep(1000);
		sendKeys(raChargesProductSearch2, raChargesProductSearchVal2);
		pressEnter(raChargesProductSearch2);
		Thread.sleep(1000);
		doubleClick(raChargesSelectProduct2);
		click(raChargesFromDate2);
		j8.executeScript("$(\"#dp1598782366502\").datepicker(\"setDate\", new Date())");
		click(raChargesToDate2);
		sendKeys(raChargesToDate2, RaFutureDate);
		click(raChargesRate2);
		sendKeys(raChargesRate2, "2000");
		click(raChargesDiscountPercentage2);
		sendKeys(raChargesDiscountPercentage2, "20");
		click(raChargesTaxGroup1);
		selectText(raChargesTaxGroup2, raTaxGroupVal);

		click(raAccessoriesProductLookup1);
		Thread.sleep(1000);
		if (isDisplayed(raAccessoriesProductHeader1)) {

			writeTestResults("Click on the product lookup", "Product Lookup should pop-up",
					"Product header display in product window", "pass");
		} else {

			writeTestResults("Click on the product lookup", "Product Lookup should pop-up",
					"Product header is not display in product window", "fail");
		}

		click(raAccessoriesProductSearch1);
		Thread.sleep(1000);
		sendKeys(raAccessoriesProductSearch1, raAccessoriesProductSearchVal1);
		pressEnter(raAccessoriesProductSearch1);
		Thread.sleep(1000);
		doubleClick(raAccessoriesSelectProduct1);
		click(raAccessoriesFromDate1);
		j8.executeScript("$(\"#dp1598784069443\").datepicker(\"setDate\", new Date())");
		click(raAccessoriesToDate1);
		// sendKeys(raAccessoriesToDate1, FutureDate);
		click(raAccessoriesRate1);
		sendKeys(raAccessoriesRate1, "2000");
		click(raAccessoriesDiscountPercentage1);
		sendKeys(raAccessoriesDiscountPercentage1, "20");
		click(raAccessoriesTaxGroup1);
		selectText(raAccessoriesTaxGroup1, raTaxGroupVal);
		click(raAddAccessoriesLine);

		click(raAccessoriesProductLookup1);
		Thread.sleep(1000);
		if (isDisplayed(raAccessoriesProductHeader1)) {

			writeTestResults("Click on the product lookup", "Product Lookup should pop-up",
					"Product header display in product window", "pass");
		} else {

			writeTestResults("Click on the product lookup", "Product Lookup should pop-up",
					"Product header is not display in product window", "fail");
		}

		click(raAccessoriesProductSearch2);
		Thread.sleep(1000);
		sendKeys(raAccessoriesProductSearch2, raAccessoriesProductSearchVal2);
		pressEnter(raAccessoriesProductSearch2);
		Thread.sleep(1000);
		doubleClick(raAccessoriesSelectProduct2);
		click(raAccessoriesFromDate2);
		j8.executeScript("$(\"#dp1598784069445\").datepicker(\"setDate\", new Date())");
		click(raAccessoriesToDate2);
		sendKeys(raAccessoriesToDate2, RaFutureDate);
		click(raAccessoriesRate2);
		sendKeys(raAccessoriesRate2, "2000");
		click(raAccessoriesDiscountPercentage2);
		sendKeys(raAccessoriesDiscountPercentage2, "20");
		click(raAccessoriesTaxGroup2);
		selectText(raAccessoriesTaxGroup2, raTaxGroupVal);
		click(raCheckoutBtn);

		click(raDraft);

		if (isDisplayed(raDraftHeader)) {

			writeTestResults("Click on the Draft",
					"Rent Agreement should be drafted and Status should be displayed as Draft",
					"Rent Agreement is Drafted and status display as Draft", "pass");
		} else {

			writeTestResults("Click on the Draft",
					"Rent Agreement should be drafted and Status should be displayed as Draft",
					"Rent Agreement is not Drafted and status  is not display as Draft", "fail");
		}

		click(raRelease);

		if (isDisplayed(raReleaseHeader)) {

			writeTestResults("Click on the Release",
					"Rent Agreement should be Release and Status should be displayed as Released",
					"Rent Agreement is Drafted and status display as Draft", "pass");
		} else {

			writeTestResults("Click on the Release",
					"Rent Agreement should be Release and Status should be displayed as Released",
					"Rent Agreement is not Released and status  is not display as Released", "fail");
		}

		driver.navigate().refresh();

		click(raVerionBtn);
		Thread.sleep(3000);
		// mouseMove(versionTab);
		mouseMove("//div[@class='color-selected']");

		mouseMove(raConfirmVersion);
		Thread.sleep(2000);

		if (isDisplayed(raCofirmationHeader)) {
			writeTestResults("Click on confirm version", "Confirmation message should pop-up",
					"Confirmation message is pop-up", "Pass");
		} else {
			writeTestResults("Click on confirm version", "Confirmation message should pop-up",
					"Confirmation message is not pop-up", "Fail");
		}

		click(raConfirmBtn);
		Thread.sleep(3000);
		if (isDisplayed(raConfirmedStatus)) {
			writeTestResults("Click on Yes in confirmation message", "Status should be changed as Confirmed",
					"Status is display as Confirmed", "Pass");
		} else {
			writeTestResults("Click on Yes in confirmation message", "Status should be changed as Confirmed",
					"Status is not displayed as confirmed", "Fail");
		}

	}

	// WaitClick
	public void WaitClick(String Locator) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Locator)));
	}

	// WaitElement
	public void WaitElement(String Locator) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator)));
	}

}
