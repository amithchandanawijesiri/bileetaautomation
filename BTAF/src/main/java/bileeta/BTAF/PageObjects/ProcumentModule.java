package bileeta.BTAF.PageObjects;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.DoubleClickAction;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.IFactoryAnnotation;



import bileeta.BATF.Pages.ProcumentModuleData;
import io.qameta.allure.Step;


public class ProcumentModule extends ProcumentModuleData{
	
	
	public static String number,number1,ReturnOrderNo,PurchaseOrderNo,InboundShipmentNo,PurchaseInvoiceNo,ShipmentCostingInformationNo,x,y,z,w;
	public static int poval1,poval2,poval3,poval4,poval5,poval6,poval7,pival1,pival2,pival3,pival4,pival5,pival6,pival7,isval1,isval2,isval3,isval4,isval5,isval6,isval7;
	InventoryAndWarehouseModule pro7=new InventoryAndWarehouseModule();
	
	public void navigateToTheLoginPage() throws Exception  {
		openPage(siteURL);

		writeTestResults("Verify that 'Entution' page is available",
				"user should be able to see 'Entution' page", "'Entution' page is dislayed", "pass");

	}

	public void verifyTheLogo() throws Exception {
		if (isDisplayed(siteLogo)) {

			writeTestResults("Verify that 'Entution' header is available on the page","user should be able to see the logo", "logo is dislayed", "pass");
		} else {
			writeTestResults("Verify that 'Entution' header is available on the page","user should be able to see the logo", "logo is not dislayed", "fail");

		}
	}

	public void userLogin() throws Exception {
		
		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		sendKeys(txt_username, userNameData);
		sendKeys(txt_password, passwordData);
		click(btn_login);

		if (isDisplayed(lnk_home)) {

			writeTestResults("User login", "User should navigate to the Home page of Entution",
					"user navigated to the home page", "pass");
			System.out.println("Com_TC_001 Test case is Completed");
		}

		else {
			writeTestResults("User login", "User should navigate to the Home page of Entution",
					"user cant navigate to the home page", "fail");

		}
	}

	public void clickNavigation() throws Exception {
	
		click(btn_nav);
		
		if(isDisplayed(sideMenu)) {
			writeTestResults("verify sidemenu","view sidemenu", "sidemenu is display", "pass");
		}else {
			writeTestResults("verify sidemenu","view sidemenu", "sidemenu is not display", "fail");
		}
			
	}
	public void clickProbutton() throws Exception {
		
		click(btn_probutton);
		if(isDisplayed(subsideMenu)) {
			writeTestResults("verify subsideMenu","view subsideMenu", "subsideMenu is display", "pass");
			System.out.println("Com_TC_002 Test case is Completed");
		}else {
			writeTestResults("verify subsideMenu","view subsideMenu", "subsideMenu is not display", "fail");
		}
	}
	
	public void clickVendorgroupbutton() throws Exception {
		
		click(btn_vengroupbutton);
		if(isDisplayed(txt_VendorGroupConfiguration)) {
			writeTestResults("verify Vendor Group Configuration page","view Vendor Group Configuration page", "Vendor Group Configuration page is display", "pass");
		}else {
			writeTestResults("verify Vendor Group Configuration page","view Vendor Group Configuration page", "Vendor Group Configuration page is not display", "fail");
		}
	}
	
	public void clickNewVendorgroupbutton() throws Exception {
		
		click(btn_newvengroupbutton);
		if(isDisplayed(txt_newvendorgrouppage)&&(getText(Header).contentEquals("New"))) {
			writeTestResults("verify New Vendor Group Configuration page","view New Vendor Group Configuration page", "New Vendor Group Configuration page is display", "pass");
			System.out.println("PR_VGC_001 Test case is Completed");
		}else {
			writeTestResults("verify New Vendor Group Configuration page","view New Vendor Group Configuration page", "New Vendor Group Configuration page is not display", "fail");
		}
	}
	

	public void addVendorGroupConfiguration() throws Exception {
		
		click(btn_VendorGroupbtn);
		click(btn_VendorGroupAdd);
		LocalTime myObj = LocalTime.now();
		sendKeys(txt_newVendorGroupTxt, "VG"+myObj);
		click(btn_VendorGroupUpdate);
		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		selectText(txt_vendorgroup, "VG"+myObj);
		selectText(txt_currency,currency );
		click(btn_btndetails);
		selectText(txt_PurchaseTaxGroup, PurchaseTaxGroup);
		selectText(txt_VendorClassification,VendorClassification);
		selectText(txt_StandardDeliveryMode, StandardDeliveryMode);
		selectText(txt_StandardDeliveryTerm, StandardDeliveryTerm);
		selectText(txt_Warehouse, Warehouse);
		click(btn_PartialDelivery);
		click(btn_ConsolidatedDelivery);
		click(btn_btnpaymentinfo);
		selectText(txt_PaymentTerm, PaymentTerm);
		sendKeys(txt_Tolerance, Tolerance);
		sendKeys(txt_CreditDays, CreditDays);
		sendKeys(txt_CreditLimit, CreditLimit);
		Thread.sleep(3000);
		click(btn_btndraft);
		Thread.sleep(5000);
		if (isDisplayed(pageDraft)){

			writeTestResults("verify whether system allows user to draft Vendor Group Configuration","system allows user to draft Vendor Group Configuration", "Vendor Group Configuration is Draft", "pass");
		} else {
			writeTestResults("verify whether system allows user to draft Vendor Group Configuration","system allows user to draft Vendor Group Configuration", "Vendor Group Configuration is Not Draft", "fail");

		}
		
		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		number = getText(txt_VendorGroupConfigurationNumber);
		
		
		
		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		click("//a[contains(text(),'Vendor Group Configuration')]");
		sendKeys(txt_VendorGroupConfigurationSerch, number);
		pressEnter(txt_VendorGroupConfigurationSerch);
		click(sel_selVendorGroupConfiguration.replace("no", number));
		Thread.sleep(3000);
		click(btn_release);
		Thread.sleep(2000);
		trackCode= getText(Header);
		
		Thread.sleep(5000);
		if (isDisplayed(pageRelease)){

			writeTestResults("verify whether system allows user to Release Vendor Group Configuration","system allows user to Release Vendor Group Configuration", "Vendor Group Configuration is Released", "pass");
			System.out.println("PR_VGC_002 Test case is Completed");
		} else {
			writeTestResults("verify whether system allows user to Release Vendor Group Configuration","system allows user to Release Vendor Group Configuration", "Vendor Group Configuration is Not Released", "fail");

		}


		
	}
	public void vendorInfoRelease() throws Exception {
		
		click(btn_vendorinfo);
		click(btn_release);
	}
	
	public void newVendor() throws Exception {
		
		click(btn_veninfobutton);
		if(isDisplayed(txt_VendorInformationpage)) {
			writeTestResults("verify Vendor Information page","view Vendor Information page", "Vendor Information page is display", "pass");
		}else {
			writeTestResults("verify Vendor Information page","view Vendor Information page", "Vendor Information page is not display", "fail");
		}
		click(btn_newvenbutton);
		if(isDisplayed(txt_newVendorInformationpage)&&(getText(Header).contentEquals("New"))) {
			writeTestResults("verify New Vendor Information page","view New Vendor Information page", "New Vendor Information page is display", "pass");
			System.out.println("PR_VI_001 Test case is Completed");
		}else {
			writeTestResults("verify New Vendor Information page","view New Vendor Information page", "New Vendor Information page is not display", "fail");
		}
	}
	
	public void addVendor() throws Exception {
		
		selectText(txt_VendorType, VendorType);
		selectText(txt_VendorNameTitle, VendorNameTitle);
		LocalTime myObj = LocalTime.now();
		sendKeys(txt_VendorName, VendorName+myObj);
		selectText(txt_currency,currency);
		selectText(txt_vendorgroup, vendorgroupforven);
		click(btn_ReceivableAccount);
		WaitElement(btn_newReceivableAccount);
		click(btn_newReceivableAccount);
		Thread.sleep(3000);
		sendKeys(txt_newReceivableAccountname, newReceivableAccountname+myObj);
		Thread.sleep(7000);
		number = newReceivableAccountname+myObj;
		Thread.sleep(3000);
		selectIndex(txt_AccountGroup, 2);
		WaitClick(btn_btnUpdate);
		click(btn_btnUpdate);
		Thread.sleep(3000);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$(closeMessageBox()).click()");
		
		Thread.sleep(6000);
		sendKeys(txt_ReceivableAccount, number);
		Thread.sleep(3000);
		pressEnter(txt_ReceivableAccount);
		WaitElement(sel_ReceivableAccount);
		doubleClick(sel_ReceivableAccount);
		
		selectText(txt_ContactTitle, ContactTitle);
		sendKeys(txt_ContactName, ContactName);
		sendKeys(txt_MobileNumber,MobileNumber);
		click(btn_btndetails);
		selectText(txt_SelectIndustry,SelectIndustry);
		sendKeys(txt_TaxRegistrationNumber, TaxRegistrationNumber);
		sendKeys(txt_ReferenceAccoutNo, ReferenceAccoutNo);
		Thread.sleep(3000);
		click(btn_3rdPartyAccount);
		Thread.sleep(3000);
		sendKeys(txt_3rdPartyAccount, txt3rdPartyAccount);
		Thread.sleep(3000);
		pressEnter(txt_3rdPartyAccount);
		Thread.sleep(3000);
		doubleClick(sel_3rdPartyAccount);
		selectText(txt_PurchaseTaxGroup, PurchaseTaxGroup);
		sendKeys(txt_PassportNo, PassportNo);
		
		WaitClick(btn_btnpaymentinfo);
		click(btn_btnpaymentinfo);
		
		sendKeys(txt_CreditDays, CreditDays);
		sendKeys(txt_CreditLimit, CreditLimit);
		
		WaitClick(btn_btndraft);
		click(btn_btndraft);
		WaitElement(pageDraft);
		if (isDisplayed(pageDraft)){

			writeTestResults("verify whether system allows user to draft vendor information","system allows user to draft vendor information", "vendor information is Draft", "pass");
		} else {
			writeTestResults("verify whether system allows user to draft vendor information","system allows user to draft vendor information", "vendor information is Not Draft", "fail");

		}
		
		WaitElement(txt_newVendorName);
		number = getText(txt_newVendorName);
		WaitClick("//a[contains(text(),'Vendor Information')]");
		click("//a[contains(text(),'Vendor Information')]");
		
		WaitElement(txt_VendorInformationserch);
		sendKeys(txt_VendorInformationserch, number);
		pressEnter(txt_VendorInformationserch);
		WaitElement(sel_selVendorInformation.replace("no", number));
		doubleClick(sel_selVendorInformation.replace("no", number));
		
		WaitClick(btn_release);
		click(btn_release);
		Thread.sleep(2000);
		trackCode= getText(Header);
		WaitElement(pageRelease);
		if (isDisplayed(pageRelease)){

			writeTestResults("verify whether system allows user to Release vendor information","system allows user to Release vendor information", "vendor information is Released", "pass");
			System.out.println("PR_VI_002 Test case is Completed");
		} else {
			writeTestResults("verify whether system allows user to Release vendor information","system allows user to Release vendor information", "vendor information is Not Released", "fail");

		}

	}
	
	public void vendorRelease() throws Exception {
		
		click(btn_veninfobutton);
		click(btn_vendor);
		click(btn_release);
	}
	
	public void addVendorAgreement() throws Exception {
		
		click(VendorAgreementbutton);
		if(isDisplayed(VendorAgreementpage)) {
			writeTestResults("verify Vendor Agreement page","view Vendor Agreement page", "Vendor Agreement page is display", "pass");
		}else {
			writeTestResults("verify Vendor Agreement page","view Vendor Agreement page", "Vendor Agreement page is not display", "fail");
		}
		click(newVendorAgreementbutton);
		if(isDisplayed(newVendorAgreementpage)&&(getText(Header).contentEquals("New"))) {
			writeTestResults("verify New Vendor Agreement page","view New Vendor Agreement page", "New Vendor Agreement page is display", "pass");
		}else {
			writeTestResults("verify New Vendor Agreement page","view New Vendor Agreement page", "New Vendor Agreement page is not display", "fail");
		}
		LocalTime myObj = LocalTime.now();
		sendKeys(txt_AgreementNo, AgreementNo+myObj);
		sendKeys(txt_AgreementTitle, AgreementTitle);
		click(btn_btnAgreementDate);
		click(btn_selAgreementDate);
		selectText(txt_AgreementGroup, AgreementGroup);
		selectText(txt_ACurrency, Acurrency);
		selectText(txt_ValidityPeriodFor, ValidityPeriodFor);
		sendKeys(txt_Description, Description);
		click(btn_btnVendorAccount);
		sendKeys(txt_txtVendorAccount, txtVendorAccount);
		Thread.sleep(2000);
		pressEnter(txt_txtVendorAccount);
		Thread.sleep(2000);
		doubleClick(sel_selVendorAccount);
		click(btn_btnproductdetails);
		sendKeys(txt_ProductRelation, ProductRelation);
		
		WaitClick(btn_btnproduct);
		click(btn_btnproduct);
		WaitElement(txt_txtproduct);
		sendKeys(txt_txtproduct, txtproduct);
		Thread.sleep(4000);
		pressEnter(txt_txtproduct);
		WaitElement(sel_selproduct.replace("Product", txtproduct));
		doubleClick(sel_selproduct.replace("Product", txtproduct));

		
		sendKeys(txt_minOrderQty, minOrderQty);
		sendKeys(txt_maxOrderQty, maxOrderQty);
		WaitClick(btn_btndraft);
		click(btn_btndraft);
		WaitElement(pageDraft);
		if (isDisplayed(pageDraft)){

			writeTestResults("verify whether system allows user to draft vendor agreement Document","system allows user to draft vendor agreement Document", "vendor agreement Document is Draft", "pass");
			System.out.println("PM_VA_001 Test case is Completed");
		} else {
			writeTestResults("verify whether system allows user to draft vendor agreement Document","system allows user to draft vendor agreement Document", "vendor agreement Document is Not Draft", "fail");

		}
		
		WaitClick(btn_release);
		click(btn_release);
		
		Thread.sleep(2000);
		trackCode= getText(Header);
		WaitElement(pageRelease);
		if (isDisplayed(pageRelease)){

			writeTestResults("Verify whether system allows user to release vendor agreement","system allows user to release vendor agreement", "vendor agreement Document is Released", "pass");
		} else {
			writeTestResults("Verify whether system allows user to release vendor agreement","system allows user to release vendor agreement", "vendor agreement Document is not Released", "fail");

		}

	}
	
public void newPurchaseRequisition() throws Exception {
		
		click(btn_PurchaseRequisitionbutton);
		if(isDisplayed(txt_PurchaseRequisitionbuttonpage)) {
			writeTestResults("verify Purchase Requisition page","view Purchase Requisition page", "Purchase Requisition page is display", "pass");
		}else {
			writeTestResults("verify Purchase Requisition page","view Purchase Requisition page", "Purchase Requisition page is not display", "fail");
		}
		click(btn_newPurchaseRequisitionbutton);
		if(isDisplayed(txt_newPurchaseRequisitionbuttonpage)&&(getText(Header).contentEquals("New"))) {
			writeTestResults("verify New Purchase Requisition page","view New Purchase Requisition page", "New Purchase Requisition page is display", "pass");
			System.out.println("PM_PR_001 Test case is Completed");
		}else {
			writeTestResults("verify New Purchase Requisition page","view New Purchase Requisition page", "New Purchase Requisition page is not display", "fail");
		}
	}

public void addPurchaseRequisition() throws Exception {
	
	click(btn_RequestDate);
	click(btn_selRequestDate);
	selectText(txt_RCurrency, RCurrency);
	
	ScrollDownAruna();
	
	click(btn_Rbtnproduct);
	Thread.sleep(3000);
	sendKeys(txt_Rtxtproduct, pro7.readTestCreation("BatchSpecific"));
	Thread.sleep(3000);
	pressEnter(txt_Rtxtproduct);
	Thread.sleep(3000);
	doubleClick(sel_Rselproduct);
	Thread.sleep(3000);
	click(PRPlus1);
	
	Thread.sleep(3000);
	click(PRPro2);
	Thread.sleep(3000);
	sendKeys(txt_Rtxtproduct, pro7.readTestCreation("BatchFifo"));
	Thread.sleep(3000);
	pressEnter(txt_Rtxtproduct);
	Thread.sleep(3000);
	doubleClick(sel_Rselproduct);
	Thread.sleep(3000);
	click(PRPlus2);
	
	Thread.sleep(3000);
	click(PRPro3);
	Thread.sleep(3000);
	sendKeys(txt_Rtxtproduct, pro7.readTestCreation("Lot"));
	Thread.sleep(3000);
	pressEnter(txt_Rtxtproduct);
	Thread.sleep(3000);
	doubleClick(sel_Rselproduct);
	Thread.sleep(3000);
	click(PRPlus3);
	
	Thread.sleep(3000);
	click(PRPro4);
	Thread.sleep(3000);
	sendKeys(txt_Rtxtproduct, pro7.readTestCreation("SerielSpecific"));
	Thread.sleep(3000);
	pressEnter(txt_Rtxtproduct);
	Thread.sleep(3000);
	doubleClick(sel_Rselproduct);
	Thread.sleep(3000);
	click(PRPlus4);
	
	Thread.sleep(3000);
	click(PRPro5);
	Thread.sleep(3000);
	sendKeys(txt_Rtxtproduct, pro7.readTestCreation("SerielBatchSpecific"));
	Thread.sleep(3000);
	pressEnter(txt_Rtxtproduct);
	Thread.sleep(3000);
	doubleClick(sel_Rselproduct);
	Thread.sleep(3000);
	click(PRPlus5);
	
	Thread.sleep(3000);
	click(PRPro6);
	Thread.sleep(3000);
	sendKeys(txt_Rtxtproduct, pro7.readTestCreation("SerielBatchFifo"));
	Thread.sleep(3000);
	pressEnter(txt_Rtxtproduct);
	Thread.sleep(3000);
	doubleClick(sel_Rselproduct);
	Thread.sleep(3000);
	click(PRPlus6);
	
	Thread.sleep(3000);
	click(PRPro7);
	Thread.sleep(3000);
	sendKeys(txt_Rtxtproduct, pro7.readTestCreation("SerirlFifo"));
	Thread.sleep(3000);
	pressEnter(txt_Rtxtproduct);
	Thread.sleep(3000);
	doubleClick(sel_Rselproduct);
	
	WaitClick(btn_btnCheckout);
	click(btn_btnCheckout);
	WaitClick(btn_btndraft);
	click(btn_btndraft);
	WaitElement(pageDraft);
	if (isDisplayed(pageDraft)){

		writeTestResults("Verify whether system allows user to draft the record","Purchase Requisition Document Draft", "Purchase Requisition Document is Draft", "pass");
	} else {
		writeTestResults("Verify whether system allows user to draft the record","Purchase Requisition Document Draft", "Purchase Requisition Document is Not Draft", "fail");

	}
	
	WaitElement(txt_PurchaseRequisitionNumber);
	number= getText(txt_PurchaseRequisitionNumber);
	WaitClick(btn_goPurchaseRequisitionpage);
	click(btn_goPurchaseRequisitionpage);
	WaitElement(txt_serchPurchaseRequisition);
	sendKeys(txt_serchPurchaseRequisition,number);
	pressEnter(txt_serchPurchaseRequisition);
	WaitElement(sel_selPurchaseRequisitionNumber.replace("no", number));
	doubleClick(sel_selPurchaseRequisitionNumber.replace("no", number));
	
	WaitClick(btn_release);
	click(btn_release);
	
	Thread.sleep(2000);
	trackCode= getText(Header);
	WaitElement(pageRelease);
	if (isDisplayed(pageRelease)){

		writeTestResults("Verify whether system allows user to draft & Releas the record","Purchase Requisition Document Released", "Purchase Requisition Document is Released", "pass");
		System.out.println("PM_PR_002 Test case is Completed");
	} else {
		writeTestResults("Verify whether system allows user to draft & Releas the record","Purchase Requisition Document Released", "Purchase Requisition Document is not Released", "fail");

	}
}

public void relesePurchaseRequisition() throws Exception {
	
	click(btn_PurchaseRequisitionbutton);
	click(btn_btnPurchaseRequisition);
	click(btn_release);
}

public void checkUnitPrice() throws Exception {
	
	click(btn_PurchaseRequisitionbutton);
	if(isDisplayed(txt_PurchaseRequisitionbuttonpage)) {
		writeTestResults("verify Purchase Requisition page","view Purchase Requisition page", "Purchase Requisition page is display", "pass");
	}else {
		writeTestResults("verify Purchase Requisition page","view Purchase Requisition page", "Purchase Requisition page is not display", "fail");
	}
	click(btn_newPurchaseRequisitionbutton);
	if(isDisplayed(txt_newPurchaseRequisitionbuttonpage)&&(getText(Header).contentEquals("New"))) {
		writeTestResults("verify New Purchase Requisition page","view New Purchase Requisition page", "New Purchase Requisition page is display", "pass");
	}else {
		writeTestResults("verify New Purchase Requisition page","view New Purchase Requisition page", "New Purchase Requisition page is not display", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_Rbtnproduct);
	Thread.sleep(3000);
	sendKeys(txt_Rtxtproduct, pro7.readTestCreation("BatchSpecific"));
	Thread.sleep(3000);
	pressEnter(txt_Rtxtproduct);
	Thread.sleep(3000);
	doubleClick(sel_Rselproduct);
	Thread.sleep(3000);
	click(PRPlus1);
	
	Thread.sleep(3000);
	click(PRPro2);
	Thread.sleep(3000);
	sendKeys(txt_Rtxtproduct, pro7.readTestCreation("BatchFifo"));
	Thread.sleep(3000);
	pressEnter(txt_Rtxtproduct);
	Thread.sleep(3000);
	doubleClick(sel_Rselproduct);
	Thread.sleep(3000);
	click(PRPlus2);
	
	Thread.sleep(3000);
	click(PRPro3);
	Thread.sleep(3000);
	sendKeys(txt_Rtxtproduct, pro7.readTestCreation("Lot"));
	Thread.sleep(3000);
	pressEnter(txt_Rtxtproduct);
	Thread.sleep(3000);
	doubleClick(sel_Rselproduct);
	Thread.sleep(3000);
	click(PRPlus3);
	
	Thread.sleep(3000);
	click(PRPro4);
	Thread.sleep(3000);
	sendKeys(txt_Rtxtproduct, pro7.readTestCreation("SerielSpecific"));
	Thread.sleep(3000);
	pressEnter(txt_Rtxtproduct);
	Thread.sleep(3000);
	doubleClick(sel_Rselproduct);
	Thread.sleep(3000);
	click(PRPlus4);
	
	Thread.sleep(3000);
	click(PRPro5);
	Thread.sleep(3000);
	sendKeys(txt_Rtxtproduct, pro7.readTestCreation("SerielBatchSpecific"));
	Thread.sleep(3000);
	pressEnter(txt_Rtxtproduct);
	Thread.sleep(3000);
	doubleClick(sel_Rselproduct);
	Thread.sleep(3000);
	click(PRPlus5);
	
	Thread.sleep(3000);
	click(PRPro6);
	Thread.sleep(3000);
	sendKeys(txt_Rtxtproduct, pro7.readTestCreation("SerielBatchFifo"));
	Thread.sleep(3000);
	pressEnter(txt_Rtxtproduct);
	Thread.sleep(3000);
	doubleClick(sel_Rselproduct);
	Thread.sleep(3000);
	click(PRPlus6);
	
	Thread.sleep(3000);
	click(PRPro7);
	Thread.sleep(3000);
	sendKeys(txt_Rtxtproduct, pro7.readTestCreation("SerirlFifo"));
	Thread.sleep(3000);
	pressEnter(txt_Rtxtproduct);
	Thread.sleep(3000);
	doubleClick(sel_Rselproduct);
	
	
	click(btn_RbtnGridMasterInfo);
	click(btn_RbtnProductPurchasingHistory);
	Thread.sleep(2000);
	click(btn_RPHclose);
	Thread.sleep(2000);
	if(getText(txt_RUnitPrice).equals(getText(txt_RPHUnitPrice))) {
		writeTestResults("Verify whether system displays LPP of product accurately","unit price and purchase price is same", "prices are same", "pass");
		System.out.println("PM_PR_004 Test case is Completed");
	}else {
		writeTestResults("Verify whether system displays LPP of product accurately","unit price and purchase price is same", "prices are not same", "fail");
	}
	
}


public void ConvertToPOaction() throws Exception {
	
	newPurchaseRequisition();
	addPurchaseRequisition();
	
	Thread.sleep(6000);
	clickNavigation();
	clickProbutton();

	click(btn_PurchaseRequisitionbutton);
	if(isDisplayed(txt_PurchaseRequisitionbuttonpage)) {
		writeTestResults("verify Purchase Requisition page","view Purchase Requisition page", "Purchase Requisition page is display", "pass");
	}else {
		writeTestResults("verify Purchase Requisition page","view Purchase Requisition page", "Purchase Requisition page is not display", "fail");
	}
	
	click(btn_goPurchaseRequisitionpage);
	sendKeys(txt_serchPurchaseRequisition,number);
	pressEnter(txt_serchPurchaseRequisition);
	Thread.sleep(3000);
	click(sel_selPurchaseRequisitionNumber.replace("no", number));
	

	click(btn_btnAction);
	click(btn_btnConverttoPurchaseOrder);
	click(btn_btnGeneratePO);
	
	Thread.sleep(2000);
	click(btn_btnPOtoShipmentCosting);
	
	switchWindow();
	click(btn_RRDclose);
	
	driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
	click(btn_btnVendor);
	sendKeys(txt_txtVendor, txtVendor);
	Thread.sleep(3000);
	pressEnter(txt_txtVendor);
	Thread.sleep(3000);
	doubleClick(sel_selVendor);

	click(btn_btnBillingAddress);
	sendKeys(txt_txtBillingAddress, txtBillingAddress);
	click(btn_btnApply);
	click(btn_btnShippingAddress);
	sendKeys(txt_txtShippingAddress, txtShippingAddress);
	click(btn_btnApply);

	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	
	
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	Thread.sleep(5000);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify Purchase Order Document Released","Purchase Order Document Released", "Purchase Order Document is Released", "pass");
		System.out.println("PM_PR_005 Test case is Completed");
	} else {
		writeTestResults("verify Purchase Order Document Released","Purchase Order Document Released", "Purchase Order Document is not Released", "fail");
	}
}

public void completePurchaseOrderToShipmentCosting() throws Exception {

	WaitClick(btn_PurchaseOrderbutton);
	click(btn_PurchaseOrderbutton);
	
	WaitElement(txt_PurchaseOrderpage);
	if(isDisplayed(txt_PurchaseOrderpage)) {
		writeTestResults("verify Purchase Order page","view Purchase Order page", "Purchase Order page is display", "pass");
	}else {
		writeTestResults("verify Purchase Order page","view Purchase Order page", "Purchase Order page is not display", "fail");
	}
	
	WaitClick(btn_newPurchaseOrderbutton);
	click(btn_newPurchaseOrderbutton);
	
	WaitClick(btn_btnPOtoShipmentCosting);
	click(btn_btnPOtoShipmentCosting);
	
	WaitElement(txt_newPurchaseOrderpage);
	if(isDisplayed(txt_newPurchaseOrderpage)&&(getText(Status).contentEquals("New"))) {
		writeTestResults("verify  New Purchase Order page","view  New Purchase Order page", " New Purchase Order page is display", "pass");
	}else {
		writeTestResults("verify  New Purchase Order page","view  New Purchase Order page", " New Purchase Order page is not display", "fail");
	}
	
	WaitClick(btn_PbtnVendor);
	click(btn_PbtnVendor);
	sendKeys(txt_txtVendor, txtVendor);
	pressEnter(txt_txtVendor);
	Thread.sleep(3000);
	doubleClick(sel_selVendor);
	
	click(btn_btnBillingAddress);
	sendKeys(txt_txtBillingAddress, txtBillingAddress);
	click(btn_btnApply);
	click(btn_btnShippingAddress);
	sendKeys(txt_txtShippingAddress, txtShippingAddress);
	click(btn_btnApply);
	click(btn_Pbtndetails);
	click(btn_btnPInvoiceAccount);
	
	WaitElement(txt_txtVendor);
	sendKeys(txt_txtVendor, txtVendor);
	pressEnter(txt_txtVendor);
	Thread.sleep(3000);
	doubleClick(sel_selVendor);
	click(btn_btnsummary);
	
	ScrollDownAruna();
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("BatchSpecific"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse1, "WH-PARTS [Main Warehouse - Parts]");

	WaitClick(btn_pro7POLook1);
	click(btn_pro7POLook1);
	WaitElement(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval1= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", "").replace(",", ""));
	System.out.println("poval1:"+poval1);
	JavascriptExecutor j9 = (JavascriptExecutor)driver;
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("BatchFifo"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse2, "WH-PARTS [Main Warehouse - Parts]");
	
	WaitClick(btn_pro7POLook2);
	click(btn_pro7POLook2);
	WaitElement(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval2= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", "").replace(",", ""));
	System.out.println("poval2:"+poval2);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("Lot"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse3, "WH-PARTS [Main Warehouse - Parts]");
	
	WaitClick(btn_pro7POLook3);
	click(btn_pro7POLook3);
	WaitElement(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval3= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", "").replace(",", ""));
	System.out.println("poval3:"+poval3);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("SerielSpecific"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse4, "WH-PARTS [Main Warehouse - Parts]");
	
	WaitClick(btn_pro7POLook4);
	click(btn_pro7POLook4);
	WaitElement(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval4= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", "").replace(",", ""));
	System.out.println("poval4:"+poval4);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("SerielBatchSpecific"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse5, "WH-PARTS [Main Warehouse - Parts]");
	
	WaitClick(btn_pro7POLook5);
	click(btn_pro7POLook5);
	WaitElement(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval5= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", "").replace(",", ""));
	System.out.println("poval5:"+poval5);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("SerielBatchFifo"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse6, "WH-PARTS [Main Warehouse - Parts]");
	
	WaitClick(btn_pro7POLook6);
	click(btn_pro7POLook6);
	WaitElement(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval6= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", "").replace(",", ""));
	System.out.println("poval6:"+poval6);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("SerirlFifo"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse7, "WH-PARTS [Main Warehouse - Parts]");
	
	WaitClick(btn_pro7POLook7);
	click(btn_pro7POLook7);
	WaitElement(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval7= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", "").replace(",", ""));
	System.out.println("poval7:"+poval7);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");

	WaitClick(btn_btnCheckout);
	click(btn_btnCheckout);
	WaitClick(btn_btndraft);
	click(btn_btndraft);
	WaitElement(pageDraft);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Order Document Draft","Purchase Order Document Draft", "Purchase Order Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Order Document Draft","Purchase Order Document Draft", "Purchase Order Document is not Draft", "fail");
	}
	
	WaitClick(btn_release);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	PurchaseOrderNo=trackCode;
	
	WaitElement(btn_btnGotopage);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify Purchase Order Document Released","Purchase Order Document Released", "Purchase Order Document is Released", "pass");
	} else {
		writeTestResults("verify Purchase Order Document Released","Purchase Order Document Released", "Purchase Order Document is not Released", "fail");
	}
	
	WaitClick(btn_btnGotopage);
	click(btn_btnGotopage);
	
	switchWindow();
	WaitElement(txt_InboundShipmentpage);
	if(isDisplayed(txt_InboundShipmentpage)) {
		writeTestResults("User enter Inbound Shipment page","view  Inbound Shipment page", "Inbound Shipment page is display", "pass");
	}else {
		writeTestResults("User enter Inbound Shipment page","view  Inbound Shipment page", "Inbound Shipment page is not display", "fail");
	}
	
	WaitClick(btn_btnCheckout);
	click(btn_btnCheckout);
	WaitClick(btn_btndraft);
	click(btn_btndraft);
	
	WaitElement(pageDraft);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify enter Inbound Shipment Document Draft","enter Inbound Shipment Document Draft", "enter Inbound Shipment Document is Draft", "pass");
	} else {
		writeTestResults("verify enter Inbound Shipment Document Draft","enter Inbound Shipment Document Draft", "enter Inbound Shipment Document is not Draft", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnSerialBatch);
	

	Thread.sleep(4000);
	Actions action1 = new Actions(driver);
	WebElement a1 = driver.findElement(By.xpath(Cap1));
	action1.moveToElement(a1).build().perform();
	click(Cap1);
	LocalTime p1 = LocalTime.now();
	sendKeys(BatchNu, "BN"+p1);
	sendKeys(BatchLotNu, "LN"+p1);
	click(BatchExpiryDate);
	WaitElement(nextdate);
	selectText(nextdate, "2030");
	WaitClick(BatchExpiryDateSel);
	click(BatchExpiryDateSel);
	pressEnter(BatchNu);
	WaitClick(btn_updateSerial);
	click(btn_updateSerial);
	
	Actions action2 = new Actions(driver);
	WebElement a2 = driver.findElement(By.xpath(Cap2));
	action2.moveToElement(a2).build().perform();
	click(Cap2);
	LocalTime p2 = LocalTime.now();
	sendKeys(BatchNu, "BN"+p2);
	sendKeys(BatchLotNu, "LN"+p2);
	click(BatchExpiryDate);
	WaitElement(nextdate);
	selectText(nextdate, "2030");
	WaitClick(BatchExpiryDateSel);
	click(BatchExpiryDateSel);
	pressEnter(BatchNu);
	WaitClick(btn_updateSerial);
	click(btn_updateSerial);

	Actions action4 = new Actions(driver);
	WebElement a4 = driver.findElement(By.xpath(Cap4));
	action4.moveToElement(a4).build().perform();
	click(Cap4);
	WaitClick(btn_SerialRange);
	click(btn_SerialRange);
	JavascriptExecutor j1 = (JavascriptExecutor)driver;
	j1.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",5)");
	LocalTime p3 = LocalTime.now();
	sendKeys(SerialFromNu, "SN"+p3);
	sendKeys(SerialLotNu, "LN"+p3);
	WaitClick(SerialExpiryDate);
	click(SerialExpiryDate);
	WaitElement(nextdate);
	selectText(nextdate, "2030");
	WaitClick(BatchExpiryDateSel);
	click(BatchExpiryDateSel);
	pressEnter(SerialFromNu);
	WaitClick(btn_updateSerial);
	click(btn_updateSerial);

	Actions action5 = new Actions(driver);
	WebElement a5 = driver.findElement(By.xpath(Cap5));
	action5.moveToElement(a5).build().perform();
	click(Cap5);
	WaitClick(btn_SerialRange);
	click(btn_SerialRange);
	JavascriptExecutor j2 = (JavascriptExecutor)driver;
	j2.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",5)");
	LocalTime p4 = LocalTime.now();
	sendKeys(SerialFromNu, "SN"+p4);
	sendKeys(SerialLotNu, "LN"+p4);
	sendKeys(SerialBatchNu, "BN"+p4);
	WaitClick(SerialExpiryDate);
	click(SerialExpiryDate);
	WaitElement(nextdate);
	selectText(nextdate, "2030");
	WaitClick(BatchExpiryDateSel);
	click(BatchExpiryDateSel);
	pressEnter(SerialFromNu);
	WaitClick(btn_updateSerial);
	click(btn_updateSerial);

	Actions action6 = new Actions(driver);
	WebElement a6 = driver.findElement(By.xpath(Cap6));
	action6.moveToElement(a6).build().perform();
	click(Cap6);
	WaitClick(btn_SerialRange);
	click(btn_SerialRange);
	JavascriptExecutor j3 = (JavascriptExecutor)driver;
	j3.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",5)");
	LocalTime p5 = LocalTime.now();
	sendKeys(SerialFromNu, "SN"+p5);
	sendKeys(SerialLotNu, "LN"+p5);
	sendKeys(SerialBatchNu, "BN"+p5);
	WaitClick(SerialExpiryDate);
	click(SerialExpiryDate);
	WaitElement(nextdate);
	selectText(nextdate, "2030");
	WaitClick(BatchExpiryDateSel);
	click(BatchExpiryDateSel);
	pressEnter(SerialFromNu);
	WaitClick(btn_updateSerial);
	click(btn_updateSerial);
	
	Actions action7 = new Actions(driver);
	WebElement a7 = driver.findElement(By.xpath(Cap7));
	action7.moveToElement(a7).build().perform();
	click(Cap7);
	WaitClick(btn_SerialRange);
	click(btn_SerialRange);
	JavascriptExecutor j4 = (JavascriptExecutor)driver;
	j4.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",5)");
	LocalTime p6 = LocalTime.now();
	sendKeys(SerialFromNu, "SN"+p6);
	sendKeys(SerialLotNu, "LN"+p6);
	WaitClick(SerialExpiryDate);
	click(SerialExpiryDate);
	WaitElement(nextdate);
	selectText(nextdate, "2030");
	WaitClick(BatchExpiryDateSel);
	click(BatchExpiryDateSel);
	pressEnter(SerialFromNu);
	WaitClick(btn_updateSerial);
	click(btn_updateSerial);
	
	WaitClick(btn_closeProductList);
	click(btn_closeProductList);
	
	
	WaitClick(btn_release);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	InboundShipmentNo=trackCode;
	WaitElement(btn_btnGotopage);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify enter Inbound Shipment Document Released","enter Inbound Shipment Document Released", "enter Inbound Shipment Document is Released", "pass");
	} else {
		writeTestResults("verify enter Inbound Shipment Document Released","enter Inbound Shipment Document Released", "enter Inbound Shipment Document is not Released", "fail");
	}
	
	WaitClick(btn_btnGotopage);
	click(btn_btnGotopage);

	switchWindow();
	if(isDisplayed(txt_PurchaseInvoicepage)) {
		writeTestResults("User enter Purchase Invoice page","view  Purchase Invoice page", "Purchase Invoice page is display", "pass");
	}else {
		writeTestResults("User enter Purchase Invoice page","view  Purchase Invoice page", "Purchase Invoice page is not display", "fail");
	}
	click(btn_btnCheckout);
	WaitClick(btn_btndraft);
	click(btn_btndraft);
	WaitElement(pageDraft);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Invoice Document Draft","Purchase Invoice Document Draft", "Purchase Invoice Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Invoice Document Draft","Purchase Invoice Document Draft", "Purchase Invoice Document is not Draft", "fail");
	}
	
	WaitClick(btn_pro7PILook1);
	click(btn_pro7PILook1);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	pival1= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", "").replace(",", ""));
	System.out.println("pival1:"+pival1);
	Thread.sleep(3000);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitClick(btn_pro7PILook2);
	click(btn_pro7PILook2);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	pival2= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", "").replace(",", ""));
	System.out.println("pival2:"+pival2);
	Thread.sleep(3000);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitClick(btn_pro7PILook3);
	click(btn_pro7PILook3);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	pival3= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", "").replace(",", ""));
	System.out.println("pival3:"+pival3);
	Thread.sleep(3000);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitClick(btn_pro7PILook4);
	click(btn_pro7PILook4);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	pival4= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("pival4:"+pival4);
	Thread.sleep(3000);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitClick(btn_pro7PILook5);
	click(btn_pro7PILook5);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	pival5= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("pival5:"+pival5);
	Thread.sleep(3000);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitClick(btn_pro7PILook6);
	click(btn_pro7PILook6);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	pival6= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("pival6:"+pival6);
	Thread.sleep(3000);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitClick(btn_pro7PILook7);
	click(btn_pro7PILook7);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	pival7= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("pival7:"+pival7);
	Thread.sleep(3000);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitClick(btn_release);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	PurchaseInvoiceNo=trackCode;
	WaitElement(btn_btnGotopage);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify Purchase Invoice Document Released","Purchase Invoice Document Released", "Purchase Invoice Document is Released", "pass");
	} else {
		writeTestResults("verify Purchase Invoice Document Released","Purchase Invoice Document Released", "Purchase Invoice Document is not Released", "fail");
	}
	
	WaitClick(btn_btnGotopage);
	click(btn_btnGotopage);
	
	switchWindow();
	if(isDisplayed(txt_ShipmentCostingInformationpage)) {
		writeTestResults("User enter Shipment Costing Information page","view  Shipment Costing Information page", "Shipment Costing Information page is display", "pass");
	}else {
		writeTestResults("User enter Shipment Costing Information page","view  Shipment Costing Information page", "Shipment Costing Information is not display", "fail");
	}
	WaitClick(btn_btndraft);
	click(btn_btndraft);
	WaitElement(pageDraft);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Shipment Costing Information Document Draft","Shipment Costing Information Document Draft", "Shipment Costing Information Document is Draft", "pass");
	} else {
		writeTestResults("verify Shipment Costing Information Document Draft","Shipment Costing Information Document Draft", "Shipment Costing Information Document is not Draft", "fail");

	}
	
	WaitClick(btn_release);
	click(btn_release);
	explicitWait(pageRelease,40);
	Thread.sleep(3000);
	trackCode= getText(Header);
	ShipmentCostingInformationNo=trackCode;
	
	WaitElement(pageRelease);
	if (isDisplayed(pageRelease)){

		writeTestResults("verify Shipment Costing Information Document Released","Shipment Costing Information Document Released", "Shipment Costing Information Document is Released", "pass");
		System.out.println("PM_PO_001 Test case is Completed");
	} else {
		writeTestResults("verify Shipment Costing Information Document Released","Shipment Costing Information Document Released", "Shipment Costing Information Document is not Released", "fail");

	}
	int testquantity = Integer.parseInt(TestQuantity);
	if (pival1==(poval1+testquantity)){
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Correct for BATCH-SPECIFIC Product", "pass");
	} else {
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Not Correct for BATCH-SPECIFIC Product", "fail");
	}
	if (pival2==(poval2+testquantity)){
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Correct for BATCH-FIFO Product", "pass");
	} else {
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Not Correct for BATCH-FIFO Product", "fail");
	}
	if (pival3==(poval3+testquantity)){
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Correct for LOT Product", "pass");
	} else {
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Not Correct for LOT Product", "fail");
	}
	if (pival4==(poval4+testquantity)){
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Correct for SERIEL-SPECIFIC Product", "pass");
	} else {
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Not Correct for SERIEL-SPECIFIC Product", "fail");
	}
	if (pival5==(poval5+testquantity)){
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Correct for SERIEL-BATCH-SPECIFIC Product", "pass");
	} else {
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Not Correct for SERIEL-BATCH-SPECIFIC Product", "fail");
	}
	if (pival6==(poval6+testquantity)){
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Correct for SERIEL-BATCH-FIFO Product", "pass");
	} else {
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Not Correct for SERIEL-BATCH-FIFO Product", "fail");
	}
	if (pival7==(poval7+testquantity)){
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Correct for SERIEL-FIFO Product", "pass");
	} else {
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Not Correct for SERIEL-FIFO Product", "fail");
	}
}

public void completePurchaseOrderToPI() throws Exception {
	
	click(btn_PurchaseOrderbutton);
	if(isDisplayed(txt_PurchaseOrderpage)) {
		writeTestResults("verify Purchase Order page","view Purchase Order page", "Purchase Order page is display", "pass");
	}else {
		writeTestResults("verify Purchase Order page","view Purchase Order page", "Purchase Order page is not display", "fail");
	}
	click(btn_newPurchaseOrderbutton);
	Thread.sleep(2000);
	click(btn_btnPOtoPIService);
	if(isDisplayed(txt_newPurchaseOrderpage)&&(getText(Status).contentEquals("New"))) {
		writeTestResults("verify  New Purchase Order page","view  New Purchase Order page", " New Purchase Order page is display", "pass");
	}else {
		writeTestResults("verify  New Purchase Order page","view  New Purchase Order page", " New Purchase Order page is not display", "fail");
	}
	driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
	click(btn_PbtnVendor);
	sendKeys(txt_txtVendor, txtVendor);
	Thread.sleep(3000);
	pressEnter(txt_txtVendor);
	Thread.sleep(3000);
	doubleClick(sel_selVendor);
	
	click(btn_btnBillingAddress);
	sendKeys(txt_txtBillingAddress, txtBillingAddress);
	click(btn_btnApply);
	click(btn_btnShippingAddress);
	sendKeys(txt_txtShippingAddress, txtShippingAddress);
	click(btn_btnApply);
	click(btn_Pbtndetails);
	
	click(btn_btnPInvoiceAccount);
	
	driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
	sendKeys(txt_txtVendor, txtVendor);
	Thread.sleep(3000);
	pressEnter(txt_txtVendor);
	Thread.sleep(3000);
	doubleClick(sel_selVendor);
	click(btn_btnsummary);
	
	ScrollDownAruna();
	
	Thread.sleep(4000);
	sendKeys(Product, ptxtSproduct);
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse1, "WH-PARTS [Main Warehouse - Parts]");
	
	sendKeys(txt_txtVendorReferenceNo, txtVendorReferenceNo);
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Order Document Draft","Purchase Order Document Draft", "Purchase Order Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Order Document Draft","Purchase Order Document Draft", "Purchase Order Document is not Draft", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	Thread.sleep(5000);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify Purchase Order Document Released","Purchase Order Document Released", "Purchase Order Document is Released", "pass");
	} else {
		writeTestResults("verify Purchase Order Document Released","Purchase Order Document Released", "Purchase Order Document is not Released", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnGotopage);
	
	switchWindow();
	if(isDisplayed(txt_PurchaseInvoicepage)) {
		writeTestResults("User enter Purchase Invoice page","view  Purchase Invoice page", "Purchase Invoice page is display", "pass");
	}else {
		writeTestResults("User enter Purchase Invoice page","view  Purchase Invoice page", "Purchase Invoice page is not display", "fail");
	}
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Invoice Document Draft","Purchase Invoice Document Draft", "Purchase Invoice Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Invoice Document Draft","Purchase Invoice Document Draft", "Purchase Invoice Document is not Draft", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	Thread.sleep(5000);
	if (isDisplayed(pageRelease)){

		writeTestResults("verify Purchase Invoice Document Released","Purchase Invoice Document Released", "Purchase Invoice Document is Released", "pass");
		System.out.println("PM_PO_002 Test case is Completed");
	} else {
		writeTestResults("verify Purchase Invoice Document Released","Purchase Invoice Document Released", "Purchase Invoice Document is not Released", "fail");

	}
	
}

public void completePurchaseOrderToPurchaseInvoice() throws Exception {
	click(btn_PurchaseOrderbutton);
	if(isDisplayed(txt_PurchaseOrderpage)) {
		writeTestResults("verify Purchase Order page","view Purchase Order page", "Purchase Order page is display", "pass");
	}else {
		writeTestResults("verify Purchase Order page","view Purchase Order page", "Purchase Order page is not display", "fail");
	}
	
	WaitClick(btn_newPurchaseOrderbutton);
	click(btn_newPurchaseOrderbutton);
	WaitClick(btn_btndropdown);
	click(btn_btndropdown);
	WaitClick(btn_btnLPOtoShipmentCosting);
	click(btn_btnLPOtoShipmentCosting);
	
	
	if(isDisplayed(txt_newPurchaseOrderpage)&&(getText(Status).contentEquals("New"))) {
		writeTestResults("verify  New Purchase Order page","view  New Purchase Order page", " New Purchase Order page is display", "pass");
	}else {
		writeTestResults("verify  New Purchase Order page","view  New Purchase Order page", " New Purchase Order page is not display", "fail");
	}
	
	WaitClick(btn_PbtnVendor);
	click(btn_PbtnVendor);
	sendKeys(txt_txtVendor, txtVendor);
	pressEnter(txt_txtVendor);
	Thread.sleep(3000);
	doubleClick(sel_selVendor);
	
	click(btn_btnBillingAddress);
	sendKeys(txt_txtBillingAddress, txtBillingAddress);
	click(btn_btnApply);
	click(btn_btnShippingAddress);
	sendKeys(txt_txtShippingAddress, txtShippingAddress);
	click(btn_btnApply);
	
	click(btn_Pbtndetails);

	click(btn_btnPInvoiceAccount);
	
	WaitElement(txt_txtVendor);
	sendKeys(txt_txtVendor, txtVendor);
	pressEnter(txt_txtVendor);
	Thread.sleep(3000);
	doubleClick(sel_selVendor);
	
	click(btn_btnsummary);
	
	ScrollDownAruna();
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("BatchSpecific"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse1, "WH-PARTS [Main Warehouse - Parts]");

	WaitClick(btn_pro7POLook1);
	click(btn_pro7POLook1);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval1= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("poval1:"+poval1);
	JavascriptExecutor j9 = (JavascriptExecutor)driver;
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("BatchFifo"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse2, "WH-PARTS [Main Warehouse - Parts]");
	
	WaitClick(btn_pro7POLook2);
	click(btn_pro7POLook2);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval2= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("poval2:"+poval2);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("Lot"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse3, "WH-PARTS [Main Warehouse - Parts]");
	
	WaitClick(btn_pro7POLook3);
	click(btn_pro7POLook3);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval3= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("poval3:"+poval3);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("SerielSpecific"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse4, "WH-PARTS [Main Warehouse - Parts]");
	
	WaitClick(btn_pro7POLook4);
	click(btn_pro7POLook4);
	WaitClick(btn_POProductAvailability);;
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval4= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("poval4:"+poval4);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("SerielBatchSpecific"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse5, "WH-PARTS [Main Warehouse - Parts]");
	
	WaitClick(btn_pro7POLook5);
	click(btn_pro7POLook5);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval5= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("poval5:"+poval5);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("SerielBatchFifo"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse6, "WH-PARTS [Main Warehouse - Parts]");
	
	WaitClick(btn_pro7POLook6);
	click(btn_pro7POLook6);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval6= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("poval6:"+poval6);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("SerirlFifo"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse7, "WH-PARTS [Main Warehouse - Parts]");
	
	WaitClick(btn_pro7POLook7);
	click(btn_pro7POLook7);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval7= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("poval7:"+poval7);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitClick(btn_btnCheckout);
	click(btn_btnCheckout);
	
	WaitClick(btn_btndraft);
	click(btn_btndraft);
	
	WaitElement(pageDraft);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Order Document Draft","Purchase Order Document Draft", "Purchase Order Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Order Document Draft","Purchase Order Document Draft", "Purchase Order Document is not Draft", "fail");
	}
	
	WaitClick(btn_release);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	
	WaitElement(btn_btnGotopage);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify Purchase Order Document Released","Purchase Order Document Released", "Purchase Order Document is Released", "pass");
	} else {
		writeTestResults("verify Purchase Order Document Released","Purchase Order Document Released", "Purchase Order Document is not Released", "fail");
	}
	
	WaitClick(btn_btnGotopage);
	click(btn_btnGotopage);
	
	switchWindow();
	if(isDisplayed(txt_PurchaseInvoicepage)) {
		writeTestResults("User enter Purchase Invoice page","view  Purchase Invoice page", "Purchase Invoice page is display", "pass");
	}else {
		writeTestResults("User enter Purchase Invoice page","view  Purchase Invoice page", "Purchase Invoice page is not display", "fail");
	}
	
	WaitClick(btn_btnCheckout);
	click(btn_btnCheckout);
	WaitClick(btn_btndraft);
	click(btn_btndraft);
	
	WaitElement(pageDraft);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Invoice Document Draft","Purchase Invoice Document Draft", "Purchase Invoice Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Invoice Document Draft","Purchase Invoice Document Draft", "Purchase Invoice Document is not Draft", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	
	WaitElement(btn_btnGotopage);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify Purchase Invoice Document Released","Purchase Invoice Document Released", "Purchase Invoice Document is Released", "pass");
	} else {
		writeTestResults("verify Purchase Invoice Document Released","Purchase Invoice Document Released", "Purchase Invoice Document is not Released", "fail");
	}
	
	WaitClick(btn_btnGotopage);
	click(btn_btnGotopage);
	
	switchWindow();
	if(isDisplayed(txt_InboundShipmentpage)) {
		writeTestResults("User enter Inbound Shipment page","view  Inbound Shipment page", "Inbound Shipment page is display", "pass");
	}else {
		writeTestResults("User enter Inbound Shipment page","view  Inbound Shipment page", "Inbound Shipment page is not display", "fail");
	}
	
	WaitClick(btn_btnCheckout);
	click(btn_btnCheckout);
	
	WaitClick(btn_btndraft);
	click(btn_btndraft);
	
	WaitElement(pageDraft);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify enter Inbound Shipment Document Draft","enter Inbound Shipment Document Draft", "enter Inbound Shipment Document is Draft", "pass");
	} else {
		writeTestResults("verify enter Inbound Shipment Document Draft","enter Inbound Shipment Document Draft", "enter Inbound Shipment Document is not Draft", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnSerialBatch);
	
	Thread.sleep(6000);
	Actions action1 = new Actions(driver);
	WebElement a1 = driver.findElement(By.xpath(Cap1));
	action1.moveToElement(a1).build().perform();
	WaitClick(Cap1);
	click(Cap1);
	LocalTime p1 = LocalTime.now();
	sendKeys(BatchNu, "BN"+p1);
	sendKeys(BatchLotNu, "LN"+p1);
	WaitClick(BatchExpiryDate);
	click(BatchExpiryDate);
	WaitElement(nextdate);
	selectText(nextdate, "2030");
	WaitClick(BatchExpiryDateSel);
	click(BatchExpiryDateSel);
	pressEnter(BatchNu);
	WaitClick(btn_updateSerial);
	click(btn_updateSerial);

	Actions action2 = new Actions(driver);
	WebElement a2 = driver.findElement(By.xpath(Cap2));
	action2.moveToElement(a2).build().perform();
	WaitClick(Cap2);
	click(Cap2);
	LocalTime p2 = LocalTime.now();
	sendKeys(BatchNu, "BN"+p2);
	sendKeys(BatchLotNu, "LN"+p2);
	WaitClick(BatchExpiryDate);
	click(BatchExpiryDate);
	WaitElement(nextdate);
	selectText(nextdate, "2030");
	WaitClick(BatchExpiryDateSel);
	click(BatchExpiryDateSel);
	pressEnter(BatchNu);
	WaitClick(btn_updateSerial);
	click(btn_updateSerial);

	Actions action4 = new Actions(driver);
	WebElement a4 = driver.findElement(By.xpath(Cap4));
	action4.moveToElement(a4).build().perform();
	WaitClick(Cap4);
	click(Cap4);
	WaitClick(btn_SerialRange);
	click(btn_SerialRange);
	JavascriptExecutor j1 = (JavascriptExecutor)driver;
	j1.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",5)");
	LocalTime p3 = LocalTime.now();
	sendKeys(SerialFromNu, "SN"+p3);
	sendKeys(SerialLotNu, "LN"+p3);
	WaitClick(SerialExpiryDate);
	click(SerialExpiryDate);
	WaitElement(nextdate);
	selectText(nextdate, "2030");
	WaitClick(BatchExpiryDateSel);
	click(BatchExpiryDateSel);
	pressEnter(SerialFromNu);
	WaitClick(btn_updateSerial);
	click(btn_updateSerial);
	
	Actions action5 = new Actions(driver);
	WebElement a5 = driver.findElement(By.xpath(Cap5));
	action5.moveToElement(a5).build().perform();
	WaitClick(Cap5);
	click(Cap5);
	WaitClick(btn_SerialRange);
	click(btn_SerialRange);
	JavascriptExecutor j2 = (JavascriptExecutor)driver;
	j2.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",5)");
	LocalTime p4 = LocalTime.now();
	sendKeys(SerialFromNu, "SN"+p4);
	sendKeys(SerialLotNu, "LN"+p4);
	sendKeys(SerialBatchNu, "BN"+p4);
	WaitClick(SerialExpiryDate);
	click(SerialExpiryDate);
	WaitElement(nextdate);
	selectText(nextdate, "2030");
	WaitClick(BatchExpiryDateSel);
	click(BatchExpiryDateSel);
	pressEnter(SerialFromNu);
	WaitClick(btn_updateSerial);
	click(btn_updateSerial);
	
	Actions action6 = new Actions(driver);
	WebElement a6 = driver.findElement(By.xpath(Cap6));
	action6.moveToElement(a6).build().perform();
	WaitClick(Cap6);
	click(Cap6);
	WaitClick(btn_SerialRange);
	click(btn_SerialRange);
	JavascriptExecutor j3 = (JavascriptExecutor)driver;
	j3.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",5)");
	LocalTime p5 = LocalTime.now();
	sendKeys(SerialFromNu, "SN"+p5);
	sendKeys(SerialLotNu, "LN"+p5);
	sendKeys(SerialBatchNu, "BN"+p5);
	WaitClick(SerialExpiryDate);
	click(SerialExpiryDate);
	WaitElement(nextdate);
	selectText(nextdate, "2030");
	WaitClick(BatchExpiryDateSel);
	click(BatchExpiryDateSel);
	pressEnter(SerialFromNu);
	WaitClick(btn_updateSerial);
	click(btn_updateSerial);
	
	Actions action7 = new Actions(driver);
	WebElement a7 = driver.findElement(By.xpath(Cap7));
	action7.moveToElement(a7).build().perform();
	WaitClick(Cap7);
	click(Cap7);
	WaitClick(btn_SerialRange);
	click(btn_SerialRange);
	JavascriptExecutor j4 = (JavascriptExecutor)driver;
	j4.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",5)");
	LocalTime p6 = LocalTime.now();
	sendKeys(SerialFromNu, "SN"+p6);
	sendKeys(SerialLotNu, "LN"+p6);
	WaitClick(SerialExpiryDate);
	click(SerialExpiryDate);
	WaitElement(nextdate);
	selectText(nextdate, "2030");
	WaitClick(BatchExpiryDateSel);
	click(BatchExpiryDateSel);
	pressEnter(SerialFromNu);
	WaitClick(btn_updateSerial);
	click(btn_updateSerial);
	
	WaitClick(btn_closeProductList);
	click(btn_closeProductList);
	
	WaitClick(btn_release);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	
	WaitElement(btn_btnGotopage);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify Inbound Shipment Document Released","Inbound Shipment Document Released", "Inbound Shipment Document is Released", "pass");
		System.out.println("PM_PO_003 Test case is Completed");
	} else {
		writeTestResults("verify Inbound Shipment Document Released","Inbound Shipment Document Released", "Inbound Shipment Document is not Released", "fail");
	}
	
	WaitClick(btn_headerclose);
	click(btn_headerclose);
	
	WaitClick(btn_pro7ISLook1);
	click(btn_pro7ISLook1);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	isval1= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("isval1:"+isval1);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitClick(btn_pro7ISLook2);
	click(btn_pro7ISLook2);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	isval2= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("isval2:"+isval2);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitClick(btn_pro7ISLook3);
	click(btn_pro7ISLook3);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	isval3= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("isval3:"+isval3);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitClick(btn_pro7ISLook4);
	click(btn_pro7ISLook4);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	isval4= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("isval4:"+isval4);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitClick(btn_pro7ISLook5);
	click(btn_pro7ISLook5);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	isval5= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("isval5:"+isval5);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitClick(btn_pro7ISLook6);
	click(btn_pro7ISLook6);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	isval6= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("isval6:"+isval6);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitClick(btn_pro7ISLook7);
	click(btn_pro7ISLook7);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	isval7= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("isval7:"+isval7);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	int testquantity = Integer.parseInt(TestQuantity);
	if (isval1==(poval1+testquantity)){
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Correct for BATCH-SPECIFIC Product", "pass");
	} else {
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Not Correct for BATCH-SPECIFIC Product", "fail");
	}
	if (isval2==(poval2+testquantity)){
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Correct for BATCH-FIFO Product", "pass");
	} else {
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Not Correct for BATCH-FIFO Product", "fail");
	}
	if (isval3==(poval3+testquantity)){
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Correct for LOT Product", "pass");
	} else {
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Not Correct for LOT Product", "fail");
	}
	if (isval4==(poval4+testquantity)){
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Correct for SERIEL-SPECIFIC Product", "pass");
	} else {
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Not Correct for SERIEL-SPECIFIC Product", "fail");
	}
	if (isval5==(poval5+testquantity)){
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Correct for SERIEL-BATCH-SPECIFIC Product", "pass");
	} else {
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Not Correct for SERIEL-BATCH-SPECIFIC Product", "fail");
	}
	if (isval6==(poval6+testquantity)){
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Correct for SERIEL-BATCH-FIFO Product", "pass");
	} else {
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Not Correct for SERIEL-BATCH-FIFO Product", "fail");
	}
	if (isval7==(poval7+testquantity)){
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Correct for SERIEL-FIFO Product", "pass");
	} else {
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Not Correct for SERIEL-FIFO Product", "fail");
	}
}

public void completePurchaseOrderToShipmentCostingjourney() throws Exception {
	
	WaitElement(btn_PurchaseOrderbutton);
	click(btn_PurchaseOrderbutton);
	if(isDisplayed(txt_PurchaseOrderpage)) {
		writeTestResults("verify Purchase Order page","view Purchase Order page", "Purchase Order page is display", "pass");
	}else {
		writeTestResults("verify Purchase Order page","view Purchase Order page", "Purchase Order page is not display", "fail");
	}
	
	WaitElement(btn_newPurchaseOrderbutton);
	click(btn_newPurchaseOrderbutton);
	
	WaitElement(btn_btndropdown);
	click(btn_btndropdown);
	WaitElement(btn_btnLPOtoShipmentCosting);
	click(btn_btnLPOtoShipmentCosting);
	
	if(isDisplayed(txt_newPurchaseOrderpage)&&(getText(Status).contentEquals("New"))) {
		writeTestResults("verify  New Purchase Order page","view  New Purchase Order page", " New Purchase Order page is display", "pass");
	}else {
		writeTestResults("verify  New Purchase Order page","view  New Purchase Order page", " New Purchase Order page is not display", "fail");
	}
	
	WaitElement(btn_PbtnVendor);
	click(btn_PbtnVendor);
	sendKeys(txt_txtVendor, txtVendor);
	pressEnter(txt_txtVendor);
	Thread.sleep(3000);
	doubleClick(sel_selVendor);

	click(btn_btnBillingAddress);
	sendKeys(txt_txtBillingAddress, txtBillingAddress);
	click(btn_btnApply);
	click(btn_btnShippingAddress);
	sendKeys(txt_txtShippingAddress, txtShippingAddress);
	click(btn_btnApply);

	click(btn_Pbtndetails);
	click(btn_btnPInvoiceAccount);
	
	WaitElement(txt_txtVendor);
	sendKeys(txt_txtVendor, txtVendor);
	pressEnter(txt_txtVendor);
	Thread.sleep(3000);
	doubleClick(sel_selVendor);

	WaitClick(btn_btnsummary);
	click(btn_btnsummary);
	
	ScrollDownAruna();

	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("BatchSpecific"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse1, "WH-PARTS [Main Warehouse - Parts]");

	WaitClick(btn_pro7POLook1);
	click(btn_pro7POLook1);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval1= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("poval1:"+poval1);
	JavascriptExecutor j9 = (JavascriptExecutor)driver;
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("BatchFifo"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse2, "WH-PARTS [Main Warehouse - Parts]");
	
	WaitClick(btn_pro7POLook2);
	click(btn_pro7POLook2);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval2= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("poval2:"+poval2);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("Lot"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse3, "WH-PARTS [Main Warehouse - Parts]");
	
	WaitClick(btn_pro7POLook3);
	click(btn_pro7POLook3);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval3= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("poval3:"+poval3);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("SerielSpecific"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse4, "WH-PARTS [Main Warehouse - Parts]");
	
	WaitClick(btn_pro7POLook4);
	click(btn_pro7POLook4);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval4= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("poval4:"+poval4);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("SerielBatchSpecific"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse5, "WH-PARTS [Main Warehouse - Parts]");
	
	WaitClick(btn_pro7POLook5);
	click(btn_pro7POLook5);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval5= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("poval5:"+poval5);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("SerielBatchFifo"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse6, "WH-PARTS [Main Warehouse - Parts]");
	
	WaitClick(btn_pro7POLook6);
	click(btn_pro7POLook6);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval6= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("poval6:"+poval6);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("SerirlFifo"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse7, "WH-PARTS [Main Warehouse - Parts]");
	
	WaitClick(btn_pro7POLook7);
	click(btn_pro7POLook7);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval7= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("poval7:"+poval7);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitClick(btn_btnCheckout);
	click(btn_btnCheckout);
	WaitClick(btn_btndraft);
	click(btn_btndraft);
	
	WaitElement(pageDraft);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Order Document Draft","Purchase Order Document Draft", "Purchase Order Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Order Document Draft","Purchase Order Document Draft", "Purchase Order Document is not Draft", "fail");
	}
	
	WaitClick(btn_release);
	click(btn_release);
	Thread.sleep(3000);
	trackCode= getText(Header);
	
	WaitElement(btn_btnGotopage);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify Purchase Order Document Released","Purchase Order Document Released", "Purchase Order Document is Released", "pass");
	} else {
		writeTestResults("verify Purchase Order Document Released","Purchase Order Document Released", "Purchase Order Document is not Released", "fail");
	}
	
	WaitClick(btn_btnGotopage);
	click(btn_btnGotopage);
	
	switchWindow();
	
	WaitElement(txt_PurchaseInvoicepage);
	if(isDisplayed(txt_PurchaseInvoicepage)) {
		writeTestResults("User enter Purchase Invoice page","view  Purchase Invoice page", "Purchase Invoice page is display", "pass");
	}else {
		writeTestResults("User enter Purchase Invoice page","view  Purchase Invoice page", "Purchase Invoice page is not display", "fail");
	}
	
	WaitClick(btn_btnCheckout);
	click(btn_btnCheckout);
	WaitClick(btn_btndraft);
	click(btn_btndraft);
	
	WaitClick(pageDraft);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Invoice Document Draft","Purchase Invoice Document Draft", "Purchase Invoice Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Invoice Document Draft","Purchase Invoice Document Draft", "Purchase Invoice Document is not Draft", "fail");
	}
	
	Thread.sleep(4000);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	
	WaitElement(btn_btnGotopage);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify Purchase Invoice Document Released","Purchase Invoice Document Released", "Purchase Invoice Document is Released", "pass");
	} else {
		writeTestResults("verify Purchase Invoice Document Released","Purchase Invoice Document Released", "Purchase Invoice Document is not Released", "fail");
	}
	WaitClick(btn_btnGotopage);
	click(btn_btnGotopage);
	
	switchWindow();
	
	WaitElement(txt_InboundShipmentpage);
	if(isDisplayed(txt_InboundShipmentpage)) {
		writeTestResults("User enter Inbound Shipment page","view  Inbound Shipment page", "Inbound Shipment page is display", "pass");
	}else {
		writeTestResults("User enter Inbound Shipment page","view  Inbound Shipment page", "Inbound Shipment page is not display", "fail");
	}
	
	WaitClick(btn_btnCheckout);
	click(btn_btnCheckout);
	WaitClick(btn_btndraft);
	click(btn_btndraft);
	
	WaitElement(pageDraft);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify enter Inbound Shipment Document Draft","enter Inbound Shipment Document Draft", "enter Inbound Shipment Document is Draft", "pass");
	} else {
		writeTestResults("verify enter Inbound Shipment Document Draft","enter Inbound Shipment Document Draft", "enter Inbound Shipment Document is not Draft", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnSerialBatch);
	
	Thread.sleep(6000);
	Actions action1 = new Actions(driver);
	WebElement a1 = driver.findElement(By.xpath(Cap1));
	action1.moveToElement(a1).build().perform();
	WaitClick(Cap1);
	click(Cap1);
	LocalTime p1 = LocalTime.now();
	sendKeys(BatchNu, "BN"+p1);
	sendKeys(BatchLotNu, "LN"+p1);
	WaitClick(BatchExpiryDate);
	click(BatchExpiryDate);
	WaitElement(nextdate);
	selectText(nextdate, "2030");
	WaitClick(BatchExpiryDateSel);
	click(BatchExpiryDateSel);
	pressEnter(BatchNu);
	WaitClick(btn_updateSerial);
	click(btn_updateSerial);

	Actions action2 = new Actions(driver);
	WebElement a2 = driver.findElement(By.xpath(Cap2));
	action2.moveToElement(a2).build().perform();
	WaitClick(Cap2);
	click(Cap2);
	LocalTime p2 = LocalTime.now();
	sendKeys(BatchNu, "BN"+p2);
	sendKeys(BatchLotNu, "LN"+p2);
	WaitClick(BatchExpiryDate);
	click(BatchExpiryDate);
	WaitElement(nextdate);
	selectText(nextdate, "2030");
	WaitClick(BatchExpiryDateSel);
	click(BatchExpiryDateSel);
	pressEnter(BatchNu);
	WaitClick(btn_updateSerial);
	click(btn_updateSerial);
	
	Actions action4 = new Actions(driver);
	WebElement a4 = driver.findElement(By.xpath(Cap4));
	action4.moveToElement(a4).build().perform();
	WaitClick(Cap4);
	click(Cap4);
	WaitClick(btn_SerialRange);
	click(btn_SerialRange);
	JavascriptExecutor j1 = (JavascriptExecutor)driver;
	j1.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",5)");
	LocalTime p3 = LocalTime.now();
	sendKeys(SerialFromNu, "SN"+p3);
	sendKeys(SerialLotNu, "LN"+p3);
	WaitClick(SerialExpiryDate);
	click(SerialExpiryDate);
	WaitElement(nextdate);
	selectText(nextdate, "2030");
	WaitClick(BatchExpiryDateSel);
	click(BatchExpiryDateSel);
	pressEnter(SerialFromNu);
	WaitClick(btn_updateSerial);
	click(btn_updateSerial);
	
	Actions action5 = new Actions(driver);
	WebElement a5 = driver.findElement(By.xpath(Cap5));
	action5.moveToElement(a5).build().perform();
	WaitClick(Cap5);
	click(Cap5);
	WaitClick(btn_SerialRange);
	click(btn_SerialRange);
	JavascriptExecutor j2 = (JavascriptExecutor)driver;
	j2.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",5)");
	LocalTime p4 = LocalTime.now();
	sendKeys(SerialFromNu, "SN"+p4);
	sendKeys(SerialLotNu, "LN"+p4);
	sendKeys(SerialBatchNu, "BN"+p4);
	WaitClick(SerialExpiryDate);
	click(SerialExpiryDate);
	WaitElement(nextdate);
	selectText(nextdate, "2030");
	WaitClick(BatchExpiryDateSel);
	click(BatchExpiryDateSel);
	pressEnter(SerialFromNu);
	WaitClick(btn_updateSerial);
	click(btn_updateSerial);
	
	Actions action6 = new Actions(driver);
	WebElement a6 = driver.findElement(By.xpath(Cap6));
	action6.moveToElement(a6).build().perform();
	WaitClick(Cap6);
	click(Cap6);
	WaitClick(btn_SerialRange);
	click(btn_SerialRange);
	JavascriptExecutor j3 = (JavascriptExecutor)driver;
	j3.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",5)");
	LocalTime p5 = LocalTime.now();
	sendKeys(SerialFromNu, "SN"+p5);
	sendKeys(SerialLotNu, "LN"+p5);
	sendKeys(SerialBatchNu, "BN"+p5);
	WaitClick(SerialExpiryDate);
	click(SerialExpiryDate);
	WaitElement(nextdate);
	selectText(nextdate, "2030");
	WaitClick(BatchExpiryDateSel);
	click(BatchExpiryDateSel);
	pressEnter(SerialFromNu);
	WaitClick(btn_updateSerial);
	click(btn_updateSerial);
	
	Actions action7 = new Actions(driver);
	WebElement a7 = driver.findElement(By.xpath(Cap7));
	action7.moveToElement(a7).build().perform();
	WaitClick(Cap7);
	click(Cap7);
	WaitClick(btn_SerialRange);
	click(btn_SerialRange);
	JavascriptExecutor j4 = (JavascriptExecutor)driver;
	j4.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",5)");
	LocalTime p6 = LocalTime.now();
	sendKeys(SerialFromNu, "SN"+p6);
	sendKeys(SerialLotNu, "LN"+p6);
	WaitClick(SerialExpiryDate);
	click(SerialExpiryDate);
	WaitElement(nextdate);
	selectText(nextdate, "2030");
	WaitClick(BatchExpiryDateSel);
	click(BatchExpiryDateSel);
	pressEnter(SerialFromNu);
	WaitClick(btn_updateSerial);
	click(btn_updateSerial);
	
	WaitClick(btn_closeProductList);
	click(btn_closeProductList);
	
	WaitClick(btn_release);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	
	WaitElement(btn_btnGotopage);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify Inbound Shipment Document Released","Inbound Shipment Document Released", "Inbound Shipment Document is Released", "pass");
	} else {
		writeTestResults("verify Inbound Shipment Document Released","Inbound Shipment Document Released", "Inbound Shipment Document is not Released", "fail");
	}
	
	WaitClick(btn_btnGotopage);
	click(btn_btnGotopage);
	
	switchWindow();
	WaitElement(txt_ShipmentCostingInformationpage);
	if(isDisplayed(txt_ShipmentCostingInformationpage)) {
		writeTestResults("User enter Shipment Costing Information page","view  Shipment Costing Information page", "Shipment Costing Information page is display", "pass");
	}else {
		writeTestResults("User enter Shipment Costing Information page","view  Shipment Costing Information page", "Shipment Costing Information is not display", "fail");
	}
	
	WaitClick(btn_btndraft);
	click(btn_btndraft);
	
	WaitElement(pageDraft);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Shipment Costing Information Document Draft","Shipment Costing Information Document Draft", "Shipment Costing Information Document is Draft", "pass");
	} else {
		writeTestResults("verify Shipment Costing Information Document Draft","Shipment Costing Information Document Draft", "Shipment Costing Information Document is not Draft", "fail");

	}
	
	WaitClick(btn_release);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	
	WaitElement(pageRelease);
	if (isDisplayed(pageRelease)){

		writeTestResults("verify Shipment Costing Information Document Released","Shipment Costing Information Document Released", "Shipment Costing Information Document is Released", "pass");
		System.out.println("PM_PO_004 Test case is Completed");
	} else {
		writeTestResults("verify Shipment Costing Information Document Released","Shipment Costing Information Document Released", "Shipment Costing Information Document is not Released", "fail");

	}
	
	switchWindow();
	
	WaitClick(btn_headerclose);
	click(btn_headerclose);
	
	WaitClick(btn_pro7ISLook1);
	click(btn_pro7ISLook1);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	isval1= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("isval1:"+isval1);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitClick(btn_pro7ISLook2);
	click(btn_pro7ISLook2);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	isval2= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("isval2:"+isval2);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitClick(btn_pro7ISLook3);
	click(btn_pro7ISLook3);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	isval3= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("isval3:"+isval3);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitClick(btn_pro7ISLook4);
	click(btn_pro7ISLook4);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	isval4= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("isval4:"+isval4);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitClick(btn_pro7ISLook5);
	click(btn_pro7ISLook5);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	isval5= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("isval5:"+isval5);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitClick(btn_pro7ISLook6);
	click(btn_pro7ISLook6);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	isval6= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("isval6:"+isval6);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitClick(btn_pro7ISLook7);
	click(btn_pro7ISLook7);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	isval7= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("isval7:"+isval7);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	int testquantity = Integer.parseInt(TestQuantity);
	if (isval1==(poval1+testquantity)){
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Correct for BATCH-SPECIFIC Product", "pass");
	} else {
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Not Correct for BATCH-SPECIFIC Product", "fail");
	}
	if (isval2==(poval2+testquantity)){
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Correct for BATCH-FIFO Product", "pass");
	} else {
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Not Correct for BATCH-FIFO Product", "fail");
	}
	if (isval3==(poval3+testquantity)){
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Correct for LOT Product", "pass");
	} else {
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Not Correct for LOT Product", "fail");
	}
	if (isval4==(poval4+testquantity)){
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Correct for SERIEL-SPECIFIC Product", "pass");
	} else {
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Not Correct for SERIEL-SPECIFIC Product", "fail");
	}
	if (isval5==(poval5+testquantity)){
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Correct for SERIEL-BATCH-SPECIFIC Product", "pass");
	} else {
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Not Correct for SERIEL-BATCH-SPECIFIC Product", "fail");
	}
	if (isval6==(poval6+testquantity)){
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Correct for SERIEL-BATCH-FIFO Product", "pass");
	} else {
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Not Correct for SERIEL-BATCH-FIFO Product", "fail");
	}
	if (isval7==(poval7+testquantity)){
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Correct for SERIEL-FIFO Product", "pass");
	} else {
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Not Correct for SERIEL-FIFO Product", "fail");
	}
	
	
}

public void consolidatePurchaseOrders() throws Exception {
	click(btn_PurchaseOrderbutton);
	if(isDisplayed(txt_PurchaseOrderpage)) {
		writeTestResults("verify Purchase Order page","view Purchase Order page", "Purchase Order page is display", "pass");
	}else {
		writeTestResults("verify Purchase Order page","view Purchase Order page", "Purchase Order page is not display", "fail");
	}
	click(btn_newPurchaseOrderbutton);
	Thread.sleep(3000);
	click(btn_btnPOtoShipmentCosting);
	if(isDisplayed(txt_newPurchaseOrderpage)&&(getText(Status).contentEquals("New"))) {
		writeTestResults("verify  New Purchase Order page","view  New Purchase Order page", " New Purchase Order page is display", "pass");
	}else {
		writeTestResults("verify  New Purchase Order page","view  New Purchase Order page", " New Purchase Order page is not display", "fail");
	}
	driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
	click(btn_PbtnVendor);
	sendKeys(txt_txtVendor, txtVendor);
	Thread.sleep(3000);
	pressEnter(txt_txtVendor);
	Thread.sleep(3000);
	doubleClick(sel_selVendor);
	
	click(btn_btnBillingAddress);
	sendKeys(txt_txtBillingAddress, txtBillingAddress);
	click(btn_btnApply);
	click(btn_btnShippingAddress);
	sendKeys(txt_txtShippingAddress, txtShippingAddress);
	click(btn_btnApply);
	
	click(btn_btnConsolidatedPO);
	click(btn_btnDocumentList);
	click(btn_btnproductline1);
	click(btn_btnproductline2);
	Thread.sleep(3000);
	JavascriptExecutor j1 = (JavascriptExecutor)driver;
	j1.executeScript("$(\"#divConsolidatedDocList\").parent().find('.dialogbox-buttonarea > .button').click()");
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Order Document Draft","Purchase Order Document Draft", "Purchase Order Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Order Document Draft","Purchase Order Document Draft", "Purchase Order Document is not Draft", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(3000);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify Purchase Order Document Released","Purchase Order Document Released", "Purchase Order Document is Released", "pass");
		System.out.println("PM_PO_005 Test case is Completed");
	} else {
		writeTestResults("verify Purchase Order Document Released","Purchase Order Document Released", "Purchase Order Document is not Released", "fail");
	}
}

public void attachCostEstimationForaDocument() throws Exception {
	
	WaitElement(btn_PurchaseOrderbutton);
	click(btn_PurchaseOrderbutton);
	
	WaitElement(txt_PurchaseOrderpage);
	if(isDisplayed(txt_PurchaseOrderpage)) {
		writeTestResults("verify Purchase Order page","view Purchase Order page", "Purchase Order page is display", "pass");
	}else {
		writeTestResults("verify Purchase Order page","view Purchase Order page", "Purchase Order page is not display", "fail");
	}
	
	WaitElement(btn_newPurchaseOrderbutton);
	click(btn_newPurchaseOrderbutton);
	WaitClick(btn_btnPOtoShipmentCosting);
	click(btn_btnPOtoShipmentCosting);
	
	if(isDisplayed(txt_newPurchaseOrderpage)&&(getText(Status).contentEquals("New"))) {
		writeTestResults("verify  New Purchase Order page","view  New Purchase Order page", " New Purchase Order page is display", "pass");
	}else {
		writeTestResults("verify  New Purchase Order page","view  New Purchase Order page", " New Purchase Order page is not display", "fail");
	}
	
	
	WaitClick(btn_PbtnVendor);
	click(btn_PbtnVendor);
	sendKeys(txt_txtVendor, txtVendor);
	pressEnter(txt_txtVendor);
	Thread.sleep(3000);
	doubleClick(sel_selVendor);
	

	click(btn_btnBillingAddress);
	sendKeys(txt_txtBillingAddress, txtBillingAddress);
	click(btn_btnApply);
	click(btn_btnShippingAddress);
	sendKeys(txt_txtShippingAddress, txtShippingAddress);
	click(btn_btnApply);

	ScrollDownAruna();
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("BatchSpecific"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse1, "WH-PARTS [Main Warehouse - Parts]");

	WaitClick(btn_pro7POLook1);
	click(btn_pro7POLook1);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval1= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("poval1:"+poval1);
	JavascriptExecutor j9 = (JavascriptExecutor)driver;
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("BatchFifo"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse2, "WH-PARTS [Main Warehouse - Parts]");
	
	WaitClick(btn_pro7POLook2);
	click(btn_pro7POLook2);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval2= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("poval2:"+poval2);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("Lot"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse3, "WH-PARTS [Main Warehouse - Parts]");
	
	WaitClick(btn_pro7POLook3);
	click(btn_pro7POLook3);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval3= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("poval3:"+poval3);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("SerielSpecific"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse4, "WH-PARTS [Main Warehouse - Parts]");
	
	WaitClick(btn_pro7POLook4);
	click(btn_pro7POLook4);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval4= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("poval4:"+poval4);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("SerielBatchSpecific"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse5, "WH-PARTS [Main Warehouse - Parts]");
	
	WaitClick(btn_pro7POLook5);
	click(btn_pro7POLook5);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval5= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("poval5:"+poval5);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("SerielBatchFifo"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse6, "WH-PARTS [Main Warehouse - Parts]");
	
	WaitClick(btn_pro7POLook6);
	click(btn_pro7POLook6);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval6= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("poval6:"+poval6);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("SerirlFifo"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse7, "WH-PARTS [Main Warehouse - Parts]");
	
	WaitClick(btn_pro7POLook7);
	click(btn_pro7POLook7);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval7= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("poval7:"+poval7);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
		
	click(btn_btnCheckout);
	click(btn_btnAction);
	
	click(btn_btnCostAllocation);
	click(btn_ImportCostEstimationShipmentbutton);
	sendKeys(txt_txtCEstDescription, txtCEstDescription);
	
	selectIndex(txt_txtCostingType, 1);
	sendKeys(txt_EstimateCost, EstimateCost);
	click(btn_Apportionmentbutton);
	
	selectIndex(txt_ApportionmentBasis,3);
	click(btn_AppoCheckout);
	Thread.sleep(4000);
	JavascriptExecutor app = (JavascriptExecutor)driver;
	app.executeScript("$(\"#dialogApportion\").nextAll(\".dialogbox-buttonarea\").find(\".button\").click()");
	click(btn_btnGenerateEstimation);
	click(btn_btnApplytomaingrid);
	click(btn_ApponxtCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Order Document Draft","Purchase Order Document Draft", "Purchase Order Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Order Document Draft","Purchase Order Document Draft", "Purchase Order Document is not Draft", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	Thread.sleep(5000);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify Purchase Order Document Released","Purchase Order Document Released", "Purchase Order Document is Released", "pass");
	} else {
		writeTestResults("verify Purchase Order Document Released","Purchase Order Document Released", "Purchase Order Document is not Released", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnGotopage);
	
	switchWindow();
	if(isDisplayed(txt_InboundShipmentpage)) {
		writeTestResults("User enter Inbound Shipment page","view  Inbound Shipment page", "Inbound Shipment page is display", "pass");
	}else {
		writeTestResults("User enter Inbound Shipment page","view  Inbound Shipment page", "Inbound Shipment page is not display", "fail");
	}
	click(btn_ApponxtCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify enter Inbound Shipment Document Draft","enter Inbound Shipment Document Draft", "enter Inbound Shipment Document is Draft", "pass");
	} else {
		writeTestResults("verify enter Inbound Shipment Document Draft","enter Inbound Shipment Document Draft", "enter Inbound Shipment Document is not Draft", "fail");
	}

	Thread.sleep(3000);
	click(btn_btnSerialBatch);
	
	Thread.sleep(6000);
	Actions action1 = new Actions(driver);
	WebElement a1 = driver.findElement(By.xpath(Cap1));
	action1.moveToElement(a1).build().perform();
	click(Cap1);
	LocalTime p1 = LocalTime.now();
	sendKeys(BatchNu, "BN"+p1);
	sendKeys(BatchLotNu, "LN"+p1);
	Thread.sleep(3000);
	click(BatchExpiryDate);
	Thread.sleep(3000);
	selectText(nextdate, "2030");
	Thread.sleep(3000);
	click(BatchExpiryDateSel);
	Thread.sleep(3000);
	pressEnter(BatchNu);
	Thread.sleep(3000);
	click(btn_updateSerial);
	
	Actions action2 = new Actions(driver);
	WebElement a2 = driver.findElement(By.xpath(Cap2));
	action2.moveToElement(a2).build().perform();
	click(Cap2);
	LocalTime p2 = LocalTime.now();
	sendKeys(BatchNu, "BN"+p2);
	sendKeys(BatchLotNu, "LN"+p2);
	Thread.sleep(3000);
	click(BatchExpiryDate);
	Thread.sleep(3000);
	selectText(nextdate, "2030");
	Thread.sleep(3000);
	click(BatchExpiryDateSel);
	Thread.sleep(3000);
	pressEnter(BatchNu);
	Thread.sleep(3000);
	click(btn_updateSerial);
	
	Actions action4 = new Actions(driver);
	WebElement a4 = driver.findElement(By.xpath(Cap4));
	action4.moveToElement(a4).build().perform();
	click(Cap4);
	Thread.sleep(3000);
	click(btn_SerialRange);
	Thread.sleep(3000);
	JavascriptExecutor j1 = (JavascriptExecutor)driver;
	j1.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",5)");
	LocalTime p3 = LocalTime.now();
	sendKeys(SerialFromNu, "SN"+p3);
	sendKeys(SerialLotNu, "LN"+p3);
	Thread.sleep(3000);
	click(SerialExpiryDate);
	Thread.sleep(3000);
	selectText(nextdate, "2030");
	Thread.sleep(3000);
	click(BatchExpiryDateSel);
	Thread.sleep(3000);
	pressEnter(SerialFromNu);
	Thread.sleep(3000);
	click(btn_updateSerial);
	
	Actions action5 = new Actions(driver);
	WebElement a5 = driver.findElement(By.xpath(Cap5));
	action5.moveToElement(a5).build().perform();
	click(Cap5);
	Thread.sleep(3000);
	click(btn_SerialRange);
	Thread.sleep(3000);
	JavascriptExecutor j2 = (JavascriptExecutor)driver;
	j2.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",5)");
	LocalTime p4 = LocalTime.now();
	sendKeys(SerialFromNu, "SN"+p4);
	sendKeys(SerialLotNu, "LN"+p4);
	sendKeys(SerialBatchNu, "BN"+p4);
	Thread.sleep(3000);
	click(SerialExpiryDate);
	Thread.sleep(3000);
	selectText(nextdate, "2030");
	Thread.sleep(3000);
	click(BatchExpiryDateSel);
	Thread.sleep(3000);
	pressEnter(SerialFromNu);
	Thread.sleep(3000);
	click(btn_updateSerial);
	
	Actions action6 = new Actions(driver);
	WebElement a6 = driver.findElement(By.xpath(Cap6));
	action6.moveToElement(a6).build().perform();
	click(Cap6);
	Thread.sleep(3000);
	click(btn_SerialRange);
	Thread.sleep(3000);
	JavascriptExecutor j3 = (JavascriptExecutor)driver;
	j3.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",5)");
	LocalTime p5 = LocalTime.now();
	sendKeys(SerialFromNu, "SN"+p5);
	sendKeys(SerialLotNu, "LN"+p5);
	sendKeys(SerialBatchNu, "BN"+p5);
	Thread.sleep(3000);
	click(SerialExpiryDate);
	Thread.sleep(3000);
	selectText(nextdate, "2030");
	Thread.sleep(3000);
	click(BatchExpiryDateSel);
	Thread.sleep(3000);
	pressEnter(SerialFromNu);
	Thread.sleep(3000);
	click(btn_updateSerial);
	
	Actions action7 = new Actions(driver);
	WebElement a7 = driver.findElement(By.xpath(Cap7));
	action7.moveToElement(a7).build().perform();
	click(Cap7);
	Thread.sleep(3000);
	click(btn_SerialRange);
	Thread.sleep(3000);
	JavascriptExecutor j4 = (JavascriptExecutor)driver;
	j4.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",5)");
	LocalTime p6 = LocalTime.now();
	sendKeys(SerialFromNu, "SN"+p6);
	sendKeys(SerialLotNu, "LN"+p6);
	Thread.sleep(3000);
	click(SerialExpiryDate);
	Thread.sleep(3000);
	selectText(nextdate, "2030");
	Thread.sleep(3000);
	click(BatchExpiryDateSel);
	Thread.sleep(3000);
	pressEnter(SerialFromNu);
	Thread.sleep(3000);
	click(btn_updateSerial);
	
	Thread.sleep(3000);
	click(btn_closeProductList);
	
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	Thread.sleep(5000);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify Inbound Shipment Document Released","Inbound Shipment Document Released", "Inbound Shipment Document is Released", "pass");
	} else {
		writeTestResults("verify Inbound Shipment Document Released","Inbound Shipment Document Released", "Inbound Shipment Document is not Released", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnGotopage);
	
	switchWindow();
	if(isDisplayed(txt_PurchaseInvoicepage)) {
		writeTestResults("User enter Purchase Invoice page","view  Purchase Invoice page", "Purchase Invoice page is display", "pass");
	}else {
		writeTestResults("User enter Purchase Invoice page","view  Purchase Invoice page", "Purchase Invoice page is not display", "fail");
	}
	click(btn_ApponxtCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Invoice Document Draft","Purchase Invoice Document Draft", "Purchase Invoice Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Invoice Document Draft","Purchase Invoice Document Draft", "Purchase Invoice Document is not Draft", "fail");
	}
	
	Thread.sleep(4000);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	Thread.sleep(5000);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify Purchase Invoice Document Released","Purchase Invoice Document Released", "Purchase Invoice Document is Released", "pass");
	} else {
		writeTestResults("verify Purchase Invoice Document Released","Purchase Invoice Document Released", "Purchase Invoice Document is not Released", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnGotopage);
	
	switchWindow();
	if(isDisplayed(txt_ShipmentCostingInformationpage)) {
		writeTestResults("User enter Shipment Costing Information page","view  Shipment Costing Information page", "Shipment Costing Information page is display", "pass");
	}else {
		writeTestResults("User enter Shipment Costing Information page","view  Shipment Costing Information page", "Shipment Costing Information is not display", "fail");
	}
	click(btn_ApponxtCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Shipment Costing Information Document Draft","Shipment Costing Information Document Draft", "Shipment Costing Information Document is Draft", "pass");
	} else {
		writeTestResults("verify Shipment Costing Information Document Draft","Shipment Costing Information Document Draft", "Shipment Costing Information Document is not Draft", "fail");

	}
	
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	Thread.sleep(5000);
	if (isDisplayed(pageRelease)){

		writeTestResults("verify Shipment Costing Information Document Released","Shipment Costing Information Document Released", "Shipment Costing Information Document is Released", "pass");
		System.out.println("PM_PO_006 Test case is Completed");
	} else {
		writeTestResults("verify Shipment Costing Information Document Released","Shipment Costing Information Document Released", "Shipment Costing Information Document is not Released", "fail");

	}
	
	switchWindow();
	
	Thread.sleep(3000);
	click(btn_headerclose);
	
	Thread.sleep(4000);
	click(btn_pro7PILook1);
	Thread.sleep(4000);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	pival1= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("pival1:"+pival1);
	Thread.sleep(4000);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	Thread.sleep(4000);
	click(btn_pro7PILook2);
	Thread.sleep(4000);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	pival2= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("pival2:"+pival2);
	Thread.sleep(4000);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	Thread.sleep(4000);
	click(btn_pro7PILook3);
	Thread.sleep(4000);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	pival3= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("pival3:"+pival3);
	Thread.sleep(4000);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	Thread.sleep(4000);
	click(btn_pro7PILook4);
	Thread.sleep(4000);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	pival4= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("pival4:"+pival4);
	Thread.sleep(4000);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	Thread.sleep(4000);
	click(btn_pro7PILook5);
	Thread.sleep(4000);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	pival5= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("pival5:"+pival5);
	Thread.sleep(4000);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	Thread.sleep(4000);
	click(btn_pro7PILook6);
	Thread.sleep(4000);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	pival6= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("pival6:"+pival6);
	Thread.sleep(4000);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	Thread.sleep(4000);
	click(btn_pro7PILook7);
	Thread.sleep(4000);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	pival7= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("pival7:"+pival7);
	Thread.sleep(4000);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	int testquantity = Integer.parseInt(TestQuantity);
	if (pival1==(poval1+testquantity)){
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Correct for BATCH-SPECIFIC Product", "pass");
	} else {
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Not Correct for BATCH-SPECIFIC Product", "fail");
	}
	if (pival2==(poval2+testquantity)){
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Correct for BATCH-FIFO Product", "pass");
	} else {
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Not Correct for BATCH-FIFO Product", "fail");
	}
	if (pival3==(poval3+testquantity)){
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Correct for LOT Product", "pass");
	} else {
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Not Correct for LOT Product", "fail");
	}
	if (pival4==(poval4+testquantity)){
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Correct for SERIEL-SPECIFIC Product", "pass");
	} else {
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Not Correct for SERIEL-SPECIFIC Product", "fail");
	}
	if (pival5==(poval5+testquantity)){
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Correct for SERIEL-BATCH-SPECIFIC Product", "pass");
	} else {
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Not Correct for SERIEL-BATCH-SPECIFIC Product", "fail");
	}
	if (pival6==(poval6+testquantity)){
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Correct for SERIEL-BATCH-FIFO Product", "pass");
	} else {
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Not Correct for SERIEL-BATCH-FIFO Product", "fail");
	}
	if (pival7==(poval7+testquantity)){
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Correct for SERIEL-FIFO Product", "pass");
	} else {
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Not Correct for SERIEL-FIFO Product", "fail");
	}
	
}

public void attachCostEstimationsForEachProduct() throws Exception {
	
	click(btn_PurchaseOrderbutton);
	if(isDisplayed(txt_PurchaseOrderpage)) {
		writeTestResults("verify Purchase Order page","view Purchase Order page", "Purchase Order page is display", "pass");
	}else {
		writeTestResults("verify Purchase Order page","view Purchase Order page", "Purchase Order page is not display", "fail");
	}
	click(btn_newPurchaseOrderbutton);
	Thread.sleep(3000);
	click(btn_btnPOtoShipmentCosting);
	if(isDisplayed(txt_newPurchaseOrderpage)&&(getText(Status).contentEquals("New"))) {
		writeTestResults("verify  New Purchase Order page","view  New Purchase Order page", " New Purchase Order page is display", "pass");
	}else {
		writeTestResults("verify  New Purchase Order page","view  New Purchase Order page", " New Purchase Order page is not display", "fail");
	}
	driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
	click(btn_PbtnVendor);
	sendKeys(txt_txtVendor, txtVendor);
	Thread.sleep(3000);
	pressEnter(txt_txtVendor);
	Thread.sleep(3000);
	doubleClick(sel_selVendor);
	

	click(btn_btnBillingAddress);
	sendKeys(txt_txtBillingAddress, txtBillingAddress);
	click(btn_btnApply);
	click(btn_btnShippingAddress);
	sendKeys(txt_txtShippingAddress, txtShippingAddress);
	click(btn_btnApply);
	
	ScrollDownAruna();
	  
	Thread.sleep(4000);
	sendKeys(Product, pro7.readTestCreation("BatchSpecific"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse1, "WH-PARTS [Main Warehouse - Parts]");

	Thread.sleep(3000);
	click(btn_pro7POLook1);
	Thread.sleep(3000);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval1= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("poval1:"+poval1);
	Thread.sleep(3000);
	JavascriptExecutor j9 = (JavascriptExecutor)driver;
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	Thread.sleep(4000);
	sendKeys(Product, pro7.readTestCreation("BatchFifo"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse2, "WH-PARTS [Main Warehouse - Parts]");
	
	Thread.sleep(3000);
	click(btn_pro7POLook2);
	Thread.sleep(3000);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval2= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("poval2:"+poval2);
	Thread.sleep(3000);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	Thread.sleep(4000);
	sendKeys(Product, pro7.readTestCreation("Lot"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse3, "WH-PARTS [Main Warehouse - Parts]");
	
	Thread.sleep(3000);
	click(btn_pro7POLook3);
	Thread.sleep(3000);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval3= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("poval3:"+poval3);
	Thread.sleep(3000);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	Thread.sleep(4000);
	sendKeys(Product, pro7.readTestCreation("SerielSpecific"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse4, "WH-PARTS [Main Warehouse - Parts]");
	
	Thread.sleep(3000);
	click(btn_pro7POLook4);
	Thread.sleep(3000);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval4= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("poval4:"+poval4);
	Thread.sleep(3000);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	Thread.sleep(4000);
	sendKeys(Product, pro7.readTestCreation("SerielBatchSpecific"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse5, "WH-PARTS [Main Warehouse - Parts]");
	
	Thread.sleep(3000);
	click(btn_pro7POLook5);
	Thread.sleep(3000);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval5= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("poval5:"+poval5);
	Thread.sleep(3000);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	Thread.sleep(4000);
	sendKeys(Product, pro7.readTestCreation("SerielBatchFifo"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse6, "WH-PARTS [Main Warehouse - Parts]");
	
	Thread.sleep(3000);
	click(btn_pro7POLook6);
	Thread.sleep(3000);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval6= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("poval6:"+poval6);
	Thread.sleep(3000);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	Thread.sleep(4000);
	sendKeys(Product, pro7.readTestCreation("SerirlFifo"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, TestQuantity);
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse7, "WH-PARTS [Main Warehouse - Parts]");
	
	Thread.sleep(3000);
	click(btn_pro7POLook7);
	Thread.sleep(3000);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval7= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("poval7:"+poval7);
	Thread.sleep(3000);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");

	
	click(btn_btnCheckout);
	click(btn_btnAction);
	
	click(btn_btnCostAllocation);
	click(btn_ImportEstimationbutton);
	click(btn_addNewEstimationbutton);
	sendKeys(txt_txtQESTDescription, txtQESTDescription);
	sendKeys(txt_EstimatedQty, "5");
	click(btn_EstimationDetailsbutton);
	selectIndex(txt_CostingType, 1);
	sendKeys(txt_EstimatenewCost, EstimatenewCost);
	click(btn_Summary);
	click(btn_Checkout);
	
	click(btn_btnnxtGenerateEstimation);
	click(btn_btnApplytomaingrid);
	click(btn_ApponxtCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Order Document Draft","Purchase Order Document Draft", "Purchase Order Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Order Document Draft","Purchase Order Document Draft", "Purchase Order Document is not Draft", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	Thread.sleep(5000);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify Purchase Order Document Released","Purchase Order Document Released", "Purchase Order Document is Released", "pass");
	} else {
		writeTestResults("verify Purchase Order Document Released","Purchase Order Document Released", "Purchase Order Document is not Released", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnGotopage);
	
	switchWindow();
	if(isDisplayed(txt_InboundShipmentpage)) {
		writeTestResults("User enter Inbound Shipment page","view  Inbound Shipment page", "Inbound Shipment page is display", "pass");
	}else {
		writeTestResults("User enter Inbound Shipment page","view  Inbound Shipment page", "Inbound Shipment page is not display", "fail");
	}
	click(btn_ApponxtCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify enter Inbound Shipment Document Draft","enter Inbound Shipment Document Draft", "enter Inbound Shipment Document is Draft", "pass");
	} else {
		writeTestResults("verify enter Inbound Shipment Document Draft","enter Inbound Shipment Document Draft", "enter Inbound Shipment Document is not Draft", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnSerialBatch);
	
	Thread.sleep(6000);
	Actions action1 = new Actions(driver);
	WebElement a1 = driver.findElement(By.xpath(Cap1));
	action1.moveToElement(a1).build().perform();
	click(Cap1);
	LocalTime p1 = LocalTime.now();
	sendKeys(BatchNu, "BN"+p1);
	sendKeys(BatchLotNu, "LN"+p1);
	Thread.sleep(3000);
	click(BatchExpiryDate);
	Thread.sleep(3000);
	selectText(nextdate, "2030");
	Thread.sleep(3000);
	click(BatchExpiryDateSel);
	Thread.sleep(3000);
	pressEnter(BatchNu);
	Thread.sleep(3000);
	click(btn_updateSerial);
	
	Actions action2 = new Actions(driver);
	WebElement a2 = driver.findElement(By.xpath(Cap2));
	action2.moveToElement(a2).build().perform();
	click(Cap2);
	LocalTime p2 = LocalTime.now();
	sendKeys(BatchNu, "BN"+p2);
	sendKeys(BatchLotNu, "LN"+p2);
	Thread.sleep(3000);
	click(BatchExpiryDate);
	Thread.sleep(3000);
	selectText(nextdate, "2030");
	Thread.sleep(3000);
	click(BatchExpiryDateSel);
	Thread.sleep(3000);
	pressEnter(BatchNu);
	Thread.sleep(3000);
	click(btn_updateSerial);
	
	Actions action4 = new Actions(driver);
	WebElement a4 = driver.findElement(By.xpath(Cap4));
	action4.moveToElement(a4).build().perform();
	click(Cap4);
	Thread.sleep(3000);
	click(btn_SerialRange);
	Thread.sleep(3000);
	JavascriptExecutor j1 = (JavascriptExecutor)driver;
	j1.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",5)");
	LocalTime p3 = LocalTime.now();
	sendKeys(SerialFromNu, "SN"+p3);
	sendKeys(SerialLotNu, "LN"+p3);
	Thread.sleep(3000);
	click(SerialExpiryDate);
	Thread.sleep(3000);
	selectText(nextdate, "2030");
	Thread.sleep(3000);
	click(BatchExpiryDateSel);
	Thread.sleep(3000);
	pressEnter(SerialFromNu);
	Thread.sleep(3000);
	click(btn_updateSerial);
	
	Actions action5 = new Actions(driver);
	WebElement a5 = driver.findElement(By.xpath(Cap5));
	action5.moveToElement(a5).build().perform();
	click(Cap5);
	Thread.sleep(3000);
	click(btn_SerialRange);
	Thread.sleep(3000);
	JavascriptExecutor j2 = (JavascriptExecutor)driver;
	j2.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",5)");
	LocalTime p4 = LocalTime.now();
	sendKeys(SerialFromNu, "SN"+p4);
	sendKeys(SerialLotNu, "LN"+p4);
	sendKeys(SerialBatchNu, "BN"+p4);
	Thread.sleep(3000);
	click(SerialExpiryDate);
	Thread.sleep(3000);
	selectText(nextdate, "2030");
	Thread.sleep(3000);
	click(BatchExpiryDateSel);
	Thread.sleep(3000);
	pressEnter(SerialFromNu);
	Thread.sleep(3000);
	click(btn_updateSerial);
	
	Actions action6 = new Actions(driver);
	WebElement a6 = driver.findElement(By.xpath(Cap6));
	action6.moveToElement(a6).build().perform();
	click(Cap6);
	Thread.sleep(3000);
	click(btn_SerialRange);
	Thread.sleep(3000);
	JavascriptExecutor j3 = (JavascriptExecutor)driver;
	j3.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",5)");
	LocalTime p5 = LocalTime.now();
	sendKeys(SerialFromNu, "SN"+p5);
	sendKeys(SerialLotNu, "LN"+p5);
	sendKeys(SerialBatchNu, "BN"+p5);
	Thread.sleep(3000);
	click(SerialExpiryDate);
	Thread.sleep(3000);
	selectText(nextdate, "2030");
	Thread.sleep(3000);
	click(BatchExpiryDateSel);
	Thread.sleep(3000);
	pressEnter(SerialFromNu);
	Thread.sleep(3000);
	click(btn_updateSerial);
	
	Actions action7 = new Actions(driver);
	WebElement a7 = driver.findElement(By.xpath(Cap7));
	action7.moveToElement(a7).build().perform();
	click(Cap7);
	Thread.sleep(3000);
	click(btn_SerialRange);
	Thread.sleep(3000);
	JavascriptExecutor j4 = (JavascriptExecutor)driver;
	j4.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",5)");
	LocalTime p6 = LocalTime.now();
	sendKeys(SerialFromNu, "SN"+p6);
	sendKeys(SerialLotNu, "LN"+p6);
	Thread.sleep(3000);
	click(SerialExpiryDate);
	Thread.sleep(3000);
	selectText(nextdate, "2030");
	Thread.sleep(3000);
	click(BatchExpiryDateSel);
	Thread.sleep(3000);
	pressEnter(SerialFromNu);
	Thread.sleep(3000);
	click(btn_updateSerial);
	
	Thread.sleep(3000);
	click(btn_closeProductList);
	
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	Thread.sleep(3000);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify Inbound Shipment Document Released","Inbound Shipment Document Released", "Inbound Shipment Document is Released", "pass");
	} else {
		writeTestResults("verify Inbound Shipment Document Released","Inbound Shipment Document Released", "Inbound Shipment Document is not Released", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnGotopage);
	
	switchWindow();
	if(isDisplayed(txt_PurchaseInvoicepage)) {
		writeTestResults("User enter Purchase Invoice page","view  Purchase Invoice page", "Purchase Invoice page is display", "pass");
	}else {
		writeTestResults("User enter Purchase Invoice page","view  Purchase Invoice page", "Purchase Invoice page is not display", "fail");
	}
	click(btn_ApponxtCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Invoice Document Draft","Purchase Invoice Document Draft", "Purchase Invoice Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Invoice Document Draft","Purchase Invoice Document Draft", "Purchase Invoice Document is not Draft", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_pro7PILook1);
	Thread.sleep(3000);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	pival1= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("pival1:"+pival1);
	Thread.sleep(3000);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	Thread.sleep(3000);
	click(btn_pro7PILook2);
	Thread.sleep(3000);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	pival2= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("pival2:"+pival2);
	Thread.sleep(3000);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	Thread.sleep(3000);
	click(btn_pro7PILook3);
	Thread.sleep(3000);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	pival3= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("pival3:"+pival3);
	Thread.sleep(3000);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	Thread.sleep(3000);
	click(btn_pro7PILook4);
	Thread.sleep(3000);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	pival4= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("pival4:"+pival4);
	Thread.sleep(3000);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	Thread.sleep(3000);
	click(btn_pro7PILook5);
	Thread.sleep(3000);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	pival5= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("pival5:"+pival5);
	Thread.sleep(3000);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	Thread.sleep(3000);
	click(btn_pro7PILook6);
	Thread.sleep(3000);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	pival6= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("pival6:"+pival6);
	Thread.sleep(3000);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	Thread.sleep(3000);
	click(btn_pro7PILook7);
	Thread.sleep(3000);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	pival7= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("pival7:"+pival7);
	Thread.sleep(3000);
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	


	int testquantity = Integer.parseInt(TestQuantity);
	if (pival1==(poval1+testquantity)){
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Correct for BATCH-SPECIFIC Product", "pass");
	} else {
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Not Correct for BATCH-SPECIFIC Product", "fail");
	}
	if (pival2==(poval2+testquantity)){
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Correct for BATCH-FIFO Product", "pass");
	} else {
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Not Correct for BATCH-FIFO Product", "fail");
	}
	if (pival3==(poval3+testquantity)){
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Correct for LOT Product", "pass");
	} else {
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Not Correct for LOT Product", "fail");
	}
	if (pival4==(poval4+testquantity)){
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Correct for SERIEL-SPECIFIC Product", "pass");
	} else {
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Not Correct for SERIEL-SPECIFIC Product", "fail");
	}
	if (pival5==(poval5+testquantity)){
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Correct for SERIEL-BATCH-SPECIFIC Product", "pass");
	} else {
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Not Correct for SERIEL-BATCH-SPECIFIC Product", "fail");
	}
	if (pival6==(poval6+testquantity)){
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Correct for SERIEL-BATCH-FIFO Product", "pass");
	} else {
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Not Correct for SERIEL-BATCH-FIFO Product", "fail");
	}
	if (pival7==(poval7+testquantity)){
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Correct for SERIEL-FIFO Product", "pass");
	} else {
		writeTestResults("verify Inbound Stock Quantity","Inbound Stock Quantity Correct", "Inbound Stock Quantity Is Not Correct for SERIEL-FIFO Product", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	Thread.sleep(5000);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify Purchase Invoice Document Released","Purchase Invoice Document Released", "Purchase Invoice Document is Released", "pass");
	} else {
		writeTestResults("verify Purchase Invoice Document Released","Purchase Invoice Document Released", "Purchase Invoice Document is not Released", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnGotopage);
	
	switchWindow();
	if(isDisplayed(txt_ShipmentCostingInformationpage)) {
		writeTestResults("User enter Shipment Costing Information page","view  Shipment Costing Information page", "Shipment Costing Information page is display", "pass");
	}else {
		writeTestResults("User enter Shipment Costing Information page","view  Shipment Costing Information page", "Shipment Costing Information is not display", "fail");
	}
	click(btn_ApponxtCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Shipment Costing Information Document Draft","Shipment Costing Information Document Draft", "Shipment Costing Information Document is Draft", "pass");
	} else {
		writeTestResults("verify Shipment Costing Information Document Draft","Shipment Costing Information Document Draft", "Shipment Costing Information Document is not Draft", "fail");

	}
	
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	Thread.sleep(5000);
	if (isDisplayed(pageRelease)){

		writeTestResults("verify Shipment Costing Information Document Released","Shipment Costing Information Document Released", "Shipment Costing Information Document is Released", "pass");
		System.out.println("PM_PO_007 Test case is Completed");
	} else {
		writeTestResults("verify Shipment Costing Information Document Released","Shipment Costing Information Document Released", "Shipment Costing Information Document is not Released", "fail");

	}
}

public void completePurchaseInvoiceToShipmentCosting() throws Exception {
	
	click(btn_PurchaseInvoicebutton);
	if(isDisplayed(txt_PurchaseInvoicebuttonpage)) {
		writeTestResults("verify Purchase Invoice page","view Purchase Invoice page", "Purchase Invoice page is display", "pass");
	}else {
		writeTestResults("verify Purchase Invoice page","view Purchase Invoice page", "Purchase Invoice page is not display", "fail");
	}
	click(btn_newPurchaseInvoicebutton);
	WaitElement(btn_btnPIToShipmentCosting);
	click(btn_btnPIToShipmentCosting);
	if(isDisplayed(txt_newPurchaseInvoicebuttonpage)&&(getText(Status).contentEquals("New"))) {
		writeTestResults("verify New Purchase Invoice page","view New Purchase Invoice page", "New Purchase Invoice page is display", "pass");
	}else {
		writeTestResults("verify Purchase Invoice page","view New Purchase Invoice page", "New Purchase Invoice page is not display", "fail");
	}
	WaitClick(btn_btnPIVendor);
	click(btn_btnPIVendor);
	WaitElement(txt_txtPIVendor);
	sendKeys(txt_txtPIVendor, txtPIVendor);
	pressEnter(txt_txtPIVendor);
	Thread.sleep(3000);
	doubleClick(sel_selPIVendor);
	selectText(txt_PIcurrency, PIcurrency);
	
	click(btn_btnPIBillingAddress);
	sendKeys(txt_txtPIBillingAddress, txtPIBillingAddress);
	click(btn_btnApply);
	click(btn_btnPIShippingAddress);
	sendKeys(txt_txtPIShippingAddress, txtPIShippingAddress);
	click(btn_btnApply);
	
	ScrollDownAruna();
	
	sendKeys(Product, pro7.readTestCreation("BatchSpecific"));
	Thread.sleep(3000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(3000);
	pressEnter(Product);
	sendKeys(ProductQty, minOrderQty);
	Thread.sleep(3000);
	pressEnter(ProductQty);
	selectText(WarehousePI1, "WH-PARTS [Main Warehouse - Parts]");
	
	sendKeys(Product, pro7.readTestCreation("BatchFifo"));
	Thread.sleep(3000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(3000);
	pressEnter(Product);
	sendKeys(ProductQty, minOrderQty);
	Thread.sleep(3000);
	pressEnter(ProductQty);
	selectText(WarehousePI2, "WH-PARTS [Main Warehouse - Parts]");
	
	sendKeys(Product, pro7.readTestCreation("Lot"));
	Thread.sleep(3000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(3000);
	pressEnter(Product);
	sendKeys(ProductQty, minOrderQty);
	Thread.sleep(3000);
	pressEnter(ProductQty);
	selectText(WarehousePI3, "WH-PARTS [Main Warehouse - Parts]");
	
	sendKeys(Product, pro7.readTestCreation("SerielSpecific"));
	Thread.sleep(3000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(3000);
	pressEnter(Product);
	sendKeys(ProductQty, minOrderQty);
	Thread.sleep(3000);
	pressEnter(ProductQty);
	selectText(WarehousePI4, "WH-PARTS [Main Warehouse - Parts]");
	
	sendKeys(Product, pro7.readTestCreation("SerielBatchSpecific"));
	Thread.sleep(3000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(3000);
	pressEnter(Product);
	sendKeys(ProductQty, minOrderQty);
	Thread.sleep(3000);
	pressEnter(ProductQty);
	selectText(WarehousePI5, "WH-PARTS [Main Warehouse - Parts]");
	
	sendKeys(Product, pro7.readTestCreation("SerielBatchFifo"));
	Thread.sleep(3000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(3000);
	pressEnter(Product);
	sendKeys(ProductQty, minOrderQty);
	Thread.sleep(3000);
	pressEnter(ProductQty);
	selectText(WarehousePI6, "WH-PARTS [Main Warehouse - Parts]");
	
	sendKeys(Product, pro7.readTestCreation("SerirlFifo"));
	Thread.sleep(3000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(3000);
	pressEnter(Product);
	sendKeys(ProductQty, minOrderQty);
	Thread.sleep(3000);
	pressEnter(ProductQty);
	selectText(WarehousePI7, "WH-PARTS [Main Warehouse - Parts]");
	
	WaitClick(btn_btnCheckout);
	click(btn_btnCheckout);
	
	Thread.sleep(3000);
	click(btn_btndraft);
	
	WaitClick(pageDraft);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Invoice Document Draft","Purchase Invoice Document Draft", "Purchase Invoice Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Invoice Document Draft","Purchase Invoice Document Draft", "Purchase Invoice Document is not Draft", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	
	WaitElement(btn_btnGotopage);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify Purchase Invoice Document Released","Purchase Invoice Document Released", "Purchase Invoice Document is Released", "pass");
	} else {
		writeTestResults("verify Purchase Invoice Document Released","Purchase Invoice Document Released", "Purchase Invoice Document is not Released", "fail");
	}
	
	WaitClick(btn_btnGotopage);
	click(btn_btnGotopage);
	
	switchWindow();
	if(isDisplayed(txt_InboundShipmentpage)) {
		writeTestResults("User enter Inbound Shipment page","view  Inbound Shipment page", "Inbound Shipment page is display", "pass");
	}else {
		writeTestResults("User enter Inbound Shipment page","view  Inbound Shipment page", "Inbound Shipment page is not display", "fail");
	}
	
	WaitClick(btn_btnCheckout);
	click(btn_btnCheckout);
	
	Thread.sleep(3000);
	click(btn_btndraft);
	
	WaitElement(pageDraft);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Inbound Shipment Document Draft","view Inbound Shipment Document Draft", "Inbound Shipment Document is Draft", "pass");
	} else {
		writeTestResults("verify Inbound Shipment Document Draft","view Inbound Shipment Document Draft", "Inbound Shipment Document is not Draft", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnSerialBatch);
	
	Thread.sleep(4000);
	Actions action1 = new Actions(driver);
	WebElement a1 = driver.findElement(By.xpath(Cap1));
	action1.moveToElement(a1).build().perform();
	WaitClick(Cap1);
	click(Cap1);
	LocalTime p1 = LocalTime.now();
	sendKeys(BatchNu, "BN"+p1);
	sendKeys(BatchLotNu, "LN"+p1);
	WaitClick(BatchExpiryDate);
	click(BatchExpiryDate);
	WaitElement(nextdate);
	selectText(nextdate, "2030");
	WaitClick(BatchExpiryDateSel);
	click(BatchExpiryDateSel);
	pressEnter(BatchNu);
	WaitClick(btn_updateSerial);
	click(btn_updateSerial);
	
	Actions action2 = new Actions(driver);
	WebElement a2 = driver.findElement(By.xpath(Cap2));
	action2.moveToElement(a2).build().perform();
	WaitClick(Cap2);
	click(Cap2);
	LocalTime p2 = LocalTime.now();
	sendKeys(BatchNu, "BN"+p2);
	sendKeys(BatchLotNu, "LN"+p2);
	WaitClick(BatchExpiryDate);
	click(BatchExpiryDate);
	WaitElement(nextdate);
	selectText(nextdate, "2030");
	WaitClick(BatchExpiryDateSel);
	click(BatchExpiryDateSel);
	pressEnter(BatchNu);
	WaitClick(btn_updateSerial);
	click(btn_updateSerial);
	
	Actions action4 = new Actions(driver);
	WebElement a4 = driver.findElement(By.xpath(Cap4));
	action4.moveToElement(a4).build().perform();
	WaitClick(Cap4);
	click(Cap4);
	WaitClick(btn_SerialRange);
	click(btn_SerialRange);
	JavascriptExecutor j1 = (JavascriptExecutor)driver;
	j1.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",1)");
	LocalTime p3 = LocalTime.now();
	sendKeys(SerialFromNu, "SN"+p3);
	sendKeys(SerialLotNu, "LN"+p3);
	WaitClick(SerialExpiryDate);
	click(SerialExpiryDate);
	WaitElement(nextdate);
	selectText(nextdate, "2030");
	WaitClick(BatchExpiryDateSel);
	click(BatchExpiryDateSel);
	pressEnter(SerialFromNu);
	WaitClick(btn_updateSerial);
	click(btn_updateSerial);
	
	Actions action5 = new Actions(driver);
	WebElement a5 = driver.findElement(By.xpath(Cap5));
	action5.moveToElement(a5).build().perform();
	WaitClick(Cap5);
	click(Cap5);
	WaitClick(btn_SerialRange);
	click(btn_SerialRange);
	JavascriptExecutor j2 = (JavascriptExecutor)driver;
	j2.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",1)");
	LocalTime p4 = LocalTime.now();
	sendKeys(SerialFromNu, "SN"+p4);
	sendKeys(SerialLotNu, "LN"+p4);
	sendKeys(SerialBatchNu, "BN"+p4);
	WaitClick(SerialExpiryDate);
	click(SerialExpiryDate);
	WaitElement(nextdate);
	selectText(nextdate, "2030");
	WaitClick(BatchExpiryDateSel);
	click(BatchExpiryDateSel);
	pressEnter(SerialFromNu);
	WaitClick(btn_updateSerial);
	click(btn_updateSerial);
	
	Actions action6 = new Actions(driver);
	WebElement a6 = driver.findElement(By.xpath(Cap6));
	action6.moveToElement(a6).build().perform();
	WaitClick(Cap6);
	click(Cap6);
	WaitClick(btn_SerialRange);
	click(btn_SerialRange);
	JavascriptExecutor j3 = (JavascriptExecutor)driver;
	j3.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",1)");
	LocalTime p5 = LocalTime.now();
	sendKeys(SerialFromNu, "SN"+p5);
	sendKeys(SerialLotNu, "LN"+p5);
	sendKeys(SerialBatchNu, "BN"+p5);
	WaitClick(SerialExpiryDate);
	click(SerialExpiryDate);
	WaitElement(nextdate);
	selectText(nextdate, "2030");
	WaitClick(BatchExpiryDateSel);
	click(BatchExpiryDateSel);
	pressEnter(SerialFromNu);
	WaitClick(btn_updateSerial);
	click(btn_updateSerial);
	
	Actions action7 = new Actions(driver);
	WebElement a7 = driver.findElement(By.xpath(Cap7));
	action7.moveToElement(a7).build().perform();
	WaitClick(Cap7);
	click(Cap7);
	WaitClick(btn_SerialRange);
	click(btn_SerialRange);
	JavascriptExecutor j4 = (JavascriptExecutor)driver;
	j4.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",1)");
	LocalTime p6 = LocalTime.now();
	sendKeys(SerialFromNu, "SN"+p6);
	sendKeys(SerialLotNu, "LN"+p6);
	WaitClick(SerialExpiryDate);
	click(SerialExpiryDate);
	WaitElement(nextdate);
	selectText(nextdate, "2030");
	WaitClick(BatchExpiryDateSel);
	click(BatchExpiryDateSel);
	pressEnter(SerialFromNu);
	WaitClick(btn_updateSerial);
	click(btn_updateSerial);
	
	Thread.sleep(3000);
	click(btn_closeProductList);
	
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	
	WaitElement(btn_btnGotopage);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify Inbound Shipment Document Released","Inbound Shipment Document Released", "Inbound Shipment Document is Released", "pass");
	} else {
		writeTestResults("verify Inbound Shipment Document Released","Inbound Shipment Document Released", "Inbound Shipment Document is not Released", "fail");
	}
	
	WaitClick(btn_btnGotopage);
	click(btn_btnGotopage);
	
	switchWindow();
	if(isDisplayed(txt_ShipmentCostingInformationpage)) {
		writeTestResults("User enter Shipment Costing Information page","view  Shipment Costing Information page", "Shipment Costing Information page is display", "pass");
	}else {
		writeTestResults("User enter Shipment Costing Information page","view  Shipment Costing Information page", "Shipment Costing Information is not display", "fail");
	}
	
//	WaitElement(btn_btnCheckout);
//	click(btn_btnCheckout);
	
	Thread.sleep(3000);
	click(btn_btndraft);
	
	WaitElement(pageDraft);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Shipment Costing Information Document Draft","Shipment Costing Information Document Draft", "Shipment Costing Information Document is Draft", "pass");
	} else {
		writeTestResults("verify Shipment Costing Information Document Draft","Shipment Costing Information Document Draft", "Shipment Costing Information Document is not Draft", "fail");

	}
	
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	
	WaitElement(pageRelease);
	if (isDisplayed(pageRelease)){

		writeTestResults("verify Shipment Costing Information Document Released","Shipment Costing Information Document Released", "Shipment Costing Information Document is Released", "pass");
		System.out.println("PM_PI_001 Test case is Completed");
	} else {
		writeTestResults("verify Shipment Costing Information Document Released","Shipment Costing Information Document Released", "Shipment Costing Information Document is not Released", "fail");
	}
}

public void completePurchaseInvoiceToInboundShipment() throws Exception {
	click(btn_PurchaseInvoicebutton);
	if(isDisplayed(txt_PurchaseInvoicebuttonpage)) {
		writeTestResults("verify Purchase Invoice page","view Purchase Invoice page", "Purchase Invoice page is display", "pass");
	}else {
		writeTestResults("verify Purchase Invoice page","view Purchase Invoice page", "Purchase Invoice page is not display", "fail");
	}
	click(btn_newPurchaseInvoicebutton);
	Thread.sleep(3000);
	click(btn_btnPIToInboundShipment);
	if(isDisplayed(txt_newPurchaseInvoicebuttonpage)&&(getText(Status).contentEquals("New"))) {
		writeTestResults("verify New Purchase Invoice page","view New Purchase Invoice page", "New Purchase Invoice page is display", "pass");
	}else {
		writeTestResults("verify Purchase Invoice page","view New Purchase Invoice page", "New Purchase Invoice page is not display", "fail");
	}
	Thread.sleep(3000);
	click(btn_btnPIVendor);
	Thread.sleep(3000);
	sendKeys(txt_txtPIVendor, txtPIVendor);
	Thread.sleep(3000);
	pressEnter(txt_txtPIVendor);
	Thread.sleep(3000);
	doubleClick(sel_selPIVendor);
	selectText(txt_PIcurrency, PIcurrency);
	
	click(btn_btnPIBillingAddress);
	sendKeys(txt_txtPIBillingAddress, txtPIBillingAddress);
	click(btn_btnApply);
	click(btn_btnPIShippingAddress);
	sendKeys(txt_txtPIShippingAddress, txtPIShippingAddress);
	click(btn_btnApply);
	
	sendKeys(Product, pro7.readTestCreation("BatchSpecific"));
	Thread.sleep(3000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(3000);
	pressEnter(Product);
	sendKeys(ProductQty, minOrderQty);
	Thread.sleep(3000);
	pressEnter(ProductQty);
	selectText(WarehousePI1, "WH-PARTS [Main Warehouse - Parts]");
	
	sendKeys(Product, pro7.readTestCreation("BatchFifo"));
	Thread.sleep(3000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(3000);
	pressEnter(Product);
	sendKeys(ProductQty, minOrderQty);
	Thread.sleep(3000);
	pressEnter(ProductQty);
	selectText(WarehousePI2, "WH-PARTS [Main Warehouse - Parts]");
	
	sendKeys(Product, pro7.readTestCreation("Lot"));
	Thread.sleep(3000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(3000);
	pressEnter(Product);
	sendKeys(ProductQty, minOrderQty);
	Thread.sleep(3000);
	pressEnter(ProductQty);
	selectText(WarehousePI3, "WH-PARTS [Main Warehouse - Parts]");
	
	sendKeys(Product, pro7.readTestCreation("SerielSpecific"));
	Thread.sleep(3000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(3000);
	pressEnter(Product);
	sendKeys(ProductQty, minOrderQty);
	Thread.sleep(3000);
	pressEnter(ProductQty);
	selectText(WarehousePI4, "WH-PARTS [Main Warehouse - Parts]");
	
	sendKeys(Product, pro7.readTestCreation("SerielBatchSpecific"));
	Thread.sleep(3000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(3000);
	pressEnter(Product);
	sendKeys(ProductQty, minOrderQty);
	Thread.sleep(3000);
	pressEnter(ProductQty);
	selectText(WarehousePI5, "WH-PARTS [Main Warehouse - Parts]");
	
	sendKeys(Product, pro7.readTestCreation("SerielBatchFifo"));
	Thread.sleep(3000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(3000);
	pressEnter(Product);
	sendKeys(ProductQty, minOrderQty);
	Thread.sleep(3000);
	pressEnter(ProductQty);
	selectText(WarehousePI6, "WH-PARTS [Main Warehouse - Parts]");
	
	sendKeys(Product, pro7.readTestCreation("SerirlFifo"));
	Thread.sleep(3000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(3000);
	pressEnter(Product);
	sendKeys(ProductQty, minOrderQty);
	Thread.sleep(3000);
	pressEnter(ProductQty);
	selectText(WarehousePI7, "WH-PARTS [Main Warehouse - Parts]");
	
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Invoice Document Draft","Purchase Invoice Document Draft", "Purchase Invoice Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Invoice Document Draft","Purchase Invoice Document Draft", "Purchase Invoice Document is not Draft", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	Thread.sleep(5000);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify Purchase Invoice Document Released","Purchase Invoice Document Released", "Purchase Invoice Document is Released", "pass");
	} else {
		writeTestResults("verify Purchase Invoice Document Released","Purchase Invoice Document Released", "Purchase Invoice Document is not Released", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnGotopage);
	
	switchWindow();
	if(isDisplayed(txt_InboundShipmentpage)) {
		writeTestResults("User enter Inbound Shipment page","view  Inbound Shipment page", "Inbound Shipment page is display", "pass");
	}else {
		writeTestResults("User enter Inbound Shipment page","view  Inbound Shipment page", "Inbound Shipment page is not display", "fail");
	}
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify enter Inbound Shipment Document Draft","enter Inbound Shipment Document Draft", "enter Inbound Shipment Document is Draft", "pass");
	} else {
		writeTestResults("verify enter Inbound Shipment Document Draft","enter Inbound Shipment Document Draft", "enter Inbound Shipment Document is not Draft", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnSerialBatch);
	
	Thread.sleep(4000);
	Actions action1 = new Actions(driver);
	WebElement a1 = driver.findElement(By.xpath(Cap1));
	action1.moveToElement(a1).build().perform();
	click(Cap1);
	LocalTime p1 = LocalTime.now();
	sendKeys(BatchNu, "BN"+p1);
	sendKeys(BatchLotNu, "LN"+p1);
	Thread.sleep(3000);
	click(BatchExpiryDate);
	Thread.sleep(3000);
	selectText(nextdate, "2030");
	Thread.sleep(3000);
	click(BatchExpiryDateSel);
	Thread.sleep(3000);
	pressEnter(BatchNu);
	Thread.sleep(3000);
	click(btn_updateSerial);
	
	Actions action2 = new Actions(driver);
	WebElement a2 = driver.findElement(By.xpath(Cap2));
	action2.moveToElement(a2).build().perform();
	click(Cap2);
	LocalTime p2 = LocalTime.now();
	sendKeys(BatchNu, "BN"+p2);
	sendKeys(BatchLotNu, "LN"+p2);
	Thread.sleep(3000);
	click(BatchExpiryDate);
	Thread.sleep(3000);
	selectText(nextdate, "2030");
	Thread.sleep(3000);
	click(BatchExpiryDateSel);
	Thread.sleep(3000);
	pressEnter(BatchNu);
	Thread.sleep(3000);
	click(btn_updateSerial);
	
	Actions action4 = new Actions(driver);
	WebElement a4 = driver.findElement(By.xpath(Cap4));
	action4.moveToElement(a4).build().perform();
	click(Cap4);
	Thread.sleep(3000);
	click(btn_SerialRange);
	Thread.sleep(3000);
	JavascriptExecutor j1 = (JavascriptExecutor)driver;
	j1.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",1)");
	LocalTime p3 = LocalTime.now();
	sendKeys(SerialFromNu, "SN"+p3);
	sendKeys(SerialLotNu, "LN"+p3);
	Thread.sleep(3000);
	click(SerialExpiryDate);
	Thread.sleep(3000);
	selectText(nextdate, "2030");
	Thread.sleep(3000);
	click(BatchExpiryDateSel);
	Thread.sleep(3000);
	pressEnter(SerialFromNu);
	Thread.sleep(3000);
	click(btn_updateSerial);
	
	Actions action5 = new Actions(driver);
	WebElement a5 = driver.findElement(By.xpath(Cap5));
	action5.moveToElement(a5).build().perform();
	click(Cap5);
	Thread.sleep(3000);
	click(btn_SerialRange);
	Thread.sleep(3000);
	JavascriptExecutor j2 = (JavascriptExecutor)driver;
	j2.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",1)");
	LocalTime p4 = LocalTime.now();
	sendKeys(SerialFromNu, "SN"+p4);
	sendKeys(SerialLotNu, "LN"+p4);
	sendKeys(SerialBatchNu, "BN"+p4);
	Thread.sleep(3000);
	click(SerialExpiryDate);
	Thread.sleep(3000);
	selectText(nextdate, "2030");
	Thread.sleep(3000);
	click(BatchExpiryDateSel);
	Thread.sleep(3000);
	pressEnter(SerialFromNu);
	Thread.sleep(3000);
	click(btn_updateSerial);
	
	Actions action6 = new Actions(driver);
	WebElement a6 = driver.findElement(By.xpath(Cap6));
	action6.moveToElement(a6).build().perform();
	click(Cap6);
	Thread.sleep(3000);
	click(btn_SerialRange);
	Thread.sleep(3000);
	JavascriptExecutor j3 = (JavascriptExecutor)driver;
	j3.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",1)");
	LocalTime p5 = LocalTime.now();
	sendKeys(SerialFromNu, "SN"+p5);
	sendKeys(SerialLotNu, "LN"+p5);
	sendKeys(SerialBatchNu, "BN"+p5);
	Thread.sleep(3000);
	click(SerialExpiryDate);
	Thread.sleep(3000);
	selectText(nextdate, "2030");
	Thread.sleep(3000);
	click(BatchExpiryDateSel);
	Thread.sleep(3000);
	pressEnter(SerialFromNu);
	Thread.sleep(3000);
	click(btn_updateSerial);
	
	Actions action7 = new Actions(driver);
	WebElement a7 = driver.findElement(By.xpath(Cap7));
	action7.moveToElement(a7).build().perform();
	click(Cap7);
	Thread.sleep(3000);
	click(btn_SerialRange);
	Thread.sleep(3000);
	JavascriptExecutor j4 = (JavascriptExecutor)driver;
	j4.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",1)");
	LocalTime p6 = LocalTime.now();
	sendKeys(SerialFromNu, "SN"+p6);
	sendKeys(SerialLotNu, "LN"+p6);
	Thread.sleep(3000);
	click(SerialExpiryDate);
	Thread.sleep(3000);
	selectText(nextdate, "2030");
	Thread.sleep(3000);
	click(BatchExpiryDateSel);
	Thread.sleep(3000);
	pressEnter(SerialFromNu);
	Thread.sleep(3000);
	click(btn_updateSerial);
	
	Thread.sleep(3000);
	click(btn_closeProductList);
	
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	Thread.sleep(5000);
	if (isDisplayed(pageRelease)){

		writeTestResults("verify Inbound Shipment Document Released","Inbound Shipment Document Released", "Inbound Shipment Document is Released", "pass");
		System.out.println("PM_PI_002 Test case is Completed");
	} else {
		writeTestResults("verify Inbound Shipment Document Released","Inbound Shipment Document Released", "Inbound Shipment Document is not Released", "fail");

	}
}

public void completePurchaseInvoiceService() throws Exception {
	
	click(btn_PurchaseInvoicebutton);
	if(isDisplayed(txt_PurchaseInvoicebuttonpage)) {
		writeTestResults("verify Purchase Invoice page","view Purchase Invoice page", "Purchase Invoice page is display", "pass");
	}else {
		writeTestResults("verify Purchase Invoice page","view Purchase Invoice page", "Purchase Invoice page is not display", "fail");
	}

	click(btn_newPurchaseInvoicebutton);
	Thread.sleep(3000);
	click(btn_btnPIService);
	if(isDisplayed(txt_newPurchaseInvoicebuttonpage)&&(getText(Status).contentEquals("New"))) {
		writeTestResults("verify New Purchase Invoice page","view New Purchase Invoice page", "New Purchase Invoice page is display", "pass");
	}else {
		writeTestResults("verify Purchase Invoice page","view New Purchase Invoice page", "New Purchase Invoice page is not display", "fail");
	}
	Thread.sleep(3000);
	click(btn_btnPIVendor);
	Thread.sleep(3000);
	sendKeys(txt_txtPIVendor, txtPIVendor);
	Thread.sleep(3000);
	pressEnter(txt_txtPIVendor);
	Thread.sleep(3000);
	doubleClick(sel_selPIVendor);
	
	click(btn_btnPIBillingAddress);
	sendKeys(txt_txtPIBillingAddress, txtPIBillingAddress);
	click(btn_btnApply);
	click(btn_btnPIShippingAddress);
	sendKeys(txt_txtPIShippingAddress, txtPIShippingAddress);
	click(btn_btnApply);
	click(btn_Pbtndetails);
	click(btn_btnPIsummary);
	
	sendKeys(Product, PItxtSproduct);
	Thread.sleep(3000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(3000);
	pressEnter(Product);
	sendKeys(ProductQty, minOrderQty);
	Thread.sleep(3000);
	pressEnter(ProductQty);
	selectText(WarehousePI1, "WH-PARTS [Main Warehouse - Parts]");

	
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Invoice Document Draft","Purchase Invoice Document Draft", "Purchase Invoice Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Invoice Document Draft","Purchase Invoice Document Draft", "Purchase Invoice Document is not Draft", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	Thread.sleep(5000);
	if (isDisplayed(pageRelease)){

		writeTestResults("verify Purchase Invoice Document Released","Purchase Invoice Document Released", "Purchase Invoice Document is Released", "pass");
		System.out.println("PM_PI_003 Test case is Completed");
	} else {
		writeTestResults("verify Purchase Invoice Document Released","Purchase Invoice Document Released", "Purchase Invoice Document is not Released", "fail");

	}
}

public void completePurchaseReturnJourneyUsingDocumentList() throws Exception {
	
	click(btn_PurchaseReturnOrderbutton);
	click(btn_newPurchaseReturnOrderbutton);
	
	driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
	click(btn_btnPROVendor);
	sendKeys(txt_txtPROVendor, txtPROVendor);
	Thread.sleep(3000);
	pressEnter(txt_txtPROVendor);
	WaitElement(sel_selPROVendor);
	doubleClick(sel_selPROVendor);
	
	
	click(btn_btnPOBillingAddress);
	sendKeys(txt_txtPOBillingAddress, txtPOBillingAddress);
	click(btn_btnPOApply);
	click(btn_btnPOShippingAddress);
	sendKeys(txt_txtPOShippingAddress, txtPOShippingAddress);
	click(btn_btnPOApply);
	Thread.sleep(3000);
	JavascriptExecutor pr1 = (JavascriptExecutor)driver;
	pr1.executeScript("viewRefDocList()");
	
	
	InventoryAndWarehouseModule obj = new InventoryAndWarehouseModule();
	WaitElement("//input[@id='txtSearchText']");
	sendKeys("//input[@id='txtSearchText']", obj.readTestCreation("BatchSpecific"));
	WaitClick(btn_PROsearch);
	click(btn_PROsearch);
//	InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();
//	obj1.customizeLoadingDelay("(//div[text()=\"Purchase Order\"])[1]", 80);
	Thread.sleep(20000);
	click(btn_PROcheckbox);
	Thread.sleep(3000);
	JavascriptExecutor j1 = (JavascriptExecutor)driver;
	j1.executeScript("$(\"#divDocList\").parent().find('.dialogbox-buttonarea > .button').click()");
	sendKeys(txt_PROqty, PROqty);
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Return Order Document Draft","Purchase Return Order Document Draft", "Purchase Return Order Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Return Order Document Draft","Purchase Return Order Document Draft", "Purchase Return Order Document is not Draft", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	Thread.sleep(5000);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify Purchase Return Order Document Released","Purchase Return Order Document Released", "Purchase Return Order Document is Released", "pass");
	} else {
		writeTestResults("verify Purchase Return Order Document Released","Purchase Return Order Document Released", "Purchase Return Order Document is not Released", "fail");
	}
	Thread.sleep(3000);
	click(btn_btnGotopage);

	
	switchWindow();
	
	Thread.sleep(3000);
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_PROdraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Outbound Shipment Document Draft","Outbound Shipment Document Draft", "Outbound Shipment Document is Draft", "pass");
	} else {
		writeTestResults("verify Outbound Shipment Document Draft","Outbound Shipment Document Draft", "Outbound Shipment Document is not Draft", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnSerialBatch);
	Thread.sleep(3000);
	click(btn_btnselProductList);
	Thread.sleep(3000);
	click(btn_btnPOSerialNo);
	Thread.sleep(3000);
	JavascriptExecutor j2 = (JavascriptExecutor)driver;
	j2.executeScript("$(\"#attr-divSerialBatchNos\").jqxGrid('setcellvalue', 0, \"Qty\", \"1\");");
	
	Thread.sleep(3000);
	click(btn_updateSerial);
	Thread.sleep(3000);
	click(btn_closeProductList);
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(3000);
	trackCode= getText(Header);
	Thread.sleep(5000);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify Outbound Shipment Document Released","Outbound Shipment Document Released", "Outbound Shipment Document is Released", "pass");
	} else {
		writeTestResults("verify Outbound Shipment Document Released","Outbound Shipment Document Released", "Outbound Shipment Document is not Released", "fail");
	}
	Thread.sleep(3000);
	click(btn_btnGotopage);

	
	switchWindow();
	
	Thread.sleep(3000);
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Return Invoice Document Draft","Purchase Return Invoice Document Draft", "Purchase Return Invoice Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Return Invoice Document Draft","Purchase Return Invoice Document Draft", "Purchase Return Invoice Document is not Draft", "fail");

	}
	
	Thread.sleep(3000);
	click(btn_release);
	
	Thread.sleep(2000);
	trackCode= getText(Header);
	Thread.sleep(5000);
	if (isDisplayed(pageRelease)){

		writeTestResults("verify Purchase Return Invoice Document Released","Purchase Return Invoice Document Released", "Purchase Return Invoice Document is Released", "pass");
		
	} else {
		writeTestResults("verify Purchase Return Invoice Document Released","Purchase Return Invoice Document Released", "Purchase Return Invoice Document is not Released", "fail");

	}
	
	
}
public void completePurchaseReturnJourneyDirectProducts() throws Exception {
	
	WaitClick(btn_PurchaseReturnOrderbutton);
	click(btn_PurchaseReturnOrderbutton);
	WaitClick(btn_newPurchaseReturnOrderbutton);
	click(btn_newPurchaseReturnOrderbutton);
	
	WaitClick(btn_btnPROVendor);
	click(btn_btnPROVendor);
	WaitElement(txt_txtPROVendor);
	sendKeys(txt_txtPROVendor, txtPROVendor);
	Thread.sleep(3000);
	pressEnter(txt_txtPROVendor);
	Thread.sleep(3000);
	doubleClick(sel_selPROVendor);
	
	click(btn_btnPOBillingAddress);
	sendKeys(txt_txtPOBillingAddress, txtPOBillingAddress);
	click(btn_btnPOApply);
	click(btn_btnPOShippingAddress);
	sendKeys(txt_txtPOShippingAddress, txtPOShippingAddress);
	click(btn_btnPOApply);
	
	ScrollDownAruna();
	
	sendKeys(Product, pro7.readTestCreation("SerielBatchSpecific"));
	Thread.sleep(3000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(3000);
	pressEnter(Product);
	sendKeys(ProductQty, PROqty);
	Thread.sleep(3000);
	pressEnter(ProductQty);
	selectText(btn_POwarehouse, "WH-PARTS [Main Warehouse - Parts]");
	
	WaitClick(btn_PROLook1);
	click(btn_PROLook1);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue);
	poval1= Integer.parseInt(getText(txt_POProductAvailabilityValue).replace(",", ""));
	System.out.println("poval1:"+poval1);
	JavascriptExecutor j9 = (JavascriptExecutor)driver;
	j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	WaitClick(btn_btnCheckout);
	click(btn_btnCheckout);
	
	WaitClick(btn_btndraft);
	click(btn_btndraft);
	WaitElement(pageDraft);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Return Order Document Draft","Purchase Return Order Document Draft", "Purchase Return Order Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Return Order Document Draft","Purchase Return Order Document Draft", "Purchase Return Order Document is not Draft", "fail");
	}
	
	WaitClick(btn_release);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	
	WaitElement(btn_btnGotopage);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify Purchase Return Order Document Released","Purchase Return Order Document Released", "Purchase Return Order Document is Released", "pass");
	} else {
		writeTestResults("verify Purchase Return Order Document Released","Purchase Return Order Document Released", "Purchase Return Order Document is not Released", "fail");
	}
	
	WaitClick(btn_btnGotopage);
	click(btn_btnGotopage);
	
	switchWindow();
	
	Thread.sleep(3000);
	click(btn_btnCheckout);
	WaitClick(btn_PROdraft);
	click(btn_PROdraft);
	
	WaitElement(pageDraft);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Outbound Shipment Document Draft","Outbound Shipment Document Draft", "Outbound Shipment Document is Draft", "pass");
	} else {
		writeTestResults("verify Outbound Shipment Document Draft","Outbound Shipment Document Draft", "Outbound Shipment Document is not Draft", "fail");
	}
	
	WaitClick(btn_POMasterInfo);
	click(btn_POMasterInfo);
	WaitClick(btn_POProductBS);
	click(btn_POProductBS);
	WaitElement(txt_POSNo);
	number = getText(txt_POSNo);
	WaitClick(btn_headerclose);
	click(btn_headerclose);
	
	Thread.sleep(3000);
	click(btn_btnSerialBatch);
	Thread.sleep(3000);
	click(btn_btnselProductList);
	WaitElement(txt_POSerialNo);
	sendKeys(txt_POSerialNo,number);
	pressEnter(txt_POSerialNo);
//	WaitClick(btn_btnPOSerialNo);
//	click(btn_btnPOSerialNo);
	Thread.sleep(3000);
	click(btn_updateSerial);
	Thread.sleep(3000);
	click(btn_closeProductList);
	
	WaitClick(btn_release);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	WaitElement(btn_btnGotopage);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify Outbound Shipment Document Released","Outbound Shipment Document Released", "Outbound Shipment Document is Released", "pass");
	} else {
		writeTestResults("verify Outbound Shipment Document Released","Outbound Shipment Document Released", "Outbound Shipment Document is not Released", "fail");
	}
	WaitClick(btn_btnGotopage);
	click(btn_btnGotopage);
	
	switchWindow();
	
	Thread.sleep(3000);
	click(btn_btnCheckout);
	WaitClick(btn_btndraft);
	click(btn_btndraft);
	WaitElement(pageDraft);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Return Invoice Document Draft","Purchase Return Invoice Document Draft", "Purchase Return Invoice Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Return Invoice Document Draft","Purchase Return Invoice Document Draft", "Purchase Return Invoice Document is not Draft", "fail");

	}
	
	WaitClick(btn_release);
	click(btn_release);
	
	Thread.sleep(2000);
	trackCode= getText(Header);
	WaitElement(pageRelease);
	if (isDisplayed(pageRelease)){

		writeTestResults("verify Purchase Return Invoice Document Released","Purchase Return Invoice Document Released", "Purchase Return Invoice Document is Released", "pass");
		System.out.println("PM_PRO_002 Test case is Completed");
	} else {
		writeTestResults("verify Purchase Return Invoice Document Released","Purchase Return Invoice Document Released", "Purchase Return Invoice Document is not Released", "fail");

	}
	
	WaitClick(btn_PRILook1);
	click(btn_PRILook1);
	WaitClick(btn_POProductAvailability);
	click(btn_POProductAvailability);
	WaitElement(txt_POProductAvailabilityValue1);
	pival1= Integer.parseInt(getText(txt_POProductAvailabilityValue1));
	System.out.println("pival1:"+pival1);
	JavascriptExecutor j10 = (JavascriptExecutor)driver;
	j10.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
	
	
	if (pival1==(poval1-1)){
		writeTestResults("verify Outbound Stock Quantity","Outbound Stock Quantity Correct", "Outbound Stock Quantity Is Correct", "pass");
	} else {
		writeTestResults("verify Outbound Stock Quantity","Outbound Stock Quantity Correct", "Outbound Stock Quantity Is Not Correct", "fail");
	}
}

public void releaseRFQ() throws Exception {
	WaitClick(btn_RequestForQuotation);
	click(btn_RequestForQuotation);
	
	WaitElement(txt_RequestForQuotationpage);
	if(isDisplayed(txt_RequestForQuotationpage)) {
		writeTestResults("verify Request For Quotation page","view Request For Quotation page", "Request For Quotation page is display", "pass");
	}else {
		writeTestResults("verify Request For Quotation page","view Request For Quotation page", "Request For Quotation page is not display", "fail");
	}
	
	WaitClick(btn_NewRequestForQuotation);
	click(btn_NewRequestForQuotation);
	if(isDisplayed(txt_NewRequestForQuotationpage)&&(getText(Header).contentEquals("New"))) {
		writeTestResults("verify New Request For Quotation page","view New Request For Quotation page", "New Request For Quotation page is display", "pass");
	}else {
		writeTestResults("verify New Request For Quotation page","view New Request For Quotation page", "New Request For Quotation page is not display", "fail");
	}
	

	Thread.sleep(3000);

	JavascriptExecutor j7 = (JavascriptExecutor)driver;
	j7.executeScript("$(\"#txtdeadLine\").datepicker(\"setDate\", new Date())");



	Thread.sleep(3000);
	click(btn_btncontactperson);
	sendKeys(txt_txtcontactperson, txtcontactperson);
	pressEnter(txt_txtcontactperson);
	doubleClick(sel_selcontactperson);
	
	WaitClick(btn_btnRFQVendor);
	click(btn_btnRFQVendor);
	WaitElement(txt_txtRFQVendor);
	sendKeys(txt_txtRFQVendor, txtRFQVendor);
	Thread.sleep(3000);
	pressEnter(txt_txtRFQVendor);
	Thread.sleep(3000);
	doubleClick(sel_selRFQVendor);
	
	ScrollDownAruna();
	
	Thread.sleep(3000);
	click(RFQPro1);
	Thread.sleep(3000);
	sendKeys(txt_ptxtproduct,pro7.readTestCreation("BatchSpecific"));
	Thread.sleep(3000);
	pressEnter(txt_ptxtproduct);
	Thread.sleep(3000);
	doubleClick(sel_pselproduct);
	Thread.sleep(3000);
	click(RFQPlus1);
	
	Thread.sleep(3000);
	click(PRPro2);
	Thread.sleep(3000);
	sendKeys(txt_Rtxtproduct, pro7.readTestCreation("BatchFifo"));
	Thread.sleep(3000);
	pressEnter(txt_Rtxtproduct);
	Thread.sleep(3000);
	doubleClick(sel_Rselproduct);
	Thread.sleep(3000);
	click(RFQPlus2);
	
	Thread.sleep(3000);
	click(PRPro3);
	Thread.sleep(3000);
	sendKeys(txt_Rtxtproduct, pro7.readTestCreation("Lot"));
	Thread.sleep(3000);
	pressEnter(txt_Rtxtproduct);
	Thread.sleep(3000);
	doubleClick(sel_Rselproduct);
	Thread.sleep(3000);
	click(RFQPlus3);
	
	Thread.sleep(3000);
	click(PRPro4);
	Thread.sleep(3000);
	sendKeys(txt_Rtxtproduct, pro7.readTestCreation("SerielSpecific"));
	Thread.sleep(3000);
	pressEnter(txt_Rtxtproduct);
	Thread.sleep(3000);
	doubleClick(sel_Rselproduct);
	Thread.sleep(3000);
	click(RFQPlus4);
	
	Thread.sleep(3000);
	click(PRPro5);
	Thread.sleep(3000);
	sendKeys(txt_Rtxtproduct, pro7.readTestCreation("SerielBatchSpecific"));
	Thread.sleep(3000);
	pressEnter(txt_Rtxtproduct);
	Thread.sleep(3000);
	doubleClick(sel_Rselproduct);
	Thread.sleep(3000);
	click(RFQPlus5);
	
	Thread.sleep(3000);
	click(PRPro6);
	Thread.sleep(3000);
	sendKeys(txt_Rtxtproduct, pro7.readTestCreation("SerielBatchFifo"));
	Thread.sleep(3000);
	pressEnter(txt_Rtxtproduct);
	Thread.sleep(3000);
	doubleClick(sel_Rselproduct);
	Thread.sleep(3000);
	click(RFQPlus6);
	
	Thread.sleep(3000);
	click(PRPro7);
	Thread.sleep(3000);
	sendKeys(txt_Rtxtproduct, pro7.readTestCreation("SerirlFifo"));
	Thread.sleep(3000);
	pressEnter(txt_Rtxtproduct);
	Thread.sleep(3000);
	doubleClick(sel_Rselproduct);

	

//	sendKeys(txt_RFQqty,RFQqty);
	WaitClick(btn_btndraft);
	click(btn_btndraft);
	WaitElement(pageDraft);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Request For Quotation Document Draft","Request For Quotation Document Draft", "Request For Quotation Document is Draft", "pass");
	} else {
		writeTestResults("verify Request For Quotation Document Draft","Request For Quotation Document Draft", "Request For Quotation Document is not Draft", "fail");

	}
	
	WaitClick(btn_release);
	click(btn_release);
	WaitClick(btn_Edit);
	click(btn_Edit);
	
	WaitClick(btn_QuotationTab);
	click(btn_QuotationTab);
	Thread.sleep(3000);
	click(btn_RFQadd);
	WaitElement(txt_RFQaddVendor);
	selectIndex(txt_RFQaddVendor, 1);
	LocalTime myObj = LocalTime.now();
	WaitElement(txt_RFQQuotationNo);
	sendKeys(txt_RFQQuotationNo, RFQQuotationNo+myObj);
	WaitElement(txt_RFQShippingTerm);
	selectIndex(txt_RFQShippingTerm, 2);
	
	sendKeys(txt_RFQUnitPrice, RFQUnitPrice);
	sendKeys(RFQUnitPrice2, RFQUnitPrice);
	sendKeys(RFQUnitPrice3, RFQUnitPrice);
	sendKeys(RFQUnitPrice4, RFQUnitPrice);
	sendKeys(RFQUnitPrice5, RFQUnitPrice);
	sendKeys(RFQUnitPrice6, RFQUnitPrice);
	sendKeys(RFQUnitPrice7, RFQUnitPrice);
	
	sendKeys(txt_RFQMinQty, RFQMinQty);
	sendKeys(RFQMinQty2, RFQMinQty);
	sendKeys(RFQMinQty3, RFQMinQty);
	sendKeys(RFQMinQty4, RFQMinQty);
	sendKeys(RFQMinQty5, RFQMinQty);
	sendKeys(RFQMinQty6, RFQMinQty);
	sendKeys(RFQMinQty7, RFQMinQty);
	
	sendKeys(txt_RFQMaxQty, RFQMaxQty);
	sendKeys(RFQMaxQty2, RFQMaxQty);
	sendKeys(RFQMaxQty3, RFQMaxQty);
	sendKeys(RFQMaxQty4, RFQMaxQty);
	sendKeys(RFQMaxQty5, RFQMaxQty);
	sendKeys(RFQMaxQty6, RFQMaxQty);
	sendKeys(RFQMaxQty7, RFQMaxQty);
	
	WaitClick(btn_RFQcheckout);
	click(btn_RFQcheckout);
	Thread.sleep(3000);
	click(btn_RFQapply);
	JavascriptExecutor jse = (JavascriptExecutor)driver;
	jse.executeScript("$(closeMessageBox()).click()");
	WaitClick(btn_RFQsummarytab);
	click(btn_RFQsummarytab);
	Thread.sleep(5000);
	click(btn_qualifiedvendorbutton);
	
	click(btn_Qualifiedtik);
	click(Qualifiedtik2);
	click(Qualifiedtik3);
	click(Qualifiedtik4);
	click(Qualifiedtik5);
	click(Qualifiedtik6);
	click(Qualifiedtik7);
	
	Thread.sleep(3000);
	JavascriptExecutor j1 = (JavascriptExecutor)driver;
	j1.executeScript("$(\"#priceTab\").parent().find('.dialogbox-buttonarea > .button').click()");
	
	WaitClick(btn_btnRFQapply);
	click(btn_btnRFQapply);
	
	Thread.sleep(3000);
	click(btn_Update);
	
	WaitClick(btn_release);
	click(btn_release);
	
	Thread.sleep(2000);
	trackCode= getText(Header);
	
	WaitElement(pageRelease);
	if (isDisplayed(pageRelease)){

		writeTestResults("verify Request For Quotation Document Released","Request For Quotation Document Released", "Request For Quotation Document is Released", "pass");
	} else {
		writeTestResults("verify Request For Quotation Document Released","Request For Quotation Document Released", "Request For Quotation Document is not Released", "fail");

	}
	
	Thread.sleep(2000);
	click(btn_btnGeneratePO);
	click(btn_btnPOtoShipmentCosting);
	
	switchWindow();
	//click(btn_RRDclose);
	Thread.sleep(3000);
	click(btn_btnBillingAddress);
	sendKeys(txt_txtBillingAddress, txtBillingAddress);
	click(btn_btnApply);
	click(btn_btnShippingAddress);
	sendKeys(txt_txtShippingAddress, txtShippingAddress);
	click(btn_btnApply);
	
//	Thread.sleep(2000);
//	click(btn_pbtnproduct);
//	Thread.sleep(2000);
//	sendKeys(txt_ptxtproduct,ptxtproduct);
//	Thread.sleep(2000);
//	pressEnter(txt_ptxtproduct);
//	Thread.sleep(2000);
//	doubleClick(sel_pselproduct.replace("productname", ptxtproduct));
	
	WaitClick(btn_btnCheckout);
	click(btn_btnCheckout);
	WaitClick(btn_btndraft);
	click(btn_btndraft);
	WaitElement(pageDraft);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Order Document Draft","Purchase Order Document Draft", "Purchase Order Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Order Document Draft","Purchase Order Document Draft", "Purchase Order Document is not Draft", "fail");
	}
	
	WaitClick(btn_release);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	WaitElement(btn_btnGotopage);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify Purchase Order Document Released","Purchase Order Document Released", "Purchase Order Document is Released", "pass");
	} else {
		writeTestResults("verify Purchase Order Document Released","Purchase Order Document Released", "Purchase Order Document is not Released", "fail");
	}
}

	
public void VerifyWhetherUserCanBeAbleToCreateAImportCostEstimation() throws Exception {

	navigateToTheLoginPage();
	verifyTheLogo();
	userLogin();
	clickNavigation();
	clickProbutton();
	click(btn_ImportCostEstimation);
	Thread.sleep(3000);
	if(isDisplayed(ImportCostEstimationpage)) {
		writeTestResults("verify Import Cost Estimation page","view Import Cost Estimation page", "Import Cost Estimation page is display", "pass");
	}else {
		writeTestResults("verify Import Cost Estimation page","view Import Cost Estimation page", "Import Cost Estimation page is not display", "fail");
	}
	
	click(btn_newImportCostEstimation);
	Thread.sleep(3000);
	if(isDisplayed(newImportCostEstimationpage)) {
		writeTestResults("verify New Import Cost Estimation page","view New Import Cost Estimation page", "New Import Cost Estimation page is display", "pass");
	}else {
		writeTestResults("verify New Import Cost Estimation page","view New Import Cost Estimation page", "New Import Cost Estimation page is not display", "fail");
	}
	
	sendKeys(txt_ICEDescription, ICEDescription);
	
	Thread.sleep(2000);
	click(btn_ICEProductbutoon);
	Thread.sleep(2000);
	sendKeys(txt_ICEProducttxt, ICEProducttxt);
	Thread.sleep(2000);
	pressEnter(txt_ICEProducttxt);
	Thread.sleep(2000);
	doubleClick(sel_ICEProductsel);
	
	click(btn_ICEEstimationDetails);
	Thread.sleep(3000);
	selectIndex(txt_ICECostingType, 1);
	sendKeys(txt_ICEEstimateCost, ICEEstimateCost);
	
	click(btn_ICESummary);
	
	Thread.sleep(3000);
	click(btn_ICECheckout);
	Thread.sleep(3000);
	click(btn_ICEDraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Import Cost Estimation Document Draft","Import Cost Estimation Document Draft", "Import Cost Estimation Document is Draft", "pass");
	} else {
		writeTestResults("verify Import Cost Estimation Document Draft","Import Cost Estimation Document Draft", "Import Cost Estimation Document is not Draft", "fail");

	}
	
	Thread.sleep(3000);
	click(btn_ICERelease);
	Thread.sleep(2000);
	trackCode= getText(Header);
	Thread.sleep(5000);
	if (isDisplayed(pageRelease)){

		writeTestResults("verify Import Cost Estimation Document Released","Import Cost Estimation Document Released", "Import Cost Estimation Document is Released", "pass");
	} else {
		writeTestResults("verify Import Cost Estimation Document Released","Import Cost Estimation Document Released", "Import Cost Estimation Document is not Released", "fail");

	}
}

public void VerifyWhetherUserCanViewAndSelectTheRequisitionInThePurchaseDesk() throws Exception {
	
	navigateToTheLoginPage();
	verifyTheLogo();
	userLogin();
	clickNavigation();
	clickProbutton();
	
	newPurchaseRequisition();
	addPurchaseRequisition();
	
	clickNavigation();
	clickProbutton();
	
	WaitClick(btn_PurchaseDesk);
	click(btn_PurchaseDesk);
	WaitElement(PurchaseDeskpage);
	if(isDisplayed(PurchaseDeskpage)) {
		writeTestResults("verify Purchase Desk page","view Purchase Desk page", "Purchase Desk page is display", "pass");
	}else {
		writeTestResults("verify Purchase Desk page","view Purchase Desk page", "Purchase Desk page is not display", "fail");
	}
	

	//	Thread.sleep(15000);
//	WebDriver drv = null;
//	drv.manage().timeouts().implicitlyWait(20000, TimeUnit.SECONDS);
	Thread.sleep(6000);
	sendKeys(txt_SearchPRN, number);
	
	Thread.sleep(6000);
	click(btn_SearchbuttonPRN);
	
//	Thread.sleep(10000);
	WaitClick(sel_Requisitionsel);
	if(click(sel_Requisitionsel)==true) {
		writeTestResults("Verify User Select the Requisition in the Purchase Desk","User Can Select the Requisition in the Purchase Desk", "User Can Select the Requisition", "pass");
	}else {
		writeTestResults("Verify User Select the Requisition in the Purchase Desk","User Can Select the Requisition in the Purchase Desk", "User Cannot Select the Requisition", "fail");
	}	
}

public void VerifyWhetherUserCanDoAReturnAgainstToThePurchase() throws Exception {
	
	navigateToTheLoginPage();
	verifyTheLogo();
	userLogin();
	clickNavigation();
	clickProbutton();
	
	completePurchaseReturnJourneyUsingDocumentList();
}

public void VerifyWhetherDocumentFlowDisplaysAccurately() throws Exception {
	
	navigateToTheLoginPage();
	verifyTheLogo();
	userLogin();
	clickNavigation();
	clickProbutton();
	
	completePurchaseOrderToShipmentCosting();
	
	Thread.sleep(2000);
	click(btn_btnAction);
	Thread.sleep(2000);
	click(btn_DocFlow);
	
	
	System.out.println(PurchaseOrderNo);
	System.out.println(InboundShipmentNo);
	System.out.println(PurchaseInvoiceNo);
	System.out.println(ShipmentCostingInformationNo);
	
	
	JavascriptExecutor jse = (JavascriptExecutor)driver;
	w =  (String) jse.executeScript("return $($(\"#ctrlDocumentFlow-svg\").find(\".pf-content tspan\")[0]).text()");
	
	x = (String) jse.executeScript("return $($(\"#ctrlDocumentFlow-svg\").find(\".pf-content tspan\")[5]).text()");

	y = (String) jse.executeScript("return $($(\"#ctrlDocumentFlow-svg\").find(\".pf-content tspan\")[10]).text()");
	
	z = (String) jse.executeScript("return $($(\"#ctrlDocumentFlow-svg\").find(\".pf-content tspan\")[20]).text()");
	
	if((PurchaseOrderNo.equals(w))&&(InboundShipmentNo.equals(x))&&(PurchaseInvoiceNo.equals(y))&&(ShipmentCostingInformationNo.equals(z))){
		writeTestResults("verify Doc Flow Details are Correct","Doc Flow Details are Correct", "Doc Flow Details Correct Successfully", "pass");
	}else {
		writeTestResults("verify Doc Flow Details are Correct","Doc Flow Details are Correct", "Doc Flow Details Incorrect", "fail");
	}
}

public void Product7PurchaseOrder()  throws Exception {
	
	navigateToTheLoginPage();
	verifyTheLogo();
	userLogin();
	clickNavigation();
	clickProbutton();
	
	click(btn_PurchaseOrderbutton);
	if(isDisplayed(txt_PurchaseOrderpage)) {
		writeTestResults("verify Purchase Order page","view Purchase Order page", "Purchase Order page is display", "pass");
	}else {
		writeTestResults("verify Purchase Order page","view Purchase Order page", "Purchase Order page is not display", "fail");
	}
	click(btn_newPurchaseOrderbutton);
	
	WaitClick(btn_btnPOtoPurchaseInvoice);
	click(btn_btnPOtoPurchaseInvoice);
	if(isDisplayed(txt_newPurchaseOrderpage)) {
		writeTestResults("verify  New Purchase Order page","view  New Purchase Order page", " New Purchase Order page is display", "pass");
	}else {
		writeTestResults("verify  New Purchase Order page","view  New Purchase Order page", " New Purchase Order page is not display", "fail");
	}
	WaitClick(btn_PbtnVendor);
	click(btn_PbtnVendor);
	WaitElement(txt_txtVendor);
	sendKeys(txt_txtVendor, txtVendor);
	pressEnter(txt_txtVendor);
	Thread.sleep(3000);
	doubleClick(sel_selVendor);
	
	click(btn_btnBillingAddress);
	sendKeys(txt_txtBillingAddress, txtBillingAddress);
	click(btn_btnApply);
	click(btn_btnShippingAddress);
	sendKeys(txt_txtShippingAddress, txtShippingAddress);
	click(btn_btnApply);
	click(btn_Pbtndetails);
	click(btn_btnPInvoiceAccount);
	
	WaitElement(txt_txtVendor);
	sendKeys(txt_txtVendor, txtVendor);
	pressEnter(txt_txtVendor);
	WaitElement(sel_selVendor);
	Thread.sleep(3000);
	doubleClick(sel_selVendor);
	
	WaitElement(btn_btnsummary);
	click(btn_btnsummary);
	
	ScrollDownAruna();
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("BatchSpecific"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, "100");
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse1, "WH-PARTS [Main Warehouse - Parts]");
	sendKeys(pro7price1, "100");
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("BatchFifo"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, "100");
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse2, "WH-PARTS [Main Warehouse - Parts]");
	sendKeys(pro7price2, "100");
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("Lot"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, "100");
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse3, "WH-PARTS [Main Warehouse - Parts]");
	sendKeys(pro7price3, "100");
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("SerielSpecific"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, "100");
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse4, "WH-PARTS [Main Warehouse - Parts]");
	sendKeys(pro7price4, "100");
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("SerielBatchSpecific"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, "100");
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse5, "WH-PARTS [Main Warehouse - Parts]");
	sendKeys(pro7price5, "100");
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("SerielBatchFifo"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, "100");
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse6, "WH-PARTS [Main Warehouse - Parts]");
	sendKeys(pro7price6, "100");
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("SerirlFifo"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, "100");
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse7, "WH-PARTS [Main Warehouse - Parts]");
	sendKeys(pro7price7, "100");

	
	WaitClick(btn_btnCheckout);
	click(btn_btnCheckout);
	
	WaitClick(btn_btndraft);
	click(btn_btndraft);
	
	WaitClick(btn_release);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	PurchaseOrderNo=trackCode;
	
	WaitElement(btn_btnGotopage);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify Purchase Order Document Released","Purchase Order Document Released", "Purchase Order Document is Released", "pass");
	} else {
		writeTestResults("verify Purchase Order Document Released","Purchase Order Document Released", "Purchase Order Document is not Released", "fail");
	}
	WaitClick(btn_btnGotopage);
	click(btn_btnGotopage);
	
	switchWindow();
	if(isDisplayed(txt_InboundShipmentpage)) {
		writeTestResults("User enter Inbound Shipment page","view  Inbound Shipment page", "Inbound Shipment page is display", "pass");
	}else {
		writeTestResults("User enter Inbound Shipment page","view  Inbound Shipment page", "Inbound Shipment page is not display", "fail");
	}
	
	WaitClick(btn_btnCheckout);
	click(btn_btnCheckout);
	
	WaitClick(btn_btndraft);
	click(btn_btndraft);
	
	Thread.sleep(3000);
	click(btn_btnSerialBatch);
	
	Thread.sleep(6000);
	Actions action1 = new Actions(driver);
	WebElement a1 = driver.findElement(By.xpath(Cap1));
	action1.moveToElement(a1).build().perform();	
	WaitClick(Cap1);
	click(Cap1);
	LocalTime p1 = LocalTime.now();
	sendKeys(BatchNu, "BN"+p1);
	sendKeys(BatchLotNu, "LN"+p1);
	WaitClick(BatchExpiryDate);
	click(BatchExpiryDate);
	selectText(nextdate, "2030");
	WaitClick(BatchExpiryDateSel);
	click(BatchExpiryDateSel);
	Thread.sleep(3000);
	pressEnter(BatchNu);
	WaitClick(btn_updateSerial);
	click(btn_updateSerial);
	

	Actions action2 = new Actions(driver);
	WebElement a2 = driver.findElement(By.xpath(Cap2));
	action2.moveToElement(a2).build().perform();
	WaitClick(Cap2);
	click(Cap2);
	LocalTime p2 = LocalTime.now();
	sendKeys(BatchNu, "BN"+p2);
	sendKeys(BatchLotNu, "LN"+p2);
	WaitClick(BatchExpiryDate);
	click(BatchExpiryDate);
	selectText(nextdate, "2030");
	WaitClick(BatchExpiryDateSel);
	click(BatchExpiryDateSel);
	Thread.sleep(3000);
	pressEnter(BatchNu);
	WaitClick(btn_updateSerial);
	click(btn_updateSerial);
	
	Actions action4 = new Actions(driver);
	WebElement a4 = driver.findElement(By.xpath(Cap4));
	action4.moveToElement(a4).build().perform();
	WaitClick(Cap4);
	click(Cap4);
	WaitClick(btn_SerialRange);
	click(btn_SerialRange);
	Thread.sleep(3000);
	JavascriptExecutor j1 = (JavascriptExecutor)driver;
	j1.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",100)");
	LocalTime p3 = LocalTime.now();
	sendKeys(SerialFromNu, "SN"+p3);
	sendKeys(SerialLotNu, "LN"+p3);
	WaitClick(SerialExpiryDate);
	click(SerialExpiryDate);
	selectText(nextdate, "2030");
	WaitClick(BatchExpiryDateSel);
	click(BatchExpiryDateSel);
	Thread.sleep(3000);
	pressEnter(SerialFromNu);
	WaitClick(btn_updateSerial);
	click(btn_updateSerial);

	Actions action5 = new Actions(driver);
	WebElement a5 = driver.findElement(By.xpath(Cap5));
	action5.moveToElement(a5).build().perform();
	WaitClick(Cap5);
	click(Cap5);
	WaitClick(btn_SerialRange);
	click(btn_SerialRange);
	Thread.sleep(3000);
	JavascriptExecutor j2 = (JavascriptExecutor)driver;
	j2.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",100)");
	LocalTime p4 = LocalTime.now();
	sendKeys(SerialFromNu, "SN"+p4);
	sendKeys(SerialLotNu, "LN"+p4);
	sendKeys(SerialBatchNu, "BN"+p4);
	WaitClick(SerialExpiryDate);
	click(SerialExpiryDate);
	selectText(nextdate, "2030");
	WaitClick(BatchExpiryDateSel);
	click(BatchExpiryDateSel);
	Thread.sleep(3000);
	pressEnter(SerialFromNu);
	WaitClick(btn_updateSerial);
	click(btn_updateSerial);

	Actions action6 = new Actions(driver);
	WebElement a6 = driver.findElement(By.xpath(Cap6));
	action6.moveToElement(a6).build().perform();
	WaitClick(Cap6);
	click(Cap6);
	WaitClick(btn_SerialRange);
	click(btn_SerialRange);
	Thread.sleep(3000);
	JavascriptExecutor j3 = (JavascriptExecutor)driver;
	j3.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",100)");
	LocalTime p5 = LocalTime.now();
	sendKeys(SerialFromNu, "SN"+p5);
	sendKeys(SerialLotNu, "LN"+p5);
	sendKeys(SerialBatchNu, "BN"+p5);
	WaitClick(SerialExpiryDate);
	click(SerialExpiryDate);
	selectText(nextdate, "2030");
	WaitClick(BatchExpiryDateSel);
	click(BatchExpiryDateSel);
	Thread.sleep(3000);
	pressEnter(SerialFromNu);
	WaitClick(btn_updateSerial);
	click(btn_updateSerial);

	Actions action7 = new Actions(driver);
	WebElement a7 = driver.findElement(By.xpath(Cap7));
	action7.moveToElement(a7).build().perform();
	WaitClick(Cap7);
	click(Cap7);
	WaitClick(btn_SerialRange);
	click(btn_SerialRange);
	Thread.sleep(3000);
	JavascriptExecutor j4 = (JavascriptExecutor)driver;
	j4.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",100)");
	LocalTime p6 = LocalTime.now();
	sendKeys(SerialFromNu, "SN"+p6);
	sendKeys(SerialLotNu, "LN"+p6);
	click(SerialExpiryDate);
	selectText(nextdate, "2030");
	WaitClick(BatchExpiryDateSel);
	click(BatchExpiryDateSel);
	Thread.sleep(3000);
	pressEnter(SerialFromNu);
	WaitClick(btn_updateSerial);
	click(btn_updateSerial);
	
	WaitClick(btn_closeProductList);
	click(btn_closeProductList);
	
	
	WaitClick(btn_release);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	InboundShipmentNo=trackCode;
	WaitElement(btn_btnGotopage);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify enter Inbound Shipment Document Released","enter Inbound Shipment Document Released", "enter Inbound Shipment Document is Released", "pass");
	} else {
		writeTestResults("verify enter Inbound Shipment Document Released","enter Inbound Shipment Document Released", "enter Inbound Shipment Document is not Released", "fail");
	}
	
	WaitClick(btn_btnGotopage);
	click(btn_btnGotopage);

	switchWindow();
	if(isDisplayed(txt_PurchaseInvoicepage)) {
		writeTestResults("User enter Purchase Invoice page","view  Purchase Invoice page", "Purchase Invoice page is display", "pass");
	}else {
		writeTestResults("User enter Purchase Invoice page","view  Purchase Invoice page", "Purchase Invoice page is not display", "fail");
	}
	click(btn_btnCheckout);
	WaitClick(btn_btndraft);
	click(btn_btndraft);
	
	Thread.sleep(4000);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	PurchaseInvoiceNo=trackCode;
	WaitElement(pageRelease);
	if (isDisplayed(pageRelease)){

		writeTestResults("verify Purchase Invoice Document Released","Purchase Invoice Document Released", "Purchase Invoice Document is Released", "pass");
		System.out.println("Pro7PO Test case is Completed");
	} else {
		writeTestResults("verify Purchase Invoice Document Released","Purchase Invoice Document Released", "Purchase Invoice Document is not Released", "fail");
	}
	
}


public void Master1()  throws Exception {
	
	navigateToTheLoginPage();
	verifyTheLogo();
	userLogin();
	clickNavigation();
	clickProbutton();
	clickVendorgroupbutton();
	clickNewVendorgroupbutton();

	click(btn_VendorGroupbtn);
	click(btn_VendorGroupAdd);
	LocalTime myObj = LocalTime.now();
	sendKeys(txt_newVendorGroupTxt, "VG"+myObj);
	click(btn_VendorGroupUpdate);
	Thread.sleep(3000);
	selectText(txt_vendorgroup, "VG"+myObj);
	selectText(txt_currency,currency );
	click(btn_btndetails);
	Thread.sleep(3000);
	selectText(txt_PurchaseTaxGroup, PurchaseTaxGroup);
	Thread.sleep(1000);
	selectText(txt_VendorClassification,VendorClassification);
	Thread.sleep(1000);
	selectText(txt_StandardDeliveryMode, StandardDeliveryMode);
	Thread.sleep(1000);
	selectText(txt_StandardDeliveryTerm, StandardDeliveryTerm);
	Thread.sleep(1000);
	selectText(txt_Warehouse, Warehouse);
	Thread.sleep(1000);
	click(btn_PartialDelivery);
	Thread.sleep(2000);
	click(btn_ConsolidatedDelivery);
	Thread.sleep(2000);
	click(btn_btnpaymentinfo);
	Thread.sleep(2000);
	selectText(txt_PaymentTerm, PaymentTerm);
	Thread.sleep(2000);
	sendKeys(txt_Tolerance, Tolerance);
	Thread.sleep(2000);
	sendKeys(txt_CreditDays, CreditDays);
	Thread.sleep(2000);
	sendKeys(txt_CreditLimit, CreditLimit);
	
	Thread.sleep(3000);
	click(btn_DraftAndNew);
	Thread.sleep(3000);
	if(isDisplayed(btn_btndraft)) {
		writeTestResults("verify Draft And New Button","Working Draft And New Button", "Draft And New Button is Working", "pass");
	}else {
		writeTestResults("verify Draft And New Button","Working Draft And New Button", "Draft And New Button is not Working", "fail");
	}
	//==============================================================================================================================
	
	click(btn_VendorGroupbtn);
	click(btn_VendorGroupAdd);
	LocalTime myObj1 = LocalTime.now();
	sendKeys(txt_newVendorGroupTxt, "VG"+myObj1);
	click(btn_VendorGroupUpdate);
	Thread.sleep(3000);
	selectText(txt_vendorgroup, "VG"+myObj1);
	selectText(txt_currency,currency );
	click(btn_btndetails);
	selectText(txt_PurchaseTaxGroup, PurchaseTaxGroup);
	selectText(txt_VendorClassification,VendorClassification);
	selectText(txt_StandardDeliveryMode, StandardDeliveryMode);
	selectText(txt_StandardDeliveryTerm, StandardDeliveryTerm);
	selectText(txt_Warehouse, Warehouse);
	click(btn_PartialDelivery);
	click(btn_ConsolidatedDelivery);
	click(btn_btnpaymentinfo);
	selectText(txt_PaymentTerm, PaymentTerm);
	sendKeys(txt_Tolerance, Tolerance);
	sendKeys(txt_CreditDays, CreditDays);
	sendKeys(txt_CreditLimit, CreditLimit);
	
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(3000);
	if(isDisplayed(btn_release)) {
		writeTestResults("verify Draft Button","Working Draft Button", "Draft Button is Working", "pass");
	}else {
		writeTestResults("verify Draft Button","Working Draft Button", "Draft Button is not Working", "fail");
	}

	number = getText(txt_VendorGroupConfigurationNumber);
	
	Thread.sleep(3000);
	click(btn_btnEdit);
	Thread.sleep(3000);
	if(isDisplayed(btn_btnUpdate)) {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is Working", "pass");
	}else {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_UpdateAndNew);
	Thread.sleep(3000);
	if(isDisplayed(btn_btndraft)) {
		writeTestResults("verify Update And New Button","Working Update And New Button", "Update And New Button is Working", "pass");
	}else {
		writeTestResults("verify Update And New Button","Working Update And New Button", "Update And New Button is not Working", "fail");
	}
	//==================================================================================================================================
	
	click(btn_VendorGroupbtn);
	click(btn_VendorGroupAdd);
	LocalTime myObj2 = LocalTime.now();
	sendKeys(txt_newVendorGroupTxt, "VG"+myObj2);
	click(btn_VendorGroupUpdate);
	Thread.sleep(3000);
	selectText(txt_vendorgroup, "VG"+myObj2);
	selectText(txt_currency,currency );
	click(btn_btndetails);
	selectText(txt_PurchaseTaxGroup, PurchaseTaxGroup);
	selectText(txt_VendorClassification,VendorClassification);
	selectText(txt_StandardDeliveryMode, StandardDeliveryMode);
	selectText(txt_StandardDeliveryTerm, StandardDeliveryTerm);
	selectText(txt_Warehouse, Warehouse);
	click(btn_PartialDelivery);
	click(btn_ConsolidatedDelivery);
	click(btn_btnpaymentinfo);
	selectText(txt_PaymentTerm, PaymentTerm);
	sendKeys(txt_Tolerance, Tolerance);
	sendKeys(txt_CreditDays, CreditDays);
	sendKeys(txt_CreditLimit, CreditLimit);
	
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(3000);
	if(isDisplayed(btn_release)) {
		writeTestResults("verify Draft Button","Working Draft Button", "Draft Button is Working", "pass");
	}else {
		writeTestResults("verify Draft Button","Working Draft Button", "Draft Button is not Working", "fail");
	}
	
	
	Thread.sleep(3000);
	click(btn_btnEdit);
	Thread.sleep(3000);
	if(isDisplayed(btn_btnUpdate)) {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is Working", "pass");
	}else {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnUpdate);
	Thread.sleep(3000);
	if(isDisplayed(btn_btnEdit)) {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is Working", "pass");
	}else {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	number = getText(txt_VendorGroupConfigurationNumber);
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_History);
	Thread.sleep(3000);
	if(isDisplayed(txt_HistoryText)) {
		writeTestResults("verify History Button","Working History Button", "History Button is Working", "pass");
	}else {
		writeTestResults("verify History Button","Working History Button", "History Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_Activities);
	Thread.sleep(3000);
	if(isDisplayed(txt_ActivitiesText)) {
		writeTestResults("verify Activities Button","Working Activities Button", "Activities Button is Working", "pass");
	}else {
		writeTestResults("verify Activities Button","Working Activities Button", "Activities Button is not Working", "fail");
	}
    pageRefersh();
	
	
	
	Thread.sleep(3000);
	click(btn_VendorGroupConfiguration);
	Thread.sleep(3000);
	sendKeys(txt_VendorGroupConfigurationSerch, number);
	Thread.sleep(3000);
	pressEnter(txt_VendorGroupConfigurationSerch);
	Thread.sleep(3000);
	click(sel_selVendorGroupConfiguration.replace("no", number));
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(3000);
	if(getText(txt_ReleseText).contentEquals("(Released)")) {
		writeTestResults("verify Release Button","Working Release Button", "Release Button is Working", "pass");
	}else {
		writeTestResults("verify Release Button","Working Release Button", "Release Button is not Working", "fail");
	}
	
	
	Thread.sleep(3000);
	click(btn_Duplicate);
	Thread.sleep(3000);
	if(getText(Status).contentEquals("New")) 
	{
		writeTestResults("verify Duplicate Button","Working Duplicate Button", "Duplicate Button is Working", "pass");
	}else {
		writeTestResults("verify Duplicate Button","Working Duplicate Button", "Duplicate Button is not Working", "fail");
	}
	
	click(btn_VendorGroupbtn);
	click(btn_VendorGroupAdd);
	LocalTime myObj3 = LocalTime.now();
	sendKeys(txt_newVendorGroupTxt, "VG"+myObj3);
	click(btn_VendorGroupUpdate);
	Thread.sleep(3000);
	selectText(txt_vendorgroup, "VG"+myObj3);
	selectText(txt_currency,currency );
	click(btn_btndetails);
	selectText(txt_PurchaseTaxGroup, PurchaseTaxGroup);
	selectText(txt_VendorClassification,VendorClassification);
	selectText(txt_StandardDeliveryMode, StandardDeliveryMode);
	selectText(txt_StandardDeliveryTerm, StandardDeliveryTerm);
	selectText(txt_Warehouse, Warehouse);
	click(btn_PartialDelivery);
	click(btn_ConsolidatedDelivery);
	click(btn_btnpaymentinfo);
	selectText(txt_PaymentTerm, PaymentTerm);
	sendKeys(txt_Tolerance, Tolerance);
	sendKeys(txt_CreditDays, CreditDays);
	sendKeys(txt_CreditLimit, CreditLimit);
	
	Thread.sleep(3000);
	click(btn_btndraft);
	
	Thread.sleep(3000);
	click(btn_btnAction);
	
	Thread.sleep(3000);
	click(btn_Delete);
	Thread.sleep(3000);
	click(btn_Yes);
	
	Thread.sleep(3000);
	if(getText(txt_DeleteText).contentEquals("(Deleted)")) {
		writeTestResults("verify Delete Button","Working Delete  Button", "Delete  Button is Working", "pass");
	}else {
		writeTestResults("verify Delete Button","Working Delete  Button", "Delete  Button is not Working", "fail");
	}
	
}

public void Master2()  throws Exception {
	
	navigateToTheLoginPage();
	verifyTheLogo();
	userLogin();
	clickNavigation();
	clickProbutton();
	newVendor();
	
	selectText(txt_VendorType, VendorType);
	selectText(txt_VendorNameTitle, VendorNameTitle);
	LocalTime myObj = LocalTime.now();
	sendKeys(txt_VendorName, VendorName+myObj);
	selectText(txt_currency,currency);
	selectText(txt_vendorgroup, vendorgroupforven);
	click(btn_ReceivableAccount);
	Thread.sleep(3000);
	click(btn_newReceivableAccount);
	Thread.sleep(3000);
	sendKeys(txt_newReceivableAccountname, newReceivableAccountname+myObj);
	Thread.sleep(3000);
	number = newReceivableAccountname+myObj;
	
	selectIndex(txt_AccountGroup, 2);
	Thread.sleep(3000);
	click(btn_btnUpdate);
	
	Thread.sleep(5000);
	sendKeys(txt_ReceivableAccount, number);
	Thread.sleep(3000);
	pressEnter(txt_ReceivableAccount);
	Thread.sleep(5000);
	doubleClick(sel_ReceivableAccount);
	
	selectText(txt_ContactTitle, ContactTitle);
	sendKeys(txt_ContactName, ContactName);
	sendKeys(txt_MobileNumber,MobileNumber);
	click(btn_btndetails);
	selectText(txt_SelectIndustry,SelectIndustry);
	sendKeys(txt_TaxRegistrationNumber, TaxRegistrationNumber);
	sendKeys(txt_ReferenceAccoutNo, ReferenceAccoutNo);
	click(btn_3rdPartyAccount);
	sendKeys(txt_3rdPartyAccount, txt3rdPartyAccount);
	Thread.sleep(3000);
	pressEnter(txt_3rdPartyAccount);
	Thread.sleep(3000);
	doubleClick(sel_3rdPartyAccount);
	selectText(txt_PurchaseTaxGroup, PurchaseTaxGroup);
	sendKeys(txt_PassportNo, PassportNo);
	sendKeys(txt_CreditDays, CreditDays);
	sendKeys(txt_CreditLimit, CreditLimit);

	
	Thread.sleep(3000);
	click(btn_DraftAndNew);
	Thread.sleep(3000);
	if(isDisplayed(btn_btndraft)) {
		writeTestResults("verify Draft And New Button","Working Draft And New Button", "Draft And New Button is Working", "pass");
	}else {
		writeTestResults("verify Draft And New Button","Working Draft And New Button", "Draft And New Button is not Working", "fail");
	}
	//==============================================================================================================================
	
	selectText(txt_VendorType, VendorType);
	selectText(txt_VendorNameTitle, VendorNameTitle);
	LocalTime myObj1 = LocalTime.now();
	sendKeys(txt_VendorName, VendorName+myObj1);
	selectText(txt_currency,currency);
	selectText(txt_vendorgroup, vendorgroupforven);
	click(btn_ReceivableAccount);
	Thread.sleep(3000);
	click(btn_newReceivableAccount);
	Thread.sleep(3000);
	sendKeys(txt_newReceivableAccountname, newReceivableAccountname+myObj1);
	Thread.sleep(3000);
	number = newReceivableAccountname+myObj1;
	
	selectIndex(txt_AccountGroup, 2);
	Thread.sleep(3000);
	click(btn_btnUpdate);
	
	Thread.sleep(5000);
	sendKeys(txt_ReceivableAccount, number);
	Thread.sleep(3000);
	pressEnter(txt_ReceivableAccount);
	Thread.sleep(5000);
	doubleClick(sel_ReceivableAccount);
	
	selectText(txt_ContactTitle, ContactTitle);
	sendKeys(txt_ContactName, ContactName);
	sendKeys(txt_MobileNumber,MobileNumber);
	click(btn_btndetails);
	selectText(txt_SelectIndustry,SelectIndustry);
	sendKeys(txt_TaxRegistrationNumber, TaxRegistrationNumber);
	sendKeys(txt_ReferenceAccoutNo, ReferenceAccoutNo);
	click(btn_3rdPartyAccount);
	sendKeys(txt_3rdPartyAccount, txt3rdPartyAccount);
	Thread.sleep(3000);
	pressEnter(txt_3rdPartyAccount);
	Thread.sleep(3000);
	doubleClick(sel_3rdPartyAccount);
	selectText(txt_PurchaseTaxGroup, PurchaseTaxGroup);
	sendKeys(txt_PassportNo, PassportNo);
	sendKeys(txt_CreditDays, CreditDays);
	sendKeys(txt_CreditLimit, CreditLimit);
	
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(3000);
	if(isDisplayed(btn_release)) {
		writeTestResults("verify Draft Button","Working Draft Button", "Draft Button is Working", "pass");
	}else {
		writeTestResults("verify Draft Button","Working Draft Button", "Draft Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_History);
	Thread.sleep(3000);
	if(isDisplayed(txt_HistoryText)) {
		writeTestResults("verify History Button","Working History Button", "History Button is Working", "pass");
	}else {
		writeTestResults("verify History Button","Working History Button", "History Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_Reminders);
	Thread.sleep(3000);
	if(isDisplayed(txt_RemindersText)) {
		writeTestResults("verify Reminders Button","Working Reminders Button", "Reminders Button is Working", "pass");
	}else {
		writeTestResults("verify Reminders Button","Working Reminders Button", "Reminders Button is not Working", "fail");
	}
	Thread.sleep(3000);
	click(btn_headerclose);
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_Activities);
	Thread.sleep(3000);
	if(isDisplayed(txt_ActivitiesText)) {
		writeTestResults("verify Activities Button","Working Activities Button", "Activities Button is Working", "pass");
	}else {
		writeTestResults("verify Activities Button","Working Activities Button", "Activities Button is not Working", "fail");
	}
	pageRefersh();
	
	Thread.sleep(3000);
	click(btn_btnEdit);
	Thread.sleep(3000);
	if(isDisplayed(btn_btnUpdate)) {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is Working", "pass");
	}else {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_UpdateAndNew);
	Thread.sleep(3000);
	if(isDisplayed(btn_btndraft)) {
		writeTestResults("verify Update And New Button","Working Update And New Button", "Update And New Button is Working", "pass");
	}else {
		writeTestResults("verify Update And New Button","Working Update And New Button", "Update And New Button is not Working", "fail");
	}
	//===========================================================================================================
	
	selectText(txt_VendorType, VendorType);
	selectText(txt_VendorNameTitle, VendorNameTitle);
	LocalTime myObj2 = LocalTime.now();
	sendKeys(txt_VendorName, VendorName+myObj2);
	selectText(txt_currency,currency);
	selectText(txt_vendorgroup, vendorgroupforven);
	Thread.sleep(3000);
	click(btn_ReceivableAccount);
	Thread.sleep(3000);
	click(btn_newReceivableAccount);
	Thread.sleep(3000);
	sendKeys(txt_newReceivableAccountname, newReceivableAccountname+myObj2);
	Thread.sleep(3000);
	number = newReceivableAccountname+myObj2;
	
	selectIndex(txt_AccountGroup, 2);
	Thread.sleep(3000);
	click(btn_btnUpdate);
	
	Thread.sleep(5000);
	sendKeys(txt_ReceivableAccount, number);
	Thread.sleep(3000);
	pressEnter(txt_ReceivableAccount);
	Thread.sleep(5000);
	doubleClick(sel_ReceivableAccount);
	
	selectText(txt_ContactTitle, ContactTitle);
	sendKeys(txt_ContactName, ContactName);
	sendKeys(txt_MobileNumber,MobileNumber);
	click(btn_btndetails);
	selectText(txt_SelectIndustry,SelectIndustry);
	sendKeys(txt_TaxRegistrationNumber, TaxRegistrationNumber);
	sendKeys(txt_ReferenceAccoutNo, ReferenceAccoutNo);
	click(btn_3rdPartyAccount);
	sendKeys(txt_3rdPartyAccount, txt3rdPartyAccount);
	Thread.sleep(3000);
	pressEnter(txt_3rdPartyAccount);
	Thread.sleep(3000);
	doubleClick(sel_3rdPartyAccount);
	selectText(txt_PurchaseTaxGroup, PurchaseTaxGroup);
	sendKeys(txt_PassportNo, PassportNo);
	sendKeys(txt_CreditDays, CreditDays);
	sendKeys(txt_CreditLimit, CreditLimit);
	Thread.sleep(3000);
	click(btn_btndraft);
	
	Thread.sleep(3000);
	click(btn_btnEdit);
	
	Thread.sleep(3000);
	click(btn_btnUpdate);
	Thread.sleep(3000);
	if(isDisplayed(btn_btnEdit)) {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is Working", "pass");
	}else {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is not Working", "fail");
	}
	
	number = getText(txt_newVendorName);
	Thread.sleep(3000);
	click(btn_VendorInformation);
	Thread.sleep(3000);
	sendKeys(txt_VendorInformationserch, number);
	Thread.sleep(3000);
	pressEnter(txt_VendorInformationserch);
	Thread.sleep(3000);
	click(sel_selVendorInformation.replace("no", number));
	
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(3000);
	if(getText(txt_ReleseText).contentEquals("(Released)")) {
		writeTestResults("verify Release Button","Working Release Button", "Release Button is Working", "pass");
	}else {
		writeTestResults("verify Release Button","Working Release Button", "Release Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_Duplicate);
	Thread.sleep(5000);
	if(getText(Status).contentEquals("New")) 
	{
		writeTestResults("verify Duplicate Button","Working Duplicate Button", "Duplicate Button is Working", "pass");
	}else {
		writeTestResults("verify Duplicate Button","Working Duplicate Button", "Duplicate Button is not Working", "fail");
	}
	
	selectText(txt_VendorType, VendorType);
	selectText(txt_VendorNameTitle, VendorNameTitle);
	LocalTime myObj3 = LocalTime.now();
	sendKeys(txt_VendorName, VendorName+myObj3);
	selectText(txt_currency,currency);
	selectText(txt_vendorgroup, vendorgroupforven);
	Thread.sleep(3000);
	click(btn_ReceivableAccount);
	Thread.sleep(3000);
	click(btn_newReceivableAccount);
	Thread.sleep(3000);
	sendKeys(txt_newReceivableAccountname, newReceivableAccountname+myObj3);
	Thread.sleep(3000);
	number = newReceivableAccountname+myObj3;
	
	selectIndex(txt_AccountGroup, 2);
	Thread.sleep(3000);
	click(btn_btnUpdate);
	
	Thread.sleep(5000);
	sendKeys(txt_ReceivableAccount, number);
	Thread.sleep(3000);
	pressEnter(txt_ReceivableAccount);
	Thread.sleep(5000);
	doubleClick(sel_ReceivableAccount);
	
	selectText(txt_ContactTitle, ContactTitle);
	sendKeys(txt_ContactName, ContactName);
	sendKeys(txt_MobileNumber,MobileNumber);
	click(btn_btndetails);
	selectText(txt_SelectIndustry,SelectIndustry);
	sendKeys(txt_TaxRegistrationNumber, TaxRegistrationNumber);
	sendKeys(txt_ReferenceAccoutNo, ReferenceAccoutNo);
	click(btn_3rdPartyAccount);
	sendKeys(txt_3rdPartyAccount, txt3rdPartyAccount);
	Thread.sleep(3000);
	pressEnter(txt_3rdPartyAccount);
	Thread.sleep(3000);
	doubleClick(sel_3rdPartyAccount);
	selectText(txt_PurchaseTaxGroup, PurchaseTaxGroup);
	sendKeys(txt_PassportNo, PassportNo);
	sendKeys(txt_CreditDays, CreditDays);
	sendKeys(txt_CreditLimit, CreditLimit);
	Thread.sleep(3000);
	click(btn_btndraft);
	
	Thread.sleep(3000);
	click(btn_btnAction);
	
	Thread.sleep(3000);
	click(btn_Delete);
	
	Thread.sleep(3000);
	click(btn_Yes);
	
	Thread.sleep(3000);
	if(isDisplayed("//label[@id='lbldocstatus'][text()='(Deleted)']")) {
		writeTestResults("verify Delete Button","Working Delete  Button", "Delete  Button is Working", "pass");
	}else {
		writeTestResults("verify Delete Button","Working Delete  Button", "Delete  Button is not Working", "fail");
	}

}

public void Master3() throws Exception{
	
	navigateToTheLoginPage();
	verifyTheLogo();
	userLogin();
	clickNavigation();
	clickProbutton();
	click(VendorAgreementbutton);
	
	if(isDisplayed(VendorAgreementpage)) {
		writeTestResults("verify Vendor Agreement page","view Vendor Agreement page", "Vendor Agreement page is display", "pass");
	}else {
		writeTestResults("verify Vendor Agreement page","view Vendor Agreement page", "Vendor Agreement page is not display", "fail");
	}
	click(newVendorAgreementbutton);
	if(isDisplayed(newVendorAgreementpage)) {
		writeTestResults("verify New Vendor Agreement page","view New Vendor Agreement page", "New Vendor Agreement page is display", "pass");
	}else {
		writeTestResults("verify New Vendor Agreement page","view New Vendor Agreement page", "New Vendor Agreement page is not display", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_CopyFrom);
	Thread.sleep(3000);
	if(isDisplayed(txt_CopyFromText)) {
		writeTestResults("verify Copy From Button","Working Copy From Button", "Copy From Button is Working", "pass");
	}else {
		writeTestResults("verify Copy From Button","Working Copy From Button", "Copy From Button is not Working", "fail");
	}
	Thread.sleep(3000);
	click(btn_headerclose);
	
	LocalTime myObj = LocalTime.now();
	sendKeys(txt_AgreementNo, AgreementNo+myObj);
	sendKeys(txt_AgreementTitle, AgreementTitle);
	click(btn_btnAgreementDate);
	click(btn_selAgreementDate);
	selectText(txt_AgreementGroup, AgreementGroup);
	selectText(txt_ACurrency, Acurrency);
	selectText(txt_ValidityPeriodFor, ValidityPeriodFor);
	sendKeys(txt_Description, Description);
	click(btn_btnVendorAccount);
	sendKeys(txt_txtVendorAccount, txtVendorAccount);
	Thread.sleep(3000);
	pressEnter(txt_txtVendorAccount);
	Thread.sleep(3000);
	doubleClick(sel_selVendorAccount);
	click(btn_btnproductdetails);
	sendKeys(txt_ProductRelation, ProductRelation);
	click(btn_btnproduct);
	sendKeys(txt_txtproduct, txtproduct);
	Thread.sleep(3000);
	pressEnter(txt_txtproduct);
	Thread.sleep(3000);
	doubleClick(sel_selproduct);
	sendKeys(txt_minOrderQty, minOrderQty);
	sendKeys(txt_maxOrderQty, maxOrderQty);
	Thread.sleep(3000);
	click(btn_DraftAndNew);
	Thread.sleep(3000);
	if(isDisplayed(btn_btndraft)) {
		writeTestResults("verify Draft And New Button","Working Draft And New Button", "Draft And New Button is Working", "pass");
	}else {
		writeTestResults("verify Draft And New Button","Working Draft And New Button", "Draft And New Button is not Working", "fail");
	}
	//==========================================================================================================
	LocalTime myObj1 = LocalTime.now();
	sendKeys(txt_AgreementNo, AgreementNo+myObj1);
	sendKeys(txt_AgreementTitle, AgreementTitle);
	click(btn_btnAgreementDate);
	click(btn_selAgreementDate);
	selectText(txt_AgreementGroup, AgreementGroup);
	selectText(txt_ACurrency, Acurrency);
	selectText(txt_ValidityPeriodFor, ValidityPeriodFor);
	sendKeys(txt_Description, Description);
	click(btn_btnVendorAccount);
	sendKeys(txt_txtVendorAccount, txtVendorAccount);
	Thread.sleep(3000);
	pressEnter(txt_txtVendorAccount);
	Thread.sleep(3000);
	doubleClick(sel_selVendorAccount);
	click(btn_btnproductdetails);
	sendKeys(txt_ProductRelation, ProductRelation);
	click(btn_btnproduct);
	sendKeys(txt_txtproduct, txtproduct);
	Thread.sleep(3000);
	pressEnter(txt_txtproduct);
	Thread.sleep(3000);
	doubleClick(sel_selproduct);
	sendKeys(txt_minOrderQty, minOrderQty);
	sendKeys(txt_maxOrderQty, maxOrderQty);
	Thread.sleep(3000);
	click(btn_btndraft);
	
	Thread.sleep(3000);
	click(btn_btnEdit);
	Thread.sleep(3000);
	if(isDisplayed(btn_btnUpdate)) {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is Working", "pass");
	}else {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is not Working", "fail");
	}

	Thread.sleep(3000);
	click(btn_UpdateAndNew);
	Thread.sleep(3000);
	if(isDisplayed(btn_btndraft)) {
		writeTestResults("verify Update And New Button","Working Update And New Button", "Update And New Button is Working", "pass");
	}else {
		writeTestResults("verify Update And New Button","Working Update And New Button", "Update And New Button is not Working", "fail");
	}
	
	
	//=========================================================================================================

	
	LocalTime myObj2 = LocalTime.now();
	sendKeys(txt_AgreementNo, AgreementNo+myObj2);
	Thread.sleep(3000);
	sendKeys(txt_AgreementTitle, AgreementTitle);
	Thread.sleep(3000);
	click(btn_btnAgreementDate);
	Thread.sleep(3000);
	click(btn_selAgreementDate);
	Thread.sleep(3000);
	selectText(txt_AgreementGroup, AgreementGroup);
	Thread.sleep(3000);
	selectText(txt_ACurrency, Acurrency);
	Thread.sleep(3000);
	selectText(txt_ValidityPeriodFor, ValidityPeriodFor);
	Thread.sleep(3000);
	sendKeys(txt_Description, Description);
	Thread.sleep(3000);
	click(btn_btnVendorAccount);
	Thread.sleep(3000);
	sendKeys(txt_txtVendorAccount, txtVendorAccount);
	Thread.sleep(3000);
	pressEnter(txt_txtVendorAccount);
	Thread.sleep(3000);
	doubleClick(sel_selVendorAccount);
	Thread.sleep(3000);
	click(btn_btnproductdetails);
	Thread.sleep(3000);
	sendKeys(txt_ProductRelation, ProductRelation);
	Thread.sleep(3000);
	click(btn_btnproduct);
	Thread.sleep(3000);
	sendKeys(txt_txtproduct, txtproduct);
	Thread.sleep(3000);
	pressEnter(txt_txtproduct);
	Thread.sleep(3000);
	doubleClick(sel_selproduct);
	sendKeys(txt_minOrderQty, minOrderQty);
	sendKeys(txt_maxOrderQty, maxOrderQty);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(3000);
	if(isDisplayed(btn_release)) {
		writeTestResults("verify Draft Button","Working Draft Button", "Draft Button is Working", "pass");
	}else {
		writeTestResults("verify Draft Button","Working Draft Button", "Draft Button is not Working", "fail");
	}
	

	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_History);
	Thread.sleep(3000);
	if(isDisplayed(txt_HistoryText)) {
		writeTestResults("verify History Button","Working History Button", "History Button is Working", "pass");
	}else {
		writeTestResults("verify History Button","Working History Button", "History Button is not Working", "fail");
	}

	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_Activities);
	Thread.sleep(3000);
	if(isDisplayed(txt_ActivitiesText)) {
		writeTestResults("verify Activities Button","Working Activities Button", "Activities Button is Working", "pass");
	}else {
		writeTestResults("verify Activities Button","Working Activities Button", "Activities Button is not Working", "fail");
	}
	pageRefersh();
	
	Thread.sleep(3000);
	click(btn_btnEdit);
	Thread.sleep(3000);
	if(isDisplayed(btn_btnUpdate)) {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is Working", "pass");
	}else {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is not Working", "fail");
	}

	Thread.sleep(3000);
	click(btn_btnUpdate);
	Thread.sleep(3000);
	if(isDisplayed(btn_btnEdit)) {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is Working", "pass");
	}else {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(3000);
	if(getText(txt_ReleseText).contentEquals("(Released)")) {
		writeTestResults("verify Release Button","Working Release Button", "Release Button is Working", "pass");
	}else {
		writeTestResults("verify Release Button","Working Release Button", "Release Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_ConverttoPurchaseOrder);
	Thread.sleep(3000);
	click(btn_Yes);
	Thread.sleep(3000);
	if(isDisplayed(btn_btnPOtoShipmentCosting)) {
		writeTestResults("verify Convert to Purchase Order Button","Working Convert to Purchase Order Button", "Convert to Purchase Order Button is Working", "pass");
	}else {
		writeTestResults("verify Convert to Purchase Order Button","Working Convert to Purchase Order Button", "Convert to Purchase Order Button is not Working", "fail");
	}
	Thread.sleep(3000);
	click(btn_ActivitiesHeaderclose);
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_Reverse);
	Thread.sleep(3000);
	click(btn_Yes);
	Thread.sleep(3000);
	if(getText(txt_ReverseText).contentEquals("(Reversed)")) {
		writeTestResults("verify Reversed Button","Working Reversed Button", "Reversed Button is Working", "pass");
	}else {
		writeTestResults("verify Reversed Button","Working Reversed Button", "Reversed Button is not Working", "fail");
	}
	String a= getText(Header);
	String b= AgreementNo+myObj2;
	
	Thread.sleep(3000);
	click(btn_Duplicate);
	Thread.sleep(5000);
	if(getText(Status).contentEquals("New")) 
	{
		writeTestResults("verify Duplicate Button","Working Duplicate Button", "Duplicate Button is Working", "pass");
	}else {
		writeTestResults("verify Duplicate Button","Working Duplicate Button", "Duplicate Button is not Working", "fail");
	}
	
}

public void Transaction1() throws Exception{
	
	navigateToTheLoginPage();
	verifyTheLogo();
	userLogin();
	clickNavigation();
	clickProbutton();
	click(btn_ImportCostEstimation);
	Thread.sleep(3000);
	if(isDisplayed(ImportCostEstimationpage)) {
		writeTestResults("verify Import Cost Estimation page","view Import Cost Estimation page", "Import Cost Estimation page is display", "pass");
	}else {
		writeTestResults("verify Import Cost Estimation page","view Import Cost Estimation page", "Import Cost Estimation page is not display", "fail");
	}
	
	click(btn_newImportCostEstimation);
	Thread.sleep(3000);
	if(isDisplayed(newImportCostEstimationpage)) {
		writeTestResults("verify New Import Cost Estimation page","view New Import Cost Estimation page", "New Import Cost Estimation page is display", "pass");
	}else {
		writeTestResults("verify New Import Cost Estimation page","view New Import Cost Estimation page", "New Import Cost Estimation page is not display", "fail");
	}
	
sendKeys(txt_ICEDescription, ICEDescription);
	
	Thread.sleep(3000);
	click(btn_ICEProductbutoon);
	Thread.sleep(3000);
	sendKeys(txt_ICEProducttxt, ICEProducttxt);
	Thread.sleep(3000);
	pressEnter(txt_ICEProducttxt);
	Thread.sleep(3000);
	doubleClick(sel_ICEProductsel);
	
	click(btn_ICEEstimationDetails);
	Thread.sleep(3000);
	selectIndex(txt_ICECostingType, 1);
	sendKeys(txt_ICEEstimateCost, ICEEstimateCost);
	
	click(btn_ICESummary);
	
	Thread.sleep(3000);
	click(btn_ICECheckout);
	Thread.sleep(3000);
	click(btn_ICEDraft);
	Thread.sleep(3000);
	if(isDisplayed(btn_release)) {
		writeTestResults("verify Draft Button","Working Draft Button", "Draft Button is Working", "pass");
	}else {
		writeTestResults("verify Draft Button","Working Draft Button", "Draft Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnEdit);
	
	Thread.sleep(3000);
	click(btn_ICECheckout);
	
	Thread.sleep(3000);
	click(btn_UpdateAndNew);
	Thread.sleep(3000);
	if(isDisplayed(btn_btndraft)) {
		writeTestResults("verify Update And New Button","Working Update And New Button", "Update And New Button is Working", "pass");
	}else {
		writeTestResults("verify Update And New Button","Working Update And New Button", "Update And New Button is not Working", "fail");
	}
	
	
	
	
	//=================================================================================================================
	
sendKeys(txt_ICEDescription, ICEDescription);
	
	Thread.sleep(3000);
	click(btn_ICEProductbutoon);
	Thread.sleep(3000);
	sendKeys(txt_ICEProducttxt, ICEProducttxt);
	Thread.sleep(3000);
	pressEnter(txt_ICEProducttxt);
	Thread.sleep(3000);
	doubleClick(sel_ICEProductsel);
	
	click(btn_ICEEstimationDetails);
	Thread.sleep(3000);
	selectIndex(txt_ICECostingType, 1);
	sendKeys(txt_ICEEstimateCost, ICEEstimateCost);
	
	click(btn_ICESummary);
	
	Thread.sleep(3000);
	click(btn_ICECheckout);
	Thread.sleep(3000);
	click(btn_DraftAndNew);
	Thread.sleep(3000);
	if(isDisplayed(btn_btndraft)) {
		writeTestResults("verify Draft And New Button","Working Draft And New Button", "Draft And New Button is Working", "pass");
	}else {
		writeTestResults("verify Draft And New Button","Working Draft And New Button", "Draft And New Button is not Working", "fail");
	}
	//===========================================================================================================================
	sendKeys(txt_ICEDescription, ICEDescription);
	
	Thread.sleep(3000);
	click(btn_ICEProductbutoon);
	Thread.sleep(3000);
	sendKeys(txt_ICEProducttxt, ICEProducttxt);
	Thread.sleep(3000);
	pressEnter(txt_ICEProducttxt);
	Thread.sleep(3000);
	doubleClick(sel_ICEProductsel);
	
	click(btn_ICEEstimationDetails);
	Thread.sleep(3000);
	selectIndex(txt_ICECostingType, 1);
	sendKeys(txt_ICEEstimateCost, ICEEstimateCost);
	
	click(btn_ICESummary);
	
	Thread.sleep(3000);
	click(btn_ICECheckout);
	Thread.sleep(3000);
	click(btn_ICEDraft);
	
	Thread.sleep(3000);
	click(btn_btnEdit);
	Thread.sleep(3000);
	if(isDisplayed(btn_btnUpdate)) {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is Working", "pass");
	}else {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_ICECheckout);

	Thread.sleep(3000);
	click(btn_btnUpdate);
	Thread.sleep(3000);
	if(isDisplayed(btn_btnEdit)) {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is Working", "pass");
	}else {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_History);
	Thread.sleep(3000);
	if(isDisplayed(txt_HistoryText)) {
		writeTestResults("verify History Button","Working History Button", "History Button is Working", "pass");
	}else {
		writeTestResults("verify History Button","Working History Button", "History Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_ICERelease);
	Thread.sleep(3000);
	trackCode= getText(Header);
	Thread.sleep(5000);
	if (isDisplayed(pageRelease)){

		writeTestResults("verify Import Cost Estimation Document Released","Import Cost Estimation Document Released", "Import Cost Estimation Document is Released", "pass");
	} else {
		writeTestResults("verify Import Cost Estimation Document Released","Import Cost Estimation Document Released", "Import Cost Estimation Document is not Released", "fail");

	}
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_Reverse);
	Thread.sleep(3000);
	click(btn_Yes);
	Thread.sleep(3000);
	if(getText(txt_ReverseText).contentEquals("(Reversed)")) {
		writeTestResults("verify Reversed Button","Working Reversed Button", "Reversed Button is Working", "pass");
	}else {
		writeTestResults("verify Reversed Button","Working Reversed Button", "Reversed Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_Duplicate);
	Thread.sleep(5000);
	if(getText(Status).contentEquals("New")) 
	{
		writeTestResults("verify Duplicate Button","Working Duplicate Button", "Duplicate Button is Working", "pass");
	}else {
		writeTestResults("verify Duplicate Button","Working Duplicate Button", "Duplicate Button is not Working", "fail");
	}
	
	//========================================================================================================================
	
sendKeys(txt_ICEDescription, ICEDescription);
	
	Thread.sleep(3000);
	click(btn_ICEProductbutoon);
	Thread.sleep(3000);
	sendKeys(txt_ICEProducttxt, ICEProducttxt);
	Thread.sleep(3000);
	pressEnter(txt_ICEProducttxt);
	Thread.sleep(3000);
	doubleClick(sel_ICEProductsel);
	
	click(btn_ICEEstimationDetails);
	Thread.sleep(3000);
	selectIndex(txt_ICECostingType, 1);
	sendKeys(txt_ICEEstimateCost, ICEEstimateCost);
	
	click(btn_ICESummary);
	
	Thread.sleep(3000);
	click(btn_ICECheckout);
	Thread.sleep(3000);
	click(btn_ICEDraft);
	
	Thread.sleep(3000);
	click(btn_btnAction);
	
	Thread.sleep(3000);
	click(btn_Delete);
	
	Thread.sleep(3000);
	click(btn_Yes);
	
	Thread.sleep(3000);
	if(getText(txt_DeleteText).contentEquals("(Deleted)")) {
		writeTestResults("verify Delete Button","Working Delete  Button", "Delete  Button is Working", "pass");
	}else {
		writeTestResults("verify Delete Button","Working Delete  Button", "Delete  Button is not Working", "fail");
	}
	
}


public void Transaction2() throws Exception{
	
	navigateToTheLoginPage();
	verifyTheLogo();
	userLogin();
	clickNavigation();
	clickProbutton();
	
	
	click(btn_RequestForQuotation);
	if(isDisplayed(txt_RequestForQuotationpage)) {
		writeTestResults("verify Request For Quotation page","view Request For Quotation page", "Request For Quotation page is display", "pass");
	}else {
		writeTestResults("verify Request For Quotation page","view Request For Quotation page", "Request For Quotation page is not display", "fail");
	}
	click(btn_NewRequestForQuotation);
	if(isDisplayed(txt_NewRequestForQuotationpage)&&(getText(Header).contentEquals("New"))) {
		writeTestResults("verify New Request For Quotation page","view New Request For Quotation page", "New Request For Quotation page is display", "pass");
	}else {
		writeTestResults("verify New Request For Quotation page","view New Request For Quotation page", "New Request For Quotation page is not display", "fail");
	}
	
//	Thread.sleep(3000);
//	click(btn_RFQdeadline);
	Thread.sleep(3000);
//	click(sel_selRFQdeadline);

	JavascriptExecutor j7 = (JavascriptExecutor)driver;
	j7.executeScript("$(\"#txtdeadLine\").datepicker(\"setDate\", new Date())");


	Thread.sleep(3000);
	click(btn_btncontactperson);
	sendKeys(txt_txtcontactperson, txtcontactperson);
	pressEnter(txt_txtcontactperson);
	doubleClick(sel_selcontactperson);
	
	click(btn_btnRFQVendor);
	driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
	sendKeys(txt_txtRFQVendor, txtRFQVendor);
	Thread.sleep(3000);
	pressEnter(txt_txtRFQVendor);
	Thread.sleep(3000);
	doubleClick(sel_selRFQVendor);
	
	Thread.sleep(3000);
	click(RFQPro1);
	Thread.sleep(3000);
	sendKeys(txt_ptxtproduct,pro7.readTestCreation("BatchSpecific"));
	Thread.sleep(3000);
	pressEnter(txt_ptxtproduct);
	Thread.sleep(3000);
	doubleClick(sel_pselproduct);
	
	Thread.sleep(3000);
	click(btn_DraftAndNew);
	Thread.sleep(3000);
	if(isDisplayed(btn_btndraft)) {
		writeTestResults("verify Draft And New Button","Working Draft And New Button", "Draft And New Button is Working", "pass");
	}else {
		writeTestResults("verify Draft And New Button","Working Draft And New Button", "Draft And New Button is not Working", "fail");
	}

	Thread.sleep(3000);
	click(btn_CopyFrom);
	Thread.sleep(3000);
	if(isDisplayed(txt_CopyFromText)) {
		writeTestResults("verify Copy From Button","Working Copy From Button", "Copy From Button is Working", "pass");
	}else {
		writeTestResults("verify Copy From Button","Working Copy From Button", "Copy From Button is not Working", "fail");
	}
	Thread.sleep(3000);
	click(btn_headerclose);
	
	//==========================================================================================================
	
	
//	Thread.sleep(3000);
//	click(btn_RFQdeadline);
	Thread.sleep(3000);
//	click(sel_selRFQdeadline);


	j7.executeScript("$(\"#txtdeadLine\").datepicker(\"setDate\", new Date())");


	Thread.sleep(3000);
	click(btn_btncontactperson);
	sendKeys(txt_txtcontactperson, txtcontactperson);
	pressEnter(txt_txtcontactperson);
	doubleClick(sel_selcontactperson);
	
	click(btn_btnRFQVendor);
	driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
	sendKeys(txt_txtRFQVendor, txtRFQVendor);
	Thread.sleep(3000);
	pressEnter(txt_txtRFQVendor);
	Thread.sleep(3000);
	doubleClick(sel_selRFQVendor);
	
	Thread.sleep(3000);
	click(RFQPro1);
	Thread.sleep(3000);
	sendKeys(txt_ptxtproduct,pro7.readTestCreation("BatchSpecific"));
	Thread.sleep(3000);
	pressEnter(txt_ptxtproduct);
	Thread.sleep(3000);
	doubleClick(sel_pselproduct);
	
//	sendKeys(txt_RFQqty,RFQqty);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Request For Quotation Document Draft","Request For Quotation Document Draft", "Request For Quotation Document is Draft", "pass");
	} else {
		writeTestResults("verify Request For Quotation Document Draft","Request For Quotation Document Draft", "Request For Quotation Document is not Draft", "fail");

	}
	
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(3000);
	click(btn_Edit);
	Thread.sleep(3000);
	if(isDisplayed(btn_btnUpdate)) {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is Working", "pass");
	}else {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_UpdateAndNew);
	Thread.sleep(3000);
	if(isDisplayed(btn_btndraft)) {
		writeTestResults("verify Update And New Button","Working Update And New Button", "Update And New Button is Working", "pass");
	}else {
		writeTestResults("verify Update And New Button","Working Update And New Button", "Update And New Button is not Working", "fail");
	}
	
	//==================================================================================================

	
//	Thread.sleep(3000);
//	click(btn_RFQdeadline);
	Thread.sleep(3000);
//	click(sel_selRFQdeadline);


	j7.executeScript("$(\"#txtdeadLine\").datepicker(\"setDate\", new Date())");


	Thread.sleep(3000);
	click(btn_btncontactperson);
	sendKeys(txt_txtcontactperson, txtcontactperson);
	pressEnter(txt_txtcontactperson);
	doubleClick(sel_selcontactperson);
	
	click(btn_btnRFQVendor);
	driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
	sendKeys(txt_txtRFQVendor, txtRFQVendor);
	Thread.sleep(3000);
	pressEnter(txt_txtRFQVendor);
	Thread.sleep(3000);
	doubleClick(sel_selRFQVendor);
	
	Thread.sleep(3000);
	click(RFQPro1);
	Thread.sleep(3000);
	sendKeys(txt_ptxtproduct,pro7.readTestCreation("BatchSpecific"));
	Thread.sleep(3000);
	pressEnter(txt_ptxtproduct);
	Thread.sleep(3000);
	doubleClick(sel_pselproduct);
	
//	sendKeys(txt_RFQqty,RFQqty);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Request For Quotation Document Draft","Request For Quotation Document Draft", "Request For Quotation Document is Draft", "pass");
	} else {
		writeTestResults("verify Request For Quotation Document Draft","Request For Quotation Document Draft", "Request For Quotation Document is not Draft", "fail");

	}
	
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(3000);
	click(btn_Edit);
	Thread.sleep(3000);
	if(isDisplayed(btn_btnUpdate)) {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is Working", "pass");
	}else {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is not Working", "fail");
	}
	
	click(btn_QuotationTab);
	Thread.sleep(3000);
	click(btn_RFQadd);
	Thread.sleep(3000);
	selectIndex(txt_RFQaddVendor, 1);
	LocalTime myObj = LocalTime.now();
	sendKeys(txt_RFQQuotationNo, RFQQuotationNo+myObj);
	selectIndex(txt_RFQShippingTerm, 2);
	sendKeys(txt_RFQUnitPrice, RFQUnitPrice);
	sendKeys(txt_RFQMinQty, RFQMinQty);
	sendKeys(txt_RFQMaxQty, RFQMaxQty);
	
	Thread.sleep(3000);
	click(btn_RFQcheckout);
	Thread.sleep(3000);
	click(btn_RFQapply);
	Thread.sleep(3000);
	JavascriptExecutor jse = (JavascriptExecutor)driver;
	jse.executeScript("$(closeMessageBox()).click()");
	Thread.sleep(3000);
	click(btn_RFQsummarytab);
	Thread.sleep(5000);
	click(btn_qualifiedvendorbutton);
	
	click(btn_Qualifiedtik);
	
	Thread.sleep(3000);
	JavascriptExecutor j1 = (JavascriptExecutor)driver;
	j1.executeScript("$(\"#priceTab\").parent().find('.dialogbox-buttonarea > .button').click()");
	
	Thread.sleep(2000);
	click(btn_btnRFQapply);
	Thread.sleep(3000);
	click(btn_Update);
	Thread.sleep(3000);
	if(isDisplayed(btn_btnEdit)) {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is Working", "pass");
	}else {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_History);
	Thread.sleep(3000);
	if(isDisplayed(txt_HistoryText)) {
		writeTestResults("verify History Button","Working History Button", "History Button is Working", "pass");
	}else {
		writeTestResults("verify History Button","Working History Button", "History Button is not Working", "fail");
	}
	
	
	Thread.sleep(3000);
	click(btn_release);
	
	Thread.sleep(2000);
	trackCode= getText(Header);
	Thread.sleep(5000);
	if (isDisplayed(pageRelease)){

		writeTestResults("verify Request For Quotation Document Released","Request For Quotation Document Released", "Request For Quotation Document is Released", "pass");
	} else {
		writeTestResults("verify Request For Quotation Document Released","Request For Quotation Document Released", "Request For Quotation Document is not Released", "fail");
	}
}

public void Transaction3() throws Exception{
	
	navigateToTheLoginPage();
	verifyTheLogo();
	userLogin();
	clickNavigation();
	clickProbutton();
	newPurchaseRequisition();
	
	
	click(btn_RequestDate);
	click(btn_selRequestDate);
	selectText(txt_RCurrency, RCurrency);
	
	click(btn_Rbtnproduct);
	Thread.sleep(3000);
	sendKeys(txt_Rtxtproduct, pro7.readTestCreation("Lot"));
	Thread.sleep(3000);
	pressEnter(txt_Rtxtproduct);
	Thread.sleep(3000);
	doubleClick(sel_Rselproduct);
	
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_DraftAndNew);
	Thread.sleep(3000);
	if(isDisplayed(btn_btndraft)) {
		writeTestResults("verify Draft And New Button","Working Draft And New Button", "Draft And New Button is Working", "pass");
	}else {
		writeTestResults("verify Draft And New Button","Working Draft And New Button", "Draft And New Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_CopyFrom);
	Thread.sleep(3000);
	if(isDisplayed(txt_CopyFromText)) {
		writeTestResults("verify Copy From Button","Working Copy From Button", "Copy From Button is Working", "pass");
	}else {
		writeTestResults("verify Copy From Button","Working Copy From Button", "Copy From Button is not Working", "fail");
	}
	Thread.sleep(3000);
	click(btn_headerclose);
	
	//=============================================================
	click(btn_RequestDate);
	click(btn_selRequestDate);
	selectText(txt_RCurrency, RCurrency);
	
	click(btn_Rbtnproduct);
	Thread.sleep(3000);
	sendKeys(txt_Rtxtproduct, pro7.readTestCreation("Lot"));
	Thread.sleep(3000);
	pressEnter(txt_Rtxtproduct);
	Thread.sleep(3000);
	doubleClick(sel_Rselproduct);
	Thread.sleep(3000);
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("Verify whether system allows user to draft the record","Purchase Requisition Document Draft", "Purchase Requisition Document is Draft", "pass");
	} else {
		writeTestResults("Verify whether system allows user to draft the record","Purchase Requisition Document Draft", "Purchase Requisition Document is Not Draft", "fail");

	}
	

	Thread.sleep(3000);
	click(btn_Edit);
	Thread.sleep(3000);
	if(isDisplayed(btn_btnUpdate)) {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is Working", "pass");
	}else {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is not Working", "fail");
	}
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_UpdateAndNew);
	Thread.sleep(3000);
	if(isDisplayed(btn_btndraft)) {
		writeTestResults("verify Update And New Button","Working Update And New Button", "Update And New Button is Working", "pass");
	}else {
		writeTestResults("verify Update And New Button","Working Update And New Button", "Update And New Button is not Working", "fail");
	}
	
	//==============================================================
	click(btn_RequestDate);
	click(btn_selRequestDate);
	selectText(txt_RCurrency, RCurrency);
	
	click(btn_Rbtnproduct);
	Thread.sleep(3000);
	sendKeys(txt_Rtxtproduct, pro7.readTestCreation("Lot"));
	Thread.sleep(3000);
	pressEnter(txt_Rtxtproduct);
	Thread.sleep(3000);
	doubleClick(sel_Rselproduct);
	
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("Verify whether system allows user to draft the record","Purchase Requisition Document Draft", "Purchase Requisition Document is Draft", "pass");
	} else {
		writeTestResults("Verify whether system allows user to draft the record","Purchase Requisition Document Draft", "Purchase Requisition Document is Not Draft", "fail");

	}
	
	click(btn_Edit);
	Thread.sleep(3000);
	if(isDisplayed(btn_btnUpdate)) {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is Working", "pass");
	}else {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is not Working", "fail");
	}
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_Update);
	Thread.sleep(3000);
	if(isDisplayed(btn_btnEdit)) {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is Working", "pass");
	}else {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is not Working", "fail");
	}
	

	
	driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
	number= getText(txt_PurchaseRequisitionNumber);
	click(btn_goPurchaseRequisitionpage);
	sendKeys(txt_serchPurchaseRequisition,number);
	Thread.sleep(3000);
	pressEnter(txt_serchPurchaseRequisition);
	Thread.sleep(3000);
	click(sel_selPurchaseRequisitionNumber.replace("no", number));
	Thread.sleep(3000);
	click(btn_release);
	
	Thread.sleep(2000);
	trackCode= getText(Header);
	Thread.sleep(5000);
	if (isDisplayed(pageRelease)){

		writeTestResults("Verify whether system allows user to draft & Releas the record","Purchase Requisition Document Released", "Purchase Requisition Document is Released", "pass");
	} else {
		writeTestResults("Verify whether system allows user to draft & Releas the record","Purchase Requisition Document Released", "Purchase Requisition Document is not Released", "fail");

	}
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_Complete);
	Thread.sleep(3000);
	click(btn_Yes);
	Thread.sleep(3000);
	if(isDisplayed(txt_CompleteText)) {
		writeTestResults("verify Complete Button","Working Complete Button", "Complete Button is Working", "pass");
	}else {
		writeTestResults("verify Complete Button","Working Complete Button", "Complete Button is not Working", "fail");
	}
}

public void Transaction4() throws Exception{
	
	navigateToTheLoginPage();
	verifyTheLogo();
	userLogin();
	clickNavigation();
	clickProbutton();
	
	click(btn_PurchaseReturnOrderbutton);
	click(btn_newPurchaseReturnOrderbutton);
	
//	driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
//	click(btn_btnPROVendor);
//	sendKeys(txt_txtPROVendor, txtPROVendor);
//	Thread.sleep(3000);
//	pressEnter(txt_txtPROVendor);
//	Thread.sleep(3000);
//	doubleClick(sel_selPROVendor);
//	
//	click(btn_btnPOBillingAddress);
//	sendKeys(txt_txtPOBillingAddress, txtPOBillingAddress);
//	click(btn_btnPOApply);
//	click(btn_btnPOShippingAddress);
//	sendKeys(txt_txtPOShippingAddress, txtPOShippingAddress);
//	click(btn_btnPOApply);
//	Thread.sleep(2000);
//	click(btn_PObtnproduct);
//	Thread.sleep(2000);
//	sendKeys(txt_POtxtproduct, POtxtproduct);
//	pressEnter(txt_POtxtproduct);
//	Thread.sleep(2000);
//	doubleClick(sel_POselproduct.replace("productname", POtxtproduct));
//	sendKeys(txt_PROqty, PROqty);
//	selectIndex(btn_POwarehouse, 101);
//	click(btn_btnCheckout);
//	Thread.sleep(3000);
//	click(btn_DraftAndNew);
//	Thread.sleep(3000);
//	if(isDisplayed(btn_btndraft)) {
//		writeTestResults("verify Draft And New Button","Working Draft And New Button", "Draft And New Button is Working", "pass");
//	}else {
//		writeTestResults("verify Draft And New Button","Working Draft And New Button", "Draft And New Button is not Working", "fail");
//	}
	
	//==============================================================================================
//	driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
//	click(btn_btnPROVendor);
//	sendKeys(txt_txtPROVendor, txtPROVendor);
//	Thread.sleep(3000);
//	pressEnter(txt_txtPROVendor);
//	Thread.sleep(3000);
//	doubleClick(sel_selPROVendor);
//	
//	click(btn_btnPOBillingAddress);
//	sendKeys(txt_txtPOBillingAddress, txtPOBillingAddress);
//	click(btn_btnPOApply);
//	click(btn_btnPOShippingAddress);
//	sendKeys(txt_txtPOShippingAddress, txtPOShippingAddress);
//	click(btn_btnPOApply);
//	Thread.sleep(2000);
//	click(btn_PObtnproduct);
//	Thread.sleep(2000);
//	sendKeys(txt_POtxtproduct, POtxtproduct);
//	pressEnter(txt_POtxtproduct);
//	Thread.sleep(2000);
//	doubleClick(sel_POselproduct.replace("productname", POtxtproduct));
//	sendKeys(txt_PROqty, PROqty);
//	selectIndex(btn_POwarehouse, 101);
//	click(btn_btnCheckout);
//	Thread.sleep(3000);
//	click(btn_btndraft);
//	Thread.sleep(5000);
//	if (isDisplayed(pageDraft)){
//
//		writeTestResults("verify Purchase Return Order Document Draft","Purchase Return Order Document Draft", "Purchase Return Order Document is Draft", "pass");
//	} else {
//		writeTestResults("verify Purchase Return Order Document Draft","Purchase Return Order Document Draft", "Purchase Return Order Document is not Draft", "fail");
//	}
//	
//	Thread.sleep(3000);
//	click(btn_Edit);
//	Thread.sleep(3000);
//	if(isDisplayed(btn_btnUpdate)) {
//		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is Working", "pass");
//	}else {
//		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is not Working", "fail");
//	}
//	click(btn_btnCheckout);
//	Thread.sleep(3000);
//	click(btn_UpdateAndNew);
//	Thread.sleep(3000);
//	if(isDisplayed(btn_btndraft)) {
//		writeTestResults("verify Update And New Button","Working Update And New Button", "Update And New Button is Working", "pass");
//	}else {
//		writeTestResults("verify Update And New Button","Working Update And New Button", "Update And New Button is not Working", "fail");
//	}
	
	//===============================================================================================
	driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
	click(btn_btnPROVendor);
	sendKeys(txt_txtPROVendor, txtPROVendor);
	Thread.sleep(3000);
	pressEnter(txt_txtPROVendor);
	Thread.sleep(3000);
	doubleClick(sel_selPROVendor);
	
	click(btn_btnPOBillingAddress);
	sendKeys(txt_txtPOBillingAddress, txtPOBillingAddress);
	click(btn_btnPOApply);
	click(btn_btnPOShippingAddress);
	sendKeys(txt_txtPOShippingAddress, txtPOShippingAddress);
	click(btn_btnPOApply);
	Thread.sleep(2000);
	click(btn_PObtnproduct);
	Thread.sleep(2000);
	sendKeys(txt_POtxtproduct, POtxtproduct);
	pressEnter(txt_POtxtproduct);
	Thread.sleep(2000);
	doubleClick(sel_POselproduct.replace("productname", POtxtproduct));
	sendKeys(txt_PROqty, PROqty);
	selectIndex(btn_POwarehouse, 101);
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_Delete);
	Thread.sleep(3000);
	click(btn_Yes);
	
	Thread.sleep(3000);
	if(isDisplayed("//label[@id='lbldocstatus'][text()='(Deleted)']")) {
		writeTestResults("verify Delete Button","Working Delete  Button", "Delete  Button is Working", "pass");
	}else {
		writeTestResults("verify Delete Button","Working Delete  Button", "Delete  Button is not Working", "fail");
	}
	Thread.sleep(3000);
	click(btn_AddNewDocument);
	//==============================================================================================
	
	driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
	click(btn_btnPROVendor);
	sendKeys(txt_txtPROVendor, txtPROVendor);
	Thread.sleep(3000);
	pressEnter(txt_txtPROVendor);
	Thread.sleep(3000);
	doubleClick(sel_selPROVendor);
	
	click(btn_btnPOBillingAddress);
	sendKeys(txt_txtPOBillingAddress, txtPOBillingAddress);
	click(btn_btnPOApply);
	click(btn_btnPOShippingAddress);
	sendKeys(txt_txtPOShippingAddress, txtPOShippingAddress);
	click(btn_btnPOApply);
	Thread.sleep(2000);
	click(btn_PObtnproduct);
	Thread.sleep(2000);
	sendKeys(txt_POtxtproduct, POtxtproduct);
	pressEnter(txt_POtxtproduct);
	Thread.sleep(2000);
	doubleClick(sel_POselproduct.replace("productname", POtxtproduct));
	sendKeys(txt_PROqty, PROqty);
	selectIndex(btn_POwarehouse, 101);
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Return Order Document Draft","Purchase Return Order Document Draft", "Purchase Return Order Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Return Order Document Draft","Purchase Return Order Document Draft", "Purchase Return Order Document is not Draft", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_Edit);
	Thread.sleep(3000);
	if(isDisplayed(btn_btnUpdate)) {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is Working", "pass");
	}else {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is not Working", "fail");
	}
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_Update);
	Thread.sleep(3000);
	if(isDisplayed(btn_btnEdit)) {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is Working", "pass");
	}else {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_Reminders);
	Thread.sleep(3000);
	if(isDisplayed(txt_RemindersText)) {
		writeTestResults("verify Reminders Button","Working Reminders Button", "Reminders Button is Working", "pass");
	}else {
		writeTestResults("verify Reminders Button","Working Reminders Button", "Reminders Button is not Working", "fail");
	}
	click(btn_headerclose);
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_Activities);
	Thread.sleep(3000);
	if(isDisplayed(txt_ActivitiesText)) {
		writeTestResults("verify Activities Button","Working Activities Button", "Activities Button is Working", "pass");
	}else {
		writeTestResults("verify Activities Button","Working Activities Button", "Activities Button is not Working", "fail");
	}
	pageRefersh();
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_History);
	Thread.sleep(3000);
	if(isDisplayed(txt_HistoryText)) {
		writeTestResults("verify History Button","Working History Button", "History Button is Working", "pass");
	}else {
		writeTestResults("verify History Button","Working History Button", "History Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_DocFlow);
	Thread.sleep(3000);
	if(isDisplayed(ZoomBar)) {
		writeTestResults("verify DocFlow Button","Working DocFlow Button", "DocFlow Button is Working", "pass");
	}else {
		writeTestResults("verify DocFlow Button","Working DocFlow Button", "DocFlow Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	Thread.sleep(5000);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify Purchase Return Order Document Released","Purchase Return Order Document Released", "Purchase Return Order Document is Released", "pass");
	} else {
		writeTestResults("verify Purchase Return Order Document Released","Purchase Return Order Document Released", "Purchase Return Order Document is not Released", "fail");
	}
	Thread.sleep(3000);
	click(btn_btnGotopage);
	
	switchWindow();
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_PROdraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Outbound Shipment Document Draft","Outbound Shipment Document Draft", "Outbound Shipment Document is Draft", "pass");
	} else {
		writeTestResults("verify Outbound Shipment Document Draft","Outbound Shipment Document Draft", "Outbound Shipment Document is not Draft", "fail");
	}
	

	
	Thread.sleep(3000);
	click(btn_POMasterInfo);
	Thread.sleep(3000);
	click(btn_POProductBS);
	Thread.sleep(3000);
	number = getText(txt_POSNo);
	Thread.sleep(3000);
	click(btn_headerclose);
	
	Thread.sleep(3000);
	click(btn_POMasterInfo);
	Thread.sleep(3000);
	if(click(btn_RackwiseAvailable)==true) {
		writeTestResults("verify Rack wise Available Button","Working Rack wise Available  Button", "Rack wise Available  Button is Working", "pass");
	}else {
		writeTestResults("verify Rack wise Available Button","Working Rack wise Available  Button", "Rack wise Available  Button is not Working", "fail");
	}
	Thread.sleep(3000);
	click(btn_headerclose);
	
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_Delete);
	
	Thread.sleep(3000);
	if(click(btn_No)==true) {
		writeTestResults("verify Delete Button","Working Delete  Button", "Delete  Button is Working", "pass");
	}else {
		writeTestResults("verify Delete Button","Working Delete  Button", "Delete  Button is not Working", "fail");
	}

	Thread.sleep(3000);
	if(click(btn_btnSerialBatch)==true) {
		writeTestResults("verify Serial Batch Button","Working Serial Batch  Button", "Serial Batch  Button is Working", "pass");
	}else {
		writeTestResults("verify Serial Batch Button","Working Serial Batch  Button", "Serial Batch  Button is not Working", "fail");
	}
	Thread.sleep(3000);
	click(btn_btnselProductList);
	Thread.sleep(3000);
	sendKeys(txt_POSerialNo,number);
	pressEnter(txt_POSerialNo);
	Thread.sleep(3000);
	click(btn_btnPOSerialNo);
	Thread.sleep(3000);
	click(btn_updateSerial);
	Thread.sleep(3000);
	click(btn_closeProductList);

	
	
	Thread.sleep(3000);
	click(btn_Edit);
	Thread.sleep(3000);
	if(isDisplayed(btn_btnUpdate)) {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is Working", "pass");
	}else {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is not Working", "fail");
	}
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_Update);
	Thread.sleep(3000);
	if(isDisplayed(btn_btnEdit)) {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is Working", "pass");
	}else {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_Reminders);
	Thread.sleep(3000);
	if(isDisplayed(txt_RemindersText)) {
		writeTestResults("verify Reminders Button","Working Reminders Button", "Reminders Button is Working", "pass");
	}else {
		writeTestResults("verify Reminders Button","Working Reminders Button", "Reminders Button is not Working", "fail");
	}
	click(btn_headerclose);
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_Activities);
	Thread.sleep(3000);
	if(isDisplayed(txt_ActivitiesText)) {
		writeTestResults("verify Activities Button","Working Activities Button", "Activities Button is Working", "pass");
	}else {
		writeTestResults("verify Activities Button","Working Activities Button", "Activities Button is not Working", "fail");
	}
	pageRefersh();
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_History);
	Thread.sleep(3000);
	if(isDisplayed(txt_HistoryText)) {
		writeTestResults("verify History Button","Working History Button", "History Button is Working", "pass");
	}else {
		writeTestResults("verify History Button","Working History Button", "History Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_DocFlow);
	Thread.sleep(3000);
	if(isDisplayed(ZoomBar)) {
		writeTestResults("verify DocFlow Button","Working DocFlow Button", "DocFlow Button is Working", "pass");
	}else {
		writeTestResults("verify DocFlow Button","Working DocFlow Button", "DocFlow Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	Thread.sleep(5000);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify Outbound Shipment Document Released","Outbound Shipment Document Released", "Outbound Shipment Document is Released", "pass");
	} else {
		writeTestResults("verify Outbound Shipment Document Released","Outbound Shipment Document Released", "Outbound Shipment Document is not Released", "fail");
	}
	
	Thread.sleep(3000);
	if(click(btn_btnGotopage)==true) {
		writeTestResults("verify Convert to Purchase Return Invoice Button","Working Convert to Purchase Return Invoice Button", "Convert to Purchase Return Invoice Button is Working", "pass");
	}else {
		writeTestResults("verify Convert to Purchase Return Invoice Button","Working Convert to Purchase Return Invoice Button", "Convert to Purchase Return Invoice Button is not Working", "fail");
	}
	
	switchWindow();
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Return Invoice Document Draft","Purchase Return Invoice Document Draft", "Purchase Return Invoice Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Return Invoice Document Draft","Purchase Return Invoice Document Draft", "Purchase Return Invoice Document is not Draft", "fail");

	}
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_Delete);
	Thread.sleep(3000);
	//click(btn_No);
	
	Thread.sleep(3000);
	if(click(btn_No)==true) {
		writeTestResults("verify Delete Button","Working Delete  Button", "Delete  Button is Working", "pass");
	}else {
		writeTestResults("verify Delete Button","Working Delete  Button", "Delete  Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_Edit);
	Thread.sleep(3000);
	if(isDisplayed(btn_btnUpdate)) {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is Working", "pass");
	}else {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is not Working", "fail");
	}
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_Update);
	Thread.sleep(3000);
	if(isDisplayed(btn_btnEdit)) {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is Working", "pass");
	}else {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_Reminders);
	Thread.sleep(3000);
	if(isDisplayed(txt_RemindersText)) {
		writeTestResults("verify Reminders Button","Working Reminders Button", "Reminders Button is Working", "pass");
	}else {
		writeTestResults("verify Reminders Button","Working Reminders Button", "Reminders Button is not Working", "fail");
	}
	click(btn_headerclose);
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_Activities);
	Thread.sleep(3000);
	if(isDisplayed(txt_ActivitiesText)) {
		writeTestResults("verify Activities Button","Working Activities Button", "Activities Button is Working", "pass");
	}else {
		writeTestResults("verify Activities Button","Working Activities Button", "Activities Button is not Working", "fail");
	}
	
	pageRefersh();
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_History);
	Thread.sleep(3000);
	if(isDisplayed(txt_HistoryText)) {
		writeTestResults("verify History Button","Working History Button", "History Button is Working", "pass");
	}else {
		writeTestResults("verify History Button","Working History Button", "History Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_DocFlow);
	Thread.sleep(3000);
	if(isDisplayed(ZoomBar)) {
		writeTestResults("verify DocFlow Button","Working DocFlow Button", "DocFlow Button is Working", "pass");
	}else {
		writeTestResults("verify DocFlow Button","Working DocFlow Button", "DocFlow Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_release);
	
	Thread.sleep(2000);
	trackCode= getText(Header);
	Thread.sleep(5000);
	if (isDisplayed(pageRelease)){

		writeTestResults("verify Purchase Return Invoice Document Released","Purchase Return Invoice Document Released", "Purchase Return Invoice Document is Released", "pass");
	} else {
		writeTestResults("verify Purchase Return Invoice Document Released","Purchase Return Invoice Document Released", "Purchase Return Invoice Document is not Released", "fail");

	}
	
	Thread.sleep(3000);
	click(btn_Print);
	Thread.sleep(3000);
	if(isDisplayed(txt_PrintoutTemplates)) {
		writeTestResults("verify Print Button","Working Print Button", "Print Button is Working", "pass");
	}else {
		writeTestResults("verify Print Button","Working Print Button", "Print Button is not Working", "fail");
	}
	Thread.sleep(3000);
	click(btn_Back);
	
	Thread.sleep(3000);
	click(btn_btnAction);
	click(btn_Reverse);
	Thread.sleep(3000);
	click(btn_ReverseButton);
	Thread.sleep(3000);
	click(btn_Yes);
	Thread.sleep(3000);
	if(getText(txt_ReverseText).contentEquals("(Reversed)")) {
		writeTestResults("verify Reversed Button","Working Reversed Button", "Reversed Button is Working", "pass");
	}else {
		writeTestResults("verify Reversed Button","Working Reversed Button", "Reversed Button is not Working", "fail");
	}
	
}

public void Transaction5() throws Exception{
	
	navigateToTheLoginPage();
	verifyTheLogo();
	userLogin();
	clickNavigation();
	clickProbutton();
	
	click(btn_PurchaseInvoicebutton);
	if(isDisplayed(txt_PurchaseInvoicebuttonpage)) {
		writeTestResults("verify Purchase Invoice page","view Purchase Invoice page", "Purchase Invoice page is display", "pass");
	}else {
		writeTestResults("verify Purchase Invoice page","view Purchase Invoice page", "Purchase Invoice page is not display", "fail");
	}

	click(btn_newPurchaseInvoicebutton);
	Thread.sleep(3000);
	click(btn_btnPIService);
	if(isDisplayed(txt_newPurchaseInvoicebuttonpage)&&(getText(Status).contentEquals("New"))) {
		writeTestResults("verify New Purchase Invoice page","view New Purchase Invoice page", "New Purchase Invoice page is display", "pass");
	}else {
		writeTestResults("verify Purchase Invoice page","view New Purchase Invoice page", "New Purchase Invoice page is not display", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_CopyFrom);
	Thread.sleep(3000);
	if(isDisplayed(txt_PurchaseInvoicetxt)) {
		writeTestResults("verify Copy From Button","Working Copy From Button", "Copy From Button is Working", "pass");
	}else {
		writeTestResults("verify Copy From Button","Working Copy From Button", "Copy From Button is not Working", "fail");
	}
	Thread.sleep(3000);
	click(btn_headerclose);
	
	Thread.sleep(3000);
	click(btn_btnPIVendor);
	Thread.sleep(3000);
	sendKeys(txt_txtPIVendor, txtPIVendor);
	Thread.sleep(3000);
	pressEnter(txt_txtPIVendor);
	Thread.sleep(3000);
	doubleClick(sel_selPIVendor);
	
	click(btn_btnPIBillingAddress);
	sendKeys(txt_txtPIBillingAddress, txtPIBillingAddress);
	click(btn_btnApply);
	click(btn_btnPIShippingAddress);
	sendKeys(txt_txtPIShippingAddress, txtPIShippingAddress);
	click(btn_btnApply);
	click(btn_Pbtndetails);
	click(btn_btnPIsummary);
	
	Thread.sleep(3000);
	click(btn_PIbtnproduct);
	Thread.sleep(3000);
	sendKeys(txt_PItxtSproduct,PItxtSproduct);
	Thread.sleep(3000);
	pressEnter(txt_PItxtSproduct);
	Thread.sleep(3000);
	doubleClick(sel_PIselproduct.replace("productname", PItxtSproduct));

	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Invoice Document Draft","Purchase Invoice Document Draft", "Purchase Invoice Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Invoice Document Draft","Purchase Invoice Document Draft", "Purchase Invoice Document is not Draft", "fail");
	}
	
	
	Thread.sleep(3000);
	click(btn_Duplicate);
	Thread.sleep(3000);
	if(getText(Status).contentEquals("New")) 
	{
		writeTestResults("verify Duplicate Button","Working Duplicate Button", "Duplicate Button is Working", "pass");
	}else {
		writeTestResults("verify Duplicate Button","Working Duplicate Button", "Duplicate Button is not Working", "fail");
	}
	
	click(btn_btnCheckout);
	
	Thread.sleep(3000);
	click(btn_DraftAndNew);
	Thread.sleep(3000);
	if(isDisplayed(btn_btndraft)) {
		writeTestResults("verify Draft And New Button","Working Draft And New Button", "Draft And New Button is Working", "pass");
	}else {
		writeTestResults("verify Draft And New Button","Working Draft And New Button", "Draft And New Button is not Working", "fail");
	}
	//=========================================================================================
	
	Thread.sleep(3000);
	click(btn_btnPIVendor);
	Thread.sleep(3000);
	sendKeys(txt_txtPIVendor, txtPIVendor);
	Thread.sleep(3000);
	pressEnter(txt_txtPIVendor);
	Thread.sleep(3000);
	doubleClick(sel_selPIVendor);
	
	click(btn_btnPIBillingAddress);
	sendKeys(txt_txtPIBillingAddress, txtPIBillingAddress);
	click(btn_btnApply);
	click(btn_btnPIShippingAddress);
	sendKeys(txt_txtPIShippingAddress, txtPIShippingAddress);
	click(btn_btnApply);
	click(btn_Pbtndetails);
	click(btn_btnPIsummary);
	
	Thread.sleep(3000);
	click(btn_PIbtnproduct);
	Thread.sleep(3000);
	sendKeys(txt_PItxtSproduct,PItxtSproduct);
	Thread.sleep(3000);
	pressEnter(txt_PItxtSproduct);
	Thread.sleep(3000);
	doubleClick(sel_PIselproduct.replace("productname", PItxtSproduct));

	
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Invoice Document Draft","Purchase Invoice Document Draft", "Purchase Invoice Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Invoice Document Draft","Purchase Invoice Document Draft", "Purchase Invoice Document is not Draft", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_Edit);
	Thread.sleep(3000);
	if(isDisplayed(btn_btnUpdate)) {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is Working", "pass");
	}else {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is not Working", "fail");
	}

	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_UpdateAndNew);
	Thread.sleep(3000);
	if(isDisplayed(btn_btndraft)) {
		writeTestResults("verify Update And New Button","Working Update And New Button", "Update And New Button is Working", "pass");
	}else {
		writeTestResults("verify Update And New Button","Working Update And New Button", "Update And New Button is not Working", "fail");
	}
	
	//==========================================================================================
	
	Thread.sleep(3000);
	click(btn_btnPIVendor);
	Thread.sleep(3000);
	sendKeys(txt_txtPIVendor, txtPIVendor);
	Thread.sleep(3000);
	pressEnter(txt_txtPIVendor);
	Thread.sleep(3000);
	doubleClick(sel_selPIVendor);
	
	click(btn_btnPIBillingAddress);
	sendKeys(txt_txtPIBillingAddress, txtPIBillingAddress);
	click(btn_btnApply);
	click(btn_btnPIShippingAddress);
	sendKeys(txt_txtPIShippingAddress, txtPIShippingAddress);
	click(btn_btnApply);
	click(btn_Pbtndetails);
	click(btn_btnPIsummary);
	
	Thread.sleep(3000);
	click(btn_PIbtnproduct);
	Thread.sleep(3000);
	sendKeys(txt_PItxtSproduct,PItxtSproduct);
	Thread.sleep(3000);
	pressEnter(txt_PItxtSproduct);
	Thread.sleep(3000);
	doubleClick(sel_PIselproduct.replace("productname", PItxtSproduct));

	
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Invoice Document Draft","Purchase Invoice Document Draft", "Purchase Invoice Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Invoice Document Draft","Purchase Invoice Document Draft", "Purchase Invoice Document is not Draft", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnAction);
	
	Thread.sleep(3000);
	click(btn_Delete);
	Thread.sleep(3000);
	click(btn_Yes);
	
	
	Thread.sleep(3000);
	if(getText(txt_DeleteText).contentEquals("(Deleted)")) {
		writeTestResults("verify Delete Button","Working Delete  Button", "Delete  Button is Working", "pass");
	}else {
		writeTestResults("verify Delete Button","Working Delete  Button", "Delete  Button is not Working", "fail");
	}
	Thread.sleep(3000);
	click(btn_AddNewDocument);
	//==========================================================================================
	
	Thread.sleep(3000);
	click(btn_btnPIService);
	Thread.sleep(3000);
	click(btn_btnPIVendor);
	Thread.sleep(3000);
	sendKeys(txt_txtPIVendor, txtPIVendor);
	Thread.sleep(3000);
	pressEnter(txt_txtPIVendor);
	Thread.sleep(3000);
	doubleClick(sel_selPIVendor);
	
	click(btn_btnPIBillingAddress);
	sendKeys(txt_txtPIBillingAddress, txtPIBillingAddress);
	click(btn_btnApply);
	click(btn_btnPIShippingAddress);
	sendKeys(txt_txtPIShippingAddress, txtPIShippingAddress);
	click(btn_btnApply);
	click(btn_Pbtndetails);
	click(btn_btnPIsummary);
	
	Thread.sleep(3000);
	click(btn_PIbtnproduct);
	Thread.sleep(3000);
	sendKeys(txt_PItxtSproduct,PItxtSproduct);
	Thread.sleep(3000);
	pressEnter(txt_PItxtSproduct);
	Thread.sleep(3000);
	doubleClick(sel_PIselproduct.replace("productname", PItxtSproduct));

//	Thread.sleep(3000);
//	click(btn_CopyFrom);
//	Thread.sleep(3000);
//	if(isDisplayed("//span[@class='headertext']")) {
//		writeTestResults("verify Copy From Button","Working Copy From Button", "Copy From Button is Working", "pass");
//	}else {
//		writeTestResults("verify Copy From Button","Working Copy From Button", "Copy From Button is not Working", "fail");
//	}
//	Thread.sleep(3000);
//	click(btn_headerclose);
	
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Invoice Document Draft","Purchase Invoice Document Draft", "Purchase Invoice Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Invoice Document Draft","Purchase Invoice Document Draft", "Purchase Invoice Document is not Draft", "fail");
	}
	

	
	Thread.sleep(3000);
	click(btn_Edit);
	Thread.sleep(3000);
	if(isDisplayed(btn_btnUpdate)) {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is Working", "pass");
	}else {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is not Working", "fail");
	}
	
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_Update);
	Thread.sleep(3000);
	if(isDisplayed(btn_btnEdit)) {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is Working", "pass");
	}else {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_Reminders);
	Thread.sleep(3000);
	if(isDisplayed(txt_RemindersText)) {
		writeTestResults("verify Reminders Button","Working Reminders Button", "Reminders Button is Working", "pass");
	}else {
		writeTestResults("verify Reminders Button","Working Reminders Button", "Reminders Button is not Working", "fail");
	}
	click(btn_headerclose);
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_Activities);
	Thread.sleep(3000);
	if(isDisplayed(txt_ActivitiesText)) {
		writeTestResults("verify Activities Button","Working Activities Button", "Activities Button is Working", "pass");
	}else {
		writeTestResults("verify Activities Button","Working Activities Button", "Activities Button is not Working", "fail");
	}
	pageRefersh();
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_History);
	Thread.sleep(3000);
	if(isDisplayed(txt_HistoryText)) {
		writeTestResults("verify History Button","Working History Button", "History Button is Working", "pass");
	}else {
		writeTestResults("verify History Button","Working History Button", "History Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_DocFlow);
	Thread.sleep(3000);
	if(isDisplayed(ZoomBar)) {
		writeTestResults("verify DocFlow Button","Working DocFlow Button", "DocFlow Button is Working", "pass");
	}else {
		writeTestResults("verify DocFlow Button","Working DocFlow Button", "DocFlow Button is not Working", "fail");
	}
	
	
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	Thread.sleep(5000);
	if (isDisplayed(pageRelease)){

		writeTestResults("verify Purchase Invoice Document Released","Purchase Invoice Document Released", "Purchase Invoice Document is Released", "pass");
	} else {
		writeTestResults("verify Purchase Invoice Document Released","Purchase Invoice Document Released", "Purchase Invoice Document is not Released", "fail");

	}
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_Acknowledgement);
	Thread.sleep(3000);
	if(isDisplayed(txt_Acknowledgementtxt)) {
		writeTestResults("verify Acknowledgement Button","Working Acknowledgement Button", "Acknowledgement Button is Working", "pass");
	}else {
		writeTestResults("verify Acknowledgement Button","Working Acknowledgement Button", "Acknowledgement Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_headerclose);
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_ChangeCostAllocations);
	Thread.sleep(3000);
	if(isDisplayed(txt_ChangeCostAllocationstxt)) {
		writeTestResults("verify Change Cost Allocations Button","Working Change Cost Allocations Button", "Change Cost Allocations Button is Working", "pass");
	}else {
		writeTestResults("verify Change Cost Allocations Button","Working Change Cost Allocations Button", "Change Cost Allocations Button is not Working", "fail");
	}
	
	pageRefersh();
	
	Thread.sleep(3000);
	click(btn_Print);
	Thread.sleep(3000);
	if(isDisplayed(txt_PrintoutTemplates)) {
		writeTestResults("verify Print Button","Working Print Button", "Print Button is Working", "pass");
	}else {
		writeTestResults("verify Print Button","Working Print Button", "Print Button is not Working", "fail");
	}
	Thread.sleep(3000);
	click(btn_Back);
	
	Thread.sleep(3000);
	click(btn_btnAction);
	click(btn_Reverse);
	Thread.sleep(3000);
	click(btn_ReverseButton);
	Thread.sleep(3000);
	click(btn_Yes);
	Thread.sleep(5000);
	if(getText(txt_ReverseText).contentEquals("(Reversed)")) {
		writeTestResults("verify Reversed Button","Working Reversed Button", "Reversed Button is Working", "pass");
	}else {
		writeTestResults("verify Reversed Button","Working Reversed Button", "Reversed Button is not Working", "fail");
	}
	
}

public void Transaction6() throws Exception{
	
	
	navigateToTheLoginPage();
	verifyTheLogo();
	userLogin();
	clickNavigation();
	clickProbutton();
	
	//=========================================PO=========================================================================================================
	
	click(btn_PurchaseOrderbutton);
	if(isDisplayed(txt_PurchaseOrderpage)) {
		writeTestResults("verify Purchase Order page","view Purchase Order page", "Purchase Order page is display", "pass");
	}else {
		writeTestResults("verify Purchase Order page","view Purchase Order page", "Purchase Order page is not display", "fail");
	}
	click(btn_newPurchaseOrderbutton);
	
	driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
	click(btn_btnPOtoShipmentCosting);
	if(isDisplayed(txt_newPurchaseOrderpage)&&(getText(Status).contentEquals("New"))) {
		writeTestResults("verify  New Purchase Order page","view  New Purchase Order page", " New Purchase Order page is display", "pass");
	}else {
		writeTestResults("verify  New Purchase Order page","view  New Purchase Order page", " New Purchase Order page is not display", "fail");
	}
	
	//=========================================PO=========================================================================================================
	Thread.sleep(3000);
	click(btn_CopyFrom);
	Thread.sleep(3000);
	if(isDisplayed(txt_PurchaseOrdertxt)) {
		writeTestResults("verify Copy From Button","Working Copy From Button", "Copy From Button is Working", "pass");
	}else {
		writeTestResults("verify Copy From Button","Working Copy From Button", "Copy From Button is not Working", "fail");
	}
	Thread.sleep(3000);
	click(btn_headerclose);
	
	
	driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
	click(btn_PbtnVendor);
	sendKeys(txt_txtVendor, txtVendor);
	Thread.sleep(3000);
	pressEnter(txt_txtVendor);
	Thread.sleep(3000);
	doubleClick(sel_selVendor);
	
	click(btn_btnBillingAddress);
	sendKeys(txt_txtBillingAddress, txtBillingAddress);
	click(btn_btnApply);
	click(btn_btnShippingAddress);
	sendKeys(txt_txtShippingAddress, txtShippingAddress);
	click(btn_btnApply);
	click(btn_Pbtndetails);
	click(btn_btnPInvoiceAccount);
	
	driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
	sendKeys(txt_txtVendor, txtVendor);
	Thread.sleep(3000);
	pressEnter(txt_txtVendor);
	Thread.sleep(3000);
	doubleClick(sel_selVendor);
	click(btn_btnsummary);
	
	Thread.sleep(3000);
	click(btn_pbtnproduct);
	Thread.sleep(3000);
	sendKeys(txt_ptxtproduct,pro7.readTestCreation("BatchSpecific"));
	Thread.sleep(3000);
	pressEnter(txt_ptxtproduct);
	Thread.sleep(3000);
	doubleClick(sel_pselproduct);
	Thread.sleep(3000);
	sendKeys(txt_pro7POqty1, TestQuantity);
	
	click(btn_btnCheckout);
	
	Thread.sleep(3000);
	click(btn_DraftAndNew);
	Thread.sleep(3000);
	if(isDisplayed(btn_btndraft)) {
		writeTestResults("verify Draft And New Button","Working Draft And New Button", "Draft And New Button is Working", "pass");
	}else {
		writeTestResults("verify Draft And New Button","Working Draft And New Button", "Draft And New Button is not Working", "fail");
	}
	//=========================================PO=========================================================================================================
	driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
	click(btn_PbtnVendor);
	sendKeys(txt_txtVendor, txtVendor);
	Thread.sleep(3000);
	pressEnter(txt_txtVendor);
	Thread.sleep(3000);
	doubleClick(sel_selVendor);
	
	click(btn_btnBillingAddress);
	sendKeys(txt_txtBillingAddress, txtBillingAddress);
	click(btn_btnApply);
	click(btn_btnShippingAddress);
	sendKeys(txt_txtShippingAddress, txtShippingAddress);
	click(btn_btnApply);
	click(btn_Pbtndetails);
	click(btn_btnPInvoiceAccount);
	
	driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
	sendKeys(txt_txtVendor, txtVendor);
	Thread.sleep(3000);
	pressEnter(txt_txtVendor);
	Thread.sleep(3000);
	doubleClick(sel_selVendor);
	click(btn_btnsummary);
	
	Thread.sleep(3000);
	click(btn_pbtnproduct);
	Thread.sleep(3000);
	sendKeys(txt_ptxtproduct,pro7.readTestCreation("BatchSpecific"));
	Thread.sleep(3000);
	pressEnter(txt_ptxtproduct);
	Thread.sleep(3000);
	doubleClick(sel_pselproduct);
	Thread.sleep(3000);
	sendKeys(txt_pro7POqty1, TestQuantity);
	
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Order Document Draft","Purchase Order Document Draft", "Purchase Order Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Order Document Draft","Purchase Order Document Draft", "Purchase Order Document is not Draft", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_Duplicate);
	Thread.sleep(3000);
	if(getText(Status).contentEquals("New")) 
	{
		writeTestResults("verify Duplicate Button","Working Duplicate Button", "Duplicate Button is Working", "pass");
	}else {
		writeTestResults("verify Duplicate Button","Working Duplicate Button", "Duplicate Button is not Working", "fail");
	}
	
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_Delete);
	Thread.sleep(3000);
	click(btn_Yes);
	
	Thread.sleep(3000);
	if(getText(txt_DeleteText).contentEquals("(Deleted)")) {
		writeTestResults("verify Delete Button","Working Delete  Button", "Delete  Button is Working", "pass");
	}else {
		writeTestResults("verify Delete Button","Working Delete  Button", "Delete  Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_AddNewDocument);
	//=========================================PO=========================================================================================================
	click(btn_btnPOtoShipmentCosting);
	
	click(btn_btnPOtoShipmentCosting);
	driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
	click(btn_PbtnVendor);
	sendKeys(txt_txtVendor, txtVendor);
	Thread.sleep(3000);
	pressEnter(txt_txtVendor);
	Thread.sleep(3000);
	doubleClick(sel_selVendor);
	
	click(btn_btnBillingAddress);
	sendKeys(txt_txtBillingAddress, txtBillingAddress);
	click(btn_btnApply);
	click(btn_btnShippingAddress);
	sendKeys(txt_txtShippingAddress, txtShippingAddress);
	click(btn_btnApply);
	click(btn_Pbtndetails);
	click(btn_btnPInvoiceAccount);
	
	driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
	sendKeys(txt_txtVendor, txtVendor);
	Thread.sleep(3000);
	pressEnter(txt_txtVendor);
	Thread.sleep(3000);
	doubleClick(sel_selVendor);
	click(btn_btnsummary);
	
	Thread.sleep(3000);
	click(btn_pbtnproduct);
	Thread.sleep(3000);
	sendKeys(txt_ptxtproduct,pro7.readTestCreation("BatchSpecific"));
	Thread.sleep(3000);
	pressEnter(txt_ptxtproduct);
	Thread.sleep(3000);
	doubleClick(sel_pselproduct);
	Thread.sleep(3000);
	sendKeys(txt_pro7POqty1, TestQuantity);
	
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Order Document Draft","Purchase Order Document Draft", "Purchase Order Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Order Document Draft","Purchase Order Document Draft", "Purchase Order Document is not Draft", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_Edit);
	Thread.sleep(3000);
	if(isDisplayed(btn_btnUpdate)) {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is Working", "pass");
	}else {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is not Working", "fail");
	}

	
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_UpdateAndNew);
	Thread.sleep(3000);
	if(isDisplayed(btn_btndraft)) {
		writeTestResults("verify Update And New Button","Working Update And New Button", "Update And New Button is Working", "pass");
	}else {
		writeTestResults("verify Update And New Button","Working Update And New Button", "Update And New Button is not Working", "fail");
	}
	
	//=========================================PO=========================================================================================================
	click(btn_btnPOtoShipmentCosting);
	driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
	click(btn_PbtnVendor);
	sendKeys(txt_txtVendor, txtVendor);
	Thread.sleep(3000);
	pressEnter(txt_txtVendor);
	Thread.sleep(3000);
	doubleClick(sel_selVendor);
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_CostEstimation);
	Thread.sleep(3000);
	if(isDisplayed(txt_CostEstimationtxt)) {
		writeTestResults("verify Cost Estimation Button","Working Cost Estimation Button", "Cost Estimation Button is Working", "pass");
	}else {
		writeTestResults("verify Cost Estimation Button","Working Cost Estimation Button", "Cost Estimation Button is not Working", "fail");
	}
	Thread.sleep(3000);
	click(btn_CostEstimationClose);
	Thread.sleep(3000);
	pageRefersh();
	
	click(btn_btnPOtoShipmentCosting);
	driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
	click(btn_PbtnVendor);
	sendKeys(txt_txtVendor, txtVendor);
	Thread.sleep(3000);
	pressEnter(txt_txtVendor);
	Thread.sleep(3000);
	doubleClick(sel_selVendor);
	
	click(btn_btnBillingAddress);
	sendKeys(txt_txtBillingAddress, txtBillingAddress);
	click(btn_btnApply);
	click(btn_btnShippingAddress);
	sendKeys(txt_txtShippingAddress, txtShippingAddress);
	click(btn_btnApply);
	click(btn_Pbtndetails);
	click(btn_btnPInvoiceAccount);
	
	driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
	sendKeys(txt_txtVendor, txtVendor);
	Thread.sleep(3000);
	pressEnter(txt_txtVendor);
	Thread.sleep(3000);
	doubleClick(sel_selVendor);
	click(btn_btnsummary);
	
	Thread.sleep(3000);
	click(btn_pbtnproduct);
	Thread.sleep(3000);
	sendKeys(txt_ptxtproduct,pro7.readTestCreation("BatchSpecific"));
	Thread.sleep(3000);
	pressEnter(txt_ptxtproduct);
	Thread.sleep(3000);
	doubleClick(sel_pselproduct);
	Thread.sleep(3000);
	sendKeys(txt_pro7POqty1, TestQuantity);
	
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Order Document Draft","Purchase Order Document Draft", "Purchase Order Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Order Document Draft","Purchase Order Document Draft", "Purchase Order Document is not Draft", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_Edit);
	Thread.sleep(3000);
	if(isDisplayed(btn_btnUpdate)) {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is Working", "pass");
	}else {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_Update);
	Thread.sleep(3000);
	if(isDisplayed(btn_btnEdit)) {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is Working", "pass");
	}else {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_Reminders);
	Thread.sleep(3000);
	if(isDisplayed(txt_RemindersText)) {
		writeTestResults("verify Reminders Button","Working Reminders Button", "Reminders Button is Working", "pass");
	}else {
		writeTestResults("verify Reminders Button","Working Reminders Button", "Reminders Button is not Working", "fail");
	}
	click(btn_headerclose);

	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_Activities);
	Thread.sleep(3000);
	if(isDisplayed(txt_ActivitiesText)) {
		writeTestResults("verify Activities Button","Working Activities Button", "Activities Button is Working", "pass");
	}else {
		writeTestResults("verify Activities Button","Working Activities Button", "Activities Button is not Working", "fail");
	}
	pageRefersh();
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_History);
	Thread.sleep(3000);
	if(isDisplayed(txt_HistoryText)) {
		writeTestResults("verify History Button","Working History Button", "History Button is Working", "pass");
	}else {
		writeTestResults("verify History Button","Working History Button", "History Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	PurchaseOrderNo=trackCode;
	Thread.sleep(5000);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify Purchase Order Document Released","Purchase Order Document Released", "Purchase Order Document is Released", "pass");
	} else {
		writeTestResults("verify Purchase Order Document Released","Purchase Order Document Released", "Purchase Order Document is not Released", "fail");
	}
	
	Thread.sleep(3000);
	pageRefersh();
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_DocFlow);
	Thread.sleep(3000);
	if(isDisplayed(ZoomBar)) {
		writeTestResults("verify DocFlow Button","Working DocFlow Button", "DocFlow Button is Working", "pass");
	}else {
		writeTestResults("verify DocFlow Button","Working DocFlow Button", "DocFlow Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_Close);
	Thread.sleep(3000);
	if(isDisplayed(txt_Closetxt)) {
		writeTestResults("verify Close Button","Working Close Button", "Close Button is Working", "pass");
	}else {
		writeTestResults("verify Close Button","Working Close Button", "Close Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	pageRefersh();
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_Hold);
	Thread.sleep(3000);
	if(isDisplayed(txt_Closetxt)) {
		writeTestResults("verify Hold Button","Working Hold Button", "Hold Button is Working", "pass");
	}else {
		writeTestResults("verify Hold Button","Working Hold Button", "Hold Button is not Working", "fail");
	}
	Thread.sleep(3000);
	sendKeys(txt_Reason, ICEDescription);
	Thread.sleep(3000);
	click(btn_OK);
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_UnHold);
	Thread.sleep(3000);
	if(isDisplayed(txt_Closetxt)) {
		writeTestResults("verify UnHold Button","Working UnHold Button", "UnHold Button is Working", "pass");
	}else {
		writeTestResults("verify UnHold Button","Working UnHold Button", "UnHold Button is not Working", "fail");
	}
	Thread.sleep(3000);
	sendKeys(txt_Reason, ICEDescription);
	Thread.sleep(3000);
	click(btn_OK);
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_ExtendExpiryDate);
	Thread.sleep(3000);
	if(isDisplayed(txt_ExtendExpiryDatetxt)) {
		writeTestResults("verify Extend Expiry Date Button","Working Extend Expiry Date Button", "Extend Expiry Date Button is Working", "pass");
	}else {
		writeTestResults("verify Extend Expiry Date Button","Working Extend Expiry Date Button", "Extend Expiry Date Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	pageRefersh();
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_UpdateLogisticDates);
	Thread.sleep(3000);
	if(isDisplayed(txt_UpdateLogisticDatestxt)) {
		writeTestResults("verify Update Logistic Dates Button","Working Update Logistic Dates Button", "Update Logistic Dates Button is Working", "pass");
	}else {
		writeTestResults("verify Update Logistic Dates Button","Working Update Logistic Dates Button", "Update Logistic Dates Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	pageRefersh();
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_DotheOutboundAdvance);
	Thread.sleep(3000);
	if(isDisplayed(txt_DotheOutboundAdvancetxt)) {
		writeTestResults("verify Do the Outbound Advance Button","Working Do the Outbound Advance Button", "Do the Outbound Advance Button is Working", "pass");
	}else {
		writeTestResults("verify Do the Outbound Advance Button","Working Do the Outbound Advance Button", "Do the Outbound Advance Button is not Working", "fail");
	}
	
}

public void Transaction7() throws Exception{
	
	navigateToTheLoginPage();
	verifyTheLogo();
	userLogin();
	clickNavigation();
	clickProbutton();
	
	click(btn_PurchaseOrderbutton);
	if(isDisplayed(txt_PurchaseOrderpage)) {
		writeTestResults("verify Purchase Order page","view Purchase Order page", "Purchase Order page is display", "pass");
	}else {
		writeTestResults("verify Purchase Order page","view Purchase Order page", "Purchase Order page is not display", "fail");
	}
	click(btn_newPurchaseOrderbutton);
	
	driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
	click(btn_btnPOtoShipmentCosting);
	if(isDisplayed(txt_newPurchaseOrderpage)&&(getText(Status).contentEquals("New"))) {
		writeTestResults("verify  New Purchase Order page","view  New Purchase Order page", " New Purchase Order page is display", "pass");
	}else {
		writeTestResults("verify  New Purchase Order page","view  New Purchase Order page", " New Purchase Order page is not display", "fail");
	}
	
	driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
	click(btn_PbtnVendor);
	sendKeys(txt_txtVendor, txtVendor);
	Thread.sleep(3000);
	pressEnter(txt_txtVendor);
	Thread.sleep(3000);
	doubleClick(sel_selVendor);
	
	click(btn_btnBillingAddress);
	sendKeys(txt_txtBillingAddress, txtBillingAddress);
	click(btn_btnApply);
	click(btn_btnShippingAddress);
	sendKeys(txt_txtShippingAddress, txtShippingAddress);
	click(btn_btnApply);
	click(btn_Pbtndetails);
	click(btn_btnPInvoiceAccount);
	
	driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
	sendKeys(txt_txtVendor, txtVendor);
	Thread.sleep(3000);
	pressEnter(txt_txtVendor);
	Thread.sleep(3000);
	doubleClick(sel_selVendor);
	click(btn_btnsummary);
	
	Thread.sleep(3000);
	click(btn_pbtnproduct);
	Thread.sleep(3000);
	sendKeys(txt_ptxtproduct,pro7.readTestCreation("BatchSpecific"));
	Thread.sleep(3000);
	pressEnter(txt_ptxtproduct);
	Thread.sleep(3000);
	doubleClick(sel_pselproduct);
	Thread.sleep(3000);
	sendKeys(txt_pro7POqty1, TestQuantity);
	
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Order Document Draft","Purchase Order Document Draft", "Purchase Order Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Order Document Draft","Purchase Order Document Draft", "Purchase Order Document is not Draft", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	PurchaseOrderNo=trackCode;
	Thread.sleep(5000);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify Purchase Order Document Released","Purchase Order Document Released", "Purchase Order Document is Released", "pass");
	} else {
		writeTestResults("verify Purchase Order Document Released","Purchase Order Document Released", "Purchase Order Document is not Released", "fail");
	}
	Thread.sleep(3000);
	click(btn_btnGotopage);
	
	switchWindow();
	if(isDisplayed(txt_InboundShipmentpage)) {
		writeTestResults("User enter Inbound Shipment page","view  Inbound Shipment page", "Inbound Shipment page is display", "pass");
	}else {
		writeTestResults("User enter Inbound Shipment page","view  Inbound Shipment page", "Inbound Shipment page is not display", "fail");
	}
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Inbound Shipment Document Draft","Inbound Shipment Document Draft", "Inbound Shipment Document is Draft", "pass");
	} else {
		writeTestResults("verify Inbound Shipment Document Draft","Inbound Shipment Document Draft", "Inbound Shipment Document is not Draft", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_Delete);
	
	Thread.sleep(3000);
	if(click(btn_No)==true) {
		writeTestResults("verify Delete Button","Working Delete  Button", "Delete  Button is Working", "pass");
	}else {
		writeTestResults("verify Delete Button","Working Delete  Button", "Delete  Button is not Working", "fail");
	}
	Thread.sleep(3000);
	pageRefersh();
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_History);
	Thread.sleep(3000);
	if(isDisplayed(txt_HistoryText)) {
		writeTestResults("verify History Button","Working History Button", "History Button is Working", "pass");
	}else {
		writeTestResults("verify History Button","Working History Button", "History Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	pageRefersh();
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_Reminders);
	Thread.sleep(3000);
	if(isDisplayed(txt_RemindersText)) {
		writeTestResults("verify Reminders Button","Working Reminders Button", "Reminders Button is Working", "pass");
	}else {
		writeTestResults("verify Reminders Button","Working Reminders Button", "Reminders Button is not Working", "fail");
	}
	Thread.sleep(3000);
	pageRefersh();

	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_Activities);
	Thread.sleep(3000);
	if(isDisplayed(txt_ActivitiesText)) {
		writeTestResults("verify Activities Button","Working Activities Button", "Activities Button is Working", "pass");
	}else {
		writeTestResults("verify Activities Button","Working Activities Button", "Activities Button is not Working", "fail");
	}
	Thread.sleep(3000);
	pageRefersh();
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_DocFlow);
	Thread.sleep(3000);
	if(isDisplayed(ZoomBar)) {
		writeTestResults("verify DocFlow Button","Working DocFlow Button", "DocFlow Button is Working", "pass");
	}else {
		writeTestResults("verify DocFlow Button","Working DocFlow Button", "DocFlow Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	if(click(btn_btnSerialBatch)==true) {
		writeTestResults("verify Serial Batch Button","Working Serial Batch  Button", "Serial Batch  Button is Working", "pass");
	}else {
		writeTestResults("verify Serial Batch Button","Working Serial Batch  Button", "Serial Batch  Button is not Working", "fail");
	}
	
	Thread.sleep(4000);
	Actions action1 = new Actions(driver);
	WebElement a1 = driver.findElement(By.xpath(Cap1));
	action1.moveToElement(a1).build().perform();
	Thread.sleep(3000);
	click(Cap1);
	Thread.sleep(3000);
	LocalTime p1 = LocalTime.now();
	sendKeys(BatchNu, "BN"+p1);
	Thread.sleep(3000);
	sendKeys(BatchLotNu, "LN"+p1);
	Thread.sleep(3000);
	click(BatchExpiryDate);
	Thread.sleep(3000);
	selectText(nextdate, "2030");
	Thread.sleep(3000);
	click(BatchExpiryDateSel);
	Thread.sleep(3000);
	pressEnter(BatchNu);
	Thread.sleep(3000);
	click(btn_updateSerial);
	
	Thread.sleep(3000);
	click(btn_closeProductList);
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_Delete);
	
	Thread.sleep(3000);
	if(click(btn_No)==true) {
		writeTestResults("verify Delete Button","Working Delete  Button", "Delete  Button is Working", "pass");
	}else {
		writeTestResults("verify Delete Button","Working Delete  Button", "Delete  Button is not Working", "fail");
	}
	Thread.sleep(3000);
	pageRefersh();
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_History);
	Thread.sleep(3000);
	if(isDisplayed(txt_HistoryText)) {
		writeTestResults("verify History Button","Working History Button", "History Button is Working", "pass");
	}else {
		writeTestResults("verify History Button","Working History Button", "History Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	pageRefersh();
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_Reminders);
	Thread.sleep(3000);
	if(isDisplayed(txt_RemindersText)) {
		writeTestResults("verify Reminders Button","Working Reminders Button", "Reminders Button is Working", "pass");
	}else {
		writeTestResults("verify Reminders Button","Working Reminders Button", "Reminders Button is not Working", "fail");
	}
	Thread.sleep(3000);
	pageRefersh();

	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_Activities);
	Thread.sleep(3000);
	if(isDisplayed(txt_ActivitiesText)) {
		writeTestResults("verify Activities Button","Working Activities Button", "Activities Button is Working", "pass");
	}else {
		writeTestResults("verify Activities Button","Working Activities Button", "Activities Button is not Working", "fail");
	}
	Thread.sleep(3000);
	pageRefersh();
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_DocFlow);
	Thread.sleep(3000);
	if(isDisplayed(ZoomBar)) {
		writeTestResults("verify DocFlow Button","Working DocFlow Button", "DocFlow Button is Working", "pass");
	}else {
		writeTestResults("verify DocFlow Button","Working DocFlow Button", "DocFlow Button is not Working", "fail");
	}
	
	
	Thread.sleep(3000);
	click(btn_Edit);
	Thread.sleep(3000);
	if(isDisplayed(btn_btnUpdate)) {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is Working", "pass");
	}else {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is not Working", "fail");
	}
	
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_Update);
	Thread.sleep(3000);
	if(isDisplayed(btn_btnEdit)) {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is Working", "pass");
	}else {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is not Working", "fail");
	}

	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	InboundShipmentNo=trackCode;
	Thread.sleep(5000);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify enter Inbound Shipment Document Released","enter Inbound Shipment Document Released", "enter Inbound Shipment Document is Released", "pass");
	} else {
		writeTestResults("verify enter Inbound Shipment Document Released","enter Inbound Shipment Document Released", "enter Inbound Shipment Document is not Released", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnGotopage);

	switchWindow();
	if(isDisplayed(txt_PurchaseInvoicepage)) {
		writeTestResults("User enter Purchase Invoice page","view  Purchase Invoice page", "Purchase Invoice page is display", "pass");
	}else {
		writeTestResults("User enter Purchase Invoice page","view  Purchase Invoice page", "Purchase Invoice page is not display", "fail");
	}
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Invoice Document Draft","Purchase Invoice Document Draft", "Purchase Invoice Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Invoice Document Draft","Purchase Invoice Document Draft", "Purchase Invoice Document is not Draft", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_Delete);
	
	Thread.sleep(3000);
	if(click(btn_No)==true) {
		writeTestResults("verify Delete Button","Working Delete  Button", "Delete  Button is Working", "pass");
	}else {
		writeTestResults("verify Delete Button","Working Delete  Button", "Delete  Button is not Working", "fail");
	}
	Thread.sleep(3000);
	pageRefersh();
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_History);
	Thread.sleep(3000);
	if(isDisplayed(txt_HistoryText)) {
		writeTestResults("verify History Button","Working History Button", "History Button is Working", "pass");
	}else {
		writeTestResults("verify History Button","Working History Button", "History Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	pageRefersh();
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_Reminders);
	Thread.sleep(3000);
	if(isDisplayed(txt_RemindersText)) {
		writeTestResults("verify Reminders Button","Working Reminders Button", "Reminders Button is Working", "pass");
	}else {
		writeTestResults("verify Reminders Button","Working Reminders Button", "Reminders Button is not Working", "fail");
	}
	Thread.sleep(3000);
	pageRefersh();

	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_Activities);
	Thread.sleep(3000);
	if(isDisplayed(txt_ActivitiesText)) {
		writeTestResults("verify Activities Button","Working Activities Button", "Activities Button is Working", "pass");
	}else {
		writeTestResults("verify Activities Button","Working Activities Button", "Activities Button is not Working", "fail");
	}
	Thread.sleep(3000);
	pageRefersh();
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_DocFlow);
	Thread.sleep(3000);
	if(isDisplayed(ZoomBar)) {
		writeTestResults("verify DocFlow Button","Working DocFlow Button", "DocFlow Button is Working", "pass");
	}else {
		writeTestResults("verify DocFlow Button","Working DocFlow Button", "DocFlow Button is not Working", "fail");
	}
	
	
	Thread.sleep(3000);
	click(btn_Edit);
	Thread.sleep(3000);
	if(isDisplayed(btn_btnUpdate)) {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is Working", "pass");
	}else {
		writeTestResults("verify Edit Button","Working Edit Button", "Edit Button is not Working", "fail");
	}
	
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_Update);
	Thread.sleep(3000);
	if(isDisplayed(btn_btnEdit)) {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is Working", "pass");
	}else {
		writeTestResults("verify Update Button","Working Update Button", "Update Button is not Working", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	PurchaseInvoiceNo=trackCode;
	Thread.sleep(5000);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify Purchase Invoice Document Released","Purchase Invoice Document Released", "Purchase Invoice Document is Released", "pass");
	} else {
		writeTestResults("verify Purchase Invoice Document Released","Purchase Invoice Document Released", "Purchase Invoice Document is not Released", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnGotopage);
	
	switchWindow();
	if(isDisplayed(txt_ShipmentCostingInformationpage)) {
		writeTestResults("User enter Shipment Costing Information page","view  Shipment Costing Information page", "Shipment Costing Information page is display", "pass");
	}else {
		writeTestResults("User enter Shipment Costing Information page","view  Shipment Costing Information page", "Shipment Costing Information is not display", "fail");
	}
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Shipment Costing Information Document Draft","Shipment Costing Information Document Draft", "Shipment Costing Information Document is Draft", "pass");
	} else {
		writeTestResults("verify Shipment Costing Information Document Draft","Shipment Costing Information Document Draft", "Shipment Costing Information Document is not Draft", "fail");

	}
	
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	ShipmentCostingInformationNo=trackCode;
	Thread.sleep(5000);
	if (isDisplayed(pageRelease)){

		writeTestResults("verify Shipment Costing Information Document Released","Shipment Costing Information Document Released", "Shipment Costing Information Document is Released", "pass");
	} else {
		writeTestResults("verify Shipment Costing Information Document Released","Shipment Costing Information Document Released", "Shipment Costing Information Document is not Released", "fail");

	}
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_Reverse);
	Thread.sleep(3000);
	click(btn_ReverseButton);
	Thread.sleep(3000);
	click(btn_Yes);
	Thread.sleep(3000);
	if(getText(txt_ReverseText).contentEquals("(Reversed)")) {
		writeTestResults("verify Reversed Button","Working Reversed Button", "Reversed Button is Working", "pass");
	}else {
		writeTestResults("verify Reversed Button","Working Reversed Button", "Reversed Button is not Working", "fail");
	}
}

public void Master4() throws Exception{
	
	navigateToTheLoginPage();
	verifyTheLogo();
	userLogin();
	clickNavigation();
	clickProbutton();
	Thread.sleep(3000);
	click(btn_CustomerDemand);
	Thread.sleep(6000);
	click(btn_CustomerDemandsel);
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_GenerateProductionOrder);
	Thread.sleep(3000);
	if(isDisplayed(txt_GenerateProductionOrdertxt)) {
		writeTestResults("verify Generate Production Order Button","Working Generate Production Order Button", "Generate Production Order Button is Working", "pass");
	}else {
		writeTestResults("verify Generate Production Order Button","Working Generate Production Order Button", "Generate Production Order Button is not Working", "fail");
	}
	Thread.sleep(3000);
	click(btn_headerclose);
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_GenerateAssemblyProposal);
	Thread.sleep(3000);
	if(isDisplayed(txt_GenerateAssemblyProposaltxt)) {
		writeTestResults("verify Generate Assembly Proposal Button","Working Generate Assembly Proposal Button", "Generate Assembly Proposal Button is Working", "pass");
	}else {
		writeTestResults("verify Generate Assembly Proposal Button","Working Generate Assembly Proposal Button", "Generate Assembly Proposal Button is not Working", "fail");
	}
}

public void Transaction8() throws Exception{
	
	navigateToTheLoginPage();
	verifyTheLogo();
	userLogin();
	clickNavigation();
	clickProbutton();

	Thread.sleep(3000);
	click(btn_PurchaseDesk);
	Thread.sleep(3000);
	if(isDisplayed(PurchaseDeskpage)) {
		writeTestResults("verify Purchase Desk page","view Purchase Desk page", "Purchase Desk page is display", "pass");
	}else {
		writeTestResults("verify Purchase Desk page","view Purchase Desk page", "Purchase Desk page is not display", "fail");
	}
	


	Thread.sleep(10000);
	if(click(sel_Requisitionsel)==true) {
		writeTestResults("Verify User Select the Requisition in the Purchase Desk","User Can Select the Requisition in the Purchase Desk", "User Can Select the Requisition", "pass");
	}else {
		writeTestResults("Verify User Select the Requisition in the Purchase Desk","User Can Select the Requisition in the Purchase Desk", "User Cannot Select the Requisition", "fail");
	}	
	
	Thread.sleep(3000);
	click(btn_btnAction);
	Thread.sleep(3000);
	click(btn_GeneratePurchaseOrder);
	Thread.sleep(3000);
	if(isDisplayed(btn_btnPOtoShipmentCosting)) {
		writeTestResults("verify Generate Purchase Order Button","Working Generate Purchase Order Button", "Generate Purchase Order Button is Working", "pass");
	}else {
		writeTestResults("verify Generate Purchase Order Button","Working Generate Purchase Order Button", "Generate Purchase Order Button is not Working", "fail");
	}
	
}

//============================================================================================================

public void Master001() throws Exception{

	navigateToTheLoginPage();
	verifyTheLogo();
	userLogin();
	clickNavigation();
	clickProbutton();
	
	click(btn_SalesAndMarketing);
	click(btn_AccountGroupConfiguration);
	click(btn_NewAccountGroupConfiguration);
	
	LocalTime Obj = LocalTime.now();
	Thread.sleep(3000);
	click(btn_addAccountGroup);
	Thread.sleep(3000);
	click(btn_addNewAccountGroup);
	Thread.sleep(3000);
	sendKeys(txt_NewAccountGrouptxt, txtsubject+Obj);
	Thread.sleep(3000);
	click(btn_AccountGroupUpdate);
	
	Thread.sleep(3000);
	selectText(sel_AccountGroupsel, txtsubject+Obj);
	
	Thread.sleep(1000);
	WriteData(2, 1, txtsubject+Obj);
	
	Thread.sleep(4000);
	click(btn_btndraft);
	
	Thread.sleep(4000);
	click(btn_release);
	
	Thread.sleep(4000);
	clickNavigation();
	clickProbutton();
	newVendor();
	
	selectText(txt_VendorType, VendorType);
	selectText(txt_VendorNameTitle, VendorNameTitle);
	sendKeys(txt_VendorName, VendorName+Obj);
	selectText(txt_currency,currency);
	selectIndex(txt_vendorgroup, 1);
	click(btn_ReceivableAccount);
	click(btn_newReceivableAccount);
	sendKeys(txt_newReceivableAccountname, newReceivableAccountname+Obj);
	driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
	number = newReceivableAccountname+Obj;
	
	Thread.sleep(1000);
	String a = ReadData(2, 1);
	Thread.sleep(3000);
	selectText(txt_AccountGroup, a);
	Thread.sleep(3000);
	click(btn_btnUpdate);
	JavascriptExecutor jse = (JavascriptExecutor) driver;
	jse.executeScript("$(closeMessageBox()).click()");
	
	Thread.sleep(6000);
	sendKeys(txt_ReceivableAccount, number);
	Thread.sleep(3000);
	pressEnter(txt_ReceivableAccount);
	Thread.sleep(3000);
	doubleClick(sel_ReceivableAccount);
	
	Thread.sleep(1000);
	WriteData(1, 1, VendorName+Obj);
	
	selectText(txt_ContactTitle, ContactTitle);
	sendKeys(txt_ContactName, ContactName);
	sendKeys(txt_MobileNumber,MobileNumber);
	click(btn_btndetails);
	selectText(txt_SelectIndustry,SelectIndustry);
	sendKeys(txt_TaxRegistrationNumber, TaxRegistrationNumber);
	sendKeys(txt_ReferenceAccoutNo, ReferenceAccoutNo);

	selectText(txt_PurchaseTaxGroup, PurchaseTaxGroup);
	sendKeys(txt_PassportNo, PassportNo);
	sendKeys(txt_CreditDays, CreditDays);
	sendKeys(txt_CreditLimit, CreditLimit);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(5000);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify whether system allows user to draft vendor information","system allows user to draft vendor information", "vendor information is Draft", "pass");
	} else {
		writeTestResults("verify whether system allows user to draft vendor information","system allows user to draft vendor information", "vendor information is Not Draft", "fail");

	}
	
	number = getText(txt_newVendorName);
	Thread.sleep(3000);
	click("//a[contains(text(),'Vendor Information')]");
	
	driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
	sendKeys(txt_VendorInformationserch, number);
	pressEnter(txt_VendorInformationserch);
	Thread.sleep(3000);
	click(sel_selVendorInformation.replace("no", number));
	
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	Thread.sleep(5000);
	if (isDisplayed(pageRelease)){

		writeTestResults("verify whether system allows user to Release vendor information","system allows user to Release vendor information", "vendor information is Released", "pass");
	} else {
		writeTestResults("verify whether system allows user to Release vendor information","system allows user to Release vendor information", "vendor information is Not Released", "fail");

	}
	
	Thread.sleep(4000);
	clickNavigation();
	click(btn_InventoryAndWarehousing);
	click(btn_WarehouseInformation);
	click(btn_NewWarehouse);

	Thread.sleep(3000);
	sendKeys(txt_WarehouseCode, WarehouseCode+Obj);
	Thread.sleep(3000);
	sendKeys(txt_WarehouseName, WarehouseName+Obj);
	Thread.sleep(1000);
	WriteData(3, 1, WarehouseCode+Obj);
	Thread.sleep(3000);
	selectIndex(txt_QuaranWarehouse, 1);
	Thread.sleep(3000);
	click(btn_AutoLot);
	Thread.sleep(3000);
	selectIndex(txt_LotBookNo, 1);
	
	Thread.sleep(5000);
	click(btn_btndraft);
	Thread.sleep(5000);
	click(btn_release);
	
	clickNavigation();
	clickProbutton();
	click(VendorAgreementbutton);
	if(isDisplayed(VendorAgreementpage)) {
		writeTestResults("verify Vendor Agreement page","view Vendor Agreement page", "Vendor Agreement page is display", "pass");
	}else {
		writeTestResults("verify Vendor Agreement page","view Vendor Agreement page", "Vendor Agreement page is not display", "fail");
	}
	click(newVendorAgreementbutton);
	if(isDisplayed(newVendorAgreementpage)&&(getText(Header).contentEquals("New"))) {
		writeTestResults("verify New Vendor Agreement page","view New Vendor Agreement page", "New Vendor Agreement page is display", "pass");
	}else {
		writeTestResults("verify New Vendor Agreement page","view New Vendor Agreement page", "New Vendor Agreement page is not display", "fail");
	}
	LocalTime myObj = LocalTime.now();
	sendKeys(txt_AgreementNo, AgreementNo+myObj);
	sendKeys(txt_AgreementTitle, AgreementTitle);
	click(btn_btnAgreementDate);
	click(btn_selAgreementDate);
	Thread.sleep(3000);
	click(btn_AgreementGroupAdd);
	Thread.sleep(3000);
	click(btn_AgreementGroupParameters);
	Thread.sleep(3000);
	sendKeys(txt_AgreementGroupParameterstxt, AgreementGroupParameterstxt+myObj);
	Thread.sleep(1000);
	WriteData(4, 1, AgreementGroupParameterstxt+myObj);
	click(btn_AgreementGroupParametersUpdate);
	
	selectText(txt_AgreementGroup, AgreementGroupParameterstxt+myObj);
}

public void WriteData(int x,int y,String z) throws Exception{
	
	File f1= new File("D:\\bitbucket project\\bileetaautomation\\BTAF\\ProcrumentData\\ProcrumentData.xlsx");
	FileInputStream fi1 = new FileInputStream(f1);
	XSSFWorkbook w1 = new XSSFWorkbook(fi1);
	XSSFSheet s1 = w1.getSheetAt(0);
	
	s1.getRow(x).createCell(y).setCellValue(z);
	
	FileOutputStream fo1 = new FileOutputStream(f1);
	w1.write(fo1);
}

public String ReadData(int x,int y) throws Exception{
	
	File f1= new File("D:\\bitbucket project\\bileetaautomation\\BTAF\\ProcrumentData\\ProcrumentData.xlsx");
	FileInputStream fi1 = new FileInputStream(f1);
	XSSFWorkbook w1 = new XSSFWorkbook(fi1);
	XSSFSheet s1 = w1.getSheetAt(0);
	
	String Result = s1.getRow(x).getCell(y).getStringCellValue();
	return Result;
}

public void PurchaseOrderForInventory64()  throws Exception {
	
	clickProbutton();
	
	click(btn_PurchaseOrderbutton);
	if(isDisplayed(txt_PurchaseOrderpage)) {
		writeTestResults("verify Purchase Order page","view Purchase Order page", "Purchase Order page is display", "pass");
	}else {
		writeTestResults("verify Purchase Order page","view Purchase Order page", "Purchase Order page is not display", "fail");
	}
	click(btn_newPurchaseOrderbutton);
	
	Thread.sleep(3000);
	click(btn_btnPOtoPurchaseInvoice);
	if(isDisplayed(txt_newPurchaseOrderpage)) {
		writeTestResults("verify  New Purchase Order page","view  New Purchase Order page", " New Purchase Order page is display", "pass");
	}else {
		writeTestResults("verify  New Purchase Order page","view  New Purchase Order page", " New Purchase Order page is not display", "fail");
	}
	Thread.sleep(3000);
	click(btn_PbtnVendor);
	Thread.sleep(3000);
	sendKeys(txt_txtVendor, txtVendor);
	Thread.sleep(3000);
	pressEnter(txt_txtVendor);
	Thread.sleep(3000);
	doubleClick(sel_selVendor);
	
	click(btn_btnBillingAddress);
	sendKeys(txt_txtBillingAddress, txtBillingAddress);
	click(btn_btnApply);
	click(btn_btnShippingAddress);
	sendKeys(txt_txtShippingAddress, txtShippingAddress);
	click(btn_btnApply);
	click(btn_Pbtndetails);
	click(btn_btnPInvoiceAccount);
	
	Thread.sleep(3000);
	sendKeys(txt_txtVendor, txtVendor);
	Thread.sleep(3000);
	pressEnter(txt_txtVendor);
	Thread.sleep(3000);
	doubleClick(sel_selVendor);
	click(btn_btnsummary);
	
	Thread.sleep(3000);
	click(btn_pbtnproduct);
	Thread.sleep(3000);
	sendKeys(txt_ptxtproduct,"INMOD64");
	Thread.sleep(3000);
	pressEnter(txt_ptxtproduct);
	Thread.sleep(3000);
	doubleClick(sel_pselproduct);
	Thread.sleep(3000);
	selectText(Warehouse1, "WH-PARTS [Main Warehouse - Parts]");
	Thread.sleep(3000);
	sendKeys(pro7qty1, "20");
	Thread.sleep(3000);
	sendKeys(pro7price1, "100");
	
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	PurchaseOrderNo=trackCode;
	Thread.sleep(5000);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify Purchase Order Document Released","Purchase Order Document Released", "Purchase Order Document is Released", "pass");
	} else {
		writeTestResults("verify Purchase Order Document Released","Purchase Order Document Released", "Purchase Order Document is not Released", "fail");
	}
	Thread.sleep(3000);
	click(btn_btnGotopage);
	
	switchWindow();
	if(isDisplayed(txt_InboundShipmentpage)) {
		writeTestResults("User enter Inbound Shipment page","view  Inbound Shipment page", "Inbound Shipment page is display", "pass");
	}else {
		writeTestResults("User enter Inbound Shipment page","view  Inbound Shipment page", "Inbound Shipment page is not display", "fail");
	}
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	
	
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	Thread.sleep(2000);
	InboundShipmentNo=trackCode;
	Thread.sleep(5000);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify enter Inbound Shipment Document Released","enter Inbound Shipment Document Released", "enter Inbound Shipment Document is Released", "pass");
	} else {
		writeTestResults("verify enter Inbound Shipment Document Released","enter Inbound Shipment Document Released", "enter Inbound Shipment Document is not Released", "fail");
	}
	
	Thread.sleep(3000);
	click(btn_btnGotopage);

	switchWindow();
	if(isDisplayed(txt_PurchaseInvoicepage)) {
		writeTestResults("User enter Purchase Invoice page","view  Purchase Invoice page", "Purchase Invoice page is display", "pass");
	}else {
		writeTestResults("User enter Purchase Invoice page","view  Purchase Invoice page", "Purchase Invoice page is not display", "fail");
	}
	click(btn_btnCheckout);
	Thread.sleep(3000);
	click(btn_btndraft);
	Thread.sleep(3000);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	PurchaseInvoiceNo=trackCode;
	Thread.sleep(5000);
	if (isDisplayed(pageRelease)){

		writeTestResults("verify Purchase Invoice Document Released","Purchase Invoice Document Released", "Purchase Invoice Document is Released", "pass");
	} else {
		writeTestResults("verify Purchase Invoice Document Released","Purchase Invoice Document Released", "Purchase Invoice Document is not Released", "fail");
	}
}

public void Product4PurchaseOrder()  throws Exception {
	
	navigateToTheLoginPage();
	verifyTheLogo();
	userLogin();
	clickNavigation();
	clickProbutton();
	
	WaitClick(btn_PurchaseOrderbutton);
	click(btn_PurchaseOrderbutton);
	WaitElement(txt_PurchaseOrderpage);
	if(isDisplayed(txt_PurchaseOrderpage)) {
		writeTestResults("verify Purchase Order page","view Purchase Order page", "Purchase Order page is display", "pass");
	}else {
		writeTestResults("verify Purchase Order page","view Purchase Order page", "Purchase Order page is not display", "fail");
	}
	
	WaitClick(btn_newPurchaseOrderbutton);
	click(btn_newPurchaseOrderbutton);
	
	WaitClick(btn_btnPOtoPurchaseInvoice);
	click(btn_btnPOtoPurchaseInvoice);
	WaitElement(txt_newPurchaseOrderpage);
	if(isDisplayed(txt_newPurchaseOrderpage)) {
		writeTestResults("verify  New Purchase Order page","view  New Purchase Order page", " New Purchase Order page is display", "pass");
	}else {
		writeTestResults("verify  New Purchase Order page","view  New Purchase Order page", " New Purchase Order page is not display", "fail");
	}
	
	WaitClick(btn_PbtnVendor);
	click(btn_PbtnVendor);
	WaitElement(txt_txtVendor);
	sendKeys(txt_txtVendor, txtVendor);
	pressEnter(txt_txtVendor);
	Thread.sleep(3000);
	doubleClick(sel_selVendor);
	
	click(btn_btnBillingAddress);
	sendKeys(txt_txtBillingAddress, txtBillingAddress);
	click(btn_btnApply);
	click(btn_btnShippingAddress);
	sendKeys(txt_txtShippingAddress, txtShippingAddress);
	click(btn_btnApply);
	click(btn_Pbtndetails);
	click(btn_btnPInvoiceAccount);
	
	WaitElement(txt_txtVendor);
	sendKeys(txt_txtVendor, txtVendor);
	pressEnter(txt_txtVendor);
	WaitElement(sel_selVendor);
	Thread.sleep(3000);
	doubleClick(sel_selVendor);
	
	WaitElement(btn_btnsummary);
	click(btn_btnsummary);
	
	ScrollDownAruna();
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("BatchSpecific"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, "100");
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse1, "WH-PARTS [Main Warehouse - Parts]");
	sendKeys(pro7price1, "100");
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("BatchFifo"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, "100");
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse2, "WH-PARTS [Main Warehouse - Parts]");
	sendKeys(pro7price2, "100");
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("Lot"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, "100");
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse3, "WH-PARTS [Main Warehouse - Parts]");
	sendKeys(pro7price3, "100");
	
	WaitElement(Product);
	sendKeys(Product, pro7.readTestCreation("SerielSpecific"));
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, "100");
	Thread.sleep(4000);
	pressEnter(ProductQty);
	selectText(Warehouse4, "WH-PARTS [Main Warehouse - Parts]");
	sendKeys(pro7price4, "100");
	
	WaitElement(btn_btnCheckout);
	WaitClick(btn_btnCheckout);
	click(btn_btnCheckout);
	
	WaitClick(btn_btndraft);
	click(btn_btndraft);
	
	WaitClick(btn_release);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	PurchaseOrderNo=trackCode;
	
	WaitElement(btn_btnGotopage);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify Purchase Order Document Released","Purchase Order Document Released", "Purchase Order Document is Released", "pass");
	} else {
		writeTestResults("verify Purchase Order Document Released","Purchase Order Document Released", "Purchase Order Document is not Released", "fail");
	}
	WaitClick(btn_btnGotopage);
	click(btn_btnGotopage);
	
	switchWindow();
	if(isDisplayed(txt_InboundShipmentpage)) {
		writeTestResults("User enter Inbound Shipment page","view  Inbound Shipment page", "Inbound Shipment page is display", "pass");
	}else {
		writeTestResults("User enter Inbound Shipment page","view  Inbound Shipment page", "Inbound Shipment page is not display", "fail");
	}
	
	WaitElement(btn_btnCheckout);
	WaitClick(btn_btnCheckout);
	click(btn_btnCheckout);
	
	WaitClick(btn_btndraft);
	click(btn_btndraft);
	
	Thread.sleep(3000);
	click(btn_btnSerialBatch);
	
	Thread.sleep(6000);
	Actions action1 = new Actions(driver);
	WebElement a1 = driver.findElement(By.xpath(Cap1));
	action1.moveToElement(a1).build().perform();	
	WaitClick(Cap1);
	click(Cap1);
	LocalTime p1 = LocalTime.now();
	sendKeys(BatchNu, "BN"+p1);
	sendKeys(BatchLotNu, "LN"+p1);
	WaitClick(BatchExpiryDate);
	click(BatchExpiryDate);
	selectText(nextdate, "2030");
	WaitClick(BatchExpiryDateSel);
	click(BatchExpiryDateSel);
	Thread.sleep(3000);
	pressEnter(BatchNu);
	WaitClick(btn_updateSerial);
	click(btn_updateSerial);
	

	Actions action2 = new Actions(driver);
	WebElement a2 = driver.findElement(By.xpath(Cap2));
	action2.moveToElement(a2).build().perform();
	WaitClick(Cap2);
	click(Cap2);
	LocalTime p2 = LocalTime.now();
	sendKeys(BatchNu, "BN"+p2);
	sendKeys(BatchLotNu, "LN"+p2);
	WaitClick(BatchExpiryDate);
	click(BatchExpiryDate);
	selectText(nextdate, "2030");
	WaitClick(BatchExpiryDateSel);
	click(BatchExpiryDateSel);
	Thread.sleep(3000);
	pressEnter(BatchNu);
	WaitClick(btn_updateSerial);
	click(btn_updateSerial);
	
	Actions action4 = new Actions(driver);
	WebElement a4 = driver.findElement(By.xpath(Cap4));
	action4.moveToElement(a4).build().perform();
	WaitClick(Cap4);
	click(Cap4);
	WaitClick(btn_SerialRange);
	click(btn_SerialRange);
	Thread.sleep(3000);
	JavascriptExecutor j1 = (JavascriptExecutor)driver;
	j1.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",100)");
	LocalTime p3 = LocalTime.now();
	sendKeys(SerialFromNu, "SN"+p3);
	sendKeys(SerialLotNu, "LN"+p3);
	WaitClick(SerialExpiryDate);
	click(SerialExpiryDate);
	selectText(nextdate, "2030");
	WaitClick(BatchExpiryDateSel);
	click(BatchExpiryDateSel);
	Thread.sleep(3000);
	pressEnter(SerialFromNu);
	WaitClick(btn_updateSerial);
	click(btn_updateSerial);
	
	WaitClick(btn_closeProductList);
	click(btn_closeProductList);
	
	WaitClick(btn_release);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	InboundShipmentNo=trackCode;
	WaitElement(btn_btnGotopage);
	if (isDisplayed(btn_btnGotopage)){

		writeTestResults("verify enter Inbound Shipment Document Released","enter Inbound Shipment Document Released", "enter Inbound Shipment Document is Released", "pass");
	} else {
		writeTestResults("verify enter Inbound Shipment Document Released","enter Inbound Shipment Document Released", "enter Inbound Shipment Document is not Released", "fail");
	}
	
	WaitClick(btn_btnGotopage);
	click(btn_btnGotopage);

	switchWindow();
	if(isDisplayed(txt_PurchaseInvoicepage)) {
		writeTestResults("User enter Purchase Invoice page","view  Purchase Invoice page", "Purchase Invoice page is display", "pass");
	}else {
		writeTestResults("User enter Purchase Invoice page","view  Purchase Invoice page", "Purchase Invoice page is not display", "fail");
	}
	
	WaitElement(btn_btnCheckout);
	WaitClick(btn_btnCheckout);
	click(btn_btnCheckout);
	WaitClick(btn_btndraft);
	click(btn_btndraft);
	
	Thread.sleep(4000);
	click(btn_release);
	Thread.sleep(2000);
	trackCode= getText(Header);
	PurchaseInvoiceNo=trackCode;
	WaitElement(pageRelease);
	if (isDisplayed(pageRelease)){

		writeTestResults("verify Purchase Invoice Document Released","Purchase Invoice Document Released", "Purchase Invoice Document is Released", "pass");
		System.out.println("Pro4PO Test case is Completed");
	} else {
		writeTestResults("verify Purchase Invoice Document Released","Purchase Invoice Document Released", "Purchase Invoice Document is not Released", "fail");
	}
	
}

//scrollDown
public void ScrollDownAruna() throws Exception {
	JavascriptExecutor jse = (JavascriptExecutor)driver;
	jse.executeScript("window.scrollBy(0,400)");
}

//WaitClick
public void WaitClick(String Locator)  throws Exception {
	
	WebDriverWait wait = new WebDriverWait(driver,60);
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Locator)));
}

//WaitElement
public void WaitElement(String Locator)  throws Exception {
	
	WebDriverWait wait = new WebDriverWait(driver,60);
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator)));
}
}
