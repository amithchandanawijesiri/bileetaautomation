package bileeta.BTAF.PageObjects;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import bileeta.BATF.Pages.ProductionModuleData;

public class ProductionModule extends ProductionModuleData {

	public static String productionOrder;

	public static String unit;

	public static String locationCode;

	public static String BOONO;

	public static String bulkNo;

	public static String product;

	public void navigateToTheLoginPage() throws Exception {
		openPage(siteURL);

		writeTestResults("Verify that 'Entution' header is available on the page",
				"user should be able to see the logo", "logo is dislayed", "pass");

	}

	public void verifyTheLogo() throws Exception {
		if (isDisplayed(siteLogo)) {

			writeTestResults("Verify that 'Entution' header is available on the page",
					"user should be able to see the logo", "logo is dislayed", "pass");
		} else {
			writeTestResults("Verify that 'Entution' header is available on the page",
					"user should be able to see the logo", "logo is not dislayed", "fail");

		}
	}

	public void userLogin() throws Exception {

		

		sendKeys(txt_username, UserNameData);

		sendKeys(txt_password, PasswordData);

		

		click(btn_login);

		if (isDisplayed(lnk_home)) {

			writeTestResults("User login", "User should navigate to the Home page of Entution",
					"user navigated to the home page", "pass");
		}

		else {
			writeTestResults("User login", "User should navigate to the Home page of Entution",
					"user cant navigate to the home page", "fail");

		}
	}

	public void navigateToSideBar() throws Exception {
		InventoryAndWarehouseModuleRegression obj = new InventoryAndWarehouseModuleRegression();
//		obj.activeQcAcceptance();
		click(navigateMenu);
		Thread.sleep(3000);
		if (isDisplayed(sideNavBar)) {

			writeTestResults("Verify that user can successfully navigate to side menu",
					"User successfully navigate to side menu", "Side menu displayed", "pass");

		} else {

			writeTestResults("Verify that user can successfully navigate to side menu",
					"User can't successfully navigate to side menu", "Side menu is not displayed", "fail");
		}
	}

	public void navigateToProductionMenu() throws Exception {

		click(btn_Production);
		Thread.sleep(3000);
		if (isDisplayed(logo_Production)) {

			writeTestResults("Verify that user can view fixed asset logo", "User can view the logo", "logo viewed",
					"pass");

		}

		else {

			writeTestResults("Verify that  user can view fixed asset logo", "User can't view the logo",
					"logo is not viewed", "fail");
		}

	}

	/* Con_TC_003 */

	// Verify that user can create a product level BOM

	public void navigateToInventory() throws Exception {
		Thread.sleep(3000);
		click(btn_inventory);
		Thread.sleep(3000);
		if (isDisplayed("//a[contains(text(),'Bill of Material')]")) {

			writeTestResults("Verify that user can view inventory label", "User can view the logo",
					"Inventory Label viewed", "pass");

		} else {

			writeTestResults("Verify that user can view inventory label", "User can view the logo",
					"Inventory Label doesn't viewed", "fail");

		}
	}

	public void navigateToBillOfMaterial() throws Exception {
		
		Thread.sleep(5000);
		click(btn_billOfMaterial);

		if (isDisplayed(label_billOfMaterial)) {

			writeTestResults("Verify that user can view bill of material logo", "User can view the logo",
					"Logo displayed", "pass");
		}

		else {

			writeTestResults("Verify that user can view bill of material logo", "User can view the logo",
					"Logo can't be displayed", "fail");
		}
	}

	public void navigateTonewBOM() throws Exception {

		click(btn_newBOM);

		if (isDisplayed(txt_newBOM)) {

			writeTestResults("Verify that the user can navigate to new BOM", "New bill of material page displayed",
					"new BOM page loaded", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to new BOM", "New bill of material page displayed",
					"new BOM page not loaded", "fail");
		}
	}

	public void fillingBOMForm() throws Exception {

		sendKeys(txt_desc, descData);

		selectText(btn_productGrp, productGrpData);

		// Select output Product
		click(btn_outputProductSearch);

		Thread.sleep(2000);

		// sendKeys(txt_outputProductSearch, outputProductData);
		sendKeys(txt_outputProductSearch, productCodeData);

		pressEnter(txt_outputProductSearch);

		Thread.sleep(2000);
		doubleClick(doubleClickoutputProduct);

		sendKeys(txt_BOMQuantity, BOMQuantityData);

		click(btn_expiryDate);

		click(clickExpiryDate);

		click(btn_rawMaterial);

		Thread.sleep(5000);

		sendKeys(txt_rawMaterial, rawMaterialData);

		Thread.sleep(5000);

		pressEnter(txt_rawMaterial);

		Thread.sleep(4000);

		doubleClick(doubleClickrawMaterial);

		sendKeys(txt_rawQuantity, rawQuantityData);

		if (isDisplayed(txt_newBOM)) {

			writeTestResults(
					"Verify that the user can navigate to new BOMUser selected the raw materials under the raw materials section should be displayed successfully",
					"raw materials section should be displayed", "raw materials section displayed", "pass");
		} else {

			writeTestResults(
					"User selected the raw materials under the raw materials section should be displayed successfully",
					"raw materials section shoul be displayed", "raw materials section not displayed", "fail");
		}

	}

	public void completeNreleaseBOM() throws Exception {

		click(btn_draft);

		click(btn_release);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(txt_BOM)) {

			writeTestResults("Verify that the user can release the bill of materials successfully",
					"bill of materials released", "bill of materials released", "pass");
		} else {

			writeTestResults("Verify that the user can release the bill of materials successfully",
					"bill of materials released", "bill of materials not released", "fail");
		}
	}

	/* Con_TC_004 */
	// Verify that user can create a group level BOM

	public void navigateToInventoryGroup() throws Exception {

		click(btn_inventory);

		if (isDisplayed(txt_inventory)) {

			writeTestResults("Verify that user can view inventory label", "User can view the logo", "Label viewed",
					"pass");

		} else {

			writeTestResults("Verify that user can view inventory label", "User can view the logo",
					"Label doesn't viewed", "fail");

		}
	}

	public void navigateToBillOfMaterialGroup() throws Exception {

		click(btn_billOfMaterial);

		if (isDisplayed(label_billOfMaterial)) {

			writeTestResults("Verify that user can view bill of material logo", "User can view the logo",
					"Logo displayed", "pass");
		}

		else {

			writeTestResults("Verify that user can view bill of material logo", "User can view the logo",
					"Logo can't be displayed", "fail");
		}
	}

	public void navigateTonewBOMGroup() throws Exception {

		click(btn_newBOM);

		if (isDisplayed(txt_newBOM)) {

			writeTestResults("Verify that the user can navigate to new BOM", "New bill of material page displayed",
					"new BOM page loaded", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to new BOM", "New bill of material page displayed",
					"new BOM page not loaded", "fail");
		}
	}

	public void fllingBOMFormGroup() throws Exception {

		sendKeys(txt_desc, descData);

		selectText(btn_productGrp, productGrpData);

		sendKeys(txt_BOMQuantity, BOMQuantityData);

		click(btn_expiryDate);

		click(clickExpiryDate);

		click(btn_rawMaterial);

		Thread.sleep(5000);

		sendKeys(txt_rawMaterial, rawMaterialData);

		Thread.sleep(5000);

		pressEnter(txt_rawMaterial);

		Thread.sleep(2000);

		doubleClick(doubleClickrawMaterial);

		sendKeys(txt_rawQuantity, rawQuantityData);

		if (isDisplayed(txt_newBOM)) {

			writeTestResults(
					"Selected the raw materials under the raw materials section should be displayed successfully",
					"New bill of material page displayed", "new BOM page loaded", "pass");
		} else {

			writeTestResults(
					"Selected the raw materials under the raw materials section should be displayed successfully",
					"New bill of material page displayed", "new BOM page not loaded", "fail");
		}
	}

	public void completeNreleaseBOMGroup() throws Exception {

		click(btn_draft);

		click(btn_release);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(txt_BOM)) {

			writeTestResults("Verify that the user can release the bill of materials successfully",
					"bill of materials released", "bill of materials released", "pass");
		} else {

			writeTestResults("Verify that the user can release the bill of materials successfully",
					"bill of materials released", "bill of materials not released", "fail");
		}
	}

	/* Con_TC_006 */

	// Verify whether user can fill summary data of bill of operations successully

	public void navigateToProduction() throws Exception {

		click(btn_production);
//
//		if (isDisplayed(txt_production)) {
//
//			writeTestResults("Verify that the user can navigate to Production page",
//					"The Bill of operations by page should be loaded successfully", "Production page loaded", "pass");
//		} else {
//
//			writeTestResults("Verify that the user can navigate to Production page",
//					"The Bill of operations by page should be loaded successfully", "Production page not loaded",
//					"fail");
//		}
	}

	public void navigateToBillOfOperation() throws Exception {

		click(btn_BOO);

		if (isDisplayed(txt_BOO)) {

			writeTestResults("Verify that the user can navigate to Bill of operation by page",
					"The New bill of operations page should be loaded successfully", "Bill Of Operation page loaded",
					"pass");
		} else {

			writeTestResults("Verify that the user can navigate to Bill of operation by page",
					"The New bill of operations page should be loaded successfully",
					"Bill Of Operation page not loaded", "fail");
		}
	}

	public void navigateTonewBOO() throws Exception {
		click(btn_newBOO);

		if (isDisplayed(txt_newBOO)) {

			writeTestResults("Verify that the user can navigate to new Bill of operation by page",
					"The New bill of operations page should be loaded successfully",
					"new Bill Of Operation page loaded", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to new Bill of operation by page",
					"The New bill of operations page should be loaded successfully",
					"new Bill Of Operation page not loaded", "fail");
		}
	}

	public void fillBOOForm() throws Exception {

		Random rand = new Random();
		int rand_int1 = rand.nextInt(100000000);

		BOOCodeData = BOOCodeData + Integer.toString(rand_int1);

		sendKeys(txt_BOOCode, BOOCodeData);

		// Description
		click(btn_descriptionBOO);

		sendKeys(txt_descrBOO, BOODescrData);

		click(btn_BOOApply);

		if (isDisplayed(txt_BOOLogo)) {

			writeTestResults("Verify that the user can navigate to Bill of operation by page",
					"The bill of operations page should be loaded successfully", "Bill Of Operation page loaded again",
					"pass");
		} else {

			writeTestResults("Verify that the user can navigate to Bill of operation by page",
					"The bill of operations page should be loaded successfully",
					"Bill Of Operation page not loaded again", "fail");
		}
	}

	/* Con_TC_007 */
	// Verify whether user can add initail operation as a supply under the bill of
	// operations details

	public void OperationActivitySupply() throws Exception {

		click(btn_plusBOOSupply);

		selectText(txt_ElementCategory, ElementCategoryData);

		sendKeys(txt_ElementDescription, ElementDescData);

//		click(btn_applyOperation);
		JavascriptExecutor j1 = (JavascriptExecutor)driver;
		j1.executeScript("$(\"#divOperation\").parent().find('.dialogbox-buttonarea > .button').click()");

		if (isDisplayed(label_operationActivity)) {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"Supply operation should be added successfully", "operation/activity page loaded again", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"Supply operation should be added successfully", "operation/activity page not loaded again",
					"fail");
		}

	}
	/* Con_TC_008 */
	// Verify whether user can add next operation as a production under the bill of
	// operations details

	public void OperationActivityOperation() throws Exception {

		click(btn_plusBOOOperation);

		selectIndex(txt_ElementCategoryOperation, 1);

		sendKeys(txt_GroupId, GroupIdData);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

//		click(btn_applyOperation);
		JavascriptExecutor j1 = (JavascriptExecutor)driver;
		j1.executeScript("$(\"#divOperation\").parent().find('.dialogbox-buttonarea > .button').click()");

		if (isDisplayed(label_operationActivity)) {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page loaded again", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page not loaded again", "fail");
		}

	}

	/* Con_TC_009 */
	// Verify whether user can add final operation as a QC under the bill of
	// operations details

	public void QCUnderTheBillOfOperations() throws Exception {

		click(btn_plusBOOQCBillOfOperation);

		selectIndex(txt_ElementCategoryOperation, 1);

		selectIndex(txt_activityType, 1);

		click(txt_FinalInvestigation);

		sendKeys(txt_GroupId, GroupIdDataQC);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);
		
//		Thread.sleep(3000);
//		click(btn_applyOperation);
		JavascriptExecutor j1 = (JavascriptExecutor)driver;
		j1.executeScript("$(\"#divOperation\").parent().find('.dialogbox-buttonarea > .button').click()");
		
		click(btn_draft);

		Thread.sleep(3000);

		click(btn_release);

		BOONO = getText(txt_BOONo);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(label_operationActivity)) {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page loaded again", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page not loaded again", "fail");
		}
	}

	/* Con_TC_011 */
	// Verify that user can line production operations to the BOO

	public void addLineProduction() throws Exception {

		Thread.sleep(3000);

		click(btn_supplyBillOfOperationPlusMark);

		selectIndex(txt_ElementCategoryOperation, 1);

		sendKeys(txt_GroupId, GroupIdData);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		click(btn_applyOperation);

		if (isDisplayed(label_operationActivity)) {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page loaded again", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page not loaded again", "fail");
		}

		click(btn_supplyBillOfOperationPlusMark);

		selectIndex(txt_ElementCategoryOperation, 1);

		sendKeys(txt_GroupId, GroupIdData);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		click(btn_applyOperation);

		if (isDisplayed(label_operationActivity)) {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page loaded again", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page not loaded again", "fail");
		}

		click(btn_supplyBillOfOperationPlusMark);

		selectIndex(txt_ElementCategoryOperation, 1);

		sendKeys(txt_GroupId, GroupIdData);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		click(btn_applyOperation);

		if (isDisplayed(label_operationActivity)) {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page loaded again", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page not loaded again", "fail");
		}

		click(btn_supplyBillOfOperationPlusMark);

		selectIndex(txt_ElementCategoryOperation, 1);

		selectIndex(txt_activityType, 1);

		click(txt_FinalInvestigation);

		sendKeys(txt_GroupId, GroupIdData1);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		click(btn_applyOperation);

		click(btn_draft);

		Thread.sleep(2000);

		click(btn_release);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(label_operationActivity)) {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page loaded again", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page not loaded again", "fail");
		}
	}

	/* Con_TC_012 */
	// Verify that user can add multiple production operations to the BOO

	public void addMultipleProduction() throws Exception {

		Thread.sleep(3000);

		click(btn_supplyBillOfOperationPlusMark);

		selectIndex(txt_ElementCategoryOperation, 1);

		sendKeys(txt_GroupId, GroupIdData);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		click(btn_applyOperation);

		if (isDisplayed(label_operationActivity)) {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page loaded again", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page not loaded again", "fail");
		}

		click(btn_supplyBillOfOperationPlusMark);

		selectIndex(txt_ElementCategoryOperation, 1);

		sendKeys(txt_GroupId, GroupIdData1);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		click(btn_applyOperation);

		if (isDisplayed(label_operationActivity)) {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page loaded again", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page not loaded again", "fail");
		}

		click(btn_supplyBillOfOperationPlusMark);

		selectIndex(txt_ElementCategoryOperation, 1);

		sendKeys(txt_GroupId, GroupIdData4);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		click(btn_applyOperation);

		if (isDisplayed(label_operationActivity)) {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page loaded again", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page not loaded again", "fail");
		}

		click(btn_supplyBillOfOperationPlusMark);

		selectIndex(txt_ElementCategoryOperation, 1);

		selectIndex(txt_activityType, 1);

		click(txt_FinalInvestigation);

		sendKeys(txt_GroupId, GroupIdDatafinal);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		click(btn_applyOperation);

		click(btn_draft);

		Thread.sleep(2000);

		click(btn_release);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(label_operationActivity)) {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page loaded again", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page not loaded again", "fail");
		}
	}

	/* Con_TC_015 */
	// Verify whether user can navigate to new production model page successfully

	public void navigateToProductionModel() throws Exception {

		click(btn_productionModel);

	}

	/* Con_TC_016 */
	// Verify whether user can fill summary data of production model successfully

	public void ProductionModelForm() throws Exception {

		click(btn_newProductionModel);

		Random rand = new Random();
		int rand_int1 = rand.nextInt(1000000);

		modelCodeData = modelCodeData + Integer.toString(rand_int1);

		sendKeys(txt_modelCode, modelCodeData);

		selectText(txt_productGroup, productGroupData);

		click(btn_yes);

		click(btn_product);

		// sendKeys(txt_product, productData);

		sendKeys(txt_product, productCodeData);

		pressEnter(txt_product);

		Thread.sleep(3000);

		doubleClick(doubleClickproduct);

		click(btn_yes);

		click(btn_ProductionUnit);

		sendKeys(txt_ProductionUnitLookup, ProductionUnitData);

		pressEnter(txt_ProductionUnitLookup);

		Thread.sleep(3000);

		doubleClick(doubleClickProductionUnit);

		click(btn_yes);

		if (isDisplayed(txt_newproductionModel)) {

			writeTestResults("Verify whether user can fill summary data of production model successfully",
					"The new production model page should be loaded successfully", "new production model page loaded",
					"pass");
		} else {

			writeTestResults("Verify whether user can fill summary data of production model successfully",
					"The new production model page should be loaded successfully",
					"new production model page not loaded", "fail");
		}
	}

	/* Con_TC_017 */
	// Verify whether user can fill other information of production model with
	// respect to MRP production type successfully

	public void ProductionModelFormOnlyMRPEnable() throws Exception {

		click(btn_MRPEnabled);

		sendKeys(txt_batchQty, batchQtyData);

		selectIndex(txt_barcodeBook, 1);

		selectIndex(txt_CostingPriority, 1);

		sendKeys(txt_standardCost, standardCostData);

		click(btn_pricingProfile);

		sendKeys(txt_PricingProfile, PricingProfileData);

		pressEnter(txt_PricingProfile);

		doubleClick(doubleClickPricingProfile);

		if (isDisplayed(txt_newproductionModel)) {

			writeTestResults(
					"Verify whether user can fill other information of production model with respect to MRP prodution type successfully",
					"The new production model page filled information of production model with respect to MRP production type successfully",
					"MRP production type production model filled successfully", "pass");
		} else {

			writeTestResults(
					"Verify whether user can fill other information of production model with respect to MRP prodution type successfully",
					"The new production model page filled information of production model with respect to MRP production type successfully",
					"MRP production type production model not filled successfully", "fail");
		}
	}

	/* Con_TC_018 */
	// Verify whether user can fill other information of production model with
	// respect to Infinite production type successfully

	public void ProductionModelFormOnlyInfiniteEnable() throws Exception {

//		click(btn_newProductionModel);
//
//		Random rand = new Random();
//		int rand_int1 = rand.nextInt(10000);
//
//		modelCodeData = modelCodeData + Integer.toString(rand_int1);
//
//		sendKeys(txt_modelCode, modelCodeData);
//
//		selectText(txt_productGroup, productGroupData);
//
//		click(btn_yes);
//
//		click(btn_product);
//
//		sendKeys(txt_product, productData);
//
//		pressEnter(txt_product);
//
//		Thread.sleep(3000);
//
//		doubleClick(doubleClickproduct);
//
//		click(btn_yes);
//
//		click(btn_ProductionUnit);
//
//		sendKeys(txt_ProductionUnitLookup, ProductionUnitData);
//
//		pressEnter(txt_ProductionUnitLookup);
//
//		doubleClick(doubleClickProductionUnit);
//
//		click(btn_yes);

		click(btn_Infinite);

		selectIndex(txt_barcodeBook, 1);

		selectIndex(txt_CostingPriority, 1);

		sendKeys(txt_standardCost, standardCostData);

		click(btn_pricingProfile);

		sendKeys(txt_PricingProfile, PricingProfileData);

		pressEnter(txt_PricingProfile);

		doubleClick(doubleClickPricingProfile);

		if (isDisplayed(txt_newproductionModel)) {

			writeTestResults(
					"Verify whether user can fill other information of production model with respect to Infinite prodution type successfully",
					"The new production model page filled information of production model with respect to infinite production type successfully",
					"infinite production type production model filled successfully", "pass");
		} else {

			writeTestResults(
					"Verify whether user can fill other information of production model with respect to Infinite prodution type successfully",
					"The new production model page filled information of production model with respect to infinite production type successfully",
					"infinite production type production model not filled successfully", "fail");
		}

	}

	/* Con_TC_019 */
	// Verify whether user can fill other information of production model with
	// respect to MRP and Infinite production type successfully
	public void ProductionModelFormMRPInfiniteEnable() throws Exception {

//		click(btn_newProductionModel);
//
//		Random rand = new Random();
//		int rand_int1 = rand.nextInt(10000);
//
//		modelCodeData = modelCodeData + Integer.toString(rand_int1);
//
//		sendKeys(txt_modelCode, modelCodeData);
//
//		selectText(txt_productGroup, productGroupData);
//
//		click(btn_yes);
//
//		click(btn_product);
//
//		sendKeys(txt_product, productData);
//
//		pressEnter(txt_product);
//
//		Thread.sleep(3000);
//
//		doubleClick(doubleClickproduct);
//
//		click(btn_yes);
//
//		click(btn_ProductionUnit);
//
//		sendKeys(txt_ProductionUnitLookup, ProductionUnitData);
//
//		pressEnter(txt_ProductionUnitLookup);
//
//		doubleClick(doubleClickProductionUnit);
//
//		click(btn_yes);

		click(btn_MRPEnabled);

		click(btn_Infinite);

		sendKeys(txt_batchQty, batchQtyData);

		selectIndex(txt_barcodeBook, 1);

		selectIndex(txt_CostingPriority, 1);

		sendKeys(txt_standardCost, standardCostData);

		click(btn_pricingProfile);

		sendKeys(txt_PricingProfile, PricingProfileData);

		pressEnter(txt_PricingProfile);

		doubleClick(doubleClickPricingProfile);

		if (isDisplayed(txt_newproductionModel)) {

			writeTestResults(
					"Verify whether user can fill other information of production model with respect to MRP and Infinite prodution type successfully",
					"The new production model page filled information of production model with respect to infinite and MRP production type successfully",
					"infinite and MRP production type production model filled successfully", "pass");
		} else {

			writeTestResults(
					"Verify whether user can fill other information of production model with respect to MRP and Infinite prodution type successfully",
					"The new production model page filled information of production model with respect to infinite and MRP production type successfully",
					"infinite and MRP production type production model not filled successfully", "fail");
		}
	}

	/* Con_TC_020 */
	// Verify whether user can search bill of opertaions details successfully
	public void BOODetails() throws Exception {

		click(btn_BOONoSearch);

		sendKeys(txt_BOOOperation, BOONO);

		pressEnter(txt_BOOOperation);

		doubleClick(doubleClickBOOOperation);

		click(btn_informationOK);
	}

	/* Con_TC_021 - duplicate test case */

	/* Con_TC_022 */
	// Verify whether user added BOO details are displaying on the Bill of
	// operations tab

	/* Con_TC_023 */
	// Verify whether user able to add raw materials to production model

	public void BOODetailsTabSupply() throws Exception {

		click(btn_BOOTab);

		click(btn_Expand);

		click(btn_supply);

		selectIndex(txt_ElementCategoryOperation, 2);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		click(btn_inputProducts);

		click(btn_checkBox);

		click(btn_apply);
	}

	/* Con_TC_024 */
	// Verify whether user able to add production operations to production model
	public void BOODetailsTabOperation() throws Exception {

		click(btn_operation);

		selectIndex(txt_ElementCategoryOperation, 1);

		sendKeys(txt_GroupId, GroupIdData);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		selectIndex(txt_processTime, 1);

		click(btn_inputProducts);

		click(btn_checkBox);

		click(btn_resources);

		click(btn_resourceSearch);

		sendKeys(txt_additionalResources, additionalResourcesData);

		pressEnter(txt_additionalResources);

		doubleClick(doubleCLickAdditinalResources);

		click(btn_apply);
	}

	/* Con_TC_025, Con_TC_026, Con_TC_027 */
	// Verify whether user able to add QC operations to production model
	public void BOODetailsTabQC() throws Exception {

		click(btn_QC);

		selectIndex(txt_ElementCategoryOperation, 1);

		selectIndex(txt_activityType, 1);

		sendKeys(txt_GroupId, GroupIdData2);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		selectIndex(txt_processTime, 1);

		click(btn_apply);

		Thread.sleep(3000);

		click(btn_draft);

		click(btn_release);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(txt_productionModelRelease)) {

			writeTestResults("Verify whether user can add QC details to QC process successfully",
					"User should be added QC details to QC process successfully", "production model released", "pass");
		} else {

			writeTestResults("Verify whether user can add QC details to QC process successfully",
					"User should be added QC details to QC process successfully", "production model not released",
					"fail");
		}

	}

	/* Con_TC_028 */
	// Verify whether user can navigate to new production order page successfully
	public void navigateTonewProductionOrderPage() throws Exception {

		click(btn_productionOrder);

		click(btn_newProductionOrder);

	}

	/* Con_TC_029 */
	// Verify whether user can fill production information data of production order
	// successfully

	public void informationProductionOrder() throws Exception {

		click(btn_outputProduct);

		Thread.sleep(2000);

		sendKeys(txt_product, productCodeData);

//		sendKeys(txt_product, productData);

		pressEnter(txt_product);

		Thread.sleep(3000);

		doubleClick(doubleClickproduct);

		sendKeys(txt_plannedqty, plannedqtyData);

		selectIndex(txt_CostingPriority, 1);

//		click(btn_Infinite);

		click(btn_MRPEnabled);

	}

	/* Con_TC_030, Con_TC_031, Con_TC_032, Con_TC_033 */

	// Verify whether user can fill summary data of production order successfully
	// Verify that user able to run the MRP successfully

	public void fillProductionOrder() throws Exception {

		selectIndex(txt_productionOrderGroup, 1);

		sendKeys(txt_productionOrderDesc, productionOrderDescData);

		click(btn_productionOrderModel);

		sendKeys(txt_productionOrderModel, modelCodeData);

		pressEnter(txt_productionOrderModel);

		doubleClick(doubleClickOrderModel);

		click(btn_draft);

		click(btn_release);

		productionOrder = getText(txt_productionOrderNo);

		click(navigateMenu);

		click(btn_Production);

		click(btn_ProductionControl);

		Thread.sleep(3000);

		sendKeys(txt_searchProductionOrder, productionOrder);

		click(btn_searchProductionOrder);

		Thread.sleep(3000);

		click(btn_checkBoxProductionOrderNo);

		click(btn_action);

		click(btn_RunMRP);

		click(txt_MRPNo);

		click(btn_run);

		click(btn_releaseProductionOrder);

		Thread.sleep(3000);

		click(btn_close);

	}

	/* Con_TC_034 */
	// Verify that user able to release the internal order for production order
	// successfully

	public void releaseInternalOrder() throws Exception {

		click(btn_entutionLogo);

		click(btn_taskEvent);

		click(btn_InternalDispatchOrder);

		Thread.sleep(3000);

		click(btn_arrow);

		Thread.sleep(3000);

		switchWindow();

		sendKeys(txt_shippingAddress, shippingAddressData);

		click(btn_draft);

		Thread.sleep(3000);

		click(btn_release);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");
		Thread.sleep(3000);
		if (isDisplayed(txt_internalDispatchOrderReleased)) {

			writeTestResults(
					"Verify that user able to release the internal dispatch order for production order successfully",
					"The internal dispatch order should be released successfully",
					"The internal dispatch order released", "pass");
		} else {

			writeTestResults(
					"Verify that user able to release the internal dispatch order for production order successfully",
					"The internal dispatch order should be released successfully",
					"The internal dispatch order not released", "fail");
		}
	}

	/* Con_TC_035 */

	// Verify that user can complete shop floor update successfully

	public void completeShopFloorUpdate() throws Exception {

//		selectIndex(txt_productionOrderGroup, 1);
//
//		sendKeys(txt_productionOrderDesc, productionOrderDescData);
//
//		click(btn_productionOrderModel);
//
//		sendKeys(txt_productionOrderModel, modelCodeData);
//
//		pressEnter(txt_productionOrderModel);
//
//		doubleClick(doubleClickOrderModel);
//
//		click(btn_draft);
//
//		click(btn_release);
//
//		productionOrder = getText(txt_productionOrderNo);

//		click(navigateMenu);
//
//		click(btn_Production);
//
//		click(btn_ProductionControl);
//
//		Thread.sleep(3000);
//
//		sendKeys(txt_searchProductionOrder, productionOrder);
//
//		click(btn_searchProductionOrder);
//
//		Thread.sleep(3000);
//
//		click(btn_checkBoxProductionOrderNo);
//
//		click(btn_action);
//
//		click(btn_RunMRP);
//
//		click(txt_MRPNo);
//
//		click(btn_run);
//
//		click(btn_releaseProductionOrder);
//
//		click(btn_close);

//		click(navigateMenu);
//
//		click(btn_production);
		Thread.sleep(2000);

		click(btn_productionOrder);

		Thread.sleep(2000);

		sendKeys(txt_prodOrderNo, productionOrder);

		pressEnter(txt_prodOrderNo);

		Thread.sleep(3000);

		click(doubleClickprodNo.replace("number", productionOrder));

		click(btn_action);

		click(btn_shopFloorUpdate);

		Thread.sleep(3000);

		click(btn_start);

		Thread.sleep(2000);

		click(btn_startProcess);

		Thread.sleep(2000);

		click(btn_actual);

		Thread.sleep(4000);
		sendKeys(txt_producedQty, producedData);

		Thread.sleep(2000);

		click(btn_update);

		Thread.sleep(2000);

		click(btn_inputProduct);

		sendKeys(txt_actualQty, actualQtyData);

		click(btn_update);

//		Thread.sleep(2000);
//
////		click(btn_closeShop);
//		click("//div[9]//span[2][@class='headerclose']");

		pageRefersh();
		Thread.sleep(2000);
		
		click(btn_action);

		click(btn_shopFloorUpdate);


		click(btn_completeTick);

		click(txt_complete);
		
		pageRefersh();

	}

	/* Con_TC_036 */

	// Verify that user can complete production label print successfully

	public void completeproductionLabel() throws Exception {

		click(btn_labelPrint);

		click(btn_view);

		selectIndex(txt_batchNo, 1);

		selectIndex(txt_machineNo, 1);

		selectIndex(txt_color, 2);

		click(btn_draftNPrint);
		switchWindow();
		switchWindow();
		click("//*[@id=\"inspect\"]/div[6]/div[1]/span[2]");

	}

	/* Con_TC_037 */

	// Verify that user can complete the production QC successfully

	public void productionLabelPrint() throws Exception {

		click(btn_labelPrint);

		click(btn_labelPrintHistory);

		click(btn_productOrder);

		click(btn_QcComplete);

		Thread.sleep(3000);
	}

	/* Con_TC_038 */
	// Verify that user can complete the production costing successfully

	public void productionCosting() throws Exception {

//		click(navigateMenu);
//
//		click(btn_Production);

		click(btn_productionOrder);

		sendKeys(txt_productonOrder, productionOrder);

		click(doubleClickprodNo.replace("number", productionOrder));

		click(btn_action);

		click(btn_productionCosting);

		click(btn_costingRefresh);

		click(btn_inputProductsCostingTab);

		click(btn_updateCosting);

		Thread.sleep(2000);

		click(btn_closeCosting);

		click(btn_tick);

		click(btn_update);

	}

	/* Con_TC_039 */
	// Verify that user can generate prodction internal receipt successfully

	public void internalReceipt() throws Exception {

		Thread.sleep(3000);

		click(btn_internalReceiptControl);

		click(btn_smallcheckBox);

		Random rand = new Random();

		// Generate random integers in range 0 to 999
		int rand_int1 = rand.nextInt(10000);

		lotNo = lotNo + Integer.toString(rand_int1);

		sendKeys(txt_lotNo, lotNo);

		click(btn_generateReceipt);

	}

	/* Con_TC_040 */
	// Verify that user can successfully navigate to the Organization Management
	// module

	public void organizationManagement() throws Exception {

		click(btn_organizationManagement);

		if (isDisplayed(txt_organizationManagement)) {

			writeTestResults("Verify whether user can select Organization Management module successfully",
					"The new organization module should be loaded successfully", "organization module page loaded",
					"pass");
		} else {

			writeTestResults("Verify whether user can select Organization Management module successfully",
					"The new organization module should be loaded successfully", "organization module page not loaded",
					"fail");
		}

	}

	/* Con_TC_041 */

	// Verify that user can successfully navigate to the Location Information Form

	public void locationInformation() throws Exception {

		click(btn_locationInfo);

		if (isDisplayed(txt_locationInformation)) {

			writeTestResults("Verify whether user can User should be able to view a Existing Location Information",
					"Existing Location Information should be loaded successfully",
					"Existing Location Information page loaded", "pass");
		} else {

			writeTestResults("Verify whether user can User should be able to view a Existing Location Information",
					"Existing Location Information should be loaded successfully",
					"Existing Location Information page not loaded", "fail");
		}
	}

	/* Con_TC_042 */

	// Verify that user can successfully Create a New Location Information and Fill
	// Required Information in Summary Tab

	public void newLocation() throws Exception {

		Random rand = new Random();
		int rand_int2 = rand.nextInt(100000);

		locationCodeData = locationCodeData + Integer.toString(rand_int2);

		click(btn_locationInfo);

		click(btn_newLocation);

		sendKeys(txt_locationCode, locationCodeData);

		// locationCode = getText(txt_locationCode);

		sendKeys(txt_description, descriptionData);

		selectIndex(txt_businessUnit, 6);
		Thread.sleep(3000);
		selectText(txt_inputWare, inputwareData);

		selectText(txt_outWare, outputwareData);

		selectText(txt_WIPWare, WIPwareData);

		selectIndex(txt_site, 1);

		click(txt_overheadInformation);

		click(btn_searchNewLocation);

		sendKeys(txt_overhead, overheadData);

		pressEnter(txt_overhead);

		doubleClick(doubleClickOverhead);

		sendKeys(txt_rate, rateData);

		click(btn_draftLocation);

		if (isDisplayed(txt_locationInformation)) {

			writeTestResults(
					"Verify that user can successfully Create a New Location Information and Fill Required Information in Summary Tab",
					"new location information should be loaded successfully",
					"new location Location Information page loaded", "pass");
		} else {

			writeTestResults(
					"Verify that user can successfully Create a New Location Information and Fill Required Information in Summary Tab",
					"new location information should be loaded successfully",
					"new location Location Information page not loaded", "fail");
		}

	}

	/* Con_TC_043 */

	// Verify that user can sucessfully Activate the Location Information
	public void overheadInformation() throws Exception {

		keyDownClick(txt_Unit, locationCodeData);

		Thread.sleep(2000);

		click(txt_activate);

		click(btn_yes);

	}

	/* Con_TC_044 */

	// Verify that user can sucessfully create the Overhead for production

	public void navigateToOverheadInfo() throws Exception {

		click(txt_overheadInfo);
	}

	public void createOverheadInformation() throws Exception {

		click(btn_new);

		Random rand = new Random();
		int rand_int2 = rand.nextInt(10000);

		overheadCodeData = overheadCodeData + Integer.toString(rand_int2);

		sendKeys(txt_overheadCode, overheadCodeData);

		sendKeys(txt_descOverhead, overheadDescData);

		selectIndex(txt_overheadGroup, 1);

		selectIndex(txt_rateType, 1);

		click(btn_draftOverhead);

	}

	public void searchcreatedOverheadInformation() throws Exception {

		click(txt_newOverheadCode.replace("code", overheadCodeData));

		click(btn_active);

		click(btn_yes);

	}

	/* Con_TC_045 */
	/* Con_TC_046 */
	// Verify that user can successfully create a new Pricing Profile and Fill
	// requried summary information
	public void newPricingProfile() throws Exception {

		click(btn_PricingProfile);

		click(btn_newPricingProfile);

		// create instance of Random class
		Random rand = new Random();

		// Generate random integers in range 0 to 999
		int rand_int1 = rand.nextInt(10000);

		pricingProfileCodeData = pricingProfileCodeData + Integer.toString(rand_int1);

		sendKeys(txt_pricingProfileCode, pricingProfileCodeData);

		sendKeys(txt_descPricingProfile, descPricingProfileData);

		selectIndex(txt_pricingGroup, 2);

	}

	/* Con_TC_047 */
	// Verify that user can sucessfully fill the Requried information in Estimation
	// Tab

	public void estimatePricingProfile() throws Exception {

		click(btn_PricingProfile);

		click(btn_newPricingProfile);

		click(btn_estimate);

		selectIndex(txt_material, 1);
		selectIndex(txt_labour, 1);
		selectIndex(txt_Overhead, 1);
		selectIndex(txt_service, 1);
		selectIndex(txt_expense, 1);
	}

	/* Con_TC_048 */
	// Verify that user can successfully fill the required information in Actual Tab
	public void actualCalculationPricingProfile() throws Exception {

		click(btn_PricingProfile);

		click(btn_newPricingProfile);

		click(btn_actualCalculation);

		selectIndex(txt_materialAC, 2);
		selectIndex(txt_labourAC, 2);
		selectIndex(txt_OverheadAC, 2);
		selectIndex(txt_serviceAC, 2);
		selectIndex(txt_expenseAC, 2);
	}

	/* Con_TC_049 */
	// Verify that user can Draft and Active the Pricing Profile
	public void draftandReleasePricingProfile() throws Exception {

		Thread.sleep(2000);

		click(btn_draftLocation);

		keyDownClick(txt_Pricing, pricingProfileCodeData);

		click(btn_active);

		click(btn_yes);

		if (isDisplayed(txt_activateStatus)) {

			writeTestResults("Verify the Pricing Profile Status", " Pricing Profile Status should update as Activated ",
					"Pricing Profile Status viewed as active", "pass");
		} else {

			writeTestResults("Verify the Pricing Profile Status", " Pricing Profile Status should update as Activated",
					"Pricing Profile Status not viewed as active", "fail");
		}

	}

	/* Con_TC_050 */
	// Verify that user can successfully delete a existing pricing profile
	public void deletePricingProfile() throws Exception {

		click(btn_draftLocation);

		keyDownClick(txt_Pricing, pricingProfileCodeData);

		if (isDisplayed(txt_openStatus)) {

			writeTestResults("Verify the Pricing Profile Status", " Pricing Profile Status should update as open ",
					"Pricing Profile Status viewed as open", "pass");
		} else {

			writeTestResults("Verify the Pricing Profile Status", " Pricing Profile Status should update as open",
					"Pricing Profile Status not viewed as open", "fail");
		}

		click(btn_delete);

		click(btn_yes);

		if (isDisplayed(txt_activateStatus)) {

			writeTestResults("Verify the Pricing Profile Status", " Pricing Profile Status should update as Activated ",
					"Pricing Profile Status viewed as active", "pass");
		} else {

			writeTestResults("Verify the Pricing Profile Status", " Pricing Profile Status should update as Activated",
					"Pricing Profile Status not viewed as active", "fail");
		}

		click(btn_delete);

	}

	/* Con_TC_051 */
	// Verify that user can successfully navigate to the Bulk Production Form

	public void navigateToBulkProductionForm() throws Exception {

		click(btn_bulkProductionOrder);

		if (isDisplayed(txt_BulkProduction)) {

			writeTestResults("Verify that user can successfully Open a Bulk Production order",
					" User should be able to Open a Bulk Production order Sucessfully",
					"Bulk Production order form page loaded", "pass");
		} else {

			writeTestResults("Verify that user can successfully Open a Bulk Production order",
					" User should be able to Open a Bulk Production order Sucessfully",
					"Bulk Production order form page not loaded", "fail");
		}

	}

	/* Con_TC_052 */
	// Verify whether user can enter summary information of Bulk Prodution Order

	public void SummaryBulkProductionOrder() throws Exception {

		click(new_bulkProductionOrder);

		selectIndex(txt_productionOrderGroup, 2);

		sendKeys(txt_descrProductionOrderGroup, productionOrderGroupData);

		selectIndex(txt_referenceType, 1);

		click(btn_yes);

		click(btn_productionUnitBulk);

		sendKeys(txt_productionUnitBulk, productionUnitBulkData);

		pressEnter(txt_productionUnitBulk);

		doubleClick(doubleClickProductionUnitBulk);

		if (isDisplayed(txt_BulkProductionForm)) {

			writeTestResults("Verify whether user can enter summary information of Bulk Prodution Order",
					"Bulk production form should be displayed successfully", "Bulk production form page loaded",
					"pass");
		} else {

			writeTestResults("Verify whether user can enter summary information of Bulk Prodution Order",
					"Bulk production form should be displayed successfully", "Bulk production form page not loaded",
					"fail");
		}

	}

	/* Con_TC_053 */
	// Verify whether user can enter Product informations on Bulk Prodution Order
	public void ProductBulkProductionOrder() throws Exception {

		click(btn_productTab);

		click(btn_productSearch);

		sendKeys(txt_productSearch, productSearchData);

		pressEnter(txt_productSearch);

		Thread.sleep(3000);

		doubleClick(doubleClickProductSearch);

		Thread.sleep(3000);

		click(btn_productModel);

		sendKeys(txt_productModel, modelData);

		pressEnter(txt_productModel);

		doubleClick(doubleClickProductModel);

		sendKeys(txt_plannedQtyBulk, plannedqtyBulkData);

		sendKeys(txt_standardCost, standardCostData);

		click(btn_checkOut);

		Thread.sleep(10000);

		click(btn_draftProductionOrder);
	}

	/* Con_TC_054 */
	// Verify Whether user can draft and release the Bulk Production Order

	public void draftNReleaseBulkProductionOrder() throws Exception {

		Thread.sleep(3000);

		click(btn_releaseBulkProductionOrder);
	}

	/* Con_TC_055 */
	// Verify Whether user can generate a Internal Order for the Input Products
	public void generateInternalOrder() throws Exception {

		click(btn_actionBulk);

		click(btn_createInternalOrder);

		Thread.sleep(3000);

		click(btn_selectAll);

		click(btn_applyInternal);
	}

	/* Con_TC_056 */
	// Verify whether user can load the internal order related to the Bulk
	// Production Order
	public void loadInternalOrderOrder() throws Exception {

		switchWindow();

		sendKeys(txt_title, titleData);

		click(btn_requester);

		sendKeys(txt_requester, requesterData);

		pressEnter(txt_requester);

		Thread.sleep(3000);

		doubleClick(doubleClickRequester);
	}

	/* Con_TC_057 */
	// Verify Whether user can draft and release the Internal order
	public void draftNReleaseInternalOrder() throws Exception {

		click(btn_draft);

		Thread.sleep(3000);

		click(btn_release);

		click(btn_goTopage);
	}

	/* Con_TC_058 */
	// Verify whether user can complete the bulk Production Order
	public void completeProductionOrder() throws Exception {

		switchWindow();

		sendKeys(txt_shipping, shippingData);

		click(btn_draft);

		Thread.sleep(4000);

		click(btn_Capture);

		Thread.sleep(4000);

		click(btn_productListClick);

		sendKeys(txt_batchNoTXT, batchData);

		pressEnter(txt_batchNoTXT);

		click(btn_refresh);

		click(btn_back);

		click(btn_release);

//		click("//*[@id=\"inspect\"]/div[5]/div[3]/a");

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(txt_internalDispatchRelease)) {

			writeTestResults("Verify the document status", "document status should be displayed successfully",
					"Document status displayed as Released", "pass");
		} else {

			writeTestResults("Verify the document status", "document status should be displayed successfully",
					"Document status shouldn't displayed as Released", "fail");
		}
	}

	/* Con_TC_059 */
	// Verify Whether user can generate a Internal Order for the Input Products
	public void generateInternalOrderForInputProducts() throws Exception {

		closeWindow();

		switchWindow();

		click(btn_closeTab);

		Thread.sleep(3000);

		click(btn_actionBulk);

		click(btn_productionCosting);

		click(btn_date);

		click(btn_actualDate);

		sendKeys(txt_actualQty1, actualQtyData1);

		click(btn_update);

		click(btn_input);

		sendKeys(txt_actualQty1, actualQtyData1);

		click(btn_releaseTab);

		click("//*[@id=\"inspect\"]/div[5]/div[3]/a");

		if (isDisplayed(txt_internalReceipt)) {

			writeTestResults(" Verify Internal Receipt Number is generated",
					"Internal Receipt Number should be displayed successfully", "Internal Receipt Number loaded",
					"pass");
		} else {

			writeTestResults(" Verify Internal Receipt Number is generated",
					"Internal Receipt Number should be displayed successfully", "Internal Receipt Number not loaded",
					"fail");
		}
	}

	/* Con_TC_060 */
	// Verfiy whether user can convert the bulk Production order to Internal Receipt
	public void convertInternalOrder() throws Exception {

		Thread.sleep(3000);

		click(btn_actionBulk);

		click(btn_internalReceipt);

		click(btn_smallcheckBox);

		sendKeys(txt_lotNo, lotNo);

		click(btn_generate);

		click(btn_closeTab);

		bulkNo = getText(bulkno);

		if (isDisplayed(txt_internalReceipt)) {

			writeTestResults(" Verify Internal Receipt Number is generated",
					"Internal Receipt Number should be displayed successfully", "Internal Receipt Number loaded",
					"pass");
		} else {

			writeTestResults(" Verify Internal Receipt Number is generated",
					"Internal Receipt Number should be displayed successfully", "Internal Receipt Number not loaded",
					"fail");
		}

		navigateToSideBar();
		navigateToProduction();
		Thread.sleep(2000);
		click(btn_bulkProductionOrder);
		Thread.sleep(2000);
		navigateBulkProductionForm();

	}

	/* Con_TC_061 */
	// Verify that user can successfully navigate to the Bulk Production Form
	public void navigateBulkProductionForm() throws Exception {

		sendKeys(txt_bulkOrder, bulkNo);

		pressEnter(txt_bulkOrder);

		Thread.sleep(3000);

		click(bulkOrder.replace("no", bulkNo));

	}

	/* Con_TC_062 */
	// Verify whether user can Create a Bulk Production order and Enter Required
	// Information in Summary Tab

	public void SummaryBulkProductionOrder1() throws Exception {

		click(new_bulkProductionOrder);

		selectIndex(txt_productionOrderGroup, 2);

		Thread.sleep(2000);

		sendKeys(txt_descrProductionOrderGroup, productionOrderGroupData);

		Thread.sleep(2000);

		selectIndex(txt_referenceType, 0);

		click(btn_yes);

		click(btn_productionUnitBulk);

		sendKeys(txt_productionUnitBulk, productionUnitBulkData);

		pressEnter(txt_productionUnitBulk);

		doubleClick(doubleClickProductionUnitBulk);

		if (isDisplayed(txt_BulkProductionForm)) {

			writeTestResults("Verify whether user can enter summary information of Bulk Prodution Order",
					"Bulk production form should be displayed successfully", "Bulk production form page loaded",
					"pass");
		} else {

			writeTestResults("Verify whether user can enter summary information of Bulk Prodution Order",
					"Bulk production form should be displayed successfully", "Bulk production form page not loaded",
					"fail");
		}

	}

	/* Con_TC_063 */
	// Verify whether user can enter Required Information in Product Tab
	public void ProductBulkProductionOrder1() throws Exception {

		click(btn_productTab);

		click(btn_productSearch);

		sendKeys(txt_productSearch, productSearchData);

		pressEnter(txt_productSearch);

		Thread.sleep(3000);

		doubleClick(doubleClickProductSearch);

		Thread.sleep(3000);

		sendKeys(txt_plannedQtyBulk, plannedqtyBulkData);

		Thread.sleep(2000);

		sendKeys(txt_standardCost1, standardCostData1);
	}

	/* Con_TC_064 */
	// Verify wherther user can select Input Products for the bulk Production
	public void productBulkProduction() throws Exception {

		click(btn_inputProductsTab);

		click(btn_searchProduct);

		Thread.sleep(2000);

		sendKeys(txt_searchProduct, searchProductData);
		Thread.sleep(2000);
		pressEnter(txt_searchProduct);

		Thread.sleep(2000);

		doubleClick(doubleClickSearchProduct);

		Thread.sleep(2000);

		sendKeys(txt_plannedqty1, plannedqtyData1);

	}

	/* Con_TC_065 */
	// Verify whether user can Draft and Release the Bulk Production Order

	public void ReleaseBulkProductionOrder() throws Exception {

		click(btn_draftProductionOrder);

		Thread.sleep(2000);

		click(btn_releaseBulkProductionOrder);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(txt_BulkOrderRelease)) {

			writeTestResults("Verify the document status", "document status should be displayed successfully",
					"Document status displayed as Released", "pass");
		} else {

			writeTestResults("Verify the document status", "document status should be displayed successfully",
					"Document status shouldn't displayed as Released", "fail");
		}
	}

	/* Con_TC_066 */
	// Verify Whether user can generate a Internal Order for the Input Products
	public void generateInternalOrderForInputProduct() throws Exception {

		click(btn_actionBulk);

		click(btn_createInternalOrder);

		Thread.sleep(3000);

		click(btn_selectAll);

		click(btn_applyInternal);
	}

	/* Con_TC_067 */
	// Verify whether user can load the internal order related to the Bulk
	// Production Order
	public void loadInternalOrderOrderRelatedToBulk() throws Exception {

		switchWindow();

		sendKeys(txt_title, titleData);

		click(btn_requester);

		sendKeys(txt_requester, requesterData);

		pressEnter(txt_requester);

		Thread.sleep(3000);

		doubleClick(doubleClickRequester);

	}

	/* Con_TC_068 */
	// Verify Whether user can draft and release the Internal order
	public void draftReleaseInternalOrder() throws Exception {

		click(btn_draft);

		Thread.sleep(3000);

		click(btn_release);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(txt_internalOrderRelease)) {

			writeTestResults("Verify the document status", "document status should be displayed successfully",
					"Document status displayed as Released", "pass");
		} else {

			writeTestResults("Verify the document status", "document status should be displayed successfully",
					"Document status shouldn't displayed as Released", "fail");
		}

		click(btn_goTopage);

	}

	/* Con_TC_069 */
	// Verify whther user can convert the Internal Order to Internal Dispatch order

	public void completeProductionOrder1() throws Exception {

		switchWindow();

		sendKeys(txt_shipping, shippingData);

		click(btn_draft);

		Thread.sleep(3000);

		click(btn_release);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(txt_internalDispatchRelease)) {

			writeTestResults("Verify the document status", "document status should be displayed successfully",
					"Document status displayed as Released", "pass");
		} else {

			writeTestResults("Verify the document status", "document status should be displayed successfully",
					"Document status shouldn't displayed as Released", "fail");
		}
	}

	/* Con_TC_070 */
	// Verify whether user can complete the bulk Production Order.
	public void completeBulkProductionOrder() throws Exception {

		closeWindow();

		switchWindow();

		click(btn_closeTab);

		Thread.sleep(3000);

		click(btn_actionBulk);

		click(btn_productionCosting);

		click(btn_date);

		click(btn_actualDate);

		sendKeys(txt_actualQty1, actualQtyData1);

		Thread.sleep(2000);

		click(btn_update);

		click(btn_input);

		sendKeys(txt_actualQty1, actualQtyData1);

		click(btn_releaseTab);

		if (isDisplayed(txt_internalReceipt)) {

			writeTestResults(" Verify Internal Receipt Number is generated",
					"Internal Receipt Number should be displayed successfully", "Internal Receipt Number loaded",
					"pass");
		} else {

			writeTestResults(" Verify Internal Receipt Number is generated",
					"Internal Receipt Number should be displayed successfully", "Internal Receipt Number not loaded",
					"fail");
		}

	}

	/* Con_TC_071 */
	// Verfiy whether user can convert the bulk Production order to Internal Receipt

	public void convertInternalReceipt() throws Exception {

		Thread.sleep(2000);

		click(btn_actionBulk);

		click(btn_internalReceipt);

		Thread.sleep(2000);

		click(btn_smallcheckBox);

		Random rand = new Random();

		// Generate random integers in range 0 to 999
		int rand_int1 = rand.nextInt(1000);

		lotNo = lotNo + Integer.toString(rand_int1);

		sendKeys(txt_lotNo, lotNo);

		click(btn_generate);

		click(btn_closeTab);

		if (isDisplayed(txt_internalReceipt)) {

			writeTestResults(" Verify Internal Receipt Number is generated",
					"Internal Receipt Number should be displayed successfully", "Internal Receipt Number loaded",
					"pass");
		} else {

			writeTestResults(" Verify Internal Receipt Number is generated",
					"Internal Receipt Number should be displayed successfully", "Internal Receipt Number not loaded",
					"fail");
		}

	}

	/* common key down click */
	public void keyDownClick(String locator, String value) throws Exception {
		sendKeys(locator, value);
		Thread.sleep(3000);
		driver.findElement(getLocator(locator)).sendKeys(Keys.ARROW_DOWN);
		pressEnter(locator);
	}

	// creating output product
	// CON_TC_072
	public void newOutputProduct() throws Exception {

//		navigateToTheLoginPage();
//		verifyTheLogo();
//		userLogin();
//		navigateToSideBar();
//		navigateToInventory();

		click(btn_productInfo);

		click(btn_newProduct);

		Random random = new Random();
		int rand_intcode = random.nextInt(100000);

		productCodeData = productCodeData + Integer.toString(rand_intcode);

		sendKeys(txt_productCode, productCodeData);

		sendKeys(txt_productDescription, productCodeData);

		selectText(txt_prodGrop, productGroupData);

		sendKeys(txt_basePrice, basePriceData);

		click(btn_manufacturer);

		sendKeys(txt_manufacturer, manufacturerData);

		pressEnter(txt_manufacturer);

		doubleClick(doubleCLickManufacturer);

		selectIndex(txt_UOMGroup, 1);

		selectIndex(txt_UOM, 1);

		Thread.sleep(2000);

		// Length
		sendKeys(txt_length, lengthData);
		selectIndex(txt_lengthVal, 1);

		// Width
		sendKeys(txt_width, widthData);
		selectIndex(txt_widthVal, 4);

		// Height
		sendKeys(txt_height, heightData);
		selectIndex(txt_heightVal, 6);

		// Volume
		sendKeys(txt_volume, volumeData);
		selectIndex(txt_VolumeVal, 2);

		click(btn_tabDetails);

		click(btn_allowInventoryWithoutCosting);

		selectIndex(txt_manufacturingType, 1);

		selectIndex(txt_batchBook, 1);

		click(btn_draft);

		Thread.sleep(2000);

		click(btn_release);

		// product = getText(txt_productName);

	}

	// FROM CON_TC_003 to CON_TC_039
	/* SMOKE 001 */
	public void All() throws Exception {

		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		navigateToSideBar();
		navigateToInventory();
		newOutputProduct();
		//driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		Thread.sleep(2000);

		navigateToSideBar();
		navigateToInventory();
		navigateToBillOfMaterial();
		navigateTonewBOM();
		fillingBOMForm();
		completeNreleaseBOM();

		navigateToSideBar();
		navigateToProduction();
		navigateToBillOfOperation();
		navigateTonewBOO();
		fillBOOForm();
		OperationActivitySupply();
		OperationActivityOperation();
		QCUnderTheBillOfOperations();
		
		//driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		Thread.sleep(2000);

		navigateToSideBar();
		navigateToProduction();
		navigateToProductionModel();
		ProductionModelForm();
		ProductionModelFormOnlyMRPEnable();
		BOODetails();
		BOODetailsTabSupply();
		BOODetailsTabOperation();
		BOODetailsTabQC();
		//driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		Thread.sleep(2000);

		navigateToSideBar();
		navigateToProduction();
		navigateTonewProductionOrderPage();
		informationProductionOrder();
		fillProductionOrder();
		releaseInternalOrder();
		//driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		Thread.sleep(3000);

		navigateToSideBar();
		navigateToProduction();
		completeShopFloorUpdate();

		click("//*[@id=\"inspect\"]/div[8]/div[1]/span[2]");
		
		Thread.sleep(2000);
		//driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		navigateToSideBar();
		navigateToProduction();
		completeproductionLabel();

	}

	/* SMOKE 002 */
	public void AllInOne() throws Exception {

		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		navigateToSideBar();
		navigateToProduction();
		click(btn_labelPrint);
		productionLabelPrint();
		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		//Thread.sleep(4000);
		navigateToSideBar();
		navigateToProduction();
		productionCosting();

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$(closeMessageBox()).click()");

		click("//*[@id=\"inspect\"]/div[7]/div[1]/span[2]");
		
		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		//Thread.sleep(2000);

		click(btn_action);

		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		//Thread.sleep(4000);

		click(btn_internalReceiptControl);
		
		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		//Thread.sleep(3000);

		click(btn_smallcheckBox);
 
		Random rand = new Random();

		// Generate random integers in range 0 to 999
		int rand_int1 = rand.nextInt(10000);

		lotNo = lotNo + Integer.toString(rand_int1);

		sendKeys(txt_lotNo, lotNo);

		click(btn_generateReceipt);

		click("//span[@class='headerclose']");

		click(btn_actionBulk);
		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		//Thread.sleep(3000);
		click(btn_internalReceipt);

		click(link_internalReceipt);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(txt_internalReceiptReleased)) {

			writeTestResults(" Verify that user able to release internal receipt successfully",
					"internal receipt should released successfully", "internal receipt released successfully", "pass");
		} else {

			writeTestResults("Verify that user able to release internal receipt successfully",
					"internal receipt should released successfully", "internal receipt not released successfully",
					"fail");
		}
		
		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		//Thread.sleep(2000);

		switchWindow();
		click(btn_release);
		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		//Thread.sleep(3000);
		Actions action = new Actions(driver);
		WebElement we = driver.findElement(By.xpath(btn_actionJournal));
		action.moveToElement(we).build().perform();
		click(btn_actionJournal);

		click(btn_journalEntry);

		click(link_gernalEntryDetails);

		switchWindow();
		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		//Thread.sleep(5000);

		if (isDisplayed(txt_journalEntryReleased)) {

			writeTestResults(" Verify that user able to view journal entry successfully",
					"journal entry should released successfully", "journal entry released successfully", "pass");
		} else {

			writeTestResults("Verify that user able to view journal entry successfully",
					"journal entry should released successfully", "journal entry not released successfully", "fail");
		}

	}

	/* New Regression Test Cases */

	public void PROD_PO_010() throws Exception {

		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		navigateToSideBar();
		navigateToInventory();
		newOutputProduct();

		//Thread.sleep(2000);

		navigateToSideBar();
		navigateToInventory();
		navigateToBillOfMaterial();
		navigateTonewBOM();
		fillingBOMForm();
		completeNreleaseBOM();

		navigateToSideBar();
		navigateToProduction();
		navigateToBillOfOperation();
		navigateTonewBOO();
		fillBOOForm();
		OperationActivitySupply();
		OperationActivityOperation();
		QCUnderTheBillOfOperations();
		
		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		//Thread.sleep(2000);

		navigateToSideBar();
		navigateToProduction();
		navigateToProductionModel();
		ProductionModelForm();
		ProductionModelFormOnlyMRPEnable();
		BOODetails();
		BOODetailsTabSupply();
		BOODetailsTabOperation();
		BOODetailsTabQC();
		
		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		//Thread.sleep(2000);

		navigateToSideBar();
		navigateToProduction();
		navigateTonewProductionOrderPage();
		informationProductionOrder();

		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		//Thread.sleep(2000);

		selectIndex(txt_productionOrderGroup, 1);

		sendKeys(txt_productionOrderDesc, productionOrderDescData);

		click(btn_productionOrderModel);

		sendKeys(txt_productionOrderModel, modelCodeData);

		pressEnter(txt_productionOrderModel);

		doubleClick(doubleClickOrderModel);

		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		//Thread.sleep(2000);

		click(btn_draft);

		productionOrder = getText(txt_productionOrderNo);

		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		//Thread.sleep(2000);

		navigateToSideBar();
		navigateToProduction();

		click(btn_productionorder);

		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		//Thread.sleep(2000);

		sendKeys(txt_searchProductionOrder1, productionOrder);

		pressEnter(txt_searchProductionOrder1);

		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		//Thread.sleep(3000);

		click(doubleClickprodNo.replace("number", productionOrder));

		click(btn_edit);

		selectIndex(txt_productionOrderGroup, 2);

		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		//Thread.sleep(3000);

		click(btn_Update);

	}

	public void PROD_PO_011() throws Exception {

		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		navigateToSideBar();
		navigateToInventory();
		newOutputProduct();

		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		//Thread.sleep(2000);

		navigateToSideBar();
		navigateToInventory();
		navigateToBillOfMaterial();
		navigateTonewBOM();
		fillingBOMForm();
		completeNreleaseBOM();

		navigateToSideBar();
		navigateToProduction();
		navigateToBillOfOperation();
		navigateTonewBOO();
		fillBOOForm();
		OperationActivitySupply();
		OperationActivityOperation();
		QCUnderTheBillOfOperations();

		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		//Thread.sleep(2000);

		navigateToSideBar();
		navigateToProduction();
		navigateToProductionModel();
		ProductionModelForm();
		ProductionModelFormOnlyMRPEnable();
		BOODetails();
		BOODetailsTabSupply();
		BOODetailsTabOperation();
		BOODetailsTabQC();

		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		//Thread.sleep(2000);

		navigateToSideBar();
		navigateToProduction();
		navigateTonewProductionOrderPage();
		informationProductionOrder();
		
		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		//Thread.sleep(2000);

		selectIndex(txt_productionOrderGroup, 1);

		sendKeys(txt_productionOrderDesc, productionOrderDescData);

		click(btn_productionOrderModel);

		sendKeys(txt_productionOrderModel, modelCodeData);

		pressEnter(txt_productionOrderModel);

		doubleClick(doubleClickOrderModel);

		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		//Thread.sleep(2000);

		click(btn_draft);

		productionOrder = getText(txt_productionOrderNo);

		Thread.sleep(2000);

		navigateToSideBar();
		navigateToProduction();

		click(btn_productionorder);
		
		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		//Thread.sleep(2000);

		sendKeys(txt_searchProductionOrder1, productionOrder);

		pressEnter(txt_searchProductionOrder1);

		Thread.sleep(3000);

		click(doubleClickprodNo.replace("number", productionOrder));

		click(btn_duplicate);

		sendKeys(txt_plannedqty, plannedqtyData);

		String outputProduct1 = getText(txt_outputProduct1);
		String outputProduct2 = getText(txt_outputProduct2);

		if (outputProduct1.equals(outputProduct2)) {
			writeTestResults("output product values should be equal", "Should be able to equal output product values",
					"output products are equal", "pass");
		}

		else {
			writeTestResults("output product values should be equal", "Should be able to equal output product values",
					"output products are not equal", "fail");
		}

		Thread.sleep(2000);

		String costingPriority1 = getText(txt_costingPriority1);
		String costingPriority2 = getText(txt_costingPriority2);

		if (costingPriority1.equals(costingPriority2)) {
			writeTestResults("costing priority values should be equal",
					"Should be able to equal costing priority values", "costing priority values are equal", "pass");
		}

		else {
			writeTestResults("costing priority values should be equal",
					"Should be able to equal costing priority values", "costing priority values are not equal", "fail");
		}

		Thread.sleep(2000);

		String prodOrderGroup1 = getText(txt_prodOrderGroup1);
		String prodOrderGroup2 = getText(txt_prodOrderGroup2);

		if (prodOrderGroup1.equals(prodOrderGroup2)) {
			writeTestResults("Product Order Group values should be equal",
					"Should be able to equal product order group values", "product order groups are equal", "pass");
		}

		else {
			writeTestResults("Product Order Group values should be equal",
					"Should be able to equal product order group values", "product order groups are not equal", "fail");
		}

		Thread.sleep(2000);

		String description1 = getText(txt_description1);
		String description2 = getText(txt_description2);

		if (description1.equals(description2)) {
			writeTestResults("Description values should be equal", "Should be able to equal Description values",
					"Description are equal", "pass");
		}

		else {
			writeTestResults("Description values should be equal", "Should be able to equal Description values",
					"Description are not equal", "fail");
		}

		Thread.sleep(2000);

		String prodModel1 = getText(txt_prodModel1);
		String prodModel2 = getText(txt_prodModel2);

		if (prodModel1.equals(prodModel2)) {
			writeTestResults("product model values should be equal", "Should be able to equal product model values",
					"product model are equal", "pass");
		}

		else {
			writeTestResults("product model values should be equal", "Should be able to equal product model values",
					"product model are not equal", "fail");
		}

	}

	public void PROD_PO_012() throws Exception {

		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		navigateToSideBar();
		navigateToInventory();
		newOutputProduct();

		Thread.sleep(2000);

		navigateToSideBar();
		navigateToInventory();
		navigateToBillOfMaterial();
		navigateTonewBOM();
		fillingBOMForm();
		completeNreleaseBOM();

		navigateToSideBar();
		navigateToProduction();
		navigateToBillOfOperation();
		navigateTonewBOO();
		fillBOOForm();
		OperationActivitySupply();
		OperationActivityOperation();
		QCUnderTheBillOfOperations();

		Thread.sleep(2000);

		navigateToSideBar();
		navigateToProduction();
		navigateToProductionModel();
		ProductionModelForm();
		ProductionModelFormOnlyMRPEnable();
		BOODetails();
		BOODetailsTabSupply();
		BOODetailsTabOperation();
		BOODetailsTabQC();

		Thread.sleep(2000);

		navigateToSideBar();
		navigateToProduction();
		navigateTonewProductionOrderPage();
		informationProductionOrder();

		Thread.sleep(2000);

		selectIndex(txt_productionOrderGroup, 1);

		sendKeys(txt_productionOrderDesc, productionOrderDescData);

		click(btn_productionOrderModel);

		sendKeys(txt_productionOrderModel, modelCodeData);

		pressEnter(txt_productionOrderModel);

		doubleClick(doubleClickOrderModel);

		click(btn_draft);

		click(btn_release);

		productionOrder = getText(txt_productionOrderNo);

		Thread.sleep(2000);

		navigateToSideBar();
		navigateToProduction();

		click(btn_productionorder);

		Thread.sleep(2000);

		sendKeys(txt_searchProductionOrder1, productionOrder);

		pressEnter(txt_searchProductionOrder1);

		Thread.sleep(3000);

		click(doubleClickprodNo.replace("number", productionOrder));

		click(btn_duplicate);

		sendKeys(txt_plannedqty, plannedqtyData);

		String outputProduct1 = getText(txt_outputProduct1);
		String outputProduct2 = getText(txt_outputProduct2);

		if (outputProduct1.equals(outputProduct2)) {
			writeTestResults("output product values should be equal", "Should be able to equal output product values",
					"output products are equal", "pass");
		}

		else {
			writeTestResults("output product values should be equal", "Should be able to equal output product values",
					"output products are not equal", "fail");
		}

		Thread.sleep(2000);

		String costingPriority1 = getText(txt_costingPriority1);
		String costingPriority2 = getText(txt_costingPriority2);

		if (costingPriority1.equals(costingPriority2)) {
			writeTestResults("costing priority values should be equal",
					"Should be able to equal costing priority values", "costing priority values are equal", "pass");
		}

		else {
			writeTestResults("costing priority values should be equal",
					"Should be able to equal costing priority values", "costing priority values are not equal", "fail");
		}

		Thread.sleep(2000);

		String prodOrderGroup1 = getText(txt_prodOrderGroup1);
		String prodOrderGroup2 = getText(txt_prodOrderGroup2);

		if (prodOrderGroup1.equals(prodOrderGroup2)) {
			writeTestResults("Product Order Group values should be equal",
					"Should be able to equal product order group values", "product order groups are equal", "pass");
		}

		else {
			writeTestResults("Product Order Group values should be equal",
					"Should be able to equal product order group values", "product order groups are not equal", "fail");
		}

		Thread.sleep(2000);

		String description1 = getText(txt_description1);
		String description2 = getText(txt_description2);

		if (description1.equals(description2)) {
			writeTestResults("Description values should be equal", "Should be able to equal Description values",
					"Description are equal", "pass");
		}

		else {
			writeTestResults("Description values should be equal", "Should be able to equal Description values",
					"Description are not equal", "fail");
		}

		Thread.sleep(2000);

		String prodModel1 = getText(txt_prodModel1);
		String prodModel2 = getText(txt_prodModel2);

		if (prodModel1.equals(prodModel2)) {
			writeTestResults("product model values should be equal", "Should be able to equal product model values",
					"product model are equal", "pass");
		}

		else {
			writeTestResults("product model values should be equal", "Should be able to equal product model values",
					"product model are not equal", "fail");
		}

	}

	/* QA Form level action verification */

	public void navigateToTheLogin() throws Exception {
		openPage(siteURL);

		writeTestResults("Verify that 'Entution' header is available on the page",
				"user should be able to see the logo", "logo is dislayed", "pass");

	}

	public void verifyThelogo() throws Exception {
		if (isDisplayed(siteLogo)) {

			writeTestResults("Verify that 'Entution' header is available on the page",
					"user should be able to see the logo", "logo is dislayed", "pass");
		} else {
			writeTestResults("Verify that 'Entution' header is available on the page",
					"user should be able to see the logo", "logo is not dislayed", "fail");

		}
	}

	public void userlogin() throws Exception {
		sendKeys(txt_username, UserNameData);

		sendKeys(txt_password, PasswordData);

		click(btn_login);

//		Thread.sleep(5000);
//		driver.switchTo().alert().dismiss();

		if (isDisplayed(lnk_home)) {

			writeTestResults("User login", "User should navigate to the Home page of Entution",
					"user navigated to the home page", "pass");
		}

		else {
			writeTestResults("User login", "User should navigate to the Home page of Entution",
					"user cant navigate to the home page", "fail");

		}
	}

	public void navigateTosideBar() throws Exception {

		click(navigateMenu);

		if (isDisplayed(sideNavBar)) {

			writeTestResults("Verify that user can successfully navigate to side menu",
					"User successfully navigate to side menu", "Side menu displayed", "pass");

		} else {

			writeTestResults("Verify that user can successfully navigate to side menu",
					"User can't successfully navigate to side menu", "Side menu is not displayed", "fail");
		}
	}

	public void navigateToProductionmenu() throws Exception {

		click(btn_Production);

		if (isDisplayed(logo_Production)) {

			writeTestResults("Verify that user can view fixed asset logo", "User can view the logo", "logo viewed",
					"pass");

		}

		else {

			writeTestResults("Verify that  user can view fixed asset logo", "User can't view the logo",
					"logo is not viewed", "fail");
		}

	}

	/* Con_TC_003 */

	// Verify that user can create a product level BOM

	public void navigatetoInventory() throws Exception {
		
		click(navigateMenu);
		click(btn_inventory);

		if (isDisplayed(txt_inventory)) {

			writeTestResults("Verify that user can view inventory label", "User can view the logo",
					"Inventory Label viewed", "pass");

		} else {

			writeTestResults("Verify that user can view inventory label", "User can view the logo",
					"Inventory Label doesn't viewed", "fail");

		}
	}

	public void navigatetoBillOfMaterial() throws Exception {

		click(btn_billOfMaterial);

		if (isDisplayed(label_billOfMaterial)) {

			writeTestResults("Verify that user can view bill of material logo", "User can view the logo",
					"Logo displayed", "pass");
		}

		else {

			writeTestResults("Verify that user can view bill of material logo", "User can view the logo",
					"Logo can't be displayed", "fail");
		}
	}

	public void navigatetonewBOM() throws Exception {

		click(btn_newBOM);

		if (isDisplayed(txt_newBOM)) {

			writeTestResults("Verify that the user can navigate to new BOM", "New bill of material page displayed",
					"new BOM page loaded", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to new BOM", "New bill of material page displayed",
					"new BOM page not loaded", "fail");
		}
	}

	public void fillingBOMform() throws Exception {

		sendKeys(txt_desc, descData);

		selectText(btn_productGrp, productGrpData);

		// Select output Product
		click(btn_outputProductSearch);

		Thread.sleep(2000);

		// sendKeys(txt_outputProductSearch, outputProductData);
		sendKeys(txt_outputProductSearch, productCodeData);

		pressEnter(txt_outputProductSearch);

		Thread.sleep(2000);
		doubleClick(doubleClickoutputProduct);

		sendKeys(txt_BOMQuantity, BOMQuantityData);

		click(btn_expiryDate);

		click(clickExpiryDate);

		click(btn_rawMaterial);

		Thread.sleep(5000);

		sendKeys(txt_rawMaterial, rawMaterialData);

		Thread.sleep(5000);

		pressEnter(txt_rawMaterial);

		Thread.sleep(4000);

		doubleClick(doubleClickrawMaterial);

		sendKeys(txt_rawQuantity, rawQuantityData);

		if (isDisplayed(txt_newBOM)) {

			writeTestResults(
					"Verify that the user can navigate to new BOMUser selected the raw materials under the raw materials section should be displayed successfully",
					"raw materials section should be displayed", "raw materials section displayed", "pass");
		} else {

			writeTestResults(
					"User selected the raw materials under the raw materials section should be displayed successfully",
					"raw materials section shoul be displayed", "raw materials section not displayed", "fail");
		}

	}

	public void completenreleaseBOM() throws Exception {

		click(btn_draft);
		Thread.sleep(2000);
		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the bill of materials successfully",
					"bill of materials drafted", "bill of materials drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the bill of materials successfully",
					"bill of materials drafted", "bill of materials not drafted", "fail");
		}

		Thread.sleep(2000);
		
		click(btn_action);
		Thread.sleep(2000);
		click(btn_Delete);
		Thread.sleep(2000);
		click(btn_yes);

		if (isDisplayed(txt_delete)) {

			writeTestResults("Verify that the user can delete the bill of materials successfully",
					"bill of materials deleted", "bill of materials deleted", "pass");
		} else {

			writeTestResults("Verify that the user can release the bill of materials successfully",
					"bill of materials deleted", "bill of materials not deleted", "fail");
		}

		click(btn_duplicate);

		if (isDisplayed(txt_duplicateBOM)) {

			writeTestResults("Verify that the user can duplicate the bill of materials successfully",
					"bill of materials duplicated", "bill of materials duplicated", "pass");
		} else {

			writeTestResults("Verify that the user can duplicate the bill of materials successfully",
					"bill of materials duplicated", "bill of materials not duplicated", "fail");
		}

		sendKeys(txt_prodGrop, productGrpData);

		click(btn_draft);

		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the bill of materials successfully",
					"bill of materials drafted", "bill of materials drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the bill of materials successfully",
					"bill of materials drafted", "bill of materials not drafted", "fail");
		}

		click(btn_edit);

		if (isDisplayed(txt_editBOM)) {

			writeTestResults("Verify that the user can edit the bill of materials successfully",
					"bill of materials edited", "bill of materials edited", "pass");
		} else {

			writeTestResults("Verify that the user can edit the bill of materials successfully",
					"bill of materials edited", "bill of materials not edited", "fail");
		}

		sendKeys(txt_prodGrop, productGrpData);

		click(btn_updateNNew);

		if (isDisplayed(txt_updateNNewBOM)) {

			writeTestResults("Verify that the user can update the bill of materials and create new successfully",
					"bill of materials updated and create a new", "bill of materials updated and create a new", "pass");
		} else {

			writeTestResults("Verify that the user can update the bill of materials and create new successfully",
					"bill of materials updated and create a new", "bill of materials not updated and create a new",
					"fail");
		}

		fillingBOMForm();

		click(btn_draft);

		Thread.sleep(2000);

		click(btn_release);

	}

	/* edit,update,draft,release,duplicate,draftNnew,CopyFrom */
	public void editNUpdateBOM() throws Exception {

		click(btn_draft);

		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the bill of materials successfully",
					"bill of materials drafted", "bill of materials drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the bill of materials successfully",
					"bill of materials drafted", "bill of materials not drafted", "fail");
		}

		String number = getText(txt_num);

		Thread.sleep(2000);

		click(btn_edit);

		if (isDisplayed(txt_editBOM)) {

			writeTestResults("Verify that the user can edit the bill of materials successfully",
					"bill of materials edited", "bill of materials edited", "pass");
		} else {

			writeTestResults("Verify that the user can edit the bill of materials successfully",
					"bill of materials edited", "bill of materials not edited", "fail");
		}

		sendKeys(txt_prodGrop, productGrpData);

		click(btn_Update);

		if (isDisplayed(txt_updateBOM)) {

			writeTestResults("Verify that the user can update the bill of materials successfully",
					"bill of materials updated", "bill of materials updated", "pass");
		} else {

			writeTestResults("Verify that the user can update the bill of materials successfully",
					"bill of materials updated", "bill of materials not updated", "fail");
		}

		click(btn_duplicate);

		if (isDisplayed(txt_duplicateBOM)) {

			writeTestResults("Verify that the user can duplicate the bill of materials successfully",
					"bill of materials duplicated", "bill of materials duplicated", "pass");
		} else {

			writeTestResults("Verify that the user can duplicate the bill of materials successfully",
					"bill of materials duplicated", "bill of materials not duplicated", "fail");
		}

		sendKeys(txt_prodGrop, productGrpData);

		click(btn_draftNNew);

		if (isDisplayed(txt_draftNNewBOM)) {

			writeTestResults("Verify that the user can draft the bill of materials and create new successfully",
					"bill of materials drafted and create a new", "bill of materials drafted and create a new", "pass");
		} else {

			writeTestResults("Verify that the user can draft the bill of materials and create new successfully",
					"bill of materials drafted and create a new", "bill of materials not drafted and create a new",
					"fail");
		}

		fillingBOMForm();

		click(btn_copyFrom);

		if (isDisplayed(txt_BOm)) {

			writeTestResults("Verify that the user can take a copy from the previously created BOM",
					"take a copy from the previously created BOM", "take a copy from the previously created BOM",
					"pass");
		} else {

			writeTestResults("Verify that the user can take a copy from the previously created BOM",
					"take a copy from the previously created BOM", "can't take a copy from the previously created BOM",
					"fail");
		}

		Thread.sleep(2000);

		sendKeys(txt_previousBOM, number);

		Thread.sleep(2000);

		pressEnter(txt_previousBOM);
		Thread.sleep(2000);

		doubleClick(doublePreviousBOM);

		Thread.sleep(2000);

		click(btn_draft);

		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the bill of materials successfully",
					"bill of materials drafted", "bill of materials drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the bill of materials successfully",
					"bill of materials drafted", "bill of materials not drafted", "fail");
		}

		Thread.sleep(3000);

		click(btn_release);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(txt_BOM)) {

			writeTestResults("Verify that the user can release the bill of materials successfully",
					"bill of materials released", "bill of materials released", "pass");
		} else {

			writeTestResults("Verify that the user can release the bill of materials successfully",
					"bill of materials released", "bill of materials not released", "fail");
		}

		click(btn_action);
		Thread.sleep(2000);
		click(btn_hold);

		if (isDisplayed(txt_changeStatus)) {

			writeTestResults("Verify that the user can hold the bill of materials successfully",
					"bill of materials hold", "bill of materials hold", "pass");
		} else {

			writeTestResults("Verify that the user can hold the bill of materials successfully",
					"bill of materials hold", "bill of materials not hold", "fail");
		}

		sendKeys(txt_reason, reasonData);
		Thread.sleep(2000);

		click(btn_ok);
		Thread.sleep(2000);

		click(btn_action);
		Thread.sleep(2000);

		click(btn_unHold);

		if (isDisplayed(txt_changeStatus)) {

			writeTestResults("Verify that the user can unhold the bill of materials successfully",
					"bill of materials unhold", "bill of materials unhold", "pass");
		} else {

			writeTestResults("Verify that the user can unhold the bill of materials successfully",
					"bill of materials unhold", "bill of materials not unhold", "fail");
		}

		sendKeys(txt_reason, reasonData1);

		click(btn_ok);
		Thread.sleep(2000);
		click(btn_action);
		Thread.sleep(2000);
		click(btn_newVersion);

		String valid = getText(txt_validMsg);
		if (valid.equals("successfully generate a new version .....")) {

			writeTestResults("Verify that the user can create a new version of bill of materials successfully",
					"created new version of bill of materials ", "created new version of bill of materials ", "pass");
		} else {

			writeTestResults("Verify that the user can create a new version of bill of materials successfully",
					"created new version of bill of materials ", "not created new version of bill of materials ",
					"fail");
		}

		Thread.sleep(2000);

		click(btn_action);
		Thread.sleep(2000);
		click(btn_reminder);

		Thread.sleep(3000);

		click(btn_updateReminder);

		Thread.sleep(2000);

		writeTestResults("Verify that the user can createReminder for the bill of materials successfully",
				"bill of materials create reminder viewed", "bill of materials create reminder viewed", "pass");

		click(btn_action);
		Thread.sleep(2000);
		click(btn_history);

		if (isDisplayed(txt_history)) {

			writeTestResults("Verify that the user can view history of the bill of materials successfully",
					"bill of materials history viewed", "bill of materials history viewed", "pass");
		} else {

			writeTestResults("Verify that the user can unhold the bill of materials successfully",
					"bill of materials history viewed", "bill of materials history not viewed", "fail");
		}

		Thread.sleep(2000);

		click(btn_action);
		Thread.sleep(2000);
		click(btn_reverse);
		Thread.sleep(2000);
		click(btn_yes);

		if (isDisplayed(txt_reverse)) {

			writeTestResults("Verify that the user can reverse the bill of materials successfully",
					"bill of materials reverse", "bill of materials reverse", "pass");
		} else {

			writeTestResults("Verify that the user can reverse the bill of materials successfully",
					"bill of materials reverse", "bill of materials not reverse", "fail");
		}

	}

	/* Con_TC_006 */

	// Verify whether user can fill summary data of bill of operations successully

	public void navigateToProduction1() throws Exception {

		click(btn_production);
		Thread.sleep(5000);
		if (isDisplayed(txt_production)) {

			writeTestResults("Verify that the user can navigate to Production page",
					"The Bill of operations by page should be loaded successfully", "Production page loaded", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to Production page",
					"The Bill of operations by page should be loaded successfully", "Production page not loaded",
					"fail");
		}
	}

	public void navigateToBillOfOperation1() throws Exception {

		click(btn_BOO);

		if (isDisplayed(txt_BOO)) {

			writeTestResults("Verify that the user can navigate to Bill of operation by page",
					"The New bill of operations page should be loaded successfully", "Bill Of Operation page loaded",
					"pass");
		} else {

			writeTestResults("Verify that the user can navigate to Bill of operation by page",
					"The New bill of operations page should be loaded successfully",
					"Bill Of Operation page not loaded", "fail");
		}
	}

	public void navigateTonewBOO1() throws Exception {
		click(btn_newBOO);

		if (isDisplayed(txt_newBOO)) {

			writeTestResults("Verify that the user can navigate to new Bill of operation by page",
					"The New bill of operations page should be loaded successfully",
					"new Bill Of Operation page loaded", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to new Bill of operation by page",
					"The New bill of operations page should be loaded successfully",
					"new Bill Of Operation page not loaded", "fail");
		}
	}

	public void fillBOOForm1() throws Exception {

		Random rand = new Random();
		int rand_int1 = rand.nextInt(100000000);

		BOOCodeData = BOOCodeData + Integer.toString(rand_int1);

		sendKeys(txt_BOOCode, BOOCodeData);

		// Description
		click(btn_descriptionBOO);

		sendKeys(txt_descrBOO, BOODescrData);

		click(btn_BOOApply);

		if (isDisplayed(txt_BOOLogo)) {

			writeTestResults("Verify that the user can navigate to Bill of operation by page",
					"The bill of operations page should be loaded successfully", "Bill Of Operation page loaded again",
					"pass");
		} else {

			writeTestResults("Verify that the user can navigate to Bill of operation by page",
					"The bill of operations page should be loaded successfully",
					"Bill Of Operation page not loaded again", "fail");
		}
	}

	/* Con_TC_007 */
	// Verify whether user can add initail operation as a supply under the bill of
	// operations details

	public void OperationActivitySupply1() throws Exception {

		click(btn_plusBOOSupply);

		selectText(txt_ElementCategory, ElementCategoryData);

		sendKeys(txt_ElementDescription, ElementDescData);

		//click(btn_applyOperation);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$('.dialogbox').nextAll().find('.pic16-apply').click()");
		if (isDisplayed(label_operationActivity)) {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"Supply operation should be added successfully", "operation/activity page loaded again", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"Supply operation should be added successfully", "operation/activity page not loaded again",
					"fail");
		}

	}
	/* Con_TC_008 */
	// Verify whether user can add next operation as a production under the bill of
	// operations details

	public void OperationActivityOperation1() throws Exception {

		click(btn_plusBOOOperation);

		selectIndex(txt_ElementCategoryOperation, 1);

		sendKeys(txt_GroupId, GroupIdData);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		//click(btn_applyOperation);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$('.dialogbox').nextAll().find('.pic16-apply').click()");

		if (isDisplayed(label_operationActivity)) {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page loaded again", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page not loaded again", "fail");
		}

	}

	/* Con_TC_009 */
	// Verify whether user can add final operation as a QC under the bill of
	// operations details

	public void QCUnderTheBillOfOperations1() throws Exception {

		click(btn_plusBOOQCBillOfOperation);
		Thread.sleep(2000);
		selectIndex(txt_ElementCategoryOperation, 1);
		Thread.sleep(2000);
		selectIndex(txt_activityType, 1);
		Thread.sleep(2000);
		click(txt_FinalInvestigation);
		Thread.sleep(2000);
		sendKeys(txt_GroupId, GroupIdDataQC);
		Thread.sleep(2000);
		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);
		
//		click(btn_applyOperation);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$('.dialogbox').nextAll().find('.pic16-apply').click()");
		

	}

	public void completeNReleaseBOO() throws Exception {

		click(btn_draft);

		if (isDisplayed(label_operationActivity)) {

			writeTestResults("Verify that the user can draft the BOO", "BOO should be drafted successfully",
					"BOO drafted", "pass");
		} else {

			writeTestResults("BOO should be drafted successfully", "BOO should be drafted successfully",
					"BOO not drafted", "fail");
		}

		Thread.sleep(2000);

		click(btn_action);

		click(btn_Delete);

		click(btn_yes);

		if (isDisplayed(txt_delete)) {

			writeTestResults("Verify that the user can delete the bill of operations successfully",
					"bill of operations deleted", "bill of operations deleted", "pass");
		} else {

			writeTestResults("Verify that the user can delete the bill of operations successfully",
					"bill of operations deleted", "bill of operations not deleted", "fail");
		}

		click(btn_duplicate);

		if (isDisplayed(txt_duplicateBOM)) {

			writeTestResults("Verify that the user can duplicate the bill of operations successfully",
					"bill of operations duplicated", "bill of operations duplicated", "pass");
		} else {

			writeTestResults("Verify that the user can duplicate the bill of operations successfully",
					"bill of operations duplicated", "bill of operations not duplicated", "fail");
		}

		sendKeys(txt_BOOCode, BOOCodeData);

		click(btn_draft);

		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the bill of operations successfully",
					"bill of operations drafted", "bill of operations drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the bill of operations successfully",
					"bill of operations drafted", "bill of operations not drafted", "fail");
		}

		click(btn_edit);

		if (isDisplayed(txt_editBOM)) {

			writeTestResults("Verify that the user can edit the bill of operations successfully",
					"bill of operations edited", "bill of operations edited", "pass");
		} else {

			writeTestResults("Verify that the user can edit the bill of operations successfully",
					"bill of operations edited", "bill of operations not edited", "fail");
		}

		sendKeys(txt_BOOCode, BOOCodeData);

		click(btn_updateNNew);

		if (isDisplayed(txt_updateNNewBOM)) {

			writeTestResults("Verify that the user can update the bill of operations and create new successfully",
					"bill of operations updated and create a new", "bill of operations updated and create a new",
					"pass");
		} else {

			writeTestResults("Verify that the user can update the bill of operations and create new successfully",
					"bill of operations updated and create a new", "bill of operations not updated and create a new",
					"fail");
		}

		fillBOOForm1();
		OperationActivitySupply1();
		OperationActivityOperation1();
		QCUnderTheBillOfOperations1();

		click(btn_draft);

		Thread.sleep(2000);

		click(btn_release);

	}

	/* edit,update,draft,release,duplicate,draftNnew,CopyFrom BOO */
	public void editNUpdateBOO() throws Exception {

		click(btn_draft);

		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the bill of operations successfully",
					"bill of operations drafted", "bill of operations drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the bill of operations successfully",
					"bill of operations drafted", "bill of operations not drafted", "fail");
		}

		Thread.sleep(2000);

		click(btn_edit);

		if (isDisplayed(txt_editBOM)) {

			writeTestResults("Verify that the user can edit the bill of operations successfully",
					"bill of operations edited", "bill of operations edited", "pass");
		} else {

			writeTestResults("Verify that the user can edit the bill of operations successfully",
					"bill of operations edited", "bill of operations not edited", "fail");
		}

		sendKeys(txt_BOOCode, BOOCodeData);

		click(btn_Update);

		if (isDisplayed(txt_updateBOM)) {

			writeTestResults("Verify that the user can update the bill of operations successfully",
					"bill of operations updated", "bill of operations updated", "pass");
		} else {

			writeTestResults("Verify that the user can update the bill of operations successfully",
					"bill of operations updated", "bill of operations not updated", "fail");
		}

		click(btn_duplicate);

		if (isDisplayed(txt_duplicateBOO)) {

			writeTestResults("Verify that the user can duplicate the bill of operations successfully",
					"bill of operations duplicated", "bill of operations duplicated", "pass");
		} else {

			writeTestResults("Verify that the user can duplicate the bill of operations successfully",
					"bill of operations duplicated", "bill of operations not duplicated", "fail");
		}

		Thread.sleep(2000);

		Random rand = new Random();
		int rand_int1 = rand.nextInt(1000000);

		BOOCodeData1 = BOOCodeData1 + Integer.toString(rand_int1);

		sendKeys(txt_BOOCode, BOOCodeData1);

		click(btn_draftNNew);

		if (isDisplayed(txt_draftNNewBOM)) {

			writeTestResults("Verify that the user can draft the bill of operations and create new successfully",
					"bill of operations drafted and create a new", "production model drafted and create a new", "pass");
		} else {

			writeTestResults("Verify that the user can draft the bill of operations and create new successfully",
					"bill of operations drafted and create a new", "bill of operations not drafted and create a new",
					"fail");
		}

		fillBOOForm1();

		click(btn_copyFrom);

		if (isDisplayed(txt_Boo)) {

			writeTestResults("Verify that the user can take a copy from the previously created bill of operations",
					"take a copy from the previously created bill of operations",
					"take a copy from the previously created bill of operations", "pass");
		} else {

			writeTestResults("Verify that the user can take a copy from the previously created bill of operations",
					"take a copy from the previously created bill of operations",
					"can't take a copy from the previously created bill of operations", "fail");
		}

		Thread.sleep(2000);

		sendKeys(txt_previousBOO, BOOCodeData1);

		pressEnter(txt_previousBOO);
		Thread.sleep(2000);

		doubleClick(doublePreviousBOO);

		Thread.sleep(2000);

		sendKeys(txt_BOOCode, BOOCodeData);

		Thread.sleep(2000);

		click(btn_draft);

		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the bill of operations successfully",
					"bill of operations drafted", "bill of operations drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the bill of operations successfully",
					"bill of operations drafted", "bill of operations not drafted", "fail");
		}

		Thread.sleep(3000);

		click(btn_release);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(txt_BOOAction)) {

			writeTestResults("Verify that the user can release the bill of operations successfully",
					"bill of operations released", "bill of operations released", "pass");
		} else {

			writeTestResults("Verify that the user can release the bill of operations successfully",
					"bill of operations released", "bill of operations not released", "fail");
		}

		Thread.sleep(2000);

		click(btn_action);

		click(btn_history);

		if (isDisplayed(txt_history)) {

			writeTestResults("Verify that the user can view history of the bill of operations successfully",
					"bill of operations history viewed", "bill of operations history viewed", "pass");
		} else {

			writeTestResults("Verify that the user can unhold the bill of operations successfully",
					"bill of operations history viewed", "bill of operations history not viewed", "fail");
		}

		Thread.sleep(2000);

		click(btn_action);

		click(btn_reverse);

		click(btn_yes);

		if (isDisplayed(txt_reverse)) {

			writeTestResults("Verify that the user can reverse the bill of operations successfully",
					"bill of operations reverse", "bill of operations reverse", "pass");
		} else {

			writeTestResults("Verify that the user can reverse the bill of operations successfully",
					"bill of operations reverse", "bill of operations not reverse", "fail");
		}

		Thread.sleep(2000);

		click(btn_action);

		click(btn_activities);

		sendKeys(txt_subject, subjectData);

		click(btn_assignTo);

		sendKeys(txt_AssignTo, AssignToData);

		pressEnter(txt_AssignTo);

		Thread.sleep(2000);

		doubleClick(doubleAssignTo);

		click(btn_UpdateTask);

		if (isDisplayed(txt_reverse)) {

			writeTestResults("Verify that the user can view activities of the bill of operations successfully",
					"bill of operations activites displayed", "bill of operations activities displayed", "pass");
		} else {

			writeTestResults("Verify that the user can view activities of the bill of operations successfully",
					"bill of operations activites displayed", "bill of operations not activities displayed", "fail");
		}

	}

	/* Con_TC_015 */
	// Verify whether user can navigate to new production model page successfully

	public void navToProductionModel() throws Exception {

		click(btn_productionModel);

	}

	/* Con_TC_016 */
	// Verify whether user can fill summary data of production model successfully

	public void ProductionModelForm1() throws Exception {

		click(btn_newProductionModel);

		Random rand = new Random();
		int rand_int1 = rand.nextInt(100000);

		modelCodeData = modelCodeData + Integer.toString(rand_int1);

		sendKeys(txt_modelCode, modelCodeData);

		selectText(txt_productGroup, productGroupData);

		click(btn_yes);

		click(btn_product);

		// sendKeys(txt_product, productData);

		sendKeys(txt_product, productCodeData);

		pressEnter(txt_product);

		Thread.sleep(3000);

		doubleClick(doubleClickproduct);

		click(btn_yes);

		click(btn_ProductionUnit);

		sendKeys(txt_ProductionUnitLookup, ProductionUnitData);

		pressEnter(txt_ProductionUnitLookup);

		Thread.sleep(3000);

		doubleClick(doubleClickProductionUnit);

		click(btn_yes);

		if (isDisplayed(txt_newproductionModel)) {

			writeTestResults("Verify whether user can fill summary data of production model successfully",
					"The new production model page should be loaded successfully", "new production model page loaded",
					"pass");
		} else {

			writeTestResults("Verify whether user can fill summary data of production model successfully",
					"The new production model page should be loaded successfully",
					"new production model page not loaded", "fail");
		}
	}

	/* Con_TC_017 */
	// Verify whether user can fill other information of production model with
	// respect to MRP production type successfully

	public void ProductionModelFormOnlyMRPEnable1() throws Exception {

		click(btn_MRPEnabled);

		sendKeys(txt_batchQty, batchQtyData);

		selectIndex(txt_barcodeBook, 1);

		selectIndex(txt_CostingPriority, 1);

		sendKeys(txt_standardCost, standardCostData);

		click(btn_pricingProfile);

		sendKeys(txt_PricingProfile, PricingProfileData);

		pressEnter(txt_PricingProfile);

		doubleClick(doubleClickPricingProfile);

		if (isDisplayed(txt_newproductionModel)) {

			writeTestResults(
					"Verify whether user can fill other information of production model with respect to MRP prodution type successfully",
					"The new production model page filled information of production model with respect to MRP production type successfully",
					"MRP production type production model filled successfully", "pass");
		} else {

			writeTestResults(
					"Verify whether user can fill other information of production model with respect to MRP prodution type successfully",
					"The new production model page filled information of production model with respect to MRP production type successfully",
					"MRP production type production model not filled successfully", "fail");
		}

	}

	/* Con_TC_020 */
	// Verify whether user can search bill of opertaions details successfully
	public void BOODetails1() throws Exception {

		click(btn_BOONoSearch);

		sendKeys(txt_BOOOperation, BOOCodeData);

		pressEnter(txt_BOOOperation);

		doubleClick(doubleClickBOOOperation);

		click(btn_informationOK);

	}

	/* Con_TC_023 */
	// Verify whether user able to add raw materials to production model

	public void BOODetailsTabSupply1() throws Exception {

		click(btn_BOOTab);

		click(btn_Expand);

		click(btn_supply);

		selectIndex(txt_ElementCategoryOperation, 2);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		click(btn_inputProducts);

		click(btn_checkBox);

		click(btn_apply);
	}

	/* Con_TC_024 */
	// Verify whether user able to add production operations to production model
	public void BOODetailsTabOperation1() throws Exception {

		click(btn_operation);

		selectIndex(txt_ElementCategoryOperation, 1);

		sendKeys(txt_GroupId, GroupIdData);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		selectIndex(txt_processTime, 1);

		click(btn_inputProducts);

		click(btn_checkBox);

		click(btn_resources);

		click(btn_resourceSearch);

		sendKeys(txt_additionalResources, additionalResourcesData);

		pressEnter(txt_additionalResources);

		doubleClick(doubleCLickAdditinalResources);

		click(btn_apply);
	}

	/* Con_TC_025, Con_TC_026, Con_TC_027 */
	// Verify whether user able to add QC operations to production model
	public void BOODetailsTabQC1() throws Exception {

		click(btn_QC);

		selectIndex(txt_ElementCategoryOperation, 1);

		selectIndex(txt_activityType, 1);

		sendKeys(txt_GroupId, GroupIdData2);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		selectIndex(txt_processTime, 1);

		click(btn_apply);

		Thread.sleep(3000);

	}

	public void completenreleaseProductionModel() throws Exception {

		click(btn_draft);

		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the production model successfully",
					"production model drafted", "production model drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the bill of materials successfully",
					"production model drafted", "production model not drafted", "fail");
		}

		Thread.sleep(2000);

		click(btn_action);

		click(btn_Delete);

		click(btn_yes);

		if (isDisplayed(txt_delete)) {

			writeTestResults("Verify that the user can delete the production model successfully",
					"bill of materials deleted", "production model deleted", "pass");
		} else {

			writeTestResults("Verify that the user can release the production model successfully",
					"production model deleted", "production model not deleted", "fail");
		}

		click(btn_duplicate);

		if (isDisplayed(txt_duplicateBOM)) {

			writeTestResults("Verify that the user can duplicate the production model successfully",
					"production model duplicated", "production model duplicated", "pass");
		} else {

			writeTestResults("Verify that the user can duplicate the production model successfully",
					"production model duplicated", "production model not duplicated", "fail");
		}

		sendKeys(txt_productionModelCode, modelCodeData);

		click(btn_draft);

		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the production model successfully",
					"production model drafted", "production model drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the production model successfully",
					"production model drafted", "production model not drafted", "fail");
		}

		click(btn_edit);

		if (isDisplayed(txt_editBOM)) {

			writeTestResults("Verify that the user can edit the production model successfully",
					"production model edited", "production model edited", "pass");
		} else {

			writeTestResults("Verify that the user can edit the production model successfully",
					"production model edited", "production model not edited", "fail");
		}

		sendKeys(txt_productionModelCode, modelCodeData);

		BOODetailsTabSupply1();

		BOODetailsTabOperation1();

		BOODetailsTabQC1();

		click(btn_updateNNew);

		if (isDisplayed(txt_updateNNewBOM)) {

			writeTestResults("Verify that the user can update the production model and create new successfully",
					"production model updated and create a new", "production model updated and create a new", "pass");
		} else {

			writeTestResults("Verify that the user can update the production model and create new successfully",
					"production model updated and create a new", "production model not updated and create a new",
					"fail");
		}

		ProductionModelForm1();
		ProductionModelFormOnlyMRPEnable1();
		BOODetails1();
		BOODetailsTabSupply1();
		BOODetailsTabOperation1();
		BOODetailsTabQC1();

		click(btn_draft);

		Thread.sleep(2000);

		click(btn_release);

	}

	/* edit,update,draft,release,duplicate,draftNnew,CopyFrom production model */
	public void editNUpdateProductionModel1() throws Exception {

		click(btn_draft);

		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the production model successfully",
					"production model drafted", "production model drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the production model successfully",
					"production model drafted", "production model not drafted", "fail");
		}

		Thread.sleep(2000);

		click(btn_edit);

		if (isDisplayed(txt_editBOM)) {

			writeTestResults("Verify that the user can edit the production model successfully",
					"production model edited", "production model edited", "pass");
		} else {

			writeTestResults("Verify that the user can edit the production model successfully",
					"production model edited", "production model not edited", "fail");
		}

		sendKeys(txt_BOOCode, BOOCodeData);

		BOODetailsTabSupply1();

		BOODetailsTabOperation1();

		BOODetailsTabQC1();

		click(btn_Update);

		if (isDisplayed(txt_updateBOM)) {

			writeTestResults("Verify that the user can update the production model successfully",
					"production model updated", "production model updated", "pass");
		} else {

			writeTestResults("Verify that the user can update the production model successfully",
					"production model updated", "production model not updated", "fail");
		}

		click(btn_duplicate);

		if (isDisplayed(txt_duplicateBOO)) {

			writeTestResults("Verify that the user can duplicate the production model successfully",
					"production model duplicated", "production model duplicated", "pass");
		} else {

			writeTestResults("Verify that the user can duplicate the production model successfully",
					"production model duplicated", "production model not duplicated", "fail");
		}

		Thread.sleep(2000);

		Random rand = new Random();
		int rand_int1 = rand.nextInt(100000);

		modelCodeData1 = modelCodeData1 + Integer.toString(rand_int1);

		sendKeys(txt_productionModelCode, modelCodeData1);

		click(btn_draftNNew);

		if (isDisplayed(txt_draftNNewBOM)) {

			writeTestResults("Verify that the user can draft the production models and create new successfully",
					"production model drafted and create a new", "production model drafted and create a new", "pass");
		} else {

			writeTestResults("Verify that the user can draft the production model and create new successfully",
					"production model drafted and create a new", "production model not drafted and create a new",
					"fail");
		}
		Thread.sleep(2000);

		ProductionModelForm1();
		ProductionModelFormOnlyMRPEnable1();
		BOODetails1();
		BOODetailsTabSupply1();
		BOODetailsTabOperation1();
		BOODetailsTabQC1();

		click(btn_copyFrom);

		if (isDisplayed(txt_productionModelTab)) {

			writeTestResults("Verify that the user can take a copy from the previously created production model",
					"take a copy from the previously created production model",
					"take a copy from the previously created production model", "pass");
		} else {

			writeTestResults("Verify that the user can take a copy from the previously created BOM",
					"take a copy from the previously created production model",
					"can't take a copy from the previously created production model", "fail");
		}

		Thread.sleep(2000);

		sendKeys(txt_previousmodel, modelCode);

		pressEnter(txt_previousmodel);
		Thread.sleep(2000);

		doubleClick(doublePreviousmodel);

		click(tab_summary);

		Thread.sleep(2000);

		sendKeys(txt_productionModelCode, modelCodeData);

		selectIndex(txt_BillOfMaterial, 0);

		click(btn_yes);

		Thread.sleep(2000);

		click(btn_BOOTab);

		click("//tr[@class='ev']//span[@class='pic16 pic16-plus buttons0']");

		click("//div[@class='doc-detail']//tr[2]//td[7]//span[1]");

		selectIndex(txt_ElementCategoryOperation, 2);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		click(btn_inputProducts);

		click(btn_checkBox);

		click(btn_apply);

		click("//div[@class='doc-detail']//tr[2]//td[7]//span[1]");

		selectIndex(txt_ElementCategoryOperation, 1);

		sendKeys(txt_GroupId, GroupIdData);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		selectIndex(txt_processTime, 1);

		click(btn_inputProducts);

		click(btn_checkBox);

		click(btn_resources);

		click(btn_resourceSearch);

		sendKeys(txt_additionalResources, additionalResourcesData);

		pressEnter(txt_additionalResources);

		doubleClick(doubleCLickAdditinalResources);

		click(btn_apply);

		click("//div[@class='doc-detail']//tr[2]//td[7]//span[1]");

		selectIndex(txt_ElementCategoryOperation, 1);

		selectIndex(txt_activityType, 1);

		sendKeys(txt_GroupId, GroupIdData2);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		click(txt_FinalInvestigation);

		selectIndex(txt_processTime, 1);

		click(btn_apply);

		click(btn_draft);

		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the production model successfully",
					"production model drafted", "production model drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the production model successfully",
					"production model drafted", "production model not drafted", "fail");
		}

		Thread.sleep(3000);

		click(btn_release);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(txt_BOOAction)) {

			writeTestResults("Verify that the user can release the production model successfully",
					"production model released", "production model released", "pass");
		} else {

			writeTestResults("Verify that the user can release the production model successfully",
					"production model released", "production model not released", "fail");
		}

		Thread.sleep(2000);

		click(btn_action);

		click(btn_history);

		if (isDisplayed(txt_history)) {

			writeTestResults("Verify that the user can view history of the production model successfully",
					"production model history viewed", "production model history viewed", "pass");
		} else {

			writeTestResults("Verify that the user can unhold the production model successfully",
					"production model history viewed", "production model history not viewed", "fail");
		}

		Thread.sleep(2000);

	}

	/* Con_TC_028 */
	// Verify whether user can navigate to new production order page successfully
	public void navigateTonewProductionOrderPage1() throws Exception {

		click(btn_productionOrder);

		click(btn_newProductionOrder);

	}

	/* Con_TC_029 */
	// Verify whether user can fill production information data of production order
	// successfully

	public void informationProductionOrder1() throws Exception {

		click(btn_outputProduct);

		Thread.sleep(2000);

		sendKeys(txt_product, productCodeData);

//		sendKeys(txt_product, productData);

		pressEnter(txt_product);

		Thread.sleep(3000);

		doubleClick(doubleClickproduct);

		sendKeys(txt_plannedqty, plannedqtyData);

		selectIndex(txt_CostingPriority, 1);

		click(btn_MRPEnabled);

	}

	/* Con_TC_030, Con_TC_031, Con_TC_032, Con_TC_033 */

	// Verify whether user can fill summary data of production order successfully
	// Verify that user able to run the MRP successfully

	public void fillProductionOrder1() throws Exception {

		selectIndex(txt_productionOrderGroup, 1);

		sendKeys(txt_productionOrderDesc, productionOrderDescData);

		click(btn_productionOrderModel);

		sendKeys(txt_productionOrderModel, modelCodeData);

		pressEnter(txt_productionOrderModel);

		doubleClick(doubleClickOrderModel);

	}

	public void completenreleaseProductionOrder() throws Exception {

		click(btn_draft);

		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the production model successfully",
					"production model drafted", "production model drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the bill of materials successfully",
					"production model drafted", "production model not drafted", "fail");
		}

		Thread.sleep(2000);

		click(btn_action);

		click(btn_Delete);

		click(btn_yes);

		if (isDisplayed(txt_delete)) {

			writeTestResults("Verify that the user can delete the production model successfully",
					"production model deleted", "production model deleted", "pass");
		} else {

			writeTestResults("Verify that the user can release the production model successfully",
					"production model deleted", "production model not deleted", "fail");
		}

		click(btn_duplicate);

		if (isDisplayed(txt_duplicateBOM)) {

			writeTestResults("Verify that the user can duplicate the production model successfully",
					"production model duplicated", "production model duplicated", "pass");
		} else {

			writeTestResults("Verify that the user can duplicate the production model successfully",
					"production model duplicated", "production model not duplicated", "fail");
		}

		sendKeys(txt_plannedQty1, plannedqtyDataver);

		click(btn_draft);

		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the production model successfully",
					"production model drafted", "production model drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the production model successfully",
					"production model drafted", "production model not drafted", "fail");
		}

		click(btn_edit);

		if (isDisplayed(txt_editBOM)) {

			writeTestResults("Verify that the user can edit the production model successfully",
					"production model edited", "production model edited", "pass");
		} else {

			writeTestResults("Verify that the user can edit the production model successfully",
					"production model edited", "production model not edited", "fail");
		}

		sendKeys(txt_plannedQty, plannedqtyData);

		click(btn_updateNNew);

		if (isDisplayed(txt_updateNNewBOM)) {

			writeTestResults("Verify that the user can update the production model and create new successfully",
					"production model updated and create a new", "production model updated and create a new", "pass");
		} else {

			writeTestResults("Verify that the user can update the production model and create new successfully",
					"production model updated and create a new", "production model not updated and create a new",
					"fail");
		}

		informationProductionOrder1();
		fillProductionOrder1();

		click(btn_draft);

		Thread.sleep(2000);

		click(btn_release);

		productionOrder = getText("//*[@id=\"lblTemplateFormHeader\"]");

		click(navigateMenu);

		click(btn_Production);

		click(btn_ProductionControl);

		Thread.sleep(3000);

		sendKeys(txt_searchProductionOrder, productionOrder);

		click(btn_searchProductionOrder);

		Thread.sleep(3000);

		click(btn_checkBoxProductionOrderNo);

		click(btn_action);

		click(btn_RunMRP);

		if (isDisplayed(txt_prodControl)) {

			writeTestResults("Verify that the user can run MRP successfully", "run MRP", "run MRP", "pass");
		} else {

			writeTestResults("Verify that the user can run MRP successfully", "run MRP", "can't run MRP", "fail");
		}

		click(txt_MRPNo);

		click(btn_run);

		click(btn_releaseProductionOrder);

		Thread.sleep(3000);

		click(btn_close);

		click(btn_entutionLogo);

		click(btn_taskEvent);

		click(btn_InternalDispatchOrder);

		Thread.sleep(3000);

		click(btn_arrow);

		Thread.sleep(3000);

		switchWindow();

		sendKeys(txt_shippingAddress, shippingAddressData);

		click(btn_draft);

//		if (isDisplayed(txt_internalDispatchOrderdrafted)) {
//
//			writeTestResults(
//					"Verify that user able to draft the internal dispatch order for production order successfully",
//					"The internal dispatch order should be drafted successfully",
//					"The internal dispatch order drafted", "pass");
//		} else {
//
//			writeTestResults(
//					"Verify that user able to draft the internal dispatch order for production order successfully",
//					"The internal dispatch order should be drafted successfully",
//					"The internal dispatch order not drafted", "fail");
//		}
		Thread.sleep(3000);
		click(btn_editInternal);

		if (isDisplayed(txt_editBOM)) {

			writeTestResults("Verify that the user can edit the production order successfully",
					"internal dispatch order edited", "internal dispatch order edited", "pass");
		} else {

			writeTestResults("Verify that the user can edit the production order successfully",
					"internal dispatch order edited", "internal dispatch order not edited", "fail");
		}

		click(btn_Update);

		if (isDisplayed(txt_update)) {

			writeTestResults(
					"Verify that user able to update the internal dispatch order for production order successfully",
					"internal dispatch order updated", "internal dispatch order updated", "pass");
		} else {

			writeTestResults(
					"Verify that user able to update the internal dispatch order for production order successfully",
					"internal dispatch order updated", "internal dispatch order not updated", "fail");
		}

		Thread.sleep(3000);

		click(btn_release);

		Thread.sleep(4000);
		if (isDisplayed(txt_release)) {

			writeTestResults("Verify that the user can release the internal dispatch order successfully",
					"internal dispatch order released", "internal dispatch order released", "pass");
		} else {

			writeTestResults("Verify that the user can release the internal dispatch order successfully",
					"internal dispatch order released", "internal dispatch order not released", "fail");
		}

		click(btn_action);

		click(btn_dockFlow);

		if (isDisplayed(txt_dockFlow)) {

			writeTestResults("Verify that the user can click dockFlow successfully", "dockFlow clicked",
					"dockFlow clicked", "pass");
		} else {

			writeTestResults("Verify that the user can click dockFlow successfully", "dockFlow clicked",
					"dockFlow not clicked", "fail");
		}

		click(btn_action);

		click(btn_history);

		if (isDisplayed(txt_history)) {

			writeTestResults("Verify that the user can view history of the internal dispatch order successfully",
					"internal dispatch order history viewed", "internal dispatch order history viewed", "pass");
		} else {

			writeTestResults("Verify that the user can view history of the internal dispatch order successfully",
					"internal dispatch order history viewed", "internal dispatch order not history viewed", "fail");
		}

		click(btn_action);

		click(btn_reverse);

		click(btn_yes);

		if (isDisplayed(txt_reverse)) {

			writeTestResults("Verify that the user can reverse the internal dispatch order successfully",
					"internal dispatch order reverse", "internal dispatch order reverse", "pass");
		} else {

			writeTestResults("Verify that the user can reverse the internal dispatch order successfully",
					"internal dispatch order reverse", "internal dispatch order not reverse", "fail");
		}

		click(txt_date);

		click(btn_datenew);

		click(btn_reverseClick);

		click(btn_yes);

		click(btn_action);

		click(btn_journalEntry);

		if (isDisplayed(txt_reverse)) {

			writeTestResults("Verify that the user can view the journal in internal dispatch order successfully",
					"journal entry displayed", "journal entry displayed", "pass");
		} else {

			writeTestResults("Verify that the user can view the journal in internal dispatch order successfully",
					"journal entry displayed", "journal entry not displayed", "fail");
		}

	}

	/* edit,update,draft,release,duplicate,draftNnew,CopyFrom production order */
	public void editNUpdateProductionOrder1() throws Exception {

		click(btn_draft);

		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the production order successfully",
					"production order drafted", "production order drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the production order successfully",
					"production order drafted", "production order not drafted", "fail");
		}

		Thread.sleep(2000);

		click(btn_edit);

		if (isDisplayed(txt_editBOM)) {

			writeTestResults("Verify that the user can edit the production order successfully",
					"production order edited", "production order edited", "pass");
		} else {

			writeTestResults("Verify that the user can edit the production order successfully",
					"production order edited", "production order not edited", "fail");
		}

		selectIndex(txt_productionOrderGroup, 2);

		selectIndex(txt_costingPriority1, 1);

		selectIndex(txt_priority, 1);

		selectIndex(txt_site, 1);

		click(btn_Update);

		if (isDisplayed(txt_updateBOM)) {

			writeTestResults("Verify that the user can update the production order successfully",
					"production order updated", "production order updated", "pass");
		} else {

			writeTestResults("Verify that the user can update the production order successfully",
					"production order updated", "production order not updated", "fail");
		}

		click(btn_duplicate);

		if (isDisplayed(txt_duplicateBOO)) {

			writeTestResults("Verify that the user can duplicate the production order successfully",
					"production order duplicated", "production order duplicated", "pass");
		} else {

			writeTestResults("Verify that the user can duplicate the production order successfully",
					"production order duplicated", "production order not duplicated", "fail");
		}

		Thread.sleep(2000);

		sendKeys(txt_plannedqty, plannedqtyData);

		click(btn_draftNNew);

		if (isDisplayed(txt_draftNNewBOM)) {

			writeTestResults("Verify that the user can draft the production order and create new successfully",
					"production order drafted and create a new", "production order drafted and create a new", "pass");
		} else {

			writeTestResults("Verify that the user can draft the production order and create new successfully",
					"production order drafted and create a new", "production order not drafted and create a new",
					"fail");
		}
		Thread.sleep(2000);

		informationProductionOrder1();

		fillProductionOrder1();

		click(btn_copyFrom);

		if (isDisplayed(txt_productionOrderTab)) {

			writeTestResults("Verify that the user can take a copy from the previously created production order",
					"take a copy from the previously created production order",
					"take a copy from the previously created production order", "pass");
		} else {

			writeTestResults("Verify that the user can take a copy from the previously created production order",
					"take a copy from the previously created production order",
					"can't take a copy from the previously created production order", "fail");
		}

		Thread.sleep(2000);

		sendKeys(txt_previousproductionOrder, productionOrderData);

		pressEnter(txt_previousproductionOrder);
		Thread.sleep(2000);

		doubleClick(doublePreviousProductionOrder);

		sendKeys(txt_plannedqty, plannedqtyData);

		click(btn_draft);

		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the production order successfully",
					"production order drafted", "production order drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the production order successfully",
					"production order drafted", "production order not drafted", "fail");
		}

		Thread.sleep(3000);

		click(btn_release);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(txt_BOOAction)) {

			writeTestResults("Verify that the user can release the production order successfully",
					"production order released", "production order released", "pass");
		} else {

			writeTestResults("Verify that the user can release the production order successfully",
					"production order released", "production order not released", "fail");
		}

		click(btn_action);

		click(btn_hold);

		if (isDisplayed(txt_changeStatus)) {

			writeTestResults("Verify that the user can hold the bill of materials successfully",
					"bill of materials hold", "bill of materials hold", "pass");
		} else {

			writeTestResults("Verify that the user can hold the bill of materials successfully",
					"bill of materials hold", "bill of materials not hold", "fail");
		}

		sendKeys(txt_reason, reasonData);

		click(btn_okHold);

		Thread.sleep(2000);

		click(btn_action);

		click(btn_unHold);

		if (isDisplayed(txt_changeStatus)) {

			writeTestResults("Verify that the user can unhold the bill of materials successfully",
					"bill of materials unhold", "bill of materials unhold", "pass");
		} else {

			writeTestResults("Verify that the user can unhold the bill of materials successfully",
					"bill of materials unhold", "bill of materials not unhold", "fail");
		}

		sendKeys(txt_reason, reasonData1);

		click(btn_okHold);

		Thread.sleep(2000);

		click(btn_action);

		click(btn_jobTechnicalDetail);

		click(btn_apply);

		click("(//span[@class='headerclose'])[last()]");

		click(btn_action);

		click(btn_dockFlow);

		if (isDisplayed(txt_dockFlow)) {

			writeTestResults("Verify that the user can view dock flow successfully", "view dock flow",
					"dock flow displayed", "pass");
		} else {

			writeTestResults("Verify that the user can view dock flow successfully", "view dock flow",
					"dock flow not displayed", "fail");
		}

//		click(btn_action);
//
//		click(btn_generatePackinOrder);
//
//		if (isDisplayed(txt_generatePackingOrder)) {
//
//			writeTestResults("Verify that the user can generate a packing order successfully",
//					"generate a packing order", "packing order generated", "pass");
//		} else {
//
//			writeTestResults("Verify that the user can generate a packing order successfully",
//					"generate a packing order", "packing order not generated", "fail");
//		}
//
//		informationProductionOrder1();
//
//		fillProductionOrder1();

//		click(btn_action);
//
//		click(btn_costEstimation);
//
//		if (isDisplayed(txt_costEstimation)) {
//
//			writeTestResults("Verify that the user can generate a cost estimation successfully",
//					"generate a cost estimation", "cost estimation generated", "pass");
//		} else {
//
//			writeTestResults("Verify that the user can generate a cost estimation successfully",
//					"generate a cost estimation", "cost estimation not generated", "fail");
//		}

		click(btn_action);

		click(btn_reverse);

		click(btn_yes);

		if (isDisplayed(txt_reverse)) {

			writeTestResults("Verify that the user can reverse the bill of operations successfully",
					"bill of operations reverse", "bill of operations reverse", "pass");
		} else {

			writeTestResults("Verify that the user can reverse the bill of operations successfully",
					"bill of operations reverse", "bill of operations not reverse", "fail");
		}

		Thread.sleep(2000);

		click(btn_action);

		click(btn_history);

		if (isDisplayed(txt_history)) {

			writeTestResults("Verify that the user can view history of the production order successfully",
					"production order history viewed", "production order history viewed", "pass");
		} else {

			writeTestResults("Verify that the user can unhold the production order successfully",
					"production order history viewed", "production order history not viewed", "fail");
		}

		Thread.sleep(2000);

	}

	/* Con_TC_051 */
	// Verify that user can successfully navigate to the Bulk Production Form

	public void navigateToBulkProductionForm1() throws Exception {

		click(btn_bulkProductionOrder);

		if (isDisplayed(txt_BulkProduction)) {

			writeTestResults("Verify that user can successfully Open a Bulk Production order",
					" User should be able to Open a Bulk Production order Sucessfully",
					"Bulk Production order form page loaded", "pass");
		} else {

			writeTestResults("Verify that user can successfully Open a Bulk Production order",
					" User should be able to Open a Bulk Production order Sucessfully",
					"Bulk Production order form page not loaded", "fail");
		}

	}

	/* Con_TC_052 */
	// Verify whether user can enter summary information of Bulk Prodution Order

	public void SummaryBulkProductionOrder2() throws Exception {

		click(new_bulkProductionOrder);

		selectIndex(txt_productionOrderGroup, 2);

		sendKeys(txt_descrProductionOrderGroup, productionOrderGroupData);

		selectIndex(txt_referenceType, 1);

		click(btn_yes);

		click(btn_productionUnitBulk);

		sendKeys(txt_productionUnitBulk, productionUnitBulkData);

		pressEnter(txt_productionUnitBulk);

		doubleClick(doubleClickProductionUnitBulk);

		if (isDisplayed(txt_BulkProductionForm)) {

			writeTestResults("Verify whether user can enter summary information of Bulk Prodution Order",
					"Bulk production form should be displayed successfully", "Bulk production form page loaded",
					"pass");
		} else {

			writeTestResults("Verify whether user can enter summary information of Bulk Prodution Order",
					"Bulk production form should be displayed successfully", "Bulk production form page not loaded",
					"fail");
		}

	}

	/* Con_TC_053 */
	// Verify whether user can enter Product informations on Bulk Prodution Order
	public void ProductBulkProductionOrder2() throws Exception {

		click(btn_productTab);

		click(btn_productSearch);

		sendKeys(txt_productSearch, productSearchData);

		pressEnter(txt_productSearch);

		Thread.sleep(3000);

		doubleClick(doubleClickProductSearch);

		Thread.sleep(3000);

		click(btn_productModel);

		sendKeys(txt_productModel, modelData);

		pressEnter(txt_productModel);

		doubleClick(doubleClickProductModel);

		sendKeys(txt_plannedQtyBulk, plannedqtyBulkData);

		sendKeys(txt_standardCost, standardCostData);

		click(btn_checkOut);

		Thread.sleep(10000);

		click(btn_draftProductionOrder);

		if (isDisplayed(txt_draftProductionOrder)) {

			writeTestResults("Verify the production order drafted", "production order drafted successfully",
					"production order drafted successfully", "pass");
		} else {

			writeTestResults("Verify the production order drafted", "production order drafted successfully",
					"production order drafted successfully", "fail");
		}

		Thread.sleep(2000);

		click(btn_action);

		click(btn_Delete);

		click(btn_yes);

		if (isDisplayed(txt_delete)) {

			writeTestResults("Verify that the user can delete the bulk production order successfully",
					"bulk production order deleted", "bulk production order deleted", "pass");
		} else {

			writeTestResults("Verify that the user can release the bulk production order successfully",
					"bulk production order deleted", "bulk production order not deleted", "fail");
		}

		click(btn_duplicate);

		if (isDisplayed(txt_duplicateBOM)) {

			writeTestResults("Verify that the user can duplicate the bulk production order successfully",
					"bulk production order duplicated", "bulk production order duplicated", "pass");
		} else {

			writeTestResults("Verify that the user can duplicate the bulk production order successfully",
					"bulk production order duplicated", "bulk production order not duplicated", "fail");
		}

		click(btn_productTab);

		click(btn_productSearch);

		sendKeys(txt_productSearch, productSearchData);

		pressEnter(txt_productSearch);

		Thread.sleep(3000);

		doubleClick(doubleClickProductSearch);

		Thread.sleep(3000);

		click(btn_productModel);

		sendKeys(txt_productModel, modelData);

		pressEnter(txt_productModel);

		Thread.sleep(2000);

		doubleClick(doubleClickProductModel);

		sendKeys(txt_plannedQtyBulk, plannedqtyBulkData);

		sendKeys(txt_standardCost, standardCostData);

		click(btn_checkOut);

		Thread.sleep(10000);

		click(btn_draft);

		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the bulk production order successfully",
					"bulk production order drafted", "bulk production order drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the bulk production order successfully",
					"bulk production order drafted", "bulk production order not drafted", "fail");
		}

		click(btn_edit);

		if (isDisplayed(txt_editBOM)) {

			writeTestResults("Verify that the user can edit the bulk production order successfully",
					"bulk production order edited", "bulk production order edited", "pass");
		} else {

			writeTestResults("Verify that the user can edit the bulk production order successfully",
					"bulk production order edited", "bulk production order not edited", "fail");
		}

		click(btn_products);

		click(btn_checkOut);

		Thread.sleep(10000);

		click(btn_updateNNew);

		if (isDisplayed(txt_updateNNew)) {

			writeTestResults("Verify that the user can update the bulk production order and create new successfully",
					"bulk production order updated and create a new", "bulk production order updated and create a new",
					"pass");
		} else {

			writeTestResults("Verify that the user can update the bulk production order and create new successfully",
					"bulk production order updated and create a new",
					"bulk production order not updated and create a new", "fail");
		}

		selectIndex(txt_productionOrderGroup, 2);

		sendKeys(txt_descrProductionOrderGroup, productionOrderGroupData);

		selectIndex(txt_referenceType, 1);

		click(btn_yes);

		click(btn_productionUnitBulk);

		sendKeys(txt_productionUnitBulk, productionUnitBulkData);

		pressEnter(txt_productionUnitBulk);

		doubleClick(doubleClickProductionUnitBulk);

		click(btn_productTab);

		click(btn_productSearch);

		sendKeys(txt_productSearch, productSearchData);

		pressEnter(txt_productSearch);

		Thread.sleep(3000);

		doubleClick(doubleClickProductSearch);

		Thread.sleep(3000);

		click(btn_productModel);

		sendKeys(txt_productModel, modelData);

		pressEnter(txt_productModel);

		doubleClick(doubleClickProductModel);

		sendKeys(txt_plannedQtyBulk, plannedqtyBulkData);

		sendKeys(txt_standardCost, standardCostData);

		click(btn_checkOut);

		Thread.sleep(10000);

		click(btn_draftProductionOrder);

		Thread.sleep(2000);

		click(btn_release);

	}

	/*
	 * edit,update,draft,release,duplicate,draftNnew,CopyFrom Bulk production order
	 */

	public void editNUpdateBulkProductionOrder() throws Exception {

		click(new_bulkProductionOrder);

		selectIndex(txt_productionOrderGroup, 2);

		sendKeys(txt_descrProductionOrderGroup, productionOrderGroupData);

		selectIndex(txt_referenceType, 1);

		click(btn_yes);

		click(btn_productionUnitBulk);

		sendKeys(txt_productionUnitBulk, productionUnitBulkData);

		pressEnter(txt_productionUnitBulk);

		doubleClick(doubleClickProductionUnitBulk);

		click(btn_productTab);

		click(btn_productSearch);

		sendKeys(txt_productSearch, productSearchData);

		pressEnter(txt_productSearch);

		Thread.sleep(3000);

		doubleClick(doubleClickProductSearch);

		Thread.sleep(3000);

		click(btn_productModel);

		sendKeys(txt_productModel, modelData);

		pressEnter(txt_productModel);

		doubleClick(doubleClickProductModel);

		sendKeys(txt_plannedQtyBulk, plannedqtyBulkData);

		sendKeys(txt_standardCost, standardCostData);

		click(btn_checkOut);

		Thread.sleep(10000);

		click(btn_draftProductionOrder);

		click(btn_edit);

		if (isDisplayed(txt_editBOM)) {

			writeTestResults("Verify that the user can edit the bulk production order successfully",
					"bulk production order edited", "bulk production order edited", "pass");
		} else {

			writeTestResults("Verify that the user can edit the bulk production order successfully",
					"bulk production order edited", " bulk production order not edited", "fail");
		}

		click(btn_products);

		click(btn_checkOut);

		Thread.sleep(10000);

		click(btn_Update);

		if (isDisplayed(txt_updateBOM)) {

			writeTestResults("Verify that the user can update the bulk production order successfully",
					"bulk production order updated", "bulk production order updated", "pass");
		} else {

			writeTestResults("Verify that the user can update the bulk production order successfully",
					"bulk production order updated", "bulk production order not updated", "fail");
		}

		click(btn_duplicate);

		if (isDisplayed(txt_duplicateBOO)) {

			writeTestResults("Verify that the user can duplicate the bulk production order successfully",
					"bulk production order duplicated", "bulk production order duplicated", "pass");
		} else {

			writeTestResults("Verify that the user can duplicate the bulk production order successfully",
					"bulk production order duplicated", "bulk production order not duplicated", "fail");
		}

		click(btn_products);

		click(btn_checkOut);

		Thread.sleep(10000);

		click(btn_draftNNew);

		Thread.sleep(10000);

		if (isDisplayed(txt_draftNNewBOM)) {

			writeTestResults("Verify that the user can draft the bulk production order and create new successfully",
					"bulk production order drafted and create a new", "bulk production order drafted and create a new",
					"pass");
		} else {

			writeTestResults("Verify that the user can draft the bulk production order and create new successfully",
					"bulk production order drafted and create a new",
					"bulk production order not drafted and create a new", "fail");
		}
		Thread.sleep(2000);

		click(btn_copyFrom);

		Thread.sleep(3000);

		if (isDisplayed(txt_bulkproductionorderTab)) {

			writeTestResults("Verify that the user can take a copy from the previously created bulk production order",
					"take a copy from the previously created bulk production order",
					"take a copy from the previously created bulk production order", "pass");
		} else {

			writeTestResults("Verify that the user can take a copy from the previously created bulk production order",
					"take a copy from the previously created bulk production order",
					"can't take a copy from the previously created bulk production order", "fail");
		}

		sendKeys(txt_previousBulkOrder, previousbulkOrderData);

		pressEnter(txt_previousBulkOrder);

		Thread.sleep(2000);

		doubleClick(doublePreviousbulkOrder);

		click(btn_products);

		click(btn_checkOut);

		Thread.sleep(10000);

		click(btn_draft);

		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the bulk production order successfully",
					"bulk production order drafted", "bulk production order drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the bulk production order successfully",
					"bulk production order drafted", "bulk production order not drafted", "fail");
		}

		Thread.sleep(3000);

		click(btn_release);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(txt_BOOAction)) {

			writeTestResults("Verify that the user can release the bulk production order successfully",
					"bulk production order released", "bulk production order released", "pass");
		} else {

			writeTestResults("Verify that the user can release the bulk production order successfully",
					"bulk production order released", "bulk production order not released", "fail");
		}

		Thread.sleep(2000);

		click(btn_action);

		click(btn_InternalOrder);

		Thread.sleep(3000);

		click(btn_selectAll);

		Thread.sleep(2000);
		click(btn_applyInternal);

		if (isDisplayed(txt_InternalOrder)) {

			writeTestResults("Verify that the user can release internal order successfully", "internal order viewed",
					"internal order viewed", "pass");
		} else {

			writeTestResults("Verify that the user can release internal order successfully", "internal order viewed",
					"internal order not viewed", "fail");
		}

		switchWindow();

		sendKeys(txt_title1, titleData);

		click(btn_requester);

		sendKeys(txt_requester, requesterData);

		pressEnter(txt_requester);

		Thread.sleep(3000);

		doubleClick(doubleClickRequester);

		click(btn_draft);

		Thread.sleep(3000);

		click(btn_release);

		Thread.sleep(2000);

		click(btn_goTopage);

		switchWindow();

		sendKeys(txt_shipping, shippingData);

		click(btn_draft);

		Thread.sleep(4000);

		click(btn_Capture);

		Thread.sleep(4000);

		click(btn_productListClick);

		sendKeys(txt_batchNoTXT, batchData);

		pressEnter(txt_batchNoTXT);

		click(btn_refresh);

		click(btn_back);

		click(btn_release);

		Thread.sleep(3000);

		click(btn_action);

		click(btn_history);

		Thread.sleep(2000);
		if (isDisplayed(txt_history)) {

			writeTestResults("Verify that the user can view history of the bulk production order successfully",
					"bulk production order history viewed", "bulk production order history viewed", "pass");
		} else {

			writeTestResults("Verify that the user can unhold the bulk production order successfully",
					"bulk production order history viewed", "bulk production order history not viewed", "fail");
		}

		Thread.sleep(2000);

		click(btn_action);

		click(btn_dockFlow);

		Thread.sleep(2000);

		if (isDisplayed(txt_dockFlow)) {

			writeTestResults("Verify that the user can view dock flow successfully", "view dock flow",
					"dock flow displayed", "pass");
		} else {

			writeTestResults("Verify that the user can view dock flow successfully", "view dock flow",
					"dock flow not displayed", "fail");
		}

		Thread.sleep(2000);

		click(btn_action);

		click(btn_reverse);

		click(txt_date);

		click(btn_datenew1);

		click(btn_reverseClick);

		click(btn_yes);

		Thread.sleep(2000);
		if (isDisplayed(txt_reverse)) {

			writeTestResults("Verify that the user can reverse the bulk production order successfully",
					"bulk production order reverse", "bulk production order reverse", "pass");
		} else {

			writeTestResults("Verify that the user can reverse the bulk production order successfully",
					"bulk production order reverse", "bulk production order not reverse", "fail");
		}

		Thread.sleep(5000);

		click(btn_action);

		click(btn_journalEntry);

		click(link_gernalEntryDetails);

		Thread.sleep(2000);

		if (isDisplayed(txt_journalEntryReleased)) {

			writeTestResults(" Verify that user able to view journal entry successfully",
					"journal entry should released successfully", "journal entry released successfully", "pass");
		} else {

			writeTestResults("Verify that user able to view journal entry successfully",
					"journal entry should released successfully", "journal entry not released successfully", "fail");
		}

	}

	public void reportProduction() throws Exception {

		click(btn_report);

		click(btn_viewReport);

		if (isDisplayed(txt_viewReport)) {

			writeTestResults(" Verify that user able to view report successfully", "report viewed successfully",
					"report viewed successfully", "pass");
		} else {

			writeTestResults("Verify that user able to view report successfully", "report viewed successfully",
					"report not viewed successfully", "fail");
		}

	}

	public void actionOverheadInformation() throws Exception {

		click(txt_overheadInfo);

		click(btn_new);

		if (isDisplayed(txt_new)) {

			writeTestResults("Verify that user can create a new overhead information successfully",
					"new overhead information created successfully", "new overhead information created successfully",
					"pass");
		} else {

			writeTestResults("Verify that user can create a new overhead information successfully",
					"new overhead information created successfully",
					"new overhead information not created successfully", "fail");
		}

		Random rand = new Random();
		int rand_int2 = rand.nextInt(100000);

		overheadCodeData = overheadCodeData + Integer.toString(rand_int2);

		sendKeys(txt_overheadCode, overheadCodeData);

		sendKeys(txt_descOverhead, overheadDescData);

		selectIndex(txt_overheadGroup, 1);

		selectIndex(txt_rateType, 1);

		click(btn_draftOverhead);

		click(txt_newOverheadCode.replace("code", overheadCodeData));

		click(btn_active);

		click(btn_yes1);

		Thread.sleep(2000);

		if (isDisplayed(txt_active)) {

			writeTestResults(" Verify that user can activate the overhead information successfully",
					"overhead information activated successfully", "overhead information activated successfully",
					"pass");
		} else {

			writeTestResults("Verify that user can activate the overhead information successfully",
					"overhead information activated successfully", "overhead information not activated successfully",
					"fail");
		}

		click(btn_edit);

		if (isDisplayed(txt_edit)) {

			writeTestResults("Verify that user can edit the overhead information successfully",
					"overhead information edit successfully", "overhead information edited successfully", "pass");
		} else {

			writeTestResults("Verify that user can edit the overhead information successfully",
					"overhead information edit successfully", "overhead information not edited successfully", "fail");
		}

		Random rand1 = new Random();
		int rand1_int2 = rand1.nextInt(100000);

		overheadCodeData = overheadCodeData + Integer.toString(rand1_int2);

		sendKeys(txt_overheadCode, overheadCodeData);

		selectIndex(txt_overheadGroup, 3);

		click(btn_update);

		if (isDisplayed(txt_updateOrganization)) {

			writeTestResults("Verify that user can update the overhead information successfully",
					"overhead information update successfully", "overhead information updated successfully", "pass");
		} else {

			writeTestResults("Verify that user can update the overhead information successfully",
					"overhead information update successfully", "overhead information not updated successfully",
					"fail");
		}

		if (isDisplayed(txt_inactive)) {

			writeTestResults("Verify that user can inactivate the overhead information successfully",
					"overhead information inactivated successfully", "overhead information inactivated successfully",
					"pass");
		} else {

			writeTestResults("Verify that user can inactivate the overhead information successfully",
					"overhead information inactivated successfully",
					"overhead information not inactivated successfully", "fail");
		}

		click(btn_inactive);

		Thread.sleep(2000);

		click(btn_yes2);
		Thread.sleep(2000);

	}

	public void deleteOverheadInformation() throws Exception {

		click(txt_overheadInfo);

		click(btn_new);

		if (isDisplayed(txt_new)) {

			writeTestResults("Verify that user can create a new overhead information successfully",
					"new overhead information created successfully", "new overhead information created successfully",
					"pass");
		} else {

			writeTestResults("Verify that user can create a new overhead information successfully",
					"new overhead information created successfully",
					"new overhead information not created successfully", "fail");
		}

		Random rand = new Random();
		int rand_int2 = rand.nextInt(100000);

		overheadCodeData = overheadCodeData + Integer.toString(rand_int2);

		sendKeys(txt_overheadCode, overheadCodeData);

		sendKeys(txt_descOverhead, overheadDescData);

		selectIndex(txt_overheadGroup, 1);

		selectIndex(txt_rateType, 1);

		click(btn_draftOverhead);

		click(txt_newOverheadCode.replace("code", overheadCodeData));

		Thread.sleep(2000);

		if (isDisplayed(txt_deleteAc)) {

			writeTestResults("Verify that user can delete a new overhead information successfully",
					"new overhead information deleted successfully", "new overhead information deleted successfully",
					"pass");
		} else {

			writeTestResults("Verify that user can delete a new overhead information successfully",
					"new overhead information deleted successfully",
					"new overhead information not deleted successfully", "fail");
		}

		click(btn_deleteAc);

		Thread.sleep(2000);

		click(btn_yes4);
		Thread.sleep(2000);

	}

	public void editPricingProfile() throws Exception {
		click(btn_PricingProfile);

		click(btn_newPricingProfile);

		// create instance of Random class
		Random rand = new Random();

		// Generate random integers in range 0 to 999
		int rand_int1 = rand.nextInt(10000);

		pricingProfileCodeData = pricingProfileCodeData + Integer.toString(rand_int1);

		sendKeys(txt_pricingProfileCode, pricingProfileCodeData);

		sendKeys(txt_descPricingProfile, descPricingProfileData);

		selectIndex(txt_pricingGroup, 2);

		click(btn_PricingProfile);

		click(btn_newPricingProfile);

		click(btn_estimate);

		selectIndex(txt_material, 1);
		selectIndex(txt_labour, 1);
		selectIndex(txt_Overhead, 1);
		selectIndex(txt_service, 1);
		selectIndex(txt_expense, 1);

		click(btn_PricingProfile);

		click(btn_newPricingProfile);

		click(btn_actualCalculation);

		selectIndex(txt_materialAC, 2);
		selectIndex(txt_labourAC, 2);
		selectIndex(txt_OverheadAC, 2);
		selectIndex(txt_serviceAC, 2);
		selectIndex(txt_expenseAC, 2);

		Thread.sleep(2000);

		click(btn_draftLocation);

		keyDownClick(txt_Pricing, pricingProfileCodeData);

		Thread.sleep(2000);

		if (isDisplayed(txt_search)) {

			writeTestResults("Verify the Pricing Profile search Successfully", "Pricing Profile searched successfully",
					"Pricing Profile Searched successfully", "pass");
		} else {

			writeTestResults("Verify the Pricing Profile search Successfully", "Pricing Profile searched successfully",
					" Pricing Profile not searched successfullye", "fail");
		}

		click(btn_reset);

		Thread.sleep(2000);

		if (isDisplayed(txt_reset)) {

			writeTestResults("Verify the Pricing Profile reset Successfully", "Pricing Profile reset successfully",
					"Pricing Profile reset successfully", "pass");
		} else {

			writeTestResults("Verify the Pricing Profile reset Successfully", "Pricing Profile reset successfully",
					" Pricing Profile not reset successfully", "fail");
		}

		keyDownClick(txt_Pricing, pricingProfileCodeData);

		click(btn_active);

		click(btn_yes);

		if (isDisplayed(txt_activateStatus)) {

			writeTestResults("Verify the Pricing Profile Status", " Pricing Profile Status should update as Activated ",
					"Pricing Profile Status viewed as active", "pass");
		} else {

			writeTestResults("Verify the Pricing Profile Status", " Pricing Profile Status should update as Activated",
					"Pricing Profile Status not viewed as active", "fail");
		}

		if (isDisplayed(txt_inactive)) {

			writeTestResults("Verify that user can inactivate the Pricing Profile successfully",
					"Pricing Profile inactivated successfully", "Pricing Profile inactivated successfully", "pass");
		} else {

			writeTestResults("Verify that user can inactivate the Pricing Profile successfully",
					"Pricing Profile inactivated successfully", "Pricing Profile not inactivated successfully", "fail");
		}

		click(btn_inactive);

		Thread.sleep(2000);

		click(btn_yes2);
		Thread.sleep(2000);
	}

	public void ActionproductionLabelPrint() throws Exception {

		click(btn_draft);

		click(btn_release);

		productionOrder = getText(txt_productionOrderNo);

		click(navigateMenu);

		click(btn_Production);

		click(btn_ProductionControl);

		Thread.sleep(3000);

		sendKeys(txt_searchProductionOrder, productionOrder);

		click(btn_searchProductionOrder);

		Thread.sleep(3000);

		click(btn_checkBoxProductionOrderNo);

		click(btn_action);

		click(btn_RunMRP);

		click(txt_MRPNo);

		click(btn_run);

		click(btn_releaseProductionOrder);

		Thread.sleep(3000);

		click(btn_close);

		click(btn_entutionLogo);

		click(btn_taskEvent);

		click(btn_InternalDispatchOrder);

		Thread.sleep(3000);

		click(btn_arrow);

		Thread.sleep(3000);

		switchWindow();

		sendKeys(txt_shippingAddress, shippingAddressData);

		click(btn_draft);

		Thread.sleep(2000);

		click(btn_release);

		Thread.sleep(3000);

		click(navigateMenu);

		click(btn_Production);

		Thread.sleep(2000);

		click(btn_productionOrder);

		Thread.sleep(2000);

		sendKeys(txt_prodOrderNo, productionOrder);

		pressEnter(txt_prodOrderNo);

		Thread.sleep(3000);

		click(doubleClickprodNo.replace("number", productionOrder));

		click(btn_action);

		click(btn_shopFloorUpdate);

		Thread.sleep(3000);

		click(btn_start);

		Thread.sleep(2000);

		click(btn_startProcess);

		Thread.sleep(2000);

		click(btn_actual);

		Thread.sleep(4000);
		sendKeys(txt_producedQty, producedData);

		Thread.sleep(2000);

		click(btn_update);

		Thread.sleep(2000);

		click(btn_inputProduct);

		sendKeys(txt_actualQty, actualQtyData);

		click(btn_update);

		Thread.sleep(2000);

		click(btn_closeShop);

		Thread.sleep(2000);

		click(btn_completeTick);

		click(txt_complete);

		click("//span[@class='headerclose']");

		Thread.sleep(3000);

		click(navigateMenu);

		click(btn_Production);

		click(btn_labelPrint);

		click(btn_view);

		selectIndex(txt_batchNo, 1);

		selectIndex(txt_machineNo, 1);

		selectIndex(txt_color, 2);

		click(btn_draftNPrint);
		switchWindow();
		switchWindow();
		click("//*[@id=\"inspect\"]/div[6]/div[1]/span[2]");

	}

	public void QCproductionLabelPrint() throws Exception {

		Thread.sleep(2000);

		click(btn_labelPrint);
		Thread.sleep(2000);
		click(btn_labelPrintHistory);
		Thread.sleep(2000);
		click(btn_productOrder);
		Thread.sleep(2000);
		click(btn_QcComplete);
		
		Thread.sleep(3000);

		click(navigateMenu);
		Thread.sleep(2000);
		click(btn_Production);
		Thread.sleep(2000);
		click(btn_productionOrder);
		Thread.sleep(5000);
		sendKeys(txt_productonOrder, productionOrder);
		Thread.sleep(2000);
		click(doubleClickprodNo.replace("number", productionOrder));
		Thread.sleep(2000);
		click(btn_action);
		Thread.sleep(2000);
		click(btn_productionCosting);
		Thread.sleep(2000);
		click(btn_costingRefresh);
		Thread.sleep(2000);
		click(btn_inputProductsCostingTab);
		Thread.sleep(2000);
		click(btn_updateCosting);
		
		Thread.sleep(2000);

		click(btn_closeCosting);
		Thread.sleep(2000);
		click(btn_tick);
		Thread.sleep(2000);
		click(btn_update);

		Thread.sleep(3000);

		click(btn_internalReceiptControl);
		Thread.sleep(2000);
		click(btn_smallcheckBox);

		Random rand = new Random();

		// Generate random integers in range 0 to 999
		int rand_int1 = rand.nextInt(10000);

		lotNo = lotNo + Integer.toString(rand_int1);

		sendKeys(txt_lotNo, lotNo);

		click(btn_generateReceipt);

		if (isDisplayed(txt_internalReceiptLabel)) {

			writeTestResults("Verify that user can generate the internal receipt successfully",
					"internal receipt generated successfully", "internal receipt generated successfully", "pass");
		} else {

			writeTestResults("Verify that user can generate the internal receipt successfully",
					"internal receipt generated successfully", "internal receipt not generated successfully", "fail");
		}

		click("//span[@class='headerclose']");

		click(btn_action);

		click(btn_dockFlow);

		if (isDisplayed(txt_dockFlow)) {

			writeTestResults("Verify that the user can click dockFlow successfully", "dockFlow clicked",
					"dockFlow clicked", "pass");
		} else {

			writeTestResults("Verify that the user can click dockFlow successfully", "dockFlow clicked",
					"dockFlow not clicked", "fail");
		}

		click(btn_history);

		if (isDisplayed(txt_history)) {

			writeTestResults("Verify that the user can view history of the bill of materials successfully",
					"bill of materials history viewed", "bill of materials history viewed", "pass");
		} else {

			writeTestResults("Verify that the user can unhold the bill of materials successfully",
					"bill of materials history viewed", "bill of materials history not viewed", "fail");
		}

	}

	public void productionParameter() throws Exception {

		click(btn_productionParameter);

		click(btn_inactivePP);
		if (!isSelected(btn_inactivePP)) {
			click(btn_inactivePP);
		}

		if (isSelected(btn_inactivePP)) {

			writeTestResults("Verify that the user can inactive production parameters successfully",
					"production parameters inactivated", "production parameters inactivated", "pass");
		} else {

			writeTestResults("Verify that the user can inactive production parameters successfully",
					"production parameters inactivated", "production parameters not inactivated", "fail");
		}

		Thread.sleep(2000);
		click(btn_UPDATE);

		if (isDisplayed(txt_UPDATE)) {

			writeTestResults("Verify that the user can update production parameters successfully",
					"production parameters updated", "production parameters updated", "pass");
		} else {

			writeTestResults("Verify that the user can update production parameters successfully",
					"production parameters updated", "production parameters not updated", "fail");
		}

	}

	public void deleteproductionCostEstimation() throws Exception {

		click(btn_productcostEstimation);

		click(btn_newCostEstimation);

		sendKeys(txt_costEstDescription, costEstDescription);

		Thread.sleep(3000);
		
		click(btn_costProduct);

		sendKeys(txt_costProduct, costProductData);

		pressEnter(txt_costProduct);
		Thread.sleep(3000);
		doubleClick(costProductDoubleClick);
		Thread.sleep(3000);
		sendKeys(txt_costEstQuantity, costEstQuantityData);
		
		Thread.sleep(3000);

		click(btn_pricingProfileCost);
		Thread.sleep(2000);
		sendKeys(txt_pricingProfileCost, pricingProfileCostData);
		Thread.sleep(3000);
		pressEnter(txt_pricingProfileCost);
		Thread.sleep(2000);
		
		doubleClick(pricingProfileCostDoubleClick);
		
		
		Thread.sleep(6000);
		
		click(btn_checkout);
		
		Thread.sleep(3000);
		
		click(btn_draft);
		Thread.sleep(3000);
		click(btn_action);
		Thread.sleep(3000);
		click(btn_Delete);
		Thread.sleep(3000);
		click(btn_yes);

		if (isDisplayed(txt_delete)) {

			writeTestResults("Verify that the user can delete the bill of materials successfully",
					"bill of materials deleted", "bill of materials deleted", "pass");
		} else {

			writeTestResults("Verify that the user can delete the bill of materials successfully",
					"bill of materials deleted", "bill of materials not deleted", "fail");
		}

		click(btn_duplicate);

		if (isDisplayed(txt_duplicateBOM)) {

			writeTestResults("Verify that the user can duplicate the bill of materials successfully",
					"bill of materials duplicated", "bill of materials duplicated", "pass");
		} else {

			writeTestResults("Verify that the user can duplicate the bill of materials successfully",
					"bill of materials duplicated", "bill of materials not duplicated", "fail");
		}

		sendKeys(txt_prodGrop, productGrpData);
		
		click(btn_checkout);
		
		Thread.sleep(3000);
		
		click(btn_draft);

		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the bill of materials successfully",
					"bill of materials drafted", "bill of materials drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the bill of materials successfully",
					"bill of materials drafted", "bill of materials not drafted", "fail");
		}

		click(btn_edit);

		if (isDisplayed(txt_editBOM)) {

			writeTestResults("Verify that the user can edit the bill of materials successfully",
					"bill of materials edited", "bill of materials edited", "pass");
		} else {

			writeTestResults("Verify that the user can edit the bill of materials successfully",
					"bill of materials edited", "bill of materials not edited", "fail");
		}

		sendKeys(txt_prodGrop, productGrpData);
		
		click(btn_checkout);
		
		Thread.sleep(3000);
		
		click(btn_updateNNew);

		if (isDisplayed(txt_updateNNewBOM)) {

			writeTestResults("Verify that the user can update the bill of materials and create new successfully",
					"bill of materials updated and create a new", "bill of materials updated and create a new", "pass");
		} else {

			writeTestResults("Verify that the user can update the bill of materials and create new successfully",
					"bill of materials updated and create a new", "bill of materials not updated and create a new",
					"fail");
		}

		//fillingBOMForm();
		sendKeys(txt_costEstDescription, costEstDescription);

		Thread.sleep(3000);
		
		click(btn_costProduct);

		sendKeys(txt_costProduct, costProductData);

		pressEnter(txt_costProduct);
		Thread.sleep(3000);
		doubleClick(costProductDoubleClick);
		Thread.sleep(3000);
		sendKeys(txt_costEstQuantity, costEstQuantityData);
		
		Thread.sleep(3000);

		click(btn_pricingProfileCost);
		Thread.sleep(2000);
		sendKeys(txt_pricingProfileCost, pricingProfileCostData);
		Thread.sleep(3000);
		pressEnter(txt_pricingProfileCost);
		Thread.sleep(2000);
		
		doubleClick(pricingProfileCostDoubleClick);
		
		
		Thread.sleep(6000);
		
		click(btn_checkout);
		
		Thread.sleep(3000);
		

		click(btn_draft);

		Thread.sleep(2000);

		click(btn_release);

	}
	
	
	
	
	
}