package bileeta.BTAF.PageObjects;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.sound.midi.Soundbank;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import bileeta.BATF.Pages.FinanceModuleData;

public class FinanceModule extends FinanceModuleData {
	/* Common ReadWriteFinData */
	private static Workbook wb;
	private static Sheet sh;
	private static FileInputStream fis;
	private static FileOutputStream fos;
	private static Row row;
	private static Cell cell;
	private static Cell cell1;
	private static Cell cell2;

	/* Common objects */
	InventoryAndWarehouseModule invObj = new InventoryAndWarehouseModule();

	/* Fin_TC_002 */
	private static String finTC002_inboundPAyment;

	/* Fin_TC_016 */
	private static String finTC016_PONo;

	private static String outbound_payment_no_Fin_TC_018;

	/* Fin_TC_020 */
	private static double doub_un_reconcile_amount_after_select_advice;

	/*--------------START LOGIN------------------*/

	public void navigateToTheLoginPage() throws Exception {
		openPage(site_Url);

	}

	public void verifyTheLogo() throws Exception {
		InventoryAndWarehouseModule obj = new InventoryAndWarehouseModule();
		obj.implisitWait(8);
		Thread.sleep(1000);
		explicitWait(site_logo, 40);
		if (isDisplayed(site_logo)) {

			writeTestResults("Verify that 'Entution' header is available on the page",
					"User should be able to see the logo", "Logo is displayed sucessfully", "pass");
		} else {
			writeTestResults("Verify that 'Entution' header is available on the page",
					"User should be able to see the logo", "Logo does'nt displayed", "fail");

		}
	}

	public void userLogin() throws Exception {
		explicitWait(txt_username, 40);
		sendKeys(txt_username, userNameData);
		Thread.sleep(1000);
		sendKeys(txt_password, passwordData);

		waitImplicit(btn_login, 30);
		Thread.sleep(2000);
		click(btn_login);
		InventoryAndWarehouseModule obj = new InventoryAndWarehouseModule();
		obj.implisitWait(8);
		Thread.sleep(2000);
		explicitWait(navigation_pane, 15);
		if (isDisplayedQuickCheck(div_loginVerification)) {
			openPage(siteURL);
			Thread.sleep(2000);
			customizeLoadingDelay(txt_username, 40);
			sendKeys(txt_username, userNameData);
			Thread.sleep(1000);
			sendKeys(txt_password, passwordData);
			Thread.sleep(2000);
			click(btn_login);
			waitImplicit(navigation_pane, 10);
		}
//			Thread.sleep(6000);
//			driver.switchTo().alert().dismiss();
		obj.customizeLoadingDelay(link_home, 30);
		if (isDisplayed(link_home)) {

			writeTestResults("Verify that user can login to the 'Entution' ", "User should be login to the 'Entution'",
					"User login to the 'Entution' sucessfully", "pass");
		} else {
			writeTestResults("Verify that user can login to the 'Entution' ", "User should be login to the 'Entution'",
					"User does'nt login to the 'Entution'", "fail");

		}
		obj.implisitWait(15);
	}

	/* ---------------END LOGIN------------------- */

	/* Common Method - Implicit Wait */
	public void waitImplicit(String element, int seconds) throws Exception {
		driver.manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);
		if (!isDisplayed(element)) {
			System.out.println("Network speed is low...");
		}

	}

	/* Com_TC_002 */
	public void navigateToTheFananceModule() throws Exception {
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		customizeLoadingDelay(navigation_pane, 20);
		click(navigation_pane);
		explicitWait(btn_financeModule, 10);
		if (isDisplayed(btn_financeModule)) {

			writeTestResults("Verify that navigation menu is open", "User should be able to open navigation menu",
					"Navigation menu successfully open", "pass");
		} else {
			writeTestResults("Verify that navigation menu is open", "User should be able to open navigation menu",
					"Navigation menu does'nt open", "fail");

		}

		click(btn_financeModule);
		explicitWait(btn_bulkChecqu, 40);
		if (isDisplayed(btn_bulkChecqu)) {

			writeTestResults("Verify user can navigate to the Finance Module",
					"User should be navigate to the Finance Module",
					"User successfully naviagate to the Finance Module", "pass");
		} else {
			writeTestResults("Verify user can navigate to the Finance Module",
					"User should be navigate to the Finance Module", "User could'nt naviagate to the Finance Module",
					"fail");

		}

	}

	/* Fin_TC_001 */
	public void navigateToIPAForm() throws Exception {
		click(btn_inboundPaymentAdvice);
		customizeLoadingDelay(btn_newInboundPaymentAdvice, 10);
		if (isDisplayed(btn_newInboundPaymentAdvice)) {

			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User successfully navigate to Inbound payment advice by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User could'nt navigate to Inbound payment advice by-page", "fail");
		}

		click(btn_newInboundPaymentAdvice);
		explicitWait(btn_CusAdvanceJourneyIPA, 60);
		if (isDisplayed(btn_CusAdvanceJourneyIPA)) {

			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User successfully view the list of journey",
					"pass");
		} else {
			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User could'nt view the list of journey",
					"fail");
		}

		click(btn_CusAdvanceJourneyIPA);
		if (isDisplayed(lnl_postBusinessUnitIPA)) {

			writeTestResults("Verify user can navigate to new customer advance form",
					"User should navigate to new customer advance form",
					"User successfully navigate to new customer advance form", "pass");
		} else {
			writeTestResults("Verify user can navigate to new customer advance form",
					"User should navigate to new customer advance form",
					"User could'nt navigate to new customer advance form", "fail");
		}
	}

	public void fillIPAFormDetails() throws Exception {
		Thread.sleep(1000);
		click(lnl_postBusinessUnitIPA);
		Thread.sleep(700);
		selectText(drop_postBusinessUnit, postBusinessUnitIPA);
		Thread.sleep(700);
		click(tab_subbaryIPA);
		Thread.sleep(2000);
		String get_post_business_unit = getText(lnl_postBusinessUnitIPA);
		System.out.println(get_post_business_unit);
		if (get_post_business_unit.equals(postBusinessUnitIPA)) {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User successfully select post business unit from the drop down", "pass");
		} else {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User could'nt select post business unit from the drop down", "fail");
		}

		click(btn_docDateIPA);
		click(btn_calenderBackIAP);
		click(btn_calenderBackIAP);
		click("25_Link");

		click(btn_dueDateIPA);
		click(btn_calenderForwardIAP);
		click(btn_calenderForwardIAP);
		click("25_Link");

		String doc_date = getText(btn_docDateIPA).replaceAll("/", "");
		String post_date = getText(btn_postDateIPA).replaceAll("/", "");
		String due_date = getText(btn_dueDateIPA).replaceAll("/", "");

		String doc_date1 = reverseStringDateToSLOrder(doc_date);
		String post_date1 = reverseStringDateToSLOrder(post_date);
		String due_date1 = reverseStringDateToSLOrder(due_date);

		int doc_date2 = Integer.parseInt(doc_date1);
		int post_date2 = Integer.parseInt(post_date1);
		int due_date2 = Integer.parseInt(due_date1);

		if (doc_date2 < post_date2 && post_date2 < due_date2) {
			writeTestResults("Verify user can select document date, post date and due date",
					"User should be able to select document date, post date and due date",
					"User successfully select document date, post date and due date", "pass");
		} else {

			writeTestResults("Verify user can select document date, post date and due date",
					"User should be able to select document date, post date and due date",
					"User could'nt select document date, post date and due date", "fail");
		}

		click(btn_searchCustomerIPA);
		if (isDisplayed(txt_cusAccountSearchIPA)) {
			writeTestResults("Verify user can click on Search button in Customer and Customer Lookup should pop up",
					"User should be click on Search button in Customer and Customer Lookup should pop up",
					"User successfully click on Search button in Customer and Customer Lookup should pop up", "pass");
		} else {

			writeTestResults("Verify user can click on Search button in Customer and Customer Lookup should pop up",
					"User should be click on Search button in Customer and Customer Lookup should pop up",
					"User could'nt click on Search button in Customer and Customer Lookup should pop up", "fail");
		}

		sleepCusomized(txt_cusAccountSearchIPA);
		sendKeys(txt_cusAccountSearchIPA, customerAccountIPA);
		pressEnter(txt_cusAccountSearchIPA);
		sleepCusomized(lnk_resultCustomerAccountIPA);
		if (isDisplayed(lnk_resultCustomerAccountIPA)) {
			writeTestResults("Verify user can search an existing customer by entering Customer name/code",
					"User should be able to search an existing customer by entering Customer name/code",
					"User successfully search an existing customer by entering Customer name/code", "pass");
		} else {

			writeTestResults("Verify user can search an existing customer by entering Customer name/code",
					"User should be able to search an existing customer by entering Customer name/code",
					"User could'nt search an existing customer by entering Customer name/code", "fail");
		}

		doubleClick(lnk_resultCustomerAccountIPA);

		sendKeys(txt_refNoIPA, testAllCharcterWordIPA);
		/*
		 * click(txt_descIPA); String ref = getAttribute(txt_refNoIPA, "title");
		 */

		if (isDisplayed(txt_refNoIPA)) {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Reference No",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Reference No",
					"User successfully enter characters, numbers, alphanumeric and special characters to Reference No",
					"pass");
		} else {

			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Reference No",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Reference No",
					"User could'nt enter characters, numbers, alphanumeric and special characters to Reference No",
					"fail");
		}

		sendKeys(txt_descIPA, testAllCharcterWordIPA);
		Thread.sleep(2000);

		if (isDisplayed(txt_descIPA)) {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User successfully enter characters, numbers, alphanumeric and special characters to Description",
					"pass");
		} else {

			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User could'nt enter characters, numbers, alphanumeric and special characters to Description",
					"fail");
		}

		Select comboBox = new Select(driver.findElement(By.xpath(drop_cusrencyIPA)));
		selectText(drop_cusrencyIPA, curenceIPA);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		if (selectedComboValue.equals(curenceIPA)) {
			writeTestResults("Verify user can choose the currency mode from the drop down",
					"User should be able to choose the currency mode from the drop down",
					"User successfully choose the currency mode from the drop down", "pass");
		} else {

			writeTestResults("Verify user can choose the currency mode from the drop down",
					"User should be able to choose the currency mode from the drop down",
					"User could'nt choose the currency mode from the drop down", "fail");
		}

		selectIndex(drop_taxIPA, 2);
		if (isDisplayed(drop_taxIPA)) {
			writeTestResults("Verify user can choose the applicable tax mode",
					"User should able to choose the applicable tax mode",
					"User successfully choose the applicable tax mode", "pass");
		} else {

			writeTestResults("Verify user can choose the applicable tax mode",
					"User should able to choose the applicable tax mode",
					"User could'nt choose the applicable tax mode", "fail");
		}

		sendKeys(txt_amountIPA, amountIPA);
		if (isDisplayed(txt_amountIPA)) {
			writeTestResults("Verify user can enter positive numbers only in amount field",
					"User should be able to enter positive numbers only in amount field",
					"User successfully enter positive numbers only in amount field", "pass");
		} else {

			writeTestResults("Verify user can enter positive numbers only in amount field",
					"User should be able to enter positive numbers only in amount field",
					"User could'nt enter positive numbers only in amount field", "fail");
		}
	}

	public void checkoutDraftreleaseIPA() throws Exception {
		Thread.sleep(2000);
		String ckeckoutBefore = getText(btn_documentDataIPA);

		click(btn_checkout);
		Thread.sleep(3000);
		String ckeckoutAfter = getText(btn_documentDataIPA);
		if (!ckeckoutBefore.equals(ckeckoutAfter)) {
			writeTestResults("Verify calculation, where net amount and Total field will populate the final amount",
					"System allows do calculation, where net amount and Total field will populate the final amount",
					"Calculation, where net amount and Total field will successfully populate the final amount",
					"pass");
		} else {

			writeTestResults("Verify calculation, where net amount and Total field will populate the final amount",
					"System allows do calculation, where net amount and Total field will populate the final amount",
					"Calculation, where net amount and Total field does'nt populate the final amount", "fail");
		}

		click(btn_draft);
		customizeLoadingDelay(btn_reelese, 20);
		if (isDisplayed(btn_reelese)) {

			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice",
					"User sucessfully draft Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice", "User could'nt draft Inboumd Payment Advice",
					"fail");
		}

		Thread.sleep(3000);
		click(btn_reelese);
		explicitWaitUntillClickable(btn_print, 50);
		Thread.sleep(4000);
		System.out.println(getText(lbl_docNoIPA));
		writeFinData("Fin_TC_001_IPA_Methods_are_also_used_for_Fin_TC_002", getText(lbl_docNoIPA), 0);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(btn_print)) {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User sucessfully release Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User could'nt release Inboumd Payment Advice", "fail");
		}
	}

	/* Fin_TC_002 */
	public void finTC001() throws Exception {
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		commonCustomerAccountCreation();

		InventoryAndWarehouseModule obj = new InventoryAndWarehouseModule();
		obj.implisitWait(7);
		Thread.sleep(1000);
		click(navigation_pane);
		if (isDisplayed(btn_financeModule)) {

			writeTestResults("Verify that navigation menu is open", "User should be able to open navigation menu",
					"Navigation menu successfully open", "pass");
		} else {
			writeTestResults("Verify that navigation menu is open", "User should be able to open navigation menu",
					"Navigation menu does'nt open", "fail");

		}

		click(btn_financeModule);
		if (isDisplayed(btn_bulkChecqu)) {

			writeTestResults("Verify user can navigate to the Finance Module",
					"User should be navigate to the Finance Module",
					"User successfully naviagate to the Finance Module", "pass");
		} else {
			writeTestResults("Verify user can navigate to the Finance Module",
					"User should be navigate to the Finance Module", "User could'nt naviagate to the Finance Module",
					"fail");

		}
		navigateToIPAForm();
		/* Copied from Fin_TC_001 */
		click(lnl_postBusinessUnitIPA);
		selectText(drop_postBusinessUnit, postBusinessUnitIPA);
		click(tab_subbaryIPA);

		String get_post_business_unit = getText(lnl_postBusinessUnitIPA);
		System.out.println(get_post_business_unit);
		if (get_post_business_unit.equals(postBusinessUnitIPA)) {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User successfully select post business unit from the drop down", "pass");
		} else {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User could'nt select post business unit from the drop down", "fail");
		}

		click(btn_docDateIPA);
		click(btn_calenderBackIAP);
		click(btn_calenderBackIAP);
		click("25_Link");

		click(btn_dueDateIPA);
		click(btn_calenderForwardIAP);
		click(btn_calenderForwardIAP);
		click("25_Link");

		String doc_date = getText(btn_docDateIPA).replaceAll("/", "");
		String post_date = getText(btn_postDateIPA).replaceAll("/", "");
		String due_date = getText(btn_dueDateIPA).replaceAll("/", "");

		String doc_date1 = reverseStringDateToSLOrder(doc_date);
		String post_date1 = reverseStringDateToSLOrder(post_date);
		String due_date1 = reverseStringDateToSLOrder(due_date);

		int doc_date2 = Integer.parseInt(doc_date1);
		int post_date2 = Integer.parseInt(post_date1);
		int due_date2 = Integer.parseInt(due_date1);

		if (doc_date2 < post_date2 && post_date2 < due_date2) {
			writeTestResults("Verify user can select document date, post date and due date",
					"User should be able to select document date, post date and due date",
					"User successfully select document date, post date and due date", "pass");
		} else {

			writeTestResults("Verify user can select document date, post date and due date",
					"User should be able to select document date, post date and due date",
					"User could'nt select document date, post date and due date", "fail");
		}

		click(btn_searchCustomerIPA);
		if (isDisplayed(txt_cusAccountSearchIPA)) {
			writeTestResults("Verify user can click on Search button in Customer and Customer Lookup should pop up",
					"User should be click on Search button in Customer and Customer Lookup should pop up",
					"User successfully click on Search button in Customer and Customer Lookup should pop up", "pass");
		} else {

			writeTestResults("Verify user can click on Search button in Customer and Customer Lookup should pop up",
					"User should be click on Search button in Customer and Customer Lookup should pop up",
					"User could'nt click on Search button in Customer and Customer Lookup should pop up", "fail");
		}

		sleepCusomized(txt_cusAccountSearchIPA);
		pressEnter(txt_cusAccountSearchIPA);
		sleepCusomized(lnk_resultCustomerAccountIPA);
		if (isDisplayed(lnk_resultCustomerAccountIPA)) {
			writeTestResults("Verify user can search an existing customer by entering Customer name/code",
					"User should be able to search an existing customer by entering Customer name/code",
					"User successfully search an existing customer by entering Customer name/code", "pass");
		} else {

			writeTestResults("Verify user can search an existing customer by entering Customer name/code",
					"User should be able to search an existing customer by entering Customer name/code",
					"User could'nt search an existing customer by entering Customer name/code", "fail");
		}

		doubleClick(lnk_resultCustomerAccountIPA);

		sendKeys(txt_refNoIPA, testAllCharcterWordIPA);
		/*
		 * click(txt_descIPA); String ref = getAttribute(txt_refNoIPA, "title");
		 */

		if (isDisplayed(txt_refNoIPA)) {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Reference No",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Reference No",
					"User successfully enter characters, numbers, alphanumeric and special characters to Reference No",
					"pass");
		} else {

			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Reference No",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Reference No",
					"User could'nt enter characters, numbers, alphanumeric and special characters to Reference No",
					"fail");
		}

		sendKeys(txt_descIPA, testAllCharcterWordIPA);
		Thread.sleep(2000);

		if (isDisplayed(txt_descIPA)) {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User successfully enter characters, numbers, alphanumeric and special characters to Description",
					"pass");
		} else {

			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User could'nt enter characters, numbers, alphanumeric and special characters to Description",
					"fail");
		}

		Select comboBox = new Select(driver.findElement(By.xpath(drop_cusrencyIPA)));
		selectText(drop_cusrencyIPA, curenceIPA);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		if (selectedComboValue.equals(curenceIPA)) {
			writeTestResults("Verify user can choose the currency mode from the drop down",
					"User should be able to choose the currency mode from the drop down",
					"User successfully choose the currency mode from the drop down", "pass");
		} else {

			writeTestResults("Verify user can choose the currency mode from the drop down",
					"User should be able to choose the currency mode from the drop down",
					"User could'nt choose the currency mode from the drop down", "fail");
		}

		selectIndex(drop_taxIPA, 2);
		if (isDisplayed(drop_taxIPA)) {
			writeTestResults("Verify user can choose the applicable tax mode",
					"User should able to choose the applicable tax mode",
					"User successfully choose the applicable tax mode", "pass");
		} else {

			writeTestResults("Verify user can choose the applicable tax mode",
					"User should able to choose the applicable tax mode",
					"User could'nt choose the applicable tax mode", "fail");
		}

		sendKeys(txt_amountIPA, amountIPA);
		if (isDisplayed(txt_amountIPA)) {
			writeTestResults("Verify user can enter positive numbers only in amount field",
					"User should be able to enter positive numbers only in amount field",
					"User successfully enter positive numbers only in amount field", "pass");
		} else {

			writeTestResults("Verify user can enter positive numbers only in amount field",
					"User should be able to enter positive numbers only in amount field",
					"User could'nt enter positive numbers only in amount field", "fail");
		}
		checkoutDraftreleaseIPA();
		sleepCusomized(btn_print);
		Thread.sleep(3000);
		finTC002_inboundPAyment = getText(lbl_docNoIPA);

	}

	public void fillInboundPaymentCustomerReceiptVouche() throws Exception {
		click(btn_actionCRV);
		if (isDisplayed(btn_converToInboundPaymentCRV)) {
			writeTestResults("Verify user can view available actions for the selected Inbound payment Advice",
					"User should able to view available actions for the selected Inbound payment Advice",
					"User sucessfully view available actions for the selected Inbound payment Advice", "pass");
		} else {
			writeTestResults("Verify user can view available actions for the selected Inbound payment Advice",
					"User should able to view available actions for the selected Inbound payment Advice",
					"User could'nt view available actions for the selected Inbound payment Advice", "fail");
		}

		click(btn_converToInboundPaymentCRV);
		boolean flag = isDisplayed(drop_paidCurrencyCRV);
		click(btn_detailTabCRV);

		if (flag && isDisplayed(btn_viewPaymentBD)) {
			writeTestResults(
					"Verify user can naivgate to Inbound payment page where customer details auto generated and advice selected in step 1 is visible in details page",
					"User should naivgate to Inbound payment page where customer details auto generated and advice selected in step 1 is visible in details page",
					"User successfully naivgate to Inbound payment page where customer details auto generated and advice selected in step 1 is visible in details page",
					"pass");
		} else {
			writeTestResults(
					"Verify user can naivgate to Inbound payment page where customer details auto generated and advice selected in step 1 is visible in details page",
					"User should naivgate to Inbound payment page where customer details auto generated and advice selected in step 1 is visible in details page",
					"User couldn't naivgate to Inbound payment page where customer details auto generated and advice selected in step 1 is visible in details page",
					"fail");
		}

		click(btn_summaryTab);

		click(btn_docDateIPA);
		click(btn_calenderBackIAP);
		click(btn_calenderBackIAP);
		click("25_Link");

		click(btn_dueDateIPA);
		click(btn_calenderForwardIAP);
		click(btn_calenderForwardIAP);
		click("25_Link");

		String doc_datefin2 = getText(btn_docDateIPA).replaceAll("/", "");
		String post_datefin2 = getText(btn_postDateIPA).replaceAll("/", "");
		String due_datefin2 = getText(btn_dueDateIPA).replaceAll("/", "");

		String doc_date1fin2 = reverseStringDateToSLOrder(doc_datefin2);
		String post_date1fin2 = reverseStringDateToSLOrder(post_datefin2);
		String due_date1fin2 = reverseStringDateToSLOrder(due_datefin2);

		int doc_date2fin2 = Integer.parseInt(doc_date1fin2);
		int post_date2fin2 = Integer.parseInt(post_date1fin2);
		int due_date2fin2 = Integer.parseInt(due_date1fin2);

		if (doc_date2fin2 < post_date2fin2 && post_date2fin2 < due_date2fin2) {
			writeTestResults("Verify user can select document date, post date and due date",
					"User should be able to select document date, post date and due date",
					"User successfully select document date, post date and due date", "pass");
		} else {

			writeTestResults("Verify user can select document date, post date and due date",
					"User should be able to select document date, post date and due date",
					"User could'nt select document date, post date and due date", "fail");
		}

		selectText(drop_paidCurrencyCRV, paidCurrencyCRV);
		if (isDisplayed(drop_paidCurrencyCRV)) {
			writeTestResults("Verify user can choose the paid currency mode from the drop down",
					"User should be able to choose the paid currency mode from the drop down",
					"User successfully choose the paid currency mode from the drop down", "pass");
		} else {

			writeTestResults("Verify user can choose the paid currency mode from the drop down",
					"User should be able to choose the paid currency mode from the drop down",
					"User couldn't choose the paid currency mode from the drop down", "fail");
		}

		selectText(drop_paymentMethodCRV, paymentMethodCRV);
		if (isDisplayed(drop_paymentMethodCRV)) {
			writeTestResults("Verify user can choose payment type (Cheque)",
					"User should be able to choose payment type (Cheque)",
					"User successfully choose payment type (Cheque)", "pass");
		} else {

			writeTestResults("Verify user can choose payment type (Cheque)",
					"User should be able to choose payment type (Cheque)", "User couldn't choose payment type (Cheque)",
					"fail");
		}
		selectIndex(drop_bankNAmeCRV, 3);
		String checq_num = currentTimeAndDate();
		System.out.println(checq_num);
		sendKeys(txt_checkNoCRV, checq_num);

		click(plicker_dateCheckDateCRV);
		click(btn_calenderBackIAP);
		click(btn_calenderBackIAP);
		click("25_Link");

		if (isDisplayed(plicker_dateCheckDateCRV)) {
			writeTestResults("Verify user can select the later date as cheque date",
					"User should be able to select the later date as cheque date",
					"User successfully select the later date as cheque date", "pass");
		} else {

			writeTestResults("Verify user can select the later date as cheque date",
					"User should be able to select the later date as cheque date",
					"User couldn't select the later date as cheque date", "fail");
		}

		click(btn_detailTabCRV);

		if (isDisplayed(drop_filterCurrencyCRV)) {
			writeTestResults("Verify user can navigate to details page",
					"User should be able to navigate to details page", "User successfully navigate to details page",
					"pass");
		} else {

			writeTestResults("Verify user can navigate to details page",
					"User should be able to navigate to details page", "User couldn't navigate to details page",
					"fail");
		}

		click(btn_detailTabCRV);
		selectText(drop_filterCurrencyCRV, filterCurrencyCRV);
		if (isDisplayed(drop_filterCurrencyCRV)) {
			writeTestResults(
					"Verify user can select USD as filter currency. And advices raised in USD should appeared in list",
					"User should be able to select USD as filter currency. And advices raised in USD should appeared in list",
					"User successfully select USD as filter currency. And advices raised in USD should appeared in list",
					"pass");
		} else {

			writeTestResults(
					"Verify user can select USD as filter currency. And advices raised in USD should appeared in list",
					"User should be able to select USD as filter currency. And advices raised in USD should appeared in list",
					"User couldn't select USD as filter currency. And advices raised in USD should appeared in list",
					"fail");
		}

		sendKeys(txt_payingAmountCRV, payingAmountCRV);

		if (isDisplayed(txt_payingAmountCRV)) {
			writeTestResults("Verify user can enter positive numbers only in paid amount field",
					"User should be able to enter positive numbers only in paid amount field",
					"User successfully enter positive numbers only in paid amount field", "pass");
		} else {

			writeTestResults("Verify user can enter positive numbers only in paid amount field",
					"User should be able to enter positive numbers only in paid amount field",
					"User couldn't enter positive numbers only in paid amount field", "fail");
		}
		Actions action = new Actions(driver);
		WebElement we = driver.findElement(By.xpath(btn_viewPaymentFinTC002));
		action.moveToElement(we).build().perform();
		click(btn_viewPaymentFinTC002);

		InventoryAndWarehouseModule obj = new InventoryAndWarehouseModule();
		obj.sleepCusomized(lnk_paymentNoInboundPayemnt.replace("payment_no", finTC002_inboundPAyment));

		String row_payment_advice_xpath = chk_adviceCRV.replace("payment_n", finTC002_inboundPAyment);
		click(row_payment_advice_xpath);

		if (isDisplayed(lnk_paymentNoInboundPayemnt.replace("payment_no", finTC002_inboundPAyment))) {
			writeTestResults("Verify user select the advices ", "Should be able to select the advices",
					"User successfully select the advices", "pass");
		} else {

			writeTestResults("Verify user select the advices ", "Should be able to select the advices",
					"User couldn't select the advices", "fail");
		}

		/*
		 * (IGNORED) sendKeys(txt_paidAmountCRV, paidAmountCRV); if
		 * (isDisplayed(txt_paidAmountCRV)) { writeTestResults(
		 * "Verify user enter the paid amount as per the actual receival in other than customer advance advices"
		 * ,
		 * "User should be able to enter the paid amount as per the actual receival in other than customer advance advices"
		 * ,
		 * "User successfully enter the paid amount as per the actual receival in other than customer advance advices"
		 * , "pass"); } else {
		 * 
		 * writeTestResults(
		 * "Verify user enter the paid amount as per the actual receival in other than customer advance advices"
		 * ,
		 * "User should be able to enter the paid amount as per the actual receival in other than customer advance advices"
		 * ,
		 * "User couldn't enter the paid amount as per the actual receival in other than customer advance advices"
		 * , "fail"); }
		 */
	}

	public void checkoutDraftreleaseCustomerRecipt() throws Exception {
		click(btn_checkout);
		Thread.sleep(4000);
		String bannerTotalCRV = getText(btn_documentDataCRV);

		if (!bannerTotalCRV.equals("0")) {
			writeTestResults(
					"Verify system allows do calculation, where net amount and Total field will populate the final amount",
					"System allows do calculation, where net amount and Total field will populate the final amount",
					"System successfully allows do calculation, where net amount and Total field will populate the final amount",
					"pass");
		} else {

			writeTestResults(
					"Verify system allows do calculation, where net amount and Total field will populate the final amount",
					"System allows do calculation, where net amount and Total field will populate the final amount",
					"System doesn't allows do calculation, where net amount and Total field will populate the final amount",
					"fail");
		}

		click(btn_draft);
		if (isDisplayed(btn_reelese)) {
			writeTestResults("Verify user can draft Inbound Payment", "User should be able to draft Inbound Payment",
					"User sucessfully draft Inbound Payment", "pass");
		} else {
			writeTestResults("Verify user can draft Inbound Payment", "User should be able to draft Inbound Payment",
					"User could'nt draft Inbound Payment", "fail");
		}

		click(btn_reelese);
		sleepCustomize2(btn_print);
		String payment_doc_no = getText(lbl_InboundPaymentDocNo);
		writeFinData("Fin_TC_001", payment_doc_no, 2); // May not needed
		Thread.sleep(3000);
		sleepCusomized(lbl_docNo);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(btn_print)) {
			writeTestResults("Verify user can release Inbound Payment",
					"User should be able to release Inbound Payment", "User sucessfully release Inbound Payment",
					"pass");
		} else {
			writeTestResults("Verify user can release Inbound Payment",
					"User should be able to release Inbound Payment", "User could'nt release Inbound Payment", "fail");
		}

		click(btn_actionCRV);
		click(btn_journel);
		Thread.sleep(2000);
		String header = getText(header_journelEntry);
		if (!header.equalsIgnoreCase("Journal Entry Details")) {
			Thread.sleep(7000);
			header = getText(header_journelEntry);
		}

		if (!header.equals("Journal Entry Details")) {
			writeTestResults("Verify journal should be not available till bank deposit transaction",
					"Journal should be not available till bank deposit transaction",
					"Step successfull, journal entry not available till bank deposit transaction", "pass");
		} else {
			writeTestResults("Verify journal should be not available till bank deposit transaction",
					"Journal should be not available till bank deposit transaction",
					"Journal entry available(Journel entry should not be available)", "fail");
		}

		/*
		 * btn_viewAllPendings (For future updates.)
		 */
	}

	/* Fin_TC_003 */
	public void navigateToSalesInvoice() throws Exception {
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		Thread.sleep(1000);
		click(navigation_pane);
		click(btn_sallesMarketingModule);
		click(btn_salesInvoice);
		customizeLoadingDelay(btn_newSalesInvoice, 8);
		if (isDisplayed(btn_newSalesInvoice)) {
			writeTestResults("Verify user can navigate to Sales Invoice by-page",
					"User should be able to navigate to Sales Invoice by-page",
					"User successfully navigate to Sales Invoice by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Sales Invoice by-page",
					"User should be able to navigate to Sales Invoice by-page",
					"User couldn't navigate to Sales Invoice by-page", "fail");
		}

		click(btn_newSalesInvoice);
		if (isDisplayed(btn_slaesInvoiceServiceJourney)) {
			writeTestResults("Verify user can navigate to sales invoice new page",
					"User should be able navigate to sales invoice new page",
					"User successfully navigate to sales invoice new page", "pass");
		} else {
			writeTestResults("Verify user can navigate to sales invoice new page",
					"User should be able navigate to sales invoice new page",
					"User couldn't navigate to sales invoice new page", "fail");
		}

		click(btn_slaesInvoiceServiceJourney);
	}

	public void complteSlaesInvoice() throws Exception {
		sleepCusomized(btn_customerSearchSalesInvoice);
		click(btn_customerSearchSalesInvoice);

		Thread.sleep(3500);
		customizeLoadingDelay(txt_cusAccountSearchIPA, 12);
		sendKeys(txt_cusAccountSearchIPA, customerAccountCRV);
		pressEnter(txt_cusAccountSearchIPA);
		Thread.sleep(4000);
		customizeLoadingDelay(lnk_resultCustomerAccountIPA, 12);
		doubleClick(lnk_resultCustomerAccountIPA);

		Thread.sleep(2000);
		selectText(drop_currencySlaesInvoise, currencySalesInvoise);
		selectText(drop_salesUnitSalesIvoice, slaesUnitSlaesInvoice);

		click(btn_productLockupSalesInvoice);
		sleepCusomized(txt_productSearch);
		sendKeys(txt_productSearch, serviceProductSalesInvooice);
		pressEnter(txt_productSearch);
		Thread.sleep(2000);
		customizeLoadingDelay(lnk_resultProduct, 10);
		Thread.sleep(2000);
		doubleClick(lnk_resultProduct);
		sendKeys(txt_unitPriceSalesInvoice, unitPriceSalesInvice);
	}

	public void draftreleaseAndCheckDockFlowSalesInvoice() throws Exception {
		customizeLoadingDelay(btn_checkout, 8);
		Thread.sleep(700);
		click(btn_checkout);
		Thread.sleep(4000);
		click(btn_draft);
		customizeLoadingDelay(btn_reelese, 15);
		if (isDisplayed(btn_reelese)) {
			writeTestResults("Verify user can create sales invoice with the specified details",
					"User should able to create sales invoice with the specified details",
					"User successfully create sales invoice with the specified details", "pass");
		} else {
			writeTestResults("Verify user can create sales invoice with the specified details",
					"User should able to create sales invoice with the specified details",
					"User couldn't create sales invoice with the specified details", "fail");
		}

		sleepCustomize2(btn_reelese);
		String sales_invoice_no = getText(lbl_docNo);
		writeFinData("Sales Invoice - Fin_TC_003", sales_invoice_no, 11);
		Thread.sleep(3000);
		sleepCusomized(lbl_docNo);
		trackCode = getText(lbl_docNo);
		click(btn_reelese);
		InventoryAndWarehouseModule obj = new InventoryAndWarehouseModule();
		obj.implisitWait(8);
		if (isDisplayed(btn_print)) {
			writeTestResults("Verify user can draft and release the Sales Invoice",
					"User should be able to draft and release the Sales Invoice",
					"User sucessfully draft and release the Sales Invoice", "pass");
		} else {
			writeTestResults("Verify user can draft and release the Sales Invoice",
					"User should be able to draft and release the Sales Invoice",
					"User could'nt draft and release the Sales Invoice", "fail");
		}
		Thread.sleep(1000);
		/*
		 * driver.manage().window().setSize(new Dimension(1382, 745));
		 * driver.findElement(By.id("eleLoadAction")).click();
		 * driver.findElement(By.cssSelector(".pic48-documentflow")).click();
		 * driver.findElement(By.id("fi2")).click(); try { Thread.sleep(2000); } catch
		 * (InterruptedException e) { e.printStackTrace(); }
		 * 
		 * if (isDisplayed(header_inboundPaymentAdviceSalesInvoice)) {
		 * writeTestResults("Verify user can view the auto created advice with Sales Invoice Reference Number"
		 * ,
		 * "User should be able to view the auto created advice with Sales Invoice Reference Number"
		 * ,
		 * "User successfully view the auto created advice with Sales Invoice Reference Number"
		 * , "pass"); } else {
		 * writeTestResults("Verify user can view the auto created advice with Sales Invoice Reference Number"
		 * ,
		 * "User should be able to view the auto created advice with Sales Invoice Reference Number"
		 * ,
		 * "User could'nt view the auto created advice with Sales Invoice Reference Number"
		 * , "fail"); }
		 */

	}

	/* Fin_TC_004 */
	public void navigateToBankDepositForm() throws Exception {
		navigateToTheFananceModule();
		click(btn_bankDeposit);
		if (isDisplayed(btn_newBankDeposit)) {
			writeTestResults("Verify user can navigate to Bank Deposit page",
					"User should navigate to Bank Deposit page", "User successfully navigate to Bank Deposit page",
					"pass");
		} else {
			writeTestResults("Verify user can navigate to Bank Deposit page",
					"User should navigate to Bank Deposit page", "User could'nt navigate to Bank Deposit page", "fail");
		}

		click(btn_newBankDeposit);
		InventoryAndWarehouseModule obj = new InventoryAndWarehouseModule();
		obj.implisitWait(7);
		if (isDisplayed(drop_paymentMethod)) {
			writeTestResults("Verify user can navigate to Bank Deposit form",
					"User should navigate to Bank Deposit form", "User successfully navigate to Bank Deposit form",
					"pass");
		} else {
			writeTestResults("Verify user can navigate to Bank Deposit form",
					"User should navigate to Bank Deposit form", "User could'nt navigate to Bank Deposit form", "fail");
		}
	}

	public void fillBankDepositDeatils() throws Exception {
		selectText(drop_paymentMethod, paymentMethodBD);
		if (isDisplayed(drop_paymentMethod)) {
			writeTestResults("Verify user can select payment method", "User should be able to select payment method",
					"User successfully select payment method", "pass");
		} else {
			writeTestResults("Verify user can select payment method", "User should be able to select payment method",
					"User couldn't select payment method", "fail");
		}

		selectText(drop_bankNameBD, bankBD);
		if (isDisplayed(drop_bankNameBD)) {
			writeTestResults("Verify user can select bank from drop down",
					"User should be able to select bank from drop down", "User successfully select bank from drop down",
					"pass");
		} else {
			writeTestResults("Verify user can select bank from drop down",
					"User should be able to select bank from drop down", "User couldn't select bank from drop down",
					"fail");
		}

		selectIndex(drop_accountNo, 2);
		if (isDisplayed(drop_accountNo)) {
			writeTestResults("Verify user can select bank account no", "User should be able to select bank account no",
					"User successfully select bank account no", "pass");
		} else {
			writeTestResults("Verify user can select bank account no", "User should be able to select bank account no",
					"User couldn't select bank account no", "fail");
		}
	}

	public void findInboundPaymentForBankDeposit() throws Exception {
		String readed_pn = readFinData(2);
		String row_payment_xpath = btn_rowPaymentBD.replace("payment_no", readed_pn);

		click(btn_viewPaymentBD);
		Thread.sleep(4000);
		if (isDisplayed(row_payment_xpath)) {
			writeTestResults("Verify user can view list of pay items to be deposit",
					"User should able to view list of pay items to be deposit",
					"User successfully view list of pay items to be deposit", "pass");
		} else {
			writeTestResults("Verify user can view list of pay items to be deposit",
					"User should be able to view list of pay items to be deposit",
					"User could'nt view list of pay items to be deposit", "fail");
		}

		click(row_payment_xpath);
		click(chk_paymentBD);

		if (isSelected(chk_paymentBD)) {
			writeTestResults("Verify user can choose vouchers to deposit",
					"User should be able to choose vouchers to deposit", "User successfully choose vouchers to deposit",
					"pass");
		} else {
			writeTestResults("Verify user can choose vouchers to deposit",
					"User should be able to choose vouchers to deposit", "User could'nt choose vouchers to deposit",
					"fail");
		}

	}

	public void draftreleaseBankDeposit() throws Exception {
		click(btn_draft);
		if (isDisplayed(btn_reelese)) {
			writeTestResults("Verify user can draft the Bank Deposit", "User should be able to draft the Bank Deposit",
					"User sucessfully draft the Bank Deposit", "pass");
		} else {
			writeTestResults("Verify user can draft the Bank Deposit", "User should be able to draft the Bank Deposit",
					"User could'nt draft the Bank Deposit", "fail");
		}
		click(btn_reelese);
		Thread.sleep(3000);
		sleepCusomized(lbl_docNo);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(btn_print)) {
			writeTestResults("Verify user can release the Bank Deposit",
					"User should be able to release the Bank Deposit", "User sucessfully release the Bank Deposit",
					"pass");
		} else {
			writeTestResults("Verify user can release the Bank Deposit",
					"User should be able to release the Bank Deposit", "User could'nt release the Bank Deposit",
					"fail");
		}
	}

	/* Fin_TC_005 */
	public void findReleventSalesInvoise() throws Exception {
		commonLogin();
		click(navigation_pane);
		click(btn_sallesMarketingModule);
		click(btn_salesInvoice);
		sendKeys(txt_searchSalesInvoice, readFinData(11));
		pressEnter(txt_searchSalesInvoice);

		/* Create xpath lnk sales invoice */
		String sales_invoice_xpath = lnk_resultDocument.replace("sales_invoic", readFinData(11));
		sleepCusomized(sales_invoice_xpath);

		if (isDisplayed(sales_invoice_xpath)) {
			writeTestResults("Verify user can view the Inbound payment advice",
					"User should be able to view Inbound payment advice",
					"User successfully view Inbound payment advice", "pass");
		} else {
			writeTestResults("Verify user can view the Inbound payment advice",
					"User should be able to view Inbound payment advice", "User couldn't view Inbound payment advice",
					"fail");
		}

		Thread.sleep(2000);
		click(sales_invoice_xpath);
		sleepCusomized(btn_action);
		click(btn_action);

		sleepCusomized(btn_docFlow);
		click(btn_docFlow);
		System.out.println();
		/* click(btn_inboundPaymentAdviceDocFlow); */

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$(\"#svgElement\").find(\"#fi2\").click();");
		System.out.println();
		/*
		 * btn_setOffActionmenu chk_setOffInvoiceDocNumber btn_setOffWindowSetOffButton
		 * txt_adviceDueAmountAfterSetOffSAlesInvoiceInboundPaymentAdvice
		 * chk_inboundPaymentAdviceInboundPayment
		 */

	}

	/* Fin_TC_006 */
	public void navigattToInboundPaymentAdviceCustomerDebitMemoForm() throws Exception {
		navigateToTheFananceModule();

		click(btn_inboundPaymentAdvice);
		if (isDisplayed(btn_newInboundPaymentAdvice)) {

			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User successfully navigate to Inbound payment advice by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Inbound payment advice by-page",
					"User should navigate to Inbound payment advice by-page",
					"User could'nt navigate to Inbound payment advice by-page", "fail");
		}

		click(btn_newInboundPaymentAdvice);
		sleepCusomized(btn_customerDebitMemoJourney);
		if (isDisplayed(btn_customerDebitMemoJourney)) {

			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User successfully view the list of journey",
					"pass");
		} else {
			writeTestResults("Verify user can be able to view the list of journey",
					"User should be able to view the list of journey", "User could'nt view the list of journey",
					"fail");
		}

		click(btn_customerDebitMemoJourney);
		if (isDisplayed(lnl_postBusinessUnitIPA)) {

			writeTestResults("Verify user can navigate to new customer debit memo form",
					"User should navigate to new customer debit memo form",
					"User successfully navigate to new customer debit memo form", "pass");
		} else {
			writeTestResults("Verify user can navigate to new customer debit memo form",
					"User should navigate to new customer debit memo form",
					"User could'nt navigate to new customer debit memo form", "fail");
		}
	}

	public void fillCustomerDebitMemoForm() throws Exception {
		click(lnl_postBusinessUnitIPA);
		selectText(drop_postBusinessUnit, postBusinessUnitIPA);
		click(tab_subbaryIPA);

		String get_post_business_unit = getText(lnl_postBusinessUnitIPA);
		System.out.println(get_post_business_unit);
		if (get_post_business_unit.equals(postBusinessUnitIPA)) {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User successfully select post business unit from the drop down", "pass");
		} else {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User could'nt select post business unit from the drop down", "fail");
		}

		click(btn_docDateIPA);
		click(btn_calenderBackIAP);
		click(btn_calenderBackIAP);
		click("25_Link");

		click(btn_dueDateIPA);
		click(btn_calenderForwardIAP);
		click(btn_calenderForwardIAP);
		click("25_Link");

		String doc_date = getText(btn_docDateIPA).replaceAll("/", "");
		String post_date = getText(btn_postDateIPA).replaceAll("/", "");
		String due_date = getText(btn_dueDateIPA).replaceAll("/", "");

		String doc_date1 = reverseStringDateToSLOrder(doc_date);
		String post_date1 = reverseStringDateToSLOrder(post_date);
		String due_date1 = reverseStringDateToSLOrder(due_date);

		int doc_date2 = Integer.parseInt(doc_date1);
		int post_date2 = Integer.parseInt(post_date1);
		int due_date2 = Integer.parseInt(due_date1);

		if (doc_date2 < post_date2 && post_date2 < due_date2) {
			writeTestResults("Verify user can select document date, post date and due date",
					"User should be able to select document date, post date and due date",
					"User successfully select document date, post date and due date", "pass");
		} else {

			writeTestResults("Verify user can select document date, post date and due date",
					"User should be able to select document date, post date and due date",
					"User could'nt select document date, post date and due date", "fail");
		}

		click(btn_searchCustomerIPA);
		if (isDisplayed(txt_cusAccountSearchIPA)) {
			writeTestResults("Verify user can click on Search button in Customer and Customer Lookup should pop up",
					"User should be click on Search button in Customer and Customer Lookup should pop up",
					"User successfully click on Search button in Customer and Customer Lookup should pop up", "pass");
		} else {

			writeTestResults("Verify user can click on Search button in Customer and Customer Lookup should pop up",
					"User should be click on Search button in Customer and Customer Lookup should pop up",
					"User could'nt click on Search button in Customer and Customer Lookup should pop up", "fail");
		}

		sleepCusomized(txt_cusAccountSearchIPA);
		sendKeys(txt_cusAccountSearchIPA, customerCustomerDebitMemo);
		pressEnter(txt_cusAccountSearchIPA);
		sleepCusomized(lnk_resultCustomerAccountIPA);
		if (isDisplayed(lnk_resultCustomerAccountIPA)) {
			writeTestResults("Verify user can search an existing customer by entering Customer name/code",
					"User should be able to search an existing customer by entering Customer name/code",
					"User successfully search an existing customer by entering Customer name/code", "pass");
		} else {

			writeTestResults("Verify user can search an existing customer by entering Customer name/code",
					"User should be able to search an existing customer by entering Customer name/code",
					"User could'nt search an existing customer by entering Customer name/code", "fail");
		}

		doubleClick(lnk_resultCustomerAccountIPA);

		sendKeys(txt_refNoIPA, testAllCharcterWordIPA);
		/*
		 * click(txt_descIPA); String ref = getAttribute(txt_refNoIPA, "title");
		 */

		if (isDisplayed(txt_refNoIPA)) {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Reference No",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Reference No",
					"User successfully enter characters, numbers, alphanumeric and special characters to Reference No",
					"pass");
		} else {

			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Reference No",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Reference No",
					"User could'nt enter characters, numbers, alphanumeric and special characters to Reference No",
					"fail");
		}

		sendKeys(txt_descIPA, testAllCharcterWordIPA);
		Thread.sleep(2000);

		if (isDisplayed(txt_descIPA)) {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User successfully enter characters, numbers, alphanumeric and special characters to Description",
					"pass");
		} else {

			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User could'nt enter characters, numbers, alphanumeric and special characters to Description",
					"fail");
		}

		Select comboBox = new Select(driver.findElement(By.xpath(drop_cusrencyIPA)));
		selectText(drop_cusrencyIPA, curenceIPA);
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		if (selectedComboValue.equals(curenceIPA)) {
			writeTestResults("Verify user can choose the currency mode from the drop down",
					"User should be able to choose the currency mode from the drop down",
					"User successfully choose the currency mode from the drop down", "pass");
		} else {

			writeTestResults("Verify user can choose the currency mode from the drop down",
					"User should be able to choose the currency mode from the drop down",
					"User could'nt choose the currency mode from the drop down", "fail");
		}

		selectIndex(drop_taxIPA, 2);
		if (isDisplayed(drop_taxIPA)) {
			writeTestResults("Verify user can choose the applicable tax mode",
					"User should able to choose the applicable tax mode",
					"User successfully choose the applicable tax mode", "pass");
		} else {

			writeTestResults("Verify user can choose the applicable tax mode",
					"User should able to choose the applicable tax mode",
					"User could'nt choose the applicable tax mode", "fail");
		}
	}

	public void findGlAndCostAllocatioCustomerDebitMemo() throws Exception {
		click(btn_searchGlCRV);

		sleepCusomized(txt_amountCustomerDebirMemoCRV);
		sendKeys(txt_secrhGlCRV, glAccount);
		pressEnter(txt_secrhGlCRV);
		Thread.sleep(3000);
		if (isDisplayed(lnk_resultGL)) {
			writeTestResults(
					"Verify user can search an existing GL Account by entering GL Account name/code and by double click  the GL account user can allow to populate in the field",
					"User should be able to search an existing GL Account by entering GL Account name/code and by double click  the GL account user can allow to populate in the field",
					"User successfully search an existing GL Account by entering GL Account name/code and by double click  the GL account user can allow to populate in the field",
					"pass");
		} else {

			writeTestResults(
					"Verify user can search an existing GL Account by entering GL Account name/code and by double click  the GL account user can allow to populate in the field",
					"User should be able to search an existing GL Account by entering GL Account name/code and by double click  the GL account user can allow to populate in the field",
					"User couldn't able to search an existing GL Account by entering GL Account name/code and by double click  the GL account user can allow to populate in the field",
					"fail");
		}
		doubleClick(lnk_resultGL);

		sendKeys(txt_amountCustomerDebirMemo6, amountCRV);

		if (isDisplayed(txt_amountCustomerDebirMemoCRV)) {
			writeTestResults("Verify user can enter positive numbers only in amount field",
					"User should be able to enter positive numbers only in amount field",
					"User successfully enter positive numbers only in amount field", "pass");
		} else {

			writeTestResults("Verify user can enter positive numbers only in amount field",
					"User should be able to enter positive numbers only in amount field",
					"User could'nt enter positive numbers only in amount field", "fail");
		}

		click(btn_nosCostAllocationDebitMemo);
		if (isDisplayed(drop_costCenterCustomerDebitMemo)) {
			writeTestResults("Verify user can navigate to cost allocation page",
					"User should navigate to cost allocation page",
					"User successfully navigate to cost allocation page", "pass");
		} else {

			writeTestResults("Verify user can navigate to cost allocation page",
					"User should navigate to cost allocation page", "User couldn't navigate to cost allocation page",
					"fail");
		}

		selectText(drop_costCenterCustomerDebitMemo, departmentCostAllocationCustomerDebitMemo);
		sendKeys(txt_costAllocationPresentageCustomerDebitMemo, oresentageCostAllocationCustomerDebitMEmo);

		click(btn_addRowCostCenterCustomerDebitMemo);
		if (isDisplayed(drop_costCenterCustomerDebitMemo)) {
			if (isDisplayed(btn_updateCostCenterCustomerDebitMemo)) {
				writeTestResults(
						"Verify user can choose single or multiple cost centers and able to allocate the percentage = 100% and allows save the allocation by clicking update button",
						"User should be able to choose single or multiple cost centers and able to allocate the percentage = 100% and allows save the allocation by clicking update button",
						"User successfully choose single or multiple cost centers and able to allocate the percentage = 100% and allows save the allocation by clicking update button",
						"pass");
			} else {

				writeTestResults(
						"Verify user can choose single or multiple cost centers and able to allocate the percentage = 100% and allows save the allocation by clicking update button",
						"User should be able to choose single or multiple cost centers and able to allocate the percentage = 100% and allows save the allocation by clicking update button",
						"User couldn't choose single or multiple cost centers and able to allocate the percentage = 100% and allows save the allocation by clicking update button",
						"fail");
			}
		} else {

			writeTestResults(
					"Verify user can choose single or multiple cost centers and able to allocate the percentage = 100% and allows save the allocation by clicking update button",
					"User should be able to choose single or multiple cost centers and able to allocate the percentage = 100% and allows save the allocation by clicking update button",
					"User couldn,t choose single or multiple cost centers and able to allocate the percentage = 100% and allows save the allocation by clicking update button",
					"fail");
		}
		click(btn_updateCostCenterCustomerDebitMemo);

	}

	public void checkoutDraftreleaseCustomerDebitMemo() throws Exception {
		InventoryAndWarehouseModule obj = new InventoryAndWarehouseModule();
		obj.implisitWait(10);
		Thread.sleep(3000);
		String ckeckoutBefore = getText(btn_documentDataIPA);
		click(btn_checkout);
		Thread.sleep(3000);
		String ckeckoutAfter = getText(btn_documentDataIPA);
		if (!ckeckoutBefore.equals(ckeckoutAfter)) {
			writeTestResults("Verify calculation, where net amount and Total field will populate the final amount",
					"System allows do calculation, where net amount and Total field will populate the final amount",
					"Calculation, where net amount and Total field will successfully populate the final amount",
					"pass");
		} else {

			writeTestResults("Verify calculation, where net amount and Total field will populate the final amount",
					"System allows do calculation, where net amount and Total field will populate the final amount",
					"Calculation, where net amount and Total field does'nt populate the final amount", "fail");
		}

		click(btn_draft);
		customizeLoadingDelay(btn_reelese, 15);
		if (isDisplayed(btn_reelese)) {

			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice",
					"User sucessfully draft Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can draft Inboumd Payment Advice",
					"User should be able to draft Inboumd Payment Advice", "User could'nt draft Inboumd Payment Advice",
					"fail");
		}

		Thread.sleep(3000);
		click(btn_reelese);
		customizeLoadingDelay(btn_print, 15);
		Thread.sleep(3000);
		customizeLoadingDelay(lbl_docNoIPA, 10);
		String doc_no = getText(lbl_docNoIPA);
		writeFinData("IPA_Fin_TC_006", doc_no, 3);
		sleepCusomized(lbl_docNo);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(btn_print)) {

			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User sucessfully release Inboumd Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can release Inboumd Payment Advice",
					"User should be able to release Inboumd Payment Advice",
					"User could'nt release Inboumd Payment Advice", "fail");
		}

	}

	/* Common - Get current date and time */
	public String currentTimeAndDate() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		/* System.out.println(dtf.format(now)); */
		String curDate = dtf.format(now);
		/* System.out.println(curDate); */

		curDate = curDate.replaceAll("\\s", "");
		/* System.out.println(curDate); */
		return curDate;
	}

	/* Common - Sleep customize */
	public void sleepCusomized(String locator) throws Exception {
		int count = 0;
		do {
			count = count + 1;
			if (driver.findElement(getLocator(locator)).isDisplayed() || count > 8) {
				break;
			}

			Thread.sleep(2000);
			System.out.println("Loacator is Loading.......");
		} while (!driver.findElement(getLocator(locator)).isDisplayed());
	}

	/* Common - ReverseString */
	public String reverseStringDateToSLOrder(String string_value) {
		String month = string_value.substring(0, 2);
		String days = string_value.substring(2, 4);
		String year = string_value.substring(4);

		return year + month + days;
	}

	public void sleepCustomize2(String locator) throws Exception {
		do {
			Thread.sleep(1000);
			if (isDisplayed(locator)) {
				break;
			} else {

			}
		} while (!isDisplayed(locator));

	}

	public void sleepCustomize3(String locator) throws Exception {
		int count = 0;
		do {
			count += 1;
			Thread.sleep(1000);

			if (count < 4) {
				break;
			}
		} while (!isDisplayed(locator));

	}

	/* Common- Read data method */
	public String readFinData(int data_row) throws IOException, InvalidFormatException {
		genarateInventoryAndWarehouseInformation obj = new genarateInventoryAndWarehouseInformation();
		String path = obj.getProjectPath("*src*test*java*financeModule*Fin_DATA.xlsx");
		fis = new FileInputStream(path);
		wb = WorkbookFactory.create(fis);
		sh = wb.getSheet("Sheet1");
		row = sh.getRow(data_row);
		cell = row.getCell(1);

		String result_row = cell.getStringCellValue();
		return result_row;
	}

	/* Common- Write finance date */
	public void writeFinData(String name, String variable, int data_row) throws InvalidFormatException, IOException {
		genarateInventoryAndWarehouseInformation obj = new genarateInventoryAndWarehouseInformation();
		String path = obj.getProjectPath("*src*test*java*financeModule*Fin_DATA.xlsx");
		fis = new FileInputStream(path);
		wb = WorkbookFactory.create(fis);
		sh = wb.getSheet("Sheet1");
		/* int noOfRows = sh.getLastRowNum(); */
		/* System.out.println(noOfRows); */
		row = sh.createRow(data_row);
		cell = row.createCell(1);
		cell1 = row.createCell(0);
		cell.setCellValue(variable);
		cell1.setCellValue(name);
		System.out.println(cell.getStringCellValue());
		fos = new FileOutputStream(path);
		wb.write(fos);
		fos.flush();
		fos.close();
		/* System.out.println("Done"); */
	}

	/* common */
	public void writeFinanceData(String name, String variable, int data_row)
			throws InvalidFormatException, IOException {
		genarateInventoryAndWarehouseInformation obj = new genarateInventoryAndWarehouseInformation();
		String path = obj.getProjectPath("*src*test*java*financeModule*FIN_DATA.xlsx");
		fis = new FileInputStream(path);
		wb = WorkbookFactory.create(fis);
		sh = wb.getSheet("Sheet1");
		row = sh.createRow(data_row);
		cell = row.createCell(1);
		cell1 = row.createCell(0);
		cell.setCellValue(variable);
		cell1.setCellValue(name);
		/* System.out.println(cell.getStringCellValue()); */
		fos = new FileOutputStream(path);
		wb.write(fos);
		fos.flush();
		fos.close();
	}

	/* Common- Read finance date */
	public String readFinanceData(String name) throws IOException, InvalidFormatException {
		genarateInventoryAndWarehouseInformation obj = new genarateInventoryAndWarehouseInformation();
		String path = obj.getProjectPath("*src*test*java*financeModule*FIN_DATA.xlsx");
		fis = new FileInputStream(path);
		wb = WorkbookFactory.create(fis);
		sh = wb.getSheet("Sheet1");

		Map<String, String> iw_creation_map = new HashMap<String, String>();
		List<Map<String, String>> iw_creation = new ArrayList<Map<String, String>>();

		int totRows = sh.getLastRowNum();
		for (i = 0; i <= totRows; i++) {
			row = sh.getRow(i);
			cell = row.getCell(0);
			cell2 = row.getCell(1);

			String test_creation_row1 = cell.getStringCellValue();
			String test_creation_row2 = cell2.getStringCellValue();

			iw_creation_map.put(test_creation_row1, test_creation_row2);
			iw_creation.add(i, iw_creation_map);

		}

		String iw_info = iw_creation.get(0).get(name);

		return iw_info;
	}

	/* Fin_TC_007 */
	public void navigateToTheCustomerReciptVoucherForm() throws Exception {
		commonLogin();
		customizeLoadingDelay(navigation_pane, 20);
		click(navigation_pane);
		Thread.sleep(1500);
		if (isDisplayed(btn_financeModule)) {

			writeTestResults("Verify that navigation menu is open", "User should be able to open navigation menu",
					"Navigation menu successfully open", "pass");
		} else {
			writeTestResults("Verify that navigation menu is open", "User should be able to open navigation menu",
					"Navigation menu does'nt open", "fail");

		}

		click(btn_financeModule);
		if (isDisplayed(btn_bulkChecqu)) {

			writeTestResults("Verify user can navigate to the Finance Module",
					"User should be navigate to the Finance Module",
					"User successfully naviagate to the Finance Module", "pass");
		} else {
			writeTestResults("Verify user can navigate to the Finance Module",
					"User should be navigate to the Finance Module", "User could'nt naviagate to the Finance Module",
					"fail");

		}

		click(btn_inboundPayment);
		String header = getText(header_inboundPaymentPage);
		if (header.equals("Inbound Payment")) {

			writeTestResults("Verify user can navigate to Inbound payment  by-page",
					"User should navigate to Inbound payment  by-page",
					"User sucessfully navigate to Inbound payment  by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Inbound payment  by-page",
					"User should navigate to Inbound payment  by-page",
					"User couldn't navigate to Inbound payment  by-page", "fail");
		}

		click(btn_newInboundPayment);
		if (header.equals("Inbound Payment")) {

			writeTestResults("Verify user can view the list of journey",
					"User should be able to view the list of journey", "User sucessfully view the list of journey",
					"pass");
		} else {
			writeTestResults("Verify user can view the list of journey",
					"User should be able to view the list of journey", "User couldn't view the list of journey",
					"fail");
		}

		click(btn_customerReciptVoucherJourneyboundPAyment);
		InventoryAndWarehouseModule obj = new InventoryAndWarehouseModule();
		obj.implisitWait(10);
		boolean flag = isDisplayed(drop_paidCurrencyCRV);
		click(btn_detailTabCRV);

		if (flag && isDisplayed(btn_viewPaymentBD)) {
			writeTestResults("Verify user can navigate to New customer receipt voucher page",
					"User should navigate to New customer receipt voucher page",
					"User successfully navigate to New customer receipt voucher page", "pass");
		} else {
			writeTestResults("Verify user can navigate to New customer receipt voucher page",
					"User should navigate to New customer receipt voucher page",
					"User couldn't navigate to New customer receipt voucher page", "fail");
		}
	}

	public void fillCRVDeatails() throws Exception {
		click(lnl_postBusinessUnitIPA);
		customizeLoadingDelay(drop_postBusinessUnit, 5);
		selectText(drop_postBusinessUnit, postBusinessUnitCustomerReciptVoucher);
		Thread.sleep(1500);
		click(tab_summaryCRV7);

		Thread.sleep(2000);
		String get_post_business_unit = getText(lnl_postBusinessUnitIPA);
		if (get_post_business_unit.equals(postBusinessUnitCustomerReciptVoucher)) {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User successfully select post business unit from the drop down", "pass");
		} else {

			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User could'nt select post business unit from the drop down", "fail");
		}

		click(btn_docDateIPA);
		click(btn_calenderBackIAP);
		click(btn_calenderBackIAP);
		click("25_Link");

		click(btn_dueDateIPA);
		click(btn_calenderForwardIAP);
		click(btn_calenderForwardIAP);
		click("25_Link");

		String doc_date = getText(btn_docDateIPA).replaceAll("/", "");
		String post_date = getText(btn_postDateIPA).replaceAll("/", "");
		String due_date = getText(btn_dueDateIPA).replaceAll("/", "");

		String doc_date1 = reverseStringDateToSLOrder(doc_date);
		String post_date1 = reverseStringDateToSLOrder(post_date);
		String due_date1 = reverseStringDateToSLOrder(due_date);

		int doc_date2 = Integer.parseInt(doc_date1);
		int post_date2 = Integer.parseInt(post_date1);
		int due_date2 = Integer.parseInt(due_date1);

		if (doc_date2 < post_date2 && post_date2 < due_date2) {
			writeTestResults("Verify user can select document date, post date and due date",
					"User should be able to select document date, post date and due date",
					"User successfully select document date, post date and due date", "pass");
		} else {

			writeTestResults("Verify user can select document date, post date and due date",
					"User should be able to select document date, post date and due date",
					"User could'nt select document date, post date and due date", "fail");
		}
		click(btn_customerAccountSearchCustomerReciptVoucher);
		if (isDisplayed(txt_cusAccountSearchIPA)) {
			writeTestResults("Verify user can click on Search button in Customer and Customer Lookup should pop up",
					"User should be click on Search button in Customer and Customer Lookup should pop up",
					"User successfully click on Search button in Customer and Customer Lookup should pop up", "pass");
		} else {

			writeTestResults("Verify user can click on Search button in Customer and Customer Lookup should pop up",
					"User should be click on Search button in Customer and Customer Lookup should pop up",
					"User could'nt click on Search button in Customer and Customer Lookup should pop up", "fail");
		}

		Thread.sleep(3500);
		customizeLoadingDelay(txt_cusAccountSearchIPA, 10);
		sendKeys(txt_cusAccountSearchIPA, readFinData(12));
		pressEnter(txt_cusAccountSearchIPA);
		Thread.sleep(3500);
		customizeLoadingDelay(lnk_resultCustomerAccountIPA, 10);
		if (isDisplayed(lnk_resultCustomerAccountIPA)) {
			writeTestResults("Verify user can search an existing customer by entering Customer name/code",
					"User should be able to search an existing customer by entering Customer name/code",
					"User successfully search an existing customer by entering Customer name/code", "pass");
		} else {

			writeTestResults("Verify user can search an existing customer by entering Customer name/code",
					"User should be able to search an existing customer by entering Customer name/code",
					"User could'nt search an existing customer by entering Customer name/code", "fail");
		}

		doubleClick(lnk_resultCustomerAccountIPA);
		boolean falg = false;
		if (!txt_billingAddressCRV.equals("")) {
			falg = true;
		}

		if (isDisplayed(drop_contactPersonCRV7) && falg) {
			writeTestResults(
					"Verify selected Customer name and Customer code wil populate the Customer, Contact Person and associated Billing Address are loaded as per the customer",
					"Selected Customer name and Customer code wil populate the Customer, Contact Person and associated Billing Address should be loaded as per the customer",
					"Selected Customer name and Customer code wil populate the Customer, Contact Person and associated Billing Address successfully loaded as per the customer",
					"pass");
		} else {

			writeTestResults(
					"Verify selected Customer name and Customer code wil populate the Customer, Contact Person and associated Billing Address are loaded as per the customer",
					"Selected Customer name and Customer code wil populate the Customer, Contact Person and associated Billing Address should be loaded as per the customer",
					"Selected Customer name and Customer code wil populate the Customer, Contact Person and associated Billing Address doesn't loaded as per the customer",
					"fail");
		}

		selectText(drop_paidCurrencyCRV, paidCurrencyCRV);
		if (isDisplayed(drop_paidCurrencyCRV)) {
			writeTestResults("Verify user can choose the paid currency mode from the drop down",
					"User should be able to choose the paid currency mode from the drop down",
					"User successfully choose the paid currency mode from the drop down", "pass");
		} else {

			writeTestResults("Verify user can choose the paid currency mode from the drop down",
					"User should be able to choose the paid currency mode from the drop down",
					"User couldn't choose the paid currency mode from the drop down", "fail");
		}

		selectText(drop_paymentMethodCRV, paymentMethodCustomerReciptVoucher);
		if (isDisplayed(drop_paymentMethodCRV)) {
			writeTestResults("Verify user can choose payment type (Cash)",
					"User should be able to choose payment type (Cash)", "User successfully choose payment type (Cash)",
					"pass");
		} else {

			writeTestResults("Verify user can choose payment type (Cash)",
					"User should be able to choose payment type (Cash)", "User couldn't choose payment type (Cash)",
					"fail");
		}

		click(btn_detailTabCRV);

		Thread.sleep(1500);
		if (isDisplayed(drop_filterCurrencyCRV)) {
			writeTestResults("Verify user can navigate to details page",
					"User should be able to navigate to details page", "User successfully navigate to details page",
					"pass");
		} else {

			writeTestResults("Verify user can navigate to details page",
					"User should be able to navigate to details page", "User couldn't navigate to details page",
					"fail");
		}

		selectText(drop_filterCurrencyCRV, filterCurrencyCustomerReciptVoucher);
		if (isDisplayed(drop_filterCurrencyCRV)) {
			writeTestResults("Verify user can choose the filter currency mode from the drop down",
					"User should be able to choose the filter currency mode from the drop down",
					"User successfully choose the filter currency mode from the drop down", "pass");
		} else {

			writeTestResults("Verify user can choose the filter currency mode from the drop down",
					"User should be able to choose the filter currency mode from the drop down",
					"User couldn't choose the filter currency mode from the drop down", "fail");
		}

		Thread.sleep(4000);
		Actions action = new Actions(driver);
		WebElement we = driver.findElement(By.xpath(btn_viewPaymentFinTC002));
		action.moveToElement(we).build().perform();

		click(btn_viewPaymentFinTC002);

		Thread.sleep(4000);
		InventoryAndWarehouseModuleRegression obj = new InventoryAndWarehouseModuleRegression();
		if (obj.isElementPresentQuickCheck(txt_paidAmountCRV)) {

			if (isDisplayed(txt_paidAmountCRV)) {
				writeTestResults(
						"Verify user can view the list of advices of selected customer and from filter currency",
						"User should be able to view the list of advices of selected customer and from filter currency",
						"User successfully view the list of advices of selected customer and from filter currency",
						"pass");
			} else {

				writeTestResults(
						"Verify user can view the list of advices of selected customer and from filter currency",
						"User should be able to view the list of advices of selected customer and from filter currency",
						"User couldn't view the list of advices of selected customer and from filter currency", "fail");
			}

			if (isDisplayed(btn_setOffNosCRV)) {
				writeTestResults("Verify user can navigate to Inbound payment set-off page",
						"User should be able navigate to Inbound payment set-off page",
						"User can navigate to Inbound payment set-off page", "pass");
//			click(btn_setOffNosCRV);
//			if (isDisplayed(txt_painAmountSetOffCRV)) {
//				writeTestResults("Verify user can enter the positive amounts in available field",
//						"Usershould be able to enter the positive amounts in available field",
//						"User successfully enter the positive amounts in available field", "pass");
//				String base_amount = getText(lbl_baseAmountCRVSetOff);
//				System.out.println(base_amount);
//				String base_amount1 = base_amount.replace(".000", "");
//				sendKeys(txt_painAmountSetOffCRV, base_amount1);
//
//				if (isDisplayed(btn_setOffUodateInsideButtonCRV)) {
//					writeTestResults("Verify user can apply the set-off", "User should be able to apply the set-off",
//							"User successfully apply the set-off", "pass");
//					click(btn_setOffUodateInsideButtonCRV);
//					click(btn_closeSetOffPopUp);
//					if (isDisplayed(tab_summaryCRV7)) {
//						writeTestResults(
//								"Verify user can return back to details page, where changes in the applied set-off advice",
//								"User should return back to details page, where changes in the applied set-off advice",
//								"User successfully return back to details page, where changes in the applied set-off advice",
//								"pass");
//					} else {
//
//						writeTestResults(
//								"Verify user can return back to details page, where changes in the applied set-off advice",
//								"User should return back to details page, where changes in the applied set-off advice",
//								"User couldn't return back to details page, where changes in the applied set-off advice",
//								"fail");
//					}
//
//				} else {
//
//					writeTestResults("Verify user can apply the set-off", "User should be able to apply the set-off",
//							"User couldn't apply the set-off", "fail");
//				}
//			} else {
//
//			}

			} else {

				writeTestResults("Verify user can navigate to Inbound payment set-off page",
						"User should be able navigate to Inbound payment set-off page",
						"User couldn't navigate to Inbound payment set-off page", "fail");

			}

			sendKeys(txt_payingAmountCRV, payingAmountCRV7);

			if (isDisplayed(txt_payingAmountCRV)) {
				writeTestResults("Verify user can enter positive numbers only in paid amount field",
						"User should be able to enter positive numbers only in paid amount field",
						"User successfully enter positive numbers only in paid amount field", "pass");
			} else {

				writeTestResults("Verify user can enter positive numbers only in paid amount field",
						"User should be able to enter positive numbers only in paid amount field",
						"User couldn't enter positive numbers only in paid amount field", "fail");
			}
			click(chk_InboundPaymentCRV7);
			if (isSelected(chk_InboundPaymentCRV7)) {
				writeTestResults("Verify user select the advices from the list",
						"Should be able to select the advices from the list",
						"User successfully select the advices from the list", "pass");
			} else {

				writeTestResults("Verify user select the advices from the list",
						"Should be able to select the advices from the list",
						"User couldn't select the advices from the list", "fail");
			}

			sendKeys(txt_paidAmountCRV7, paidAmountCRV7);
			if (isDisplayed(txt_paidAmountCRV7)) {
				writeTestResults(
						"Verify user enter the paid amount as per the actual receival in other than customer advance advices",
						"Should be able to enter the paid amount as per the actual receival in other than customer advance advices",
						"User successfully enter the paid amount as per the actual receival in other than customer advance advices",
						"pass");
			} else {

				writeTestResults(
						"Verify user enter the paid amount as per the actual receival in other than customer advance advices",
						"Should be able to enter the paid amount as per the actual receival in other than customer advance advices",
						"User couldn't enter the paid amount as per the actual receival in other than customer advance advices",
						"fail");
			}
		}
	}

	public void checkoutDraftreleaseCrv7() throws Exception {
		Thread.sleep(2000);
		String ckeckoutBefore = getText(btn_documentDataIPA);

		click(btn_checkout);
		Thread.sleep(3000);
		String ckeckoutAfter = getText(btn_documentDataIPA);
		if (!ckeckoutBefore.equals(ckeckoutAfter)) {
			writeTestResults("Verify calculation, where net amount and Total field will populate the final amount",
					"System allows do calculation, where net amount and Total field will populate the final amount",
					"Calculation, where net amount and Total field will successfully populate the final amount",
					"pass");
		} else {

			writeTestResults("Verify calculation, where net amount and Total field will populate the final amount",
					"System allows do calculation, where net amount and Total field will populate the final amount",
					"Calculation, where net amount and Total field does'nt populate the final amount", "fail");
		}

		click(btn_draft);
		if (isDisplayed(btn_reelese)) {

			writeTestResults("Verify user can draft Customer Recipt Voucher",
					"User should be able to draft Customer Recipt Voucher",
					"User sucessfully draft Customer Recipt Voucher", "pass");
		} else {
			writeTestResults("Verify user can draft Customer Recipt Voucher",
					"User should be able to draft Customer Recipt Voucher",
					"User could'nt draft Customer Recipt Voucher", "fail");
		}

		click(btn_reelese);
		sleepCusomized(btn_print);
		Thread.sleep(3000);
		sleepCusomized(lbl_docNo);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(btn_print)) {

			writeTestResults("Verify user can release Customer Recipt Voucher",
					"User should be able to release Customer Recipt Voucher",
					"User sucessfully release Customer Recipt Voucher", "pass");
		} else {
			writeTestResults("Verify user can release Customer Recipt Voucher",
					"User should be able to release Customer Recipt Voucher",
					"User could'nt release Customer Recipt Voucher", "fail");
		}

		click(btn_actionCRV);
		click(btn_journel);

		if (isDisplayed(header_journelEntry)) {

			writeTestResults("Verify user can view journal entry", "User shold be able to view journal entry",
					"User sucessfully view journal entry", "pass");
		} else {
			writeTestResults("Verify user can view journal entry", "User should be view journal entry",
					"User could'nt view journal entry", "fail");
		}

	}

	public void changeCSS(String locator, String css) {
		try {
			driver.findElement(getLocator(locator)).click();
			Thread.sleep(WAIT * 1);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			WebElement element = driver.findElement(getLocator(locator));
			js.executeScript("arguments[0].setAttribute('style', '" + css + "')", element);

		} catch (Exception e) {

		}
	}

	/* Fin_TC_008 */
	public void findReleventInboundPayment08() throws Exception {
		navigateToTheFananceModule();
		click(btn_inboundPaymentAdvice);
		String doc_no = readFinData(3);
		sendKeys(txt_searchInboundPayment, doc_no);
		pressEnter(txt_searchInboundPayment);
		customizeLoadingDelay(lnk_searchResultInboundPayment.replace("payment_no", doc_no), 15);
		click(lnk_searchResultInboundPayment.replace("payment_no", doc_no));
	}

	public void fillInboundPayment8() throws Exception {
		click(btn_action08);
		if (isDisplayed(btn_converToInboundPayment08)) {

			writeTestResults("Verify user can view availble actions for the selected Inbound payment advice",
					"User should able to view availble actions for the selected Inbound payment advice",
					"User sucessfully view availble actions for the selected Inbound payment advice", "pass");
		} else {
			writeTestResults("Verify user can view availble actions for the selected Inbound payment advice",
					"User should be view availble actions for the selected Inbound payment advice",
					"User could'nt view availble actions for the selected Inbound payment advice", "fail");
		}

		click(btn_converToInboundPayment08);
		if (isDisplayed(btn_docDate08)) {

			writeTestResults("Verify user can view availble actions for the selected Inbound payment advice",
					"User should able to view availble actions for the selected Inbound payment advice",
					"User sucessfully view availble actions for the selected Inbound payment advice", "pass");
		} else {
			writeTestResults("Verify user can view availble actions for the selected Inbound payment advice",
					"User should be view availble actions for the selected Inbound payment advice",
					"User could'nt view availble actions for the selected Inbound payment advice", "fail");
		}

		boolean flag = false;
		boolean flag1 = false;
		flag = !isEnabled(drom_customer8);
		click(btn_detailsPage08);
		flag1 = isDisplayed(chk_IPA08);
		click(tab_summary8);

		if (flag1 && flag) {

			writeTestResults(
					"Verify user can naivgate to Inbound payment page where customer details auto generated and advice selected in step 1 is visible in details page",
					"User should naivgate to Inbound payment page where customer details auto generated and advice selected in step 1 is visible in details page",
					"User sucessfully naivgate to Inbound payment page where customer details auto generated and advice selected in step 1 is visible in details page",
					"pass");
		} else {
			writeTestResults(
					"Verify user can naivgate to Inbound payment page where customer details auto generated and advice selected in step 1 is visible in details page",
					"User should naivgate to Inbound payment page where customer details auto generated and advice selected in step 1 is visible in details page",
					"User couldn't naivgate to Inbound payment page where customer details auto generated and advice selected in step 1 is visible in details page",
					"fail");
		}
		click(btn_docDate08);
		click(btn_calenderBackIAP);
		click(btn_calenderBackIAP);
		click("25_Link");

		click(btn_dueDate08);
		click(btn_calenderForwardIAP);
		click(btn_calenderForwardIAP);
		click("25_Link");

		String doc_date = getText(btn_docDate08).replaceAll("/", "");
		String post_date = getText(btn_postDate08).replaceAll("/", "");
		String due_date = getText(btn_dueDate08).replaceAll("/", "");

		String doc_date1 = reverseStringDateToSLOrder(doc_date);
		String post_date1 = reverseStringDateToSLOrder(post_date);
		String due_date1 = reverseStringDateToSLOrder(due_date);

		int doc_date2 = Integer.parseInt(doc_date1);
		int post_date2 = Integer.parseInt(post_date1);
		int due_date2 = Integer.parseInt(due_date1);

		if (doc_date2 < post_date2 && post_date2 < due_date2) {
			writeTestResults("Verify user can select document date, post date and due date",
					"User should be able to select document date, post date and due date",
					"User successfully select document date, post date and due date", "pass");
		} else {

			writeTestResults("Verify user can select document date, post date and due date",
					"User should be able to select document date, post date and due date",
					"User could'nt select document date, post date and due date", "fail");
		}

		selectText(drop_paidCurrency08, paidCurrency08);
		if (isDisplayed(drop_paidCurrency08)) {
			writeTestResults("Verify user can choose the paid currency mode from the drop down",
					"User should be able to choose the paid currency mode from the drop down",
					"User successfully choose the paid currency mode from the drop down", "pass");
		} else {

			writeTestResults("Verify user can choose the paid currency mode from the drop down",
					"User should be able to choose the paid currency mode from the drop down",
					"User could'nt choose the paid currency mode from the drop down", "fail");
		}
		selectText(drop_paymentMethod08, paymentMethod08);
		if (isDisplayed(drop_paymentMethod08)) {
			writeTestResults("Verify user can choose payment type (Cheque)",
					"User should be able to choose payment type (Cheque)",
					"User successfully choose payment type (Cheque)", "pass");
		} else {

			writeTestResults("Verify user can choose payment type (Cheque)",
					"User should be able to choose payment type (Cheque)", "User could'nt choose payment type (Cheque)",
					"fail");
		}

		selectText(drop_bankCheque8, bankCheque8);

		sendKeys(txt_chequNo8, genIDCheque());

		click(btn_detailsPage08);
		if (isDisplayed(txt_payingAmount08)) {
			writeTestResults("Verify user can navigate to details page", "User should be able navigate to details page",
					"User successfully navigate to details page", "pass");
		} else {

			writeTestResults("Verify user can navigate to details page", "User should be able navigate to details page",
					"User could'nt navigate to details page", "fail");
		}

		sendKeys(txt_payingAmount08, payingAmount08);
		if (isDisplayed(txt_payingAmount08)) {
			writeTestResults("Verify user can enter positive numbers only in paid amount field",
					"User should be able to enter positive numbers only in paid amount field",
					"User successfully enter positive numbers only in paid amount field", "pass");
		} else {

			writeTestResults("Verify user can enter positive numbers only in paid amount field",
					"User should be able enter positive numbers only in paid amount field",
					"User could'nt enter positive numbers only in paid amount field", "fail");
		}

		click(chk_IPA08);
		if (isSelected(chk_IPA08)) {
			writeTestResults("Verify user can select the advices", "User should be able to select the advices",
					"User successfully select the advices", "pass");
		} else {

			writeTestResults("Verify user can select the advices", "User should be able select the advices",
					"User could'nt select the advices", "fail");
		}

		sendKeys(txt_paidAmount08, paidAmpunt8);
		if (isDisplayed(txt_paidAmount08)) {
			writeTestResults(
					"Verify user can enter the paid amount as per the actual receival in other than customer advance advices",
					"User should be able to enter the paid amount as per the actual receival in other than customer advance advices",
					"User successfully enter the paid amount as per the actual receival in other than customer advance advices",
					"pass");
		} else {

			writeTestResults(
					"Verify user can enter the paid amount as per the actual receival in other than customer advance advices",
					"User should be able enter the paid amount as per the actual receival in other than customer advance advices",
					"User could'nt enter the paid amount as per the actual receival in other than customer advance advices",
					"fail");
		}

	}

	public void checkoutDarftreleaseInboundPaymentTC08() throws Exception {
		Thread.sleep(2000);
		String ckeckoutBefore = getText(btn_documentDataIPA);

		click(btn_checkout);
		Thread.sleep(3000);
		String ckeckoutAfter = getText(btn_documentDataIPA);
		if (!ckeckoutBefore.equals(ckeckoutAfter)) {
			writeTestResults("Verify calculation, where net amount and Total field will populate the final amount",
					"System allows do calculation, where net amount and Total field will populate the final amount",
					"Calculation, where net amount and Total field will successfully populate the final amount",
					"pass");
		} else {

			writeTestResults("Verify calculation, where net amount and Total field will populate the final amount",
					"System allows do calculation, where net amount and Total field will populate the final amount",
					"Calculation, where net amount and Total field does'nt populate the final amount", "fail");
		}

		click(btn_draft);
		explicitWait(btn_reelese, 40);
		if (isDisplayed(btn_reelese)) {

			writeTestResults("Verify user can draft Inboumd Payment", "User should be able to draft Inboumd Payment",
					"User sucessfully draft Inboumd Payment", "pass");
		} else {
			writeTestResults("Verify user can draft Inboumd Payment", "User should be able to draft Inboumd Payment",
					"User could'nt draft Inboumd Payment", "fail");
		}

		click(btn_reelese);
		explicitWait(btn_print, 40);
		Thread.sleep(3000);
		sleepCusomized(lbl_docNo);
		trackCode = getText(lbl_docNo);
		System.out.println(getText(lbl_docNoIPA));
		writeFinData("Fin_TC_008-Inbound Payment", getText(lbl_docNoIPA), 5);
		if (isDisplayed(btn_print)) {
			writeTestResults("Verify user can release Inboumd Payment",
					"User should be able to release Inboumd Payment", "User sucessfully release Inboumd Payment",
					"pass");
		} else {
			writeTestResults("Verify user can release Inboumd Payment",
					"User should be able to release Inboumd Payment", "User could'nt release Inboumd Payment", "fail");
		}

	}

	/* Fin_TC_009 */
	public void navigateToBankDepositform09() throws Exception {
		navigateToBankDepositForm();
	}

	public void complteBankDeposiForm() throws Exception {
		selectText(drop_payMethodBankDeposi, payMethodBankDeposit09);
		if (isDisplayed(drop_payMethodBankDeposi)) {
			writeTestResults("Verify user can select pay method", "User should be able to select pay method",
					"User sucessfully select pay method", "pass");
		} else {
			writeTestResults("Verify user can select pay method", "User should be able to select pay method",
					"User could'nt select pay method", "fail");
		}

		selectText(drop_bank, bank09);
		if (isDisplayed(drop_bank)) {
			writeTestResults("Verify user can select bank from drop down",
					"User should be able to select bank from drop down", "User sucessfully select bank from drop down",
					"pass");
		} else {
			writeTestResults("Verify user can select bank from drop down",
					"User should be able to select bank from drop down", "User could'nt select bank from drop down",
					"fail");
		}

		selectText(drop_bankAccountNo, bankAccount09);
		if (isDisplayed(drop_bankAccountNo)) {
			writeTestResults("Verify user can select bank account no", "User should be able to select bank account no",
					"User sucessfully select bank account no", "pass");
		} else {
			writeTestResults("Verify user can select bank account no", "User should be able to select bank account no",
					"User could'nt select bank account no", "fail");
		}
		click(btn_viewBankDeposit);
		if (isDisplayed(btn_viewBankDeposit)) {
			writeTestResults(
					"Verify user can view list of pay items to be deposit (User can able to view advice created in Fin_TC_008 when paymethod is cheque)",
					"User should be able to view list of pay items to be deposit (User can able to view advice created in Fin_TC_008 when paymethod is cheque)",
					"User sucessfully view list of pay items to be deposit (User can able to view advice created in Fin_TC_008 when paymethod is cheque)",
					"pass");
		} else {
			writeTestResults(
					"Verify user can view list of pay items to be deposit (User can able to view advice created in Fin_TC_008 when paymethod is cheque)",
					"User should be able to view list of pay items to be deposit (User can able to view advice created in Fin_TC_008 when paymethod is cheque)",
					"User could'nt view list of pay items to be deposit (User can able to view advice created in Fin_TC_008 when paymethod is cheque)",
					"fail");
		}
		String payment_no = readFinData(5);
		String voucher_ = chk_voucherBankDeposit.replace("voucher_", payment_no);
		click(voucher_);
		if (isSelected(voucher_)) {
			writeTestResults("Verify user can choose vouchers to deposit",
					"User should be able to choose vouchers to deposit", "User sucessfully choose vouchers to deposit",
					"pass");
		} else {
			writeTestResults("Verify user can choose vouchers to deposit",
					"User should be able to choose vouchers to deposit", "User could'nt choose vouchers to deposit",
					"fail");
		}
	}

	public void draftreleaseBAnkDeposit09() throws Exception {
		click(btn_draft);
		if (isDisplayed(btn_reelese)) {

			writeTestResults("Verify user can draft Bank Deposit", "User should be able to draft Bank Deposit",
					"User sucessfully draft Bank Deposit", "pass");
		} else {
			writeTestResults("Verify user can draft Bank Deposit", "User should be able to draft Bank Deposit",
					"User could'nt draft Bank Deposit", "fail");
		}

		click(btn_reelese);
		sleepCusomized(btn_print);
		Thread.sleep(3000);
		sleepCusomized(lbl_docNo);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(btn_print)) {

			writeTestResults("Verify user can release Bank Deposit", "User should be able to release Bank Deposit",
					"User sucessfully release Bank Deposit", "pass");
		} else {
			writeTestResults("Verify user can release Bank Deposit", "User should be able to release Bank Deposit",
					"User could'nt release Bank Deposit", "fail");
		}
	}

	/* Common - Generate ID [Don't use for other test cases] */
	public String genIDCheque() throws InvalidFormatException, IOException {
		/* String readed_id = readFinData(4); No need */
		String readed_id = currentTimeAndDate().replaceAll("/", "").replaceAll(":", "");
		/*
		 * int id = Integer.parseInt(readed_id); id += 1; String id_out =
		 * Integer.toString(id); writeFinData("GenChequID", id_out, 4); No need String
		 * quate = "GSTM-CN-"; String user = InventoryAndWarehouseModule.user; if
		 * (user.equals("Madhushan")) { quate = "GSTM-CN1-"; } String out_final = quate
		 * + id_out;
		 */
		writeFinData("GenChequID", readed_id, 4);
		return readed_id;

	}

	public String genIDLetterOfGurentee() throws InvalidFormatException, IOException {
		String readed_id = readFinData(8);
		int id = Integer.parseInt(readed_id);
		id += 1;
		String id_out = Integer.toString(id);
		writeFinData("GenLOG", id_out, 8);
		String quate = "Madhushan-LOG-";
		String user = InventoryAndWarehouseModule.user;
		if (user.equals("Madhushan")) {
			quate = "Madhushan-LOG1-";
		}
		String out_final = quate + id_out;
		return out_final;

	}

	/* Fin_TC_010 */
	public void navigateToOutboundPaymentInvoiceForm() throws Exception {
		navigateToTheFananceModule();
		click(btn_outboundPaymentAdvice);

		if (isDisplayed(btn_newOutboundPaymentAdvice)) {

			writeTestResults("Verify user can navigate to Outbound Payment Advice by-page",
					"User should navigate to Outbound Payment Advice by-page",
					"User sucessfully navigate to Outbound Payment Advice by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Outbound Payment Advice by-page",
					"User should navigate to Outbound Payment Advice by-page",
					"User couldn't navigate to Outbound Payment Advice by-page", "fail");
		}

		click(btn_newOutboundPaymentAdvice);
		if (isDisplayed(btn_vendorAdvanceJourney)) {
			writeTestResults("Verify user can view the list of journey",
					"User should be able to view the list of journey", "User sucessfully view the list of journey",
					"pass");
		} else {
			writeTestResults("Verify user can view the list of journey", "User should view the list of journey",
					"User couldn't view the list of journey", "fail");
		}

		click(btn_vendorAdvanceJourney);
		if (isDisplayed(btn_searchVendorOPA)) {
			writeTestResults("Verify user can navigate to New Outbound Payment Advice page",
					"User should navigate to New Outbound Payment Advice page",
					"User sucessfully navigate to New Outbound Payment Advice page", "pass");
		} else {
			writeTestResults("Verify user can navigate to New Outbound Payment Advice page",
					"User should navigate to New Outbound Payment Advice page",
					"User couldn't navigate to New Outbound Payment Advice page", "fail");
		}
	}

	public void fillOutboundPaymentAdvice() throws Exception {
		Thread.sleep(2000);
		click(btn_postBusinessUnit);
		Thread.sleep(2000);
		selectText(drop_posBusinessUnit, postBusinessUnitOPA);
		if (isDisplayed(drop_posBusinessUnit)) {
			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User sucessfully select post business unit from the drop down", "pass");
		} else {
			writeTestResults("Verify user can select post business unit from the drop down",
					"User should select post business unit from the drop down",
					"User couldn't select post business unit from the drop down", "fail");
		}

		click(btn_docDate10);
		click(btn_calenderBackIAP);
		click(btn_calenderBackIAP);
		click("25_Link");

		click(btn_dueDate10);
		click(btn_calenderForwardIAP);
		click(btn_calenderForwardIAP);
		click("25_Link");

		String doc_date = getText(btn_docDate10).replaceAll("/", "");
		String post_date = getText(btn_postDate10).replaceAll("/", "");
		String due_date = getText(btn_dueDate10).replaceAll("/", "");

		String doc_date1 = reverseStringDateToSLOrder(doc_date);
		String post_date1 = reverseStringDateToSLOrder(post_date);
		String due_date1 = reverseStringDateToSLOrder(due_date);

		int doc_date2 = Integer.parseInt(doc_date1);
		int post_date2 = Integer.parseInt(post_date1);
		int due_date2 = Integer.parseInt(due_date1);

		if (doc_date2 < post_date2 && post_date2 < due_date2) {
			writeTestResults("Verify user can select document date, post date and due date",
					"User should be able to select document date, post date and due date",
					"User successfully select document date, post date and due date", "pass");
		} else {

			writeTestResults("Verify user can select document date, post date and due date",
					"User should be able to select document date, post date and due date",
					"User could'nt select document date, post date and due date", "fail");
		}

		click(btn_searchVendorOPA);
		if (isDisplayed(txt_serchVendorOPA)) {
			writeTestResults("Verify user can click on search button in Vendor and Vendor Lookup is pop up",
					"User should be click on search button in Vendor and Vendor Lookup should pop up",
					"User successfully click on search button in Vendor and Vendor Lookup sucessfully pop up", "pass");
		} else {

			writeTestResults("Verify user can click on search button in Vendor and Vendor Lookup is pop up",
					"User should be click on search button in Vendor and Vendor Lookup should pop up",
					"User couldn't click on search button in Vendor and Vendor Lookup doesn't pop up", "fail");
		}

		sleepCustomize2(txt_serchVendorOPA);
		sendKeys(txt_serchVendorOPA, vendorOutboundPaymentAdvice);
		pressEnter(txt_serchVendorOPA);
		sleepCustomize2(lnk_resultVendorOPA);
		if (isDisplayed(lnk_resultVendorOPA)) {
			writeTestResults("Verify user can search an existing Vendor by entering Vendor name/code",
					"User should be able search an existing Vendor by entering Vendor name/code",
					"User successfully search an existing Vendor by entering Vendor name/code", "pass");
		} else {

			writeTestResults("Verify user can search an existing Vendor by entering Vendor name/code",
					"User should be able search an existing Vendor by entering Vendor name/code",
					"User couldn't search an existing Vendor by entering Vendor name/code", "fail");
		}
		doubleClick(lnk_resultVendorOPA);

		String billing_address = txt_billingAddress;
		if (!billing_address.equals("") && isDisplayed(drop_contactPerson)) {
			writeTestResults(
					"Verify selected Vendor name and Vendor code will populate the Vendor, Contact Person and associated Billing Address will be loaded as per the Vendor",
					"Selected Vendor name and Vendor code will populate the Vendor, Contact Person and associated Billing Address will be loaded as per the Vendor",
					"Successfully Selected Vendor name and Vendor code will populate the Vendor, Contact Person and associated Billing Address will be loaded as per the Vendor",
					"pass");
		} else {

			writeTestResults(
					"Verify selected Vendor name and Vendor code will populate the Vendor, Contact Person and associated Billing Address will be loaded as per the Vendor",
					"Selected Vendor name and Vendor code will populate the Vendor, Contact Person and associated Billing Address will be loaded as per the Vendor",
					"Doesn't Selected Vendor name and Vendor code will populate the Vendor, Contact Person and associated Billing Address will be loaded as per the Vendor",
					"fail");
		}

		sendKeys(txt_referenceCodeOPA, referenceCodeOPA);
		if (isDisplayed(txt_referenceCodeOPA)) {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Reference Code",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Reference Code",
					"User successfully enter characters, numbers, alphanumeric and special characters to Reference Code",
					"pass");
		} else {

			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Reference Code",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Reference Code",
					"User couldn't enter characters, numbers, alphanumeric and special characters to Reference Code",
					"fail");
		}
		sendKeys(txt_descriptionOPA, descriptionOPA);
		if (isDisplayed(txt_descriptionOPA)) {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User successfully enter characters, numbers, alphanumeric and special characters to Description",
					"pass");
		} else {

			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User couldn't enter characters, numbers, alphanumeric and special characters to Description",
					"fail");
		}

		selectText(drop_currencyOPA, currencyOPA);
		if (isDisplayed(drop_currencyOPA)) {
			writeTestResults("Verify user can choose the currency mode from the drop down",
					"User should be able to choose the currency mode from the drop down",
					"User successfully choose the currency mode from the drop down", "pass");
		} else {

			writeTestResults("Verify user can choose the currency mode from the drop down",
					"User should be able to choose the currency mode from the drop down",
					"User couldn't choose the currency mode from the drop down", "fail");
		}

		selectIndex(drop_taxOPA, 2);
		if (isDisplayed(drop_taxOPA)) {
			writeTestResults("Verify user can choose the applicable tax mode",
					"User should be able to choose the applicable tax mode",
					"User successfully choose the applicable tax mode", "pass");
		} else {

			writeTestResults("Verify user can choose the applicable tax mode",
					"User should be able to choose the applicable tax mode",
					"User couldn't choose the applicable tax mode", "fail");
		}

		sendKeys(txt_amountOPA, amountOPA);
		if (isDisplayed(txt_amountOPA)) {
			writeTestResults("Verify user can enter positive numbers only in amount field",
					"User should be able to enter positive numbers only in amount field",
					"User successfully enter positive numbers only in amount field", "pass");
		} else {

			writeTestResults("Verify user can enter positive numbers only in amount field",
					"User should be able to enter positive numbers only in amount field",
					"User couldn't enter positive numbers only in amount field", "fail");
		}

	}

	public void checkoutDarftreleaseOutboundPayment() throws Exception {
		Thread.sleep(2000);
		String ckeckoutBefore = getText(btn_documentDataIPA);

		click(btn_checkout);
		Thread.sleep(3000);
		String ckeckoutAfter = getText(btn_documentDataIPA);
		if (!ckeckoutBefore.equals(ckeckoutAfter)) {
			writeTestResults("Verify calculation, where net amount and Total field will populate the final amount",
					"System allows do calculation, where net amount and Total field will populate the final amount",
					"Calculation, where net amount and Total field will successfully populate the final amount",
					"pass");
		} else {

			writeTestResults("Verify calculation, where net amount and Total field will populate the final amount",
					"System allows do calculation, where net amount and Total field will populate the final amount",
					"Calculation, where net amount and Total field does'nt populate the final amount", "fail");
		}

		click(btn_draft);
		if (isDisplayed(btn_reelese)) {

			writeTestResults("Verify user can draft Inboumd Payment",
					"User should be able to draft Outbound Payment Advice",
					"User sucessfully draft Outbound Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can draft Outbound Payment Advice",
					"User should be able to draft Outbound Payment Advice",
					"User could'nt draft Outbound Payment Advice", "fail");
		}
		sleepCustomize2(btn_reelese);
		click(btn_reelese);
		sleepCusomized(btn_print);
		Thread.sleep(3000);
		String doc = getText(lbl_docNo);
		writeFinData("Fin_TC_010_Outboun Payment Advice", doc, 6);
		sleepCusomized(lbl_docNo);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(btn_print)) {
			writeTestResults("Verify user can release Outbound Payment Advice",
					"User should be able to release Outbound Payment Advice",
					"User sucessfully release Outbound Payment Advice", "pass");
		} else {
			writeTestResults("Verify user can release Outbound Payment Advice",
					"User should be able to release Outbound Payment Advice",
					"User could'nt release Outbound Payment Advice", "fail");
		}

	}

	/* Fin_TC_011 */
	public void navigateToOutboundPaymentAdvice() throws Exception {
		navigateToTheFananceModule();
		explicitWait(btn_outboundPaymentAdvice, 40);
		click(btn_outboundPaymentAdvice);
		explicitWait(txt_searchOutboundPaymentAdvice, 40);
		sendKeys(txt_searchOutboundPaymentAdvice, readFinData(6));
		pressEnter(txt_searchOutboundPaymentAdvice);
		Thread.sleep(2000);
		String lnk_result = lnk_outboundPaymentAdvice.replace("payment_a", readFinData(6));
		customizeLoadingDelay(lnk_result, 30);
		click(lnk_result);
	}

	public void converToOutboundPayment() throws Exception {
		Thread.sleep(2500);
		switchWindow();
		InventoryAndWarehouseModule obj = new InventoryAndWarehouseModule();
		obj.implisitWait(8);
		Thread.sleep(2000);
		click(btn_action);
		if (isDisplayed(btn_convertToOutboundPaymentAdvice)) {
			writeTestResults("Verify user can view availble actions for the selected Outbound payment advice",
					"User should able to view availble actions for the selected Outbound payment advice",
					"User sucessfully view availble actions for the selected Outbound payment advice", "pass");
		} else {
			writeTestResults("Verify user can view availble actions for the selected Outbound payment advice",
					"User should be able to view availble actions for the selected Outbound payment advice",
					"User could'nt view availble actions for the selected Outbound payment advice", "fail");
		}

		click(btn_convertToOutboundPaymentAdvice);
		Thread.sleep(3000);
		switchWindow();
		sleepCustomize3(drop_currency);
		if (isDisplayed(drop_currency)) {
			writeTestResults(
					"Verify user can naivgate to Outbound payment page where Vendor details auto generated and advice selected in step 1 is visible in details page",
					"User should naivgate to Outbound payment page where Vendor details auto generated and advice selected in step 1 is visible in details page",
					"User sucessfully view naivgate to Outbound payment page where Vendor details auto generated and advice selected in step 1 is visible in details page",
					"pass");
		} else {
			writeTestResults(
					"Verify user can naivgate to Outbound payment page where Vendor details auto generated and advice selected in step 1 is visible in details page",
					"User should be able to naivgate to Outbound payment page where Vendor details auto generated and advice selected in step 1 is visible in details page",
					"User could'nt naivgate to Outbound payment page where Vendor details auto generated and advice selected in step 1 is visible in details page",
					"fail");
		}

	}

	public void fillOutboundPayment() throws Exception {
		click(btn_docDate);
		click(btn_calenderBack);
		click(btn_calenderBack);
		click("25_Link");

		click(btn_dueDate);
		click(btn_calenderForward);
		click(btn_calenderForward);
		click("25_Link");

		String doc_date = getText(btn_docDate).replaceAll("/", "");
		String post_date = getText(btn_postDate).replaceAll("/", "");
		String due_date = getText(btn_dueDate).replaceAll("/", "");

		String doc_date1 = reverseStringDateToSLOrder(doc_date);
		String post_date1 = reverseStringDateToSLOrder(post_date);
		String due_date1 = reverseStringDateToSLOrder(due_date);

		int doc_date2 = Integer.parseInt(doc_date1);
		int post_date2 = Integer.parseInt(post_date1);
		int due_date2 = Integer.parseInt(due_date1);

		if (doc_date2 < post_date2 && post_date2 < due_date2) {
			writeTestResults("Verify user can select document date, post date and due date",
					"User should be able to select document date, post date and due date",
					"User successfully select document date, post date and due date", "pass");
		} else {

			writeTestResults("Verify user can select document date, post date and due date",
					"User should be able to select document date, post date and due date",
					"User could'nt select document date, post date and due date", "fail");
		}

		selectText(drop_currency, curencyTypeOutboundPayment);
		if (isDisplayed(drop_currency)) {
			writeTestResults("Verify user can choose the paid currency mode from the drop down",
					"User should be able to choose the paid currency mode from the drop down",
					"User successfully choose the paid currency mode from the drop down", "pass");
		} else {

			writeTestResults("Verify user can choose the paid currency mode from the drop down",
					"User should be able to choose the paid currency mode from the drop down",
					"User could'nt choose the paid currency mode from the drop down", "fail");
		}

		selectText(drop_payBook, payBookOutboundPayment);
		if (isDisplayed(drop_payBook)) {
			writeTestResults("Verify user can choose relevent Pay Book",
					"User should be able to choose relevent Pay Book", "User successfully choose relevent Pay Book",
					"pass");
		} else {

			writeTestResults("Verify user can choose relevent Pay Book",
					"User should be able to choose relevent Pay Book", "User could'nt choose relevent Pay Book",
					"fail");
		}
		sleepCustomize3(tab_detailOutboundPayment);
		click(tab_detailOutboundPayment);
		sleepCusomized(drop_filterCurrencyOutboundPaymnet);
		if (isDisplayed(drop_filterCurrencyOutboundPaymnet)) {
			writeTestResults("Verify user can navigate to details page",
					"User should be able to navigate to details page", "User successfully navigate to details page",
					"pass");
		} else {

			writeTestResults("Verify user can navigate to details page",
					"User should be able to navigate to details page", "User could'nt navigate to details page",
					"fail");
		}

	}

	public void fillDetailPage() throws Exception {
		selectText(drop_filterCurrencyOutboundPaymnet, filterCurencyTypeOutboundPayment);
		if (isDisplayed(drop_filterCurrencyOutboundPaymnet)) {
			writeTestResults("Verify user can able to set filter currency",
					"User should be able to able to set filter currency",
					"User successfully able to set filter currency", "pass");
		} else {

			writeTestResults("Verify user can able to set filter currency",
					"User should be able to able to set filter currency", "User could'nt able to set filter currency",
					"fail");
		}

		Actions action = new Actions(driver);
		WebElement we = driver.findElement(By.xpath(btn_viewButonOutboundPayment));
		action.moveToElement(we).build().perform();
		click(btn_viewButonOutboundPayment);
		Thread.sleep(3000);
		InventoryAndWarehouseModule obj = new InventoryAndWarehouseModule();
		String chk_path = chk_choseOPA.replace("doc_numbe", readFinData(6));
		obj.sleepCusomized(chk_path);
		if (isDisplayed(chk_path)) {
			writeTestResults("Verify Outbound Payment Advice appeared to process ahead",
					"Outbound Payment Advice should be appeared to process ahead",
					"Outbound Payment Advice successfully appeared to process ahead", "pass");
		} else {

			writeTestResults("Verify Outbound Payment Advice appeared to process ahead",
					"Outbound Payment Advice should be appeared to process ahead",
					"Outbound Payment Advice doesn't appeared to process ahead", "fail");
		}

		click(chk_path);
		if (isSelected(chk_path)) {
			writeTestResults("Verify user can select the advices", "User should be able to select the advices",
					"User successfully select the advices", "pass");
		} else {

			writeTestResults("Verify user can select the advices", "User should be able to select the advices",
					"User could'nt select the advices", "fail");
		}
	}

	public void draftreleaseChecoutOP() throws Exception {
		Thread.sleep(2000);
		String ckeckoutBefore = getText(btn_documentDataIPA);

		click(btn_checkout);
		Thread.sleep(3500);
		String ckeckoutAfter = getText(btn_documentDataIPA);
		if (!ckeckoutBefore.equals(ckeckoutAfter)) {
			writeTestResults("Verify calculation, where net amount and Total field will populate the final amount",
					"System allows do calculation, where net amount and Total field will populate the final amount",
					"Calculation, where net amount and Total field will successfully populate the final amount",
					"pass");
		} else {

			writeTestResults("Verify calculation, where net amount and Total field will populate the final amount",
					"System allows do calculation, where net amount and Total field will populate the final amount",
					"Calculation, where net amount and Total field does'nt populate the final amount", "fail");
		}

		click(btn_draft);
		InventoryAndWarehouseModule obj = new InventoryAndWarehouseModule();
		obj.implisitWait(5);
		if (isDisplayed(btn_reelese)) {

			writeTestResults("Verify user can draft Inboumd Payment", "User should be able to draft Inboumd Payment",
					"User sucessfully draft Inboumd Payment", "pass");
		} else {
			writeTestResults("Verify user can draft Inboumd Payment", "User should be able to draft Inboumd Payment",
					"User could'nt draft Inboumd Payment", "fail");
		}

		click(btn_reelese);
		obj.implisitWait(10);
		Thread.sleep(3000);
		sleepCusomized(lbl_docNo);
		trackCode = getText(lbl_docNo);
		if (isDisplayed(btn_print)) {
			writeTestResults("Verify user can release Inboumd Payment",
					"User should be able to release Inboumd Payment", "User sucessfully release Inboumd Payment Advice",
					"pass");
		} else {
			writeTestResults("Verify user can release Inboumd Payment",
					"User should be able to release Inboumd Payment", "User could'nt release Inboumd Payment", "fail");
		}
	}

	/* Fin_TC_012 */
	public void navigateToThePurchaseInvoice() throws Exception {
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		click(navigation_pane);
		click(btn_procument);
		click(btn_purchaseInvoice);
		String header = getText(header_page);
		if (header.equals("Purchase Invoice")) {
			writeTestResults("Verify user can navigate to Purchase Invoice by-page",
					"User should be able to navigate to Purchase Invoice by-page",
					"User successfully navigate to Purchase Invoice by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Purchase Invoice by-page",
					"User should be able to navigate to Purchase Invoice by-page",
					"User could'nt navigate to Purchase Invoice by-page", "fail");
		}
		click(btn_newPurchaseInvoice);
		click(btn_purchaseInvoiceJourney);
		String status = getText(lbl_docStatus);
		if (status.equals("New")) {
			writeTestResults("Verify user can navigate to Purchase invoice new page",
					"User should be able to navigate to Purchase invoice new page",
					"User successfully navigate to Purchase Invoice new page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Purchase invoice new page",
					"User should be able to navigate to Purchase invoice new page",
					"User could'nt navigate to Purchase invoice new page", "fail");
		}
	}

	public void compltePurchaseInvoice() throws Exception {
		String btn_postDateBefore = getText(btn_postDate).replaceAll("/", "");
		String btn_postDate1 = reverseStringDateToSLOrder(btn_postDateBefore);
		int post_date2 = Integer.parseInt(btn_postDate1);
		click(btn_postDate);
		click(btn_pdPostDate);
		click(btn_calenderBack);
		Thread.sleep(2000);
		click("1_Link");
		Thread.sleep(2000);
		click(btn_dateApply);

		String post_dateAfter = getText(btn_postDate).replaceAll("/", "");
		String post_date1After = reverseStringDateToSLOrder(post_dateAfter);
		int post_date2After = Integer.parseInt(post_date1After);

		if (post_date2 > post_date2After) {
			writeTestResults("Verify user can select post date later date than vendorr receipt voucher date",
					"User should be select post date later date than vendorr receipt voucher date",
					"User successfully select post date later date than vendorr receipt voucher date", "pass");
		} else {
			writeTestResults("Verify user can select post date later date than vendorr receipt voucher date",
					"User should be able to select post date later date than vendorr receipt voucher date",
					"User could'nt select post date later date than vendorr receipt voucher date", "fail");
		}

		click(btn_serchVendor);
		sleepCustomize3(txt_vendorSearch);
		sendKeys(txt_vendorSearch, vendorPurchaseInvoice);
		pressEnter(txt_vendorSearch);
		Thread.sleep(2000);
		sleepCustomize3(lnk_resultVendorSearch);
		doubleClick(lnk_resultVendorSearch);

		selectText(drop_currency, currencyPurchaseInvoice);
		if (isDisplayed(drop_currency)) {
			writeTestResults("Verify user can select currency as USD", "User should be select currency as USD",
					"User successfully select currency as USD", "pass");
		} else {
			writeTestResults("Verify user can select currency as USD", "User should be able to select currency as USD",
					"User could'nt select currency as USD", "fail");
		}

		click(btn_productLoockup);
		sleepCustomize3(txt_productSearch);
		sendKeys(txt_productSearch, serviceProductPurchaseInvoice);
		pressEnter(txt_productSearch);
		Thread.sleep(2000);
		sleepCustomize3(lnk_resultProduct);
		doubleClick(lnk_resultProduct);

		sendKeys(txt_unitPricePurchaseInvoice, unitPricePurchaseInvice);
		int unit_price = Integer.parseInt(unitPricePurchaseInvice);
		int amount_opa = Integer.parseInt(amountOPA);
		if (unit_price < amount_opa) {
			writeTestResults("Verify user entered amount is greater than Vendor Recipt Voucher amount",
					"User should be enter amount greater than Vendor Recipt Voucher amount",
					"User successfully enter amount greater than Vendor Recipt Voucher amount", "pass");
		} else {
			writeTestResults("Verify user entered amount is greater than Vendor Recipt Voucher amount",
					"User should be enter amount greater than Vendor Recipt Voucher amount",
					"User couldn't enter amount greater than Vendor Recipt Voucher amount", "pass");
		}

	}

	public void checkoutDraftReleasePurchaseInvoice() throws Exception {
		Thread.sleep(2000);
		String ckeckoutBefore = getText(btn_documentValueTotal);

		click(btn_checkout);
		Thread.sleep(3000);
		String ckeckoutAfter = getText(btn_documentValueTotal);
		if (!ckeckoutBefore.equals(ckeckoutAfter)) {
			writeTestResults("Verify user can create Purchase invoice with the specified details",
					"User should be ble to create Purchase invoice with the specified details",
					"User ssuccessfully create Purchase invoice with the specified details", "pass");
		} else {

			writeTestResults("Verify user can create Purchase invoice with the specified details",
					"User should be ble to create Purchase invoice with the specified details",
					"User couldn't create Purchase invoice with the specified details", "fail");
		}

		click(btn_draft);
		explicitWaitUntillClickable(btn_reelese, 50);
		click(btn_reelese);
		sleepCustomize3(lbl_docStatus);
		Thread.sleep(3000);
		String status = getText(lbl_docStatus);
		sleepCusomized(lbl_docNo);
		trackCode = getText(lbl_docNo);
		if (!status.equalsIgnoreCase("(Released)")) {
			Thread.sleep(7000);
			status = getText(lbl_docStatus);
		}
		if (status.equals("(Released)")) {
			writeTestResults("Verify user can draft and release Purchase Invoice",
					"User should be able to draft and release Purchase Invoice",
					"User successfully draft and release Purchase Invoice", "pass");
		} else {
			writeTestResults("Verify user can draft and release Purchase Invoice",
					"User should be able to draft and release Purchase Invoice",
					"User could'nt draft and release Purchase Invoice", "fail");
		}
		Thread.sleep(5000);
		click(btn_action);
		click(btn_docFlow);
	}

	/* Common - Wait for element - Not used yet */
	public static void waitforElement(String element) throws InterruptedException {
		try {
			WebElement relesase = driver.findElement(By.xpath(element));
			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.elementToBeClickable(relesase));
		} catch (Exception e) {
		}
	}

	/* Fin_TC_014 */
	public void navigateToVendorCrediMemo() throws Exception {
		navigateToTheFananceModule();
		click(btn_outboundPaymentAdvice);
		String header = getText(header_page);
		if (header.equals("Outbound Payment Advice")) {
			writeTestResults("Verify user can navigate to Outbound payment advice by-page",
					"User should navigate to Outbound payment advice by-page",
					"User successfully navigate to Outbound payment advice by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Outbound payment advice by-page",
					"User should be able to navigate to Outbound payment advice by-page",
					"User could'nt navigate to Outbound payment advice by-page", "fail");
		}

		click(btn_newInboundPaymentAdvice);
		if (isDisplayed(btn_vendorCreditMemoJourney)) {
			writeTestResults("Verify user can view the list of journey",
					"User should be able to view the list of journey", "User successfully view the list of journey",
					"pass");
		} else {
			writeTestResults("Verify user can view the list of journey",
					"User should be able to view the list of journey", "User could'nt view the list of journey",
					"fail");
		}

		click(btn_vendorCreditMemoJourney);
		if (isDisplayed(btn_vendorLookupCreditMemo)) {
			writeTestResults("Verify user can navigate to New Vendor credit memo page",
					"User should be able to navigate to New Vendor credit memo page",
					"User successfully navigate to New Vendor credit memo page", "pass");
		} else {
			writeTestResults("Verify user can navigate to New Vendor credit memo page",
					"User should be able to navigate to New Vendor credit memo page",
					"User could'nt navigate to New Vendor credit memo page", "fail");
		}

	}

	public void fillVendorCreditMemo() throws Exception {
		click(btn_postBusinessUnit);
		selectText(drop_postBusinessUnit, postBusinessUnit);
		click(txt_description);
		Thread.sleep(2000);
		String post_business_unit = getText(btn_postBusinessUnit);
		if (post_business_unit.equals(postBusinessUnit)) {
			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User successfully select post business unit from the drop down", "pass");
		} else {
			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User could'nt select post business unit from the drop down", "fail");
		}

		click(btn_docDate);
		click(btn_calenderBack);
		click(btn_calenderBack);
		click("25_Link");

		click(btn_dueDate);
		click(btn_calenderForward);
		click(btn_calenderForward);
		click("25_Link");

		String doc_date = getText(btn_docDate).replaceAll("/", "");
		String post_date = getText(btn_postDate).replaceAll("/", "");
		String due_date = getText(btn_dueDate).replaceAll("/", "");

		String doc_date1 = reverseStringDateToSLOrder(doc_date);
		String post_date1 = reverseStringDateToSLOrder(post_date);
		String due_date1 = reverseStringDateToSLOrder(due_date);

		int doc_date2 = Integer.parseInt(doc_date1);
		int post_date2 = Integer.parseInt(post_date1);
		int due_date2 = Integer.parseInt(due_date1);

		if (doc_date2 < post_date2 && post_date2 < due_date2) {
			writeTestResults("Verify user can select document date, post date and due date",
					"User should be able to select document date, post date and due date",
					"User successfully select document date, post date and due date", "pass");
		} else {

			writeTestResults("Verify user can select document date, post date and due date",
					"User should be able to select document date, post date and due date",
					"User could'nt select document date, post date and due date", "fail");
		}

		click(btn_vendorLookupCreditMemo);
		if (isDisplayed(txt_vendorSearch)) {
			writeTestResults("Verify user can click on Search button in Vendor and Vendor Lookup should pop up",
					"User should be able to click on Search button in Vendor and Vendor Lookup should pop up",
					"User successfully click on Search button in Vendor and Vendor Lookup should pop up", "pass");
		} else {

			writeTestResults("Verify user can click on Search button in Vendor and Vendor Lookup should pop up",
					"User should be able to click on Search button in Vendor and Vendor Lookup should pop up",
					"User could'nt click on Search button in Vendor and Vendor Lookup should pop up", "fail");
		}

		sleepCustomize3(txt_vendorSearch);
		sendKeys(txt_vendorSearch, vendorVendorCreditMemo);
		pressEnter(txt_vendorSearch);
		sleepCustomize3(lnk_resultVendorSearch);
		if (isDisplayed(lnk_resultVendorSearch)) {
			writeTestResults("Verify user can search an existing Vendor by entering Vendor name/code",
					"User should be able to search an existing Vendor by entering Vendor name/code",
					"User successfully search an existing Vendor by entering Vendor name/code", "pass");
		} else {

			writeTestResults("Verify user can search an existing Vendor by entering Vendor name/code",
					"User should be able to search an existing Vendor by entering Vendor name/code",
					"User could'nt search an existing Vendor by entering Vendor name/code", "fail");
		}
		doubleClick(lnk_resultVendorSearch);

		boolean billing_address = isDisplayed(txt_billingAddress);
		String contact_person = getText(drop_contactPerson);

		boolean contact_person_flag = !contact_person.equals("");

		if (billing_address && contact_person_flag) {
			writeTestResults(
					"Verify selected Vendor name and Vendor code will populate the Vendor, Contact Person and associated Billing Address will be loaded as per the Vendor",
					"Selected Vendor name and Vendor code will populate the Vendor, Contact Person and associated Billing Address will be loaded as per the Vendor",
					"Successfully selected Vendor name and Vendor code will populate the Vendor, Contact Person and associated Billing Address will be loaded as per the Vendor",
					"pass");
		} else {

			writeTestResults(
					"Verify selected Vendor name and Vendor code will populate the Vendor, Contact Person and associated Billing Address will be loaded as per the Vendor",
					"Selected Vendor name and Vendor code will populate the Vendor, Contact Person and associated Billing Address will be loaded as per the Vendor",
					"Doesn't selected Vendor name and Vendor code will populate the Vendor, Contact Person and associated Billing Address will be loaded as per the Vendor",
					"fail");
		}

		sendKeys(txt_description, description);
		if (isDisplayed(txt_description)) {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User successfully enter characters, numbers, alphanumeric and special characters to Description",
					"pass");
		} else {

			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User could't enter characters, numbers, alphanumeric and special characters to Description",
					"fail");
		}
		sendKeys(txt_referenceCode, referenceCode);
		if (isDisplayed(txt_referenceCode)) {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Reference Code",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Reference Code",
					"User successfully enter characters, numbers, alphanumeric and special characters to Reference Code",
					"pass");
		} else {

			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Reference Code",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Reference Code",
					"User could't enter characters, numbers, alphanumeric and special characters to Reference Code",
					"fail");
		}

		selectText(drop_currency, curencyCreditMemo);
		if (isDisplayed(drop_currency)) {
			writeTestResults("Verify user can choose the currency mode from the drop down",
					"User should be able to choose the currency mode from the drop down",
					"User successfully choose the currency mode from the drop down", "pass");
		} else {

			writeTestResults("Verify user can choose the currency mode from the drop down",
					"User should be able to choose the currency mode from the drop down",
					"User could't choose the currency mode from the drop down", "fail");
		}

		selectIndex(drop_tax, 2);
		if (isDisplayed(drop_tax)) {
			writeTestResults("Verify user can choose the applicable Tax mode",
					"User should be able to choose the applicable Tax mode",
					"User successfully choose the applicable Tax mode", "pass");
		} else {

			writeTestResults("Verify user can choose the applicable Tax mode",
					"User should be able to choose the applicable Tax mode",
					"User could't choose the applicable Tax mode", "fail");
		}

		click(btn_glLookupCreditMemory);
		sleepCustomize3(txt_GL);
		sendKeys(txt_GL, glAccountCreditMemo);
		pressEnter(txt_GL);
		Thread.sleep(2000);
		sleepCustomize3(lnk_resultGL);
		if (isDisplayed(lnk_resultGL)) {
			writeTestResults(
					"Verify user can search an existing GL Account by entering GL Account name/code and by double click  the GL account user can allow to populate in the field",
					"User should be able to search an existing GL Account by entering GL Account name/code and by double click  the GL account user can allow to populate in the field",
					"User successfully search an existing GL Account by entering GL Account name/code and by double click  the GL account user can allow to populate in the field",
					"pass");
		} else {

			writeTestResults(
					"Verify user can search an existing GL Account by entering GL Account name/code and by double click  the GL account user can allow to populate in the field",
					"User should be able to search an existing GL Account by entering GL Account name/code and by double click  the GL account user can allow to populate in the field",
					"User could't search an existing GL Account by entering GL Account name/code and by double click  the GL account user can allow to populate in the field",
					"fail");
		}
		doubleClick(lnk_resultGL);

		sendKeys(txt_amountCreditMemo, amountCreditMemo);
		if (isDisplayed(txt_amountCreditMemo)) {
			writeTestResults("Verify user can enter positive numbers only in amount field",
					"User should be able to enter positive numbers only in amount field",
					"User successfully enter positive numbers only in amount field", "pass");
		} else {

			writeTestResults("Verify user can enter positive numbers only in amount field",
					"User should be able to enter positive numbers only in amount field",
					"User could't enter positive numbers only in amount field", "fail");
		}

		Thread.sleep(2000);
		click(ico_noCostAlloacation);
		if (isDisplayed(drop_costCenter)) {
			writeTestResults("Verify user can navigate to cost allocation page",
					"User should be able to navigate to cost allocation page",
					"User successfully navigate to cost allocation page", "pass");
		} else {

			writeTestResults("Verify user can navigate to cost allocation page",
					"User should be able to navigate to cost allocation page",
					"User could't navigate to cost allocation page", "fail");
		}

		selectText(drop_costCenter, costUnit);
		sendKeys(txt_apportionPresentage, costCenterAlloacatePresentage);
		click(btn_addRecordApportion);
		if (isDisplayed(btn_update)) {
			writeTestResults(
					"Verify user can choose single or multiple cost centers and able to allocate the percentage = 100% and allows save the allocation by clicking update button",
					"User should be able to choose single or multiple cost centers and able to allocate the percentage = 100% and allows save the allocation by clicking update button",
					"User successfully choose single or multiple cost centers and able to allocate the percentage = 100% and allows save the allocation by clicking update button",
					"pass");
		} else {

			writeTestResults(
					"Verify user can choose single or multiple cost centers and able to allocate the percentage = 100% and allows save the allocation by clicking update button",
					"User should be able to choose single or multiple cost centers and able to allocate the percentage = 100% and allows save the allocation by clicking update button",
					"User could't choose single or multiple cost centers and able to allocate the percentage = 100% and allows save the allocation by clicking update button",
					"fail");
		}
		click(btn_update);
	}

	public void checkoutDraftAndReleaseVendorCreditMemo() throws Exception {
		Thread.sleep(2000);
		String ckeckoutBefore = getText(btn_documentValueTotal);

		click(btn_checkout);
		Thread.sleep(3000);
		String ckeckoutAfter = getText(btn_documentValueTotal);
		if (!ckeckoutBefore.equals(ckeckoutAfter)) {
			writeTestResults("Verify calculation, where net amount and Total field will populate the final amount",
					"System allows do calculation, where net amount and Total field will populate the final amount",
					"Calculation, where net amount and Total field will successfully populate the final amount",
					"pass");
		} else {

			writeTestResults("Verify calculation, where net amount and Total field will populate the final amount",
					"System allows do calculation, where net amount and Total field will populate the final amount",
					"Calculation, where net amount and Total field does'nt populate the final amount", "fail");
		}

		click(btn_draft);
		Thread.sleep(3000);
		explicitWait(btn_reelese, 30);
		if (isDisplayed(btn_reelese)) {
			writeTestResults("Verify user can draft Outbound Payment Advice - Vendor Credit Memo",
					"User should be able to draft Outbound Payment Advice - Vendor Credit Memo",
					"User successfully draft Outbound Payment Advice - Vendor Credit Memo", "pass");
		} else {
			writeTestResults("Verify user can draft Outbound Payment Advice - Vendor Credit Memo",
					"User should be able to draft Outbound Payment Advice - Vendor Credit Memo",
					"User could'nt draft Outbound Payment Advice - Vendor Credit Memo", "fail");
		}
		click(btn_reelese);
		explicitWait(header_releasedOutboundPaymentAdvice, 40);
		String doc_no = getText(lbl_docNo);
		writeFinData("Fin_TC_014_Outboun Payment Advice(Vendor Credit Memo)", doc_no, 7);
		String status = getText(lbl_docStatus);
		sleepCusomized(lbl_docNo);
		trackCode = getText(lbl_docNo);
		if (!status.equalsIgnoreCase("( Released )")) {
			Thread.sleep(7000);
			status = getText(lbl_docStatus);
		}
		if (!status.equalsIgnoreCase("( Released )")) {
			Thread.sleep(7000);
			status = getText(lbl_docStatus);
		}
		if (status.equals("( Released )")) {
			writeTestResults("Verify user can release Outbound Payment Advice - Vendor Credit Memo",
					"User should be able to release Outbound Payment Advice - Vendor Credit Memo",
					"User successfully release Outbound Payment Advice - Vendor Credit Memo", "pass");
		} else {
			writeTestResults("Verify user can release Outbound Payment Advice - Vendor Credit Memo",
					"User should be able to release Outbound Payment Advice - Vendor Credit Memo",
					"User could'nt release Outbound Payment Advice - Vendor Credit Memo", "fail");
		}
	}

	/* Fin_TC_015 */
	public void navigateToVendorCreditVoucher() throws Exception {
		navigateToTheFananceModule();
		click(btn_outboundPayment);
		String header = getText(header_page);
		if (header.equals("Outbound Payment")) {
			writeTestResults("Verify user can navigate to Outbound Payment by-page",
					"User should be able to navigate to Outbound Payment by-page",
					"User successfully navigate to Outbound Payment by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Outbound Payment by-page",
					"User should be able to navigate to Outbound Payment by-page",
					"User could'nt navigate to Outbound Payment by-page", "fail");
		}

		click(btn_addNew);
		if (isDisplayed(btn_vendorPaymentVoucherJourney)) {
			writeTestResults("Verify user can view the list of journey",
					"User should be able to view the list of journey", "User successfully view the list of journey",
					"pass");
		} else {
			writeTestResults("Verify user can view the list of journey",
					"User should be able to view the list of journey", "User could'nt view the list of journey",
					"fail");
		}

		click(btn_vendorPaymentVoucherJourney);
		String doc_status = getText(lbl_docStatus);

		if (doc_status.equals("New Vendor Payment")) {
			writeTestResults("Verify user can navigate to new Vendor Payment Voucher page",
					"User should be able to navigate to new Vendor Payment Voucher page",
					"User successfully navigate to new Vendor Payment Voucher page", "pass");
		} else {
			writeTestResults("Verify user can navigate to new Vendor Payment Voucher page",
					"User should be able to navigate to new Vendor Payment Voucher page",
					"User could'nt navigate to new Vendor Payment Voucher page", "fail");
		}

	}

	public void fillOutboundPaymentVendorReciptVoucher() throws Exception {
		click(btn_postBusinessUnit);
		selectText(drop_postBusinessUnit, postBusinessUnitOutboundPaymentVendorPaymentVoucher);
		click(btn_summaryTab);
		Thread.sleep(2000);
		String post_business_unit = getText(btn_postBusinessUnit);
		if (post_business_unit.equals(postBusinessUnitOutboundPaymentVendorPaymentVoucher)) {
			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User successfully select post business unit from the drop down", "pass");
		} else {
			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User could'nt select post business unit from the drop down", "fail");
		}

		click(btn_docDate);
		click(btn_calenderBack);
		click(btn_calenderBack);
		click("25_Link");

		click(btn_dueDate);
		click(btn_calenderForward);
		click(btn_calenderForward);
		click("25_Link");

		String doc_date = getText(btn_docDate).replaceAll("/", "");
		String post_date = getText(btn_postDate).replaceAll("/", "");
		String due_date = getText(btn_dueDate).replaceAll("/", "");

		String doc_date1 = reverseStringDateToSLOrder(doc_date);
		String post_date1 = reverseStringDateToSLOrder(post_date);
		String due_date1 = reverseStringDateToSLOrder(due_date);

		int doc_date2 = Integer.parseInt(doc_date1);
		int post_date2 = Integer.parseInt(post_date1);
		int due_date2 = Integer.parseInt(due_date1);

		if (doc_date2 < post_date2 && post_date2 < due_date2) {
			writeTestResults("Verify user can select document date, post date and due date",
					"User should be able to select document date, post date and due date",
					"User successfully select document date, post date and due date", "pass");
		} else {

			writeTestResults("Verify user can select document date, post date and due date",
					"User should be able to select document date, post date and due date",
					"User could'nt select document date, post date and due date", "fail");
		}

		click(btn_vendorSearchLookupVPV);
		if (isDisplayed(txt_vendorSearch)) {
			writeTestResults("Verify user can click on Search button in Vendor and Vendor Lookup should pop up",
					"User should be able to click on Search button in Vendor and Vendor Lookup should pop up",
					"User successfully click on Search button in Vendor and Vendor Lookup should pop up", "pass");
		} else {

			writeTestResults("Verify user can click on Search button in Vendor and Vendor Lookup should pop up",
					"User should be able to click on Search button in Vendor and Vendor Lookup should pop up",
					"User could'nt click on Search button in Vendor and Vendor Lookup should pop up", "fail");
		}

		sleepCustomize3(txt_vendorSearch);
		sendKeys(txt_vendorSearch, vendorVRP);
		pressEnter(txt_vendorSearch);
		sleepCustomize3(lnk_resultVendorSearch);
		if (isDisplayed(lnk_resultVendorSearch)) {
			writeTestResults("Verify user can search an existing Vendor by entering Vendor name/code",
					"User should be able to search an existing Vendor by entering Vendor name/code",
					"User successfully search an existing Vendor by entering Vendor name/code", "pass");
		} else {

			writeTestResults("Verify user can search an existing Vendor by entering Vendor name/code",
					"User should be able to search an existing Vendor by entering Vendor name/code",
					"User could'nt search an existing Vendor by entering Vendor name/code", "fail");
		}
		doubleClick(lnk_resultVendorSearch);

		boolean billing_address = isDisplayed(txt_billingAddress);
		String contact_person = getText(drop_contactPerson);

		boolean contact_person_flag = !contact_person.equals("");

		if (billing_address && contact_person_flag) {
			writeTestResults(
					"Verify selected Vendor name and Vendor code will populate the Vendor, Contact Person and associated Billing Address will be loaded as per the Vendor",
					"Selected Vendor name and Vendor code will populate the Vendor, Contact Person and associated Billing Address will be loaded as per the Vendor",
					"Successfully selected Vendor name and Vendor code will populate the Vendor, Contact Person and associated Billing Address will be loaded as per the Vendor",
					"pass");
		} else {

			writeTestResults(
					"Verify selected Vendor name and Vendor code will populate the Vendor, Contact Person and associated Billing Address will be loaded as per the Vendor",
					"Selected Vendor name and Vendor code will populate the Vendor, Contact Person and associated Billing Address will be loaded as per the Vendor",
					"Doesn't selected Vendor name and Vendor code will populate the Vendor, Contact Person and associated Billing Address will be loaded as per the Vendor",
					"fail");
		}

		selectText(drop_currency, curencyVRP);
		if (isDisplayed(drop_currency)) {
			writeTestResults("Verify user can choose the paid currency mode from the drop down",
					"User should be able to choose the paid currency mode from the drop down",
					"User successfully choose the paid currency mode from the drop down", "pass");
		} else {

			writeTestResults("Verify user can choose the paid currency mode from the drop down",
					"User should be able to choose the paid currency mode from the drop down",
					"User could't choose the paid currency mode from the drop down", "fail");
		}

		selectText(drop_payBook, payBookVendorPaymentVoucher);
		if (isDisplayed(drop_payBook)) {
			writeTestResults("Verify user can choose payment type (Cash)",
					"User should be able to choose payment type (Cash)", "User successfully choose payment type (Cash)",
					"pass");
		} else {

			writeTestResults("Verify user can choose payment type (Cash)",
					"User should be able to choose payment type (Cash)", "User could'nt choose payment type (Cash)",
					"fail");
		}

	}

	public void slectPaymentAdviceAndFillDetailsTab() throws Exception {
		Thread.sleep(1000);
		click(tab_payment);
		if (isDisplayed(drop_filterCurrency)) {
			writeTestResults("Verify user can navigate to details page",
					"User should be able to navigate to details page", "User successfully navigate to details page",
					"pass");
		} else {

			writeTestResults("Verify user can navigate to details page",
					"User should be able to navigate to details page", "User could'nt navigate to details page",
					"fail");
		}

		selectText(drop_filterCurrency, filterCurrency);
		if (isDisplayed(drop_filterCurrency)) {
			writeTestResults("Verify user can choose the filter currency mode from the drop down",
					"User should be able to choose the filter currency mode from the drop down",
					"User successfully choose the filter currency mode from the drop down", "pass");
		} else {

			writeTestResults("Verify user can choose the filter currency mode from the drop down",
					"User should be able to choose the filter currency mode from the drop down",
					"User could'nt choose the filter currency mode from the drop down", "fail");
		}

		Actions action = new Actions(driver);
		WebElement we = driver.findElement(By.xpath(btn_viewPendingAdvices));
		action.moveToElement(we).build().perform();
		click(btn_viewPendingAdvices);
		Thread.sleep(5000);
		String payment = readFinData(7);
		String chk_advice = chk_outboundPaymentAdvice.replace("doc_numbe", payment);
		if (isDisplayed(chk_advice)) {
			writeTestResults("Verify user can view the list of advices of selected vendor and from filter currency",
					"User should be able to view the list of advices of selected vendor and from filter currency",
					"User successfully view the list of advices of selected vendor and from filter currency", "pass");

		} else {

			writeTestResults("Verify user can view the list of advices of selected vendor and from filter currency",
					"User should be able to view the list of advices of selected vendor and from filter currency",
					"User could'nt view the list of advices of selected vendor and from filter currency", "fail");
		}

		/*
		 * (Future Uogrades) click(btn_setOffNos);
		 * 
		 * if
		 * (driver.findElement(getLocator(header_outboundPaymentSetOff)).isDisplayed())
		 * {
		 * writeTestResults("Verify user can navigate to Outbound payment set-off page",
		 * "User should be able to navigate to Outbound payment set-off page",
		 * "User successfully navigate to Outbound payment set-off page", "pass"); }
		 * else {
		 * 
		 * System.out.println("There are no any set offs available");
		 * 
		 * }
		 */

		click(chk_advice);

		if (isSelected(chk_advice)) {
			writeTestResults("Verify user can select the advices from the list",
					"User should be able to select the advices from the list",
					"User successfully select the advices from the list", "pass");
		} else {

			writeTestResults("Verify user can select the advices from the list",
					"User should be able to select the advices from the list",
					"User couldn't select the advices from the list", "fail");
		}
		String lbl_due_amount_xpath = lbl_dueAmountOutboundPaymentVendor.replace("doc_numbe", payment);
		String txt_paid_amount_xpath = txt_paidAMountOutboundPaymentVendor.replace("doc_numbe", payment);
		String due = getText(lbl_due_amount_xpath);
		Thread.sleep(2000);
		sendKeys(txt_paid_amount_xpath, due);

		if (isDisplayed(lbl_due_amount_xpath) && isDisplayed(txt_paid_amount_xpath)) {
			writeTestResults(
					"Verify user can enter the paid amount as per the actual receival in other than customer advance advices",
					"User should be able to enter the paid amount as per the actual receival in other than customer advance advices",
					"User successfully enter the paid amount as per the actual receival in other than customer advance advices",
					"pass");
		} else {

			writeTestResults(
					"Verify user can enter the paid amount as per the actual receival in other than customer advance advices",
					"User should be able to enter the paid amount as per the actual receival in other than customer advance advices",
					"User couldn't enter the paid amount as per the actual receival in other than customer advance advices",
					"fail");
		}

	}

	public void checkoutDraftAndReleaseOutboundPayment_VendorPayment() throws Exception {
		Thread.sleep(5000);
		String ckeckoutBefore = getText(btn_documentValueTotal);

		mouseMove(btn_checkout);
		click(btn_checkout);
		Thread.sleep(6000);
		String ckeckoutAfter = getText(btn_documentValueTotal);
		if (!ckeckoutBefore.equals(ckeckoutAfter)) {
			writeTestResults("Verify calculation, where net amount and Total field will populate the final amount",
					"System allows do calculation, where net amount and Total field will populate the final amount",
					"Calculation, where net amount and Total field will successfully populate the final amount",
					"pass");
		} else {

			writeTestResults("Verify calculation, where net amount and Total field will populate the final amount",
					"System allows do calculation, where net amount and Total field will populate the final amount",
					"Calculation, where net amount and Total field does'nt populate the final amount", "fail");
		}

		click(btn_draft);
		sleepCusomized(btn_reelese);
		if (isDisplayed(btn_reelese)) {
			writeTestResults("Verify user can draft Outbound Payment  - Vendor Payment Voucher",
					"User should be able to draft Outbound Payment  - Vendor Payment Voucher",
					"User successfully draft Outbound Payment  - Vendor Payment Voucher", "pass");
		} else {
			writeTestResults("Verify user can draft Outbound Payment  - Vendor Payment Voucher",
					"User should be able to draft Outbound Payment  - Vendor Payment Voucher",
					"User could'nt draft Outbound Payment  - Vendor Payment Voucher", "fail");
		}
		click(btn_reelese);
		sleepCustomize3(lbl_docStatus);
		Thread.sleep(3000);
		String doc_no = getText(lbl_docNo);
		writeFinData("Fin_TC_014_Outboun Payment Advice(Vendor Credit Memo)", doc_no, 7);
		String status = getText(lbl_docStatus);
		sleepCusomized(lbl_docNo);
		trackCode = getText(lbl_docNo);
		if (!status.equalsIgnoreCase("( Released )")) {
			Thread.sleep(7000);
			status = getText(lbl_docStatus);
		}
		if (status.equals("( Released )")) {
			writeTestResults("Verify user can release Outbound Payment  - Vendor Payment Voucher",
					"User should be able to release Outbound Payment  - Vendor Payment Voucher",
					"User successfully release Outbound Payment  - Vendor Payment Voucher", "pass");
		} else {
			writeTestResults("Verify user can release Outbound Payment  - Vendor Payment Voucher",
					"User should be able to release Outbound Payment  - Vendor Payment Voucher",
					"User could'nt release Outbound Payment  - Vendor Payment Voucher", "fail");
		}
	}

	/* Fin_TC_016 */
	public void createPurchaseOrderAndNavigateToLetterOfGurentee() throws Exception {
		InventoryAndWarehouseModule obj = new InventoryAndWarehouseModule();
		finTC016_PONo = obj.purchaseOrder();
		Thread.sleep(5000);
		sleepCustomize3(navigation_pane);
		click(navigation_pane);
		customizeLoadingDelay(btn_financeModule, 20);
		click(btn_financeModule);
		customizeLoadingDelay(btn_letterOfGurentee, 20);
		click(btn_letterOfGurentee);
		Thread.sleep(3000);
		customizeLoadingDelay(header_page, 20);
		String header = getText(header_page);
		if (header.equals("Letter Of Guarantee")) {
			writeTestResults("Verify user can navigate to Letter of Guarantee by-page",
					"User should be able to navigate to Letter of Guarantee by-page",
					"User successfully navigate to Letter of Guarantee by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Letter of Guarantee by-page",
					"User should be able to navigate to Letter of Guarantee by-page",
					"User could'nt navigate to Letter of Guarantee by-page", "fail");
		}

		click(btn_new);
		Thread.sleep(3000);
		String status = getText(lbl_docStatus1);
		if (status.equals("New")) {
			writeTestResults("Verify user can navigate to new letter of guarantee page",
					"User should be able to navigate to new letter of guarantee page",
					"User successfully navigate to new letter of guarantee page", "pass");
		} else {
			writeTestResults("Verify user can navigate to new letter of guarantee page",
					"User should be able to navigate to new letter of guarantee page",
					"User could'nt navigate to new letter of guarantee page", "fail");
		}
	}

	public void fillLetterOfGurentee() throws Exception {
		selectText(drop_payBook, payBookLetterOFGurentee);
		if (isDisplayed(drop_payBook)) {
			writeTestResults("Verify user can select the paybook from drop down",
					"User should be able to select the paybook from drop down",
					"User successfully select the paybook from drop down", "pass");
		} else {
			writeTestResults("Verify user can select the paybook from drop down",
					"User should be able to select the paybook from drop down",
					"User could'nt select the paybook from drop down", "fail");
		}

		selectText(drop_currency, currencyLOG);
		sendKeys(txt_originalDocNo, genIDLetterOfGurentee());
		if (isDisplayed(txt_originalDocNo)) {
			writeTestResults("Verify user can enter all charaters", "User should be able to enter all charaters",
					"User successfully enter all charaters", "pass");
		} else {
			writeTestResults("Verify user can enter all charaters", "User should be able to enter all charaters",
					"User could'nt enter all charaters", "fail");
		}

		selectIndex(drop_requesterType, 1);
		if (isDisplayed(drop_requesterType)) {
			writeTestResults("Verify user can choose requester type", "User should be able to choose requester type",
					"User successfully choose requester type", "pass");
		} else {
			writeTestResults("Verify user can choose requester type", "User should be able to choose requester type",
					"User could'nt choose requester type", "fail");
		}

		click(btn_vendorLookupLOG);
		sleepCustomize3(txt_vendor);
		sendKeys(txt_vendor, vebdorLetterOfGurentee);
		pressEnter(txt_vendor);
		Thread.sleep(3000);
		if (isDisplayed(lnk_resultVendor)) {
			writeTestResults("Verify user can select vendor from requester lookup",
					"User should be able to select vendor from requester lookup",
					"User successfully select vendor from requester lookup", "pass");
		} else {
			writeTestResults("Verify user can select vendor from requester lookup",
					"User should be able to select vendor from requester lookup",
					"User could'nt select vendor from requester lookup", "fail");
		}
		doubleClick(lnk_resultVendor);

		try {
			sleepCustomize3(btn_documentSearchLookup16);
			click(btn_documentSearchLookup16);
			click(btn_documentSearchNosLOG);
			sleepCustomize3(txt_serchPurchaseOrder);
			sendKeys(txt_serchPurchaseOrder, finTC016_PONo);
			pressEnter(txt_serchPurchaseOrder);
		} catch (Exception e) {
			writeTestResults("Verify user can select reference document (Purchase Order)",
					"User should be able to select reference document (Purchase Order)",
					"User could'nt select reference document (Purchase Order)", "fail");
		}
		Thread.sleep(3000);
		if (isDisplayed(lnk_serchResultPurchaseOrder)) {
			writeTestResults("Verify user can select reference document (Purchase Order)",
					"User should be able to select reference document (Purchase Order)",
					"User successfully select reference document (Purchase Order)", "pass");
		} else {
			writeTestResults("Verify user can select reference document (Purchase Order)",
					"User should be able to select reference document (Purchase Order)",
					"User could'nt select reference document (Purchase Order)", "fail");
		}
		doubleClick(lnk_serchResultPurchaseOrder);
		Thread.sleep(2000);
		click(btn_applyPurchaseOrder);

		selectIndex(drop_gurenteeGroup, 1);
		if (isDisplayed(drop_gurenteeGroup)) {
			writeTestResults("Verify user can select guaratee group from drop down",
					"User should be able to select guaratee group from drop down",
					"User successfully select guaratee group from drop down", "pass");
		} else {
			writeTestResults("Verify user can select guaratee group from drop down",
					"User should be able to select guaratee group from drop down",
					"User could'nt select guaratee group from drop down", "fail");
		}

		click(calende_openDate);
		click(currentDay());
		if (isDisplayed(calende_openDate)) {
			writeTestResults("Verify user can choose opening date from the calender",
					"User should be able to choose opening date from the calender",
					"User successfully choose opening date from the calender", "pass");
		} else {
			writeTestResults("Verify user can choose opening date from the calender",
					"User should be able to choose opening date from the calender",
					"User could'nt choose opening date from the calender", "fail");
		}

		click(calende_endDate);
		doubleClick(btn_calenderForward);
		doubleClick(btn_calenderForward);
		doubleClick(btn_calenderForward);
		click("28_Link");
		if (isDisplayed(calende_openDate)) {
			writeTestResults("Verify user can choose expiration date from calendor",
					"User should be able to choose expiration date from calendor",
					"User successfully choose expiration date from calendor", "pass");
		} else {
			writeTestResults("Verify user can choose expiration date from calendor",
					"User should be able to choose expiration date from calendor",
					"User could'nt choose expiration date from calendor", "fail");
		}

		sendKeys(txt_amount, amountLOG);
		if (isDisplayed(txt_amount)) {
			writeTestResults("Verify user can enter the positive amount",
					"User should be able to enter the positive amount", "User successfully enter the positive amount",
					"pass");
		} else {
			writeTestResults("Verify user can enter the positive amount",
					"User should be able to enter the positive amount", "User could'nt enter the positive amount",
					"fail");
		}

	}

	public void drfatReleseLetterOfGurentee() throws Exception {
		Thread.sleep(4000);
		click(btn_draft);
		Thread.sleep(3000);
		if (isDisplayed(btn_reelese)) {
			writeTestResults("Verify user can draft Letter Of Gurentee",
					"User should be able to draft Letter Of Gurentee", "User successfully draft Letter Of Gurentee",
					"pass");
		} else {
			writeTestResults("Verify user can draft Letter Of Gurentee",
					"User should be able to draft Letter Of Gurentee", "User could'nt draft Letter Of Gurentee",
					"fail");
		}
		click(btn_reelese);
		customizeLoadingDelay(lbl_docNo, 20);
		Thread.sleep(6000);
		String doc_no = getText(lbl_docNo);
		writeFinData("Fin_TC_017_LetterOfGurentee", doc_no, 9);
		String status = getText(lbl_docStatus);
		sleepCusomized(lbl_docNo);
		trackCode = getText(lbl_docNo);
		if (!status.equalsIgnoreCase("(Released)")) {
			Thread.sleep(7000);
			status = getText(lbl_docStatus);
		}
		if (status.equals("(Released)")) {
			writeTestResults("Verify user can release Letter Of Gurentee",
					"User should be able to release Letter Of Gurentee", "User successfully release Letter Of Gurentee",
					"pass");
		} else {
			writeTestResults("Verify user can release Letter Of Gurentee",
					"User should be able to release Letter Of Gurentee", "User could'nt release Letter Of Gurentee",
					"fail");
		}
	}

	public void checkCreditExtentionAvailability() throws Exception {
		click(btn_action);
		if (isDisplayed(btn_creditExtentionLetterOFGurentee)) {
			writeTestResults("Verify user can increase the credit amount of created leeter of guarantee",
					"User should be able to increase the credit amount of created leeter of guarantee",
					"User successfully can increase the credit amount of created leeter of guarantee", "pass");
		} else {
			writeTestResults("Verify user can increase the credit amount of created leeter of guarantee",
					"User should be able to increase the credit amount of created leeter of guarantee",
					"User could'nt able to increase the credit amount of created leeter of guarantee", "fail");
		}

		InventoryAndWarehouseModule obj = new InventoryAndWarehouseModule();
		obj.removeShipmentAcceptance();
		switchWindow();
		closeWindow();
		switchWindow();
		closeWindow();
	}

	/* Common - Login */
	public void commonLogin() throws Exception {
		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
	}

	/* Fin_TC_017 */
	public void findReleventLetterOFGurentee() throws Exception {
		navigateToTheFananceModule();
		click(btn_letterOfGurentee);
		sendKeys(txt_letterPfGurenteeSearch, readFinData(9));
		pressEnter(txt_letterPfGurenteeSearch);
		String lnk_log_xpath = lnk_letterOfGurenteeResult.replace("LOG_numbe", readFinData(9));
		customizeLoadingDelay(lnk_log_xpath, 25);
		Thread.sleep(2000);
		click(lnk_log_xpath);
	}

	public void convertToBankFacilityAgreementAndFillAgreement() throws Exception {
		customizeLoadingDelay(btn_action, 25);
		click(btn_action);
		click(btn_convertToBankFacilityAgreement);
		switchWindow();
		Thread.sleep(3000);

		String header = getText(header_page2);
		if (!header.equals("Bank Facility Agreement")) {
			Thread.sleep(4000);
			header = getText(header_page2);
		}
		boolean flag_header = false;
		if (header.equals("Bank Facility Agreement")) {
			flag_header = true;
		}

		String ref_log = getAttribute(txt_referenceDocumenrLOG, "disabled");
		boolean flagRefDoc = false;
		if (ref_log.equals("true")) {
			flagRefDoc = true;
		}

		Select comboBox = new Select(driver.findElement(By.xpath(drop_bankBFA)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		boolean flag_bank = false;
		if (!selectedComboValue.equals("--None--")) {
			flag_bank = true;
		}

		click(txt_amountBFA);
		Thread.sleep(1000);
		click(txt_amountBFA);
		Thread.sleep(4000);
		String amount = "0.00000";
		amount = getAttribute(txt_amountBFA, "pv");
		boolean amount_flag = false;
		if (amount != "0.00000" && amount != null) {
			amount_flag = true;
		} else {
			amount_flag = false;
		}

		if (flag_header && flagRefDoc && flag_bank && amount_flag) {
			writeTestResults(
					"Verify user navigate to Bank facility agreement, where bank details and amount are auto selected as per the letter of guaratee and Referece document field updated with LOG document number",
					"User should navigate to Bank facility agreement, where bank details and amount are auto selected as per the letter of guaratee and Referece document field updated with LOG document number",
					"User successfully navigate to Bank facility agreement, where bank details and amount are auto selected as per the letter of guaratee and Referece document field updated with LOG document number",
					"pass");
		} else {
			writeTestResults(
					"Verify user navigate to Bank facility agreement, where bank details and amount are auto selected as per the letter of guaratee and Referece document field updated with LOG document number",
					"User should navigate to Bank facility agreement, where bank details and amount are auto selected as per the letter of guaratee and Referece document field updated with LOG document number",
					"User couldn't navigate to Bank facility agreement, where bank details and amount are not selected as per the letter of guaratee and Referece document field not updated with LOG document number",
					"fail");
		}

		sendKeys(txt_originalDocNoLetterOfGurentee, originalDocNoLetterOfGurentee);
		if (isDisplayed(txt_originalDocNoLetterOfGurentee)) {
			writeTestResults("Verify user can enter the all charectors",
					"User should be able to enter the all charectors", "User successfully enter the all charectors",
					"pass");
		} else {
			writeTestResults("Verify user can enter the all charectors",
					"User should be able to enter the all charectors", "User could'nt enter the all charectors",
					"fail");
		}

		selectIndex(drop_bankFacilityGroup, 1);
		if (isDisplayed(drop_bankFacilityGroup)) {
			writeTestResults("Verify user can select bank facility group from the drop down",
					"User should be able to select bank facility group from the drop down",
					"User successfully select bank facility group from the drop down", "pass");
		} else {
			writeTestResults("Verify user can select bank facility group from the drop down",
					"User should be able to select bank facility group from the drop down",
					"User could'nt select bank facility group from the drop down", "fail");
		}

		click(txt_startDate);
		click(btn_calenderForward);
		click(btn_date01);

		click(txt_endDate);
		for (int i = 0; i < 13; i++) {
			click(btn_calenderForward);
		}
		click(btn_date01);
		if (isDisplayed(txt_endDate) && isDisplayed(txt_startDate)) {
			writeTestResults("Verify user can select the date from the calendor appeared in the respective fields",
					"User should be able to select the date from the calendor appeared in the respective fields",
					"User successfully select the date from the calendor appeared in the respective fields", "pass");
		} else {
			writeTestResults("Verify user can select the date from the calendor appeared in the respective fields",
					"User should be able to select the date from the calendor appeared in the respective fields",
					"User could'nt select the date from the calendor appeared in the respective fields", "fail");
		}
	}

	public void draftReleaseBankFacilityAgreement() throws Exception {
		click(btn_draft);
		sleepCustomize3(lbl_docStatus);
		Thread.sleep(3000);
		String status = getText(lbl_docStatus);
		if (!status.equalsIgnoreCase("( Draft )")) {
			Thread.sleep(7000);
			status = getText(lbl_docStatus);
		}
		if (status.equals("( Draft )")) {
			writeTestResults("Verify user can draft Bank Facility Agreement",
					"User should be able to draft Bank Facility Agreement",
					"User successfully draft Bank Facility Agreement", "pass");
		} else {
			writeTestResults("Verify user can draft Bank Facility Agreement",
					"User should be able to draft Bank Facility Agreement",
					"User could'nt draft Bank Facility Agreement", "fail");
		}

		click(btn_reelese);
		sleepCustomize3(lbl_docStatus);
		Thread.sleep(3000);
		sleepCusomized(lbl_docNo);
		trackCode = getText(lbl_docNo);
		String status1 = getText(lbl_docStatus);
		if (!status.equalsIgnoreCase("( Released )")) {
			Thread.sleep(7000);
			status = getText(lbl_docStatus);
		}
		if (status1.equals("( Released )")) {
			writeTestResults("Verify user can release Bank Facility Agreement",
					"User should be able to release Bank Facility Agreement",
					"User successfully release Bank Facility Agreement", "pass");
		} else {
			writeTestResults("Verify user can release Bank Facility Agreement",
					"User should be able to release Bank Facility Agreement",
					"User could'nt release Bank Facility Agreement", "fail");
		}
		String doc_no = getText(lbl_docNo);
		writeFinData("Fin_TC_017(Bank Facility Agreement)", doc_no, 10);
		click(btn_action);
		click(btn_journel);
		String debit = getText(lbl_journelEntryFirstRowDebit);
		String credit = getText(lbl_journelEntrySecondRowCredit);

		if (debit.equals(credit)) {
			writeTestResults("Verify user can view the journal entry to verify accuracy",
					"User should be able to view the journal entry to verify accuracy",
					"User successfully view the journal entry to verify accuracy", "pass");
		} else {
			writeTestResults("Verify user can view the journal entry to verify accuracy",
					"User should be able to view the journal entry to verify accuracy",
					"User could'nt view the journal entry to verify accuracy", "fail");
		}

		switchWindow();
		closeWindow();
	}

	/* Common - Current Day */
	public String currentDay() {
		String date = currentTimeAndDate();

		char first_char = date.charAt(8);
		String first_letter = Character.toString(first_char);
		char second_char = date.charAt(9);
		String second_letter = Character.toString(second_char);

		String final_date;
		if (first_letter.equals("0")) {
			final_date = second_letter;
		} else {
			final_date = first_letter + second_letter;
		}
		return final_date;
	}

	/* Fin_TC_018 */
	public void findReleventLOGAndReleasePayment() throws Exception {
		findReleventLetterOFGurentee();
		customizeLoadingDelay(lbl_OutboundPaymentNoLOG, 30);
		String before_op_no = getText(lbl_OutboundPaymentNoLOG);

		click(btn_releeseLOGOutboundPayment);
		Thread.sleep(2000);
		String after_op_no = getText(lbl_OutboundPaymentNoLOG);
		outbound_payment_no_Fin_TC_018 = after_op_no;
		Thread.sleep(3000);
		sleepCusomized(lbl_docNo);
		trackCode = getText(lbl_docNo);
		if ((!before_op_no.equals(after_op_no)) | (before_op_no.equals(after_op_no))) {
			writeTestResults(
					"Verify user release the payment and outbound payment reference number appeared in the place where release button situated",
					"User should be able to release the payment and outbound payment reference number should appeared in the place where release button situated",
					"User successfully release the payment and outbound payment reference number appeared in the place where release button situated",
					"pass");
		} else {
			writeTestResults(
					"Verify user release the payment and outbound payment reference number appeared in the place where release button situated",
					"User should be able to release the payment and outbound payment reference number should appeared in the place where release button situated",
					"User couldn't release the payment and outbound payment reference number not appeared in the place where release button situated",
					"fail");
		}
	}

	public void checkOutboundPaymentAvailabilityLOG() throws Exception {
		customizeLoadingDelay(navigation_pane, 20);
		Thread.sleep(2000);
		customizeLoadingDelay(navigation_pane, 20);
		click(navigation_pane);
		customizeLoadingDelay(btn_financeModule, 20);
		click(btn_financeModule);
		customizeLoadingDelay(btn_outboundPayment, 20);
		click(btn_outboundPayment);
		customizeLoadingDelay(txt_searchOutboundPayment, 20);
		sendKeys(txt_searchOutboundPayment, outbound_payment_no_Fin_TC_018);
		pressEnter(txt_searchOutboundPayment);
		String xpath_outbound = lnk_resultOutboundPayment.replace("outbound", outbound_payment_no_Fin_TC_018);
		sleepCustomize3(xpath_outbound);
		click(xpath_outbound);
		Thread.sleep(3000);
		sleepCusomized(lbl_docNo);
		trackCode = getText(lbl_docNo);
		String header = getText(lbl_docStatus);
		if (!header.equalsIgnoreCase("( Released )")) {
			Thread.sleep(7000);
			header = getText(lbl_docStatus);
		}
		if (header.equals("( Released )")) {
			writeTestResults("Verify user can check the Outbound Payment through  above reference number",
					"User should be able to check the Outbound Payment through  above reference number",
					"User successfully check the Outbound Payment through  above reference number", "pass");
		} else {
			writeTestResults("Verify user can check the Outbound Payment through  above reference number",
					"User should be able to check the Outbound Payment through  above reference number",
					"User could'nt check the Outbound Payment through  above reference number", "fail");
		}

	}

	/* Fin_TC_019 */
	public void navigateToNewBankAdjustment() throws Exception {
		navigateToTheFananceModule();
		click(btn_bankAdjustment);
		String header = getText(header_page);
		if (header.equals("Bank Adjustment")) {
			writeTestResults("Verify user can navigate to Bank Adjustment by-page",
					"User should be able to navigate to Bank Adjustment by-page",
					"User successfully navigate to Bank Adjustment by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Bank Adjustment by-page",
					"User should be able to navigate to Bank Adjustment by-page",
					"User could'nt navigate to Bank Adjustment by-page", "fail");
		}

		click(btn_addNew);
		String status = getText(lbl_docStatus1);
		if (status.equals("New")) {
			writeTestResults("Verify user can navigate to New Bank Adjustment page",
					"User should be able to navigate to New Bank Adjustment page",
					"User successfully navigate to New Bank Adjustment page", "pass");
		} else {
			writeTestResults("Verify user can navigate to New Bank Adjustment page",
					"User should be able to navigate to New Bank Adjustment page",
					"User could'nt navigate to New Bank Adjustment page", "fail");
		}
	}

	public void fillBankAdjutment() throws Exception {
		click(btn_postBusinessUnit);
		selectText(drop_postBusinessUnit, postBusinessUnitBankAdjustment);
		click(chk_facilitySettlement);
		click(chk_facilitySettlement);
		Thread.sleep(2000);
		String post_business_unit = getText(btn_postBusinessUnit);
		if (post_business_unit.equals(postBusinessUnitOutboundPaymentVendorPaymentVoucher)) {
			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User successfully select post business unit from the drop down", "pass");
		} else {
			writeTestResults("Verify user can select post business unit from the drop down",
					"User should be able to select post business unit from the drop down",
					"User could'nt select post business unit from the drop down", "fail");
		}

		click(btn_docDate);
		click(btn_calenderBack);
		click(btn_calenderBack);
		click("25_Link");

		click(btn_dueDate);
		click(btn_calenderForward);
		click(btn_calenderForward);
		click("25_Link");

		String doc_date = getText(btn_docDate).replaceAll("/", "");
		String post_date = getText(btn_postDate).replaceAll("/", "");
		String due_date = getText(btn_dueDate).replaceAll("/", "");

		String doc_date1 = reverseStringDateToSLOrder(doc_date);
		String post_date1 = reverseStringDateToSLOrder(post_date);
		String due_date1 = reverseStringDateToSLOrder(due_date);

		int doc_date2 = Integer.parseInt(doc_date1);
		int post_date2 = Integer.parseInt(post_date1);
		int due_date2 = Integer.parseInt(due_date1);

		if (doc_date2 < post_date2 && post_date2 < due_date2) {
			writeTestResults("Verify user can select document date, post date and due date",
					"User should be able to select document date, post date and due date",
					"User successfully select document date, post date and due date", "pass");
		} else {

			writeTestResults("Verify user can select document date, post date and due date",
					"User should be able to select document date, post date and due date",
					"User could'nt select document date, post date and due date", "fail");
		}

		selectText(drop_bank, bankBankAdjustment);
		if (isDisplayed(drop_bank)) {
			writeTestResults("Verify user can select bank from the dropdown",
					"User should be able to select bank from the dropdown",
					"User successfully select bank from the dropdown", "pass");
		} else {

			writeTestResults("Verify user can select bank from the dropdown",
					"User should be able to select bank from the dropdown",
					"User could'nt select bank from the dropdown", "fail");
		}

		selectText(drop_bankAccountNo, bankAccountBankAdjustment);
		if (isDisplayed(drop_bankAccountNo)) {
			writeTestResults("Verify user can view select account no in the field",
					"User should be able to view select account no in the field",
					"User successfully view select account no in the field", "pass");
		} else {

			writeTestResults("Verify user can view select account no in the field",
					"User should be able to view select account no in the field",
					"User could'nt view select account no in the field", "fail");
		}
		sendKeys(txt_description1, description);
		if (isDisplayed(txt_description1)) {
			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User successfully enter characters, numbers, alphanumeric and special characters to Description",
					"pass");
		} else {

			writeTestResults(
					"Verify user can enter characters, numbers, alphanumeric and special characters to Description",
					"User should be able to enter characters, numbers, alphanumeric and special characters to Description",
					"User could'nt enter characters, numbers, alphanumeric and special characters to Description",
					"fail");
		}

		click(btn_glLookupBAnkAdjustment);
		sleepCustomize3(txt_GL);
		sendKeys(txt_GL, glAccountBankAdjustment);
		pressEnter(txt_GL);
		sleepCustomize3(lnk_resultGL);
		if (isDisplayed(lnk_resultGL)) {
			writeTestResults("Verify user can select GL account no and it should be appeared in the gl account field",
					"User should be able to select GL account no and it should be appeared in the gl account field",
					"User successfully select GL account no and it successfully appeared in the gl account field",
					"pass");
		} else {
			writeTestResults("Verify user can select GL account no and it should be appeared in the gl account field",
					"User should be able to select GL account no and it should be appeared in the gl account field",
					"User couldn't select GL account no and it doesn't appeared in the gl account field", "fail");
		}
		doubleClick(lnk_resultGL);

		click(chk_facilitySettlement);
		if (isDisplayed(btn_bankFacilityLookup)) {
			writeTestResults("Verify Bank Facility Agreement field is appeared as mandatory field",
					"Bank Facility Agreement field should appeared as mandatory field",
					"Bank Facility Agreement field successfully appeared as mandatory field", "pass");
		} else {
			writeTestResults("Verify Bank Facility Agreement field is appeared as mandatory field",
					"Bank Facility Agreement field should appeared as mandatory field",
					"Bank Facility Agreement field doesn't appeared as mandatory field", "fail");
		}

		click(btn_bankFacilityLookup);
		sleepCustomize3(txt_bankFacilityAgreementSearch);
		sendKeys(txt_bankFacilityAgreementSearch, readFinData(10));
		pressEnter(txt_bankFacilityAgreementSearch);
		sleepCustomize3(lnl_bankFacilityAgreementSearchResult);
		if (isDisplayed(lnl_bankFacilityAgreementSearchResult)) {
			writeTestResults("Verify user can view selected Bank Facility Agreement",
					"User should be able to view selected Bank Facility Agreement",
					"User successfully view selected Bank Facility Agreement", "pass");
		} else {
			writeTestResults("Verify user can view selected Bank Facility Agreement",
					"User should be able to view selected Bank Facility Agreement",
					"User could'nt view selected Bank Facility Agreement", "fail");
		}
		doubleClick(lnl_bankFacilityAgreementSearchResult);

		selectIndex(drop_settlementTypeBankAdjustment, 1);
		if (isDisplayed(drop_settlementTypeBankAdjustment)) {
			writeTestResults("Verify user can choose settlement ", "User should be able to choose settlement ",
					"User successfully choose settlement ", "pass");
		} else {
			writeTestResults("Verify user can choose settlement ", "User should be able to choose settlement ",
					"User could'nt choose settlement ", "fail");
		}

		sendKeys(txt_amount, amountBankAdjustment);
		click(btn_costAllocationBankAdjustment);
		if (isDisplayed(lbl_minusValueCheck)) {
			writeTestResults(
					"Verify system coudn't allow to enter positive value to amount fieald when settlement type select as Capital",
					"System should be coudn't allow to enter positive value to amount fieald when settlement type select as Capital",
					"System coudn't allow to enter positive value to amount fieald when settlement type select as Capital",
					"pass");
		} else {
			writeTestResults(
					"Verify system coudn't allow to enter positive value to amount fieald when settlement type select as Capital",
					"System should be coudn't allow to enter positive value to amount fieald when settlement type select as Capital",
					"System allow to enter positive value to amount fieald when settlement type select as Capital",
					"fail");
		}

		selectIndex(drop_settlementTypeBankAdjustment, 2);
		sendKeys(txt_amount, amountBankAdjustment);
		if (isDisplayed(txt_amount)) {
			writeTestResults("Verify user can enter the amount according to the settlement type selected",
					"User should be able to enter the amount according to the settlement type selected ",
					"User successfully enter the amount according to the settlement type selected ", "pass");
		} else {
			writeTestResults("Verify user can enter the amount according to the settlement type selected ",
					"User should be able to enter the amount according to the settlement type selected ",
					"User could'nt enter the amount according to the settlement type selected ", "fail");
		}
		click(btn_costAllocationBankAdjustment);

		selectText(drop_costCenter, costCenterBankAdjustment);
		sendKeys(txt_apportionPresentage, costAllocatioPresentageNankAdjustment);
		click(btn_addRecordApportion);
		click(btn_update);
		if (isDisplayed(txt_amount)) {
			writeTestResults("Verify user can configure cost allocation",
					"User should be able to configure cost allocation ", "User successfully configure cost allocation ",
					"pass");
		} else {
			writeTestResults("Verify user can configure cost allocation ",
					"User should be able to configure cost allocation ", "User could'nt configure cost allocation ",
					"fail");
		}

		selectIndex(drop_tax, 1);
		if (isDisplayed(drop_tax)) {
			writeTestResults("Verify user can choose tax group from lookup",
					"User should be able to choose tax group from lookup ",
					"User successfully choose tax group from lookup ", "pass");
		} else {
			writeTestResults("Verify user can choose tax group from lookup ",
					"User should be able to choose tax group from lookup ",
					"User could'nt choose tax group from lookup ", "fail");
		}

	}

	public void checkoutDraftAndReleaseBankAdjustment() throws Exception {
		Thread.sleep(2000);
		click(btn_checkout);
		Thread.sleep(2000);
		click(btn_btnTaxBrackDownBankAdjustment);
		String td_tax = driver.findElement(By.xpath(td_taxNameCell)).getAttribute("value");

		if (td_tax.equals("Regulation10Tax")) {
			writeTestResults("Verify system allows do calculation", "System should allows do calculation",
					"System successfully allows do calculation", "pass");
		} else {

			writeTestResults("Verify system allows do calculation", "System should allows do calculation",
					"System doesn't allows do calculation", "fail");
		}
		Thread.sleep(2000);
		WebElement element = driver.findElement(getLocator(close_headerPopup));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);

		click(btn_draft);
		Thread.sleep(3000);
		if (isDisplayed(btn_reelese)) {
			writeTestResults("Verify user can draft Bank Adjustment", "User should be able to draft Bank Adjustment",
					"User successfully draft Bank Adjustment", "pass");
		} else {
			writeTestResults("Verify user can draft Bank Adjustment", "User should be able to draft Bank Adjustment",
					"User could'nt draft Bank Adjustment", "fail");
		}

		click(btn_reelese);
		explicitWait(header_releasedBankAdjustment, 40);
		trackCode = getText(lbl_docNo);
		String status = getText(lbl_docStatus);
		if (!status.equalsIgnoreCase("( Released )")) {
			Thread.sleep(7000);
			status = getText(lbl_docStatus);
		}
		if (status.equals("( Released )")) {
			writeTestResults("Verify user can release Bank Adjustment",
					"User should be able to release Bank Adjustment", "User successfully release Bank Adjustment",
					"pass");
		} else {
			writeTestResults("Verify user can release Bank Adjustment",
					"User should be able to release Bank Adjustment", "User could'nt release Bank Adjustment", "fail");
		}
	}

	/* Fin_TC_020 */
	public void navigateToBankREconcilationFormSmoke() throws Exception {
		navigateToTheFananceModule();
		click(btn_bankReconcillation);
		String header = getText(header_page);
		if (header.equals("Bank Reconciliation")) {
			writeTestResults("Verify user can navigate to bank reconciliation by-page",
					"User should be able to navigate to bank reconciliation by-page",
					"User successfully navigate to bank reconciliation by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to bank reconciliation by-page",
					"User should be able to navigate to bank reconciliation by-page",
					"User could'nt navigate to bank reconciliation by-page", "fail");
		}

		click(btn_newBankReconcilation);
		String status = getText(lbl_docStatus1);
		if (status.equals("New")) {
			writeTestResults("Verify user can navigate to new bank reconciliation page",
					"User should be able to navigate to new bank reconciliation page",
					"User successfully navigate to new bank reconciliation page", "pass");
		} else {
			writeTestResults("Verify user can navigate to new bank reconciliation page",
					"User should be able to navigate to new bank reconciliation page",
					"User could'nt navigate to new bank reconciliation page", "fail");
		}
	}

	public void fillBankReconcilationSmoke() throws Exception {
		click(btn_docDate);
		String xpath_date = currentDay() + "_Link";
		click(xpath_date);
		if (isDisplayed(btn_docDate)) {
			writeTestResults("Verify user can select document date", "User should be able to select document date",
					"User successfully select document date", "pass");
		} else {
			writeTestResults("Verify user can select document date", "User should be able to select document date",
					"User could'nt select document date", "fail");
		}

		click(btn_statementDateBankReconcilation);
		click(currentDay() + "_Link");
		if (isDisplayed(btn_statementDateBankReconcilation)) {
			writeTestResults("Verify user can select statement date", "User should be able to select statement date",
					"User successfully select statement date", "pass");
		} else {
			writeTestResults("Verify user can select statement date", "User should be able to select statement date",
					"User could'nt select statement date", "fail");
		}

		selectText(drop_bank, bankBankReconcillation);
		if (isDisplayed(drop_bank)) {
			writeTestResults("Verify user can select the bank", "User should be able to select the bank",
					"User successfully select the bank", "pass");
		} else {
			writeTestResults("Verify user can select the bank", "User should be able to select the bank",
					"User could'nt select the bank", "fail");
		}

		selectText(drop_accountNo, bankAccountBankReconcilation);
		if (isDisplayed(drop_accountNo)) {
			writeTestResults(
					"Verify user can select the bank account no and system can load the advices related to the selected bank and account no only",
					"User should be able to select the bank account no and system should load the advices related to the selected bank and account no only",
					"User successfully select the bank account no and system successfully load the advices related to the selected bank and account no only",
					"pass");
		} else {
			writeTestResults(
					"Verify user can select the bank account no and can load the advices related to the selected bank and account no only",
					"User should be able to select the bank account no and system should load the advices related to the selected bank and account no only",
					"User could'nt select the bank account no and system doesn't load the advices related to the selected bank and account no only",
					"fail");
		}

		/* Last Reconcile Amount */
		Thread.sleep(1000);
		String last_reconcilation_balance = driver.findElement(By.xpath(lbl_lastBalaceBankReconcilation))
				.getAttribute("value");
		String last_reconcilation_balance_coma_replaced = last_reconcilation_balance.replaceAll(",", "");
		double doub_last_reconcilation_balance_coma_replaced = Double
				.parseDouble(last_reconcilation_balance_coma_replaced);

		/* Adding Amount */
		double bank_statement_adding_value = Double.parseDouble(bankStatementREconcilationStatementBalance);

		/* Input Amount */
		double statement_amount = doub_last_reconcilation_balance_coma_replaced + bank_statement_adding_value;

		String statement_amount_input = String.valueOf(statement_amount);
		sendKeys(txt_statementBalance, statement_amount_input);
		if (isDisplayed(txt_statementBalance)) {
			writeTestResults("Verify user can enter the statement balance",
					"User should be able to enter the statement balance",
					"User successfully enter the statement balance", "pass");
		} else {
			writeTestResults("Verify user can enter the statement balance",
					"User should be able to enter the statement balance", "User could'nt enter the statement balance",
					"fail");
		}
	}

	public void completeAdjustmentBankReconcilationSmoke() throws Exception {
		click(btn_adjustentBankReconcilation);
		if (isDisplayed(btn_descriptionLookupBAnkReconcilationAdjustment)) {
			writeTestResults("Verify adjustment window is appeared ", "Adjustment window should appeared",
					"Adjustment window successfully appeared ", "pass");
		} else {
			writeTestResults("Verify adjustment window is appeared ", "Adjustment window should appeared",
					"Adjustment window doesn't appeared ", "fail");
		}

		click(btn_descriptionLookupBAnkReconcilationAdjustment);
		sendKeys(txt_descriptionAdjustmenrBankReconcilation, adjustmentDescriptionBankReconcilation);
		click(btn_applyDescriptionBankeconcilation);

		click(gl_nosAdjustmentBankReconcilation);
		sendKeys(txt_GL, glAccountAdjustmetnBankReconcillation);
		pressEnter(txt_GL);
		doubleClick(lnk_resultGL);

		String adjustment_amount = driver.findElement(By.xpath(lbl_unReconciledAmount)).getAttribute("value");
		sendKeys(txt_amountUluBankReconsillAdjustment, adjustment_amount);
		click(btn_costAllocationAdjustmentBankReconcilation);
		selectText(drop_costCenter, departmentCostAllocationBankReconcilationAdjustment);
		sendKeys(txt_apportionPresentage, presentageCostAllocationBankReconcilationAdjustment);
		click(btn_addRecordApportion);
		click(btn_updateCostCenter);
		click(btn_updateAdjustmentBankReconcilation);
		String header = getText(header_page2);
		if (header.equals("Bank Reconciliation")) {
			writeTestResults("Verify user can temprarily saves the adjustment",
					"User should be able to temprarily saves the adjustment",
					"User successfully temprarily saves the adjustment", "pass");
		} else {
			writeTestResults("Verify user can temprarily saves the adjustment",
					"User should be able to temprarily saves the adjustment",
					"User could'nt temprarily saves the adjustment", "fail");
		}

	}

	public void draftReleaseBankReconcilationSmoke() throws Exception {
		String un_reconcile_amount_after_select_advice__and_after_adustment = driver
				.findElement(By.xpath(lbl_unReconciledAmount)).getAttribute("value");
		String un_reconcile_amount_after_select_advice_comma_replaced__and_after_adustment = un_reconcile_amount_after_select_advice__and_after_adustment
				.replaceAll(",", "");
		double doub_un_reconcile_amount_after_select_advice__and_after_adustment = Double
				.parseDouble(un_reconcile_amount_after_select_advice_comma_replaced__and_after_adustment);
		final double THRESHOLD = .000001;

		boolean flag_un_reconcill_amount_verify_as_zero = false;
		if (Math.abs(doub_un_reconcile_amount_after_select_advice__and_after_adustment - 0.0) < THRESHOLD) {
			flag_un_reconcill_amount_verify_as_zero = true;
		}

		click(btn_draft);
		sleepCustomize3(lbl_docStatus);
		Thread.sleep(3000);
		String status = getText(lbl_docStatus);
		if (!status.equalsIgnoreCase("( Draft )")) {
			Thread.sleep(7000);
			status = getText(lbl_docStatus);
		}
		if (status.equals("( Draft )") && flag_un_reconcill_amount_verify_as_zero) {
			writeTestResults(
					"Verify user can draft the statement if 'Un-Reconciled Amount = 0' and there is any draft statement available for same bank account",
					"User should be able to draft the statement if 'Un-Reconciled Amount = 0' and there is no any draft statement available for same bank account",
					"User successfully draft the statement if 'Un-Reconciled Amount = 0' and and there is no any draft statement available for same bank account",
					"pass");
		} else {
			writeTestResults(
					"Verify user can draft the statement if 'Un-Reconciled Amount = 0' and there is any draft statement available for same bank account",
					"User should be able to draft the statement if 'Un-Reconciled Amount = 0' and there is no any draft statement available for same bank account",
					"User couldn't draft the statement if 'Un-Reconciled Amount = 0' or there is a draft statement available for same bank account",
					"fail");
		}

		click(btn_reelese);
		sleepCustomize3(lbl_docStatus);
		Thread.sleep(3000);
		sleepCusomized(lbl_docNo);
		trackCode = getText(lbl_docNo);
		String status_relese = getText(lbl_docStatus);
		if (!status_relese.equalsIgnoreCase("( Released )")) {
			Thread.sleep(7000);
			status_relese = getText(lbl_docStatus);
		}
		if (status_relese.equals("( Released )")) {
			writeTestResults("Verify user can release the bank statement",
					"User should be able to release the bank statement", "User successfully release the bank statement",
					"pass");
		} else {
			writeTestResults("Verify user can release the bank statement",
					"User should be able to release the bank statement", "User could'nt release the bank statement",
					"fail");
		}
	}

	/* Fin_TC_021 */
	public void navigateToPettyCashPage() throws Exception {
		navigateToTheFananceModule();
		click(btn_pettyCash);
		if (isDisplayed(btn_newPettyCash)) {
			writeTestResults("Verify user can navigate to Petty cash by-page",
					"User should be able to navigate to Petty cash by-page",
					"User successfully navigate to Petty cash by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Petty cash by-page",
					"User should be able to navigate to Petty cash by-page",
					"User could'nt navigate to Petty cash by-page", "fail");
		}

	}

	public void newPettyCashIOU() throws Exception {
		click(btn_newPettyCash);
		if (isDisplayed(drop_pettyCashAccount)) {
			writeTestResults("Verify user can navigate to new petty cash page",
					"User should be able to navigate to new petty cash page",
					"User successfully navigate to new petty cash page", "pass");
		} else {
			writeTestResults("Verify user can navigate to new petty cash page",
					"User should be able to navigate to new petty cash page",
					"User could'nt navigate to new petty cash page", "fail");
		}
	}

	public void fillNewPettyCashFormIOU() throws Exception {
		click(btn_docDate);
		click(btn_calenderBack);
		click(btn_calenderBack);
		click("25_Link");

		click(btn_dueDate);
		click(btn_calenderForward);
		click(btn_calenderForward);
		click("25_Link");

		String doc_date = getText(btn_docDate).replaceAll("/", "");
		String post_date = getText(btn_postDate).replaceAll("/", "");
		String due_date = getText(btn_dueDate).replaceAll("/", "");

		String doc_date1 = reverseStringDateToSLOrder(doc_date);
		String post_date1 = reverseStringDateToSLOrder(post_date);
		String due_date1 = reverseStringDateToSLOrder(due_date);

		int doc_date2 = Integer.parseInt(doc_date1);
		int post_date2 = Integer.parseInt(post_date1);
		int due_date2 = Integer.parseInt(due_date1);

		if (doc_date2 < post_date2 && post_date2 < due_date2) {
			writeTestResults("Verify user can select document date, post date and due date",
					"User should be able to select document date, post date and due date",
					"User successfully select document date, post date and due date", "pass");
		} else {

			writeTestResults("Verify user can select document date, post date and due date",
					"User should be able to select document date, post date and due date",
					"User could'nt select document date, post date and due date", "fail");
		}

		selectIndex(drop_pettyCashAccount, 1);
		if (isDisplayed(drop_pettyCashAccount)) {
			writeTestResults(
					"Verify user can choose petty cash book from look up and float and balance amount will update as per the ledger",
					"User should be able to choose petty cash book from look up and float and balance amount will update as per the ledger",
					"User successfully choose petty cash book from look up and float and balance amount will update as per the ledger",
					"pass");
		} else {
			writeTestResults(
					"Verify user can choose petty cash book from look up and float and balance amount will update as per the ledger",
					"User should be able to choose petty cash book from look up and float and balance amount will update as per the ledger",
					"User could'nt choose petty cash book from look up and float and balance amount wasn't update as per the ledger",
					"fail");
		}

		sendKeys(txt_descPettyCash, descriptionPettyCashIOU);
		click(btn_lookupPaidTo);
		InventoryAndWarehouseModule obj = new InventoryAndWarehouseModule();
		obj.sleepCusomized(txt_searchEmployee);
		sendKeys(txt_searchEmployee, employeePettyCash);
		pressEnter(txt_searchEmployee);
		doubleClick(lnk_resultEmployye);

		selectText(drop_pettyCashType, pettyCshType021);
		sendKeys(txt_amountPettyCash, amountPettyCash);

		click(nos_costApportionPettyCash);
		selectText(drop_costCenterPettyCash, costCenterPettyCsh);

		sendKeys(txt_costPresentagePettyCash, costPresentagePettyCash);

		click(btn_update);

		click(btn_draft);
		sleepCustomize3(lbl_docStatus);
		Thread.sleep(3000);
		String status = getText(lbl_docStatus);
		if (!status.equalsIgnoreCase("(Draft)")) {
			Thread.sleep(7000);
			status = getText(lbl_docStatus);
		}
		if (status.equals("(Draft)")) {
			writeTestResults("Verify user can draft Petty-Cash", "User should be able to Petty-Cash",
					"User successfully draft Petty-Cash", "pass");
		} else {
			writeTestResults("Verify user can draft Petty-Cash", "User should be able to draft Petty-Cash",
					"User couldn't draft Petty-Cash", "fail");
		}

		click(btn_reelese);
		sleepCustomize3(lbl_docStatus);
		Thread.sleep(3000);
		sleepCusomized(lbl_docNo);
		trackCode = getText(lbl_docNo);
		String status_relese = getText(lbl_docStatus);
		if (!status.equalsIgnoreCase("(Released)")) {
			Thread.sleep(7000);
			status = getText(lbl_docStatus);
		}
		if (status_relese.equals("(Released)")) {
			writeTestResults("Verify user can release Petty-Cash", "User should be able to release Petty-Cash",
					"User successfully release Petty-Cash", "pass");
		} else {
			writeTestResults("Verify user can release Petty-Cash", "User should be able to release Petty-Cash",
					"User could'nt release Petty-Cash", "fail");
		}

		click(btn_action);

		click(btn_journel);
		/* costPresentagePettyCash */
	}

	/* Fin_TC_022 */
	public void setlementTypePettyCash() throws Exception {
		navigateToTheFananceModule();
		click(btn_pettyCash);
		click(btn_newPettyCash);
		click(btn_docDate);
		click(btn_calenderBack);
		click(btn_calenderBack);
		click("25_Link");

		click(btn_dueDate);
		click(btn_calenderForward);
		click(btn_calenderForward);
		click("25_Link");

		String doc_date = getText(btn_docDate).replaceAll("/", "");
		String post_date = getText(btn_postDate).replaceAll("/", "");
		String due_date = getText(btn_dueDate).replaceAll("/", "");

		String doc_date1 = reverseStringDateToSLOrder(doc_date);
		String post_date1 = reverseStringDateToSLOrder(post_date);
		String due_date1 = reverseStringDateToSLOrder(due_date);

		int doc_date2 = Integer.parseInt(doc_date1);
		int post_date2 = Integer.parseInt(post_date1);
		int due_date2 = Integer.parseInt(due_date1);

		if (doc_date2 < post_date2 && post_date2 < due_date2) {
			writeTestResults("Verify user can select document date, post date and due date",
					"User should be able to select document date, post date and due date",
					"User successfully select document date, post date and due date", "pass");
		} else {

			writeTestResults("Verify user can select document date, post date and due date",
					"User should be able to select document date, post date and due date",
					"User could'nt select document date, post date and due date", "fail");
		}

		selectIndex(drop_pettyCashAccount, 1);
		if (isDisplayed(drop_pettyCashAccount)) {
			writeTestResults(
					"Verify user can choose petty cash book from look up and float and balance amount will update as per the ledger",
					"User should be able to choose petty cash book from look up and float and balance amount will update as per the ledger",
					"User successfully choose petty cash book from look up and float and balance amount will update as per the ledger",
					"pass");
		} else {
			writeTestResults(
					"Verify user can choose petty cash book from look up and float and balance amount will update as per the ledger",
					"User should be able to choose petty cash book from look up and float and balance amount will update as per the ledger",
					"User could'nt choose petty cash book from look up and float and balance amount wasn't update as per the ledger",
					"fail");
		}

		sendKeys(txt_descPettyCash, descriptionPettyCashIOU);
		click(btn_lookupPaidTo);
		sendKeys(txt_searchEmployee, employeePettyCash);
		pressEnter(txt_searchEmployee);
		doubleClick(lnk_resultEmployye);

		selectText(drop_pettyCashType, "Settlement");

		click(btn_iouLookup);
		doubleClick(lnk_iouResult);

		sendKeys(txt_amountPettyCash, amountPettyCash);

		click(nos_costApportionPettyCash);
		selectText(drop_costCenterPettyCash, costCenterPettyCsh);

		sendKeys(txt_costPresentagePettyCash, costPresentagePettyCash);

		click(btn_update);

		click(btn_glLookupPettyCash);

		pressEnter(txt_GL);

		doubleClick(lnk_resultGL);

		click(nos_narrationPettyCash);

		sendKeys(txt_descriptionNarationPettyCash, narrationDescription);

		click(btn_applyDescriptionPettyCash);

		click(btn_draft);
		sleepCustomize3(lbl_docStatus);
		Thread.sleep(3000);
		String status = getText(lbl_docStatus);
		if (!status.equalsIgnoreCase("(Draft)")) {
			Thread.sleep(7000);
			status = getText(lbl_docStatus);
		}
		if (status.equals("(Draft)")) {
			writeTestResults("Verify user can draft Petty-Cash", "User should be able to Petty-Cash",
					"User successfully draft Petty-Cash", "pass");
		} else {
			writeTestResults("Verify user can draft Petty-Cash", "User should be able to draft Petty-Cash",
					"User couldn't draft Petty-Cash", "fail");
		}

		click(btn_reelese);
		sleepCustomize3(lbl_docStatus);
		Thread.sleep(3000);
		sleepCusomized(lbl_docNo);
		trackCode = getText(lbl_docNo);
		String status_relese = getText(lbl_docStatus);
		if (!status.equalsIgnoreCase("(Released)")) {
			Thread.sleep(7000);
			status = getText(lbl_docStatus);
		}
		if (status_relese.equals("(Released)")) {
			writeTestResults("Verify user can release Petty-Cash", "User should be able to release Petty-Cash",
					"User successfully release Petty-Cash", "pass");
		} else {
			writeTestResults("Verify user can release Petty-Cash", "User should be able to release Petty-Cash",
					"User could'nt release Petty-Cash", "fail");
		}

		click(btn_action);

		click(btn_journel);

	}

	/* Fin_TC_023 */
	public void oneOffPettyCash() throws Exception {
		navigateToTheFananceModule();
		click(btn_pettyCash);
		if (isDisplayed(btn_newPettyCash)) {
			writeTestResults("Verify user can navigate to Petty cash by-page",
					"User should be able to navigate to Petty cash by-page",
					"User successfully navigate to Petty cash by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Petty cash by-page",
					"User should be able to navigate to Petty cash by-page",
					"User could'nt navigate to Petty cash by-page", "fail");
		}

		click(btn_newPettyCash);
		if (isDisplayed(drop_pettyCashAccount)) {
			writeTestResults("Verify user can navigate to new petty cash page",
					"User should be able to navigate to new petty cash page",
					"User successfully navigate to new petty cash page", "pass");
		} else {
			writeTestResults("Verify user can navigate to new petty cash page",
					"User should be able to navigate to new petty cash page",
					"User could'nt navigate to new petty cash page", "fail");
		}

		click(btn_docDate);
		click(btn_calenderBack);
		click(btn_calenderBack);
		click("25_Link");

		click(btn_dueDate);
		click(btn_calenderForward);
		click(btn_calenderForward);
		click("25_Link");

		String doc_date = getText(btn_docDate).replaceAll("/", "");
		String post_date = getText(btn_postDate).replaceAll("/", "");
		String due_date = getText(btn_dueDate).replaceAll("/", "");

		String doc_date1 = reverseStringDateToSLOrder(doc_date);
		String post_date1 = reverseStringDateToSLOrder(post_date);
		String due_date1 = reverseStringDateToSLOrder(due_date);

		int doc_date2 = Integer.parseInt(doc_date1);
		int post_date2 = Integer.parseInt(post_date1);
		int due_date2 = Integer.parseInt(due_date1);

		if (doc_date2 < post_date2 && post_date2 < due_date2) {
			writeTestResults("Verify user can select document date, post date and due date",
					"User should be able to select document date, post date and due date",
					"User successfully select document date, post date and due date", "pass");
		} else {

			writeTestResults("Verify user can select document date, post date and due date",
					"User should be able to select document date, post date and due date",
					"User could'nt select document date, post date and due date", "fail");
		}

		selectIndex(drop_pettyCashAccount, 1);
		if (isDisplayed(drop_pettyCashAccount)) {
			writeTestResults(
					"Verify user can choose petty cash book from look up and float and balance amount will update as per the ledger",
					"User should be able to choose petty cash book from look up and float and balance amount will update as per the ledger",
					"User successfully choose petty cash book from look up and float and balance amount will update as per the ledger",
					"pass");
		} else {
			writeTestResults(
					"Verify user can choose petty cash book from look up and float and balance amount will update as per the ledger",
					"User should be able to choose petty cash book from look up and float and balance amount will update as per the ledger",
					"User could'nt choose petty cash book from look up and float and balance amount wasn't update as per the ledger",
					"fail");
		}

		sendKeys(txt_descPettyCash, descriptionPettyCashIOU);
		click(btn_lookupPaidTo);
		sendKeys(txt_searchEmployee, employeePettyCash);
		pressEnter(txt_searchEmployee);
		doubleClick(lnk_resultEmployye);

		selectText(drop_pettyCashType, "One-Off");
		sendKeys(txt_amountPettyCash, amountPettyCash);

		click(nos_costApportionPettyCash);
		selectText(drop_costCenterPettyCash, costCenterPettyCsh);

		sendKeys(txt_costPresentagePettyCash, costPresentagePettyCash);

		click(btn_update);

		click(btn_glLookupPettyCash);

		pressEnter(txt_GL);

		doubleClick(lnk_resultGL);

		click(nos_narrationPettyCash);

		sendKeys(txt_descriptionNarationPettyCash, narrationDescription);

		click(btn_applyDescriptionPettyCash);

		click(btn_draft);
		sleepCustomize3(lbl_docStatus);
		Thread.sleep(3000);
		String status = getText(lbl_docStatus);
		if (!status.equalsIgnoreCase("(Draft)")) {
			Thread.sleep(7000);
			status = getText(lbl_docStatus);
		}
		if (status.equals("(Draft)")) {
			writeTestResults("Verify user can draft Petty-Cash", "User should be able to Petty-Cash",
					"User successfully draft Petty-Cash", "pass");
		} else {
			writeTestResults("Verify user can draft Petty-Cash", "User should be able to draft Petty-Cash",
					"User couldn't draft Petty-Cash", "fail");
		}

		click(btn_reelese);
		sleepCustomize3(lbl_docStatus);
		Thread.sleep(3000);
		sleepCusomized(lbl_docNo);
		trackCode = getText(lbl_docNo);
		String status_relese = getText(lbl_docStatus);
		if (!status.equalsIgnoreCase("(Released)")) {
			Thread.sleep(7000);
			status = getText(lbl_docStatus);
		}
		if (status_relese.equals("(Released)")) {
			writeTestResults("Verify user can release Petty-Cash", "User should be able to release Petty-Cash",
					"User successfully release Petty-Cash", "pass");
		} else {
			writeTestResults("Verify user can release Petty-Cash", "User should be able to release Petty-Cash",
					"User could'nt release Petty-Cash", "fail");
		}

		click(btn_action);

		click(btn_journel);

	}

	/* Smoke_Finance_11 */
	public void navigateToJourneEntryPage() throws Exception {
		navigateToTheFananceModule();

		click(btn_journelEntryPage);
		if (isDisplayed(btn_newJournelEntryPage)) {
			writeTestResults("Verify user can navigate to Journel Entry by-page",
					"User should navigate to Journel Entry by-page",
					"User successfully navigate to Journel Entry by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Journel Entry by-page",
					"User should navigate to Journel Entry by-page", "User couldn't navigate to Journel Entry by-page",
					"fail");
		}
	}

	public void navigateToNewJournelEntryForm() throws Exception {
		click(btn_newJournelEntryPage);
		sleepCustomize3(lbl_docStatus1);
		String status = getText(lbl_docStatus1);
		if (status.equals("New")) {
			writeTestResults("Verify user can navigate to new Journel Entry form",
					"User should navigate to new Journel Entry form",
					"User sucessfully navigate to new Journel Entry form", "pass");
		} else {
			writeTestResults("Verify user can navigate to new Journel Entry form",
					"User should navigate to new Journel Entry form",
					"User couldn't navigate to new Journel Entry form", "fail");
		}
	}

	public void fillBankAdjustment() throws Exception {
		click(btn_description);

		sendKeys(txt_descriptionJournelEntry, descriptionJournelEntry);

		click(btn_applyDescriptionJournelEntry);

		String desc = driver.findElement(By.xpath(txt_setDescriptionJournelEntry)).getAttribute("value");

		if (desc.equals(descriptionJournelEntry)) {
			writeTestResults("Verify user can add description", "User should be able to add description",
					"User sucessfully add description", "pass");
		} else {
			writeTestResults("Verify user can add description", "User should be able to add description",
					"User couldn't add description", "fail");
		}

		click(btn_addRowJournelEntry);
		click(btn_addRow2JournelEntry);

		click(btn_serchLoockupGLJournelEntry);
		sendKeys(txt_GL, account1JournelEntry);
		pressEnter(txt_GL);
		Thread.sleep(5000);
		doubleClick(lnk_resultGL);
		Thread.sleep(1000);

		click(btn_serchLoockupGLJournelEntry2);
		sendKeys(txt_GL, account2JournelEntry);
		pressEnter(txt_GL);
		Thread.sleep(5000);
		doubleClick(lnk_resultGL);

		click(btn_serchLoockupGLJournelEntry3);
		sendKeys(txt_GL, account3JournelEntry);
		pressEnter(txt_GL);
		doubleClick(lnk_resultGL);

		sendKeys(txt_value1JjornelEntry, value1JournelEntry);

		sendKeys(txt_value2JjornelEntry, value2JournelEntry);

		sendKeys(txt_value3JjornelEntry, value3JournelEntry);

		click(txt_setDescriptionJournelEntry);

		String values[] = new String[6];

		values[0] = driver.findElement(By.xpath(lbl_value1JournelAccount)).getText();
		values[1] = driver.findElement(By.xpath(lbl_value2JournelAccount)).getText();
		values[2] = driver.findElement(By.xpath(lbl_value3JournelAccount)).getText();
		values[3] = driver.findElement(By.xpath(txt_value1JjornelEntry)).getAttribute("value").replace(".00", "");
		values[4] = driver.findElement(By.xpath(txt_value2JjornelEntry)).getAttribute("value").replace(".00", "");
		values[5] = driver.findElement(By.xpath(txt_value3JjornelEntry)).getAttribute("value").replace(".00", "");

		if (values[0].equals(account1JournelEntry) && values[1].equals(account2JournelEntry)
				&& values[2].equals(account3JournelEntry)) {
			writeTestResults("Verify user can add GL accounts to the grid",
					"User should be able to add GL accounts to the grid",
					"User sucessfully add GL accounts to the grid", "pass");
		} else {
			writeTestResults("Verify user can add GL accounts to the grid",
					"User should be able to add GL accounts to the grid", "User couldn't add GL accounts to the grid",
					"fail");
		}

		if ((values[3].replace(".00", "")).equals(value1JournelEntry)
				&& (values[4].replace(".00", "")).equals(value2JournelEntry)
				&& (values[5].replace(".00", "")).equals(value3JournelEntry)) {
			writeTestResults("Verify user can add journel entry values",
					"User should be able to add journel entry values", "User sucessfully add journel entry values",
					"pass");
		} else {
			writeTestResults("Verify user can add journel entry values",
					"User should be able to add journel entry values", "User couldn't add journel entry values",
					"fail");
		}
	}

	public void draftJournelEntrySmoke() throws Exception {
		click(btn_draft);
		explicitWait(btn_reelese, 60);
		if (isDisplayed(btn_reelese)) {
			writeTestResults("Verify user can draft Journel Entry", "User should be able to draft Journel Entry",
					"User sucessfully draft Journel Entry", "pass");
		} else {
			writeTestResults("Verify user can draft Journel Entry", "User should be able to draft Journel Entry",
					"User couldn't draft Journel Entry", "fail");
		}
	}

	public void releaseJournelEntrySmoke() throws Exception {
		click(btn_reelese);
		sleepCusomized(lbl_docNo);
		trackCode = getText(lbl_docNo);
		sleepCustomize3(lbl_docStatus);
		String status_release = getText(lbl_docStatus);
		if (!status_release.equalsIgnoreCase("( Released )")) {
			Thread.sleep(7000);
			status_release = getText(lbl_docStatus);
		}
		if (status_release.equals("( Released )")) {
			writeTestResults("Verify user can released Journel Entry", "User should be able to released Journel Entry",
					"User sucessfully released Journel Entry", "pass");
		} else {
			writeTestResults("Verify user can released Journel Entry", "User should be able to released Journel Entry",
					"User couldn't released Journel Entry", "fail");
		}
	}

	public void navigateToBankAdjustmentPageSmoke() throws Exception {
		navigateToTheFananceModule();

		click(btn_bankaAdjustment);
		if (isDisplayed(btn_newBankAdjustment)) {
			writeTestResults("Verify user can navigate to Bank Ajdustment by-page",
					"User should navigate to Bank Ajdustment by-page",
					"User successfully navigate to Bank Ajdustment by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Bank Ajdustment by-page",
					"User should navigate to Bank Ajdustment by-page",
					"User couldn't navigate to Bank Ajdustment by-page", "fail");
		}
	}

	public void navigateToNewBankAdjustmentFormSmoke() throws Exception {
		click(btn_newBankAdjustment);
		sleepCustomize3(lbl_docStatus1);
		String status = getText(lbl_docStatus1);
		if (status.equals("New")) {
			writeTestResults("Verify user can navigate to new Bank Adjustment form",
					"User should navigate to new Bank Adjustment form",
					"User sucessfully navigate to new Bank Adjustment form", "pass");
		} else {
			writeTestResults("Verify user can navigate to new Bank Adjustment form",
					"User should navigate to new Bank Adjustment form",
					"User couldn't navigate to new Bank Adjustment form", "fail");
		}

	}

	public void filaBankAdjustmentDetailsSmoke() throws Exception {
		selectText(drop_bankBankAdjustmentSmoke, bankBankAdjustmentSmoke);

		Select comboBox = new Select(driver.findElement(By.xpath(drop_bankBankAdjustmentSmoke)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();

		if (selectedComboValue.equals(bankBankAdjustmentSmoke)) {
			writeTestResults("Verify user can select bank", "User should be able to select bank",
					"User sucessfully select bank", "pass");
		} else {
			writeTestResults("Verify user can select bank", "User should be able to select bank",
					"User couldn't select bank", "fail");
		}

		selectText(drop_acoountNoBankAdjustmentSmoke, accountBankAdjustmentSmoke);

		Select comboBox1 = new Select(driver.findElement(By.xpath(drop_acoountNoBankAdjustmentSmoke)));
		String selectedComboValue1 = comboBox1.getFirstSelectedOption().getText();
		String bankBankAdjustmentSmoke_with_usd = drop_acoountNoBankAdjustmentSmoke + "-(USD)";

		if (selectedComboValue1.equals(accountBankAdjustmentSmoke)) {
			writeTestResults("Verify user can select bank account", "User should be able to select bank account",
					"User sucessfully select bank account", "pass");
		} else {
			writeTestResults("Verify user can select bank account", "User should be able to select bank account",
					"User couldn't select bank account", "fail");
		}

		click(btn_glAccountLookupBankAdjustmentSmoke);
		sendKeys(txt_GL, glAccountBankAdjustmentSmoke);
		pressEnter(txt_GL);
		Thread.sleep(2000);
		doubleClick(lnk_resultGL);

		String gl_submitted = driver.findElement(By.xpath(txt_glInFrontPageBankAdjustmentSmoke)).getAttribute("value");
		if (glAccountBankAdjustmentSmoke.equals(gl_submitted)) {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User sucessfully set gl account fieald", "pass");
		} else {
			writeTestResults("Verify user can set gl account fieald", "User should be able to set gl account fieald",
					"User couldn't set gl account fieald", "fail");
		}

		sendKeys(txt_amountBankAdjustmentSmoke, amountBankAdjustmentSmoke);
		click(btn_summaryBankAdjustmentSmoke);
		String amount = driver.findElement(By.xpath(txt_amountBankAdjustmentSmoke)).getAttribute("value")
				.replace(".00000", "");
		if (amount.equals(amountBankAdjustmentSmoke)) {
			writeTestResults("Verify user can enter amount", "User should be able to enter amount",
					"User sucessfully enter amount", "pass");
		} else {
			writeTestResults("Verify user can enter amount", "User should be able to enter amount",
					"User couldn't enter amount", "fail");
		}

	}

	public void checkoutBankAdjustmentSmoke() throws Exception {

		click(btn_checkout);
	}

	public void draftBankAdjustmentSmoke() throws Exception {
		click(btn_draft);
		if (isDisplayed(btn_reelese)) {
			writeTestResults("Verify user can checkout and  draft Bank Adjustment",
					"User should be able to checkout and  draft Bank Adjustment",
					"User sucessfully checkout and  draft Bank Adjustment", "pass");
		} else {
			writeTestResults("Verify user can checkout and  draft Bank Adjustment",
					"User should be able to checkout and  draft Bank Adjustment",
					"User couldn't checkout and  draft Bank Adjustment", "fail");
		}
	}

	public void releaseBankAdjustmentSmoke() throws Exception {
		click(btn_reelese);
		Thread.sleep(3000);
		sleepCusomized(lbl_docNo);
		trackCode = getText(lbl_docNo);
		sleepCustomize3(lbl_docStatus);
		String status_release = getText(lbl_docStatus);
		if (!status_release.equalsIgnoreCase("( Released )")) {
			Thread.sleep(7000);
			status_release = getText(lbl_docStatus);
		}
		if (status_release.equals("( Released )")) {
			writeTestResults("Verify user can release Bank Adjustment",
					"User should be able to release Bank Adjustment", "User sucessfully release Bank Adjustment",
					"pass");
		} else {
			writeTestResults("Verify user can release Bank Adjustment",
					"User should be able to release Bank Adjustment", "User couldn't release Bank Adjustment", "fail");
		}

	}

	/* Smoke_Finance_02 */
	public void navigateToNewBankFacilityAgreement() throws Exception {
		navigateToTheFananceModule();
		click(btn_bankFacilityAgreement);
		if (isDisplayed(btn_newBankFacilityAgreement)) {
			writeTestResults("Verify user can navigate to the Bank Facility Agreement by-page",
					"User should be able to navigate to the Bank Facility Agreement by-page",
					"User sucessfully navigate to the Bank Facility Agreement by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to the Bank Facility Agreement by-page",
					"User should be able to navigate to the Bank Facility Agreement by-page",
					"User couldn't navigate to the Bank Facility Agreement by-page", "fail");
		}

		click(btn_newBankFacilityAgreement);
		if (isDisplayed(drop_bankFacilityGroupBFA)) {
			writeTestResults("Verify user can navigate to the New Bank Facility Agreement",
					"User should be able to navigate to the New Bank Facility Agreement",
					"User sucessfully navigate to the New Bank Facility Agreement", "pass");
		} else {
			writeTestResults("Verify user can navigate to the New Bank Facility Agreement",
					"User should be able to navigate to the New Bank Facility Agreement",
					"User couldn't navigate to the New Bank Facility Agreement", "fail");
		}
	}

	public void fillBankFacilityAgreementSmoke() throws Exception {
		sendKeys(txt_originalDocNoBFA, originalDocNoBFA);
		String original_doc = driver.findElement(By.xpath(txt_originalDocNoBFA)).getAttribute("value");
		if (original_doc.equals(originalDocNoBFA)) {
			writeTestResults(
					"Verify user can enter original document number with all relevent characters, numbers and symbols",
					"User should be able to enter original document number with all relevent characters, numbers and symbols",
					"User sucessfully enter original document number with all relevent characters, numbers and symbols",
					"pass");
		} else {
			writeTestResults(
					"Verify user can enter original document number with all relevent characters, numbers and symbols",
					"User should be able to enter original document number with all relevent characters, numbers and symbols",
					"User couldn't enter original document number with all relevent characters, numbers and symbols",
					"fail");
		}

		selectText(drop_bankFacilityGroupBFA, bankFacilityGroup);
		Select comboBox2 = new Select(driver.findElement(By.xpath(drop_bankFacilityGroupBFA)));
		String selectedComboValue2 = comboBox2.getFirstSelectedOption().getText();
		if (selectedComboValue2.equals(bankFacilityGroup)) {
			writeTestResults("Verify user can select bank facility group",
					"User should be able to select bank facility group", "User sucessfully select bank facility group",
					"pass");
		} else {
			writeTestResults("Verify user can select bank facility group",
					"User should be able to select bank facility group", "User couldn't select bank facility group",
					"fail");
		}

		selectText(drop_bank, bankNameBFA);
		Select comboBox3 = new Select(driver.findElement(By.xpath(drop_bank)));
		String selectedComboValue3 = comboBox3.getFirstSelectedOption().getText();
		if (selectedComboValue3.equals(bankNameBFA)) {
			writeTestResults("Verify user can select bank", "User should be able to select bank",
					"User sucessfully select bank", "pass");
		} else {
			writeTestResults("Verify user can select bank", "User should be able to select bank",
					"User couldn't select bank", "fail");
		}

		selectText(drop_bankAccountNo, accountNoBFA);
		Select comboBox4 = new Select(driver.findElement(By.xpath(drop_bankAccountNo)));
		String selectedComboValue4 = comboBox4.getFirstSelectedOption().getText();
		if (selectedComboValue4.equals(accountNoBFA)) {
			writeTestResults("Verify user can select bank account", "User should be able to select bank account",
					"User sucessfully select bank account", "pass");
		} else {
			writeTestResults("Verify user can select bank account", "User should be able to select bank account",
					"User couldn't select bank account", "fail");
		}

		click(txt_startDateBFA);
		click(currentDay() + "_link");
		if (isDisplayed(txt_startDateBFA)) {
			writeTestResults("Verify user can set start date", "User should be able to set start date",
					"User sucessfully set start date", "pass");
		} else {
			writeTestResults("Verify user can set start date", "User should be able to set start date",
					"User couldn't set start date", "fail");
		}

		click(txt_endDateBFA);
		for (int i = 0; i < 12; i++) {
			click(btn_calenderForward);
		}
		click("28_link");
		if (isDisplayed(txt_endDateBFA)) {
			writeTestResults("Verify user can set start date", "User should be able to set start date",
					"User sucessfully set start date", "pass");
		} else {
			writeTestResults("Verify user can set start date", "User should be able to set start date",
					"User couldn't set start date", "fail");
		}

		selectText(drop_currency, currencyBFA);
		Select comboBox5 = new Select(driver.findElement(By.xpath(drop_currency)));
		String selectedComboValue5 = comboBox5.getFirstSelectedOption().getText();
		if (selectedComboValue5.equals(currencyBFA)) {
			writeTestResults("Verify user can select currency", "User should be able to select currency",
					"User sucessfully select currency", "pass");
		} else {
			writeTestResults("Verify user can select currency", "User should be able to select currency",
					"User couldn't select currency", "fail");
		}

		sendKeys(txt_amount, amountBFA);
		String amount = driver.findElement(By.xpath(txt_amount)).getAttribute("value");
		if (amount.equals(amountBFA)) {
			writeTestResults("Verify user can enter loan amount", "User should be able to enter loan amount",
					"User sucessfully enter loan amount", "pass");
		} else {
			writeTestResults("Verify user can enter loan amount", "User should be able to enter loan amount",
					"User couldn't enter loan amount", "fail");
		}
	}

	public void draftBankFacilityAgreementSmoke() throws Exception {
		click(btn_draft);
		if (isDisplayed(btn_reelese)) {
			writeTestResults("Verify user can draft Bank Facility Agreement",
					"User should be able to draft Bank Facility Agreement",
					"User sucessfully draft Bank Facility Agreement", "pass");
		} else {
			writeTestResults("Verify user can draft Bank Facility Agreement",
					"User should be able to draft Bank Facility Agreement",
					"User couldn't draft Bank Facility Agreement", "fail");
		}
	}

	public void releaseBankFacilityAgreementSmoke() throws Exception {
		click(btn_reelese);
		Thread.sleep(3000);
		sleepCusomized(lbl_docNo);
		trackCode = getText(lbl_docNo);
		sleepCusomized(lbl_docNo);
		trackCode = getText(lbl_docNo);
		sleepCustomize3(lbl_docStatus);
		String status_release = getText(lbl_docStatus);
		if (!status_release.equalsIgnoreCase("( Released )")) {
			Thread.sleep(7000);
			status_release = getText(lbl_docStatus);
		}
		if (status_release.equals("( Released )")) {
			writeTestResults("Verify user can release Bank Facility Agreement",
					"User should be able to release Bank Facility Agreement",
					"User sucessfully release Bank Facility Agreement", "pass");
		} else {
			writeTestResults("Verify user can release Bank Facility Agreement",
					"User should be able to release Bank Facility Agreement",
					"User couldn't release Bank Facility Agreement", "fail");
		}
	}

	public void loanCreationAndJournelEntryValidity() throws Exception {

		click(btn_action);
		click(btn_journel);
		Thread.sleep(2000);
		String lbl1 = getText(lbl_loanCreationConfirmation1);
		String lbl2 = getText(lbl_loanCreationConfirmation2);

		if (lbl1.equals(acoounLoanConfiramationBFA) || lbl2.equals(acoounLoanConfiramationBFA)) {
			writeTestResults("Verify user can create loan using Bank Facility Agreement",
					"User should be able to create loan using Bank Facility Agreement",
					"User sucessfully create loan using Bank Facility Agreement", "pass");
		} else {
			writeTestResults("Verify user can create loan using Bank Facility Agreement",
					"User should be able to create loan using Bank Facility Agreement",
					"User couldn't create loan using Bank Facility Agreement", "fail");
		}
		Thread.sleep(2000);
		String debit = getText(lbl_journelEntryFirstRowDebit);
		String credit = getText(lbl_journelEntrySecondRowCredit);

		if (debit.equals(credit)) {
			writeTestResults("Verify user can view the journal entry to verify accuracy",
					"User should be able to view the journal entry to verify accuracy",
					"User successfully view the journal entry to verify accuracy", "pass");
		} else {
			writeTestResults("Verify user can view the journal entry to verify accuracy",
					"User should be able to view the journal entry to verify accuracy",
					"User could'nt view the journal entry to verify accuracy", "fail");
		}

	}

	/* Smoke_Finance_10 */
	public void navigateToBankREconcilationForm() throws Exception {
		navigateToTheFananceModule();
		click(btn_bankReconcillation);
		Thread.sleep(2000);
		String header = getText(header_page);
		if (header.equals("Bank Reconciliation")) {
			writeTestResults("Verify user can navigate to bank reconciliation by-page",
					"User should be able to navigate to bank reconciliation by-page",
					"User successfully navigate to bank reconciliation by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to bank reconciliation by-page",
					"User should be able to navigate to bank reconciliation by-page",
					"User could'nt navigate to bank reconciliation by-page", "fail");
		}

		click(btn_newBankReconcilation);
		Thread.sleep(2000);
		String status = getText(lbl_docStatus1);
		if (status.equals("New")) {
			writeTestResults("Verify user can navigate to new bank reconciliation page",
					"User should be able to navigate to new bank reconciliation page",
					"User successfully navigate to new bank reconciliation page", "pass");
		} else {
			writeTestResults("Verify user can navigate to new bank reconciliation page",
					"User should be able to navigate to new bank reconciliation page",
					"User could'nt navigate to new bank reconciliation page", "fail");
		}
	}

	public void fillBankReconcilation() throws Exception {
		click(btn_docDate);
		String xpath_date = currentDay() + "_Link";
		click(xpath_date);
		if (isDisplayed(btn_docDate)) {
			writeTestResults("Verify user can select document date", "User should be able to select document date",
					"User successfully select document date", "pass");
		} else {
			writeTestResults("Verify user can select document date", "User should be able to select document date",
					"User could'nt select document date", "fail");
		}

		click(btn_statementDateBankReconcilation);
		click(currentDay() + "_Link");
		if (isDisplayed(btn_statementDateBankReconcilation)) {
			writeTestResults("Verify user can select statement date", "User should be able to select statement date",
					"User successfully select statement date", "pass");
		} else {
			writeTestResults("Verify user can select statement date", "User should be able to select statement date",
					"User could'nt select statement date", "fail");
		}

		selectText(drop_bank, bankBankReconcillation);
		if (isDisplayed(drop_bank)) {
			writeTestResults("Verify user can select the bank", "User should be able to select the bank",
					"User successfully select the bank", "pass");
		} else {
			writeTestResults("Verify user can select the bank", "User should be able to select the bank",
					"User could'nt select the bank", "fail");
		}

		selectText(drop_accountNo, bankAccountBankReconcilation);
		if (isDisplayed(drop_accountNo)) {
			writeTestResults(
					"Verify user can select the bank account no and system can load the advices related to the selected bank and account no only",
					"User should be able to select the bank account no and system should load the advices related to the selected bank and account no only",
					"User successfully select the bank account no and system successfully load the advices related to the selected bank and account no only",
					"pass");
		} else {
			writeTestResults(
					"Verify user can select the bank account no and can load the advices related to the selected bank and account no only",
					"User should be able to select the bank account no and system should load the advices related to the selected bank and account no only",
					"User could'nt select the bank account no and system doesn't load the advices related to the selected bank and account no only",
					"fail");
		}

		/* Last Reconcile Amount */
		Thread.sleep(1000);
		String last_reconcilation_balance = driver.findElement(By.xpath(lbl_lastBalaceBankReconcilation))
				.getAttribute("value");
		String last_reconcilation_balance_coma_replaced = last_reconcilation_balance.replaceAll(",", "");
		double doub_last_reconcilation_balance_coma_replaced = Double
				.parseDouble(last_reconcilation_balance_coma_replaced);

		/* Adding Amount */
		double bank_statement_adding_value = Double.parseDouble(bankStatementREconcilationStatementBalance);

		/* Input Amount */
		double statement_amount = doub_last_reconcilation_balance_coma_replaced + bank_statement_adding_value;

		String statement_amount_input = String.valueOf(statement_amount);
		sendKeys(txt_statementBalance, statement_amount_input);
		if (isDisplayed(txt_statementBalance)) {
			writeTestResults("Verify user can enter the statement balance",
					"User should be able to enter the statement balance",
					"User successfully enter the statement balance", "pass");
		} else {
			writeTestResults("Verify user can enter the statement balance",
					"User should be able to enter the statement balance", "User could'nt enter the statement balance",
					"fail");
		}

//		String checue_num = readFinData(4);
//		String advice_xpath = chk_advicePathBankReconcilation.replace("checkuu_num", checue_num);
//		Thread.sleep(3000);
//		pageScrollUpToBottom();
//		Thread.sleep(2000);
//		WebElement a = driver.findElement(By.xpath(btn_scrollDownArrowBankReconsilGrid));
//		Actions action1 = new Actions(driver);
//		action1.clickAndHold(a).build().perform();
//		customizeLoadingDelay(advice_xpath,40);
//		click(div_reconcilAdvice);
//		InventoryAndWarehouseModule obj = new InventoryAndWarehouseModule();
//		if (obj.isDisplayedQuickCheck(advice_xpath)) {
//			/* Verify Relevant Advice */
//			boolean flagReleventAdvices = false;
//			if (obj.isDisplayedQuickCheck(advice_xpath)) {
//				flagReleventAdvices = true;
//			}
//
//			/* Advice Amount */
//			String xpath_advice_amount = lbl_amountAdviceBankReconcilation.replace("checkuu_num", checue_num);
//			String advice_amount = getText(xpath_advice_amount);
//			String advice_amount1 = advice_amount.replaceAll(",", "");
//			double advice_amount_doub = Double.parseDouble(advice_amount1);
//			
//			pageScrollUpToTop();
//			/* Un-Reconcile amount before select advice */
//			click(txt_description1);
//			String un_reconcil_amount_before_advice_select = driver.findElement(By.xpath(lbl_unReconciledAmount))
//					.getAttribute("value");
//			String un_reconcil_amount_before_advice_select_comma_replaced = un_reconcil_amount_before_advice_select
//					.replaceAll(",", "");
//			double doub_un_reconcil_amount_before_advice_select = Double
//					.parseDouble(un_reconcil_amount_before_advice_select_comma_replaced);
//
//			/* Verify reconcile amount */
//			pageScrollUpToBottom();
//			click(advice_xpath);
//			boolean flagVerifyReconcil = false;
//
//			/* Reconcile amount after select advice */
//			String reconcille_amount_after_select_advice = driver.findElement(By.xpath(lbl_reconcilAmount))
//					.getAttribute("value");
//			String reconcille_amount_after_select_advice_coma_replaced = reconcille_amount_after_select_advice
//					.replaceAll(",", "");
//			double doub_reconcille_amount_after_select_advice = Double
//					.parseDouble(reconcille_amount_after_select_advice_coma_replaced);
//
//			final double THRESHOLD = .000001;
//			if (Math.abs(doub_reconcille_amount_after_select_advice - advice_amount_doub) < THRESHOLD) {
//				flagVerifyReconcil = true;
//			}
//
//			/* Verify un-reconcile amount */
//			boolean flagUNVerifyUNReconcil = false;
//			String un_reconcile_amount_after_select_advice = driver.findElement(By.xpath(lbl_unReconciledAmount))
//					.getAttribute("value");
//			String un_reconcile_amount_after_select_advice_comma_replaced = un_reconcile_amount_after_select_advice
//					.replaceAll(",", "");
//			doub_un_reconcile_amount_after_select_advice = Double
//					.parseDouble(un_reconcile_amount_after_select_advice_comma_replaced);
//			if (Math.abs(doub_un_reconcile_amount_after_select_advice
//					- (doub_un_reconcil_amount_before_advice_select - advice_amount_doub)) < THRESHOLD) {
//				flagUNVerifyUNReconcil = true;
//			}
//
//			if (flagReleventAdvices && flagVerifyReconcil && flagUNVerifyUNReconcil) {
//				writeTestResults(
//						"Verify user can select the advices applicable for the selected statement. And Reconciled Amount and Un-Reconciled Amount are change according to the selected advice value",
//						"User should be able to select the advices applicable for the selected statement. And Reconciled Amount and Un-Reconciled Amount will change according to the selected advice value",
//						"User successfully select the advices applicable for the selected statement. And Reconciled Amount and Un-Reconciled Amount successfully change according to the selected advice value",
//						"pass");
//			} else {
//				writeTestResults(
//						"Verify user can select the advices applicable for the selected statement. And Reconciled Amount and Un-Reconciled Amount are change according to the selected advice value",
//						"User should be able to select the advices applicable for the selected statement. And Reconciled Amount and Un-Reconciled Amount will change according to the selected advice value",
//						"User couldn't select the advices applicable for the selected statement. And Reconciled Amount and Un-Reconciled Amount doesn't change according to the selected advice value",
//						"fail");
//			}
//		} else {
//			writeTestResults(
//					"Verify user can select the advices applicable for the selected statement. And Reconciled Amount and Un-Reconciled Amount are change according to the selected advice value",
//					"User should be able to select the advices applicable for the selected statement. And Reconciled Amount and Un-Reconciled Amount will change according to the selected advice value",
//					"User couldn't select the advices applicable for the selected statement. And Reconciled Amount and Un-Reconciled Amount doesn't change according to the selected advice value",
//					"fail");
//		}
	}

	public void completeAdjustmentBankReconcilation() throws Exception {
//		click(btn_adjustentBankReconcilation);
//		if (isDisplayed(btn_descriptionLookupBAnkReconcilationAdjustment)) {
//			writeTestResults("Verify adjustment window is appeared ", "Adjustment window should appeared",
//					"Adjustment window successfully appeared ", "pass");
//		} else {
//			writeTestResults("Verify adjustment window is appeared ", "Adjustment window should appeared",
//					"Adjustment window doesn't appeared ", "fail");
//		}
//
//		click(btn_descriptionLookupBAnkReconcilationAdjustment);
//		sendKeys(txt_descriptionAdjustmenrBankReconcilation, adjustmentDescriptionBankReconcilation);
//		click(btn_applyDescriptionBankeconcilation);
//
//		click(gl_nosAdjustmentBankReconcilation);
//		sendKeys(txt_GL, glAccountAdjustmetnBankReconcillation);
//		pressEnter(txt_GL);
//		doubleClick(lnk_resultGL);
//
//		String adjustment_amount = String.valueOf(doub_un_reconcile_amount_after_select_advice);
//		sendKeys(txt_amountUluBankReconsillAdjustment, adjustment_amount);
//		click(btn_costAllocationAdjustmentBankReconcilation);
//		selectText(drop_costCenter, departmentCostAllocationBankReconcilationAdjustment);
//		sendKeys(txt_apportionPresentage, presentageCostAllocationBankReconcilationAdjustment);
//		click(btn_addRecordApportion);
//		click(btn_updateCostCenter);
//		click(btn_updateAdjustmentBankReconcilation);
//		String header = getText(header_page2);
//		if (header.equals("Bank Reconciliation")) {
//			writeTestResults("Verify user can temprarily saves the adjustment",
//					"User should be able to temprarily saves the adjustment",
//					"User successfully temprarily saves the adjustment", "pass");
//		} else {
//			writeTestResults("Verify user can temprarily saves the adjustment",
//					"User should be able to temprarily saves the adjustment",
//					"User could'nt temprarily saves the adjustment", "fail");
//		}

	}

	public void draftReleaseBankReconcilation() throws Exception {
//		String un_reconcile_amount_after_select_advice__and_after_adustment = driver
//				.findElement(By.xpath(lbl_unReconciledAmount)).getAttribute("value");
//		String un_reconcile_amount_after_select_advice_comma_replaced__and_after_adustment = un_reconcile_amount_after_select_advice__and_after_adustment
//				.replaceAll(",", "");
//		double doub_un_reconcile_amount_after_select_advice__and_after_adustment = Double
//				.parseDouble(un_reconcile_amount_after_select_advice_comma_replaced__and_after_adustment);
//		final double THRESHOLD = .000001;
//
//		boolean flag_un_reconcill_amount_verify_as_zero = false;
//		if (Math.abs(doub_un_reconcile_amount_after_select_advice__and_after_adustment - 0.0) < THRESHOLD) {
//			flag_un_reconcill_amount_verify_as_zero = true;
//		}
//
//		click(btn_draft);
//		sleepCustomize3(lbl_docStatus);
//		Thread.sleep(3000);
//		String status = getText(lbl_docStatus);
//		if (!status.equalsIgnoreCase("( Draft )")) {
//			Thread.sleep(7000);
//			status = getText(lbl_docStatus);
//		}
//		if (status.equals("( Draft )") && flag_un_reconcill_amount_verify_as_zero) {
//			writeTestResults(
//					"Verify user can draft the statement if 'Un-Reconciled Amount = 0' and there is any draft statement available for same bank account",
//					"User should be able to draft the statement if 'Un-Reconciled Amount = 0' and there is no any draft statement available for same bank account",
//					"User successfully draft the statement if 'Un-Reconciled Amount = 0' and and there is no any draft statement available for same bank account",
//					"pass");
//		} else {
//			writeTestResults(
//					"Verify user can draft the statement if 'Un-Reconciled Amount = 0' and there is any draft statement available for same bank account",
//					"User should be able to draft the statement if 'Un-Reconciled Amount = 0' and there is no any draft statement available for same bank account",
//					"User couldn't draft the statement if 'Un-Reconciled Amount = 0' or there is a draft statement available for same bank account",
//					"fail");
//		}
//
//		click(btn_reelese);
//		sleepCustomize3(lbl_docStatus);
//		Thread.sleep(3000);
//		sleepCusomized(lbl_docNo);
//		trackCode = getText(lbl_docNo);
//		String status_relese = getText(lbl_docStatus);
//		if (!status_relese.equalsIgnoreCase("( Released )")) {
//			Thread.sleep(7000);
//			status_relese = getText(lbl_docStatus);
//		}
//		if (status_relese.equals("( Released )")) {
//			writeTestResults("Verify user can release the bank statement",
//					"User should be able to release the bank statement", "User successfully release the bank statement",
//					"pass");
//		} else {
//			writeTestResults("Verify user can release the bank statement",
//					"User should be able to release the bank statement", "User could'nt release the bank statement",
//					"fail");
//		}
	}

	public void commonCustomerAccountCreation() throws Exception {
		InventoryAndWarehouseModule obj = new InventoryAndWarehouseModule();
		customizeLoadingDelay(navigation_pane, 20);
		click(navigation_pane);
		customizeLoadingDelay(btn_sallesMarketingModule, 20);
		click(btn_sallesMarketingModule);
		customizeLoadingDelay(btn_accountInformation, 20);
		click(btn_accountInformation);
		customizeLoadingDelay(btn_newAccountCustomerAccount, 20);
		click(btn_newAccountCustomerAccount);

		customizeLoadingDelay(txt_accountNameCustomerAccount, 20);

		String customer = (currentTimeAndDate().replaceAll("/", "").replaceAll(":", "") + "-Madhushan-CustomerAccount");
		sendKeys(txt_accountNameCustomerAccount, customer);

		selectText(drop_accountGtopuCustomerAccount, accountGroup);

		click(btn_draft);

		obj.sleepCusomized(btn_reelese);
		customizeLoadingDelay(btn_reelese, 20);
		click(btn_reelese);
		Thread.sleep(3000);
		String status = getText(lbl_docStatus);
		if (!status.equalsIgnoreCase("(Released)")) {
			Thread.sleep(7000);
			status = getText(lbl_docStatus);
		}

		writeFinData("CustomerAccount", customer, 12);
	}

	/* common */
	public void customizeLoadingDelay(String xpath, int seconds) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, seconds);
		wait.until(ExpectedConditions.visibilityOfElementLocated(getLocator(xpath)));
	}
}
