package bileeta.BTAF.PageObjects;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.JavascriptExecutor;

import bileeta.BATF.Pages.ProductionModuleData;
import bileeta.BTAF.Utilities.CommonMethods;

public class ProductionModuleRegression2 extends ProductionModuleData {
	/* write production data */
	private static Workbook wb;
	private static Sheet sh;
	private static FileInputStream fis;
	private static FileOutputStream fos;
	private static Row row;
	private static Cell cell;
	private static Cell cell1;
	private static Cell cell2;

	/* Test Case Variables */
	/* PROD_PO_050 */
	String draftedProductionOrder;

	/* PROD_PO_050 */
	String releasedProductionOrder;

	/* PROD_PO_052 */
	String uniqueDescriptionPO;
	String firstPO;

	/* PROD_PC_003 */
	String postBusinessUnit;
	
	/* PROD_PC_005 */
	String selectedPriority;
	
	/* PROD_PC_009 */
	String requestedStartDate;

	/* Common Methods */
	public void loginBiletaAutomation() throws Exception {
		openPage(siteURL);
		Thread.sleep(2000);
		explicitWait(txt_username, 40);
		sendKeys(txt_username, automationUserEmail);
		Thread.sleep(1000);
		sendKeys(txt_password, automationUserPassword);
		Thread.sleep(2000);
		click(btn_login);
		explicitWait(navigateMenu, 40);
		if (isDisplayedQuickCheck(div_loginVerification)) {
			openPage(siteURL);
			Thread.sleep(2000);
			explicitWait(txt_username, 40);
			sendKeys(txt_username, automationUserEmail);
			Thread.sleep(1000);
			sendKeys(txt_password, automationUserPassword);
			Thread.sleep(2000);
			click(btn_login);
			explicitWait(navigateMenu, 40);
		}

	}

	public void writeProductionData(String name, String variable, int data_row)
			throws InvalidFormatException, IOException {
		String path = "";
		String location = System.getProperty("user.dir");

		String real_project_path = "*src*test*java*productionModule*ProductionData.xlsx".replace("*", "\\");
		path = location + real_project_path;

		fis = new FileInputStream(path);
		wb = WorkbookFactory.create(fis);
		sh = wb.getSheet("Sheet1");
		row = sh.createRow(data_row);
		cell = row.createCell(1);
		cell1 = row.createCell(0);
		cell.setCellValue(variable);
		cell1.setCellValue(name);
		/* System.out.println(cell.getStringCellValue()); */
		fos = new FileOutputStream(path);
		wb.write(fos);
		fos.flush();
		fos.close();
	}

	public String readProductionData(String name) throws IOException, InvalidFormatException {
		String path = "";
		String location = System.getProperty("user.dir");

		String real_project_path = "*src*test*java*productionModule*ProductionData.xlsx".replace("*", "\\");
		path = location + real_project_path;

		fis = new FileInputStream(path);
		wb = WorkbookFactory.create(fis);
		sh = wb.getSheet("Sheet1");

		Map<String, String> iw_creation_map = new HashMap<String, String>();
		List<Map<String, String>> iw_creation = new ArrayList<Map<String, String>>();

		int totRows = sh.getLastRowNum();
		for (i = 0; i <= totRows; i++) {
			row = sh.getRow(i);
			cell = row.getCell(0);
			cell2 = row.getCell(1);

			String test_creation_row1 = cell.getStringCellValue();
			String test_creation_row2 = cell2.getStringCellValue();

			iw_creation_map.put(test_creation_row1, test_creation_row2);
			iw_creation.add(i, iw_creation_map);

		}

		String iw_info = iw_creation.get(0).get(name);

		return iw_info;
	}

	/* common objects */
	CommonMethods common = new CommonMethods();

	/* PROD_PO_048_049 */
	public void productionOrder_1_PROD_PO_048_049() throws Exception {
		loginBiletaAutomation();
		explicitWaitUntillClickable(img_navigationPane, 60);
		Thread.sleep(2000);
		explicitWaitUntillClickable(img_navigationPane, 20);
		if (click(img_navigationPane)) {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User successfully click navigation pane", "pass");
		} else {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User couldn't click navigation pane", "fail");
		}

		explicitWait(btn_production, 4);
		if (isEnabledQuickCheck(btn_production)) {
			writeTestResults("Verify user able to view the \"Production\" module under main navigation menu",
					"User should be able to view the \"Production\" module under main navigation menu",
					"User successfully view the \"Production\" module under main navigation menu", "pass");
		} else {
			writeTestResults("Verify user able to view the \"Production\" module under main navigation menu",
					"User should be able to view the \"Production\" module under main navigation menu",
					"User couldn't view the \"Production\" module under main navigation menu", "fail");
		}

		click(btn_production);

		explicitWait(btn_productionOrder, 4);
		if (isEnabledQuickCheck(btn_productionOrder)) {
			writeTestResults("Verify user able to view the \"Production Order\" option under production sub menu",
					"User should be able to view the \"Production Order\" option under production sub menu",
					"User able to view the \"Production Order\" option under production sub menu", "pass");
		} else {
			writeTestResults("Verify user able to view the \"Production Order\" option under production sub menu",
					"User should be able to view the \"Production Order\" option under production sub menu",
					"User couldn't view the \"Production Order\" option under production sub menu", "pass");
		}

		click(btn_productionOrder);

		explicitWait(header_productionOrderByPage, 60);
		if (isDisplayed(header_productionOrderByPage)) {
			writeTestResults("Verify user able to navigate to Production Order by-page",
					"User should be able to navigate to Production Order by-page",
					"User successfully navigate to Production Order by-page", "pass");
		} else {
			writeTestResults("Verify user able to navigate to Production Order by-page",
					"User should be able to navigate to Production Order by-page",
					"User coulsn't navigate to Production Order by-page", "fail");
		}

		click(btn_newProductionOrder);
		explicitWait(header_newProductionOrder, 60);
		if (isDisplayed(header_newProductionOrder)) {
			writeTestResults("Verify user able to navigate to Production Order new-form",
					"User should be able to navigate to Production Order new-form",
					"User successfully navigate to Production Order new-form", "pass");
		} else {
			writeTestResults("Verify user able to navigate to Production Order new-form",
					"User should be able to navigate to Production Order new-form",
					"User coulsn't navigate to Production Order new-form", "fail");
		}

		click(span_productLookupPO);
		explicitWait(lookup_product, 30);
		if (isDisplayed(lookup_product)) {
			writeTestResults("Verify that product lookup is open", "Product lookup should be open",
					"Product lookup successfully open", "pass");
		} else {
			writeTestResults("Verify that product lookup is open", "Product lookup should be open",
					"Product lookup doesn't open", "fail");
		}

		sendKeys(txt_searchProductProductLookup, readProductionData("ProductionTypeProductRegressionPO"));
		String enteredProduct = getAttribute(txt_searchProductProductLookup, "value");
		if (enteredProduct.equals(readProductionData("ProductionTypeProductRegressionPO"))) {
			writeTestResults("Verify that user able to enter the relevent product",
					"User should be able to enter the relevent product", "User successfully enter the relevent product",
					"pass");
		} else {
			writeTestResults("Verify that user able to enter the relevent product",
					"User should be able to enter the relevent product", "User doesn't enter the relevent product",
					"fail");
		}

		click(btn_searchProductProductLookup);
		explicitWaitUntillClickable(td_resultProductRegressionPO.replace("product_code",
				readProductionData("ProductionTypeProductRegressionPO")), 60);
		if (isEnabled(td_resultProductRegressionPO.replace("product_code",
				readProductionData("ProductionTypeProductRegressionPO")))) {
			writeTestResults("Verify that relevent product is available", "Relevent product should be available",
					"Relevent product successfully available", "pass");
		} else {
			writeTestResults("Verify that relevent product is available", "Relevent product should be available",
					"Relevent product not available", "fail");
		}

		doubleClick(td_resultProductRegressionPO.replace("product_code",
				readProductionData("ProductionTypeProductRegressionPO")));
		Thread.sleep(2000);

		String selectedProduct = getAttribute(txt_outputProductProductInformationSectionSummaryTabPO, "value");
		if (selectedProduct.equals(selectedProductFrontPageRegressionPO.replace("product_code",
				readProductionData("ProductionTypeProductRegressionPO")))) {
			writeTestResults("Verify that user able to select the relevent product",
					"User should be able to select the relevent product",
					"User successfully select the relevent product", "pass");
		} else {
			writeTestResults("Verify that user able to select the relevent product",
					"User should be able to select the relevent product", "User couldn't select the relevent product",
					"fail");
		}
	}

	public void productionOrder_2_PROD_PO_048_049() throws Exception {
		click(btn_productionModelLookupPO);
		explicitWait(lookup_productionModel, 30);
		if (isDisplayed(lookup_productionModel)) {
			writeTestResults("Verify that Production Model lookup is open", "Production Model lookup should be open",
					"Production Model lookup successfully open", "pass");
		} else {
			writeTestResults("Verify that Production Model lookup is open", "Production Model lookup should be open",
					"Production Model lookup doesn't open", "fail");
		}

		sendKeys(txt_searchProductionModel, readProductionData("ProductionModeleMRPAndInfiniteEnabledRegressionPO"));
		String enteredProductionModel = getAttribute(txt_searchProductionModel, "value");
		if (enteredProductionModel.equals(readProductionData("ProductionModeleMRPAndInfiniteEnabledRegressionPO"))) {
			writeTestResults("Verify that user able to enter the relevent Production Model",
					"User should be able to enter the relevent Production Model",
					"User successfully enter the relevent Production Model", "pass");
		} else {
			writeTestResults("Verify that user able to enter the relevent Production Model",
					"User should be able to enter the relevent Production Model",
					"User doesn't enter the relevent Production Model", "fail");
		}

		click(btn_searchProductionModel);
		explicitWaitUntillClickable(td_resultProductionModelDocReplace.replace("doc_no",
				readProductionData("ProductionModeleMRPAndInfiniteEnabledRegressionPO")), 60);
		if (isEnabled(td_resultProductionModelDocReplace.replace("doc_no",
				readProductionData("ProductionModeleMRPAndInfiniteEnabledRegressionPO")))) {
			writeTestResults("Verify that relevent Production Model is available",
					"Relevent Production Model should be available", "Relevent Production Model successfully available",
					"pass");
		} else {
			writeTestResults("Verify that relevent Production Model is available",
					"Relevent Production Model should be available", "Relevent Production Model not available", "fail");
		}

		doubleClick(td_resultProductionModelDocReplace.replace("doc_no",
				readProductionData("ProductionModeleMRPAndInfiniteEnabledRegressionPO")));
		Thread.sleep(2000);

		String selectedProducttionModel = getAttribute(txt_productionModelSummarySectionSummaryTabPO, "value");
		if (selectedProducttionModel.equals(readProductionData("ProductionModeleMRPAndInfiniteEnabledRegressionPO"))) {
			writeTestResults("Verify that user able to select the relevent Production Model",
					"User should be able to select the relevent Production Model",
					"User successfully select the relevent Production Model", "pass");
		} else {
			writeTestResults("Verify that user able to select the relevent Production Model",
					"User should be able to select the relevent Production Model",
					"User couldn't select the relevent Production Model", "fail");
		}

		String loadedProductionUnit = getAttribute(txt_productionUnitSummarySectionSummaryTabPO, "value");
		if (loadedProductionUnit.equals(prductionUnitAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that Production Unit auto loaded according to the Production Model in summary section",
					"Production Unit should be auto loaded according to the Production Model in summary section",
					"Production Unit successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Production Unit auto loaded according to the Production Model in summary section",
					"Production Unit should be auto loaded according to the Production Model in summary section",
					"Production Unit doesn't auto loaded according to the Production Model in summary section", "fail");
		}

		String loadedInputWare = getSelectedOptionInDropdown(drop_inputWarehouseSummarySectionSummaryTabPO);
		if (loadedInputWare.equals(inputWarehouseAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that Input Warehouse auto loaded according to the Production Model in summary section",
					"Input Warehouse should be auto loaded according to the Production Model in summary section",
					"Input Warehouse successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Input Warehouse auto loaded according to the Production Model in summary section",
					"Input Warehouse should be auto loaded according to the Production Model in summary section",
					"Input Warehouse doesn't auto loaded according to the Production Model in summary section", "fail");
		}

		String loadedOutputWare = getSelectedOptionInDropdown(drop_outputWarehouseSummarySectionSummaryTabPO);
		if (loadedOutputWare.equals(outputWarehouseAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that Output Warehouse auto loaded according to the Production Model in summary section",
					"Output Warehouse should be auto loaded according to the Production Model in summary section",
					"Output Warehouse successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Output Warehouse auto loaded according to the Production Model in summary section",
					"Output Warehouse should be auto loaded according to the Production Model in summary section",
					"Output Warehouse doesn't auto loaded according to the Production Model in summary section",
					"fail");
		}

		String loadedWIPWare = getSelectedOptionInDropdown(drop_WIPWarehouseSummarySectionSummaryTabPO);
		if (loadedWIPWare.equals(wipWarehouseAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that WIP Warehouse auto loaded according to the Production Model in summary section",
					"WIP Warehouse should be auto loaded according to the Production Model in summary section",
					"WIP Warehouse successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that WIP Warehouse auto loaded according to the Production Model in summary section",
					"WIP Warehouse should be auto loaded according to the Production Model in summary section",
					"WIP Warehouse doesn't auto loaded according to the Production Model in summary section", "fail");
		}

		String loadedSite = getSelectedOptionInDropdown(drop_siteSummarySectionSummaryTabPO);
		if (loadedSite.equals(siteAaccordigToPMRegression)) {
			writeTestResults("Verify that Site auto loaded according to the Production Model in summary section",
					"Site should be auto loaded according to the Production Model in summary section",
					"Site successfully auto loaded according to the Production Model in summary section", "pass");
		} else {
			writeTestResults("Verify that Site auto loaded according to the Production Model in summary section",
					"Site should be auto loaded according to the Production Model in summary section",
					"Site doesn't auto loaded according to the Production Model in summary section", "fail");
		}

		String loadedCostingPriority = getSelectedOptionInDropdown(
				drop_costingPriorityProductInformationSectionSummaryTabPO);
		if (loadedCostingPriority.equals(costingPriorityAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that Costing Priority auto loaded according to the Production Model in product information section",
					"Costing Priority should be auto loaded according to the Production Model in product information section",
					"Costing Priority successfully auto loaded according to the Production Model in product information section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Costing Priority auto loaded according to the Production Model in product information section",
					"Costing Priority should be auto loaded according to the Production Model in product information section",
					"Costing Priority doesn't auto loaded according to the Production Model in product information section",
					"fail");
		}

		String loadedBoM = getText(drop_boMBoMDetailsSectionSummaryTabPO);
		if (loadedBoM.equals("000095")) {
			writeTestResults(
					"Verify that BoM Document Number auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number should be auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number successfully auto loaded according to the Production Model in bill of material details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoM Document Number auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number should be auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number doesn't auto loaded according to the Production Model in bill of material details section",
					"fail");
		}

		String loadedBoMVarient = getText(div_boMVarientBoMDetailsSectionSummaryTabPO);
		if (loadedBoMVarient.equals(bomVarientAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that BoM Varient auto loaded according to the Production Model in bill of material details section",
					"BoM Varient should be auto loaded according to the Production Model in bill of material details section",
					"BoM Varient successfully auto loaded according to the Production Model in bill of material details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoM Varient auto loaded according to the Production Model in bill of material details section",
					"BoM Varient should be auto loaded according to the Production Model in bill of material details section",
					"BoM Varient doesn't auto loaded according to the Production Model in bill of material details section",
					"fail");
		}

		String loadedBoMVarientDescription = getText(div_varientDescriptionBoMDetailsSectionSummaryTabPO);
		if (loadedBoMVarientDescription.equals(varientDescriptionAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that BoM Varient Description auto loaded according to the Production Model in bill of material details section",
					"BoM Varient Description should be auto loaded according to the Production Model in bill of material details section",
					"BoM Varient Description successfully auto loaded according to the Production Model in bill of material details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoM Varient Description auto loaded according to the Production Model in bill of material details section",
					"BoM Varient Description should be auto loaded according to the Production Model in bill of material details section",
					"BoM Varient Description doesn't auto loaded according to the Production Model in bill of material details section",
					"fail");
		}

		String loadedBoO = getText(div_boOBoODetailsSectionSummaryTabPO);
		if (loadedBoO.equals(booNoAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that BoO Number auto loaded according to the Production Model in bill of operation details section",
					"BoO Number should be auto loaded according to the Production Model in bill of operation details section",
					"BoO Number successfully auto loaded according to the Production Model in bill of operation details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoO Number auto loaded according to the Production Model in bill of operation details section",
					"BoO Number should be auto loaded according to the Production Model in bill of operation details section",
					"BoO Number doesn't auto loaded according to the Production Model in bill of operation details section",
					"fail");
		}

		String loadedBoODescription = getText(div_boODescriptionDetailsSectionSummaryTabPO);
		if (loadedBoODescription.equals(booDescriptionAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that BoO Description auto loaded according to the Production Model in bill of operation details section",
					"BoO Description should be auto loaded according to the Production Model in bill of operation details section",
					"BoO Description successfully auto loaded according to the Production Model in bill of operation details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoO Description auto loaded according to the Production Model in bill of operation details section",
					"BoO Description should be auto loaded according to the Production Model in bill of operation details section",
					"BoO Description doesn't auto loaded according to the Production Model in bill of operation details section",
					"fail");
		}

		/*
		 * There is a bug. Supply production checkbox should be loaded as selected. Need
		 * to change after the bug was fixed
		 */
		if (!isSelected(chk_supplyToOperationPO)) {
			writeTestResults(
					"Verify that Supply to Production checkbox auto loaded according to the Production Model in bill of operation details section",
					"Supply to Production checkbox should be auto loaded according to the Production Model in bill of operation details section",
					"Supply to Production checkbox successfully auto loaded according to the Production Model in bill of operation details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Supply to Production checkbox auto loaded according to the Production Model in bill of operation details section",
					"Supply to Production checkbox should be auto loaded according to the Production Model in bill of operation details section",
					"Supply to Production checkbox doesn't auto loaded according to the Production Model in bill of operation details section",
					"fail");
		}

		String loadedRequestedStartDate = getAttribute(txt_requestStartDateSheduleSectionSummaryTabPO, "value");
		String currentDate = common.currentDate();
		if (loadedRequestedStartDate.equals(currentDate)) {
			writeTestResults("Verify that Reqested Start Date auto loaded in shedule section",
					"Reqested Start Date should be auto loaded in shedule section",
					"Reqested Start Date successfully auto loaded in shedule section", "pass");
		} else {
			writeTestResults("Verify that Reqested Start Date auto loaded in shedule section",
					"Reqested Start Date should be auto loaded in shedule section",
					"Reqested Start Date doesn't auto loaded in shedule section", "fail");
		}

		String loadedRequestedEndDate = getAttribute(txt_requestEndDateSheduleSectionSummaryTabPO, "value");
		if (loadedRequestedEndDate.equals(currentDate)) {
			writeTestResults("Verify that Reqested End Date auto loaded in shedule section",
					"Reqested End Date should be auto loaded in shedule section",
					"Reqested End Date successfully auto loaded in shedule section", "pass");
		} else {
			writeTestResults("Verify that Reqested End Date auto loaded in shedule section",
					"Reqested End Date should be auto loaded in shedule section",
					"Reqested End Date doesn't auto loaded in shedule section", "fail");
		}

		selectText(drop_prioritySummarySectionSummaryTabPO, productionOrderPriorityRegression);
		Thread.sleep(1500);
		String selectedPriority = getSelectedOptionInDropdown(drop_prioritySummarySectionSummaryTabPO);
		if (selectedPriority.equals(productionOrderPriorityRegression)) {
			writeTestResults("Verify that selected priority is display successfully",
					"Selected priority should be display successfully", "Selected priority display successfully",
					"pass");
		} else {
			writeTestResults("Verify that selected priority is display successfully",
					"Selected priority should be display successfully", "Selected priority not display", "fail");
		}

		driver.findElement(getLocator(txt_planedQtyProductInformationSectionSummaryTabPO)).click();
		driver.findElement(getLocator(txt_planedQtyProductInformationSectionSummaryTabPO))
				.sendKeys(planedQtyPORegression);
		Thread.sleep(1500);
		String enteredPlanedQty = getAttribute(txt_planedQtyProductInformationSectionSummaryTabPO, "value");
		if (enteredPlanedQty.equals(planedQtyPORegression)) {
			writeTestResults("Verify that entered Planed Qty is display successfully",
					"Entered Planed Qty should be display successfully", "Entered Planed Qty display successfully",
					"pass");
		} else {
			writeTestResults("Verify that entered Planed Qty is display successfully",
					"Entered Planed Qty should be display successfully", "Entered Planed Qty not display", "fail");
		}

		if (isSelected(chk_mrpProductionOrderRegression)) {
			writeTestResults("Verify that the system is auto enabled the MRP Enabled",
					"The system should be auto enabled the MRP Enabled",
					"The system successfully auto enabled the MRP Enabled", "pass");
		} else {
			writeTestResults("Verify that the system is auto enabled the MRP Enabled",
					"The system should be auto enabled the MRP Enabled",
					"The system doesn't auto enabled the MRP Enabled", "fail");
		}

		if (isSelected(chk_infinitePO)) {
			writeTestResults("Verify that the system is auto enabled the Infinite Enabled",
					"The system should be auto enabled the Infinite Enabled",
					"The system successfully auto enabled the Infinite Enabled", "pass");
		} else {
			writeTestResults("Verify that the system is auto enabled the Infinite Enabled",
					"The system should be auto enabled the Infinite Enabled",
					"The system doesn't auto enabled the Infinite Enabled", "fail");
		}
	}

	public void productionOrder_3_PROD_PO_048_049() throws Exception {
		selectText(drop_barcodeBookSummarySectionSummaryTabPO, barcodeBookPORegression);
		String selectedBarcodeBook = getSelectedOptionInDropdown(drop_barcodeBookSummarySectionSummaryTabPO);
		if (selectedBarcodeBook.equals(barcodeBookPORegression)) {
			writeTestResults("Verify that user able to select Barcode Book",
					"User should be able to select Barcode Book", "User able to select Barcode Book", "pass");
		} else {
			writeTestResults("Verify that user able to select Barcode Book",
					"User should be able to select Barcode Book", "User not able to select Barcode Book", "fail");
		}

		selectText(drop_productOrderGroupSummarySectionSummaryTabPO, productionOrderGroupRegression);
		Thread.sleep(1000);
		String selectedProductionGroup = getSelectedOptionInDropdown(drop_productOrderGroupSummarySectionSummaryTabPO);
		if (selectedProductionGroup.equals(productionOrderGroupRegression)) {
			writeTestResults("Verify user able to select the Production Group",
					"User should be able to select the Production Group",
					"User successfully select the Production Group", "pass");
		} else {
			writeTestResults("Verify user able to select the Production Group",
					"User should be able to select the Production Group", "User couldn't select the Production Group",
					"fail");
		}

		sendKeys(txt_descriptionSummarySectionSummaryTabPO, productionOrderDescriptionRegression);
		String enteredDescription = getAttribute(txt_descriptionSummarySectionSummaryTabPO, "value");
		if (enteredDescription.equals(productionOrderDescriptionRegression)) {
			writeTestResults("Verify user able to enter the Description",
					"User should be able to enter the Description", "User successfully enter the Description", "pass");
		} else {
			writeTestResults("Verify user able to enter the Description",
					"User should be able to enter the Description", "User couldn't enter the Description", "fail");
		}

		click(txt_requestStartDateSheduleSectionSummaryTabPO);
		explicitWaitUntillClickable(a_requestStartDatePOWhenDuplicating, 6);
		click(a_requestStartDatePOWhenDuplicating);
		Thread.sleep(2000);
		String selectedRequestedStartDate = getAttribute(txt_requestStartDateSheduleSectionSummaryTabPO, "value");
		String currentYearMonthWithselectedDate = common.currentDate().substring(0, 8) + "01";
		if (selectedRequestedStartDate.equals(currentYearMonthWithselectedDate)) {
			writeTestResults("Verify user able to select Requested Start Date",
					"User should be able to select Requested Start Date",
					"User successfully select Requested Start Date", "pass");
		} else {
			writeTestResults("Verify user able to select Requested Start Date",
					"User should be able to select Requested Start Date", "User couldn't select Requested Start Date",
					"fail");
		}

		click(txt_requestEndDateSheduleSectionSummaryTabPO);
		explicitWaitUntillClickable(a_requestEndDatePOWhenDuplicating, 6);
		click(a_requestEndDatePOWhenDuplicating);
		Thread.sleep(2000);
		String selectedRequestedEndDate = getAttribute(txt_requestEndDateSheduleSectionSummaryTabPO, "value");
		String currentYearMonthWithselectedEndDate = common.currentDate().substring(0, 8) + "28";
		if (selectedRequestedEndDate.equals(currentYearMonthWithselectedEndDate)) {
			writeTestResults("Verify user able to select Requested End Date",
					"User should be able to select Requested End Date", "User successfully select Requested End Date",
					"pass");
		} else {
			writeTestResults("Verify user able to select Requested End Date",
					"User should be able to select Requested End Date", "User couldn't select Requested End Date",
					"fail");
		}

		click(btn_draft);
		explicitWait(header_draftedProductionOrder, 60);
		if (isDisplayed(header_draftedProductionOrder)) {
			writeTestResults("Verify user able to draft the Production Order",
					"User should be able to draft the Production Order", "User successfully draft the Production Order",
					"pass");
		} else {
			writeTestResults("Verify user able to draft the Production Order",
					"User should be able to draft the Production Order", "User couldn't draft the Production Order",
					"fail");
		}

		click(btn_plusIconPO);
		Thread.sleep(2000);
		switchWindow();
		Thread.sleep(1000);
		explicitWait(header_newProductionOrder, 60);
		if (isDisplayed(header_newProductionOrder)) {
			writeTestResults(
					"Verify whether system is loading a new Production Order in another tab when user clicks on (+) in a drafted Production Order",
					"The system should be re direted the user to the new Production Order form",
					"The system successfully re direted the user to the new Production Order form", "pass");
		} else {
			writeTestResults(
					"Verify whether system is loading a new Production Order in another tab when user clicks on (+) in a drafted Production Order",
					"The system should be re direted the user to the new Production Order form",
					"The system doesn't re direted the user to the new Production Order form", "fail");
		}

		closeWindow();

		click(btn_release);
		explicitWait(header_releasedProductionOrder, 30);
		if (isDisplayed(header_releasedProductionOrder)) {
			writeTestResults("Verify user able to release the Production Order",
					"User should be able to release the Production Order",
					"User successfully release the Production Order", "pass");
		} else {
			writeTestResults("Verify user able to release the Production Order",
					"User should be able to release the Production Order", "User couldn't release the Production Order",
					"fail");
		}

		click(btn_plusIconPO);
		Thread.sleep(2000);
		switchWindow();
		Thread.sleep(1000);
		explicitWait(header_newProductionOrder, 60);
		if (isDisplayed(header_newProductionOrder)) {
			writeTestResults(
					"Verify whether system is loading a new Production Order in another tab when user clicks on (+) in a released Production Order",
					"The system should be re direted the user to the new Production Order form",
					"The system successfully re direted the user to the new Production Order form", "pass");
		} else {
			writeTestResults(
					"Verify whether system is loading a new Production Order in another tab when user clicks on (+) in a released Production Order",
					"The system should be re direted the user to the new Production Order form",
					"The system doesn't re direted the user to the new Production Order form", "fail");
		}
	}

	/* PROD_PO_050 */
	public void productionOrder_1_PROD_PO_050() throws Exception {
		loginBiletaAutomation();
		explicitWaitUntillClickable(img_navigationPane, 60);
		Thread.sleep(2000);
		explicitWaitUntillClickable(img_navigationPane, 20);
		if (click(img_navigationPane)) {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User successfully click navigation pane", "pass");
		} else {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User couldn't click navigation pane", "fail");
		}

		explicitWait(btn_production, 4);
		if (isEnabledQuickCheck(btn_production)) {
			writeTestResults("Verify user able to view the \"Production\" module under main navigation menu",
					"User should be able to view the \"Production\" module under main navigation menu",
					"User successfully view the \"Production\" module under main navigation menu", "pass");
		} else {
			writeTestResults("Verify user able to view the \"Production\" module under main navigation menu",
					"User should be able to view the \"Production\" module under main navigation menu",
					"User couldn't view the \"Production\" module under main navigation menu", "fail");
		}

		click(btn_production);

		explicitWait(btn_productionOrder, 4);
		if (isEnabledQuickCheck(btn_productionOrder)) {
			writeTestResults("Verify user able to view the \"Production Order\" option under production sub menu",
					"User should be able to view the \"Production Order\" option under production sub menu",
					"User able to view the \"Production Order\" option under production sub menu", "pass");
		} else {
			writeTestResults("Verify user able to view the \"Production Order\" option under production sub menu",
					"User should be able to view the \"Production Order\" option under production sub menu",
					"User couldn't view the \"Production Order\" option under production sub menu", "pass");
		}

		click(btn_productionOrder);

		explicitWait(header_productionOrderByPage, 60);
		if (isDisplayed(header_productionOrderByPage)) {
			writeTestResults("Verify user able to navigate to Production Order by-page",
					"User should be able to navigate to Production Order by-page",
					"User successfully navigate to Production Order by-page", "pass");
		} else {
			writeTestResults("Verify user able to navigate to Production Order by-page",
					"User should be able to navigate to Production Order by-page",
					"User coulsn't navigate to Production Order by-page", "fail");
		}

		click(btn_newProductionOrder);
		explicitWait(header_newProductionOrder, 60);
		if (isDisplayed(header_newProductionOrder)) {
			writeTestResults("Verify user able to navigate to Production Order new-form",
					"User should be able to navigate to Production Order new-form",
					"User successfully navigate to Production Order new-form", "pass");
		} else {
			writeTestResults("Verify user able to navigate to Production Order new-form",
					"User should be able to navigate to Production Order new-form",
					"User coulsn't navigate to Production Order new-form", "fail");
		}

		click(span_productLookupPO);
		explicitWait(lookup_product, 30);
		if (isDisplayed(lookup_product)) {
			writeTestResults("Verify that product lookup is open", "Product lookup should be open",
					"Product lookup successfully open", "pass");
		} else {
			writeTestResults("Verify that product lookup is open", "Product lookup should be open",
					"Product lookup doesn't open", "fail");
		}

		sendKeys(txt_searchProductProductLookup, readProductionData("ProductionTypeProductRegressionPO"));
		String enteredProduct = getAttribute(txt_searchProductProductLookup, "value");
		if (enteredProduct.equals(readProductionData("ProductionTypeProductRegressionPO"))) {
			writeTestResults("Verify that user able to enter the relevent product",
					"User should be able to enter the relevent product", "User successfully enter the relevent product",
					"pass");
		} else {
			writeTestResults("Verify that user able to enter the relevent product",
					"User should be able to enter the relevent product", "User doesn't enter the relevent product",
					"fail");
		}

		click(btn_searchProductProductLookup);
		explicitWaitUntillClickable(td_resultProductRegressionPO.replace("product_code",
				readProductionData("ProductionTypeProductRegressionPO")), 60);
		if (isEnabled(td_resultProductRegressionPO.replace("product_code",
				readProductionData("ProductionTypeProductRegressionPO")))) {
			writeTestResults("Verify that relevent product is available", "Relevent product should be available",
					"Relevent product successfully available", "pass");
		} else {
			writeTestResults("Verify that relevent product is available", "Relevent product should be available",
					"Relevent product not available", "fail");
		}

		doubleClick(td_resultProductRegressionPO.replace("product_code",
				readProductionData("ProductionTypeProductRegressionPO")));
		Thread.sleep(2000);

		String selectedProduct = getAttribute(txt_outputProductProductInformationSectionSummaryTabPO, "value");
		if (selectedProduct.equals(selectedProductFrontPageRegressionPO.replace("product_code",
				readProductionData("ProductionTypeProductRegressionPO")))) {
			writeTestResults("Verify that user able to select the relevent product",
					"User should be able to select the relevent product",
					"User successfully select the relevent product", "pass");
		} else {
			writeTestResults("Verify that user able to select the relevent product",
					"User should be able to select the relevent product", "User couldn't select the relevent product",
					"fail");
		}
	}

	public void productionOrder_2_PROD_PO_050() throws Exception {
		click(btn_productionModelLookupPO);
		explicitWait(lookup_productionModel, 30);
		if (isDisplayed(lookup_productionModel)) {
			writeTestResults("Verify that Production Model lookup is open", "Production Model lookup should be open",
					"Production Model lookup successfully open", "pass");
		} else {
			writeTestResults("Verify that Production Model lookup is open", "Production Model lookup should be open",
					"Production Model lookup doesn't open", "fail");
		}

		sendKeys(txt_searchProductionModel, readProductionData("ProductionModeleMRPEnabledRegressionPO"));
		String enteredProductionModel = getAttribute(txt_searchProductionModel, "value");
		if (enteredProductionModel.equals(readProductionData("ProductionModeleMRPEnabledRegressionPO"))) {
			writeTestResults("Verify that user able to enter the relevent Production Model",
					"User should be able to enter the relevent Production Model",
					"User successfully enter the relevent Production Model", "pass");
		} else {
			writeTestResults("Verify that user able to enter the relevent Production Model",
					"User should be able to enter the relevent Production Model",
					"User doesn't enter the relevent Production Model", "fail");
		}

		click(btn_searchProductionModel);
		explicitWaitUntillClickable(td_resultProductionModelDocReplace.replace("doc_no",
				readProductionData("ProductionModeleMRPEnabledRegressionPO")), 60);
		if (isEnabled(td_resultProductionModelDocReplace.replace("doc_no",
				readProductionData("ProductionModeleMRPEnabledRegressionPO")))) {
			writeTestResults("Verify that relevent Production Model is available",
					"Relevent Production Model should be available", "Relevent Production Model successfully available",
					"pass");
		} else {
			writeTestResults("Verify that relevent Production Model is available",
					"Relevent Production Model should be available", "Relevent Production Model not available", "fail");
		}

		doubleClick(td_resultProductionModelDocReplace.replace("doc_no",
				readProductionData("ProductionModeleMRPEnabledRegressionPO")));
		Thread.sleep(2000);

		String selectedProducttionModel = getAttribute(txt_productionModelSummarySectionSummaryTabPO, "value");
		if (selectedProducttionModel.equals(readProductionData("ProductionModeleMRPEnabledRegressionPO"))) {
			writeTestResults("Verify that user able to select the relevent Production Model",
					"User should be able to select the relevent Production Model",
					"User successfully select the relevent Production Model", "pass");
		} else {
			writeTestResults("Verify that user able to select the relevent Production Model",
					"User should be able to select the relevent Production Model",
					"User couldn't select the relevent Production Model", "fail");
		}

		String loadedProductionUnit = getAttribute(txt_productionUnitSummarySectionSummaryTabPO, "value");
		if (loadedProductionUnit.equals(prductionUnitAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that Production Unit auto loaded according to the Production Model in summary section",
					"Production Unit should be auto loaded according to the Production Model in summary section",
					"Production Unit successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Production Unit auto loaded according to the Production Model in summary section",
					"Production Unit should be auto loaded according to the Production Model in summary section",
					"Production Unit doesn't auto loaded according to the Production Model in summary section", "fail");
		}

		String loadedInputWare = getSelectedOptionInDropdown(drop_inputWarehouseSummarySectionSummaryTabPO);
		if (loadedInputWare.equals(inputWarehouseAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that Input Warehouse auto loaded according to the Production Model in summary section",
					"Input Warehouse should be auto loaded according to the Production Model in summary section",
					"Input Warehouse successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Input Warehouse auto loaded according to the Production Model in summary section",
					"Input Warehouse should be auto loaded according to the Production Model in summary section",
					"Input Warehouse doesn't auto loaded according to the Production Model in summary section", "fail");
		}

		String loadedOutputWare = getSelectedOptionInDropdown(drop_outputWarehouseSummarySectionSummaryTabPO);
		if (loadedOutputWare.equals(outputWarehouseAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that Output Warehouse auto loaded according to the Production Model in summary section",
					"Output Warehouse should be auto loaded according to the Production Model in summary section",
					"Output Warehouse successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Output Warehouse auto loaded according to the Production Model in summary section",
					"Output Warehouse should be auto loaded according to the Production Model in summary section",
					"Output Warehouse doesn't auto loaded according to the Production Model in summary section",
					"fail");
		}

		String loadedWIPWare = getSelectedOptionInDropdown(drop_WIPWarehouseSummarySectionSummaryTabPO);
		if (loadedWIPWare.equals(wipWarehouseAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that WIP Warehouse auto loaded according to the Production Model in summary section",
					"WIP Warehouse should be auto loaded according to the Production Model in summary section",
					"WIP Warehouse successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that WIP Warehouse auto loaded according to the Production Model in summary section",
					"WIP Warehouse should be auto loaded according to the Production Model in summary section",
					"WIP Warehouse doesn't auto loaded according to the Production Model in summary section", "fail");
		}

		String loadedSite = getSelectedOptionInDropdown(drop_siteSummarySectionSummaryTabPO);
		if (loadedSite.equals(siteAaccordigToPMRegression)) {
			writeTestResults("Verify that Site auto loaded according to the Production Model in summary section",
					"Site should be auto loaded according to the Production Model in summary section",
					"Site successfully auto loaded according to the Production Model in summary section", "pass");
		} else {
			writeTestResults("Verify that Site auto loaded according to the Production Model in summary section",
					"Site should be auto loaded according to the Production Model in summary section",
					"Site doesn't auto loaded according to the Production Model in summary section", "fail");
		}

		String loadedCostingPriority = getSelectedOptionInDropdown(
				drop_costingPriorityProductInformationSectionSummaryTabPO);
		if (loadedCostingPriority.equals(costingPriorityAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that Costing Priority auto loaded according to the Production Model in product information section",
					"Costing Priority should be auto loaded according to the Production Model in product information section",
					"Costing Priority successfully auto loaded according to the Production Model in product information section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Costing Priority auto loaded according to the Production Model in product information section",
					"Costing Priority should be auto loaded according to the Production Model in product information section",
					"Costing Priority doesn't auto loaded according to the Production Model in product information section",
					"fail");
		}

		String loadedBoM = getText(drop_boMBoMDetailsSectionSummaryTabPO);
		if (loadedBoM.equals("000095")) {
			writeTestResults(
					"Verify that BoM Document Number auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number should be auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number successfully auto loaded according to the Production Model in bill of material details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoM Document Number auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number should be auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number doesn't auto loaded according to the Production Model in bill of material details section",
					"fail");
		}

		String loadedBoMVarient = getText(div_boMVarientBoMDetailsSectionSummaryTabPO);
		if (loadedBoMVarient.equals(bomVarientAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that BoM Varient auto loaded according to the Production Model in bill of material details section",
					"BoM Varient should be auto loaded according to the Production Model in bill of material details section",
					"BoM Varient successfully auto loaded according to the Production Model in bill of material details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoM Varient auto loaded according to the Production Model in bill of material details section",
					"BoM Varient should be auto loaded according to the Production Model in bill of material details section",
					"BoM Varient doesn't auto loaded according to the Production Model in bill of material details section",
					"fail");
		}

		String loadedBoMVarientDescription = getText(div_varientDescriptionBoMDetailsSectionSummaryTabPO);
		if (loadedBoMVarientDescription.equals(varientDescriptionAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that BoM Varient Description auto loaded according to the Production Model in bill of material details section",
					"BoM Varient Description should be auto loaded according to the Production Model in bill of material details section",
					"BoM Varient Description successfully auto loaded according to the Production Model in bill of material details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoM Varient Description auto loaded according to the Production Model in bill of material details section",
					"BoM Varient Description should be auto loaded according to the Production Model in bill of material details section",
					"BoM Varient Description doesn't auto loaded according to the Production Model in bill of material details section",
					"fail");
		}

		String loadedBoO = getText(div_boOBoODetailsSectionSummaryTabPO);
		if (loadedBoO.equals(booNoAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that BoO Number auto loaded according to the Production Model in bill of operation details section",
					"BoO Number should be auto loaded according to the Production Model in bill of operation details section",
					"BoO Number successfully auto loaded according to the Production Model in bill of operation details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoO Number auto loaded according to the Production Model in bill of operation details section",
					"BoO Number should be auto loaded according to the Production Model in bill of operation details section",
					"BoO Number doesn't auto loaded according to the Production Model in bill of operation details section",
					"fail");
		}

		String loadedBoODescription = getText(div_boODescriptionDetailsSectionSummaryTabPO);
		if (loadedBoODescription.equals(booDescriptionAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that BoO Description auto loaded according to the Production Model in bill of operation details section",
					"BoO Description should be auto loaded according to the Production Model in bill of operation details section",
					"BoO Description successfully auto loaded according to the Production Model in bill of operation details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoO Description auto loaded according to the Production Model in bill of operation details section",
					"BoO Description should be auto loaded according to the Production Model in bill of operation details section",
					"BoO Description doesn't auto loaded according to the Production Model in bill of operation details section",
					"fail");
		}

		/*
		 * There is a bug. Supply production checkbox should be loaded as selected. Need
		 * to change after the bug was fixed
		 */
		if (!isSelected(chk_supplyToOperationPO)) {
			writeTestResults(
					"Verify that Supply to Production checkbox auto loaded according to the Production Model in bill of operation details section",
					"Supply to Production checkbox should be auto loaded according to the Production Model in bill of operation details section",
					"Supply to Production checkbox successfully auto loaded according to the Production Model in bill of operation details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Supply to Production checkbox auto loaded according to the Production Model in bill of operation details section",
					"Supply to Production checkbox should be auto loaded according to the Production Model in bill of operation details section",
					"Supply to Production checkbox doesn't auto loaded according to the Production Model in bill of operation details section",
					"fail");
		}

		String loadedRequestedStartDate = getAttribute(txt_requestStartDateSheduleSectionSummaryTabPO, "value");
		String currentDate = common.currentDate();
		if (loadedRequestedStartDate.equals(currentDate)) {
			writeTestResults("Verify that Reqested Start Date auto loaded in shedule section",
					"Reqested Start Date should be auto loaded in shedule section",
					"Reqested Start Date successfully auto loaded in shedule section", "pass");
		} else {
			writeTestResults("Verify that Reqested Start Date auto loaded in shedule section",
					"Reqested Start Date should be auto loaded in shedule section",
					"Reqested Start Date doesn't auto loaded in shedule section", "fail");
		}

		String loadedRequestedEndDate = getAttribute(txt_requestEndDateSheduleSectionSummaryTabPO, "value");
		if (loadedRequestedEndDate.equals(currentDate)) {
			writeTestResults("Verify that Reqested End Date auto loaded in shedule section",
					"Reqested End Date should be auto loaded in shedule section",
					"Reqested End Date successfully auto loaded in shedule section", "pass");
		} else {
			writeTestResults("Verify that Reqested End Date auto loaded in shedule section",
					"Reqested End Date should be auto loaded in shedule section",
					"Reqested End Date doesn't auto loaded in shedule section", "fail");
		}

		selectText(drop_prioritySummarySectionSummaryTabPO, productionOrderPriorityRegression);
		Thread.sleep(1500);
		String selectedPriority = getSelectedOptionInDropdown(drop_prioritySummarySectionSummaryTabPO);
		if (selectedPriority.equals(productionOrderPriorityRegression)) {
			writeTestResults("Verify that selected priority is display successfully",
					"Selected priority should be display successfully", "Selected priority display successfully",
					"pass");
		} else {
			writeTestResults("Verify that selected priority is display successfully",
					"Selected priority should be display successfully", "Selected priority not display", "fail");
		}

		driver.findElement(getLocator(txt_planedQtyProductInformationSectionSummaryTabPO)).click();
		driver.findElement(getLocator(txt_planedQtyProductInformationSectionSummaryTabPO))
				.sendKeys(planedQtyPORegression);
		Thread.sleep(1500);
		String enteredPlanedQty = getAttribute(txt_planedQtyProductInformationSectionSummaryTabPO, "value");
		if (enteredPlanedQty.equals(planedQtyPORegression)) {
			writeTestResults("Verify that entered Planed Qty is display successfully",
					"Entered Planed Qty should be display successfully", "Entered Planed Qty display successfully",
					"pass");
		} else {
			writeTestResults("Verify that entered Planed Qty is display successfully",
					"Entered Planed Qty should be display successfully", "Entered Planed Qty not display", "fail");
		}

		if (isSelected(chk_mrpProductionOrderRegression)) {
			writeTestResults("Verify that the system is auto enabled the MRP Enabled",
					"The system should be auto enabled the MRP Enabled",
					"The system successfully auto enabled the MRP Enabled", "pass");
		} else {
			writeTestResults("Verify that the system is auto enabled the MRP Enabled",
					"The system should be auto enabled the MRP Enabled",
					"The system doesn't auto enabled the MRP Enabled", "fail");
		}

	}

	public void productionOrder_3_PROD_PO_050() throws Exception {
		selectText(drop_barcodeBookSummarySectionSummaryTabPO, barcodeBookPORegression);
		String selectedBarcodeBook = getSelectedOptionInDropdown(drop_barcodeBookSummarySectionSummaryTabPO);
		if (selectedBarcodeBook.equals(barcodeBookPORegression)) {
			writeTestResults("Verify that user able to select Barcode Book",
					"User should be able to select Barcode Book", "User able to select Barcode Book", "pass");
		} else {
			writeTestResults("Verify that user able to select Barcode Book",
					"User should be able to select Barcode Book", "User not able to select Barcode Book", "fail");
		}

		selectText(drop_productOrderGroupSummarySectionSummaryTabPO, productionOrderGroupRegression);
		Thread.sleep(1000);
		String selectedProductionGroup = getSelectedOptionInDropdown(drop_productOrderGroupSummarySectionSummaryTabPO);
		if (selectedProductionGroup.equals(productionOrderGroupRegression)) {
			writeTestResults("Verify user able to select the Production Group",
					"User should be able to select the Production Group",
					"User successfully select the Production Group", "pass");
		} else {
			writeTestResults("Verify user able to select the Production Group",
					"User should be able to select the Production Group", "User couldn't select the Production Group",
					"fail");
		}

		sendKeys(txt_descriptionSummarySectionSummaryTabPO, productionOrderDescriptionRegression);
		String enteredDescription = getAttribute(txt_descriptionSummarySectionSummaryTabPO, "value");
		if (enteredDescription.equals(productionOrderDescriptionRegression)) {
			writeTestResults("Verify user able to enter the Description",
					"User should be able to enter the Description", "User successfully enter the Description", "pass");
		} else {
			writeTestResults("Verify user able to enter the Description",
					"User should be able to enter the Description", "User couldn't enter the Description", "fail");
		}

		click(txt_requestStartDateSheduleSectionSummaryTabPO);
		explicitWaitUntillClickable(a_requestStartDatePOWhenDuplicating, 6);
		click(a_requestStartDatePOWhenDuplicating);
		Thread.sleep(2000);
		String selectedRequestedStartDate = getAttribute(txt_requestStartDateSheduleSectionSummaryTabPO, "value");
		String currentYearMonthWithselectedDate = common.currentDate().substring(0, 8) + "01";
		if (selectedRequestedStartDate.equals(currentYearMonthWithselectedDate)) {
			writeTestResults("Verify user able to select Requested Start Date",
					"User should be able to select Requested Start Date",
					"User successfully select Requested Start Date", "pass");
		} else {
			writeTestResults("Verify user able to select Requested Start Date",
					"User should be able to select Requested Start Date", "User couldn't select Requested Start Date",
					"fail");
		}

		click(txt_requestEndDateSheduleSectionSummaryTabPO);
		explicitWaitUntillClickable(a_requestEndDatePOWhenDuplicating, 6);
		click(a_requestEndDatePOWhenDuplicating);
		Thread.sleep(2000);
		String selectedRequestedEndDate = getAttribute(txt_requestEndDateSheduleSectionSummaryTabPO, "value");
		String currentYearMonthWithselectedEndDate = common.currentDate().substring(0, 8) + "28";
		if (selectedRequestedEndDate.equals(currentYearMonthWithselectedEndDate)) {
			writeTestResults("Verify user able to select Requested End Date",
					"User should be able to select Requested End Date", "User successfully select Requested End Date",
					"pass");
		} else {
			writeTestResults("Verify user able to select Requested End Date",
					"User should be able to select Requested End Date", "User couldn't select Requested End Date",
					"fail");
		}

		click(btn_draft);
		trackCode = getText(lbl_docNo);
		draftedProductionOrder = trackCode;
		explicitWait(header_draftedProductionOrder, 60);
		if (isDisplayed(header_draftedProductionOrder)) {
			writeTestResults("Verify user able to draft the Production Order",
					"User should be able to draft the Production Order", "User successfully draft the Production Order",
					"pass");
		} else {
			writeTestResults("Verify user able to draft the Production Order",
					"User should be able to draft the Production Order", "User couldn't draft the Production Order",
					"fail");
		}

		explicitWaitUntillClickable(img_navigationPane, 60);
		Thread.sleep(2000);
		explicitWaitUntillClickable(img_navigationPane, 20);
		if (click(img_navigationPane)) {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User successfully click navigation pane", "pass");
		} else {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User couldn't click navigation pane", "fail");
		}

		explicitWait(btn_production, 4);
		if (isEnabledQuickCheck(btn_production)) {
			writeTestResults("Verify user able to view the \"Production\" module under main navigation menu",
					"User should be able to view the \"Production\" module under main navigation menu",
					"User successfully view the \"Production\" module under main navigation menu", "pass");
		} else {
			writeTestResults("Verify user able to view the \"Production\" module under main navigation menu",
					"User should be able to view the \"Production\" module under main navigation menu",
					"User couldn't view the \"Production\" module under main navigation menu", "fail");
		}

		click(btn_production);

		explicitWait(btn_productionOrder, 4);
		if (isEnabledQuickCheck(btn_productionOrder)) {
			writeTestResults("Verify user able to view the \"Production Order\" option under production sub menu",
					"User should be able to view the \"Production Order\" option under production sub menu",
					"User able to view the \"Production Order\" option under production sub menu", "pass");
		} else {
			writeTestResults("Verify user able to view the \"Production Order\" option under production sub menu",
					"User should be able to view the \"Production Order\" option under production sub menu",
					"User couldn't view the \"Production Order\" option under production sub menu", "pass");
		}

		click(btn_productionOrder);

		explicitWait(header_productionOrderByPage, 60);
		if (isDisplayed(header_productionOrderByPage)) {
			writeTestResults("Verify user able to navigate to Production Order by-page",
					"User should be able to navigate to Production Order by-page",
					"User successfully navigate to Production Order by-page", "pass");
		} else {
			writeTestResults("Verify user able to navigate to Production Order by-page",
					"User should be able to navigate to Production Order by-page",
					"User coulsn't navigate to Production Order by-page", "fail");
		}

		click(btn_newProductionOrder);
		explicitWait(header_newProductionOrder, 60);
		if (isDisplayed(header_newProductionOrder)) {
			writeTestResults("Verify user able to navigate to Production Order new-form",
					"User should be able to navigate to Production Order new-form",
					"User successfully navigate to Production Order new-form", "pass");
		} else {
			writeTestResults("Verify user able to navigate to Production Order new-form",
					"User should be able to navigate to Production Order new-form",
					"User coulsn't navigate to Production Order new-form", "fail");
		}

		click(btn_copyFrom);
		Thread.sleep(1000);
		explicitWait(header_loockupProductionOrder, 40);
		if (isDisplayed(header_loockupProductionOrder)) {
			writeTestResults("Verify user able to view Production Order lookup",
					"User should be able to view Production Order lookup",
					"User successfully view Production Order lookup", "pass");
		} else {
			writeTestResults("Verify user able to view Production Order lookup",
					"User should be able to view Production Order lookup", "User doesn't view Production Order lookup",
					"fail");
		}

		sendKeys(txt_searchProductionOrder2, draftedProductionOrder);
		pressEnter(txt_searchProductionOrder2);

		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", draftedProductionOrder), 15);

		if (isDisplayed(lnk_inforOnLookupInformationReplace.replace("info", draftedProductionOrder))) {
			writeTestResults(
					"Verify relevent existing Production Order was loaded and user allow to select the particular Production Order",
					"Relevent existing Production Order should be loaded and user should allow to select the particular Production Order",
					"Relevent existing Production Order successfully loaded and user allow to select the particular Production Order",
					"pass");
		} else {
			writeTestResults(
					"Verify relevent existing Production Order was loaded and user allow to select the particular Production Order",
					"Relevent existing Production Order should be loaded and user should allow to select the particular Production Order",
					"Relevent existing Production Order doesn't loaded and user not allow to select the particular Production Order",
					"fail");
		}

		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", draftedProductionOrder));

		explicitWaitUntillClickable(drop_productOrderGroupSummarySectionSummaryTabPO, 60);
		String loadedProductionGroup = getSelectedOptionInDropdown(drop_productOrderGroupSummarySectionSummaryTabPO);
		if (loadedProductionGroup.equals(productionOrderGroupRegression)) {
			writeTestResults("Verify that Production Group is available same as source document",
					"Production Group available same as source document",
					"Production Group successfully available same as source document", "pass");
		} else {
			writeTestResults("Verify that Production Group is available same as source document",
					"Production Group available same as source document",
					"Production Group doesn't available same as source document", "fail");
		}

		String loadedDescription = getAttribute(txt_descriptionSummarySectionSummaryTabPO, "value");
		if (loadedDescription.equals(productionOrderDescriptionRegression)) {
			writeTestResults("Verify that Description is available same as source document",
					"Description available same as source document",
					"Description successfully available same as source document", "pass");
		} else {
			writeTestResults("Verify that Description is available same as source document",
					"Description available same as source document",
					"Description doesn't available same as source document", "fail");
		}

		String loadedProducttionModel = getAttribute(txt_productionModelSummarySectionSummaryTabPO, "value");
		if (loadedProducttionModel.equals(readProductionData("ProductionModeleMRPEnabledRegressionPO"))) {
			writeTestResults("Verify that Production Model is available same as source document",
					"Production Model available same as source document",
					"Production Model successfully available same as source document", "pass");
		} else {
			writeTestResults("Verify that Production Model is available same as source document",
					"Production Model available same as source document",
					"Production Model doesn't available same as source document", "fail");
		}

		String loadedBarcodeBook = getSelectedOptionInDropdown(drop_barcodeBookSummarySectionSummaryTabPO);
		if (loadedBarcodeBook.equals(barcodeBookPORegression)) {
			writeTestResults("Verify that Barcode Book is available same as source document",
					"Barcode Book available same as source document",
					"Barcode Book successfully available same as source document", "pass");
		} else {
			writeTestResults("Verify that Barcode Book is available same as source document",
					"Barcode Book available same as source document",
					"Barcode Book doesn't available same as source document", "fail");
		}

		String loadedPriority = getSelectedOptionInDropdown(drop_prioritySummarySectionSummaryTabPO);
		if (loadedPriority.equals(productionOrderPriorityRegression)) {
			writeTestResults("Verify that Priority is available same as source document",
					"Priority available same as source document",
					"Priority successfully available same as source document", "pass");
		} else {
			writeTestResults("Verify that Priority is available same as source document",
					"Priority available same as source document", "Priority doesn't available same as source document",
					"fail");
		}

		String loadedProductionUnitCopyFromDocument = getAttribute(txt_productionUnitSummarySectionSummaryTabPO,
				"value");
		if (loadedProductionUnitCopyFromDocument.equals(productionUnitRegressionPOCopyFrom)) {
			writeTestResults("Verify that Production Unit is available same as source document",
					"Production Unit available same as source document",
					"Production Unit successfully available same as source document", "pass");
		} else {
			writeTestResults("Verify that Production Unit is available same as source document",
					"Production Unit available same as source document",
					"Production Unit doesn't available same as source document", "fail");
		}

		String loadedInputWareCopyFrom = getSelectedOptionInDropdown(drop_inputWarehouseSummarySectionSummaryTabPO);
		if (loadedInputWareCopyFrom.equals(inputWarehouseRegressionPOCopyFrom)) {
			writeTestResults("Verify that Input Warehouse is available same as source document",
					"Input Warehouse available same as source document",
					"Input Warehouse successfully available same as source document", "pass");
		} else {
			writeTestResults("Verify that Input Warehouse is available same as source document",
					"Input Warehouse available same as source document",
					"Input Warehouse doesn't available same as source document", "fail");
		}

		String loadedOutputWareDuplicated = getSelectedOptionInDropdown(drop_outputWarehouseSummarySectionSummaryTabPO);
		if (loadedOutputWareDuplicated.equals(outputWarehouseRegressionPOCopyFrom)) {
			writeTestResults("Verify that Output Warehouse is available same as source document",
					"Output Warehouse available same as source document",
					"Output Warehouse successfully available same as source document", "pass");
		} else {
			writeTestResults("Verify that Output Warehouse is available same as source document",
					"Output Warehouse available same as source document",
					"Output Warehouse doesn't available same as source document", "fail");
		}

		String loadedWIPWareCopyFrom = getSelectedOptionInDropdown(drop_WIPWarehouseSummarySectionSummaryTabPO);
		if (loadedWIPWareCopyFrom.equals(wipWarehouseRegressionPOCopyFrom)) {
			writeTestResults("Verify that WIP Warehouse is available same as source document",
					"WIP Warehouse available same as source document",
					"WIP Warehouse successfully available same as source document", "pass");
		} else {
			writeTestResults("Verify that WIP Warehouse is available same as source document",
					"WIP Warehouse available same as source document",
					"WIP Warehouse doesn't available same as source document", "fail");
		}

		String loadedSiteCopyFrom = getSelectedOptionInDropdown(drop_siteSummarySectionSummaryTabPO);
		if (loadedSiteCopyFrom.equals(siteRegressionPOCopyFrom)) {
			writeTestResults("Verify that Site is available same as source document",
					"Site available same as source document", "Site successfully available same as source document",
					"pass");
		} else {
			writeTestResults("Verify that Site is available same as source document",
					"Site available same as source document", "Site doesn't available same as source document", "fail");
		}

		String selectedProduct = getAttribute(txt_outputProductProductInformationSectionSummaryTabPO, "value");
		if (selectedProduct.equals(selectedProductFrontPageRegressionPO.replace("product_code",
				readProductionData("ProductionTypeProductRegressionPO")))) {
			writeTestResults("Verify that Output Product is available same as source document",
					"Output Product available same as source document",
					"Output Product successfully available same as source document", "pass");
		} else {
			writeTestResults("Verify that Output Product is available same as source document",
					"Output Product available same as source document",
					"Output Product doesn't available same as source document", "fail");
		}

		String loadedCostingPriorityCopyFrom = getSelectedOptionInDropdown(
				drop_costingPriorityProductInformationSectionSummaryTabPO);
		if (loadedCostingPriorityCopyFrom.equals(costingPriorityRegressionPOCopyFrom)) {
			writeTestResults("Verify that Costing Priority is available same as source document",
					"Costing Priority available same as source document",
					"Costing Priority successfully available same as source document", "pass");
		} else {
			writeTestResults("Verify that Costing Priority is available same as source document",
					"Costing Priority available same as source document",
					"Costing Priority doesn't available same as source document", "fail");
		}

		if (isSelected(chk_mrpProductionOrderRegression)) {
			writeTestResults("Verify that MRP Enabled checkbox is enabled same as source document",
					"MRP Enabled checkbox enabled same as source document",
					"MRP Enabled checkbox successfully enabled same as source document", "pass");
		} else {
			writeTestResults("Verify that MRP Enabled checkbox is enabled same as source document",
					"MRP Enabled checkbox enabled same as source document",
					"MRP Enabled checkbox doesn't enabled same as source document", "fail");
		}

		String loadedBoMCopyFrom = getText(drop_boMBoMDetailsSectionSummaryTabPO);
		if (loadedBoMCopyFrom.equals("000095")) {
			writeTestResults("Verify that BoM is available same as source document",
					"BoM available same as source document", "BoM successfully available same as source document",
					"pass");
		} else {
			writeTestResults("Verify that BoM is available same as source document",
					"BoM available same as source document", "BoM doesn't available same as source document", "fail");
		}

		String loadedBoOCopyFrom = getText(div_boOBoODetailsSectionSummaryTabPO);
		if (loadedBoOCopyFrom.equals(booRegressionPODupliacted)) {
			writeTestResults("Verify that BoO is available same as source document",
					"BoO available same as source document", "BoO successfully available same as source document",
					"pass");
		} else {
			writeTestResults("Verify that BoO is available same as source document",
					"BoO available same as source document", "BoO doesn't available same as source document", "fail");
		}

		/*
		 * There is a bug at initial steps. User not able to auto enable Supply to
		 * Production checkbox by the Production Model. Need to correct after bu was
		 * fixed
		 */

		if (!isSelected(chk_supplyToProductionPO)) {
			writeTestResults("Verify that Supply to Production checkbox is not disabled same as source document",
					"Supply to Production checkbox not disabled same as source document",
					"Supply to Production checkbox successfully not disabled same as source document", "pass");
		} else {
			writeTestResults("Verify that Supply to Production checkbox is not disabled same as source document",
					"Supply to Production checkbox not disabled same as source document",
					"Supply to Production checkbox doesn't not disabled same as source document", "fail");
		}

		String loadedRequestedEndDateCopyFrom = getAttribute(txt_requestEndDateSheduleSectionSummaryTabPO, "value");
		String currentYearMonthWithselectedEndDateCopyFrom = common.currentDate().substring(0, 8) + "28";
		if (loadedRequestedEndDateCopyFrom.equals(currentYearMonthWithselectedEndDateCopyFrom)) {
			writeTestResults("Verify that Reqested End Date auto loaded as source document",
					"Reqested End Date should be auto loaded as source document",
					"Reqested End Date successfully auto loaded as source document", "pass");
		} else {
			writeTestResults("Verify that Reqested End Date auto loaded as source document",
					"Reqested End Date should be auto loaded as source document",
					"Reqested End Date doesn't auto loaded as source document", "fail");
		}

		sendKeys(txt_descriptionSummarySectionSummaryTabPO, descriptionCopyFromPORegression);
		String enteredDescriptionDuplicated = getAttribute(txt_descriptionSummarySectionSummaryTabPO, "value");
		if (enteredDescriptionDuplicated.equals(descriptionCopyFromPORegression)) {
			writeTestResults("Verify user able to change the Description",
					"User should be able to change the Description", "User successfully change the Description",
					"pass");
		} else {
			writeTestResults("Verify user able to change the Description",
					"User should be able to change the Description", "User couldn't change the Description", "fail");
		}

		selectText(drop_prioritySummarySectionSummaryTabPO, priorityCopyFromPORegression);
		String selectedPriorityDuplicated = getSelectedOptionInDropdown(drop_prioritySummarySectionSummaryTabPO);
		if (selectedPriorityDuplicated.equals(priorityCopyFromPORegression)) {
			writeTestResults("Verify user able to change the Priority", "User should be able to change the Priority",
					"User successfully change the Priority", "pass");
		} else {
			writeTestResults("Verify user able to change the Priority", "User should be able to change the Priority",
					"User couldn't change the Priority", "fail");
		}

		driver.findElement(getLocator(txt_planedQtyProductInformationSectionSummaryTabPO)).click();
		driver.findElement(getLocator(txt_planedQtyProductInformationSectionSummaryTabPO))
				.sendKeys(planedQtyPORegression);
		Thread.sleep(1500);
		String enteredPlanedQtyDuplicated = getAttribute(txt_planedQtyProductInformationSectionSummaryTabPO, "value");
		if (enteredPlanedQtyDuplicated.equals(planedQtyPORegression)) {
			writeTestResults("Verify that entered Planed Qty is display successfully",
					"Entered Planed Qty should be display successfully", "Entered Planed Qty display successfully",
					"pass");
		} else {
			writeTestResults("Verify that entered Planed Qty is display successfully",
					"Entered Planed Qty should be display successfully", "Entered Planed Qty not display", "fail");
		}

		click(btn_draft);
		explicitWait(header_draftedProductionOrder, 60);
		if (isDisplayed(header_draftedProductionOrder)) {
			writeTestResults("Verify user able to draft the Production Order",
					"User should be able to draft the Production Order", "User successfully draft the Production Order",
					"pass");
		} else {
			writeTestResults("Verify user able to draft the Production Order",
					"User should be able to draft the Production Order", "User couldn't draft the Production Order",
					"fail");
		}

		click(btn_release);
		explicitWait(header_releasedProductionOrder, 30);
		if (isDisplayed(header_releasedProductionOrder)) {
			writeTestResults("Verify user able to release the Production Order",
					"User should be able to release the Production Order",
					"User successfully release the Production Order", "pass");
		} else {
			writeTestResults("Verify user able to release the Production Order",
					"User should be able to release the Production Order", "User couldn't release the Production Order",
					"fail");
		}
	}

	/* PROD_PO_051 */
	public void productionOrder_1_PROD_PO_051() throws Exception {
		loginBiletaAutomation();
		explicitWaitUntillClickable(img_navigationPane, 60);
		Thread.sleep(2000);
		explicitWaitUntillClickable(img_navigationPane, 20);
		if (click(img_navigationPane)) {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User successfully click navigation pane", "pass");
		} else {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User couldn't click navigation pane", "fail");
		}

		explicitWait(btn_production, 4);
		if (isEnabledQuickCheck(btn_production)) {
			writeTestResults("Verify user able to view the \"Production\" module under main navigation menu",
					"User should be able to view the \"Production\" module under main navigation menu",
					"User successfully view the \"Production\" module under main navigation menu", "pass");
		} else {
			writeTestResults("Verify user able to view the \"Production\" module under main navigation menu",
					"User should be able to view the \"Production\" module under main navigation menu",
					"User couldn't view the \"Production\" module under main navigation menu", "fail");
		}

		click(btn_production);

		explicitWait(btn_productionOrder, 4);
		if (isEnabledQuickCheck(btn_productionOrder)) {
			writeTestResults("Verify user able to view the \"Production Order\" option under production sub menu",
					"User should be able to view the \"Production Order\" option under production sub menu",
					"User able to view the \"Production Order\" option under production sub menu", "pass");
		} else {
			writeTestResults("Verify user able to view the \"Production Order\" option under production sub menu",
					"User should be able to view the \"Production Order\" option under production sub menu",
					"User couldn't view the \"Production Order\" option under production sub menu", "pass");
		}

		click(btn_productionOrder);

		explicitWait(header_productionOrderByPage, 60);
		if (isDisplayed(header_productionOrderByPage)) {
			writeTestResults("Verify user able to navigate to Production Order by-page",
					"User should be able to navigate to Production Order by-page",
					"User successfully navigate to Production Order by-page", "pass");
		} else {
			writeTestResults("Verify user able to navigate to Production Order by-page",
					"User should be able to navigate to Production Order by-page",
					"User coulsn't navigate to Production Order by-page", "fail");
		}

		click(btn_newProductionOrder);
		explicitWait(header_newProductionOrder, 60);
		if (isDisplayed(header_newProductionOrder)) {
			writeTestResults("Verify user able to navigate to Production Order new-form",
					"User should be able to navigate to Production Order new-form",
					"User successfully navigate to Production Order new-form", "pass");
		} else {
			writeTestResults("Verify user able to navigate to Production Order new-form",
					"User should be able to navigate to Production Order new-form",
					"User coulsn't navigate to Production Order new-form", "fail");
		}

		click(span_productLookupPO);
		explicitWait(lookup_product, 30);
		if (isDisplayed(lookup_product)) {
			writeTestResults("Verify that product lookup is open", "Product lookup should be open",
					"Product lookup successfully open", "pass");
		} else {
			writeTestResults("Verify that product lookup is open", "Product lookup should be open",
					"Product lookup doesn't open", "fail");
		}

		sendKeys(txt_searchProductProductLookup, readProductionData("ProductionTypeProductRegressionPO"));
		String enteredProduct = getAttribute(txt_searchProductProductLookup, "value");
		if (enteredProduct.equals(readProductionData("ProductionTypeProductRegressionPO"))) {
			writeTestResults("Verify that user able to enter the relevent product",
					"User should be able to enter the relevent product", "User successfully enter the relevent product",
					"pass");
		} else {
			writeTestResults("Verify that user able to enter the relevent product",
					"User should be able to enter the relevent product", "User doesn't enter the relevent product",
					"fail");
		}

		click(btn_searchProductProductLookup);
		explicitWaitUntillClickable(td_resultProductRegressionPO.replace("product_code",
				readProductionData("ProductionTypeProductRegressionPO")), 60);
		if (isEnabled(td_resultProductRegressionPO.replace("product_code",
				readProductionData("ProductionTypeProductRegressionPO")))) {
			writeTestResults("Verify that relevent product is available", "Relevent product should be available",
					"Relevent product successfully available", "pass");
		} else {
			writeTestResults("Verify that relevent product is available", "Relevent product should be available",
					"Relevent product not available", "fail");
		}

		doubleClick(td_resultProductRegressionPO.replace("product_code",
				readProductionData("ProductionTypeProductRegressionPO")));
		Thread.sleep(2000);

		String selectedProduct = getAttribute(txt_outputProductProductInformationSectionSummaryTabPO, "value");
		if (selectedProduct.equals(selectedProductFrontPageRegressionPO.replace("product_code",
				readProductionData("ProductionTypeProductRegressionPO")))) {
			writeTestResults("Verify that user able to select the relevent product",
					"User should be able to select the relevent product",
					"User successfully select the relevent product", "pass");
		} else {
			writeTestResults("Verify that user able to select the relevent product",
					"User should be able to select the relevent product", "User couldn't select the relevent product",
					"fail");
		}
	}

	public void productionOrder_2_PROD_PO_051() throws Exception {
		click(btn_productionModelLookupPO);
		explicitWait(lookup_productionModel, 30);
		if (isDisplayed(lookup_productionModel)) {
			writeTestResults("Verify that Production Model lookup is open", "Production Model lookup should be open",
					"Production Model lookup successfully open", "pass");
		} else {
			writeTestResults("Verify that Production Model lookup is open", "Production Model lookup should be open",
					"Production Model lookup doesn't open", "fail");
		}

		sendKeys(txt_searchProductionModel, readProductionData("ProductionModeleMRPEnabledRegressionPO"));
		String enteredProductionModel = getAttribute(txt_searchProductionModel, "value");
		if (enteredProductionModel.equals(readProductionData("ProductionModeleMRPEnabledRegressionPO"))) {
			writeTestResults("Verify that user able to enter the relevent Production Model",
					"User should be able to enter the relevent Production Model",
					"User successfully enter the relevent Production Model", "pass");
		} else {
			writeTestResults("Verify that user able to enter the relevent Production Model",
					"User should be able to enter the relevent Production Model",
					"User doesn't enter the relevent Production Model", "fail");
		}

		click(btn_searchProductionModel);
		explicitWaitUntillClickable(td_resultProductionModelDocReplace.replace("doc_no",
				readProductionData("ProductionModeleMRPEnabledRegressionPO")), 60);
		if (isEnabled(td_resultProductionModelDocReplace.replace("doc_no",
				readProductionData("ProductionModeleMRPEnabledRegressionPO")))) {
			writeTestResults("Verify that relevent Production Model is available",
					"Relevent Production Model should be available", "Relevent Production Model successfully available",
					"pass");
		} else {
			writeTestResults("Verify that relevent Production Model is available",
					"Relevent Production Model should be available", "Relevent Production Model not available", "fail");
		}

		doubleClick(td_resultProductionModelDocReplace.replace("doc_no",
				readProductionData("ProductionModeleMRPEnabledRegressionPO")));
		Thread.sleep(2000);

		String selectedProducttionModel = getAttribute(txt_productionModelSummarySectionSummaryTabPO, "value");
		if (selectedProducttionModel.equals(readProductionData("ProductionModeleMRPEnabledRegressionPO"))) {
			writeTestResults("Verify that user able to select the relevent Production Model",
					"User should be able to select the relevent Production Model",
					"User successfully select the relevent Production Model", "pass");
		} else {
			writeTestResults("Verify that user able to select the relevent Production Model",
					"User should be able to select the relevent Production Model",
					"User couldn't select the relevent Production Model", "fail");
		}

		String loadedProductionUnit = getAttribute(txt_productionUnitSummarySectionSummaryTabPO, "value");
		if (loadedProductionUnit.equals(prductionUnitAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that Production Unit auto loaded according to the Production Model in summary section",
					"Production Unit should be auto loaded according to the Production Model in summary section",
					"Production Unit successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Production Unit auto loaded according to the Production Model in summary section",
					"Production Unit should be auto loaded according to the Production Model in summary section",
					"Production Unit doesn't auto loaded according to the Production Model in summary section", "fail");
		}

		String loadedInputWare = getSelectedOptionInDropdown(drop_inputWarehouseSummarySectionSummaryTabPO);
		if (loadedInputWare.equals(inputWarehouseAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that Input Warehouse auto loaded according to the Production Model in summary section",
					"Input Warehouse should be auto loaded according to the Production Model in summary section",
					"Input Warehouse successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Input Warehouse auto loaded according to the Production Model in summary section",
					"Input Warehouse should be auto loaded according to the Production Model in summary section",
					"Input Warehouse doesn't auto loaded according to the Production Model in summary section", "fail");
		}

		String loadedOutputWare = getSelectedOptionInDropdown(drop_outputWarehouseSummarySectionSummaryTabPO);
		if (loadedOutputWare.equals(outputWarehouseAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that Output Warehouse auto loaded according to the Production Model in summary section",
					"Output Warehouse should be auto loaded according to the Production Model in summary section",
					"Output Warehouse successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Output Warehouse auto loaded according to the Production Model in summary section",
					"Output Warehouse should be auto loaded according to the Production Model in summary section",
					"Output Warehouse doesn't auto loaded according to the Production Model in summary section",
					"fail");
		}

		String loadedWIPWare = getSelectedOptionInDropdown(drop_WIPWarehouseSummarySectionSummaryTabPO);
		if (loadedWIPWare.equals(wipWarehouseAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that WIP Warehouse auto loaded according to the Production Model in summary section",
					"WIP Warehouse should be auto loaded according to the Production Model in summary section",
					"WIP Warehouse successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that WIP Warehouse auto loaded according to the Production Model in summary section",
					"WIP Warehouse should be auto loaded according to the Production Model in summary section",
					"WIP Warehouse doesn't auto loaded according to the Production Model in summary section", "fail");
		}

		String loadedSite = getSelectedOptionInDropdown(drop_siteSummarySectionSummaryTabPO);
		if (loadedSite.equals(siteAaccordigToPMRegression)) {
			writeTestResults("Verify that Site auto loaded according to the Production Model in summary section",
					"Site should be auto loaded according to the Production Model in summary section",
					"Site successfully auto loaded according to the Production Model in summary section", "pass");
		} else {
			writeTestResults("Verify that Site auto loaded according to the Production Model in summary section",
					"Site should be auto loaded according to the Production Model in summary section",
					"Site doesn't auto loaded according to the Production Model in summary section", "fail");
		}

		String loadedCostingPriority = getSelectedOptionInDropdown(
				drop_costingPriorityProductInformationSectionSummaryTabPO);
		if (loadedCostingPriority.equals(costingPriorityAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that Costing Priority auto loaded according to the Production Model in product information section",
					"Costing Priority should be auto loaded according to the Production Model in product information section",
					"Costing Priority successfully auto loaded according to the Production Model in product information section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Costing Priority auto loaded according to the Production Model in product information section",
					"Costing Priority should be auto loaded according to the Production Model in product information section",
					"Costing Priority doesn't auto loaded according to the Production Model in product information section",
					"fail");
		}

		String loadedBoM = getText(drop_boMBoMDetailsSectionSummaryTabPO);
		if (loadedBoM.equals("000095")) {
			writeTestResults(
					"Verify that BoM Document Number auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number should be auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number successfully auto loaded according to the Production Model in bill of material details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoM Document Number auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number should be auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number doesn't auto loaded according to the Production Model in bill of material details section",
					"fail");
		}

		String loadedBoMVarient = getText(div_boMVarientBoMDetailsSectionSummaryTabPO);
		if (loadedBoMVarient.equals(bomVarientAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that BoM Varient auto loaded according to the Production Model in bill of material details section",
					"BoM Varient should be auto loaded according to the Production Model in bill of material details section",
					"BoM Varient successfully auto loaded according to the Production Model in bill of material details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoM Varient auto loaded according to the Production Model in bill of material details section",
					"BoM Varient should be auto loaded according to the Production Model in bill of material details section",
					"BoM Varient doesn't auto loaded according to the Production Model in bill of material details section",
					"fail");
		}

		String loadedBoMVarientDescription = getText(div_varientDescriptionBoMDetailsSectionSummaryTabPO);
		if (loadedBoMVarientDescription.equals(varientDescriptionAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that BoM Varient Description auto loaded according to the Production Model in bill of material details section",
					"BoM Varient Description should be auto loaded according to the Production Model in bill of material details section",
					"BoM Varient Description successfully auto loaded according to the Production Model in bill of material details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoM Varient Description auto loaded according to the Production Model in bill of material details section",
					"BoM Varient Description should be auto loaded according to the Production Model in bill of material details section",
					"BoM Varient Description doesn't auto loaded according to the Production Model in bill of material details section",
					"fail");
		}

		String loadedBoO = getText(div_boOBoODetailsSectionSummaryTabPO);
		if (loadedBoO.equals(booNoAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that BoO Number auto loaded according to the Production Model in bill of operation details section",
					"BoO Number should be auto loaded according to the Production Model in bill of operation details section",
					"BoO Number successfully auto loaded according to the Production Model in bill of operation details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoO Number auto loaded according to the Production Model in bill of operation details section",
					"BoO Number should be auto loaded according to the Production Model in bill of operation details section",
					"BoO Number doesn't auto loaded according to the Production Model in bill of operation details section",
					"fail");
		}

		String loadedBoODescription = getText(div_boODescriptionDetailsSectionSummaryTabPO);
		if (loadedBoODescription.equals(booDescriptionAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that BoO Description auto loaded according to the Production Model in bill of operation details section",
					"BoO Description should be auto loaded according to the Production Model in bill of operation details section",
					"BoO Description successfully auto loaded according to the Production Model in bill of operation details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoO Description auto loaded according to the Production Model in bill of operation details section",
					"BoO Description should be auto loaded according to the Production Model in bill of operation details section",
					"BoO Description doesn't auto loaded according to the Production Model in bill of operation details section",
					"fail");
		}

		/*
		 * There is a bug. Supply production checkbox should be loaded as selected. Need
		 * to change after the bug was fixed
		 */
		if (!isSelected(chk_supplyToOperationPO)) {
			writeTestResults(
					"Verify that Supply to Production checkbox auto loaded according to the Production Model in bill of operation details section",
					"Supply to Production checkbox should be auto loaded according to the Production Model in bill of operation details section",
					"Supply to Production checkbox successfully auto loaded according to the Production Model in bill of operation details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Supply to Production checkbox auto loaded according to the Production Model in bill of operation details section",
					"Supply to Production checkbox should be auto loaded according to the Production Model in bill of operation details section",
					"Supply to Production checkbox doesn't auto loaded according to the Production Model in bill of operation details section",
					"fail");
		}

		String loadedRequestedStartDate = getAttribute(txt_requestStartDateSheduleSectionSummaryTabPO, "value");
		String currentDate = common.currentDate();
		if (loadedRequestedStartDate.equals(currentDate)) {
			writeTestResults("Verify that Reqested Start Date auto loaded in shedule section",
					"Reqested Start Date should be auto loaded in shedule section",
					"Reqested Start Date successfully auto loaded in shedule section", "pass");
		} else {
			writeTestResults("Verify that Reqested Start Date auto loaded in shedule section",
					"Reqested Start Date should be auto loaded in shedule section",
					"Reqested Start Date doesn't auto loaded in shedule section", "fail");
		}

		String loadedRequestedEndDate = getAttribute(txt_requestEndDateSheduleSectionSummaryTabPO, "value");
		if (loadedRequestedEndDate.equals(currentDate)) {
			writeTestResults("Verify that Reqested End Date auto loaded in shedule section",
					"Reqested End Date should be auto loaded in shedule section",
					"Reqested End Date successfully auto loaded in shedule section", "pass");
		} else {
			writeTestResults("Verify that Reqested End Date auto loaded in shedule section",
					"Reqested End Date should be auto loaded in shedule section",
					"Reqested End Date doesn't auto loaded in shedule section", "fail");
		}

		selectText(drop_prioritySummarySectionSummaryTabPO, productionOrderPriorityRegression);
		Thread.sleep(1500);
		String selectedPriority = getSelectedOptionInDropdown(drop_prioritySummarySectionSummaryTabPO);
		if (selectedPriority.equals(productionOrderPriorityRegression)) {
			writeTestResults("Verify that selected priority is display successfully",
					"Selected priority should be display successfully", "Selected priority display successfully",
					"pass");
		} else {
			writeTestResults("Verify that selected priority is display successfully",
					"Selected priority should be display successfully", "Selected priority not display", "fail");
		}

		driver.findElement(getLocator(txt_planedQtyProductInformationSectionSummaryTabPO)).click();
		driver.findElement(getLocator(txt_planedQtyProductInformationSectionSummaryTabPO))
				.sendKeys(planedQtyPORegression);
		Thread.sleep(1500);
		String enteredPlanedQty = getAttribute(txt_planedQtyProductInformationSectionSummaryTabPO, "value");
		if (enteredPlanedQty.equals(planedQtyPORegression)) {
			writeTestResults("Verify that entered Planed Qty is display successfully",
					"Entered Planed Qty should be display successfully", "Entered Planed Qty display successfully",
					"pass");
		} else {
			writeTestResults("Verify that entered Planed Qty is display successfully",
					"Entered Planed Qty should be display successfully", "Entered Planed Qty not display", "fail");
		}

		if (isSelected(chk_mrpProductionOrderRegression)) {
			writeTestResults("Verify that the system is auto enabled the MRP Enabled",
					"The system should be auto enabled the MRP Enabled",
					"The system successfully auto enabled the MRP Enabled", "pass");
		} else {
			writeTestResults("Verify that the system is auto enabled the MRP Enabled",
					"The system should be auto enabled the MRP Enabled",
					"The system doesn't auto enabled the MRP Enabled", "fail");
		}

	}

	public void productionOrder_3_PROD_PO_051() throws Exception {
		selectText(drop_barcodeBookSummarySectionSummaryTabPO, barcodeBookPORegression);
		String selectedBarcodeBook = getSelectedOptionInDropdown(drop_barcodeBookSummarySectionSummaryTabPO);
		if (selectedBarcodeBook.equals(barcodeBookPORegression)) {
			writeTestResults("Verify that user able to select Barcode Book",
					"User should be able to select Barcode Book", "User able to select Barcode Book", "pass");
		} else {
			writeTestResults("Verify that user able to select Barcode Book",
					"User should be able to select Barcode Book", "User not able to select Barcode Book", "fail");
		}

		selectText(drop_productOrderGroupSummarySectionSummaryTabPO, productionOrderGroupRegression);
		Thread.sleep(1000);
		String selectedProductionGroup = getSelectedOptionInDropdown(drop_productOrderGroupSummarySectionSummaryTabPO);
		if (selectedProductionGroup.equals(productionOrderGroupRegression)) {
			writeTestResults("Verify user able to select the Production Group",
					"User should be able to select the Production Group",
					"User successfully select the Production Group", "pass");
		} else {
			writeTestResults("Verify user able to select the Production Group",
					"User should be able to select the Production Group", "User couldn't select the Production Group",
					"fail");
		}

		sendKeys(txt_descriptionSummarySectionSummaryTabPO, productionOrderDescriptionRegression);
		String enteredDescription = getAttribute(txt_descriptionSummarySectionSummaryTabPO, "value");
		if (enteredDescription.equals(productionOrderDescriptionRegression)) {
			writeTestResults("Verify user able to enter the Description",
					"User should be able to enter the Description", "User successfully enter the Description", "pass");
		} else {
			writeTestResults("Verify user able to enter the Description",
					"User should be able to enter the Description", "User couldn't enter the Description", "fail");
		}

		click(txt_requestStartDateSheduleSectionSummaryTabPO);
		explicitWaitUntillClickable(a_requestStartDatePOWhenDuplicating, 6);
		click(a_requestStartDatePOWhenDuplicating);
		Thread.sleep(2000);
		String selectedRequestedStartDate = getAttribute(txt_requestStartDateSheduleSectionSummaryTabPO, "value");
		String currentYearMonthWithselectedDate = common.currentDate().substring(0, 8) + "01";
		if (selectedRequestedStartDate.equals(currentYearMonthWithselectedDate)) {
			writeTestResults("Verify user able to select Requested Start Date",
					"User should be able to select Requested Start Date",
					"User successfully select Requested Start Date", "pass");
		} else {
			writeTestResults("Verify user able to select Requested Start Date",
					"User should be able to select Requested Start Date", "User couldn't select Requested Start Date",
					"fail");
		}

		click(txt_requestEndDateSheduleSectionSummaryTabPO);
		explicitWaitUntillClickable(a_requestEndDatePOWhenDuplicating, 6);
		click(a_requestEndDatePOWhenDuplicating);
		Thread.sleep(2000);
		String selectedRequestedEndDate = getAttribute(txt_requestEndDateSheduleSectionSummaryTabPO, "value");
		String currentYearMonthWithselectedEndDate = common.currentDate().substring(0, 8) + "28";
		if (selectedRequestedEndDate.equals(currentYearMonthWithselectedEndDate)) {
			writeTestResults("Verify user able to select Requested End Date",
					"User should be able to select Requested End Date", "User successfully select Requested End Date",
					"pass");
		} else {
			writeTestResults("Verify user able to select Requested End Date",
					"User should be able to select Requested End Date", "User couldn't select Requested End Date",
					"fail");
		}

		click(btn_draft);
		explicitWait(header_draftedProductionOrder, 60);
		if (isDisplayed(header_draftedProductionOrder)) {
			writeTestResults("Verify user able to draft the Production Order",
					"User should be able to draft the Production Order", "User successfully draft the Production Order",
					"pass");
		} else {
			writeTestResults("Verify user able to draft the Production Order",
					"User should be able to draft the Production Order", "User couldn't draft the Production Order",
					"fail");
		}

		click(btn_release);
		explicitWait(header_releasedProductionOrder, 30);
		trackCode = getText(lbl_docNo);
		releasedProductionOrder = trackCode;
		if (isDisplayed(header_releasedProductionOrder)) {
			writeTestResults("Verify user able to release the Production Order",
					"User should be able to release the Production Order",
					"User successfully release the Production Order", "pass");
		} else {
			writeTestResults("Verify user able to release the Production Order",
					"User should be able to release the Production Order", "User couldn't release the Production Order",
					"fail");
		}

		explicitWaitUntillClickable(img_navigationPane, 60);
		Thread.sleep(2000);
		explicitWaitUntillClickable(img_navigationPane, 20);
		if (click(img_navigationPane)) {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User successfully click navigation pane", "pass");
		} else {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User couldn't click navigation pane", "fail");
		}

		explicitWait(btn_production, 4);
		if (isEnabledQuickCheck(btn_production)) {
			writeTestResults("Verify user able to view the \"Production\" module under main navigation menu",
					"User should be able to view the \"Production\" module under main navigation menu",
					"User successfully view the \"Production\" module under main navigation menu", "pass");
		} else {
			writeTestResults("Verify user able to view the \"Production\" module under main navigation menu",
					"User should be able to view the \"Production\" module under main navigation menu",
					"User couldn't view the \"Production\" module under main navigation menu", "fail");
		}

		click(btn_production);

		explicitWait(btn_productionOrder, 4);
		if (isEnabledQuickCheck(btn_productionOrder)) {
			writeTestResults("Verify user able to view the \"Production Order\" option under production sub menu",
					"User should be able to view the \"Production Order\" option under production sub menu",
					"User able to view the \"Production Order\" option under production sub menu", "pass");
		} else {
			writeTestResults("Verify user able to view the \"Production Order\" option under production sub menu",
					"User should be able to view the \"Production Order\" option under production sub menu",
					"User couldn't view the \"Production Order\" option under production sub menu", "pass");
		}

		click(btn_productionOrder);

		explicitWait(header_productionOrderByPage, 60);
		if (isDisplayed(header_productionOrderByPage)) {
			writeTestResults("Verify user able to navigate to Production Order by-page",
					"User should be able to navigate to Production Order by-page",
					"User successfully navigate to Production Order by-page", "pass");
		} else {
			writeTestResults("Verify user able to navigate to Production Order by-page",
					"User should be able to navigate to Production Order by-page",
					"User coulsn't navigate to Production Order by-page", "fail");
		}

		click(btn_newProductionOrder);
		explicitWait(header_newProductionOrder, 60);
		if (isDisplayed(header_newProductionOrder)) {
			writeTestResults("Verify user able to navigate to Production Order new-form",
					"User should be able to navigate to Production Order new-form",
					"User successfully navigate to Production Order new-form", "pass");
		} else {
			writeTestResults("Verify user able to navigate to Production Order new-form",
					"User should be able to navigate to Production Order new-form",
					"User coulsn't navigate to Production Order new-form", "fail");
		}

		click(btn_copyFrom);
		Thread.sleep(1000);
		explicitWait(header_loockupProductionOrder, 40);
		if (isDisplayed(header_loockupProductionOrder)) {
			writeTestResults("Verify user able to view Production Order lookup",
					"User should be able to view Production Order lookup",
					"User successfully view Production Order lookup", "pass");
		} else {
			writeTestResults("Verify user able to view Production Order lookup",
					"User should be able to view Production Order lookup", "User doesn't view Production Order lookup",
					"fail");
		}

		sendKeys(txt_searchProductionOrder2, releasedProductionOrder);
		pressEnter(txt_searchProductionOrder2);

		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", releasedProductionOrder), 15);

		if (isDisplayed(lnk_inforOnLookupInformationReplace.replace("info", releasedProductionOrder))) {
			writeTestResults(
					"Verify relevent existing Production Order was loaded and user allow to select the particular Production Order",
					"Relevent existing Production Order should be loaded and user should allow to select the particular Production Order",
					"Relevent existing Production Order successfully loaded and user allow to select the particular Production Order",
					"pass");
		} else {
			writeTestResults(
					"Verify relevent existing Production Order was loaded and user allow to select the particular Production Order",
					"Relevent existing Production Order should be loaded and user should allow to select the particular Production Order",
					"Relevent existing Production Order doesn't loaded and user not allow to select the particular Production Order",
					"fail");
		}

		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", releasedProductionOrder));

		explicitWaitUntillClickable(drop_productOrderGroupSummarySectionSummaryTabPO, 60);
		String loadedProductionGroup = getSelectedOptionInDropdown(drop_productOrderGroupSummarySectionSummaryTabPO);
		if (loadedProductionGroup.equals(productionOrderGroupRegression)) {
			writeTestResults("Verify that Production Group is available same as source document",
					"Production Group available same as source document",
					"Production Group successfully available same as source document", "pass");
		} else {
			writeTestResults("Verify that Production Group is available same as source document",
					"Production Group available same as source document",
					"Production Group doesn't available same as source document", "fail");
		}

		String loadedDescription = getAttribute(txt_descriptionSummarySectionSummaryTabPO, "value");
		if (loadedDescription.equals(productionOrderDescriptionRegression)) {
			writeTestResults("Verify that Description is available same as source document",
					"Description available same as source document",
					"Description successfully available same as source document", "pass");
		} else {
			writeTestResults("Verify that Description is available same as source document",
					"Description available same as source document",
					"Description doesn't available same as source document", "fail");
		}

		String loadedProducttionModel = getAttribute(txt_productionModelSummarySectionSummaryTabPO, "value");
		if (loadedProducttionModel.equals(readProductionData("ProductionModeleMRPEnabledRegressionPO"))) {
			writeTestResults("Verify that Production Model is available same as source document",
					"Production Model available same as source document",
					"Production Model successfully available same as source document", "pass");
		} else {
			writeTestResults("Verify that Production Model is available same as source document",
					"Production Model available same as source document",
					"Production Model doesn't available same as source document", "fail");
		}

		String loadedBarcodeBook = getSelectedOptionInDropdown(drop_barcodeBookSummarySectionSummaryTabPO);
		if (loadedBarcodeBook.equals(barcodeBookPORegression)) {
			writeTestResults("Verify that Barcode Book is available same as source document",
					"Barcode Book available same as source document",
					"Barcode Book successfully available same as source document", "pass");
		} else {
			writeTestResults("Verify that Barcode Book is available same as source document",
					"Barcode Book available same as source document",
					"Barcode Book doesn't available same as source document", "fail");
		}

		String loadedPriority = getSelectedOptionInDropdown(drop_prioritySummarySectionSummaryTabPO);
		if (loadedPriority.equals(productionOrderPriorityRegression)) {
			writeTestResults("Verify that Priority is available same as source document",
					"Priority available same as source document",
					"Priority successfully available same as source document", "pass");
		} else {
			writeTestResults("Verify that Priority is available same as source document",
					"Priority available same as source document", "Priority doesn't available same as source document",
					"fail");
		}

		String loadedProductionUnitCopyFromDocument = getAttribute(txt_productionUnitSummarySectionSummaryTabPO,
				"value");
		if (loadedProductionUnitCopyFromDocument.equals(productionUnitRegressionPOCopyFrom)) {
			writeTestResults("Verify that Production Unit is available same as source document",
					"Production Unit available same as source document",
					"Production Unit successfully available same as source document", "pass");
		} else {
			writeTestResults("Verify that Production Unit is available same as source document",
					"Production Unit available same as source document",
					"Production Unit doesn't available same as source document", "fail");
		}

		String loadedInputWareCopyFrom = getSelectedOptionInDropdown(drop_inputWarehouseSummarySectionSummaryTabPO);
		if (loadedInputWareCopyFrom.equals(inputWarehouseRegressionPOCopyFrom)) {
			writeTestResults("Verify that Input Warehouse is available same as source document",
					"Input Warehouse available same as source document",
					"Input Warehouse successfully available same as source document", "pass");
		} else {
			writeTestResults("Verify that Input Warehouse is available same as source document",
					"Input Warehouse available same as source document",
					"Input Warehouse doesn't available same as source document", "fail");
		}

		String loadedOutputWareDuplicated = getSelectedOptionInDropdown(drop_outputWarehouseSummarySectionSummaryTabPO);
		if (loadedOutputWareDuplicated.equals(outputWarehouseRegressionPOCopyFrom)) {
			writeTestResults("Verify that Output Warehouse is available same as source document",
					"Output Warehouse available same as source document",
					"Output Warehouse successfully available same as source document", "pass");
		} else {
			writeTestResults("Verify that Output Warehouse is available same as source document",
					"Output Warehouse available same as source document",
					"Output Warehouse doesn't available same as source document", "fail");
		}

		String loadedWIPWareCopyFrom = getSelectedOptionInDropdown(drop_WIPWarehouseSummarySectionSummaryTabPO);
		if (loadedWIPWareCopyFrom.equals(wipWarehouseRegressionPOCopyFrom)) {
			writeTestResults("Verify that WIP Warehouse is available same as source document",
					"WIP Warehouse available same as source document",
					"WIP Warehouse successfully available same as source document", "pass");
		} else {
			writeTestResults("Verify that WIP Warehouse is available same as source document",
					"WIP Warehouse available same as source document",
					"WIP Warehouse doesn't available same as source document", "fail");
		}

		String loadedSiteCopyFrom = getSelectedOptionInDropdown(drop_siteSummarySectionSummaryTabPO);
		if (loadedSiteCopyFrom.equals(siteRegressionPOCopyFrom)) {
			writeTestResults("Verify that Site is available same as source document",
					"Site available same as source document", "Site successfully available same as source document",
					"pass");
		} else {
			writeTestResults("Verify that Site is available same as source document",
					"Site available same as source document", "Site doesn't available same as source document", "fail");
		}

		String selectedProduct = getAttribute(txt_outputProductProductInformationSectionSummaryTabPO, "value");
		if (selectedProduct.equals(selectedProductFrontPageRegressionPO.replace("product_code",
				readProductionData("ProductionTypeProductRegressionPO")))) {
			writeTestResults("Verify that Output Product is available same as source document",
					"Output Product available same as source document",
					"Output Product successfully available same as source document", "pass");
		} else {
			writeTestResults("Verify that Output Product is available same as source document",
					"Output Product available same as source document",
					"Output Product doesn't available same as source document", "fail");
		}

		String loadedCostingPriorityCopyFrom = getSelectedOptionInDropdown(
				drop_costingPriorityProductInformationSectionSummaryTabPO);
		if (loadedCostingPriorityCopyFrom.equals(costingPriorityRegressionPOCopyFrom)) {
			writeTestResults("Verify that Costing Priority is available same as source document",
					"Costing Priority available same as source document",
					"Costing Priority successfully available same as source document", "pass");
		} else {
			writeTestResults("Verify that Costing Priority is available same as source document",
					"Costing Priority available same as source document",
					"Costing Priority doesn't available same as source document", "fail");
		}

		if (isSelected(chk_mrpProductionOrderRegression)) {
			writeTestResults("Verify that MRP Enabled checkbox is enabled same as source document",
					"MRP Enabled checkbox enabled same as source document",
					"MRP Enabled checkbox successfully enabled same as source document", "pass");
		} else {
			writeTestResults("Verify that MRP Enabled checkbox is enabled same as source document",
					"MRP Enabled checkbox enabled same as source document",
					"MRP Enabled checkbox doesn't enabled same as source document", "fail");
		}

		String loadedBoMCopyFrom = getText(drop_boMBoMDetailsSectionSummaryTabPO);
		if (loadedBoMCopyFrom.equals("000095")) {
			writeTestResults("Verify that BoM is available same as source document",
					"BoM available same as source document", "BoM successfully available same as source document",
					"pass");
		} else {
			writeTestResults("Verify that BoM is available same as source document",
					"BoM available same as source document", "BoM doesn't available same as source document", "fail");
		}

		String loadedBoOCopyFrom = getText(div_boOBoODetailsSectionSummaryTabPO);
		if (loadedBoOCopyFrom.equals(booRegressionPODupliacted)) {
			writeTestResults("Verify that BoO is available same as source document",
					"BoO available same as source document", "BoO successfully available same as source document",
					"pass");
		} else {
			writeTestResults("Verify that BoO is available same as source document",
					"BoO available same as source document", "BoO doesn't available same as source document", "fail");
		}

		/*
		 * There is a bug at initial steps. User not able to auto enable Supply to
		 * Production checkbox by the Production Model. Need to correct after bu was
		 * fixed
		 */

		if (!isSelected(chk_supplyToProductionPO)) {
			writeTestResults("Verify that Supply to Production checkbox is not disabled same as source document",
					"Supply to Production checkbox not disabled same as source document",
					"Supply to Production checkbox successfully not disabled same as source document", "pass");
		} else {
			writeTestResults("Verify that Supply to Production checkbox is not disabled same as source document",
					"Supply to Production checkbox not disabled same as source document",
					"Supply to Production checkbox doesn't not disabled same as source document", "fail");
		}

		String loadedRequestedEndDateCopyFrom = getAttribute(txt_requestEndDateSheduleSectionSummaryTabPO, "value");
		String currentYearMonthWithselectedEndDateCopyFrom = common.currentDate().substring(0, 8) + "28";
		if (loadedRequestedEndDateCopyFrom.equals(currentYearMonthWithselectedEndDateCopyFrom)) {
			writeTestResults("Verify that Reqested End Date auto loaded as source document",
					"Reqested End Date should be auto loaded as source document",
					"Reqested End Date successfully auto loaded as source document", "pass");
		} else {
			writeTestResults("Verify that Reqested End Date auto loaded as source document",
					"Reqested End Date should be auto loaded as source document",
					"Reqested End Date doesn't auto loaded as source document", "fail");
		}

		sendKeys(txt_descriptionSummarySectionSummaryTabPO, descriptionCopyFromPORegression);
		String enteredDescriptionDuplicated = getAttribute(txt_descriptionSummarySectionSummaryTabPO, "value");
		if (enteredDescriptionDuplicated.equals(descriptionCopyFromPORegression)) {
			writeTestResults("Verify user able to change the Description",
					"User should be able to change the Description", "User successfully change the Description",
					"pass");
		} else {
			writeTestResults("Verify user able to change the Description",
					"User should be able to change the Description", "User couldn't change the Description", "fail");
		}

		selectText(drop_prioritySummarySectionSummaryTabPO, priorityCopyFromPORegression);
		String selectedPriorityDuplicated = getSelectedOptionInDropdown(drop_prioritySummarySectionSummaryTabPO);
		if (selectedPriorityDuplicated.equals(priorityCopyFromPORegression)) {
			writeTestResults("Verify user able to change the Priority", "User should be able to change the Priority",
					"User successfully change the Priority", "pass");
		} else {
			writeTestResults("Verify user able to change the Priority", "User should be able to change the Priority",
					"User couldn't change the Priority", "fail");
		}

		driver.findElement(getLocator(txt_planedQtyProductInformationSectionSummaryTabPO)).click();
		driver.findElement(getLocator(txt_planedQtyProductInformationSectionSummaryTabPO))
				.sendKeys(planedQtyPORegression);
		Thread.sleep(1500);
		String enteredPlanedQtyDuplicated = getAttribute(txt_planedQtyProductInformationSectionSummaryTabPO, "value");
		if (enteredPlanedQtyDuplicated.equals(planedQtyPORegression)) {
			writeTestResults("Verify that entered Planed Qty is display successfully",
					"Entered Planed Qty should be display successfully", "Entered Planed Qty display successfully",
					"pass");
		} else {
			writeTestResults("Verify that entered Planed Qty is display successfully",
					"Entered Planed Qty should be display successfully", "Entered Planed Qty not display", "fail");
		}

		click(btn_draft);
		explicitWait(header_draftedProductionOrder, 60);
		if (isDisplayed(header_draftedProductionOrder)) {
			writeTestResults("Verify user able to draft the Production Order",
					"User should be able to draft the Production Order", "User successfully draft the Production Order",
					"pass");
		} else {
			writeTestResults("Verify user able to draft the Production Order",
					"User should be able to draft the Production Order", "User couldn't draft the Production Order",
					"fail");
		}

		click(btn_release);
		explicitWait(header_releasedProductionOrder, 30);
		if (isDisplayed(header_releasedProductionOrder)) {
			writeTestResults("Verify user able to release the Production Order",
					"User should be able to release the Production Order",
					"User successfully release the Production Order", "pass");
		} else {
			writeTestResults("Verify user able to release the Production Order",
					"User should be able to release the Production Order", "User couldn't release the Production Order",
					"fail");
		}
	}

	/* PROD_PO_052 */
	public void productionOrder_1_PROD_PO_052() throws Exception {
		loginBiletaAutomation();
		explicitWaitUntillClickable(img_navigationPane, 60);
		Thread.sleep(2000);
		explicitWaitUntillClickable(img_navigationPane, 20);
		if (click(img_navigationPane)) {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User successfully click navigation pane", "pass");
		} else {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User couldn't click navigation pane", "fail");
		}

		explicitWait(btn_production, 4);
		if (isEnabledQuickCheck(btn_production)) {
			writeTestResults("Verify user able to view the \"Production\" module under main navigation menu",
					"User should be able to view the \"Production\" module under main navigation menu",
					"User successfully view the \"Production\" module under main navigation menu", "pass");
		} else {
			writeTestResults("Verify user able to view the \"Production\" module under main navigation menu",
					"User should be able to view the \"Production\" module under main navigation menu",
					"User couldn't view the \"Production\" module under main navigation menu", "fail");
		}

		click(btn_production);

		explicitWait(btn_productionOrder, 4);
		if (isEnabledQuickCheck(btn_productionOrder)) {
			writeTestResults("Verify user able to view the \"Production Order\" option under production sub menu",
					"User should be able to view the \"Production Order\" option under production sub menu",
					"User able to view the \"Production Order\" option under production sub menu", "pass");
		} else {
			writeTestResults("Verify user able to view the \"Production Order\" option under production sub menu",
					"User should be able to view the \"Production Order\" option under production sub menu",
					"User couldn't view the \"Production Order\" option under production sub menu", "pass");
		}

		click(btn_productionOrder);

		explicitWait(header_productionOrderByPage, 60);
		if (isDisplayed(header_productionOrderByPage)) {
			writeTestResults("Verify user able to navigate to Production Order by-page",
					"User should be able to navigate to Production Order by-page",
					"User successfully navigate to Production Order by-page", "pass");
		} else {
			writeTestResults("Verify user able to navigate to Production Order by-page",
					"User should be able to navigate to Production Order by-page",
					"User coulsn't navigate to Production Order by-page", "fail");
		}

		click(btn_newProductionOrder);
		explicitWait(header_newProductionOrder, 60);
		if (isDisplayed(header_newProductionOrder)) {
			writeTestResults("Verify user able to navigate to Production Order new-form",
					"User should be able to navigate to Production Order new-form",
					"User successfully navigate to Production Order new-form", "pass");
		} else {
			writeTestResults("Verify user able to navigate to Production Order new-form",
					"User should be able to navigate to Production Order new-form",
					"User coulsn't navigate to Production Order new-form", "fail");
		}

		click(span_productLookupPO);
		explicitWait(lookup_product, 30);
		if (isDisplayed(lookup_product)) {
			writeTestResults("Verify that product lookup is open", "Product lookup should be open",
					"Product lookup successfully open", "pass");
		} else {
			writeTestResults("Verify that product lookup is open", "Product lookup should be open",
					"Product lookup doesn't open", "fail");
		}

		sendKeys(txt_searchProductProductLookup, readProductionData("ProductionTypeProductRegressionPO"));
		String enteredProduct = getAttribute(txt_searchProductProductLookup, "value");
		if (enteredProduct.equals(readProductionData("ProductionTypeProductRegressionPO"))) {
			writeTestResults("Verify that user able to enter the relevent product",
					"User should be able to enter the relevent product", "User successfully enter the relevent product",
					"pass");
		} else {
			writeTestResults("Verify that user able to enter the relevent product",
					"User should be able to enter the relevent product", "User doesn't enter the relevent product",
					"fail");
		}

		click(btn_searchProductProductLookup);
		explicitWaitUntillClickable(td_resultProductRegressionPO.replace("product_code",
				readProductionData("ProductionTypeProductRegressionPO")), 60);
		if (isEnabled(td_resultProductRegressionPO.replace("product_code",
				readProductionData("ProductionTypeProductRegressionPO")))) {
			writeTestResults("Verify that relevent product is available", "Relevent product should be available",
					"Relevent product successfully available", "pass");
		} else {
			writeTestResults("Verify that relevent product is available", "Relevent product should be available",
					"Relevent product not available", "fail");
		}

		doubleClick(td_resultProductRegressionPO.replace("product_code",
				readProductionData("ProductionTypeProductRegressionPO")));
		Thread.sleep(2000);

		String selectedProduct = getAttribute(txt_outputProductProductInformationSectionSummaryTabPO, "value");
		if (selectedProduct.equals(selectedProductFrontPageRegressionPO.replace("product_code",
				readProductionData("ProductionTypeProductRegressionPO")))) {
			writeTestResults("Verify that user able to select the relevent product",
					"User should be able to select the relevent product",
					"User successfully select the relevent product", "pass");
		} else {
			writeTestResults("Verify that user able to select the relevent product",
					"User should be able to select the relevent product", "User couldn't select the relevent product",
					"fail");
		}
	}

	public void productionOrder_2_PROD_PO_052() throws Exception {
		click(btn_productionModelLookupPO);
		explicitWait(lookup_productionModel, 30);
		if (isDisplayed(lookup_productionModel)) {
			writeTestResults("Verify that Production Model lookup is open", "Production Model lookup should be open",
					"Production Model lookup successfully open", "pass");
		} else {
			writeTestResults("Verify that Production Model lookup is open", "Production Model lookup should be open",
					"Production Model lookup doesn't open", "fail");
		}

		sendKeys(txt_searchProductionModel, readProductionData("ProductionModeleMRPEnabledRegressionPO"));
		String enteredProductionModel = getAttribute(txt_searchProductionModel, "value");
		if (enteredProductionModel.equals(readProductionData("ProductionModeleMRPEnabledRegressionPO"))) {
			writeTestResults("Verify that user able to enter the relevent Production Model",
					"User should be able to enter the relevent Production Model",
					"User successfully enter the relevent Production Model", "pass");
		} else {
			writeTestResults("Verify that user able to enter the relevent Production Model",
					"User should be able to enter the relevent Production Model",
					"User doesn't enter the relevent Production Model", "fail");
		}

		click(btn_searchProductionModel);
		explicitWaitUntillClickable(td_resultProductionModelDocReplace.replace("doc_no",
				readProductionData("ProductionModeleMRPEnabledRegressionPO")), 60);
		if (isEnabled(td_resultProductionModelDocReplace.replace("doc_no",
				readProductionData("ProductionModeleMRPEnabledRegressionPO")))) {
			writeTestResults("Verify that relevent Production Model is available",
					"Relevent Production Model should be available", "Relevent Production Model successfully available",
					"pass");
		} else {
			writeTestResults("Verify that relevent Production Model is available",
					"Relevent Production Model should be available", "Relevent Production Model not available", "fail");
		}

		doubleClick(td_resultProductionModelDocReplace.replace("doc_no",
				readProductionData("ProductionModeleMRPEnabledRegressionPO")));
		Thread.sleep(2000);

		String selectedProducttionModel = getAttribute(txt_productionModelSummarySectionSummaryTabPO, "value");
		if (selectedProducttionModel.equals(readProductionData("ProductionModeleMRPEnabledRegressionPO"))) {
			writeTestResults("Verify that user able to select the relevent Production Model",
					"User should be able to select the relevent Production Model",
					"User successfully select the relevent Production Model", "pass");
		} else {
			writeTestResults("Verify that user able to select the relevent Production Model",
					"User should be able to select the relevent Production Model",
					"User couldn't select the relevent Production Model", "fail");
		}

		String loadedProductionUnit = getAttribute(txt_productionUnitSummarySectionSummaryTabPO, "value");
		if (loadedProductionUnit.equals(prductionUnitAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that Production Unit auto loaded according to the Production Model in summary section",
					"Production Unit should be auto loaded according to the Production Model in summary section",
					"Production Unit successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Production Unit auto loaded according to the Production Model in summary section",
					"Production Unit should be auto loaded according to the Production Model in summary section",
					"Production Unit doesn't auto loaded according to the Production Model in summary section", "fail");
		}

		String loadedInputWare = getSelectedOptionInDropdown(drop_inputWarehouseSummarySectionSummaryTabPO);
		if (loadedInputWare.equals(inputWarehouseAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that Input Warehouse auto loaded according to the Production Model in summary section",
					"Input Warehouse should be auto loaded according to the Production Model in summary section",
					"Input Warehouse successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Input Warehouse auto loaded according to the Production Model in summary section",
					"Input Warehouse should be auto loaded according to the Production Model in summary section",
					"Input Warehouse doesn't auto loaded according to the Production Model in summary section", "fail");
		}

		String loadedOutputWare = getSelectedOptionInDropdown(drop_outputWarehouseSummarySectionSummaryTabPO);
		if (loadedOutputWare.equals(outputWarehouseAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that Output Warehouse auto loaded according to the Production Model in summary section",
					"Output Warehouse should be auto loaded according to the Production Model in summary section",
					"Output Warehouse successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Output Warehouse auto loaded according to the Production Model in summary section",
					"Output Warehouse should be auto loaded according to the Production Model in summary section",
					"Output Warehouse doesn't auto loaded according to the Production Model in summary section",
					"fail");
		}

		String loadedWIPWare = getSelectedOptionInDropdown(drop_WIPWarehouseSummarySectionSummaryTabPO);
		if (loadedWIPWare.equals(wipWarehouseAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that WIP Warehouse auto loaded according to the Production Model in summary section",
					"WIP Warehouse should be auto loaded according to the Production Model in summary section",
					"WIP Warehouse successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that WIP Warehouse auto loaded according to the Production Model in summary section",
					"WIP Warehouse should be auto loaded according to the Production Model in summary section",
					"WIP Warehouse doesn't auto loaded according to the Production Model in summary section", "fail");
		}

		String loadedSite = getSelectedOptionInDropdown(drop_siteSummarySectionSummaryTabPO);
		if (loadedSite.equals(siteAaccordigToPMRegression)) {
			writeTestResults("Verify that Site auto loaded according to the Production Model in summary section",
					"Site should be auto loaded according to the Production Model in summary section",
					"Site successfully auto loaded according to the Production Model in summary section", "pass");
		} else {
			writeTestResults("Verify that Site auto loaded according to the Production Model in summary section",
					"Site should be auto loaded according to the Production Model in summary section",
					"Site doesn't auto loaded according to the Production Model in summary section", "fail");
		}

		String loadedCostingPriority = getSelectedOptionInDropdown(
				drop_costingPriorityProductInformationSectionSummaryTabPO);
		if (loadedCostingPriority.equals(costingPriorityAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that Costing Priority auto loaded according to the Production Model in product information section",
					"Costing Priority should be auto loaded according to the Production Model in product information section",
					"Costing Priority successfully auto loaded according to the Production Model in product information section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Costing Priority auto loaded according to the Production Model in product information section",
					"Costing Priority should be auto loaded according to the Production Model in product information section",
					"Costing Priority doesn't auto loaded according to the Production Model in product information section",
					"fail");
		}

		String loadedBoM = getText(drop_boMBoMDetailsSectionSummaryTabPO);
		if (loadedBoM.equals("000095")) {
			writeTestResults(
					"Verify that BoM Document Number auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number should be auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number successfully auto loaded according to the Production Model in bill of material details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoM Document Number auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number should be auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number doesn't auto loaded according to the Production Model in bill of material details section",
					"fail");
		}

		String loadedBoMVarient = getText(div_boMVarientBoMDetailsSectionSummaryTabPO);
		if (loadedBoMVarient.equals(bomVarientAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that BoM Varient auto loaded according to the Production Model in bill of material details section",
					"BoM Varient should be auto loaded according to the Production Model in bill of material details section",
					"BoM Varient successfully auto loaded according to the Production Model in bill of material details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoM Varient auto loaded according to the Production Model in bill of material details section",
					"BoM Varient should be auto loaded according to the Production Model in bill of material details section",
					"BoM Varient doesn't auto loaded according to the Production Model in bill of material details section",
					"fail");
		}

		String loadedBoMVarientDescription = getText(div_varientDescriptionBoMDetailsSectionSummaryTabPO);
		if (loadedBoMVarientDescription.equals(varientDescriptionAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that BoM Varient Description auto loaded according to the Production Model in bill of material details section",
					"BoM Varient Description should be auto loaded according to the Production Model in bill of material details section",
					"BoM Varient Description successfully auto loaded according to the Production Model in bill of material details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoM Varient Description auto loaded according to the Production Model in bill of material details section",
					"BoM Varient Description should be auto loaded according to the Production Model in bill of material details section",
					"BoM Varient Description doesn't auto loaded according to the Production Model in bill of material details section",
					"fail");
		}

		String loadedBoO = getText(div_boOBoODetailsSectionSummaryTabPO);
		if (loadedBoO.equals(booNoAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that BoO Number auto loaded according to the Production Model in bill of operation details section",
					"BoO Number should be auto loaded according to the Production Model in bill of operation details section",
					"BoO Number successfully auto loaded according to the Production Model in bill of operation details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoO Number auto loaded according to the Production Model in bill of operation details section",
					"BoO Number should be auto loaded according to the Production Model in bill of operation details section",
					"BoO Number doesn't auto loaded according to the Production Model in bill of operation details section",
					"fail");
		}

		String loadedBoODescription = getText(div_boODescriptionDetailsSectionSummaryTabPO);
		if (loadedBoODescription.equals(booDescriptionAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that BoO Description auto loaded according to the Production Model in bill of operation details section",
					"BoO Description should be auto loaded according to the Production Model in bill of operation details section",
					"BoO Description successfully auto loaded according to the Production Model in bill of operation details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoO Description auto loaded according to the Production Model in bill of operation details section",
					"BoO Description should be auto loaded according to the Production Model in bill of operation details section",
					"BoO Description doesn't auto loaded according to the Production Model in bill of operation details section",
					"fail");
		}

		/*
		 * There is a bug. Supply production checkbox should be loaded as selected. Need
		 * to change after the bug was fixed
		 */
		if (!isSelected(chk_supplyToOperationPO)) {
			writeTestResults(
					"Verify that Supply to Production checkbox auto loaded according to the Production Model in bill of operation details section",
					"Supply to Production checkbox should be auto loaded according to the Production Model in bill of operation details section",
					"Supply to Production checkbox successfully auto loaded according to the Production Model in bill of operation details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Supply to Production checkbox auto loaded according to the Production Model in bill of operation details section",
					"Supply to Production checkbox should be auto loaded according to the Production Model in bill of operation details section",
					"Supply to Production checkbox doesn't auto loaded according to the Production Model in bill of operation details section",
					"fail");
		}

		String loadedRequestedStartDate = getAttribute(txt_requestStartDateSheduleSectionSummaryTabPO, "value");
		String currentDate = common.currentDate();
		if (loadedRequestedStartDate.equals(currentDate)) {
			writeTestResults("Verify that Reqested Start Date auto loaded in shedule section",
					"Reqested Start Date should be auto loaded in shedule section",
					"Reqested Start Date successfully auto loaded in shedule section", "pass");
		} else {
			writeTestResults("Verify that Reqested Start Date auto loaded in shedule section",
					"Reqested Start Date should be auto loaded in shedule section",
					"Reqested Start Date doesn't auto loaded in shedule section", "fail");
		}

		String loadedRequestedEndDate = getAttribute(txt_requestEndDateSheduleSectionSummaryTabPO, "value");
		if (loadedRequestedEndDate.equals(currentDate)) {
			writeTestResults("Verify that Reqested End Date auto loaded in shedule section",
					"Reqested End Date should be auto loaded in shedule section",
					"Reqested End Date successfully auto loaded in shedule section", "pass");
		} else {
			writeTestResults("Verify that Reqested End Date auto loaded in shedule section",
					"Reqested End Date should be auto loaded in shedule section",
					"Reqested End Date doesn't auto loaded in shedule section", "fail");
		}

		selectText(drop_prioritySummarySectionSummaryTabPO, productionOrderPriorityRegression);
		Thread.sleep(1500);
		String selectedPriority = getSelectedOptionInDropdown(drop_prioritySummarySectionSummaryTabPO);
		if (selectedPriority.equals(productionOrderPriorityRegression)) {
			writeTestResults("Verify that selected priority is display successfully",
					"Selected priority should be display successfully", "Selected priority display successfully",
					"pass");
		} else {
			writeTestResults("Verify that selected priority is display successfully",
					"Selected priority should be display successfully", "Selected priority not display", "fail");
		}

		driver.findElement(getLocator(txt_planedQtyProductInformationSectionSummaryTabPO)).click();
		driver.findElement(getLocator(txt_planedQtyProductInformationSectionSummaryTabPO))
				.sendKeys(planedQtyPORegression);
		Thread.sleep(1500);
		String enteredPlanedQty = getAttribute(txt_planedQtyProductInformationSectionSummaryTabPO, "value");
		if (enteredPlanedQty.equals(planedQtyPORegression)) {
			writeTestResults("Verify that entered Planed Qty is display successfully",
					"Entered Planed Qty should be display successfully", "Entered Planed Qty display successfully",
					"pass");
		} else {
			writeTestResults("Verify that entered Planed Qty is display successfully",
					"Entered Planed Qty should be display successfully", "Entered Planed Qty not display", "fail");
		}

		if (isSelected(chk_mrpProductionOrderRegression)) {
			writeTestResults("Verify that the system is auto enabled the MRP Enabled",
					"The system should be auto enabled the MRP Enabled",
					"The system successfully auto enabled the MRP Enabled", "pass");
		} else {
			writeTestResults("Verify that the system is auto enabled the MRP Enabled",
					"The system should be auto enabled the MRP Enabled",
					"The system doesn't auto enabled the MRP Enabled", "fail");
		}

	}

	public void productionOrder_3_PROD_PO_052() throws Exception {
		selectText(drop_barcodeBookSummarySectionSummaryTabPO, barcodeBookPORegression);
		String selectedBarcodeBook = getSelectedOptionInDropdown(drop_barcodeBookSummarySectionSummaryTabPO);
		if (selectedBarcodeBook.equals(barcodeBookPORegression)) {
			writeTestResults("Verify that user able to select Barcode Book",
					"User should be able to select Barcode Book", "User able to select Barcode Book", "pass");
		} else {
			writeTestResults("Verify that user able to select Barcode Book",
					"User should be able to select Barcode Book", "User not able to select Barcode Book", "fail");
		}

		selectText(drop_productOrderGroupSummarySectionSummaryTabPO, productionOrderGroupRegression);
		Thread.sleep(1000);
		String selectedProductionGroup = getSelectedOptionInDropdown(drop_productOrderGroupSummarySectionSummaryTabPO);
		if (selectedProductionGroup.equals(productionOrderGroupRegression)) {
			writeTestResults("Verify user able to select the Production Group",
					"User should be able to select the Production Group",
					"User successfully select the Production Group", "pass");
		} else {
			writeTestResults("Verify user able to select the Production Group",
					"User should be able to select the Production Group", "User couldn't select the Production Group",
					"fail");
		}

		uniqueDescriptionPO = common.getCurrentDateTimeAsUniqueCode();
		sendKeys(txt_descriptionSummarySectionSummaryTabPO, uniqueDescriptionPO);
		String enteredDescription = getAttribute(txt_descriptionSummarySectionSummaryTabPO, "value");
		if (enteredDescription.equals(uniqueDescriptionPO)) {
			writeTestResults("Verify user able to enter the Description",
					"User should be able to enter the Description", "User successfully enter the Description", "pass");
		} else {
			writeTestResults("Verify user able to enter the Description",
					"User should be able to enter the Description", "User couldn't enter the Description", "fail");
		}

		click(txt_requestStartDateSheduleSectionSummaryTabPO);
		explicitWaitUntillClickable(a_requestStartDatePOWhenDuplicating, 6);
		click(a_requestStartDatePOWhenDuplicating);
		Thread.sleep(2000);
		String selectedRequestedStartDate = getAttribute(txt_requestStartDateSheduleSectionSummaryTabPO, "value");
		String currentYearMonthWithselectedDate = common.currentDate().substring(0, 8) + "01";
		if (selectedRequestedStartDate.equals(currentYearMonthWithselectedDate)) {
			writeTestResults("Verify user able to select Requested Start Date",
					"User should be able to select Requested Start Date",
					"User successfully select Requested Start Date", "pass");
		} else {
			writeTestResults("Verify user able to select Requested Start Date",
					"User should be able to select Requested Start Date", "User couldn't select Requested Start Date",
					"fail");
		}

		click(txt_requestEndDateSheduleSectionSummaryTabPO);
		explicitWaitUntillClickable(a_requestEndDatePOWhenDuplicating, 6);
		click(a_requestEndDatePOWhenDuplicating);
		Thread.sleep(2000);
		String selectedRequestedEndDate = getAttribute(txt_requestEndDateSheduleSectionSummaryTabPO, "value");
		String currentYearMonthWithselectedEndDate = common.currentDate().substring(0, 8) + "28";
		if (selectedRequestedEndDate.equals(currentYearMonthWithselectedEndDate)) {
			writeTestResults("Verify user able to select Requested End Date",
					"User should be able to select Requested End Date", "User successfully select Requested End Date",
					"pass");
		} else {
			writeTestResults("Verify user able to select Requested End Date",
					"User should be able to select Requested End Date", "User couldn't select Requested End Date",
					"fail");
		}

		explicitWaitUntillClickable(btn_draftAndNew, 30);
		click(btn_draftAndNew);
		explicitWait(header_newProductionOrder, 40);
		if (isDisplayed(header_newProductionOrder)) {
			writeTestResults("Verify user able to navigate to new Production Order",
					"User should be able to navigate to new Production Order",
					"User successfully navigate to new Production Order", "pass");
		} else {
			writeTestResults("Verify user able to navigate to new Production Order",
					"User should be able to navigate to new Production Order",
					"User doesn't navigate to new Production Order", "fail");
		}

		((JavascriptExecutor) driver).executeScript("window.open('about:blank','_blank');");
		switchWindow();
		openPage(siteURL);
		explicitWait(navigateMenu, 30);
		Thread.sleep(2000);
		explicitWait(img_navigationPane, 10);
		click(img_navigationPane);
		explicitWait(btn_productionModule, 30);
		if (isDisplayed(btn_productionModule)) {
			writeTestResults("Verify Production Module is displayed", "Production Module should be displayed",
					"Production Module successfully displayed", "pass");
		} else {
			writeTestResults("Verify Production Module is displayed", "Production Module should be displayed",
					"Production Module not displayed", "fail");
		}

		click(btn_productionModule);
		explicitWait(btn_productionOrder, 30);
		if (isDisplayed(btn_productionOrder)) {
			writeTestResults("Verify Production Module is clicked", "Production should be clicked",
					"Production module clicked successfully", "pass");
		} else {
			writeTestResults("Verify Production Module is clicked", "Production should be clicked",
					"Production module doesn't clicked successfully", "fail");
		}

		click(btn_productionOrder);
		explicitWait(header_productionOrderByPage, 30);
		if (isDisplayed(header_productionOrderByPage)) {
			writeTestResults("Verify user can navigate to Production Order by page",
					"User should be navigate to Production Order by page",
					"User successfully navigate to Production Order by page", "pass");
		} else {
			writeTestResults("Verify user can navigate to Production Order by page",
					"User should be navigate to Production Order by page",
					"User doesn't navigate to Production Order by page", "fail");
		}

		click(lbl_recentlyDraftedProductionOrder);

		explicitWait(div_descriptionAfterDraftPO, 30);

		String fondedDescription = getText(div_descriptionAfterDraftPO);

		trackCode = getText(lbl_docNo);

		firstPO = trackCode;
		if (fondedDescription.equals(uniqueDescriptionPO)) {
			writeTestResults(
					"Verify user able to temporarily saves the document and found the relevent drafted Production Order",
					"User should be able to temporarily saves the document and found the relevent drafted Production Order",
					"User successfully temporarily saves the document and found the relevent drafted Production Order",
					"pass");
		} else {
			writeTestResults(
					"Verify user able to temporarily saves the document and found the relevent drafted Production Order",
					"User should be able to temporarily saves the document and found the relevent drafted Production Order",
					"User doesn't temporarily saves the document and/or not found the relevent drafted Production Order",
					"fail");
		}

		switchWindow();

		if (isDisplayed(header_newProductionOrder)) {
			writeTestResults("Verify that user able to navigate to Production Order new form",
					"User should be able to navigate to Production Order new form",
					"User able to navigate to Production Order new form", "pass");
		} else {
			writeTestResults("Verify that user able to navigate to Production Order new form",
					"User should be able to navigate to Production Order new form",
					"User not able navigate to Production Order new form", "fail");
		}

		click(span_productLookupPO);
		explicitWait(lookup_product, 30);
		if (isDisplayed(lookup_product)) {
			writeTestResults("Verify that product lookup is open", "Product lookup should be open",
					"Product lookup successfully open", "pass");
		} else {
			writeTestResults("Verify that product lookup is open", "Product lookup should be open",
					"Product lookup doesn't open", "fail");
		}

		sendKeys(txt_searchProductProductLookup, readProductionData("ProductionTypeProductRegressionPO"));
		String enteredProduct = getAttribute(txt_searchProductProductLookup, "value");
		if (enteredProduct.equals(readProductionData("ProductionTypeProductRegressionPO"))) {
			writeTestResults("Verify that user able to enter the relevent product",
					"User should be able to enter the relevent product", "User successfully enter the relevent product",
					"pass");
		} else {
			writeTestResults("Verify that user able to enter the relevent product",
					"User should be able to enter the relevent product", "User doesn't enter the relevent product",
					"fail");
		}

		click(btn_searchProductProductLookup);
		explicitWaitUntillClickable(td_resultProductRegressionPO.replace("product_code",
				readProductionData("ProductionTypeProductRegressionPO")), 60);
		if (isEnabled(td_resultProductRegressionPO.replace("product_code",
				readProductionData("ProductionTypeProductRegressionPO")))) {
			writeTestResults("Verify that relevent product is available", "Relevent product should be available",
					"Relevent product successfully available", "pass");
		} else {
			writeTestResults("Verify that relevent product is available", "Relevent product should be available",
					"Relevent product not available", "fail");
		}

		doubleClick(td_resultProductRegressionPO.replace("product_code",
				readProductionData("ProductionTypeProductRegressionPO")));
		Thread.sleep(2000);

		String selectedProduct = getAttribute(txt_outputProductProductInformationSectionSummaryTabPO, "value");
		if (selectedProduct.equals(selectedProductFrontPageRegressionPO.replace("product_code",
				readProductionData("ProductionTypeProductRegressionPO")))) {
			writeTestResults("Verify that user able to select the relevent product",
					"User should be able to select the relevent product",
					"User successfully select the relevent product", "pass");
		} else {
			writeTestResults("Verify that user able to select the relevent product",
					"User should be able to select the relevent product", "User couldn't select the relevent product",
					"fail");
		}
	}

	public void productionOrder_4_PROD_PO_052() throws Exception {
		click(btn_productionModelLookupPO);
		explicitWait(lookup_productionModel, 30);
		if (isDisplayed(lookup_productionModel)) {
			writeTestResults("Verify that Production Model lookup is open", "Production Model lookup should be open",
					"Production Model lookup successfully open", "pass");
		} else {
			writeTestResults("Verify that Production Model lookup is open", "Production Model lookup should be open",
					"Production Model lookup doesn't open", "fail");
		}

		sendKeys(txt_searchProductionModel, readProductionData("ProductionModeleMRPEnabledRegressionPO"));
		String enteredProductionModel = getAttribute(txt_searchProductionModel, "value");
		if (enteredProductionModel.equals(readProductionData("ProductionModeleMRPEnabledRegressionPO"))) {
			writeTestResults("Verify that user able to enter the relevent Production Model",
					"User should be able to enter the relevent Production Model",
					"User successfully enter the relevent Production Model", "pass");
		} else {
			writeTestResults("Verify that user able to enter the relevent Production Model",
					"User should be able to enter the relevent Production Model",
					"User doesn't enter the relevent Production Model", "fail");
		}

		click(btn_searchProductionModel);
		explicitWaitUntillClickable(td_resultProductionModelDocReplace.replace("doc_no",
				readProductionData("ProductionModeleMRPEnabledRegressionPO")), 60);
		if (isEnabled(td_resultProductionModelDocReplace.replace("doc_no",
				readProductionData("ProductionModeleMRPEnabledRegressionPO")))) {
			writeTestResults("Verify that relevent Production Model is available",
					"Relevent Production Model should be available", "Relevent Production Model successfully available",
					"pass");
		} else {
			writeTestResults("Verify that relevent Production Model is available",
					"Relevent Production Model should be available", "Relevent Production Model not available", "fail");
		}

		doubleClick(td_resultProductionModelDocReplace.replace("doc_no",
				readProductionData("ProductionModeleMRPEnabledRegressionPO")));
		Thread.sleep(2000);

		String selectedProducttionModel = getAttribute(txt_productionModelSummarySectionSummaryTabPO, "value");
		if (selectedProducttionModel.equals(readProductionData("ProductionModeleMRPEnabledRegressionPO"))) {
			writeTestResults("Verify that user able to select the relevent Production Model",
					"User should be able to select the relevent Production Model",
					"User successfully select the relevent Production Model", "pass");
		} else {
			writeTestResults("Verify that user able to select the relevent Production Model",
					"User should be able to select the relevent Production Model",
					"User couldn't select the relevent Production Model", "fail");
		}

		String loadedProductionUnit = getAttribute(txt_productionUnitSummarySectionSummaryTabPO, "value");
		if (loadedProductionUnit.equals(prductionUnitAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that Production Unit auto loaded according to the Production Model in summary section",
					"Production Unit should be auto loaded according to the Production Model in summary section",
					"Production Unit successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Production Unit auto loaded according to the Production Model in summary section",
					"Production Unit should be auto loaded according to the Production Model in summary section",
					"Production Unit doesn't auto loaded according to the Production Model in summary section", "fail");
		}

		String loadedInputWare = getSelectedOptionInDropdown(drop_inputWarehouseSummarySectionSummaryTabPO);
		if (loadedInputWare.equals(inputWarehouseAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that Input Warehouse auto loaded according to the Production Model in summary section",
					"Input Warehouse should be auto loaded according to the Production Model in summary section",
					"Input Warehouse successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Input Warehouse auto loaded according to the Production Model in summary section",
					"Input Warehouse should be auto loaded according to the Production Model in summary section",
					"Input Warehouse doesn't auto loaded according to the Production Model in summary section", "fail");
		}

		String loadedOutputWare = getSelectedOptionInDropdown(drop_outputWarehouseSummarySectionSummaryTabPO);
		if (loadedOutputWare.equals(outputWarehouseAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that Output Warehouse auto loaded according to the Production Model in summary section",
					"Output Warehouse should be auto loaded according to the Production Model in summary section",
					"Output Warehouse successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Output Warehouse auto loaded according to the Production Model in summary section",
					"Output Warehouse should be auto loaded according to the Production Model in summary section",
					"Output Warehouse doesn't auto loaded according to the Production Model in summary section",
					"fail");
		}

		String loadedWIPWare = getSelectedOptionInDropdown(drop_WIPWarehouseSummarySectionSummaryTabPO);
		if (loadedWIPWare.equals(wipWarehouseAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that WIP Warehouse auto loaded according to the Production Model in summary section",
					"WIP Warehouse should be auto loaded according to the Production Model in summary section",
					"WIP Warehouse successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that WIP Warehouse auto loaded according to the Production Model in summary section",
					"WIP Warehouse should be auto loaded according to the Production Model in summary section",
					"WIP Warehouse doesn't auto loaded according to the Production Model in summary section", "fail");
		}

		String loadedSite = getSelectedOptionInDropdown(drop_siteSummarySectionSummaryTabPO);
		if (loadedSite.equals(siteAaccordigToPMRegression)) {
			writeTestResults("Verify that Site auto loaded according to the Production Model in summary section",
					"Site should be auto loaded according to the Production Model in summary section",
					"Site successfully auto loaded according to the Production Model in summary section", "pass");
		} else {
			writeTestResults("Verify that Site auto loaded according to the Production Model in summary section",
					"Site should be auto loaded according to the Production Model in summary section",
					"Site doesn't auto loaded according to the Production Model in summary section", "fail");
		}

		String loadedCostingPriority = getSelectedOptionInDropdown(
				drop_costingPriorityProductInformationSectionSummaryTabPO);
		if (loadedCostingPriority.equals(costingPriorityAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that Costing Priority auto loaded according to the Production Model in product information section",
					"Costing Priority should be auto loaded according to the Production Model in product information section",
					"Costing Priority successfully auto loaded according to the Production Model in product information section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Costing Priority auto loaded according to the Production Model in product information section",
					"Costing Priority should be auto loaded according to the Production Model in product information section",
					"Costing Priority doesn't auto loaded according to the Production Model in product information section",
					"fail");
		}

		String loadedBoM = getText(drop_boMBoMDetailsSectionSummaryTabPO);
		if (loadedBoM.equals("000095")) {
			writeTestResults(
					"Verify that BoM Document Number auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number should be auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number successfully auto loaded according to the Production Model in bill of material details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoM Document Number auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number should be auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number doesn't auto loaded according to the Production Model in bill of material details section",
					"fail");
		}

		String loadedBoMVarient = getText(div_boMVarientBoMDetailsSectionSummaryTabPO);
		if (loadedBoMVarient.equals(bomVarientAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that BoM Varient auto loaded according to the Production Model in bill of material details section",
					"BoM Varient should be auto loaded according to the Production Model in bill of material details section",
					"BoM Varient successfully auto loaded according to the Production Model in bill of material details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoM Varient auto loaded according to the Production Model in bill of material details section",
					"BoM Varient should be auto loaded according to the Production Model in bill of material details section",
					"BoM Varient doesn't auto loaded according to the Production Model in bill of material details section",
					"fail");
		}

		String loadedBoMVarientDescription = getText(div_varientDescriptionBoMDetailsSectionSummaryTabPO);
		if (loadedBoMVarientDescription.equals(varientDescriptionAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that BoM Varient Description auto loaded according to the Production Model in bill of material details section",
					"BoM Varient Description should be auto loaded according to the Production Model in bill of material details section",
					"BoM Varient Description successfully auto loaded according to the Production Model in bill of material details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoM Varient Description auto loaded according to the Production Model in bill of material details section",
					"BoM Varient Description should be auto loaded according to the Production Model in bill of material details section",
					"BoM Varient Description doesn't auto loaded according to the Production Model in bill of material details section",
					"fail");
		}

		String loadedBoO = getText(div_boOBoODetailsSectionSummaryTabPO);
		if (loadedBoO.equals(booNoAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that BoO Number auto loaded according to the Production Model in bill of operation details section",
					"BoO Number should be auto loaded according to the Production Model in bill of operation details section",
					"BoO Number successfully auto loaded according to the Production Model in bill of operation details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoO Number auto loaded according to the Production Model in bill of operation details section",
					"BoO Number should be auto loaded according to the Production Model in bill of operation details section",
					"BoO Number doesn't auto loaded according to the Production Model in bill of operation details section",
					"fail");
		}

		String loadedBoODescription = getText(div_boODescriptionDetailsSectionSummaryTabPO);
		if (loadedBoODescription.equals(booDescriptionAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that BoO Description auto loaded according to the Production Model in bill of operation details section",
					"BoO Description should be auto loaded according to the Production Model in bill of operation details section",
					"BoO Description successfully auto loaded according to the Production Model in bill of operation details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoO Description auto loaded according to the Production Model in bill of operation details section",
					"BoO Description should be auto loaded according to the Production Model in bill of operation details section",
					"BoO Description doesn't auto loaded according to the Production Model in bill of operation details section",
					"fail");
		}

		/*
		 * There is a bug. Supply production checkbox should be loaded as selected. Need
		 * to change after the bug was fixed
		 */
		if (!isSelected(chk_supplyToOperationPO)) {
			writeTestResults(
					"Verify that Supply to Production checkbox auto loaded according to the Production Model in bill of operation details section",
					"Supply to Production checkbox should be auto loaded according to the Production Model in bill of operation details section",
					"Supply to Production checkbox successfully auto loaded according to the Production Model in bill of operation details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Supply to Production checkbox auto loaded according to the Production Model in bill of operation details section",
					"Supply to Production checkbox should be auto loaded according to the Production Model in bill of operation details section",
					"Supply to Production checkbox doesn't auto loaded according to the Production Model in bill of operation details section",
					"fail");
		}

		String loadedRequestedStartDate = getAttribute(txt_requestStartDateSheduleSectionSummaryTabPO, "value");
		String currentDate = common.currentDate();
		if (loadedRequestedStartDate.equals(currentDate)) {
			writeTestResults("Verify that Reqested Start Date auto loaded in shedule section",
					"Reqested Start Date should be auto loaded in shedule section",
					"Reqested Start Date successfully auto loaded in shedule section", "pass");
		} else {
			writeTestResults("Verify that Reqested Start Date auto loaded in shedule section",
					"Reqested Start Date should be auto loaded in shedule section",
					"Reqested Start Date doesn't auto loaded in shedule section", "fail");
		}

		String loadedRequestedEndDate = getAttribute(txt_requestEndDateSheduleSectionSummaryTabPO, "value");
		if (loadedRequestedEndDate.equals(currentDate)) {
			writeTestResults("Verify that Reqested End Date auto loaded in shedule section",
					"Reqested End Date should be auto loaded in shedule section",
					"Reqested End Date successfully auto loaded in shedule section", "pass");
		} else {
			writeTestResults("Verify that Reqested End Date auto loaded in shedule section",
					"Reqested End Date should be auto loaded in shedule section",
					"Reqested End Date doesn't auto loaded in shedule section", "fail");
		}

		selectText(drop_prioritySummarySectionSummaryTabPO, productionOrderPriorityRegression);
		Thread.sleep(1500);
		String selectedPriority = getSelectedOptionInDropdown(drop_prioritySummarySectionSummaryTabPO);
		if (selectedPriority.equals(productionOrderPriorityRegression)) {
			writeTestResults("Verify that selected priority is display successfully",
					"Selected priority should be display successfully", "Selected priority display successfully",
					"pass");
		} else {
			writeTestResults("Verify that selected priority is display successfully",
					"Selected priority should be display successfully", "Selected priority not display", "fail");
		}

		driver.findElement(getLocator(txt_planedQtyProductInformationSectionSummaryTabPO)).click();
		driver.findElement(getLocator(txt_planedQtyProductInformationSectionSummaryTabPO))
				.sendKeys(planedQtyPORegression);
		Thread.sleep(1500);
		String enteredPlanedQty = getAttribute(txt_planedQtyProductInformationSectionSummaryTabPO, "value");
		if (enteredPlanedQty.equals(planedQtyPORegression)) {
			writeTestResults("Verify that entered Planed Qty is display successfully",
					"Entered Planed Qty should be display successfully", "Entered Planed Qty display successfully",
					"pass");
		} else {
			writeTestResults("Verify that entered Planed Qty is display successfully",
					"Entered Planed Qty should be display successfully", "Entered Planed Qty not display", "fail");
		}

		if (isSelected(chk_mrpProductionOrderRegression)) {
			writeTestResults("Verify that the system is auto enabled the MRP Enabled",
					"The system should be auto enabled the MRP Enabled",
					"The system successfully auto enabled the MRP Enabled", "pass");
		} else {
			writeTestResults("Verify that the system is auto enabled the MRP Enabled",
					"The system should be auto enabled the MRP Enabled",
					"The system doesn't auto enabled the MRP Enabled", "fail");
		}
		selectText(drop_barcodeBookSummarySectionSummaryTabPO, barcodeBookPORegression);
		String selectedBarcodeBook = getSelectedOptionInDropdown(drop_barcodeBookSummarySectionSummaryTabPO);
		if (selectedBarcodeBook.equals(barcodeBookPORegression)) {
			writeTestResults("Verify that user able to select Barcode Book",
					"User should be able to select Barcode Book", "User able to select Barcode Book", "pass");
		} else {
			writeTestResults("Verify that user able to select Barcode Book",
					"User should be able to select Barcode Book", "User not able to select Barcode Book", "fail");
		}

		selectText(drop_productOrderGroupSummarySectionSummaryTabPO, productionOrderGroupRegression);
		Thread.sleep(1000);
		String selectedProductionGroup = getSelectedOptionInDropdown(drop_productOrderGroupSummarySectionSummaryTabPO);
		if (selectedProductionGroup.equals(productionOrderGroupRegression)) {
			writeTestResults("Verify user able to select the Production Group",
					"User should be able to select the Production Group",
					"User successfully select the Production Group", "pass");
		} else {
			writeTestResults("Verify user able to select the Production Group",
					"User should be able to select the Production Group", "User couldn't select the Production Group",
					"fail");
		}

		sendKeys(txt_descriptionSummarySectionSummaryTabPO, productionOrderDescriptionRegression);
		String enteredDescription = getAttribute(txt_descriptionSummarySectionSummaryTabPO, "value");
		if (enteredDescription.equals(productionOrderDescriptionRegression)) {
			writeTestResults("Verify user able to enter the Description",
					"User should be able to enter the Description", "User successfully enter the Description", "pass");
		} else {
			writeTestResults("Verify user able to enter the Description",
					"User should be able to enter the Description", "User couldn't enter the Description", "fail");
		}

		click(txt_requestStartDateSheduleSectionSummaryTabPO);
		explicitWaitUntillClickable(a_requestStartDatePOWhenDuplicating, 6);
		click(a_requestStartDatePOWhenDuplicating);
		Thread.sleep(2000);
		String selectedRequestedStartDate = getAttribute(txt_requestStartDateSheduleSectionSummaryTabPO, "value");
		String currentYearMonthWithselectedDate = common.currentDate().substring(0, 8) + "01";
		if (selectedRequestedStartDate.equals(currentYearMonthWithselectedDate)) {
			writeTestResults("Verify user able to select Requested Start Date",
					"User should be able to select Requested Start Date",
					"User successfully select Requested Start Date", "pass");
		} else {
			writeTestResults("Verify user able to select Requested Start Date",
					"User should be able to select Requested Start Date", "User couldn't select Requested Start Date",
					"fail");
		}

		click(txt_requestEndDateSheduleSectionSummaryTabPO);
		explicitWaitUntillClickable(a_requestEndDatePOWhenDuplicating, 6);
		click(a_requestEndDatePOWhenDuplicating);
		Thread.sleep(2000);
		String selectedRequestedEndDate = getAttribute(txt_requestEndDateSheduleSectionSummaryTabPO, "value");
		String currentYearMonthWithselectedEndDate = common.currentDate().substring(0, 8) + "28";
		if (selectedRequestedEndDate.equals(currentYearMonthWithselectedEndDate)) {
			writeTestResults("Verify user able to select Requested End Date",
					"User should be able to select Requested End Date", "User successfully select Requested End Date",
					"pass");
		} else {
			writeTestResults("Verify user able to select Requested End Date",
					"User should be able to select Requested End Date", "User couldn't select Requested End Date",
					"fail");
		}

		click(btn_draft);
		explicitWait(header_draftedProductionOrder, 60);

		trackCode = getText(lbl_docNo);

		String secondPO = trackCode;
		if (isDisplayed(header_draftedProductionOrder)) {
			writeTestResults("Verify user able to draft the second Production Order",
					"User should be able to draft thesecond Production Order",
					"User successfully draft thesecond Production Order", "pass");
		} else {
			writeTestResults("Verify user able to draft thesecond Production Order",
					"User should be able to draft thesecond Production Order",
					"User doesn't draft thesecond Production Order", "fail");
		}

		switchWindow();
		goBack();

		explicitWait(txt_searchProductionOrder2, 30);
		sendKeys(txt_searchProductionOrder2, firstPO);
		pressEnter(txt_searchProductionOrder2);
		Thread.sleep(2000);
		explicitWait(lnk_releventDraftedProductionOrderDocReplace.replace("doc_no", firstPO), 40);

		if (isDisplayed(lnk_releventDraftedProductionOrderDocReplace.replace("doc_no", firstPO))) {
			writeTestResults("Verify user able to find first Production Order the Production Order by page",
					"User should be able to find first Production Order the Production Order by page",
					"User successfully find first Production Order the Production Order by page", "pass");
		} else {
			writeTestResults("Verify user able to find first Production Order the Production Order by page",
					"User should be able to find first Production Order the Production Order by page",
					"User doesn't find first Production Order the Production Order by page", "fail");
		}

		sendKeys(txt_searchProductionOrder2, secondPO);
		pressEnter(txt_searchProductionOrder2);
		Thread.sleep(2000);
		explicitWait(lnk_releventDraftedProductionOrderDocReplace.replace("doc_no", secondPO), 40);

		if (isDisplayed(lnk_releventDraftedProductionOrderDocReplace.replace("doc_no", secondPO))) {
			writeTestResults("Verify user able to find second Production Order the Production Order by page",
					"User should be able to find second Production Order the Production Order by page",
					"User successfully find second Production Order the Production Order by page", "pass");
		} else {
			writeTestResults("Verify user able to find second Production Order the Production Order by page",
					"User should be able to find second Production Order the Production Order by page",
					"User doesn't find second Production Order the Production Order by page", "fail");
		}
	}

	/* Production Control */
	/* PROD_PC_001_002_010 */
	public void productionControl_PROD_PC_001_002_010() throws Exception {
		loginBiletaAutomation();

		explicitWaitUntillClickable(img_navigationPane, 60);
		Thread.sleep(2000);
		explicitWaitUntillClickable(img_navigationPane, 20);
		if (click(img_navigationPane)) {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User successfully click navigation pane", "pass");
		} else {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User couldn't click navigation pane", "fail");
		}

		explicitWait(btn_production, 4);
		if (isEnabledQuickCheck(btn_production)) {
			writeTestResults("Verify user able to view the \"Production\" module under main navigation menu",
					"User should be able to view the \"Production\" module under main navigation menu",
					"User successfully view the \"Production\" module under main navigation menu", "pass");
		} else {
			writeTestResults("Verify user able to view the \"Production\" module under main navigation menu",
					"User should be able to view the \"Production\" module under main navigation menu",
					"User couldn't view the \"Production\" module under main navigation menu", "fail");
		}

		click(btn_production);

		explicitWait(btn_productionControl, 4);
		if (isEnabledQuickCheck(btn_productionControl)) {
			writeTestResults("Verify user able to view the \"Production Control\" option under production sub menu",
					"User should be able to view the \"Production Control\" option under production sub menu",
					"User able to view the \"Production Control\" option under production sub menu", "pass");
		} else {
			writeTestResults("Verify user able to view the \"Production Control\" option under production sub menu",
					"User should be able to view the \"Production Control\" option under production sub menu",
					"User couldn't view the \"Production Control\" option under production sub menu", "pass");
		}

		click(btn_productionControl);

		explicitWait(header_productionControlByPage, 60);
		if (isDisplayed(header_productionControlByPage)) {
			writeTestResults("Verify user able to navigate to Production Control by-page",
					"User should be able to navigate to Production Control by-page",
					"User successfully navigate to Production Control by-page", "pass");
		} else {
			writeTestResults("Verify user able to navigate to Production Control by-page",
					"User should be able to navigate to Production Control by-page",
					"User coulsn't navigate to Production Control by-page", "fail");
		}

		if (isDisplayed(div_columnProductionOrderStatusPC)) {
			writeTestResults("Verify that Production Order Status column header is display same as earlier",
					"Production Order Status column header should display same as earlier",
					"Production Order Status column header successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that Production Order Status column header is display same as earlier",
					"Production Order Status column header should display same as earlier",
					"Production Order Status column header doesn't display same as earlier", "fail");
		}

		if (isDisplayed(div_columnPostBusinessUnitPC)) {
			writeTestResults("Verify that Post Business Unit column header is display same as earlier",
					"Post Business Unit column header should display same as earlier",
					"Post Business Unit column header successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that Post Business Unit column header is display same as earlier",
					"Post Business Unit column header should display same as earlier",
					"Post Business Unit column header doesn't display same as earlier", "fail");
		}

		if (isDisplayed(div_columnDocNoPC)) {
			writeTestResults("Verify that Doc No column header is display same as earlier",
					"Doc No column header should display same as earlier",
					"Doc No column header successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that Doc No column header is display same as earlier",
					"Doc No column header should display same as earlier",
					"Doc No column header doesn't display same as earlier", "fail");
		}

		if (isDisplayed(div_columnPriorityPC)) {
			writeTestResults("Verify that Priority column header is display same as earlier",
					"Priority column header should display same as earlier",
					"Priority column header successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that Priority column header is display same as earlier",
					"Priority column header should display same as earlier",
					"Priority column header doesn't display same as earlier", "fail");
		}

		if (isDisplayed(div_columnProductionOrderNoPC)) {
			writeTestResults("Verify that Production Order No column header is display same as earlier",
					"Production Order No column header should display same as earlier",
					"Production Order No column header successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that Production Order No column header is display same as earlier",
					"Production Order No column header should display same as earlier",
					"Production Order No column header doesn't display same as earlier", "fail");
		}

		if (isDisplayed(div_columnOutputProductPC)) {
			writeTestResults("Verify that Output Product column header is display same as earlier",
					"Output Product column header should display same as earlier",
					"Output Product column header successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that Output Product column header is display same as earlier",
					"Output Product column header should display same as earlier",
					"Output Product column header doesn't display same as earlier", "fail");
		}

		if (isDisplayed(div_columnOpenQtyPC)) {
			writeTestResults("Verify that Open Qty column header is display same as earlier",
					"Open Qty column header should display same as earlier",
					"Open Qty column header successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that Open Qty column header is display same as earlier",
					"Open Qty column header should display same as earlier",
					"Open Qty column header doesn't display same as earlier", "fail");
		}

		if (isDisplayed(div_columnRequestedStartDatePC)) {
			writeTestResults("Verify that Request Statrt Date column header is display same as earlier",
					"Request Statrt Date column header should display same as earlier",
					"Request Statrt Date column header successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that Request Statrt Date column header is display same as earlier",
					"Request Statrt Date column header should display same as earlier",
					"Request Statrt Date column header doesn't display same as earlier", "fail");
		}

		click(btn_action);
		explicitWaitUntillClickable(btn_runMRP, 8);
		if (isDisplayed(btn_runMRP)) {
			writeTestResults("Verify that the Run MRP button is visible under action dropdown",
					"Run MRP button should visible under action dropdown",
					"Run MRP button successfully visible under action dropdown", "pass");
		} else {
			writeTestResults("Verify that the Run MRP button is visible under action dropdown",
					"Run MRP button should visible under action dropdown",
					"Run MRP button not visible under action dropdown", "fail");
		}

		click(btn_runMRP);
		explicitWait(div_productionOrderSelectValidationPC, 8);
		if (isDisplayed(div_productionOrderSelectValidationPC)) {
			writeTestResults(
					"Verfiy that the system is dispalyed the below mentioed validation message Validation message:Please select the production order(s) to take action..!",
					"The system should be dispalyed the below mentioed validation message Validation message:Please select the production order(s) to take action..!",
					"The system is dispalyed the below mentioed validation message Validation message:Please select the production order(s) to take action..!",
					"pass");
		} else {
			writeTestResults(
					"Verfiy that the system is dispalyed the below mentioed validation message Validation message:Please select the production order(s) to take action..!",
					"The system should be dispalyed the below mentioed validation message Validation message:Please select the production order(s) to take action..!",
					"The system doesn't dispalyed the below mentioed validation message Validation message:Please select the production order(s) to take action..!",
					"fail");
		}

	}

	/* PROD_PC_003_005_006_007_008_009_011_012 */
	public void productionOrderForProductionControl() throws Exception {
		loginBiletaAutomation();
		explicitWaitUntillClickable(img_navigationPane, 60);
		Thread.sleep(2000);
		explicitWaitUntillClickable(img_navigationPane, 20);
		if (click(img_navigationPane)) {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User successfully click navigation pane", "pass");
		} else {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User couldn't click navigation pane", "fail");
		}

		explicitWait(btn_production, 4);
		if (isEnabledQuickCheck(btn_production)) {
			writeTestResults("Verify user able to view the \"Production\" module under main navigation menu",
					"User should be able to view the \"Production\" module under main navigation menu",
					"User successfully view the \"Production\" module under main navigation menu", "pass");
		} else {
			writeTestResults("Verify user able to view the \"Production\" module under main navigation menu",
					"User should be able to view the \"Production\" module under main navigation menu",
					"User couldn't view the \"Production\" module under main navigation menu", "fail");
		}

		click(btn_production);

		explicitWait(btn_productionOrder, 4);
		if (isEnabledQuickCheck(btn_productionOrder)) {
			writeTestResults("Verify user able to view the \"Production Order\" option under production sub menu",
					"User should be able to view the \"Production Order\" option under production sub menu",
					"User able to view the \"Production Order\" option under production sub menu", "pass");
		} else {
			writeTestResults("Verify user able to view the \"Production Order\" option under production sub menu",
					"User should be able to view the \"Production Order\" option under production sub menu",
					"User couldn't view the \"Production Order\" option under production sub menu", "pass");
		}

		click(btn_productionOrder);

		explicitWait(header_productionOrderByPage, 60);
		if (isDisplayed(header_productionOrderByPage)) {
			writeTestResults("Verify user able to navigate to Production Order by-page",
					"User should be able to navigate to Production Order by-page",
					"User successfully navigate to Production Order by-page", "pass");
		} else {
			writeTestResults("Verify user able to navigate to Production Order by-page",
					"User should be able to navigate to Production Order by-page",
					"User coulsn't navigate to Production Order by-page", "fail");
		}

		if (isDisplayed(header_productionOrderDocNoColumnOnByPage)) {
			writeTestResults("Verify that 'Doc No' column is displayed as earlier",
					"'Doc No' column should be displayed as earlier",
					"'Doc No' column successfully displayed as earlier", "pass");
		} else {
			writeTestResults("Verify that 'Doc No' column is displayed as earlier",
					"'Doc No' column should be displayed as earlier", "'Doc No' column doesn't displayed as earlier",
					"fail");
		}

		int docLinkCount = getCount(lnk_countProductionOrdersOnByPAge);
		if (docLinkCount == 20) {
			writeTestResults("Check whether Production Order's 'Order No' is displaying as a blue color link",
					"Production Order's 'Order No' should be display as a blue color link",
					"Production Order's 'Order No' successfully displayed as a blue color link", "pass");
		} else {
			writeTestResults("Check whether Production Order's 'Order No' is displaying as a blue color link",
					"Production Order's 'Order No' should be display as a blue color link",
					"Production Order's 'Order No' do not display as a blue color link", "fail");
		}

		click(btn_newProductionOrder);
		explicitWait(header_newProductionOrder, 60);
		if (isDisplayed(header_newProductionOrder)) {
			writeTestResults("Verify user able to navigate to Production Order new-form",
					"User should be able to navigate to Production Order new-form",
					"User successfully navigate to Production Order new-form", "pass");
		} else {
			writeTestResults("Verify user able to navigate to Production Order new-form",
					"User should be able to navigate to Production Order new-form",
					"User coulsn't navigate to Production Order new-form", "fail");
		}

		String orderStructureTabStatus = getAttribute(li_orderStructureTabPO, "disabled");
		String costingSummaryTabStatus = getAttribute(li_costingSummaryTabPO, "disabled");
		if (orderStructureTabStatus.equals("true") && costingSummaryTabStatus.equals("true")) {
			writeTestResults("Verify that 'Order Structure' and 'Costing Summary' tabs are disabled",
					"'Order Structure' and 'Costing Summary' tabs should be disabled",
					"'Order Structure' and 'Costing Summary' tabs successfully disabled", "pass");
		} else {
			writeTestResults("Verify that 'Order Structure' and 'Costing Summary' tabs are disabled",
					"'Order Structure' and 'Costing Summary' tabs should be disabled",
					"'Order Structure' and 'Costing Summary' tabs not disabled", "fail");
		}

		if (isDisplayed(drop_productOrderGroupSummarySectionSummaryTabPO)
				&& isDisplayed(txt_descriptionSummarySectionSummaryTabPO)
				&& isDisplayed(txt_productionModelSummarySectionSummaryTabPO)
				&& isDisplayed(txt_costEstimationSummarySectionSummaryTabPO)
				&& isDisplayed(drop_barcodeBookSummarySectionSummaryTabPO)
				&& isDisplayed(div_batchNoSummarySectionSummaryTabPO)
				&& isDisplayed(txt_productionLotNoSummarySectionSummaryTabPO)
				&& isDisplayed(drop_prioritySummarySectionSummaryTabPO)
				&& isDisplayed(txt_productionUnitSummarySectionSummaryTabPO)
				&& isDisplayed(drop_inputWarehouseSummarySectionSummaryTabPO)
				&& isDisplayed(drop_outputWarehouseSummarySectionSummaryTabPO)
				&& isDisplayed(drop_WIPWarehouseSummarySectionSummaryTabPO)
				&& isDisplayed(drop_siteSummarySectionSummaryTabPO)) {
			writeTestResults(
					"Verify that the system is displayed fields mentioned in parentheses (Production Order Group,Description,Production Model,Cost Estimation,Barcode Book No,Batch No,Production Lot No,Priority,Production Unit,Input Warehouses,Output Warehouses,WIP Warehouses,Site)",
					"The system should be displayed fields mentioned in parentheses (Production Order Group,Description,Production Model,Cost Estimation,Barcode Book No,Batch No,Production Lot No,Priority,Production Unit,Input Warehouses,Output Warehouses,WIP Warehouses,Site)",
					"The system successfully displayed fields mentioned in parentheses (Production Order Group,Description,Production Model,Cost Estimation,Barcode Book No,Batch No,Production Lot No,Priority,Production Unit,Input Warehouses,Output Warehouses,WIP Warehouses,Site)",
					"pass");
		} else {
			writeTestResults(
					"Verify that the system is displayed fields mentioned in parentheses (Production Order Group,Description,Production Model,Cost Estimation,Barcode Book No,Batch No,Production Lot No,Priority,Production Unit,Input Warehouses,Output Warehouses,WIP Warehouses,Site)",
					"The system should be displayed fields mentioned in parentheses (Production Order Group,Description,Production Model,Cost Estimation,Barcode Book No,Batch No,Production Lot No,Priority,Production Unit,Input Warehouses,Output Warehouses,WIP Warehouses,Site)",
					"The system doesn't displayed fields mentioned in parentheses (Production Order Group,Description,Production Model,Cost Estimation,Barcode Book No,Batch No,Production Lot No,Priority,Production Unit,Input Warehouses,Output Warehouses,WIP Warehouses,Site)",
					"fail");
		}

		if (isDisplayed(txt_outputProductProductInformationSectionSummaryTabPO)
				&& isDisplayed(txt_planedQtyProductInformationSectionSummaryTabPO)
				&& isDisplayed(txt_producedQtyProductInformationSectionSummaryTabPO)
				&& isDisplayed(txt_balanceQtyProductInformationSectionSummaryTabPO)
				&& isDisplayed(txt_minimumLotSizeProductInformationSectionSummaryTabPO)
				&& isDisplayed(txt_maximumLotSizeProductInformationSectionSummaryTabPO)
				&& isDisplayed(txt_batchQtyProductInformationSectionSummaryTabPO)
				&& isDisplayed(txt_productionScrapProductInformationSectionSummaryTabPO)
				&& isDisplayed(drop_costingPriorityProductInformationSectionSummaryTabPO)
				&& isDisplayed(txt_standardCostProductInformationSectionSummaryTabPO)
				&& isDisplayed(chk_planningEnabledtProductInformationSectionSummaryTabPO)
				&& isDisplayed(chk_MRPEnabledtProductInformationSectionSummaryTabPO)
				&& isDisplayed(chk_infiniteProductInformationSectionSummaryTabPO)) {
			writeTestResults(
					"Verify that the system is displayed fields mentioned in parentheses (Output Product,Planned Qty,Produced Qty,Balance Qty,Minimum Lot Size,Maximum Lot Size,Batch Qty,Production Scrap(%),Costing Priority,Standard Cost,Planning Enabled,MRP EnabledInfinite)",
					"The system should be displayed fields mentioned in parentheses (Output Product,Planned Qty,Produced Qty,Balance Qty,Minimum Lot Size,Maximum Lot Size,Batch Qty,Production Scrap(%),Costing Priority,Standard Cost,Planning Enabled,MRP EnabledInfinite)",
					"The system successfully displayed fields mentioned in parentheses (Output Product,Planned Qty,Produced Qty,Balance Qty,Minimum Lot Size,Maximum Lot Size,Batch Qty,Production Scrap(%),Costing Priority,Standard Cost,Planning Enabled,MRP EnabledInfinite)",
					"pass");
		} else {
			writeTestResults(
					"Verify that the system is displayed fields mentioned in parentheses (Output Product,Planned Qty,Produced Qty,Balance Qty,Minimum Lot Size,Maximum Lot Size,Batch Qty,Production Scrap(%),Costing Priority,Standard Cost,Planning Enabled,MRP EnabledInfinite)",
					"The system should be displayed fields mentioned in parentheses (Output Product,Planned Qty,Produced Qty,Balance Qty,Minimum Lot Size,Maximum Lot Size,Batch Qty,Production Scrap(%),Costing Priority,Standard Cost,Planning Enabled,MRP EnabledInfinite)",
					"The system doesn't displayed fields mentioned in parentheses (Output Product,Planned Qty,Produced Qty,Balance Qty,Minimum Lot Size,Maximum Lot Size,Batch Qty,Production Scrap(%),Costing Priority,Standard Cost,Planning Enabled,MRP EnabledInfinite)",
					"fail");
		}

		if (isDisplayed(drop_boMBoMDetailsSectionSummaryTabPO)
				&& isDisplayed(div_boMVarientBoMDetailsSectionSummaryTabPO)
				&& isDisplayed(div_varientDescriptionBoMDetailsSectionSummaryTabPO)) {
			writeTestResults(
					"Verify that the system is displayed fields mentioned in parentheses (BoM, BoM Variant,Variant Description)",
					"The system should be displayed fields mentioned in parentheses (BoM, BoM Variant,Variant Description)",
					"The system successfully displayed fields mentioned in parentheses (BoM, BoM Variant,Variant Description)",
					"pass");
		} else {
			writeTestResults(
					"Verify that the system is displayed fields mentioned in parentheses (BoM, BoM Variant,Variant Description)",
					"The system should be displayed fields mentioned in parentheses (BoM, BoM Variant,Variant Description)",
					"The system doesn't displayed fields mentioned in parentheses (BoM, BoM Variant,Variant Description)",
					"fail");
		}

		if (isDisplayed(div_boOBoODetailsSectionSummaryTabPO)
				&& isDisplayed(div_boODescriptionDetailsSectionSummaryTabPO)
				&& isDisplayed(chk_supplyToOperationDetailsSectionSummaryTabPO)) {
			writeTestResults(
					"Verify that the system is displayed fields mentioned in parentheses (BoO No, BoO Description, Supply to Production)",
					"The system should be displayed fields mentioned in parentheses (BoO No, BoO Description, Supply to Production)",
					"The system successfully displayed fields mentioned in parentheses (BoO No, BoO Description, Supply to Production)",
					"pass");
		} else {
			writeTestResults(
					"Verify that the system is displayed fields mentioned in parentheses (BoO No, BoO Description, Supply to Production)",
					"The system should be displayed fields mentioned in parentheses (BoO No, BoO Description, Supply to Production)",
					"The system doesn't displayed fields mentioned in parentheses (BoO No, BoO Description, Supply to Production)",
					"fail");
		}

		/* PROD_PO_009 */
		if (isDisplayed(div_currentStatusStatusSectionSummaryTabPO)
				&& isDisplayed(div_shedulingStatusStatusSectionSummaryTabPO)) {
			writeTestResults(
					"Verify that the system is displayed fields mentioned in parentheses (Current Status, Scheduling Status)",
					"The system should be displayed fields mentioned in parentheses (Current Status, Scheduling Status)",
					"The system successfully displayed fields mentioned in parentheses (Current Status, Scheduling Status)",
					"pass");
		} else {
			writeTestResults(
					"Verify that the system is displayed fields mentioned in parentheses (Current Status, Scheduling Status)",
					"The system should be displayed fields mentioned in parentheses (Current Status, Scheduling Status)",
					"The system doesn't displayed fields mentioned in parentheses (Current Status, Scheduling Status)",
					"fail");
		}

		if (isDisplayed(txt_requestStartDateSheduleSectionSummaryTabPO)
				&& isDisplayed(txt_requestEndDateSheduleSectionSummaryTabPO)
				&& isDisplayed(div_earliestStartDateSheduleSectionSummaryTabPO)
				&& isDisplayed(div_earliestEndDateSheduleSectionSummaryTabPO)
				&& isDisplayed(div_latestStartDateSheduleSectionSummaryTabPO)
				&& isDisplayed(div_latestEndDateSheduleSectionSummaryTabPO)) {
			writeTestResults(
					"Verify that the system is displayed fields mentioned in parentheses (Requested Start Date, Requested End Date, Earliest Start Date, Earliest End Date, Latest Start Date, Latest End Date	)",
					"The system should be displayed fields mentioned in parentheses (Requested Start Date, Requested End Date, Earliest Start Date, Earliest End Date, Latest Start Date, Latest End Date	)",
					"The system successfully displayed fields mentioned in parentheses (Requested Start Date, Requested End Date, Earliest Start Date, Earliest End Date, Latest Start Date, Latest End Date	)",
					"pass");
		} else {
			writeTestResults(
					"Verify that the system is displayed fields mentioned in parentheses (Requested Start Date, Requested End Date, Earliest Start Date, Earliest End Date, Latest Start Date, Latest End Date	)",
					"The system should be displayed fields mentioned in parentheses (Requested Start Date, Requested End Date, Earliest Start Date, Earliest End Date, Latest Start Date, Latest End Date	)",
					"The system doesn't displayed fields mentioned in parentheses (Requested Start Date, Requested End Date, Earliest Start Date, Earliest End Date, Latest Start Date, Latest End Date	)",
					"fail");
		}

		postBusinessUnit = getText(a_postBusinessUnitHeaderDeatailsPO);

		click(span_productLookupPO);
		explicitWait(lookup_product, 30);
		if (isDisplayed(lookup_product)) {
			writeTestResults("Verify that product lookup is open", "Product lookup should be open",
					"Product lookup successfully open", "pass");
		} else {
			writeTestResults("Verify that product lookup is open", "Product lookup should be open",
					"Product lookup doesn't open", "fail");
		}

		sendKeys(txt_searchProductProductLookup, readProductionData("ProductionTypeProductRegressionPO"));
		String enteredProduct = getAttribute(txt_searchProductProductLookup, "value");
		if (enteredProduct.equals(readProductionData("ProductionTypeProductRegressionPO"))) {
			writeTestResults("Verify that user able to enter the relevent product",
					"User should be able to enter the relevent product", "User successfully enter the relevent product",
					"pass");
		} else {
			writeTestResults("Verify that user able to enter the relevent product",
					"User should be able to enter the relevent product", "User doesn't enter the relevent product",
					"fail");
		}

		click(btn_searchProductProductLookup);
		explicitWaitUntillClickable(td_resultProductRegressionPO.replace("product_code",
				readProductionData("ProductionTypeProductRegressionPO")), 60);
		if (isEnabled(td_resultProductRegressionPO.replace("product_code",
				readProductionData("ProductionTypeProductRegressionPO")))) {
			writeTestResults("Verify that relevent product is available", "Relevent product should be available",
					"Relevent product successfully available", "pass");
		} else {
			writeTestResults("Verify that relevent product is available", "Relevent product should be available",
					"Relevent product not available", "fail");
		}

		doubleClick(td_resultProductRegressionPO.replace("product_code",
				readProductionData("ProductionTypeProductRegressionPO")));
		Thread.sleep(2000);

		String selectedProduct = getAttribute(txt_outputProductProductInformationSectionSummaryTabPO, "value");
		if (selectedProduct.equals(selectedProductFrontPageRegressionPO.replace("product_code",
				readProductionData("ProductionTypeProductRegressionPO")))) {
			writeTestResults("Verify that user able to select the relevent product",
					"User should be able to select the relevent product",
					"User successfully select the relevent product", "pass");
		} else {
			writeTestResults("Verify that user able to select the relevent product",
					"User should be able to select the relevent product", "User couldn't select the relevent product",
					"fail");
		}

		click(btn_productionModelLookupPO);
		explicitWait(lookup_productionModel, 30);
		if (isDisplayed(lookup_productionModel)) {
			writeTestResults("Verify that Production Model lookup is open", "Production Model lookup should be open",
					"Production Model lookup successfully open", "pass");
		} else {
			writeTestResults("Verify that Production Model lookup is open", "Production Model lookup should be open",
					"Production Model lookup doesn't open", "fail");
		}

		sendKeys(txt_searchProductionModel, readProductionData("ProductionModeleMRPEnabledRegressionPO"));
		String enteredProductionModel = getAttribute(txt_searchProductionModel, "value");
		if (enteredProductionModel.equals(readProductionData("ProductionModeleMRPEnabledRegressionPO"))) {
			writeTestResults("Verify that user able to enter the relevent Production Model",
					"User should be able to enter the relevent Production Model",
					"User successfully enter the relevent Production Model", "pass");
		} else {
			writeTestResults("Verify that user able to enter the relevent Production Model",
					"User should be able to enter the relevent Production Model",
					"User doesn't enter the relevent Production Model", "fail");
		}

		click(btn_searchProductionModel);
		explicitWaitUntillClickable(td_resultProductionModelDocReplace.replace("doc_no",
				readProductionData("ProductionModeleMRPEnabledRegressionPO")), 60);
		if (isEnabled(td_resultProductionModelDocReplace.replace("doc_no",
				readProductionData("ProductionModeleMRPEnabledRegressionPO")))) {
			writeTestResults("Verify that relevent Production Model is available",
					"Relevent Production Model should be available", "Relevent Production Model successfully available",
					"pass");
		} else {
			writeTestResults("Verify that relevent Production Model is available",
					"Relevent Production Model should be available", "Relevent Production Model not available", "fail");
		}

		doubleClick(td_resultProductionModelDocReplace.replace("doc_no",
				readProductionData("ProductionModeleMRPEnabledRegressionPO")));
		Thread.sleep(2000);

		String selectedProducttionModel = getAttribute(txt_productionModelSummarySectionSummaryTabPO, "value");
		if (selectedProducttionModel.equals(readProductionData("ProductionModeleMRPEnabledRegressionPO"))) {
			writeTestResults("Verify that user able to select the relevent Production Model",
					"User should be able to select the relevent Production Model",
					"User successfully select the relevent Production Model", "pass");
		} else {
			writeTestResults("Verify that user able to select the relevent Production Model",
					"User should be able to select the relevent Production Model",
					"User couldn't select the relevent Production Model", "fail");
		}

		String loadedProductionUnit = getAttribute(txt_productionUnitSummarySectionSummaryTabPO, "value");
		if (loadedProductionUnit.equals(prductionUnitAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that Production Unit auto loaded according to the Production Model in summary section",
					"Production Unit should be auto loaded according to the Production Model in summary section",
					"Production Unit successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Production Unit auto loaded according to the Production Model in summary section",
					"Production Unit should be auto loaded according to the Production Model in summary section",
					"Production Unit doesn't auto loaded according to the Production Model in summary section", "fail");
		}

		String loadedInputWare = getSelectedOptionInDropdown(drop_inputWarehouseSummarySectionSummaryTabPO);
		if (loadedInputWare.equals(inputWarehouseAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that Input Warehouse auto loaded according to the Production Model in summary section",
					"Input Warehouse should be auto loaded according to the Production Model in summary section",
					"Input Warehouse successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Input Warehouse auto loaded according to the Production Model in summary section",
					"Input Warehouse should be auto loaded according to the Production Model in summary section",
					"Input Warehouse doesn't auto loaded according to the Production Model in summary section", "fail");
		}

		String loadedOutputWare = getSelectedOptionInDropdown(drop_outputWarehouseSummarySectionSummaryTabPO);
		if (loadedOutputWare.equals(outputWarehouseAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that Output Warehouse auto loaded according to the Production Model in summary section",
					"Output Warehouse should be auto loaded according to the Production Model in summary section",
					"Output Warehouse successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Output Warehouse auto loaded according to the Production Model in summary section",
					"Output Warehouse should be auto loaded according to the Production Model in summary section",
					"Output Warehouse doesn't auto loaded according to the Production Model in summary section",
					"fail");
		}

		String loadedWIPWare = getSelectedOptionInDropdown(drop_WIPWarehouseSummarySectionSummaryTabPO);
		if (loadedWIPWare.equals(wipWarehouseAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that WIP Warehouse auto loaded according to the Production Model in summary section",
					"WIP Warehouse should be auto loaded according to the Production Model in summary section",
					"WIP Warehouse successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that WIP Warehouse auto loaded according to the Production Model in summary section",
					"WIP Warehouse should be auto loaded according to the Production Model in summary section",
					"WIP Warehouse doesn't auto loaded according to the Production Model in summary section", "fail");
		}

		String loadedSite = getSelectedOptionInDropdown(drop_siteSummarySectionSummaryTabPO);
		if (loadedSite.equals(siteAaccordigToPMRegression)) {
			writeTestResults("Verify that Site auto loaded according to the Production Model in summary section",
					"Site should be auto loaded according to the Production Model in summary section",
					"Site successfully auto loaded according to the Production Model in summary section", "pass");
		} else {
			writeTestResults("Verify that Site auto loaded according to the Production Model in summary section",
					"Site should be auto loaded according to the Production Model in summary section",
					"Site doesn't auto loaded according to the Production Model in summary section", "fail");
		}

		String loadedBatchQty = getAttribute(txt_batchQtyProductInformationSectionSummaryTabPO, "value");
		if (loadedBatchQty.equals("100.00")) {
			writeTestResults(
					"Verify that Batch Qty auto loaded according to the Production Model in product information section",
					"Batch Qty should be auto loaded according to the Production Model in product information section",
					"Batch Qty successfully auto loaded according to the Production Model in product information section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Batch Qty auto loaded according to the Production Model in product information section",
					"Batch Qty should be auto loaded according to the Production Model in product information section",
					"Batch Qty doesn't auto loaded according to the Production Model in product information section",
					"fail");
		}

		String loadedCostingPriority = getSelectedOptionInDropdown(
				drop_costingPriorityProductInformationSectionSummaryTabPO);
		if (loadedCostingPriority.equals(costingPriorityAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that Costing Priority auto loaded according to the Production Model in product information section",
					"Costing Priority should be auto loaded according to the Production Model in product information section",
					"Costing Priority successfully auto loaded according to the Production Model in product information section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Costing Priority auto loaded according to the Production Model in product information section",
					"Costing Priority should be auto loaded according to the Production Model in product information section",
					"Costing Priority doesn't auto loaded according to the Production Model in product information section",
					"fail");
		}

		String loadedBoM = getText(drop_boMBoMDetailsSectionSummaryTabPO);
		if (loadedBoM.equals("000095")) {
			writeTestResults(
					"Verify that BoM Document Number auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number should be auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number successfully auto loaded according to the Production Model in bill of material details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoM Document Number auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number should be auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number doesn't auto loaded according to the Production Model in bill of material details section",
					"fail");
		}

		String loadedBoMVarient = getText(div_boMVarientBoMDetailsSectionSummaryTabPO);
		if (loadedBoMVarient.equals(bomVarientAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that BoM Varient auto loaded according to the Production Model in bill of material details section",
					"BoM Varient should be auto loaded according to the Production Model in bill of material details section",
					"BoM Varient successfully auto loaded according to the Production Model in bill of material details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoM Varient auto loaded according to the Production Model in bill of material details section",
					"BoM Varient should be auto loaded according to the Production Model in bill of material details section",
					"BoM Varient doesn't auto loaded according to the Production Model in bill of material details section",
					"fail");
		}

		String loadedBoMVarientDescription = getText(div_varientDescriptionBoMDetailsSectionSummaryTabPO);
		if (loadedBoMVarientDescription.equals(varientDescriptionAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that BoM Varient Description auto loaded according to the Production Model in bill of material details section",
					"BoM Varient Description should be auto loaded according to the Production Model in bill of material details section",
					"BoM Varient Description successfully auto loaded according to the Production Model in bill of material details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoM Varient Description auto loaded according to the Production Model in bill of material details section",
					"BoM Varient Description should be auto loaded according to the Production Model in bill of material details section",
					"BoM Varient Description doesn't auto loaded according to the Production Model in bill of material details section",
					"fail");
		}

		String loadedBoO = getText(div_boOBoODetailsSectionSummaryTabPO);
		if (loadedBoO.equals(booNoAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that BoO Number auto loaded according to the Production Model in bill of operation details section",
					"BoO Number should be auto loaded according to the Production Model in bill of operation details section",
					"BoO Number successfully auto loaded according to the Production Model in bill of operation details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoO Number auto loaded according to the Production Model in bill of operation details section",
					"BoO Number should be auto loaded according to the Production Model in bill of operation details section",
					"BoO Number doesn't auto loaded according to the Production Model in bill of operation details section",
					"fail");
		}

		String loadedBoODescription = getText(div_boODescriptionDetailsSectionSummaryTabPO);
		if (loadedBoODescription.equals(booDescriptionAaccordigToPMRegression)) {
			writeTestResults(
					"Verify that BoO Description auto loaded according to the Production Model in bill of operation details section",
					"BoO Description should be auto loaded according to the Production Model in bill of operation details section",
					"BoO Description successfully auto loaded according to the Production Model in bill of operation details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoO Description auto loaded according to the Production Model in bill of operation details section",
					"BoO Description should be auto loaded according to the Production Model in bill of operation details section",
					"BoO Description doesn't auto loaded according to the Production Model in bill of operation details section",
					"fail");
		}

		/*
		 * There is a bug. Supply production checkbox should be loaded as selected. Need
		 * to change after the bug was fixed
		 */
		if (!isSelected(chk_supplyToOperationDetailsSectionSummaryTabPO)) {
			writeTestResults(
					"Verify that Supply to Production checkbox auto loaded according to the Production Model in bill of operation details section",
					"Supply to Production checkbox should be auto loaded according to the Production Model in bill of operation details section",
					"Supply to Production checkbox successfully auto loaded according to the Production Model in bill of operation details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Supply to Production checkbox auto loaded according to the Production Model in bill of operation details section",
					"Supply to Production checkbox should be auto loaded according to the Production Model in bill of operation details section",
					"Supply to Production checkbox doesn't auto loaded according to the Production Model in bill of operation details section",
					"fail");
		}

		String loadedRequestedStartDate = getAttribute(txt_requestStartDateSheduleSectionSummaryTabPO, "value");
		String currentDate = common.currentDate();
		if (loadedRequestedStartDate.equals(currentDate)) {
			writeTestResults("Verify that Reqested Start Date auto loaded in shedule section",
					"Reqested Start Date should be auto loaded in shedule section",
					"Reqested Start Date successfully auto loaded in shedule section", "pass");
		} else {
			writeTestResults("Verify that Reqested Start Date auto loaded in shedule section",
					"Reqested Start Date should be auto loaded in shedule section",
					"Reqested Start Date doesn't auto loaded in shedule section", "fail");
		}

		String loadedRequestedEndDate = getAttribute(txt_requestEndDateSheduleSectionSummaryTabPO, "value");
		if (loadedRequestedEndDate.equals(currentDate)) {
			writeTestResults("Verify that Reqested End Date auto loaded in shedule section",
					"Reqested End Date should be auto loaded in shedule section",
					"Reqested End Date successfully auto loaded in shedule section", "pass");
		} else {
			writeTestResults("Verify that Reqested End Date auto loaded in shedule section",
					"Reqested End Date should be auto loaded in shedule section",
					"Reqested End Date doesn't auto loaded in shedule section", "fail");
		}

		selectText(drop_prioritySummarySectionSummaryTabPO, productionOrderPriorityRegression);
		Thread.sleep(1500);
		selectedPriority = getSelectedOptionInDropdown(drop_prioritySummarySectionSummaryTabPO);
		if (selectedPriority.equals(productionOrderPriorityRegression)) {
			writeTestResults("Verify that selected priority is display successfully",
					"Selected priority should be display successfully", "Selected priority display successfully",
					"pass");
		} else {
			writeTestResults("Verify that selected priority is display successfully",
					"Selected priority should be display successfully", "Selected priority not display", "fail");
		}

		driver.findElement(getLocator(txt_planedQtyProductInformationSectionSummaryTabPO)).click();
		driver.findElement(getLocator(txt_planedQtyProductInformationSectionSummaryTabPO))
				.sendKeys(planedQtyPORegression);
		Thread.sleep(1500);
		String enteredPlanedQty = getAttribute(txt_planedQtyProductInformationSectionSummaryTabPO, "value");
		if (enteredPlanedQty.equals(planedQtyPORegression)) {
			writeTestResults("Verify that entered Planed Qty is display successfully",
					"Entered Planed Qty should be display successfully", "Entered Planed Qty display successfully",
					"pass");
		} else {
			writeTestResults("Verify that entered Planed Qty is display successfully",
					"Entered Planed Qty should be display successfully", "Entered Planed Qty not display", "fail");
		}

		if (isSelected(chk_mrpProductionOrderRegression)) {
			writeTestResults("Verify that the system is auto enabled the MRP Enabled",
					"The system should be auto enabled the MRP Enabled",
					"The system successfully auto enabled the MRP Enabled", "pass");
		} else {
			writeTestResults("Verify that the system is auto enabled the MRP Enabled",
					"The system should be auto enabled the MRP Enabled",
					"The system doesn't auto enabled the MRP Enabled", "fail");
		}

		selectText(drop_productOrderGroupSummarySectionSummaryTabPO, productionOrderGroupRegression);
		Thread.sleep(1000);
		String selectedProductionGroup = getSelectedOptionInDropdown(drop_productOrderGroupSummarySectionSummaryTabPO);
		if (selectedProductionGroup.equals(productionOrderGroupRegression)) {
			writeTestResults("Verify user able to select the Production Group",
					"User should be able to select the Production Group",
					"User successfully select the Production Group", "pass");
		} else {
			writeTestResults("Verify user able to select the Production Group",
					"User should be able to select the Production Group", "User couldn't select the Production Group",
					"fail");
		}

		sendKeys(txt_descriptionSummarySectionSummaryTabPO, productionOrderDescriptionRegression);
		String enteredDescription = getAttribute(txt_descriptionSummarySectionSummaryTabPO, "value");
		if (enteredDescription.equals(productionOrderDescriptionRegression)) {
			writeTestResults("Verify user able to enter the Description",
					"User should be able to enter the Description", "User successfully enter the Description", "pass");
		} else {
			writeTestResults("Verify user able to enter the Description",
					"User should be able to enter the Description", "User couldn't enter the Description", "fail");
		}

		click(txt_requestStartDateSheduleSectionSummaryTabPO);
		explicitWaitUntillClickable(a_requestStartDatePOWhenDuplicating, 6);
		click(a_requestStartDatePOWhenDuplicating);
		Thread.sleep(2000);
		String selectedRequestedStartDate = getAttribute(txt_requestStartDateSheduleSectionSummaryTabPO, "value");
		String currentYearMonthWithselectedDate = common.currentDate().substring(0, 8) + "01";
		if (selectedRequestedStartDate.equals(currentYearMonthWithselectedDate)) {
			writeTestResults("Verify user able to select Requested Start Date",
					"User should be able to select Requested Start Date",
					"User successfully select Requested Start Date", "pass");
		} else {
			writeTestResults("Verify user able to select Requested Start Date",
					"User should be able to select Requested Start Date", "User couldn't select Requested Start Date",
					"fail");
		}
		
		requestedStartDate = selectedRequestedStartDate;

		click(txt_requestEndDateSheduleSectionSummaryTabPO);
		explicitWaitUntillClickable(a_requestEndDatePOWhenDuplicating, 6);
		click(a_requestEndDatePOWhenDuplicating);
		Thread.sleep(2000);
		String selectedRequestedEndDate = getAttribute(txt_requestEndDateSheduleSectionSummaryTabPO, "value");
		String currentYearMonthWithselectedEndDate = common.currentDate().substring(0, 8) + "28";
		if (selectedRequestedEndDate.equals(currentYearMonthWithselectedEndDate)) {
			writeTestResults("Verify user able to select Requested End Date",
					"User should be able to select Requested End Date", "User successfully select Requested End Date",
					"pass");
		} else {
			writeTestResults("Verify user able to select Requested End Date",
					"User should be able to select Requested End Date", "User couldn't select Requested End Date",
					"fail");
		}

		click(btn_draft);
		explicitWait(header_draftedProductionOrder, 60);
		if (isDisplayed(header_draftedProductionOrder)) {
			writeTestResults("VErify user able to draft the Production Order",
					"User should be able to draft the Production Order", "User successfully draft the Production Order",
					"pass");
		} else {
			writeTestResults("VErify user able to draft the Production Order",
					"User should be able to draft the Production Order", "User couldn't draft the Production Order",
					"fail");
		}

		click(btn_release);
		explicitWait(header_releasedProductionOrder, 30);
		trackCode = getText(lbl_docNo);
		writeProductionData("ProductionOrderFroProductionControl", trackCode, 11);
		if (isDisplayed(header_releasedProductionOrder)) {
			writeTestResults("Verify user able to release the Production Order",
					"User should be able to release the Production Order",
					"User successfully release the Production Order", "pass");
		} else {
			writeTestResults("Verify user able to release the Production Order",
					"User should be able to release the Production Order", "User couldn't release the Production Order",
					"fail");
		}

	}

	public void productionControl_PROD_PC_003_005_006_007_008_009_011_012() throws Exception {
		explicitWaitUntillClickable(img_navigationPane, 60);
		Thread.sleep(2000);
		explicitWaitUntillClickable(img_navigationPane, 20);
		if (click(img_navigationPane)) {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User successfully click navigation pane", "pass");
		} else {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User couldn't click navigation pane", "fail");
		}

		explicitWait(btn_production, 4);
		if (isEnabledQuickCheck(btn_production)) {
			writeTestResults("Verify user able to view the \"Production\" module under main navigation menu",
					"User should be able to view the \"Production\" module under main navigation menu",
					"User successfully view the \"Production\" module under main navigation menu", "pass");
		} else {
			writeTestResults("Verify user able to view the \"Production\" module under main navigation menu",
					"User should be able to view the \"Production\" module under main navigation menu",
					"User couldn't view the \"Production\" module under main navigation menu", "fail");
		}

		click(btn_production);

		explicitWait(btn_productionControl, 4);
		if (isEnabledQuickCheck(btn_productionControl)) {
			writeTestResults("Verify user able to view the \"Production Control\" option under production sub menu",
					"User should be able to view the \"Production Control\" option under production sub menu",
					"User able to view the \"Production Control\" option under production sub menu", "pass");
		} else {
			writeTestResults("Verify user able to view the \"Production Control\" option under production sub menu",
					"User should be able to view the \"Production Control\" option under production sub menu",
					"User couldn't view the \"Production Control\" option under production sub menu", "pass");
		}

		click(btn_productionControl);

		explicitWait(header_productionControlByPage, 60);
		if (isDisplayed(header_productionControlByPage)) {
			writeTestResults("Verify user able to navigate to Production Control by-page",
					"User should be able to navigate to Production Control by-page",
					"User successfully navigate to Production Control by-page", "pass");
		} else {
			writeTestResults("Verify user able to navigate to Production Control by-page",
					"User should be able to navigate to Production Control by-page",
					"User coulsn't navigate to Production Control by-page", "fail");
		}

		if (isDisplayed(div_columnProductionOrderStatusPC)) {
			writeTestResults("Verify that Production Order Status column header is display same as earlier",
					"Production Order Status column header should display same as earlier",
					"Production Order Status column header successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that Production Order Status column header is display same as earlier",
					"Production Order Status column header should display same as earlier",
					"Production Order Status column header doesn't display same as earlier", "fail");
		}

		if (isDisplayed(div_columnPostBusinessUnitPC)) {
			writeTestResults("Verify that Post Business Unit column header is display same as earlier",
					"Post Business Unit column header should display same as earlier",
					"Post Business Unit column header successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that Post Business Unit column header is display same as earlier",
					"Post Business Unit column header should display same as earlier",
					"Post Business Unit column header doesn't display same as earlier", "fail");
		}

		if (isDisplayed(div_columnDocNoPC)) {
			writeTestResults("Verify that Doc No column header is display same as earlier",
					"Doc No column header should display same as earlier",
					"Doc No column header successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that Doc No column header is display same as earlier",
					"Doc No column header should display same as earlier",
					"Doc No column header doesn't display same as earlier", "fail");
		}

		if (isDisplayed(div_columnPriorityPC)) {
			writeTestResults("Verify that Priority column header is display same as earlier",
					"Priority column header should display same as earlier",
					"Priority column header successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that Priority column header is display same as earlier",
					"Priority column header should display same as earlier",
					"Priority column header doesn't display same as earlier", "fail");
		}

		if (isDisplayed(div_columnProductionOrderNoPC)) {
			writeTestResults("Verify that Production Order No column header is display same as earlier",
					"Production Order No column header should display same as earlier",
					"Production Order No column header successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that Production Order No column header is display same as earlier",
					"Production Order No column header should display same as earlier",
					"Production Order No column header doesn't display same as earlier", "fail");
		}

		if (isDisplayed(div_columnOutputProductPC)) {
			writeTestResults("Verify that Output Product column header is display same as earlier",
					"Output Product column header should display same as earlier",
					"Output Product column header successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that Output Product column header is display same as earlier",
					"Output Product column header should display same as earlier",
					"Output Product column header doesn't display same as earlier", "fail");
		}

		if (isDisplayed(div_columnOpenQtyPC)) {
			writeTestResults("Verify that Open Qty column header is display same as earlier",
					"Open Qty column header should display same as earlier",
					"Open Qty column header successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that Open Qty column header is display same as earlier",
					"Open Qty column header should display same as earlier",
					"Open Qty column header doesn't display same as earlier", "fail");
		}

		if (isDisplayed(div_columnRequestedStartDatePC)) {
			writeTestResults("Verify that Request Statrt Date column header is display same as earlier",
					"Request Statrt Date column header should display same as earlier",
					"Request Statrt Date column header successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that Request Statrt Date column header is display same as earlier",
					"Request Statrt Date column header should display same as earlier",
					"Request Statrt Date column header doesn't display same as earlier", "fail");
		}

		sendKeys(txt_searchProductionControlOnByPage, readProductionData("ProductionOrderFroProductionControl"));
		String enteredPO = getAttribute(txt_searchProductionControlOnByPage, "value");
		if (enteredPO.equals(readProductionData("ProductionOrderFroProductionControl"))) {
			writeTestResults("Verify that user able to enter the Production Order No in search field",
					"User should be able to enter the Production Order No in search field",
					"User able to enter the Production Order No in search field", "pass");
		} else {
			writeTestResults("Verify that user able to enter the Production Order No in search field",
					"User should be able to enter the Production Order No in search field",
					"User not able to enter the Production Order No in search field", "pass");
		}

		click(btn_searchProductionControlByPage);
		Thread.sleep(2000);

		if (isDisplayed(div_chkSelectDocReplacePC.replace("doc_no",
				readProductionData("ProductionOrderFroProductionControl")))) {
			writeTestResults("Verify that relevent Production Order is available on search results",
					"Relevent Production Order should be available on search results",
					"Relevent Production Order successfully available on search results", "pass");
		} else {
			writeTestResults("Verify that relevent Production Order is available on search results",
					"Relevent Production Order should be available on search results",
					"Relevent Production Order is not available on search results", "fail");
		}

		int resultcount = getCount(div_resultCountPCByPage);
		if (resultcount == 1) {
			writeTestResults("Verify that only relevant Production Order is available on search results",
					"Only relevant Production Order should be available on search results",
					"Only relevant Production Order successfully available on search results", "pass");
		} else {
			writeTestResults("Verify that only relevant Production Order is available on search results",
					"Only relevant Production Order should be available on search results",
					"Not only relevant Production Order is available on search results", "fail");
		}

		/* PROD_PC_003 */
		int firstWhiteSpaceIndex = postBusinessUnit.indexOf(" ");
		String firstPart = postBusinessUnit.substring(0, firstWhiteSpaceIndex);
		String secondPart = postBusinessUnit.substring(firstWhiteSpaceIndex + 1);
		String pbu = firstPart + secondPart;

		if (isDisplayed(div_postBusinessUnitProductControlByPage.replace("pbu", pbu))) {
			writeTestResults(
					"Verify whether system is dispaying relevent post business unit under 'post business unit' column in production control by page",
					"The system should be displaying relevant post business unit under the 'post business unit' column in production control by page",
					"The system successfully displaying relevant post business unit under the 'post business unit' column in production control by page",
					"pass");
		} else {
			writeTestResults(
					"Verify whether system is dispaying relevent post business unit under 'post business unit' column in production control by page",
					"The system should be displaying relevant post business unit under the 'post business unit' column in production control by page",
					"The system doesn't display relevant post business unit under the 'post business unit' column in production control by page",
					"fail");
		}

		/* PROD_PC_005 */
		if (isDisplayed(div_priorityProductControlByPage.replace("priority", selectedPriority))) {
			writeTestResults(
					"Verify whether system is dispaying relevent priority under 'priority' column in production control by page",
					"The system should be displaying relevant priority the under 'priority' column in production control by page",
					"The system successfully displaying relevant priority the under 'priority' column in production control by page",
					"pass");
		} else {
			writeTestResults(
					"Verify whether system is dispaying relevent priority under 'priority' column in production control by page",
					"The system should be displaying relevant priority the under 'priority' column in production control by page",
					"The system doesn't display relevant priority the under 'priority' column in production control by page",
					"fail");
		}

		/* PROD_PC_006 */
		if (isDisplayed(div_productionOrderNoProductControlByPage.replace("production_order_no",
				readProductionData("ProductionOrderFroProductionControl")))) {
			writeTestResults(
					"Verify whether system is dispaying relevent production order number under 'production order number' column in production control by page",
					"The system should be displaying relevant production order number the under 'production order number' column in production control by page",
					"The system successfully displaying relevant production order number the under 'production order number' column in production control by page",
					"pass");
		} else {
			writeTestResults(
					"Verify whether system is dispaying relevent production order number under 'production order number' column in production control by page",
					"The system should be displaying relevant production order number the under 'production order number' column in production control by page",
					"The system doesn't display relevant production order number the under 'production order number' column in production control by page",
					"fail");
		}

		/* PROD_PC_007 */
		if (isDisplayed(div_outputProductProductControlByPage.replace("output_product",
				selectedProductFrontPageRegressionPO.replace("product_code",readProductionData("ProductionTypeProductRegressionPO"))))) {
			writeTestResults(
					"Verify whether system is dispaying relevent output product under 'output product' column in production control by page",
					"The system should be displaying relevant output product the under 'output product' column in production control by page",
					"The system successfully displaying relevant output product the under 'output product' column in production control by page",
					"pass");
		} else {
			writeTestResults(
					"Verify whether system is dispaying relevent output product under 'output product' column in production control by page",
					"The system should be displaying relevant output product the under 'output product' column in production control by page",
					"The system doesn't display relevant output product the under 'output product' column in production control by page",
					"fail");
		}
		
		/* PROD_PC_008 */
		if (isDisplayed(div_openQtyProductControlByPage.replace("qty",
				planedQtyPORegression))) {
			writeTestResults(
					"Verify whether system is dispaying relevent open qty under 'open qty' column in production control by page",
					"The system should be displaying relevant open qty the under 'open qty' column in production control by page",
					"The system successfully displaying relevant open qty the under 'open qty' column in production control by page",
					"pass");
		} else {
			writeTestResults(
					"Verify whether system is dispaying relevent open qty under 'open qty' column in production control by page",
					"The system should be displaying relevant open qty the under 'open qty' column in production control by page",
					"The system doesn't display relevant open qty the under 'open qty' column in production control by page",
					"fail");
		}
		
		/* PROD_PC_009 */
		if (isDisplayed(div_requestedStartDateProductControlByPage.replace("date",
				requestedStartDate))) {
			writeTestResults(
					"Verify whether system is dispaying relevent Requested Start Date under 'Requested Start Date' column in production control by page",
					"The system should be displaying relevant Requested Start Date the under 'Requested Start Date' column in production control by page",
					"The system successfully displaying relevant Requested Start Date the under 'Requested Start Date' column in production control by page",
					"pass");
		} else {
			writeTestResults(
					"Verify whether system is dispaying relevent Requested Start Date under 'Requested Start Date' column in production control by page",
					"The system should be displaying relevant Requested Start Date the under 'Requested Start Date' column in production control by page",
					"The system doesn't display relevant Requested Start Date the under 'Requested Start Date' column in production control by page",
					"fail");
		}

		click(div_chkSelectDocReplacePC.replace("doc_no", readProductionData("ProductionOrderFroProductionControl")));
		Thread.sleep(2000);
		if (isDisplayed(div_chkSelectedDocReplacePC.replace("doc_no",
				readProductionData("ProductionOrderFroProductionControl")))) {
			writeTestResults("Verify that relevent Production Order is selected",
					"Relevent Production Order should be selected", "Relevent Production Order successfully selected",
					"pass");
		} else {
			writeTestResults("Verify that relevent Production Order is selected",
					"Relevent Production Order should be selected", "Relevent Production Order is not selected",
					"fail");
		}

		/* PROD_PC_012 */
		click(btn_action);
		explicitWaitUntillClickable(btn_runMRP, 8);
		if (isDisplayed(btn_runMRP)) {
			writeTestResults("Verify that the Run MRP button is visible under action dropdown",
					"Run MRP button should visible under action dropdown",
					"Run MRP button successfully visible under action dropdown", "pass");
		} else {
			writeTestResults("Verify that the Run MRP button is visible under action dropdown",
					"Run MRP button should visible under action dropdown",
					"Run MRP button not visible under action dropdown", "fail");
		}

		click(btn_runMRP);
		explicitWait(lookup_productionControl, 8);
		if (isDisplayed(lookup_productionControl)) {
			writeTestResults("Verify that the production control screen was loaded",
					"The production control screen should be loaded successfully",
					"The production control screen loaded successfully", "pass");
		} else {
			writeTestResults("Verify that the production control screen was loaded",
					"The production control screen should be loaded successfully",
					"The production control screen doesn't loaded", "fail");
		}

	}

}
