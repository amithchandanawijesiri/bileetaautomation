package bileeta.BTAF.PageObjects;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import bileeta.BATF.Pages.ProductionModuleDataSmoke;

public class ProductionModuleSmoke extends ProductionModuleDataSmoke {

	public static String productionOrder;

	public static String unit;

	public static String locationCode;

	public static String BOONO;

	public static String bulkNo;

	public static String product;

	/* write production data */
	private static Workbook wb;
	private static Sheet sh;
	private static FileInputStream fis;
	private static FileOutputStream fos;
	private static Row row;
	private static Cell cell;
	private static Cell cell1;
	private static Cell cell2;

	/* common objects */
	private InventoryAndWarehouseModule invObj = new InventoryAndWarehouseModule();

	/* common getters and setters */
	public InventoryAndWarehouseModule getInvObj() {
		return invObj;
	}

	/* common methods */
	public void writeProductionData(String name, String variable, int data_row)
			throws InvalidFormatException, IOException {
		String path = "";
		String location = System.getProperty("user.dir");

		String real_project_path = "*src*test*java*productionModule*ProductionData.xlsx".replace("*", "\\");
		path = location + real_project_path;

		fis = new FileInputStream(path);
		wb = WorkbookFactory.create(fis);
		sh = wb.getSheet("Sheet1");
		row = sh.createRow(data_row);
		cell = row.createCell(1);
		cell1 = row.createCell(0);
		cell.setCellValue(variable);
		cell1.setCellValue(name);
		/* System.out.println(cell.getStringCellValue()); */
		fos = new FileOutputStream(path);
		wb.write(fos);
		fos.flush();
		fos.close();
	}

	public String readProductionData(String name) throws IOException, InvalidFormatException {
		String path = "";
		String location = System.getProperty("user.dir");

		String real_project_path = "*src*test*java*productionModule*ProductionData.xlsx".replace("*", "\\");
		path = location + real_project_path;

		fis = new FileInputStream(path);
		wb = WorkbookFactory.create(fis);
		sh = wb.getSheet("Sheet1");

		Map<String, String> iw_creation_map = new HashMap<String, String>();
		List<Map<String, String>> iw_creation = new ArrayList<Map<String, String>>();

		int totRows = sh.getLastRowNum();
		for (i = 0; i <= totRows; i++) {
			row = sh.getRow(i);
			cell = row.getCell(0);
			cell2 = row.getCell(1);

			String test_creation_row1 = cell.getStringCellValue();
			String test_creation_row2 = cell2.getStringCellValue();

			iw_creation_map.put(test_creation_row1, test_creation_row2);
			iw_creation.add(i, iw_creation_map);

		}

		String iw_info = iw_creation.get(0).get(name);

		return iw_info;
	}

	public void setInvObj(InventoryAndWarehouseModule invObj) {
		this.invObj = invObj;
	}

	public void navigateToTheLoginPage() throws Exception {
		openPage(siteURL);

		writeTestResults("Verify that 'Entution' header is available on the page",
				"user should be able to see the logo", "logo is dislayed", "pass");

	}

	public void verifyTheLogo() throws Exception {
		if (isDisplayed(siteLogo)) {

			writeTestResults("Verify that 'Entution' header is available on the page",
					"user should be able to see the logo", "logo is dislayed", "pass");
		} else {
			writeTestResults("Verify that 'Entution' header is available on the page",
					"user should be able to see the logo", "logo is not dislayed", "fail");

		}
	}

	public void userLogin() throws Exception {

		sendKeys(txt_username, UserNameData);

		sendKeys(txt_password, PasswordData);

		click(btn_login);

		if (isDisplayed(lnk_home)) {

			writeTestResults("User login", "User should navigate to the Home page of Entution",
					"user navigated to the home page", "pass");
		}

		else {
			writeTestResults("User login", "User should navigate to the Home page of Entution",
					"user cant navigate to the home page", "fail");

		}
	}

	public void navigateToSideBar() throws Exception {
		InventoryAndWarehouseModuleRegression obj = new InventoryAndWarehouseModuleRegression();
//		obj.activeQcAcceptance();
		explicitWait(navigateMenu, 30);
		Thread.sleep(2000);
		explicitWait(navigateMenu, 30);
		click(navigateMenu);
		Thread.sleep(3000);
		if (isDisplayed(sideNavBar)) {

			writeTestResults("Verify that user can successfully navigate to side menu",
					"User successfully navigate to side menu", "Side menu displayed", "pass");

		} else {

			writeTestResults("Verify that user can successfully navigate to side menu",
					"User can't successfully navigate to side menu", "Side menu is not displayed", "fail");
		}
	}

	public void navigateToProductionMenu() throws Exception {

		click(btn_Production);
		Thread.sleep(3000);
		if (isDisplayed(logo_Production)) {

			writeTestResults("Verify that user can view fixed asset logo", "User can view the logo", "logo viewed",
					"pass");

		}

		else {

			writeTestResults("Verify that  user can view fixed asset logo", "User can't view the logo",
					"logo is not viewed", "fail");
		}

	}

	/* Con_TC_003 */

	// Verify that user can create a product level BOM

	public void navigateToInventory() throws Exception {
		Thread.sleep(3000);
		click(btn_inventory);
		Thread.sleep(3000);
		if (isDisplayed("//a[contains(text(),'Bill of Material')]")) {

			writeTestResults("Verify that user can view inventory label", "User can view the logo",
					"Inventory Label viewed", "pass");

		} else {

			writeTestResults("Verify that user can view inventory label", "User can view the logo",
					"Inventory Label doesn't viewed", "fail");

		}
	}

	public void navigateToBillOfMaterial() throws Exception {

		Thread.sleep(5000);
		click(btn_billOfMaterial);

		if (isDisplayed(label_billOfMaterial)) {

			writeTestResults("Verify that user can view bill of material logo", "User can view the logo",
					"Logo displayed", "pass");
		}

		else {

			writeTestResults("Verify that user can view bill of material logo", "User can view the logo",
					"Logo can't be displayed", "fail");
		}
	}

	public void navigateTonewBOM() throws Exception {

		click(btn_newBOM);

		if (isDisplayed(txt_newBOM)) {

			writeTestResults("Verify that the user can navigate to new BOM", "New bill of material page displayed",
					"new BOM page loaded", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to new BOM", "New bill of material page displayed",
					"new BOM page not loaded", "fail");
		}
	}

	public void fillingBOMForm() throws Exception {

		sendKeys(txt_desc, descData);

		selectText(btn_productGrp, productGrpData);

		// Select output Product
		click(btn_outputProductSearch);

		Thread.sleep(2000);

		// sendKeys(txt_outputProductSearch, outputProductData);
		sendKeys(txt_outputProductSearch, productCodeData);

		Thread.sleep(3000);
		pressEnter(txt_outputProductSearch);

		Thread.sleep(3000);
		doubleClick(doubleClickoutputProduct);

		sendKeys(txt_BOMQuantity, BOMQuantityData);

		click(btn_expiryDate);

		click(clickExpiryDate);

		click(btn_rawMaterial);

		Thread.sleep(5000);

		sendKeys(txt_rawMaterial, rawMaterialData);

		Thread.sleep(5000);

		pressEnter(txt_rawMaterial);

		Thread.sleep(4000);

		doubleClick(doubleClickrawMaterial);

		sendKeys(txt_rawQuantity, rawQuantityData);

		if (isDisplayed(txt_newBOM)) {

			writeTestResults(
					"Verify that the user can navigate to new BOMUser selected the raw materials under the raw materials section should be displayed successfully",
					"raw materials section should be displayed", "raw materials section displayed", "pass");
		} else {

			writeTestResults(
					"User selected the raw materials under the raw materials section should be displayed successfully",
					"raw materials section shoul be displayed", "raw materials section not displayed", "fail");
		}

	}

	public void completeNreleaseBOM() throws Exception {

		click(btn_draft);

		explicitWait(btn_release, 30);
		click(btn_release);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(txt_BOM)) {

			writeTestResults("Verify that the user can release the bill of materials successfully",
					"bill of materials released", "bill of materials released", "pass");
		} else {

			writeTestResults("Verify that the user can release the bill of materials successfully",
					"bill of materials released", "bill of materials not released", "fail");
		}
	}

	/* Con_TC_004 */
	// Verify that user can create a group level BOM

	public void navigateToInventoryGroup() throws Exception {

		click(btn_inventory);

		if (isDisplayed(txt_inventory)) {

			writeTestResults("Verify that user can view inventory label", "User can view the logo", "Label viewed",
					"pass");

		} else {

			writeTestResults("Verify that user can view inventory label", "User can view the logo",
					"Label doesn't viewed", "fail");

		}
	}

	public void navigateToBillOfMaterialGroup() throws Exception {

		click(btn_billOfMaterial);

		if (isDisplayed(label_billOfMaterial)) {

			writeTestResults("Verify that user can view bill of material logo", "User can view the logo",
					"Logo displayed", "pass");
		}

		else {

			writeTestResults("Verify that user can view bill of material logo", "User can view the logo",
					"Logo can't be displayed", "fail");
		}
	}

	public void navigateTonewBOMGroup() throws Exception {

		click(btn_newBOM);

		if (isDisplayed(txt_newBOM)) {

			writeTestResults("Verify that the user can navigate to new BOM", "New bill of material page displayed",
					"new BOM page loaded", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to new BOM", "New bill of material page displayed",
					"new BOM page not loaded", "fail");
		}
	}

	public void fllingBOMFormGroup() throws Exception {

		sendKeys(txt_desc, descData);

		selectText(btn_productGrp, productGrpData);

		sendKeys(txt_BOMQuantity, BOMQuantityData);

		click(btn_expiryDate);

		click(clickExpiryDate);

		click(btn_rawMaterial);

		Thread.sleep(5000);

		sendKeys(txt_rawMaterial, rawMaterialData);

		Thread.sleep(5000);

		pressEnter(txt_rawMaterial);

		Thread.sleep(2000);

		doubleClick(doubleClickrawMaterial);

		sendKeys(txt_rawQuantity, rawQuantityData);

		if (isDisplayed(txt_newBOM)) {

			writeTestResults(
					"Selected the raw materials under the raw materials section should be displayed successfully",
					"New bill of material page displayed", "new BOM page loaded", "pass");
		} else {

			writeTestResults(
					"Selected the raw materials under the raw materials section should be displayed successfully",
					"New bill of material page displayed", "new BOM page not loaded", "fail");
		}
	}

	public void completeNreleaseBOMGroup() throws Exception {

		click(btn_draft);

		click(btn_release);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(txt_BOM)) {

			writeTestResults("Verify that the user can release the bill of materials successfully",
					"bill of materials released", "bill of materials released", "pass");
		} else {

			writeTestResults("Verify that the user can release the bill of materials successfully",
					"bill of materials released", "bill of materials not released", "fail");
		}
	}

	/* Con_TC_006 */

	// Verify whether user can fill summary data of bill of operations successully

	public void navigateToProduction() throws Exception {

		click(btn_production);
		Thread.sleep(4000);
		if (isDisplayed(txt_production)) {

			writeTestResults("Verify that the user can navigate to Production page",
					"The Bill of operations by page should be loaded successfully", "Production page loaded", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to Production page",
					"The Bill of operations by page should be loaded successfully", "Production page not loaded",
					"fail");
		}
	}

	public void navigateToBillOfOperation() throws Exception {

		click(btn_BOO);
		Thread.sleep(4000);
		if (isDisplayed(txt_BOO)) {

			writeTestResults("Verify that the user can navigate to Bill of operation by page",
					"The New bill of operations page should be loaded successfully", "Bill Of Operation page loaded",
					"pass");
		} else {

			writeTestResults("Verify that the user can navigate to Bill of operation by page",
					"The New bill of operations page should be loaded successfully",
					"Bill Of Operation page not loaded", "fail");
		}
	}

	public void navigateTonewBOO() throws Exception {
		click(btn_newBOO);
		Thread.sleep(4000);
		if (isDisplayed(txt_newBOO)) {

			writeTestResults("Verify that the user can navigate to new Bill of operation by page",
					"The New bill of operations page should be loaded successfully",
					"new Bill Of Operation page loaded", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to new Bill of operation by page",
					"The New bill of operations page should be loaded successfully",
					"new Bill Of Operation page not loaded", "fail");
		}
	}

	public void fillBOOForm() throws Exception {

		Random rand = new Random();
		int rand_int1 = rand.nextInt(100000000);

		BOOCodeData = BOOCodeData + Integer.toString(rand_int1);

		sendKeys(txt_BOOCode, BOOCodeData);

		// Description
		click(btn_descriptionBOO);

		sendKeys(txt_descrBOO, BOODescrData);

		click(btn_BOOApply);

		if (isDisplayed(txt_BOOLogo)) {

			writeTestResults("Verify that the user can navigate to Bill of operation by page",
					"The bill of operations page should be loaded successfully", "Bill Of Operation page loaded again",
					"pass");
		} else {

			writeTestResults("Verify that the user can navigate to Bill of operation by page",
					"The bill of operations page should be loaded successfully",
					"Bill Of Operation page not loaded again", "fail");
		}
	}

	/* Con_TC_007 */
	// Verify whether user can add initail operation as a supply under the bill of
	// operations details

	public void OperationActivitySupply() throws Exception {

		click(btn_plusBOOSupply);
		Thread.sleep(4000);
		selectText(txt_ElementCategory, ElementCategoryData);

		sendKeys(txt_ElementDescription, ElementDescData);

//		click(btn_applyOperation);
		JavascriptExecutor j1 = (JavascriptExecutor) driver;
		j1.executeScript("$(\"#divOperation\").parent().find('.dialogbox-buttonarea > .button').click()");

		if (isDisplayed(label_operationActivity)) {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"Supply operation should be added successfully", "operation/activity page loaded again", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"Supply operation should be added successfully", "operation/activity page not loaded again",
					"fail");
		}

	}
	/* Con_TC_008 */
	// Verify whether user can add next operation as a production under the bill of
	// operations details

	public void OperationActivityOperation() throws Exception {

		click(btn_plusBOOOperation);
		Thread.sleep(4000);
		selectIndex(txt_ElementCategoryOperation, 1);

		sendKeys(txt_GroupId, GroupIdData);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

//		click(btn_applyOperation);
		JavascriptExecutor j1 = (JavascriptExecutor) driver;
		j1.executeScript("$(\"#divOperation\").parent().find('.dialogbox-buttonarea > .button').click()");

		if (isDisplayed(label_operationActivity)) {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page loaded again", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page not loaded again", "fail");
		}

	}

	/* Con_TC_009 */
	// Verify whether user can add final operation as a QC under the bill of
	// operations details

	public void QCUnderTheBillOfOperations() throws Exception {

		click(btn_plusBOOQCBillOfOperation);

		selectIndex(txt_ElementCategoryOperation, 1);

		selectIndex(txt_activityType, 1);

		click(txt_FinalInvestigation);

		sendKeys(txt_GroupId, GroupIdDataQC);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

//		Thread.sleep(3000);
//		click(btn_applyOperation);
		JavascriptExecutor j1 = (JavascriptExecutor) driver;
		j1.executeScript("$(\"#divOperation\").parent().find('.dialogbox-buttonarea > .button').click()");

		click(btn_draft);

		Thread.sleep(3000);

		click(btn_release);

		BOONO = getText(txt_BOONo);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(label_operationActivity)) {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page loaded again", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page not loaded again", "fail");
		}
	}

	/* Con_TC_011 */
	// Verify that user can line production operations to the BOO

	public void addLineProduction() throws Exception {

		Thread.sleep(3000);

		click(btn_supplyBillOfOperationPlusMark);

		selectIndex(txt_ElementCategoryOperation, 1);

		sendKeys(txt_GroupId, GroupIdData);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		click(btn_applyOperation);

		if (isDisplayed(label_operationActivity)) {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page loaded again", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page not loaded again", "fail");
		}

		click(btn_supplyBillOfOperationPlusMark);

		selectIndex(txt_ElementCategoryOperation, 1);

		sendKeys(txt_GroupId, GroupIdData);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		click(btn_applyOperation);

		if (isDisplayed(label_operationActivity)) {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page loaded again", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page not loaded again", "fail");
		}

		click(btn_supplyBillOfOperationPlusMark);

		selectIndex(txt_ElementCategoryOperation, 1);

		sendKeys(txt_GroupId, GroupIdData);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		click(btn_applyOperation);

		if (isDisplayed(label_operationActivity)) {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page loaded again", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page not loaded again", "fail");
		}

		click(btn_supplyBillOfOperationPlusMark);

		selectIndex(txt_ElementCategoryOperation, 1);

		selectIndex(txt_activityType, 1);

		click(txt_FinalInvestigation);

		sendKeys(txt_GroupId, GroupIdData1);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		click(btn_applyOperation);

		click(btn_draft);

		Thread.sleep(2000);

		click(btn_release);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(label_operationActivity)) {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page loaded again", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page not loaded again", "fail");
		}
	}

	/* Con_TC_012 */
	// Verify that user can add multiple production operations to the BOO

	public void addMultipleProduction() throws Exception {

		Thread.sleep(3000);

		click(btn_supplyBillOfOperationPlusMark);

		selectIndex(txt_ElementCategoryOperation, 1);

		sendKeys(txt_GroupId, GroupIdData);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		click(btn_applyOperation);

		if (isDisplayed(label_operationActivity)) {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page loaded again", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page not loaded again", "fail");
		}

		click(btn_supplyBillOfOperationPlusMark);

		selectIndex(txt_ElementCategoryOperation, 1);

		sendKeys(txt_GroupId, GroupIdData1);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		click(btn_applyOperation);

		if (isDisplayed(label_operationActivity)) {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page loaded again", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page not loaded again", "fail");
		}

		click(btn_supplyBillOfOperationPlusMark);

		selectIndex(txt_ElementCategoryOperation, 1);

		sendKeys(txt_GroupId, GroupIdData4);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		click(btn_applyOperation);

		if (isDisplayed(label_operationActivity)) {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page loaded again", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page not loaded again", "fail");
		}

		click(btn_supplyBillOfOperationPlusMark);

		selectIndex(txt_ElementCategoryOperation, 1);

		selectIndex(txt_activityType, 1);

		click(txt_FinalInvestigation);

		sendKeys(txt_GroupId, GroupIdDatafinal);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		click(btn_applyOperation);

		click(btn_draft);

		Thread.sleep(2000);

		click(btn_release);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(label_operationActivity)) {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page loaded again", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page not loaded again", "fail");
		}
	}

	/* Con_TC_015 */
	// Verify whether user can navigate to new production model page successfully

	public void navigateToProductionModel() throws Exception {

		click(btn_productionModel);

	}

	/* Con_TC_016 */
	// Verify whether user can fill summary data of production model successfully

	public void ProductionModelForm() throws Exception {

		click(btn_newProductionModel);

		Random rand = new Random();
		int rand_int1 = rand.nextInt(1000000);

		modelCodeData = modelCodeData + Integer.toString(rand_int1);

		sendKeys(txt_modelCode, modelCodeData);

		selectText(txt_productGroup, productGroupData);

		click(btn_yes);

		click(btn_product);

		// sendKeys(txt_product, productData);

		sendKeys(txt_product, productCodeData);

		pressEnter(txt_product);

		Thread.sleep(3000);

		doubleClick(doubleClickproduct);

		click(btn_yes);

		click(btn_ProductionUnit);

		sendKeys(txt_ProductionUnitLookup, ProductionUnitData);

		pressEnter(txt_ProductionUnitLookup);

		Thread.sleep(3000);

		explicitWait(doubleClickProductionUnit, 50);
		doubleClick(doubleClickProductionUnit);

		click(btn_yes);

		if (isDisplayed(txt_newproductionModel)) {

			writeTestResults("Verify whether user can fill summary data of production model successfully",
					"The new production model page should be loaded successfully", "new production model page loaded",
					"pass");
		} else {

			writeTestResults("Verify whether user can fill summary data of production model successfully",
					"The new production model page should be loaded successfully",
					"new production model page not loaded", "fail");
		}
	}

	/* Con_TC_017 */
	// Verify whether user can fill other information of production model with
	// respect to MRP production type successfully

	public void ProductionModelFormOnlyMRPEnable() throws Exception {

		click(btn_MRPEnabled);

		sendKeys(txt_batchQty, batchQtyData);

		selectIndex(txt_barcodeBook, 1);

		selectIndex(txt_CostingPriority, 1);

		sendKeys(txt_standardCost, standardCostData);

		click(btn_pricingProfile);

		sendKeys(txt_PricingProfile, PricingProfileData);

		pressEnter(txt_PricingProfile);

		doubleClick(doubleClickPricingProfile);

		if (isDisplayed(txt_newproductionModel)) {

			writeTestResults(
					"Verify whether user can fill other information of production model with respect to MRP prodution type successfully",
					"The new production model page filled information of production model with respect to MRP production type successfully",
					"MRP production type production model filled successfully", "pass");
		} else {

			writeTestResults(
					"Verify whether user can fill other information of production model with respect to MRP prodution type successfully",
					"The new production model page filled information of production model with respect to MRP production type successfully",
					"MRP production type production model not filled successfully", "fail");
		}
	}

	/* Con_TC_018 */
	// Verify whether user can fill other information of production model with
	// respect to Infinite production type successfully

	public void ProductionModelFormOnlyInfiniteEnable() throws Exception {

//		click(btn_newProductionModel);
//
//		Random rand = new Random();
//		int rand_int1 = rand.nextInt(10000);
//
//		modelCodeData = modelCodeData + Integer.toString(rand_int1);
//
//		sendKeys(txt_modelCode, modelCodeData);
//
//		selectText(txt_productGroup, productGroupData);
//
//		click(btn_yes);
//
//		click(btn_product);
//
//		sendKeys(txt_product, productData);
//
//		pressEnter(txt_product);
//
//		Thread.sleep(3000);
//
//		doubleClick(doubleClickproduct);
//
//		click(btn_yes);
//
//		click(btn_ProductionUnit);
//
//		sendKeys(txt_ProductionUnitLookup, ProductionUnitData);
//
//		pressEnter(txt_ProductionUnitLookup);
//
//		doubleClick(doubleClickProductionUnit);
//
//		click(btn_yes);

		click(btn_Infinite);

		selectIndex(txt_barcodeBook, 1);

		selectIndex(txt_CostingPriority, 1);

		sendKeys(txt_standardCost, standardCostData);

		click(btn_pricingProfile);

		sendKeys(txt_PricingProfile, PricingProfileData);

		pressEnter(txt_PricingProfile);

		doubleClick(doubleClickPricingProfile);

		if (isDisplayed(txt_newproductionModel)) {

			writeTestResults(
					"Verify whether user can fill other information of production model with respect to Infinite prodution type successfully",
					"The new production model page filled information of production model with respect to infinite production type successfully",
					"infinite production type production model filled successfully", "pass");
		} else {

			writeTestResults(
					"Verify whether user can fill other information of production model with respect to Infinite prodution type successfully",
					"The new production model page filled information of production model with respect to infinite production type successfully",
					"infinite production type production model not filled successfully", "fail");
		}

	}

	/* Con_TC_019 */
	// Verify whether user can fill other information of production model with
	// respect to MRP and Infinite production type successfully
	public void ProductionModelFormMRPInfiniteEnable() throws Exception {

//		click(btn_newProductionModel);
//
//		Random rand = new Random();
//		int rand_int1 = rand.nextInt(10000);
//
//		modelCodeData = modelCodeData + Integer.toString(rand_int1);
//
//		sendKeys(txt_modelCode, modelCodeData);
//
//		selectText(txt_productGroup, productGroupData);
//
//		click(btn_yes);
//
//		click(btn_product);
//
//		sendKeys(txt_product, productData);
//
//		pressEnter(txt_product);
//
//		Thread.sleep(3000);
//
//		doubleClick(doubleClickproduct);
//
//		click(btn_yes);
//
//		click(btn_ProductionUnit);
//
//		sendKeys(txt_ProductionUnitLookup, ProductionUnitData);
//
//		pressEnter(txt_ProductionUnitLookup);
//
//		doubleClick(doubleClickProductionUnit);
//
//		click(btn_yes);

		click(btn_MRPEnabled);

		click(btn_Infinite);

		sendKeys(txt_batchQty, batchQtyData);

		selectIndex(txt_barcodeBook, 1);

		selectIndex(txt_CostingPriority, 1);

		sendKeys(txt_standardCost, standardCostData);

		click(btn_pricingProfile);

		sendKeys(txt_PricingProfile, PricingProfileData);

		pressEnter(txt_PricingProfile);

		doubleClick(doubleClickPricingProfile);

		if (isDisplayed(txt_newproductionModel)) {

			writeTestResults(
					"Verify whether user can fill other information of production model with respect to MRP and Infinite prodution type successfully",
					"The new production model page filled information of production model with respect to infinite and MRP production type successfully",
					"infinite and MRP production type production model filled successfully", "pass");
		} else {

			writeTestResults(
					"Verify whether user can fill other information of production model with respect to MRP and Infinite prodution type successfully",
					"The new production model page filled information of production model with respect to infinite and MRP production type successfully",
					"infinite and MRP production type production model not filled successfully", "fail");
		}
	}

	/* Con_TC_020 */
	// Verify whether user can search bill of opertaions details successfully
	public void BOODetails() throws Exception {

		click(btn_BOONoSearch);

		sendKeys(txt_BOOOperation, BOONO);

		pressEnter(txt_BOOOperation);

		doubleClick(doubleClickBOOOperation);

		click(btn_informationOK);
	}

	/* Con_TC_021 - duplicate test case */

	/* Con_TC_022 */
	// Verify whether user added BOO details are displaying on the Bill of
	// operations tab

	/* Con_TC_023 */
	// Verify whether user able to add raw materials to production model

	public void BOODetailsTabSupply() throws Exception {

		click(btn_BOOTab);

		click(btn_Expand);

		click(btn_supply);

		selectIndex(txt_ElementCategoryOperation, 2);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		click(btn_inputProducts);

		click(btn_checkBox);

		click(btn_apply);
	}

	/* Con_TC_024 */
	// Verify whether user able to add production operations to production model
	public void BOODetailsTabOperation() throws Exception {

		click(btn_operation);

		selectIndex(txt_ElementCategoryOperation, 1);

		sendKeys(txt_GroupId, GroupIdData);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		selectIndex(txt_processTime, 1);

		click(btn_inputProducts);

		click(btn_checkBox);

		click(btn_resources);

		click(btn_resourceSearch);

		sendKeys(txt_additionalResources, additionalResourcesData);

		pressEnter(txt_additionalResources);

		doubleClick(doubleCLickAdditinalResources);

		click(btn_apply);
	}

	/* Con_TC_025, Con_TC_026, Con_TC_027 */
	// Verify whether user able to add QC operations to production model
	public void BOODetailsTabQC() throws Exception {

		click(btn_QC);

		selectIndex(txt_ElementCategoryOperation, 1);

		selectIndex(txt_activityType, 1);

		sendKeys(txt_GroupId, GroupIdData2);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		selectIndex(txt_processTime, 1);

		click(btn_apply);

		Thread.sleep(3000);

		click(btn_draft);

		click(btn_release);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(txt_productionModelRelease)) {

			writeTestResults("Verify whether user can add QC details to QC process successfully",
					"User should be added QC details to QC process successfully", "production model released", "pass");
		} else {

			writeTestResults("Verify whether user can add QC details to QC process successfully",
					"User should be added QC details to QC process successfully", "production model not released",
					"fail");
		}

	}

	/* Con_TC_028 */
	// Verify whether user can navigate to new production order page successfully
	public void navigateTonewProductionOrderPage() throws Exception {

		click(btn_productionOrder);

		click(btn_newProductionOrder);

	}

	/* Con_TC_029 */
	// Verify whether user can fill production information data of production order
	// successfully

	public void informationProductionOrder() throws Exception {

		click(btn_outputProduct);

		Thread.sleep(2000);

		sendKeys(txt_product, productCodeData);

//		sendKeys(txt_product, productData);

		pressEnter(txt_product);

		Thread.sleep(3000);

		doubleClick(doubleClickproduct);

		sendKeys(txt_plannedqty, plannedqtyData);

		selectIndex(txt_CostingPriority, 1);

//		click(btn_Infinite);

		click(btn_MRPEnabled);

	}

	/* Con_TC_030, Con_TC_031, Con_TC_032, Con_TC_033 */

	// Verify whether user can fill summary data of production order successfully
	// Verify that user able to run the MRP successfully

	public void fillProductionOrder() throws Exception {

		selectIndex(txt_productionOrderGroup, 1);

		sendKeys(txt_productionOrderDesc, productionOrderDescData);

		click(btn_productionOrderModel);

		sendKeys(txt_productionOrderModel, modelCodeData);

		pressEnter(txt_productionOrderModel);

		doubleClick(doubleClickOrderModel);

		click(btn_draft);

		Thread.sleep(4000);
		click(btn_release);

		productionOrder = getText(txt_productionOrderNo);

		click(navigateMenu);

		click(btn_Production);

		click(btn_ProductionControl);

		explicitWait(txt_searchProductionOrder, 30);

		sendKeys(txt_searchProductionOrder, productionOrder);

		click(btn_searchProductionOrder);

		Thread.sleep(3000);

		click(btn_checkBoxProductionOrderNo);

		click(btn_action);

		click(btn_RunMRP);

		click(txt_MRPNo);

		click(btn_run);

		click(btn_releaseProductionOrder);

		Thread.sleep(3000);

//		click(btn_close);
		pageRefersh();

	}

	/* Con_TC_034 */
	// Verify that user able to release the internal order for production order
	// successfully

	public void releaseInternalOrder() throws Exception {

		click(btn_entutionLogo);

		click(btn_taskEvent);

		click(btn_InternalDispatchOrder);

		Thread.sleep(3000);

		click(btn_arrow);

		Thread.sleep(3000);

		switchWindow();

		sendKeys(txt_shippingAddress, shippingAddressData);

		click(btn_draft);

		Thread.sleep(3000);

		click(btn_release);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");
		explicitWait(txt_internalDispatchOrderReleased, 40);
		if (isDisplayed(txt_internalDispatchOrderReleased)) {

			writeTestResults(
					"Verify that user able to release the internal dispatch order for production order successfully",
					"The internal dispatch order should be released successfully",
					"The internal dispatch order released", "pass");
		} else {

			writeTestResults(
					"Verify that user able to release the internal dispatch order for production order successfully",
					"The internal dispatch order should be released successfully",
					"The internal dispatch order not released", "fail");
		}
	}

	/* Con_TC_035 */

	// Verify that user can complete shop floor update successfully

	public void completeShopFloorUpdate() throws Exception {

//		selectIndex(txt_productionOrderGroup, 1);
//
//		sendKeys(txt_productionOrderDesc, productionOrderDescData);
//
//		click(btn_productionOrderModel);
//
//		sendKeys(txt_productionOrderModel, modelCodeData);
//
//		pressEnter(txt_productionOrderModel);
//
//		doubleClick(doubleClickOrderModel);
//
//		click(btn_draft);
//
//		click(btn_release);
//
//		productionOrder = getText(txt_productionOrderNo);

//		click(navigateMenu);
//
//		click(btn_Production);
//
//		click(btn_ProductionControl);
//
//		Thread.sleep(3000);
//
//		sendKeys(txt_searchProductionOrder, productionOrder);
//
//		click(btn_searchProductionOrder);
//
//		Thread.sleep(3000);
//
//		click(btn_checkBoxProductionOrderNo);
//
//		click(btn_action);
//
//		click(btn_RunMRP);
//
//		click(txt_MRPNo);
//
//		click(btn_run);
//
//		click(btn_releaseProductionOrder);
//
//		click(btn_close);

//		click(navigateMenu);
//
//		click(btn_production);
		Thread.sleep(2000);

		click(btn_productionOrder);

		Thread.sleep(2000);

		sendKeys(txt_prodOrderNo, productionOrder);

		pressEnter(txt_prodOrderNo);

		Thread.sleep(3000);

		click(doubleClickprodNo.replace("number", productionOrder));

		pageRefersh();

		click(btn_action);

		click(btn_shopFloorUpdate);

		Thread.sleep(3000);

		click(btn_start);

		Thread.sleep(2000);

		click(btn_startProcess);

		Thread.sleep(2000);

		click(btn_actual);

		Thread.sleep(4000);
		sendKeys(txt_producedQty, producedData);

		Thread.sleep(2000);

		click(btn_update);

		Thread.sleep(2000);

		click(btn_inputProduct);

		sendKeys(txt_actualQty, actualQtyData);

		click(btn_update);

//		Thread.sleep(2000);
//
////		click(btn_closeShop);
//		click("//div[9]//span[2][@class='headerclose']");

		pageRefersh();
		Thread.sleep(2000);

		click(btn_action);

		click(btn_shopFloorUpdate);

		click(btn_completeTick);

		click(txt_complete);

		pageRefersh();

	}

	/* Con_TC_036 */

	// Verify that user can complete production label print successfully

	public void completeproductionLabel() throws Exception {

		click(btn_labelPrint);

		click(btn_view);

		selectIndex(txt_batchNo, 1);

		selectIndex(txt_machineNo, 1);

		selectIndex(txt_color, 2);

		click(btn_draftNPrint);
		switchWindow();
		switchWindow();
		click("//*[@id=\"inspect\"]/div[6]/div[1]/span[2]");

	}

	/* Con_TC_037 */

	// Verify that user can complete the production QC successfully

	public void productionLabelPrint() throws Exception {

		click(btn_labelPrint);

		click(btn_labelPrintHistory);

		click(btn_productOrder);

		click(btn_QcComplete);

		Thread.sleep(3000);
	}

	/* Con_TC_038 */
	// Verify that user can complete the production costing successfully

	public void productionCosting() throws Exception {

//		click(navigateMenu);
//
//		click(btn_Production);

		click(btn_productionOrder);

		sendKeys(txt_productonOrder, productionOrder);

		click(doubleClickprodNo.replace("number", productionOrder));

		click(btn_action);

		click(btn_productionCosting);

		click(btn_costingRefresh);

		click(btn_inputProductsCostingTab);

		click(btn_updateCosting);

		Thread.sleep(2000);

		click(btn_closeCosting);

		click(btn_tick);

		click(btn_update);

	}

	/* Con_TC_039 */
	// Verify that user can generate prodction internal receipt successfully

	public void internalReceipt() throws Exception {

		Thread.sleep(3000);

		click(btn_internalReceiptControl);

		click(btn_smallcheckBox);

		Random rand = new Random();

		// Generate random integers in range 0 to 999
		int rand_int1 = rand.nextInt(10000);

		lotNo = lotNo + Integer.toString(rand_int1);

		sendKeys(txt_lotNo, lotNo);

		click(btn_generateReceipt);

	}

	/* Con_TC_040 */
	// Verify that user can successfully navigate to the Organization Management
	// module

	public void organizationManagement() throws Exception {

		click(btn_organizationManagement);

		if (isDisplayed(txt_organizationManagement)) {

			writeTestResults("Verify whether user can select Organization Management module successfully",
					"The new organization module should be loaded successfully", "organization module page loaded",
					"pass");
		} else {

			writeTestResults("Verify whether user can select Organization Management module successfully",
					"The new organization module should be loaded successfully", "organization module page not loaded",
					"fail");
		}

	}

	/* Con_TC_041 */

	// Verify that user can successfully navigate to the Location Information Form

	public void locationInformation() throws Exception {

		click(btn_locationInfo);

		if (isDisplayed(txt_locationInformation)) {

			writeTestResults("Verify whether user can User should be able to view a Existing Location Information",
					"Existing Location Information should be loaded successfully",
					"Existing Location Information page loaded", "pass");
		} else {

			writeTestResults("Verify whether user can User should be able to view a Existing Location Information",
					"Existing Location Information should be loaded successfully",
					"Existing Location Information page not loaded", "fail");
		}
	}

	/* Con_TC_042 */

	// Verify that user can successfully Create a New Location Information and Fill
	// Required Information in Summary Tab

	public void newLocation() throws Exception {

		Random rand = new Random();
		int rand_int2 = rand.nextInt(100000);

		locationCodeData = locationCodeData + Integer.toString(rand_int2);

		click(btn_locationInfo);

		click(btn_newLocation);

		sendKeys(txt_locationCode, locationCodeData);

		// locationCode = getText(txt_locationCode);

		sendKeys(txt_description, descriptionData);

		selectIndex(txt_businessUnit, 6);
		Thread.sleep(3000);
		selectText(txt_inputWare, inputwareData);

		selectText(txt_outWare, outputwareData);

		selectText(txt_WIPWare, WIPwareData);

		selectIndex(txt_site, 1);

		click(txt_overheadInformation);

		click(btn_searchNewLocation);

		sendKeys(txt_overhead, overheadData);

		pressEnter(txt_overhead);

		doubleClick(doubleClickOverhead);

		sendKeys(txt_rate, rateData);

		click(btn_draftLocation);

		if (isDisplayed(txt_locationInformation)) {

			writeTestResults(
					"Verify that user can successfully Create a New Location Information and Fill Required Information in Summary Tab",
					"new location information should be loaded successfully",
					"new location Location Information page loaded", "pass");
		} else {

			writeTestResults(
					"Verify that user can successfully Create a New Location Information and Fill Required Information in Summary Tab",
					"new location information should be loaded successfully",
					"new location Location Information page not loaded", "fail");
		}

	}

	/* Con_TC_043 */

	// Verify that user can sucessfully Activate the Location Information
	public void overheadInformation() throws Exception {

		keyDownClick(txt_Unit, locationCodeData);

		Thread.sleep(2000);

		click(txt_activate);

		click(btn_yes);

	}

	/* Con_TC_044 */

	// Verify that user can sucessfully create the Overhead for production

	public void navigateToOverheadInfo() throws Exception {

		click(txt_overheadInfo);
	}

	public void createOverheadInformation() throws Exception {

		click(btn_new);

		Random rand = new Random();
		int rand_int2 = rand.nextInt(10000);

		overheadCodeData = overheadCodeData + Integer.toString(rand_int2);

		sendKeys(txt_overheadCode, overheadCodeData);

		sendKeys(txt_descOverhead, overheadDescData);

		selectIndex(txt_overheadGroup, 1);

		selectIndex(txt_rateType, 1);

		click(btn_draftOverhead);

	}

	public void searchcreatedOverheadInformation() throws Exception {

		click(txt_newOverheadCode.replace("code", overheadCodeData));

		click(btn_active);

		click(btn_yes);

	}

	/* Con_TC_045 */
	/* Con_TC_046 */
	// Verify that user can successfully create a new Pricing Profile and Fill
	// requried summary information
	public void newPricingProfile() throws Exception {

		click(btn_PricingProfile);

		click(btn_newPricingProfile);

		// create instance of Random class
		Random rand = new Random();

		// Generate random integers in range 0 to 999
		int rand_int1 = rand.nextInt(10000);

		pricingProfileCodeData = pricingProfileCodeData + Integer.toString(rand_int1);

		sendKeys(txt_pricingProfileCode, pricingProfileCodeData);

		sendKeys(txt_descPricingProfile, descPricingProfileData);

		selectIndex(txt_pricingGroup, 2);

	}

	/* Con_TC_047 */
	// Verify that user can sucessfully fill the Requried information in Estimation
	// Tab

	public void estimatePricingProfile() throws Exception {

		click(btn_PricingProfile);

		click(btn_newPricingProfile);

		click(btn_estimate);

		selectIndex(txt_material, 1);
		selectIndex(txt_labour, 1);
		selectIndex(txt_Overhead, 1);
		selectIndex(txt_service, 1);
		selectIndex(txt_expense, 1);
	}

	/* Con_TC_048 */
	// Verify that user can successfully fill the required information in Actual Tab
	public void actualCalculationPricingProfile() throws Exception {

		click(btn_PricingProfile);

		click(btn_newPricingProfile);

		click(btn_actualCalculation);

		selectIndex(txt_materialAC, 2);
		selectIndex(txt_labourAC, 2);
		selectIndex(txt_OverheadAC, 2);
		selectIndex(txt_serviceAC, 2);
		selectIndex(txt_expenseAC, 2);
	}

	/* Con_TC_049 */
	// Verify that user can Draft and Active the Pricing Profile
	public void draftandReleasePricingProfile() throws Exception {

		Thread.sleep(2000);

		click(btn_draftLocation);

		keyDownClick(txt_Pricing, pricingProfileCodeData);

		click(btn_active);

		click(btn_yes);

		if (isDisplayed(txt_activateStatus)) {

			writeTestResults("Verify the Pricing Profile Status", " Pricing Profile Status should update as Activated ",
					"Pricing Profile Status viewed as active", "pass");
		} else {

			writeTestResults("Verify the Pricing Profile Status", " Pricing Profile Status should update as Activated",
					"Pricing Profile Status not viewed as active", "fail");
		}

	}

	/* Con_TC_050 */
	// Verify that user can successfully delete a existing pricing profile
	public void deletePricingProfile() throws Exception {

		click(btn_draftLocation);

		keyDownClick(txt_Pricing, pricingProfileCodeData);

		if (isDisplayed(txt_openStatus)) {

			writeTestResults("Verify the Pricing Profile Status", " Pricing Profile Status should update as open ",
					"Pricing Profile Status viewed as open", "pass");
		} else {

			writeTestResults("Verify the Pricing Profile Status", " Pricing Profile Status should update as open",
					"Pricing Profile Status not viewed as open", "fail");
		}

		click(btn_delete);

		click(btn_yes);

		if (isDisplayed(txt_activateStatus)) {

			writeTestResults("Verify the Pricing Profile Status", " Pricing Profile Status should update as Activated ",
					"Pricing Profile Status viewed as active", "pass");
		} else {

			writeTestResults("Verify the Pricing Profile Status", " Pricing Profile Status should update as Activated",
					"Pricing Profile Status not viewed as active", "fail");
		}

		click(btn_delete);

	}

	/* Con_TC_051 */
	// Verify that user can successfully navigate to the Bulk Production Form

	public void navigateToBulkProductionForm() throws Exception {

		click(btn_bulkProductionOrder);

		if (isDisplayed(txt_BulkProduction)) {

			writeTestResults("Verify that user can successfully Open a Bulk Production order",
					" User should be able to Open a Bulk Production order Sucessfully",
					"Bulk Production order form page loaded", "pass");
		} else {

			writeTestResults("Verify that user can successfully Open a Bulk Production order",
					" User should be able to Open a Bulk Production order Sucessfully",
					"Bulk Production order form page not loaded", "fail");
		}

	}

	/* Con_TC_052 */
	// Verify whether user can enter summary information of Bulk Prodution Order

	public void SummaryBulkProductionOrder() throws Exception {

		click(new_bulkProductionOrder);

		selectIndex(txt_productionOrderGroup, 2);

		sendKeys(txt_descrProductionOrderGroup, productionOrderGroupData);

		selectIndex(txt_referenceType, 1);

		click(btn_yes);

		click(btn_productionUnitBulk);

		sendKeys(txt_productionUnitBulk, productionUnitBulkData);

		pressEnter(txt_productionUnitBulk);

		doubleClick(doubleClickProductionUnitBulk);

		if (isDisplayed(txt_BulkProductionForm)) {

			writeTestResults("Verify whether user can enter summary information of Bulk Prodution Order",
					"Bulk production form should be displayed successfully", "Bulk production form page loaded",
					"pass");
		} else {

			writeTestResults("Verify whether user can enter summary information of Bulk Prodution Order",
					"Bulk production form should be displayed successfully", "Bulk production form page not loaded",
					"fail");
		}

	}

	/* Con_TC_053 */
	// Verify whether user can enter Product informations on Bulk Prodution Order
	public void ProductBulkProductionOrder() throws Exception {

		click(btn_productTab);

		click(btn_productSearch);

		sendKeys(txt_productSearch, productSearchData);

		pressEnter(txt_productSearch);

		Thread.sleep(3000);

		doubleClick(doubleClickProductSearch);

		Thread.sleep(3000);

		click(btn_productModel);

		sendKeys(txt_productModel, modelData);

		pressEnter(txt_productModel);

		doubleClick(doubleClickProductModel);

		sendKeys(txt_plannedQtyBulk, plannedqtyBulkData);

		sendKeys(txt_standardCost, standardCostData);

		click(btn_checkOut);

		Thread.sleep(10000);

		click(btn_draftProductionOrder);
	}

	/* Con_TC_054 */
	// Verify Whether user can draft and release the Bulk Production Order

	public void draftNReleaseBulkProductionOrder() throws Exception {

		Thread.sleep(3000);

		click(btn_releaseBulkProductionOrder);
	}

	/* Con_TC_055 */
	// Verify Whether user can generate a Internal Order for the Input Products
	public void generateInternalOrder() throws Exception {

		click(btn_actionBulk);

		click(btn_createInternalOrder);

		Thread.sleep(3000);

		click(btn_selectAll);

		click(btn_applyInternal);
	}

	/* Con_TC_056 */
	// Verify whether user can load the internal order related to the Bulk
	// Production Order
	public void loadInternalOrderOrder() throws Exception {

		switchWindow();

		sendKeys(txt_title, titleData);

		click(btn_requester);

		sendKeys(txt_requester, requesterData);

		pressEnter(txt_requester);

		Thread.sleep(3000);

		doubleClick(doubleClickRequester);
	}

	/* Con_TC_057 */
	// Verify Whether user can draft and release the Internal order
	public void draftNReleaseInternalOrder() throws Exception {

		click(btn_draft);

		Thread.sleep(3000);

		click(btn_release);

		click(btn_goTopage);
	}

	/* Con_TC_058 */
	// Verify whether user can complete the bulk Production Order
	public void completeProductionOrder() throws Exception {

		switchWindow();

		sendKeys(txt_shipping, shippingData);

		click(btn_draft);

		Thread.sleep(4000);

		click(btn_Capture);

		Thread.sleep(4000);

		click(btn_productListClick);

		sendKeys(txt_batchNoTXT, batchData);

		pressEnter(txt_batchNoTXT);

		click(btn_refresh);

		click(btn_back);

		click(btn_release);

		click("//*[@id=\"inspect\"]/div[5]/div[3]/a");

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(txt_internalDispatchRelease)) {

			writeTestResults("Verify the document status", "document status should be displayed successfully",
					"Document status displayed as Released", "pass");
		} else {

			writeTestResults("Verify the document status", "document status should be displayed successfully",
					"Document status shouldn't displayed as Released", "fail");
		}
	}

	/* Con_TC_059 */
	// Verify Whether user can generate a Internal Order for the Input Products
	public void generateInternalOrderForInputProducts() throws Exception {

		closeWindow();

		switchWindow();

		click(btn_closeTab);

		Thread.sleep(3000);

		click(btn_actionBulk);

		click(btn_productionCosting);

		click(btn_date);

		click(btn_actualDate);

		sendKeys(txt_actualQty1, actualQtyData1);

		click(btn_update);

		click(btn_input);

		sendKeys(txt_actualQty1, actualQtyData1);

		click(btn_releaseTab);

		click("//*[@id=\"inspect\"]/div[5]/div[3]/a");

		if (isDisplayed(txt_internalReceipt)) {

			writeTestResults(" Verify Internal Receipt Number is generated",
					"Internal Receipt Number should be displayed successfully", "Internal Receipt Number loaded",
					"pass");
		} else {

			writeTestResults(" Verify Internal Receipt Number is generated",
					"Internal Receipt Number should be displayed successfully", "Internal Receipt Number not loaded",
					"fail");
		}
	}

	/* Con_TC_060 */
	// Verfiy whether user can convert the bulk Production order to Internal Receipt
	public void convertInternalOrder() throws Exception {

		Thread.sleep(3000);

		click(btn_actionBulk);

		click(btn_internalReceipt);

		click(btn_smallcheckBox);

		sendKeys(txt_lotNo, lotNo);

		click(btn_generate);

		click(btn_closeTab);

		bulkNo = getText(bulkno);

		if (isDisplayed(txt_internalReceipt)) {

			writeTestResults(" Verify Internal Receipt Number is generated",
					"Internal Receipt Number should be displayed successfully", "Internal Receipt Number loaded",
					"pass");
		} else {

			writeTestResults(" Verify Internal Receipt Number is generated",
					"Internal Receipt Number should be displayed successfully", "Internal Receipt Number not loaded",
					"fail");
		}

		navigateToSideBar();
		navigateToProduction();
		Thread.sleep(2000);
		click(btn_bulkProductionOrder);
		Thread.sleep(2000);
		navigateBulkProductionForm();

	}

	/* Con_TC_061 */
	// Verify that user can successfully navigate to the Bulk Production Form
	public void navigateBulkProductionForm() throws Exception {

		sendKeys(txt_bulkOrder, bulkNo);

		pressEnter(txt_bulkOrder);

		Thread.sleep(3000);

		click(bulkOrder.replace("no", bulkNo));

	}

	/* Con_TC_062 */
	// Verify whether user can Create a Bulk Production order and Enter Required
	// Information in Summary Tab

	public void SummaryBulkProductionOrder1() throws Exception {

		click(new_bulkProductionOrder);

		selectIndex(txt_productionOrderGroup, 2);

		Thread.sleep(2000);

		sendKeys(txt_descrProductionOrderGroup, productionOrderGroupData);

		Thread.sleep(2000);

		selectIndex(txt_referenceType, 0);

		click(btn_yes);

		click(btn_productionUnitBulk);

		sendKeys(txt_productionUnitBulk, productionUnitBulkData);

		pressEnter(txt_productionUnitBulk);

		doubleClick(doubleClickProductionUnitBulk);

		if (isDisplayed(txt_BulkProductionForm)) {

			writeTestResults("Verify whether user can enter summary information of Bulk Prodution Order",
					"Bulk production form should be displayed successfully", "Bulk production form page loaded",
					"pass");
		} else {

			writeTestResults("Verify whether user can enter summary information of Bulk Prodution Order",
					"Bulk production form should be displayed successfully", "Bulk production form page not loaded",
					"fail");
		}

	}

	/* Con_TC_063 */
	// Verify whether user can enter Required Information in Product Tab
	public void ProductBulkProductionOrder1() throws Exception {

		click(btn_productTab);

		click(btn_productSearch);

		sendKeys(txt_productSearch, productSearchData);

		pressEnter(txt_productSearch);

		Thread.sleep(3000);

		doubleClick(doubleClickProductSearch);

		Thread.sleep(3000);

		sendKeys(txt_plannedQtyBulk, plannedqtyBulkData);

		Thread.sleep(2000);

		sendKeys(txt_standardCost1, standardCostData1);
	}

	/* Con_TC_064 */
	// Verify wherther user can select Input Products for the bulk Production
	public void productBulkProduction() throws Exception {

		click(btn_inputProductsTab);

		click(btn_searchProduct);

		Thread.sleep(2000);

		sendKeys(txt_searchProduct, searchProductData);

		pressEnter(txt_searchProduct);

		Thread.sleep(2000);

		doubleClick(doubleClickSearchProduct);

		Thread.sleep(2000);

		sendKeys(txt_plannedqty1, plannedqtyData1);

	}

	/* Con_TC_065 */
	// Verify whether user can Draft and Release the Bulk Production Order

	public void ReleaseBulkProductionOrder() throws Exception {

		click(btn_draftProductionOrder);

		Thread.sleep(2000);

		click(btn_releaseBulkProductionOrder);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(txt_BulkOrderRelease)) {

			writeTestResults("Verify the document status", "document status should be displayed successfully",
					"Document status displayed as Released", "pass");
		} else {

			writeTestResults("Verify the document status", "document status should be displayed successfully",
					"Document status shouldn't displayed as Released", "fail");
		}
	}

	/* Con_TC_066 */
	// Verify Whether user can generate a Internal Order for the Input Products
	public void generateInternalOrderForInputProduct() throws Exception {

		click(btn_actionBulk);

		click(btn_createInternalOrder);

		Thread.sleep(3000);

		click(btn_selectAll);

		click(btn_applyInternal);
	}

	/* Con_TC_067 */
	// Verify whether user can load the internal order related to the Bulk
	// Production Order
	public void loadInternalOrderOrderRelatedToBulk() throws Exception {

		switchWindow();

		sendKeys(txt_title, titleData);

		click(btn_requester);

		sendKeys(txt_requester, requesterData);

		pressEnter(txt_requester);

		Thread.sleep(3000);

		doubleClick(doubleClickRequester);

	}

	/* Con_TC_068 */
	// Verify Whether user can draft and release the Internal order
	public void draftReleaseInternalOrder() throws Exception {

		click(btn_draft);

		Thread.sleep(3000);

		click(btn_release);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(txt_internalOrderRelease)) {

			writeTestResults("Verify the document status", "document status should be displayed successfully",
					"Document status displayed as Released", "pass");
		} else {

			writeTestResults("Verify the document status", "document status should be displayed successfully",
					"Document status shouldn't displayed as Released", "fail");
		}

		click(btn_goTopage);

	}

	/* Con_TC_069 */
	// Verify whther user can convert the Internal Order to Internal Dispatch order

	public void completeProductionOrder1() throws Exception {

		switchWindow();

		sendKeys(txt_shipping, shippingData);

		click(btn_draft);

		Thread.sleep(3000);

		click(btn_release);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(txt_internalDispatchRelease)) {

			writeTestResults("Verify the document status", "document status should be displayed successfully",
					"Document status displayed as Released", "pass");
		} else {

			writeTestResults("Verify the document status", "document status should be displayed successfully",
					"Document status shouldn't displayed as Released", "fail");
		}
	}

	/* Con_TC_070 */
	// Verify whether user can complete the bulk Production Order.
	public void completeBulkProductionOrder() throws Exception {

		closeWindow();

		switchWindow();

		click(btn_closeTab);

		Thread.sleep(3000);

		click(btn_actionBulk);

		click(btn_productionCosting);

		click(btn_date);

		click(btn_actualDate);

		sendKeys(txt_actualQty1, actualQtyData1);

		Thread.sleep(2000);

		click(btn_update);

		click(btn_input);

		sendKeys(txt_actualQty1, actualQtyData1);

		click(btn_releaseTab);

		if (isDisplayed(txt_internalReceipt)) {

			writeTestResults(" Verify Internal Receipt Number is generated",
					"Internal Receipt Number should be displayed successfully", "Internal Receipt Number loaded",
					"pass");
		} else {

			writeTestResults(" Verify Internal Receipt Number is generated",
					"Internal Receipt Number should be displayed successfully", "Internal Receipt Number not loaded",
					"fail");
		}

	}

	/* Con_TC_071 */
	// Verfiy whether user can convert the bulk Production order to Internal Receipt

	public void convertInternalReceipt() throws Exception {

		Thread.sleep(2000);

		click(btn_actionBulk);

		click(btn_internalReceipt);

		Thread.sleep(2000);

		click(btn_smallcheckBox);

		Random rand = new Random();

		// Generate random integers in range 0 to 999
		int rand_int1 = rand.nextInt(1000);

		lotNo = lotNo + Integer.toString(rand_int1);

		sendKeys(txt_lotNo, lotNo);

		click(btn_generate);

		click(btn_closeTab);

		if (isDisplayed(txt_internalReceipt)) {

			writeTestResults(" Verify Internal Receipt Number is generated",
					"Internal Receipt Number should be displayed successfully", "Internal Receipt Number loaded",
					"pass");
		} else {

			writeTestResults(" Verify Internal Receipt Number is generated",
					"Internal Receipt Number should be displayed successfully", "Internal Receipt Number not loaded",
					"fail");
		}

	}

	/* common key down click */
	public void keyDownClick(String locator, String value) throws Exception {
		sendKeys(locator, value);
		Thread.sleep(3000);
		driver.findElement(getLocator(locator)).sendKeys(Keys.ARROW_DOWN);
		pressEnter(locator);
	}

	// creating output product
	// CON_TC_072
	public void newOutputProduct() throws Exception {

//		navigateToTheLoginPage();
//		verifyTheLogo();
//		userLogin();
//		navigateToSideBar();
//		navigateToInventory();

		click(btn_productInfo);

		click(btn_newProduct);

		Random random = new Random();
		int rand_intcode = random.nextInt(100000);

		productCodeData = productCodeData + Integer.toString(rand_intcode);

		sendKeys(txt_productCode, productCodeData);

		sendKeys(txt_productDescription, productCodeData);

		selectText(txt_prodGrop, productGroupData);

		sendKeys(txt_basePrice, basePriceData);

		click(btn_manufacturer);

		sendKeys(txt_manufacturer, manufacturerData);

		pressEnter(txt_manufacturer);

		doubleClick(doubleCLickManufacturer);

		selectIndex(txt_UOMGroup, 1);

		selectIndex(txt_UOM, 1);

		Thread.sleep(2000);

		// Length
		sendKeys(txt_length, lengthData);
		selectIndex(txt_lengthVal, 1);

		// Width
		sendKeys(txt_width, widthData);
		selectIndex(txt_widthVal, 4);

		// Height
		sendKeys(txt_height, heightData);
		selectIndex(txt_heightVal, 6);

		// Volume
		sendKeys(txt_volume, volumeData);
		selectIndex(txt_VolumeVal, 2);

		click(btn_tabDetails);

		click(btn_allowInventoryWithoutCosting);

		selectIndex(txt_manufacturingType, 1);

		selectIndex(txt_batchBook, 1);

		click(btn_draft);

		Thread.sleep(2000);

		click(btn_release);

		// product = getText(txt_productName);

	}

	// FROM CON_TC_003 to CON_TC_039
	/* SMOKE 001 */
	public void All() throws Exception {

		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		navigateToSideBar();
		navigateToInventory();
		newOutputProduct();
		Thread.sleep(2000);

		navigateToSideBar();
		navigateToInventory();
		navigateToBillOfMaterial();
		navigateTonewBOM();
		fillingBOMForm();
		completeNreleaseBOM();

		navigateToSideBar();
		navigateToProduction();
		navigateToBillOfOperation();
		navigateTonewBOO();
		fillBOOForm();
		OperationActivitySupply();
		OperationActivityOperation();
		QCUnderTheBillOfOperations();

		Thread.sleep(2000);

		navigateToSideBar();
		navigateToProduction();
		navigateToProductionModel();
		ProductionModelForm();
		ProductionModelFormOnlyMRPEnable();
		BOODetails();
		BOODetailsTabSupply();
		BOODetailsTabOperation();
		BOODetailsTabQC();
		Thread.sleep(2000);

		navigateToSideBar();
		navigateToProduction();
		navigateTonewProductionOrderPage();
		informationProductionOrder();
		fillProductionOrder();
		releaseInternalOrder();
		Thread.sleep(3000);

		navigateToSideBar();
		navigateToProduction();
		completeShopFloorUpdate();

		click("//*[@id=\"inspect\"]/div[8]/div[1]/span[2]");

		Thread.sleep(2000);
		navigateToSideBar();
		navigateToProduction();
		completeproductionLabel();

	}

	/* SMOKE 002 */
	public void AllInOne() throws Exception {

		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		navigateToSideBar();
		navigateToProduction();
		click(btn_labelPrint);
		productionLabelPrint();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Thread.sleep(4000);
		navigateToSideBar();
		navigateToProduction();
		productionCosting();

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$(closeMessageBox()).click()");

		click("//*[@id=\"inspect\"]/div[7]/div[1]/span[2]");

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Thread.sleep(2000);

		click(btn_action);

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Thread.sleep(4000);

		click(btn_internalReceiptControl);

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Thread.sleep(3000);

		click(btn_smallcheckBox);

		Random rand = new Random();

		// Generate random integers in range 0 to 999
		int rand_int1 = rand.nextInt(10000);

		lotNo = lotNo + Integer.toString(rand_int1);

		sendKeys(txt_lotNo, lotNo);

		click(btn_generateReceipt);

		click("//span[@class='headerclose']");

		click(btn_actionBulk);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Thread.sleep(3000);
		click(btn_internalReceipt);

		click(link_internalReceipt);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(txt_internalReceiptReleased)) {

			writeTestResults(" Verify that user able to release internal receipt successfully",
					"internal receipt should released successfully", "internal receipt released successfully", "pass");
		} else {

			writeTestResults("Verify that user able to release internal receipt successfully",
					"internal receipt should released successfully", "internal receipt not released successfully",
					"fail");
		}

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Thread.sleep(2000);

		switchWindow();
		click(btn_release);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Thread.sleep(3000);
		Actions action = new Actions(driver);
		WebElement we = driver.findElement(By.xpath(btn_actionJournal));
		action.moveToElement(we).build().perform();
		click(btn_actionJournal);

		click(btn_journalEntry);

		click(link_gernalEntryDetails);

		switchWindow();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Thread.sleep(5000);

		if (isDisplayed(txt_journalEntryReleased)) {

			writeTestResults(" Verify that user able to view journal entry successfully",
					"journal entry should released successfully", "journal entry released successfully", "pass");
		} else {

			writeTestResults("Verify that user able to view journal entry successfully",
					"journal entry should released successfully", "journal entry not released successfully", "fail");
		}

	}

	/* New Regression Test Cases */

	public void PROD_PO_010() throws Exception {

		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		navigateToSideBar();
		navigateToInventory();
		newOutputProduct();

		// Thread.sleep(2000);

		navigateToSideBar();
		navigateToInventory();
		navigateToBillOfMaterial();
		navigateTonewBOM();
		fillingBOMForm();
		completeNreleaseBOM();

		navigateToSideBar();
		navigateToProduction();
		navigateToBillOfOperation();
		navigateTonewBOO();
		fillBOOForm();
		OperationActivitySupply();
		OperationActivityOperation();
		QCUnderTheBillOfOperations();

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Thread.sleep(2000);

		navigateToSideBar();
		navigateToProduction();
		navigateToProductionModel();
		ProductionModelForm();
		ProductionModelFormOnlyMRPEnable();
		BOODetails();
		BOODetailsTabSupply();
		BOODetailsTabOperation();
		BOODetailsTabQC();

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Thread.sleep(2000);

		navigateToSideBar();
		navigateToProduction();
		navigateTonewProductionOrderPage();
		informationProductionOrder();

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Thread.sleep(2000);

		selectIndex(txt_productionOrderGroup, 1);

		sendKeys(txt_productionOrderDesc, productionOrderDescData);

		click(btn_productionOrderModel);

		sendKeys(txt_productionOrderModel, modelCodeData);

		pressEnter(txt_productionOrderModel);

		doubleClick(doubleClickOrderModel);

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Thread.sleep(2000);

		click(btn_draft);

		productionOrder = getText(txt_productionOrderNo);

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Thread.sleep(2000);

		navigateToSideBar();
		navigateToProduction();

		click(btn_productionorder);

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Thread.sleep(2000);

		sendKeys(txt_searchProductionOrder1, productionOrder);

		pressEnter(txt_searchProductionOrder1);

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Thread.sleep(3000);

		click(doubleClickprodNo.replace("number", productionOrder));

		click(btn_edit);

		selectIndex(txt_productionOrderGroup, 2);

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Thread.sleep(3000);

		click(btn_Update);

	}

	public void PROD_PO_011() throws Exception {

		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		navigateToSideBar();
		navigateToInventory();
		newOutputProduct();

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Thread.sleep(2000);

		navigateToSideBar();
		navigateToInventory();
		navigateToBillOfMaterial();
		navigateTonewBOM();
		fillingBOMForm();
		completeNreleaseBOM();

		navigateToSideBar();
		navigateToProduction();
		navigateToBillOfOperation();
		navigateTonewBOO();
		fillBOOForm();
		OperationActivitySupply();
		OperationActivityOperation();
		QCUnderTheBillOfOperations();

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Thread.sleep(2000);

		navigateToSideBar();
		navigateToProduction();
		navigateToProductionModel();
		ProductionModelForm();
		ProductionModelFormOnlyMRPEnable();
		BOODetails();
		BOODetailsTabSupply();
		BOODetailsTabOperation();
		BOODetailsTabQC();

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Thread.sleep(2000);

		navigateToSideBar();
		navigateToProduction();
		navigateTonewProductionOrderPage();
		informationProductionOrder();

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Thread.sleep(2000);

		selectIndex(txt_productionOrderGroup, 1);

		sendKeys(txt_productionOrderDesc, productionOrderDescData);

		click(btn_productionOrderModel);

		sendKeys(txt_productionOrderModel, modelCodeData);

		pressEnter(txt_productionOrderModel);

		doubleClick(doubleClickOrderModel);

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Thread.sleep(2000);

		click(btn_draft);

		productionOrder = getText(txt_productionOrderNo);

		Thread.sleep(2000);

		navigateToSideBar();
		navigateToProduction();

		click(btn_productionorder);

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Thread.sleep(2000);

		sendKeys(txt_searchProductionOrder1, productionOrder);

		pressEnter(txt_searchProductionOrder1);

		Thread.sleep(3000);

		click(doubleClickprodNo.replace("number", productionOrder));

		click(btn_duplicate);

		sendKeys(txt_plannedqty, plannedqtyData);

		String outputProduct1 = getText(txt_outputProduct1);
		String outputProduct2 = getText(txt_outputProduct2);

		if (outputProduct1.equals(outputProduct2)) {
			writeTestResults("output product values should be equal", "Should be able to equal output product values",
					"output products are equal", "pass");
		}

		else {
			writeTestResults("output product values should be equal", "Should be able to equal output product values",
					"output products are not equal", "fail");
		}

		Thread.sleep(2000);

		String costingPriority1 = getText(txt_costingPriority1);
		String costingPriority2 = getText(txt_costingPriority2);

		if (costingPriority1.equals(costingPriority2)) {
			writeTestResults("costing priority values should be equal",
					"Should be able to equal costing priority values", "costing priority values are equal", "pass");
		}

		else {
			writeTestResults("costing priority values should be equal",
					"Should be able to equal costing priority values", "costing priority values are not equal", "fail");
		}

		Thread.sleep(2000);

		String prodOrderGroup1 = getText(txt_prodOrderGroup1);
		String prodOrderGroup2 = getText(txt_prodOrderGroup2);

		if (prodOrderGroup1.equals(prodOrderGroup2)) {
			writeTestResults("Product Order Group values should be equal",
					"Should be able to equal product order group values", "product order groups are equal", "pass");
		}

		else {
			writeTestResults("Product Order Group values should be equal",
					"Should be able to equal product order group values", "product order groups are not equal", "fail");
		}

		Thread.sleep(2000);

		String description1 = getText(txt_description1);
		String description2 = getText(txt_description2);

		if (description1.equals(description2)) {
			writeTestResults("Description values should be equal", "Should be able to equal Description values",
					"Description are equal", "pass");
		}

		else {
			writeTestResults("Description values should be equal", "Should be able to equal Description values",
					"Description are not equal", "fail");
		}

		Thread.sleep(2000);

		String prodModel1 = getText(txt_prodModel1);
		String prodModel2 = getText(txt_prodModel2);

		if (prodModel1.equals(prodModel2)) {
			writeTestResults("product model values should be equal", "Should be able to equal product model values",
					"product model are equal", "pass");
		}

		else {
			writeTestResults("product model values should be equal", "Should be able to equal product model values",
					"product model are not equal", "fail");
		}

	}

	public void PROD_PO_012() throws Exception {

		navigateToTheLoginPage();
		verifyTheLogo();
		userLogin();
		navigateToSideBar();
		navigateToInventory();
		newOutputProduct();

		Thread.sleep(2000);

		navigateToSideBar();
		navigateToInventory();
		navigateToBillOfMaterial();
		navigateTonewBOM();
		fillingBOMForm();
		completeNreleaseBOM();

		navigateToSideBar();
		navigateToProduction();
		navigateToBillOfOperation();
		navigateTonewBOO();
		fillBOOForm();
		OperationActivitySupply();
		OperationActivityOperation();
		QCUnderTheBillOfOperations();

		Thread.sleep(2000);

		navigateToSideBar();
		navigateToProduction();
		navigateToProductionModel();
		ProductionModelForm();
		ProductionModelFormOnlyMRPEnable();
		BOODetails();
		BOODetailsTabSupply();
		BOODetailsTabOperation();
		BOODetailsTabQC();

		Thread.sleep(2000);

		navigateToSideBar();
		navigateToProduction();
		navigateTonewProductionOrderPage();
		informationProductionOrder();

		Thread.sleep(2000);

		selectIndex(txt_productionOrderGroup, 1);

		sendKeys(txt_productionOrderDesc, productionOrderDescData);

		click(btn_productionOrderModel);

		sendKeys(txt_productionOrderModel, modelCodeData);

		pressEnter(txt_productionOrderModel);

		doubleClick(doubleClickOrderModel);

		click(btn_draft);

		click(btn_release);

		productionOrder = getText(txt_productionOrderNo);

		Thread.sleep(2000);

		navigateToSideBar();
		navigateToProduction();

		click(btn_productionorder);

		Thread.sleep(2000);

		sendKeys(txt_searchProductionOrder1, productionOrder);

		pressEnter(txt_searchProductionOrder1);

		Thread.sleep(3000);

		click(doubleClickprodNo.replace("number", productionOrder));

		click(btn_duplicate);

		sendKeys(txt_plannedqty, plannedqtyData);

		String outputProduct1 = getText(txt_outputProduct1);
		String outputProduct2 = getText(txt_outputProduct2);

		if (outputProduct1.equals(outputProduct2)) {
			writeTestResults("output product values should be equal", "Should be able to equal output product values",
					"output products are equal", "pass");
		}

		else {
			writeTestResults("output product values should be equal", "Should be able to equal output product values",
					"output products are not equal", "fail");
		}

		Thread.sleep(2000);

		String costingPriority1 = getText(txt_costingPriority1);
		String costingPriority2 = getText(txt_costingPriority2);

		if (costingPriority1.equals(costingPriority2)) {
			writeTestResults("costing priority values should be equal",
					"Should be able to equal costing priority values", "costing priority values are equal", "pass");
		}

		else {
			writeTestResults("costing priority values should be equal",
					"Should be able to equal costing priority values", "costing priority values are not equal", "fail");
		}

		Thread.sleep(2000);

		String prodOrderGroup1 = getText(txt_prodOrderGroup1);
		String prodOrderGroup2 = getText(txt_prodOrderGroup2);

		if (prodOrderGroup1.equals(prodOrderGroup2)) {
			writeTestResults("Product Order Group values should be equal",
					"Should be able to equal product order group values", "product order groups are equal", "pass");
		}

		else {
			writeTestResults("Product Order Group values should be equal",
					"Should be able to equal product order group values", "product order groups are not equal", "fail");
		}

		Thread.sleep(2000);

		String description1 = getText(txt_description1);
		String description2 = getText(txt_description2);

		if (description1.equals(description2)) {
			writeTestResults("Description values should be equal", "Should be able to equal Description values",
					"Description are equal", "pass");
		}

		else {
			writeTestResults("Description values should be equal", "Should be able to equal Description values",
					"Description are not equal", "fail");
		}

		Thread.sleep(2000);

		String prodModel1 = getText(txt_prodModel1);
		String prodModel2 = getText(txt_prodModel2);

		if (prodModel1.equals(prodModel2)) {
			writeTestResults("product model values should be equal", "Should be able to equal product model values",
					"product model are equal", "pass");
		}

		else {
			writeTestResults("product model values should be equal", "Should be able to equal product model values",
					"product model are not equal", "fail");
		}

	}

	/* QA Form level action verification */

	public void navigateToTheLogin() throws Exception {
		openPage(siteURL);

		writeTestResults("Verify that 'Entution' header is available on the page",
				"user should be able to see the logo", "logo is dislayed", "pass");

	}

	public void verifyThelogo() throws Exception {
		if (isDisplayed(siteLogo)) {

			writeTestResults("Verify that 'Entution' header is available on the page",
					"user should be able to see the logo", "logo is dislayed", "pass");
		} else {
			writeTestResults("Verify that 'Entution' header is available on the page",
					"user should be able to see the logo", "logo is not dislayed", "fail");

		}
	}

	public void userlogin() throws Exception {
		sendKeys(txt_username, UserNameData);

		sendKeys(txt_password, PasswordData);

		click(btn_login);

//		Thread.sleep(5000);
//		driver.switchTo().alert().dismiss();

		if (isDisplayed(lnk_home)) {

			writeTestResults("User login", "User should navigate to the Home page of Entution",
					"user navigated to the home page", "pass");
		}

		else {
			writeTestResults("User login", "User should navigate to the Home page of Entution",
					"user cant navigate to the home page", "fail");

		}
	}

	public void navigateTosideBar() throws Exception {

		click(navigateMenu);

		if (isDisplayed(sideNavBar)) {

			writeTestResults("Verify that user can successfully navigate to side menu",
					"User successfully navigate to side menu", "Side menu displayed", "pass");

		} else {

			writeTestResults("Verify that user can successfully navigate to side menu",
					"User can't successfully navigate to side menu", "Side menu is not displayed", "fail");
		}
	}

	public void navigateToProductionmenu() throws Exception {

		click(btn_Production);

		if (isDisplayed(logo_Production)) {

			writeTestResults("Verify that user can view fixed asset logo", "User can view the logo", "logo viewed",
					"pass");

		}

		else {

			writeTestResults("Verify that  user can view fixed asset logo", "User can't view the logo",
					"logo is not viewed", "fail");
		}

	}

	/* Con_TC_003 */

	// Verify that user can create a product level BOM

	public void navigatetoInventory() throws Exception {

		click(navigateMenu);
		click(btn_inventory);

		if (isDisplayed(txt_inventory)) {

			writeTestResults("Verify that user can view inventory label", "User can view the logo",
					"Inventory Label viewed", "pass");

		} else {

			writeTestResults("Verify that user can view inventory label", "User can view the logo",
					"Inventory Label doesn't viewed", "fail");

		}
	}

	public void navigatetoBillOfMaterial() throws Exception {

		click(btn_billOfMaterial);

		if (isDisplayed(label_billOfMaterial)) {

			writeTestResults("Verify that user can view bill of material logo", "User can view the logo",
					"Logo displayed", "pass");
		}

		else {

			writeTestResults("Verify that user can view bill of material logo", "User can view the logo",
					"Logo can't be displayed", "fail");
		}
	}

	public void navigatetonewBOM() throws Exception {

		click(btn_newBOM);

		if (isDisplayed(txt_newBOM)) {

			writeTestResults("Verify that the user can navigate to new BOM", "New bill of material page displayed",
					"new BOM page loaded", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to new BOM", "New bill of material page displayed",
					"new BOM page not loaded", "fail");
		}
	}

	public void fillingBOMform() throws Exception {

		sendKeys(txt_desc, descData);

		selectText(btn_productGrp, productGrpData);

		// Select output Product
		click(btn_outputProductSearch);

		Thread.sleep(2000);

		// sendKeys(txt_outputProductSearch, outputProductData);
		sendKeys(txt_outputProductSearch, productCodeData);

		pressEnter(txt_outputProductSearch);

		Thread.sleep(2000);
		doubleClick(doubleClickoutputProduct);

		sendKeys(txt_BOMQuantity, BOMQuantityData);

		click(btn_expiryDate);

		click(clickExpiryDate);

		click(btn_rawMaterial);

		Thread.sleep(5000);

		sendKeys(txt_rawMaterial, rawMaterialData);

		Thread.sleep(5000);

		pressEnter(txt_rawMaterial);

		Thread.sleep(4000);

		doubleClick(doubleClickrawMaterial);

		sendKeys(txt_rawQuantity, rawQuantityData);

		if (isDisplayed(txt_newBOM)) {

			writeTestResults(
					"Verify that the user can navigate to new BOMUser selected the raw materials under the raw materials section should be displayed successfully",
					"raw materials section should be displayed", "raw materials section displayed", "pass");
		} else {

			writeTestResults(
					"User selected the raw materials under the raw materials section should be displayed successfully",
					"raw materials section shoul be displayed", "raw materials section not displayed", "fail");
		}

	}

	public void completenreleaseBOM() throws Exception {

		click(btn_draft);
		Thread.sleep(2000);
		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the bill of materials successfully",
					"bill of materials drafted", "bill of materials drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the bill of materials successfully",
					"bill of materials drafted", "bill of materials not drafted", "fail");
		}

		Thread.sleep(2000);

		click(btn_action);
		Thread.sleep(2000);
		click(btn_Delete);
		Thread.sleep(2000);
		click(btn_yes);

		if (isDisplayed(txt_delete)) {

			writeTestResults("Verify that the user can delete the bill of materials successfully",
					"bill of materials deleted", "bill of materials deleted", "pass");
		} else {

			writeTestResults("Verify that the user can release the bill of materials successfully",
					"bill of materials deleted", "bill of materials not deleted", "fail");
		}

		click(btn_duplicate);

		if (isDisplayed(txt_duplicateBOM)) {

			writeTestResults("Verify that the user can duplicate the bill of materials successfully",
					"bill of materials duplicated", "bill of materials duplicated", "pass");
		} else {

			writeTestResults("Verify that the user can duplicate the bill of materials successfully",
					"bill of materials duplicated", "bill of materials not duplicated", "fail");
		}

		sendKeys(txt_prodGrop, productGrpData);

		click(btn_draft);

		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the bill of materials successfully",
					"bill of materials drafted", "bill of materials drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the bill of materials successfully",
					"bill of materials drafted", "bill of materials not drafted", "fail");
		}

		click(btn_edit);

		if (isDisplayed(txt_editBOM)) {

			writeTestResults("Verify that the user can edit the bill of materials successfully",
					"bill of materials edited", "bill of materials edited", "pass");
		} else {

			writeTestResults("Verify that the user can edit the bill of materials successfully",
					"bill of materials edited", "bill of materials not edited", "fail");
		}

		sendKeys(txt_prodGrop, productGrpData);

		click(btn_updateNNew);

		if (isDisplayed(txt_updateNNewBOM)) {

			writeTestResults("Verify that the user can update the bill of materials and create new successfully",
					"bill of materials updated and create a new", "bill of materials updated and create a new", "pass");
		} else {

			writeTestResults("Verify that the user can update the bill of materials and create new successfully",
					"bill of materials updated and create a new", "bill of materials not updated and create a new",
					"fail");
		}

		fillingBOMForm();

		click(btn_draft);

		Thread.sleep(2000);

		click(btn_release);

	}

	/* edit,update,draft,release,duplicate,draftNnew,CopyFrom */
	public void editNUpdateBOM() throws Exception {

		click(btn_draft);

		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the bill of materials successfully",
					"bill of materials drafted", "bill of materials drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the bill of materials successfully",
					"bill of materials drafted", "bill of materials not drafted", "fail");
		}

		String number = getText(txt_num);

		Thread.sleep(2000);

		click(btn_edit);

		if (isDisplayed(txt_editBOM)) {

			writeTestResults("Verify that the user can edit the bill of materials successfully",
					"bill of materials edited", "bill of materials edited", "pass");
		} else {

			writeTestResults("Verify that the user can edit the bill of materials successfully",
					"bill of materials edited", "bill of materials not edited", "fail");
		}

		sendKeys(txt_prodGrop, productGrpData);

		click(btn_Update);

		if (isDisplayed(txt_updateBOM)) {

			writeTestResults("Verify that the user can update the bill of materials successfully",
					"bill of materials updated", "bill of materials updated", "pass");
		} else {

			writeTestResults("Verify that the user can update the bill of materials successfully",
					"bill of materials updated", "bill of materials not updated", "fail");
		}

		click(btn_duplicate);

		if (isDisplayed(txt_duplicateBOM)) {

			writeTestResults("Verify that the user can duplicate the bill of materials successfully",
					"bill of materials duplicated", "bill of materials duplicated", "pass");
		} else {

			writeTestResults("Verify that the user can duplicate the bill of materials successfully",
					"bill of materials duplicated", "bill of materials not duplicated", "fail");
		}

		sendKeys(txt_prodGrop, productGrpData);

		click(btn_draftNNew);

		if (isDisplayed(txt_draftNNewBOM)) {

			writeTestResults("Verify that the user can draft the bill of materials and create new successfully",
					"bill of materials drafted and create a new", "bill of materials drafted and create a new", "pass");
		} else {

			writeTestResults("Verify that the user can draft the bill of materials and create new successfully",
					"bill of materials drafted and create a new", "bill of materials not drafted and create a new",
					"fail");
		}

		fillingBOMForm();

		click(btn_copyFrom);

		if (isDisplayed(txt_BOm)) {

			writeTestResults("Verify that the user can take a copy from the previously created BOM",
					"take a copy from the previously created BOM", "take a copy from the previously created BOM",
					"pass");
		} else {

			writeTestResults("Verify that the user can take a copy from the previously created BOM",
					"take a copy from the previously created BOM", "can't take a copy from the previously created BOM",
					"fail");
		}

		Thread.sleep(2000);

		sendKeys(txt_previousBOM, number);

		Thread.sleep(2000);

		pressEnter(txt_previousBOM);
		Thread.sleep(2000);

		doubleClick(doublePreviousBOM);

		Thread.sleep(2000);

		click(btn_draft);

		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the bill of materials successfully",
					"bill of materials drafted", "bill of materials drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the bill of materials successfully",
					"bill of materials drafted", "bill of materials not drafted", "fail");
		}

		Thread.sleep(3000);

		click(btn_release);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(txt_BOM)) {

			writeTestResults("Verify that the user can release the bill of materials successfully",
					"bill of materials released", "bill of materials released", "pass");
		} else {

			writeTestResults("Verify that the user can release the bill of materials successfully",
					"bill of materials released", "bill of materials not released", "fail");
		}

		click(btn_action);
		Thread.sleep(2000);
		click(btn_hold);

		if (isDisplayed(txt_changeStatus)) {

			writeTestResults("Verify that the user can hold the bill of materials successfully",
					"bill of materials hold", "bill of materials hold", "pass");
		} else {

			writeTestResults("Verify that the user can hold the bill of materials successfully",
					"bill of materials hold", "bill of materials not hold", "fail");
		}

		sendKeys(txt_reason, reasonData);
		Thread.sleep(2000);

		click(btn_ok);
		Thread.sleep(2000);

		click(btn_action);
		Thread.sleep(2000);

		click(btn_unHold);

		if (isDisplayed(txt_changeStatus)) {

			writeTestResults("Verify that the user can unhold the bill of materials successfully",
					"bill of materials unhold", "bill of materials unhold", "pass");
		} else {

			writeTestResults("Verify that the user can unhold the bill of materials successfully",
					"bill of materials unhold", "bill of materials not unhold", "fail");
		}

		sendKeys(txt_reason, reasonData1);

		click(btn_ok);
		Thread.sleep(2000);
		click(btn_action);
		Thread.sleep(2000);
		click(btn_newVersion);

		String valid = getText(txt_validMsg);
		if (valid.equals("successfully generate a new version .....")) {

			writeTestResults("Verify that the user can create a new version of bill of materials successfully",
					"created new version of bill of materials ", "created new version of bill of materials ", "pass");
		} else {

			writeTestResults("Verify that the user can create a new version of bill of materials successfully",
					"created new version of bill of materials ", "not created new version of bill of materials ",
					"fail");
		}

		Thread.sleep(2000);

		click(btn_action);
		Thread.sleep(2000);
		click(btn_reminder);

		Thread.sleep(3000);

		click(btn_updateReminder);

		Thread.sleep(2000);

		writeTestResults("Verify that the user can createReminder for the bill of materials successfully",
				"bill of materials create reminder viewed", "bill of materials create reminder viewed", "pass");

		click(btn_action);
		Thread.sleep(2000);
		click(btn_history);

		if (isDisplayed(txt_history)) {

			writeTestResults("Verify that the user can view history of the bill of materials successfully",
					"bill of materials history viewed", "bill of materials history viewed", "pass");
		} else {

			writeTestResults("Verify that the user can unhold the bill of materials successfully",
					"bill of materials history viewed", "bill of materials history not viewed", "fail");
		}

		Thread.sleep(2000);

		click(btn_action);
		Thread.sleep(2000);
		click(btn_reverse);
		Thread.sleep(2000);
		click(btn_yes);

		if (isDisplayed(txt_reverse)) {

			writeTestResults("Verify that the user can reverse the bill of materials successfully",
					"bill of materials reverse", "bill of materials reverse", "pass");
		} else {

			writeTestResults("Verify that the user can reverse the bill of materials successfully",
					"bill of materials reverse", "bill of materials not reverse", "fail");
		}

	}

	/* Con_TC_006 */

	// Verify whether user can fill summary data of bill of operations successully

	public void navigateToProduction1() throws Exception {

		click(btn_production);
		Thread.sleep(5000);
		if (isDisplayed(txt_production)) {

			writeTestResults("Verify that the user can navigate to Production page",
					"The Bill of operations by page should be loaded successfully", "Production page loaded", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to Production page",
					"The Bill of operations by page should be loaded successfully", "Production page not loaded",
					"fail");
		}
	}

	public void navigateToBillOfOperation1() throws Exception {

		click(btn_BOO);

		if (isDisplayed(txt_BOO)) {

			writeTestResults("Verify that the user can navigate to Bill of operation by page",
					"The New bill of operations page should be loaded successfully", "Bill Of Operation page loaded",
					"pass");
		} else {

			writeTestResults("Verify that the user can navigate to Bill of operation by page",
					"The New bill of operations page should be loaded successfully",
					"Bill Of Operation page not loaded", "fail");
		}
	}

	public void navigateTonewBOO1() throws Exception {
		click(btn_newBOO);

		if (isDisplayed(txt_newBOO)) {

			writeTestResults("Verify that the user can navigate to new Bill of operation by page",
					"The New bill of operations page should be loaded successfully",
					"new Bill Of Operation page loaded", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to new Bill of operation by page",
					"The New bill of operations page should be loaded successfully",
					"new Bill Of Operation page not loaded", "fail");
		}
	}

	public void fillBOOForm1() throws Exception {

		Random rand = new Random();
		int rand_int1 = rand.nextInt(100000000);

		BOOCodeData = BOOCodeData + Integer.toString(rand_int1);

		sendKeys(txt_BOOCode, BOOCodeData);

		// Description
		click(btn_descriptionBOO);

		sendKeys(txt_descrBOO, BOODescrData);

		click(btn_BOOApply);

		if (isDisplayed(txt_BOOLogo)) {

			writeTestResults("Verify that the user can navigate to Bill of operation by page",
					"The bill of operations page should be loaded successfully", "Bill Of Operation page loaded again",
					"pass");
		} else {

			writeTestResults("Verify that the user can navigate to Bill of operation by page",
					"The bill of operations page should be loaded successfully",
					"Bill Of Operation page not loaded again", "fail");
		}
	}

	/* Con_TC_007 */
	// Verify whether user can add initail operation as a supply under the bill of
	// operations details

	public void OperationActivitySupply1() throws Exception {

		click(btn_plusBOOSupply);

		selectText(txt_ElementCategory, ElementCategoryData);

		sendKeys(txt_ElementDescription, ElementDescData);

		// click(btn_applyOperation);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$('.dialogbox').nextAll().find('.pic16-apply').click()");
		if (isDisplayed(label_operationActivity)) {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"Supply operation should be added successfully", "operation/activity page loaded again", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"Supply operation should be added successfully", "operation/activity page not loaded again",
					"fail");
		}

	}
	/* Con_TC_008 */
	// Verify whether user can add next operation as a production under the bill of
	// operations details

	public void OperationActivityOperation1() throws Exception {

		click(btn_plusBOOOperation);

		selectIndex(txt_ElementCategoryOperation, 1);

		sendKeys(txt_GroupId, GroupIdData);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		// click(btn_applyOperation);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$('.dialogbox').nextAll().find('.pic16-apply').click()");

		if (isDisplayed(label_operationActivity)) {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page loaded again", "pass");
		} else {

			writeTestResults("Verify that the user can navigate to operation/activity by page",
					"operation should be added successfully", "operation/activity page not loaded again", "fail");
		}

	}

	/* Con_TC_009 */
	// Verify whether user can add final operation as a QC under the bill of
	// operations details

	public void QCUnderTheBillOfOperations1() throws Exception {

		click(btn_plusBOOQCBillOfOperation);
		Thread.sleep(2000);
		selectIndex(txt_ElementCategoryOperation, 1);
		Thread.sleep(2000);
		selectIndex(txt_activityType, 1);
		Thread.sleep(2000);
		click(txt_FinalInvestigation);
		Thread.sleep(2000);
		sendKeys(txt_GroupId, GroupIdDataQC);
		Thread.sleep(2000);
		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

//		click(btn_applyOperation);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$('.dialogbox').nextAll().find('.pic16-apply').click()");

	}

	public void completeNReleaseBOO() throws Exception {

		click(btn_draft);

		if (isDisplayed(label_operationActivity)) {

			writeTestResults("Verify that the user can draft the BOO", "BOO should be drafted successfully",
					"BOO drafted", "pass");
		} else {

			writeTestResults("BOO should be drafted successfully", "BOO should be drafted successfully",
					"BOO not drafted", "fail");
		}

		Thread.sleep(2000);

		click(btn_action);

		click(btn_Delete);

		click(btn_yes);

		if (isDisplayed(txt_delete)) {

			writeTestResults("Verify that the user can delete the bill of operations successfully",
					"bill of operations deleted", "bill of operations deleted", "pass");
		} else {

			writeTestResults("Verify that the user can delete the bill of operations successfully",
					"bill of operations deleted", "bill of operations not deleted", "fail");
		}

		click(btn_duplicate);

		if (isDisplayed(txt_duplicateBOM)) {

			writeTestResults("Verify that the user can duplicate the bill of operations successfully",
					"bill of operations duplicated", "bill of operations duplicated", "pass");
		} else {

			writeTestResults("Verify that the user can duplicate the bill of operations successfully",
					"bill of operations duplicated", "bill of operations not duplicated", "fail");
		}

		sendKeys(txt_BOOCode, BOOCodeData);

		click(btn_draft);

		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the bill of operations successfully",
					"bill of operations drafted", "bill of operations drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the bill of operations successfully",
					"bill of operations drafted", "bill of operations not drafted", "fail");
		}

		click(btn_edit);

		if (isDisplayed(txt_editBOM)) {

			writeTestResults("Verify that the user can edit the bill of operations successfully",
					"bill of operations edited", "bill of operations edited", "pass");
		} else {

			writeTestResults("Verify that the user can edit the bill of operations successfully",
					"bill of operations edited", "bill of operations not edited", "fail");
		}

		sendKeys(txt_BOOCode, BOOCodeData);

		click(btn_updateNNew);

		if (isDisplayed(txt_updateNNewBOM)) {

			writeTestResults("Verify that the user can update the bill of operations and create new successfully",
					"bill of operations updated and create a new", "bill of operations updated and create a new",
					"pass");
		} else {

			writeTestResults("Verify that the user can update the bill of operations and create new successfully",
					"bill of operations updated and create a new", "bill of operations not updated and create a new",
					"fail");
		}

		fillBOOForm1();
		OperationActivitySupply1();
		OperationActivityOperation1();
		QCUnderTheBillOfOperations1();

		click(btn_draft);

		Thread.sleep(2000);

		click(btn_release);

	}

	/* edit,update,draft,release,duplicate,draftNnew,CopyFrom BOO */
	public void editNUpdateBOO() throws Exception {

		click(btn_draft);

		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the bill of operations successfully",
					"bill of operations drafted", "bill of operations drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the bill of operations successfully",
					"bill of operations drafted", "bill of operations not drafted", "fail");
		}

		Thread.sleep(2000);

		click(btn_edit);

		if (isDisplayed(txt_editBOM)) {

			writeTestResults("Verify that the user can edit the bill of operations successfully",
					"bill of operations edited", "bill of operations edited", "pass");
		} else {

			writeTestResults("Verify that the user can edit the bill of operations successfully",
					"bill of operations edited", "bill of operations not edited", "fail");
		}

		sendKeys(txt_BOOCode, BOOCodeData);

		click(btn_Update);

		if (isDisplayed(txt_updateBOM)) {

			writeTestResults("Verify that the user can update the bill of operations successfully",
					"bill of operations updated", "bill of operations updated", "pass");
		} else {

			writeTestResults("Verify that the user can update the bill of operations successfully",
					"bill of operations updated", "bill of operations not updated", "fail");
		}

		click(btn_duplicate);

		if (isDisplayed(txt_duplicateBOO)) {

			writeTestResults("Verify that the user can duplicate the bill of operations successfully",
					"bill of operations duplicated", "bill of operations duplicated", "pass");
		} else {

			writeTestResults("Verify that the user can duplicate the bill of operations successfully",
					"bill of operations duplicated", "bill of operations not duplicated", "fail");
		}

		Thread.sleep(2000);

		Random rand = new Random();
		int rand_int1 = rand.nextInt(1000000);

		BOOCodeData1 = BOOCodeData1 + Integer.toString(rand_int1);

		sendKeys(txt_BOOCode, BOOCodeData1);

		click(btn_draftNNew);

		if (isDisplayed(txt_draftNNewBOM)) {

			writeTestResults("Verify that the user can draft the bill of operations and create new successfully",
					"bill of operations drafted and create a new", "production model drafted and create a new", "pass");
		} else {

			writeTestResults("Verify that the user can draft the bill of operations and create new successfully",
					"bill of operations drafted and create a new", "bill of operations not drafted and create a new",
					"fail");
		}

		fillBOOForm1();

		click(btn_copyFrom);

		if (isDisplayed(txt_Boo)) {

			writeTestResults("Verify that the user can take a copy from the previously created bill of operations",
					"take a copy from the previously created bill of operations",
					"take a copy from the previously created bill of operations", "pass");
		} else {

			writeTestResults("Verify that the user can take a copy from the previously created bill of operations",
					"take a copy from the previously created bill of operations",
					"can't take a copy from the previously created bill of operations", "fail");
		}

		Thread.sleep(2000);

		sendKeys(txt_previousBOO, BOOCodeData1);

		pressEnter(txt_previousBOO);
		Thread.sleep(2000);

		doubleClick(doublePreviousBOO);

		Thread.sleep(2000);

		sendKeys(txt_BOOCode, BOOCodeData);

		Thread.sleep(2000);

		click(btn_draft);

		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the bill of operations successfully",
					"bill of operations drafted", "bill of operations drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the bill of operations successfully",
					"bill of operations drafted", "bill of operations not drafted", "fail");
		}

		Thread.sleep(3000);

		click(btn_release);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(txt_BOOAction)) {

			writeTestResults("Verify that the user can release the bill of operations successfully",
					"bill of operations released", "bill of operations released", "pass");
		} else {

			writeTestResults("Verify that the user can release the bill of operations successfully",
					"bill of operations released", "bill of operations not released", "fail");
		}

		Thread.sleep(2000);

		click(btn_action);

		click(btn_history);

		if (isDisplayed(txt_history)) {

			writeTestResults("Verify that the user can view history of the bill of operations successfully",
					"bill of operations history viewed", "bill of operations history viewed", "pass");
		} else {

			writeTestResults("Verify that the user can unhold the bill of operations successfully",
					"bill of operations history viewed", "bill of operations history not viewed", "fail");
		}

		Thread.sleep(2000);

		click(btn_action);

		click(btn_reverse);

		click(btn_yes);

		if (isDisplayed(txt_reverse)) {

			writeTestResults("Verify that the user can reverse the bill of operations successfully",
					"bill of operations reverse", "bill of operations reverse", "pass");
		} else {

			writeTestResults("Verify that the user can reverse the bill of operations successfully",
					"bill of operations reverse", "bill of operations not reverse", "fail");
		}

		Thread.sleep(2000);

		click(btn_action);

		click(btn_activities);

		sendKeys(txt_subject, subjectData);

		click(btn_assignTo);

		sendKeys(txt_AssignTo, AssignToData);

		pressEnter(txt_AssignTo);

		Thread.sleep(2000);

		doubleClick(doubleAssignTo);

		click(btn_UpdateTask);

		if (isDisplayed(txt_reverse)) {

			writeTestResults("Verify that the user can view activities of the bill of operations successfully",
					"bill of operations activites displayed", "bill of operations activities displayed", "pass");
		} else {

			writeTestResults("Verify that the user can view activities of the bill of operations successfully",
					"bill of operations activites displayed", "bill of operations not activities displayed", "fail");
		}

	}

	/* Con_TC_015 */
	// Verify whether user can navigate to new production model page successfully

	public void navToProductionModel() throws Exception {

		click(btn_productionModel);

	}

	/* Con_TC_016 */
	// Verify whether user can fill summary data of production model successfully

	public void ProductionModelForm1() throws Exception {

		click(btn_newProductionModel);

		Random rand = new Random();
		int rand_int1 = rand.nextInt(100000);

		modelCodeData = modelCodeData + Integer.toString(rand_int1);

		sendKeys(txt_modelCode, modelCodeData);

		selectText(txt_productGroup, productGroupData);

		click(btn_yes);

		click(btn_product);

		// sendKeys(txt_product, productData);

		sendKeys(txt_product, productCodeData);

		pressEnter(txt_product);

		Thread.sleep(3000);

		doubleClick(doubleClickproduct);

		click(btn_yes);

		click(btn_ProductionUnit);

		sendKeys(txt_ProductionUnitLookup, ProductionUnitData);

		pressEnter(txt_ProductionUnitLookup);

		Thread.sleep(3000);

		doubleClick(doubleClickProductionUnit);

		click(btn_yes);

		if (isDisplayed(txt_newproductionModel)) {

			writeTestResults("Verify whether user can fill summary data of production model successfully",
					"The new production model page should be loaded successfully", "new production model page loaded",
					"pass");
		} else {

			writeTestResults("Verify whether user can fill summary data of production model successfully",
					"The new production model page should be loaded successfully",
					"new production model page not loaded", "fail");
		}
	}

	/* Con_TC_017 */
	// Verify whether user can fill other information of production model with
	// respect to MRP production type successfully

	public void ProductionModelFormOnlyMRPEnable1() throws Exception {

		click(btn_MRPEnabled);

		sendKeys(txt_batchQty, batchQtyData);

		selectIndex(txt_barcodeBook, 1);

		selectIndex(txt_CostingPriority, 1);

		sendKeys(txt_standardCost, standardCostData);

		click(btn_pricingProfile);

		sendKeys(txt_PricingProfile, PricingProfileData);

		pressEnter(txt_PricingProfile);

		doubleClick(doubleClickPricingProfile);

		if (isDisplayed(txt_newproductionModel)) {

			writeTestResults(
					"Verify whether user can fill other information of production model with respect to MRP prodution type successfully",
					"The new production model page filled information of production model with respect to MRP production type successfully",
					"MRP production type production model filled successfully", "pass");
		} else {

			writeTestResults(
					"Verify whether user can fill other information of production model with respect to MRP prodution type successfully",
					"The new production model page filled information of production model with respect to MRP production type successfully",
					"MRP production type production model not filled successfully", "fail");
		}

	}

	/* Con_TC_020 */
	// Verify whether user can search bill of opertaions details successfully
	public void BOODetails1() throws Exception {

		click(btn_BOONoSearch);

		sendKeys(txt_BOOOperation, BOOCodeData);

		pressEnter(txt_BOOOperation);

		doubleClick(doubleClickBOOOperation);

		click(btn_informationOK);

	}

	/* Con_TC_023 */
	// Verify whether user able to add raw materials to production model

	public void BOODetailsTabSupply1() throws Exception {

		click(btn_BOOTab);

		click(btn_Expand);

		click(btn_supply);

		selectIndex(txt_ElementCategoryOperation, 2);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		click(btn_inputProducts);

		click(btn_checkBox);

		click(btn_apply);
	}

	/* Con_TC_024 */
	// Verify whether user able to add production operations to production model
	public void BOODetailsTabOperation1() throws Exception {

		click(btn_operation);

		selectIndex(txt_ElementCategoryOperation, 1);

		sendKeys(txt_GroupId, GroupIdData);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		selectIndex(txt_processTime, 1);

		click(btn_inputProducts);

		click(btn_checkBox);

		click(btn_resources);

		click(btn_resourceSearch);

		sendKeys(txt_additionalResources, additionalResourcesData);

		pressEnter(txt_additionalResources);

		doubleClick(doubleCLickAdditinalResources);

		click(btn_apply);
	}

	/* Con_TC_025, Con_TC_026, Con_TC_027 */
	// Verify whether user able to add QC operations to production model
	public void BOODetailsTabQC1() throws Exception {

		click(btn_QC);

		selectIndex(txt_ElementCategoryOperation, 1);

		selectIndex(txt_activityType, 1);

		sendKeys(txt_GroupId, GroupIdData2);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		selectIndex(txt_processTime, 1);

		click(btn_apply);

		Thread.sleep(3000);

	}

	public void completenreleaseProductionModel() throws Exception {

		click(btn_draft);

		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the production model successfully",
					"production model drafted", "production model drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the bill of materials successfully",
					"production model drafted", "production model not drafted", "fail");
		}

		Thread.sleep(2000);

		click(btn_action);

		click(btn_Delete);

		click(btn_yes);

		if (isDisplayed(txt_delete)) {

			writeTestResults("Verify that the user can delete the production model successfully",
					"bill of materials deleted", "production model deleted", "pass");
		} else {

			writeTestResults("Verify that the user can release the production model successfully",
					"production model deleted", "production model not deleted", "fail");
		}

		click(btn_duplicate);

		if (isDisplayed(txt_duplicateBOM)) {

			writeTestResults("Verify that the user can duplicate the production model successfully",
					"production model duplicated", "production model duplicated", "pass");
		} else {

			writeTestResults("Verify that the user can duplicate the production model successfully",
					"production model duplicated", "production model not duplicated", "fail");
		}

		sendKeys(txt_productionModelCode, modelCodeData);

		click(btn_draft);

		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the production model successfully",
					"production model drafted", "production model drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the production model successfully",
					"production model drafted", "production model not drafted", "fail");
		}

		click(btn_edit);

		if (isDisplayed(txt_editBOM)) {

			writeTestResults("Verify that the user can edit the production model successfully",
					"production model edited", "production model edited", "pass");
		} else {

			writeTestResults("Verify that the user can edit the production model successfully",
					"production model edited", "production model not edited", "fail");
		}

		sendKeys(txt_productionModelCode, modelCodeData);

		BOODetailsTabSupply1();

		BOODetailsTabOperation1();

		BOODetailsTabQC1();

		click(btn_updateNNew);

		if (isDisplayed(txt_updateNNewBOM)) {

			writeTestResults("Verify that the user can update the production model and create new successfully",
					"production model updated and create a new", "production model updated and create a new", "pass");
		} else {

			writeTestResults("Verify that the user can update the production model and create new successfully",
					"production model updated and create a new", "production model not updated and create a new",
					"fail");
		}

		ProductionModelForm1();
		ProductionModelFormOnlyMRPEnable1();
		BOODetails1();
		BOODetailsTabSupply1();
		BOODetailsTabOperation1();
		BOODetailsTabQC1();

		click(btn_draft);

		Thread.sleep(2000);

		click(btn_release);

	}

	/* edit,update,draft,release,duplicate,draftNnew,CopyFrom production model */
	public void editNUpdateProductionModel1() throws Exception {

		click(btn_draft);

		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the production model successfully",
					"production model drafted", "production model drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the production model successfully",
					"production model drafted", "production model not drafted", "fail");
		}

		Thread.sleep(2000);

		click(btn_edit);

		if (isDisplayed(txt_editBOM)) {

			writeTestResults("Verify that the user can edit the production model successfully",
					"production model edited", "production model edited", "pass");
		} else {

			writeTestResults("Verify that the user can edit the production model successfully",
					"production model edited", "production model not edited", "fail");
		}

		sendKeys(txt_BOOCode, BOOCodeData);

		BOODetailsTabSupply1();

		BOODetailsTabOperation1();

		BOODetailsTabQC1();

		click(btn_Update);

		if (isDisplayed(txt_updateBOM)) {

			writeTestResults("Verify that the user can update the production model successfully",
					"production model updated", "production model updated", "pass");
		} else {

			writeTestResults("Verify that the user can update the production model successfully",
					"production model updated", "production model not updated", "fail");
		}

		click(btn_duplicate);

		if (isDisplayed(txt_duplicateBOO)) {

			writeTestResults("Verify that the user can duplicate the production model successfully",
					"production model duplicated", "production model duplicated", "pass");
		} else {

			writeTestResults("Verify that the user can duplicate the production model successfully",
					"production model duplicated", "production model not duplicated", "fail");
		}

		Thread.sleep(2000);

		Random rand = new Random();
		int rand_int1 = rand.nextInt(100000);

		modelCodeData1 = modelCodeData1 + Integer.toString(rand_int1);

		sendKeys(txt_productionModelCode, modelCodeData1);

		click(btn_draftNNew);

		if (isDisplayed(txt_draftNNewBOM)) {

			writeTestResults("Verify that the user can draft the production models and create new successfully",
					"production model drafted and create a new", "production model drafted and create a new", "pass");
		} else {

			writeTestResults("Verify that the user can draft the production model and create new successfully",
					"production model drafted and create a new", "production model not drafted and create a new",
					"fail");
		}
		Thread.sleep(2000);

		ProductionModelForm1();
		ProductionModelFormOnlyMRPEnable1();
		BOODetails1();
		BOODetailsTabSupply1();
		BOODetailsTabOperation1();
		BOODetailsTabQC1();

		click(btn_copyFrom);

		if (isDisplayed(txt_productionModelTab)) {

			writeTestResults("Verify that the user can take a copy from the previously created production model",
					"take a copy from the previously created production model",
					"take a copy from the previously created production model", "pass");
		} else {

			writeTestResults("Verify that the user can take a copy from the previously created BOM",
					"take a copy from the previously created production model",
					"can't take a copy from the previously created production model", "fail");
		}

		Thread.sleep(2000);

		sendKeys(txt_previousmodel, modelCode);

		pressEnter(txt_previousmodel);
		Thread.sleep(2000);

		doubleClick(doublePreviousmodel);

		click(tab_summary);

		Thread.sleep(2000);

		sendKeys(txt_productionModelCode, modelCodeData);

		selectIndex(txt_BillOfMaterial, 0);

		click(btn_yes);

		Thread.sleep(2000);

		click(btn_BOOTab);

		click("//tr[@class='ev']//span[@class='pic16 pic16-plus buttons0']");

		click("//div[@class='doc-detail']//tr[2]//td[7]//span[1]");

		selectIndex(txt_ElementCategoryOperation, 2);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		click(btn_inputProducts);

		click(btn_checkBox);

		click(btn_apply);

		click("//div[@class='doc-detail']//tr[2]//td[7]//span[1]");

		selectIndex(txt_ElementCategoryOperation, 1);

		sendKeys(txt_GroupId, GroupIdData);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		selectIndex(txt_processTime, 1);

		click(btn_inputProducts);

		click(btn_checkBox);

		click(btn_resources);

		click(btn_resourceSearch);

		sendKeys(txt_additionalResources, additionalResourcesData);

		pressEnter(txt_additionalResources);

		doubleClick(doubleCLickAdditinalResources);

		click(btn_apply);

		click("//div[@class='doc-detail']//tr[2]//td[7]//span[1]");

		selectIndex(txt_ElementCategoryOperation, 1);

		selectIndex(txt_activityType, 1);

		sendKeys(txt_GroupId, GroupIdData2);

		sendKeys(txt_ElementDescriptionOperation, ElementDescDataOperation);

		click(txt_FinalInvestigation);

		selectIndex(txt_processTime, 1);

		click(btn_apply);

		click(btn_draft);

		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the production model successfully",
					"production model drafted", "production model drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the production model successfully",
					"production model drafted", "production model not drafted", "fail");
		}

		Thread.sleep(3000);

		click(btn_release);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(txt_BOOAction)) {

			writeTestResults("Verify that the user can release the production model successfully",
					"production model released", "production model released", "pass");
		} else {

			writeTestResults("Verify that the user can release the production model successfully",
					"production model released", "production model not released", "fail");
		}

		Thread.sleep(2000);

		click(btn_action);

		click(btn_history);

		if (isDisplayed(txt_history)) {

			writeTestResults("Verify that the user can view history of the production model successfully",
					"production model history viewed", "production model history viewed", "pass");
		} else {

			writeTestResults("Verify that the user can unhold the production model successfully",
					"production model history viewed", "production model history not viewed", "fail");
		}

		Thread.sleep(2000);

	}

	/* Con_TC_028 */
	// Verify whether user can navigate to new production order page successfully
	public void navigateTonewProductionOrderPage1() throws Exception {

		click(btn_productionOrder);

		click(btn_newProductionOrder);

	}

	/* Con_TC_029 */
	// Verify whether user can fill production information data of production order
	// successfully

	public void informationProductionOrder1() throws Exception {

		click(btn_outputProduct);

		Thread.sleep(2000);

		sendKeys(txt_product, productCodeData);

//		sendKeys(txt_product, productData);

		pressEnter(txt_product);

		Thread.sleep(3000);

		doubleClick(doubleClickproduct);

		sendKeys(txt_plannedqty, plannedqtyData);

		selectIndex(txt_CostingPriority, 1);

		click(btn_MRPEnabled);

	}

	/* Con_TC_030, Con_TC_031, Con_TC_032, Con_TC_033 */

	// Verify whether user can fill summary data of production order successfully
	// Verify that user able to run the MRP successfully

	public void fillProductionOrder1() throws Exception {

		selectIndex(txt_productionOrderGroup, 1);

		sendKeys(txt_productionOrderDesc, productionOrderDescData);

		click(btn_productionOrderModel);

		sendKeys(txt_productionOrderModel, modelCodeData);

		pressEnter(txt_productionOrderModel);

		doubleClick(doubleClickOrderModel);

	}

	public void completenreleaseProductionOrder() throws Exception {

		click(btn_draft);

		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the production model successfully",
					"production model drafted", "production model drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the bill of materials successfully",
					"production model drafted", "production model not drafted", "fail");
		}

		Thread.sleep(2000);

		click(btn_action);

		click(btn_Delete);

		click(btn_yes);

		if (isDisplayed(txt_delete)) {

			writeTestResults("Verify that the user can delete the production model successfully",
					"production model deleted", "production model deleted", "pass");
		} else {

			writeTestResults("Verify that the user can release the production model successfully",
					"production model deleted", "production model not deleted", "fail");
		}

		click(btn_duplicate);

		if (isDisplayed(txt_duplicateBOM)) {

			writeTestResults("Verify that the user can duplicate the production model successfully",
					"production model duplicated", "production model duplicated", "pass");
		} else {

			writeTestResults("Verify that the user can duplicate the production model successfully",
					"production model duplicated", "production model not duplicated", "fail");
		}

		sendKeys(txt_plannedQty1, plannedqtyDataver);

		click(btn_draft);

		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the production model successfully",
					"production model drafted", "production model drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the production model successfully",
					"production model drafted", "production model not drafted", "fail");
		}

		click(btn_edit);

		if (isDisplayed(txt_editBOM)) {

			writeTestResults("Verify that the user can edit the production model successfully",
					"production model edited", "production model edited", "pass");
		} else {

			writeTestResults("Verify that the user can edit the production model successfully",
					"production model edited", "production model not edited", "fail");
		}

		sendKeys(txt_plannedQty, plannedqtyData);

		click(btn_updateNNew);

		if (isDisplayed(txt_updateNNewBOM)) {

			writeTestResults("Verify that the user can update the production model and create new successfully",
					"production model updated and create a new", "production model updated and create a new", "pass");
		} else {

			writeTestResults("Verify that the user can update the production model and create new successfully",
					"production model updated and create a new", "production model not updated and create a new",
					"fail");
		}

		informationProductionOrder1();
		fillProductionOrder1();

		click(btn_draft);

		Thread.sleep(2000);

		click(btn_release);

		productionOrder = getText("//*[@id=\"lblTemplateFormHeader\"]");

		click(navigateMenu);

		click(btn_Production);

		click(btn_ProductionControl);

		Thread.sleep(3000);

		sendKeys(txt_searchProductionOrder, productionOrder);

		click(btn_searchProductionOrder);

		Thread.sleep(3000);

		click(btn_checkBoxProductionOrderNo);

		click(btn_action);

		click(btn_RunMRP);

		if (isDisplayed(txt_prodControl)) {

			writeTestResults("Verify that the user can run MRP successfully", "run MRP", "run MRP", "pass");
		} else {

			writeTestResults("Verify that the user can run MRP successfully", "run MRP", "can't run MRP", "fail");
		}

		click(txt_MRPNo);

		click(btn_run);

		click(btn_releaseProductionOrder);

		Thread.sleep(3000);

		click(btn_close);

		click(btn_entutionLogo);

		click(btn_taskEvent);

		click(btn_InternalDispatchOrder);

		Thread.sleep(3000);

		click(btn_arrow);

		Thread.sleep(3000);

		switchWindow();

		sendKeys(txt_shippingAddress, shippingAddressData);

		click(btn_draft);

//		if (isDisplayed(txt_internalDispatchOrderdrafted)) {
//
//			writeTestResults(
//					"Verify that user able to draft the internal dispatch order for production order successfully",
//					"The internal dispatch order should be drafted successfully",
//					"The internal dispatch order drafted", "pass");
//		} else {
//
//			writeTestResults(
//					"Verify that user able to draft the internal dispatch order for production order successfully",
//					"The internal dispatch order should be drafted successfully",
//					"The internal dispatch order not drafted", "fail");
//		}
		Thread.sleep(3000);
		click(btn_editInternal);

		if (isDisplayed(txt_editBOM)) {

			writeTestResults("Verify that the user can edit the production order successfully",
					"internal dispatch order edited", "internal dispatch order edited", "pass");
		} else {

			writeTestResults("Verify that the user can edit the production order successfully",
					"internal dispatch order edited", "internal dispatch order not edited", "fail");
		}

		click(btn_Update);

		if (isDisplayed(txt_update)) {

			writeTestResults(
					"Verify that user able to update the internal dispatch order for production order successfully",
					"internal dispatch order updated", "internal dispatch order updated", "pass");
		} else {

			writeTestResults(
					"Verify that user able to update the internal dispatch order for production order successfully",
					"internal dispatch order updated", "internal dispatch order not updated", "fail");
		}

		Thread.sleep(3000);

		click(btn_release);

		Thread.sleep(4000);
		if (isDisplayed(txt_release)) {

			writeTestResults("Verify that the user can release the internal dispatch order successfully",
					"internal dispatch order released", "internal dispatch order released", "pass");
		} else {

			writeTestResults("Verify that the user can release the internal dispatch order successfully",
					"internal dispatch order released", "internal dispatch order not released", "fail");
		}

		click(btn_action);

		click(btn_dockFlow);

		if (isDisplayed(txt_dockFlow)) {

			writeTestResults("Verify that the user can click dockFlow successfully", "dockFlow clicked",
					"dockFlow clicked", "pass");
		} else {

			writeTestResults("Verify that the user can click dockFlow successfully", "dockFlow clicked",
					"dockFlow not clicked", "fail");
		}

		click(btn_action);

		click(btn_history);

		if (isDisplayed(txt_history)) {

			writeTestResults("Verify that the user can view history of the internal dispatch order successfully",
					"internal dispatch order history viewed", "internal dispatch order history viewed", "pass");
		} else {

			writeTestResults("Verify that the user can view history of the internal dispatch order successfully",
					"internal dispatch order history viewed", "internal dispatch order not history viewed", "fail");
		}

		click(btn_action);

		click(btn_reverse);

		click(btn_yes);

		if (isDisplayed(txt_reverse)) {

			writeTestResults("Verify that the user can reverse the internal dispatch order successfully",
					"internal dispatch order reverse", "internal dispatch order reverse", "pass");
		} else {

			writeTestResults("Verify that the user can reverse the internal dispatch order successfully",
					"internal dispatch order reverse", "internal dispatch order not reverse", "fail");
		}

		click(txt_date);

		click(btn_datenew);

		click(btn_reverseClick);

		click(btn_yes);

		click(btn_action);

		click(btn_journalEntry);

		if (isDisplayed(txt_reverse)) {

			writeTestResults("Verify that the user can view the journal in internal dispatch order successfully",
					"journal entry displayed", "journal entry displayed", "pass");
		} else {

			writeTestResults("Verify that the user can view the journal in internal dispatch order successfully",
					"journal entry displayed", "journal entry not displayed", "fail");
		}

	}

	/* edit,update,draft,release,duplicate,draftNnew,CopyFrom production order */
	public void editNUpdateProductionOrder1() throws Exception {

		click(btn_draft);

		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the production order successfully",
					"production order drafted", "production order drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the production order successfully",
					"production order drafted", "production order not drafted", "fail");
		}

		Thread.sleep(2000);

		click(btn_edit);

		if (isDisplayed(txt_editBOM)) {

			writeTestResults("Verify that the user can edit the production order successfully",
					"production order edited", "production order edited", "pass");
		} else {

			writeTestResults("Verify that the user can edit the production order successfully",
					"production order edited", "production order not edited", "fail");
		}

		selectIndex(txt_productionOrderGroup, 2);

		selectIndex(txt_costingPriority1, 1);

		selectIndex(txt_priority, 1);

		selectIndex(txt_site, 1);

		click(btn_Update);

		if (isDisplayed(txt_updateBOM)) {

			writeTestResults("Verify that the user can update the production order successfully",
					"production order updated", "production order updated", "pass");
		} else {

			writeTestResults("Verify that the user can update the production order successfully",
					"production order updated", "production order not updated", "fail");
		}

		click(btn_duplicate);

		if (isDisplayed(txt_duplicateBOO)) {

			writeTestResults("Verify that the user can duplicate the production order successfully",
					"production order duplicated", "production order duplicated", "pass");
		} else {

			writeTestResults("Verify that the user can duplicate the production order successfully",
					"production order duplicated", "production order not duplicated", "fail");
		}

		Thread.sleep(2000);

		sendKeys(txt_plannedqty, plannedqtyData);

		click(btn_draftNNew);

		if (isDisplayed(txt_draftNNewBOM)) {

			writeTestResults("Verify that the user can draft the production order and create new successfully",
					"production order drafted and create a new", "production order drafted and create a new", "pass");
		} else {

			writeTestResults("Verify that the user can draft the production order and create new successfully",
					"production order drafted and create a new", "production order not drafted and create a new",
					"fail");
		}
		Thread.sleep(2000);

		informationProductionOrder1();

		fillProductionOrder1();

		click(btn_copyFrom);

		if (isDisplayed(txt_productionOrderTab)) {

			writeTestResults("Verify that the user can take a copy from the previously created production order",
					"take a copy from the previously created production order",
					"take a copy from the previously created production order", "pass");
		} else {

			writeTestResults("Verify that the user can take a copy from the previously created production order",
					"take a copy from the previously created production order",
					"can't take a copy from the previously created production order", "fail");
		}

		Thread.sleep(2000);

		sendKeys(txt_previousproductionOrder, productionOrderData);

		pressEnter(txt_previousproductionOrder);
		Thread.sleep(2000);

		doubleClick(doublePreviousProductionOrder);

		sendKeys(txt_plannedqty, plannedqtyData);

		click(btn_draft);

		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the production order successfully",
					"production order drafted", "production order drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the production order successfully",
					"production order drafted", "production order not drafted", "fail");
		}

		Thread.sleep(3000);

		click(btn_release);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(txt_BOOAction)) {

			writeTestResults("Verify that the user can release the production order successfully",
					"production order released", "production order released", "pass");
		} else {

			writeTestResults("Verify that the user can release the production order successfully",
					"production order released", "production order not released", "fail");
		}

		click(btn_action);

		click(btn_hold);

		if (isDisplayed(txt_changeStatus)) {

			writeTestResults("Verify that the user can hold the bill of materials successfully",
					"bill of materials hold", "bill of materials hold", "pass");
		} else {

			writeTestResults("Verify that the user can hold the bill of materials successfully",
					"bill of materials hold", "bill of materials not hold", "fail");
		}

		sendKeys(txt_reason, reasonData);

		click(btn_okHold);

		Thread.sleep(2000);

		click(btn_action);

		click(btn_unHold);

		if (isDisplayed(txt_changeStatus)) {

			writeTestResults("Verify that the user can unhold the bill of materials successfully",
					"bill of materials unhold", "bill of materials unhold", "pass");
		} else {

			writeTestResults("Verify that the user can unhold the bill of materials successfully",
					"bill of materials unhold", "bill of materials not unhold", "fail");
		}

		sendKeys(txt_reason, reasonData1);

		click(btn_okHold);

		Thread.sleep(2000);

		click(btn_action);

		click(btn_jobTechnicalDetail);

		click(btn_apply);

		click("(//span[@class='headerclose'])[last()]");

		click(btn_action);

		click(btn_dockFlow);

		if (isDisplayed(txt_dockFlow)) {

			writeTestResults("Verify that the user can view dock flow successfully", "view dock flow",
					"dock flow displayed", "pass");
		} else {

			writeTestResults("Verify that the user can view dock flow successfully", "view dock flow",
					"dock flow not displayed", "fail");
		}

//		click(btn_action);
//
//		click(btn_generatePackinOrder);
//
//		if (isDisplayed(txt_generatePackingOrder)) {
//
//			writeTestResults("Verify that the user can generate a packing order successfully",
//					"generate a packing order", "packing order generated", "pass");
//		} else {
//
//			writeTestResults("Verify that the user can generate a packing order successfully",
//					"generate a packing order", "packing order not generated", "fail");
//		}
//
//		informationProductionOrder1();
//
//		fillProductionOrder1();

//		click(btn_action);
//
//		click(btn_costEstimation);
//
//		if (isDisplayed(txt_costEstimation)) {
//
//			writeTestResults("Verify that the user can generate a cost estimation successfully",
//					"generate a cost estimation", "cost estimation generated", "pass");
//		} else {
//
//			writeTestResults("Verify that the user can generate a cost estimation successfully",
//					"generate a cost estimation", "cost estimation not generated", "fail");
//		}

		click(btn_action);

		click(btn_reverse);

		click(btn_yes);

		if (isDisplayed(txt_reverse)) {

			writeTestResults("Verify that the user can reverse the bill of operations successfully",
					"bill of operations reverse", "bill of operations reverse", "pass");
		} else {

			writeTestResults("Verify that the user can reverse the bill of operations successfully",
					"bill of operations reverse", "bill of operations not reverse", "fail");
		}

		Thread.sleep(2000);

		click(btn_action);

		click(btn_history);

		if (isDisplayed(txt_history)) {

			writeTestResults("Verify that the user can view history of the production order successfully",
					"production order history viewed", "production order history viewed", "pass");
		} else {

			writeTestResults("Verify that the user can unhold the production order successfully",
					"production order history viewed", "production order history not viewed", "fail");
		}

		Thread.sleep(2000);

	}

	/* Con_TC_051 */
	// Verify that user can successfully navigate to the Bulk Production Form

	public void navigateToBulkProductionForm1() throws Exception {

		click(btn_bulkProductionOrder);

		if (isDisplayed(txt_BulkProduction)) {

			writeTestResults("Verify that user can successfully Open a Bulk Production order",
					" User should be able to Open a Bulk Production order Sucessfully",
					"Bulk Production order form page loaded", "pass");
		} else {

			writeTestResults("Verify that user can successfully Open a Bulk Production order",
					" User should be able to Open a Bulk Production order Sucessfully",
					"Bulk Production order form page not loaded", "fail");
		}

	}

	/* Con_TC_052 */
	// Verify whether user can enter summary information of Bulk Prodution Order

	public void SummaryBulkProductionOrder2() throws Exception {

		click(new_bulkProductionOrder);

		selectIndex(txt_productionOrderGroup, 2);

		sendKeys(txt_descrProductionOrderGroup, productionOrderGroupData);

		selectIndex(txt_referenceType, 1);

		click(btn_yes);

		click(btn_productionUnitBulk);

		sendKeys(txt_productionUnitBulk, productionUnitBulkData);

		pressEnter(txt_productionUnitBulk);

		doubleClick(doubleClickProductionUnitBulk);

		if (isDisplayed(txt_BulkProductionForm)) {

			writeTestResults("Verify whether user can enter summary information of Bulk Prodution Order",
					"Bulk production form should be displayed successfully", "Bulk production form page loaded",
					"pass");
		} else {

			writeTestResults("Verify whether user can enter summary information of Bulk Prodution Order",
					"Bulk production form should be displayed successfully", "Bulk production form page not loaded",
					"fail");
		}

	}

	/* Con_TC_053 */
	// Verify whether user can enter Product informations on Bulk Prodution Order
	public void ProductBulkProductionOrder2() throws Exception {

		click(btn_productTab);

		click(btn_productSearch);

		sendKeys(txt_productSearch, productSearchData);

		pressEnter(txt_productSearch);

		Thread.sleep(3000);

		doubleClick(doubleClickProductSearch);

		Thread.sleep(3000);

		click(btn_productModel);

		sendKeys(txt_productModel, modelData);

		pressEnter(txt_productModel);

		doubleClick(doubleClickProductModel);

		sendKeys(txt_plannedQtyBulk, plannedqtyBulkData);

		sendKeys(txt_standardCost, standardCostData);

		click(btn_checkOut);

		Thread.sleep(10000);

		click(btn_draftProductionOrder);

		if (isDisplayed(txt_draftProductionOrder)) {

			writeTestResults("Verify the production order drafted", "production order drafted successfully",
					"production order drafted successfully", "pass");
		} else {

			writeTestResults("Verify the production order drafted", "production order drafted successfully",
					"production order drafted successfully", "fail");
		}

		Thread.sleep(2000);

		click(btn_action);

		click(btn_Delete);

		click(btn_yes);

		if (isDisplayed(txt_delete)) {

			writeTestResults("Verify that the user can delete the bulk production order successfully",
					"bulk production order deleted", "bulk production order deleted", "pass");
		} else {

			writeTestResults("Verify that the user can release the bulk production order successfully",
					"bulk production order deleted", "bulk production order not deleted", "fail");
		}

		click(btn_duplicate);

		if (isDisplayed(txt_duplicateBOM)) {

			writeTestResults("Verify that the user can duplicate the bulk production order successfully",
					"bulk production order duplicated", "bulk production order duplicated", "pass");
		} else {

			writeTestResults("Verify that the user can duplicate the bulk production order successfully",
					"bulk production order duplicated", "bulk production order not duplicated", "fail");
		}

		click(btn_productTab);

		click(btn_productSearch);

		sendKeys(txt_productSearch, productSearchData);

		pressEnter(txt_productSearch);

		Thread.sleep(3000);

		doubleClick(doubleClickProductSearch);

		Thread.sleep(3000);

		click(btn_productModel);

		sendKeys(txt_productModel, modelData);

		pressEnter(txt_productModel);

		Thread.sleep(2000);

		doubleClick(doubleClickProductModel);

		sendKeys(txt_plannedQtyBulk, plannedqtyBulkData);

		sendKeys(txt_standardCost, standardCostData);

		click(btn_checkOut);

		Thread.sleep(10000);

		click(btn_draft);

		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the bulk production order successfully",
					"bulk production order drafted", "bulk production order drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the bulk production order successfully",
					"bulk production order drafted", "bulk production order not drafted", "fail");
		}

		click(btn_edit);

		if (isDisplayed(txt_editBOM)) {

			writeTestResults("Verify that the user can edit the bulk production order successfully",
					"bulk production order edited", "bulk production order edited", "pass");
		} else {

			writeTestResults("Verify that the user can edit the bulk production order successfully",
					"bulk production order edited", "bulk production order not edited", "fail");
		}

		click(btn_products);

		click(btn_checkOut);

		Thread.sleep(10000);

		click(btn_updateNNew);

		if (isDisplayed(txt_updateNNew)) {

			writeTestResults("Verify that the user can update the bulk production order and create new successfully",
					"bulk production order updated and create a new", "bulk production order updated and create a new",
					"pass");
		} else {

			writeTestResults("Verify that the user can update the bulk production order and create new successfully",
					"bulk production order updated and create a new",
					"bulk production order not updated and create a new", "fail");
		}

		selectIndex(txt_productionOrderGroup, 2);

		sendKeys(txt_descrProductionOrderGroup, productionOrderGroupData);

		selectIndex(txt_referenceType, 1);

		click(btn_yes);

		click(btn_productionUnitBulk);

		sendKeys(txt_productionUnitBulk, productionUnitBulkData);

		pressEnter(txt_productionUnitBulk);

		doubleClick(doubleClickProductionUnitBulk);

		click(btn_productTab);

		click(btn_productSearch);

		sendKeys(txt_productSearch, productSearchData);

		pressEnter(txt_productSearch);

		Thread.sleep(3000);

		doubleClick(doubleClickProductSearch);

		Thread.sleep(3000);

		click(btn_productModel);

		sendKeys(txt_productModel, modelData);

		pressEnter(txt_productModel);

		doubleClick(doubleClickProductModel);

		sendKeys(txt_plannedQtyBulk, plannedqtyBulkData);

		sendKeys(txt_standardCost, standardCostData);

		click(btn_checkOut);

		Thread.sleep(10000);

		click(btn_draftProductionOrder);

		Thread.sleep(2000);

		click(btn_release);

	}

	/*
	 * edit,update,draft,release,duplicate,draftNnew,CopyFrom Bulk production order
	 */

	public void editNUpdateBulkProductionOrder() throws Exception {

		click(new_bulkProductionOrder);

		selectIndex(txt_productionOrderGroup, 2);

		sendKeys(txt_descrProductionOrderGroup, productionOrderGroupData);

		selectIndex(txt_referenceType, 1);

		click(btn_yes);

		click(btn_productionUnitBulk);

		sendKeys(txt_productionUnitBulk, productionUnitBulkData);

		pressEnter(txt_productionUnitBulk);

		doubleClick(doubleClickProductionUnitBulk);

		click(btn_productTab);

		click(btn_productSearch);

		sendKeys(txt_productSearch, productSearchData);

		pressEnter(txt_productSearch);

		Thread.sleep(3000);

		doubleClick(doubleClickProductSearch);

		Thread.sleep(3000);

		click(btn_productModel);

		sendKeys(txt_productModel, modelData);

		pressEnter(txt_productModel);

		doubleClick(doubleClickProductModel);

		sendKeys(txt_plannedQtyBulk, plannedqtyBulkData);

		sendKeys(txt_standardCost, standardCostData);

		click(btn_checkOut);

		Thread.sleep(10000);

		click(btn_draftProductionOrder);

		click(btn_edit);

		if (isDisplayed(txt_editBOM)) {

			writeTestResults("Verify that the user can edit the bulk production order successfully",
					"bulk production order edited", "bulk production order edited", "pass");
		} else {

			writeTestResults("Verify that the user can edit the bulk production order successfully",
					"bulk production order edited", " bulk production order not edited", "fail");
		}

		click(btn_products);

		click(btn_checkOut);

		Thread.sleep(10000);

		click(btn_Update);

		if (isDisplayed(txt_updateBOM)) {

			writeTestResults("Verify that the user can update the bulk production order successfully",
					"bulk production order updated", "bulk production order updated", "pass");
		} else {

			writeTestResults("Verify that the user can update the bulk production order successfully",
					"bulk production order updated", "bulk production order not updated", "fail");
		}

		click(btn_duplicate);

		if (isDisplayed(txt_duplicateBOO)) {

			writeTestResults("Verify that the user can duplicate the bulk production order successfully",
					"bulk production order duplicated", "bulk production order duplicated", "pass");
		} else {

			writeTestResults("Verify that the user can duplicate the bulk production order successfully",
					"bulk production order duplicated", "bulk production order not duplicated", "fail");
		}

		click(btn_products);

		click(btn_checkOut);

		Thread.sleep(10000);

		click(btn_draftNNew);

		Thread.sleep(10000);

		if (isDisplayed(txt_draftNNewBOM)) {

			writeTestResults("Verify that the user can draft the bulk production order and create new successfully",
					"bulk production order drafted and create a new", "bulk production order drafted and create a new",
					"pass");
		} else {

			writeTestResults("Verify that the user can draft the bulk production order and create new successfully",
					"bulk production order drafted and create a new",
					"bulk production order not drafted and create a new", "fail");
		}
		Thread.sleep(2000);

		click(btn_copyFrom);

		Thread.sleep(3000);

		if (isDisplayed(txt_bulkproductionorderTab)) {

			writeTestResults("Verify that the user can take a copy from the previously created bulk production order",
					"take a copy from the previously created bulk production order",
					"take a copy from the previously created bulk production order", "pass");
		} else {

			writeTestResults("Verify that the user can take a copy from the previously created bulk production order",
					"take a copy from the previously created bulk production order",
					"can't take a copy from the previously created bulk production order", "fail");
		}

		sendKeys(txt_previousBulkOrder, previousbulkOrderData);

		pressEnter(txt_previousBulkOrder);

		Thread.sleep(2000);

		doubleClick(doublePreviousbulkOrder);

		click(btn_products);

		click(btn_checkOut);

		Thread.sleep(10000);

		click(btn_draft);

		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the bulk production order successfully",
					"bulk production order drafted", "bulk production order drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the bulk production order successfully",
					"bulk production order drafted", "bulk production order not drafted", "fail");
		}

		Thread.sleep(3000);

		click(btn_release);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(txt_BOOAction)) {

			writeTestResults("Verify that the user can release the bulk production order successfully",
					"bulk production order released", "bulk production order released", "pass");
		} else {

			writeTestResults("Verify that the user can release the bulk production order successfully",
					"bulk production order released", "bulk production order not released", "fail");
		}

		Thread.sleep(2000);

		click(btn_action);

		click(btn_InternalOrder);

		Thread.sleep(3000);

		click(btn_selectAll);

		Thread.sleep(2000);
		click(btn_applyInternal);

		if (isDisplayed(txt_InternalOrder)) {

			writeTestResults("Verify that the user can release internal order successfully", "internal order viewed",
					"internal order viewed", "pass");
		} else {

			writeTestResults("Verify that the user can release internal order successfully", "internal order viewed",
					"internal order not viewed", "fail");
		}

		switchWindow();

		sendKeys(txt_title1, titleData);

		click(btn_requester);

		sendKeys(txt_requester, requesterData);

		pressEnter(txt_requester);

		Thread.sleep(3000);

		doubleClick(doubleClickRequester);

		click(btn_draft);

		Thread.sleep(3000);

		click(btn_release);

		Thread.sleep(2000);

		click(btn_goTopage);

		switchWindow();

		sendKeys(txt_shipping, shippingData);

		click(btn_draft);

		Thread.sleep(4000);

		click(btn_Capture);

		Thread.sleep(4000);

		click(btn_productListClick);

		sendKeys(txt_batchNoTXT, batchData);

		pressEnter(txt_batchNoTXT);

		click(btn_refresh);

		click(btn_back);

		click(btn_release);

		Thread.sleep(3000);

		click(btn_action);

		click(btn_history);

		Thread.sleep(2000);
		if (isDisplayed(txt_history)) {

			writeTestResults("Verify that the user can view history of the bulk production order successfully",
					"bulk production order history viewed", "bulk production order history viewed", "pass");
		} else {

			writeTestResults("Verify that the user can unhold the bulk production order successfully",
					"bulk production order history viewed", "bulk production order history not viewed", "fail");
		}

		Thread.sleep(2000);

		click(btn_action);

		click(btn_dockFlow);

		Thread.sleep(2000);

		if (isDisplayed(txt_dockFlow)) {

			writeTestResults("Verify that the user can view dock flow successfully", "view dock flow",
					"dock flow displayed", "pass");
		} else {

			writeTestResults("Verify that the user can view dock flow successfully", "view dock flow",
					"dock flow not displayed", "fail");
		}

		Thread.sleep(2000);

		click(btn_action);

		click(btn_reverse);

		click(txt_date);

		click(btn_datenew1);

		click(btn_reverseClick);

		click(btn_yes);

		Thread.sleep(2000);
		if (isDisplayed(txt_reverse)) {

			writeTestResults("Verify that the user can reverse the bulk production order successfully",
					"bulk production order reverse", "bulk production order reverse", "pass");
		} else {

			writeTestResults("Verify that the user can reverse the bulk production order successfully",
					"bulk production order reverse", "bulk production order not reverse", "fail");
		}

		Thread.sleep(5000);

		click(btn_action);

		click(btn_journalEntry);

		click(link_gernalEntryDetails);

		Thread.sleep(2000);

		if (isDisplayed(txt_journalEntryReleased)) {

			writeTestResults(" Verify that user able to view journal entry successfully",
					"journal entry should released successfully", "journal entry released successfully", "pass");
		} else {

			writeTestResults("Verify that user able to view journal entry successfully",
					"journal entry should released successfully", "journal entry not released successfully", "fail");
		}

	}

	public void reportProduction() throws Exception {

		click(btn_report);

		click(btn_viewReport);

		if (isDisplayed(txt_viewReport)) {

			writeTestResults(" Verify that user able to view report successfully", "report viewed successfully",
					"report viewed successfully", "pass");
		} else {

			writeTestResults("Verify that user able to view report successfully", "report viewed successfully",
					"report not viewed successfully", "fail");
		}

	}

	public void actionOverheadInformation() throws Exception {

		click(txt_overheadInfo);

		click(btn_new);

		if (isDisplayed(txt_new)) {

			writeTestResults("Verify that user can create a new overhead information successfully",
					"new overhead information created successfully", "new overhead information created successfully",
					"pass");
		} else {

			writeTestResults("Verify that user can create a new overhead information successfully",
					"new overhead information created successfully",
					"new overhead information not created successfully", "fail");
		}

		Random rand = new Random();
		int rand_int2 = rand.nextInt(100000);

		overheadCodeData = overheadCodeData + Integer.toString(rand_int2);

		sendKeys(txt_overheadCode, overheadCodeData);

		sendKeys(txt_descOverhead, overheadDescData);

		selectIndex(txt_overheadGroup, 1);

		selectIndex(txt_rateType, 1);

		click(btn_draftOverhead);

		click(txt_newOverheadCode.replace("code", overheadCodeData));

		click(btn_active);

		click(btn_yes1);

		Thread.sleep(2000);

		if (isDisplayed(txt_active)) {

			writeTestResults(" Verify that user can activate the overhead information successfully",
					"overhead information activated successfully", "overhead information activated successfully",
					"pass");
		} else {

			writeTestResults("Verify that user can activate the overhead information successfully",
					"overhead information activated successfully", "overhead information not activated successfully",
					"fail");
		}

		click(btn_edit);

		if (isDisplayed(txt_edit)) {

			writeTestResults("Verify that user can edit the overhead information successfully",
					"overhead information edit successfully", "overhead information edited successfully", "pass");
		} else {

			writeTestResults("Verify that user can edit the overhead information successfully",
					"overhead information edit successfully", "overhead information not edited successfully", "fail");
		}

		Random rand1 = new Random();
		int rand1_int2 = rand1.nextInt(100000);

		overheadCodeData = overheadCodeData + Integer.toString(rand1_int2);

		sendKeys(txt_overheadCode, overheadCodeData);

		selectIndex(txt_overheadGroup, 3);

		click(btn_update);

		if (isDisplayed(txt_updateOrganization)) {

			writeTestResults("Verify that user can update the overhead information successfully",
					"overhead information update successfully", "overhead information updated successfully", "pass");
		} else {

			writeTestResults("Verify that user can update the overhead information successfully",
					"overhead information update successfully", "overhead information not updated successfully",
					"fail");
		}

		if (isDisplayed(txt_inactive)) {

			writeTestResults("Verify that user can inactivate the overhead information successfully",
					"overhead information inactivated successfully", "overhead information inactivated successfully",
					"pass");
		} else {

			writeTestResults("Verify that user can inactivate the overhead information successfully",
					"overhead information inactivated successfully",
					"overhead information not inactivated successfully", "fail");
		}

		click(btn_inactive);

		Thread.sleep(2000);

		click(btn_yes2);
		Thread.sleep(2000);

	}

	public void deleteOverheadInformation() throws Exception {

		click(txt_overheadInfo);

		click(btn_new);

		if (isDisplayed(txt_new)) {

			writeTestResults("Verify that user can create a new overhead information successfully",
					"new overhead information created successfully", "new overhead information created successfully",
					"pass");
		} else {

			writeTestResults("Verify that user can create a new overhead information successfully",
					"new overhead information created successfully",
					"new overhead information not created successfully", "fail");
		}

		Random rand = new Random();
		int rand_int2 = rand.nextInt(100000);

		overheadCodeData = overheadCodeData + Integer.toString(rand_int2);

		sendKeys(txt_overheadCode, overheadCodeData);

		sendKeys(txt_descOverhead, overheadDescData);

		selectIndex(txt_overheadGroup, 1);

		selectIndex(txt_rateType, 1);

		click(btn_draftOverhead);

		click(txt_newOverheadCode.replace("code", overheadCodeData));

		Thread.sleep(2000);

		if (isDisplayed(txt_deleteAc)) {

			writeTestResults("Verify that user can delete a new overhead information successfully",
					"new overhead information deleted successfully", "new overhead information deleted successfully",
					"pass");
		} else {

			writeTestResults("Verify that user can delete a new overhead information successfully",
					"new overhead information deleted successfully",
					"new overhead information not deleted successfully", "fail");
		}

		click(btn_deleteAc);

		Thread.sleep(2000);

		click(btn_yes4);
		Thread.sleep(2000);

	}

	public void editPricingProfile() throws Exception {
		click(btn_PricingProfile);

		click(btn_newPricingProfile);

		// create instance of Random class
		Random rand = new Random();

		// Generate random integers in range 0 to 999
		int rand_int1 = rand.nextInt(10000);

		pricingProfileCodeData = pricingProfileCodeData + Integer.toString(rand_int1);

		sendKeys(txt_pricingProfileCode, pricingProfileCodeData);

		sendKeys(txt_descPricingProfile, descPricingProfileData);

		selectIndex(txt_pricingGroup, 2);

		click(btn_PricingProfile);

		click(btn_newPricingProfile);

		click(btn_estimate);

		selectIndex(txt_material, 1);
		selectIndex(txt_labour, 1);
		selectIndex(txt_Overhead, 1);
		selectIndex(txt_service, 1);
		selectIndex(txt_expense, 1);

		click(btn_PricingProfile);

		click(btn_newPricingProfile);

		click(btn_actualCalculation);

		selectIndex(txt_materialAC, 2);
		selectIndex(txt_labourAC, 2);
		selectIndex(txt_OverheadAC, 2);
		selectIndex(txt_serviceAC, 2);
		selectIndex(txt_expenseAC, 2);

		Thread.sleep(2000);

		click(btn_draftLocation);

		keyDownClick(txt_Pricing, pricingProfileCodeData);

		Thread.sleep(2000);

		if (isDisplayed(txt_search)) {

			writeTestResults("Verify the Pricing Profile search Successfully", "Pricing Profile searched successfully",
					"Pricing Profile Searched successfully", "pass");
		} else {

			writeTestResults("Verify the Pricing Profile search Successfully", "Pricing Profile searched successfully",
					" Pricing Profile not searched successfullye", "fail");
		}

		click(btn_reset);

		Thread.sleep(2000);

		if (isDisplayed(txt_reset)) {

			writeTestResults("Verify the Pricing Profile reset Successfully", "Pricing Profile reset successfully",
					"Pricing Profile reset successfully", "pass");
		} else {

			writeTestResults("Verify the Pricing Profile reset Successfully", "Pricing Profile reset successfully",
					" Pricing Profile not reset successfully", "fail");
		}

		keyDownClick(txt_Pricing, pricingProfileCodeData);

		click(btn_active);

		click(btn_yes);

		if (isDisplayed(txt_activateStatus)) {

			writeTestResults("Verify the Pricing Profile Status", " Pricing Profile Status should update as Activated ",
					"Pricing Profile Status viewed as active", "pass");
		} else {

			writeTestResults("Verify the Pricing Profile Status", " Pricing Profile Status should update as Activated",
					"Pricing Profile Status not viewed as active", "fail");
		}

		if (isDisplayed(txt_inactive)) {

			writeTestResults("Verify that user can inactivate the Pricing Profile successfully",
					"Pricing Profile inactivated successfully", "Pricing Profile inactivated successfully", "pass");
		} else {

			writeTestResults("Verify that user can inactivate the Pricing Profile successfully",
					"Pricing Profile inactivated successfully", "Pricing Profile not inactivated successfully", "fail");
		}

		click(btn_inactive);

		Thread.sleep(2000);

		click(btn_yes2);
		Thread.sleep(2000);
	}

	public void ActionproductionLabelPrint() throws Exception {

		click(btn_draft);

		click(btn_release);

		productionOrder = getText(txt_productionOrderNo);

		click(navigateMenu);

		click(btn_Production);

		click(btn_ProductionControl);

		Thread.sleep(3000);

		sendKeys(txt_searchProductionOrder, productionOrder);

		click(btn_searchProductionOrder);

		Thread.sleep(3000);

		click(btn_checkBoxProductionOrderNo);

		click(btn_action);

		click(btn_RunMRP);

		click(txt_MRPNo);

		click(btn_run);

		click(btn_releaseProductionOrder);

		Thread.sleep(3000);

		click(btn_close);

		click(btn_entutionLogo);

		click(btn_taskEvent);

		click(btn_InternalDispatchOrder);

		Thread.sleep(3000);

		click(btn_arrow);

		Thread.sleep(3000);

		switchWindow();

		sendKeys(txt_shippingAddress, shippingAddressData);

		click(btn_draft);

		Thread.sleep(2000);

		click(btn_release);

		Thread.sleep(3000);

		click(navigateMenu);

		click(btn_Production);

		Thread.sleep(2000);

		click(btn_productionOrder);

		Thread.sleep(2000);

		sendKeys(txt_prodOrderNo, productionOrder);

		pressEnter(txt_prodOrderNo);

		Thread.sleep(3000);

		click(doubleClickprodNo.replace("number", productionOrder));

		click(btn_action);

		click(btn_shopFloorUpdate);

		Thread.sleep(3000);

		click(btn_start);

		Thread.sleep(2000);

		click(btn_startProcess);

		Thread.sleep(2000);

		click(btn_actual);

		Thread.sleep(4000);
		sendKeys(txt_producedQty, producedData);

		Thread.sleep(2000);

		click(btn_update);

		Thread.sleep(2000);

		click(btn_inputProduct);

		sendKeys(txt_actualQty, actualQtyData);

		click(btn_update);

		Thread.sleep(2000);

		click(btn_closeShop);

		Thread.sleep(2000);

		click(btn_completeTick);

		click(txt_complete);

		click("//span[@class='headerclose']");

		Thread.sleep(3000);

		click(navigateMenu);

		click(btn_Production);

		click(btn_labelPrint);

		click(btn_view);

		selectIndex(txt_batchNo, 1);

		selectIndex(txt_machineNo, 1);

		selectIndex(txt_color, 2);

		click(btn_draftNPrint);
		switchWindow();
		switchWindow();
		click("//*[@id=\"inspect\"]/div[6]/div[1]/span[2]");

	}

	public void QCproductionLabelPrint() throws Exception {

		Thread.sleep(2000);

		click(btn_labelPrint);
		Thread.sleep(2000);
		click(btn_labelPrintHistory);
		Thread.sleep(2000);
		click(btn_productOrder);
		Thread.sleep(2000);
		click(btn_QcComplete);

		Thread.sleep(3000);

		click(navigateMenu);
		Thread.sleep(2000);
		click(btn_Production);
		Thread.sleep(2000);
		click(btn_productionOrder);
		Thread.sleep(5000);
		sendKeys(txt_productonOrder, productionOrder);
		Thread.sleep(2000);
		click(doubleClickprodNo.replace("number", productionOrder));
		Thread.sleep(2000);
		click(btn_action);
		Thread.sleep(2000);
		click(btn_productionCosting);
		Thread.sleep(2000);
		click(btn_costingRefresh);
		Thread.sleep(2000);
		click(btn_inputProductsCostingTab);
		Thread.sleep(2000);
		click(btn_updateCosting);

		Thread.sleep(2000);

		click(btn_closeCosting);
		Thread.sleep(2000);
		click(btn_tick);
		Thread.sleep(2000);
		click(btn_update);

		Thread.sleep(3000);

		click(btn_internalReceiptControl);
		Thread.sleep(2000);
		click(btn_smallcheckBox);

		Random rand = new Random();

		// Generate random integers in range 0 to 999
		int rand_int1 = rand.nextInt(10000);

		lotNo = lotNo + Integer.toString(rand_int1);

		sendKeys(txt_lotNo, lotNo);

		click(btn_generateReceipt);

		if (isDisplayed(txt_internalReceiptLabel)) {

			writeTestResults("Verify that user can generate the internal receipt successfully",
					"internal receipt generated successfully", "internal receipt generated successfully", "pass");
		} else {

			writeTestResults("Verify that user can generate the internal receipt successfully",
					"internal receipt generated successfully", "internal receipt not generated successfully", "fail");
		}

		click("//span[@class='headerclose']");

		click(btn_action);

		click(btn_dockFlow);

		if (isDisplayed(txt_dockFlow)) {

			writeTestResults("Verify that the user can click dockFlow successfully", "dockFlow clicked",
					"dockFlow clicked", "pass");
		} else {

			writeTestResults("Verify that the user can click dockFlow successfully", "dockFlow clicked",
					"dockFlow not clicked", "fail");
		}

		click(btn_history);

		if (isDisplayed(txt_history)) {

			writeTestResults("Verify that the user can view history of the bill of materials successfully",
					"bill of materials history viewed", "bill of materials history viewed", "pass");
		} else {

			writeTestResults("Verify that the user can unhold the bill of materials successfully",
					"bill of materials history viewed", "bill of materials history not viewed", "fail");
		}

	}

	public void productionParameter() throws Exception {

		click(btn_productionParameter);

		click(btn_inactivePP);
		if (!isSelected(btn_inactivePP)) {
			click(btn_inactivePP);
		}

		if (isSelected(btn_inactivePP)) {

			writeTestResults("Verify that the user can inactive production parameters successfully",
					"production parameters inactivated", "production parameters inactivated", "pass");
		} else {

			writeTestResults("Verify that the user can inactive production parameters successfully",
					"production parameters inactivated", "production parameters not inactivated", "fail");
		}

		Thread.sleep(2000);
		click(btn_UPDATE);

		if (isDisplayed(txt_UPDATE)) {

			writeTestResults("Verify that the user can update production parameters successfully",
					"production parameters updated", "production parameters updated", "pass");
		} else {

			writeTestResults("Verify that the user can update production parameters successfully",
					"production parameters updated", "production parameters not updated", "fail");
		}

	}

	public void deleteproductionCostEstimation() throws Exception {

		click(btn_productcostEstimation);

		click(btn_newCostEstimation);

		sendKeys(txt_costEstDescription, costEstDescription);

		Thread.sleep(3000);

		click(btn_costProduct);

		sendKeys(txt_costProduct, costProductData);

		pressEnter(txt_costProduct);
		Thread.sleep(3000);
		doubleClick(costProductDoubleClick);
		Thread.sleep(3000);
		sendKeys(txt_costEstQuantity, costEstQuantityData);

		Thread.sleep(3000);

		click(btn_pricingProfileCost);
		Thread.sleep(2000);
		sendKeys(txt_pricingProfileCost, pricingProfileCostData);
		Thread.sleep(3000);
		pressEnter(txt_pricingProfileCost);
		Thread.sleep(2000);

		doubleClick(pricingProfileCostDoubleClick);

		Thread.sleep(6000);

		click(btn_checkout);

		Thread.sleep(3000);

		click(btn_draft);
		Thread.sleep(3000);
		click(btn_action);
		Thread.sleep(3000);
		click(btn_Delete);
		Thread.sleep(3000);
		click(btn_yes);

		if (isDisplayed(txt_delete)) {

			writeTestResults("Verify that the user can delete the bill of materials successfully",
					"bill of materials deleted", "bill of materials deleted", "pass");
		} else {

			writeTestResults("Verify that the user can delete the bill of materials successfully",
					"bill of materials deleted", "bill of materials not deleted", "fail");
		}

		click(btn_duplicate);

		if (isDisplayed(txt_duplicateBOM)) {

			writeTestResults("Verify that the user can duplicate the bill of materials successfully",
					"bill of materials duplicated", "bill of materials duplicated", "pass");
		} else {

			writeTestResults("Verify that the user can duplicate the bill of materials successfully",
					"bill of materials duplicated", "bill of materials not duplicated", "fail");
		}

		sendKeys(txt_prodGrop, productGrpData);

		click(btn_checkout);

		Thread.sleep(3000);

		click(btn_draft);

		if (isDisplayed(txt_draftBOM)) {

			writeTestResults("Verify that the user can draft the bill of materials successfully",
					"bill of materials drafted", "bill of materials drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the bill of materials successfully",
					"bill of materials drafted", "bill of materials not drafted", "fail");
		}

		click(btn_edit);

		if (isDisplayed(txt_editBOM)) {

			writeTestResults("Verify that the user can edit the bill of materials successfully",
					"bill of materials edited", "bill of materials edited", "pass");
		} else {

			writeTestResults("Verify that the user can edit the bill of materials successfully",
					"bill of materials edited", "bill of materials not edited", "fail");
		}

		sendKeys(txt_prodGrop, productGrpData);

		click(btn_checkout);

		Thread.sleep(3000);

		click(btn_updateNNew);

		if (isDisplayed(txt_updateNNewBOM)) {

			writeTestResults("Verify that the user can update the bill of materials and create new successfully",
					"bill of materials updated and create a new", "bill of materials updated and create a new", "pass");
		} else {

			writeTestResults("Verify that the user can update the bill of materials and create new successfully",
					"bill of materials updated and create a new", "bill of materials not updated and create a new",
					"fail");
		}

		// fillingBOMForm();
		sendKeys(txt_costEstDescription, costEstDescription);

		Thread.sleep(3000);

		click(btn_costProduct);

		sendKeys(txt_costProduct, costProductData);

		pressEnter(txt_costProduct);
		Thread.sleep(3000);
		doubleClick(costProductDoubleClick);
		Thread.sleep(3000);
		sendKeys(txt_costEstQuantity, costEstQuantityData);

		Thread.sleep(3000);

		click(btn_pricingProfileCost);
		Thread.sleep(2000);
		sendKeys(txt_pricingProfileCost, pricingProfileCostData);
		Thread.sleep(3000);
		pressEnter(txt_pricingProfileCost);
		Thread.sleep(2000);

		doubleClick(pricingProfileCostDoubleClick);

		Thread.sleep(6000);

		click(btn_checkout);

		Thread.sleep(3000);

		click(btn_draft);

		Thread.sleep(2000);

		click(btn_release);

	}

	/* Remade */
	/* Login */
	public void autmationTenantLogin() throws Exception {
		openPage(siteURL);
		Thread.sleep(2000);
		explicitWait(txt_username, 40);
		sendKeys(txt_username, automationTenantUsername);
		Thread.sleep(1000);
		sendKeys(txt_password, automationTenantPassword);
		Thread.sleep(2000);
		click(btn_login);
		explicitWait(btn_navigationPane, 40);
		if (isDisplayedQuickCheck(div_loginVerification)) {
			openPage(siteURL);
			Thread.sleep(2000);
			explicitWait(txt_username, 40);
			sendKeys(txt_username, automationTenantUsername);
			Thread.sleep(1000);
			sendKeys(txt_password, automationTenantPassword);
			Thread.sleep(2000);
			click(btn_login);
			explicitWait(btn_navigationPane, 40);
		}
	}

	/* Smoke_Production_0001_1 */
	public void billOfMaterial_Smoke_Production_0001_1() throws Exception {
		autmationTenantLogin();
		explicitWait(btn_navigationPane, 20);
		Thread.sleep(2000);
		explicitWait(btn_navigationPane, 20);
		click(btn_navigationPane);
		explicitWait(btn_inventoryAndWarehouse, 10);
		click(btn_inventoryAndWarehouse);
		explicitWait(btn_billOfMaterials, 10);
		click(btn_billOfMaterials);
		explicitWait(header_BillOfMaterailByPage, 40);
		if (isDisplayed(header_BillOfMaterailByPage)) {
			writeTestResults("Verify user can navigate to the Bill Of Material by-page",
					"User should be able to navigate to Bill Of Material by-page",
					"User successfully navigate to Bill Of Material by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to the Bill Of Material by-page",
					"User should be able to navigate to Bill Of Material by-page",
					"User doesn't navigate to Bill Of Material by-page", "fail");

		}
		click(btn_newBillOfMaterial);
		explicitWait(header_newBillOfMaterial, 50);
		if (isDisplayed(header_newBillOfMaterial)) {
			writeTestResults("Verify user can navigate to the new Bill Of Material",
					"User should be able to navigate to new Bill Of Material",
					"User successfully navigate to new Bill Of Material", "pass");
		} else {
			writeTestResults("Verify user can navigate to the new Bill Of Material",
					"User should be able to navigate to new Bill Of Material",
					"User doesn't navigate to new Bill Of Material", "fail");

		}

		sendKeys(txt_desc, description);
		String enteredDescription = getAttribute(txt_desc, "value");
		if (enteredDescription.equals(description)) {
			writeTestResults("Verify that user able to enter the Description",
					"User should be able to enter the Description", "User successfully enter the Description", "pass");
		} else {
			writeTestResults("Verify that user able to enter the Description",
					"User should be able to enter the Description", "User doesn't enter the Description", "fail");

		}

		selectText(drop_productGroup, productGroupSmokeProduction);
		Thread.sleep(1000);
		String selectedProductGroup = getSelectedOptionInDropdown(drop_productGroup);
		if (selectedProductGroup.equals(productGroupSmokeProduction)) {
			writeTestResults("Verify that user able to select the Product Group",
					"User should be able to select the Product Group", "User successfully select the Product Group",
					"pass");
		} else {
			writeTestResults("Verify that user able to select the Product Group",
					"User should be able to select the Product Group", "User doesn't select the Product Group", "fail");

		}

		click(btn_lookupOutputProduct);
		sendKeysLookup(txt_productSearch, getInvObj().readTestCreation("BatchFifoProductionSmokeProductionType"));
		pressEnter(txt_productSearch);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info",
				getInvObj().readTestCreation("BatchFifoProductionSmokeProductionType")), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info",
				getInvObj().readTestCreation("BatchFifoProductionSmokeProductionType")));

		Thread.sleep(1500);

		String selectedProduct = getAttribute(txt_productFrontPageBOM, "value");
		if (selectedProduct.contains(getInvObj().readTestCreation("BatchFifoProductionSmokeProductionType"))) {
			writeTestResults("Verify that user able to select Production Type Product",
					"User should be able to select Production Type Product",
					"User successfully select Production Type Product", "pass");
		} else {
			writeTestResults("Verify that user able to select Production Type Product",
					"User should be able to select Production Type Product",
					"User doesn't select Production Type Product", "fail");

		}

		sendKeys(txt_quantityBOM, hundred);
		String enterdQty = getAttribute(txt_quantityBOM, "value");
		if (enterdQty.equals(hundred)) {
			writeTestResults("Verify that user able to enter the Quantity", "User should be able to enter the Quantity",
					"User successfully enter the Quantity", "pass");
		} else {
			writeTestResults("Verify that user able to enter the Quantity", "User should be able to enter the Quantity",
					"User doesn't enter the Quantity", "fail");

		}

		click(btn_lookupOnLineBOMRowReplace.replace("row", "1"));
		sendKeysLookup(txt_productSearch, getInvObj().readTestCreation("BatchFifoProductionSmokeOne"));
		pressEnter(txt_productSearch);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info",
				getInvObj().readTestCreation("BatchFifoProductionSmokeOne")), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info",
				getInvObj().readTestCreation("BatchFifoProductionSmokeOne")));

		explicitWait(txt_qtyBOMInputProductLinesRowReplace.replace("row", "1"), 10);
		sendKeys(txt_qtyBOMInputProductLinesRowReplace.replace("row", "1"), ten);

		click(btn_addNewRecord);
		click(btn_lookupOnLineBOMRowReplace.replace("row", "2"));
		sendKeysLookup(txt_productSearch, getInvObj().readTestCreation("BatchFifoProductionSmokeTwo"));
		pressEnter(txt_productSearch);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info",
				getInvObj().readTestCreation("BatchFifoProductionSmokeTwo")), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info",
				getInvObj().readTestCreation("BatchFifoProductionSmokeTwo")));

		explicitWait(txt_qtyBOMInputProductLinesRowReplace.replace("row", "2"), 10);
		sendKeys(txt_qtyBOMInputProductLinesRowReplace.replace("row", "2"), five);

		click(btn_draft);
		explicitWait(header_draftedBOM, 50);
		if (isDisplayed(header_draftedBOM)) {
			writeTestResults("Verify user able to draft the Bill Of Material",
					"User should be able to draft the Bill Of Material", "User successfully draft the Bill Of Material",
					"pass");
		} else {
			writeTestResults("Verify user able to draft the Bill Of Material",
					"User should be able to draft the Bill Of Material", "User doesn't draft the Bill Of Material",
					"fail");

		}

		click(btn_releaseCommon);
		explicitWait(header_releasedBillOfMaterial, 50);
		trackCode = getText(lbl_docNumber);
		writeProductionData("BOM_Smoke_Production_0001_1", trackCode, 0);
		if (isDisplayed(header_releasedBillOfMaterial)) {
			writeTestResults("Verify user able to release the Bill Of Material",
					"User should be able to release the Bill Of Material",
					"User successfully release the Bill Of Material", "pass");
		} else {
			writeTestResults("Verify user able to release the Bill Of Material",
					"User should be able to release the Bill Of Material", "User doesn't release the Bill Of Material",
					"fail");

		}
	}

	/* Smoke_Production_0001_2 */
	public void billOfOperation_Smoke_Production_0001_2() throws Exception {
		autmationTenantLogin();
		explicitWait(btn_navigationPane, 20);
		Thread.sleep(2000);
		explicitWait(btn_navigationPane, 20);
		click(btn_navigationPane);
		explicitWait(btn_productionModule, 10);
		click(btn_productionModule);
		explicitWait(btn_billOfOperation, 10);
		click(btn_billOfOperation);
		explicitWait(header_BillOfOperationByPage, 40);
		if (isDisplayed(header_BillOfOperationByPage)) {
			writeTestResults("Verify user can navigate to the Bill Of Operation by-page",
					"User should be able to navigate to Bill Of Operation by-page",
					"User successfully navigate to Bill Of Operation by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to the Bill Of Operation by-page",
					"User should be able to navigate to Bill Of Operation by-page",
					"User doesn't navigate to Bill Of Operation by-page", "fail");

		}
		click(btn_newBillOfOperation);
		explicitWait(header_newBillOfOperation, 50);
		if (isDisplayed(header_newBillOfOperation)) {
			writeTestResults("Verify user can navigate to the new Bill Of Operation",
					"User should be able to navigate to new Bill Of Operation",
					"User successfully navigate to new Bill Of Operation", "pass");
		} else {
			writeTestResults("Verify user can navigate to the new Bill Of Operation",
					"User should be able to navigate to new Bill Of Operation",
					"User doesn't navigate to new Bill Of Operation", "fail");
		}

		String bookCodeBOO = getInvObj().currentTimeAndDate().replace("/", "").replace(":", "");
		sendKeys(txt_booCodeBillOfOperation, bookCodeBOO);

		click(btn_addBillOfOperationRowReplace.replace("row", "1"));

		explicitWait(drop_elementCategoryBOO, 20);
		selectText(drop_elementCategoryBOO, elementCategorySupply);
		Thread.sleep(1000);
		String initialBOOElementCategory = getSelectedOptionInDropdown(drop_elementCategoryBOO);
		if (initialBOOElementCategory.equals(elementCategorySupply)) {
			writeTestResults("Verify user able to select 'Supply' as Element Category for the initial BOO",
					"User should be able to select 'Supply' as Element Category for the initial BOO",
					"User successfully select 'Supply' as Element Category for the initial BOO", "pass");
		} else {
			writeTestResults("Verify user able to select 'Supply' as Element Category for the initial BOO",
					"User should be able to select 'Supply' as Element Category for the initial BOO",
					"User doesn't select 'Supply' as Element Category for the initial BOO", "fail");
		}

		selectText(drop_activityTypeBOO, activityTypeMoveStock);
		Thread.sleep(1000);
		String initialBOOActivityType = getSelectedOptionInDropdown(drop_activityTypeBOO);
		if (initialBOOActivityType.equals(activityTypeMoveStock)) {
			writeTestResults("Verify user able to select 'Move Stock' as Activity Type for the initial BOO",
					"User should be able to select 'Move Stock' as Activity Type for the initial BOO",
					"User successfully select 'Move Stock' as Activity Type for the initial BOO", "pass");
		} else {
			writeTestResults("Verify user able to select 'Move Stock' as Activity Type for the initial BOO",
					"User should be able to select 'Move Stock' as Activity Type for the initial BOO",
					"User doesn't select 'Move Stock' as Activity Type for the initial BOO", "fail");
		}

		sendKeys(txt_elementDescriptionBOO, moveStockElementDescription);
		Thread.sleep(1000);
		String initialBOOEnteredDesc = getAttribute(txt_elementDescriptionBOO, "value");
		if (initialBOOEnteredDesc.equals(moveStockElementDescription)) {
			writeTestResults("Verify user able to enter Element Description for the initial BOO",
					"User should be able to enter Element Description for the initial BOO",
					"User successfully enter Element Description for the initial BOO", "pass");
		} else {
			writeTestResults("Verify user able to enter Element Description for the initial BOO",
					"User should be able to enter Element Description for the initial BOO",
					"User doesn't enter Element Description for the initial BOO", "fail");
		}

		click(btn_applyOperationOneBOO);

		explicitWait(btn_addBillOfOperationRowReplace.replace("row", "2"), 10);
		click(btn_addBillOfOperationRowReplace.replace("row", "2"));

		explicitWait(drop_elementCategoryBOO, 20);
		selectText(drop_elementCategoryBOO, elementCategoryOperation);
		Thread.sleep(1000);
		String firstBOOElementCategory = getSelectedOptionInDropdown(drop_elementCategoryBOO);
		if (firstBOOElementCategory.equals(elementCategoryOperation)) {
			writeTestResults("Verify user able to select 'Operation' as Element Category for the first BOO",
					"User should be able to select 'Operation' as Element Category for the first BOO",
					"User successfully select 'Operation' as Element Category for the first BOO", "pass");
		} else {
			writeTestResults("Verify user able to select 'Operation' as Element Category for the first BOO",
					"User should be able to select 'Operation' as Element Category for the first BOO",
					"User doesn't select 'Operation' as Element Category for the first BOO", "fail");
		}

		Thread.sleep(1000);
		String firstBOOActivityType = getSelectedOptionInDropdown(drop_activityTypeBOO);
		if (firstBOOActivityType.equals(activityTypeProduce)) {
			writeTestResults("Verify auto select 'Produce' as Activity Type for the first BOO",
					"Should auto select 'Produce' as Activity Type for the first BOO",
					"Auto select 'Produce' as Activity Type for the first BOO", "pass");
		} else {
			writeTestResults("Verify auto select 'Produce' as Activity Type for the first BOO",
					"Should auto select 'Produce' as Activity Type for the first BOO",
					"Not auto select 'Produce' as Activity Type for the first BOO", "fail");
		}

		sendKeys(txt_elementDescriptionBOO, produceElementDescription1);
		Thread.sleep(1000);
		String firstBOOEnteredDesc = getAttribute(txt_elementDescriptionBOO, "value");
		if (firstBOOEnteredDesc.equals(produceElementDescription1)) {
			writeTestResults("Verify user able to enter Element Description for the first BOO",
					"User should be able to enter Element Description for the first BOO",
					"User successfully enter Element Description for the first BOO", "pass");
		} else {
			writeTestResults("Verify user able to enter Element Description for the first BOO",
					"User should be able to enter Element Description for the first BOO",
					"User doesn't enter Element Description for the first BOO", "fail");
		}

		sendKeys(txt_groupIdBOO, groupIdBOOOne);
		Thread.sleep(1000);
		String firstBOOEnteredGroupID = getAttribute(txt_groupIdBOO, "value");
		if (firstBOOEnteredGroupID.equals(groupIdBOOOne)) {
			writeTestResults("Verify user able to enter Group ID for the first BOO",
					"User should be able to enter Group ID for the first BOO",
					"User successfully enter Group ID for the first BOO", "pass");
		} else {
			writeTestResults("Verify user able to enter Group ID for the first BOO",
					"User should be able to enter Group ID for the first BOO",
					"User doesn't enter Group ID for the first BOO", "fail");
		}

		Thread.sleep(1000);
		String firstBOOBatchMethod = getSelectedOptionInDropdown(drop_batchMethodBOO);
		if (firstBOOBatchMethod.equals(batchMethodTransferBOO)) {
			writeTestResults("Verify auto select 'Transfer' as Batch Method for the first BOO",
					"Should auto select 'Transfer' as Batch Method for the first BOO",
					"Auto select 'Transfer' as Batch Method for the first BOO", "pass");
		} else {
			writeTestResults("Verify auto select 'Transfer' as Batch Method for the first BOO",
					"Should auto select 'Transfer' as Batch Method for the first BOO",
					"Not auto select 'Transfer' as Batch Method for the first BOO", "fail");
		}

		click(btn_applyOperationOneBOO);

		explicitWait(btn_addBillOfOperationRowReplace.replace("row", "2"), 10);
		click(btn_addBillOfOperationRowReplace.replace("row", "2"));

		explicitWait(drop_elementCategoryBOO, 20);
		selectText(drop_elementCategoryBOO, elementCategoryOperation);
		Thread.sleep(1000);
		String secondBOOElementCategory = getSelectedOptionInDropdown(drop_elementCategoryBOO);
		if (secondBOOElementCategory.equals(elementCategoryOperation)) {
			writeTestResults("Verify user able to select 'Operation' as Element Category for the second BOO",
					"User should be able to select 'Operation' as Element Category for the second BOO",
					"User successfully select 'Operation' as Element Category for the second BOO", "pass");
		} else {
			writeTestResults("Verify user able to select 'Operation' as Element Category for the second BOO",
					"User should be able to select 'Operation' as Element Category for the second BOO",
					"User doesn't select 'Operation' as Element Category for the second BOO", "fail");
		}

		Thread.sleep(1000);
		String secondBOOActivityType = getSelectedOptionInDropdown(drop_activityTypeBOO);
		if (secondBOOActivityType.equals(activityTypeProduce)) {
			writeTestResults("Verify auto select 'Produce' as Activity Type for the second BOO",
					"Should auto select 'Produce' as Activity Type for the second BOO",
					"Auto select 'Produce' as Activity Type for the second BOO", "pass");
		} else {
			writeTestResults("Verify auto select 'Produce' as Activity Type for the second BOO",
					"Should auto select 'Produce' as Activity Type for the second BOO",
					"Not auto select 'Produce' as Activity Type for the second BOO", "fail");
		}

		sendKeys(txt_elementDescriptionBOO, produceElementDescription2);
		Thread.sleep(1000);
		String secondBOOEnteredDesc = getAttribute(txt_elementDescriptionBOO, "value");
		if (secondBOOEnteredDesc.equals(produceElementDescription2)) {
			writeTestResults("Verify user able to enter Element Description for the second BOO",
					"User should be able to enter Element Description for the second BOO",
					"User successfully enter Element Description for the second BOO", "pass");
		} else {
			writeTestResults("Verify user able to enter Element Description for the second BOO",
					"User should be able to enter Element Description for the second BOO",
					"User doesn't enter Element Description for the second BOO", "fail");
		}

		sendKeys(txt_groupIdBOO, groupIdBOOTwo);
		Thread.sleep(1000);
		String secondBOOEnteredGroupID = getAttribute(txt_groupIdBOO, "value");
		if (secondBOOEnteredGroupID.equals(groupIdBOOTwo)) {
			writeTestResults("Verify user able to enter Group ID for the second BOO",
					"User should be able to enter Group ID for the second BOO",
					"User successfully enter Group ID for the second BOO", "pass");
		} else {
			writeTestResults("Verify user able to enter Group ID for the second BOO",
					"User should be able to enter Group ID for the second BOO",
					"User doesn't enter Group ID for the second BOO", "fail");
		}

		Thread.sleep(1000);
		String secondBOOBatchMethod = getSelectedOptionInDropdown(drop_batchMethodBOO);
		if (secondBOOBatchMethod.equals(batchMethodTransferBOO)) {
			writeTestResults("Verify auto select 'Transfer' as Batch Method for the second BOO",
					"Should auto select 'Transfer' as Batch Method for the second BOO",
					"Auto select 'Transfer' as Batch Method for the second BOO", "pass");
		} else {
			writeTestResults("Verify auto select 'Transfer' as Batch Method for the second BOO",
					"Should auto select 'Transfer' as Batch Method for the second BOO",
					"Not auto select 'Transfer' as Batch Method for the second BOO", "fail");
		}

		click(btn_applyOperationOneBOO);

		explicitWait(btn_addBillOfOperationRowReplace.replace("row", "2"), 10);
		click(btn_addBillOfOperationRowReplace.replace("row", "2"));

		explicitWait(drop_elementCategoryBOO, 20);
		selectText(drop_elementCategoryBOO, elementCategoryOperation);
		Thread.sleep(1000);
		String thirdBOOElementCategory = getSelectedOptionInDropdown(drop_elementCategoryBOO);
		if (thirdBOOElementCategory.equals(elementCategoryOperation)) {
			writeTestResults("Verify user able to select 'Operation' as Element Category for the third BOO",
					"User should be able to select 'Operation' as Element Category for the third BOO",
					"User successfully select 'Operation' as Element Category for the third BOO", "pass");
		} else {
			writeTestResults("Verify user able to select 'Operation' as Element Category for the third BOO",
					"User should be able to select 'Operation' as Element Category for the third BOO",
					"User doesn't select 'Operation' as Element Category for the third BOO", "fail");
		}

		Thread.sleep(1000);
		selectText(drop_activityTypeBOO, activityTypeQualityCheck);
		String thirdBOOActivityType = getSelectedOptionInDropdown(drop_activityTypeBOO);
		if (thirdBOOActivityType.equals(activityTypeQualityCheck)) {
			writeTestResults("Verify user able to select 'Quality Check' as Activity Type for the third BOO",
					"User should be able to select 'Quality Check' as Activity Type for the third BOO",
					"User successfully select 'Quality Check' as Activity Type for the third BOO", "pass");
		} else {
			writeTestResults("Verify user able to select 'Quality Check' as Activity Type for the third BOO",
					"User should be able to select 'Quality Check' as Activity Type for the third BOO",
					"User doesn't select 'Quality Check' as Activity Type for the third BOO", "fail");
		}

		sendKeys(txt_elementDescriptionBOO, qcElementDescription);
		Thread.sleep(1000);
		String thirdBOOEnteredDesc = getAttribute(txt_elementDescriptionBOO, "value");
		if (thirdBOOEnteredDesc.equals(qcElementDescription)) {
			writeTestResults("Verify user able to enter Element Description for the third BOO",
					"User should be able to enter Element Description for the third BOO",
					"User successfully enter Element Description for the third BOO", "pass");
		} else {
			writeTestResults("Verify user able to enter Element Description for the third BOO",
					"User should be able to enter Element Description for the third BOO",
					"User doesn't enter Element Description for the third BOO", "fail");
		}

		sendKeys(txt_groupIdBOO, groupIdBOOThree);
		Thread.sleep(1000);
		String thirdBOOEnteredGroupID = getAttribute(txt_groupIdBOO, "value");
		if (thirdBOOEnteredGroupID.equals(groupIdBOOThree)) {
			writeTestResults("Verify user able to enter Group ID for the third BOO",
					"User should be able to enter Group ID for the third BOO",
					"User successfully enter Group ID for the third BOO", "pass");
		} else {
			writeTestResults("Verify user able to enter Group ID for the third BOO",
					"User should be able to enter Group ID for the third BOO",
					"User doesn't enter Group ID for the third BOO", "fail");
		}

		click(chk_finalInvestigationBOO);
		if (isSelected(chk_finalInvestigationBOO)) {
			writeTestResults("Verify user able to enable Final Investigation flag for the third BOO",
					"User should be able to enable Final Investigation flag for the third BOO",
					"User successfully enable Final Investigation flag for the third BOO", "pass");
		} else {
			writeTestResults("Verify user able to enable Final Investigation flag for the third BOO",
					"User should be able to enable Final Investigation flag for the third BOO",
					"User doesn't enable Final Investigation flag for the third BOO", "fail");
		}

		Thread.sleep(1000);
		String thirdBOOBatchMethod = getSelectedOptionInDropdown(drop_batchMethodBOO);
		if (thirdBOOBatchMethod.equals(batchMethodTransferBOO)) {
			writeTestResults("Verify auto select 'Transfer' as Batch Method for the third BOO",
					"Should auto select 'Transfer' as Batch Method for the third BOO",
					"Auto select 'Transfer' as Batch Method for the third BOO", "pass");
		} else {
			writeTestResults("Verify auto select 'Transfer' as Batch Method for the third BOO",
					"Should auto select 'Transfer' as Batch Method for the third BOO",
					"Not auto select 'Transfer' as Batch Method for the third BOO", "fail");
		}

		click(btn_applyOperationOneBOO);

		click(btn_draft);
		explicitWait(header_draftedBOO, 50);
		if (isDisplayed(header_draftedBOO)) {
			writeTestResults("Verify user able to draft the Bill Of Operation",
					"User should be able to draft the Bill Of Operation",
					"User successfully draft the Bill Of Operation", "pass");
		} else {
			writeTestResults("Verify user able to draft the Bill Of Operation",
					"User should be able to draft the Bill Of Operation", "User doesn't draft the Bill Of Operation",
					"fail");

		}

		click(btn_releaseCommon);
		explicitWait(header_releasedBOO, 50);
		trackCode = getText(lbl_docNumber);
		writeProductionData("BOO_Production_Smoke_0001_2", trackCode, 1);
		if (isDisplayed(header_releasedBOO)) {
			writeTestResults("Verify user able to release the Bill Of Operation",
					"User should be able to release the Bill Of Operation",
					"User successfully release the Bill Of Operation", "pass");
		} else {
			writeTestResults("Verify user able to release the Bill Of Operation",
					"User should be able to release the Bill Of Operation",
					"User doesn't release the Bill Of Operation", "fail");

		}
	}

	/* Smoke_Production_0001_3 */
	public void productionModel_Smoke_Production_0001_3_1() throws Exception {
		autmationTenantLogin();
		explicitWait(btn_navigationPane, 20);
		Thread.sleep(2000);
		explicitWait(btn_navigationPane, 20);
		click(btn_navigationPane);
		explicitWait(btn_productionModule, 10);
		click(btn_productionModule);
		explicitWait(btn_productionModelRebuilt, 10);
		click(btn_productionModelRebuilt);
		explicitWait(header_productionModelByPage, 40);
		if (isDisplayed(header_productionModelByPage)) {
			writeTestResults("Verify user can navigate to the Production Model by-page",
					"User should be able to navigate to Production Model by-page",
					"User successfully navigate to Production Model by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to the Production Model by-page",
					"User should be able to navigate to Production Model by-page",
					"User doesn't navigate to Production Model by-page", "fail");

		}
		click(btn_newProductionModelRebult);
		explicitWait(header_newProductionModel, 50);
		if (isDisplayed(header_newProductionModel)) {
			writeTestResults("Verify user can navigate to the new Production Model",
					"User should be able to navigate to new Production Model",
					"User successfully navigate to new Production Model", "pass");
		} else {
			writeTestResults("Verify user can navigate to the new Production Model",
					"User should be able to navigate to new Production Model",
					"User doesn't navigate to new Production Model", "fail");
		}

		String modelCode = getInvObj().currentTimeAndDate().replace("/", "").replace(":", "");
		sendKeys(txt_modelCodePM, modelCode);
		String enteredModelCode = getAttribute(txt_modelCodePM, "value");
		if (enteredModelCode.equals(modelCode)) {
			writeTestResults("Verify user can enter the Model Code", "User should be able to enter the Model Code",
					"User successfully enter the Model Code", "pass");
		} else {
			writeTestResults("Verify user can enter the Model Code", "User should be able to enter the Model Code",
					"User doesn't enter the Model Code", "fail");
		}

		selectText(drop_productGroup, productGroupSmokeProduction);
		Thread.sleep(1500);
		explicitWait(btn_yesClearConfirmationPM, 10);
		click(btn_yesClearConfirmationPM);
		Thread.sleep(1000);
		String selectedProductGroup = getSelectedOptionInDropdown(drop_productGroup);
		if (selectedProductGroup.equals(productGroupSmokeProduction)) {
			writeTestResults("Verify that user able to select the Product Group",
					"User should be able to select the Product Group", "User successfully select the Product Group",
					"pass");
		} else {
			writeTestResults("Verify that user able to select the Product Group",
					"User should be able to select the Product Group", "User doesn't select the Product Group", "fail");

		}

		click(btn_lookupOutputProduct);
		sendKeysLookup(txt_productSearch, getInvObj().readTestCreation("BatchFifoProductionSmokeProductionType"));
		pressEnter(txt_productSearch);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info",
				getInvObj().readTestCreation("BatchFifoProductionSmokeProductionType")), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info",
				getInvObj().readTestCreation("BatchFifoProductionSmokeProductionType")));

		Thread.sleep(1500);
		explicitWait(btn_yesClearConfirmationPM, 10);
		click(btn_yesClearConfirmationPM);
		Thread.sleep(1500);
		String selectedProduct = getAttribute(txt_produxtProductionModelFrontPage, "value");
		if (selectedProduct.contains(getInvObj().readTestCreation("BatchFifoProductionSmokeProductionType"))) {
			writeTestResults("Verify that user able to select the Output Product",
					"User should be able to select the Output Product", "User successfully select the Output Product",
					"pass");
		} else {
			writeTestResults("Verify that user able to select the Output Product",
					"User should be able to select the Output Product", "User doesn't select the Output Product",
					"fail");

		}

		click(btn_productionUnitLookup);
		sendKeysLookup(txt_prductionUnitSearch, productionUnit);
		pressEnter(txt_prductionUnitSearch);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", productionUnit), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", productionUnit));

		Thread.sleep(1500);

		String selectedUnit = getAttribute(txt_productionUnitFrontPagePM, "value");
		if (selectedUnit.contains(productionUnit)) {
			writeTestResults("Verify that user able to select the Production Unit",
					"User should be able to select the Production Unit", "User successfully select the Production Unit",
					"pass");
		} else {
			writeTestResults("Verify that user able to select the Production Unit",
					"User should be able to select the Production Unit", "User doesn't select the Production Unit",
					"fail");

		}

		String selectedSite = getSelectedOptionInDropdown(drop_sitePM);
		if (selectedSite.equals(siteNegombo)) {
			writeTestResults("Verify that site is selected as Negombo",
					"Site should be automatically select as Negombo", "Site automatically select as Negombo", "pass");
		} else {
			writeTestResults("Verify that site is selected as Negombo",
					"Site should be automatically select as Negombo", "Site not automatically select as Negombo",
					"fail");

		}

		String selectedBOM = getSelectedOptionInDropdown(drop_bomPM);
		String bom = readProductionData("BOM_Smoke_Production_0001_1");
		if (selectedBOM.contains(bom)) {
			writeTestResults("Verify that BOM is automatically selected", "BOB should be automatically selected",
					"BOM successfully selected automatically", "pass");
		} else {
			writeTestResults("Verify that BOM is automatically selected", "BOB should be automatically selected",
					"BOM deosn't selected automatically", "fail");
		}

		sendKeys(txt_batchQuantityPM, hundred);
		String enteredBatchQty = getAttribute(txt_batchQuantityPM, "value");
		if (enteredBatchQty.equals(hundred)) {
			writeTestResults("Verify user can enter the Batch Quantity",
					"User should be able to enter the Batch Quantity", "User successfully enter the Batch Quantity",
					"pass");
		} else {
			writeTestResults("Verify user can enter the Batch Quantity",
					"User should be able to enter the Batch Quantity", "User doesn't enter the Batch Quantity", "fail");
		}

		selectText(drop_barcodeBookPM, barcodeBookBG);
		Thread.sleep(1000);
		String selectedBB = getSelectedOptionInDropdown(drop_barcodeBookPM);
		if (selectedBB.equals(barcodeBookBG)) {
			writeTestResults("Verify that user able to select the Barcode Book",
					"User should be able to select the Barcode Book", "User successfully select the Barcode Book",
					"pass");
		} else {
			writeTestResults("Verify that user able to select the Barcode Book",
					"User should be able to select the Barcode Book", "User doesn't select the Barcode Book", "fail");

		}

		selectText(drop_costinPriority, costingProrityNormal);
		Thread.sleep(1000);
		String selecteCostingPriority = getSelectedOptionInDropdown(drop_costinPriority);
		if (selecteCostingPriority.equals(costingProrityNormal)) {
			writeTestResults("Verify that user able to select the Costing Priority",
					"User should be able to select the Costing Priority",
					"User successfully select the Costing Priority", "pass");
		} else {
			writeTestResults("Verify that user able to select the Costing Priority",
					"User should be able to select the Costing Priority", "User doesn't select the Costing Priority",
					"fail");

		}

	}

	public void productionModel_Smoke_Production_0001_3_2() throws Exception {
		click(btn_pricingProfileLookup);
		sendKeysLookup(txt_searchPricingProfile, pricingProfile);
		pressEnter(txt_searchPricingProfile);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", pricingProfile), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", pricingProfile));

		Thread.sleep(1500);

		String selectedUnit = getAttribute(txt_pricingProfileProntPage, "value");
		if (selectedUnit.contains(pricingProfile)) {
			writeTestResults("Verify that user able to select the Pricing Profile",
					"User should be able to select the Pricing Profile", "User successfully select the Pricing Profile",
					"pass");
		} else {
			writeTestResults("Verify that user able to select the Pricing Profile",
					"User should be able to select the Pricing Profile", "User doesn't select the Pricing Profile",
					"fail");

		}

		click(chk_mrpPM);
		if (isSelected(chk_mrpPM)) {
			writeTestResults("Verify that user able to enable MRP", "User should be able to enable MRP",
					"User successfully enable MRP", "pass");
		} else {
			writeTestResults("Verify that user able to enable MRP", "User should be able to enable MRP",
					"User doesn't enable MRP", "fail");
		}

		click(btn_billOfOperationLookup);
		sendKeysLookup(txt_searchBillOfOperation, readProductionData("BOO_Production_Smoke_0001_2"));
		pressEnter(txt_searchBillOfOperation);
		Thread.sleep(2000);
		explicitWait(
				lnk_inforOnLookupInformationReplace.replace("info", readProductionData("BOO_Production_Smoke_0001_2")),
				20);
		doubleClick(
				lnk_inforOnLookupInformationReplace.replace("info", readProductionData("BOO_Production_Smoke_0001_2")));

		Thread.sleep(1500);
		explicitWait(btn_okClearMsgBOOPM, 10);
		click(btn_okClearMsgBOOPM);

		Thread.sleep(2000);
		String selectedBOO = getAttribute(txt_frontPageBOOPM, "value");
		if (selectedBOO.contains(readProductionData("BOO_Production_Smoke_0001_2"))) {
			writeTestResults("Verify that user able to select the Bill Of Operation",
					"User should be able to select the Bill Of Operation",
					"User successfully select the Bill Of Operation", "pass");
		} else {
			writeTestResults("Verify that user able to select the Bill Of Operation",
					"User should be able to select the Bill Of Operation", "User doesn't select the Bill Of Operation",
					"fail");

		}

		click(btn_tabBillOfOperationPM);

		explicitWait(btn_expandBOOPM.replace("row", "1"), 20);
		Thread.sleep(1500);
		click(btn_expandBOOPM.replace("row", "1"));

		explicitWait(btn_expandBOOPM.replace("row", "2"), 20);
		click(btn_expandBOOPM.replace("row", "2"));

		click(btn_editBillOfOperationPMRowReplace.replace("row", "1"));
		click(btn_inputProductsTabBOOPM);
		click(chk_inputProductsRowReplaceBOOPM.replace("row", "1"));
		click(chk_inputProductsRowReplaceBOOPM.replace("row", "2"));
		if (isSelected(chk_inputProductsRowReplaceBOOPM.replace("row", "1"))
				&& isSelected(chk_inputProductsRowReplaceBOOPM.replace("row", "2"))) {
			writeTestResults("Verify that user able to enable both input products for initial operation",
					"User should be able to enable both input products for initial operation",
					"User successfully enable both input products for initial operation", "pass");
		} else {
			writeTestResults("Verify that user able to enable both input products for initial operation",
					"User should be able to enable both input products for initial operation",
					"User doesn't enable both input products for initial operation", "fail");
		}

		click(btn_applyBOOPM);

		explicitWait(btn_editBillOfOperationPMRowReplace.replace("row", "2"), 10);
		click(btn_editBillOfOperationPMRowReplace.replace("row", "2"));

		selectText(drop_processTimeTypeBOOPM, processTimeType);

		click(btn_inputProductsTabBOOPM);
		click(chk_inputProductsRowReplaceBOOPM.replace("row", "1"));
		if (isSelected(chk_inputProductsRowReplaceBOOPM.replace("row", "1"))) {
			writeTestResults("Verify that user able to enable a product for the first operation",
					"User should be able to enable a product for the first operation",
					"User successfully enable a product for the first operation", "pass");
		} else {
			writeTestResults("Verify that user able to enable a product for the first operation",
					"User should be able to enable a product for the first operation",
					"User doesn't enable a product for the first operation", "fail");
		}

		click(btn_applyBOOPM);

		explicitWait(btn_editBillOfOperationPMRowReplace.replace("row", "3"), 10);
		click(btn_editBillOfOperationPMRowReplace.replace("row", "3"));

		selectText(drop_processTimeTypeBOOPM, processTimeType);

		click(btn_inputProductsTabBOOPM);
		click(chk_inputProductsRowReplaceBOOPM.replace("row", "1"));
		if (isSelected(chk_inputProductsRowReplaceBOOPM.replace("row", "1"))) {
			writeTestResults("Verify that user able to enable a product for the second operation",
					"User should be able to enable a product for the second operation",
					"User successfully enable a product for the second operation", "pass");
		} else {
			writeTestResults("Verify that user able to enable a product for the second operation",
					"User should be able to enable a product for the second operation",
					"User doesn't enable a product for the second operation", "fail");
		}

		click(btn_applyBOOPM);

		click(btn_draft);
		explicitWait(header_draftedPM, 50);
		if (isDisplayed(header_draftedPM)) {
			writeTestResults("Verify user able to draft the Production Model",
					"User should be able to draft the Production Model", "User successfully draft the Production Model",
					"pass");
		} else {
			writeTestResults("Verify user able to draft the Production Model",
					"User should be able to draft the Production Model", "User doesn't draft the Production Model",
					"fail");

		}

		click(btn_releaseCommon);
		explicitWait(header_releasedPM, 50);
		trackCode = getText(lbl_docNumber);
		writeProductionData("PM_Production_Smoke_0001_3", trackCode, 2);
		if (isDisplayed(header_releasedPM)) {
			writeTestResults("Verify user able to release the Production Model",
					"User should be able to release the Production Model",
					"User successfully release the Production Model", "pass");
		} else {
			writeTestResults("Verify user able to release the Production Model",
					"User should be able to release the Production Model", "User doesn't release the Production Model",
					"fail");

		}
	}

	/* Smoke_Production_0002 */
	public void productionOrder_Smoke_Production_0002() throws Exception {
		autmationTenantLogin();
		explicitWait(btn_navigationPane, 20);
		Thread.sleep(2000);
		explicitWait(btn_navigationPane, 20);
		click(btn_navigationPane);
		explicitWait(btn_productionModule, 10);
		click(btn_productionModule);
		explicitWait(btn_productionOrder2, 10);
		click(btn_productionOrder2);
		explicitWait(header_productionOrderByPage, 40);
		if (isDisplayed(header_productionOrderByPage)) {
			writeTestResults("Verify user can navigate to the Production Order by-page",
					"User should be able to navigate to Production Order by-page",
					"User successfully navigate to Production Order by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to the Production Order by-page",
					"User should be able to navigate to Production Order by-page",
					"User doesn't navigate to Production Order by-page", "fail");

		}
		click(btn_newProductionOrder2);
		explicitWait(header_newProductionOrder, 50);
		if (isDisplayed(header_newProductionOrder)) {
			writeTestResults("Verify user can navigate to the new Production Order",
					"User should be able to navigate to new Production Order",
					"User successfully navigate to new Production Order", "pass");
		} else {
			writeTestResults("Verify user can navigate to the new Production Order",
					"User should be able to navigate to new Production Order",
					"User doesn't navigate to new Production Order", "fail");
		}

		selectText(drop_productionOrderGroupPO, productionOrderGroup);
		Thread.sleep(1000);
		String selectePOGroup = getSelectedOptionInDropdown(drop_productionOrderGroupPO);
		if (selectePOGroup.equals(productionOrderGroup)) {
			writeTestResults("Verify that user able to select the Production Order Group",
					"User should be able to select the Production Order Group",
					"User successfully select the Production Order Group", "pass");
		} else {
			writeTestResults("Verify that user able to select the Production Order Group",
					"User should be able to select the Production Order Group",
					"User doesn't select the Production Order Group", "fail");

		}

		sendKeys(txt_descriptionPO, productionOrderDescription);
		String enteredDescription = getAttribute(txt_descriptionPO, "value");
		if (enteredDescription.equals(productionOrderDescription)) {
			writeTestResults("Verify user can enter the Description", "User should be able to enter the Description",
					"User successfully enter the Description", "pass");
		} else {
			writeTestResults("Verify user can enter the Description", "User should be able to enter the Description",
					"User doesn't enter the Description", "fail");
		}

		click(btn_lookupOutputProduct);
		sendKeysLookup(txt_productSearch, getInvObj().readTestCreation("BatchFifoProductionSmokeProductionType"));
		pressEnter(txt_productSearch);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info",
				getInvObj().readTestCreation("BatchFifoProductionSmokeProductionType")), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info",
				getInvObj().readTestCreation("BatchFifoProductionSmokeProductionType")));

		Thread.sleep(1500);
		String selectedProduct = getAttribute(txt_frontPageOutputProductPO, "value");
		if (selectedProduct.contains(getInvObj().readTestCreation("BatchFifoProductionSmokeProductionType"))) {
			writeTestResults("Verify that user able to select the Output Product",
					"User should be able to select the Output Product", "User successfully select the Output Product",
					"pass");
		} else {
			writeTestResults("Verify that user able to select the Output Product",
					"User should be able to select the Output Product", "User doesn't select the Output Product",
					"fail");

		}

		sendKeys(txt_planedQuantityPO, hundred);
		String enteredPlanedQty = getAttribute(txt_planedQuantityPO, "value");
		if (enteredPlanedQty.equals(hundred)) {
			writeTestResults("Verify user can enter the Planed Quantity",
					"User should be able to enter the Planed Quantity", "User successfully enter the Planed Quantity",
					"pass");
		} else {
			writeTestResults("Verify user can enter the Planed Quantity",
					"User should be able to enter the Planed Quantity", "User doesn't enter the Planed Quantity",
					"fail");
		}

		click(btn_productionModuleLookup);
		sendKeysLookup(txt_searchProductionModule, readProductionData("PM_Production_Smoke_0001_3"));
		pressEnter(txt_searchProductionModule);
		Thread.sleep(2000);
		explicitWait(
				lnk_inforOnLookupInformationReplace.replace("info", readProductionData("PM_Production_Smoke_0001_3")),
				20);
		doubleClick(
				lnk_inforOnLookupInformationReplace.replace("info", readProductionData("PM_Production_Smoke_0001_3")));

		Thread.sleep(1500);
		String selectedProductionModule = getAttribute(txt_productionModuleFrontPagePO, "value");
		if (selectedProductionModule.contains(readProductionData("PM_Production_Smoke_0001_3"))) {
			writeTestResults("Verify that user able to select the Production Module",
					"User should be able to select the Production Module",
					"User successfully select the Production Module", "pass");
		} else {
			writeTestResults("Verify that user able to select the Production Module",
					"User should be able to select the Production Module", "User doesn't select the Production Module",
					"fail");

		}

		click(btn_draft);
		explicitWait(header_draftedPO, 50);
		if (isDisplayed(header_draftedPO)) {
			writeTestResults("Verify user able to draft the Production Order",
					"User should be able to draft the Production Order", "User successfully draft the Production Order",
					"pass");
		} else {
			writeTestResults("Verify user able to draft the Production Order",
					"User should be able to draft the Production Order", "User doesn't draft the Production Order",
					"fail");

		}

		click(btn_releaseCommon);
		explicitWait(header_releasedPO, 50);
		trackCode = getText(lbl_docNumber);
		writeProductionData("PO_Production_Smoke_0002", trackCode, 3);
		writeProductionData("ReleasePOSmoke", getCurrentUrl(), 4);
		if (isDisplayed(header_releasedPO)) {
			writeTestResults("Verify user able to release the Production Order",
					"User should be able to release the Production Order",
					"User successfully release the Production Order", "pass");
		} else {
			writeTestResults("Verify user able to release the Production Order",
					"User should be able to release the Production Order", "User doesn't release the Production Order",
					"fail");

		}

	}

	/* Smoke_Production_0003 */
	public void mrpRun_Smoke_Production_0003() throws Exception {
		autmationTenantLogin();
		explicitWait(btn_navigationPane, 20);
		Thread.sleep(2000);
		explicitWait(btn_navigationPane, 20);
		click(btn_navigationPane);
		explicitWait(btn_productionModule, 10);
		click(btn_productionModule);
		explicitWait(btn_productionControl, 10);
		click(btn_productionControl);
		explicitWait(header_productionControlByPage, 40);
		if (isDisplayed(header_productionControlByPage)) {
			writeTestResults("Verify user can navigate to the Production Control by-page",
					"User should be able to navigate to Production Control by-page",
					"User successfully navigate to Production Control by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to the Production Control by-page",
					"User should be able to navigate to Production Control by-page",
					"User doesn't navigate to Production Control by-page", "fail");

		}

		String chkMRPPath = chk_mrpPODoRepalce.replace("doc_no", readProductionData("PO_Production_Smoke_0002"));
		explicitWait(chkMRPPath, 10);
		if (isDisplayed(chkMRPPath)) {
			writeTestResults("Verify relevent Production Order is available",
					"Relevent Production Order should be available", "Relevent Production Order is available", "pass");
		} else {
			writeTestResults("Verify relevent Production Order is available",
					"Relevent Production Order should be available", "Relevent Production Order doesn't available",
					"fail");

		}
		click(chkMRPPath);

		click(btn_actionMRP);
		explicitWait(btn_runMRP, 10);
		if (isDisplayed(btn_runMRP)) {
			writeTestResults("Verify that Run MRP action is availabe under Actions",
					"Run MRP action should be availabe under Actions", "Run MRP action is availabe under Actions",
					"pass");
		} else {
			writeTestResults("Verify that Run MRP action is availabe under Actions",
					"Run MRP action should be availabe under Actions", "Run MRP action doesn't availabe under Actions",
					"fail");

		}
		click(btn_runMRP);

		explicitWait(header_productionControlWindow, 10);
		if (isDisplayed(header_productionControlWindow)) {
			writeTestResults("Verify that Production Control window is open",
					"Production Control window should be open", "Production Control window suceessfully open", "pass");
		} else {
			writeTestResults("Verify that Production Control window is open",
					"Production Control window should be open", "Production Control window doesn't open", "fail");

		}

		click(btn_runMRPWindow);

		explicitWait(btn_releaseMRP, 10);
		if (isEnabled(btn_releaseMRP)) {
			writeTestResults("Verify that Release button is available", "Release button should be available",
					"Release button successfully available", "pass");
		} else {
			writeTestResults("Verify that Release button is available", "Release button should be available",
					"Release button doesn't available", "fail");

		}

		click(btn_releaseMRP);

		explicitWait(para_MRPReleaseValidation, 10);
		if (isEnabled(para_MRPReleaseValidation)) {
			writeTestResults("Verify that \"Successfully Released.\" validation is available",
					"\"Successfully Released.\" validation should be available",
					"\"Successfully Released.\" validation successfully available", "pass");
		} else {
			writeTestResults("Verify that \"Successfully Released.\" validation is available",
					"\"Successfully Released.\" validation should be available",
					"\"Successfully Released.\" validation doesn't available", "fail");

		}

		Thread.sleep(3000);
		explicitWait(btn_logTabProductionControlWindow, 10);
		click(btn_logTabProductionControlWindow);

		explicitWaitUntillClickable(lbl_genaratedInternalOrderMRP, 10);
		String genaratedIO = getText(lbl_genaratedInternalOrderMRP);

		click(btn_cloaseMRPWindow);

		explicitWait(btn_homeLink, 10);
		click(btn_homeLink);
		explicitWait(btn_taskEvent2, 10);
		click(btn_taskEvent2);
		explicitWait(btn_internalDispatchOrderTaskAndEvent, 10);
		click(btn_internalDispatchOrderTaskAndEvent);
		String extractedIO = genaratedIO.substring(28, (genaratedIO.length() - 1));
		explicitWaitUntillClickable(btn_pendingIDODocRepalce.replace("doc_no", extractedIO), 10);
		Thread.sleep(2000);
		click(btn_pendingIDODocRepalce.replace("doc_no", extractedIO));
		Thread.sleep(2000);
		switchWindow();
		Thread.sleep(1000);
		explicitWait(header_newIDO, 50);
		if (isDisplayed(header_newIDO)) {
			writeTestResults("Verify user can navigate to the new Internal Dispatch Order",
					"User should be able to navigate to new Internal Dispatch Order",
					"User successfully navigate to new Internal Dispatch Order", "pass");
		} else {
			writeTestResults("Verify user can navigate to the new Internal Dispatch Order",
					"User should be able to navigate to new Internal Dispatch Order",
					"User doesn't navigate to new Internal Dispatch Order", "fail");
		}

		sendKeys(txt_shippingAddressIDO, commonAddress);

		click(btn_draft);
		explicitWait(header_draftedIDO, 50);
		if (isDisplayed(header_draftedIDO)) {
			writeTestResults("Verify user able to draft the Internal Dispatch Order",
					"User should be able to draft the Internal Dispatch Order",
					"User successfully draft the Internal Dispatch Order", "pass");
		} else {
			writeTestResults("Verify user able to draft the Internal Dispatch Order",
					"User should be able to draft the Internal Dispatch Order",
					"User doesn't draft the Internal Dispatch Order", "fail");

		}

		click(btn_releaseCommon);
		explicitWait(header_releasedIDO, 50);
		trackCode = getText(lbl_docNumber);
		if (isDisplayed(header_releasedIDO)) {
			writeTestResults("Verify user able to release the Internal Dispatch Order",
					"User should be able to release the Internal Dispatch Order",
					"User successfully release the Internal Dispatch Order", "pass");
		} else {
			writeTestResults("Verify user able to release the Internal Dispatch Order",
					"User should be able to release the Internal Dispatch Order",
					"User doesn't release the Internal Dispatch Order", "fail");

		}
	}

	/* Smoke_Production_0004 */
	public void shopFloorUpdate_Smoke_Production_0004() throws InvalidFormatException, IOException, Exception {
		autmationTenantLogin();
		openPage(readProductionData("ReleasePOSmoke"));
		explicitWaitUntillClickable(btn_action2, 50);
		click(btn_action2);

		explicitWaitUntillClickable(btn_shopFloorUpdate2, 10);
		if (isDisplayed(btn_shopFloorUpdate2)) {
			writeTestResults("Verify that Shop floor update option is available under Actions",
					"Shop floor update option should be available under Actions",
					"Shop floor update option successfully available under Actions", "pass");
		} else {
			writeTestResults("Verify that Shop floor update option is available under Actions",
					"Shop floor update option should be available under Actions",
					"Shop floor update option doesn't available under Actions", "fail");
		}
		click(btn_shopFloorUpdate2);

		explicitWait(header_shopFloorUpdateWindow, 20);
		if (isDisplayed(header_shopFloorUpdateWindow)) {
			writeTestResults("Verify that shop floor update window is open", "Shop floor update window should be open",
					"Shop floor update window succeessfully open", "pass");
		} else {
			writeTestResults("Verify that shop floor update window is open", "Shop floor update window should be open",
					"Shop floor update window doesn't open", "fail");
		}

		/* First Operation */
		System.out.println("First Operation");
		click(btn_startFirstOperationSFU);
		explicitWaitUntillClickable(btn_startSecondLeveleSFU, 10);
		if (isDisplayed(btn_startSecondLeveleSFU)) {
			writeTestResults("Verify that start button is available", "Start button should be available",
					"Start button successfully available", "pass");
		} else {
			writeTestResults("Verify that start button is available", "Start button should be available",
					"Start button doesn't available", "fail");
		}

		click(btn_startSecondLeveleSFU);

		explicitWaitUntillClickable(btn_actualUpdateSFU, 10);
		if (isDisplayed(btn_actualUpdateSFU)) {
			writeTestResults("Verify that actual update option is available on shop floor update main window",
					"Actual update option should be available on shop floor update main window",
					"Actual update option successfully available on shop floor update main window", "pass");
		} else {
			writeTestResults("Verify that actual update option is available on shop floor update main window",
					"Actual update option should be available on shop floor update main window",
					"Actual update option doesn't available on shop floor update main window", "fail");
		}

		click(btn_actualUpdateSFU);
		Thread.sleep(1000);
		sendKeys(txt_outoutQuantityACtualUpdateSFU, actualQuantityOutputProductFirstOperation);
		String outputActualUpdateQuantityFirstOperation = getAttribute(txt_outoutQuantityACtualUpdateSFU, "value");
		if (outputActualUpdateQuantityFirstOperation.equals(actualQuantityOutputProductFirstOperation)) {
			writeTestResults("Verify user able to enter Output Product qty for the first operation",
					"User should be able to enter Output Product qty for the first operation",
					"User successfully enter Output Product qty for the first operation", "pass");
		} else {
			writeTestResults("Verify user able to enter Output Product qty for the first operation",
					"User should be able to enter Output Product qty for the first operation",
					"User doesn't enter Output Product qty for the first operation", "fail");
		}
		Thread.sleep(1000);

		click(btn_upateActualSFU);
		explicitWait(para_successfullValidationOutProductActualUpdate, 8);
		if (isDisplayed(para_successfullValidationOutProductActualUpdate)) {
			writeTestResults("Verify that successfully updated validation is appear for the Output Product update",
					"Successfully updated validation should appear for the Output Product update",
					"Successfully updated validation successfully appear for the Output Product update", "pass");
		} else {
			writeTestResults("Verify that successfully updated validation is appear for the Output Product update",
					"Successfully updated validation should appear for the Output Product update",
					"Successfully updated validation doesn't appear for the Output Product update", "fail");
		}
		Thread.sleep(3000);

		click(btn_tapInputProductsShopFloorUpdate);

		sendKeys(txt_actualUpdaInputProductQuantityFirstOperation, actualQuantityInputProductFirstOperation);
		String inputActualUpdateQuantityFirstOperation = getAttribute(txt_actualUpdaInputProductQuantityFirstOperation,
				"value");
		if (inputActualUpdateQuantityFirstOperation.equals(actualQuantityInputProductFirstOperation)) {
			writeTestResults("Verify user able to enter Input Product qty for the first operation",
					"User should be able to enter Input Product qty for the first operation",
					"User successfully enter Input Product qty for the first operation", "pass");
		} else {
			writeTestResults("Verify user able to enter Input Product qty for the first operation",
					"User should be able to enter Input Product qty for the first operation",
					"User doesn't enter Input Product qty for the first operation", "fail");
		}

		click(btn_upateActualSFU);
		explicitWait(para_successfullValidationActualUpdate, 8);
		if (isDisplayed(para_successfullValidationActualUpdate)) {
			writeTestResults("Verify that successfully updated validation is appear for the Input Product update",
					"Successfully updated validation should appear for the Input Product update",
					"Successfully updated validation successfully appear for the Input Product update", "pass");
		} else {
			writeTestResults("Verify that successfully updated validation is appear for the Input Product update",
					"Successfully updated validation should appear for the Input Product update",
					"Successfully updated validation doesn't appear for the Input Product update", "fail");
		}
		Thread.sleep(3000);

		click(btn_closeFisrstOperationActualUpdateWindow);

		explicitWait(btn_completeFirstOperation, 8);
		if (isDisplayed(btn_completeFirstOperation)) {
			writeTestResults("Verify that complete button is available for the first operation",
					"Complete button should be available for the first operation",
					"Complete button successfully available for the first operation", "pass");
		} else {
			writeTestResults("Verify that complete button is available for the first operation",
					"Complete button should be available for the first operation",
					"Complete button doesn't available for the first operation", "fail");
		}
		click(btn_completeFirstOperation);

		explicitWait(btn_completeSecondLeveleSFU, 8);
		if (isDisplayed(btn_completeSecondLeveleSFU)) {
			writeTestResults("Verify that complete button is available for the complete window of first operation",
					"Complete button should be available for the complete window of first operation",
					"Complete button successfully available for the complete window of first operation", "pass");
		} else {
			writeTestResults("Verify that complete button is available for the complete window of first operation",
					"Complete button should be available for the complete window of first operation",
					"Complete button doesn't available for the complete window of first operation", "fail");
		}
		click(btn_completeSecondLeveleSFU);

		explicitWait(lbl_completedShopFloorUpdateBarFirstOperation, 8);
		if (isDisplayed(lbl_completedShopFloorUpdateBarFirstOperation)) {
			writeTestResults("Verify that shop flow update is completed for the first operation",
					"Shop flow update should be completed for the first operation",
					"Shop flow update successfully completed for the first operation", "pass");
		} else {
			writeTestResults("Verify that shop flow update is completed for the first operation",
					"Shop flow update should be completed for the first operation",
					"Shop flow update doesn't completed for the first operation", "pass");
		}

		/* Second Operation */
		System.out.println("Second Operation");

		explicitWaitUntillClickable(btn_actualUpdateSFU, 10);
		if (isDisplayed(btn_actualUpdateSFU)) {
			writeTestResults(
					"Verify that actual update option is available on shop floor update main window for second operation",
					"Actual update option should be available on shop floor update main window for second operation",
					"Actual update option successfully available on shop floor update main window for second operation",
					"pass");
		} else {
			writeTestResults(
					"Verify that actual update option is available on shop floor update main window for second operation",
					"Actual update option should be available on shop floor update main window for second operation",
					"Actual update option doesn't available on shop floor update main window for second operation",
					"fail");
		}

		click(btn_actualUpdateSFU);
		Thread.sleep(1000);
		sendKeys(txt_outoutQuantityACtualUpdateSFU, actualQuantityOutputProductSecondOperation);
		String outputActualUpdateQuantitySecondOperation = getAttribute(txt_outoutQuantityACtualUpdateSFU, "value");
		if (outputActualUpdateQuantitySecondOperation.equals(actualQuantityOutputProductSecondOperation)) {
			writeTestResults("Verify user able to enter Output Product qty for the second operation",
					"User should be able to enter Output Product qty for the second operation",
					"User successfully enter Output Product qty for the second operation", "pass");
		} else {
			writeTestResults("Verify user able to enter Output Product qty for the second operation",
					"User should be able to enter Output Product qty for the second operation",
					"User doesn't enter Output Product qty for the second operation", "fail");
		}
		Thread.sleep(1000);
		
		click(btn_upateActualSFU);
		explicitWait(para_successfullValidationOutProductActualUpdate, 8);
		if (isDisplayed(para_successfullValidationOutProductActualUpdate)) {
			writeTestResults("Verify that successfully updated validation is appear for the Output Product update",
					"Successfully updated validation should appear for the Output Product update",
					"Successfully updated validation successfully appear for the Output Product update", "pass");
		} else {
			writeTestResults("Verify that successfully updated validation is appear for the Output Product update",
					"Successfully updated validation should appear for the Output Product update",
					"Successfully updated validation doesn't appear for the Output Product update", "fail");
		}
		Thread.sleep(3000);

		click(btn_tapInputProductsShopFloorUpdate);

		sendKeys(txt_actualUpdaInputProductQuantitySecondOperation, actualQuantityInputProductSecondOperation);
		String inputActualUpdateQuantitySecondOperation = getAttribute(
				txt_actualUpdaInputProductQuantitySecondOperation, "value");
		if (inputActualUpdateQuantitySecondOperation.equals(actualQuantityInputProductSecondOperation)) {
			writeTestResults("Verify user able to enter Input Product qty for the second operation",
					"User should be able to enter Input Product qty for the second operation",
					"User successfully enter Input Product qty for the second operation", "pass");
		} else {
			writeTestResults("Verify user able to enter Input Product qty for the second operation",
					"User should be able to enter Input Product qty for the second operation",
					"User doesn't enter Input Product qty for the second operation", "fail");
		}

		click(btn_upateActualSFU);
		explicitWait(para_successfullValidationActualUpdate, 8);
		if (isDisplayed(para_successfullValidationActualUpdate)) {
			writeTestResults("Verify that successfully updated validation is appear for the Input Product update",
					"Successfully updated validation should appear for the Input Product update",
					"Successfully updated validation successfully appear for the Input Product update", "pass");
		} else {
			writeTestResults("Verify that successfully updated validation is appear for the Input Product update",
					"Successfully updated validation should appear for the Input Product update",
					"Successfully updated validation doesn't appear for the Input Product update", "fail");
		}
		Thread.sleep(3000);

		click(btn_closeSecondOperationActualUpdateWindow);

		explicitWait(btn_completeSecondOperation, 8);
		if (isDisplayed(btn_completeSecondOperation)) {
			writeTestResults("Verify that complete button is available for the second operation",
					"Complete button should be available for the second operation",
					"Complete button successfully available for the second operation", "pass");
		} else {
			writeTestResults("Verify that complete button is available for the second operation",
					"Complete button should be available for the second operation",
					"Complete button doesn't available for the second operation", "fail");
		}
		click(btn_completeSecondOperation);

		explicitWait(btn_completeSecondLeveleSFU, 8);
		if (isDisplayed(btn_completeSecondLeveleSFU)) {
			writeTestResults("Verify that complete button is available for the complete window of second operation",
					"Complete button should be available for the complete window of second operation",
					"Complete button successfully available for the complete window of second operation", "pass");
		} else {
			writeTestResults("Verify that complete button is available for the complete window of second operation",
					"Complete button should be available for the complete window of second operation",
					"Complete button doesn't available for the complete window of second operation", "fail");
		}
		click(btn_completeSecondLeveleSFU);

		explicitWait(lbl_completedShopFloorUpdateBarSecondOperation, 8);
		if (isDisplayed(lbl_completedShopFloorUpdateBarSecondOperation)) {
			writeTestResults("Verify that shop flow update is completed for the second operation",
					"Shop flow update should be completed for the second operation",
					"Shop flow update successfully completed for the second operation", "pass");
		} else {
			writeTestResults("Verify that shop flow update is completed for the second operation",
					"Shop flow update should be completed for the second operation",
					"Shop flow update doesn't completed for the second operation", "pass");
		}
	}

	/* Smoke_Production_0005 */
	public void qcProduction_Smoke_Production_0005() throws Exception {
		autmationTenantLogin();
		explicitWait(btn_navigationPane, 20);
		Thread.sleep(2000);
		explicitWait(btn_navigationPane, 20);
		click(btn_navigationPane);
		explicitWait(btn_productionModule, 10);
		click(btn_productionModule);
		explicitWait(btn_productionQCAndSorting, 10);
		if (isDisplayed(btn_productionQCAndSorting)) {
			writeTestResults("Verif that Production QC & Sorting option is display under Production Module",
					"Production QC & Sorting option should be display under Production Module",
					"Production QC & Sorting option successfully display under Production Module", "pass");
		} else {
			writeTestResults("Verif that Production QC & Sorting option is display under Production Module",
					"Production QC & Sorting option should be display under Production Module",
					"Production QC & Sorting option doesn't display under Production Module", "fail");
		}

		click(btn_productionQCAndSorting);

		explicitWait(drop_documentTypeProductionQC, 50);
		selectText(drop_documentTypeProductionQC, qcTypeProductionOrder);
		Thread.sleep(1000);
		String selecteDocType = getSelectedOptionInDropdown(drop_documentTypeProductionQC);
		if (selecteDocType.equals(qcTypeProductionOrder)) {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User able to select 'Production Order' as document type", "pass");
		} else {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User not able to select 'Production Order' as document type", "fail");

		}

		sendKeys(txt_docNumberQC, readProductionData("PO_Production_Smoke_0002"));
		String enteredPO = getAttribute(txt_docNumberQC, "value");
		if (enteredPO.equals(readProductionData("PO_Production_Smoke_0002"))) {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User successfully enter Production Order document number", "pass");
		} else {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User doesn't enter Production Order document number", "fail");
		}

		Thread.sleep(1000);
		driver.findElement(getLocator(txt_docNumberQC)).sendKeys(Keys.ARROW_DOWN);
		pressEnter(txt_docNumberQC);

		explicitWait(div_selectedQcDocProduction.replace("doc_no", readProductionData("PO_Production_Smoke_0002")), 6);
		if (isDisplayed(
				div_selectedQcDocProduction.replace("doc_no", readProductionData("PO_Production_Smoke_0002")))) {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User successfully select the relevent Production Order", "pass");
		} else {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User doesn't select the relevent Production Order", "fail");
		}

		mouseMove(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				getInvObj().readTestCreation("BatchFifoProductionSmokeProductionType")));
		click(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				getInvObj().readTestCreation("BatchFifoProductionSmokeProductionType")));

		if (isSelected(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				getInvObj().readTestCreation("BatchFifoProductionSmokeProductionType")))) {
			writeTestResults("Verify user able to select the relevent Output Product",
					"User should be able to select the relevent Output Product",
					"User successfully select the relevent Output Product", "pass");
		} else {
			writeTestResults("Verify user able to select the relevent Output Product",
					"User should be able to select the relevent Output Product",
					"User doesn't select the relevent Output Product", "fail");
		}

		sendKeys(txt_passQuantityProductionQCOneRow, passQuantityProductionSmoke);
		String enteredPassQty = getAttribute(txt_passQuantityProductionQCOneRow, "value");
		if (enteredPassQty.equals(passQuantityProductionSmoke)) {
			writeTestResults("Verify user able to enter pass qty", "User should be able to enter pass qty",
					"User successfully enter pass qty", "pass");
		} else {
			writeTestResults("Verify user able to enter pass qty", "User should be able to enter pass qty",
					"User doesn't enter pass qty", "fail");
		}

		Thread.sleep(1500);
		sendKeys(txt_failQuantityProductionQCOneRow, failQuantityProductionSmoke);
		String enteredFailQty = getAttribute(txt_failQuantityProductionQCOneRow, "value");
		if (enteredFailQty.equals(failQuantityProductionSmoke)) {
			writeTestResults("Verify user able to enter fail qty", "User should be able to enter fail qty",
					"User successfully enter fail qty", "pass");
		} else {
			writeTestResults("Verify user able to enter fail qty", "User should be able to enter fail qty",
					"User doesn't enter fail qty", "fail");
		}

		click(btn_releseProductionQC);
		explicitWait(para_successfullValidationProductionQC, 7);

		if (isDisplayed(para_successfullValidationProductionQC)) {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation is appear", "pass");
		} else {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation doesn't appear", "fail");
		}
	}

	/* Smoke_Production_0006 */
	public void productionCosting_Smoke_Production_0006() throws Exception {
		autmationTenantLogin();
		openPage(readProductionData("ReleasePOSmoke"));
		explicitWaitUntillClickable(btn_action2, 50);
		click(btn_action2);

		explicitWaitUntillClickable(btn_productionCostingActions, 10);
		if (isDisplayed(btn_productionCostingActions)) {
			writeTestResults("Verify that Production Costing option is available under Actions",
					"Production Costing option should be available under Actions",
					"Production Costing option successfully available under Actions", "pass");
		} else {
			writeTestResults("Verify that Production Costing option is available under Actions",
					"Production Costing option should be available under Actions",
					"Production Costing option doesn't available under Actions", "fail");
		}
		click(btn_productionCostingActions);

		explicitWait(header_productionCostingWindow, 20);
		if (isDisplayed(header_productionCostingWindow)) {
			writeTestResults("Verify that production costing window is open",
					"Production costing window should be open", "Production costing window succeessfully open", "pass");
		} else {
			writeTestResults("Verify that production costing window is open",
					"Production costing window should be open", "Production costing window doesn't open", "fail");
		}

		click(btn_actualUpdateProductionCosting);

		explicitWait(tab_inputProductsProductionCosting, 20);

		click(tab_inputProductsProductionCosting);

		explicitWait(txt_actualUpdateProductionCosting, 10);

		sendKeys(txt_actualUpdateProductionCosting, actualUpdateQuantityProductionQuntity);
		String enteredActualUpdateQuantity = getAttribute(txt_actualUpdateProductionCosting, "value");
		if (enteredActualUpdateQuantity.equals(actualUpdateQuantityProductionQuntity)) {
			writeTestResults("Verify user able to enter actual update quantity",
					"User should be able to enter actual update quantity",
					"User successfully enter actual update quantity", "pass");
		} else {
			writeTestResults("Verify user able to enter actual update quantity",
					"User should be able to enter actual update quantity", "User doesn't enter actual update quantity",
					"fail");
		}

		click(btn_updateProductionCosting);
		explicitWait(para_successfullValidationActualUpdateProductionCosting, 10);
		if (isDisplayed(para_successfullValidationActualUpdateProductionCosting)) {
			writeTestResults("Verify that succesfull validation is appear for Input Products",
					"Succesfull validation should appear for Input Products",
					"Succesfull validation is appear for Input Products", "pass");
		} else {
			writeTestResults("Verify that succesfull validation is appear for Input Products",
					"Succesfull validation should appear for Input Products",
					"Succesfull validation doesn't appear for Input Products", "fail");
		}

		explicitWait(btn_closeSecondWindowCostingUpdate, 5);
		Thread.sleep(2500);
		click(btn_closeSecondWindowCostingUpdate);
		explicitWait(chk_pendingProductionCosting, 10);
		click(chk_pendingProductionCosting);
		if (isDisplayed(chk_pendingProductionCostingSelected)) {
			writeTestResults("Verify user able to select batch costing checkbox on production costing main window",
					"User should be able to select batch costing checkbox on production costing main window",
					"User able to select batch costing checkbox on production costing main window", "pass");
		} else {
			writeTestResults("Verify user able to select batch costing checkbox on production costing main window",
					"User should be able to select batch costing checkbox on production costing main window",
					"User not able to select batch costing checkbox on production costing main window", "fail");
		}

		click(btn_updateProductionCostingMainWindow);
		explicitWait(para_batchCostingUpdateValidation, 10);
		if (isDisplayed(para_batchCostingUpdateValidation)) {
			writeTestResults("Verify that succesfull validation is appear for Batch Costing",
					"Succesfull validation should appear for Batch Costing",
					"Succesfull validation is appear for Batch Costing", "pass");
		} else {
			writeTestResults("Verify that succesfull validation is appear for Batch Costing",
					"Succesfull validation should appear for Batch Costing",
					"Succesfull validation doesn't appear for Batch Costing", "fail");
		}

		explicitWait(btn_closeMainCostingUpdateWindowProductionOrder, 5);
		Thread.sleep(2500);
		click(btn_closeMainCostingUpdateWindowProductionOrder);

		explicitWait(header_completedPO, 10);
		if (isDisplayed(header_completedPO)) {
			writeTestResults("Verify that header is changed as Completed", "Header should be change as Completed",
					"Header successfully changed as Completed", "pass");
		} else {
			writeTestResults("Verify that header is changed as Completed", "Header should be change as Completed",
					"Header doesn't changed as Completed", "fail");
		}

	}

	/* Smoke_Production_0007_008 */
	public void internalReciptAndJournelEntry_Smoke_Production_0007_0008() throws Exception {
		autmationTenantLogin();
		openPage(readProductionData("ReleasePOSmoke"));
		explicitWaitUntillClickable(btn_action2, 50);
		click(btn_action2);

		explicitWaitUntillClickable(btn_convertToInternalReceipt, 10);
		if (isDisplayed(btn_convertToInternalReceipt)) {
			writeTestResults("Verify that Convert to Internel Recipt option is available under Actions",
					"Convert to Internel Recipt option should be available under Actions",
					"Convert to Internel Recipt option successfully available under Actions", "pass");
		} else {
			writeTestResults("Verify that Convert to Internel Recipt option is available under Actions",
					"Convert to Internel Recipt option should be available under Actions",
					"Convert to Internel Recipt option doesn't available under Actions", "fail");
		}
		click(btn_convertToInternalReceipt);

		explicitWait(header_genarateInternalReciptSmoke, 20);
		if (isDisplayed(header_genarateInternalReciptSmoke)) {
			writeTestResults("Verify that genarate internel receipt window is open",
					"genarate internel receipt window should be open",
					"genarate internel receipt window succeessfully open", "pass");
		} else {
			writeTestResults("Verify that genarate internel receipt window is open",
					"genarate internel receipt window should be open", "genarate internel receipt window doesn't open",
					"fail");
		}

		click(chk_seletcOroductForInternalRecipt);

		if (isSelected(chk_seletcOroductForInternalRecipt)) {
			writeTestResults("Verify user able to select the product line",
					"User should be able to select the product line", "User successfully select the product line",
					"pass");
		} else {
			writeTestResults("Verify user able to select the product line",
					"User should be able to select the product line", "User doesn't select the product line", "fail");
		}

		click(btn_genarateInternalRecipt);
		explicitWait(para_validationInternalREciptGenarateSuccessfull, 10);
		if (isDisplayed(para_validationInternalREciptGenarateSuccessfull)) {
			writeTestResults("Verify that succesfull validation is appear for Internal Receipt Genaration",
					"Succesfull validation should appear for Internal Receipt Genaration",
					"Succesfull validation is appear for Internal Receipt Genaration", "pass");
		} else {
			writeTestResults("Verify that succesfull validation is appear for Internal Receipt Genaration",
					"Succesfull validation should appear for Internal Receipt Genaration",
					"Succesfull validation doesn't appear for Internal Receipt Genaration", "fail");
		}

		Thread.sleep(3000);
		explicitWaitUntillClickable(lnk_genaratedInternalREciptSmoke, 15);
		Thread.sleep(1000);
		click(lnk_genaratedInternalREciptSmoke);
		Thread.sleep(2000);
		switchWindow();
		explicitWait(header_releasedInternalRecipt, 10);
		trackCode = getText(lbl_docNumber);
		if (isDisplayed(header_releasedInternalRecipt)) {
			writeTestResults("Verify that Internal Receiptis open as released status",
					"Internal Receipt should be open as released status",
					"Internal Receipt successfully open as released status", "pass");
		} else {
			writeTestResults("Verify that Internal Receiptis open as released status",
					"Internal Receipt should be open as released status",
					"Internal Receipt doesn't open or not in released status", "fail");
		}

		click(btn_action2);

		explicitWaitUntillClickable(btn_journal, 10);
		if (isDisplayed(btn_journal)) {
			writeTestResults("Verify that Journal Entry option is available under Actions",
					"Journal Entry option should be available under Actions",
					"Journal Entry option successfully available under Actions", "pass");
		} else {
			writeTestResults("Verify that Journal Entry option is available under Actions",
					"Journal Entry option should be available under Actions",
					"Journal Entry option doesn't available under Actions", "fail");
		}
		click(btn_journal);

		explicitWait(header_journalEntryPopup, 10);
		if (isDisplayed(header_journalEntryPopup)) {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded",
					"Journal Entry details pop up successfully get loaded", "pass");
		} else {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded", "Journal Entry details pop up doesn't get loaded",
					"fail");
		}

		String debitAmount = getText(lbl_debitValueJournelEntrySmoke);

		if (debitAmount.equals("1300.000000")) {
			writeTestResults("Verify that amount was debited to the relevent GL Account",
					"Amount should be debited to the relevent GL Account",
					"Amount successfully debited to the relevent GL Account", "pass");
		} else {
			writeTestResults("Verify that amount was debited to the relevent GL Account",
					"Amount should be debited to the relevent GL Account",
					"Amount doesn't debited to the relevent GL Account", "fail");
		}

		String creditAmount = getText(lbl_creditValueJournelEntrySmoke);
		if (creditAmount.equals("1300.000000")) {
			writeTestResults("Verify that amount was credited to the relevent GL Account",
					"Amount should be credited to the relevent GL Account",
					"Amount successfully credited to the relevent GL Account", "pass");
		} else {
			writeTestResults("Verify that amount was credited to the relevent GL Account",
					"Amount should be credited to the relevent GL Account",
					"Amount doesn't credited to the relevent GL Account", "fail");
		}

		String debitTotal = getText(lbl_debitTotalJournelEntrySingleEntry);
		String credTotal = getText(lbl_creditTotalJournelEntrySingleEntry);

		if (debitTotal.equals("1300.000000") && debitTotal.equals(credTotal)) {
			writeTestResults("Verify total debit and credit amounts were balanced",
					"Total debit and credit amounts should balanced",
					"Total debit and credit amounts successfully balanced", "pass");
		} else {
			writeTestResults("Verify total debit and credit amounts were balanced",
					"Total debit and credit amounts should balanced", "Total debit and credit amounts weren't balanced",
					"fail");
		}
	}
}
