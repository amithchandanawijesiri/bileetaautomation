package bileeta.BTAF.PageObjects;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.xmlbeans.impl.regex.REUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import bileeta.BATF.Pages.ProjectModuleData;

public class ProjectModuleBusinessScenarios extends ProjectModuleData{
	
	InventoryAndWarehouseModule pro4=new InventoryAndWarehouseModule();
	
	public void navigateToTheLoginPage() throws Exception {
		openPage(siteURL);

		writeTestResults("Verify that 'Entution' header is available on the page",
				"user should be able to see the logo", "logo is dislayed", "pass");

	}
	
	//=============================================Common
	
	public void verifyTheLogo() throws Exception {
		
		WaitElement(siteLogo);
		if (isDisplayed(siteLogo)) {
			writeTestResults("Verify that 'Entution' header is available on the page","user should be able to see the logo", "logo is dislayed", "pass");
		} else {
			writeTestResults("Verify that 'Entution' header is available on the page","user should be able to see the logo", "logo is not dislayed", "fail");

		}
	}
	
	public void userLogin() throws Exception {
		
		WaitElement(txt_username);
		sendKeys(txt_username, userNameData);
		WaitElement(txt_password);
		sendKeys(txt_password, passwordData);
		WaitClick(btn_login);
		click(btn_login);

		WaitElement(lnk_home);
		if (isDisplayed(lnk_home)) {

			writeTestResults("User login", "User should navigate to the Home page of Entution",
					"user navigated to the home page", "pass");
		}

		else {
			writeTestResults("User login", "User should navigate to the Home page of Entution",
					"user cant navigate to the home page", "fail");

		}
	}
	
	public void clickNavigation() throws Exception {
		
		WaitClick(btn_nav);
		click(btn_nav);
		
		WaitElement(sideMenu);
		if(isDisplayed(sideMenu)) {
			writeTestResults("verify sidemenu","view sidemenu", "sidemenu is display", "pass");
		}else {
			writeTestResults("verify sidemenu","view sidemenu", "sidemenu is not display", "fail");
		} 
	}
	
public void clickProjectbutton() throws Exception {
		
		WaitClick(btn_projbutton);
		click(btn_projbutton);
		if(isDisplayed(subsideMenu)) {
			writeTestResults("verify subsideMenu","view subsideMenu", "subsideMenu is display", "pass");
		}else {
			writeTestResults("verify subsideMenu","view subsideMenu", "subsideMenu is not display", "fail");
		}
	}

public void searchProject() throws Exception {
	
	WaitClick(btn_projectbutton);
	click(btn_projectbutton);
	
	WaitElement(projectpage);
	if(isDisplayed(projectpage)) {
		writeTestResults("verify project page","view project page", "project page is display", "pass");
	}else {
		writeTestResults("verify project page","view project page", "project page is not display", "fail");
	}
	
	WaitElement(txt_projectcodeserch);
	sendKeys(txt_projectcodeserch, ReadData(1, 1));
	pressEnter(txt_projectcodeserch);
	WaitElement(sel_projectcodeserchsel.replace("ProCode", ReadData(1, 1)));
	doubleClick(sel_projectcodeserchsel.replace("ProCode", ReadData(1, 1)));
}

//============================================= Create a project / Add tasks / Actual Update

//Project_TC_001
public void createProject() throws Exception {
	
	WaitClick(btn_projectbutton);
	click(btn_projectbutton);
	
	WaitElement(projectpage);
	if(isDisplayed(projectpage)) {
		writeTestResults("verify project page","view project page", "project page is display", "pass");
	}else {
		writeTestResults("verify project page","view project page", "project page is not display", "fail");
	}
	
	WaitClick(btn_newprojectbutton);
	click(btn_newprojectbutton);
	
	WaitElement(newprojectpage);
	if(isDisplayed(newprojectpage)) {
		writeTestResults("verify new project page","view new project page", "new project page is display", "pass");
	}else {
		writeTestResults("verify new project page","view new project page", "new project page is not display", "fail");
	}
	
	WaitElement(txt_ProjectCode);
	LocalTime myObj = LocalTime.now();
	sendKeys(txt_ProjectCode, ProjectCode+myObj);
	
	WaitElement(txt_Title);
	sendKeys(txt_Title, Title+myObj);
	
	WaitElement(txt_ProjectGroup);
	selectIndex(txt_ProjectGroup, 1);
	
	WaitClick(btn_ResponsiblePersonbutton);
	click(btn_ResponsiblePersonbutton);
	WaitElement(txt_ResponsiblePersontxt);
	sendKeys(txt_ResponsiblePersontxt, ResponsiblePersontxt);
	pressEnter(txt_ResponsiblePersontxt);
	WaitElement(sel_ResponsiblePersonsel.replace("ResName", ResponsiblePersontxt));
	doubleClick(sel_ResponsiblePersonsel.replace("ResName", ResponsiblePersontxt));
	
	WaitElement(txt_AssignedBusinessUnits);
	selectIndex(txt_AssignedBusinessUnits, 1);
	
	WaitClick(btn_PricingProfilebutton);
	click(btn_PricingProfilebutton);
	WaitElement(txt_PricingProfiletxt);
	sendKeys(txt_PricingProfiletxt,PricingProfiletxt);
	pressEnter(txt_PricingProfiletxt);
	WaitElement(sel_PricingProfilesel.replace("PriProName", PricingProfiletxt)); 
	doubleClick(sel_PricingProfilesel.replace("PriProName", PricingProfiletxt));
	
	ScrollDown();
	
	WaitClick(btn_CustomerAccountbutton);
	click(btn_CustomerAccountbutton);
	WaitElement(txt_CustomerAccounttxt);
	sendKeys(txt_CustomerAccounttxt, CustomerAccounttxt);
	pressEnter(txt_CustomerAccounttxt);
	WaitElement(sel_CustomerAccountsel.replace("CusAcc", CustomerAccounttxt));
	doubleClick(sel_CustomerAccountsel.replace("CusAcc", CustomerAccounttxt));
	
	WaitClick(txt_RequestedStartDate);
	click(txt_RequestedStartDate);
	WaitElement(sel_RequestedStartDatesel.replace("Date", "25"));
	click(sel_RequestedStartDatesel.replace("Date", "25"));
	
	WaitClick(btn_ProjectLocationbutton);
	click(btn_ProjectLocationbutton);
	WaitElement(txt_ProjectLocationtxt);
	sendKeys(txt_ProjectLocationtxt, ProjectLocationtxt);
	pressEnter(txt_ProjectLocationtxt);
	WaitElement(sel_ProjectLocationsel.replace("ProLoc", ProjectLocationtxt));
	doubleClick(sel_ProjectLocationsel.replace("ProLoc", ProjectLocationtxt));
	
	WaitClick(btn_draft);
	click(btn_draft);
	WaitElement(pageDraft);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Project Document Draft","Project Document Draft", "Project Document is Draft", "pass");
	} else {
		writeTestResults("verify Project Document Draft","Project Document Draft", "Project Document is not Draft", "fail");

	}
	
	WaitClick(btn_release);
	click(btn_release);
	
	Thread.sleep(4000);
	trackCode= getText(Header);
	WriteData(1, 1, trackCode);
	
	WaitElement(pageRelease);
	if (isDisplayed(pageRelease)){

		writeTestResults("verify Project Document Released","Project Document Released", "Project Document is Released", "pass");
	} else {
		writeTestResults("verify Project Document Released","Project Document Released", "Project Document is not Released", "fail");

	}
}

//Project_TC_002
public void addingAprojectTask() throws Exception {
	
	searchProject();
	
	WaitClick(btn_edit);
	click(btn_edit);
	WaitClick(btn_WorkBreakdown);
	click(btn_WorkBreakdown);
	
	WaitClick(btn_AddTask);
	click(btn_AddTask);
	WaitElement(txt_TaskName);
	LocalTime myObj = LocalTime.now();
	sendKeys(txt_TaskName,TaskName+myObj);
	WaitElement(txt_TaskType);
	selectIndex(txt_TaskType, 2);
	
	//Input
	
	WaitClick(btn_InputProducts);
	click(btn_InputProducts);
	
	WaitClick(btn_productbutton.replace("raw", "1"));
	click(btn_productbutton.replace("raw", "1"));
	sendKeysLookup(txt_producttxt, pro4.readTestCreation("BatchSpecific"));
	pressEnter(txt_producttxt);
	WaitElement(sel_productsel.replace("InputPro", pro4.readTestCreation("BatchSpecific")));
	doubleClick(sel_productsel.replace("InputPro", pro4.readTestCreation("BatchSpecific")));
	WaitElement(txt_EstimatedQty.replace("raw", "1"));
	sendKeys(txt_EstimatedQty.replace("raw", "1"), EstimatedQty);
	WaitClick(btn_AddProPlusBtn);
	click(btn_AddProPlusBtn);
	
	WaitClick(btn_productbutton.replace("raw", "2"));
	click(btn_productbutton.replace("raw", "2"));
	sendKeysLookup(txt_producttxt, pro4.readTestCreation("BatchFifo"));
	pressEnter(txt_producttxt);
	WaitElement(sel_productsel.replace("InputPro", pro4.readTestCreation("BatchFifo")));
	doubleClick(sel_productsel.replace("InputPro", pro4.readTestCreation("BatchFifo")));
	WaitElement(txt_EstimatedQty.replace("raw", "2"));
	sendKeys(txt_EstimatedQty.replace("raw", "2"), EstimatedQty);
	WaitClick(btn_AddProPlusBtn);
	click(btn_AddProPlusBtn);
	
	WaitClick(btn_productbutton.replace("raw", "3"));
	click(btn_productbutton.replace("raw", "3"));
	sendKeysLookup(txt_producttxt, pro4.readTestCreation("Lot"));
	pressEnter(txt_producttxt);
	WaitElement(sel_productsel.replace("InputPro", pro4.readTestCreation("Lot")));
	doubleClick(sel_productsel.replace("InputPro", pro4.readTestCreation("Lot")));
	WaitElement(txt_EstimatedQty.replace("raw", "3"));
	sendKeys(txt_EstimatedQty.replace("raw", "3"), EstimatedQty);
	WaitClick(btn_AddProPlusBtn);
	click(btn_AddProPlusBtn);
	
	WaitClick(btn_productbutton.replace("raw", "4"));
	click(btn_productbutton.replace("raw", "4"));
	sendKeysLookup(txt_producttxt, pro4.readTestCreation("SerielSpecific"));
	pressEnter(txt_producttxt);
	WaitElement(sel_productsel.replace("InputPro", pro4.readTestCreation("SerielSpecific")));
	doubleClick(sel_productsel.replace("InputPro", pro4.readTestCreation("SerielSpecific")));
	WaitElement(txt_EstimatedQty.replace("raw", "4"));
	sendKeys(txt_EstimatedQty.replace("raw", "4"), EstimatedQty);
	
	JavascriptExecutor js1 = (JavascriptExecutor)driver;
	js1.executeScript("$(\"#tblSADProduct\").children().find(\".unchecked\").click()");
	
	//Labour
	
	WaitClick(btn_Labour);
	click(btn_Labour);
	WaitClick(btn_ServiceProductbutton);
	click(btn_ServiceProductbutton);
	sendKeysLookup(txt_ServiceProducttxt, ServiceProducttxt);
	pressEnter(txt_ServiceProducttxt);
	WaitElement(sel_ServiceProductsel.replace("ServicePro", ServiceProducttxt));
	doubleClick(sel_ServiceProductsel.replace("ServicePro", ServiceProducttxt));
	
	WaitClick(txt_EstimatedWork);
	click(txt_EstimatedWork);
	WaitClick(sel_EstimatedWorksel);
	click(sel_EstimatedWorksel);
	WaitClick(btn_EstimatedWorkhourok);
	click(btn_EstimatedWorkhourok);
	js1.executeScript("$(\"#tblSADService\").children().find(\".unchecked\").click()");
	
	//Resource
	
	WaitClick(btn_Resource);
	click(btn_Resource);
	WaitClick(btn_Resourcebutton);
	click(btn_Resourcebutton);
	sendKeysLookup(txt_Resourcetxt, Resourcetxt);
	pressEnter(txt_Resourcetxt);
	WaitElement(sel_Resourcesel.replace("ResourcePro", Resourcetxt));
	doubleClick(sel_Resourcesel.replace("ResourcePro", Resourcetxt));
	
	WaitClick(btn_ResourceServiceProductbutton);
	click(btn_ResourceServiceProductbutton);
	sendKeysLookup(txt_ServiceProducttxt, ServiceProducttxt);
	pressEnter(txt_ServiceProducttxt);
	Thread.sleep(5000);
	doubleClick(sel_ServiceProductsel.replace("ServicePro", ServiceProducttxt));
	
	WaitClick(txt_ResourceEstimatedWork);
	click(txt_ResourceEstimatedWork);
	click(sel_ResourceEstimatedWorksel);
	js1.executeScript("$(\"#tblSADResource\").children().find(\".unchecked\").click()");
	
	//Expence
	
	WaitClick(btn_Expense);
	click(btn_Expense);
	WaitElement(txt_AnalysisCode);
	selectIndex(txt_AnalysisCode, 2);
	WaitClick(btn_ExpenseServiceProductbutton);
	click(btn_ExpenseServiceProductbutton);
	sendKeysLookup(txt_ServiceProducttxt, ServiceProducttxt);
	pressEnter(txt_ServiceProducttxt);
	Thread.sleep(5000);
	doubleClick(sel_ServiceProductsel.replace("ServicePro", ServiceProducttxt));
	
	WaitElement(txt_EstimatedCost);
	sendKeys(txt_EstimatedCost, EstimatedCost);
	js1.executeScript("$(\"#tblSADExpence\").children().find(\".unchecked\").click()");
	
	WaitClick(btn_TaskUpdate);
	click(btn_TaskUpdate);
	
	pageRefersh();
	Thread.sleep(3000);
	WaitClick(btn_WorkBreakdown);
	click(btn_WorkBreakdown);
	WaitClick(btn_releaseTaskdropdown);
	click(btn_releaseTaskdropdown);
	WaitClick(btn_releaseTask);
	click(btn_releaseTask);
	
	pageRefersh();
	Thread.sleep(3000);
	WaitClick(btn_update);
	click(btn_update);
	
	WaitClick(btn_WorkBreakdown);
	click(btn_WorkBreakdown);
	WaitClick(btn_releaseTaskdropdown);
	click(btn_releaseTaskdropdown);
	
	WaitElement(txt_TaskReleaseTxt.replace("TaskName", TaskName+myObj));
	if (isDisplayed(txt_TaskReleaseTxt.replace("TaskName", TaskName+myObj))){

		writeTestResults("Verify the ability of adding a project task with input products, labour, resource, expense",
				"User can adding a project task with input products, labour, resource, expense", 
				"User can adding a project task with input products, labour, resource, expense Successfully", "pass");
	} else {
		writeTestResults("Verify the ability of adding a project task with input products, labour, resource, expense",
				"User can adding a project task with input products, labour, resource, expense", 
				"User can adding a project task with input products, labour, resource, expense Fail", "fail");
	}
}

//Project_TC_003
public void   VerifyThatEstimatedCost() throws Exception {
	
	searchProject();
	
	WaitClick(btn_CostingSummary);
	click(btn_CostingSummary);
//	WaitClick(btn_Material);
//	click(btn_Material);
	WaitElement(txt_Materialvalue);
	int MaterialValue= Integer.parseInt(RemoveZero(getText(txt_Materialvalue)).replaceAll(",", ""));
//	System.out.println(MaterialValue);
	
	WaitClick(btn_WorkBreakdown);
	click(btn_WorkBreakdown);
	WaitClick(btn_releaseTaskdropdown);
	click(btn_releaseTaskdropdown);
	WaitClick(btn_editTask);
	click(btn_editTask);
	
	WaitClick(btn_InputProducts);
	click(btn_InputProducts);
	
	WaitElement(btn_productinfo.replace("raw", "2"));
	
	WaitClick(btn_productinfo.replace("raw", "1"));
	click(btn_productinfo.replace("raw", "1"));
	WaitClick(btn_ProductRelatedPrice);
	click(btn_ProductRelatedPrice);
	WaitElement(txt_LastPurchasePrice);
	int LastPurchasePrice1=Integer.parseInt(RemoveZero(getText(txt_LastPurchasePrice)));
	WaitClick(btn_ProductRelatedPriceHeaderclose);
	click(btn_ProductRelatedPriceHeaderclose);
	
	WaitClick(btn_productinfo.replace("raw", "2"));
	click(btn_productinfo.replace("raw", "2"));
	WaitClick(btn_ProductRelatedPrice);
	click(btn_ProductRelatedPrice);
	WaitElement(txt_LastPurchasePrice);
	int LastPurchasePrice2=Integer.parseInt(RemoveZero(getText(txt_LastPurchasePrice)));
	WaitClick(btn_ProductRelatedPriceHeaderclose);
	click(btn_ProductRelatedPriceHeaderclose);
	
	WaitClick(btn_productinfo.replace("raw", "3"));
	click(btn_productinfo.replace("raw", "3"));
	WaitClick(btn_ProductRelatedPrice);
	click(btn_ProductRelatedPrice);
	WaitElement(txt_LastPurchasePrice);
	int LastPurchasePrice3=Integer.parseInt(RemoveZero(getText(txt_LastPurchasePrice)));
	WaitClick(btn_ProductRelatedPriceHeaderclose);
	click(btn_ProductRelatedPriceHeaderclose);
	
	WaitClick(btn_productinfo.replace("raw", "4"));
	click(btn_productinfo.replace("raw", "4"));
	WaitClick(btn_ProductRelatedPrice);
	click(btn_ProductRelatedPrice);
	WaitElement(txt_LastPurchasePrice);
	int LastPurchasePrice4=Integer.parseInt(RemoveZero(getText(txt_LastPurchasePrice)));
	WaitClick(btn_ProductRelatedPriceHeaderclose);
	click(btn_ProductRelatedPriceHeaderclose);
	
	int EstimatedQtyValue= Integer.parseInt(EstimatedQty);
	
	if(MaterialValue==((LastPurchasePrice1*EstimatedQtyValue)+(LastPurchasePrice2*EstimatedQtyValue)+(LastPurchasePrice3*EstimatedQtyValue)+(LastPurchasePrice4*EstimatedQtyValue))) {
		writeTestResults("Verify that estimated cost for input products is updated in costing summary tab","correct estimated cost for input products is updated", "correct estimated cost is updated in 'allocated' column against material", "pass");
	}else {
		writeTestResults("Verify that estimated cost for input products is updated in costing summary tab","correct estimated cost for input products is updated", "correct estimated cost is not updated in 'allocated' column against material", "fail");
	}
}

//Project_TC_004
public void VerifyThatEstimatedCostForLabour() throws Exception {
	
	searchProject();
	
	WaitClick(btn_CostingSummary);
	click(btn_CostingSummary);
//	WaitClick(btn_Hours);
//	click(btn_Hours);
	WaitElement(txt_Hoursvalue);
	int HoursValue= Integer.parseInt(RemoveZero(getText(txt_Hoursvalue)).replaceAll(",", ""));
//	System.out.println(HoursValue);
	
	clickNavigation();
	WaitClick(btn_Orgabutton);
	click(btn_Orgabutton);
	WaitClick(btn_EmployeeInformation);
	click(btn_EmployeeInformation);
	
	WaitElement(txt_EmployeeInformationTemplate);
	selectIndex(txt_EmployeeInformationTemplate, 0);
	WaitElement(txt_EmployeeInformationtxt);
	sendKeys(txt_EmployeeInformationtxt, ResponsiblePersontxt);
	pressEnter(txt_EmployeeInformationtxt);
	WaitClick(sel_EmployeeInformationsel.replace("EmpName", ResponsiblePersontxt));
	doubleClick(sel_EmployeeInformationsel.replace("EmpName", ResponsiblePersontxt));
	
	WaitElement(txt_RateProfiletxt);
	String RateProfiletxt= getText(txt_RateProfiletxt);
	
	clickNavigation();
	WaitClick(btn_Orgabutton);
	click(btn_Orgabutton);
	WaitClick(btn_RateProfile);
	click(btn_RateProfile);
	
	WaitElement(txt_RateProfileserch);
	sendKeys(txt_RateProfileserch, RateProfiletxt);
	Thread.sleep(4000);
	pressEnter(txt_RateProfileserch);
	Thread.sleep(4000);
	driver.findElement(getLocator(txt_RateProfileserch)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(txt_RateProfileserch);
	

	WaitElement(txt_ExternalHourlyRate);
	int ExternalHourlyRate=Integer.parseInt(RemoveZero(getText(txt_ExternalHourlyRate)).replaceAll(",", ""));
	
	if(HoursValue == ExternalHourlyRate*2) {
		writeTestResults("Verify that estimated cost for labour is updated in costing summary tab","correct estimated cost for labour is updated", "correct estimated cost is updated in 'allocated' column against Hours", "pass");
	}else {
		writeTestResults("Verify that estimated cost for labour is updated in costing summary tab","correct estimated cost for labour is updated", "correct estimated cost is not updated in 'allocated' column against Hours", "fail");
	}
}

//Project_TC_005
public void VerifyThatEstimatedCostForResource() throws Exception {
	
	searchProject();
	
	WaitClick(btn_CostingSummary);
	click(btn_CostingSummary);
//	WaitClick(btn_ResourceCost);
//	click(btn_ResourceCost);
	WaitElement(txt_ResourceCostvalue);
	int ResourceCostvalue= Integer.parseInt(RemoveZero(getText(txt_ResourceCostvalue)).replaceAll(",", ""));
//	System.out.println(ResourceCostvalue);
	
	clickNavigation();
	WaitClick(btn_Orgabutton);
	click(btn_Orgabutton);
	WaitClick(btn_EmployeeInformation);
	click(btn_EmployeeInformation);
	
	WaitElement(txt_EmployeeInformationTemplate);
	selectIndex(txt_EmployeeInformationTemplate, 0);
	WaitElement(txt_EmployeeInformationtxt);
	sendKeys(txt_EmployeeInformationtxt, ResponsiblePersontxt);
	pressEnter(txt_EmployeeInformationtxt);
	WaitClick(sel_EmployeeInformationsel.replace("EmpName", ResponsiblePersontxt));
	doubleClick(sel_EmployeeInformationsel.replace("EmpName", ResponsiblePersontxt));
	
	WaitElement(txt_RateProfiletxt);
	String RateProfiletxt= getText(txt_RateProfiletxt);
	
	clickNavigation();
	WaitClick(btn_Orgabutton);
	click(btn_Orgabutton);
	WaitClick(btn_RateProfile);
	click(btn_RateProfile);
	
	WaitElement(txt_RateProfileserch);
	sendKeys(txt_RateProfileserch, RateProfiletxt);
	Thread.sleep(4000);
	pressEnter(txt_RateProfileserch);
	Thread.sleep(4000);
	driver.findElement(getLocator(txt_RateProfileserch)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(txt_RateProfileserch);
	
	WaitElement(txt_ExternalHourlyRate);
	int ExternalHourlyRate=Integer.parseInt(RemoveZero(getText(txt_ExternalHourlyRate)).replaceAll(",", ""));
	
	if(ResourceCostvalue == ExternalHourlyRate*2) {
		writeTestResults("Verify that estimated cost for resource is updated in costing summary tab","correct estimated cost for resource is updated", "correct estimated cost is updated in 'allocated' column against Resource", "pass");
	}else {
		writeTestResults("Verify that estimated cost for resource is updated in costing summary tab","correct estimated cost for resource is updated", "correct estimated cost is not updated in 'allocated' column against Resource", "fail");
	}
}

//Project_TC_006
public void VerifyThatEstimatedCostForExpense() throws Exception {
	
	searchProject();
	
	WaitClick(btn_CostingSummary);
	click(btn_CostingSummary);
//	WaitClick(btn_Expensearrow);
//	click(btn_Expensearrow);
	WaitElement(txt_Expensevalue);
	int Expensevalue= Integer.parseInt(RemoveZero(getText(txt_Expensevalue)).replaceAll(",", ""));
//	System.out.println(Expensevalue);
	
	WaitClick(btn_WorkBreakdown);
	click(btn_WorkBreakdown);
	WaitClick(btn_releaseTaskdropdown);
	click(btn_releaseTaskdropdown);
	WaitClick(btn_editTask);
	click(btn_editTask);
	WaitClick(btn_Expense);
	click(btn_Expense);
	
	WaitElement(txt_EstimatedCostAfter);
	int EstimatedCostAfterValue= Integer.parseInt(RemoveZero(getText(txt_EstimatedCostAfter)));
	if(Expensevalue==EstimatedCostAfterValue) {
		writeTestResults("Verify that estimated cost for expense is updated in costing summary tab","correct estimated cost for expense is updated", "correct estimated cost is updated in 'allocated' column against Expense", "pass");
	}else {
		writeTestResults("Verify that estimated cost for expense is updated in costing summary tab","correct estimated cost for expense is updated", "correct estimated cost is not updated in 'allocated' column against Expense", "fail");
	}
}

//Project_TC_007to008
public void Project_TC_007to008() throws Exception {
	
	//employee creation
	
	WaitClick(btn_Orgabutton);
	click(btn_Orgabutton);
	WaitClick(btn_EmployeeInformation);
	click(btn_EmployeeInformation);
	
	WaitElement(txt_EmployeeInformationTemplate);
	selectIndex(txt_EmployeeInformationTemplate, 0);
	WaitElement(txt_EmployeeInformationtxt);
	sendKeys(txt_EmployeeInformationtxt, ResponsiblePersontxt);
	pressEnter(txt_EmployeeInformationtxt);
	WaitClick(sel_EmployeeInformationsel.replace("EmpName", ResponsiblePersontxt));
	doubleClick(sel_EmployeeInformationsel.replace("EmpName", ResponsiblePersontxt));
	
	WaitClick(btn_DuplicateA);
	click(btn_DuplicateA);
	
	WaitElement(txt_txtEmpNo);
	LocalTime Obj1 = LocalTime.now();
	sendKeys(txt_txtEmpNo, "ProEMP:"+Obj1);
	
	WaitClick(btn_draft);
	click(btn_draft);
	WaitElement(pageDraft);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Employee Information Form Draft",
				"Employee Information Form Draft", 
				"Employee Information Form Draft Successfully", "pass");
	} else {
		writeTestResults("verify Employee Information Form Draft",
				"Employee Information Form Draft", 
				"Employee Information Form Draft Fail", "fail");

	}
	
	WaitClick(btn_release);
	click(btn_release);
	WaitElement(pageRelease);
	if (isDisplayed(pageRelease)){

		writeTestResults("verify Employee Information Form Release",
				"Employee Information Form Release", 
				"Employee Information Form Release Successfully", "pass");
	} else {
		writeTestResults("verify Employee Information Form Release",
				"Employee Information Form Release", 
				"Employee Information Form Release Fail", "fail");

	}
	
	WaitElement(txt_EmployeeCode);
	WaitElement(txt_EmployeeFullName);
//	System.out.println(getText(txt_EmployeeCode));
//	System.out.println(getText(txt_EmployeeFullName));
	
	String Employee = ((getText(txt_EmployeeCode))+" ["+(getText(txt_EmployeeFullName))+"]");
//	System.out.println(Employee);
	
	//========================================================Project_TC_007
	
	clickNavigation();
	
	WaitClick(btn_serbutton);
	click(btn_serbutton);
	WaitClick(btn_DailyWorkSheetbutton);
	click(btn_DailyWorkSheetbutton);
	
	
	if(isDisplayed(txt_DailyWorkSheetpage)) {
		writeTestResults("verify Daily WorkSheet page","view Daily WorkSheet page", "Daily WorkSheet page is display", "pass");
	}else {
		writeTestResults("verify Daily WorkSheet page","view Daily WorkSheet page", "Daily WorkSheet page is not display", "fail");
	}
	
	WaitElement(btn_newDailyWorkSheetbutton);
	click(btn_newDailyWorkSheetbutton);
	WaitClick(btn_DailyWorkSheetProjectbutton);
	click(btn_DailyWorkSheetProjectbutton);
	
	WaitElement(txt_newDailyWorkSheetpage);
	if(getText(txt_newDailyWorkSheetpage).contentEquals("Worked Hours - New")) {
		writeTestResults("verify New Daily WorkSheet page","view New Daily WorkSheet page", "New Daily WorkSheet page is display", "pass");
	}else {
		writeTestResults("verify New Daily WorkSheet page","view New Daily WorkSheet page", "New Daily WorkSheet page is not display", "fail");
	}
	
	WaitClick(btn_AddNewRecord);
	click(btn_AddNewRecord);
	
	WaitClick(btn_JobNobutton);
	click(btn_JobNobutton);
	WaitElement(txt_JobNotxt);
	sendKeys(txt_JobNotxt, ReadData(1, 1));
	pressEnter(txt_JobNotxt);
	WaitElement(sel_JobNosel.replace("ProCode", ReadData(1, 1)));
	doubleClick(sel_JobNosel.replace("ProCode", ReadData(1, 1)));
	
	WaitClick(btn_Employeebutton);
	click(btn_Employeebutton);
	WaitElement(txt_Employeetxt);
	sendKeys(txt_Employeetxt, Employee);
	pressEnter(txt_Employeetxt);
	WaitElement(sel_Employeesel.replace("EmpCode", Employee));
	doubleClick(sel_Employeesel.replace("EmpCode", Employee));
	
	WaitClick(txt_TimeIn);
	click(txt_TimeIn);
	WaitElement(sel_TimeInsel);
	int TimeIn=Integer.parseInt(getText(sel_TimeInsel));
	WaitClick(sel_TimeInsel);
	click(sel_TimeInsel);
	WaitClick(btn_TimeInOK);
	click(btn_TimeInOK);
	
	WaitClick(txt_TimeOut);
	click(txt_TimeOut);
	WaitElement(sel_TimeOutsel);
	int TimeOut=Integer.parseInt(getText(sel_TimeOutsel));
	String ActualWork= String.valueOf(TimeOut-TimeIn);
//	System.out.println("ActualWork:"+ActualWork);
	WaitClick(sel_TimeOutsel);
	click(sel_TimeOutsel);
	WaitClick(btn_TimeOutOK);
	click(btn_TimeOutOK);
	
	WaitClick(btn_AddNewJobupdate);
	click(btn_AddNewJobupdate);
	WaitClick(btn_draft);
	click(btn_draft);
	WaitElement(pageDraftNew);
	if (isDisplayed(pageDraftNew)){

		writeTestResults("verify Daily WorkSheet Document Draft","Daily WorkSheet Document Draft", "Daily WorkSheet Document is Draft", "pass");
	} else {
		writeTestResults("verify Daily WorkSheet Document Draft","Daily WorkSheet Document Draft", "Daily WorkSheet Document is not Draft", "fail");

	}
	WaitClick(btn_release);
	click(btn_release);
	Thread.sleep(4000);
	trackCode= getText(Header);
	String DWSNo = trackCode;
	
	WaitElement(pageReleaseNew);
	if (isDisplayed(pageReleaseNew)){

		writeTestResults("Verify the function of recording actual hours through the daily worksheet form",
				"function of recording actual hours through the daily worksheet form", 
				"function of recording actual hours through the daily worksheet form successfully", "pass");
	} else {
		writeTestResults("Verify the function of recording actual hours through the daily worksheet form",
				"function of recording actual hours through the daily worksheet form", 
				"function of recording actual hours through the daily worksheet form Fail", "fail");

	}
	
	WaitClick(btn_HourlyRateBtn);
	click(btn_HourlyRateBtn);
	WaitElement(txt_HourlyRateTxt);
	Float HourlyRateTxt= Float.parseFloat(driver.findElement(By.xpath(txt_HourlyRateTxt)).getAttribute("value"));
//	System.out.println(HourlyRateTxt);
	
	//========================================================Project_TC_008
	
	pageRefersh();
	
	Thread.sleep(2000);
	clickNavigation();
	clickProjectbutton();
	searchProject();
	
	WaitClick(btn_CostingSummary);
	click(btn_CostingSummary);
	WaitClick(btn_Hours);
	click(btn_Hours);
	WaitElement(txt_HoursvalueActual.replace("EmpCode", Employee));
	Float HoursvalueActual= Float.parseFloat(RemoveZero(getText(txt_HoursvalueActual.replace("EmpCode", Employee))).replaceAll(",", ""));
//	System.out.println(HoursvalueActual);
	
	clickNavigation();
	WaitClick(btn_Orgabutton);
	click(btn_Orgabutton);
	WaitClick(btn_EmployeeInformation);
	click(btn_EmployeeInformation);
	
	WaitElement(txt_EmployeeInformationTemplate);
	selectIndex(txt_EmployeeInformationTemplate, 0);
	WaitElement(txt_EmployeeInformationtxt);
	sendKeys(txt_EmployeeInformationtxt, Employee);
	pressEnter(txt_EmployeeInformationtxt);
	WaitClick(sel_EmployeeInformationsel.replace("EmpName", Employee));
	doubleClick(sel_EmployeeInformationsel.replace("EmpName", Employee));
	
	WaitElement(txt_RateProfiletxt);
	String RateProfiletxt= getText(txt_RateProfiletxt);
	
	clickNavigation();
	WaitClick(btn_Orgabutton);
	click(btn_Orgabutton);
	WaitClick(btn_RateProfile);
	click(btn_RateProfile);
	
	WaitElement(txt_RateProfileserch);
	sendKeys(txt_RateProfileserch, RateProfiletxt);
	Thread.sleep(4000);
	pressEnter(txt_RateProfileserch);
	Thread.sleep(4000);
	driver.findElement(getLocator(txt_RateProfileserch)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(txt_RateProfileserch);
	
	WaitElement(txt_ExternalHourlyRate);
	Float ExternalHourlyRate=Float.parseFloat(RemoveZero(getText(txt_ExternalHourlyRate)).replaceAll(",", ""));
//	System.out.println(ExternalHourlyRate);
	
	if(HoursvalueActual==(ExternalHourlyRate*HourlyRateTxt*Float.parseFloat(ActualWork))) {
		writeTestResults("Verify whether correct actual cost is updated in 'Actual' column against Hours",
				"correct actual cost is updated in 'Actual' column against Hours", 
				"correct actual cost is updated in 'Actual' column against Hours Successfully", "pass");
	}else {
		writeTestResults("Verify whether correct actual cost is updated in 'Actual' column against Hours",
				"correct actual cost is updated in 'Actual' column against Hours", 
				"correct actual cost is updated in 'Actual' column against Hours Fail", "fail");
	}
	
	clickNavigation();
	clickProjectbutton();
	searchProject();
	
	WaitClick(btn_Overview);
	click(btn_Overview);
	WaitClick(btn_Labourtab);
	click(btn_Labourtab);
	
	WaitElement(txt_OverviewTabLabourActualWork.replace("DWSNo", DWSNo));
	String OverviewTabLabourActualWork= driver.findElement(By.xpath(txt_OverviewTabLabourActualWork.replace("DWSNo", DWSNo))).getAttribute("value");
//	System.out.println(OverviewTabLabourActualWork);
	
	if(ActualWork.equals(OverviewTabLabourActualWork.replaceAll("0", ""))) {
		writeTestResults("Verify whether estimated and actual labour hours are correct",
				"estimated and actual labour hours are correct", 
				"estimated and actual labour hours are correct Successfully", "pass");
	}else {
		writeTestResults("Verify whether estimated and actual labour hours are correct",
				"estimated and actual labour hours are correct", 
				"estimated and actual labour hours are correct Fail", "fail");
	}
}

//Project_TC_009to010
public void Project_TC_009to010() throws Exception {
	
	searchProject();
	
	//========================================================Project_TC_009
	
	WaitElement(btn_Action);
	click(btn_Action);
	WaitClick(btn_ActualUpdate);
	click(btn_ActualUpdate);
	WaitClick(btn_ActionResource);
	click(btn_ActionResource);
	
	WaitClick(btn_ActualWork);
	click(btn_ActualWork);
	WaitElement(btn_ActualWorksel);
	int TimeActual=Integer.parseInt(getText(btn_ActualWorksel));
	WaitClick(btn_ActualWorksel);
	click(btn_ActualWorksel);
	WaitClick(btn_ActualWorkOK);
	click(btn_ActualWorkOK);
	
	JavascriptExecutor j1 = (JavascriptExecutor)driver;
	Thread.sleep(2000);
	j1.executeScript("$(\"#divActualUpdate\").parent().find('.dialogbox-buttonarea > .button').click()");
	Thread.sleep(2000);
	pageRefersh();
	
	//========================================================Project_TC_010
	
	WaitClick(btn_CostingSummary);
	click(btn_CostingSummary);
	WaitClick(btn_ResourceCost);
	click(btn_ResourceCost);
	WaitElement(txt_ResourceCostActualValue.replace("ResCode", Resourcetxt));
	int ResourceCostActualValue = Integer.parseInt(RemoveZero(getText(txt_ResourceCostActualValue.replace("ResCode", Resourcetxt))).replaceAll(",", ""));
	//System.out.println(ResourceCostActualValue);
	
	clickNavigation();
	WaitClick(btn_Orgabutton);
	click(btn_Orgabutton);
	WaitClick(btn_EmployeeInformation);
	click(btn_EmployeeInformation);
	
	WaitElement(txt_EmployeeInformationTemplate);
	selectIndex(txt_EmployeeInformationTemplate, 0);
	WaitElement(txt_EmployeeInformationtxt);
	sendKeys(txt_EmployeeInformationtxt, ResponsiblePersontxt);
	pressEnter(txt_EmployeeInformationtxt);
	WaitClick(sel_EmployeeInformationsel.replace("EmpName", ResponsiblePersontxt));
	doubleClick(sel_EmployeeInformationsel.replace("EmpName", ResponsiblePersontxt));
	
	WaitElement(txt_RateProfiletxt);
	String RateProfiletxt= getText(txt_RateProfiletxt);
	
	clickNavigation();
	WaitClick(btn_Orgabutton);
	click(btn_Orgabutton);
	WaitClick(btn_RateProfile);
	click(btn_RateProfile);
	
	WaitElement(txt_RateProfileserch);
	sendKeys(txt_RateProfileserch, RateProfiletxt);
	Thread.sleep(4000);
	pressEnter(txt_RateProfileserch);
	Thread.sleep(4000);
	driver.findElement(getLocator(txt_RateProfileserch)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(txt_RateProfileserch);
	
	WaitElement(txt_ExternalHourlyRate);
	int ExternalHourlyRate=Integer.parseInt(RemoveZero(getText(txt_ExternalHourlyRate)).replaceAll(",", ""));
	
	if(ResourceCostActualValue==(ExternalHourlyRate*TimeActual)) {
		writeTestResults("Verify the actual cost updated for resource",
				"actual cost updated for resource", 
				"actual cost updated for resource Successfully", "pass");
	}else {
		writeTestResults("Verify the actual cost updated for resource",
				"actual cost updated for resource", 
				"actual cost updated for resource Fail", "fail");
	}
	
	clickNavigation();
	clickProjectbutton();
	searchProject();
	
	WaitClick(btn_Overview);
	click(btn_Overview);
	WaitClick(btn_ResourcestabA);
	click(btn_ResourcestabA);
	
	WaitElement(txt_OverviewTabResourceActualWork);
	String OverviewTabResourceActualWork= driver.findElement(By.xpath(txt_OverviewTabResourceActualWork)).getAttribute("value");
//	System.out.println(OverviewTabResourceActualWork);
	
	if((String.valueOf(TimeActual)).equals(OverviewTabResourceActualWork.replaceAll("0", ""))) {
		writeTestResults("Verify the estimated and actual work for resource",
				"estimated and actual work for resource", 
				"estimated and actual work for resource Successfully", "pass");
	}else {
		writeTestResults("Verify the estimated and actual work for resource",
				"estimated and actual work for resource", 
				"estimated and actual work for resource Fail", "fail");
	}	
}

//Project_TC_011
public void VerifyTheAbilityOfCreatingInternalOrders() throws Exception {
	
	searchProject();
	
	WaitClick(btn_Action);
	click(btn_Action);
	WaitClick(btn_CreateInternalOrder);
	click(btn_CreateInternalOrder);
	WaitElement(btn_Taskdropdown);
	selectIndex(btn_Taskdropdown, 2);
	
	WaitClick(btn_checkbox.replace("raw", "2"));
	click(btn_checkbox.replace("raw", "1"));
	WaitClick(btn_checkbox.replace("raw", "2"));
	click(btn_checkbox.replace("raw", "2"));
	WaitClick(btn_checkbox.replace("raw", "3"));
	click(btn_checkbox.replace("raw", "3"));
	WaitClick(btn_checkbox.replace("raw", "4"));
	click(btn_checkbox.replace("raw", "4"));
	
	WaitClick(btn_Apply);
	click(btn_Apply);
	
	switchWindow();
	
	WaitClick(btn_draft);
	click(btn_draft);
	WaitElement(pageDraft);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify internal order form Draft","internal order form Draft", "internal order form is Draft", "pass");
	} else {
		writeTestResults("verify internal order form Draft","internal order form Draft", "internal order form is not Draft", "fail");

	}
	
	WaitClick(btn_release);
	click(btn_release);
	Thread.sleep(4000);
	trackCode= getText(Header);
	
	WaitElement(btn_Gotopage);
	if (isDisplayed(btn_Gotopage)){

		writeTestResults("verify internal order form Released","internal order form Released", "internal order form is Released", "pass");
	} else {
		writeTestResults("verify internal order form Released","internal order form Released", "internal order form is not Released", "fail");
	}
	
	WaitClick(btn_Gotopage);
	click(btn_Gotopage);
	
	switchWindow();
	
	WaitClick(btn_draft);
	click(btn_draft);
	WaitElement(pageDraft);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Internal dispatch order form Draft","Internal dispatch order form Draft", "Internal dispatch order form is Draft", "pass");
	} else {
		writeTestResults("verify Internal dispatch order form Draft","Internal dispatch order form Draft", "Internal dispatch order form not Draft", "fail");

	}
	
	WaitClick(btn_IDOMasterinfoBtn.replace("raw", "1"));
	click(btn_IDOMasterinfoBtn.replace("raw", "1"));
	WaitClick(ProductBatchSerialWiseCostA);
	click(ProductBatchSerialWiseCostA);
	WaitElement(txt_IDOBatchNoPro1);
	String IDOBatchNoPro1= getText(txt_IDOBatchNoPro1);
	WaitClick(headercloseA);
	click(headercloseA);
	
	WaitClick(btn_IDOMasterinfoBtn.replace("raw", "2"));
	click(btn_IDOMasterinfoBtn.replace("raw", "2"));
	WaitClick(ProductBatchSerialWiseCostA);
	click(ProductBatchSerialWiseCostA);
	WaitElement(txt_IDOSerialNoPro2);
	String IDOSerialNoPro2= getText(txt_IDOSerialNoPro2);
	WaitClick(headercloseA);
	click(headercloseA);
	
	Thread.sleep(2000);
	WaitClick(SerialBatchA);
	click(SerialBatchA);
	
	Thread.sleep(4000);
	
	WaitClick(Cap1A);
	click(Cap1A);
	WaitElement(BatchFromNuA);
	sendKeys(BatchFromNuA, IDOBatchNoPro1);
	pressEnter(BatchFromNuA);
	mouseMove(updateSerialA);
	WaitClick(updateSerialA);
	click(updateSerialA);
	
	WaitClick(Cap2A);
	click(Cap2A);
	WaitClick(SerialRangeA);
	click(SerialRangeA);
	JavascriptExecutor j1 = (JavascriptExecutor)driver;
	j1.executeScript("$(\"#attrSlide-txtSerialCount\").numericBox(\"setValue\",5)");
	WaitElement(SerialFromNuA);
	sendKeys(SerialFromNuA, IDOSerialNoPro2);
	pressEnter(SerialFromNuA);
	mouseMove(updateSerialA);
	WaitClick(updateSerialA);
	click(updateSerialA);
	
	Thread.sleep(2000);
	WaitClick(closeProductListA);
	click(closeProductListA);
	
	WaitClick(btn_release);
	click(btn_release);
	Thread.sleep(4000);
	trackCode= getText(Header);
	WriteData(2, 1, trackCode);
	
	WaitElement(pageRelease);
	if (isDisplayed(pageRelease)){

		writeTestResults("verify Internal dispatch order form Released","Internal dispatch order form Released", "Internal dispatch order form is Released", "pass");
	} else {
		writeTestResults("verify Internal dispatch order form Released","Internal dispatch order form Released", "Internal dispatch order form is not Released", "fail");
	}
}

//Project_TC_012to013
public void VerifyTheAbilityOfUpdatingTheActual() throws Exception {
	
	searchProject();
	
	//========================================================Project_TC_012
	
	WaitClick(btn_Action);
	click(btn_Action);
	WaitClick(btn_ActualUpdate);
	click(btn_ActualUpdate);
	
	WaitElement(txt_ActualQty.replace("raw", "2"));
	sendKeys(txt_ActualQty.replace("raw", "1"), ActualQty);
//	WaitClick(btn_ActualUpdateSerialBatchNoSelBtn.replace("raw", "1"));
//	click(btn_ActualUpdateSerialBatchNoSelBtn.replace("raw", "1"));
//	WaitClick(sel_ActualUpdateSerialNoPopupSel.replace("raw", "1"));
//	click(sel_ActualUpdateSerialNoPopupSel.replace("raw", "1"));
//	WaitClick(ActualSerialUpdate);
//	click(ActualSerialUpdate);
//	WaitClick(ActualSerialYesA);
//	click(ActualSerialYesA);
	
	WaitElement(txt_ActualQty.replace("raw", "2"));
	sendKeys(txt_ActualQty.replace("raw", "2"), ActualQty);
	
	WaitElement(txt_ActualQty.replace("raw", "3"));
	sendKeys(txt_ActualQty.replace("raw", "3"), ActualQty);
	
	WaitElement(txt_ActualQty.replace("raw", "4"));
	sendKeys(txt_ActualQty.replace("raw", "4"), ActualQty);
	WaitClick(btn_ActualUpdateSerialBatchNoSelBtn.replace("raw", "4"));
	click(btn_ActualUpdateSerialBatchNoSelBtn.replace("raw", "4"));
	WaitClick(sel_ActualUpdateSerialNoPopupSel.replace("raw", "1"));
	click(sel_ActualUpdateSerialNoPopupSel.replace("raw", "1"));
	WaitClick(sel_ActualUpdateSerialNoPopupSel.replace("raw", "2"));
	click(sel_ActualUpdateSerialNoPopupSel.replace("raw", "2"));
	WaitClick(ActualSerialUpdate);
	click(ActualSerialUpdate);
	WaitClick(ActualSerialYesA);
	click(ActualSerialYesA);
	
	JavascriptExecutor j1 = (JavascriptExecutor)driver;
	Thread.sleep(2000);
	j1.executeScript("$(\"#divActualUpdate\").parent().find('.dialogbox-buttonarea > .button').click()");
	Thread.sleep(2000);
	pageRefersh();
	
	//========================================================Project_TC_013
	
	WaitClick(btn_CostingSummary);
	click(btn_CostingSummary);
	WaitElement(txt_MaterialActualvalue);
	int MaterialActualvalue= Integer.parseInt(RemoveZero(getText(txt_MaterialActualvalue)).replaceAll(",", ""));
//	System.out.println(MaterialActualvalue);
	
	if(MaterialActualvalue==Integer.parseInt(ActualQty)*100*4) {
		writeTestResults("Verify whether actual cost of input materials is updated in 'Actual' column against material",
				"actual cost of input materials is updated in 'Actual' column", 
				"actual cost of input materials is updated in 'Actual' column correct", "pass");
	}else {
		writeTestResults("Verify whether actual cost of input materials is updated in 'Actual' column against material",
				"actual cost of input materials is updated in 'Actual' column", 
				"actual cost of input materials is updated in 'Actual' column incorrect", "fail");
	}
	
	WaitClick(btn_Overview);
	click(btn_Overview);
	WaitClick(btn_InputProductstab);
	click(btn_InputProductstab);
	
	WaitElement(txt_EstimatedQtybox.replace("raw", "1"));
	String EstimatedQtyboxVal1= getText(txt_EstimatedQtybox.replace("raw", "1"));
	WaitElement(txt_EstimatedQtybox.replace("raw", "2"));
	String EstimatedQtyboxVal2= getText(txt_EstimatedQtybox.replace("raw", "2"));
	WaitElement(txt_EstimatedQtybox.replace("raw", "3"));
	String EstimatedQtyboxVal3= getText(txt_EstimatedQtybox.replace("raw", "3"));
	WaitElement(txt_EstimatedQtybox.replace("raw", "4"));
	String EstimatedQtyboxVal4= getText(txt_EstimatedQtybox.replace("raw", "4"));
	
	WaitElement(txt_ActualQtybox.replace("raw", "1"));
	String ActualQtyboxVal1= getText(txt_ActualQtybox.replace("raw", "1"));
	WaitElement(txt_ActualQtybox.replace("raw", "2"));
	String ActualQtyboxVal2= getText(txt_ActualQtybox.replace("raw", "2"));
	WaitElement(txt_ActualQtybox.replace("raw", "3"));
	String ActualQtyboxVal3= getText(txt_ActualQtybox.replace("raw", "3"));
	WaitElement(txt_ActualQtybox.replace("raw", "4"));
	String ActualQtyboxVal4= getText(txt_ActualQtybox.replace("raw", "3"));
	
	if(EstimatedQtyboxVal1.equals(EstimatedQty) && EstimatedQtyboxVal2.equals(EstimatedQty) 
			&& EstimatedQtyboxVal3.equals(EstimatedQty) && EstimatedQtyboxVal4.equals(EstimatedQty)) {
		writeTestResults("verify same value updated for Estimated Qty box",
				"same value updated for Estimated Qty box", 
				"same value updated for Estimated Qty box Successfully", "pass");
	}else {
		writeTestResults("verify same value updated for Estimated Qty box",
				"same value updated for Estimated Qty box", 
				"incorrect value updated for Estimated Qty box", "fail");
	}
	
	if(ActualQtyboxVal1.equals(ActualQty) && ActualQtyboxVal2.equals(ActualQty) 
			&& ActualQtyboxVal3.equals(ActualQty) && ActualQtyboxVal4.equals(ActualQty)) {
		writeTestResults("verify same value updated for Actual Qty box",
				"same value updated for Actual Qty box", 
				"same value updated for Actual Qty box Successfully", "pass");
	}else {
		writeTestResults("verify same value updated for Actual Qty box",
				"same value updated for Actual Qty box", 
				"incorrect value updated for Actual Qty box", "fail");
	}
}

//Project_TC_014
public void VerifyTheAbilityOfCreatingInternalReturnOrders() throws Exception {
	
	WaitClick(btn_InventoryWarehousing);
	click(btn_InventoryWarehousing);
	WaitClick(btn_InternalReturnOrder);
	click(btn_InternalReturnOrder);
	WaitClick(btn_newInternalReturnOrder);
	click(btn_newInternalReturnOrder);
	WaitClick(btn_WIPReturns);
	click(btn_WIPReturns);
	
	WaitElement(txt_ToWarehouse);
	selectText(txt_ToWarehouse, ToWarehouseA);
	WaitClick(btn_documentMark);
	click(btn_documentMark);
	
	WaitElement(txt_CommonSearch);
	sendKeys(txt_CommonSearch, ReadData(2, 1));
	WaitClick(btn_Search);
	click(btn_Search);
	
	WaitClick(btn_checkboxIRO.replace("raw", "2"));
	click(btn_checkboxIRO.replace("raw", "1"));
	WaitClick(btn_checkboxIRO.replace("raw", "2"));
	click(btn_checkboxIRO.replace("raw", "2"));
	WaitClick(btn_checkboxIRO.replace("raw", "3"));
	click(btn_checkboxIRO.replace("raw", "3"));
	WaitClick(btn_checkboxIRO.replace("raw", "4"));
	click(btn_checkboxIRO.replace("raw", "4"));
	
	WaitClick(btn_ApplyIRO);
	click(btn_ApplyIRO);
	
	WaitClick(btn_draft);
	click(btn_draft);
	WaitElement(pageDraft);
	if (isDisplayed(pageDraft)){
		writeTestResults("verify Internal Return Order form Draft","Internal Return Order form Draft", "Internal Return Order form is Draft", "pass");
	} else {
		writeTestResults("verify Internal Return Order form Draft","Internal Return Order form Draft", "Internal Return Order form is not Draft", "fail");

	}
	
	WaitClick(btn_release);
	click(btn_release);
	Thread.sleep(4000);
	trackCode= getText(Header);
	
	WaitElement(btn_Gotopage);
	if (isDisplayed(btn_Gotopage)){

		writeTestResults("verify Internal Return Order form Released","Internal Return Order form Released", "Internal Return Order form is Released", "pass");
	} else {
		writeTestResults("verify Internal Return Order form Released","Internal Return Order form Released", "Internal Return Order form is not Released", "fail");
	}
	
	WaitClick(btn_Gotopage);
	click(btn_Gotopage);
	
	switchWindow();
	
	WaitClick(btn_draft);
	click(btn_draft);
	WaitElement(pageDraft);
	if (isDisplayed(pageDraft)){
		writeTestResults("verify Internal Receipt form Draft","Internal Receipt form Draft", "Internal Receipt form is Draft", "pass");
	} else {
		writeTestResults("verify Internal Receipt form Draft","Internal Receipt form Draft", "Internal Receipt form is not Draft", "fail");
	}
	
	Thread.sleep(2000);
	WaitClick(SerialBatchA);
	click(SerialBatchA);
	
	Thread.sleep(4000);
	
	WaitClick(Cap1A);
	click(Cap1A);
	JavascriptExecutor j1 = (JavascriptExecutor)driver;
	j1.executeScript("$(\"#attr-divSerialBatchNos\").jqxGrid('setcellvalue', 0, \"Qty\", \"3\");");
	mouseMove(updateSerialA);
	WaitClick(updateSerialA);
	click(updateSerialA);
	
	WaitClick(Cap2A);
	click(Cap2A);
	mouseMove(updateSerialA);
	WaitClick(updateSerialA);
	click(updateSerialA);
	
	WaitClick(Cap4A);
	click(Cap4A);
	j1.executeScript("$(\"#attr-divSerialBatchNos\").jqxGrid('setcellvalue', 0, \"Qty\", \"3\");");
	mouseMove(updateSerialA);
	WaitClick(updateSerialA);
	click(updateSerialA);
	
	Thread.sleep(2000);
	WaitClick(closeProductListA);
	click(closeProductListA);
	
	WaitClick(btn_release);
	click(btn_release);
	Thread.sleep(4000);
	trackCode= getText(Header);
	
	WaitElement(pageRelease);
	if (isDisplayed(pageRelease)){

		writeTestResults("verify Internal Receipt form Released","Internal Receipt form Released", "Internal Receipt form is Released", "pass");
	} else {
		writeTestResults("verify Internal Receipt form Released","Internal Receipt form Released", "Internal Receipt form is not Released", "fail");
	}
}

//Project_TC_015to016
public void VerifyTheAbilityOfAddingSubContract() throws Exception {
	
	searchProject();
	
	//========================================================Project_TC_015
	
	WaitClick(btn_edit);
	click(btn_edit);
	WaitClick(btn_WorkBreakdown);
	click(btn_WorkBreakdown);
	
	WaitClick(btn_AddTask);
	click(btn_AddTask);
	WaitElement(txt_TaskName);
	LocalTime myObj = LocalTime.now();
	sendKeys(txt_TaskName,TaskName+myObj);
	WaitElement(txt_TaskType);
	selectIndex(txt_TaskType, 2);
	WaitElement(txt_ContractTypeA);
	selectIndex(txt_ContractTypeA, 1);
	
	WaitClick(btn_SubContractServicesA);
	click(btn_SubContractServicesA);
	
	Thread.sleep(2000);
	WaitClick(btn_subVendorbuttonA);
	click(btn_subVendorbuttonA);
	WaitElement(txt_subVendortxtA);
	sendKeysLookup(txt_subVendortxtA, subVendortxtA);
	pressEnter(txt_subVendortxtA);
	WaitElement(sel_subVendorselA.replace("SubVenName", subVendortxtA));
	doubleClick(sel_subVendorselA.replace("SubVenName", subVendortxtA));
	
	WaitClick(btn_SubproductbuttonA);
	click(btn_SubproductbuttonA);
	sendKeysLookup(txt_ServiceProducttxt, ServiceProducttxt);
	pressEnter(txt_ServiceProducttxt);
	Thread.sleep(5000);
	WaitElement(sel_ServiceProductsel.replace("ServicePro", ServiceProducttxt));
	doubleClick(sel_ServiceProductsel.replace("ServicePro", ServiceProducttxt));
	
	WaitElement(txt_SubEstimatedCostA);
	sendKeys(txt_SubEstimatedCostA, EstimatedCost);
	WaitClick(btn_BillableA);
	click(btn_BillableA);
	
	WaitClick(btn_TaskUpdate);
	click(btn_TaskUpdate);
	
	pageRefersh();
	Thread.sleep(3000);
	WaitClick(btn_WorkBreakdown);
	click(btn_WorkBreakdown);
	WaitClick(btn_releaseTaskdropdown);
	click(btn_releaseTaskdropdown);
	WaitClick(btn_releaseTask);
	click(btn_releaseTask);
	
	pageRefersh();
	Thread.sleep(3000);
	WaitClick(btn_update);
	click(btn_update);
	
	WaitClick(btn_WorkBreakdown);
	click(btn_WorkBreakdown);
	WaitClick(btn_releaseTaskdropdown);
	click(btn_releaseTaskdropdown);
	
	WaitElement(txt_TaskReleaseTxt.replace("TaskName", TaskName+myObj));
	if (isDisplayed(txt_TaskReleaseTxt.replace("TaskName", TaskName+myObj))){

		writeTestResults("Verify the ability of adding sub contract type tasks into the project",
				"ability of adding sub contract type tasks into the project", 
				"ability of adding sub contract type tasks into the project Successfully", "pass");
	} else {
		writeTestResults("Verify the ability of adding sub contract type tasks into the project",
				"ability of adding sub contract type tasks into the project", 
				"ability of adding sub contract type tasks into the project Fail", "fail");
	}
	
	//========================================================Project_TC_016
	
	WaitClick(btn_CostingSummary);
	click(btn_CostingSummary);
//	WaitClick(btn_SubContractA);
//	click(btn_SubContractA);
	
	WaitClick(txt_SubContractvalueA);
	String SubContractvalue =RemoveZero(getText(txt_SubContractvalueA)).replaceAll(",", "");
	
	if(SubContractvalue.equals(EstimatedCost)) {
		writeTestResults("Verify whether correct estimated cost is updated in 'allocated' column against Sub Contract",
				"correct estimated cost is updated in 'allocated' column against Sub Contract", 
				"correct estimated cost is updated in 'allocated' column against Sub Contract Successfully", "pass");
	}else {
		writeTestResults("Verify whether correct estimated cost is updated in 'allocated' column against Sub Contract",
				"correct estimated cost is updated in 'allocated' column against Sub Contract", 
				"correct estimated cost is updated in 'allocated' column against Sub Contract Fail", "fail");
	}
}

//Project_TC_017to019
public void VerifyThatTheSubContractTypeTaskCanBeConverted() throws Exception {
	
	searchProject();
	
	//========================================================Project_TC_017
	
	WaitClick(btn_Action);
	click(btn_Action);
	WaitClick(btn_SubContractOrderA);
	click(btn_SubContractOrderA);
	WaitElement(txt_subTaskA);
	selectIndex(txt_subTaskA, 3);
	
	Thread.sleep(20000);
	WaitClick(btn_subcheckboxA);
	click(btn_subcheckboxA);
	WaitClick(btn_Apply);
	click(btn_Apply);
	
	switchWindow();
	
	WaitElement(PurchaseOrderpageA);
	if (isDisplayed(PurchaseOrderpageA)) {

		writeTestResults("verify Purchase Order page","view Purchase Order page", "Purchase Order page is display", "pass");
	} else {
		writeTestResults("verify Purchase Order page","view Purchase Order page", "Purchase Order page is not dislayed", "fail");

	}
	
	WaitElement(txt_VendorReferenceNoA);
	LocalTime myObj = LocalTime.now();
	sendKeys(txt_VendorReferenceNoA, VendorReferenceNotxtA+myObj);
	
	WaitElement(btn_CheckoutA);
	WaitClick(btn_CheckoutA);
	click(btn_CheckoutA);
	
	WaitClick(btn_draft);
	click(btn_draft);
	WaitElement(pageDraft);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Order Document Draft","Purchase Order Document Draft", "Purchase Order Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Order Document Draft","Purchase Order Document Draft", "Purchase Order Document is not Draft", "fail");

	}
	
	WaitClick(btn_release);
	click(btn_release);	
	Thread.sleep(4000);
	trackCode= getText(Header);
	String PONo=trackCode;
	
	WaitElement(txt_SubContractActualcost);
	String Actualcost=getText(txt_SubContractActualcost).replaceAll(",", "");
//	System.out.println(Actualcost);
	
	WaitElement(btn_Gotopage);
	if (isDisplayed(btn_Gotopage)){
		writeTestResults("verify Purchase Order Document Released","Purchase Order Document Released", "Purchase Order Document is Released", "pass");
	} else {
		writeTestResults("verify Purchase Order Document Released","Purchase Order Document Released", "Purchase Order Document is not Released", "fail");
	}
	
	WaitClick(btn_Gotopage);
	click(btn_Gotopage);
	
	switchWindow();
	
	WaitElement(PurchaseInvoicepageA);
	if (isDisplayed(PurchaseInvoicepageA)) {

		writeTestResults("verify Purchase Invoice page","view Purchase Invoice page", "Purchase Invoice page is display", "pass");
	} else {
		writeTestResults("verify Purchase Invoice page","view Purchase Invoice page", "Purchase Invoice page is not dislayed", "fail");

	}
	
	WaitElement(btn_CheckoutA);
	WaitClick(btn_CheckoutA);
	click(btn_CheckoutA);
	
	
	WaitClick(btn_draft);
	click(btn_draft);
	WaitElement(pageDraft);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Invoice Document Draft","Purchase Invoice Document Draft", "Purchase Invoice Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Invoice Document Draft","Purchase Invoice Document Draft", "Purchase Invoice Document is not Draft", "fail");

	}
	
	Thread.sleep(3000);
	WaitClick(btn_release);
	click(btn_release);
	Thread.sleep(4000);
	trackCode= getText(Header);
	String INo=trackCode;
	
	WaitElement(pageRelease);
	if (isDisplayed(pageRelease)){

		writeTestResults("verify Purchase Invoice Document Released","Purchase Invoice Document Released", "Purchase Invoice Document is Released", "pass");
	} else {
		writeTestResults("verify Purchase Invoice Document Released","Purchase Invoice Document Released", "Purchase Invoice Document is not Released", "fail");

	}
	
	//========================================================Project_TC_018
	
	clickNavigation();
	clickProjectbutton();
	searchProject();
	
	WaitClick(btn_CostingSummary);
	click(btn_CostingSummary);
//	WaitClick(btn_SubContractA);
//	click(btn_SubContractA);
	WaitElement(txt_SubContractvalueActualA);
	String SubContractvalueActual =getText(txt_SubContractvalueActualA).replaceAll(",", "");
	
	if(SubContractvalueActual.contentEquals(Actualcost)) {
		writeTestResults("verify correct actual cost is updated in 'actual' column",
				"correct actual cost is updated", 
				"correct actual cost is updated in 'actual' column", "pass");
	}else {
		writeTestResults("verify correct actual cost is updated in 'actual' column",
				"correct actual cost is updated", 
				"correct actual cost is not updated in 'actual' column", "fail");
	}
	
	//========================================================Project_TC_019
	
	WaitClick(btn_Overview);
	click(btn_Overview);
	WaitClick(btn_SubContractServicestabA);
	click(btn_SubContractServicestabA);
	
	WaitElement(txt_OrderDateA);
	String OrderDateA=getText(txt_OrderDateA);
	WaitElement(txt_OrderNoA);
	String OrderNoA=getText(txt_OrderNoA);
	WaitElement(txt_InvoiceDateA);
	String InvoiceDateA=getText(txt_InvoiceDateA);
	WaitElement(txt_InvoiceNoA);
	String InvoiceNoA=getText(txt_InvoiceNoA);
	WaitElement(txt_ProductA);
	String ProductA=getText(txt_ProductA);
	WaitElement(txt_EstimatedAmountA);
	String EstimatedAmountA=RemoveZero(getText(txt_EstimatedAmountA));
	WaitElement(txt_ActualAmountA);
	String ActualAmountA=RemoveZero(getText(txt_ActualAmountA));
	
	   DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy");  
	   LocalDateTime now = LocalDateTime.now(); 
	   String Date=dtf.format(now);
//	   System.out.println(Date); 
	   
		if(Date.equals(OrderDateA)) {
			writeTestResults("verify correct Order Date is updated in Table","correct Order Date is updated", "correct Order Date is updated in Table", "pass");
		}else {
			writeTestResults("verify correct Order Date is updated in Table","correct Order Date is updated", "incorrect Order Date is updated in Table", "fail");
		}

		if(PONo.equals(OrderNoA)) {
			writeTestResults("verify correct Order No Date is updated in Table","correct Order No is updated", "correct Order No is updated in Table", "pass");
		}else {
			writeTestResults("verify correct Order No is updated in Table","correct Order No is updated", "incorrect Order No is updated in Table", "fail");
		}
		

		if(Date.equals(InvoiceDateA)) {
			writeTestResults("verify correct Invoice Date is updated in Table","correct Invoice Date is updated", "correct Invoice Date is updated in Table", "pass");
		}else {
			writeTestResults("verify correct Invoice Date is updated in Table","correct Invoice Date is updated", "incorrect Invoice Date is updated in Table", "fail");
		}

		if(INo.equals(InvoiceNoA)) {
			writeTestResults("verify correct Invoice No is updated in Table","correct Invoice No is updated", "correct Invoice No is updated in Table", "pass");
		}else {
			writeTestResults("verify correct Invoice No is updated in Table","correct Invoice No is updated", "incorrect Invoice No is updated in Table", "fail");
		}
		
		if(ServiceProducttxt.equals(ProductA.replace(" [service ]", ""))) {
			writeTestResults("verify correct Product is updated in Table","correct Product is updated", "correct Product is updated in Table", "pass");
		}else {
			writeTestResults("verify correct Product is updated in Table","correct Product is updated", "incorrect Product is updated in Table", "fail");
		}
		
		if((EstimatedCost).equals(EstimatedAmountA.replace(",", ""))) {
			writeTestResults("verify correct Estimated Amount is updated in Table","correct Estimated Amount is updated", "correct Estimated Amount is updated in Table", "pass");
		}else {
			writeTestResults("verify correct Estimated Amount is updated in Table","correct Estimated Amount is updated", "incorrect Estimated Amount is updated in Table", "fail");
		}
		
		if((EstimatedCost).equals(ActualAmountA.replace(",", ""))) {
			writeTestResults("verify correct Actual Amount is updated in Table","correct Actual Amount is updated", "correct Actual Amount is updated in Table", "pass");
		}else {
			writeTestResults("verify correct Actual Amount is updated in Table","correct Actual Amount is updated", "incorrect Actual Amount is updated in Table", "fail");
		}
}

//Project_TC_020
public void VerifyTheAbilityOfAddingInterDepartmentTask() throws Exception {
	
	searchProject();
	
	//========================================================Project_TC_020
	
	WaitClick(btn_edit);
	click(btn_edit);
	WaitClick(btn_WorkBreakdown);
	click(btn_WorkBreakdown);
	
	WaitClick(btn_AddTask);
	click(btn_AddTask);
	WaitElement(txt_TaskName);
	LocalTime myObj = LocalTime.now();
	sendKeys(txt_TaskName,TaskName+myObj);
	Thread.sleep(2000);
	WriteData(4, 1, TaskName+myObj);
	WaitElement(txt_TaskType);
	selectIndex(txt_TaskType, 2);
	WaitElement(txt_ContractTypeA);
	selectIndex(txt_ContractTypeA, 2);
	
	WaitClick(btn_InterDepartmentA);
	click(btn_InterDepartmentA);
	
	WaitClick(btn_OwnerbuttonA);
	click(btn_OwnerbuttonA);
	WaitElement(txt_OwnertxtA);
	sendKeysLookup(txt_OwnertxtA, OwnertxtA);
	pressEnter(txt_OwnertxtA);
	WaitElement(sel_OwnerselA.replace("OwnerName", OwnertxtA));
	doubleClick(sel_OwnerselA.replace("OwnerName", OwnertxtA));
	
	WaitElement(txt_EstimatedCostA);
	sendKeys(txt_EstimatedCostA, EstimatedCost);
	WaitClick(txt_EstimatedDateA);
	click(txt_EstimatedDateA);
	WaitClick(sel_EstimatedDateselA);
	click(sel_EstimatedDateselA);
	
	WaitClick(btn_TaskUpdate);
	click(btn_TaskUpdate);
	
	pageRefersh();
	Thread.sleep(3000);
	WaitClick(btn_WorkBreakdown);
	click(btn_WorkBreakdown);
	WaitClick(btn_releaseTaskdropdown);
	click(btn_releaseTaskdropdown);
	WaitClick(btn_releaseTask);
	click(btn_releaseTask);
	
	pageRefersh();
	Thread.sleep(3000);
	WaitClick(btn_update);
	click(btn_update);
	
	WaitClick(btn_WorkBreakdown);
	click(btn_WorkBreakdown);
	WaitClick(btn_releaseTaskdropdown);
	click(btn_releaseTaskdropdown);
	
	WaitElement(txt_TaskReleaseTxt.replace("TaskName", TaskName+myObj));
	if (isDisplayed(txt_TaskReleaseTxt.replace("TaskName", TaskName+myObj))){

		writeTestResults("Verify the ability of adding Inter Department task into the project",
				"ability of adding Inter Department task into the project", 
				"ability of adding Inter Department task into the project Successfully", "pass");
	} else {
		writeTestResults("Verify the ability of adding Inter Department task into the project",
				"ability of adding Inter Department task into the project", 
				"ability of adding Inter Department task into the project Fail", "fail");
	}
	
	//========================================================Project_TC_021
	
	clickNavigation();
	WaitClick(btn_serbutton);
	click(btn_serbutton);
	WaitClick(btn_ServiceCalendarA);
	click(btn_ServiceCalendarA);
	
	WaitElement(txt_interdepartmentjobA.replace("number", ReadData(1, 1)));
	if(isDisplayed(txt_interdepartmentjobA.replace("number", ReadData(1, 1)))) {
		writeTestResults("Verify that inter department job is showing on the estimated date","inter department job is showing", "inter department job is showing on the estimated date", "pass");
	}else {
		writeTestResults("Verify that inter department job is showing on the estimated date","inter department job is showing", "inter department job is not showing on the estimated date", "fail");
	}
	
	//========================================================Project_TC_022
	
	WaitClick(txt_interdepartmentjobA.replace("number", ReadData(1, 1)));
	click(txt_interdepartmentjobA.replace("number", ReadData(1, 1)));
	
	switchWindow();
	
	WaitElement(txt_ServiceJobOrderpageA);
	if(getText(txt_ServiceJobOrderpageA).contentEquals("Service Job Order - New")) {
		writeTestResults("Verify whether user is navigated to a new service job order form","navigated to a new service job order form", "user is navigated to a new service job order form", "pass");
	}else {
		writeTestResults("Verify whether user is navigated to a new service job order form","navigated to a new service job order form", "user is not navigated to a new service job order form", "fail");
	}
	
	//========================================================Project_TC_023
	
	Thread.sleep(5000);
	
	WaitElement(txt_ServiceGroupA);
	selectText(txt_ServiceGroupA, ServiceGroupA);
	WaitElement(txt_PriorityA);
	selectIndex(txt_PriorityA, 3);
	
	WaitClick(btn_PerformerbuttonA);
	click(btn_PerformerbuttonA);
	WaitElement(txt_PerformertxtA);
	sendKeysLookup(txt_PerformertxtA, PerformertxtA);
	pressEnter(txt_PerformertxtA);
	WaitElement(sel_PerformerselA.replace("PerformerName", PerformertxtA));
	doubleClick(sel_PerformerselA.replace("PerformerName", PerformertxtA));
	
	WaitClick(btn_PricingProfilebuttonA);
	click(btn_PricingProfilebuttonA);
	WaitElement(txt_PricingProfiletxtA);
	sendKeys(txt_PricingProfiletxtA,PricingProfileNewtxt);
	pressEnter(txt_PricingProfiletxtA);
	WaitElement(sel_PricingProfileselA.replace("PriProName", PricingProfileNewtxt)); 
	doubleClick(sel_PricingProfileselA.replace("PriProName", PricingProfileNewtxt));
	
	ScrollDown();
	
	sendKeys(txt_OrderDescriptionA, OrderDescriptionA);
	
	WaitClick(btn_ServiceLocationbuttonA);
	click(btn_ServiceLocationbuttonA);
	WaitElement(txt_ServiceLocationtxtA);
	sendKeys(txt_ServiceLocationtxtA, ProjectLocationtxt);
	pressEnter(txt_ServiceLocationtxtA);
	WaitElement(sel_ServiceLocationselA.replace("ProLoc", ProjectLocationtxt));
	doubleClick(sel_ServiceLocationselA.replace("ProLoc", ProjectLocationtxt));
	
	WaitClick(btn_draft);
	click(btn_draft);
	WaitElement(pageDraft);
	if (isDisplayed(pageDraft)){
		writeTestResults("verify Service Job Order form Document Draft","Service Job Order form Document Draft", "Service Job Order form Document is Draft", "pass");
	} else {
		writeTestResults("verify Service Job Order form Document Draft","Service Job Order form Document Draft", "Service Job Order form Document is not Draft", "fail");

	}
	
	WaitClick(btn_release);
	click(btn_release);
	Thread.sleep(4000);
	trackCode= getText(Header);
	WriteData(3, 1, trackCode);
	
	WaitElement(pageRelease);
	if (isDisplayed(pageRelease)){
		writeTestResults("verify Service Job Order form Document Released","Service Job Order form Document Released", "Service Job Order form Document is Released", "pass");
	} else {
		writeTestResults("verify Service Job Order form Document Released","Service Job Order form Document Released", "Service Job Order form Document is not Released", "fail");

	}
}

//Project_TC_024
public void VerifyTheAbilityToDoCostAllocationForServiceJobOrder() throws Exception {
	
	WaitClick(btn_FinanceA);
	click(btn_FinanceA);
	WaitClick(btn_OutboundPaymentAdviceA);
	click(btn_OutboundPaymentAdviceA);
	
	WaitElement(txt_OutboundPaymentAdvicepageA);
	if(getText(txt_OutboundPaymentAdvicepageA).contentEquals("Outbound Payment Advice")) {
		writeTestResults("verify Outbound Payment Advice page","view Outbound Payment Advice page", "Outbound Payment Advice page is display", "pass");
	}else {
		writeTestResults("verify Outbound Payment Advice page","view Outbound Payment Advice page", "Outbound Payment Advice page is not display", "fail");
	}
	
	WaitClick(btn_NewOutboundPaymentAdviceA);
	click(btn_NewOutboundPaymentAdviceA);
	WaitClick(btn_AccrualVoucherA);
	click(btn_AccrualVoucherA);
	
	WaitClick(btn_AccrualVoucherVendorA);
	click(btn_AccrualVoucherVendorA);
	WaitClick(txt_AccrualVoucherVendortxtA);
	sendKeysLookup(txt_AccrualVoucherVendortxtA, subVendortxtA);
	pressEnter(txt_AccrualVoucherVendortxtA);
	WaitElement(sel_AccrualVoucherVendorselA.replace("AVVenName", subVendortxtA));
	doubleClick(sel_AccrualVoucherVendorselA.replace("AVVenName", subVendortxtA));
	
	WaitElement(txt_AccrualVoucherVendorReferenceNoA);
	LocalTime myObj = LocalTime.now();
	sendKeys(txt_AccrualVoucherVendorReferenceNoA, AccrualVoucherVendorReferenceNoA+myObj);
	
	WaitElement(txt_AnalysisCodeA);
	selectIndex(txt_AnalysisCodeA, 4);
	WaitElement(txt_AmountA);
	sendKeys(txt_AmountA, AmountA);
	
	WaitClick(btn_costallocationA);
	click(btn_costallocationA);
	WaitElement(txt_PercentageA);
	sendKeys(txt_PercentageA, PercentageA);
	WaitElement(txt_CostAssignmentTypeA);
	selectText(txt_CostAssignmentTypeA, "Service Job Order");
	
	WaitClick(btn_CostObjectbuttonA);
	click(btn_CostObjectbuttonA);
	WaitElement(txt_CostObjecttxtA);
	sendKeysLookup(txt_CostObjecttxtA, ReadData(3, 1));
	pressEnter(txt_CostObjecttxtA);
	WaitElement(sel_CostObjectselA.replace("SJONo", ReadData(3, 1)));
	WaitClick(sel_CostObjectselA.replace("SJONo", ReadData(3, 1)));
	doubleClick(sel_CostObjectselA.replace("SJONo", ReadData(3, 1)));
	
	WaitClick(btn_AddRecordA);
	click(btn_AddRecordA);
	WaitClick(btn_UpdateA);
	click(btn_UpdateA);
	
	WaitClick(btn_CheckoutA);
	click(btn_CheckoutA);
	
	WaitClick(btn_draft);
	click(btn_draft);
	WaitElement(pageDraftNew);
	if (isDisplayed(pageDraftNew)){

		writeTestResults("verify  Accrual Voucher Document Draft","Accrual Voucher Document Draft", "Accrual Voucher Document is Draft", "pass");
	} else {
		writeTestResults("verify  Accrual Voucher Document Draft","Accrual Voucher Document Draft", "Accrual Voucher Document is not Draft", "fail");

	}
	
	WaitClick(btn_release);
	click(btn_release);
	Thread.sleep(4000);
	trackCode= getText(Header);
	WaitElement(pageReleaseNew);
	if (isDisplayed(pageReleaseNew)){
		writeTestResults("verify  Accrual Voucher Document Released","Accrual Voucher Document Released", "Accrual Voucher Document is Released", "pass");
	} else {
		writeTestResults("verify  Accrual Voucher Document Released","Accrual Voucher Document Released", "Accrual Voucher Document is not Released", "fail");
	}
}

//Project_TC_025to026
public void VerifyThatExpensesRecordedFromTheServiceJobOrder() throws Exception {
	
	searchProject();
	
	//========================================================Project_TC_025
	
	WaitClick(btn_CostingSummary);
	click(btn_CostingSummary);
//	WaitClick(btn_InterDepartmenttabA);
//	click(btn_InterDepartmenttabA);
	
	WaitElement(txt_InterDepartmentActualvalueA);
	String InterDepartmentActualValue=RemoveZero(getText(txt_InterDepartmentActualvalueA)).replaceAll(",", "");
	
	if(InterDepartmentActualValue.equals(AmountA)) {
		writeTestResults("verify correct estimated cost is updated in 'Actual' column","correct actual cost is updated", "correct actual cost is updated in 'actual' column", "pass");
	}else {
		writeTestResults("verify correct estimated cost is updated in 'Actual' column","correct actual cost is updated", "correct actual cost is not updated in 'actual' column", "fail");
	}
	
	//========================================================Project_TC_026
	
	WaitElement(txt_InterDepartmentestimatedcostA);
	String InterDepartmentestimatedcostValue = RemoveZero(getText(txt_InterDepartmentestimatedcostA)).replaceAll(",", "");
	
	if(InterDepartmentestimatedcostValue.equals(EstimatedCost)) {
		writeTestResults("verify correct estimated cost is updated in 'allocated' column","correct estimated cost is updated", "correct estimated cost is updated in 'allocated' column", "pass");
	}else {
		writeTestResults("verify correct estimated cost is updated in 'allocated' column","correct estimated cost is updated", "correct estimated cost is not updated in 'allocated' column", "fail");
	}
}

//Project_TC_027
public void VerifyWhetherPOCOfTheInterDepartmentTask() throws Exception {
	
	WaitClick(btn_serbutton);
	click(btn_serbutton);
	WaitClick(btn_ServiceJobOrderA);
	click(btn_ServiceJobOrderA);
	
	WaitElement(txt_ServiceJobOrderserchA);
	sendKeys(txt_ServiceJobOrderserchA, ReadData(3, 1));
	pressEnter(txt_ServiceJobOrderserchA);
	WaitElement(sel_ServiceJobOrderselA.replace("SJONo", ReadData(3, 1)));
	doubleClick(sel_ServiceJobOrderselA.replace("SJONo", ReadData(3, 1)));
	
	WaitClick(btn_Action);
	click(btn_Action);
	WaitClick(btn_AddProductTagNoA);
	click(btn_AddProductTagNoA);
	WaitClick(btn_SpnSeriaA);
	click(btn_SpnSeriaA);
	WaitClick(btn_SerialNosA);
	doubleClick(btn_SerialNosA);
	WaitClick(btn_AddProductTagNoapplyA);
	click(btn_AddProductTagNoapplyA);
	
	WaitClick(btn_Action);
	click(btn_Action);
	WaitClick(btn_StopA);
	click(btn_StopA);
	WaitClick(btn_CompleteA);
	click(btn_CompleteA);
	
	Thread.sleep(2000);
	WaitClick(btn_Action);
	click(btn_Action);
	WaitClick(btn_CompleteA);
	click(btn_CompleteA);
	WaitClick(btn_YesA);
	click(btn_YesA);
	
	JavascriptExecutor j7 = (JavascriptExecutor)driver;
	j7.executeScript("$('#txtServiceJOCompPostDate').datepicker('setDate', new Date())");
	
	WaitClick(btn_Apply);
	click(btn_Apply);
	
	WaitElement(txt_pageCompleted);
	if (isDisplayed(txt_pageCompleted)){
		writeTestResults("verify  Service Job Order form Completed",
				"Service Job Order form Completed", 
				"Service Job Order form is Completed", "pass");
	} else {
		writeTestResults("verify  Service Job Order form Completed",
				"Service Job Order form Completed", 
				"Service Job Order form is not Completed", "fail");
	}
	
	Thread.sleep(2000);
	clickNavigation();
	clickProjectbutton();
	searchProject();
	
	WaitClick(btn_WorkBreakdown);
	click(btn_WorkBreakdown);
	WaitClick(btn_releaseTaskdropdown);
	click(btn_releaseTaskdropdown);
	
	WaitElement(txt_POCA.replace("IDTaskName", ReadData(4, 1)));
	if(getText(txt_POCA.replace("IDTaskName", ReadData(4, 1))).equals("100")) {
		writeTestResults("Verify whether POC of the inter department task gets 100%","POC of the inter department task gets 100%", "correct value updated POC of the inter department task 100%", "pass");
	}else {
		writeTestResults("Verify whether POC of the inter department task gets 100%","POC of the inter department task gets 100%", "incorrect value updated POC of the inter department task 100%", "fail");
	}
}

//Project_TC_028to030
public void VerifyTheAbilityToDoCostAllocationForProjectTasksUsingAccrualVoucher() throws Exception {
	
	//========================================================Project_TC_028
	
	searchProject();
	
	WaitClick(btn_CostingSummary);
	click(btn_CostingSummary);
//	WaitClick(expenseArrow);
//	click(expenseArrow);
	WaitElement(existingVal);
	int expenseExistingValBef = Integer.parseInt(RemoveZero(getText(existingVal)).replaceAll(",", ""));
//	System.out.println(expenseExistingValBef);
	
	clickNavigation();
	WaitClick(btn_FinanceA);
	click(btn_FinanceA);
	WaitClick(btn_OutboundPaymentAdviceA);
	click(btn_OutboundPaymentAdviceA);
	
	WaitElement(txt_OutboundPaymentAdvicepageA);
	if(getText(txt_OutboundPaymentAdvicepageA).contentEquals("Outbound Payment Advice")) {
		writeTestResults("verify Outbound Payment Advice page","view Outbound Payment Advice page", "Outbound Payment Advice page is display", "pass");
	}else {
		writeTestResults("verify Outbound Payment Advice page","view Outbound Payment Advice page", "Outbound Payment Advice page is not display", "fail");
	}
	
	WaitClick(btn_NewOutboundPaymentAdviceA);
	click(btn_NewOutboundPaymentAdviceA);
	WaitClick(btn_AccrualVoucherA);
	click(btn_AccrualVoucherA);
	
	WaitClick(btn_AccrualVoucherVendorA);
	click(btn_AccrualVoucherVendorA);
	WaitClick(txt_AccrualVoucherVendortxtA);
	sendKeysLookup(txt_AccrualVoucherVendortxtA, vendorVal);
	pressEnter(txt_AccrualVoucherVendortxtA);
	WaitElement(sel_AccrualVoucherVendorselA.replace("AVVenName", vendorVal));
	doubleClick(sel_AccrualVoucherVendorselA.replace("AVVenName", vendorVal));
	
	WaitElement(txt_AccrualVoucherVendorReferenceNoA);
	LocalTime myObj = LocalTime.now();
	sendKeys(txt_AccrualVoucherVendorReferenceNoA, AccrualVoucherVendorReferenceNoA+myObj);
	
	WaitElement(txt_AnalysisCodeA);
	selectText(txt_AnalysisCodeA, analysisCodeVal);
	WaitElement(txt_AmountA);
	sendKeys(txt_AmountA, amountVal);
	
	WaitClick(btn_costallocationA);
	click(btn_costallocationA);
	WaitElement(txt_PercentageA);
	sendKeys(txt_PercentageA, PercentageA);
	WaitElement(txt_CostAssignmentTypeA);
	selectIndex(txt_CostAssignmentTypeA, 1);
	
	WaitClick(btn_CostObjectbuttonA);
	click(btn_CostObjectbuttonA);
	WaitElement(costObjectText);
	sendKeysLookup(costObjectText, ReadData(1, 1));
	pressEnter(costObjectText);
	WaitElement(costObjectSelected.replace("ProjCode", ReadData(1, 1)));
	WaitClick(costObjectSelected.replace("ProjCode", ReadData(1, 1)));
	doubleClick(costObjectSelected.replace("ProjCode", ReadData(1, 1)));
	
	WaitClick(btn_AddRecordA);
	click(btn_AddRecordA);
	WaitClick(btn_UpdateA);
	click(btn_UpdateA);
	
	WaitClick(btn_CheckoutA);
	click(btn_CheckoutA);
	
	WaitClick(btn_draft);
	click(btn_draft);
	WaitElement(pageDraftNew);
	if (isDisplayed(pageDraftNew)){

		writeTestResults("verify  Accrual Voucher Document Draft","Accrual Voucher Document Draft", "Accrual Voucher Document is Draft", "pass");
	} else {
		writeTestResults("verify  Accrual Voucher Document Draft","Accrual Voucher Document Draft", "Accrual Voucher Document is not Draft", "fail");

	}
	
	WaitClick(btn_release);
	click(btn_release);
	Thread.sleep(4000);
	trackCode= getText(Header);
	String OPANo= trackCode.replace(" Accrual Voucher", "");
	
	WaitElement(pageReleaseNew);
	if (isDisplayed(pageReleaseNew)){
		writeTestResults("Verify the ability to do cost allocation for project tasks using accrual voucher",
				"ability to do cost allocation for project tasks using accrual voucher", 
				"ability to do cost allocation for project tasks using accrual voucher Successfully", "pass");
	} else {
		writeTestResults("Verify the ability to do cost allocation for project tasks using accrual voucher",
				"ability to do cost allocation for project tasks using accrual voucher", 
				"ability to do cost allocation for project tasks using accrual voucher Fail", "fail");
	}
	
	//========================================================Project_TC_029
	
	clickNavigation();
	clickProjectbutton();
	searchProject();
	
	WaitClick(btn_CostingSummary);
	click(btn_CostingSummary);
	WaitElement(existingVal);
	int expenseExistingValAft = Integer.parseInt(RemoveZero(getText(existingVal)).replaceAll(",", ""));
//	System.out.println(expenseExistingValAft);
	
	if (expenseExistingValAft == expenseExistingValBef + Integer.parseInt(amountVal)) {
		writeTestResults("Verify that expenses recorded from the accrual voucher shows in project costing summary",
				"expenses recorded from the accrual voucher shows in project costing summary",
				"expenses recorded from the accrual voucher shows in project costing summary Successfully", "Pass");
	} else {
		writeTestResults("Verify that expenses recorded from the accrual voucher shows in project costing summary",
				"expenses recorded from the accrual voucher shows in project costing summary",
				"expenses recorded from the accrual voucher shows in project costing summary Fail", "Fail");
	}
	
	//========================================================Project_TC_030
	
	WaitClick(btn_Overview);
	click(btn_Overview);
	WaitClick(expenses);
	click(expenses);
	
	WaitElement(valPre.replace("OPANo", OPANo));
	String ExpenceActualcost=(RemoveZero(getText(valPre.replace("OPANo", OPANo)))).replaceAll(",", "");
	
	if (ExpenceActualcost.equals(amountVal)) {
		writeTestResults("Verify that expenses recorded from the accrual voucher shows in project overview",
				"expenses recorded from the accrual voucher shows in project overview", 
				"expenses recorded from the accrual voucher shows in project overview Successfully", "Pass");
	} else {
		writeTestResults("Verify that expenses recorded from the accrual voucher shows in project overview",
				"expenses recorded from the accrual voucher shows in project overview", 
				"expenses recorded from the accrual voucher shows in project overview Fail", "Fail");
	}
}

//Project_TC_031to033
public void VerifyTheAbilityToDoCostAllocationForProjectTasksUsingCustomerRefund() throws Exception {
	
	//========================================================Project_TC_031
	
	WaitClick(btn_FinanceA);
	click(btn_FinanceA);
	WaitClick(btn_OutboundPaymentAdviceA);
	click(btn_OutboundPaymentAdviceA);
	
	WaitElement(txt_OutboundPaymentAdvicepageA);
	if(getText(txt_OutboundPaymentAdvicepageA).contentEquals("Outbound Payment Advice")) {
		writeTestResults("verify Outbound Payment Advice page","view Outbound Payment Advice page", "Outbound Payment Advice page is display", "pass");
	}else {
		writeTestResults("verify Outbound Payment Advice page","view Outbound Payment Advice page", "Outbound Payment Advice page is not display", "fail");
	}
	
	WaitClick(btn_NewOutboundPaymentAdviceA);
	click(btn_NewOutboundPaymentAdviceA);
	WaitClick(downArrow);
	click(downArrow);
	WaitClick(customerRefund);
	click(customerRefund);
	
	WaitClick(customerSearch);
	click(customerSearch);
	WaitElement(customerText);
	sendKeys(customerText, customerVal);
	pressEnter(customerText);
	WaitElement(customerSelectedVal.replace("CusName", customerVal));
	doubleClick(customerSelectedVal.replace("CusName", customerVal));
	
	WaitElement(vendorRef);
	LocalTime myObj = LocalTime.now();
	sendKeys(vendorRef, AccrualVoucherVendorReferenceNoA+myObj);
	
	WaitElement(txt_AnalysisCodeA);
	selectIndex(txt_AnalysisCodeA, 1);
	WaitElement(txt_AmountA);
	sendKeys(txt_AmountA, cusRefundAmountVal);
	WaitClick(btn_costallocationA);
	click(btn_costallocationA);
	
	WaitElement(txt_PercentageA);
	sendKeys(txt_PercentageA, percentageVal);
	WaitElement(txt_CostAssignmentTypeA);
	selectText(txt_CostAssignmentTypeA, "Project Task");
	
	WaitClick(btn_CostObjectbuttonA);
	click(btn_CostObjectbuttonA);
	WaitElement(costObjectText);
	sendKeysLookup(costObjectText, ReadData(1, 1));
	pressEnter(costObjectText);
	WaitElement(costObjectSelected.replace("ProjCode", ReadData(1, 1)));
	WaitClick(costObjectSelected.replace("ProjCode", ReadData(1, 1)));
	doubleClick(costObjectSelected.replace("ProjCode", ReadData(1, 1)));
	
	WaitClick(btn_AddRecordA);
	click(btn_AddRecordA);
	WaitClick(btn_UpdateA);
	click(btn_UpdateA);
	
	WaitClick(btn_CheckoutA);
	click(btn_CheckoutA);
	
	WaitClick(btn_draft);
	click(btn_draft);
	WaitElement(pageDraftNew);
	if (isDisplayed(pageDraftNew)){

		writeTestResults("verify  Customer Refund Document Draft","Customer Refund Document Draft", "Customer Refund Document is Draft", "pass");
	} else {
		writeTestResults("verify  Customer Refund Document Draft","Customer Refund Document Draft", "Customer Refund Document is not Draft", "fail");

	}
	
	WaitClick(btn_release);
	click(btn_release);
	Thread.sleep(4000);
	trackCode= getText(Header);
	String CusRefNo= trackCode.replace(" Customer Refund", "");
	
	WaitElement(pageReleaseNew);
	if (isDisplayed(pageReleaseNew)){
		writeTestResults("Verify the ability to do cost allocation for project tasks using customer refund",
				"ability to do cost allocation for project tasks using customer refund", 
				"ability to do cost allocation for project tasks using customer refund Successfully", "pass");
	} else {
		writeTestResults("Verify the ability to do cost allocation for project tasks using customer refund",
				"ability to do cost allocation for project tasks using customer refund", 
				"ability to do cost allocation for project tasks using customer refund Fail", "fail");
	}
	
	//========================================================Project_TC_032
	
	clickNavigation();
	clickProjectbutton();
	searchProject();
	
	WaitClick(btn_CostingSummary);
	click(btn_CostingSummary);
	
	WaitClick(adjustment);
	click(adjustment);
	WaitElement(cusRefundCostingSum.replace("CusRefNo", CusRefNo));
	String cusRefundCostingSumValue = RemoveZero(getText(cusRefundCostingSum.replace("CusRefNo", CusRefNo))).replaceAll(",", "");
//	System.out.println(cusRefundCostingSumValue);
	
	if (cusRefundCostingSumValue.equals("-"+cusRefundAmountVal)) {
		writeTestResults("Verify that expenses recorded from the customer refund shows in project costing summary",
				"expenses recorded from the customer refund shows in project costing summary",
				"expenses recorded from the customer refund shows in project costing summary Successfully", "Pass");
	} else {
		writeTestResults("Verify that expenses recorded from the customer refund shows in project costing summary",
				"expenses recorded from the customer refund shows in project costing summary",
				"expenses recorded from the customer refund shows in project costing summary Fail", "Fail");
	}
	
	//========================================================Project_TC_033
	
	WaitClick(btn_Overview);
	click(btn_Overview);
	WaitClick(revenueAdjustment);
	click(revenueAdjustment);
	
	WaitElement(valOverviewCusRefund.replace("CusRefNo", CusRefNo));
	String cusRefundCostingOverViewValue=(RemoveZero(getText(valOverviewCusRefund.replace("CusRefNo", CusRefNo)))).replaceAll(",", "");
	
	if (cusRefundCostingOverViewValue.equals(cusRefundAmountVal)) {
		writeTestResults("Verify that expenses recorded from the customer refund shows in project overview",
				"expenses recorded from the customer refund shows in project overview", 
				"expenses recorded from the customer refund shows in project overview Successfully", "Pass");
	} else {
		writeTestResults("Verify that expenses recorded from the customer refund shows in project overview",
				"expenses recorded from the customer refund shows in project overview", 
				"expenses recorded from the customer refund shows in project overview Fail", "Fail");
	}
}

//Project_TC_034to036
public void VerifyTheAbilityToDoCostAllocationForProjectTasksUsingPaymentYoucher() throws Exception {
	
	//========================================================Project_TC_034
	
	searchProject();
	
	WaitClick(btn_CostingSummary);
	click(btn_CostingSummary);
//	WaitClick(expenseArrow);
//	click(expenseArrow);
	WaitElement(existingVal);
	int expenseExistingValBef = Integer.parseInt(RemoveZero(getText(existingVal)).replaceAll(",", ""));
//	System.out.println(expenseExistingValBef);
	
	clickNavigation();
	WaitClick(btn_FinanceA);
	click(btn_FinanceA);
	WaitClick(btn_OutboundPaymentAdviceA);
	click(btn_OutboundPaymentAdviceA);
	
	WaitElement(txt_OutboundPaymentAdvicepageA);
	if(getText(txt_OutboundPaymentAdvicepageA).contentEquals("Outbound Payment Advice")) {
		writeTestResults("verify Outbound Payment Advice page","view Outbound Payment Advice page", "Outbound Payment Advice page is display", "pass");
	}else {
		writeTestResults("verify Outbound Payment Advice page","view Outbound Payment Advice page", "Outbound Payment Advice page is not display", "fail");
	}
	
	WaitClick(btn_NewOutboundPaymentAdviceA);
	click(btn_NewOutboundPaymentAdviceA);
	WaitClick(downArrow);
	click(downArrow);
	WaitClick(paymentVoucher);
	click(paymentVoucher);
	
	WaitElement(txt_AnalysisCodeA);
	selectText(txt_AnalysisCodeA, analysisCodePayVouVal);
	WaitElement(txt_AmountA);
	sendKeys(txt_AmountA, amountVal);
	
	WaitClick(btn_costallocationA);
	click(btn_costallocationA);
	WaitElement(txt_PercentageA);
	sendKeys(txt_PercentageA, percentageVal);
	WaitElement(txt_CostAssignmentTypeA);
	selectText(txt_CostAssignmentTypeA, "Project Task");
	
	WaitClick(btn_CostObjectbuttonA);
	click(btn_CostObjectbuttonA);
	WaitElement(costObjectText);
	sendKeysLookup(costObjectText, ReadData(1, 1));
	pressEnter(costObjectText);
	WaitElement(costObjectSelected.replace("ProjCode", ReadData(1, 1)));
	WaitClick(costObjectSelected.replace("ProjCode", ReadData(1, 1)));
	doubleClick(costObjectSelected.replace("ProjCode", ReadData(1, 1)));
	
	WaitClick(btn_AddRecordA);
	click(btn_AddRecordA);
	WaitClick(btn_UpdateA);
	click(btn_UpdateA);
	
	WaitClick(btn_CheckoutA);
	click(btn_CheckoutA);
	
	WaitClick(btn_draft);
	click(btn_draft);
	WaitElement(pageDraftNew);
	if (isDisplayed(pageDraftNew)){

		writeTestResults("verify Payment Voucher Document Draft","Payment Voucher Document Draft", "Payment Voucher Document is Draft", "pass");
	} else {
		writeTestResults("verify Payment Voucher Document Draft","Payment Voucher Document Draft", "Payment Voucher Document is not Draft", "fail");

	}
	
	WaitClick(btn_release);
	click(btn_release);
	Thread.sleep(4000);
	trackCode= getText(Header);
	String PVNo= trackCode.replace(" Payment Voucher", "");
	
	WaitElement(pageReleaseNew);
	if (isDisplayed(pageReleaseNew)){
		writeTestResults("Verify the ability to do cost allocation for project tasks using Payment Voucher",
				"ability to do cost allocation for project tasks using Payment Voucher", 
				"ability to do cost allocation for project tasks using Payment Voucher Successfully", "pass");
	} else {
		writeTestResults("Verify the ability to do cost allocation for project tasks using Payment Voucher",
				"ability to do cost allocation for project tasks using Payment Voucher", 
				"ability to do cost allocation for project tasks using Payment Voucher Fail", "fail");
	}
	
	WaitClick(btn_Action);
	click(btn_Action);
	WaitClick(convertToOutboundPayment);
	click(convertToOutboundPayment);
	
//	WaitElement(payeeText);
//	sendKeys(payeeText, payeeVal);
	WaitClick(detailsTab);
	click(detailsTab);
//	WaitClick(OutboundPaymentDocSel.replace("PVNo", PVNo));
//	click(OutboundPaymentDocSel.replace("PVNo", PVNo));
	
	WaitClick(btn_CheckoutA);
	click(btn_CheckoutA);
	
	WaitClick(btn_draft);
	click(btn_draft);
	WaitElement(pageDraftNew);
	if (isDisplayed(pageDraftNew)){

		writeTestResults("verify Outbound Payment Document Draft","Outbound Payment Document Draft", "Outbound Payment Document is Draft", "pass");
	} else {
		writeTestResults("verify Outbound Payment Document Draft","Outbound Payment Document Draft", "Outbound Payment Document is not Draft", "fail");

	}
	
	WaitClick(btn_release);
	click(btn_release);
	Thread.sleep(4000);
	trackCode= getText(Header);
	
	WaitElement(pageReleaseNew);
	if (isDisplayed(pageReleaseNew)){
		writeTestResults("verify Outbound Payment Document Released","Outbound Payment Document Released", "Outbound Payment Document is Released", "pass");
	} else {
		writeTestResults("verify Outbound Payment Document Released","Outbound Payment Document Released", "Outbound Payment Document is not Released", "fail");

	}
	
	//========================================================Project_TC_035
	
	clickNavigation();
	clickProjectbutton();
	searchProject();
	
	WaitClick(btn_CostingSummary);
	click(btn_CostingSummary);
	WaitElement(existingVal);
	int expenseExistingValAft = Integer.parseInt(RemoveZero(getText(existingVal)).replaceAll(",", ""));
//	System.out.println(expenseExistingValAft);
	
	if (expenseExistingValAft == expenseExistingValBef + Integer.parseInt(amountVal)) {
		writeTestResults("Verify that expenses recorded from the payment voucher shows in project costing summary",
				"expenses recorded from the payment voucher shows in project costing summary",
				"expenses recorded from the payment voucher shows in project costing summary Successfully", "Pass");
	} else {
		writeTestResults("Verify that expenses recorded from the payment voucher shows in project costing summary",
				"expenses recorded from the payment voucher shows in project costing summary",
				"expenses recorded from the payment voucher shows in project costing summary Fail", "Fail");
	}
	
	//========================================================Project_TC_036
	
	WaitClick(btn_Overview);
	click(btn_Overview);
	WaitClick(expenses);
	click(expenses);
	
	WaitElement(valPre.replace("OPANo", PVNo));
	String ExpenceActualcost=(RemoveZero(getText(valPre.replace("OPANo", PVNo)))).replaceAll(",", "");
	
	if (ExpenceActualcost.equals(amountVal)) {
		writeTestResults("Verify that expenses recorded from the payment voucher shows in project overview",
				"expenses recorded from the payment voucher shows in project overview", 
				"expenses recorded from the payment voucher shows in project overview Successfully", "Pass");
	} else {
		writeTestResults("Verify that expenses recorded from the payment voucher shows in project overview",
				"expenses recorded from the payment voucher shows in project overview", 
				"expenses recorded from the payment voucher shows in project overview Fail", "Fail");
	}
}

//Project_TC_037to039
public void VerifyTheAbilityToDoCostAllocationForProjectTasksUsingCustomerDebitMemo() throws Exception {
	
	//========================================================Project_TC_037
	
	WaitClick(btn_FinanceA);
	click(btn_FinanceA);
	WaitClick(inboundPaymentAdvice);
	click(inboundPaymentAdvice);
	WaitClick(newInboundPaymentAdvice);
	click(newInboundPaymentAdvice);
	
	WaitElement(txt_InboundPaymentAdvicepageA);
	if(getText(txt_InboundPaymentAdvicepageA).contentEquals("Inbound Payment Advice")) {
		writeTestResults("verify Inbound Payment Advice page","view Inbound Payment Advice page", "Inbound Payment Advice page is display", "pass");
	}else {
		writeTestResults("verify Inbound Payment Advice page","view Inbound Payment Advice page", "Inbound Payment Advice page is not display", "fail");
	}
	
	WaitClick(customerDebitMemo);
	click(customerDebitMemo);
	
	WaitClick(customerSearchDebit);
	click(customerSearchDebit);
	WaitElement(customerText);
	sendKeys(customerText, customerVal);
	pressEnter(customerText);
	WaitElement(customerSelectedVal.replace("CusName", customerVal));
	doubleClick(customerSelectedVal.replace("CusName", customerVal));
	
	WaitElement(CustomerRef);
	LocalTime myObj = LocalTime.now();
	sendKeys(CustomerRef, "TestCus"+myObj);
	
	WaitElement(txt_AnalysisCodeA);
	selectIndex(txt_AnalysisCodeA, 1);
	WaitElement(amountDebitText);
	sendKeys(amountDebitText, amountDebitVal);
	WaitClick(btn_costallocationA);
	click(btn_costallocationA);
	
	WaitElement(txt_PercentageA);
	sendKeys(txt_PercentageA, percentageVal);
	WaitElement(txt_CostAssignmentTypeA);
	selectText(txt_CostAssignmentTypeA, "Project Task");
	
	WaitClick(btn_CostObjectbuttonA);
	click(btn_CostObjectbuttonA);
	WaitElement(costObjectText);
	sendKeysLookup(costObjectText, ReadData(1, 1));
	pressEnter(costObjectText);
	WaitElement(costObjectSelected.replace("ProjCode", ReadData(1, 1)));
	WaitClick(costObjectSelected.replace("ProjCode", ReadData(1, 1)));
	doubleClick(costObjectSelected.replace("ProjCode", ReadData(1, 1)));
	
	WaitClick(btn_AddRecordA);
	click(btn_AddRecordA);
	WaitClick(btn_UpdateA);
	click(btn_UpdateA);
	
	WaitClick(btn_CheckoutA);
	click(btn_CheckoutA);
	
	WaitClick(btn_draft);
	click(btn_draft);
	WaitElement(pageDraftNew);
	if (isDisplayed(pageDraftNew)){

		writeTestResults("verify Customer Debit Memo Document Draft","Customer Debit Memo Document Draft", "Customer Debit Memo Document is Draft", "pass");
	} else {
		writeTestResults("verify Customer Debit Memo Document Draft","Customer Debit Memo Document Draft", "Customer Debit Memo Document is not Draft", "fail");

	}
	
	WaitClick(btn_release);
	click(btn_release);
	Thread.sleep(4000);
	trackCode= getText(Header);
	String CusDebNo= trackCode.replace(" Customer Debit Memo", "");
	
	WaitElement(pageReleaseNew);
	if (isDisplayed(pageReleaseNew)){
		writeTestResults("Verify the ability to do cost allocation for project tasks using customer debit memo",
				"ability to do cost allocation for project tasks using customer debit memo", 
				"ability to do cost allocation for project tasks using customer debit memo Successfully", "pass");
	} else {
		writeTestResults("Verify the ability to do cost allocation for project tasks using customer debit memo",
				"ability to do cost allocation for project tasks using customer debit memo", 
				"ability to do cost allocation for project tasks using customer debit memo Fail", "fail");
	}
	
	//========================================================Project_TC_038
	
	clickNavigation();
	clickProjectbutton();
	searchProject();
	
	WaitClick(btn_CostingSummary);
	click(btn_CostingSummary);
	
	WaitClick(adjustment);
	click(adjustment);
	
	WaitElement(cusDebitMemoSum.replace("CusDebNo", CusDebNo));
	String cusDebitMemoSumValue = RemoveZero(getText(cusDebitMemoSum.replace("CusDebNo", CusDebNo))).replaceAll(",", "");
//	System.out.println(cusDebitMemoSumValue);
	
	if (cusDebitMemoSumValue.equals(amountDebitVal)) {
		writeTestResults("Verify that expenses recorded from the customer debit memo shows in project costing summary",
				"expenses recorded from the customer debit memo shows in project costing summary",
				"expenses recorded from the customer debit memo shows in project costing summary Successfully", "Pass");
	} else {
		writeTestResults("Verify that expenses recorded from the customer debit memo shows in project costing summary",
				"expenses recorded from the customer debit memo shows in project costing summary",
				"expenses recorded from the customer debit memo shows in project costing summary Fail", "Fail");
	}
	
	//========================================================Project_TC_039
	
	WaitClick(btn_Overview);
	click(btn_Overview);
	WaitClick(revenueAdjustment);
	click(revenueAdjustment);
	
	WaitElement(valOverviewCusDebit.replace("CusDebNo", CusDebNo));
	String valOverviewCusDebitValue=(RemoveZero(getText(valOverviewCusDebit.replace("CusDebNo", CusDebNo)))).replaceAll(",", "");
	
	if (valOverviewCusDebitValue.equals(amountDebitVal)) {
		writeTestResults("Verify that expenses recorded from the customer debit memo shows in project overview",
				"expenses recorded from the customer debit memo shows in project overview", 
				"expenses recorded from the customer debit memo shows in project overview Successfully", "Pass");
	} else {
		writeTestResults("Verify that expenses recorded from the customer debit memo shows in project overview",
				"expenses recorded from the customer debit memo shows in project overview", 
				"expenses recorded from the customer debit memo shows in project overview Fail", "Fail");
	}
}

//Project_TC_040to042
public void VerifyTheAbilityToDoCostAllocationForProjectTasksUsingVendorRefund() throws Exception {
	
	//========================================================Project_TC_040
	
	WaitClick(btn_FinanceA);
	click(btn_FinanceA);
	WaitClick(inboundPaymentAdvice);
	click(inboundPaymentAdvice);
	WaitClick(newInboundPaymentAdvice);
	click(newInboundPaymentAdvice);
	
	WaitElement(txt_InboundPaymentAdvicepageA);
	if(getText(txt_InboundPaymentAdvicepageA).contentEquals("Inbound Payment Advice")) {
		writeTestResults("verify Inbound Payment Advice page","view Inbound Payment Advice page", "Inbound Payment Advice page is display", "pass");
	}else {
		writeTestResults("verify Inbound Payment Advice page","view Inbound Payment Advice page", "Inbound Payment Advice page is not display", "fail");
	}
	
	WaitClick(vendorRefund);
	click(vendorRefund);
	
	WaitClick(btn_VRVendorSearchBtn);
	click(btn_VRVendorSearchBtn);
	WaitElement(txt_VRVendorSearchTxt);
	sendKeys(txt_VRVendorSearchTxt, vendorVal);
	pressEnter(txt_VRVendorSearchTxt);
	WaitElement(sel_VRVendorSearchSel.replace("VenName", vendorVal));
	doubleClick(sel_VRVendorSearchSel.replace("VenName", vendorVal));
	
	WaitElement(vendorRef);
	LocalTime myObj = LocalTime.now();
	sendKeys(vendorRef, "TestVen"+myObj);
	
	WaitElement(txt_AnalysisCodeA);
	selectIndex(txt_AnalysisCodeA, 1);
	WaitElement(amountDebitText);
	sendKeys(amountDebitText, amountVal);
	WaitClick(btn_costallocationA);
	click(btn_costallocationA);
	
	WaitElement(txt_PercentageA);
	sendKeys(txt_PercentageA, percentageVal);
	WaitElement(txt_CostAssignmentTypeA);
	selectText(txt_CostAssignmentTypeA, "Project Task");
	
	WaitClick(btn_CostObjectbuttonA);
	click(btn_CostObjectbuttonA);
	WaitElement(costObjectText);
	sendKeysLookup(costObjectText, ReadData(1, 1));
	pressEnter(costObjectText);
	WaitElement(costObjectSelected.replace("ProjCode", ReadData(1, 1)));
	WaitClick(costObjectSelected.replace("ProjCode", ReadData(1, 1)));
	doubleClick(costObjectSelected.replace("ProjCode", ReadData(1, 1)));
	
	WaitClick(btn_AddRecordA);
	click(btn_AddRecordA);
	WaitClick(btn_UpdateA);
	click(btn_UpdateA);
	
	WaitClick(btn_CheckoutA);
	click(btn_CheckoutA);
	
	WaitClick(btn_draft);
	click(btn_draft);
	WaitElement(pageDraftNew);
	if (isDisplayed(pageDraftNew)){

		writeTestResults("verify Vendor Refund Document Draft","Vendor Refund Document Draft", "Vendor Refund Document is Draft", "pass");
	} else {
		writeTestResults("verify Vendor Refund Document Draft","Vendor Refund Document Draft", "Vendor Refund Document is not Draft", "fail");

	}
	
	WaitClick(btn_release);
	click(btn_release);
	Thread.sleep(4000);
	trackCode= getText(Header);
	String VenRefbNo= trackCode.replace(" Vendor Refund", "");
	
	WaitElement(pageReleaseNew);
	if (isDisplayed(pageReleaseNew)){
		writeTestResults("Verify the ability to do cost allocation for project tasks using vendor refund",
				"ability to do cost allocation for project tasks using vendor refund", 
				"ability to do cost allocation for project tasks using vendor refund Successfully", "pass");
	} else {
		writeTestResults("Verify the ability to do cost allocation for project tasks using vendor refund",
				"ability to do cost allocation for project tasks using vendor refund", 
				"ability to do cost allocation for project tasks using vendor refund Fail", "fail");
	}
	
	//========================================================Project_TC_041
	
	clickNavigation();
	clickProjectbutton();
	searchProject();
	
	WaitClick(btn_CostingSummary);
	click(btn_CostingSummary);
	
	WaitClick(costsAdjustment);
	click(costsAdjustment);
	
	WaitElement(vendorRefundCosting.replace("VenRefbNo", VenRefbNo));
	String vendorRefundCostingValue = RemoveZero(getText(vendorRefundCosting.replace("VenRefbNo", VenRefbNo))).replaceAll(",", "");
//	System.out.println(vendorRefundCostingValue);
	
	if (vendorRefundCostingValue.equals("-"+amountVal)) {
		writeTestResults("Verify that expenses recorded from the vendor refund shows in project costing summary",
				"expenses recorded from the vendor refund shows in project costing summary",
				"expenses recorded from the vendor refund shows in project costing summary Successfully", "Pass");
	} else {
		writeTestResults("Verify that expenses recorded from the vendor refund shows in project costing summary",
				"expenses recorded from the vendor refund shows in project costing summary",
				"expenses recorded from the vendor refund shows in project costing summary Fail", "Fail");
	}
	
	//========================================================Project_TC_042
	
	WaitClick(btn_Overview);
	click(btn_Overview);
	WaitClick(costAdjustment);
	click(costAdjustment);
	
	WaitElement(valOverviewVendorRefund.replace("VenRefbNo", VenRefbNo));
	String valOverviewVendorRefundValue=(RemoveZero(getText(valOverviewVendorRefund.replace("VenRefbNo", VenRefbNo)))).replaceAll(",", "");
	
	if (valOverviewVendorRefundValue.equals("-"+amountVal)) {
		writeTestResults("Verify that expenses recorded from the vendor refund shows in project overview",
				"expenses recorded from the vendor refund shows in project overview", 
				"expenses recorded from the vendor refund shows in project overview Successfully", "Pass");
	} else {
		writeTestResults("Verify that expenses recorded from the vendor refund shows in project overview",
				"expenses recorded from the vendor refund shows in project overview", 
				"expenses recorded from the vendor refund shows in project overview Fail", "Fail");
	}
}

//Project_TC_043to045
public void VerifyTheAbilityToDoCostAllocationForProjectTasksUsingPettyCash() throws Exception {
	
	//========================================================Project_TC_043
	
	searchProject();
	
	WaitClick(btn_CostingSummary);
	click(btn_CostingSummary);
//	WaitClick(expenseArrow);
//	click(expenseArrow);
	WaitElement(existingVal);
	int expenseExistingValBef = Integer.parseInt(RemoveZero(getText(existingVal)).replaceAll(",", ""));
//	System.out.println(expenseExistingValBef);
	
	clickNavigation();
	WaitClick(btn_FinanceA);
	click(btn_FinanceA);
	WaitClick(pettyCash);
	click(pettyCash);
	WaitClick(newPettyCash);
	click(newPettyCash);
	
	Thread.sleep(3000);
	WaitElement(pettyCashAccount);
	selectIndex(pettyCashAccount, 1);
	WaitElement(descriptionPetty);
	sendKeys(descriptionPetty, desVal);
	
	WaitClick(paidtoSearch);
	click(paidtoSearch);
	WaitElement(employeeSearchText);
	sendKeys(employeeSearchText, employeeVal);
	pressEnter(employeeSearchText);
	WaitElement(empSelectedVal.replace("EmpName", employeeVal));
	doubleClick(empSelectedVal.replace("EmpName", employeeVal));
	
	WaitElement(type);
	selectIndex(type, 2);
	
	WaitElement(analysisCodeDrop);
	selectText(analysisCodeDrop, analysisCodePettyCash);
	WaitElement(narration);
	sendKeys(narration, narrationVal);
	WaitElement(amountTextPetty);
	sendKeys(amountTextPetty, amountPettyVal);
	WaitClick(costAllocationPetty);
	click(costAllocationPetty);
	
	WaitElement(txt_PercentageA);
	sendKeys(txt_PercentageA, percentageVal);
	WaitElement(txt_CostAssignmentTypeA);
	selectText(txt_CostAssignmentTypeA, "Project Task");
	
	WaitClick(btn_CostObjectbuttonA);
	click(btn_CostObjectbuttonA);
	WaitElement(costObjectText);
	sendKeysLookup(costObjectText, ReadData(1, 1));
	pressEnter(costObjectText);
	WaitElement(costObjectSelected.replace("ProjCode", ReadData(1, 1)));
	WaitClick(costObjectSelected.replace("ProjCode", ReadData(1, 1)));
	doubleClick(costObjectSelected.replace("ProjCode", ReadData(1, 1)));
	
	WaitClick(btn_AddRecordA);
	click(btn_AddRecordA);
	WaitClick(btn_UpdateA);
	click(btn_UpdateA);
	
	WaitClick(btn_draft);
	click(btn_draft);
	WaitElement(pageDraft);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Petty Cash Document Draft","Petty Cash Document Draft", "Petty Cash Document is Draft", "pass");
	} else {
		writeTestResults("verify Petty Cash Document Draft","Petty Cash Document Draft", "Petty Cash Document is not Draft", "fail");

	}
	
	WaitClick(btn_release);
	click(btn_release);
	Thread.sleep(4000);
	trackCode= getText(Header);
	String PettyCashNo= trackCode;
	
	WaitElement(pageRelease);
	if (isDisplayed(pageRelease)){
		writeTestResults("Verify the ability to do cost allocation for project tasks using petty cash",
				"ability to do cost allocation for project tasks using petty cash", 
				"ability to do cost allocation for project tasks using petty cash Successfully", "pass");
	} else {
		writeTestResults("Verify the ability to do cost allocation for project tasks using petty cash",
				"ability to do cost allocation for project tasks using petty cash", 
				"ability to do cost allocation for project tasks using petty cash Fail", "fail");
	}
	
	//========================================================Project_TC_044
	
	clickNavigation();
	clickProjectbutton();
	searchProject();
	
	WaitClick(btn_CostingSummary);
	click(btn_CostingSummary);
	WaitElement(existingVal);
	int expenseExistingValAft = Integer.parseInt(RemoveZero(getText(existingVal)).replaceAll(",", ""));
//	System.out.println(expenseExistingValAft);
	
	if (expenseExistingValAft == expenseExistingValBef + Integer.parseInt(amountPettyVal)) {
		writeTestResults("Verify that expenses recorded from the petty cash shows in project costing summary",
				"expenses recorded from the petty cash shows in project costing summary",
				"expenses recorded from the petty cash shows in project costing summary Successfully", "Pass");
	} else {
		writeTestResults("Verify that expenses recorded from the petty cash shows in project costing summary",
				"expenses recorded from the petty cash shows in project costing summary",
				"expenses recorded from the petty cash shows in project costing summary Fail", "Fail");
	}
	
	//========================================================Project_TC_045
	
	WaitClick(btn_Overview);
	click(btn_Overview);
	WaitClick(expenses);
	click(expenses);
	
	WaitElement(valOverviewPetty.replace("PettyCashNo", PettyCashNo));
	String ExpenceActualcost=(RemoveZero(getText(valOverviewPetty.replace("PettyCashNo", PettyCashNo)))).replaceAll(",", "");
	
	if (ExpenceActualcost.equals(amountPettyVal)) {
		writeTestResults("Verify that expenses recorded from the petty cash shows in project overview",
				"expenses recorded from the petty cash shows in project overview", 
				"expenses recorded from the petty cash shows in project overview Successfully", "Pass");
	} else {
		writeTestResults("Verify that expenses recorded from the petty cash shows in project overview",
				"expenses recorded from the petty cash shows in project overview", 
				"expenses recorded from the petty cash shows in project overview Fail", "Fail");
	}
}

//Project_TC_046to048
public void VerifyTheAbilityToDoCostAllocationForProjectTasksUsingBankAdjustment() throws Exception {
	
	//========================================================Project_TC_046
	
	searchProject();
	
	WaitClick(btn_CostingSummary);
	click(btn_CostingSummary);
//	WaitClick(expenseArrow);
//	click(expenseArrow);
	WaitElement(existingVal);
	int expenseExistingValBef = Integer.parseInt(RemoveZero(getText(existingVal)).replaceAll(",", ""));
//	System.out.println(expenseExistingValBef);
	
	clickNavigation();
	WaitClick(btn_FinanceA);
	click(btn_FinanceA);
	WaitClick(bankAdjustment);
	click(bankAdjustment);
	WaitClick(newBankAdjustment);
	click(newBankAdjustment);
	
	WaitElement(selectBank);
	selectIndex(selectBank, 1);
	WaitElement(bankAccountNo);
	selectIndex(bankAccountNo, 2);
	
	WaitElement(analysisCode);
	selectText(analysisCode, analysisCodeBankAdj);
	WaitElement(amountTextt);
	sendKeys(amountTextt, minusAmountVal);
	
	WaitClick(costAllocate);
	click(costAllocate);
	
	WaitElement(txt_PercentageA);
	sendKeys(txt_PercentageA, percentageVal);
	WaitElement(txt_CostAssignmentTypeA);
	selectText(txt_CostAssignmentTypeA, "Project Task");
	
	WaitClick(btn_CostObjectbuttonA);
	click(btn_CostObjectbuttonA);
	WaitElement(costObjectText);
	sendKeysLookup(costObjectText, ReadData(1, 1));
	pressEnter(costObjectText);
	WaitElement(costObjectSelected.replace("ProjCode", ReadData(1, 1)));
	WaitClick(costObjectSelected.replace("ProjCode", ReadData(1, 1)));
	doubleClick(costObjectSelected.replace("ProjCode", ReadData(1, 1)));
	
	WaitClick(btn_AddRecordA);
	click(btn_AddRecordA);
	WaitClick(btn_UpdateA);
	click(btn_UpdateA);
	
	WaitClick(btn_CheckoutA);
	click(btn_CheckoutA);
	WaitClick(btn_draft);
	click(btn_draft);
	WaitElement(pageDraftNew);
	if (isDisplayed(pageDraftNew)){

		writeTestResults("verify Bank Adjustment Document Draft","Bank Adjustment Document Draft", "Bank Adjustment Document is Draft", "pass");
	} else {
		writeTestResults("verify Bank Adjustment Document Draft","Bank Adjustment Document Draft", "Bank Adjustment Document is not Draft", "fail");

	}
	
	WaitClick(btn_release);
	click(btn_release);
	Thread.sleep(4000);
	trackCode= getText(Header);
	String BankAdjNo= trackCode;
	
	WaitElement(pageReleaseNew);
	if (isDisplayed(pageReleaseNew)){
		writeTestResults("Verify the ability to do cost allocation for project tasks using bank adjustment",
				"ability to do cost allocation for project tasks using bank adjustment", 
				"ability to do cost allocation for project tasks using bank adjustment Successfully", "pass");
	} else {
		writeTestResults("Verify the ability to do cost allocation for project tasks using bank adjustment",
				"ability to do cost allocation for project tasks using bank adjustment", 
				"ability to do cost allocation for project tasks using bank adjustment Fail", "fail");
	}
	
	//========================================================Project_TC_047
	
	clickNavigation();
	clickProjectbutton();
	searchProject();
	
	WaitClick(btn_CostingSummary);
	click(btn_CostingSummary);
	WaitElement(existingVal);
	int expenseExistingValAft = Integer.parseInt(RemoveZero(getText(existingVal)).replaceAll(",", ""));
//	System.out.println(expenseExistingValAft);
	
	if (expenseExistingValAft == expenseExistingValBef + Integer.parseInt(minusAmountVal.replace("-", ""))) {
		writeTestResults("Verify that expenses recorded from the bank adjustment shows in project costing summary",
				"expenses recorded from the bank adjustment shows in project costing summary",
				"expenses recorded from the bank adjustment shows in project costing summary Successfully", "Pass");
	} else {
		writeTestResults("Verify that expenses recorded from the bank adjustment shows in project costing summary",
				"expenses recorded from the bank adjustment shows in project costing summary",
				"expenses recorded from the bank adjustment shows in project costing summary Fail", "Fail");
	}
	
	//========================================================Project_TC_048
	
	WaitClick(btn_Overview);
	click(btn_Overview);
	WaitClick(expenses);
	click(expenses);
	
	WaitElement(valOverviewBA.replace("BankAdjNo", BankAdjNo));
	String ExpenceActualcost=(RemoveZero(getText(valOverviewBA.replace("BankAdjNo", BankAdjNo)))).replaceAll(",", "");
	
	if (ExpenceActualcost.equals(minusAmountVal.replace("-", ""))) {
		writeTestResults("Verify that expenses recorded from the bank adjustment shows in project overview",
				"expenses recorded from the bank adjustment shows in project overview", 
				"expenses recorded from the bank adjustment shows in project overview Successfully", "Pass");
	} else {
		writeTestResults("Verify that expenses recorded from the bank adjustment shows in project overview",
				"expenses recorded from the bank adjustment shows in project overview", 
				"expenses recorded from the bank adjustment shows in project overview Fail", "Fail");
	}
}

//Project_TC_049to051
public void VerifyTheAbilityToDoCostAllocationForProjectTasksUsingPurchaseInvoice() throws Exception {
	
	//========================================================Project_TC_049
	
	searchProject();
	
	WaitClick(btn_CostingSummary);
	click(btn_CostingSummary);
//	WaitClick(expenseArrow);
//	click(expenseArrow);
	WaitElement(OtherServicesexistingVal);
	int OtherServicesexistingValBef = Integer.parseInt(RemoveZero(getText(OtherServicesexistingVal)).replaceAll(",", ""));
//	System.out.println(OtherServicesexistingValBef);
	
	clickNavigation();
	WaitClick(procument);
	click(procument);
	WaitClick(purchaseInvoice);
	click(purchaseInvoice);
	WaitClick(newPurchaseInvoice);
	click(newPurchaseInvoice);
	WaitClick(purchaseInvoiceService);
	click(purchaseInvoiceService);
	
	WaitClick(vendorSearchBtn);
	click(vendorSearchBtn);
	WaitElement(vendorText);
	sendKeys(vendorText, vendorVal);
	pressEnter(vendorText);
	WaitElement(vendorSelectedVal.replace("VenName", vendorVal));
	doubleClick(vendorSelectedVal.replace("VenName", vendorVal));
	
	ScrollDown();
	
	WaitElement(Product);
	sendKeys(Product, productSerVal);
	Thread.sleep(4000);
	driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(4000);
	pressEnter(Product);
	sendKeys(ProductQty, "1");
	Thread.sleep(4000);
	pressEnter(ProductQty);
	
	WaitClick(btn_CheckoutA);
	click(btn_CheckoutA);
	WaitClick(costAllocationInv);
	click(costAllocationInv);
	
	WaitClick(removeFirstRow);
	click(removeFirstRow);
	
	WaitElement(txt_PercentageA);
	sendKeys(txt_PercentageA, percentageVal);
	WaitElement(txt_CostAssignmentTypeA);
	selectText(txt_CostAssignmentTypeA, "Project Task");
	
	WaitClick(btn_CostObjectbuttonA);
	click(btn_CostObjectbuttonA);
	WaitElement(costObjectText);
	sendKeysLookup(costObjectText, ReadData(1, 1));
	pressEnter(costObjectText);
	WaitElement(costObjectSelected.replace("ProjCode", ReadData(1, 1)));
	WaitClick(costObjectSelected.replace("ProjCode", ReadData(1, 1)));
	doubleClick(costObjectSelected.replace("ProjCode", ReadData(1, 1)));
	
	WaitClick(btn_AddRecordA);
	click(btn_AddRecordA);
	WaitClick(btn_UpdateA);
	click(btn_UpdateA);
	
	WaitClick(btn_CheckoutA);
	click(btn_CheckoutA);
	WaitClick(btn_draft);
	click(btn_draft);
	WaitElement(pageDraft);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Purchase Invoice Document Draft","Purchase Invoice Document Draft", "Purchase Invoice Document is Draft", "pass");
	} else {
		writeTestResults("verify Purchase Invoice Document Draft","Purchase Invoice Document Draft", "Purchase Invoice Document is not Draft", "fail");

	}
	
	WaitClick(btn_release);
	click(btn_release);
	Thread.sleep(4000);
	trackCode= getText(Header);
	String PINo= trackCode;
	String bannerTot = RemoveZero(getText(bannerTotInvoice)).replaceAll(",", "");
	
	WaitElement(pageRelease);
	if (isDisplayed(pageRelease)){
		writeTestResults("Verify the ability to do cost allocation for project tasks using purchase invoice",
				"ability to do cost allocation for project tasks using purchase invoice", 
				"ability to do cost allocation for project tasks using purchase invoice Successfully", "pass");
	} else {
		writeTestResults("Verify the ability to do cost allocation for project tasks using purchase invoice",
				"ability to do cost allocation for project tasks using purchase invoice", 
				"ability to do cost allocation for project tasks using purchase invoice Fail", "fail");
	}
	
	//========================================================Project_TC_050
	
	clickNavigation();
	clickProjectbutton();
	searchProject();
	
	WaitClick(btn_CostingSummary);
	click(btn_CostingSummary);
	WaitElement(OtherServicesexistingVal);
	int OtherServicesexistingValAft = Integer.parseInt(RemoveZero(getText(OtherServicesexistingVal)).replaceAll(",", ""));
//	System.out.println(OtherServicesexistingValAft);
	
	if (OtherServicesexistingValAft == OtherServicesexistingValBef + Integer.parseInt(bannerTot)) {
		writeTestResults("Verify that expenses recorded from the purchase invoice shows in project costing summary",
				"expenses recorded from the purchase invoice shows in project costing summary",
				"expenses recorded from the purchase invoice shows in project costing summary Successfully", "Pass");
	} else {
		writeTestResults("Verify that expenses recorded from the purchase invoice shows in project costing summary",
				"expenses recorded from the purchase invoice shows in project costing summary",
				"expenses recorded from the purchase invoice shows in project costing summary Fail", "Fail");
	}
	
	//========================================================Project_TC_051
	
	WaitClick(btn_Overview);
	click(btn_Overview);
	WaitClick(expenses);
	click(expenses);
	
	WaitElement(invoiceValue.replace("PINo", PINo));
	String ExpenceActualcost=(RemoveZero(getText(invoiceValue.replace("PINo", PINo)))).replaceAll(",", "");
	
	if (ExpenceActualcost.equals(bannerTot)) {
		writeTestResults("Verify that expenses recorded from the purchase invoice shows in project overview",
				"expenses recorded from the purchase invoice shows in project overview", 
				"expenses recorded from the purchase invoice shows in project overview Successfully", "Pass");
	} else {
		writeTestResults("Verify that expenses recorded from the purchase invoice shows in project overview",
				"expenses recorded from the purchase invoice shows in project overview", 
				"expenses recorded from the purchase invoice shows in project overview Fail", "Fail");
	}
}

//Project_TC_052
public void VerifyTheAbilityToRegisterSerialProductsInAProject() throws Exception {
	
	searchProject();
	
	WaitClick(btn_Action);
	click(btn_Action);
	WaitClick(btn_ProductRegistrationBtn);
	click(btn_ProductRegistrationBtn);
	
	WaitElement(txt_ProductRegistrationFilter);
	selectText(txt_ProductRegistrationFilter, "Project");
	
	WaitClick(btn_WarrentyProfileBtn.replace("raw", "1"));
	click(btn_WarrentyProfileBtn.replace("raw", "1"));
	WaitElement(txt_WarrentyProfileTxt);
	sendKeys(txt_WarrentyProfileTxt, WarrentyProfileTxt);
	pressEnter(txt_WarrentyProfileTxt);
	WaitElement(sel_WarrentyProfileSel.replace("WarPro", WarrentyProfileTxt));
	doubleClick(sel_WarrentyProfileSel.replace("WarPro", WarrentyProfileTxt));
	
	WaitClick(btn_WarrentyProfileBtn.replace("raw", "2"));
	click(btn_WarrentyProfileBtn.replace("raw", "2"));
	WaitElement(txt_WarrentyProfileTxt);
	sendKeys(txt_WarrentyProfileTxt, WarrentyProfileTxt);
	pressEnter(txt_WarrentyProfileTxt);
	WaitElement(sel_WarrentyProfileSel.replace("WarPro", WarrentyProfileTxt));
	doubleClick(sel_WarrentyProfileSel.replace("WarPro", WarrentyProfileTxt));
	
	WaitClick(btn_WarrentyProfileBtn.replace("raw", "3"));
	click(btn_WarrentyProfileBtn.replace("raw", "3"));
	WaitElement(txt_WarrentyProfileTxt);
	sendKeys(txt_WarrentyProfileTxt, WarrentyProfileTxt);
	pressEnter(txt_WarrentyProfileTxt);
	WaitElement(sel_WarrentyProfileSel.replace("WarPro", WarrentyProfileTxt));
	doubleClick(sel_WarrentyProfileSel.replace("WarPro", WarrentyProfileTxt));
	
	WaitClick(btn_ProductRegistrationApplyBtn);
	click(btn_ProductRegistrationApplyBtn);
	
	Thread.sleep(30000);
	WaitClick(btn_Action);
	click(btn_Action);
	WaitClick(btn_ProductRegistrationBtn);
	click(btn_ProductRegistrationBtn);
	
	WaitElement(txt_ProductRegistrationValidation);
	if (isDisplayed(txt_ProductRegistrationValidation)) {
		writeTestResults("Verify the ability to register serial products in a project",
				"ability to register serial products in a project", 
				"ability to register serial products in a project Successfully", "Pass");
	} else {
		writeTestResults("Verify the ability to register serial products in a project",
				"ability to register serial products in a project", 
				"ability to register serial products in a project Fail", "Fail");
	}
	
}

//Project_TC_053to054
public void VerifyTheAbilityToDraftAndReleaseInvoiceProposalServiceInvoiceToaProject() throws Exception {
	
	//========================================================Project_TC_053
	
	searchProject();
	
	WaitClick(btn_Action);
	click(btn_Action);
	WaitClick(invoiceProposal);
	click(invoiceProposal);
	
	Thread.sleep(10000);
	WaitElement(txt_InvoiceProposalFilter);
	selectText(txt_InvoiceProposalFilter,"Tasks");
	
	WaitElement(txt_InvoiceProposalTaskSelAmount.replace("TaskName", ReadData(1, 1)+"-2"));
	sendKeys(txt_InvoiceProposalTaskSelAmount.replace("TaskName", ReadData(1, 1)+"-2"), "1000");
	
	WaitClick(btn_InvoiceProposalProBtn.replace("raw", "1"));
	click(btn_InvoiceProposalProBtn.replace("raw", "1"));
	WaitElement(txt_InvoiceProposalProTxt);
	sendKeysLookup(txt_InvoiceProposalProTxt,InvPro1);
	pressEnter(txt_InvoiceProposalProTxt);
	WaitElement(sel_InvoiceProposalProSel.replace("ProName", InvPro1)); 
	doubleClick(sel_InvoiceProposalProSel.replace("ProName", InvPro1));
	
	WaitElement(txt_InvoiceProposalTaskSelAmount.replace("TaskName", ReadData(1, 1)+"-3"));
	sendKeys(txt_InvoiceProposalTaskSelAmount.replace("TaskName", ReadData(1, 1)+"-3"), "1000");
	
	WaitClick(btn_InvoiceProposalProBtn.replace("raw", "2"));
	click(btn_InvoiceProposalProBtn.replace("raw", "2"));
	WaitElement(txt_InvoiceProposalProTxt);
	sendKeysLookup(txt_InvoiceProposalProTxt,InvPro2);
	pressEnter(txt_InvoiceProposalProTxt);
	WaitElement(sel_InvoiceProposalProSel.replace("ProName", InvPro2)); 
	doubleClick(sel_InvoiceProposalProSel.replace("ProName", InvPro2));
	
	WaitClick(checkBoxNad);
	click(checkBoxNad);
	WaitClick(checkoutInvoice);
	click(checkoutInvoice);
	
	Thread.sleep(10000);
	WaitClick(applyNad);
	click(applyNad);
	
	switchWindow();
	Thread.sleep(5000);
	WaitClick(btn_CheckoutA);
	click(btn_CheckoutA);
	
	WaitClick(btn_draft);
	click(btn_draft);
	
	WaitElement(pageDraft);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Invoice Proposal Document Draft","Invoice Proposal Document Draft", "Invoice Proposal Document is Draft", "pass");
	} else {
		writeTestResults("verify Invoice Proposal Document Draft","Invoice Proposal Document Draft", "Invoice Proposal Document is not Draft", "fail");
	}
	
	WaitClick(btn_release);
	click(btn_release);
	
	Thread.sleep(4000);
	trackCode= getText(Header);
	String InvProNo= trackCode;
	String InvProBannerTotal = RemoveZero(getText(bannerTotInvoice)).replaceAll(",", "");
	
	WaitElement(btn_Gotopage);
	if (isDisplayed(btn_Gotopage)){

		writeTestResults("verify Invoice Proposal Document Released","Invoice Proposal Document Released", "Invoice Proposal Document is Released", "pass");
	} else {
		writeTestResults("verify Invoice Proposal Document Released","Invoice Proposal Document Released", "Invoice Proposal Document is not Released", "fail");
	}
	
	WaitClick(btn_Gotopage);
	click(btn_Gotopage);
	
	switchWindow();
	
	Thread.sleep(5000);
	WaitClick(serviceDetails);
	click(serviceDetails);
	
	WaitClick(btn_CheckoutA);
	click(btn_CheckoutA);
	
	WaitClick(btn_draft);
	click(btn_draft);
	
	WaitElement(pageDraft);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Service Invoice Document Draft","Service Invoice Document Draft", "Service Invoice Document is Draft", "pass");
	} else {
		writeTestResults("verify Service Invoice Document Draft","Service Invoice Document Draft", "Service Invoice Document is not Draft", "fail");
	}
	
	WaitClick(btn_release);
	click(btn_release);
	Thread.sleep(4000);
	trackCode= getText(Header);
	String SerInvNo= trackCode;
	String SerInvBannerTotal = RemoveZero(getText(bannerTotInvoice)).replaceAll(",", "");
	
	WaitElement(pageRelease);
	if (isDisplayed(pageRelease)){

		writeTestResults("Verify the ability to draft and release invoice proposal, service invoice to a project",
				"ability to draft and release invoice proposal, service invoice to a project", 
				"ability to draft and release invoice proposal, service invoice to a project Successfully", "pass");
	} else {
		writeTestResults("Verify the ability to draft and release invoice proposal, service invoice to a project",
				"ability to draft and release invoice proposal, service invoice to a project", 
				"ability to draft and release invoice proposal, service invoice to a project Fail", "fail");
	}
	
	//========================================================Project_TC_054
	
	clickNavigation();
	clickProjectbutton();
	searchProject();
	
	WaitClick(btn_CostingSummary);
	click(btn_CostingSummary);
	WaitClick(btn_ServiceInvoice);
	click(btn_ServiceInvoice);
	
	WaitElement(txt_ServiceInvoiceActualVal.replace("SINo", SerInvNo));
	String ServiceInvoiceActualVal = RemoveZero(getText(txt_ServiceInvoiceActualVal.replace("SINo", SerInvNo))).replaceAll(",", "");
//	System.out.println(ServiceInvoiceActualVal);
	
	WaitElement(txt_ServiceInvoiceBaseVal.replace("SINo", SerInvNo));
	String ServiceInvoiceBaseVal = RemoveZero(getText(txt_ServiceInvoiceBaseVal.replace("SINo", SerInvNo))).replaceAll(",", "");
//	System.out.println(ServiceInvoiceBaseVal);
	
	if (ServiceInvoiceActualVal.equals(SerInvBannerTotal) && ServiceInvoiceBaseVal.equals(SerInvBannerTotal)) {
		writeTestResults("Verify the sub total of the service invoice appear under Actua and base amount columns with relevant invoice number",
				"sub total of the service invoice appear under Actua and base amount columns with relevant invoice number",
				"sub total of the service invoice appear under Actua and base amount columns with relevant invoice number Successfully", "Pass");
	} else {
		writeTestResults("Verify the sub total of the service invoice appear under Actua and base amount columns with relevant invoice number",
				"sub total of the service invoice appear under Actua and base amount columns with relevant invoice number",
				"sub total of the service invoice appear under Actua and base amount columns with relevant invoice number Fail", "Fail");
	}
	
	WaitClick(btn_Overview);
	click(btn_Overview);
	WaitClick(btn_InvoiceDetailsTab);
	click(btn_InvoiceDetailsTab);
	
	WaitElement(txt_ProposalDate);
	String ProposalDate=getText(txt_ProposalDate);
	WaitElement(txt_ProposalNo);
	String ProposalNo=getText(txt_ProposalNo);
	WaitElement(txt_ProposalAmount);
	String ProposalAmount=RemoveZero(getText(txt_ProposalAmount));
	
	WaitElement(txt_InvoiceDate);
	String InvoiceDate=getText(txt_InvoiceDate);
	WaitElement(txt_InvoiceNo);
	String InvoiceNo=getText(txt_InvoiceNo);
	WaitElement(txt_ProposalAmount);
	String InvoiceAmount=RemoveZero(getText(txt_InvoiceAmount));
	
	   DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy");  
	   LocalDateTime now = LocalDateTime.now(); 
	   String Date=dtf.format(now);
//	   System.out.println(Date); 
	   
		if(Date.equals(ProposalDate)) {
			writeTestResults("verify correct Proposal Date is updated in Table",
					"correct Proposal Date is updated", 
					"correct Proposal Date is updated in Table", "pass");
		}else {
			writeTestResults("verify correct Proposal Date is updated in Table",
					"correct Proposal Date is updated", 
					"incorrect Proposal Date is updated in Table", "fail");
		}
		
		if(ProposalNo.equals(InvProNo)) {
			writeTestResults("verify correct Proposal Number is updated in Table",
					"correct Proposal Number is updated", 
					"correct Proposal Number is updated in Table", "pass");
		}else {
			writeTestResults("verify correct Proposal Number is updated in Table",
					"correct Proposal Number is updated", 
					"incorrect Proposal Number is updated in Table", "fail");
		}
		
		if(ProposalAmount.equals(InvProBannerTotal)) {
			writeTestResults("verify correct Proposal Amount is updated in Table",
					"correct Proposal Amount is updated", 
					"correct Proposal Amount is updated in Table", "pass");
		}else {
			writeTestResults("verify correct Proposal Amount is updated in Table",
					"correct Proposal Amount is updated", 
					"incorrect Proposal Amount is updated in Table", "fail");
		}
		
		if(Date.equals(InvoiceDate)) {
			writeTestResults("verify correct Invoice Date is updated in Table",
					"correct Invoice Date is updated", 
					"correct Invoice Date is updated in Table", "pass");
		}else {
			writeTestResults("verify correct Invoice Date is updated in Table",
					"correct Invoice Date is updated", 
					"incorrect Invoice Date is updated in Table", "fail");
		}
		
		if(InvoiceNo.equals(SerInvNo)) {
			writeTestResults("verify correct Invoice Number is updated in Table",
					"correct Invoice Number is updated", 
					"correct Invoice Number is updated in Table", "pass");
		}else {
			writeTestResults("verify correct Invoice Number is updated in Table",
					"correct Invoice Number is updated", 
					"incorrect Invoice Number is updated in Table", "fail");
		}
		
		if(InvoiceAmount.equals(SerInvBannerTotal)) {
			writeTestResults("verify correct Invoice Amount is updated in Table",
					"correct Invoice Amount is updated", 
					"correct Invoice Amount is updated in Table", "pass");
		}else {
			writeTestResults("verify correct Invoice Amount is updated in Table",
					"correct Invoice Amount is updated", 
					"incorrect Invoice Amount is updated in Table", "fail");
		}
}

//Project_TC_055to056
public void VerifyTheAbilityOfUpdatingTaskPOC() throws Exception {
	
	//========================================================Project_TC_055
	
	searchProject();
	
	WaitClick(btn_Action);
	click(btn_Action);
	WaitClick(btn_UpdateTaskPOC);
	click(btn_UpdateTaskPOC);
	
	WaitElement(txt_cboxTaskPOC);
	selectIndex(txt_cboxTaskPOC,1);
	WaitElement(txt_TskPOC);
	sendKeys(txt_TskPOC, TskPOC);
	WaitClick(btn_POCApply);
	click(btn_POCApply);
	
	pageRefersh();
	
	WaitClick(btn_Action);
	click(btn_Action);
	WaitClick(btn_UpdateTaskPOC);
	click(btn_UpdateTaskPOC);
	
	WaitElement(txt_cboxTaskPOC);
	selectIndex(txt_cboxTaskPOC,1);
	WaitElement(txt_TskPOC);
	sendKeys(txt_TskPOC, TskPOC);
	WaitClick(btn_POCApply);
	click(btn_POCApply);
	
	pageRefersh();
	
	//========================================================Project_TC_056
	
	WaitClick(btn_Action);
	click(btn_Action);
	WaitClick(btn_CompleteA);
	click(btn_CompleteA);
	WaitClick(btn_YesA);
	click(btn_YesA);

	WaitClick(btn_PostDateA);
	JavascriptExecutor j7 = (JavascriptExecutor)driver;
	j7.executeScript("$(\"#txtTaskCompPostDate\").datepicker(\"setDate\", new Date())");

	WaitClick(btn_checkboxA);
	click(btn_checkboxA);
	WaitClick(btn_Apply);
	click(btn_Apply);
	
	WaitElement(txt_pageCompleted);
	if(isDisplayed(txt_pageCompleted)) {
		writeTestResults("Verify the ability of closing a task by project complete action",
				"ability of closing a task by project complete action", 
				"ability of closing a task by project complete action Successfully", "pass");
	}else {
		writeTestResults("Verify the ability of closing a task by project complete action",
				"ability of closing a task by project complete action", 
				"ability of closing a task by project complete action Fail", "fail");
	}
}

//Project_TC_057to059
public void VerifyTheAbilityOfDraftingAndReleasingaProjectQuotationWithMandatoryData() throws Exception {
	
	//========================================================Project_TC_057
	
	WaitClick(projectQuatation);
	click(projectQuatation);
	WaitClick(newProjectQuatation);
	click(newProjectQuatation);
	
	WaitClick(searchCus);
	click(searchCus);
	WaitElement(customerTextQuat);
	sendKeys(customerTextQuat, customerTextQuatVal);
	pressEnter(customerTextQuat);
	WaitElement(selectedValCus.replace("CusName", customerTextQuatVal));
	doubleClick(selectedValCus.replace("CusName", customerTextQuatVal));

	WaitElement(salesUnit);
	selectIndex(salesUnit, 2);
	
	ScrollDown();
	
	//Input Product
	WaitElement(productTypeDrop.replace("raw", "1"));
	selectText(productTypeDrop.replace("raw", "1"), "Input Product");
	
	WaitClick(productSearch.replace("raw", "1"));
	click(productSearch.replace("raw", "1"));
//	WaitElement(productSearchText);
	sendKeysLookup(productSearchText, productSearchTextVal);
	pressEnter(productSearchText);
	WaitElement(productSelectedVal.replace("ProName", productSearchTextVal));
	doubleClick(productSelectedVal.replace("ProName", productSearchTextVal));
	
	WaitElement(productQtyPQ.replace("raw", "1"));
	sendKeys(productQtyPQ.replace("raw", "1"),"2");
	
	//Labour
	WaitClick(addNewRecord.replace("raw", "1"));
	click(addNewRecord.replace("raw", "1"));
	
	WaitElement(productTypeDrop.replace("raw", "2"));
	selectText(productTypeDrop.replace("raw", "2"), "Labour");
	
	WaitClick(referenceSearch.replace("raw", "2"));
	click(referenceSearch.replace("raw", "2"));
//	WaitElement(referenceText);
	sendKeysLookup(referenceText, referenceTextVal);
	pressEnter(referenceText);
	WaitClick(referenceSelected.replace("ProName", referenceTextVal));
	doubleClick(referenceSelected.replace("ProName", referenceTextVal));
	
	WaitClick(productSearch.replace("raw", "2"));
	click(productSearch.replace("raw", "2"));
//	WaitElement(productSearchText);
	sendKeysLookup(productSearchText, InvPro1);
	pressEnter(productSearchText);
	WaitElement(productSelectedVal.replace("ProName", InvPro1));
	doubleClick(productSelectedVal.replace("ProName", InvPro1));
	
	WaitElement(productQtyPQ.replace("raw", "2"));
	sendKeys(productQtyPQ.replace("raw", "2"),"2");
	
	//Resource
	WaitClick(addNewRecord.replace("raw", "2"));
	click(addNewRecord.replace("raw", "2"));
	
	WaitElement(productTypeDrop.replace("raw", "3"));
	selectText(productTypeDrop.replace("raw", "3"), "Resource");
	
	WaitClick(referenceSearchResource.replace("raw", "3"));
	click(referenceSearchResource.replace("raw", "3"));
//	WaitElement(referenceTextResource);
	sendKeysLookup(referenceTextResource, referenceTextResourceVal);
	pressEnter(referenceTextResource);
	WaitClick(referenceResourceSelected.replace("ProName", referenceTextResourceVal));
	doubleClick(referenceResourceSelected.replace("ProName", referenceTextResourceVal));
	
	WaitClick(productSearch.replace("raw", "3"));
	click(productSearch.replace("raw", "3"));
//	WaitElement(productSearchText);
	sendKeysLookup(productSearchText, InvPro2);
	pressEnter(productSearchText);
	WaitElement(productSelectedVal.replace("ProName", InvPro2));
	doubleClick(productSelectedVal.replace("ProName", InvPro2));
	
	WaitElement(productQtyPQ.replace("raw", "3"));
	sendKeys(productQtyPQ.replace("raw", "3"),"2");
	
	WaitClick(btn_CheckoutA);
	click(btn_CheckoutA);
	
	WaitClick(btn_draft);
	click(btn_draft);
	
	WaitElement(pageDraft);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Project Quotation Document Draft","Project Quotation Document Draft", "Project Quotation Document is Draft", "pass");
	} else {
		writeTestResults("verify Project Quotation Document Draft","Project Quotation Document Draft", "Project Quotation Document is not Draft", "fail");
	}
	
	WaitClick(btn_release);
	click(btn_release);
	Thread.sleep(4000);
	trackCode= getText(Header);
	
	WaitElement(pageRelease);
	if (isDisplayed(pageRelease)){

		writeTestResults("Verify the ability of drafting and releasing a project quotation with mandatory data",
				"ability of drafting and releasing a project quotation with mandatory data", 
				"ability of drafting and releasing a project quotation with mandatory data Successfully", "pass");
	} else {
		writeTestResults("Verify the ability of drafting and releasing a project quotation with mandatory data",
				"ability of drafting and releasing a project quotation with mandatory data", 
				"ability of drafting and releasing a project quotation with mandatory data Fail", "fail");
	}
	
	//========================================================Project_TC_058
	

	WaitClick(version);
	click(version);
	WaitElement(colorSelect);
	mouseMove(colorSelect);
	WaitClick(tickVersion);
	click(tickVersion);
	WaitClick(confirm);
	click(confirm);
	
	WaitElement(txt_pageConfirmed);
	if (isDisplayed(txt_pageConfirmed)){

		writeTestResults("Verify the confirm action for a released project quotation",
				"confirm action for a released project quotation", 
				"confirm action for a released project quotation Successfully", "pass");
	} else {
		writeTestResults("Verify the confirm action for a released project quotation",
				"confirm action for a released project quotation", 
				"confirm action for a released project quotation Fail", "fail");
	}
	
	//========================================================Project_TC_059
	
	WaitClick(btn_Action);
	click(btn_Action);
	WaitClick(convertToProject);
	click(convertToProject);
	
	switchWindow();
	
	WaitElement(txt_ProjectCode);
	LocalTime myObj = LocalTime.now();
	sendKeys(txt_ProjectCode, ProjectCode+myObj);
	
	WaitElement(txt_Title);
	sendKeys(txt_Title, Title+myObj);
	
	WaitElement(txt_ProjectGroup);
	selectIndex(txt_ProjectGroup, 1);
	
	WaitClick(btn_ResponsiblePersonbutton);
	click(btn_ResponsiblePersonbutton);
	WaitElement(txt_ResponsiblePersontxt);
	sendKeys(txt_ResponsiblePersontxt, ResponsiblePersontxt);
	pressEnter(txt_ResponsiblePersontxt);
	WaitElement(sel_ResponsiblePersonsel.replace("ResName", ResponsiblePersontxt));
	doubleClick(sel_ResponsiblePersonsel.replace("ResName", ResponsiblePersontxt));
	
	WaitClick(btn_PricingProfilebutton);
	click(btn_PricingProfilebutton);
	WaitElement(txt_PricingProfiletxt);
	sendKeys(txt_PricingProfiletxt,PricingProfiletxt);
	pressEnter(txt_PricingProfiletxt);
	WaitElement(sel_PricingProfilesel.replace("PriProName", PricingProfiletxt)); 
	doubleClick(sel_PricingProfilesel.replace("PriProName", PricingProfiletxt));
	
	ScrollDown();
	
	WaitClick(txt_RequestedStartDate);
	click(txt_RequestedStartDate);
	WaitElement(sel_RequestedStartDatesel.replace("Date", "25"));
	click(sel_RequestedStartDatesel.replace("Date", "25"));
	
	WaitClick(btn_ProjectLocationbutton);
	click(btn_ProjectLocationbutton);
	WaitElement(txt_ProjectLocationtxt);
	sendKeys(txt_ProjectLocationtxt, ProjectLocationtxt);
	pressEnter(txt_ProjectLocationtxt);
	WaitElement(sel_ProjectLocationsel.replace("ProLoc", ProjectLocationtxt));
	doubleClick(sel_ProjectLocationsel.replace("ProLoc", ProjectLocationtxt));
	
	WaitClick(btn_draft);
	click(btn_draft);
	WaitElement(pageDraft);
	if (isDisplayed(pageDraft)){

		writeTestResults("verify Project Document Draft","Project Document Draft", "Project Document is Draft", "pass");
	} else {
		writeTestResults("verify Project Document Draft","Project Document Draft", "Project Document is not Draft", "fail");

	}
	
	WaitClick(btn_release);
	click(btn_release);
	
	Thread.sleep(4000);
	trackCode= getText(Header);
	WriteData(1, 1, trackCode);
	
	WaitElement(pageRelease);
	if (isDisplayed(pageRelease)){

		writeTestResults("Verify that the confirmed project quotation can be converted into a project",
				"The confirmed project quotation can be converted into a project", 
				"The confirmed project quotation can be converted into a project Successfully", "pass");
	} else {
		writeTestResults("Verify that the confirmed project quotation can be converted into a project",
				"The confirmed project quotation can be converted into a project", 
				"The confirmed project quotation can be converted into a project Fail", "fail");
	}
}


//READ AND WRITE XL SHEET===========================================================================================

public void WriteData(int x,int y,String z) throws Exception{
	
	File f1= new File("./TestData/ProjectData.xlsx");
	FileInputStream fi1 = new FileInputStream(f1);
	XSSFWorkbook w1 = new XSSFWorkbook(fi1);
	XSSFSheet s1 = w1.getSheetAt(0);
	
	s1.getRow(x).createCell(y).setCellValue(z);
	
	FileOutputStream fo1 = new FileOutputStream(f1);
	w1.write(fo1);
}

public String ReadData(int x,int y) throws Exception{
	
	File f1= new File("./TestData/ProjectData.xlsx");
	FileInputStream fi1 = new FileInputStream(f1);
	XSSFWorkbook w1 = new XSSFWorkbook(fi1);
	XSSFSheet s1 = w1.getSheetAt(0);
	
	String Result = s1.getRow(x).getCell(y).getStringCellValue();
	return Result;
}



//RemoveZero
public String RemoveZero(String BeforeString) throws Exception {
	
	String AfterString = BeforeString.split("\\.")[0];
	return AfterString;
}

//trackcodewait
public void trackcodewait(String Trackcode) throws Exception {
	
	if (Trackcode.contains("new")) {
		Thread.sleep(1000);
	}
}

//scrollDown
public void ScrollDown() throws Exception {
	JavascriptExecutor jse = (JavascriptExecutor)driver;
	jse.executeScript("window.scrollBy(0,400)");
}

//WaitClick
public void WaitClick(String Locator)  throws Exception {
	
	WebDriverWait wait = new WebDriverWait(driver,90);
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Locator)));
}

//WaitElement
public void WaitElement(String Locator)  throws Exception {
	
	WebDriverWait wait = new WebDriverWait(driver,90);
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator)));
}
}
