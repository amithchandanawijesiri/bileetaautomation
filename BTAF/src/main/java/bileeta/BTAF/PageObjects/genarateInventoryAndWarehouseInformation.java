package bileeta.BTAF.PageObjects;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import bileeta.BATF.Pages.InventoryAndWarehouseModuleData;

public class genarateInventoryAndWarehouseInformation extends InventoryAndWarehouseModuleData {

	/* Common */
	static String user = System.getProperty("user.name");

	/* Write IW Information */
	private static Workbook wb;
	private static Sheet sh;
	private static FileInputStream fis;
	private static FileOutputStream fos;
	private static Row row;
	private static Cell cell;
	private static Cell cell1;
	private static Cell cell2;

	/* Product Group */
	private static String product_group;
	
	/* common */
	public void login() throws Exception {
		openPage(siteURL);

		sendKeys(txt_username, userNameData);

		sendKeys(txt_password, passwordData);

		waitImplicit(20);
		click(btn_login);

	}

	/* common */
	public void waitImplicit(int seconds) throws Exception {
		driver.manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);
		Thread.sleep(500);
	}
	
	/* Product Group */
	public void productGroupConfig1() throws Exception {
		login();
		waitImplicit(10);
		click(navigation_pane);

		click(inventory_module);

		click(lnk_product_group_configuration);

		click(btn_new_group_configuration);

		click(btn_product_group_plus_mark_table);

		int rowCount = driver.findElements(By.xpath(tbl_productGroup)).size();

		final String finle_btn_product_group_plus_mark_second = btn_product_group_plus_mark_second;
		String plus_mark_second = finle_btn_product_group_plus_mark_second.replace("last_row",
				String.valueOf(rowCount));
		click(plus_mark_second);

		rowCount = rowCount + 1;
		final String finle_txt_product_group = txt_product_group;
		String txt_product_group_updated = finle_txt_product_group.replace("txt_last_row", String.valueOf(rowCount));
		product_group = currentTimeAndDateAsCode() + "-Madhushan";
		sendKeys(txt_product_group_updated, product_group);

		click(btn_productGroupUpdate);

		selectText(dropdown_productGroupConfig, product_group);
		selectText(dropdown_productGroupConfig, product_group);

	}

	public void productGroupConfig2() throws Exception {
		click(tab_pg_details);

		click(btn_inventory_active);
		click(btn_product_function_confirmation);

		click(btn_purchase_active);
		click(btn_purchaseActiveConfirmation);

		click(btn_sales_active);
		click(btn_salesActiveConfirmation);

		/* sample fields(not all) */
		Select comboBox = new Select(driver.findElement(By.xpath(dropdown_outboundCostingMethodProductGroupConfig)));
		selectText(dropdown_outboundCostingMethodProductGroupConfig, outboundCostingMethodProductGroupConfig);
		String selectedOutboundCostingMethod = comboBox.getFirstSelectedOption().getText();
		selectText(dropdown_outboundCostingMethodProductGroupConfig, outboundCostingMethodProductGroupConfig);

		click(chk_allowInventoryWithoutCostingProductGroupConfig);
		click(chk_batchProductProductGroupConfig);
		sendKeys(txt_tollerenceProductGroupConfig, tollerenceProductGroupConfig);
		sendKeys(txt_minPriceProductGroupConfig, minPriceProductGroupConfig);
		click(txt_maxPriceProductGroupConfig);

		if (selectedOutboundCostingMethod.equals(outboundCostingMethodProductGroupConfig)) {

			if (isEnabled(chk_allowInventoryWithoutCostingProductGroupConfig)) {

				if (isEnabled(chk_batchProductProductGroupConfig)) {

					String tollerence_edit1 = getAttribute(txt_tollerenceProductGroupConfig, "tpv");

					String tollerence_edit2 = tollerence_edit1.replace("\"}", "");
					String tollerence_edit3 = tollerence_edit2.replace("\"", "");
					String tollerence_edit4 = tollerence_edit3.replace("{pv:,val:", "");

					if (tollerence_edit4.equals(tollerenceProductGroupConfig)) {
						String miniPrice1 = getAttribute(txt_minPriceProductGroupConfig, "tpv");
						String miniPrice2 = miniPrice1.replace("\"}", "");
						String miniPrice3 = miniPrice2.replace("\"", "");
						String miniPrice4 = miniPrice3.replace("{pv:,val:", "");

					} else {

					}
				} else {

				}
			} else {
			}

		} else {

		}
		writeIWCreations("ProductGroup", product_group, 0);

		click(btn_draft);
		waitImplicit(10);
		click(btn_relese);
		waitImplicit(10);

		writeTestResults(" ", " ", " ", "pass");
		writeTestResults(" ", " ", " ", "pass");
	}

	/* common */
	public String currentTimeAndDateAsCode() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		String curDate = dtf.format(now);

		curDate = curDate.replaceAll("\\s", "");
		curDate = curDate.replaceAll("/", "").replaceAll(":", "");
		return curDate;
	}

	/* common */
	public void writeIWCreations(String name, String variable, int data_row)
			throws InvalidFormatException, IOException {
		String path = getProjectPath("*src*test*java*inventoryAndWarehouseModule*IW_CREATIONS.xlsx");
		fis = new FileInputStream(path);
		wb = WorkbookFactory.create(fis);
		sh = wb.getSheet("Sheet1");
		row = sh.createRow(data_row);
		cell = row.createCell(1);
		cell1 = row.createCell(0);
		cell.setCellValue(variable);
		cell1.setCellValue(name);
		System.out.println(cell.getStringCellValue());
		fos = new FileOutputStream(path);
		wb.write(fos);
		fos.flush();
		fos.close();
	}

	public String readIWCreations(String product_category) throws IOException, InvalidFormatException {
		String path = getProjectPath("*src*test*java*inventoryAndWarehouseModule*IW_CREATIONS.xlsx");
		fis = new FileInputStream(path);
		wb = WorkbookFactory.create(fis);
		sh = wb.getSheet("Sheet1");

		Map<String, String> iw_creation_map = new HashMap<String, String>();
		List<Map<String, String>> iw_creation = new ArrayList<Map<String, String>>();
		
		int totRows = sh.getLastRowNum();
		for (i = 0; i <= totRows;i++) {
			row = sh.getRow(i);
			cell = row.getCell(0);
			cell2 = row.getCell(1);

			String test_creation_row1 = cell.getStringCellValue();
			String test_creation_row2 = cell2.getStringCellValue();

			iw_creation_map.put(test_creation_row1, test_creation_row2);
			iw_creation.add(i, iw_creation_map);
			
		}

		String iw_info = iw_creation.get(0).get(product_category);

		return iw_info;
	}

	public String getProjectPath(String project_path) {
		String path = "";
		/* OLD CODE
		 * if (user.equals("Madhushan")||user.equals("Aruna")) { String
		 * real_project_path = project_path.replace("*", "/"); path = real_project_path;
		 * } else { String location =System.getProperty("user.dir");; String
		 * real_project_path = project_path.replace("*", "\\"); path = location+
		 * real_project_path; }
		 */
			String location =System.getProperty("user.dir");;
			String real_project_path = project_path.replace("*", "\\");
			path = location+ real_project_path;
			return path;
	}
	
	/* Manufacturer */
	public void createMenfacturur() throws Exception {
		waitImplicit(15);
		click(navigation_pane);
		Thread.sleep(2000);
		click(inventory_module);
		click(btn_menufacturerInformtion);
		click(btn_newMenufacturer);
		waitImplicit(15);
		String menufacturerCode = currentTimeAndDateAsCode() + "-Madhushan";

		sendKeys(txt_menufacturerCode, menufacturerCode);

		String menufacturerName = currentTimeAndDateAsCode() + "-Madhushan";
		sendKeys(txt_nameMenufacturer, menufacturerName);
		click(btn_upadateMenufacturer);
		Thread.sleep(2000);
		writeIWCreations("Menufacturer", menufacturerCode,1);
	}
	
	public void transactionBookConfigurationWarehouseCreation() throws Exception {
		waitImplicit(15);
		click(navigation_pane);
		Thread.sleep(2000);
		click(btn_adminModule);
		Thread.sleep(2000);
		click(btn_trnsactionBookConfiguration);
		waitImplicit(8);
		selectText(drop_filterByTransactionBookConfiguration, lotBookModule);
		Thread.sleep(2000);
		Actions action1 = new Actions(driver);
		WebElement we1 = driver.findElement(By.xpath("//div[contains(@class,'processitem color-selectedborder')]//span[contains(@class,'processno color-default')][contains(text(),'25')]"));
		action1.moveToElement(we1).build().perform();
		click("//div[contains(@class,'processitem color-selectedborder')]//span[contains(@class,'processno color-default')][contains(text(),'25')]");
		pageScrollUpToBottom();
//		Actions action1 = new Actions(driver);
//		WebElement we1 = driver.findElement(By.xpath(btn_warehouseLotBookConfiguration));
		action1.moveToElement(we1).build().perform();
		click(btn_warehouseLotBookConfiguration);
		Thread.sleep(2000);
		click(btn_transactionBookConfigRowAdding);
		Thread.sleep(1000);
		sendKeys(txt_bookNoTBC, genarateTransactionBookLotBookConfig());
		
		
	}
	
	public String genarateTransactionBookLotBookConfig() throws InvalidFormatException, IOException {
		String readed_id = readIWCreations("LotCodeNumber");
		String quate = "MADHUSHAN-";
		if(user.equals("MADH")) {quate = "MADH1-";}
		String out_final = quate + readed_id;	
		int id = Integer.parseInt(readed_id);
		int id_next = id + 1;
		String id_next_str = String.valueOf(id_next);
		writeIWCreations("LotCodeNumber", id_next_str, 2);
		return out_final;
	}
	
//	txt_prefixTBCWarehouse
//	chk_defoultWarehouseTBC
//	btn_updateWarehouseTransactionBookConfiguration
	
	public String uomGroupCreation() throws InvalidFormatException, IOException {
		String readed_id = readIWCreations("LotCodeNumber");
		String quate = "MADHUSHAN-";
		String out_final = quate + readed_id;	
		int id = Integer.parseInt(readed_id);
		int id_next = id + 1;
//		String id_next_str = String.valueOf(id_next);
//		writeIWCreations("LotCodeNumber", id_next_str, 2);
		return out_final;
	}
	
	/* Common Stock Adjustment */
	public void commonStockAdjustment(String product1,String product2,String product3) throws Exception {
		login();
		for (int i = 0; i < 200; i++) {
		commonStockAdjustmentMethod01();
		commonStockAdjustmentMethod02();
		}
	}
	
	public void commonStockAdjustmentMethod01() throws Exception {
		InventoryAndWarehouseModule obj = new InventoryAndWarehouseModule();
		obj.waitImplicit(navigation_pane, 10);
		click(navigation_pane);
		Thread.sleep(3000);
		click(inventory_module);
		click(btn_stockAdjustment);
		Thread.sleep(3000);
		click(btn_newStockAdjustment);
		// ware house name should be available
		sendKeys(txt_descStockAdj, descStockAdj);

		selectText(dropdown_warehouseStockAdj, warehouseStockAdj);

		/* Product add to the grid */
		int j = 0;
		for (int i = 1; i < 4; i++) {
			String str_i = String.valueOf(i);
			click(btn_productLookupStockAdjForLoop.replace("row", str_i));
			Thread.sleep(3000);
			String product = "";

			if (i == 1) {
				product = "LotFifo_IN_IO_031";
			} else if (i == 2) {
				product = "BatchFifo_IN_IO_031";
			} else if (i == 3) {
				product = "SerielFifo_IN_IO_031";
			} else if (i == 4) {
				product = "";
			} else if (i == 5) {
				product = "";
			} else if (i == 6) {
				product = "";
			} else if (i == 7) {
				product = "";
			} else {
				System.out.println("Product type cannt identified.........");
			}
			Thread.sleep(3000);
			String pro = obj.readTestCreation(product);
			sendKeys(txt_productSearch,pro);
			Thread.sleep(3000);
			pressEnter(txt_productSearch);
			Thread.sleep(5000);
			doubleClick(btn_resultProduct);
			String txt_qty = txt_qtyProductGridStockAdjustment.replace("row", str_i);
			String txt_cost = txt_costProductGridStockAdjustment.replace("row", str_i);
			
			Thread.sleep(1000);
			sendKeys(txt_qty, qtyCommonStockAdjustment);

			sendKeys(txt_cost, productCost1);

			click(btn_addNewRow);
		}

		click(btn_deleteEmotyRowCommonStockAdjustment);

	}
	public void commonStockAdjustmentMethod02() throws Exception {
		InventoryAndWarehouseModule obj = new InventoryAndWarehouseModule();
		Actions action1 = new Actions(driver);
		WebElement we1 = driver.findElement(By.xpath(btn_draft));
		action1.moveToElement(we1).build().perform();
		click(btn_draft);
		Thread.sleep(3000);
		Thread.sleep(4000);
		click(brn_serielBatchCapture);
		/* Product 02 */
		Thread.sleep(3000);
		String item_xpath;
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "2");
		click(item_xpath);

		sendKeys(txt_batchNumberCaptureILOOutboundShipment, obj.genProductId() + obj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		sendKeys(txt_lotNumberStockAdjustment,
				obj.genProductId() + obj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_expiryDateStockAdjustmentProductCapture);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_lotNumberStockAdjustment);
		Thread.sleep(3000);
		click(btn_updateCapture);
		
		/* Product 03 */
		Thread.sleep(3000);
		item_xpath = btn_itemProductCapturePage.replace("_product_number", "3");
		click(item_xpath);
		Thread.sleep(3000);
		click(chk_productCaptureRange);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$('#attrSlide-txtSerialCount').val(5)");
		String lot4111 = obj.genProductId() + obj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", "");
		String final1_lot_script11 = "$('#attrSlide-txtSerialFrom').val(\"" + lot4111 + "\")";
		jse.executeScript(final1_lot_script11);
		sendKeys(txt_searielLotNo, obj.genProductId() + obj.currentTimeAndDate().replaceAll("/", "").replaceAll(":", ""));
		click(txt_dateSerielProductProductCapturePage);
		Thread.sleep(200);
		doubleClick(btn_clanderFw);
		Thread.sleep(200);
		click("28_Link");
		pressEnter(txt_dateSerielProductProductCapturePage);
		Thread.sleep(3000);
		click(btn_updateCapture);
		Thread.sleep(3000);
		click(btn_productCaptureBackButton);
		Thread.sleep(1000);
		click(btn_relese);
	}
	
	
}
