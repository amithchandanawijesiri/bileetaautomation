package bileeta.BTAF.PageObjects;

import java.awt.Desktop;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.jsoup.select.Evaluator.ContainsOwnText;
import org.jsoup.select.Evaluator.ContainsText;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.ClickAction;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import bileeta.BATF.Pages.FinanceModuleData;
import bileeta.BTAF.Utilities.TestBase;
import net.bytebuddy.agent.builder.AgentBuilder.RedefinitionStrategy.DiscoveryStrategy.Explicit;

public class FinanceModuleRegression3 extends FinanceModuleData {
	
	public static String IPANo1,IPANo2,IPANo3,IPANo4,IPNo1,IPNo2,IPNo3,IPNo4,BDNo1,BDNo2,OPANo1,OPANo2,OPNo1,OPNo2,BANo,BRNo;

	public void navigateToTheLoginPage() throws Exception  {
		openPage(siteUrlA);

		writeTestResults("Verify that 'Entution' page is available",
				"user should be able to see 'Entution' page", "'Entution' page is dislayed", "pass");

	}

	public void verifyTheLogo() throws Exception {
		if (isDisplayed(siteLogoA)) {

			writeTestResults("Verify that 'Entution' header is available on the page","user should be able to see the logo", "logo is dislayed", "pass");
		} else {
			writeTestResults("Verify that 'Entution' header is available on the page","user should be able to see the logo", "logo is not dislayed", "fail");

		}
	}

	public void userLogin() throws Exception {
		
		WaitElement(txt_userNameA);
		sendKeys(txt_userNameA, userNameDataReg);
		sendKeys(txt_passwordA, passwordDataReg);
		
		WaitClick(btn_loginA);
		click(btn_loginA);

		WaitElement(headerLinkA);
		if (isDisplayed(headerLinkA)) {

			writeTestResults("User login", "User should navigate to the Home page of Entution",
					"user navigated to the home page", "pass");
		}

		else {
			writeTestResults("User login", "User should navigate to the Home page of Entution",
					"user cant navigate to the home page", "fail");

		}
	}
	
	public void userLoginNew() throws Exception {
		
		WaitElement(txt_username);
		sendKeys(txt_userNameA, NewUserNameA);
		sendKeys(txt_passwordA, NewPasswordA);
		
		WaitClick(btn_loginA);
		click(btn_loginA);

		WaitElement(headerLinkA);
		if (isDisplayed(headerLinkA)) {

			writeTestResults("User login", "User should navigate to the Home page of Entution",
					"user navigated to the home page", "pass");
			System.out.println("Com_TC_001 Test case is Completed");
		}

		else {
			writeTestResults("User login", "User should navigate to the Home page of Entution",
					"user cant navigate to the home page", "fail");

		}
	}

	public void clickNavigation() throws Exception {
	
		WaitElement(btn_navbuttonA);
		click(btn_navbuttonA);
		
		WaitElement(sidemenuA);
		if(isDisplayed(sidemenuA)) {
			writeTestResults("verify sidemenu","view sidemenu", "sidemenu is display", "pass");
		}else {
			writeTestResults("verify sidemenu","view sidemenu", "sidemenu is not display", "fail");
		}
			
	}
	
	public void ClickFinanceButton() throws Exception {
		
		WaitClick(btn_FinanceBtnA);
		click(btn_FinanceBtnA);
		
		WaitElement(subsidemenuA);
		if(isDisplayed(subsidemenuA)) {
			writeTestResults("verify subsideMenu","view subsideMenu", "subsideMenu is display", "pass");
		}else {
			writeTestResults("verify subsideMenu","view subsideMenu", "subsideMenu is not display", "fail");
		}
	}
	
	//FIN_18_1to18_12
	public void NavigationToJournalEntryNewPage() throws Exception {
		
		WaitElement(btn_JournalEntryBtnA);
		click(btn_JournalEntryBtnA);
		WaitElement(btn_NewJournalEntryBtnA);
		click(btn_NewJournalEntryBtnA);
		
		//===================================================FIN_18_1
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Journal Entry - New")) {
			writeTestResults("Navigation to Journal Entry- New page"
					,"User can Navigation to Journal Entry- New page"
					, "User can Navigation to Journal Entry- New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Journal Entry- New page"
					,"User can Navigation to Journal Entry- New page"
					, "User can Navigation to Journal Entry- New page Fail", "fail");
		}
		
		//===================================================FIN_18_2
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$(closeMessageBox()).click()");
		
		WaitElement(btn_TableErrorBtnA);
		click(btn_TableErrorBtnA);
		
		WaitElement(txt_TableErrorTxt1A);
		if(isDisplayed(txt_TableErrorTxt1A) && isDisplayed(txt_TableErrorTxt2A) && isDisplayed(txt_TableErrorTxt3A)) {
			writeTestResults("Verify whether all Mandatory fields are marked clearly"
					,"all Mandatory fields are marked clearly"
					, "all Mandatory fields are marked clearly Successfully", "pass");
		}else {
			writeTestResults("Verify whether all Mandatory fields are marked clearly"
					,"all Mandatory fields are marked clearly"
					, "all Mandatory fields are marked clearly Fail", "fail");
		}
		
		//===================================================FIN_18_3
		
		pageRefersh();
		
		WaitElement(btn_GLAccBtnA);
		click(btn_GLAccBtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		
		WaitElement(txt_GLAccSelTableValueA);
		if(getText(txt_GLAccSelTableValueA).contentEquals(GLAccTxtA)) {
			writeTestResults("Verify that user can select a GL Account using the GL Account Lookup"
					,"user can select a GL Account using the GL Account Lookup"
					, "user can select a GL Account using the GL Account Lookup Successfully", "pass");
		}else {
			writeTestResults("Verify that user can select a GL Account using the GL Account Lookup"
					,"user can select a GL Account using the GL Account Lookup"
					, "user can select a GL Account using the GL Account Lookup Fail", "fail");
		}
		
		//===================================================FIN_18_4
		
		WaitElement(txt_NarrationTxtA);
		sendKeys(txt_NarrationTxtA, "A11@#;");
		
		String NarrationTxtA= driver.findElement(By.xpath(txt_NarrationTxtA)).getAttribute("value");
//		System.out.println(NarrationTxtA);
		
		WaitElement(txt_NarrationTxtA);
		if(NarrationTxtA.contentEquals("A11@#;")) {
			writeTestResults("Verify that user can characters and special characters to Narration"
					,"user can characters and special characters to Narration"
					, "user can characters and special characters to Narration Successfully", "pass");
		}else {
			writeTestResults("Verify that user can characters and special characters to Narration"
					,"user can characters and special characters to Narration"
					, "user can characters and special characters to Narration Fail", "fail");
		}
		
		//===================================================FIN_18_5
		
		WaitElement(txt_DocValueTxtA);
		sendKeys(txt_DocValueTxtA, "Aasd@#;");
		
		String DocValueTxt1A= driver.findElement(By.xpath(txt_DocValueTxtA)).getAttribute("value");
//		System.out.println(DocValueTxtA);
		
		WaitElement(txt_DocValueTxtA);
		if(!DocValueTxt1A.contentEquals("Aasd@#;")) {
			writeTestResults("Verify that alphabatics or any special characters can't be include in Doc Value"
					,"alphabatics or any special characters can't be include in Doc Value"
					, "alphabatics or any special characters can't be include in Doc Value Successfully", "pass");
		}else {
			writeTestResults("Verify that alphabatics or any special characters can't be include in Doc Value"
					,"alphabatics or any special characters can't be include in Doc Value"
					, "alphabatics or any special characters can't be include in Doc Value Fail", "fail");
		}
		
		//===================================================FIN_18_6
		
		WaitElement(txt_DocValueTxtA);
		sendKeys(txt_DocValueTxtA, "1.00");
		
		String DocValueTxt2A= driver.findElement(By.xpath(txt_DocValueTxtA)).getAttribute("value");
//		System.out.println(DocValueTxt2A);
		
		WaitElement(txt_DocValueTxtA);
		if(DocValueTxt2A.contentEquals("1.00")) {
			writeTestResults("Verify that Any Numenric letters can be added in Doc Value field"
					,"Any Numenric letters can be added in Doc Value field"
					, "Any Numenric letters can be added in Doc Value field Successfully", "pass");
		}else {
			writeTestResults("Verify that Any Numenric letters can be added in Doc Value field"
					,"Any Numenric letters can be added in Doc Value field"
					, "Any Numenric letters can be added in Doc Value field Fail", "fail");
		}
		
		//===================================================FIN_18_7
		
		WaitElement(txt_NarrationTxtA);
		sendKeys(txt_NarrationTxtA, "@1");
		
		WaitElement(txt_DocValueTxtA);
		sendKeys(txt_DocValueTxtA, "100.00");
		
		WaitElement(btn_SummaryTabA);
		click(btn_SummaryTabA);
		
		String DebitValueA= driver.findElement(By.xpath(txt_DebitValueA)).getAttribute("value");
//		System.out.println(DebitValueA);
		
		WaitElement(txt_DebitValueA);
		if(DebitValueA.contentEquals("100.00")) {
			writeTestResults("Verify that entered positive value will automatically updated in Debit field"
					,"entered positive value will automatically updated in Debit field"
					, "entered positive value will automatically updated in Debit field Successfully", "pass");
		}else {
			writeTestResults("Verify that entered positive value will automatically updated in Debit field"
					,"entered positive value will automatically updated in Debit field"
					, "entered positive value will automatically updated in Debit field Fail", "fail");
		}
		
		//===================================================FIN_18_8
		
		WaitElement(btn_TablePlusBtn1A);
		click(btn_TablePlusBtn1A);
		
		WaitElement(btn_GLAccBtn2A);
		click(btn_GLAccBtn2A);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_NarrationTxt2A);
		sendKeys(txt_NarrationTxt2A, "@2");
		
		WaitElement(txt_DocValueTxt2A);
		sendKeys(txt_DocValueTxt2A, "-100.00");
		
		WaitElement(btn_SummaryTabA);
		click(btn_SummaryTabA);
		
		String CreditValueA= driver.findElement(By.xpath(txt_CreditValueA)).getAttribute("value");
//		System.out.println(CreditValueA);
		
		WaitElement(txt_DebitValueA);
		if(CreditValueA.contentEquals("100.00")) {
			writeTestResults("Verify that entered negative value will automatically updated in Debit field"
					,"entered negative value will automatically updated in Debit field"
					, "entered negative value will automatically updated in Debit field Successfully", "pass");
		}else {
			writeTestResults("Verify that entered negative value will automatically updated in Debit field"
					,"entered negative value will automatically updated in Debit field"
					, "entered negative value will automatically updated in Debit field Fail", "fail");
		}
		
		//===================================================FIN_18_09
		
		WaitElement(txt_DocValueTxt2A);
		sendKeys(txt_DocValueTxt2A, "-200.00");
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		jse.executeScript("$(closeMessageBox()).click()");
		
		WaitElement(btn_MainTableErrorBtnA);
		click(btn_MainTableErrorBtnA);
		
		WaitElement(txt_TableErrorTxt4A);
		if(isDisplayed(txt_TableErrorTxt4A)) {
			writeTestResults("Verify that Debit & Credit amount should be equal to draft the document"
					,"Debit & Credit amount should be equal to draft the document"
					, "Debit & Credit amount should be equal to draft the document Successfully", "pass");
		}else {
			writeTestResults("Verify that Debit & Credit amount should be equal to draft the document"
					,"Debit & Credit amount should be equal to draft the document"
					, "Debit & Credit amount should be equal to draft the document Fail", "fail");
		}
		
		//===================================================FIN_18_10
		
		WaitElement(txt_DocValueTxt2A);
		sendKeys(txt_DocValueTxt2A, "-100.00");
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft"
					,"user can use Draft"
					, "user can use Draft Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft"
					,"user can use Draft"
					, "user can use Draft Fail", "fail");
		}
		
		Thread.sleep(2000);
		String JENo= getText(txt_HeaderDocNoA);
		
		//===================================================FIN_18_11
		
		WaitElement(btn_EditBtnA);
		click(btn_EditBtnA);
		
		WaitElement(txt_DocValueTxtA);
		sendKeys(txt_DocValueTxtA, "200.00");
		
		WaitElement(txt_DocValueTxt2A);
		sendKeys(txt_DocValueTxt2A, "-200.00");
		
		WaitElement(btn_UpdateBtnA);
		click(btn_UpdateBtnA);
		
		String DebitValueAfterUpdateA= driver.findElement(By.xpath(txt_DocValueDebitTxtA)).getAttribute("value");
		String CreditValueAfterUpdateA= driver.findElement(By.xpath(txt_DocValueCreditTxtA)).getAttribute("value");
		
		if(DebitValueAfterUpdateA.contentEquals("200.000000") && CreditValueAfterUpdateA.contentEquals("-200.000000")) {
			writeTestResults("Verify that user can edit drafted Journal Entry (Update)"
					,"user can edit drafted Journal Entry (Update)"
					, "user can edit drafted Journal Entry (Update) Successfully", "pass");
		}else {
			writeTestResults("Verify that user can edit drafted Journal Entry (Update)"
					,"user can edit drafted Journal Entry (Update)"
					, "user can edit drafted Journal Entry (Update) Fail", "fail");
		}
		
		//===================================================FIN_18_12
		
		WaitElement(btn_EditBtnA);
		click(btn_EditBtnA);
		
		WaitElement(txt_DocValueTxtA);
		sendKeys(txt_DocValueTxtA, "300.00");
		
		WaitElement(txt_DocValueTxt2A);
		sendKeys(txt_DocValueTxt2A, "-300.00");
		
		WaitElement(btn_UpdateAndNewBtnA);
		click(btn_UpdateAndNewBtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Journal Entry - New")) {
			writeTestResults("Navigation to Journal Entry- New page"
					,"User can Navigation to Journal Entry- New page"
					, "User can Navigation to Journal Entry- New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Journal Entry- New page"
					,"User can Navigation to Journal Entry- New page"
					, "User can Navigation to Journal Entry- New page Fail", "fail");
		}
		
		WaitElement(btn_JournalEntryByPageBtnA);
		click(btn_JournalEntryByPageBtnA);
		
		WaitElement(txt_JournalEntryByPageSearchTxtA);
		sendKeys(txt_JournalEntryByPageSearchTxtA, JENo);
		pressEnter(txt_JournalEntryByPageSearchTxtA);
		WaitElement(sel_JournalEntryByPageSearchSelA.replace("JENo", JENo));
		doubleClick(sel_JournalEntryByPageSearchSelA.replace("JENo", JENo));
		
		String DebitValueAfterUpdateAndNewA= driver.findElement(By.xpath(txt_DocValueDebitTxtA)).getAttribute("value");
		String CreditValueAfterUpdateAndNewA= driver.findElement(By.xpath(txt_DocValueCreditTxtA)).getAttribute("value");
		
		if(DebitValueAfterUpdateAndNewA.contentEquals("300.000000") && CreditValueAfterUpdateAndNewA.contentEquals("-300.000000")) {
			writeTestResults("Verify that user can edit drafted Journal Entry (Update & New)"
					,"user can edit drafted Journal Entry (Update & New)"
					, "user can edit drafted Journal Entry (Update & New) Successfully", "pass");
		}else {
			writeTestResults("Verify that user can edit drafted Journal Entry (Update & New)"
					,"user can edit drafted Journal Entry (Update & New)"
					, "user can edit drafted Journal Entry (Update & New) Fail", "fail");
		}
	}
	
	//FIN_18_13to18_14
	public void VerifyThatUserCanDuplicateaDraftedJournalEntry() throws Exception {
		
		WaitElement(btn_JournalEntryBtnA);
		click(btn_JournalEntryBtnA);
		WaitElement(btn_NewJournalEntryBtnA);
		click(btn_NewJournalEntryBtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Journal Entry - New")) {
			writeTestResults("Navigation to Journal Entry- New page"
					,"User can Navigation to Journal Entry- New page"
					, "User can Navigation to Journal Entry- New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Journal Entry- New page"
					,"User can Navigation to Journal Entry- New page"
					, "User can Navigation to Journal Entry- New page Fail", "fail");
		}
		
		WaitElement(btn_GLAccBtnA);
		click(btn_GLAccBtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_NarrationTxtA);
		sendKeys(txt_NarrationTxtA, "@1");
		
		WaitElement(txt_DocValueTxtA);
		sendKeys(txt_DocValueTxtA, "100.00");
		
		WaitElement(btn_TablePlusBtn1A);
		click(btn_TablePlusBtn1A);
		
		WaitElement(btn_GLAccBtn2A);
		click(btn_GLAccBtn2A);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_NarrationTxt2A);
		sendKeys(txt_NarrationTxt2A, "@2");
		
		WaitElement(txt_DocValueTxt2A);
		sendKeys(txt_DocValueTxt2A, "-100.00");
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft"
					,"user can use Draft"
					, "user can use Draft Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft"
					,"user can use Draft"
					, "user can use Draft Fail", "fail");
		}
		
		//===================================================FIN_18_13
		
		WaitElement(btn_DuplicateBtnA);
		click(btn_DuplicateBtnA);
		
		WaitElement(txt_GLAccSelTableValueA);
		String DuplicateGLAccSelTableValue1A= getText(txt_GLAccSelTableValueA);
		String DuplicateGLAccSelTableValue2A= getText(txt_GLAccSelTableValueA);
		String DuplicateNarrationTxt1A= driver.findElement(By.xpath(txt_NarrationTxtA)).getAttribute("value");
		String DuplicateNarrationTxt2A= driver.findElement(By.xpath(txt_NarrationTxt2A)).getAttribute("value");
		String DuplicateDebitValueTxtA= driver.findElement(By.xpath(txt_DocValueTxtA)).getAttribute("value");
		String DuplicateCreditValueTxtA= driver.findElement(By.xpath(txt_DocValueTxt2A)).getAttribute("value");
		
		if(DuplicateGLAccSelTableValue1A.contentEquals(GLAccTxtA) && DuplicateGLAccSelTableValue2A.contentEquals(GLAccTxtA) &&
				DuplicateNarrationTxt1A.contentEquals("@1") && DuplicateNarrationTxt2A.contentEquals("@2") && 
				DuplicateDebitValueTxtA.contentEquals("100.00") && DuplicateCreditValueTxtA.contentEquals("-100.00")) {
			writeTestResults("Verify that user can Duplicate a Drafted Journal Entry"
					,"user can Duplicate a Drafted Journal Entry"
					, "user can Duplicate a Drafted Journal Entry Successfully", "pass");
		}else {
			writeTestResults("Verify that user can Duplicate a Drafted Journal Entry"
					,"user can Duplicate a Drafted Journal Entry"
					, "user can Duplicate a Drafted Journal Entry Fail", "fail");
		}
		
		//===================================================FIN_18_14
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft"
					,"user can use Draft"
					, "user can use Draft Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft"
					,"user can use Draft"
					, "user can use Draft Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_DeleteBtnA);
		click(btn_DeleteBtnA);
		WaitElement(btn_YesBtnA);
		click(btn_YesBtnA);
		
		WaitElement(txt_pageDeleteA);
		if(isDisplayed(txt_pageDeleteA)) {
			writeTestResults("Verify that user can Delete a Drafted Journal Entry"
					,"user can Delete a Drafted Journal Entry"
					, "user can Delete a Drafted Journal Entry Successfully", "pass");
		}else {
			writeTestResults("Verify that user can Delete a Drafted Journal Entry"
					,"user can Delete a Drafted Journal Entry"
					, "user can Delete a Drafted Journal Entry Fail", "fail");
		}
	}
	
	//FIN_18_15
	public void VerifyThatUserCanViewHistoryOfaDraftedJournalEntry() throws Exception {
		
		WaitElement(btn_JournalEntryBtnA);
		click(btn_JournalEntryBtnA);
		WaitElement(btn_NewJournalEntryBtnA);
		click(btn_NewJournalEntryBtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Journal Entry - New")) {
			writeTestResults("Navigation to Journal Entry- New page"
					,"User can Navigation to Journal Entry- New page"
					, "User can Navigation to Journal Entry- New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Journal Entry- New page"
					,"User can Navigation to Journal Entry- New page"
					, "User can Navigation to Journal Entry- New page Fail", "fail");
		}
		
		WaitElement(btn_GLAccBtnA);
		click(btn_GLAccBtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_NarrationTxtA);
		sendKeys(txt_NarrationTxtA, "@1");
		
		WaitElement(txt_DocValueTxtA);
		sendKeys(txt_DocValueTxtA, "100.00");
		
		WaitElement(btn_TablePlusBtn1A);
		click(btn_TablePlusBtn1A);
		
		WaitElement(btn_GLAccBtn2A);
		click(btn_GLAccBtn2A);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_NarrationTxt2A);
		sendKeys(txt_NarrationTxt2A, "@2");
		
		WaitElement(txt_DocValueTxt2A);
		sendKeys(txt_DocValueTxt2A, "-100.00");
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft"
					,"user can use Draft"
					, "user can use Draft Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft"
					,"user can use Draft"
					, "user can use Draft Fail", "fail");
		}
		
		//===================================================FIN_18_15
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		
		WaitElement(btn_HistoryBtnA);
		click(btn_HistoryBtnA);
		
		WaitElement(txt_DraftHistoryTxtA);
		if(isDisplayed(txt_DraftHistoryTxtA)) {
			writeTestResults("Verify that user can view history of a Drafted Journal Entry"
					,"user can view history of a Drafted Journal Entry"
					, "user can view history of a Drafted Journal Entry Successfully", "pass");
		}else {
			writeTestResults("Verify that user can view history of a Drafted Journal Entry"
					,"user can view history of a Drafted Journal Entry"
					, "user can view history of a Drafted Journal Entry Fail", "fail");
		}
	}
	
	//FIN_18_16
	public void VerifyThatUserCanUseDraftAndNew() throws Exception {
		
		WaitElement(btn_JournalEntryBtnA);
		click(btn_JournalEntryBtnA);
		WaitElement(btn_NewJournalEntryBtnA);
		click(btn_NewJournalEntryBtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Journal Entry - New")) {
			writeTestResults("Navigation to Journal Entry- New page"
					,"User can Navigation to Journal Entry- New page"
					, "User can Navigation to Journal Entry- New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Journal Entry- New page"
					,"User can Navigation to Journal Entry- New page"
					, "User can Navigation to Journal Entry- New page Fail", "fail");
		}
		
		WaitElement(btn_GLAccBtnA);
		click(btn_GLAccBtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_NarrationTxtA);
		sendKeys(txt_NarrationTxtA, "@1");
		
		WaitElement(txt_DocValueTxtA);
		sendKeys(txt_DocValueTxtA, "100.00");
		
		WaitElement(btn_TablePlusBtn1A);
		click(btn_TablePlusBtn1A);
		
		WaitElement(btn_GLAccBtn2A);
		click(btn_GLAccBtn2A);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_NarrationTxt2A);
		sendKeys(txt_NarrationTxt2A, "@2");
		
		WaitElement(txt_DocValueTxt2A);
		sendKeys(txt_DocValueTxt2A, "-100.00");
		
		//===================================================FIN_18_16
		
		WaitElement(btn_DraftAndNewBtnA);
		click(btn_DraftAndNewBtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Journal Entry - New")) {
			writeTestResults("Navigation to Journal Entry- New page"
					,"User can Navigation to Journal Entry- New page"
					, "User can Navigation to Journal Entry- New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Journal Entry- New page"
					,"User can Navigation to Journal Entry- New page"
					, "User can Navigation to Journal Entry- New page Fail", "fail");
		}
		
		Thread.sleep(3000);
		
		WaitElement(btn_GLAccBtnA);
		click(btn_GLAccBtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_NarrationTxtA);
		sendKeys(txt_NarrationTxtA, "@1");
		
		WaitElement(txt_DocValueTxtA);
		sendKeys(txt_DocValueTxtA, "100.00");
		
		WaitElement(btn_TablePlusBtn1A);
		click(btn_TablePlusBtn1A);
		
		WaitElement(btn_GLAccBtn2A);
		click(btn_GLAccBtn2A);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_NarrationTxt2A);
		sendKeys(txt_NarrationTxt2A, "@2");
		
		WaitElement(txt_DocValueTxt2A);
		sendKeys(txt_DocValueTxt2A, "-100.00");
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft and New"
					,"user can use Draft and New"
					, "user can use Draft and New Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft and New"
					,"user can use Draft and New"
					, "user can use Draft and New Fail", "fail");
		}
	}
	
	//FIN_18_17
	public void VerifyNewJournalEntryCanBeGenerateViaTheCopyFromOption() throws Exception {
		
		WaitElement(btn_JournalEntryBtnA);
		click(btn_JournalEntryBtnA);
		WaitElement(btn_NewJournalEntryBtnA);
		click(btn_NewJournalEntryBtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Journal Entry - New")) {
			writeTestResults("Navigation to Journal Entry- New page"
					,"User can Navigation to Journal Entry- New page"
					, "User can Navigation to Journal Entry- New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Journal Entry- New page"
					,"User can Navigation to Journal Entry- New page"
					, "User can Navigation to Journal Entry- New page Fail", "fail");
		}
		
		WaitElement(btn_GLAccBtnA);
		click(btn_GLAccBtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_NarrationTxtA);
		sendKeys(txt_NarrationTxtA, "@1");
		
		WaitElement(txt_DocValueTxtA);
		sendKeys(txt_DocValueTxtA, "100.00");
		
		WaitElement(btn_TablePlusBtn1A);
		click(btn_TablePlusBtn1A);
		
		WaitElement(btn_GLAccBtn2A);
		click(btn_GLAccBtn2A);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_NarrationTxt2A);
		sendKeys(txt_NarrationTxt2A, "@2");
		
		WaitElement(txt_DocValueTxt2A);
		sendKeys(txt_DocValueTxt2A, "-100.00");
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft"
					,"user can use Draft"
					, "user can use Draft Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft"
					,"user can use Draft"
					, "user can use Draft Fail", "fail");
		}
		
		WaitElement(txt_HeaderDocNoA);
		String JENo = getText(txt_HeaderDocNoA);
		
		WaitElement(btn_JournalEntryByPageBtnA);
		click(btn_JournalEntryByPageBtnA);
		WaitElement(btn_NewJournalEntryBtnA);
		click(btn_NewJournalEntryBtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Journal Entry - New")) {
			writeTestResults("Navigation to Journal Entry- New page"
					,"User can Navigation to Journal Entry- New page"
					, "User can Navigation to Journal Entry- New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Journal Entry- New page"
					,"User can Navigation to Journal Entry- New page"
					, "User can Navigation to Journal Entry- New page Fail", "fail");
		}
		
		//===================================================FIN_18_17
		
		WaitElement(btn_CopyFromBtnA);
		click(btn_CopyFromBtnA);
		WaitElement(txt_CopyFromSearchTxtA);
		sendKeys(txt_CopyFromSearchTxtA, JENo);
		pressEnter(txt_CopyFromSearchTxtA);
		WaitElement(sel_CopyFromSearchSelA.replace("JENo", JENo));
		doubleClick(sel_CopyFromSearchSelA.replace("JENo", JENo));
		
		Thread.sleep(3000);
		
		WaitElement(txt_GLAccSelTableValueA);
		String CopyFromGLAccSelTableValue1A= getText(txt_GLAccSelTableValueA);
		String CopyFromGLAccSelTableValue2A= getText(txt_GLAccSelTableValueA);
		String CopyFromNarrationTxt1A= driver.findElement(By.xpath(txt_NarrationTxtA)).getAttribute("value");
		String CopyFromNarrationTxt2A= driver.findElement(By.xpath(txt_NarrationTxt2A)).getAttribute("value");
		String CopyFromDebitValueTxtA= driver.findElement(By.xpath(txt_DocValueTxtA)).getAttribute("value");
		String CopyFromCreditValueTxtA= driver.findElement(By.xpath(txt_DocValueTxt2A)).getAttribute("value");
		
		if(CopyFromGLAccSelTableValue1A.contentEquals(GLAccTxtA) && CopyFromGLAccSelTableValue2A.contentEquals(GLAccTxtA) &&
				CopyFromNarrationTxt1A.contentEquals("@1") && CopyFromNarrationTxt2A.contentEquals("@2") && 
				CopyFromDebitValueTxtA.contentEquals("100.00") && CopyFromCreditValueTxtA.contentEquals("-100.00")) {
			writeTestResults("Verify new Journal Entry  can be generate via the 'Copy From' option"
					,"Journal Entry  can be generate via the 'Copy From' option"
					, "Journal Entry  can be generate via the 'Copy From' option Successfully", "pass");
		}else {
			writeTestResults("Verify new Journal Entry  can be generate via the 'Copy From' option"
					,"Journal Entry  can be generate via the 'Copy From' option"
					, "Journal Entry  can be generate via the 'Copy From' option Fail", "fail");
		}
	}
	
	//FIN_18_18to18_19
	public void VerifyThatUserCanReleaseJournalEntry() throws Exception {
		
		WaitElement(btn_JournalEntryBtnA);
		click(btn_JournalEntryBtnA);
		WaitElement(btn_NewJournalEntryBtnA);
		click(btn_NewJournalEntryBtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Journal Entry - New")) {
			writeTestResults("Navigation to Journal Entry- New page"
					,"User can Navigation to Journal Entry- New page"
					, "User can Navigation to Journal Entry- New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Journal Entry- New page"
					,"User can Navigation to Journal Entry- New page"
					, "User can Navigation to Journal Entry- New page Fail", "fail");
		}
		
		WaitElement(btn_GLAccBtnA);
		click(btn_GLAccBtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_NarrationTxtA);
		sendKeys(txt_NarrationTxtA, "@1");
		
		WaitElement(txt_DocValueTxtA);
		sendKeys(txt_DocValueTxtA, "100.00");
		
		WaitElement(btn_TablePlusBtn1A);
		click(btn_TablePlusBtn1A);
		
		WaitElement(btn_GLAccBtn2A);
		click(btn_GLAccBtn2A);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_NarrationTxt2A);
		sendKeys(txt_NarrationTxt2A, "@2");
		
		WaitElement(txt_DocValueTxt2A);
		sendKeys(txt_DocValueTxt2A, "-100.00");
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft"
					,"user can use Draft"
					, "user can use Draft Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft"
					,"user can use Draft"
					, "user can use Draft Fail", "fail");
		}
		
		//===================================================FIN_18_18
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release Journal Entry"
					,"user can release Journal Entry"
					, "user can release Journal Entry Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release Journal Entry"
					,"user can release Journal Entry"
					, "user can release Journal Entry Fail", "fail");
		}
		
		//===================================================FIN_18_19
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ReverseBtnA);
		click(btn_ReverseBtnA);
		WaitElement(btn_ReverseLookupBtnA);
		click(btn_ReverseLookupBtnA);
		WaitElement(btn_YesBtnA);
		click(btn_YesBtnA);
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_HistoryBtnA);
		click(btn_HistoryBtnA);
		
		WaitElement(txt_ReverseHistoryTxtA);
		if(isDisplayed(txt_ReverseHistoryTxtA)) {
			writeTestResults("Verify that user can Reverse Journal Entry"
					,"user can Reverse Journal Entry"
					, "user can Reverse Journal Entry Successfully", "pass");
		}else {
			writeTestResults("Verify that user can Reverse Journal Entry"
					,"user can Reverse Journal Entry"
					, "user can Reverse Journal Entry Fail", "fail");
		}
		
	}
	
	//FIN_18_20
	public void VerifyThatInactiveGLAccountsAreNotViewableInTheGLAccountLookup() throws Exception {
		
		WaitElement(btn_JournalEntryBtnA);
		click(btn_JournalEntryBtnA);
		WaitElement(btn_NewJournalEntryBtnA);
		click(btn_NewJournalEntryBtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Journal Entry - New")) {
			writeTestResults("Navigation to Journal Entry- New page"
					,"User can Navigation to Journal Entry- New page"
					, "User can Navigation to Journal Entry- New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Journal Entry- New page"
					,"User can Navigation to Journal Entry- New page"
					, "User can Navigation to Journal Entry- New page Fail", "fail");
		}
		
		//===================================================FIN_18_20
		
		WaitElement(btn_GLAccBtnA);
		click(btn_GLAccBtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtInactiveA);
		pressEnter(txt_GLAccTxtA);
		
		if(!isDisplayed(sel_GLAccSelA.replace("GLName", GLAccTxtInactiveA))) {
			writeTestResults("Verify that Inactive GL accounts are not viewable in the GL account lookup"
					,"Inactive GL accounts are not viewable in the GL account lookup"
					, "Inactive GL accounts are not viewable in the GL account lookup Successfully", "pass");
		}else {
			writeTestResults("Verify that Inactive GL accounts are not viewable in the GL account lookup"
					,"Inactive GL accounts are not viewable in the GL account lookup"
					, "Inactive GL accounts are not viewable in the GL account lookup Fail", "fail");
		}
	}
	
	//FIN_18_27
	public void VerifyTheCostObjectOfCreatedPurchaseInvoice() throws Exception {
		
		WaitElement(btn_ProcumentBtnA);
		click(btn_ProcumentBtnA);
		WaitElement(btn_PurchaseInvoiceBtnA);
		click(btn_PurchaseInvoiceBtnA);
		WaitElement(btn_NewPurchaseInvoiceBtnA);
		click(btn_NewPurchaseInvoiceBtnA);
		WaitElement(btn_PurchaseInvoiceServiceJourneyA);
		click(btn_PurchaseInvoiceServiceJourneyA);
		
		if(getText(txt_PageHeaderA).contentEquals("Purchase Invoice - New")) {
			writeTestResults("Verify New Purchase Invoice button functionality & Verify navigation to new form"
					,"New Purchase Invoice button functionality & navigation to new form"
					, "New Purchase Invoice button functionality & navigation to new form Successfully", "pass");
		}else {
			writeTestResults("Verify New Purchase Invoice button functionality & Verify navigation to new form"
					,"New Purchase Invoice button functionality & navigation to new form"
					, "New Purchase Invoice button functionality & navigation to new form Fail", "fail");
		}
		

//		WaitElement(btn_VendorSearchBtnA);
//		click(btn_VendorSearchBtnA);
//		WaitElement(txt_VendorSearchTxtA);
//		sendKeys(txt_VendorSearchTxtA, VendorTextFieldA);
//		pressEnter(txt_VendorSearchTxtA);
//		WaitElement(sel_VendorSearchSelA.replace("VenName", VendorTextFieldA));
//		doubleClick(sel_VendorSearchSelA.replace("VenName", VendorTextFieldA));
//		
//		
//		ScrollDownAruna();
//		
//		WaitElement(Product);
//		sendKeys(Product, TestProductTxtA);
//		Thread.sleep(4000);
//		driver.findElement(getLocator(Product)).sendKeys(Keys.ARROW_DOWN);
//		Thread.sleep(4000);
//		pressEnter(Product);
//		sendKeys(ProductQty, "10");
//		pressEnter(ProductQty);
//		
//		WaitClick(btn_btnCheckout);
//		click(btn_btnCheckout);
//		
//		WaitClick(btn_btndraft);
//		click(btn_btndraft);
//		
//		WaitElement(pageDraft);
//		if(isDisplayed(pageDraft)) {
//			writeTestResults("Verify draft Purchase Invoice"
//					,"User can draft Purchase Invoice"
//					, "User can draft Purchase Invoice Successfully", "pass");
//		}else {
//			writeTestResults("Verify draft Purchase Invoice"
//					,"User can draft Purchase Invoice"
//					, "User can draft Purchase Invoice Fail", "fail");
//		}
//		
//		WaitClick(btn_release);
//		click(btn_release);
//		Thread.sleep(2000);
//		trackCode= getText(Header);
//		
//		pageRefersh();
//		
//		WaitElement(pageRelease);
//		if(isDisplayed(pageRelease)) {
//			writeTestResults("Verify release Purchase Invoice"
//					,"User can release Purchase Invoice"
//					, "User can release Purchase Invoice Successfully", "pass");
//		}else {
//			writeTestResults("Verify release Purchase Invoice"
//					,"User can release Purchase Invoice"
//					, "User can release Purchase Invoice Fail", "fail");
//		}
	}
	
	//Inbound Payment Advice Regression
	
	//FIN_10_1
	public void VerifyWhetherUserAbleToNavigateToInboundPaymentAdvice() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		
		//===================================================FIN_10_1
		
		WaitElement(btn_InboundPaymentAdviceHeaderA);
		if(isDisplayed(btn_InboundPaymentAdviceHeaderA)) {
			writeTestResults("Verify whether user able to navigate to Inbound Payment Advice"
					,"user able to navigate to Inbound Payment Advice"
					, "user able to navigate to Inbound Payment Advice Successfully", "pass");
		}else {
			writeTestResults("Verify whether user able to navigate to Inbound Payment Advice"
					,"user able to navigate to Inbound Payment Advice"
					, "user able to navigate to Inbound Payment Advice Fail", "fail");
		}
	}
	
	//FIN_10_2
	public void VerifyWhetherIfUserDontHavePermissionToViewInboundPaymentAdviceByPageUserWantAbleToNavigateToByPage() throws Exception {
		
		//===================================================FIN_10_2
		
		if(!isDisplayed(btn_InboundPaymentAdviceBtnA)) {
			writeTestResults("Verify whether if user don't have permission to view Inbound Payment Advice by-page, user want able to navigate to by-page"
					,"if user don't have permission to view Inbound Payment Advice by-page, user want able to navigate to by-page"
					, "if user don't have permission to view Inbound Payment Advice by-page, user want able to navigate to by-page Successfully", "pass");
		}else {
			writeTestResults("Verify whether if user don't have permission to view Inbound Payment Advice by-page, user want able to navigate to by-page"
					,"if user don't have permission to view Inbound Payment Advice by-page, user want able to navigate to by-page"
					, "if user don't have permission to view Inbound Payment Advice by-page, user want able to navigate to by-page Fail", "fail");
		}
	}
	
	//FIN_10_5to10_10
	public void VerifyTheAvailableJourneiesIPA() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);
		
		//===================================================FIN_10_5
		
		if(isDisplayed(btn_CustomerAdvanceJourneyBtnA) && isDisplayed(btn_CustomerDebitMemoJourneyBtnA) 
				&& isDisplayed(btn_VendorRefundJourneyBtnA) && isDisplayed(btn_ReceiptVoucherJourneyIPABtnA)){
			writeTestResults("Verify the available journeies"
					,"available journeies"
					, "available journeies Successfully", "pass");
		}else {
			writeTestResults("Verify the available journeies"
					,"available journeies"
					, "available journeies Fail", "fail");
		}
		
		for (int i = 1; i <=2; i++) {
			WaitElement(btn_JourneyDownArrowBtnA);
			click(btn_JourneyDownArrowBtnA);
		}
		
		if(isDisplayed(btn_EmployeeReceiptAdviceJourneyBtnA) && isDisplayed(btn_CustomerRefundableDepositJourneyBtnA)
				 && isDisplayed(btn_EmployeeBalanceReceiptJourneyBtnA)){
			writeTestResults("Verify the available journeies"
					,"available journeies"
					, "available journeies Successfully", "pass");
		}else {
			writeTestResults("Verify the available journeies"
					,"available journeies"
					, "available journeies Fail", "fail");
		}
		
		//===================================================FIN_10_6
		
		for (int i = 1; i <=2; i++) {
			WaitElement(btn_JourneyUpArrowBtnA);
			click(btn_JourneyUpArrowBtnA);
		}
		
		WaitElement(btn_CustomerAdvanceJourneyBtnA);
		click(btn_CustomerAdvanceJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Customer Advance")){
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Fail", "fail");
		}
		
		//===================================================FIN_10_7
		
		WaitElement(btn_CustomerLookUpIPABtnA);
		click(btn_CustomerLookUpIPABtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxtA);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		
		WaitElement(txt_CustomerACCTxtA);
		String CustomerACCTxtA= driver.findElement(By.xpath(txt_CustomerACCTxtA)).getAttribute("value");
		
		if(CustomerACCTxtA.contentEquals(CustomerLookUpTxtA)){
			writeTestResults("Verify that user can select a Customer using the Customer Lookup"
					,"user can select a Customer using the Customer Lookup"
					, "user can select a Customer using the Customer Lookup Successfully", "pass");
		}else {
			writeTestResults("Verify that user can select a Customer using the Customer Lookup"
					,"user can select a Customer using the Customer Lookup"
					, "user can select a Customer using the Customer Lookup Fail", "fail");
		}
		
		//===================================================FIN_10_8
		
		WaitElement(txt_AmountFieldIPATxtA);
		sendKeys(txt_AmountFieldIPATxtA, "$Test");
		String AmountFieldIPATxt1A= driver.findElement(By.xpath(txt_AmountFieldIPATxtA)).getAttribute("value");
		
		if(!AmountFieldIPATxt1A.contentEquals("$Test")){
			writeTestResults("Verify that alphabatics or any special characters can't be include in Amount field"
					,"alphabatics or any special characters can't be include in Amount field"
					, "alphabatics or any special characters can't be include in Amount field Successfully", "pass");
		}else {
			writeTestResults("Verify that alphabatics or any special characters can't be include in Amount field"
					,"alphabatics or any special characters can't be include in Amount field"
					, "alphabatics or any special characters can't be include in Amount field Fail", "fail");
		}

		//===================================================FIN_10_9
		
		WaitElement(txt_AmountFieldIPATxtA);
		sendKeys(txt_AmountFieldIPATxtA, "-100");
		
		WaitElement(btn_SummaryTabIPAA);
		click(btn_SummaryTabIPAA);
		String AmountFieldIPATxt2A= driver.findElement(By.xpath(txt_AmountFieldIPATxtA)).getAttribute("value");
		
		WaitElement(txt_AmountFieldIPATxtA);
		sendKeys(txt_AmountFieldIPATxtA, "100");
		
		WaitElement(btn_SummaryTabIPAA);
		click(btn_SummaryTabIPAA);
		String AmountFieldIPATxt3A= driver.findElement(By.xpath(txt_AmountFieldIPATxtA)).getAttribute("value");
		
		if(!AmountFieldIPATxt2A.contentEquals("-100.000000") && AmountFieldIPATxt3A.contentEquals("100.000000")){
			writeTestResults("Only Positive Numeric letters can be added in Amount field"
					,"Positive Numeric letters can be added in Amount field"
					, "Positive Numeric letters can be added in Amount field Successfully", "pass");
		}else {
			writeTestResults("Only Positive Numeric letters can be added in Amount field"
					,"Positive Numeric letters can be added in Amount field"
					, "Positive Numeric letters can be added in Amount field Fail", "fail");
		}
		
		//===================================================FIN_10_10
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Fail", "fail");
		}
	}
	
	//FIN_10_11to10_12B
	public void VerifyThatUserCanConvertTheAboveAdviceToInboundPayment() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);
		
		WaitElement(btn_CustomerAdvanceJourneyBtnA);
		click(btn_CustomerAdvanceJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Customer Advance")){
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Fail", "fail");
		}
		
		WaitElement(btn_CustomerLookUpIPABtnA);
		click(btn_CustomerLookUpIPABtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxtA);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		
		WaitElement(txt_AmountFieldIPATxtA);
		sendKeys(txt_AmountFieldIPATxtA, "100");
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo = trackCode.replace(" Customer Advance", "");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Fail", "fail");
		}
		
		//===================================================FIN_10_11
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Customer Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can convert the above advice to inbound payment"
					,"user can convert the above advice to inbound payment"
					, "user can convert the above advice to inbound payment Successfully", "pass");
		}else {
			writeTestResults("Verify that user can convert the above advice to inbound payment"
					,"user can convert the above advice to inbound payment"
					, "user can convert the above advice to inbound payment Fail", "fail");
		}
		
		//===================================================FIN_10_12
		
		for (int i = 1; i <=2; i++) {
			driver.navigate().back();
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_SetOffActionBtnA);
		click(btn_SetOffActionBtnA);
		WaitElement(sel_SetOffDocSelA);
		click(sel_SetOffDocSelA);
		
		WaitElement(txt_SetOffPaidAmountTxtA);
		sendKeys(txt_SetOffPaidAmountTxtA, "50");
		WaitElement(btn_SetOffBtnA);
		click(btn_SetOffBtnA);
		
		pageRefersh();
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_SetOffDetailActionBtnA);
		click(btn_SetOffDetailActionBtnA);
		WaitElement(sel_SetOffDetailReverseDocSelA);
		click(sel_SetOffDetailReverseDocSelA);
		
		WaitElement(txt_PaidAmountTxtA);
		String PaidAmountTxtA= driver.findElement(By.xpath(txt_PaidAmountTxtA)).getAttribute("value");
//		System.out.println(PaidAmountTxtA);
		
		if(PaidAmountTxtA.contentEquals("50.000000")) {
			writeTestResults("Verify that user can set-off above executed customer advance"
					,"user can set-off above executed customer advance"
					, "user can set-off above executed customer advance Successfully", "pass");
		}else {
			writeTestResults("Verify that user can set-off above executed customer advance"
					,"user can set-off above executed customer advance"
					, "user can set-off above executed customer advance Fail", "fail");
		}
		
		//===================================================FIN_10_12B
		
		WaitElement(btn_SetOffReverseBtnA);
		click(btn_SetOffReverseBtnA);
		
		pageRefersh();
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		
		if(!isDisplayed(btn_SetOffDetailActionBtnA)) {
			writeTestResults("Verify that user can reverse the set-off value"
					,"user can reverse the set-off value"
					, "user can reverse the set-off value Successfully", "pass");
		}else {
			writeTestResults("Verify that user can reverse the set-off value"
					,"user can reverse the set-off value"
					, "user can reverse the set-off value Fail", "fail");
		}
	}
	
	//FIN_10_13
	public void VerifyThatUserCanDoCustomerPaymentVoucherToTheAboveExecutedPendingCustomerAdvance() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);
		
		WaitElement(btn_CustomerAdvanceJourneyBtnA);
		click(btn_CustomerAdvanceJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Customer Advance")){
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Fail", "fail");
		}
		
		WaitElement(btn_CustomerLookUpIPABtnA);
		click(btn_CustomerLookUpIPABtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxtA);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		
		WaitElement(txt_AmountFieldIPATxtA);
		sendKeys(txt_AmountFieldIPATxtA, "100");
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Customer Advance","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Fail", "fail");
		}
		
		
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Customer Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can convert the above advice to inbound payment"
					,"user can convert the above advice to inbound payment"
					, "user can convert the above advice to inbound payment Successfully", "pass");
		}else {
			writeTestResults("Verify that user can convert the above advice to inbound payment"
					,"user can convert the above advice to inbound payment"
					, "user can convert the above advice to inbound payment Fail", "fail");
		}
		
		//===================================================FIN_10_13
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_OutboundPaymentBtnA);
		click(btn_OutboundPaymentBtnA);
		WaitElement(btn_NewOutboundPaymentBtnA);
		click(btn_NewOutboundPaymentBtnA);
		
		WaitElement(btn_CustomerPaymentVoucherJourneyBtnA);
		click(btn_CustomerPaymentVoucherJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Outbound Payment - New Customer Payment")){
			writeTestResults("Navigation to Outbound Payment - New page"
					,"User can Navigate Outbound Payment - New page"
					, "User can Navigate Outbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Outbound Payment - New page"
					,"User can Navigate Outbound Payment - New page"
					, "User can Navigate Outbound Payment - New page Fail", "fail");
		}
		
		WaitElement(btn_CustomerLookUpBtnA);
		click(btn_CustomerLookUpBtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxtA);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		WaitElement(sel_OutboundPaymentIPASelA.replace("IPANo", IPANo));
		click(sel_OutboundPaymentIPASelA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Outbound Payment - New Customer Payment Form"
					,"user can use Draft the Outbound Payment - New Customer Payment Form"
					, "user can use Draft the Outbound Payment - New Customer Payment Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Outbound Payment - New Customer Payment Form"
					,"user can use Draft the Outbound Payment - New Customer Payment Form"
					, "user can use Draft the Outbound Payment - New Customer Payment Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can do customer payment voucher to the above executed pending customer advance"
					,"user can do customer payment voucher to the above executed pending customer advance"
					, "user can do customer payment voucher to the above executed pending customer advance Successfully", "pass");
		}else {
			writeTestResults("Verify that user can do customer payment voucher to the above executed pending customer advance"
					,"user can do customer payment voucher to the above executed pending customer advance"
					, "user can do customer payment voucher to the above executed pending customer advance Fail", "fail");
		}
	}
	
	//FIN_10_14
	public void VerifyThatOnlyPendingCustomerAdvanceAdviceCanBeViewableInCustomerPaymentVoucherForThePaymentProcess() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);
		
		WaitElement(btn_CustomerAdvanceJourneyBtnA);
		click(btn_CustomerAdvanceJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Customer Advance")){
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Fail", "fail");
		}
		
		WaitElement(btn_CustomerLookUpIPABtnA);
		click(btn_CustomerLookUpIPABtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxtA);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		
		WaitElement(txt_AmountFieldIPATxtA);
		sendKeys(txt_AmountFieldIPATxtA, "100");
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Customer Advance","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Fail", "fail");
		}
		
		
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Customer Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can convert the above advice to inbound payment"
					,"user can convert the above advice to inbound payment"
					, "user can convert the above advice to inbound payment Successfully", "pass");
		}else {
			writeTestResults("Verify that user can convert the above advice to inbound payment"
					,"user can convert the above advice to inbound payment"
					, "user can convert the above advice to inbound payment Fail", "fail");
		}
		
		for (int i = 1; i <=2; i++) {
			driver.navigate().back();
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_SetOffActionBtnA);
		click(btn_SetOffActionBtnA);
		WaitElement(sel_SetOffDocSelA);
		click(sel_SetOffDocSelA);
		
		WaitElement(txt_SetOffPaidAmountTxtA);
		sendKeys(txt_SetOffPaidAmountTxtA, "50");
		WaitElement(btn_SetOffBtnA);
		click(btn_SetOffBtnA);
		
		pageRefersh();
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_SetOffDetailActionBtnA);
		click(btn_SetOffDetailActionBtnA);
		WaitElement(sel_SetOffDetailReverseDocSelA);
		click(sel_SetOffDetailReverseDocSelA);
		
		WaitElement(txt_PaidAmountTxtA);
		String PaidAmountTxtA= driver.findElement(By.xpath(txt_PaidAmountTxtA)).getAttribute("value");
//		System.out.println(PaidAmountTxtA);
		
		if(PaidAmountTxtA.contentEquals("50.000000")) {
			writeTestResults("Verify that user can set-off above executed customer advance"
					,"user can set-off above executed customer advance"
					, "user can set-off above executed customer advance Successfully", "pass");
		}else {
			writeTestResults("Verify that user can set-off above executed customer advance"
					,"user can set-off above executed customer advance"
					, "user can set-off above executed customer advance Fail", "fail");
		}
		
		//===================================================FIN_10_14
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_OutboundPaymentBtnA);
		click(btn_OutboundPaymentBtnA);
		WaitElement(btn_NewOutboundPaymentBtnA);
		click(btn_NewOutboundPaymentBtnA);
		
		WaitElement(btn_CustomerPaymentVoucherJourneyBtnA);
		click(btn_CustomerPaymentVoucherJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Outbound Payment - New Customer Payment")){
			writeTestResults("Navigation to Outbound Payment - New page"
					,"User can Navigate Outbound Payment - New page"
					, "User can Navigate Outbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Outbound Payment - New page"
					,"User can Navigate Outbound Payment - New page"
					, "User can Navigate Outbound Payment - New page Fail", "fail");
		}
		
		WaitElement(btn_CustomerLookUpBtnA);
		click(btn_CustomerLookUpBtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxtA);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		WaitElement(sel_OutboundPaymentIPASelA.replace("IPANo", IPANo));
		click(sel_OutboundPaymentIPASelA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Outbound Payment - New Customer Payment Form"
					,"user can use Draft the Outbound Payment - New Customer Payment Form"
					, "user can use Draft the Outbound Payment - New Customer Payment Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Outbound Payment - New Customer Payment Form"
					,"user can use Draft the Outbound Payment - New Customer Payment Form"
					, "user can use Draft the Outbound Payment - New Customer Payment Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can do customer payment voucher to the above executed pending customer advance"
					,"user can do customer payment voucher to the above executed pending customer advance"
					, "user can do customer payment voucher to the above executed pending customer advance Successfully", "pass");
		}else {
			writeTestResults("Verify that user can do customer payment voucher to the above executed pending customer advance"
					,"user can do customer payment voucher to the above executed pending customer advance"
					, "user can do customer payment voucher to the above executed pending customer advance Fail", "fail");
		}
	}
	
	//FIN_10_15to10_21
	public void VerifyThatUserCanSelectaCustomerUsingTheCustomerLookup() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);

		WaitElement(btn_CustomerDebitMemoJourneyBtnA);
		click(btn_CustomerDebitMemoJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Customer Debit Memo")){
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Fail", "fail");
		}
		
		//===================================================FIN_10_15
		
		WaitElement(btn_CustomerLookUpIPABtnA);
		click(btn_CustomerLookUpIPABtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxt2A);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxt2A));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxt2A));
		
		WaitElement(txt_CustomerACCTxtA);
		String CustomerACCTxtA= driver.findElement(By.xpath(txt_CustomerACCTxtA)).getAttribute("value");
		
		if(CustomerACCTxtA.contentEquals(CustomerLookUpTxt2A)){
			writeTestResults("Verify that user can select a Customer using the Customer Lookup"
					,"user can select a Customer using the Customer Lookup"
					, "user can select a Customer using the Customer Lookup Successfully", "pass");
		}else {
			writeTestResults("Verify that user can select a Customer using the Customer Lookup"
					,"user can select a Customer using the Customer Lookup"
					, "user can select a Customer using the Customer Lookup Fail", "fail");
		}
		
		//===================================================FIN_10_16
		
		WaitElement(btn_GLAccIPABtnA);
		click(btn_GLAccIPABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		
		WaitElement(txt_GLAccSelIPATableValueA);
		if(getText(txt_GLAccSelIPATableValueA).contentEquals(GLAccTxtA)) {
			writeTestResults("Verify that user can select a GL Account using the GL Account Lookup"
					,"user can select a GL Account using the GL Account Lookup"
					, "user can select a GL Account using the GL Account Lookup Successfully", "pass");
		}else {
			writeTestResults("Verify that user can select a GL Account using the GL Account Lookup"
					,"user can select a GL Account using the GL Account Lookup"
					, "user can select a GL Account using the GL Account Lookup Fail", "fail");
		}
		
		//===================================================FIN_10_17
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "$Test");
		String AmountFieldCDMTxt1A= driver.findElement(By.xpath(txt_AmountFieldCDMTxtA)).getAttribute("value");
		
		if(!AmountFieldCDMTxt1A.contentEquals("$Test")){
			writeTestResults("Verify that alphabatics or any special characters can't be include in Amount field"
					,"alphabatics or any special characters can't be include in Amount field"
					, "alphabatics or any special characters can't be include in Amount field Successfully", "pass");
		}else {
			writeTestResults("Verify that alphabatics or any special characters can't be include in Amount field"
					,"alphabatics or any special characters can't be include in Amount field"
					, "alphabatics or any special characters can't be include in Amount field Fail", "fail");
		}

		//===================================================FIN_10_18
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "-100");
		
		WaitElement(btn_SummaryTabIPAA);
		click(btn_SummaryTabIPAA);
		String AmountFieldCDMTxt2A= driver.findElement(By.xpath(txt_AmountFieldCDMTxtA)).getAttribute("value");
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "100");
		
		WaitElement(btn_SummaryTabIPAA);
		click(btn_SummaryTabIPAA);
		String AmountFieldCDMTxt3A= driver.findElement(By.xpath(txt_AmountFieldCDMTxtA)).getAttribute("value");
		
		if(!AmountFieldCDMTxt2A.contentEquals("-100.000000") && AmountFieldCDMTxt3A.contentEquals("100.000000")){
			writeTestResults("Only Positive Numeric letters can be added in Amount field"
					,"Positive Numeric letters can be added in Amount field"
					, "Positive Numeric letters can be added in Amount field Successfully", "pass");
		}else {
			writeTestResults("Only Positive Numeric letters can be added in Amount field"
					,"Positive Numeric letters can be added in Amount field"
					, "Positive Numeric letters can be added in Amount field Fail", "fail");
		}
		
		//===================================================FIN_10_19
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Customer Debit Memo Form"
					,"user can use Draft the Customer Debit Memo Form"
					, "user can use Draft the Customer Debit Memo Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Customer Debit Memo Form"
					,"user can use Draft the Customer Debit Memo Form"
					, "user can use Draft the Customer Debit Memo Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Customer Debit Memo","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Customer Debit Memo"
					,"user can release the Customer Debit Memo"
					, "user can release the Customer Debit Memo Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Customer Debit Memo"
					,"user can release the Customer Debit Memo"
					, "user can release the Customer Debit Memo Fail", "fail");
		}
		
		//===================================================FIN_10_20
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_JournalActionBtnA);
		click(btn_JournalActionBtnA);
		
		System.out.println(getText(txt_JournalCreditTxtA));
		System.out.println(getText(txt_JournalDeditTxtA));
		
		if(getText(txt_JournalCreditTxtA).equals(getText(txt_JournalDeditTxtA))) {
			writeTestResults("Verify that user can verify the journal entry of the created customer debit memo"
					,"user can verify the journal entry of the created customer debit memo"
					, "user can verify the journal entry of the created customer debit memo Successfully", "pass");
		}else {
			writeTestResults("Verify that user can verify the journal entry of the created customer debit memo"
					,"user can verify the journal entry of the created customer debit memo"
					, "user can verify the journal entry of the created customer debit memo Fail", "fail");
		}
		
		pageRefersh();
		
		//===================================================FIN_10_21
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Customer Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can convert the above advice to inbound payment"
					,"user can convert the above advice to inbound payment"
					, "user can convert the above advice to inbound payment Successfully", "pass");
		}else {
			writeTestResults("Verify that user can convert the above advice to inbound payment"
					,"user can convert the above advice to inbound payment"
					, "user can convert the above advice to inbound payment Fail", "fail");
		}
	}
	
	//FIN_10_22to10_24
	public void VerifyThatReleasedCustomerDebitMemoCanBeSetOfUsingSetOffFunctionInAction() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);

		WaitElement(btn_CustomerDebitMemoJourneyBtnA);
		click(btn_CustomerDebitMemoJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Customer Debit Memo")){
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Fail", "fail");
		}
		
		WaitElement(btn_CustomerLookUpIPABtnA);
		click(btn_CustomerLookUpIPABtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxt2A);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxt2A));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxt2A));
		
		WaitElement(btn_GLAccIPABtnA);
		click(btn_GLAccIPABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Customer Debit Memo Form"
					,"user can use Draft the Customer Debit Memo Form"
					, "user can use Draft the Customer Debit Memo Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Customer Debit Memo Form"
					,"user can use Draft the Customer Debit Memo Form"
					, "user can use Draft the Customer Debit Memo Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Customer Debit Memo"
					,"user can release the Customer Debit Memo"
					, "user can release the Customer Debit Memo Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Customer Debit Memo"
					,"user can release the Customer Debit Memo"
					, "user can release the Customer Debit Memo Fail", "fail");
		}
		
		//===================================================FIN_10_22 and FIN_10_23
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_SetOffActionBtnA);
		click(btn_SetOffActionBtnA);
		WaitElement(sel_SetOffDocSelA);
		click(sel_SetOffDocSelA);
		
		WaitElement(txt_SetOffPaidAmountTxtA);
		sendKeys(txt_SetOffPaidAmountTxtA, "110");
		WaitElement(btn_SetOffBtnA);
		click(btn_SetOffBtnA);
		
		WaitElement(txt_SetOffValidatorA);
		if(isDisplayed(txt_SetOffValidatorA)) {
			writeTestResults("Verify that set-off value can't be exceed to the customer debit advice value"
					,"set-off value can't be exceed to the customer debit advice value"
					, "set-off value can't be exceed to the customer debit advice value Successfully", "pass");
		}else {
			writeTestResults("Verify that set-off value can't be exceed to the customer debit advice value"
					,"set-off value can't be exceed to the customer debit advice value"
					, "set-off value can't be exceed to the customer debit advice value Fail", "fail");
		}
		
		pageRefersh();
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_SetOffActionBtnA);
		click(btn_SetOffActionBtnA);
		WaitElement(sel_SetOffDocSelA);
		click(sel_SetOffDocSelA);
		
		WaitElement(txt_SetOffPaidAmountTxtA);
		sendKeys(txt_SetOffPaidAmountTxtA, "50");
		WaitElement(btn_SetOffBtnA);
		click(btn_SetOffBtnA);
		
		pageRefersh();
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_SetOffDetailActionBtnA);
		click(btn_SetOffDetailActionBtnA);
		WaitElement(sel_SetOffDetailReverseDocSelA);
		click(sel_SetOffDetailReverseDocSelA);
		
		WaitElement(txt_PaidAmountTxtA);
		String PaidAmountTxtA= driver.findElement(By.xpath(txt_PaidAmountTxtA)).getAttribute("value");
//		System.out.println(PaidAmountTxtA);
		
		if(PaidAmountTxtA.contentEquals("50.000000")) {
			writeTestResults("Verify that released customer debit memo can be set-of using set-off function in action"
					,"released customer debit memo can be set-of using set-off function in action"
					, "released customer debit memo can be set-of using set-off function in action Successfully", "pass");
		}else {
			writeTestResults("Verify that released customer debit memo can be set-of using set-off function in action"
					,"released customer debit memo can be set-of using set-off function in action"
					, "released customer debit memo can be set-of using set-off function in action Fail", "fail");
		}
		
		//===================================================FIN_10_24
		
		WaitElement(btn_SetOffReverseBtnA);
		click(btn_SetOffReverseBtnA);
		
		pageRefersh();
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		
		if(!isDisplayed(btn_SetOffDetailActionBtnA)) {
			writeTestResults("Verify that user can reverse the set-off value"
					,"user can reverse the set-off value"
					, "user can reverse the set-off value Successfully", "pass");
		}else {
			writeTestResults("Verify that user can reverse the set-off value"
					,"user can reverse the set-off value"
					, "user can reverse the set-off value Fail", "fail");
		}
	}
	
	//FIN_10_25to10_30
	public void VerifyThatUserCanSelectaVendorUsingTheVendorLookup() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);

		WaitElement(btn_VendorRefundJourneyBtnA);
		click(btn_VendorRefundJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Vendor Refund")){
			writeTestResults("Navigation to Inbound Payment Advice - New Vendor Refund page"
					,"User can Navigate Inbound Payment Advice - New Vendor Refund page"
					, "User can Navigate Inbound Payment Advice - New Vendor Refund page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Vendor Refund page"
					,"User can Navigate Inbound Payment Advice - New Vendor Refund page"
					, "User can Navigate Inbound Payment Advice - New Vendor Refund page Fail", "fail");
		}
		
		//===================================================FIN_10_25
		
		WaitElement(btn_VendorLookUpIPABtnA);
		click(btn_VendorLookUpIPABtnA);
		WaitElement(txt_VendorLookUpIPATxtA);
		sendKeys(txt_VendorLookUpIPATxtA, VendorLookUpIPATxtA);
		pressEnter(txt_VendorLookUpIPATxtA);
		WaitElement(sel_VendorLookUpIPASelA.replace("VenSel", VendorLookUpIPATxtA));
		doubleClick(sel_VendorLookUpIPASelA.replace("VenSel", VendorLookUpIPATxtA));
		
		WaitElement(txt_CustomerACCTxtA);
		String VendorTxt= driver.findElement(By.xpath(txt_CustomerACCTxtA)).getAttribute("value");
		
		if(VendorTxt.contentEquals(VendorLookUpIPATxtA)){
			writeTestResults("Verify that user can select a Vendor using the Vendor Lookup"
					,"user can select a Vendor using the Vendor Lookup"
					, "user can select a Vendor using the Vendor Lookup Successfully", "pass");
		}else {
			writeTestResults("Verify that user can select a Vendor using the Vendor Lookup"
					,"user can select a Vendor using the Vendor Lookup"
					, "user can select a Vendor using the Vendor Lookup Fail", "fail");
		}
		
		//===================================================FIN_10_26
		
		WaitElement(btn_GLAccIPABtnA);
		click(btn_GLAccIPABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		
		WaitElement(txt_GLAccSelIPATableValueA);
		if(getText(txt_GLAccSelIPATableValueA).contentEquals(GLAccTxtA)) {
			writeTestResults("Verify that user can select a GL Account using the GL Account Lookup"
					,"user can select a GL Account using the GL Account Lookup"
					, "user can select a GL Account using the GL Account Lookup Successfully", "pass");
		}else {
			writeTestResults("Verify that user can select a GL Account using the GL Account Lookup"
					,"user can select a GL Account using the GL Account Lookup"
					, "user can select a GL Account using the GL Account Lookup Fail", "fail");
		}
		
		//===================================================FIN_10_27
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "$Test");
		String AmountFieldCDMTxt1A= driver.findElement(By.xpath(txt_AmountFieldCDMTxtA)).getAttribute("value");
		
		if(!AmountFieldCDMTxt1A.contentEquals("$Test")){
			writeTestResults("Verify that alphabatics or any special characters can't be include in Amount field"
					,"alphabatics or any special characters can't be include in Amount field"
					, "alphabatics or any special characters can't be include in Amount field Successfully", "pass");
		}else {
			writeTestResults("Verify that alphabatics or any special characters can't be include in Amount field"
					,"alphabatics or any special characters can't be include in Amount field"
					, "alphabatics or any special characters can't be include in Amount field Fail", "fail");
		}
		
		//===================================================FIN_10_28
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "-100");
		
		WaitElement(btn_SummaryTabIPAA);
		click(btn_SummaryTabIPAA);
		String AmountFieldCDMTxt2A= driver.findElement(By.xpath(txt_AmountFieldCDMTxtA)).getAttribute("value");
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "100");
		
		WaitElement(btn_SummaryTabIPAA);
		click(btn_SummaryTabIPAA);
		String AmountFieldCDMTxt3A= driver.findElement(By.xpath(txt_AmountFieldCDMTxtA)).getAttribute("value");
		
		if(!AmountFieldCDMTxt2A.contentEquals("-100.000000") && AmountFieldCDMTxt3A.contentEquals("100.000000")){
			writeTestResults("Only Positive Numeric letters can be added in Amount field"
					,"Positive Numeric letters can be added in Amount field"
					, "Positive Numeric letters can be added in Amount field Successfully", "pass");
		}else {
			writeTestResults("Only Positive Numeric letters can be added in Amount field"
					,"Positive Numeric letters can be added in Amount field"
					, "Positive Numeric letters can be added in Amount field Fail", "fail");
		}
		
		//===================================================FIN_10_29
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Vendor Refund"
					,"user can use Draft the Vendor Refund"
					, "user can use Draft the Vendor Refund Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Vendor Refund Form"
					,"user can use Draft the Vendor Refund Form"
					, "user can use Draft the Vendor Refund Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Vendor Refund","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Vendor Refund"
					,"user can release the Vendor Refund"
					, "user can release the Vendor Refund Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Vendor Refund"
					,"user can release the Vendor Refund"
					, "user can release the Vendor Refund Fail", "fail");
		}
		
		//===================================================FIN_10_30
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Vendor Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Vendor Receipt Form"
					,"user can use Draft the Inbound Payment - New Vendor Receipt Form"
					, "user can use Draft the Inbound Payment - New Vendor Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Vendor Receipt Form"
					,"user can use Draft the Inbound Payment - New Vendor Receipt Form"
					, "user can use Draft the Inbound Payment - New Vendor Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can convert the above advice to inbound payment"
					,"user can convert the above advice to inbound payment"
					, "user can convert the above advice to inbound payment Successfully", "pass");
		}else {
			writeTestResults("Verify that user can convert the above advice to inbound payment"
					,"user can convert the above advice to inbound payment"
					, "user can convert the above advice to inbound payment Fail", "fail");
		}
	}
	
	//FIN_10_31to10_33
	public void VerifyThatReleasedVendorRefundCanBeSetOfUsingSetOffFunctionInAction() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);

		WaitElement(btn_VendorRefundJourneyBtnA);
		click(btn_VendorRefundJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Vendor Refund")){
			writeTestResults("Navigation to Inbound Payment Advice - New Vendor Refund page"
					,"User can Navigate Inbound Payment Advice - New Vendor Refund page"
					, "User can Navigate Inbound Payment Advice - New Vendor Refund page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Vendor Refund page"
					,"User can Navigate Inbound Payment Advice - New Vendor Refund page"
					, "User can Navigate Inbound Payment Advice - New Vendor Refund page Fail", "fail");
		}
		
		WaitElement(btn_VendorLookUpIPABtnA);
		click(btn_VendorLookUpIPABtnA);
		WaitElement(txt_VendorLookUpIPATxtA);
		sendKeys(txt_VendorLookUpIPATxtA, VendorLookUpIPATxtA);
		pressEnter(txt_VendorLookUpIPATxtA);
		WaitElement(sel_VendorLookUpIPASelA.replace("VenSel", VendorLookUpIPATxtA));
		doubleClick(sel_VendorLookUpIPASelA.replace("VenSel", VendorLookUpIPATxtA));
		
		WaitElement(btn_GLAccIPABtnA);
		click(btn_GLAccIPABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Vendor Refund"
					,"user can use Draft the Vendor Refund"
					, "user can use Draft the Vendor Refund Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Vendor Refund Form"
					,"user can use Draft the Vendor Refund Form"
					, "user can use Draft the Vendor Refund Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Vendor Refund"
					,"user can release the Vendor Refund"
					, "user can release the Vendor Refund Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Vendor Refund"
					,"user can release the Vendor Refund"
					, "user can release the Vendor Refund Fail", "fail");
		}
		
		//===================================================FIN_10_31 and FIN_10_32
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_SetOffActionBtnA);
		click(btn_SetOffActionBtnA);
		WaitElement(sel_SetOffDocSelA);
		click(sel_SetOffDocSelA);
		
		WaitElement(txt_SetOffPaidAmountTxtA);
		sendKeys(txt_SetOffPaidAmountTxtA, "110");
		WaitElement(btn_SetOffBtnA);
		click(btn_SetOffBtnA);
		
		WaitElement(txt_SetOffValidatorA);
		if(isDisplayed(txt_SetOffValidatorA)) {
			writeTestResults("Verify that set-off value can't be exceed to the vendor refund value"
					,"set-off value can't be exceed to the vendor refund value"
					, "set-off value can't be exceed to the vendor refund value Successfully", "pass");
		}else {
			writeTestResults("Verify that set-off value can't be exceed to the vendor refund value"
					,"set-off value can't be exceed to the vendor refund value"
					, "set-off value can't be exceed to the vendor refund value Fail", "fail");
		}
		
		pageRefersh();
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_SetOffActionBtnA);
		click(btn_SetOffActionBtnA);
		WaitElement(sel_SetOffDocSelA);
		click(sel_SetOffDocSelA);
		
		WaitElement(txt_SetOffPaidAmountTxtA);
		sendKeys(txt_SetOffPaidAmountTxtA, "50");
		WaitElement(btn_SetOffBtnA);
		click(btn_SetOffBtnA);
		
		pageRefersh();
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_SetOffDetailActionBtnA);
		click(btn_SetOffDetailActionBtnA);
		WaitElement(sel_SetOffDetailReverseDocSelA);
		click(sel_SetOffDetailReverseDocSelA);
		
		WaitElement(txt_PaidAmountTxtA);
		String PaidAmountTxtA= driver.findElement(By.xpath(txt_PaidAmountTxtA)).getAttribute("value");
//		System.out.println(PaidAmountTxtA);
		
		if(PaidAmountTxtA.contentEquals("50.000000")) {
			writeTestResults("Verify that released Vendor refund can be set-of using set-off function in action"
					,"released Vendor refund can be set-of using set-off function in action"
					, "released Vendor refund can be set-of using set-off function in action Successfully", "pass");
		}else {
			writeTestResults("Verify that released Vendor refund can be set-of using set-off function in action"
					,"released Vendor refund can be set-of using set-off function in action"
					, "released Vendor refund can be set-of using set-off function in action Fail", "fail");
		}
		
		//===================================================FIN_10_33
		
		WaitElement(btn_SetOffReverseBtnA);
		click(btn_SetOffReverseBtnA);
		
		pageRefersh();
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		
		if(!isDisplayed(btn_SetOffDetailActionBtnA)) {
			writeTestResults("Verify that user can reverse the set-off value"
					,"user can reverse the set-off value"
					, "user can reverse the set-off value Successfully", "pass");
		}else {
			writeTestResults("Verify that user can reverse the set-off value"
					,"user can reverse the set-off value"
					, "user can reverse the set-off value Fail", "fail");
		}
	}
	
	//FIN_10_34to10_39
	public void VerifyThatUserCanAddCharactersAndSpecialCharactersToDescription() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);

		WaitElement(btn_ReceiptVoucherJourneyIPABtnA);
		click(btn_ReceiptVoucherJourneyIPABtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Receipt Voucher")){
			writeTestResults("Navigation to Inbound Payment Advice - New Receipt Voucher page"
					,"User can Navigate Inbound Payment Advice - New Receipt Voucher page"
					, "User can Navigate Inbound Payment Advice - New Receipt Voucher page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Receipt Voucher page"
					,"User can Navigate Inbound Payment Advice - New Receipt Voucher page"
					, "User can Navigate Inbound Payment Advice - New Receipt Voucher page Fail", "fail");
		}
		
		//===================================================FIN_10_34
		
		WaitElement(txt_DescriptionIPATxtA);
		sendKeys(txt_DescriptionIPATxtA, "@Test1996");
		
		String ReceiptVoucherDescriptionTxtA=  driver.findElement(By.xpath(txt_DescriptionIPATxtA)).getAttribute("value");
		
		if(ReceiptVoucherDescriptionTxtA.contentEquals("@Test1996")){
			writeTestResults("Verify that user can add characters and special characters to Description"
					,"user can add characters and special characters to Description"
					, "user can add characters and special characters to Description Successfully", "pass");
		}else {
			writeTestResults("Verify that user can add characters and special characters to Description"
					,"user can add characters and special characters to Description"
					, "user can add characters and special characters to Description Fail", "fail");
		}
		
		//===================================================FIN_10_35
		
		WaitElement(btn_GLAccIPABtnA);
		click(btn_GLAccIPABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		
		WaitElement(txt_GLAccSelIPATableValueA);
		if(getText(txt_GLAccSelIPATableValueA).contentEquals(GLAccTxtA)) {
			writeTestResults("Verify that user can select a GL Account using the GL Account Lookup"
					,"user can select a GL Account using the GL Account Lookup"
					, "user can select a GL Account using the GL Account Lookup Successfully", "pass");
		}else {
			writeTestResults("Verify that user can select a GL Account using the GL Account Lookup"
					,"user can select a GL Account using the GL Account Lookup"
					, "user can select a GL Account using the GL Account Lookup Fail", "fail");
		}
		
		//===================================================FIN_10_36
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "$Test");
		String AmountFieldCDMTxt1A= driver.findElement(By.xpath(txt_AmountFieldCDMTxtA)).getAttribute("value");
		
		if(!AmountFieldCDMTxt1A.contentEquals("$Test")){
			writeTestResults("Verify that alphabatics or any special characters can't be include in Amount field"
					,"alphabatics or any special characters can't be include in Amount field"
					, "alphabatics or any special characters can't be include in Amount field Successfully", "pass");
		}else {
			writeTestResults("Verify that alphabatics or any special characters can't be include in Amount field"
					,"alphabatics or any special characters can't be include in Amount field"
					, "alphabatics or any special characters can't be include in Amount field Fail", "fail");
		}
		
		//===================================================FIN_10_37
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "-100");
		
		WaitElement(btn_SummaryTabIPAA);
		click(btn_SummaryTabIPAA);
		String AmountFieldCDMTxt2A= driver.findElement(By.xpath(txt_AmountFieldCDMTxtA)).getAttribute("value");
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "100");
		
		WaitElement(btn_SummaryTabIPAA);
		click(btn_SummaryTabIPAA);
		String AmountFieldCDMTxt3A= driver.findElement(By.xpath(txt_AmountFieldCDMTxtA)).getAttribute("value");
		
		if(!AmountFieldCDMTxt2A.contentEquals("-100.000000") && AmountFieldCDMTxt3A.contentEquals("100.000000")){
			writeTestResults("Only Positive Numeric letters can be added in Amount field"
					,"Positive Numeric letters can be added in Amount field"
					, "Positive Numeric letters can be added in Amount field Successfully", "pass");
		}else {
			writeTestResults("Only Positive Numeric letters can be added in Amount field"
					,"Positive Numeric letters can be added in Amount field"
					, "Positive Numeric letters can be added in Amount field Fail", "fail");
		}
		
		//===================================================FIN_10_38
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Receipt Voucher Form"
					,"user can use Draft the Receipt Voucher Form"
					, "user can use Draft the Receipt Voucher Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Receipt Voucher Form"
					,"user can use Draft the Receipt Voucher Form"
					, "user can use Draft the Receipt Voucher Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Receipt Voucher","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Receipt Voucher"
					,"user can release the Receipt Voucher"
					, "user can release the Receipt Voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Receipt Voucher"
					,"user can release the Receipt Voucher"
					, "user can release the Receipt Voucher Fail", "fail");
		}
		
		//===================================================FIN_10_39
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New GL Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New GL Receipt Form"
					,"user can use Draft the Inbound Payment - New GL Receipt Form"
					, "user can use Draft the Inbound Payment - New GL Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New GL Receipt Form"
					,"user can use Draft the Inbound Payment - New GL Receipt Form"
					, "user can use Draft the Inbound Payment - New GL Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can convert the above advice to inbound payment"
					,"user can convert the above advice to inbound payment"
					, "user can convert the above advice to inbound payment Successfully", "pass");
		}else {
			writeTestResults("Verify that user can convert the above advice to inbound payment"
					,"user can convert the above advice to inbound payment"
					, "user can convert the above advice to inbound payment Fail", "fail");
		}
	}
	
	//FIN_10_40to10_46
	public void VerifyThatUserCanSelectaEmployeeUsingTheEmployeeLookup() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);
		WaitElement(btn_JourneyDownArrowBtnA);
		click(btn_JourneyDownArrowBtnA);

		WaitElement(btn_EmployeeReceiptAdviceJourneyBtnA);
		click(btn_EmployeeReceiptAdviceJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Employee Receipt")){
			writeTestResults("Navigation to Inbound Payment Advice - New Employee Receipt page"
					,"User can Navigate Inbound Payment Advice - New Employee Receipt page"
					, "User can Navigate Inbound Payment Advice - New Employee Receipt page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Employee Receipt page"
					,"User can Navigate Inbound Payment Advice - New Employee Receipt page"
					, "User can Navigate Inbound Payment Advice - New Employee Receipt page Fail", "fail");
		}
		
		//===================================================FIN_10_40
		
		WaitElement(btn_EmployeeLookupIPABtnA);
		click(btn_EmployeeLookupIPABtnA);
		WaitElement(txt_EmployeeLookupIPATxtA);
		sendKeys(txt_EmployeeLookupIPATxtA, EmployeeLookupIPATxtA);
		pressEnter(txt_EmployeeLookupIPATxtA);
		WaitElement(sel_EmployeeLookupIPASelA.replace("EmpSel", EmployeeLookupIPATxtA));
		doubleClick(sel_EmployeeLookupIPASelA.replace("EmpSel", EmployeeLookupIPATxtA));
		
		WaitElement(txt_CustomerACCTxtA);
		String EmployeeTxt= driver.findElement(By.xpath(txt_CustomerACCTxtA)).getAttribute("value");
		
		if(EmployeeTxt.contentEquals(EmployeeLookupIPATxtA)){
			writeTestResults("Verify that user can select a Employee using the Employee Lookup"
					,"user can select a Employee using the Employee Lookup"
					, "user can select a Employee using the Employee Lookup Successfully", "pass");
		}else {
			writeTestResults("Verify that user can select a Employee using the Employee Lookup"
					,"user can select a Employee using the Employee Lookup"
					, "user can select a Employee using the Employee Lookup Fail", "fail");
		}
		
		//===================================================FIN_10_41
		
		WaitElement(txt_DescriptionIPATxtA);
		sendKeys(txt_DescriptionIPATxtA, "@Test1996");
		
		String EmployeeReceiptDescriptionTxtA=  driver.findElement(By.xpath(txt_DescriptionIPATxtA)).getAttribute("value");
		
		if(EmployeeReceiptDescriptionTxtA.contentEquals("@Test1996")){
			writeTestResults("Verify that user can add characters and special characters to Description"
					,"user can add characters and special characters to Description"
					, "user can add characters and special characters to Description Successfully", "pass");
		}else {
			writeTestResults("Verify that user can add characters and special characters to Description"
					,"user can add characters and special characters to Description"
					, "user can add characters and special characters to Description Fail", "fail");
		}
		
		//===================================================FIN_10_42
		
		WaitElement(btn_GLAccIPABtnA);
		click(btn_GLAccIPABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		
		WaitElement(txt_GLAccSelIPATableValueA);
		if(getText(txt_GLAccSelIPATableValueA).contentEquals(GLAccTxtA)) {
			writeTestResults("Verify that user can select a GL Account using the GL Account Lookup"
					,"user can select a GL Account using the GL Account Lookup"
					, "user can select a GL Account using the GL Account Lookup Successfully", "pass");
		}else {
			writeTestResults("Verify that user can select a GL Account using the GL Account Lookup"
					,"user can select a GL Account using the GL Account Lookup"
					, "user can select a GL Account using the GL Account Lookup Fail", "fail");
		}
		
		//===================================================FIN_10_43
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "$Test");
		String AmountFieldCDMTxt1A= driver.findElement(By.xpath(txt_AmountFieldCDMTxtA)).getAttribute("value");
		
		if(!AmountFieldCDMTxt1A.contentEquals("$Test")){
			writeTestResults("Verify that alphabatics or any special characters can't be include in Amount field"
					,"alphabatics or any special characters can't be include in Amount field"
					, "alphabatics or any special characters can't be include in Amount field Successfully", "pass");
		}else {
			writeTestResults("Verify that alphabatics or any special characters can't be include in Amount field"
					,"alphabatics or any special characters can't be include in Amount field"
					, "alphabatics or any special characters can't be include in Amount field Fail", "fail");
		}
		
		//===================================================FIN_10_44
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "-100");
		
		WaitElement(btn_SummaryTabIPAA);
		click(btn_SummaryTabIPAA);
		String AmountFieldCDMTxt2A= driver.findElement(By.xpath(txt_AmountFieldCDMTxtA)).getAttribute("value");
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "100");
		
		WaitElement(btn_SummaryTabIPAA);
		click(btn_SummaryTabIPAA);
		String AmountFieldCDMTxt3A= driver.findElement(By.xpath(txt_AmountFieldCDMTxtA)).getAttribute("value");
		
		if(!AmountFieldCDMTxt2A.contentEquals("-100.000000") && AmountFieldCDMTxt3A.contentEquals("100.000000")){
			writeTestResults("Only Positive Numeric letters can be added in Amount field"
					,"Positive Numeric letters can be added in Amount field"
					, "Positive Numeric letters can be added in Amount field Successfully", "pass");
		}else {
			writeTestResults("Only Positive Numeric letters can be added in Amount field"
					,"Positive Numeric letters can be added in Amount field"
					, "Positive Numeric letters can be added in Amount field Fail", "fail");
		}
		
		//===================================================FIN_10_45
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Employee Receipt Advice Form"
					,"user can use Draft the Employee Receipt Advice Form"
					, "user can use Draft the Employee Receipt Advice Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Employee Receipt Advice Form"
					,"user can use Draft the Employee Receipt Advice Form"
					, "user can use Draft the Employee Receipt Advice Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Employee Receipt","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Employee Receipt Advice"
					,"user can release the Employee Receipt Advice"
					, "user can release the Employee Receipt Advice Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Employee Receipt Advice"
					,"user can release the Employee Receipt Advice"
					, "user can release the Employee Receipt Advice Fail", "fail");
		}
		
		//===================================================FIN_10_46
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Employee Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Employee Receipt Form"
					,"user can use Draft the Inbound Payment - New Employee Receipt Form"
					, "user can use Draft the Inbound Payment - New Employee Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Employee Receipt Form"
					,"user can use Draft the Inbound Payment - New Employee Receipt Form"
					, "user can use Draft the Inbound Payment - New Employee Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can convert the above advice to inbound payment"
					,"user can convert the above advice to inbound payment"
					, "user can convert the above advice to inbound payment Successfully", "pass");
		}else {
			writeTestResults("Verify that user can convert the above advice to inbound payment"
					,"user can convert the above advice to inbound payment"
					, "user can convert the above advice to inbound payment Fail", "fail");
		}
	}
	
	//FIN_10_47to10_53
	public void VerifyThatUserCanSelectaCustomerUsingTheCustomerLookup2() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);
		WaitElement(btn_JourneyDownArrowBtnA);
		click(btn_JourneyDownArrowBtnA);

		WaitElement(btn_CustomerRefundableDepositJourneyBtnA);
		click(btn_CustomerRefundableDepositJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Customer Refundable Deposit")){
			writeTestResults("Navigation to Inbound Payment Advice - New Customer Refundable Deposit page"
					,"User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page"
					, "User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Customer Refundable Deposit page"
					,"User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page"
					, "User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page Fail", "fail");
		}
		
		//===================================================FIN_10_47
		
		WaitElement(btn_CustomerLookUpIPABtnA);
		click(btn_CustomerLookUpIPABtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxt3A);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxt3A));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxt3A));
		
		WaitElement(txt_CustomerACCTxtA);
		String CustomerACCTxtA= driver.findElement(By.xpath(txt_CustomerACCTxtA)).getAttribute("value");
		
		if(CustomerACCTxtA.contentEquals(CustomerLookUpTxt3A)){
			writeTestResults("Verify that user can select a Customer using the Customer Lookup"
					,"user can select a Customer using the Customer Lookup"
					, "user can select a Customer using the Customer Lookup Successfully", "pass");
		}else {
			writeTestResults("Verify that user can select a Customer using the Customer Lookup"
					,"user can select a Customer using the Customer Lookup"
					, "user can select a Customer using the Customer Lookup Fail", "fail");
		}
		
		//===================================================FIN_10_48
		
		WaitElement(txt_DescriptionIPATxtA);
		sendKeys(txt_DescriptionIPATxtA, "@Test1996");
		
		String CustomerRefundableDepositDescriptionTxtA=  driver.findElement(By.xpath(txt_DescriptionIPATxtA)).getAttribute("value");
		
		if(CustomerRefundableDepositDescriptionTxtA.contentEquals("@Test1996")){
			writeTestResults("Verify that user can add characters and special characters to Description"
					,"user can add characters and special characters to Description"
					, "user can add characters and special characters to Description Successfully", "pass");
		}else {
			writeTestResults("Verify that user can add characters and special characters to Description"
					,"user can add characters and special characters to Description"
					, "user can add characters and special characters to Description Fail", "fail");
		}
		
		//===================================================FIN_10_49
		
		WaitElement(txt_AmountIPATxtA);
		sendKeys(txt_AmountIPATxtA, "$Test");
		String AmountIPATxt1A= driver.findElement(By.xpath(txt_AmountIPATxtA)).getAttribute("value");
		
		if(!AmountIPATxt1A.contentEquals("$Test")){
			writeTestResults("Verify that alphabatics or any special characters can't be include in Amount field"
					,"alphabatics or any special characters can't be include in Amount field"
					, "alphabatics or any special characters can't be include in Amount field Successfully", "pass");
		}else {
			writeTestResults("Verify that alphabatics or any special characters can't be include in Amount field"
					,"alphabatics or any special characters can't be include in Amount field"
					, "alphabatics or any special characters can't be include in Amount field Fail", "fail");
		}
		
		//===================================================FIN_10_50
		
		WaitElement(txt_AmountIPATxtA);
		sendKeys(txt_AmountIPATxtA, "-100");
		
		WaitElement(btn_SummaryTabIPAA);
		click(btn_SummaryTabIPAA);
		String AmountIPATxt2A= driver.findElement(By.xpath(txt_AmountIPATxtA)).getAttribute("value");
		
		WaitElement(txt_AmountIPATxtA);
		sendKeys(txt_AmountIPATxtA, "100");
		
		WaitElement(btn_SummaryTabIPAA);
		click(btn_SummaryTabIPAA);
		String AmountIPATxt3A= driver.findElement(By.xpath(txt_AmountIPATxtA)).getAttribute("value");
		
		if(!AmountIPATxt2A.contentEquals("-100.000000") && AmountIPATxt3A.contentEquals("100.000000")){
			writeTestResults("Only Positive Numeric letters can be added in Amount field"
					,"Positive Numeric letters can be added in Amount field"
					, "Positive Numeric letters can be added in Amount field Successfully", "pass");
		}else {
			writeTestResults("Only Positive Numeric letters can be added in Amount field"
					,"Positive Numeric letters can be added in Amount field"
					, "Positive Numeric letters can be added in Amount field Fail", "fail");
		}
		
		//===================================================FIN_10_51
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the  Customer Refundable Deposit Form"
					,"user can use Draft the  Customer Refundable Deposit Form"
					, "user can use Draft the  Customer Refundable Deposit Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the  Customer Refundable Deposit Form"
					,"user can use Draft the  Customer Refundable Deposit Form"
					, "user can use Draft the  Customer Refundable Deposit Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Customer Refundable Deposit","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the  Customer Refundable Deposit"
					,"user can release the  Customer Refundable Deposit"
					, "user can release the  Customer Refundable Deposit Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the  Customer Refundable Deposit"
					,"user can release the  Customer Refundable Deposit"
					, "user can release the  Customer Refundable Deposit Fail", "fail");
		}
		
		//===================================================FIN_10_52
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Customer Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can convert the above advice to inbound payment"
					,"user can convert the above advice to inbound payment"
					, "user can convert the above advice to inbound payment Successfully", "pass");
		}else {
			writeTestResults("Verify that user can convert the above advice to inbound payment"
					,"user can convert the above advice to inbound payment"
					, "user can convert the above advice to inbound payment Fail", "fail");
		}
		
		//===================================================FIN_10_53
		
		for (int i = 1; i <= 2; i++) {
			
			driver.navigate().back();
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_SetOffActionBtnA);
		click(btn_SetOffActionBtnA);
		WaitElement(sel_SetOffDocSelA);
		click(sel_SetOffDocSelA);
		
		WaitElement(txt_SetOffPaidAmountTxtA);
		sendKeys(txt_SetOffPaidAmountTxtA, "50");
		WaitElement(btn_SetOffBtnA);
		click(btn_SetOffBtnA);
		
		pageRefersh();
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_SetOffDetailActionBtnA);
		click(btn_SetOffDetailActionBtnA);
		WaitElement(sel_SetOffDetailReverseDocSelA);
		click(sel_SetOffDetailReverseDocSelA);
		
		WaitElement(txt_PaidAmountTxtA);
		String PaidAmountTxtA= driver.findElement(By.xpath(txt_PaidAmountTxtA)).getAttribute("value");
//		System.out.println(PaidAmountTxtA);
		
		if(PaidAmountTxtA.contentEquals("50.000000")) {
			writeTestResults("Verify that released Vendor refund can be set-of using set-off function in action"
					,"released Vendor refund can be set-of using set-off function in action"
					, "released Vendor refund can be set-of using set-off function in action Successfully", "pass");
		}else {
			writeTestResults("Verify that released Vendor refund can be set-of using set-off function in action"
					,"released Vendor refund can be set-of using set-off function in action"
					, "released Vendor refund can be set-of using set-off function in action Fail", "fail");
		}
		
		WaitElement(btn_SetOffReverseBtnA);
		click(btn_SetOffReverseBtnA);
		
		pageRefersh();
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		
		if(!isDisplayed(btn_SetOffDetailActionBtnA)) {
			writeTestResults("Verify that user can reverse the set-off value"
					,"user can reverse the set-off value"
					, "user can reverse the set-off value Successfully", "pass");
		}else {
			writeTestResults("Verify that user can reverse the set-off value"
					,"user can reverse the set-off value"
					, "user can reverse the set-off value Fail", "fail");
		}
	}
	
	//FIN_10_54
	public void VerifyThatUserCanDoCustomerPaymentVoucherToTheAboveExecutedPendingCustomerRefundableDeposit() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);
		WaitElement(btn_JourneyDownArrowBtnA);
		click(btn_JourneyDownArrowBtnA);

		WaitElement(btn_CustomerRefundableDepositJourneyBtnA);
		click(btn_CustomerRefundableDepositJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Customer Refundable Deposit")){
			writeTestResults("Navigation to Inbound Payment Advice - New Customer Refundable Deposit page"
					,"User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page"
					, "User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Customer Refundable Deposit page"
					,"User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page"
					, "User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page Fail", "fail");
		}
		
		WaitElement(btn_CustomerLookUpIPABtnA);
		click(btn_CustomerLookUpIPABtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxt3A);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxt3A));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxt3A));
		
		WaitElement(txt_DescriptionIPATxtA);
		sendKeys(txt_DescriptionIPATxtA, "@Test1996");
		
		WaitElement(txt_AmountIPATxtA);
		sendKeys(txt_AmountIPATxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the  Customer Refundable Deposit Form"
					,"user can use Draft the  Customer Refundable Deposit Form"
					, "user can use Draft the  Customer Refundable Deposit Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the  Customer Refundable Deposit Form"
					,"user can use Draft the  Customer Refundable Deposit Form"
					, "user can use Draft the  Customer Refundable Deposit Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo= trackCode.replace(" Customer Refundable Deposit", "");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the  Customer Refundable Deposit"
					,"user can release the  Customer Refundable Deposit"
					, "user can release the  Customer Refundable Deposit Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the  Customer Refundable Deposit"
					,"user can release the  Customer Refundable Deposit"
					, "user can release the  Customer Refundable Deposit Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Customer Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can convert the above advice to inbound payment"
					,"user can convert the above advice to inbound payment"
					, "user can convert the above advice to inbound payment Successfully", "pass");
		}else {
			writeTestResults("Verify that user can convert the above advice to inbound payment"
					,"user can convert the above advice to inbound payment"
					, "user can convert the above advice to inbound payment Fail", "fail");
		}
		
		//===================================================FIN_10_54
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_OutboundPaymentBtnA);
		click(btn_OutboundPaymentBtnA);
		WaitElement(btn_NewOutboundPaymentBtnA);
		click(btn_NewOutboundPaymentBtnA);
		
		WaitElement(btn_CustomerPaymentVoucherJourneyBtnA);
		click(btn_CustomerPaymentVoucherJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Outbound Payment - New Customer Payment")){
			writeTestResults("Navigation to Outbound Payment - New page"
					,"User can Navigate Outbound Payment - New page"
					, "User can Navigate Outbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Outbound Payment - New page"
					,"User can Navigate Outbound Payment - New page"
					, "User can Navigate Outbound Payment - New page Fail", "fail");
		}
		
		WaitElement(btn_CustomerLookUpBtnA);
		click(btn_CustomerLookUpBtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxt3A);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxt3A));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxt3A));
		
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		WaitElement(sel_OutboundPaymentIPASelA.replace("IPANo", IPANo));
		click(sel_OutboundPaymentIPASelA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Outbound Payment - New Customer Payment Form"
					,"user can use Draft the Outbound Payment - New Customer Payment Form"
					, "user can use Draft the Outbound Payment - New Customer Payment Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Outbound Payment - New Customer Payment Form"
					,"user can use Draft the Outbound Payment - New Customer Payment Form"
					, "user can use Draft the Outbound Payment - New Customer Payment Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can do customer payment voucher to the above executed pending Customer Refundable Deposit"
					,"user can do customer payment voucher to the above executed pending Customer Refundable Deposit"
					, "user can do customer payment voucher to the above executed pending Customer Refundable Deposit Successfully", "pass");
		}else {
			writeTestResults("Verify that user can do customer payment voucher to the above executed pending Customer Refundable Deposit"
					,"user can do customer payment voucher to the above executed pending Customer Refundable Deposit"
					, "user can do customer payment voucher to the above executed pending Customer Refundable Deposit Fail", "fail");
		}
	}
	
	//FIN_10_55
	public void VerifyThatOnlyPendingCustomerRefundableDepositAdviceCanBeViewableInCustomerPaymentVoucherForThePaymentProcess() throws Exception {
		
		
	}
	
	//FIN_10_56to10_58
	public void VerifyThatUserCanUseDraft() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);
		WaitElement(btn_JourneyDownArrowBtnA);
		click(btn_JourneyDownArrowBtnA);

		WaitElement(btn_CustomerRefundableDepositJourneyBtnA);
		click(btn_CustomerRefundableDepositJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Customer Refundable Deposit")){
			writeTestResults("Navigation to Inbound Payment Advice - New Customer Refundable Deposit page"
					,"User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page"
					, "User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Customer Refundable Deposit page"
					,"User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page"
					, "User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page Fail", "fail");
		}
		
		WaitElement(btn_CustomerLookUpIPABtnA);
		click(btn_CustomerLookUpIPABtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxt3A);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxt3A));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxt3A));
		
		WaitElement(txt_DescriptionIPATxtA);
		sendKeys(txt_DescriptionIPATxtA, "@Test1996");
		
		WaitElement(txt_AmountIPATxtA);
		sendKeys(txt_AmountIPATxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		//===================================================FIN_10_56
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft"
					,"user can use Draft"
					, "user can use Draft Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft"
					,"user can use Draft"
					, "user can use Draft Fail", "fail");
		}
		
		//===================================================FIN_10_57
		
		WaitElement(btn_EditBtnA);
		click(btn_EditBtnA);
		
		WaitElement(txt_DescriptionIPATxtA);
		sendKeys(txt_DescriptionIPATxtA, "Finance Test");
		
		WaitElement(txt_AmountIPATxtA);
		sendKeys(txt_AmountIPATxtA, "200");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_UpdateBtnA);
		click(btn_UpdateBtnA);
		
		WaitElement(txt_DescriptionAfterDraftIPATxtA);
		String DescriptionAfterDraftIPATxtA= getText(txt_DescriptionAfterDraftIPATxtA);
//		System.out.println(DescriptionAfterDraftIPATxtA);
		
		WaitElement(txt_AmountAfterDraftIPATxtA);
		String AmountAfterDraftIPATxtA= driver.findElement(By.xpath(txt_AmountAfterDraftIPATxtA)).getAttribute("value");
//		System.out.println(AmountAfterDraftIPATxtA);
		
		if(DescriptionAfterDraftIPATxtA.contentEquals("Finance Test") && AmountAfterDraftIPATxtA.contentEquals("200.000000")) {
			writeTestResults("Verify that user can edit drafted Inbound Payment Advice (Update)"
					,"user can edit drafted Inbound Payment Advice (Update)"
					, "user can edit drafted Inbound Payment Advice (Update) Successfully", "pass");
		}else {
			writeTestResults("Verify that user can edit drafted Inbound Payment Advice (Update)"
					,"user can edit drafted Inbound Payment Advice (Update)"
					, "user can edit drafted Inbound Payment Advice (Update) Fail", "fail");
		}
		
		//===================================================FIN_10_58
		
		WaitElement(btn_EditBtnA);
		click(btn_EditBtnA);
		
		WaitElement(txt_DescriptionIPATxtA);
		sendKeys(txt_DescriptionIPATxtA, "Finance Test 2");
		
		WaitElement(txt_AmountIPATxtA);
		sendKeys(txt_AmountIPATxtA, "300");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_UpdateAndNewBtnA);
		click(btn_UpdateAndNewBtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Customer Refundable Deposit")){
			writeTestResults("Verify that user can edit drafted Inbound Payment Advice (Update & New)"
					,"user can edit drafted Inbound Payment Advice (Update & New)"
					, "user can edit drafted Inbound Payment Advice (Update & New) Successfully", "pass");
		}else {
			writeTestResults("Verify that user can edit drafted Inbound Payment Advice (Update & New)"
					,"user can edit drafted Inbound Payment Advice (Update & New)"
					, "user can edit drafted Inbound Payment Advice (Update & New) Fail", "fail");
		}
	}
	
	//FIN_10_59to10_60
	public void VerifyThatUserCanDuplicateaDraftedInboundPaymentAdvice() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);
		WaitElement(btn_JourneyDownArrowBtnA);
		click(btn_JourneyDownArrowBtnA);

		WaitElement(btn_CustomerRefundableDepositJourneyBtnA);
		click(btn_CustomerRefundableDepositJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Customer Refundable Deposit")){
			writeTestResults("Navigation to Inbound Payment Advice - New Customer Refundable Deposit page"
					,"User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page"
					, "User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Customer Refundable Deposit page"
					,"User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page"
					, "User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page Fail", "fail");
		}
		
		WaitElement(btn_CustomerLookUpIPABtnA);
		click(btn_CustomerLookUpIPABtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxt3A);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxt3A));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxt3A));
		
		WaitElement(txt_DescriptionIPATxtA);
		sendKeys(txt_DescriptionIPATxtA, "@Test1996");
		
		WaitElement(txt_AmountIPATxtA);
		sendKeys(txt_AmountIPATxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo = trackCode;
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft"
					,"user can use Draft"
					, "user can use Draft Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft"
					,"user can use Draft"
					, "user can use Draft Fail", "fail");
		}
		
		//===================================================FIN_10_59
		
		WaitElement(btn_DuplicateBtnA);
		click(btn_DuplicateBtnA);
		
		WaitElement(txt_CustomerACCTxtA);
		String CustomerACCTxtA= driver.findElement(By.xpath(txt_CustomerACCTxtA)).getAttribute("value");
//		System.out.println(CustomerACCTxtA);
		
		WaitElement(txt_AmountIPATxtA);
		String AmountIPATxtA= driver.findElement(By.xpath(txt_AmountIPATxtA)).getAttribute("value");
//		System.out.println(AmountIPATxtA);
		
		WaitElement(txt_DescriptionIPATxtA);
		String DescriptionIPATxtA= driver.findElement(By.xpath(txt_DescriptionIPATxtA)).getAttribute("value");
//		System.out.println(DescriptionIPATxtA);
		
		if(CustomerACCTxtA.equals(CustomerLookUpTxt3NewA) && AmountIPATxtA.equals("100.000000") && DescriptionIPATxtA.equals("@Test1996")) {
			writeTestResults("Verify that user can Duplicate a Drafted Inbound Payment Advice"
					,"user can Duplicate a Drafted Inbound Payment Advice"
					, "user can Duplicate a Drafted Inbound Payment Advice Successfully", "pass");
		}else {
			writeTestResults("Verify that user can Duplicate a Drafted Inbound Payment Advice"
					,"user can Duplicate a Drafted Inbound Payment Advice"
					, "user can Duplicate a Drafted Inbound Payment Advice Fail", "fail");
		}
		
		//===================================================FIN_10_60
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft"
					,"user can use Draft"
					, "user can use Draft Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft"
					,"user can use Draft"
					, "user can use Draft Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_DeleteBtnA);
		click(btn_DeleteBtnA);
		WaitElement(btn_YesBtnA);
		click(btn_YesBtnA);
		
		WaitElement(txt_pageDeleteA);
		if(isDisplayed(txt_pageDeleteA)) {
			writeTestResults("Verify that user can Delete a Drafted Inbound Payment Advice"
					,"user can Delete a Drafted Inbound Payment Advice"
					, "user can Delete a Drafted Inbound Payment Advice Successfully", "pass");
		}else {
			writeTestResults("Verify that user can Delete a Drafted Inbound Payment Advice"
					,"user can Delete a Drafted Inbound Payment Advice"
					, "user can Delete a Drafted Inbound Payment Advice Fail", "fail");
		}
	}
	
	//FIN_10_61
	public void VerifyThatUserCanViewHistoryOfaDraftedInboundPaymentAdvice() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);
		WaitElement(btn_JourneyDownArrowBtnA);
		click(btn_JourneyDownArrowBtnA);

		WaitElement(btn_CustomerRefundableDepositJourneyBtnA);
		click(btn_CustomerRefundableDepositJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Customer Refundable Deposit")){
			writeTestResults("Navigation to Inbound Payment Advice - New Customer Refundable Deposit page"
					,"User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page"
					, "User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Customer Refundable Deposit page"
					,"User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page"
					, "User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page Fail", "fail");
		}
		
		WaitElement(btn_CustomerLookUpIPABtnA);
		click(btn_CustomerLookUpIPABtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxt3A);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxt3A));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxt3A));
		
		WaitElement(txt_DescriptionIPATxtA);
		sendKeys(txt_DescriptionIPATxtA, "@Test1996");
		
		WaitElement(txt_AmountIPATxtA);
		sendKeys(txt_AmountIPATxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo = trackCode;
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft"
					,"user can use Draft"
					, "user can use Draft Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft"
					,"user can use Draft"
					, "user can use Draft Fail", "fail");
		}
		
		//===================================================FIN_10_61
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_HistoryBtnA);
		click(btn_HistoryBtnA);
		
		WaitElement(txt_DraftedIPAHistoryTextA);
		if(isDisplayed(txt_DraftedIPAHistoryTextA)) {
			writeTestResults("Verify that user can view history of a Drafted Inbound Payment Advice"
					," user can view history of a Drafted Inbound Payment Advice"
					, " user can view history of a Drafted Inbound Payment Advice Successfully", "pass");
		}else {
			writeTestResults("Verify that user can view history of a Drafted Inbound Payment Advice"
					," user can view history of a Drafted Inbound Payment Advice"
					, " user can view history of a Drafted Inbound Payment Advice Fail", "fail");
		}
	}
	
	//FIN_10_62
	public void VerifyThatUserCanUseDraftAndNew2() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);
		WaitElement(btn_JourneyDownArrowBtnA);
		click(btn_JourneyDownArrowBtnA);

		WaitElement(btn_CustomerRefundableDepositJourneyBtnA);
		click(btn_CustomerRefundableDepositJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Customer Refundable Deposit")){
			writeTestResults("Navigation to Inbound Payment Advice - New Customer Refundable Deposit page"
					,"User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page"
					, "User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Customer Refundable Deposit page"
					,"User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page"
					, "User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page Fail", "fail");
		}
		
		WaitElement(btn_CustomerLookUpIPABtnA);
		click(btn_CustomerLookUpIPABtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxt3A);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxt3A));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxt3A));
		
		WaitElement(txt_DescriptionIPATxtA);
		sendKeys(txt_DescriptionIPATxtA, "@Test1996");
		
		WaitElement(txt_AmountIPATxtA);
		sendKeys(txt_AmountIPATxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		//===================================================FIN_10_62
		
		WaitElement(btn_DraftAndNewBtnA);
		click(btn_DraftAndNewBtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Customer Refundable Deposit")){
			writeTestResults("Navigation to Inbound Payment Advice - New Customer Refundable Deposit page"
					,"User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page"
					, "User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Customer Refundable Deposit page"
					,"User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page"
					, "User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page Fail", "fail");
		}
		
		WaitElement(btn_CustomerLookUpIPABtnA);
		click(btn_CustomerLookUpIPABtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxt3A);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxt3A));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxt3A));
		
		WaitElement(txt_DescriptionIPATxtA);
		sendKeys(txt_DescriptionIPATxtA, "@Test1996");
		
		WaitElement(txt_AmountIPATxtA);
		sendKeys(txt_AmountIPATxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo = trackCode;
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft and New"
					,"user can use Draft and New"
					, "user can use Draft and New Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft and New"
					,"user can use Draft and New"
					, "user can use Draft and New Fail", "fail");
		}
	}
	
	//FIN_10_63to10_65
	public void VerifyThatUserCanUseCopyFrom() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);
		WaitElement(btn_JourneyDownArrowBtnA);
		click(btn_JourneyDownArrowBtnA);

		WaitElement(btn_CustomerRefundableDepositJourneyBtnA);
		click(btn_CustomerRefundableDepositJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Customer Refundable Deposit")){
			writeTestResults("Navigation to Inbound Payment Advice - New Customer Refundable Deposit page"
					,"User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page"
					, "User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Customer Refundable Deposit page"
					,"User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page"
					, "User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page Fail", "fail");
		}
		
		WaitElement(btn_CustomerLookUpIPABtnA);
		click(btn_CustomerLookUpIPABtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxt3A);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxt3A));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxt3A));
		
		WaitElement(txt_DescriptionIPATxtA);
		sendKeys(txt_DescriptionIPATxtA, "@Test1996");
		
		WaitElement(txt_AmountIPATxtA);
		sendKeys(txt_AmountIPATxtA, "$Test");
		
		WaitElement(txt_AmountIPATxtA);
		sendKeys(txt_AmountIPATxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the  Customer Refundable Deposit Form"
					,"user can use Draft the  Customer Refundable Deposit Form"
					, "user can use Draft the  Customer Refundable Deposit Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the  Customer Refundable Deposit Form"
					,"user can use Draft the  Customer Refundable Deposit Form"
					, "user can use Draft the  Customer Refundable Deposit Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Customer Refundable Deposit", "");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the  Customer Refundable Deposit"
					,"user can release the  Customer Refundable Deposit"
					, "user can release the  Customer Refundable Deposit Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the  Customer Refundable Deposit"
					,"user can release the  Customer Refundable Deposit"
					, "user can release the  Customer Refundable Deposit Fail", "fail");
		}
		
		//===================================================FIN_10_63
		
		WaitElement(btn_InboundPaymentAdvicePlusBtnA);
		click(btn_InboundPaymentAdvicePlusBtnA);
		
		WaitElement(btn_JourneyDownArrowBtnA);
		click(btn_JourneyDownArrowBtnA);

		WaitElement(btn_CustomerRefundableDepositJourneyBtnA);
		click(btn_CustomerRefundableDepositJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Customer Refundable Deposit")){
			writeTestResults("Navigation to Inbound Payment Advice - New Customer Refundable Deposit page"
					,"User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page"
					, "User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Customer Refundable Deposit page"
					,"User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page"
					, "User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page Fail", "fail");
		}
		
		WaitElement(btn_CopyFromBtnA);
		click(btn_CopyFromBtnA);
		
		WaitElement(txt_CopyFromIPATxtA);
		sendKeys(txt_CopyFromIPATxtA, IPANo);
		pressEnter(txt_CopyFromIPATxtA);
		WaitElement(sel_CopyFromIPASelA.replace("IPANo", IPANo));
		doubleClick(sel_CopyFromIPASelA.replace("IPANo", IPANo));
		
		WaitElement(txt_CustomerACCTxtA);
		String CustomerACCTxtA= driver.findElement(By.xpath(txt_CustomerACCTxtA)).getAttribute("value");
//		System.out.println(CustomerACCTxtA);
		
		WaitElement(txt_AmountIPATxtA);
		String AmountIPATxtA= driver.findElement(By.xpath(txt_AmountIPATxtA)).getAttribute("value");
//		System.out.println(AmountIPATxtA);
		
		WaitElement(txt_DescriptionIPATxtA);
		String DescriptionIPATxtA= driver.findElement(By.xpath(txt_DescriptionIPATxtA)).getAttribute("value");
//		System.out.println(DescriptionIPATxtA);
		
		if(CustomerACCTxtA.equals(CustomerLookUpTxt3NewA) && AmountIPATxtA.equals("100.000000") && DescriptionIPATxtA.equals("@Test1996")) {
			writeTestResults("Verify that user can use Copy From"
					,"user can use Copy From"
					, "user can use Copy From Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Copy From"
					,"user can use Copy From"
					, "user can use Copy From Fail", "fail");
		}
		
		//===================================================FIN_10_64
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the  Customer Refundable Deposit Form"
					,"user can use Draft the  Customer Refundable Deposit Form"
					, "user can use Draft the  Customer Refundable Deposit Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the  Customer Refundable Deposit Form"
					,"user can use Draft the  Customer Refundable Deposit Form"
					, "user can use Draft the  Customer Refundable Deposit Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo2 =trackCode.replace(" Customer Refundable Deposit", "");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release an Inbound Payment Advice"
					,"user can release an Inbound Payment Advice"
					, "user can release an Inbound Payment Advice Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release an Inbound Payment Advice"
					,"user can release an Inbound Payment Advice"
					, "user can release an Inbound Payment Advice Fail", "fail");
		}
		
		//===================================================FIN_10_65
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ReverseBtnA);
		click(btn_ReverseBtnA);
		WaitElement(btn_ReverseLookupBtnA);
		click(btn_ReverseLookupBtnA);
		WaitElement(btn_YesBtnA);
		click(btn_YesBtnA);
		
		WaitElement(txt_PageReverseA);
		if(isDisplayed(txt_PageReverseA)) {
			writeTestResults("Verify that user can Reverse an Inbound Payment Advice"
					,"user can Reverse an Inbound Payment Advice"
					, "user can Reverse an Inbound Payment Advice Successfully", "pass");
		}else {
			writeTestResults("Verify that user can Reverse an Inbound Payment Advice"
					,"user can Reverse an Inbound Payment Advice"
					, "user can Reverse an Inbound Payment Advice Fail", "fail");
		}
	}
	
	//FIN_10_66to10_67
	public void VerifyThatUserCanHoldAnInboundPaymentAdvice() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);
		WaitElement(btn_JourneyDownArrowBtnA);
		click(btn_JourneyDownArrowBtnA);

		WaitElement(btn_CustomerRefundableDepositJourneyBtnA);
		click(btn_CustomerRefundableDepositJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Customer Refundable Deposit")){
			writeTestResults("Navigation to Inbound Payment Advice - New Customer Refundable Deposit page"
					,"User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page"
					, "User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Customer Refundable Deposit page"
					,"User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page"
					, "User can Navigate Inbound Payment Advice - New Customer Refundable Deposit page Fail", "fail");
		}
		
		WaitElement(btn_CustomerLookUpIPABtnA);
		click(btn_CustomerLookUpIPABtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxt3A);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxt3A));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxt3A));
		
		WaitElement(txt_DescriptionIPATxtA);
		sendKeys(txt_DescriptionIPATxtA, "@Test1996");
		
		WaitElement(txt_AmountIPATxtA);
		sendKeys(txt_AmountIPATxtA, "$Test");
		
		WaitElement(txt_AmountIPATxtA);
		sendKeys(txt_AmountIPATxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the  Customer Refundable Deposit Form"
					,"user can use Draft the  Customer Refundable Deposit Form"
					, "user can use Draft the  Customer Refundable Deposit Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the  Customer Refundable Deposit Form"
					,"user can use Draft the  Customer Refundable Deposit Form"
					, "user can use Draft the  Customer Refundable Deposit Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the  Customer Refundable Deposit"
					,"user can release the  Customer Refundable Deposit"
					, "user can release the  Customer Refundable Deposit Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the  Customer Refundable Deposit"
					,"user can release the  Customer Refundable Deposit"
					, "user can release the  Customer Refundable Deposit Fail", "fail");
		}
		
		//===================================================FIN_10_66
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_HoldBtnA);
		click(btn_HoldBtnA);
		WaitElement(txt_ReasonTxtA);
		sendKeys(txt_ReasonTxtA, "Test Finance");
		WaitElement(btn_OkBtnA);
		click(btn_OkBtnA);
		
		WaitElement(txt_PageHoldA);
		if(isDisplayed(txt_PageHoldA)) {
			writeTestResults("Verify that user can hold an Inbound Payment Advice"
					,"user can hold an Inbound Payment Advice"
					, "user can hold an Inbound Payment Advice Successfully", "pass");
		}else {
			writeTestResults("Verify that user can hold an Inbound Payment Advice"
					,"user can hold an Inbound Payment Advice"
					, "user can hold an Inbound Payment Advice Fail", "fail");
		}
		
		//===================================================FIN_10_67
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_UnHoldBtnA);
		click(btn_UnHoldBtnA);
		WaitElement(txt_ReasonTxtA);
		sendKeys(txt_ReasonTxtA, "Test Finance");
		WaitElement(btn_OkBtnA);
		click(btn_OkBtnA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can Unhold an Inbound Payment Advice"
					,"user can Unhold an Inbound Payment Advice"
					, "user can Unhold an Inbound Payment Advice Successfully", "pass");
		}else {
			writeTestResults("Verify that user can Unhold an Inbound Payment Advice"
					,"user can Unhold an Inbound Payment Advice"
					, "user can Unhold an Inbound Payment Advice Fail", "fail");
		}
	}
	
	//FIN_10_68
	public void VerifyThatCostAllocationShouldBeEqualToAdviceValue100() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);

		WaitElement(btn_VendorRefundJourneyBtnA);
		click(btn_VendorRefundJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Vendor Refund")){
			writeTestResults("Navigation to Inbound Payment Advice - New Vendor Refund page"
					,"User can Navigate Inbound Payment Advice - New Vendor Refund page"
					, "User can Navigate Inbound Payment Advice - New Vendor Refund page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Vendor Refund page"
					,"User can Navigate Inbound Payment Advice - New Vendor Refund page"
					, "User can Navigate Inbound Payment Advice - New Vendor Refund page Fail", "fail");
		}
		
		WaitElement(btn_VendorLookUpIPABtnA);
		click(btn_VendorLookUpIPABtnA);
		WaitElement(txt_VendorLookUpIPATxtA);
		sendKeys(txt_VendorLookUpIPATxtA, VendorLookUpIPATxtA);
		pressEnter(txt_VendorLookUpIPATxtA);
		WaitElement(sel_VendorLookUpIPASelA.replace("VenSel", VendorLookUpIPATxtA));
		doubleClick(sel_VendorLookUpIPASelA.replace("VenSel", VendorLookUpIPATxtA));
		
		WaitElement(btn_GLAccIPABtnA);
		click(btn_GLAccIPABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "100");
		
		//===================================================FIN_10_68
		
		WaitClick(btn_CostAllocationIPABtnA);
		click(btn_CostAllocationIPABtnA);
		
		WaitElement(sel_CostCenterSelA);
		selectText(sel_CostCenterSelA, CostCenterSelA);
		WaitElement(txt_CostAllocationPercentageTxtA);
		sendKeys(txt_CostAllocationPercentageTxtA, "100");
		WaitClick(btn_CostAllocationAddRecordBtnA);
		click(btn_CostAllocationAddRecordBtnA);
		
		WaitElement(txt_CostAllocationAmountTxtA);
		sendKeys(txt_CostAllocationAmountTxtA, "100");
		WaitClick(btn_CostAllocationUpdateBtnA);
		click(btn_CostAllocationUpdateBtnA);
		
		WaitClick(btn_CostAllocationIPABtnA);
		click(btn_CostAllocationIPABtnA);
		
		WaitElement(txt_CostAllocationAmountTxtA);
		sendKeys(txt_CostAllocationAmountTxtA, "110");
		WaitClick(btn_CostAllocationUpdateBtnA);
		click(btn_CostAllocationUpdateBtnA);
		
		WaitElement(txt_CostAllocationValidatorTxtA);
		if(isDisplayed(txt_CostAllocationValidatorTxtA)){
			writeTestResults("Verify that Cost Allocation should be equal to advice value. (=100%)"
					,"Cost Allocation should be equal to advice value. (=100%)"
					, "Cost Allocation should be equal to advice value. (=100%) Successfully", "pass");
		}else {
			writeTestResults("Verify that Cost Allocation should be equal to advice value. (=100%)"
					,"Cost Allocation should be equal to advice value. (=100%)"
					, "Cost Allocation should be equal to advice value. (=100%) Fail", "fail");
		}
	}
	
	//FIN_10_69
	public void VerifyThatAllSalesInvocieWillAutoGenerateInboundPaymentAdviceWhileReleasingTheSalesInvoice() throws Exception {
		
		WaitElement(btn_SalesAndMarcketingBtnA);
		click(btn_SalesAndMarcketingBtnA);
		
		WaitElement(subsidemenuA);
		if(isDisplayed(subsidemenuA)) {
			writeTestResults("verify subsideMenu","view subsideMenu", "subsideMenu is display", "pass");
		}else {
			writeTestResults("verify subsideMenu","view subsideMenu", "subsideMenu is not display", "fail");
		}

		WaitElement(btn_SalesInvoiceBtnA);
		click(btn_SalesInvoiceBtnA);
		WaitElement(btn_NewSalesInvoiceBtnA);
		click(btn_NewSalesInvoiceBtnA);
		WaitElement(btn_SalesInvoiceToOutboundShipmentJourneyBtnA);
		click(btn_SalesInvoiceToOutboundShipmentJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Sales Invoice - New")){
			writeTestResults("Navigation to Sales Invoice - New page"
					,"User can Navigate Sales Invoice - New page"
					, "User can Navigate Sales Invoice - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Sales Invoice - New page"
					,"User can Navigate Sales Invoice - New page"
					, "User can Navigate Sales Invoice - New page Fail", "fail");
		}
		
		WaitElement(btn_CustomerLookUpSIBtnA);
		click(btn_CustomerLookUpSIBtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxt4NewA);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxt4NewA));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxt4NewA));
		
		ScrollDownAruna();
		
		WaitElement(txt_AutoCompleteProductTxtA);
		sendKeys(txt_AutoCompleteProductTxtA, AutoCompleteProductTxtA);
		Thread.sleep(4000);
		driver.findElement(getLocator(txt_AutoCompleteProductTxtA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(4000);
		pressEnter(txt_AutoCompleteProductTxtA);
		sendKeys(txt_AutoCompleteProductQtyTxtA, "1");
		Thread.sleep(4000);
		pressEnter(txt_AutoCompleteProductQtyTxtA);
		selectText(sel_WarehouseSISelA, WarehouseSISelA);
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft1A);
		if(isDisplayed(txt_pageDraft1A)) {
			writeTestResults("Verify that user can use Draft the  Sales Invoice Form"
					,"user can use Draft the  Sales Invoice Form"
					, "user can use Draft the Sales Invoice Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Sales Invoice Form"
					,"user can use Draft the Sales Invoice Form"
					, "user can use Draft the Sales Invoice Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		
		pageRefersh();
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease1A);
		if(isDisplayed(txt_pageRelease1A)) {
			writeTestResults("Verify that user can release the Sales Invoice Form"
					,"user can release the Sales Invoice Form"
					, "user can release the Sales Invoice Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Sales Invoice Form"
					,"user can release the Sales Invoice Form"
					, "user can release the Sales Invoice Form Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_DocFlowBtnA);
		click(btn_DocFlowBtnA);
		WaitElement(btn_InboundPaymentDocFlowBtnA);
		click(btn_InboundPaymentDocFlowBtnA);
		
		switchWindow();
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that all sales invocie will auto generate inbound payment advice while releasing the sales invoice"
					,"all sales invocie will auto generate inbound payment advice while releasing the sales invoice"
					, "all sales invocie will auto generate inbound payment advice while releasing the sales invoice Successfully", "pass");
		}else {
			writeTestResults("Verify that all sales invocie will auto generate inbound payment advice while releasing the sales invoice"
					,"all sales invocie will auto generate inbound payment advice while releasing the sales invoice"
					, "all sales invocie will auto generate inbound payment advice while releasing the sales invoice Fail", "fail");
		}
	}
	
	//Inbound Payment Regression
	
	//FIN_11_1
	public void VerifyWhetherUserAbleToNavigateToInboundPayment() throws Exception {
		
		WaitElement(btn_InboundPaymentBtnA);
		click(btn_InboundPaymentBtnA);
		
		//===================================================FIN_11_1
		
		WaitElement(txt_InboundPaymentHeaderA);
		if(getText(txt_InboundPaymentHeaderA).contentEquals("Inbound Payment")) {
			writeTestResults("Verify whether user able to navigate to Inbound Payment"
					,"user able to navigate to Inbound Payment"
					, "user able to navigate to Inbound Payment Successfully", "pass");
		}else {
			writeTestResults("Verify whether user able to navigate to Inbound Payment"
					,"user able to navigate to Inbound Payment"
					, "user able to navigate to Inbound Payment Fail", "fail");
		}
	}
	
	//FIN_11_2
	public void VerifyWhetherIfUserDontHavePermissionToViewInboundPaymentByPageUserWantAbleToNavigateToByPage() throws Exception {
		
		//===================================================FIN_11_2
		
		if(!isDisplayed(btn_InboundPaymentBtnA)) {
			writeTestResults("Verify whether if user don't have permission to view Inbound Payment by-page, user want able to navigate to by-page"
					,"if user don't have permission to view Inbound Payment by-page, user want able to navigate to by-page"
					, "if user don't have permission to view Inbound Payment by-page, user want able to navigate to by-page Successfully", "pass");
		}else {
			writeTestResults("Verify whether if user don't have permission to view Inbound Payment by-page, user want able to navigate to by-page"
					,"if user don't have permission to view Inbound Payment by-page, user want able to navigate to by-page"
					, "if user don't have permission to view Inbound Payment by-page, user want able to navigate to by-page Fail", "fail");
		}
	}
	
	
	
	//FIN_11_5to11_9
	public void VerifyTheAvailableJourneies() throws Exception {
		
		WaitElement(btn_InboundPaymentBtnA);
		click(btn_InboundPaymentBtnA);
		WaitElement(btn_NewInboundPaymentBtnA);
		click(btn_NewInboundPaymentBtnA);
		
		//===================================================FIN_11_5
		
		if(isDisplayed(btn_CustomerReceiptVoucherJourneyBtnA) && isDisplayed(btn_VendorReceiptVoucherJourneyBtnA) 
				&& isDisplayed(btn_ReceiptVoucherJourneyBtnA) && isDisplayed(btn_EmployeeReceiptVoucherJourneyBtnA)){
			writeTestResults("Verify the available journeies"
					,"available journeies"
					, "available journeies Successfully", "pass");
		}else {
			writeTestResults("Verify the available journeies"
					,"available journeies"
					, "available journeies Fail", "fail");
		}
		
		//===================================================FIN_11_6
		
		WaitElement(btn_CustomerReceiptVoucherJourneyBtnA);
		click(btn_CustomerReceiptVoucherJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Customer Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		//===================================================FIN_11_7
		
		WaitElement(btn_CustomerLookUpBtnA);
		click(btn_CustomerLookUpBtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxtA);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		
		WaitElement(txt_CustomerACCTxtA);
		String CustomerACCTxtA= driver.findElement(By.xpath(txt_CustomerACCTxtA)).getAttribute("value");
		
		if(CustomerACCTxtA.contentEquals(CustomerLookUpTxtA)){
			writeTestResults("Verify that user can select a Customer using the Customer Lookup"
					,"user can select a Customer using the Customer Lookup"
					, "user can select a Customer using the Customer Lookup Successfully", "pass");
		}else {
			writeTestResults("Verify that user can select a Customer using the Customer Lookup"
					,"user can select a Customer using the Customer Lookup"
					, "user can select a Customer using the Customer Lookup Fail", "fail");
		}
		
		//===================================================FIN_11_8
		
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		String PaymentMethodSelValueA = new Select(driver.findElement(By.xpath(sel_PaymentMethodSelA))).getFirstSelectedOption().getText();
		if(PaymentMethodSelValueA.contentEquals(PaymentMethodSelA)){
			writeTestResults("Verify that user can select a Payment method using the drop down"
					,"user can select a Payment method using the drop down"
					, "user can select a Payment method using the drop down Successfully", "pass");
		}else {
			writeTestResults("Verify that user can select a Payment method using the drop down"
					,"user can select a Payment method using the drop down"
					, "user can select a Payment method using the drop down Fail", "fail");
		}
		//===================================================FIN_11_9
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		String PaidCurrencySelAValueA = new Select(driver.findElement(By.xpath(sel_PaidCurrencySelA))).getFirstSelectedOption().getText();
		if(PaidCurrencySelAValueA.contentEquals(PaidCurrencySelA)){
			writeTestResults("Verify that user can select a Paid currency using the drop down"
					,"user can select a Paid currency using the drop down"
					, "user can select a Paid currency using the drop down Successfully", "pass");
		}else {
			writeTestResults("Verify that user can select a Paid currency using the drop down"
					,"user can select a Paid currency using the drop down"
					, "user can select a Paid currency using the drop down Fail", "fail");
		}
	}
	
	//FIN_11_10
	public void VerifyThatUserCanAbleToCreateCustomerReceiptVoucherUsingCashPayMethod() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);
		
		WaitElement(btn_CustomerAdvanceJourneyBtnA);
		click(btn_CustomerAdvanceJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Customer Advance")){
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Fail", "fail");
		}
		
		WaitElement(btn_CustomerLookUpIPABtnA);
		click(btn_CustomerLookUpIPABtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxtA);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		
		WaitElement(txt_AmountFieldIPATxtA);
		sendKeys(txt_AmountFieldIPATxtA, "100");
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Customer Advance","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Customer Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		//===================================================FIN_11_10
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can able to create customer receipt voucher using 'Cash' pay method"
					,"user can able to create customer receipt voucher using 'Cash' pay method"
					, "user can able to create customer receipt voucher using 'Cash' pay method Successfully", "pass");
		}else {
			writeTestResults("Verify that user can able to create customer receipt voucher using 'Cash' pay method"
					,"user can able to create customer receipt voucher using 'Cash' pay method"
					, "user can able to create customer receipt voucher using 'Cash' pay method Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_JournalActionBtnA);
		click(btn_JournalActionBtnA);
		
//		System.out.println(getText(txt_JournalCreditIPTxtA));
//		System.out.println(getText(txt_JournalDeditIPTxtA));
		
		if(getText(txt_JournalCreditIPTxtA).equals(getText(txt_JournalDebitIPTxtA))) {
			writeTestResults("Verify that user can verify the journal entry of the created customer receipt voucher"
					,"user can verify the journal entry of the created customer receipt voucher"
					, "user can verify the journal entry of the created customer receipt voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can verify the journal entry of the created customer receipt voucher"
					,"user can verify the journal entry of the created customer receipt voucher"
					, "user can verify the journal entry of the created customer receipt voucher Fail", "fail");
		}
	}
	
	//FIN_11_11
	public void VerifyThatUserCanAbleToCreateCustomerReceiptVoucherUsingChequePayMethod() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);
		
		WaitElement(btn_CustomerAdvanceJourneyBtnA);
		click(btn_CustomerAdvanceJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Customer Advance")){
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Fail", "fail");
		}
		
		WaitElement(btn_CustomerLookUpIPABtnA);
		click(btn_CustomerLookUpIPABtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxtA);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		
		WaitElement(txt_AmountFieldIPATxtA);
		sendKeys(txt_AmountFieldIPATxtA, "100");
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Customer Advance","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Customer Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		//===================================================FIN_11_11
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSel2A);
		
		WaitElement(sel_BankNameIPSelA);
		selectText(sel_BankNameIPSelA, BankNameIPSelA);
		
		WaitElement(txt_ChequeNoIPTxtA);
		LocalTime Obj1 = LocalTime.now();
		sendKeys(txt_ChequeNoIPTxtA, "CHK"+Obj1);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can able to create customer receipt voucher using 'Cheque' pay method"
					,"user can able to create customer receipt voucher using 'Cheque' pay method"
					, "user can able to create customer receipt voucher using 'Cheque' pay method Successfully", "pass");
		}else {
			writeTestResults("Verify that user can able to create customer receipt voucher using 'Cheque' pay method"
					,"user can able to create customer receipt voucher using 'Cheque' pay method"
					, "user can able to create customer receipt voucher using 'Cheque' pay method Fail", "fail");
		}
	}
	
	//FIN_11_12and11_20
	public void VerifyThatUserCanAbleToAccessAboveCreatedChequeAfterTheMentionedChequeDateInBankDeposit1() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);
		
		WaitElement(btn_CustomerAdvanceJourneyBtnA);
		click(btn_CustomerAdvanceJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Customer Advance")){
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Fail", "fail");
		}
		
		WaitElement(btn_CustomerLookUpIPABtnA);
		click(btn_CustomerLookUpIPABtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxtA);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		
		WaitElement(txt_AmountFieldIPATxtA);
		sendKeys(txt_AmountFieldIPATxtA, "100");
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Customer Advance","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Customer Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSel2A);
		
		WaitElement(sel_BankNameIPSelA);
		selectText(sel_BankNameIPSelA, BankNameIPSelA);
		
		WaitElement(txt_ChequeNoIPTxtA);
		LocalTime Obj1 = LocalTime.now();
		sendKeys(txt_ChequeNoIPTxtA, "CHK"+Obj1);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPNo = trackCode.replace(" Customer Receipt", "");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can able to create customer receipt voucher using 'Cheque' pay method"
					,"user can able to create customer receipt voucher using 'Cheque' pay method"
					, "user can able to create customer receipt voucher using 'Cheque' pay method Successfully", "pass");
		}else {
			writeTestResults("Verify that user can able to create customer receipt voucher using 'Cheque' pay method"
					,"user can able to create customer receipt voucher using 'Cheque' pay method"
					, "user can able to create customer receipt voucher using 'Cheque' pay method Fail", "fail");
		}
		
		//===================================================FIN_11_12
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_BankDepositBtnA);
		click(btn_BankDepositBtnA);
		WaitElement(btn_NewBankDepositBtnA);
		click(btn_NewBankDepositBtnA);
		
		WaitElement(sel_PayMethodBDSelA);
		selectText(sel_PayMethodBDSelA, PaymentMethodSel2A);
		
		WaitElement(sel_BankBDSelA);
		selectText(sel_BankBDSelA, BankBDSelA);
		
		WaitElement(sel_BankAccountNoBDSelA);
		selectText(sel_BankAccountNoBDSelA, BankAccountNoBDSelA);
		
		WaitElement(btn_SortByBDBtnA);
		selectText(btn_SortByBDBtnA, "Cheque No");
		
		WaitElement(txt_CheqNoBDTxtA);
		sendKeys(txt_CheqNoBDTxtA, "CHK"+Obj1);
		
		WaitClick(btn_ViewBDBtnA);
		click(btn_ViewBDBtnA);
		
		ScrollDownAruna();
		
		WaitElement(btn_DolSelBDBtnA.replace("IPNo", IPNo));
		if(isDisplayed(btn_DolSelBDBtnA.replace("IPNo", IPNo))) {
			writeTestResults("Verify that user can able to access above created cheque after the mentioned cheque date in bank deposit"
					,"user can able to access above created cheque after the mentioned cheque date in bank deposit"
					, "user can able to access above created cheque after the mentioned cheque date in bank deposit Successfully", "pass");
			Thread.sleep(4000);
			click(btn_DolSelBDBtnA.replace("IPNo", IPNo));
		}else {
			writeTestResults("Verify that user can able to access above created cheque after the mentioned cheque date in bank deposit"
					,"user can able to access above created cheque after the mentioned cheque date in bank deposit"
					, "user can able to access above created cheque after the mentioned cheque date in bank deposit Fail", "fail");
		}
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Bank Deposit Form"
					,"user can use Draft the Bank Deposit Form"
					, "user can use Draft the Bank Deposit Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Bank Deposit Form"
					,"user can use Draft the Bank Deposit Form"
					, "user can use Draft the Bank Deposit Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can use Release the Bank Deposit Form"
					,"user can use Release the Bank Deposit Form"
					, "user can use Release the Bank Deposit Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Release the Bank Deposit Form"
					,"user can use Release the Bank Deposit Form"
					, "user can use Release the Bank Deposit Form Fail", "fail");
		}
		
		//===================================================FIN_11_20
		
		for (int i = 1; i <=3; i++) {
			driver.navigate().back();
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ChequeReturnBtnIPA);
		click(btn_ChequeReturnBtnIPA);
		
		WaitElement(sel_ReturnResonSelIPA);
		selectText(sel_ReturnResonSelIPA, "Funds insufficient");
		
		JavascriptExecutor j7 = (JavascriptExecutor)driver;
		j7.executeScript("$('#txtReturnDate').datepicker('setDate', new Date())");
		
		WaitElement(txt_ReturnRemarkTxtIPA);
		sendKeys(txt_ReturnRemarkTxtIPA, "Return CHK"+Obj1);
		
		WaitClick(btn_ReturnBtnIPABtnIPA);
		click(btn_ReturnBtnIPABtnIPA);
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		
		if(!isDisplayed(btn_ChequeReturnBtnIPA)) {
			writeTestResults("Verify that user can do Cheque return transaction"
					,"user can do Cheque return transaction"
					, "user can do Cheque return transaction Successfully", "pass");
		}else {
			writeTestResults("Verify that user can do Cheque return transaction"
					,"user can do Cheque return transaction"
					, "user can do Cheque return transaction Fail", "fail");
		}
	}
	
	//FIN_11_13
	public void VerifyThatUserCanAbleToCreateCustomerReceiptVoucherUsingEFTPayMethod() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);
		
		WaitElement(btn_CustomerAdvanceJourneyBtnA);
		click(btn_CustomerAdvanceJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Customer Advance")){
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Fail", "fail");
		}
		
		WaitElement(btn_CustomerLookUpIPABtnA);
		click(btn_CustomerLookUpIPABtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxtA);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		
		WaitElement(txt_AmountFieldIPATxtA);
		sendKeys(txt_AmountFieldIPATxtA, "100");
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Customer Advance","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Customer Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		//===================================================FIN_11_13
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSel3A);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can able to create customer receipt voucher using 'EFT' pay method"
					,"user can able to create customer receipt voucher using 'EFT' pay method"
					, "user can able to create customer receipt voucher using 'EFT' pay method Successfully", "pass");
		}else {
			writeTestResults("Verify that user can able to create customer receipt voucher using 'EFT' pay method"
					,"user can able to create customer receipt voucher using 'EFT' pay method"
					, "user can able to create customer receipt voucher using 'EFT' pay method Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_JournalActionBtnA);
		click(btn_JournalActionBtnA);
		
//		System.out.println(getText(txt_JournalCreditIPTxtA));
//		System.out.println(getText(txt_JournalDeditIPTxtA));
		
		if(getText(txt_JournalCreditIPTxtA).equals(getText(txt_JournalDebitIPTxtA))) {
			writeTestResults("Verify that user can verify the journal entry of the created customer receipt voucher"
					,"user can verify the journal entry of the created customer receipt voucher"
					, "user can verify the journal entry of the created customer receipt voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can verify the journal entry of the created customer receipt voucher"
					,"user can verify the journal entry of the created customer receipt voucher"
					, "user can verify the journal entry of the created customer receipt voucher Fail", "fail");
		}
	}
	
	//FIN_11_14
	public void VerifyThatUserCanAbleToCreateCustomerReceiptVoucherUsingDirectDepositPayMethod() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);
		
		WaitElement(btn_CustomerAdvanceJourneyBtnA);
		click(btn_CustomerAdvanceJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Customer Advance")){
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Fail", "fail");
		}
		
		WaitElement(btn_CustomerLookUpIPABtnA);
		click(btn_CustomerLookUpIPABtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxtA);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		
		WaitElement(txt_AmountFieldIPATxtA);
		sendKeys(txt_AmountFieldIPATxtA, "100");
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Customer Advance","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Customer Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		//===================================================FIN_11_14
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSel4A);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can able to create customer receipt voucher using 'Direct Deposit' pay method"
					,"user can able to create customer receipt voucher using 'Direct Deposit' pay method"
					, "user can able to create customer receipt voucher using 'Direct Deposit' pay method Successfully", "pass");
		}else {
			writeTestResults("Verify that user can able to create customer receipt voucher using 'Direct Deposit' pay method"
					,"user can able to create customer receipt voucher using 'Direct Deposit' pay method"
					, "user can able to create customer receipt voucher using 'Direct Deposit' pay method Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_JournalActionBtnA);
		click(btn_JournalActionBtnA);
		
//		System.out.println(getText(txt_JournalCreditIPTxtA));
//		System.out.println(getText(txt_JournalDeditIPTxtA));
		
		if(getText(txt_JournalCreditIPTxtA).equals(getText(txt_JournalDebitIPTxtA))) {
			writeTestResults("Verify that user can verify the journal entry of the created customer receipt voucher"
					,"user can verify the journal entry of the created customer receipt voucher"
					, "user can verify the journal entry of the created customer receipt voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can verify the journal entry of the created customer receipt voucher"
					,"user can verify the journal entry of the created customer receipt voucher"
					, "user can verify the journal entry of the created customer receipt voucher Fail", "fail");
		}
	}
	
	//FIN_11_15
	public void VerifyThatPayingAmountCanBeGreaterThanSelectedDueAmount() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);
		
		WaitElement(btn_CustomerAdvanceJourneyBtnA);
		click(btn_CustomerAdvanceJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Customer Advance")){
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Fail", "fail");
		}
		
		WaitElement(btn_CustomerLookUpIPABtnA);
		click(btn_CustomerLookUpIPABtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxtA);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		
		WaitElement(txt_AmountFieldIPATxtA);
		sendKeys(txt_AmountFieldIPATxtA, "100");
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Customer Advance","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Customer Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		//===================================================FIN_11_15
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(txt_PayingAmountIPTxtA);
		sendKeys(txt_PayingAmountIPTxtA, "110");
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that Paying amount can be greater than selected due amount"
					,"Paying amount can be greater than selected due amount"
					, "Paying amount can be greater than selected due amount Successfully", "pass");
		}else {
			writeTestResults("Verify that Paying amount can be greater than selected due amount"
					,"Paying amount can be greater than selected due amount"
					, "Paying amount can be greater than selected due amount Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_JournalActionBtnA);
		click(btn_JournalActionBtnA);
		
//		System.out.println(getText(txt_JournalCreditIPTxtA));
//		System.out.println(getText(txt_JournalDeditIPTxtA));
		
		if(getText(txt_JournalCreditIPTxtA).equals(getText(txt_JournalDebitIPTxtA))) {
			writeTestResults("Verify that user can verify the journal entry of the created customer receipt voucher"
					,"user can verify the journal entry of the created customer receipt voucher"
					, "user can verify the journal entry of the created customer receipt voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can verify the journal entry of the created customer receipt voucher"
					,"user can verify the journal entry of the created customer receipt voucher"
					, "user can verify the journal entry of the created customer receipt voucher Fail", "fail");
		}
	}
	
	//FIN_11_16
	public void VerifyThatUserCanSetOffTheAdviceUsingCustomerReceiptVoccher() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);
		
		WaitElement(btn_CustomerAdvanceJourneyBtnA);
		click(btn_CustomerAdvanceJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Customer Advance")){
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Fail", "fail");
		}
		
		WaitElement(btn_CustomerLookUpIPABtnA);
		click(btn_CustomerLookUpIPABtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxtA);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		
		WaitElement(txt_AmountFieldIPATxtA);
		sendKeys(txt_AmountFieldIPATxtA, "100");
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Customer Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		//===================================================FIN_11_16
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
	}
	
	//FIN_11_17
	public void VerifyTheInboundPaymentWhenPayingCurrencyAndFilterCurrencyAreBaseCurrency() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);
		
		WaitElement(btn_CustomerAdvanceJourneyBtnA);
		click(btn_CustomerAdvanceJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Customer Advance")){
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Fail", "fail");
		}
		
		WaitElement(btn_CustomerLookUpIPABtnA);
		click(btn_CustomerLookUpIPABtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxtA);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		
		WaitElement(txt_AmountFieldIPATxtA);
		sendKeys(txt_AmountFieldIPATxtA, "100");
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Customer Advance","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Customer Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		//===================================================FIN_11_17
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(txt_FilterCurrencyIPTxtA);
		selectText(txt_FilterCurrencyIPTxtA, PaidCurrencySelA);
		
		WaitElement(txt_PayingAmountIPTxtA);
		sendKeys(txt_PayingAmountIPTxtA, "100");
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify the Inbound Payment when Paying currency and filter currency are base currency"
					,"Inbound Payment when Paying currency and filter currency are base currency"
					, "Inbound Payment when Paying currency and filter currency are base currency Successfully", "pass");
		}else {
			writeTestResults("Verify the Inbound Payment when Paying currency and filter currency are base currency"
					,"Inbound Payment when Paying currency and filter currency are base currency"
					, "Inbound Payment when Paying currency and filter currency are base currency Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_JournalActionBtnA);
		click(btn_JournalActionBtnA);
		
//		System.out.println(getText(txt_JournalCreditIPTxtA));
//		System.out.println(getText(txt_JournalDeditIPTxtA));
		
		if(getText(txt_JournalCreditIPTxtA).equals(getText(txt_JournalDebitIPTxtA))) {
			writeTestResults("Verify that user can verify the journal entry of the created customer receipt voucher"
					,"user can verify the journal entry of the created customer receipt voucher"
					, "user can verify the journal entry of the created customer receipt voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can verify the journal entry of the created customer receipt voucher"
					,"user can verify the journal entry of the created customer receipt voucher"
					, "user can verify the journal entry of the created customer receipt voucher Fail", "fail");
		}
	}
	
	//FIN_11_18
	public void VerifyTheInboundPaymentWhenPayingCurrencyAndFilterCurrencyAreForeignCurrency() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);
		
		WaitElement(btn_CustomerAdvanceJourneyBtnA);
		click(btn_CustomerAdvanceJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Customer Advance")){
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Fail", "fail");
		}
		
		WaitElement(btn_CustomerLookUpIPABtnA);
		click(btn_CustomerLookUpIPABtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxtA);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		
		WaitElement(txt_AmountFieldIPATxtA);
		sendKeys(txt_AmountFieldIPATxtA, "100");
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencyForignA);
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Customer Advance","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Customer Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		//===================================================FIN_11_18
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencyForignA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(txt_FilterCurrencyIPTxtA);
		selectText(txt_FilterCurrencyIPTxtA, PaidCurrencyForignA);
		
		WaitElement(txt_PayingAmountIPTxtA);
		sendKeys(txt_PayingAmountIPTxtA, "100");
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify the Inbound Payment when Paying currency and filter currency are foreign currency"
					,"Inbound Payment when Paying currency and filter currency are foreign currency"
					, "Inbound Payment when Paying currency and filter currency are foreign currency Successfully", "pass");
		}else {
			writeTestResults("Verify the Inbound Payment when Paying currency and filter currency are foreign currency"
					,"Inbound Payment when Paying currency and filter currency are foreign currency"
					, "Inbound Payment when Paying currency and filter currency are foreign currency Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_JournalActionBtnA);
		click(btn_JournalActionBtnA);
		
//		System.out.println(getText(txt_JournalCreditIPTxtA));
//		System.out.println(getText(txt_JournalDeditIPTxtA));
		
		if(getText(txt_JournalCreditIPTxtA).equals(getText(txt_JournalDebitIPTxtA))) {
			writeTestResults("Verify that user can verify the journal entry of the created customer receipt voucher"
					,"user can verify the journal entry of the created customer receipt voucher"
					, "user can verify the journal entry of the created customer receipt voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can verify the journal entry of the created customer receipt voucher"
					,"user can verify the journal entry of the created customer receipt voucher"
					, "user can verify the journal entry of the created customer receipt voucher Fail", "fail");
		}
	}
	
	//FIN_11_19
	public void VerifyTheInboundPaymentWhenPayingCurrencyIsBaseCurrencyAndFilterCurrencyIsForeignCurrency() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);
		
		WaitElement(btn_CustomerAdvanceJourneyBtnA);
		click(btn_CustomerAdvanceJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Customer Advance")){
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Fail", "fail");
		}
		
		WaitElement(btn_CustomerLookUpIPABtnA);
		click(btn_CustomerLookUpIPABtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxtA);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		
		WaitElement(txt_AmountFieldIPATxtA);
		sendKeys(txt_AmountFieldIPATxtA, "100");
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencyForignA);
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Customer Advance","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Customer Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		//===================================================FIN_11_19
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(txt_FilterCurrencyIPTxtA);
		selectText(txt_FilterCurrencyIPTxtA, PaidCurrencyForignA);
		
//		WaitElement(txt_PayingAmountIPTxtA);
//		sendKeys(txt_PayingAmountIPTxtA, "100");
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify the Inbound Payment when Paying currency and filter currency are foreign currency"
					,"Inbound Payment when Paying currency and filter currency are foreign currency"
					, "Inbound Payment when Paying currency and filter currency are foreign currency Successfully", "pass");
		}else {
			writeTestResults("Verify the Inbound Payment when Paying currency and filter currency are foreign currency"
					,"Inbound Payment when Paying currency and filter currency are foreign currency"
					, "Inbound Payment when Paying currency and filter currency are foreign currency Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_JournalActionBtnA);
		click(btn_JournalActionBtnA);
		
//		System.out.println(getText(txt_JournalCreditIPTxtA));
//		System.out.println(getText(txt_JournalDeditIPTxtA));
		
		if(getText(txt_JournalCreditIPTxtA).equals(getText(txt_JournalDebitIPTxtA))) {
			writeTestResults("Verify that user can verify the journal entry of the created customer receipt voucher"
					,"user can verify the journal entry of the created customer receipt voucher"
					, "user can verify the journal entry of the created customer receipt voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can verify the journal entry of the created customer receipt voucher"
					,"user can verify the journal entry of the created customer receipt voucher"
					, "user can verify the journal entry of the created customer receipt voucher Fail", "fail");
		}
	}
	
	//FIN_11_21to11_23
	public void VerifyThatUserCanSelectaVendorUsingTheVendorLookupNew() throws Exception {
		
		WaitElement(btn_InboundPaymentBtnA);
		click(btn_InboundPaymentBtnA);
		WaitElement(btn_NewInboundPaymentBtnA);
		click(btn_NewInboundPaymentBtnA);
		
		WaitElement(btn_VendorReceiptVoucherJourneyBtnA);
		click(btn_VendorReceiptVoucherJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Vendor Receipt")){
			writeTestResults("Navigation to Inbound Payment - New Vendor Receipt"
					,"User can Navigate Inbound Payment - New Vendor Receipt"
					, "User can Navigate Inbound Payment - New Vendor Receipt Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New Vendor Receipt"
					,"User can Navigate Inbound Payment - New Vendor Receipt"
					, "User can Navigate Inbound Payment - New Vendor Receipt Fail", "fail");
		}
		
		//===================================================FIN_11_21
		
		WaitElement(btn_VendorLookUpIPBtnA);
		click(btn_VendorLookUpIPBtnA);
		WaitElement(txt_VendorLookUpIPATxtA);
		sendKeys(txt_VendorLookUpIPATxtA, VendorLookUpIPATxtA);
		pressEnter(txt_VendorLookUpIPATxtA);
		WaitElement(sel_VendorLookUpIPASelA.replace("VenSel", VendorLookUpIPATxtA));
		doubleClick(sel_VendorLookUpIPASelA.replace("VenSel", VendorLookUpIPATxtA));
		
		WaitElement(txt_CustomerACCTxtA);
		String VendorTxt= driver.findElement(By.xpath(txt_CustomerACCTxtA)).getAttribute("value");
		
		if(VendorTxt.contentEquals(VendorLookUpIPATxtA)){
			writeTestResults("Verify that user can select a Vendor using the Vendor Lookup"
					,"user can select a Vendor using the Vendor Lookup"
					, "user can select a Vendor using the Vendor Lookup Successfully", "pass");
		}else {
			writeTestResults("Verify that user can select a Vendor using the Vendor Lookup"
					,"user can select a Vendor using the Vendor Lookup"
					, "user can select a Vendor using the Vendor Lookup Fail", "fail");
		}
		
		//===================================================FIN_11_22
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		String PaymentMethodSelValueA = new Select(driver.findElement(By.xpath(sel_PaymentMethodSelA))).getFirstSelectedOption().getText();
		if(PaymentMethodSelValueA.contentEquals(PaymentMethodSelA)){
			writeTestResults("Verify that user can select a Payment method using the drop down"
					,"user can select a Payment method using the drop down"
					, "user can select a Payment method using the drop down Successfully", "pass");
		}else {
			writeTestResults("Verify that user can select a Payment method using the drop down"
					,"user can select a Payment method using the drop down"
					, "user can select a Payment method using the drop down Fail", "fail");
		}
		
		//===================================================FIN_11_23
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		String PaidCurrencySelAValueA = new Select(driver.findElement(By.xpath(sel_PaidCurrencySelA))).getFirstSelectedOption().getText();
		if(PaidCurrencySelAValueA.contentEquals(PaidCurrencySelA)){
			writeTestResults("Verify that user can select a Paid currency using the drop down"
					,"user can select a Paid currency using the drop down"
					, "user can select a Paid currency using the drop down Successfully", "pass");
		}else {
			writeTestResults("Verify that user can select a Paid currency using the drop down"
					,"user can select a Paid currency using the drop down"
					, "user can select a Paid currency using the drop down Fail", "fail");
		}
	}
	
	//FIN_11_24
	public void VerifyThatUserCanAbleToCreateVendorReceiptVoucherUsingCashPayMethod() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);

		WaitElement(btn_VendorRefundJourneyBtnA);
		click(btn_VendorRefundJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Vendor Refund")){
			writeTestResults("Navigation to Inbound Payment Advice - New Vendor Refund page"
					,"User can Navigate Inbound Payment Advice - New Vendor Refund page"
					, "User can Navigate Inbound Payment Advice - New Vendor Refund page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Vendor Refund page"
					,"User can Navigate Inbound Payment Advice - New Vendor Refund page"
					, "User can Navigate Inbound Payment Advice - New Vendor Refund page Fail", "fail");
		}
		
		WaitElement(btn_VendorLookUpIPABtnA);
		click(btn_VendorLookUpIPABtnA);
		WaitElement(txt_VendorLookUpIPATxtA);
		sendKeys(txt_VendorLookUpIPATxtA, VendorLookUpIPATxtA);
		pressEnter(txt_VendorLookUpIPATxtA);
		WaitElement(sel_VendorLookUpIPASelA.replace("VenSel", VendorLookUpIPATxtA));
		doubleClick(sel_VendorLookUpIPASelA.replace("VenSel", VendorLookUpIPATxtA));
		
		WaitElement(btn_GLAccIPABtnA);
		click(btn_GLAccIPABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Vendor Refund"
					,"user can use Draft the Vendor Refund"
					, "user can use Draft the Vendor Refund Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Vendor Refund Form"
					,"user can use Draft the Vendor Refund Form"
					, "user can use Draft the Vendor Refund Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Vendor Refund","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Vendor Refund"
					,"user can release the Vendor Refund"
					, "user can release the Vendor Refund Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Vendor Refund"
					,"user can release the Vendor Refund"
					, "user can release the Vendor Refund Fail", "fail");
		}
		
		//===================================================FIN_11_24
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Vendor Receipt")){
			writeTestResults("Navigation to Inbound Payment - New Vendor Receipt page"
					,"User can Navigate Inbound Payment - New Vendor Receipt page"
					, "User can Navigate Inbound Payment - New Vendor Receipt page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New Vendor Receipt page"
					,"User can Navigate Inbound Payment - New Vendor Receipt page"
					, "User can Navigate Inbound Payment - New Vendor Receipt page Fail", "fail");
		}
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Vendor Receipt Form"
					,"user can use Draft the Inbound Payment - New Vendor Receipt Form"
					, "user can use Draft the Inbound Payment - New Vendor Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Vendor Receipt Form"
					,"user can use Draft the Inbound Payment - New Vendor Receipt Form"
					, "user can use Draft the Inbound Payment - New Vendor Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can able to create vendor receipt voucher using 'Cash' pay method"
					,"user can able to create vendor receipt voucher using 'Cash' pay method"
					, "user can able to create vendor receipt voucher using 'Cash' pay method Successfully", "pass");
		}else {
			writeTestResults("Verify that user can able to create vendor receipt voucher using 'Cash' pay method"
					,"user can able to create vendor receipt voucher using 'Cash' pay method"
					, "user can able to create vendor receipt voucher using 'Cash' pay method Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_JournalActionBtnA);
		click(btn_JournalActionBtnA);
		
//		System.out.println(getText(txt_JournalCreditIPTxtA));
//		System.out.println(getText(txt_JournalDeditIPTxtA));
		
		if(getText(txt_JournalCreditIPTxtA).equals(getText(txt_JournalDebitIPTxtA))) {
			writeTestResults("Verify that user can verify the journal entry of the created vendor receipt voucher"
					,"user can verify the journal entry of the created vendor receipt voucher"
					, "user can verify the journal entry of the created vendor receipt voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can verify the journal entry of the created vendor receipt voucher"
					,"user can verify the journal entry of the created vendor receipt voucher"
					, "user can verify the journal entry of the created vendor receipt voucher Fail", "fail");
		}
	}
	
	//FIN_11_25
	public void VerifyThatUserCanAbleToCreateVendorReceiptVoucherUsingChequePayMethod() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);

		WaitElement(btn_VendorRefundJourneyBtnA);
		click(btn_VendorRefundJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Vendor Refund")){
			writeTestResults("Navigation to Inbound Payment Advice - New Vendor Refund page"
					,"User can Navigate Inbound Payment Advice - New Vendor Refund page"
					, "User can Navigate Inbound Payment Advice - New Vendor Refund page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Vendor Refund page"
					,"User can Navigate Inbound Payment Advice - New Vendor Refund page"
					, "User can Navigate Inbound Payment Advice - New Vendor Refund page Fail", "fail");
		}
		
		WaitElement(btn_VendorLookUpIPABtnA);
		click(btn_VendorLookUpIPABtnA);
		WaitElement(txt_VendorLookUpIPATxtA);
		sendKeys(txt_VendorLookUpIPATxtA, VendorLookUpIPATxtA);
		pressEnter(txt_VendorLookUpIPATxtA);
		WaitElement(sel_VendorLookUpIPASelA.replace("VenSel", VendorLookUpIPATxtA));
		doubleClick(sel_VendorLookUpIPASelA.replace("VenSel", VendorLookUpIPATxtA));
		
		WaitElement(btn_GLAccIPABtnA);
		click(btn_GLAccIPABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Vendor Refund"
					,"user can use Draft the Vendor Refund"
					, "user can use Draft the Vendor Refund Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Vendor Refund Form"
					,"user can use Draft the Vendor Refund Form"
					, "user can use Draft the Vendor Refund Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Vendor Refund","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Vendor Refund"
					,"user can release the Vendor Refund"
					, "user can release the Vendor Refund Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Vendor Refund"
					,"user can release the Vendor Refund"
					, "user can release the Vendor Refund Fail", "fail");
		}
		
		//===================================================FIN_11_25
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Vendor Receipt")){
			writeTestResults("Navigation to Inbound Payment - New Vendor Receipt page"
					,"User can Navigate Inbound Payment - New Vendor Receipt page"
					, "User can Navigate Inbound Payment - New Vendor Receipt page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New Vendor Receipt page"
					,"User can Navigate Inbound Payment - New Vendor Receipt page"
					, "User can Navigate Inbound Payment - New Vendor Receipt page Fail", "fail");
		}
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSel2A);
		
		WaitElement(sel_BankNameIPSelA);
		selectText(sel_BankNameIPSelA, BankNameIPSelA);
		
		WaitElement(txt_ChequeNoIPTxtA);
		LocalTime Obj1 = LocalTime.now();
		sendKeys(txt_ChequeNoIPTxtA, "CHK"+Obj1);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Vendor receipt Form"
					,"user can use Draft the Inbound Payment - New Vendor receipt Form"
					, "user can use Draft the Inbound Payment - New Vendor receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Vendor receipt Form"
					,"user can use Draft the Inbound Payment - New Vendor receipt Form"
					, "user can use Draft the Inbound Payment - New Vendor receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can able to create Vendor receipt voucher using 'Cheque' pay method"
					,"user can able to create Vendor receipt voucher using 'Cheque' pay method"
					, "user can able to create Vendor receipt voucher using 'Cheque' pay method Successfully", "pass");
		}else {
			writeTestResults("Verify that user can able to create Vendor receipt voucher using 'Cheque' pay method"
					,"user can able to create Vendor receipt voucher using 'Cheque' pay method"
					, "user can able to create Vendor receipt voucher using 'Cheque' pay method Fail", "fail");
		}
	}
	
	//FIN_11_26and11_27
	public void VerifyThatUserCanAbleToAccessAboveCreatedChequeAfterTheMentionedChequeDateInBankDeposit2() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);

		WaitElement(btn_VendorRefundJourneyBtnA);
		click(btn_VendorRefundJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Vendor Refund")){
			writeTestResults("Navigation to Inbound Payment Advice - New Vendor Refund page"
					,"User can Navigate Inbound Payment Advice - New Vendor Refund page"
					, "User can Navigate Inbound Payment Advice - New Vendor Refund page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Vendor Refund page"
					,"User can Navigate Inbound Payment Advice - New Vendor Refund page"
					, "User can Navigate Inbound Payment Advice - New Vendor Refund page Fail", "fail");
		}
		
		WaitElement(btn_VendorLookUpIPABtnA);
		click(btn_VendorLookUpIPABtnA);
		WaitElement(txt_VendorLookUpIPATxtA);
		sendKeys(txt_VendorLookUpIPATxtA, VendorLookUpIPATxtA);
		pressEnter(txt_VendorLookUpIPATxtA);
		WaitElement(sel_VendorLookUpIPASelA.replace("VenSel", VendorLookUpIPATxtA));
		doubleClick(sel_VendorLookUpIPASelA.replace("VenSel", VendorLookUpIPATxtA));
		
		WaitElement(btn_GLAccIPABtnA);
		click(btn_GLAccIPABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Vendor Refund"
					,"user can use Draft the Vendor Refund"
					, "user can use Draft the Vendor Refund Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Vendor Refund Form"
					,"user can use Draft the Vendor Refund Form"
					, "user can use Draft the Vendor Refund Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Vendor Refund","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Vendor Refund"
					,"user can release the Vendor Refund"
					, "user can release the Vendor Refund Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Vendor Refund"
					,"user can release the Vendor Refund"
					, "user can release the Vendor Refund Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Vendor Receipt")){
			writeTestResults("Navigation to Inbound Payment - New Vendor Receipt page"
					,"User can Navigate Inbound Payment - New Vendor Receipt page"
					, "User can Navigate Inbound Payment - New Vendor Receipt page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New Vendor Receipt page"
					,"User can Navigate Inbound Payment - New Vendor Receipt page"
					, "User can Navigate Inbound Payment - New Vendor Receipt page Fail", "fail");
		}
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSel2A);
		
		WaitElement(sel_BankNameIPSelA);
		selectText(sel_BankNameIPSelA, BankNameIPSelA);
		
		WaitElement(txt_ChequeNoIPTxtA);
		LocalTime Obj1 = LocalTime.now();
		sendKeys(txt_ChequeNoIPTxtA, "CHK"+Obj1);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Vendor receipt Form"
					,"user can use Draft the Inbound Payment - New Vendor receipt Form"
					, "user can use Draft the Inbound Payment - New Vendor receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Vendor receipt Form"
					,"user can use Draft the Inbound Payment - New Vendor receipt Form"
					, "user can use Draft the Inbound Payment - New Vendor receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPNo = trackCode.replace(" Vendor Receipt", "");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can able to create Vendor receipt voucher using 'Cheque' pay method"
					,"user can able to create Vendor receipt voucher using 'Cheque' pay method"
					, "user can able to create Vendor receipt voucher using 'Cheque' pay method Successfully", "pass");
		}else {
			writeTestResults("Verify that user can able to create Vendor receipt voucher using 'Cheque' pay method"
					,"user can able to create Vendor receipt voucher using 'Cheque' pay method"
					, "user can able to create Vendor receipt voucher using 'Cheque' pay method Fail", "fail");
		}
		
	//===================================================FIN_11_26
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_BankDepositBtnA);
		click(btn_BankDepositBtnA);
		WaitElement(btn_NewBankDepositBtnA);
		click(btn_NewBankDepositBtnA);
		
		WaitElement(sel_PayMethodBDSelA);
		selectText(sel_PayMethodBDSelA, PaymentMethodSel2A);
		
		WaitElement(sel_BankBDSelA);
		selectText(sel_BankBDSelA, BankBDSelA);
		
		WaitElement(sel_BankAccountNoBDSelA);
		selectText(sel_BankAccountNoBDSelA, BankAccountNoBDSelA);
		
		WaitElement(btn_SortByBDBtnA);
		selectText(btn_SortByBDBtnA, "Cheque No");
		
		WaitElement(txt_CheqNoBDTxtA);
		sendKeys(txt_CheqNoBDTxtA, "CHK"+Obj1);
		
		WaitClick(btn_ViewBDBtnA);
		click(btn_ViewBDBtnA);
		
		ScrollDownAruna();
		
		WaitElement(btn_DolSelBDBtnA.replace("IPNo", IPNo));
		if(isDisplayed(btn_DolSelBDBtnA.replace("IPNo", IPNo))) {
			writeTestResults("Verify that user can able to access above created cheque after the mentioned cheque date in bank deposit"
					,"user can able to access above created cheque after the mentioned cheque date in bank deposit"
					, "user can able to access above created cheque after the mentioned cheque date in bank deposit Successfully", "pass");
			Thread.sleep(4000);
			click(btn_DolSelBDBtnA.replace("IPNo", IPNo));
		}else {
			writeTestResults("Verify that user can able to access above created cheque after the mentioned cheque date in bank deposit"
					,"user can able to access above created cheque after the mentioned cheque date in bank deposit"
					, "user can able to access above created cheque after the mentioned cheque date in bank deposit Fail", "fail");
		}
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Bank Deposit Form"
					,"user can use Draft the Bank Deposit Form"
					, "user can use Draft the Bank Deposit Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Bank Deposit Form"
					,"user can use Draft the Bank Deposit Form"
					, "user can use Draft the Bank Deposit Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can use Release the Bank Deposit Form"
					,"user can use Release the Bank Deposit Form"
					, "user can use Release the Bank Deposit Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Release the Bank Deposit Form"
					,"user can use Release the Bank Deposit Form"
					, "user can use Release the Bank Deposit Form Fail", "fail");
		}
		
		//===================================================FIN_11_27
		
		for (int i = 1; i <=3; i++) {
			driver.navigate().back();
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ChequeReturnBtnIPA);
		click(btn_ChequeReturnBtnIPA);
		
		WaitElement(sel_ReturnResonSelIPA);
		selectText(sel_ReturnResonSelIPA, "Funds insufficient");
		
		JavascriptExecutor j7 = (JavascriptExecutor)driver;
		j7.executeScript("$('#txtReturnDate').datepicker('setDate', new Date())");
		
		WaitElement(txt_ReturnRemarkTxtIPA);
		sendKeys(txt_ReturnRemarkTxtIPA, "Return CHK"+Obj1);
		
		WaitClick(btn_ReturnBtnIPABtnIPA);
		click(btn_ReturnBtnIPABtnIPA);
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		
		if(!isDisplayed(btn_ChequeReturnBtnIPA)) {
			writeTestResults("Verify that user can do Cheque return transaction"
					,"user can do Cheque return transaction"
					, "user can do Cheque return transaction Successfully", "pass");
		}else {
			writeTestResults("Verify that user can do Cheque return transaction"
					,"user can do Cheque return transaction"
					, "user can do Cheque return transaction Fail", "fail");
		}
	}
	
	//FIN_11_28
	public void VerifyThatUserCanAbleToCreateVendorReceiptVoucherUsingEFTPayMethod() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);

		WaitElement(btn_VendorRefundJourneyBtnA);
		click(btn_VendorRefundJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Vendor Refund")){
			writeTestResults("Navigation to Inbound Payment Advice - New Vendor Refund page"
					,"User can Navigate Inbound Payment Advice - New Vendor Refund page"
					, "User can Navigate Inbound Payment Advice - New Vendor Refund page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Vendor Refund page"
					,"User can Navigate Inbound Payment Advice - New Vendor Refund page"
					, "User can Navigate Inbound Payment Advice - New Vendor Refund page Fail", "fail");
		}
		
		WaitElement(btn_VendorLookUpIPABtnA);
		click(btn_VendorLookUpIPABtnA);
		WaitElement(txt_VendorLookUpIPATxtA);
		sendKeys(txt_VendorLookUpIPATxtA, VendorLookUpIPATxtA);
		pressEnter(txt_VendorLookUpIPATxtA);
		WaitElement(sel_VendorLookUpIPASelA.replace("VenSel", VendorLookUpIPATxtA));
		doubleClick(sel_VendorLookUpIPASelA.replace("VenSel", VendorLookUpIPATxtA));
		
		WaitElement(btn_GLAccIPABtnA);
		click(btn_GLAccIPABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Vendor Refund"
					,"user can use Draft the Vendor Refund"
					, "user can use Draft the Vendor Refund Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Vendor Refund Form"
					,"user can use Draft the Vendor Refund Form"
					, "user can use Draft the Vendor Refund Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Vendor Refund","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Vendor Refund"
					,"user can release the Vendor Refund"
					, "user can release the Vendor Refund Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Vendor Refund"
					,"user can release the Vendor Refund"
					, "user can release the Vendor Refund Fail", "fail");
		}
		
		//===================================================FIN_11_28
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Vendor Receipt")){
			writeTestResults("Navigation to Inbound Payment - New Vendor Receipt page"
					,"User can Navigate Inbound Payment - New Vendor Receipt page"
					, "User can Navigate Inbound Payment - New Vendor Receipt page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New Vendor Receipt page"
					,"User can Navigate Inbound Payment - New Vendor Receipt page"
					, "User can Navigate Inbound Payment - New Vendor Receipt page Fail", "fail");
		}
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSel3A);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Vendor receipt Form"
					,"user can use Draft the Inbound Payment - New Vendor receipt Form"
					, "user can use Draft the Inbound Payment - New Vendor receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Vendor receipt Form"
					,"user can use Draft the Inbound Payment - New Vendor receipt Form"
					, "user can use Draft the Inbound Payment - New Vendor receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can able to create Vendor receipt voucher using 'EFT' pay method"
					,"user can able to create customer receipt voucher using 'EFT' pay method"
					, "user can able to create customer receipt voucher using 'EFT' pay method Successfully", "pass");
		}else {
			writeTestResults("Verify that user can able to create Vendor receipt voucher using 'EFT' pay method"
					,"user can able to create customer receipt voucher using 'EFT' pay method"
					, "user can able to create customer receipt voucher using 'EFT' pay method Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_JournalActionBtnA);
		click(btn_JournalActionBtnA);
		
//		System.out.println(getText(txt_JournalCreditIPTxtA));
//		System.out.println(getText(txt_JournalDeditIPTxtA));
		
		if(getText(txt_JournalCreditIPTxtA).equals(getText(txt_JournalDebitIPTxtA))) {
			writeTestResults("Verify that user can verify the journal entry of the created Vendor receipt voucher"
					,"user can verify the journal entry of the created Vendor receipt voucher"
					, "user can verify the journal entry of the created Vendor receipt voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can verify the journal entry of the created Vendor receipt voucher"
					,"user can verify the journal entry of the created Vendor receipt voucher"
					, "user can verify the journal entry of the created Vendor receipt voucher Fail", "fail");
		}
	}
	
	//FIN_11_29
	public void VerifyThatUserCanAbleToCreateVendorReceiptVoucherUsingDirectDepositPayMethod() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);

		WaitElement(btn_VendorRefundJourneyBtnA);
		click(btn_VendorRefundJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Vendor Refund")){
			writeTestResults("Navigation to Inbound Payment Advice - New Vendor Refund page"
					,"User can Navigate Inbound Payment Advice - New Vendor Refund page"
					, "User can Navigate Inbound Payment Advice - New Vendor Refund page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Vendor Refund page"
					,"User can Navigate Inbound Payment Advice - New Vendor Refund page"
					, "User can Navigate Inbound Payment Advice - New Vendor Refund page Fail", "fail");
		}
		
		WaitElement(btn_VendorLookUpIPABtnA);
		click(btn_VendorLookUpIPABtnA);
		WaitElement(txt_VendorLookUpIPATxtA);
		sendKeys(txt_VendorLookUpIPATxtA, VendorLookUpIPATxtA);
		pressEnter(txt_VendorLookUpIPATxtA);
		WaitElement(sel_VendorLookUpIPASelA.replace("VenSel", VendorLookUpIPATxtA));
		doubleClick(sel_VendorLookUpIPASelA.replace("VenSel", VendorLookUpIPATxtA));
		
		WaitElement(btn_GLAccIPABtnA);
		click(btn_GLAccIPABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Vendor Refund"
					,"user can use Draft the Vendor Refund"
					, "user can use Draft the Vendor Refund Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Vendor Refund Form"
					,"user can use Draft the Vendor Refund Form"
					, "user can use Draft the Vendor Refund Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Vendor Refund","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Vendor Refund"
					,"user can release the Vendor Refund"
					, "user can release the Vendor Refund Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Vendor Refund"
					,"user can release the Vendor Refund"
					, "user can release the Vendor Refund Fail", "fail");
		}
		
		//===================================================FIN_11_29
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Vendor Receipt")){
			writeTestResults("Navigation to Inbound Payment - New Vendor Receipt page"
					,"User can Navigate Inbound Payment - New Vendor Receipt page"
					, "User can Navigate Inbound Payment - New Vendor Receipt page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New Vendor Receipt page"
					,"User can Navigate Inbound Payment - New Vendor Receipt page"
					, "User can Navigate Inbound Payment - New Vendor Receipt page Fail", "fail");
		}
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSel4A);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Vendor Receipt Form"
					,"user can use Draft the Inbound Payment - New Vendor Receipt Form"
					, "user can use Draft the Inbound Payment - New Vendor Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Vendor Receipt Form"
					,"user can use Draft the Inbound Payment - New Vendor Receipt Form"
					, "user can use Draft the Inbound Payment - New Vendor Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can able to create Vendor Receipt voucher using 'Direct Deposit' pay method"
					,"user can able to create Vendor Receipt voucher using 'Direct Deposit' pay method"
					, "user can able to create Vendor Receipt voucher using 'Direct Deposit' pay method Successfully", "pass");
		}else {
			writeTestResults("Verify that user can able to create Vendor Receipt voucher using 'Direct Deposit' pay method"
					,"user can able to create Vendor Receipt voucher using 'Direct Deposit' pay method"
					, "user can able to create Vendor Receipt voucher using 'Direct Deposit' pay method Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_JournalActionBtnA);
		click(btn_JournalActionBtnA);
		
//		System.out.println(getText(txt_JournalCreditIPTxtA));
//		System.out.println(getText(txt_JournalDeditIPTxtA));
		
		if(getText(txt_JournalCreditIPTxtA).equals(getText(txt_JournalDebitIPTxtA))) {
			writeTestResults("Verify that user can verify the journal entry of the created Vendor Receipt voucher"
					,"user can verify the journal entry of the created Vendor Receipt voucher"
					, "user can verify the journal entry of the created Vendor Receipt voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can verify the journal entry of the created Vendor Receipt voucher"
					,"user can verify the journal entry of the created Vendor Receipt voucher"
					, "user can verify the journal entry of the created Vendor Receipt voucher Fail", "fail");
		}
	}
	
	//FIN_11_30
	public void VerifyTheInboundPaymentWhenPayingCurrencyAndFilterCurrencyAreBaseCurrencyNew() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);

		WaitElement(btn_VendorRefundJourneyBtnA);
		click(btn_VendorRefundJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Vendor Refund")){
			writeTestResults("Navigation to Inbound Payment Advice - New Vendor Refund page"
					,"User can Navigate Inbound Payment Advice - New Vendor Refund page"
					, "User can Navigate Inbound Payment Advice - New Vendor Refund page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Vendor Refund page"
					,"User can Navigate Inbound Payment Advice - New Vendor Refund page"
					, "User can Navigate Inbound Payment Advice - New Vendor Refund page Fail", "fail");
		}
		
		WaitElement(btn_VendorLookUpIPABtnA);
		click(btn_VendorLookUpIPABtnA);
		WaitElement(txt_VendorLookUpIPATxtA);
		sendKeys(txt_VendorLookUpIPATxtA, VendorLookUpIPATxtA);
		pressEnter(txt_VendorLookUpIPATxtA);
		WaitElement(sel_VendorLookUpIPASelA.replace("VenSel", VendorLookUpIPATxtA));
		doubleClick(sel_VendorLookUpIPASelA.replace("VenSel", VendorLookUpIPATxtA));
		
		WaitElement(btn_GLAccIPABtnA);
		click(btn_GLAccIPABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "100");
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Vendor Refund"
					,"user can use Draft the Vendor Refund"
					, "user can use Draft the Vendor Refund Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Vendor Refund Form"
					,"user can use Draft the Vendor Refund Form"
					, "user can use Draft the Vendor Refund Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Vendor Refund","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Vendor Refund"
					,"user can release the Vendor Refund"
					, "user can release the Vendor Refund Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Vendor Refund"
					,"user can release the Vendor Refund"
					, "user can release the Vendor Refund Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Vendor Receipt")){
			writeTestResults("Navigation to Inbound Payment - New Vendor Receipt page"
					,"User can Navigate Inbound Payment - New Vendor Receipt page"
					, "User can Navigate Inbound Payment - New Vendor Receipt page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New Vendor Receipt page"
					,"User can Navigate Inbound Payment - New Vendor Receipt page"
					, "User can Navigate Inbound Payment - New Vendor Receipt page Fail", "fail");
		}
		
		//===================================================FIN_11_30
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(txt_FilterCurrencyIPTxtA);
		selectText(txt_FilterCurrencyIPTxtA, PaidCurrencySelA);
		
//		WaitElement(txt_PayingAmountIPTxtA);
//		sendKeys(txt_PayingAmountIPTxtA, "100");
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Vendor Receipt Form"
					,"user can use Draft the Inbound Payment - New Vendor Receipt Form"
					, "user can use Draft the Inbound Payment - New Vendor Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Vendor Receipt Form"
					,"user can use Draft the Inbound Payment - New Vendor Receipt Form"
					, "user can use Draft the Inbound Payment - New Vendor Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify the Inbound Payment when Paying currency and filter currency are base currency"
					,"Inbound Payment when Paying currency and filter currency are base currency"
					, "Inbound Payment when Paying currency and filter currency are base currency Successfully", "pass");
		}else {
			writeTestResults("Verify the Inbound Payment when Paying currency and filter currency are base currency"
					,"Inbound Payment when Paying currency and filter currency are base currency"
					, "Inbound Payment when Paying currency and filter currency are base currency Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_JournalActionBtnA);
		click(btn_JournalActionBtnA);
		
//		System.out.println(getText(txt_JournalCreditIPTxtA));
//		System.out.println(getText(txt_JournalDeditIPTxtA));
		
		if(getText(txt_JournalCreditIPTxtA).equals(getText(txt_JournalDebitIPTxtA))) {
			writeTestResults("Verify that user can verify the journal entry of the created vendor receipt voucher"
					,"user can verify the journal entry of the created vendor receipt voucher"
					, "user can verify the journal entry of the created vendor receipt voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can verify the journal entry of the created vendor receipt voucher"
					,"user can verify the journal entry of the created vendor receipt voucher"
					, "user can verify the journal entry of the created vendor receipt voucher Fail", "fail");
		}
	}
	
	//FIN_11_31
	public void VerifyTheInboundPaymentWhenPayingCurrencyAndFilterCurrencyAreForeignCurrencyNew() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);

		WaitElement(btn_VendorRefundJourneyBtnA);
		click(btn_VendorRefundJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Vendor Refund")){
			writeTestResults("Navigation to Inbound Payment Advice - New Vendor Refund page"
					,"User can Navigate Inbound Payment Advice - New Vendor Refund page"
					, "User can Navigate Inbound Payment Advice - New Vendor Refund page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Vendor Refund page"
					,"User can Navigate Inbound Payment Advice - New Vendor Refund page"
					, "User can Navigate Inbound Payment Advice - New Vendor Refund page Fail", "fail");
		}
		
		WaitElement(btn_VendorLookUpIPABtnA);
		click(btn_VendorLookUpIPABtnA);
		WaitElement(txt_VendorLookUpIPATxtA);
		sendKeys(txt_VendorLookUpIPATxtA, VendorLookUpIPATxtA);
		pressEnter(txt_VendorLookUpIPATxtA);
		WaitElement(sel_VendorLookUpIPASelA.replace("VenSel", VendorLookUpIPATxtA));
		doubleClick(sel_VendorLookUpIPASelA.replace("VenSel", VendorLookUpIPATxtA));
		
		WaitElement(btn_GLAccIPABtnA);
		click(btn_GLAccIPABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "100");
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencyForignA);
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Vendor Refund"
					,"user can use Draft the Vendor Refund"
					, "user can use Draft the Vendor Refund Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Vendor Refund Form"
					,"user can use Draft the Vendor Refund Form"
					, "user can use Draft the Vendor Refund Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Vendor Refund","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Vendor Refund"
					,"user can release the Vendor Refund"
					, "user can release the Vendor Refund Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Vendor Refund"
					,"user can release the Vendor Refund"
					, "user can release the Vendor Refund Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Vendor Receipt")){
			writeTestResults("Navigation to Inbound Payment - New Vendor Receipt page"
					,"User can Navigate Inbound Payment - New Vendor Receipt page"
					, "User can Navigate Inbound Payment - New Vendor Receipt page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New Vendor Receipt page"
					,"User can Navigate Inbound Payment - New Vendor Receipt page"
					, "User can Navigate Inbound Payment - New Vendor Receipt page Fail", "fail");
		}
		
		//===================================================FIN_11_31
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencyForignA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(txt_FilterCurrencyIPTxtA);
		selectText(txt_FilterCurrencyIPTxtA, PaidCurrencyForignA);
		
//		WaitElement(txt_PayingAmountIPTxtA);
//		sendKeys(txt_PayingAmountIPTxtA, "100");
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Vendor Receipt Form"
					,"user can use Draft the Inbound Payment - New Vendor Receipt Form"
					, "user can use Draft the Inbound Payment - New Vendor Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Vendor Receipt Form"
					,"user can use Draft the Inbound Payment - New Vendor Receipt Form"
					, "user can use Draft the Inbound Payment - New Vendor Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify the Inbound Payment when Paying currency and filter currency are foreign currency"
					,"Inbound Payment when Paying currency and filter currency are foreign currency"
					, "Inbound Payment when Paying currency and filter currency are foreign currency Successfully", "pass");
		}else {
			writeTestResults("Verify the Inbound Payment when Paying currency and filter currency are foreign currency"
					,"Inbound Payment when Paying currency and filter currency are foreign currency"
					, "Inbound Payment when Paying currency and filter currency are foreign currency Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_JournalActionBtnA);
		click(btn_JournalActionBtnA);
		
//		System.out.println(getText(txt_JournalCreditIPTxtA));
//		System.out.println(getText(txt_JournalDeditIPTxtA));
		
		if(getText(txt_JournalCreditIPTxtA).equals(getText(txt_JournalDebitIPTxtA))) {
			writeTestResults("Verify that user can verify the journal entry of the created vendor receipt voucher"
					,"user can verify the journal entry of the created vendor receipt voucher"
					, "user can verify the journal entry of the created vendor receipt voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can verify the journal entry of the created vendor receipt voucher"
					,"user can verify the journal entry of the created vendor receipt voucher"
					, "user can verify the journal entry of the created vendor receipt voucher Fail", "fail");
		}
	}
	
	//FIN_11_32
	public void VerifyTheInboundPaymentWhenPayingCurrencyIsBaseCurrencyAndFilterCurrencyIsForeignCurrencyNew() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);

		WaitElement(btn_VendorRefundJourneyBtnA);
		click(btn_VendorRefundJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Vendor Refund")){
			writeTestResults("Navigation to Inbound Payment Advice - New Vendor Refund page"
					,"User can Navigate Inbound Payment Advice - New Vendor Refund page"
					, "User can Navigate Inbound Payment Advice - New Vendor Refund page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Vendor Refund page"
					,"User can Navigate Inbound Payment Advice - New Vendor Refund page"
					, "User can Navigate Inbound Payment Advice - New Vendor Refund page Fail", "fail");
		}
		
		WaitElement(btn_VendorLookUpIPABtnA);
		click(btn_VendorLookUpIPABtnA);
		WaitElement(txt_VendorLookUpIPATxtA);
		sendKeys(txt_VendorLookUpIPATxtA, VendorLookUpIPATxtA);
		pressEnter(txt_VendorLookUpIPATxtA);
		WaitElement(sel_VendorLookUpIPASelA.replace("VenSel", VendorLookUpIPATxtA));
		doubleClick(sel_VendorLookUpIPASelA.replace("VenSel", VendorLookUpIPATxtA));
		
		WaitElement(btn_GLAccIPABtnA);
		click(btn_GLAccIPABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "100");
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencyForignA);
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Vendor Refund"
					,"user can use Draft the Vendor Refund"
					, "user can use Draft the Vendor Refund Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Vendor Refund Form"
					,"user can use Draft the Vendor Refund Form"
					, "user can use Draft the Vendor Refund Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Vendor Refund","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Vendor Refund"
					,"user can release the Vendor Refund"
					, "user can release the Vendor Refund Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Vendor Refund"
					,"user can release the Vendor Refund"
					, "user can release the Vendor Refund Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Vendor Receipt")){
			writeTestResults("Navigation to Inbound Payment - New Vendor Receipt page"
					,"User can Navigate Inbound Payment - New Vendor Receipt page"
					, "User can Navigate Inbound Payment - New Vendor Receipt page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New Vendor Receipt page"
					,"User can Navigate Inbound Payment - New Vendor Receipt page"
					, "User can Navigate Inbound Payment - New Vendor Receipt page Fail", "fail");
		}
		
		//===================================================FIN_11_32
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(txt_FilterCurrencyIPTxtA);
		selectText(txt_FilterCurrencyIPTxtA, PaidCurrencyForignA);
		
//		WaitElement(txt_PayingAmountIPTxtA);
//		sendKeys(txt_PayingAmountIPTxtA, "100");
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Vendor Receipt Form"
					,"user can use Draft the Inbound Payment - New Vendor Receipt Form"
					, "user can use Draft the Inbound Payment - New Vendor Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Vendor Receipt Form"
					,"user can use Draft the Inbound Payment - New Vendor Receipt Form"
					, "user can use Draft the Inbound Payment - New Vendor Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify the Inbound Payment when Paying currency is Base currency and filter currency is foreign currency"
					,"Inbound Payment when Paying currency is Base currency and filter currency is foreign currency"
					, "Inbound Payment when Paying currency is Base currency and filter currency is foreign currency Successfully", "pass");
		}else {
			writeTestResults("Verify the Inbound Payment when Paying currency is Base currency and filter currency is foreign currency"
					,"Inbound Payment when Paying currency is Base currency and filter currency is foreign currency"
					, "Inbound Payment when Paying currency is Base currency and filter currency is foreign currency Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_JournalActionBtnA);
		click(btn_JournalActionBtnA);
		
//		System.out.println(getText(txt_JournalCreditIPTxtA));
//		System.out.println(getText(txt_JournalDeditIPTxtA));
		
		if(getText(txt_JournalCreditIPTxtA).equals(getText(txt_JournalDebitIPTxtA))) {
			writeTestResults("Verify that user can verify the journal entry of the created vendor receipt voucher"
					,"user can verify the journal entry of the created vendor receipt voucher"
					, "user can verify the journal entry of the created vendor receipt voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can verify the journal entry of the created vendor receipt voucher"
					,"user can verify the journal entry of the created vendor receipt voucher"
					, "user can verify the journal entry of the created vendor receipt voucher Fail", "fail");
		}
	}
	
	//FIN_11_33to11_35
	public void VerifyThatUserCanSelectaEmployeeUsingTheEmployeeLookupNew() throws Exception {
		
		WaitElement(btn_InboundPaymentBtnA);
		click(btn_InboundPaymentBtnA);
		WaitElement(btn_NewInboundPaymentBtnA);
		click(btn_NewInboundPaymentBtnA);
		
		WaitElement(btn_EmployeeReceiptVoucherJourneyBtnA);
		click(btn_EmployeeReceiptVoucherJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Employee Receipt")){
			writeTestResults("Navigation to Inbound Payment - New Employee Receipt"
					,"User can Navigate Inbound Payment - New Employee Receipt"
					, "User can Navigate Inbound Payment - New Employee Receipt Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New Employee Receipt"
					,"User can Navigate Inbound Payment - New Employee Receipt"
					, "User can Navigate Inbound Payment - New Employee Receipt Fail", "fail");
		}
		
		//===================================================FIN_11_33
		
		WaitElement(btn_EmployeeLookupIPBtnA);
		click(btn_EmployeeLookupIPBtnA);
		WaitElement(txt_EmployeeLookupIPATxtA);
		sendKeys(txt_EmployeeLookupIPATxtA, EmployeeLookupIPATxtA);
		pressEnter(txt_EmployeeLookupIPATxtA);
		WaitElement(sel_EmployeeLookupIPASelA.replace("EmpSel", EmployeeLookupIPATxtA));
		doubleClick(sel_EmployeeLookupIPASelA.replace("EmpSel", EmployeeLookupIPATxtA));
		
		WaitElement(txt_CustomerACCTxtA);
		String EmployeeTxt= driver.findElement(By.xpath(txt_CustomerACCTxtA)).getAttribute("value");
		
		if(EmployeeTxt.contentEquals(EmployeeLookupIPATxtA)){
			writeTestResults("Verify that user can select a Employee using the Employee Lookup"
					,"user can select a Employee using the Employee Lookup"
					, "user can select a Employee using the Employee Lookup Successfully", "pass");
		}else {
			writeTestResults("Verify that user can select a Employee using the Employee Lookup"
					,"user can select a Employee using the Employee Lookup"
					, "user can select a Employee using the Employee Lookup Fail", "fail");
		}
		
		//===================================================FIN_11_34
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		String PaymentMethodSelValueA = new Select(driver.findElement(By.xpath(sel_PaymentMethodSelA))).getFirstSelectedOption().getText();
		if(PaymentMethodSelValueA.contentEquals(PaymentMethodSelA)){
			writeTestResults("Verify that user can select a Payment method using the drop down"
					,"user can select a Payment method using the drop down"
					, "user can select a Payment method using the drop down Successfully", "pass");
		}else {
			writeTestResults("Verify that user can select a Payment method using the drop down"
					,"user can select a Payment method using the drop down"
					, "user can select a Payment method using the drop down Fail", "fail");
		}
		
		//===================================================FIN_11_35
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		String PaidCurrencySelAValueA = new Select(driver.findElement(By.xpath(sel_PaidCurrencySelA))).getFirstSelectedOption().getText();
		if(PaidCurrencySelAValueA.contentEquals(PaidCurrencySelA)){
			writeTestResults("Verify that user can select a Paid currency using the drop down"
					,"user can select a Paid currency using the drop down"
					, "user can select a Paid currency using the drop down Successfully", "pass");
		}else {
			writeTestResults("Verify that user can select a Paid currency using the drop down"
					,"user can select a Paid currency using the drop down"
					, "user can select a Paid currency using the drop down Fail", "fail");
		}
	}
	
	//FIN_11_36
	public void VerifyThatUserCanAbleToCreateEmployeeReceiptVoucherUsingCashPayMethod() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);
		WaitElement(btn_JourneyDownArrowBtnA);
		click(btn_JourneyDownArrowBtnA);

		WaitElement(btn_EmployeeReceiptAdviceJourneyBtnA);
		click(btn_EmployeeReceiptAdviceJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Employee Receipt")){
			writeTestResults("Navigation to Inbound Payment Advice - New Employee Receipt page"
					,"User can Navigate Inbound Payment Advice - New Employee Receipt page"
					, "User can Navigate Inbound Payment Advice - New Employee Receipt page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Employee Receipt page"
					,"User can Navigate Inbound Payment Advice - New Employee Receipt page"
					, "User can Navigate Inbound Payment Advice - New Employee Receipt page Fail", "fail");
		}
		
		WaitElement(btn_EmployeeLookupIPABtnA);
		click(btn_EmployeeLookupIPABtnA);
		WaitElement(txt_EmployeeLookupIPATxtA);
		sendKeys(txt_EmployeeLookupIPATxtA, EmployeeLookupIPATxtA);
		pressEnter(txt_EmployeeLookupIPATxtA);
		WaitElement(sel_EmployeeLookupIPASelA.replace("EmpSel", EmployeeLookupIPATxtA));
		doubleClick(sel_EmployeeLookupIPASelA.replace("EmpSel", EmployeeLookupIPATxtA));
		
		WaitElement(txt_DescriptionIPATxtA);
		sendKeys(txt_DescriptionIPATxtA, "@Test1996");
		
		WaitElement(btn_GLAccIPABtnA);
		click(btn_GLAccIPABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Employee Receipt Advice Form"
					,"user can use Draft the Employee Receipt Advice Form"
					, "user can use Draft the Employee Receipt Advice Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Employee Receipt Advice Form"
					,"user can use Draft the Employee Receipt Advice Form"
					, "user can use Draft the Employee Receipt Advice Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Employee Receipt","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Employee Receipt Advice"
					,"user can release the Employee Receipt Advice"
					, "user can release the Employee Receipt Advice Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Employee Receipt Advice"
					,"user can release the Employee Receipt Advice"
					, "user can release the Employee Receipt Advice Fail", "fail");
		}
		
		
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Employee Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		//===================================================FIN_11_36
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Employee Receipt Form"
					,"user can use Draft the Inbound Payment - New Employee Receipt Form"
					, "user can use Draft the Inbound Payment - New Employee Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Employee Receipt Form"
					,"user can use Draft the Inbound Payment - New Employee Receipt Form"
					, "user can use Draft the Inbound Payment - New Employee Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can able to create Employee receipt voucher using 'Cash' pay method"
					,"user can able to create Employee receipt voucher using 'Cash' pay method"
					, "user can able to create Employee receipt voucher using 'Cash' pay method Successfully", "pass");
		}else {
			writeTestResults("Verify that user can able to create Employee receipt voucher using 'Cash' pay method"
					,"user can able to create Employee receipt voucher using 'Cash' pay method"
					, "user can able to create Employee receipt voucher using 'Cash' pay method Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_JournalActionBtnA);
		click(btn_JournalActionBtnA);
		
//		System.out.println(getText(txt_JournalCreditIPTxtA));
//		System.out.println(getText(txt_JournalDeditIPTxtA));
		
		if(getText(txt_JournalCreditIPTxtA).equals(getText(txt_JournalDebitIPTxtA))) {
			writeTestResults("Verify that user can verify the journal entry of the created Employee receipt voucher"
					,"user can verify the journal entry of the created Employee receipt voucher"
					, "user can verify the journal entry of the created Employee receipt voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can verify the journal entry of the created Employee receipt voucher"
					,"user can verify the journal entry of the created Employee receipt voucher"
					, "user can verify the journal entry of the created Employee receipt voucher Fail", "fail");
		}
	}
	
	//FIN_11_37
	public void VerifyThatUserCanAbleToCreateEmployeeReceiptVoucherUsingChequePayMethod() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);
		WaitElement(btn_JourneyDownArrowBtnA);
		click(btn_JourneyDownArrowBtnA);

		WaitElement(btn_EmployeeReceiptAdviceJourneyBtnA);
		click(btn_EmployeeReceiptAdviceJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Employee Receipt")){
			writeTestResults("Navigation to Inbound Payment Advice - New Employee Receipt page"
					,"User can Navigate Inbound Payment Advice - New Employee Receipt page"
					, "User can Navigate Inbound Payment Advice - New Employee Receipt page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Employee Receipt page"
					,"User can Navigate Inbound Payment Advice - New Employee Receipt page"
					, "User can Navigate Inbound Payment Advice - New Employee Receipt page Fail", "fail");
		}
		
		WaitElement(btn_EmployeeLookupIPABtnA);
		click(btn_EmployeeLookupIPABtnA);
		WaitElement(txt_EmployeeLookupIPATxtA);
		sendKeys(txt_EmployeeLookupIPATxtA, EmployeeLookupIPATxtA);
		pressEnter(txt_EmployeeLookupIPATxtA);
		WaitElement(sel_EmployeeLookupIPASelA.replace("EmpSel", EmployeeLookupIPATxtA));
		doubleClick(sel_EmployeeLookupIPASelA.replace("EmpSel", EmployeeLookupIPATxtA));
		
		WaitElement(txt_DescriptionIPATxtA);
		sendKeys(txt_DescriptionIPATxtA, "@Test1996");
		
		WaitElement(btn_GLAccIPABtnA);
		click(btn_GLAccIPABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Employee Receipt Advice Form"
					,"user can use Draft the Employee Receipt Advice Form"
					, "user can use Draft the Employee Receipt Advice Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Employee Receipt Advice Form"
					,"user can use Draft the Employee Receipt Advice Form"
					, "user can use Draft the Employee Receipt Advice Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Employee Receipt","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Employee Receipt Advice"
					,"user can release the Employee Receipt Advice"
					, "user can release the Employee Receipt Advice Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Employee Receipt Advice"
					,"user can release the Employee Receipt Advice"
					, "user can release the Employee Receipt Advice Fail", "fail");
		}
		
		
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Employee Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		//===================================================FIN_11_36
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSel2A);
		
		WaitElement(sel_BankNameIPSelA);
		selectText(sel_BankNameIPSelA, BankNameIPSelA);
		
		WaitElement(txt_ChequeNoIPTxtA);
		LocalTime Obj1 = LocalTime.now();
		sendKeys(txt_ChequeNoIPTxtA, "CHK"+Obj1);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Employee Receipt Form"
					,"user can use Draft the Inbound Payment - New Employee Receipt Form"
					, "user can use Draft the Inbound Payment - New Employee Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Employee Receipt Form"
					,"user can use Draft the Inbound Payment - New Employee Receipt Form"
					, "user can use Draft the Inbound Payment - New Employee Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can able to create Employee receipt voucher using 'Cheque' pay method"
					,"user can able to create Employee receipt voucher using 'Cheque' pay method"
					, "user can able to create Employee receipt voucher using 'Cheque' pay method Successfully", "pass");
		}else {
			writeTestResults("Verify that user can able to create Employee receipt voucher using 'Cheque' pay method"
					,"user can able to create Employee receipt voucher using 'Cheque' pay method"
					, "user can able to create Employee receipt voucher using 'Cheque' pay method Fail", "fail");
		}
	}
	
	//FIN_11_38and11_39
	public void VerifyThatUserCanAbleToAccessAboveCreatedChequeAfterTheMentionedChequeDateInBankDeposit3() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);
		WaitElement(btn_JourneyDownArrowBtnA);
		click(btn_JourneyDownArrowBtnA);

		WaitElement(btn_EmployeeReceiptAdviceJourneyBtnA);
		click(btn_EmployeeReceiptAdviceJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Employee Receipt")){
			writeTestResults("Navigation to Inbound Payment Advice - New Employee Receipt page"
					,"User can Navigate Inbound Payment Advice - New Employee Receipt page"
					, "User can Navigate Inbound Payment Advice - New Employee Receipt page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Employee Receipt page"
					,"User can Navigate Inbound Payment Advice - New Employee Receipt page"
					, "User can Navigate Inbound Payment Advice - New Employee Receipt page Fail", "fail");
		}
		
		WaitElement(btn_EmployeeLookupIPABtnA);
		click(btn_EmployeeLookupIPABtnA);
		WaitElement(txt_EmployeeLookupIPATxtA);
		sendKeys(txt_EmployeeLookupIPATxtA, EmployeeLookupIPATxtA);
		pressEnter(txt_EmployeeLookupIPATxtA);
		WaitElement(sel_EmployeeLookupIPASelA.replace("EmpSel", EmployeeLookupIPATxtA));
		doubleClick(sel_EmployeeLookupIPASelA.replace("EmpSel", EmployeeLookupIPATxtA));
		
		WaitElement(txt_DescriptionIPATxtA);
		sendKeys(txt_DescriptionIPATxtA, "@Test1996");
		
		WaitElement(btn_GLAccIPABtnA);
		click(btn_GLAccIPABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Employee Receipt Advice Form"
					,"user can use Draft the Employee Receipt Advice Form"
					, "user can use Draft the Employee Receipt Advice Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Employee Receipt Advice Form"
					,"user can use Draft the Employee Receipt Advice Form"
					, "user can use Draft the Employee Receipt Advice Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Employee Receipt","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Employee Receipt Advice"
					,"user can release the Employee Receipt Advice"
					, "user can release the Employee Receipt Advice Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Employee Receipt Advice"
					,"user can release the Employee Receipt Advice"
					, "user can release the Employee Receipt Advice Fail", "fail");
		}
		
		
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Employee Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSel2A);
		
		WaitElement(sel_BankNameIPSelA);
		selectText(sel_BankNameIPSelA, BankNameIPSelA);
		
		WaitElement(txt_ChequeNoIPTxtA);
		LocalTime Obj1 = LocalTime.now();
		sendKeys(txt_ChequeNoIPTxtA, "CHK"+Obj1);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Employee Receipt Form"
					,"user can use Draft the Inbound Payment - New Employee Receipt Form"
					, "user can use Draft the Inbound Payment - New Employee Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Employee Receipt Form"
					,"user can use Draft the Inbound Payment - New Employee Receipt Form"
					, "user can use Draft the Inbound Payment - New Employee Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPNo = trackCode.replace(" Employee Receipt", "");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can able to create Employee receipt voucher using 'Cheque' pay method"
					,"user can able to create Employee receipt voucher using 'Cheque' pay method"
					, "user can able to create Employee receipt voucher using 'Cheque' pay method Successfully", "pass");
		}else {
			writeTestResults("Verify that user can able to create Employee receipt voucher using 'Cheque' pay method"
					,"user can able to create Employee receipt voucher using 'Cheque' pay method"
					, "user can able to create Employee receipt voucher using 'Cheque' pay method Fail", "fail");
		}
		
	//===================================================FIN_11_38
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_BankDepositBtnA);
		click(btn_BankDepositBtnA);
		WaitElement(btn_NewBankDepositBtnA);
		click(btn_NewBankDepositBtnA);
		
		WaitElement(sel_PayMethodBDSelA);
		selectText(sel_PayMethodBDSelA, PaymentMethodSel2A);
		
		WaitElement(sel_BankBDSelA);
		selectText(sel_BankBDSelA, BankBDSelA);
		
		WaitElement(sel_BankAccountNoBDSelA);
		selectText(sel_BankAccountNoBDSelA, BankAccountNoBDSelA);
		
		WaitElement(btn_SortByBDBtnA);
		selectText(btn_SortByBDBtnA, "Cheque No");
		
		WaitElement(txt_CheqNoBDTxtA);
		sendKeys(txt_CheqNoBDTxtA, "CHK"+Obj1);
		
		WaitClick(btn_ViewBDBtnA);
		click(btn_ViewBDBtnA);
		
		ScrollDownAruna();
		
		WaitElement(btn_DolSelBDBtnA.replace("IPNo", IPNo));
		if(isDisplayed(btn_DolSelBDBtnA.replace("IPNo", IPNo))) {
			writeTestResults("Verify that user can able to access above created cheque after the mentioned cheque date in bank deposit"
					,"user can able to access above created cheque after the mentioned cheque date in bank deposit"
					, "user can able to access above created cheque after the mentioned cheque date in bank deposit Successfully", "pass");
			Thread.sleep(4000);
			click(btn_DolSelBDBtnA.replace("IPNo", IPNo));
		}else {
			writeTestResults("Verify that user can able to access above created cheque after the mentioned cheque date in bank deposit"
					,"user can able to access above created cheque after the mentioned cheque date in bank deposit"
					, "user can able to access above created cheque after the mentioned cheque date in bank deposit Fail", "fail");
		}
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Bank Deposit Form"
					,"user can use Draft the Bank Deposit Form"
					, "user can use Draft the Bank Deposit Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Bank Deposit Form"
					,"user can use Draft the Bank Deposit Form"
					, "user can use Draft the Bank Deposit Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can use Release the Bank Deposit Form"
					,"user can use Release the Bank Deposit Form"
					, "user can use Release the Bank Deposit Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Release the Bank Deposit Form"
					,"user can use Release the Bank Deposit Form"
					, "user can use Release the Bank Deposit Form Fail", "fail");
		}
		
		//===================================================FIN_11_39
		
		for (int i = 1; i <=3; i++) {
			driver.navigate().back();
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ChequeReturnBtnIPA);
		click(btn_ChequeReturnBtnIPA);
		
		WaitElement(sel_ReturnResonSelIPA);
		selectText(sel_ReturnResonSelIPA, "Funds insufficient");
		
		JavascriptExecutor j7 = (JavascriptExecutor)driver;
		j7.executeScript("$('#txtReturnDate').datepicker('setDate', new Date())");
		
		WaitElement(txt_ReturnRemarkTxtIPA);
		sendKeys(txt_ReturnRemarkTxtIPA, "Return CHK"+Obj1);
		
		WaitClick(btn_ReturnBtnIPABtnIPA);
		click(btn_ReturnBtnIPABtnIPA);
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		
		if(!isDisplayed(btn_ChequeReturnBtnIPA)) {
			writeTestResults("Verify that user can do Cheque return transaction"
					,"user can do Cheque return transaction"
					, "user can do Cheque return transaction Successfully", "pass");
		}else {
			writeTestResults("Verify that user can do Cheque return transaction"
					,"user can do Cheque return transaction"
					, "user can do Cheque return transaction Fail", "fail");
		}
	}
	
	//FIN_11_40
	public void VerifyThatUserCanAbleToCreateEmployeeReceiptVoucherUsingEFTPayMethod() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);
		WaitElement(btn_JourneyDownArrowBtnA);
		click(btn_JourneyDownArrowBtnA);

		WaitElement(btn_EmployeeReceiptAdviceJourneyBtnA);
		click(btn_EmployeeReceiptAdviceJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Employee Receipt")){
			writeTestResults("Navigation to Inbound Payment Advice - New Employee Receipt page"
					,"User can Navigate Inbound Payment Advice - New Employee Receipt page"
					, "User can Navigate Inbound Payment Advice - New Employee Receipt page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Employee Receipt page"
					,"User can Navigate Inbound Payment Advice - New Employee Receipt page"
					, "User can Navigate Inbound Payment Advice - New Employee Receipt page Fail", "fail");
		}
		
		WaitElement(btn_EmployeeLookupIPABtnA);
		click(btn_EmployeeLookupIPABtnA);
		WaitElement(txt_EmployeeLookupIPATxtA);
		sendKeys(txt_EmployeeLookupIPATxtA, EmployeeLookupIPATxtA);
		pressEnter(txt_EmployeeLookupIPATxtA);
		WaitElement(sel_EmployeeLookupIPASelA.replace("EmpSel", EmployeeLookupIPATxtA));
		doubleClick(sel_EmployeeLookupIPASelA.replace("EmpSel", EmployeeLookupIPATxtA));
		
		WaitElement(txt_DescriptionIPATxtA);
		sendKeys(txt_DescriptionIPATxtA, "@Test1996");
		
		WaitElement(btn_GLAccIPABtnA);
		click(btn_GLAccIPABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Employee Receipt Advice Form"
					,"user can use Draft the Employee Receipt Advice Form"
					, "user can use Draft the Employee Receipt Advice Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Employee Receipt Advice Form"
					,"user can use Draft the Employee Receipt Advice Form"
					, "user can use Draft the Employee Receipt Advice Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Employee Receipt","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Employee Receipt Advice"
					,"user can release the Employee Receipt Advice"
					, "user can release the Employee Receipt Advice Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Employee Receipt Advice"
					,"user can release the Employee Receipt Advice"
					, "user can release the Employee Receipt Advice Fail", "fail");
		}
		
		
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Employee Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		//===================================================FIN_11_40
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSel3A);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Employee Receipt Form"
					,"user can use Draft the Inbound Payment - New Employee Receipt Form"
					, "user can use Draft the Inbound Payment - New Employee Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Employee Receipt Form"
					,"user can use Draft the Inbound Payment - New Employee Receipt Form"
					, "user can use Draft the Inbound Payment - New Employee Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can able to create Employee receipt voucher using 'EFT' pay method"
					,"user can able to create Employee receipt voucher using 'EFT' pay method"
					, "user can able to create Employee receipt voucher using 'EFT' pay method Successfully", "pass");
		}else {
			writeTestResults("Verify that user can able to create Employee receipt voucher using 'EFT' pay method"
					,"user can able to create Employee receipt voucher using 'EFT' pay method"
					, "user can able to create Employee receipt voucher using 'EFT' pay method Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_JournalActionBtnA);
		click(btn_JournalActionBtnA);
		
//		System.out.println(getText(txt_JournalCreditIPTxtA));
//		System.out.println(getText(txt_JournalDeditIPTxtA));
		
		if(getText(txt_JournalCreditIPTxtA).equals(getText(txt_JournalDebitIPTxtA))) {
			writeTestResults("Verify that user can verify the journal entry of the created Employee receipt voucher"
					,"user can verify the journal entry of the created Employee receipt voucher"
					, "user can verify the journal entry of the created Employee receipt voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can verify the journal entry of the created Employee receipt voucher"
					,"user can verify the journal entry of the created Employee receipt voucher"
					, "user can verify the journal entry of the created Employee receipt voucher Fail", "fail");
		}
	}
	
	//FIN_11_41
	public void VerifyThatUserCanAbleToCreateEmployeeReceiptVoucherUsingDirectDepositPayMethod() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);
		WaitElement(btn_JourneyDownArrowBtnA);
		click(btn_JourneyDownArrowBtnA);

		WaitElement(btn_EmployeeReceiptAdviceJourneyBtnA);
		click(btn_EmployeeReceiptAdviceJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Employee Receipt")){
			writeTestResults("Navigation to Inbound Payment Advice - New Employee Receipt page"
					,"User can Navigate Inbound Payment Advice - New Employee Receipt page"
					, "User can Navigate Inbound Payment Advice - New Employee Receipt page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Employee Receipt page"
					,"User can Navigate Inbound Payment Advice - New Employee Receipt page"
					, "User can Navigate Inbound Payment Advice - New Employee Receipt page Fail", "fail");
		}
		
		WaitElement(btn_EmployeeLookupIPABtnA);
		click(btn_EmployeeLookupIPABtnA);
		WaitElement(txt_EmployeeLookupIPATxtA);
		sendKeys(txt_EmployeeLookupIPATxtA, EmployeeLookupIPATxtA);
		pressEnter(txt_EmployeeLookupIPATxtA);
		WaitElement(sel_EmployeeLookupIPASelA.replace("EmpSel", EmployeeLookupIPATxtA));
		doubleClick(sel_EmployeeLookupIPASelA.replace("EmpSel", EmployeeLookupIPATxtA));
		
		WaitElement(txt_DescriptionIPATxtA);
		sendKeys(txt_DescriptionIPATxtA, "@Test1996");
		
		WaitElement(btn_GLAccIPABtnA);
		click(btn_GLAccIPABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Employee Receipt Advice Form"
					,"user can use Draft the Employee Receipt Advice Form"
					, "user can use Draft the Employee Receipt Advice Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Employee Receipt Advice Form"
					,"user can use Draft the Employee Receipt Advice Form"
					, "user can use Draft the Employee Receipt Advice Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Employee Receipt","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Employee Receipt Advice"
					,"user can release the Employee Receipt Advice"
					, "user can release the Employee Receipt Advice Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Employee Receipt Advice"
					,"user can release the Employee Receipt Advice"
					, "user can release the Employee Receipt Advice Fail", "fail");
		}
		
		
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Employee Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		//===================================================FIN_11_41
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSel4A);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Employee Receipt Form"
					,"user can use Draft the Inbound Payment - New Employee Receipt Form"
					, "user can use Draft the Inbound Payment - New Employee Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Employee Receipt Form"
					,"user can use Draft the Inbound Payment - New Employee Receipt Form"
					, "user can use Draft the Inbound Payment - New Employee Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can able to create Employee receipt voucher using 'Direct Deposit' pay method"
					,"user can able to create Employee receipt voucher using 'Direct Deposit' pay method"
					, "user can able to create Employee receipt voucher using 'Direct Deposit' pay method Successfully", "pass");
		}else {
			writeTestResults("Verify that user can able to create Employee receipt voucher using 'Direct Deposit' pay method"
					,"user can able to create Employee receipt voucher using 'Direct Deposit' pay method"
					, "user can able to create Employee receipt voucher using 'Direct Deposit' pay method Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_JournalActionBtnA);
		click(btn_JournalActionBtnA);
		
//		System.out.println(getText(txt_JournalCreditIPTxtA));
//		System.out.println(getText(txt_JournalDeditIPTxtA));
		
		if(getText(txt_JournalCreditIPTxtA).equals(getText(txt_JournalDebitIPTxtA))) {
			writeTestResults("Verify that user can verify the journal entry of the created Employee receipt voucher"
					,"user can verify the journal entry of the created Employee receipt voucher"
					, "user can verify the journal entry of the created Employee receipt voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can verify the journal entry of the created Employee receipt voucher"
					,"user can verify the journal entry of the created Employee receipt voucher"
					, "user can verify the journal entry of the created Employee receipt voucher Fail", "fail");
		}
	}
	
	//FIN_11_42
	public void VerifyThatUserCanAbleToCreateReceiptVoucherUsingCashPayMethod() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);

		WaitElement(btn_ReceiptVoucherJourneyIPABtnA);
		click(btn_ReceiptVoucherJourneyIPABtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Receipt Voucher")){
			writeTestResults("Navigation to Inbound Payment Advice - New Receipt Voucher page"
					,"User can Navigate Inbound Payment Advice - New Receipt Voucher page"
					, "User can Navigate Inbound Payment Advice - New Receipt Voucher page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Receipt Voucher page"
					,"User can Navigate Inbound Payment Advice - New Receipt Voucher page"
					, "User can Navigate Inbound Payment Advice - New Receipt Voucher page Fail", "fail");
		}
		
		WaitElement(txt_DescriptionIPATxtA);
		sendKeys(txt_DescriptionIPATxtA, "@Test1996");
		
		WaitElement(btn_GLAccIPABtnA);
		click(btn_GLAccIPABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Receipt Voucher Form"
					,"user can use Draft the Receipt Voucher Form"
					, "user can use Draft the Receipt Voucher Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Receipt Voucher Form"
					,"user can use Draft the Receipt Voucher Form"
					, "user can use Draft the Receipt Voucher Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Receipt Voucher","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Receipt Voucher"
					,"user can release the Receipt Voucher"
					, "user can release the Receipt Voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Receipt Voucher"
					,"user can release the Receipt Voucher"
					, "user can release the Receipt Voucher Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New GL Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New GL Receipt Form"
					,"user can use Draft the Inbound Payment - New GL Receipt Form"
					, "user can use Draft the Inbound Payment - New GL Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New GL Receipt Form"
					,"user can use Draft the Inbound Payment - New GL Receipt Form"
					, "user can use Draft the Inbound Payment - New GL Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can able to create receipt voucher using 'Cash' pay method"
					,"user can able to create receipt voucher using 'Cash' pay method"
					, "user can able to create receipt voucher using 'Cash' pay method Successfully", "pass");
		}else {
			writeTestResults("Verify that user can able to create receipt voucher using 'Cash' pay method"
					,"user can able to create receipt voucher using 'Cash' pay method"
					, "user can able to create receipt voucher using 'Cash' pay method Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_JournalActionBtnA);
		click(btn_JournalActionBtnA);
		
//		System.out.println(getText(txt_JournalCreditIPTxtA));
//		System.out.println(getText(txt_JournalDeditIPTxtA));
		
		if(getText(txt_JournalCreditIPTxtA).equals(getText(txt_JournalDebitIPTxtA))) {
			writeTestResults("Verify that user can verify the journal entry of the created Receipt voucher"
					,"user can verify the journal entry of the created Receipt voucher"
					, "user can verify the journal entry of the created Receipt voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can verify the journal entry of the created Receipt voucher"
					,"user can verify the journal entry of the created Employee Receipt voucher"
					, "user can verify the journal entry of the created Receipt voucher Fail", "fail");
		}
	}
	
	//FIN_11_43
	public void VerifyThatUserCanAbleToCreateReceiptVoucherUsingChequePayMethod() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);

		WaitElement(btn_ReceiptVoucherJourneyIPABtnA);
		click(btn_ReceiptVoucherJourneyIPABtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Receipt Voucher")){
			writeTestResults("Navigation to Inbound Payment Advice - New Receipt Voucher page"
					,"User can Navigate Inbound Payment Advice - New Receipt Voucher page"
					, "User can Navigate Inbound Payment Advice - New Receipt Voucher page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Receipt Voucher page"
					,"User can Navigate Inbound Payment Advice - New Receipt Voucher page"
					, "User can Navigate Inbound Payment Advice - New Receipt Voucher page Fail", "fail");
		}
		
		WaitElement(txt_DescriptionIPATxtA);
		sendKeys(txt_DescriptionIPATxtA, "@Test1996");
		
		WaitElement(btn_GLAccIPABtnA);
		click(btn_GLAccIPABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Receipt Voucher Form"
					,"user can use Draft the Receipt Voucher Form"
					, "user can use Draft the Receipt Voucher Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Receipt Voucher Form"
					,"user can use Draft the Receipt Voucher Form"
					, "user can use Draft the Receipt Voucher Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Receipt Voucher","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Receipt Voucher"
					,"user can release the Receipt Voucher"
					, "user can release the Receipt Voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Receipt Voucher"
					,"user can release the Receipt Voucher"
					, "user can release the Receipt Voucher Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New GL Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSel2A);
		
		WaitElement(sel_BankNameIPSelA);
		selectText(sel_BankNameIPSelA, BankNameIPSelA);
		
		WaitElement(txt_ChequeNoIPTxtA);
		LocalTime Obj1 = LocalTime.now();
		sendKeys(txt_ChequeNoIPTxtA, "CHK"+Obj1);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New GL Receipt Form"
					,"user can use Draft the Inbound Payment - New GL Receipt Form"
					, "user can use Draft the Inbound Payment - New GL Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New GL Receipt Form"
					,"user can use Draft the Inbound Payment - New GL Receipt Form"
					, "user can use Draft the Inbound Payment - New GL Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can able to create receipt voucher using 'Cheque' pay method"
					,"user can able to create receipt voucher using 'Cheque' pay method"
					, "user can able to create receipt voucher using 'Cheque' pay method Successfully", "pass");
		}else {
			writeTestResults("Verify that user can able to create receipt voucher using 'Cheque' pay method"
					,"user can able to create receipt voucher using 'Cheque' pay method"
					, "user can able to create receipt voucher using 'Cheque' pay method Fail", "fail");
		}
	}
	
	//FIN_11_44and11_45
	public void VerifyThatUserCanAbleToAccessAboveCreatedChequeAfterTheMentionedChequeDateInBankDeposit4() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);

		WaitElement(btn_ReceiptVoucherJourneyIPABtnA);
		click(btn_ReceiptVoucherJourneyIPABtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Receipt Voucher")){
			writeTestResults("Navigation to Inbound Payment Advice - New Receipt Voucher page"
					,"User can Navigate Inbound Payment Advice - New Receipt Voucher page"
					, "User can Navigate Inbound Payment Advice - New Receipt Voucher page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Receipt Voucher page"
					,"User can Navigate Inbound Payment Advice - New Receipt Voucher page"
					, "User can Navigate Inbound Payment Advice - New Receipt Voucher page Fail", "fail");
		}
		
		WaitElement(txt_DescriptionIPATxtA);
		sendKeys(txt_DescriptionIPATxtA, "@Test1996");
		
		WaitElement(btn_GLAccIPABtnA);
		click(btn_GLAccIPABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Receipt Voucher Form"
					,"user can use Draft the Receipt Voucher Form"
					, "user can use Draft the Receipt Voucher Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Receipt Voucher Form"
					,"user can use Draft the Receipt Voucher Form"
					, "user can use Draft the Receipt Voucher Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Receipt Voucher","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Receipt Voucher"
					,"user can release the Receipt Voucher"
					, "user can release the Receipt Voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Receipt Voucher"
					,"user can release the Receipt Voucher"
					, "user can release the Receipt Voucher Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New GL Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSel2A);
		
		WaitElement(sel_BankNameIPSelA);
		selectText(sel_BankNameIPSelA, BankNameIPSelA);
		
		WaitElement(txt_ChequeNoIPTxtA);
		LocalTime Obj1 = LocalTime.now();
		sendKeys(txt_ChequeNoIPTxtA, "CHK"+Obj1);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New GL Receipt Form"
					,"user can use Draft the Inbound Payment - New GL Receipt Form"
					, "user can use Draft the Inbound Payment - New GL Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New GL Receipt Form"
					,"user can use Draft the Inbound Payment - New GL Receipt Form"
					, "user can use Draft the Inbound Payment - New GL Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPNo = trackCode.replace(" GL Receipt", "");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can able to create receipt voucher using 'Cheque' pay method"
					,"user can able to create receipt voucher using 'Cheque' pay method"
					, "user can able to create receipt voucher using 'Cheque' pay method Successfully", "pass");
		}else {
			writeTestResults("Verify that user can able to create receipt voucher using 'Cheque' pay method"
					,"user can able to create receipt voucher using 'Cheque' pay method"
					, "user can able to create receipt voucher using 'Cheque' pay method Fail", "fail");
		}
		
	//===================================================FIN_11_44
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_BankDepositBtnA);
		click(btn_BankDepositBtnA);
		WaitElement(btn_NewBankDepositBtnA);
		click(btn_NewBankDepositBtnA);
		
		WaitElement(sel_PayMethodBDSelA);
		selectText(sel_PayMethodBDSelA, PaymentMethodSel2A);
		
		WaitElement(sel_BankBDSelA);
		selectText(sel_BankBDSelA, BankBDSelA);
		
		WaitElement(sel_BankAccountNoBDSelA);
		selectText(sel_BankAccountNoBDSelA, BankAccountNoBDSelA);
		
		WaitElement(btn_SortByBDBtnA);
		selectText(btn_SortByBDBtnA, "Cheque No");
		
		WaitElement(txt_CheqNoBDTxtA);
		sendKeys(txt_CheqNoBDTxtA, "CHK"+Obj1);
		
		WaitClick(btn_ViewBDBtnA);
		click(btn_ViewBDBtnA);
		
		ScrollDownAruna();
		
		WaitElement(btn_DolSelBDBtnA.replace("IPNo", IPNo));
		if(isDisplayed(btn_DolSelBDBtnA.replace("IPNo", IPNo))) {
			writeTestResults("Verify that user can able to access above created cheque after the mentioned cheque date in bank deposit"
					,"user can able to access above created cheque after the mentioned cheque date in bank deposit"
					, "user can able to access above created cheque after the mentioned cheque date in bank deposit Successfully", "pass");
			Thread.sleep(4000);
			click(btn_DolSelBDBtnA.replace("IPNo", IPNo));
		}else {
			writeTestResults("Verify that user can able to access above created cheque after the mentioned cheque date in bank deposit"
					,"user can able to access above created cheque after the mentioned cheque date in bank deposit"
					, "user can able to access above created cheque after the mentioned cheque date in bank deposit Fail", "fail");
		}
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Bank Deposit Form"
					,"user can use Draft the Bank Deposit Form"
					, "user can use Draft the Bank Deposit Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Bank Deposit Form"
					,"user can use Draft the Bank Deposit Form"
					, "user can use Draft the Bank Deposit Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can use Release the Bank Deposit Form"
					,"user can use Release the Bank Deposit Form"
					, "user can use Release the Bank Deposit Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Release the Bank Deposit Form"
					,"user can use Release the Bank Deposit Form"
					, "user can use Release the Bank Deposit Form Fail", "fail");
		}
		
		//===================================================FIN_11_45
		
		for (int i = 1; i <=3; i++) {
			driver.navigate().back();
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ChequeReturnBtnIPA);
		click(btn_ChequeReturnBtnIPA);
		
		WaitElement(sel_ReturnResonSelIPA);
		selectText(sel_ReturnResonSelIPA, "Funds insufficient");
		
		JavascriptExecutor j7 = (JavascriptExecutor)driver;
		j7.executeScript("$('#txtReturnDate').datepicker('setDate', new Date())");
		
		WaitElement(txt_ReturnRemarkTxtIPA);
		sendKeys(txt_ReturnRemarkTxtIPA, "Return CHK"+Obj1);
		
		WaitClick(btn_ReturnBtnIPABtnIPA);
		click(btn_ReturnBtnIPABtnIPA);
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		
		if(!isDisplayed(btn_ChequeReturnBtnIPA)) {
			writeTestResults("Verify that user can do Cheque return transaction"
					,"user can do Cheque return transaction"
					, "user can do Cheque return transaction Successfully", "pass");
		}else {
			writeTestResults("Verify that user can do Cheque return transaction"
					,"user can do Cheque return transaction"
					, "user can do Cheque return transaction Fail", "fail");
		}
	}
	
	//FIN_11_46
	public void VerifyThatUserCanAbleToCreateReceiptVoucherUsingEFTPayMethod() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);

		WaitElement(btn_ReceiptVoucherJourneyIPABtnA);
		click(btn_ReceiptVoucherJourneyIPABtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Receipt Voucher")){
			writeTestResults("Navigation to Inbound Payment Advice - New Receipt Voucher page"
					,"User can Navigate Inbound Payment Advice - New Receipt Voucher page"
					, "User can Navigate Inbound Payment Advice - New Receipt Voucher page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Receipt Voucher page"
					,"User can Navigate Inbound Payment Advice - New Receipt Voucher page"
					, "User can Navigate Inbound Payment Advice - New Receipt Voucher page Fail", "fail");
		}
		
		WaitElement(txt_DescriptionIPATxtA);
		sendKeys(txt_DescriptionIPATxtA, "@Test1996");
		
		WaitElement(btn_GLAccIPABtnA);
		click(btn_GLAccIPABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Receipt Voucher Form"
					,"user can use Draft the Receipt Voucher Form"
					, "user can use Draft the Receipt Voucher Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Receipt Voucher Form"
					,"user can use Draft the Receipt Voucher Form"
					, "user can use Draft the Receipt Voucher Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Receipt Voucher","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Receipt Voucher"
					,"user can release the Receipt Voucher"
					, "user can release the Receipt Voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Receipt Voucher"
					,"user can release the Receipt Voucher"
					, "user can release the Receipt Voucher Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New GL Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSel3A);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New GL Receipt Form"
					,"user can use Draft the Inbound Payment - New GL Receipt Form"
					, "user can use Draft the Inbound Payment - New GL Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New GL Receipt Form"
					,"user can use Draft the Inbound Payment - New GL Receipt Form"
					, "user can use Draft the Inbound Payment - New GL Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can able to create receipt voucher using 'EFT' pay method"
					,"user can able to create receipt voucher using 'EFT' pay method"
					, "user can able to create receipt voucher using 'EFT' pay method Successfully", "pass");
		}else {
			writeTestResults("Verify that user can able to create receipt voucher using 'EFT' pay method"
					,"user can able to create receipt voucher using 'EFT' pay method"
					, "user can able to create receipt voucher using 'EFT' pay method Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_JournalActionBtnA);
		click(btn_JournalActionBtnA);
		
//		System.out.println(getText(txt_JournalCreditIPTxtA));
//		System.out.println(getText(txt_JournalDeditIPTxtA));
		
		if(getText(txt_JournalCreditIPTxtA).equals(getText(txt_JournalDebitIPTxtA))) {
			writeTestResults("Verify that user can verify the journal entry of the created Receipt voucher"
					,"user can verify the journal entry of the created Receipt voucher"
					, "user can verify the journal entry of the created Receipt voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can verify the journal entry of the created Receipt voucher"
					,"user can verify the journal entry of the created Employee Receipt voucher"
					, "user can verify the journal entry of the created Receipt voucher Fail", "fail");
		}
	}
	
	//FIN_11_47
	public void VerifyThatUserCanAbleToCreateReceiptVoucherUsingDirectDepositPayMethod() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);

		WaitElement(btn_ReceiptVoucherJourneyIPABtnA);
		click(btn_ReceiptVoucherJourneyIPABtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Receipt Voucher")){
			writeTestResults("Navigation to Inbound Payment Advice - New Receipt Voucher page"
					,"User can Navigate Inbound Payment Advice - New Receipt Voucher page"
					, "User can Navigate Inbound Payment Advice - New Receipt Voucher page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Receipt Voucher page"
					,"User can Navigate Inbound Payment Advice - New Receipt Voucher page"
					, "User can Navigate Inbound Payment Advice - New Receipt Voucher page Fail", "fail");
		}
		
		WaitElement(txt_DescriptionIPATxtA);
		sendKeys(txt_DescriptionIPATxtA, "@Test1996");
		
		WaitElement(btn_GLAccIPABtnA);
		click(btn_GLAccIPABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Receipt Voucher Form"
					,"user can use Draft the Receipt Voucher Form"
					, "user can use Draft the Receipt Voucher Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Receipt Voucher Form"
					,"user can use Draft the Receipt Voucher Form"
					, "user can use Draft the Receipt Voucher Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Receipt Voucher","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Receipt Voucher"
					,"user can release the Receipt Voucher"
					, "user can release the Receipt Voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Receipt Voucher"
					,"user can release the Receipt Voucher"
					, "user can release the Receipt Voucher Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New GL Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSel4A);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New GL Receipt Form"
					,"user can use Draft the Inbound Payment - New GL Receipt Form"
					, "user can use Draft the Inbound Payment - New GL Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New GL Receipt Form"
					,"user can use Draft the Inbound Payment - New GL Receipt Form"
					, "user can use Draft the Inbound Payment - New GL Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can able to create receipt voucher using 'Direct Deposit' pay method"
					,"user can able to create receipt voucher using 'Direct Deposit' pay method"
					, "user can able to create receipt voucher using 'Direct Deposit' pay method Successfully", "pass");
		}else {
			writeTestResults("Verify that user can able to create receipt voucher using 'Direct Deposit' pay method"
					,"user can able to create receipt voucher using 'Direct Deposit' pay method"
					, "user can able to create receipt voucher using 'Direct Deposit' pay method Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_JournalActionBtnA);
		click(btn_JournalActionBtnA);
		
//		System.out.println(getText(txt_JournalCreditIPTxtA));
//		System.out.println(getText(txt_JournalDeditIPTxtA));
		
		if(getText(txt_JournalCreditIPTxtA).equals(getText(txt_JournalDebitIPTxtA))) {
			writeTestResults("Verify that user can verify the journal entry of the created Receipt voucher"
					,"user can verify the journal entry of the created Receipt voucher"
					, "user can verify the journal entry of the created Receipt voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can verify the journal entry of the created Receipt voucher"
					,"user can verify the journal entry of the created Employee Receipt voucher"
					, "user can verify the journal entry of the created Receipt voucher Fail", "fail");
		}
	}
	
	//FIN_11_48
	public void VerifyTheInboundPaymentWhenPayingCurrencyAndFilterCurrencyAreBaseCurrency2() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);

		WaitElement(btn_ReceiptVoucherJourneyIPABtnA);
		click(btn_ReceiptVoucherJourneyIPABtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Receipt Voucher")){
			writeTestResults("Navigation to Inbound Payment Advice - New Receipt Voucher page"
					,"User can Navigate Inbound Payment Advice - New Receipt Voucher page"
					, "User can Navigate Inbound Payment Advice - New Receipt Voucher page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Receipt Voucher page"
					,"User can Navigate Inbound Payment Advice - New Receipt Voucher page"
					, "User can Navigate Inbound Payment Advice - New Receipt Voucher page Fail", "fail");
		}
		
		WaitElement(txt_DescriptionIPATxtA);
		sendKeys(txt_DescriptionIPATxtA, "@Test1996");
		
		WaitElement(btn_GLAccIPABtnA);
		click(btn_GLAccIPABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "100");
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Receipt Voucher Form"
					,"user can use Draft the Receipt Voucher Form"
					, "user can use Draft the Receipt Voucher Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Receipt Voucher Form"
					,"user can use Draft the Receipt Voucher Form"
					, "user can use Draft the Receipt Voucher Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Receipt Voucher","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Receipt Voucher"
					,"user can release the Receipt Voucher"
					, "user can release the Receipt Voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Receipt Voucher"
					,"user can release the Receipt Voucher"
					, "user can release the Receipt Voucher Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New GL Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		//===================================================FIN_11_48
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(txt_FilterCurrencyIPTxtA);
		selectText(txt_FilterCurrencyIPTxtA, PaidCurrencySelA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New GL Receipt Form"
					,"user can use Draft the Inbound Payment - New GL Receipt Form"
					, "user can use Draft the Inbound Payment - New GL Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New GL Receipt Form"
					,"user can use Draft the Inbound Payment - New GL Receipt Form"
					, "user can use Draft the Inbound Payment - New GL Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify the Inbound Payment when Paying currency and filter currency are base currency"
					,"Inbound Payment when Paying currency and filter currency are base currency"
					, "Inbound Payment when Paying currency and filter currency are base currency Successfully", "pass");
		}else {
			writeTestResults("Verify the Inbound Payment when Paying currency and filter currency are base currency"
					,"Inbound Payment when Paying currency and filter currency are base currency"
					, "Inbound Payment when Paying currency and filter currency are base currency Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_JournalActionBtnA);
		click(btn_JournalActionBtnA);
		
//		System.out.println(getText(txt_JournalCreditIPTxtA));
//		System.out.println(getText(txt_JournalDeditIPTxtA));
		
		if(getText(txt_JournalCreditIPTxtA).equals(getText(txt_JournalDebitIPTxtA))) {
			writeTestResults("Verify that user can verify the journal entry of the created Receipt voucher"
					,"user can verify the journal entry of the created Receipt voucher"
					, "user can verify the journal entry of the created Receipt voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can verify the journal entry of the created Receipt voucher"
					,"user can verify the journal entry of the created Employee Receipt voucher"
					, "user can verify the journal entry of the created Receipt voucher Fail", "fail");
		}
	}
	
	//FIN_11_49
	public void VerifyTheInboundPaymentWhenPayingCurrencyAndFilterCurrencyAreForeignCurrency2() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);

		WaitElement(btn_ReceiptVoucherJourneyIPABtnA);
		click(btn_ReceiptVoucherJourneyIPABtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Receipt Voucher")){
			writeTestResults("Navigation to Inbound Payment Advice - New Receipt Voucher page"
					,"User can Navigate Inbound Payment Advice - New Receipt Voucher page"
					, "User can Navigate Inbound Payment Advice - New Receipt Voucher page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Receipt Voucher page"
					,"User can Navigate Inbound Payment Advice - New Receipt Voucher page"
					, "User can Navigate Inbound Payment Advice - New Receipt Voucher page Fail", "fail");
		}
		
		WaitElement(txt_DescriptionIPATxtA);
		sendKeys(txt_DescriptionIPATxtA, "@Test1996");
		
		WaitElement(btn_GLAccIPABtnA);
		click(btn_GLAccIPABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "100");
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencyForignA);
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Receipt Voucher Form"
					,"user can use Draft the Receipt Voucher Form"
					, "user can use Draft the Receipt Voucher Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Receipt Voucher Form"
					,"user can use Draft the Receipt Voucher Form"
					, "user can use Draft the Receipt Voucher Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Receipt Voucher","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Receipt Voucher"
					,"user can release the Receipt Voucher"
					, "user can release the Receipt Voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Receipt Voucher"
					,"user can release the Receipt Voucher"
					, "user can release the Receipt Voucher Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New GL Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		//===================================================FIN_11_49
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencyForignA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(txt_FilterCurrencyIPTxtA);
		selectText(txt_FilterCurrencyIPTxtA, PaidCurrencyForignA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New GL Receipt Form"
					,"user can use Draft the Inbound Payment - New GL Receipt Form"
					, "user can use Draft the Inbound Payment - New GL Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New GL Receipt Form"
					,"user can use Draft the Inbound Payment - New GL Receipt Form"
					, "user can use Draft the Inbound Payment - New GL Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify the Inbound Payment when Paying currency and filter currency are foreign currency"
					,"Inbound Payment when Paying currency and filter currency are foreign currency"
					, "Inbound Payment when Paying currency and filter currency are foreign currency Successfully", "pass");
		}else {
			writeTestResults("Verify the Inbound Payment when Paying currency and filter currency are foreign currency"
					,"Inbound Payment when Paying currency and filter currency are foreign currency"
					, "Inbound Payment when Paying currency and filter currency are foreign currency Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_JournalActionBtnA);
		click(btn_JournalActionBtnA);
		
//		System.out.println(getText(txt_JournalCreditIPTxtA));
//		System.out.println(getText(txt_JournalDeditIPTxtA));
		
		if(getText(txt_JournalCreditIPTxtA).equals(getText(txt_JournalDebitIPTxtA))) {
			writeTestResults("Verify that user can verify the journal entry of the created Receipt voucher"
					,"user can verify the journal entry of the created Receipt voucher"
					, "user can verify the journal entry of the created Receipt voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can verify the journal entry of the created Receipt voucher"
					,"user can verify the journal entry of the created Employee Receipt voucher"
					, "user can verify the journal entry of the created Receipt voucher Fail", "fail");
		}
	}
	
	//FIN_11_50
	public void VerifyTheInboundPaymentWhenPayingCurrencyIsBaseCurrencyAndFilterCurrencyIsForeignCurrency2() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);

		WaitElement(btn_ReceiptVoucherJourneyIPABtnA);
		click(btn_ReceiptVoucherJourneyIPABtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Receipt Voucher")){
			writeTestResults("Navigation to Inbound Payment Advice - New Receipt Voucher page"
					,"User can Navigate Inbound Payment Advice - New Receipt Voucher page"
					, "User can Navigate Inbound Payment Advice - New Receipt Voucher page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Receipt Voucher page"
					,"User can Navigate Inbound Payment Advice - New Receipt Voucher page"
					, "User can Navigate Inbound Payment Advice - New Receipt Voucher page Fail", "fail");
		}
		
		WaitElement(txt_DescriptionIPATxtA);
		sendKeys(txt_DescriptionIPATxtA, "@Test1996");
		
		WaitElement(btn_GLAccIPABtnA);
		click(btn_GLAccIPABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "100");
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencyForignA);
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Receipt Voucher Form"
					,"user can use Draft the Receipt Voucher Form"
					, "user can use Draft the Receipt Voucher Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Receipt Voucher Form"
					,"user can use Draft the Receipt Voucher Form"
					, "user can use Draft the Receipt Voucher Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Receipt Voucher","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Receipt Voucher"
					,"user can release the Receipt Voucher"
					, "user can release the Receipt Voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Receipt Voucher"
					,"user can release the Receipt Voucher"
					, "user can release the Receipt Voucher Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New GL Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		//===================================================FIN_11_50
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(txt_FilterCurrencyIPTxtA);
		selectText(txt_FilterCurrencyIPTxtA, PaidCurrencyForignA);
		
		WaitElement(btn_ViewAllPendingBtnIPA);
		click(btn_ViewAllPendingBtnIPA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New GL Receipt Form"
					,"user can use Draft the Inbound Payment - New GL Receipt Form"
					, "user can use Draft the Inbound Payment - New GL Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New GL Receipt Form"
					,"user can use Draft the Inbound Payment - New GL Receipt Form"
					, "user can use Draft the Inbound Payment - New GL Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify the Inbound Payment when Paying currency is Base currency and filter currency is foreign currency"
					,"Inbound Payment when Paying currency is Base currency and filter currency is foreign currency"
					, "Inbound Payment when Paying currency is Base currency and filter currency is foreign currency Successfully", "pass");
		}else {
			writeTestResults("Verify the Inbound Payment when Paying currency is Base currency and filter currency is foreign currency"
					,"Inbound Payment when Paying currency is Base currency and filter currency is foreign currency"
					, "Inbound Payment when Paying currency is Base currency and filter currency is foreign currency Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_JournalActionBtnA);
		click(btn_JournalActionBtnA);
		
//		System.out.println(getText(txt_JournalCreditIPTxtA));
//		System.out.println(getText(txt_JournalDeditIPTxtA));
		
		if(getText(txt_JournalCreditIPTxtA).equals(getText(txt_JournalDebitIPTxtA))) {
			writeTestResults("Verify that user can verify the journal entry of the created Receipt voucher"
					,"user can verify the journal entry of the created Receipt voucher"
					, "user can verify the journal entry of the created Receipt voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can verify the journal entry of the created Receipt voucher"
					,"user can verify the journal entry of the created Employee Receipt voucher"
					, "user can verify the journal entry of the created Receipt voucher Fail", "fail");
		}
	}
	
	//FIN_11_51to11_53
	public void VerifyThatUserCanUseDraftIP() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);

		WaitElement(btn_ReceiptVoucherJourneyIPABtnA);
		click(btn_ReceiptVoucherJourneyIPABtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Receipt Voucher")){
			writeTestResults("Navigation to Inbound Payment Advice - New Receipt Voucher page"
					,"User can Navigate Inbound Payment Advice - New Receipt Voucher page"
					, "User can Navigate Inbound Payment Advice - New Receipt Voucher page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Receipt Voucher page"
					,"User can Navigate Inbound Payment Advice - New Receipt Voucher page"
					, "User can Navigate Inbound Payment Advice - New Receipt Voucher page Fail", "fail");
		}
		
		WaitElement(txt_DescriptionIPATxtA);
		sendKeys(txt_DescriptionIPATxtA, "@Test1996");
		
		WaitElement(btn_GLAccIPABtnA);
		click(btn_GLAccIPABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "100");
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Receipt Voucher Form"
					,"user can use Draft the Receipt Voucher Form"
					, "user can use Draft the Receipt Voucher Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Receipt Voucher Form"
					,"user can use Draft the Receipt Voucher Form"
					, "user can use Draft the Receipt Voucher Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Receipt Voucher","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Receipt Voucher"
					,"user can release the Receipt Voucher"
					, "user can release the Receipt Voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Receipt Voucher"
					,"user can release the Receipt Voucher"
					, "user can release the Receipt Voucher Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New GL Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		//===================================================FIN_11_51
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft"
					,"user can use Draft"
					, "user can use Draft Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft"
					,"user can use Draft"
					, "user can use Draft Fail", "fail");
		}
		
		//===================================================FIN_11_52
		
		WaitElement(btn_EditBtnA);
		click(btn_EditBtnA);
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSel3A);
		
		WaitElement(txt_DescriptionIPA);
		sendKeys(txt_DescriptionIPA, "@TestInboundPayment1");
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_UpdateBtnA);
		click(btn_UpdateBtnA);
		
		Thread.sleep(4000);
		
		String PaymentMethodAfterDraftValueA = getText(txt_PaymentMethodAfterDraftIPA);
		String DescriptionAfterDraftValueA = getText(txt_DescriptionAfterDraftIPA);
		
		if(PaymentMethodAfterDraftValueA.contentEquals(PaymentMethodSel3A) && DescriptionAfterDraftValueA.contentEquals("@TestInboundPayment1")){
			writeTestResults("Verify that user can edit drafted Inbound Payment (Update)"
					,"user can edit drafted Inbound Payment (Update)"
					, "user can edit drafted Inbound Payment (Update) Successfully", "pass");
		}else {
			writeTestResults("Verify that user can edit drafted Inbound Payment (Update)"
					,"user can edit drafted Inbound Payment (Update)"
					, "user can edit drafted Inbound Payment (Update) Fail", "fail");
		}
		
		//===================================================FIN_11_53
		
		WaitElement(btn_EditBtnA);
		click(btn_EditBtnA);
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		WaitElement(txt_DescriptionIPA);
		sendKeys(txt_DescriptionIPA, "@TestInboundPayment2");
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_UpdateAndNewBtnA);
		click(btn_UpdateAndNewBtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New GL Receipt")){
			writeTestResults("Verify that user can edit drafted Inbound Payment (Update & New)"
					,"user can edit drafted Inbound Payment (Update & New)"
					, "user can edit drafted Inbound Payment (Update & New) Successfully", "pass");
		}else {
			writeTestResults("Verify that user can edit drafted Inbound Payment (Update & New)"
					,"user can edit drafted Inbound Payment (Update & New)"
					, "user can edit drafted Inbound Payment (Update & New) Fail", "fail");
		}
	}
	
	//FIN_11_54
	public void VerifyThatUserCanDeleteaDraftedInboundPayment() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);

		WaitElement(btn_ReceiptVoucherJourneyIPABtnA);
		click(btn_ReceiptVoucherJourneyIPABtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Receipt Voucher")){
			writeTestResults("Navigation to Inbound Payment Advice - New Receipt Voucher page"
					,"User can Navigate Inbound Payment Advice - New Receipt Voucher page"
					, "User can Navigate Inbound Payment Advice - New Receipt Voucher page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Receipt Voucher page"
					,"User can Navigate Inbound Payment Advice - New Receipt Voucher page"
					, "User can Navigate Inbound Payment Advice - New Receipt Voucher page Fail", "fail");
		}
		
		WaitElement(txt_DescriptionIPATxtA);
		sendKeys(txt_DescriptionIPATxtA, "@Test1996");
		
		WaitElement(btn_GLAccIPABtnA);
		click(btn_GLAccIPABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "100");
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Receipt Voucher Form"
					,"user can use Draft the Receipt Voucher Form"
					, "user can use Draft the Receipt Voucher Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Receipt Voucher Form"
					,"user can use Draft the Receipt Voucher Form"
					, "user can use Draft the Receipt Voucher Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Receipt Voucher","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Receipt Voucher"
					,"user can release the Receipt Voucher"
					, "user can release the Receipt Voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Receipt Voucher"
					,"user can release the Receipt Voucher"
					, "user can release the Receipt Voucher Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New GL Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		//===================================================FIN_11_54
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft"
					,"user can use Draft"
					, "user can use Draft Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft"
					,"user can use Draft"
					, "user can use Draft Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_DeleteBtnA);
		click(btn_DeleteBtnA);
		WaitElement(btn_YesBtnA);
		click(btn_YesBtnA);
		
		WaitElement(txt_pageDeleteA);
		if(isDisplayed(txt_pageDeleteA)) {
			writeTestResults("Verify that user can Delete a Drafted Inbound Payment"
					,"user can Delete a Drafted Inbound Payment"
					, "user can Delete a Drafted Inbound Payment Successfully", "pass");
		}else {
			writeTestResults("Verify that user can Delete a Drafted Inbound Payment"
					,"user can Delete a Drafted Inbound Payment"
					, "user can Delete a Drafted Inbound Payment Fail", "fail");
		}
	}
	
	//FIN_11_55
	public void VerifyThatUserCanViewHistoryOfaDraftedInboundPayment() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);

		WaitElement(btn_ReceiptVoucherJourneyIPABtnA);
		click(btn_ReceiptVoucherJourneyIPABtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Receipt Voucher")){
			writeTestResults("Navigation to Inbound Payment Advice - New Receipt Voucher page"
					,"User can Navigate Inbound Payment Advice - New Receipt Voucher page"
					, "User can Navigate Inbound Payment Advice - New Receipt Voucher page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Receipt Voucher page"
					,"User can Navigate Inbound Payment Advice - New Receipt Voucher page"
					, "User can Navigate Inbound Payment Advice - New Receipt Voucher page Fail", "fail");
		}
		
		WaitElement(txt_DescriptionIPATxtA);
		sendKeys(txt_DescriptionIPATxtA, "@Test1996");
		
		WaitElement(btn_GLAccIPABtnA);
		click(btn_GLAccIPABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "100");
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Receipt Voucher Form"
					,"user can use Draft the Receipt Voucher Form"
					, "user can use Draft the Receipt Voucher Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Receipt Voucher Form"
					,"user can use Draft the Receipt Voucher Form"
					, "user can use Draft the Receipt Voucher Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Receipt Voucher","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Receipt Voucher"
					,"user can release the Receipt Voucher"
					, "user can release the Receipt Voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Receipt Voucher"
					,"user can release the Receipt Voucher"
					, "user can release the Receipt Voucher Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New GL Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft"
					,"user can use Draft"
					, "user can use Draft Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft"
					,"user can use Draft"
					, "user can use Draft Fail", "fail");
		}
		
		//===================================================FIN_11_55
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_HistoryBtnA);
		click(btn_HistoryBtnA);
		
		WaitElement(txt_DraftedHistoryTxtIPA);
		if(isDisplayed(txt_DraftedHistoryTxtIPA)) {
			writeTestResults("Verify that user can view history of a Drafted Inbound Payment"
					,"user can view history of a Drafted Inbound Payment"
					, "user can view history of a Drafted Inbound Payment Successfully", "pass");
		}else {
			writeTestResults("Verify that user can view history of a Drafted Inbound Payment"
					,"user can view history of a Drafted Inbound Payment"
					, "user can view history of a Drafted Inbound Payment Fail", "fail");
		}
	}
	
	//FIN_11_56
	public void VerifyThatUserCanUseDraftAndNewIP() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);

		WaitElement(btn_ReceiptVoucherJourneyIPABtnA);
		click(btn_ReceiptVoucherJourneyIPABtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Receipt Voucher")){
			writeTestResults("Navigation to Inbound Payment Advice - New Receipt Voucher page"
					,"User can Navigate Inbound Payment Advice - New Receipt Voucher page"
					, "User can Navigate Inbound Payment Advice - New Receipt Voucher page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Receipt Voucher page"
					,"User can Navigate Inbound Payment Advice - New Receipt Voucher page"
					, "User can Navigate Inbound Payment Advice - New Receipt Voucher page Fail", "fail");
		}
		
		WaitElement(txt_DescriptionIPATxtA);
		sendKeys(txt_DescriptionIPATxtA, "@Test1996");
		
		WaitElement(btn_GLAccIPABtnA);
		click(btn_GLAccIPABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Receipt Voucher Form"
					,"user can use Draft the Receipt Voucher Form"
					, "user can use Draft the Receipt Voucher Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Receipt Voucher Form"
					,"user can use Draft the Receipt Voucher Form"
					, "user can use Draft the Receipt Voucher Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Receipt Voucher","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Receipt Voucher"
					,"user can release the Receipt Voucher"
					, "user can release the Receipt Voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Receipt Voucher"
					,"user can release the Receipt Voucher"
					, "user can release the Receipt Voucher Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New GL Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		//===================================================FIN_11_56
		
		WaitElement(btn_DraftAndNewBtnA);
		click(btn_DraftAndNewBtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New GL Receipt")){
			writeTestResults("Verify that user can use Draft and New"
					,"user can use Draft and New"
					, "user can use Draft and New Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft and New"
					,"user can use Draft and New"
					, "user can use Draft and New Fail", "fail");
		}
	}
	
	//FIN_11_57to11_58
	public void VerifyThatUserCanReleaseAnInboundPayment() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);

		WaitElement(btn_ReceiptVoucherJourneyIPABtnA);
		click(btn_ReceiptVoucherJourneyIPABtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Receipt Voucher")){
			writeTestResults("Navigation to Inbound Payment Advice - New Receipt Voucher page"
					,"User can Navigate Inbound Payment Advice - New Receipt Voucher page"
					, "User can Navigate Inbound Payment Advice - New Receipt Voucher page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Receipt Voucher page"
					,"User can Navigate Inbound Payment Advice - New Receipt Voucher page"
					, "User can Navigate Inbound Payment Advice - New Receipt Voucher page Fail", "fail");
		}
		
		WaitElement(txt_DescriptionIPATxtA);
		sendKeys(txt_DescriptionIPATxtA, "@Test1996");
		
		WaitElement(btn_GLAccIPABtnA);
		click(btn_GLAccIPABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Receipt Voucher Form"
					,"user can use Draft the Receipt Voucher Form"
					, "user can use Draft the Receipt Voucher Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Receipt Voucher Form"
					,"user can use Draft the Receipt Voucher Form"
					, "user can use Draft the Receipt Voucher Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Receipt Voucher","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Receipt Voucher"
					,"user can release the Receipt Voucher"
					, "user can release the Receipt Voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Receipt Voucher"
					,"user can release the Receipt Voucher"
					, "user can release the Receipt Voucher Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New GL Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New GL Receipt Form"
					,"user can use Draft the Inbound Payment - New GL Receipt Form"
					, "user can use Draft the Inbound Payment - New GL Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New GL Receipt Form"
					,"user can use Draft the Inbound Payment - New GL Receipt Form"
					, "user can use Draft the Inbound Payment - New GL Receipt Form Fail", "fail");
		}
		
		//===================================================FIN_11_57
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release an Inbound Payment"
					,"user can release an Inbound Payment"
					, "user can release an Inbound Payment Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release an Inbound Payment"
					,"user can release an Inbound Payment"
					, "user can release an Inbound Payment Fail", "fail");
		}
		
		//===================================================FIN_11_58
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ReverseBtnA);
		click(btn_ReverseBtnA);
		WaitElement(btn_ReverseLookupBtnA);
		click(btn_ReverseLookupBtnA);
		WaitElement(btn_YesBtnA);
		click(btn_YesBtnA);
		
		WaitElement(txt_PageReverseA);
		if(isDisplayed(txt_PageReverseA)) {
			writeTestResults("Verify that user can Reverse an Inbound Payment"
					,"user can Reverse an Inbound Payment"
					, "user can Reverse an Inbound Payment Successfully", "pass");
		}else {
			writeTestResults("Verify that user can Reverse an Inbound Payment"
					,"user can Reverse an Inbound Payment"
					, "user can Reverse an Inbound Payment Fail", "fail");
		}
	}
	
	//FIN_11_59
	public void VerifyThatChequeNoFieldInPaymentDetailsCantBeDuplicated() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);

		WaitElement(btn_ReceiptVoucherJourneyIPABtnA);
		click(btn_ReceiptVoucherJourneyIPABtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Receipt Voucher")){
			writeTestResults("Navigation to Inbound Payment Advice - New Receipt Voucher page"
					,"User can Navigate Inbound Payment Advice - New Receipt Voucher page"
					, "User can Navigate Inbound Payment Advice - New Receipt Voucher page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Receipt Voucher page"
					,"User can Navigate Inbound Payment Advice - New Receipt Voucher page"
					, "User can Navigate Inbound Payment Advice - New Receipt Voucher page Fail", "fail");
		}
		
		WaitElement(txt_DescriptionIPATxtA);
		sendKeys(txt_DescriptionIPATxtA, "@Test1996");
		
		WaitElement(btn_GLAccIPABtnA);
		click(btn_GLAccIPABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Receipt Voucher Form"
					,"user can use Draft the Receipt Voucher Form"
					, "user can use Draft the Receipt Voucher Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Receipt Voucher Form"
					,"user can use Draft the Receipt Voucher Form"
					, "user can use Draft the Receipt Voucher Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo1 =trackCode.replace(" Receipt Voucher","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Receipt Voucher"
					,"user can release the Receipt Voucher"
					, "user can release the Receipt Voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Receipt Voucher"
					,"user can release the Receipt Voucher"
					, "user can release the Receipt Voucher Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New GL Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSel2A);
		
		WaitElement(sel_BankNameIPSelA);
		selectText(sel_BankNameIPSelA, BankNameIPSelA);
		
		WaitElement(txt_ChequeNoIPTxtA);
		LocalTime Obj1 = LocalTime.now();
		sendKeys(txt_ChequeNoIPTxtA, "CHK"+Obj1);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo1));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo1));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New GL Receipt Form"
					,"user can use Draft the Inbound Payment - New GL Receipt Form"
					, "user can use Draft the Inbound Payment - New GL Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New GL Receipt Form"
					,"user can use Draft the Inbound Payment - New GL Receipt Form"
					, "user can use Draft the Inbound Payment - New GL Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPNo1 =trackCode.replace(" GL Receipt", "");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can able to create receipt voucher using 'Cheque' pay method"
					,"user can able to create receipt voucher using 'Cheque' pay method"
					, "user can able to create receipt voucher using 'Cheque' pay method Successfully", "pass");
		}else {
			writeTestResults("Verify that user can able to create receipt voucher using 'Cheque' pay method"
					,"user can able to create receipt voucher using 'Cheque' pay method"
					, "user can able to create receipt voucher using 'Cheque' pay method Fail", "fail");
		}
		
		//===================================================FIN_11_59
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);

		WaitElement(btn_ReceiptVoucherJourneyIPABtnA);
		click(btn_ReceiptVoucherJourneyIPABtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Receipt Voucher")){
			writeTestResults("Navigation to Inbound Payment Advice - New Receipt Voucher page"
					,"User can Navigate Inbound Payment Advice - New Receipt Voucher page"
					, "User can Navigate Inbound Payment Advice - New Receipt Voucher page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice - New Receipt Voucher page"
					,"User can Navigate Inbound Payment Advice - New Receipt Voucher page"
					, "User can Navigate Inbound Payment Advice - New Receipt Voucher page Fail", "fail");
		}
		
		WaitElement(txt_DescriptionIPATxtA);
		sendKeys(txt_DescriptionIPATxtA, "@Test1996");
		
		WaitElement(btn_GLAccIPABtnA);
		click(btn_GLAccIPABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_AmountFieldCDMTxtA);
		sendKeys(txt_AmountFieldCDMTxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Receipt Voucher Form"
					,"user can use Draft the Receipt Voucher Form"
					, "user can use Draft the Receipt Voucher Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Receipt Voucher Form"
					,"user can use Draft the Receipt Voucher Form"
					, "user can use Draft the Receipt Voucher Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo2 =trackCode.replace(" Receipt Voucher","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Receipt Voucher"
					,"user can release the Receipt Voucher"
					, "user can release the Receipt Voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Receipt Voucher"
					,"user can release the Receipt Voucher"
					, "user can release the Receipt Voucher Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New GL Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSel2A);
		
		WaitElement(sel_BankNameIPSelA);
		selectText(sel_BankNameIPSelA, BankNameIPSelA);
		
		WaitElement(txt_ChequeNoIPTxtA);
		sendKeys(txt_ChequeNoIPTxtA, "CHK"+Obj1);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo2));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo2));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_SameChequeNoValidatorIPA.replace("IPNo1", IPNo1));
		if(isDisplayed(txt_SameChequeNoValidatorIPA.replace("IPNo1", IPNo1))) {
			writeTestResults("Verify that Cheque No field in payment details can't be duplicated"
					,"Cheque No field in payment details can't be duplicated"
					, "Cheque No field in payment details can't be duplicated Successfully", "pass");
		}else {
			writeTestResults("Verify that Cheque No field in payment details can't be duplicated"
					,"Cheque No field in payment details can't be duplicated"
					, "Cheque No field in payment details can't be duplicated Fail", "fail");
		}
	}
	
	//FIN_11_60
	public void VerifyThatOverPaymentCanBePostedInCustomerReceiptVoucher() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);
		
		WaitElement(btn_CustomerAdvanceJourneyBtnA);
		click(btn_CustomerAdvanceJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Customer Advance")){
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Fail", "fail");
		}
		
		WaitElement(btn_CustomerLookUpIPABtnA);
		click(btn_CustomerLookUpIPABtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxtA);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		
		WaitElement(txt_AmountFieldIPATxtA);
		sendKeys(txt_AmountFieldIPATxtA, "100");
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Customer Advance","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Customer Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		//===================================================FIN_11_60
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(txt_PayingAmountIPTxtA);
		sendKeys(txt_PayingAmountIPTxtA, "110");
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that over payment can be posted in customer receipt voucher"
					,"over payment can be posted in customer receipt voucher"
					, "over payment can be posted in customer receipt voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that over payment can be posted in customer receipt voucher"
					,"over payment can be posted in customer receipt voucher"
					, "over payment can be posted in customer receipt voucher Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_JournalActionBtnA);
		click(btn_JournalActionBtnA);
		
//		System.out.println(getText(txt_JournalCreditIPTxtA));
//		System.out.println(getText(txt_JournalDeditIPTxtA));
		
		if(getText(txt_JournalCreditIPTxtA).equals(getText(txt_JournalDebitIPTxtA))) {
			writeTestResults("Verify that user can verify the journal entry of the created customer receipt voucher"
					,"user can verify the journal entry of the created customer receipt voucher"
					, "user can verify the journal entry of the created customer receipt voucher Successfully", "pass");
		}else {
			writeTestResults("Verify that user can verify the journal entry of the created customer receipt voucher"
					,"user can verify the journal entry of the created customer receipt voucher"
					, "user can verify the journal entry of the created customer receipt voucher Fail", "fail");
		}
	}
	
	//Bank Reconciliation
	
	//FIN_16_1to16_3
	public void NavigationToBankReconciliationNewPage() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);
		
		WaitElement(btn_CustomerAdvanceJourneyBtnA);
		click(btn_CustomerAdvanceJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Customer Advance")){
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Fail", "fail");
		}
		
		WaitElement(btn_CustomerLookUpIPABtnA);
		click(btn_CustomerLookUpIPABtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxtA);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		
		WaitElement(txt_AmountFieldIPATxtA);
		sendKeys(txt_AmountFieldIPATxtA, "100");
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(4000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Customer Advance","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Customer Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSel3A);
		
		WaitElement(sel_cboxEFTBankIP);
		selectText(sel_cboxEFTBankIP, BankBDSelA);
		
		WaitElement(sel_cboxEFTBankAccountIP);
		selectText(sel_cboxEFTBankAccountIP, BankAccountNoBDNewSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(4000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPNo = trackCode.replace(" Customer Receipt", "");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can able to create customer receipt voucher using 'EFT' pay method"
					,"user can able to create customer receipt voucher using 'EFT' pay method"
					, "user can able to create customer receipt voucher using 'EFT' pay method Successfully", "pass");
		}else {
			writeTestResults("Verify that user can able to create customer receipt voucher using 'EFT' pay method"
					,"user can able to create customer receipt voucher using 'EFT' pay method"
					, "user can able to create customer receipt voucher using 'EFT' pay method Fail", "fail");
		}
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_BankReconciliationBtnA);
		click(btn_BankReconciliationBtnA);
		WaitElement(btn_NewBankReconciliationBtnA);
		click(btn_NewBankReconciliationBtnA);
		
		//===================================================FIN_16.1
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Reconciliation - New")){
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Fail", "fail");
		}
		
		WaitElement(sel_cboxBankBR);
		selectText(sel_cboxBankBR, BankBDSelA);
		WaitElement(sel_cboxBankAccountNoBR);
		selectText(sel_cboxBankAccountNoBR, BankAccountNoBDNewSelA);
		
		ScrollDownAruna();
		
		WaitElement(txt_DocNoSearchBRA);
		sendKeys(txt_DocNoSearchBRA, IPNo);
		pressEnter(txt_DocNoSearchBRA);
		
		WaitClick(sel_DocSelBR.replace("DocNoBR", IPNo));
		click(sel_DocSelBR.replace("DocNoBR", IPNo));
		
		String ReconciliedAmountBR= driver.findElement(By.xpath(txt_ReconciliedAmountBR)).getAttribute("value");
		//System.out.println(ReconciliedAmountBR);
		
		ScrollUpAruna();
		
		WaitElement(txt_StatementBalanceBR);
		sendKeys(txt_StatementBalanceBR, ReconciliedAmountBR.replace("-", ""));
		
		//===================================================FIN_16.2
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Bank reconciliation Form"
					,"user can use Draft the Bank reconciliation Form"
					, "user can use Draft the Bank reconciliation Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Bank reconciliation Form"
					,"user can use Draft the Bank reconciliation Form"
					, "user can use Draft the Bank reconciliation Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		BRNo =trackCode;
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verifiy that user can able to create Bank reconciliation using Inbound Payment"
					,"user can able to create Bank reconciliation using Inbound Payment"
					, "user can able to create Bank reconciliation using Inbound Payment Successfully", "pass");
		}else {
			writeTestResults("Verifiy that user can able to create Bank reconciliation using Inbound Payment"
					,"user can able to create Bank reconciliation using Inbound Payment"
					, "user can able to create Bank reconciliation using Inbound Payment Fail", "fail");
		}
		
		//===================================================FIN_16.3
		
		for (int i = 1; i <= 3; i++) {
			driver.navigate().back();
		}
		
		Thread.sleep(2000);
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		
		if(!isDisplayed(btn_ReverseBtnA)) {
			writeTestResults("Verify that Inbound Payment which payment method is EFT method used in the bank reconciliation cannot reversible"
					,"Inbound Payment which payment method is EFT method used in the bank reconciliation cannot reversible"
					, "Inbound Payment which payment method is EFT method used in the bank reconciliation cannot reversible Successfully", "pass");
		}else {
			writeTestResults("Verify that Inbound Payment which payment method is EFT method used in the bank reconciliation cannot reversible"
					,"Inbound Payment which payment method is EFT method used in the bank reconciliation cannot reversible"
					, "Inbound Payment which payment method is EFT method used in the bank reconciliation cannot reversible Fail", "fail");
		}
	}
	
	//FIN_16_4to16_5
	public void VerifyThatChequeReturnFunctionCanBeDoneForInboundPaymentWhichPaymentMethodIsChequeUsedInTheBankReconciliation() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);
		
		WaitElement(btn_CustomerAdvanceJourneyBtnA);
		click(btn_CustomerAdvanceJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Customer Advance")){
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Fail", "fail");
		}
		
		WaitElement(btn_CustomerLookUpIPABtnA);
		click(btn_CustomerLookUpIPABtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxtA);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		
		WaitElement(txt_AmountFieldIPATxtA);
		sendKeys(txt_AmountFieldIPATxtA, "100");
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(4000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Customer Advance","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Customer Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSel2A);
		
		WaitElement(sel_BankNameIPSelA);
		selectText(sel_BankNameIPSelA, BankNameIPSelA);
		
		WaitElement(txt_ChequeNoIPTxtA);
		LocalTime Obj1 = LocalTime.now();
		sendKeys(txt_ChequeNoIPTxtA, "CHK"+Obj1);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(4000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPNo = trackCode.replace(" Customer Receipt", "");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can able to create customer receipt voucher using 'Cheque' pay method"
					,"user can able to create customer receipt voucher using 'Cheque' pay method"
					, "user can able to create customer receipt voucher using 'Cheque' pay method Successfully", "pass");
		}else {
			writeTestResults("Verify that user can able to create customer receipt voucher using 'Cheque' pay method"
					,"user can able to create customer receipt voucher using 'Cheque' pay method"
					, "user can able to create customer receipt voucher using 'Cheque' pay method Fail", "fail");
		}
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_BankDepositBtnA);
		click(btn_BankDepositBtnA);
		WaitElement(btn_NewBankDepositBtnA);
		click(btn_NewBankDepositBtnA);
		
		WaitElement(sel_PayMethodBDSelA);
		selectText(sel_PayMethodBDSelA, PaymentMethodSel2A);
		
		WaitElement(sel_BankBDSelA);
		selectText(sel_BankBDSelA, BankBDSelA);
		
		WaitElement(sel_BankAccountNoBDSelA);
		selectText(sel_BankAccountNoBDSelA, BankAccountNoBDNewSelA);
		
		WaitElement(btn_SortByBDBtnA);
		selectText(btn_SortByBDBtnA, "Cheque No");
		
		WaitElement(txt_CheqNoBDTxtA);
		sendKeys(txt_CheqNoBDTxtA, "CHK"+Obj1);
		
		WaitClick(btn_ViewBDBtnA);
		click(btn_ViewBDBtnA);
		
		ScrollDownAruna();
		
		WaitElement(btn_DolSelBDBtnA.replace("IPNo", IPNo));
		Thread.sleep(4000);
		click(btn_DolSelBDBtnA.replace("IPNo", IPNo));
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Bank Deposit Form"
					,"user can use Draft the Bank Deposit Form"
					, "user can use Draft the Bank Deposit Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Bank Deposit Form"
					,"user can use Draft the Bank Deposit Form"
					, "user can use Draft the Bank Deposit Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(4000);
		trackCode= getText(txt_HeaderDocNoA);
		String BDNo =trackCode;
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can use Release the Bank Deposit Form"
					,"user can use Release the Bank Deposit Form"
					, "user can use Release the Bank Deposit Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Release the Bank Deposit Form"
					,"user can use Release the Bank Deposit Form"
					, "user can use Release the Bank Deposit Form Fail", "fail");
		}
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_BankReconciliationBtnA);
		click(btn_BankReconciliationBtnA);
		WaitElement(btn_NewBankReconciliationBtnA);
		click(btn_NewBankReconciliationBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Reconciliation - New")){
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Fail", "fail");
		}
		
		WaitElement(sel_cboxBankBR);
		selectText(sel_cboxBankBR, BankBDSelA);
		WaitElement(sel_cboxBankAccountNoBR);
		selectText(sel_cboxBankAccountNoBR, BankAccountNoBDNewSelA);
		
		ScrollDownAruna();
		
		WaitElement(txt_DocNoSearchBRA);
		sendKeys(txt_DocNoSearchBRA, BDNo);
		pressEnter(txt_DocNoSearchBRA);
		
		WaitClick(sel_DocSelBR.replace("DocNoBR", "CHK"+Obj1));
		click(sel_DocSelBR.replace("DocNoBR", "CHK"+Obj1));
		
		String ReconciliedAmountBR= driver.findElement(By.xpath(txt_ReconciliedAmountBR)).getAttribute("value");
		//System.out.println(ReconciliedAmountBR);
		
		ScrollUpAruna();
		
		WaitElement(txt_StatementBalanceBR);
		sendKeys(txt_StatementBalanceBR, ReconciliedAmountBR.replace("-", ""));
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Bank reconciliation Form"
					,"user can use Draft the Bank reconciliation Form"
					, "user can use Draft the Bank reconciliation Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Bank reconciliation Form"
					,"user can use Draft the Bank reconciliation Form"
					, "user can use Draft the Bank reconciliation Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		BRNo =trackCode;
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verifiy that user can able to create Bank reconciliation"
					,"user can able to create Bank reconciliation"
					, "user can able to create Bank reconciliation Successfully", "pass");
		}else {
			writeTestResults("Verifiy that user can able to create Bank reconciliation"
					,"user can able to create Bank reconciliation"
					, "user can able to create Bank reconciliation Fail", "fail");
		}
		
		//===================================================FIN_16.5
		
		for (int i = 1; i <= 3; i++) {
			driver.navigate().back();
		}
		
		Thread.sleep(2000);
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		
		if(!isDisplayed(btn_ReverseBtnA)) {
			writeTestResults("Verify that Bank Deposit entry used in the bank reconciliation cannot reversible"
					,"Bank Deposit entry used in the bank reconciliation cannot reversible"
					, "Bank Deposit entry used in the bank reconciliation cannot reversible Successfully", "pass");
		}else {
			writeTestResults("Verify that Bank Deposit entry used in the bank reconciliation cannot reversible"
					,"Bank Deposit entry used in the bank reconciliation cannot reversible"
					, "Bank Deposit entry used in the bank reconciliation cannot reversible Fail", "fail");
		}
		
		//===================================================FIN_16.4
		
		pageRefersh();
		Thread.sleep(2000);
		
		for (int i = 1; i <= 4; i++) {
			driver.navigate().back();
		}
		
		Thread.sleep(2000);
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ChequeReturnBtnIPA);
		click(btn_ChequeReturnBtnIPA);
		
		WaitElement(sel_ReturnResonSelIPA);
		selectText(sel_ReturnResonSelIPA, "Funds insufficient");
		
		JavascriptExecutor j7 = (JavascriptExecutor)driver;
		j7.executeScript("$('#txtReturnDate').datepicker('setDate', new Date())");
		
		WaitElement(txt_ReturnRemarkTxtIPA);
		sendKeys(txt_ReturnRemarkTxtIPA, "Return CHK");
		
		WaitClick(btn_ReturnBtnIPABtnIPA);
		click(btn_ReturnBtnIPABtnIPA);
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		
		if(!isDisplayed(btn_ChequeReturnBtnIPA)) {
			writeTestResults("Verify that 'Cheque Return' function can be done for Inbound Payment which payment method is cheque used in the bank reconciliation"
					,"'Cheque Return' function can be done for Inbound Payment which payment method is cheque used in the bank reconciliation"
					, "'Cheque Return' function can be done for Inbound Payment which payment method is cheque used in the bank reconciliation Successfully", "pass");
		}else {
			writeTestResults("Verify that 'Cheque Return' function can be done for Inbound Payment which payment method is cheque used in the bank reconciliation"
					,"'Cheque Return' function can be done for Inbound Payment which payment method is cheque used in the bank reconciliation"
					, "'Cheque Return' function can be done for Inbound Payment which payment method is cheque used in the bank reconciliation Fail", "fail");
		}
	}
	
	//FIN_16_6to16_10
	public void VerifyThatInboundPaymentWhichPaymentMethodIsDirectDepositMethodUsedInTheBankReconciliationCannotReversible() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);
		
		WaitElement(btn_CustomerAdvanceJourneyBtnA);
		click(btn_CustomerAdvanceJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Customer Advance")){
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Fail", "fail");
		}
		
		WaitElement(btn_CustomerLookUpIPABtnA);
		click(btn_CustomerLookUpIPABtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxtA);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		
		WaitElement(txt_AmountFieldIPATxtA);
		sendKeys(txt_AmountFieldIPATxtA, "100");
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(4000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Customer Advance","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Customer Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSel4A);
		
		WaitElement(sel_cboxDDBankIP);
		selectText(sel_cboxDDBankIP, BankBDSelA);
		
		WaitElement(sel_cboxDDBankAccountIP);
		selectText(sel_cboxDDBankAccountIP, BankAccountNoBDNewSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(4000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPNo = trackCode.replace(" Customer Receipt", "");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can able to create customer receipt voucher using 'Direct Deposit' pay method"
					,"user can able to create customer receipt voucher using 'Direct Deposit' pay method"
					, "user can able to create customer receipt voucher using 'Direct Deposit' pay method Successfully", "pass");
		}else {
			writeTestResults("Verify that user can able to create customer receipt voucher using 'Direct Deposit' pay method"
					,"user can able to create customer receipt voucher using 'Direct Deposit' pay method"
					, "user can able to create customer receipt voucher using 'Direct Deposit' pay method Fail", "fail");
		}
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_BankReconciliationBtnA);
		click(btn_BankReconciliationBtnA);
		WaitElement(btn_NewBankReconciliationBtnA);
		click(btn_NewBankReconciliationBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Reconciliation - New")){
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Fail", "fail");
		}
		
		WaitElement(sel_cboxBankBR);
		selectText(sel_cboxBankBR, BankBDSelA);
		WaitElement(sel_cboxBankAccountNoBR);
		selectText(sel_cboxBankAccountNoBR, BankAccountNoBDNewSelA);
		
		ScrollDownAruna();
		
		WaitElement(txt_DocNoSearchBRA);
		sendKeys(txt_DocNoSearchBRA, IPNo);
		pressEnter(txt_DocNoSearchBRA);
		
		WaitClick(sel_DocSelBR.replace("DocNoBR", IPNo));
		click(sel_DocSelBR.replace("DocNoBR", IPNo));
		
		String ReconciliedAmountBR= driver.findElement(By.xpath(txt_ReconciliedAmountBR)).getAttribute("value");
		//System.out.println(ReconciliedAmountBR);
		
		ScrollUpAruna();
		
		WaitElement(txt_StatementBalanceBR);
		sendKeys(txt_StatementBalanceBR, ReconciliedAmountBR.replace("-", ""));
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Bank reconciliation Form"
					,"user can use Draft the Bank reconciliation Form"
					, "user can use Draft the Bank reconciliation Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Bank reconciliation Form"
					,"user can use Draft the Bank reconciliation Form"
					, "user can use Draft the Bank reconciliation Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		BRNo =trackCode;
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verifiy that user can able to create Bank reconciliation"
					,"user can able to create Bank reconciliation"
					, "user can able to create Bank reconciliation Successfully", "pass");
		}else {
			writeTestResults("Verifiy that user can able to create Bank reconciliation"
					,"user can able to create Bank reconciliation"
					, "user can able to create Bank reconciliation Fail", "fail");
		}
		
		//===================================================FIN_16_6
		
		for (int i = 1; i <= 3; i++) {
			driver.navigate().back();
		}
		
		Thread.sleep(2000);
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		
		if(!isDisplayed(btn_ReverseBtnA)) {
			writeTestResults("Verify that Inbound Payment which payment method is Direct Deposit method used in the bank reconciliation cannot reversible"
					,"Inbound Payment which payment method is Direct Deposit method used in the bank reconciliation cannot reversible"
					, "Inbound Payment which payment method is Direct Deposit method used in the bank reconciliation cannot reversible Successfully", "pass");
		}else {
			writeTestResults("Verify that Inbound Payment which payment method is Direct Deposit method used in the bank reconciliation cannot reversible"
					,"Inbound Payment which payment method is Direct Deposit method used in the bank reconciliation cannot reversible"
					, "Inbound Payment which payment method is Direct Deposit method used in the bank reconciliation cannot reversible Fail", "fail");
		}
		
		//===================================================FIN_16_7
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_BankReconciliationBtnA);
		click(btn_BankReconciliationBtnA);
		WaitElement(btn_NewBankReconciliationBtnA);
		click(btn_NewBankReconciliationBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Reconciliation - New")){
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Fail", "fail");
		}
		
		WaitElement(sel_cboxBankBR);
		selectText(sel_cboxBankBR, BankBDSelA);
		WaitElement(sel_cboxBankAccountNoBR);
		selectText(sel_cboxBankAccountNoBR, BankAccountNoBDNewSelA);
		
		ScrollDownAruna();
		
		WaitElement(txt_DocNoSearchBRA);
		sendKeys(txt_DocNoSearchBRA, IPNo);
		pressEnter(txt_DocNoSearchBRA);
		
		if(!isDisplayed(sel_DocSelBR.replace("DocNoBR", IPNo))){
			writeTestResults("Verify that user don’t have the access to Inbound Payment which is already used in bank reconciliation"
					,"user don’t have the access to Inbound Payment which is already used in bank reconciliation"
					, "user don’t have the access to Inbound Payment which is already used in bank reconciliation Successfully", "pass");
		}else {
			writeTestResults("Verify that user don’t have the access to Inbound Payment which is already used in bank reconciliation"
					,"user don’t have the access to Inbound Payment which is already used in bank reconciliation"
					, "user don’t have the access to Inbound Payment which is already used in bank reconciliation Fail", "fail");
		}
		
		//===================================================FIN_16_8
		
		WaitElement(btn_BankReconciliationByPageBtnA);
		click(btn_BankReconciliationByPageBtnA);
		WaitElement(txt_BankReconciliationByPageSearchTxtA);
		sendKeys(txt_BankReconciliationByPageSearchTxtA, BRNo);
		pressEnter(txt_BankReconciliationByPageSearchTxtA);
		WaitElement(sel_BankReconciliationByPageSearchSelA.replace("BRNo", BRNo));
		doubleClick(sel_BankReconciliationByPageSearchSelA.replace("BRNo", BRNo));
		
		Thread.sleep(2000);
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ReverseBtnA);
		click(btn_ReverseBtnA);
		WaitElement(btn_YesBtnA);
		click(btn_YesBtnA);
		
		WaitElement(txt_PageReverseA);
		if(isDisplayed(txt_PageReverseA)) {
			writeTestResults("Verify that user can reverse the released bank reconciliation"
					,"user can reverse the released bank reconciliation"
					, "user can reverse the released bank reconciliation Successfully", "pass");
		}else {
			writeTestResults("Verify that user can reverse the released bank reconciliation"
					,"user can reverse the released bank reconciliation"
					, "user can reverse the released bank reconciliation Fail", "fail");
		}
		
		//===================================================FIN_16_9
		
		for (int i = 1; i <= 4; i++) {
			driver.navigate().back();
		}
		
		Thread.sleep(2000);
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ReverseBtnA);
		click(btn_ReverseBtnA);
		WaitElement(btn_ReverseLookupBtnA);
		click(btn_ReverseLookupBtnA);
		WaitElement(btn_YesBtnA);
		click(btn_YesBtnA);
		
		WaitElement(txt_PageReverseA);
		if(isDisplayed(txt_PageReverseA)) {
			writeTestResults("Verify the Inbound Payment used in above reversed bank reconciliation can be reversible"
					,"Inbound Payment used in above reversed bank reconciliation can be reversible"
					, "Inbound Payment used in above reversed bank reconciliation can be reversible Successfully", "pass");
		}else {
			writeTestResults("Verify the Inbound Payment used in above reversed bank reconciliation can be reversible"
					,"Inbound Payment used in above reversed bank reconciliation can be reversible"
					, "Inbound Payment used in above reversed bank reconciliation can be reversible Fail", "fail");
		}
		
		//===================================================FIN_16_10
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_BankReconciliationBtnA);
		click(btn_BankReconciliationBtnA);
		WaitElement(btn_NewBankReconciliationBtnA);
		click(btn_NewBankReconciliationBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Reconciliation - New")){
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Fail", "fail");
		}
		
		WaitElement(sel_cboxBankBR);
		selectText(sel_cboxBankBR, BankBDSelA);
		WaitElement(sel_cboxBankAccountNoBR);
		selectText(sel_cboxBankAccountNoBR, BankAccountNoBDNewSelA);
		
		ScrollDownAruna();
		
		WaitElement(txt_DocNoSearchBRA);
		sendKeys(txt_DocNoSearchBRA, IPNo);
		pressEnter(txt_DocNoSearchBRA);
		
		WaitElement(sel_DocSel1BR.replace("DocNoBR", IPNo));
		if(isDisplayed(sel_DocSel1BR.replace("DocNoBR", IPNo)) && isDisplayed(sel_DocSel2BR.replace("DocNoBR", IPNo))){
			writeTestResults("Verify the availablity of reversed Inbound Payment entry"
					,"availablity of reversed Inbound Payment entry"
					, "availablity of reversed Inbound Payment entry Successfully", "pass");
		}else {
			writeTestResults("Verify the availablity of reversed Inbound Payment entry"
					,"availablity of reversed Inbound Payment entry"
					, "availablity of reversed Inbound Payment entry Fail", "fail");
		}
	}
	
	//FIN_16_11to16_13
	public void VerifyThatUserCanAbleToCreateBankReconciliationUsingOutboundPayment() throws Exception {
		
		WaitElement(btn_OutboundPaymentAdviceBtnA);
		click(btn_OutboundPaymentAdviceBtnA);
		WaitElement(btn_NewOutboundPaymentAdviceBtnA);
		click(btn_NewOutboundPaymentAdviceBtnA);
		
		WaitElement(btn_VendorAdvanceJourneyBtnOPA);
		click(btn_VendorAdvanceJourneyBtnOPA);
		
		if(getText(txt_PageHeaderA).contentEquals("Outbound Payment Advice - New Vendor Advance")){
			writeTestResults("Navigation to Outbound Payment Advice - New Vendor Advance page"
					,"User can Navigate Outbound Payment Advice - New Vendor Advance page"
					, "User can Navigate Outbound Payment Advice - New Vendor Advance page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Outbound Payment Advice - New Vendor Advance page"
					,"User can Navigate Outbound Payment Advice - New Vendor Advance page"
					, "User can Navigate Outbound Payment Advice - New Vendor Advance page Fail", "fail");
		}
		
		WaitElement(btn_VendorLookUpBtnOPA);
		click(btn_VendorLookUpBtnOPA);
		WaitElement(txt_VendorLookUpIPATxtA);
		sendKeys(txt_VendorLookUpIPATxtA, VendorLookUpIPATxtA);
		pressEnter(txt_VendorLookUpIPATxtA);
		WaitElement(sel_VendorLookUpIPASelA.replace("VenSel", VendorLookUpIPATxtA));
		doubleClick(sel_VendorLookUpIPASelA.replace("VenSel", VendorLookUpIPATxtA));
		
		WaitElement(txt_AmountTextOPA);
		sendKeys(txt_AmountTextOPA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Outbound Payment Advice Form"
					,"user can use Draft the Outbound Payment Advice Form"
					, "user can use Draft the Outbound Payment Advice Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Outbound Payment Advice Form"
					,"user can use Draft the Outbound Payment Advice Form"
					, "user can use Draft the Outbound Payment Advice Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(4000);
		trackCode= getText(txt_HeaderDocNoA);
		String OPANo =trackCode.replace(" Vendor Advance","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Vendor Advance"
					,"user can release the Vendor Advance"
					, "user can release the Vendor Advance Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Vendor Advance"
					,"user can release the Vendor Advance"
					, "user can release the Vendor Advance Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToOutboundPaymentOPABtnA);
		click(btn_ConvertToOutboundPaymentOPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Outbound Payment - New Vendor Payment")){
			writeTestResults("Navigation to Outbound Payment - New Vendor Payment page"
					,"User can Navigate Outbound Payment - New Vendor Payment page"
					, "User can Navigate Outbound Payment - New Vendor Payment page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Outbound Payment - New Vendor Payment page"
					,"User can Navigate Outbound Payment - New Vendor Payment page"
					, "User can Navigate Outbound Payment - New Vendor Payment page Fail", "fail");
		}
		
		WaitElement(sel_cboxPaybook);
		selectText(sel_cboxPaybook, cboxPaybook1A);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(sel_OPAFlagBtnA.replace("OPANo", OPANo));
		click(sel_OPAFlagBtnA.replace("OPANo", OPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Outbound Payment - New Vendor Payment Form"
					,"user can use Draft the Outbound Payment - New Vendor Payment Form"
					, "user can use Draft the Outbound Payment - New Vendor Payment Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Outbound Payment - New Vendor Payment Form"
					,"user can use Draft the Outbound Payment - New Vendor Payment Form"
					, "user can use Draft the Outbound Payment - New Vendor Payment Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(4000);
		trackCode= getText(txt_HeaderDocNoA);
		String OPNo =trackCode.replace(" Vendor Payment","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can use Release the Outbound Payment - New Vendor Payment Form"
					,"user can use Release the Outbound Payment - New Vendor Payment Form"
					, "user can use Release the Outbound Payment - New Vendor Payment Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Release the Outbound Payment - New Vendor Payment Form"
					,"user can use Release the Outbound Payment - New Vendor Payment Form"
					, "user can use Release the Outbound Payment - New Vendor Payment Form Fail", "fail");
		}
		
		//===================================================FIN_16.11
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_BankReconciliationBtnA);
		click(btn_BankReconciliationBtnA);
		WaitElement(btn_NewBankReconciliationBtnA);
		click(btn_NewBankReconciliationBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Reconciliation - New")){
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Fail", "fail");
		}
		
		WaitElement(sel_cboxBankBR);
		selectText(sel_cboxBankBR, BankBDSelA);
		WaitElement(sel_cboxBankAccountNoBR);
		selectText(sel_cboxBankAccountNoBR, BankAccountNoBDNewSelA);
		
		ScrollDownAruna();
		
		WaitElement(txt_DocNoSearchBRA);
		sendKeys(txt_DocNoSearchBRA, OPNo);
		pressEnter(txt_DocNoSearchBRA);
		
		WaitClick(sel_DocSelBR.replace("DocNoBR", OPNo));
		click(sel_DocSelBR.replace("DocNoBR", OPNo));
		
		String ReconciliedAmountBR= driver.findElement(By.xpath(txt_ReconciliedAmountBR)).getAttribute("value");
		//System.out.println(ReconciliedAmountBR);
		
		ScrollUpAruna();
		
		WaitElement(txt_StatementBalanceBR);
		sendKeys(txt_StatementBalanceBR, ReconciliedAmountBR.replace("-", ""));
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Bank reconciliation Form"
					,"user can use Draft the Bank reconciliation Form"
					, "user can use Draft the Bank reconciliation Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Bank reconciliation Form"
					,"user can use Draft the Bank reconciliation Form"
					, "user can use Draft the Bank reconciliation Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		BRNo =trackCode;
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verifiy that user can able to create Bank reconciliation using Outbound Payment"
					,"user can able to create Bank reconciliation using Outbound Payment"
					, "user can able to create Bank reconciliation using Outbound Payment Successfully", "pass");
		}else {
			writeTestResults("Verifiy that user can able to create Bank reconciliation using Outbound Payment"
					,"user can able to create Bank reconciliation using Outbound Payment"
					, "user can able to create Bank reconciliation using Outbound Payment Fail", "fail");
		}
		
		//===================================================FIN_16.12
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_BankReconciliationBtnA);
		click(btn_BankReconciliationBtnA);
		WaitElement(btn_NewBankReconciliationBtnA);
		click(btn_NewBankReconciliationBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Reconciliation - New")){
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Fail", "fail");
		}
		
		WaitElement(sel_cboxBankBR);
		selectText(sel_cboxBankBR, BankBDSelA);
		WaitElement(sel_cboxBankAccountNoBR);
		selectText(sel_cboxBankAccountNoBR, BankAccountNoBDNewSelA);
		
		ScrollDownAruna();
		
		WaitElement(txt_DocNoSearchBRA);
		sendKeys(txt_DocNoSearchBRA, OPNo);
		pressEnter(txt_DocNoSearchBRA);
		
		if(!isDisplayed(sel_DocSelBR.replace("DocNoBR", OPNo))){
			writeTestResults("Verify that user don’t have the access to Outbound Payment which is already used in bank reconciliation"
					,"user don’t have the access to Outbound Payment which is already used in bank reconciliation"
					, "user don’t have the access to Outbound Payment which is already used in bank reconciliation Successfully", "pass");
		}else {
			writeTestResults("Verify that user don’t have the access to Outbound Payment which is already used in bank reconciliation"
					,"user don’t have the access to Outbound Payment which is already used in bank reconciliation"
					, "user don’t have the access to Outbound Payment which is already used in bank reconciliation Fail", "fail");
		}
		
		//===================================================FIN_16.13
		
		for (int i = 1; i <= 6; i++) {
			driver.navigate().back();
		}
		
		Thread.sleep(2000);
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ChequeReturnBtnIPA);
		click(btn_ChequeReturnBtnIPA);
		
		WaitElement(sel_ReturnResonSelIPA);
		selectText(sel_ReturnResonSelIPA, "Funds insufficient");
		
		JavascriptExecutor j7 = (JavascriptExecutor)driver;
		j7.executeScript("$('#txtReturnDate').datepicker('setDate', new Date())");
		
		WaitElement(txt_ReturnRemarkTxtIPA);
		sendKeys(txt_ReturnRemarkTxtIPA, "Return CHK");
		
		WaitClick(btn_ReturnBtnIPABtnIPA);
		click(btn_ReturnBtnIPABtnIPA);
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		
		if(!isDisplayed(btn_ChequeReturnBtnIPA)) {
			writeTestResults("Verify that 'Cheque Return' function can be done for Outbound Payment which payment method is cheque used in the bank reconciliation"
					,"'Cheque Return' function can be done for Outbound Payment which payment method is cheque used in the bank reconciliation"
					, "'Cheque Return' function can be done for Outbound Payment which payment method is cheque used in the bank reconciliation Successfully", "pass");
		}else {
			writeTestResults("Verify that 'Cheque Return' function can be done for Outbound Payment which payment method is cheque used in the bank reconciliation"
					,"'Cheque Return' function can be done for Outbound Payment which payment method is cheque used in the bank reconciliation"
					, "'Cheque Return' function can be done for Outbound Payment which payment method is cheque used in the bank reconciliation Fail", "fail");
		}
	}
	
	//FIN_16_14to16_17
	public void VerifyThatOutboundPaymentWhichPaymentMethodIsEFTMethodUsedInTheBankReconciliationCannotReversible() throws Exception {
		
		WaitElement(btn_OutboundPaymentAdviceBtnA);
		click(btn_OutboundPaymentAdviceBtnA);
		WaitElement(btn_NewOutboundPaymentAdviceBtnA);
		click(btn_NewOutboundPaymentAdviceBtnA);
		
		WaitElement(btn_VendorAdvanceJourneyBtnOPA);
		click(btn_VendorAdvanceJourneyBtnOPA);
		
		if(getText(txt_PageHeaderA).contentEquals("Outbound Payment Advice - New Vendor Advance")){
			writeTestResults("Navigation to Outbound Payment Advice - New Vendor Advance page"
					,"User can Navigate Outbound Payment Advice - New Vendor Advance page"
					, "User can Navigate Outbound Payment Advice - New Vendor Advance page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Outbound Payment Advice - New Vendor Advance page"
					,"User can Navigate Outbound Payment Advice - New Vendor Advance page"
					, "User can Navigate Outbound Payment Advice - New Vendor Advance page Fail", "fail");
		}
		
		WaitElement(btn_VendorLookUpBtnOPA);
		click(btn_VendorLookUpBtnOPA);
		WaitElement(txt_VendorLookUpIPATxtA);
		sendKeys(txt_VendorLookUpIPATxtA, VendorLookUpIPATxtA);
		pressEnter(txt_VendorLookUpIPATxtA);
		WaitElement(sel_VendorLookUpIPASelA.replace("VenSel", VendorLookUpIPATxtA));
		doubleClick(sel_VendorLookUpIPASelA.replace("VenSel", VendorLookUpIPATxtA));
		
		WaitElement(txt_AmountTextOPA);
		sendKeys(txt_AmountTextOPA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Outbound Payment Advice Form"
					,"user can use Draft the Outbound Payment Advice Form"
					, "user can use Draft the Outbound Payment Advice Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Outbound Payment Advice Form"
					,"user can use Draft the Outbound Payment Advice Form"
					, "user can use Draft the Outbound Payment Advice Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(4000);
		trackCode= getText(txt_HeaderDocNoA);
		String OPANo =trackCode.replace(" Vendor Advance","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Vendor Advance"
					,"user can release the Vendor Advance"
					, "user can release the Vendor Advance Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Vendor Advance"
					,"user can release the Vendor Advance"
					, "user can release the Vendor Advance Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToOutboundPaymentOPABtnA);
		click(btn_ConvertToOutboundPaymentOPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Outbound Payment - New Vendor Payment")){
			writeTestResults("Navigation to Outbound Payment - New Vendor Payment page"
					,"User can Navigate Outbound Payment - New Vendor Payment page"
					, "User can Navigate Outbound Payment - New Vendor Payment page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Outbound Payment - New Vendor Payment page"
					,"User can Navigate Outbound Payment - New Vendor Payment page"
					, "User can Navigate Outbound Payment - New Vendor Payment page Fail", "fail");
		}
		
		WaitElement(sel_cboxPaybook);
		selectText(sel_cboxPaybook, cboxPaybook2A);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(sel_OPAFlagBtnA.replace("OPANo", OPANo));
		click(sel_OPAFlagBtnA.replace("OPANo", OPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Outbound Payment - New Vendor Payment Form"
					,"user can use Draft the Outbound Payment - New Vendor Payment Form"
					, "user can use Draft the Outbound Payment - New Vendor Payment Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Outbound Payment - New Vendor Payment Form"
					,"user can use Draft the Outbound Payment - New Vendor Payment Form"
					, "user can use Draft the Outbound Payment - New Vendor Payment Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(4000);
		trackCode= getText(txt_HeaderDocNoA);
		String OPNo =trackCode.replace(" Vendor Payment","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can use Release the Outbound Payment - New Vendor Payment Form"
					,"user can use Release the Outbound Payment - New Vendor Payment Form"
					, "user can use Release the Outbound Payment - New Vendor Payment Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Release the Outbound Payment - New Vendor Payment Form"
					,"user can use Release the Outbound Payment - New Vendor Payment Form"
					, "user can use Release the Outbound Payment - New Vendor Payment Form Fail", "fail");
		}
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_BankReconciliationBtnA);
		click(btn_BankReconciliationBtnA);
		WaitElement(btn_NewBankReconciliationBtnA);
		click(btn_NewBankReconciliationBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Reconciliation - New")){
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Fail", "fail");
		}
		
		WaitElement(sel_cboxBankBR);
		selectText(sel_cboxBankBR, BankBDSelA);
		WaitElement(sel_cboxBankAccountNoBR);
		selectText(sel_cboxBankAccountNoBR, BankAccountNoBDNewSelA);
		
		ScrollDownAruna();
		
		WaitElement(txt_DocNoSearchBRA);
		sendKeys(txt_DocNoSearchBRA, OPNo);
		pressEnter(txt_DocNoSearchBRA);
		
		WaitClick(sel_DocSelBR.replace("DocNoBR", OPNo));
		click(sel_DocSelBR.replace("DocNoBR", OPNo));
		
		String ReconciliedAmountBR= driver.findElement(By.xpath(txt_ReconciliedAmountBR)).getAttribute("value");
		//System.out.println(ReconciliedAmountBR);
		
		ScrollUpAruna();
		
		WaitElement(txt_StatementBalanceBR);
		sendKeys(txt_StatementBalanceBR, ReconciliedAmountBR.replace("-", ""));
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Bank reconciliation Form"
					,"user can use Draft the Bank reconciliation Form"
					, "user can use Draft the Bank reconciliation Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Bank reconciliation Form"
					,"user can use Draft the Bank reconciliation Form"
					, "user can use Draft the Bank reconciliation Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		BRNo =trackCode;
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verifiy that user can able to create Bank reconciliation"
					,"user can able to create Bank reconciliation"
					, "user can able to create Bank reconciliation Successfully", "pass");
		}else {
			writeTestResults("Verifiy that user can able to create Bank reconciliation"
					,"user can able to create Bank reconciliation"
					, "user can able to create Bank reconciliation Fail", "fail");
		}
		
		//===================================================FIN_16.14
		
		for (int i = 1; i <= 3; i++) {
			driver.navigate().back();
		}
		
		Thread.sleep(2000);
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		
		if(!isDisplayed(btn_ReverseBtnA)) {
			writeTestResults("Verify that Outbound Payment which payment method is EFT method used in the bank reconciliation cannot reversible"
					,"Outbound Payment which payment method is EFT method used in the bank reconciliation cannot reversible"
					, "Outbound Payment which payment method is EFT method used in the bank reconciliation cannot reversible Successfully", "pass");
		}else {
			writeTestResults("Verify that Outbound Payment which payment method is EFT method used in the bank reconciliation cannot reversible"
					,"Outbound Payment which payment method is EFT method used in the bank reconciliation cannot reversible"
					, "Outbound Payment which payment method is EFT method used in the bank reconciliation cannot reversible Fail", "fail");
		}
		
		//===================================================FIN_16.15
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_BankReconciliationBtnA);
		click(btn_BankReconciliationBtnA);
		
		WaitElement(txt_BankReconciliationByPageSearchTxtA);
		sendKeys(txt_BankReconciliationByPageSearchTxtA, BRNo);
		pressEnter(txt_BankReconciliationByPageSearchTxtA);
		WaitElement(sel_BankReconciliationByPageSearchSelA.replace("BRNo", BRNo));
		doubleClick(sel_BankReconciliationByPageSearchSelA.replace("BRNo", BRNo));
		
		Thread.sleep(2000);
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ReverseBtnA);
		click(btn_ReverseBtnA);
		WaitElement(btn_YesBtnA);
		click(btn_YesBtnA);
		
		WaitElement(txt_PageReverseA);
		if(isDisplayed(txt_PageReverseA)) {
			writeTestResults("Verify that user can reverse the released bank reconciliation"
					,"user can reverse the released bank reconciliation"
					, "user can reverse the released bank reconciliation Successfully", "pass");
		}else {
			writeTestResults("Verify that user can reverse the released bank reconciliation"
					,"user can reverse the released bank reconciliation"
					, "user can reverse the released bank reconciliation Fail", "fail");
		}
		
		//===================================================FIN_16.16
		
		for (int i = 1; i <= 3; i++) {
			driver.navigate().back();
		}
		
		Thread.sleep(2000);
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ReverseBtnA);
		click(btn_ReverseBtnA);
		WaitElement(btn_ReverseLookupBtnA);
		click(btn_ReverseLookupBtnA);
		WaitElement(btn_YesBtnA);
		click(btn_YesBtnA);
		
		WaitElement(txt_PageReverseA);
		if(isDisplayed(txt_PageReverseA)) {
			writeTestResults("Verify the Outbound Payment used in above reversed bank reconciliation can be reversible"
					,"Outbound Payment used in above reversed bank reconciliation can be reversible"
					, "Outbound Payment used in above reversed bank reconciliation can be reversible Successfully", "pass");
		}else {
			writeTestResults("Verify the Outbound Payment used in above reversed bank reconciliation can be reversible"
					,"Outbound Payment used in above reversed bank reconciliation can be reversible"
					, "Outbound Payment used in above reversed bank reconciliation can be reversible Fail", "fail");
		}
		
		//===================================================FIN_16.17
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_BankReconciliationBtnA);
		click(btn_BankReconciliationBtnA);
		WaitElement(btn_NewBankReconciliationBtnA);
		click(btn_NewBankReconciliationBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Reconciliation - New")){
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Fail", "fail");
		}
		
		WaitElement(sel_cboxBankBR);
		selectText(sel_cboxBankBR, BankBDSelA);
		WaitElement(sel_cboxBankAccountNoBR);
		selectText(sel_cboxBankAccountNoBR, BankAccountNoBDNewSelA);
		
		ScrollDownAruna();
		
		WaitElement(txt_DocNoSearchBRA);
		sendKeys(txt_DocNoSearchBRA, OPNo);
		pressEnter(txt_DocNoSearchBRA);
		
		WaitElement(sel_DocSel1BR.replace("DocNoBR", OPNo));
		if(isDisplayed(sel_DocSel1BR.replace("DocNoBR", OPNo)) && isDisplayed(sel_DocSel2BR.replace("DocNoBR", OPNo))){
			writeTestResults("Verify the availablity of reversed Outbound Payment entry"
					,"availablity of reversed Outbound Payment entry"
					, "availablity of reversed Outbound Payment entry Successfully", "pass");
		}else {
			writeTestResults("Verify the availablity of reversed Outbound Payment entry"
					,"availablity of reversed Outbound Payment entry"
					, "availablity of reversed Outbound Payment entry Fail", "fail");
		}
	}
	
	//FIN_16_18to16_23
	public void VerifiyThatUserCanAbleToCreateBankReconciliationUsingBankAdjustment() throws Exception {
		
		WaitElement(btn_BankAdjustmentBtnA);
		click(btn_BankAdjustmentBtnA);
		WaitElement(btn_NewBankAdjustmentBtnA);
		click(btn_NewBankAdjustmentBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Adjustment - New")){
			writeTestResults("Navigation to Bank Adjustment - New page"
					,"User can Navigate Bank Adjustment - New page"
					, "User can Navigate Bank Adjustment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Bank Adjustment - New page"
					,"User can Navigate Bank Adjustment - New page"
					, "User can Navigate Bank Adjustment - New page Fail", "fail");
		}
		
		WaitElement(sel_cboxBankBA);
		selectText(sel_cboxBankBA, BankBDSelA);
		WaitElement(sel_cboxBankAccountNoBA);
		selectText(sel_cboxBankAccountNoBA, BankAccountNoBDNewSelA);
		
		WaitElement(btn_GLAccountLookupBABtnA);
		click(btn_GLAccountLookupBABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_txtAmountBATxtA);
		sendKeys(txt_txtAmountBATxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Bank Adjustment Form"
					,"user can use Draft the Bank Adjustment Form"
					, "user can use Draft the Bank Adjustment Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Bank Adjustment Form"
					,"user can use Draft the Bank Adjustment Form"
					, "user can use Draft the Bank Adjustment Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(4000);
		trackCode= getText(txt_HeaderDocNoA);
		BANo =trackCode;
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can use Release the Bank Adjustment Form"
					,"user can use Release the Bank Adjustment Form"
					, "user can use Release the Bank Adjustment Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Release the Bank Adjustment Form"
					,"user can use Release the Bank Adjustment Form"
					, "user can use Release the Bank Adjustment Form Fail", "fail");
		}
		
		//===================================================FIN_16.18
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_BankReconciliationBtnA);
		click(btn_BankReconciliationBtnA);
		WaitElement(btn_NewBankReconciliationBtnA);
		click(btn_NewBankReconciliationBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Reconciliation - New")){
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Fail", "fail");
		}
		
		WaitElement(sel_cboxBankBR);
		selectText(sel_cboxBankBR, BankBDSelA);
		WaitElement(sel_cboxBankAccountNoBR);
		selectText(sel_cboxBankAccountNoBR, BankAccountNoBDNewSelA);
		
		ScrollDownAruna();
		
		WaitElement(txt_DocNoSearchBRA);
		sendKeys(txt_DocNoSearchBRA, BANo);
		pressEnter(txt_DocNoSearchBRA);
		
		WaitClick(sel_DocSelBR.replace("DocNoBR", BANo));
		click(sel_DocSelBR.replace("DocNoBR", BANo));
		
		String ReconciliedAmountBR= driver.findElement(By.xpath(txt_ReconciliedAmountBR)).getAttribute("value");
		//System.out.println(ReconciliedAmountBR);
		
		ScrollUpAruna();
		
		WaitElement(txt_StatementBalanceBR);
		sendKeys(txt_StatementBalanceBR, ReconciliedAmountBR.replace("-", ""));
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Bank reconciliation Form"
					,"user can use Draft the Bank reconciliation Form"
					, "user can use Draft the Bank reconciliation Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Bank reconciliation Form"
					,"user can use Draft the Bank reconciliation Form"
					, "user can use Draft the Bank reconciliation Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		BRNo =trackCode;
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verifiy that user can able to create Bank reconciliation using Bank Adjustment"
					,"user can able to create Bank reconciliation using Bank Adjustment"
					, "user can able to create Bank reconciliation using Bank Adjustment Successfully", "pass");
		}else {
			writeTestResults("Verifiy that user can able to create Bank reconciliation using Bank Adjustment"
					,"user can able to create Bank reconciliation using Bank Adjustment"
					, "user can able to create Bank reconciliation using Bank Adjustment Fail", "fail");
		}
		
		//===================================================FIN_16.19
		
		for (int i = 1; i <= 3; i++) {
			driver.navigate().back();
		}
		
		Thread.sleep(2000);
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ReverseBtnA);
		click(btn_ReverseBtnA);
		WaitElement(btn_ReverseLookupBtnA);
		click(btn_ReverseLookupBtnA);
		WaitElement(btn_YesBtnA);
		click(btn_YesBtnA);
		
		WaitElement(txt_BankAdjustmentBRValidatorA);
		if(isDisplayed(txt_BankAdjustmentBRValidatorA)) {
			writeTestResults("Verify that Bank adjustment used in the bank reconciliation cannot reversible"
					,"Bank adjustment used in the bank reconciliation cannot reversible"
					, "Bank adjustment used in the bank reconciliation cannot reversible Successfully", "pass");
		}else {
			writeTestResults("Verify that Bank adjustment used in the bank reconciliation cannot reversible"
					,"Bank adjustment used in the bank reconciliation cannot reversible"
					, "Bank adjustment used in the bank reconciliation cannot reversible Fail", "fail");
		}
		
		//===================================================FIN_16.20
		
		pageRefersh();
		
		Thread.sleep(2000);
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_BankReconciliationBtnA);
		click(btn_BankReconciliationBtnA);
		WaitElement(btn_NewBankReconciliationBtnA);
		click(btn_NewBankReconciliationBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Reconciliation - New")){
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Fail", "fail");
		}
		
		WaitElement(sel_cboxBankBR);
		selectText(sel_cboxBankBR, BankBDSelA);
		WaitElement(sel_cboxBankAccountNoBR);
		selectText(sel_cboxBankAccountNoBR, BankAccountNoBDNewSelA);
		
		ScrollDownAruna();
		
		WaitElement(txt_DocNoSearchBRA);
		sendKeys(txt_DocNoSearchBRA, BANo);
		pressEnter(txt_DocNoSearchBRA);
		
		if(!isDisplayed(sel_DocSelBR.replace("DocNoBR", BANo))){
			writeTestResults("Verify that user don’t have the access to bank adjustment which is already used in bank reconciliation"
					,"user don’t have the access to bank adjustment which is already used in bank reconciliation"
					, "user don’t have the access to bank adjustment which is already used in bank reconciliation Successfully", "pass");
		}else {
			writeTestResults("Verify that user don’t have the access to bank adjustment which is already used in bank reconciliation"
					,"user don’t have the access to bank adjustment which is already used in bank reconciliation"
					, "user don’t have the access to bank adjustment which is already used in bank reconciliation Fail", "fail");
		}
		
		//===================================================FIN_16.21
		
		WaitElement(btn_BankReconciliationByPageBtnA);
		click(btn_BankReconciliationByPageBtnA);
		WaitElement(txt_BankReconciliationByPageSearchTxtA);
		sendKeys(txt_BankReconciliationByPageSearchTxtA, BRNo);
		pressEnter(txt_BankReconciliationByPageSearchTxtA);
		WaitElement(sel_BankReconciliationByPageSearchSelA.replace("BRNo", BRNo));
		doubleClick(sel_BankReconciliationByPageSearchSelA.replace("BRNo", BRNo));
		
		Thread.sleep(2000);
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ReverseBtnA);
		click(btn_ReverseBtnA);
		WaitElement(btn_YesBtnA);
		click(btn_YesBtnA);
		
		WaitElement(txt_PageReverseA);
		if(isDisplayed(txt_PageReverseA)) {
			writeTestResults("Verify that user can reverse the released bank reconciliation"
					,"user can reverse the released bank reconciliation"
					, "user can reverse the released bank reconciliation Successfully", "pass");
		}else {
			writeTestResults("Verify that user can reverse the released bank reconciliation"
					,"user can reverse the released bank reconciliation"
					, "user can reverse the released bank reconciliation Fail", "fail");
		}
		
		//===================================================FIN_16.22
		
		for (int i = 1; i <= 4; i++) {
			driver.navigate().back();
		}
		
		Thread.sleep(2000);
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ReverseBtnA);
		click(btn_ReverseBtnA);
		WaitElement(btn_ReverseLookupBtnA);
		click(btn_ReverseLookupBtnA);
		WaitElement(btn_YesBtnA);
		click(btn_YesBtnA);
		
		WaitElement(txt_PageReverseA);
		if(isDisplayed(txt_PageReverseA)) {
			writeTestResults("Verify the bank adjustment used in above reversed bank reconciliation can be reversible"
					,"bank adjustment used in above reversed bank reconciliation can be reversible"
					, "bank adjustment used in above reversed bank reconciliation can be reversible Successfully", "pass");
		}else {
			writeTestResults("Verify the bank adjustment used in above reversed bank reconciliation can be reversible"
					,"bank adjustment used in above reversed bank reconciliation can be reversible"
					, "bank adjustment used in above reversed bank reconciliation can be reversible Fail", "fail");
		}
		
		//===================================================FIN_16_23
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_BankReconciliationBtnA);
		click(btn_BankReconciliationBtnA);
		WaitElement(btn_NewBankReconciliationBtnA);
		click(btn_NewBankReconciliationBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Reconciliation - New")){
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Fail", "fail");
		}
		
		WaitElement(sel_cboxBankBR);
		selectText(sel_cboxBankBR, BankBDSelA);
		WaitElement(sel_cboxBankAccountNoBR);
		selectText(sel_cboxBankAccountNoBR, BankAccountNoBDNewSelA);
		
		ScrollDownAruna();
		
		WaitElement(txt_DocNoSearchBRA);
		sendKeys(txt_DocNoSearchBRA, BANo);
		pressEnter(txt_DocNoSearchBRA);
		
		WaitElement(sel_DocSel1BR.replace("DocNoBR", BANo));
		if(isDisplayed(sel_DocSel1BR.replace("DocNoBR", BANo)) && isDisplayed(sel_DocSel2BR.replace("DocNoBR", BANo))){
			writeTestResults("Verify the availablity of reversed bank adjustment entry"
					,"availablity of reversed bank adjustment entry"
					, "availablity of reversed bank adjustment entry Successfully", "pass");
		}else {
			writeTestResults("Verify the availablity of reversed bank adjustment entry"
					,"availablity of reversed bank adjustment entry"
					, "availablity of reversed bank adjustment entry Fail", "fail");
		}
	}
	
	//FIN_16_24to16_25
	public void VerifiyThatUserCanDeleteaDraftedBankReconciliationUsingInboundPayment() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);
		
		WaitElement(btn_CustomerAdvanceJourneyBtnA);
		click(btn_CustomerAdvanceJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Customer Advance")){
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Fail", "fail");
		}
		
		WaitElement(btn_CustomerLookUpIPABtnA);
		click(btn_CustomerLookUpIPABtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxtA);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		
		WaitElement(txt_AmountFieldIPATxtA);
		sendKeys(txt_AmountFieldIPATxtA, "100");
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(4000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Customer Advance","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Customer Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSel3A);
		
		WaitElement(sel_cboxEFTBankIP);
		selectText(sel_cboxEFTBankIP, BankBDSelA);
		
		WaitElement(sel_cboxEFTBankAccountIP);
		selectText(sel_cboxEFTBankAccountIP, BankAccountNoBDNewSelA);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(4000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPNo = trackCode.replace(" Customer Receipt", "");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can able to create customer receipt voucher using 'EFT' pay method"
					,"user can able to create customer receipt voucher using 'EFT' pay method"
					, "user can able to create customer receipt voucher using 'EFT' pay method Successfully", "pass");
		}else {
			writeTestResults("Verify that user can able to create customer receipt voucher using 'EFT' pay method"
					,"user can able to create customer receipt voucher using 'EFT' pay method"
					, "user can able to create customer receipt voucher using 'EFT' pay method Fail", "fail");
		}
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_BankReconciliationBtnA);
		click(btn_BankReconciliationBtnA);
		WaitElement(btn_NewBankReconciliationBtnA);
		click(btn_NewBankReconciliationBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Reconciliation - New")){
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Fail", "fail");
		}
		
		WaitElement(sel_cboxBankBR);
		selectText(sel_cboxBankBR, BankBDSelA);
		WaitElement(sel_cboxBankAccountNoBR);
		selectText(sel_cboxBankAccountNoBR, BankAccountNoBDNewSelA);
		
		ScrollDownAruna();
		
		WaitElement(txt_DocNoSearchBRA);
		sendKeys(txt_DocNoSearchBRA, IPNo);
		pressEnter(txt_DocNoSearchBRA);
		
		WaitClick(sel_DocSelBR.replace("DocNoBR", IPNo));
		click(sel_DocSelBR.replace("DocNoBR", IPNo));
		
		String ReconciliedAmountBR= driver.findElement(By.xpath(txt_ReconciliedAmountBR)).getAttribute("value");
		//System.out.println(ReconciliedAmountBR);
		
		ScrollUpAruna();
		
		WaitElement(txt_StatementBalanceBR);
		sendKeys(txt_StatementBalanceBR, ReconciliedAmountBR.replace("-", ""));
		
		//===================================================FIN_16.24
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Bank reconciliation Form"
					,"user can use Draft the Bank reconciliation Form"
					, "user can use Draft the Bank reconciliation Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Bank reconciliation Form"
					,"user can use Draft the Bank reconciliation Form"
					, "user can use Draft the Bank reconciliation Form Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_DeleteBtnA);
		click(btn_DeleteBtnA);
		WaitElement(btn_YesBtnA);
		click(btn_YesBtnA);
		
		WaitElement(txt_pageDeleteA);
		if(isDisplayed(txt_pageDeleteA)) {
			writeTestResults("Verifiy that user can Delete a Drafted Bank reconciliation using Inbound Payment"
					,"user can Delete a Drafted Bank reconciliation using Inbound Payment"
					, "user can Delete a Drafted Bank reconciliation using Inbound Payment Successfully", "pass");
		}else {
			writeTestResults("Verifiy that user can Delete a Drafted Bank reconciliation using Inbound Payment"
					,"user can Delete a Drafted Bank reconciliation using Inbound Payment"
					, "user can Delete a Drafted Bank reconciliation using Inbound Payment Fail", "fail");
		}
		
		//===================================================FIN_16.25
		
		for (int i = 1; i <= 3; i++) {
			driver.navigate().back();
		}
		
		Thread.sleep(2000);
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ReverseBtnA);
		click(btn_ReverseBtnA);
		WaitElement(btn_ReverseLookupBtnA);
		click(btn_ReverseLookupBtnA);
		WaitElement(btn_YesBtnA);
		click(btn_YesBtnA);
		
		WaitElement(txt_PageReverseA);
		if(isDisplayed(txt_PageReverseA)) {
			writeTestResults("Verifiy that Inbound Payment used in deleted bank reconciliation can be reverse"
					,"Inbound Payment used in deleted bank reconciliation can be reverse"
					, "Inbound Payment used in deleted bank reconciliation can be reverse Successfully", "pass");
		}else {
			writeTestResults("Verifiy that Inbound Payment used in deleted bank reconciliation can be reverse"
					,"Inbound Payment used in deleted bank reconciliation can be reverse"
					, "Inbound Payment used in deleted bank reconciliation can be reverse Fail", "fail");
		}
	}
	
	//FIN_16_26to16_27
	public void VerifyThatChequeReturnFunctionCanBeDoneForInboundPaymentWhichIsUsedInDeletedBankReconciliation() throws Exception {
		
		WaitElement(btn_InboundPaymentAdviceBtnA);
		click(btn_InboundPaymentAdviceBtnA);
		WaitElement(btn_NewInboundPaymentAdviceBtnA);
		click(btn_NewInboundPaymentAdviceBtnA);
		
		WaitElement(btn_CustomerAdvanceJourneyBtnA);
		click(btn_CustomerAdvanceJourneyBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment Advice - New Customer Advance")){
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment Advice- New page"
					,"User can Navigate Inbound Payment Advice- New page"
					, "User can Navigate Inbound Payment Advice- New page Fail", "fail");
		}
		
		WaitElement(btn_CustomerLookUpIPABtnA);
		click(btn_CustomerLookUpIPABtnA);
		WaitElement(txt_CustomerLookUpTxtA);
		sendKeys(txt_CustomerLookUpTxtA, CustomerLookUpTxtA);
		pressEnter(txt_CustomerLookUpTxtA);
		WaitElement(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		doubleClick(sel_CustomerLookUpSelA.replace("CusAcc", CustomerLookUpTxtA));
		
		WaitElement(txt_AmountFieldIPATxtA);
		sendKeys(txt_AmountFieldIPATxtA, "100");
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment Advice Form"
					,"user can use Draft the Inbound Payment Advice Form"
					, "user can use Draft the Inbound Payment Advice Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(4000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPANo =trackCode.replace(" Customer Advance","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Customer Advance"
					,"user can release the Customer Advance"
					, "user can release the Customer Advance Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToInboundPaymentIPABtnA);
		click(btn_ConvertToInboundPaymentIPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Inbound Payment - New Customer Receipt")){
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Inbound Payment - New page"
					,"User can Navigate Inbound Payment - New page"
					, "User can Navigate Inbound Payment - New page Fail", "fail");
		}
		
		WaitElement(sel_PaymentMethodSelA);
		selectText(sel_PaymentMethodSelA, PaymentMethodSel2A);
		
		WaitElement(sel_BankNameIPSelA);
		selectText(sel_BankNameIPSelA, BankNameIPSelA);
		
		WaitElement(txt_ChequeNoIPTxtA);
		LocalTime Obj1 = LocalTime.now();
		sendKeys(txt_ChequeNoIPTxtA, "CHK"+Obj1);
		
		WaitElement(sel_PaidCurrencySelA);
		selectText(sel_PaidCurrencySelA, PaidCurrencySelA);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		click(btn_IPAFlagBtnA.replace("IPANo", IPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Inbound Payment - New Customer Receipt Form"
					,"user can use Draft the Inbound Payment - New Customer Receipt Form"
					, "user can use Draft the Inbound Payment - New Customer Receipt Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(4000);
		trackCode= getText(txt_HeaderDocNoA);
		String IPNo = trackCode.replace(" Customer Receipt", "");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can able to create customer receipt voucher using 'Cheque' pay method"
					,"user can able to create customer receipt voucher using 'Cheque' pay method"
					, "user can able to create customer receipt voucher using 'Cheque' pay method Successfully", "pass");
		}else {
			writeTestResults("Verify that user can able to create customer receipt voucher using 'Cheque' pay method"
					,"user can able to create customer receipt voucher using 'Cheque' pay method"
					, "user can able to create customer receipt voucher using 'Cheque' pay method Fail", "fail");
		}
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_BankDepositBtnA);
		click(btn_BankDepositBtnA);
		WaitElement(btn_NewBankDepositBtnA);
		click(btn_NewBankDepositBtnA);
		
		WaitElement(sel_PayMethodBDSelA);
		selectText(sel_PayMethodBDSelA, PaymentMethodSel2A);
		
		WaitElement(sel_BankBDSelA);
		selectText(sel_BankBDSelA, BankBDSelA);
		
		WaitElement(sel_BankAccountNoBDSelA);
		selectText(sel_BankAccountNoBDSelA, BankAccountNoBDNewSelA);
		
		WaitElement(btn_SortByBDBtnA);
		selectText(btn_SortByBDBtnA, "Cheque No");
		
		WaitElement(txt_CheqNoBDTxtA);
		sendKeys(txt_CheqNoBDTxtA, "CHK"+Obj1);
		
		WaitClick(btn_ViewBDBtnA);
		click(btn_ViewBDBtnA);
		
		ScrollDownAruna();
		
		WaitElement(btn_DolSelBDBtnA.replace("IPNo", IPNo));
		Thread.sleep(4000);
		click(btn_DolSelBDBtnA.replace("IPNo", IPNo));
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Bank Deposit Form"
					,"user can use Draft the Bank Deposit Form"
					, "user can use Draft the Bank Deposit Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Bank Deposit Form"
					,"user can use Draft the Bank Deposit Form"
					, "user can use Draft the Bank Deposit Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(4000);
		trackCode= getText(txt_HeaderDocNoA);
		String BDNo =trackCode;
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can use Release the Bank Deposit Form"
					,"user can use Release the Bank Deposit Form"
					, "user can use Release the Bank Deposit Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Release the Bank Deposit Form"
					,"user can use Release the Bank Deposit Form"
					, "user can use Release the Bank Deposit Form Fail", "fail");
		}
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_BankReconciliationBtnA);
		click(btn_BankReconciliationBtnA);
		WaitElement(btn_NewBankReconciliationBtnA);
		click(btn_NewBankReconciliationBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Reconciliation - New")){
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Fail", "fail");
		}
		
		WaitElement(sel_cboxBankBR);
		selectText(sel_cboxBankBR, BankBDSelA);
		WaitElement(sel_cboxBankAccountNoBR);
		selectText(sel_cboxBankAccountNoBR, BankAccountNoBDNewSelA);
		
		ScrollDownAruna();
		
		WaitElement(txt_DocNoSearchBRA);
		sendKeys(txt_DocNoSearchBRA, BDNo);
		pressEnter(txt_DocNoSearchBRA);
		
		WaitClick(sel_DocSelBR.replace("DocNoBR", "CHK"+Obj1));
		click(sel_DocSelBR.replace("DocNoBR", "CHK"+Obj1));
		
		String ReconciliedAmountBR= driver.findElement(By.xpath(txt_ReconciliedAmountBR)).getAttribute("value");
		//System.out.println(ReconciliedAmountBR);
		
		ScrollUpAruna();
		
		WaitElement(txt_StatementBalanceBR);
		sendKeys(txt_StatementBalanceBR, ReconciliedAmountBR.replace("-", ""));
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Bank reconciliation Form"
					,"user can use Draft the Bank reconciliation Form"
					, "user can use Draft the Bank reconciliation Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Bank reconciliation Form"
					,"user can use Draft the Bank reconciliation Form"
					, "user can use Draft the Bank reconciliation Form Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_DeleteBtnA);
		click(btn_DeleteBtnA);
		WaitElement(btn_YesBtnA);
		click(btn_YesBtnA);
		
		WaitElement(txt_pageDeleteA);
		if(isDisplayed(txt_pageDeleteA)) {
			writeTestResults("Verifiy that user can Delete a Drafted Bank reconciliation using Inbound Payment"
					,"user can Delete a Drafted Bank reconciliation using Inbound Payment"
					, "user can Delete a Drafted Bank reconciliation using Inbound Payment Successfully", "pass");
		}else {
			writeTestResults("Verifiy that user can Delete a Drafted Bank reconciliation using Inbound Payment"
					,"user can Delete a Drafted Bank reconciliation using Inbound Payment"
					, "user can Delete a Drafted Bank reconciliation using Inbound Payment Fail", "fail");
		}
		
		//===================================================FIN_16.27
		
		for (int i = 1; i <= 3; i++) {
			driver.navigate().back();
		}
		
		Thread.sleep(2000);
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ReverseBtnA);
		click(btn_ReverseBtnA);
		WaitElement(btn_ReverseLookupBtnA);
		click(btn_ReverseLookupBtnA);
		WaitElement(btn_YesBtnA);
		click(btn_YesBtnA);
		
		WaitElement(txt_PageReverseA);
		if(isDisplayed(txt_PageReverseA)) {
			writeTestResults("Verifiy that Bank deposit used in deleted bank reconciliation can be reverse"
					,"Bank deposit used in deleted bank reconciliation can be reverse"
					, "Bank deposit used in deleted bank reconciliation can be reverse Successfully", "pass");
		}else {
			writeTestResults("Verifiy that Bank deposit used in deleted bank reconciliation can be reverse"
					,"Bank deposit used in deleted bank reconciliation can be reverse"
					, "Bank deposit used in deleted bank reconciliation can be reverse Fail", "fail");
		}
		
		//===================================================FIN_16.26
		
		for (int i = 1; i <= 5; i++) {
			driver.navigate().back();
		}
		
		Thread.sleep(2000);
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		
		if(!isDisplayed(btn_ChequeReturnBtnIPA)) {
			writeTestResults("Verify that 'Cheque Return' function can be done for Inbound Payment which is used in deleted bank reconciliation"
					,"'Cheque Return' function can be done for Inbound Payment which is used in deleted bank reconciliation"
					, "'Cheque Return' function can be done for Inbound Payment which is used in deleted bank reconciliation Successfully", "pass");
		}else {
			writeTestResults("Verify that 'Cheque Return' function can be done for Inbound Payment which is used in deleted bank reconciliation"
					,"'Cheque Return' function can be done for Inbound Payment which is used in deleted bank reconciliation"
					, "'Cheque Return' function can be done for Inbound Payment which is used in deleted bank reconciliation Fail", "fail");
		}
	}
	
	//FIN_16_28to16_29
	public void VerifiyThatUserCanDeleteaDraftedBankReconciliationUsingOutboundPayment() throws Exception {
		
		WaitElement(btn_OutboundPaymentAdviceBtnA);
		click(btn_OutboundPaymentAdviceBtnA);
		WaitElement(btn_NewOutboundPaymentAdviceBtnA);
		click(btn_NewOutboundPaymentAdviceBtnA);
		
		WaitElement(btn_VendorAdvanceJourneyBtnOPA);
		click(btn_VendorAdvanceJourneyBtnOPA);
		
		if(getText(txt_PageHeaderA).contentEquals("Outbound Payment Advice - New Vendor Advance")){
			writeTestResults("Navigation to Outbound Payment Advice - New Vendor Advance page"
					,"User can Navigate Outbound Payment Advice - New Vendor Advance page"
					, "User can Navigate Outbound Payment Advice - New Vendor Advance page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Outbound Payment Advice - New Vendor Advance page"
					,"User can Navigate Outbound Payment Advice - New Vendor Advance page"
					, "User can Navigate Outbound Payment Advice - New Vendor Advance page Fail", "fail");
		}
		
		WaitElement(btn_VendorLookUpBtnOPA);
		click(btn_VendorLookUpBtnOPA);
		WaitElement(txt_VendorLookUpIPATxtA);
		sendKeys(txt_VendorLookUpIPATxtA, VendorLookUpIPATxtA);
		pressEnter(txt_VendorLookUpIPATxtA);
		WaitElement(sel_VendorLookUpIPASelA.replace("VenSel", VendorLookUpIPATxtA));
		doubleClick(sel_VendorLookUpIPASelA.replace("VenSel", VendorLookUpIPATxtA));
		
		WaitElement(txt_AmountTextOPA);
		sendKeys(txt_AmountTextOPA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Outbound Payment Advice Form"
					,"user can use Draft the Outbound Payment Advice Form"
					, "user can use Draft the Outbound Payment Advice Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Outbound Payment Advice Form"
					,"user can use Draft the Outbound Payment Advice Form"
					, "user can use Draft the Outbound Payment Advice Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(4000);
		trackCode= getText(txt_HeaderDocNoA);
		String OPANo =trackCode.replace(" Vendor Advance","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Vendor Advance"
					,"user can release the Vendor Advance"
					, "user can release the Vendor Advance Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Vendor Advance"
					,"user can release the Vendor Advance"
					, "user can release the Vendor Advance Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToOutboundPaymentOPABtnA);
		click(btn_ConvertToOutboundPaymentOPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Outbound Payment - New Vendor Payment")){
			writeTestResults("Navigation to Outbound Payment - New Vendor Payment page"
					,"User can Navigate Outbound Payment - New Vendor Payment page"
					, "User can Navigate Outbound Payment - New Vendor Payment page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Outbound Payment - New Vendor Payment page"
					,"User can Navigate Outbound Payment - New Vendor Payment page"
					, "User can Navigate Outbound Payment - New Vendor Payment page Fail", "fail");
		}
		
		WaitElement(sel_cboxPaybook);
		selectText(sel_cboxPaybook, cboxPaybook2A);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(sel_OPAFlagBtnA.replace("OPANo", OPANo));
		click(sel_OPAFlagBtnA.replace("OPANo", OPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Outbound Payment - New Vendor Payment Form"
					,"user can use Draft the Outbound Payment - New Vendor Payment Form"
					, "user can use Draft the Outbound Payment - New Vendor Payment Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Outbound Payment - New Vendor Payment Form"
					,"user can use Draft the Outbound Payment - New Vendor Payment Form"
					, "user can use Draft the Outbound Payment - New Vendor Payment Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(4000);
		trackCode= getText(txt_HeaderDocNoA);
		String OPNo =trackCode.replace(" Vendor Payment","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can use Release the Outbound Payment - New Vendor Payment Form"
					,"user can use Release the Outbound Payment - New Vendor Payment Form"
					, "user can use Release the Outbound Payment - New Vendor Payment Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Release the Outbound Payment - New Vendor Payment Form"
					,"user can use Release the Outbound Payment - New Vendor Payment Form"
					, "user can use Release the Outbound Payment - New Vendor Payment Form Fail", "fail");
		}
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_BankReconciliationBtnA);
		click(btn_BankReconciliationBtnA);
		WaitElement(btn_NewBankReconciliationBtnA);
		click(btn_NewBankReconciliationBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Reconciliation - New")){
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Fail", "fail");
		}
		
		WaitElement(sel_cboxBankBR);
		selectText(sel_cboxBankBR, BankBDSelA);
		WaitElement(sel_cboxBankAccountNoBR);
		selectText(sel_cboxBankAccountNoBR, BankAccountNoBDNewSelA);
		
		ScrollDownAruna();
		
		WaitElement(txt_DocNoSearchBRA);
		sendKeys(txt_DocNoSearchBRA, OPNo);
		pressEnter(txt_DocNoSearchBRA);
		
		WaitClick(sel_DocSelBR.replace("DocNoBR", OPNo));
		click(sel_DocSelBR.replace("DocNoBR", OPNo));
		
		String ReconciliedAmountBR= driver.findElement(By.xpath(txt_ReconciliedAmountBR)).getAttribute("value");
		//System.out.println(ReconciliedAmountBR);
		
		ScrollUpAruna();
		
		WaitElement(txt_StatementBalanceBR);
		sendKeys(txt_StatementBalanceBR, ReconciliedAmountBR.replace("-", ""));
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Bank reconciliation Form"
					,"user can use Draft the Bank reconciliation Form"
					, "user can use Draft the Bank reconciliation Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Bank reconciliation Form"
					,"user can use Draft the Bank reconciliation Form"
					, "user can use Draft the Bank reconciliation Form Fail", "fail");
		}
		
		//===================================================FIN_16.28
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_DeleteBtnA);
		click(btn_DeleteBtnA);
		WaitElement(btn_YesBtnA);
		click(btn_YesBtnA);
		
		WaitElement(txt_pageDeleteA);
		if(isDisplayed(txt_pageDeleteA)) {
			writeTestResults("Verifiy that user can Delete a Drafted Bank reconciliation using Outbound Payment"
					,"user can Delete a Drafted Bank reconciliation using Outbound Payment"
					, "user can Delete a Drafted Bank reconciliation using Outbound Payment Successfully", "pass");
		}else {
			writeTestResults("Verifiy that user can Delete a Drafted Bank reconciliation using Outbound Payment"
					,"user can Delete a Drafted Bank reconciliation using Outbound Payment"
					, "user can Delete a Drafted Bank reconciliation using Outbound Payment Fail", "fail");
		}
		
		//===================================================FIN_16.29
		
		for (int i = 1; i <= 3; i++) {
			driver.navigate().back();
		}
		
		Thread.sleep(2000);
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ReverseBtnA);
		click(btn_ReverseBtnA);
		WaitElement(btn_ReverseLookupBtnA);
		click(btn_ReverseLookupBtnA);
		WaitElement(btn_YesBtnA);
		click(btn_YesBtnA);
		
		WaitElement(txt_PageReverseA);
		if(isDisplayed(txt_PageReverseA)) {
			writeTestResults("Verifiy that Outbound Payment used in deleted bank reconciliation can be reverse"
					,"Outbound Payment used in deleted bank reconciliation can be reverse"
					, "Outbound Payment used in deleted bank reconciliation can be reverse Successfully", "pass");
		}else {
			writeTestResults("Verifiy that Outbound Payment used in deleted bank reconciliation can be reverse"
					,"Outbound Payment used in deleted bank reconciliation can be reverse"
					, "Outbound Payment used in deleted bank reconciliation can be reverse Fail", "fail");
		}
	}
	
	//FIN_16_30
	public void VerifyThatChequeReturnFunctionCanBeDoneForOutboundPaymentWhichIsUsedInDeletedBankReconciliation() throws Exception {
		
		WaitElement(btn_OutboundPaymentAdviceBtnA);
		click(btn_OutboundPaymentAdviceBtnA);
		WaitElement(btn_NewOutboundPaymentAdviceBtnA);
		click(btn_NewOutboundPaymentAdviceBtnA);
		
		WaitElement(btn_VendorAdvanceJourneyBtnOPA);
		click(btn_VendorAdvanceJourneyBtnOPA);
		
		if(getText(txt_PageHeaderA).contentEquals("Outbound Payment Advice - New Vendor Advance")){
			writeTestResults("Navigation to Outbound Payment Advice - New Vendor Advance page"
					,"User can Navigate Outbound Payment Advice - New Vendor Advance page"
					, "User can Navigate Outbound Payment Advice - New Vendor Advance page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Outbound Payment Advice - New Vendor Advance page"
					,"User can Navigate Outbound Payment Advice - New Vendor Advance page"
					, "User can Navigate Outbound Payment Advice - New Vendor Advance page Fail", "fail");
		}
		
		WaitElement(btn_VendorLookUpBtnOPA);
		click(btn_VendorLookUpBtnOPA);
		WaitElement(txt_VendorLookUpIPATxtA);
		sendKeys(txt_VendorLookUpIPATxtA, VendorLookUpIPATxtA);
		pressEnter(txt_VendorLookUpIPATxtA);
		WaitElement(sel_VendorLookUpIPASelA.replace("VenSel", VendorLookUpIPATxtA));
		doubleClick(sel_VendorLookUpIPASelA.replace("VenSel", VendorLookUpIPATxtA));
		
		WaitElement(txt_AmountTextOPA);
		sendKeys(txt_AmountTextOPA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Outbound Payment Advice Form"
					,"user can use Draft the Outbound Payment Advice Form"
					, "user can use Draft the Outbound Payment Advice Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Outbound Payment Advice Form"
					,"user can use Draft the Outbound Payment Advice Form"
					, "user can use Draft the Outbound Payment Advice Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(4000);
		trackCode= getText(txt_HeaderDocNoA);
		String OPANo =trackCode.replace(" Vendor Advance","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can release the Vendor Advance"
					,"user can release the Vendor Advance"
					, "user can release the Vendor Advance Successfully", "pass");
		}else {
			writeTestResults("Verify that user can release the Vendor Advance"
					,"user can release the Vendor Advance"
					, "user can release the Vendor Advance Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ConvertToOutboundPaymentOPABtnA);
		click(btn_ConvertToOutboundPaymentOPABtnA);
		
		WaitElement(txt_PageHeaderA);
		if(getText(txt_PageHeaderA).contentEquals("Outbound Payment - New Vendor Payment")){
			writeTestResults("Navigation to Outbound Payment - New Vendor Payment page"
					,"User can Navigate Outbound Payment - New Vendor Payment page"
					, "User can Navigate Outbound Payment - New Vendor Payment page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Outbound Payment - New Vendor Payment page"
					,"User can Navigate Outbound Payment - New Vendor Payment page"
					, "User can Navigate Outbound Payment - New Vendor Payment page Fail", "fail");
		}
		
		WaitElement(sel_cboxPaybook);
		selectText(sel_cboxPaybook, cboxPaybook1A);
		
		WaitElement(btn_DetailsTabIPBtnA);
		click(btn_DetailsTabIPBtnA);
		
		WaitElement(sel_OPAFlagBtnA.replace("OPANo", OPANo));
		click(sel_OPAFlagBtnA.replace("OPANo", OPANo));
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Outbound Payment - New Vendor Payment Form"
					,"user can use Draft the Outbound Payment - New Vendor Payment Form"
					, "user can use Draft the Outbound Payment - New Vendor Payment Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Outbound Payment - New Vendor Payment Form"
					,"user can use Draft the Outbound Payment - New Vendor Payment Form"
					, "user can use Draft the Outbound Payment - New Vendor Payment Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(4000);
		trackCode= getText(txt_HeaderDocNoA);
		String OPNo =trackCode.replace(" Vendor Payment","");
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can use Release the Outbound Payment - New Vendor Payment Form"
					,"user can use Release the Outbound Payment - New Vendor Payment Form"
					, "user can use Release the Outbound Payment - New Vendor Payment Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Release the Outbound Payment - New Vendor Payment Form"
					,"user can use Release the Outbound Payment - New Vendor Payment Form"
					, "user can use Release the Outbound Payment - New Vendor Payment Form Fail", "fail");
		}
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_BankReconciliationBtnA);
		click(btn_BankReconciliationBtnA);
		WaitElement(btn_NewBankReconciliationBtnA);
		click(btn_NewBankReconciliationBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Reconciliation - New")){
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Fail", "fail");
		}
		
		WaitElement(sel_cboxBankBR);
		selectText(sel_cboxBankBR, BankBDSelA);
		WaitElement(sel_cboxBankAccountNoBR);
		selectText(sel_cboxBankAccountNoBR, BankAccountNoBDNewSelA);
		
		ScrollDownAruna();
		
		WaitElement(txt_DocNoSearchBRA);
		sendKeys(txt_DocNoSearchBRA, OPNo);
		pressEnter(txt_DocNoSearchBRA);
		
		WaitClick(sel_DocSelBR.replace("DocNoBR", OPNo));
		click(sel_DocSelBR.replace("DocNoBR", OPNo));
		
		String ReconciliedAmountBR= driver.findElement(By.xpath(txt_ReconciliedAmountBR)).getAttribute("value");
		//System.out.println(ReconciliedAmountBR);
		
		ScrollUpAruna();
		
		WaitElement(txt_StatementBalanceBR);
		sendKeys(txt_StatementBalanceBR, ReconciliedAmountBR.replace("-", ""));
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Bank reconciliation Form"
					,"user can use Draft the Bank reconciliation Form"
					, "user can use Draft the Bank reconciliation Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Bank reconciliation Form"
					,"user can use Draft the Bank reconciliation Form"
					, "user can use Draft the Bank reconciliation Form Fail", "fail");
		}
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_DeleteBtnA);
		click(btn_DeleteBtnA);
		WaitElement(btn_YesBtnA);
		click(btn_YesBtnA);
		
		WaitElement(txt_pageDeleteA);
		if(isDisplayed(txt_pageDeleteA)) {
			writeTestResults("Verifiy that user can Delete a Drafted Bank reconciliation using Outbound Payment"
					,"user can Delete a Drafted Bank reconciliation using Outbound Payment"
					, "user can Delete a Drafted Bank reconciliation using Outbound Payment Successfully", "pass");
		}else {
			writeTestResults("Verifiy that user can Delete a Drafted Bank reconciliation using Outbound Payment"
					,"user can Delete a Drafted Bank reconciliation using Outbound Payment"
					, "user can Delete a Drafted Bank reconciliation using Outbound Payment Fail", "fail");
		}
		
		//===================================================FIN_16.30
		
		for (int i = 1; i <= 3; i++) {
			driver.navigate().back();
		}
		
		Thread.sleep(2000);
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ChequeReturnBtnIPA);
		click(btn_ChequeReturnBtnIPA);
		
		WaitElement(sel_ReturnResonSelIPA);
		selectText(sel_ReturnResonSelIPA, "Funds insufficient");
		
		JavascriptExecutor j7 = (JavascriptExecutor)driver;
		j7.executeScript("$('#txtReturnDate').datepicker('setDate', new Date())");
		
		WaitElement(txt_ReturnRemarkTxtIPA);
		sendKeys(txt_ReturnRemarkTxtIPA, "Return CHK");
		
		WaitClick(btn_ReturnBtnIPABtnIPA);
		click(btn_ReturnBtnIPABtnIPA);
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		
		if(!isDisplayed(btn_ChequeReturnBtnIPA)) {
			writeTestResults("Verify that 'Cheque Return' function can be done for Outbound Payment which is used in deleted bank reconciliation"
					,"'Cheque Return' function can be done for Outbound Payment which is used in deleted bank reconciliation"
					, "'Cheque Return' function can be done for Outbound Payment which is used in deleted bank reconciliation Successfully", "pass");
		}else {
			writeTestResults("Verify that 'Cheque Return' function can be done for Outbound Payment which is used in deleted bank reconciliation"
					,"'Cheque Return' function can be done for Outbound Payment which is used in deleted bank reconciliation"
					, "'Cheque Return' function can be done for Outbound Payment which is used in deleted bank reconciliation Fail", "fail");
		}
	}
	
	//FIN_16_31to16_32
	public void VerifiyThatUserCanDeleteaDraftedBankReconciliationUsingBankAdjustment() throws Exception {
		
		WaitElement(btn_BankAdjustmentBtnA);
		click(btn_BankAdjustmentBtnA);
		WaitElement(btn_NewBankAdjustmentBtnA);
		click(btn_NewBankAdjustmentBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Adjustment - New")){
			writeTestResults("Navigation to Bank Adjustment - New page"
					,"User can Navigate Bank Adjustment - New page"
					, "User can Navigate Bank Adjustment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Bank Adjustment - New page"
					,"User can Navigate Bank Adjustment - New page"
					, "User can Navigate Bank Adjustment - New page Fail", "fail");
		}
		
		WaitElement(sel_cboxBankBA);
		selectText(sel_cboxBankBA, BankBDSelA);
		WaitElement(sel_cboxBankAccountNoBA);
		selectText(sel_cboxBankAccountNoBA, BankAccountNoBDNewSelA);
		
		WaitElement(btn_GLAccountLookupBABtnA);
		click(btn_GLAccountLookupBABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_txtAmountBATxtA);
		sendKeys(txt_txtAmountBATxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Bank Adjustment Form"
					,"user can use Draft the Bank Adjustment Form"
					, "user can use Draft the Bank Adjustment Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Bank Adjustment Form"
					,"user can use Draft the Bank Adjustment Form"
					, "user can use Draft the Bank Adjustment Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(4000);
		trackCode= getText(txt_HeaderDocNoA);
		BANo =trackCode;
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can use Release the Bank Adjustment Form"
					,"user can use Release the Bank Adjustment Form"
					, "user can use Release the Bank Adjustment Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Release the Bank Adjustment Form"
					,"user can use Release the Bank Adjustment Form"
					, "user can use Release the Bank Adjustment Form Fail", "fail");
		}
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_BankReconciliationBtnA);
		click(btn_BankReconciliationBtnA);
		WaitElement(btn_NewBankReconciliationBtnA);
		click(btn_NewBankReconciliationBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Reconciliation - New")){
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Fail", "fail");
		}
		
		WaitElement(sel_cboxBankBR);
		selectText(sel_cboxBankBR, BankBDSelA);
		WaitElement(sel_cboxBankAccountNoBR);
		selectText(sel_cboxBankAccountNoBR, BankAccountNoBDNewSelA);
		
		ScrollDownAruna();
		
		WaitElement(txt_DocNoSearchBRA);
		sendKeys(txt_DocNoSearchBRA, BANo);
		pressEnter(txt_DocNoSearchBRA);
		
		WaitClick(sel_DocSelBR.replace("DocNoBR", BANo));
		click(sel_DocSelBR.replace("DocNoBR", BANo));
		
		String ReconciliedAmountBR= driver.findElement(By.xpath(txt_ReconciliedAmountBR)).getAttribute("value");
		//System.out.println(ReconciliedAmountBR);
		
		ScrollUpAruna();
		
		WaitElement(txt_StatementBalanceBR);
		sendKeys(txt_StatementBalanceBR, ReconciliedAmountBR.replace("-", ""));
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Bank reconciliation Form"
					,"user can use Draft the Bank reconciliation Form"
					, "user can use Draft the Bank reconciliation Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Bank reconciliation Form"
					,"user can use Draft the Bank reconciliation Form"
					, "user can use Draft the Bank reconciliation Form Fail", "fail");
		}
		
		//===================================================FIN_16.31
		

		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_DeleteBtnA);
		click(btn_DeleteBtnA);
		WaitElement(btn_YesBtnA);
		click(btn_YesBtnA);
		
		WaitElement(txt_pageDeleteA);
		if(isDisplayed(txt_pageDeleteA)) {
			writeTestResults("Verifiy that user can Delete a Drafted Bank reconciliation using Bank Adjustment"
					,"user can Delete a Drafted Bank reconciliation using Bank Adjustment"
					, "user can Delete a Drafted Bank reconciliation using Bank Adjustment Successfully", "pass");
		}else {
			writeTestResults("Verifiy that user can Delete a Drafted Bank reconciliation using Bank Adjustment"
					,"user can Delete a Drafted Bank reconciliation using Bank Adjustment"
					, "user can Delete a Drafted Bank reconciliation using Bank Adjustment Fail", "fail");
		}
		
		//===================================================FIN_16.32
		
		for (int i = 1; i <= 3; i++) {
			driver.navigate().back();
		}
		
		Thread.sleep(2000);
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ReverseBtnA);
		click(btn_ReverseBtnA);
		WaitElement(btn_ReverseLookupBtnA);
		click(btn_ReverseLookupBtnA);
		WaitElement(btn_YesBtnA);
		click(btn_YesBtnA);
		
		WaitElement(txt_PageReverseA);
		if(isDisplayed(txt_PageReverseA)) {
			writeTestResults("Verifiy that bank adjustment used in deleted bank reconciliation can be reverse"
					,"bank adjustment used in deleted bank reconciliation can be reverse"
					, "bank adjustment used in deleted bank reconciliation can be reverse Successfully", "pass");
		}else {
			writeTestResults("Verifiy that bank adjustment used in deleted bank reconciliation can be reverse"
					,"bank adjustment used in deleted bank reconciliation can be reverse"
					, "bank adjustment used in deleted bank reconciliation can be reverse Fail", "fail");
		}
	}
	
	//FIN_16_33
	public void VerifyThatUserCanEditDraftedBankReconciliationUpdate() throws Exception {
		
		WaitElement(btn_BankAdjustmentBtnA);
		click(btn_BankAdjustmentBtnA);
		WaitElement(btn_NewBankAdjustmentBtnA);
		click(btn_NewBankAdjustmentBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Adjustment - New")){
			writeTestResults("Navigation to Bank Adjustment - New page"
					,"User can Navigate Bank Adjustment - New page"
					, "User can Navigate Bank Adjustment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Bank Adjustment - New page"
					,"User can Navigate Bank Adjustment - New page"
					, "User can Navigate Bank Adjustment - New page Fail", "fail");
		}
		
		WaitElement(sel_cboxBankBA);
		selectText(sel_cboxBankBA, BankBDSelA);
		WaitElement(sel_cboxBankAccountNoBA);
		selectText(sel_cboxBankAccountNoBA, BankAccountNoBDNewSelA);
		
		WaitElement(btn_GLAccountLookupBABtnA);
		click(btn_GLAccountLookupBABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_txtAmountBATxtA);
		sendKeys(txt_txtAmountBATxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Bank Adjustment Form"
					,"user can use Draft the Bank Adjustment Form"
					, "user can use Draft the Bank Adjustment Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Bank Adjustment Form"
					,"user can use Draft the Bank Adjustment Form"
					, "user can use Draft the Bank Adjustment Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(4000);
		trackCode= getText(txt_HeaderDocNoA);
		String BANo1 =trackCode;
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can use Release the Bank Adjustment Form"
					,"user can use Release the Bank Adjustment Form"
					, "user can use Release the Bank Adjustment Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Release the Bank Adjustment Form"
					,"user can use Release the Bank Adjustment Form"
					, "user can use Release the Bank Adjustment Form Fail", "fail");
		}
		
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_BankAdjustmentBtnA);
		click(btn_BankAdjustmentBtnA);
		WaitElement(btn_NewBankAdjustmentBtnA);
		click(btn_NewBankAdjustmentBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Adjustment - New")){
			writeTestResults("Navigation to Bank Adjustment - New page"
					,"User can Navigate Bank Adjustment - New page"
					, "User can Navigate Bank Adjustment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Bank Adjustment - New page"
					,"User can Navigate Bank Adjustment - New page"
					, "User can Navigate Bank Adjustment - New page Fail", "fail");
		}
		
		WaitElement(sel_cboxBankBA);
		selectText(sel_cboxBankBA, BankBDSelA);
		WaitElement(sel_cboxBankAccountNoBA);
		selectText(sel_cboxBankAccountNoBA, BankAccountNoBDNewSelA);
		
		WaitElement(btn_GLAccountLookupBABtnA);
		click(btn_GLAccountLookupBABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_txtAmountBATxtA);
		sendKeys(txt_txtAmountBATxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Bank Adjustment Form"
					,"user can use Draft the Bank Adjustment Form"
					, "user can use Draft the Bank Adjustment Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Bank Adjustment Form"
					,"user can use Draft the Bank Adjustment Form"
					, "user can use Draft the Bank Adjustment Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(4000);
		trackCode= getText(txt_HeaderDocNoA);
		String BANo2 =trackCode;
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can use Release the Bank Adjustment Form"
					,"user can use Release the Bank Adjustment Form"
					, "user can use Release the Bank Adjustment Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Release the Bank Adjustment Form"
					,"user can use Release the Bank Adjustment Form"
					, "user can use Release the Bank Adjustment Form Fail", "fail");
		}
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_BankReconciliationBtnA);
		click(btn_BankReconciliationBtnA);
		WaitElement(btn_NewBankReconciliationBtnA);
		click(btn_NewBankReconciliationBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Reconciliation - New")){
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Fail", "fail");
		}
		
		WaitElement(sel_cboxBankBR);
		selectText(sel_cboxBankBR, BankBDSelA);
		WaitElement(sel_cboxBankAccountNoBR);
		selectText(sel_cboxBankAccountNoBR, BankAccountNoBDNewSelA);
		
		ScrollDownAruna();
		
		WaitElement(txt_DocNoSearchBRA);
		sendKeys(txt_DocNoSearchBRA, BANo1);
		pressEnter(txt_DocNoSearchBRA);
		
		WaitClick(sel_DocSelBR.replace("DocNoBR", BANo1));
		click(sel_DocSelBR.replace("DocNoBR", BANo1));
		
		String ReconciliedAmountBR= driver.findElement(By.xpath(txt_ReconciliedAmountBR)).getAttribute("value");
		//System.out.println(ReconciliedAmountBR);
		
		ScrollUpAruna();
		
		WaitElement(txt_StatementBalanceBR);
		sendKeys(txt_StatementBalanceBR, ReconciliedAmountBR.replace("-", ""));
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Bank reconciliation Form"
					,"user can use Draft the Bank reconciliation Form"
					, "user can use Draft the Bank reconciliation Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Bank reconciliation Form"
					,"user can use Draft the Bank reconciliation Form"
					, "user can use Draft the Bank reconciliation Form Fail", "fail");
		}
		
		WaitElement(txt_BankAdjustmentGrideValueA.replace("BANo", BANo1));
		String BankAdjustmentGrideValue1A=getText(txt_BankAdjustmentGrideValueA.replace("BANo", BANo1));
		
		
		//===================================================FIN_16.33
		
		WaitElement(btn_EditBtnA);
		click(btn_EditBtnA);
		
		ScrollDownAruna();
		
		WaitElement(txt_DocNoSearchBRA);
		sendKeys(txt_DocNoSearchBRA, BANo1);
		pressEnter(txt_DocNoSearchBRA);
		
		WaitClick(sel_DocSelBR.replace("DocNoBR", BANo1));
		click(sel_DocSelBR.replace("DocNoBR", BANo1));
		
		WaitElement(txt_DocNoSearchBRA);
		sendKeys(txt_DocNoSearchBRA, BANo2);
		pressEnter(txt_DocNoSearchBRA);
		
		WaitClick(sel_DocSelBR.replace("DocNoBR", BANo2));
		click(sel_DocSelBR.replace("DocNoBR", BANo2));
		
		ScrollUpAruna();
		
		WaitElement(btn_UpdateBtnA);
		click(btn_UpdateBtnA);
		
		WaitElement(txt_BankAdjustmentGrideValueA.replace("BANo", BANo2));
		String BankAdjustmentGrideValue2A=getText(txt_BankAdjustmentGrideValueA.replace("BANo", BANo2));
		
		if(BankAdjustmentGrideValue1A.equals(BANo1) && BankAdjustmentGrideValue2A.equals(BANo2)) {
			writeTestResults("Verify that user can edit drafted Bank Reconciliation (Update)"
					,"user can edit drafted Bank Reconciliation (Update)"
					, "user can edit drafted Bank Reconciliation (Update) Successfully", "pass");
		}else {
			writeTestResults("Verify that user can edit drafted Bank Reconciliation (Update)"
					,"user can edit drafted Bank Reconciliation (Update)"
					, "user can edit drafted Bank Reconciliation (Update) Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String BRNo2 =trackCode;
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verifiy that user can able to create Bank reconciliation using Bank Adjustment"
					,"user can able to create Bank reconciliation using Bank Adjustment"
					, "user can able to create Bank reconciliation using Bank Adjustment Successfully", "pass");
		}else {
			writeTestResults("Verifiy that user can able to create Bank reconciliation using Bank Adjustment"
					,"user can able to create Bank reconciliation using Bank Adjustment"
					, "user can able to create Bank reconciliation using Bank Adjustment Fail", "fail");
		}
	}
	
	//FIN_16_34
	public void VerifyThatUserCanEditDraftedBankReconciliationUpdateAndNew() throws Exception {
		
		WaitElement(btn_BankAdjustmentBtnA);
		click(btn_BankAdjustmentBtnA);
		WaitElement(btn_NewBankAdjustmentBtnA);
		click(btn_NewBankAdjustmentBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Adjustment - New")){
			writeTestResults("Navigation to Bank Adjustment - New page"
					,"User can Navigate Bank Adjustment - New page"
					, "User can Navigate Bank Adjustment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Bank Adjustment - New page"
					,"User can Navigate Bank Adjustment - New page"
					, "User can Navigate Bank Adjustment - New page Fail", "fail");
		}
		
		WaitElement(sel_cboxBankBA);
		selectText(sel_cboxBankBA, BankBDSelA);
		WaitElement(sel_cboxBankAccountNoBA);
		selectText(sel_cboxBankAccountNoBA, BankAccountNoBDNewSelA);
		
		WaitElement(btn_GLAccountLookupBABtnA);
		click(btn_GLAccountLookupBABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_txtAmountBATxtA);
		sendKeys(txt_txtAmountBATxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Bank Adjustment Form"
					,"user can use Draft the Bank Adjustment Form"
					, "user can use Draft the Bank Adjustment Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Bank Adjustment Form"
					,"user can use Draft the Bank Adjustment Form"
					, "user can use Draft the Bank Adjustment Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(4000);
		trackCode= getText(txt_HeaderDocNoA);
		String BANo1 =trackCode;
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can use Release the Bank Adjustment Form"
					,"user can use Release the Bank Adjustment Form"
					, "user can use Release the Bank Adjustment Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Release the Bank Adjustment Form"
					,"user can use Release the Bank Adjustment Form"
					, "user can use Release the Bank Adjustment Form Fail", "fail");
		}
		
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_BankAdjustmentBtnA);
		click(btn_BankAdjustmentBtnA);
		WaitElement(btn_NewBankAdjustmentBtnA);
		click(btn_NewBankAdjustmentBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Adjustment - New")){
			writeTestResults("Navigation to Bank Adjustment - New page"
					,"User can Navigate Bank Adjustment - New page"
					, "User can Navigate Bank Adjustment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Bank Adjustment - New page"
					,"User can Navigate Bank Adjustment - New page"
					, "User can Navigate Bank Adjustment - New page Fail", "fail");
		}
		
		WaitElement(sel_cboxBankBA);
		selectText(sel_cboxBankBA, BankBDSelA);
		WaitElement(sel_cboxBankAccountNoBA);
		selectText(sel_cboxBankAccountNoBA, BankAccountNoBDNewSelA);
		
		WaitElement(btn_GLAccountLookupBABtnA);
		click(btn_GLAccountLookupBABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_txtAmountBATxtA);
		sendKeys(txt_txtAmountBATxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Bank Adjustment Form"
					,"user can use Draft the Bank Adjustment Form"
					, "user can use Draft the Bank Adjustment Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Bank Adjustment Form"
					,"user can use Draft the Bank Adjustment Form"
					, "user can use Draft the Bank Adjustment Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(4000);
		trackCode= getText(txt_HeaderDocNoA);
		String BANo2 =trackCode;
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can use Release the Bank Adjustment Form"
					,"user can use Release the Bank Adjustment Form"
					, "user can use Release the Bank Adjustment Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Release the Bank Adjustment Form"
					,"user can use Release the Bank Adjustment Form"
					, "user can use Release the Bank Adjustment Form Fail", "fail");
		}
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_BankReconciliationBtnA);
		click(btn_BankReconciliationBtnA);
		WaitElement(btn_NewBankReconciliationBtnA);
		click(btn_NewBankReconciliationBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Reconciliation - New")){
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Fail", "fail");
		}
		
		WaitElement(sel_cboxBankBR);
		selectText(sel_cboxBankBR, BankBDSelA);
		WaitElement(sel_cboxBankAccountNoBR);
		selectText(sel_cboxBankAccountNoBR, BankAccountNoBDNewSelA);
		
		ScrollDownAruna();
		
		WaitElement(txt_DocNoSearchBRA);
		sendKeys(txt_DocNoSearchBRA, BANo1);
		pressEnter(txt_DocNoSearchBRA);
		
		WaitClick(sel_DocSelBR.replace("DocNoBR", BANo1));
		click(sel_DocSelBR.replace("DocNoBR", BANo1));
		
		String ReconciliedAmountBR= driver.findElement(By.xpath(txt_ReconciliedAmountBR)).getAttribute("value");
		//System.out.println(ReconciliedAmountBR);
		
		ScrollUpAruna();
		
		WaitElement(txt_StatementBalanceBR);
		sendKeys(txt_StatementBalanceBR, ReconciliedAmountBR.replace("-", ""));
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		BRNo =trackCode;
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Bank reconciliation Form"
					,"user can use Draft the Bank reconciliation Form"
					, "user can use Draft the Bank reconciliation Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Bank reconciliation Form"
					,"user can use Draft the Bank reconciliation Form"
					, "user can use Draft the Bank reconciliation Form Fail", "fail");
		}
		
//		WaitElement(txt_BankAdjustmentGrideValueA.replace("BANo", BANo1));
//		String BankAdjustmentGrideValue1A=getText(txt_BankAdjustmentGrideValueA.replace("BANo", BANo1));
		
		
		//===================================================FIN_16.34
		
		WaitElement(btn_EditBtnA);
		click(btn_EditBtnA);
		
		ScrollDownAruna();
		
		WaitElement(txt_DocNoSearchBRA);
		sendKeys(txt_DocNoSearchBRA, BANo1);
		pressEnter(txt_DocNoSearchBRA);
		
		WaitClick(sel_DocSelBR.replace("DocNoBR", BANo1));
		click(sel_DocSelBR.replace("DocNoBR", BANo1));
		
		WaitElement(txt_DocNoSearchBRA);
		sendKeys(txt_DocNoSearchBRA, BANo2);
		pressEnter(txt_DocNoSearchBRA);
		
		WaitClick(sel_DocSelBR.replace("DocNoBR", BANo2));
		click(sel_DocSelBR.replace("DocNoBR", BANo2));
		
		ScrollUpAruna();
		
		WaitElement(btn_UpdateAndNewBtnA);
		click(btn_UpdateAndNewBtnA);
		
		switchWindow();
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Reconciliation - New")){
			writeTestResults("Verify that user can edit drafted Bank Reconciliation (Update & New)"
					,"user can edit drafted Bank Reconciliation (Update & New)"
					, "user can edit drafted Bank Reconciliation (Update & New) Successfully", "pass");
		}else {
			writeTestResults("Verify that user can edit drafted Bank Reconciliation (Update & New)"
					,"user can edit drafted Bank Reconciliation (Update & New)"
					, "user can edit drafted Bank Reconciliation (Update & New) Fail", "fail");
		}
		
		WaitElement(btn_BankReconciliationByPageBtnA);
		click(btn_BankReconciliationByPageBtnA);
		WaitElement(txt_BankReconciliationByPageSearchTxtA);
		sendKeys(txt_BankReconciliationByPageSearchTxtA, BRNo);
		pressEnter(txt_BankReconciliationByPageSearchTxtA);
		WaitElement(sel_BankReconciliationByPageSearchSelA.replace("BRNo", BRNo));
		doubleClick(sel_BankReconciliationByPageSearchSelA.replace("BRNo", BRNo));
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		BRNo =trackCode;
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verifiy that user can able to create Bank reconciliation using Bank Adjustment"
					,"user can able to create Bank reconciliation using Bank Adjustment"
					, "user can able to create Bank reconciliation using Bank Adjustment Successfully", "pass");
		}else {
			writeTestResults("Verifiy that user can able to create Bank reconciliation using Bank Adjustment"
					,"user can able to create Bank reconciliation using Bank Adjustment"
					, "user can able to create Bank reconciliation using Bank Adjustment Fail", "fail");
		}
	}
	
	//FIN_16_35
	public void VerifyThatUnReconciledAmountIs0AtTheTimeDocumentGetsReleased() throws Exception {
		
		WaitElement(btn_BankAdjustmentBtnA);
		click(btn_BankAdjustmentBtnA);
		WaitElement(btn_NewBankAdjustmentBtnA);
		click(btn_NewBankAdjustmentBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Adjustment - New")){
			writeTestResults("Navigation to Bank Adjustment - New page"
					,"User can Navigate Bank Adjustment - New page"
					, "User can Navigate Bank Adjustment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Bank Adjustment - New page"
					,"User can Navigate Bank Adjustment - New page"
					, "User can Navigate Bank Adjustment - New page Fail", "fail");
		}
		
		WaitElement(sel_cboxBankBA);
		selectText(sel_cboxBankBA, BankBDSelA);
		WaitElement(sel_cboxBankAccountNoBA);
		selectText(sel_cboxBankAccountNoBA, BankAccountNoBDNewSelA);
		
		WaitElement(btn_GLAccountLookupBABtnA);
		click(btn_GLAccountLookupBABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_txtAmountBATxtA);
		sendKeys(txt_txtAmountBATxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Bank Adjustment Form"
					,"user can use Draft the Bank Adjustment Form"
					, "user can use Draft the Bank Adjustment Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Bank Adjustment Form"
					,"user can use Draft the Bank Adjustment Form"
					, "user can use Draft the Bank Adjustment Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(4000);
		trackCode= getText(txt_HeaderDocNoA);
		BANo =trackCode;
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can use Release the Bank Adjustment Form"
					,"user can use Release the Bank Adjustment Form"
					, "user can use Release the Bank Adjustment Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Release the Bank Adjustment Form"
					,"user can use Release the Bank Adjustment Form"
					, "user can use Release the Bank Adjustment Form Fail", "fail");
		}
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_BankReconciliationBtnA);
		click(btn_BankReconciliationBtnA);
		WaitElement(btn_NewBankReconciliationBtnA);
		click(btn_NewBankReconciliationBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Reconciliation - New")){
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Fail", "fail");
		}
		
		WaitElement(sel_cboxBankBR);
		selectText(sel_cboxBankBR, BankBDSelA);
		WaitElement(sel_cboxBankAccountNoBR);
		selectText(sel_cboxBankAccountNoBR, BankAccountNoBDNewSelA);
		
		ScrollDownAruna();
		
		WaitElement(txt_DocNoSearchBRA);
		sendKeys(txt_DocNoSearchBRA, BANo);
		pressEnter(txt_DocNoSearchBRA);
		
		WaitClick(sel_DocSelBR.replace("DocNoBR", BANo));
		click(sel_DocSelBR.replace("DocNoBR", BANo));
		
		String ReconciliedAmountBR= driver.findElement(By.xpath(txt_ReconciliedAmountBR)).getAttribute("value");
		//System.out.println(ReconciliedAmountBR);
		
		ScrollUpAruna();
		
		WaitElement(txt_StatementBalanceBR);
		sendKeys(txt_StatementBalanceBR, ReconciliedAmountBR.replace("-", ""));
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Bank reconciliation Form"
					,"user can use Draft the Bank reconciliation Form"
					, "user can use Draft the Bank reconciliation Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Bank reconciliation Form"
					,"user can use Draft the Bank reconciliation Form"
					, "user can use Draft the Bank reconciliation Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		BRNo =trackCode;
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verifiy that user can able to create Bank reconciliation using Bank Adjustment"
					,"user can able to create Bank reconciliation using Bank Adjustment"
					, "user can able to create Bank reconciliation using Bank Adjustment Successfully", "pass");
		}else {
			writeTestResults("Verifiy that user can able to create Bank reconciliation using Bank Adjustment"
					,"user can able to create Bank reconciliation using Bank Adjustment"
					, "user can able to create Bank reconciliation using Bank Adjustment Fail", "fail");
		}
		
		//===================================================FIN_16.35
		
		WaitElement(txt_UnReconciliedAmountTxtA);
		if(getText(txt_UnReconciliedAmountTxtA).contentEquals("0.000000")) {
			writeTestResults("Verify that un-Reconciled amount is 0 at the time document gets released."
					,"un-Reconciled amount is 0 at the time document gets released."
					, "un-Reconciled amount is 0 at the time document gets released. Successfully", "pass");
		}else {
			writeTestResults("Verify that un-Reconciled amount is 0 at the time document gets released."
					,"un-Reconciled amount is 0 at the time document gets released."
					, "un-Reconciled amount is 0 at the time document gets released. Fail", "fail");
		}
	}
	
	//FIN_16_36
	public void VerifyThatSystemDoesNotAllowToDraftTheDocumentIfThereIsAnotherDraftedDocumentExistsForTheSelectedBankAccount() throws Exception {
		
		WaitElement(btn_BankAdjustmentBtnA);
		click(btn_BankAdjustmentBtnA);
		WaitElement(btn_NewBankAdjustmentBtnA);
		click(btn_NewBankAdjustmentBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Adjustment - New")){
			writeTestResults("Navigation to Bank Adjustment - New page"
					,"User can Navigate Bank Adjustment - New page"
					, "User can Navigate Bank Adjustment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Bank Adjustment - New page"
					,"User can Navigate Bank Adjustment - New page"
					, "User can Navigate Bank Adjustment - New page Fail", "fail");
		}
		
		WaitElement(sel_cboxBankBA);
		selectText(sel_cboxBankBA, BankBDSelA);
		WaitElement(sel_cboxBankAccountNoBA);
		selectText(sel_cboxBankAccountNoBA, BankAccountNoBDNewSelA);
		
		WaitElement(btn_GLAccountLookupBABtnA);
		click(btn_GLAccountLookupBABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_txtAmountBATxtA);
		sendKeys(txt_txtAmountBATxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Bank Adjustment Form"
					,"user can use Draft the Bank Adjustment Form"
					, "user can use Draft the Bank Adjustment Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Bank Adjustment Form"
					,"user can use Draft the Bank Adjustment Form"
					, "user can use Draft the Bank Adjustment Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(4000);
		trackCode= getText(txt_HeaderDocNoA);
		BANo =trackCode;
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can use Release the Bank Adjustment Form"
					,"user can use Release the Bank Adjustment Form"
					, "user can use Release the Bank Adjustment Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Release the Bank Adjustment Form"
					,"user can use Release the Bank Adjustment Form"
					, "user can use Release the Bank Adjustment Form Fail", "fail");
		}
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_BankReconciliationBtnA);
		click(btn_BankReconciliationBtnA);
		WaitElement(btn_NewBankReconciliationBtnA);
		click(btn_NewBankReconciliationBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Reconciliation - New")){
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Fail", "fail");
		}
		
		WaitElement(sel_cboxBankBR);
		selectText(sel_cboxBankBR, BankBDSelA);
		WaitElement(sel_cboxBankAccountNoBR);
		selectText(sel_cboxBankAccountNoBR, BankAccountNoBDNewSelA);
		
		ScrollDownAruna();
		
		WaitElement(txt_DocNoSearchBRA);
		sendKeys(txt_DocNoSearchBRA, BANo);
		pressEnter(txt_DocNoSearchBRA);
		
		WaitClick(sel_DocSelBR.replace("DocNoBR", BANo));
		click(sel_DocSelBR.replace("DocNoBR", BANo));
		
		String ReconciliedAmountBR1= driver.findElement(By.xpath(txt_ReconciliedAmountBR)).getAttribute("value");
		//System.out.println(ReconciliedAmountBR);
		
		ScrollUpAruna();
		
		WaitElement(txt_StatementBalanceBR);
		sendKeys(txt_StatementBalanceBR, ReconciliedAmountBR1.replace("-", ""));
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		BRNo =trackCode;
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Bank reconciliation Form"
					,"user can use Draft the Bank reconciliation Form"
					, "user can use Draft the Bank reconciliation Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Bank reconciliation Form"
					,"user can use Draft the Bank reconciliation Form"
					, "user can use Draft the Bank reconciliation Form Fail", "fail");
		}
		
		//===================================================FIN_16.36
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_BankAdjustmentBtnA);
		click(btn_BankAdjustmentBtnA);
		WaitElement(btn_NewBankAdjustmentBtnA);
		click(btn_NewBankAdjustmentBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Adjustment - New")){
			writeTestResults("Navigation to Bank Adjustment - New page"
					,"User can Navigate Bank Adjustment - New page"
					, "User can Navigate Bank Adjustment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Bank Adjustment - New page"
					,"User can Navigate Bank Adjustment - New page"
					, "User can Navigate Bank Adjustment - New page Fail", "fail");
		}
		
		WaitElement(sel_cboxBankBA);
		selectText(sel_cboxBankBA, BankBDSelA);
		WaitElement(sel_cboxBankAccountNoBA);
		selectText(sel_cboxBankAccountNoBA, BankAccountNoBDNewSelA);
		
		WaitElement(btn_GLAccountLookupBABtnA);
		click(btn_GLAccountLookupBABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_txtAmountBATxtA);
		sendKeys(txt_txtAmountBATxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Bank Adjustment Form"
					,"user can use Draft the Bank Adjustment Form"
					, "user can use Draft the Bank Adjustment Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Bank Adjustment Form"
					,"user can use Draft the Bank Adjustment Form"
					, "user can use Draft the Bank Adjustment Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(4000);
		trackCode= getText(txt_HeaderDocNoA);
		BANo =trackCode;
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can use Release the Bank Adjustment Form"
					,"user can use Release the Bank Adjustment Form"
					, "user can use Release the Bank Adjustment Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Release the Bank Adjustment Form"
					,"user can use Release the Bank Adjustment Form"
					, "user can use Release the Bank Adjustment Form Fail", "fail");
		}
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_BankReconciliationBtnA);
		click(btn_BankReconciliationBtnA);
		WaitElement(btn_NewBankReconciliationBtnA);
		click(btn_NewBankReconciliationBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Reconciliation - New")){
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Fail", "fail");
		}
		
		WaitElement(sel_cboxBankBR);
		selectText(sel_cboxBankBR, BankBDSelA);
		WaitElement(sel_cboxBankAccountNoBR);
		selectText(sel_cboxBankAccountNoBR, BankAccountNoBDNewSelA);
		
		ScrollDownAruna();
		
		WaitElement(txt_DocNoSearchBRA);
		sendKeys(txt_DocNoSearchBRA, BANo);
		pressEnter(txt_DocNoSearchBRA);
		
		WaitClick(sel_DocSelBR.replace("DocNoBR", BANo));
		click(sel_DocSelBR.replace("DocNoBR", BANo));
		
		String ReconciliedAmountBR2= driver.findElement(By.xpath(txt_ReconciliedAmountBR)).getAttribute("value");
		//System.out.println(ReconciliedAmountBR);
		
		ScrollUpAruna();
		
		WaitElement(txt_StatementBalanceBR);
		sendKeys(txt_StatementBalanceBR, ReconciliedAmountBR2.replace("-", ""));
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_BankReconciliationDraftValidatorA);
		if(isDisplayed(txt_BankReconciliationDraftValidatorA)) {
			writeTestResults("Verify that system does not allow to draft the document if there is another drafted document exists for the selected bank account."
					,"system does not allow to draft the document if there is another drafted document exists for the selected bank account."
					, "system does not allow to draft the document if there is another drafted document exists for the selected bank account. Successfully", "pass");
		}else {
			writeTestResults("Verify that system does not allow to draft the document if there is another drafted document exists for the selected bank account."
					,"system does not allow to draft the document if there is another drafted document exists for the selected bank account."
					, "system does not allow to draft the document if there is another drafted document exists for the selected bank account. Fail", "fail");
		}
		
		//===================================================after work
		
		pageRefersh();
		Thread.sleep(2000);
		
		WaitElement(btn_BankReconciliationByPageBtnA);
		click(btn_BankReconciliationByPageBtnA);
		WaitElement(txt_BankReconciliationByPageSearchTxtA);
		sendKeys(txt_BankReconciliationByPageSearchTxtA, BRNo);
		pressEnter(txt_BankReconciliationByPageSearchTxtA);
		WaitElement(sel_BankReconciliationByPageSearchSelA.replace("BRNo", BRNo));
		doubleClick(sel_BankReconciliationByPageSearchSelA.replace("BRNo", BRNo));
		
		Thread.sleep(2000);
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		BRNo =trackCode;
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verifiy that user can able to create Bank reconciliation using Bank Adjustment"
					,"user can able to create Bank reconciliation using Bank Adjustment"
					, "user can able to create Bank reconciliation using Bank Adjustment Successfully", "pass");
		}else {
			writeTestResults("Verifiy that user can able to create Bank reconciliation using Bank Adjustment"
					,"user can able to create Bank reconciliation using Bank Adjustment"
					, "user can able to create Bank reconciliation using Bank Adjustment Fail", "fail");
		}
	}
	
	//FIN_16_37
	public void VerifyThatIfThereIsAnotherStatementForaParticularBankAccountForAheadStatementDateThenTheCurrentDocumentCannotBeReversed() throws Exception {
		
		WaitElement(btn_BankAdjustmentBtnA);
		click(btn_BankAdjustmentBtnA);
		WaitElement(btn_NewBankAdjustmentBtnA);
		click(btn_NewBankAdjustmentBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Adjustment - New")){
			writeTestResults("Navigation to Bank Adjustment - New page"
					,"User can Navigate Bank Adjustment - New page"
					, "User can Navigate Bank Adjustment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Bank Adjustment - New page"
					,"User can Navigate Bank Adjustment - New page"
					, "User can Navigate Bank Adjustment - New page Fail", "fail");
		}
		
		WaitElement(sel_cboxBankBA);
		selectText(sel_cboxBankBA, BankBDSelA);
		WaitElement(sel_cboxBankAccountNoBA);
		selectText(sel_cboxBankAccountNoBA, BankAccountNoBDNewSelA);
		
		WaitElement(btn_GLAccountLookupBABtnA);
		click(btn_GLAccountLookupBABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_txtAmountBATxtA);
		sendKeys(txt_txtAmountBATxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Bank Adjustment Form"
					,"user can use Draft the Bank Adjustment Form"
					, "user can use Draft the Bank Adjustment Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Bank Adjustment Form"
					,"user can use Draft the Bank Adjustment Form"
					, "user can use Draft the Bank Adjustment Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(4000);
		trackCode= getText(txt_HeaderDocNoA);
		BANo =trackCode;
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can use Release the Bank Adjustment Form"
					,"user can use Release the Bank Adjustment Form"
					, "user can use Release the Bank Adjustment Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Release the Bank Adjustment Form"
					,"user can use Release the Bank Adjustment Form"
					, "user can use Release the Bank Adjustment Form Fail", "fail");
		}
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_BankReconciliationBtnA);
		click(btn_BankReconciliationBtnA);
		WaitElement(btn_NewBankReconciliationBtnA);
		click(btn_NewBankReconciliationBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Reconciliation - New")){
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Fail", "fail");
		}
		
		WaitElement(sel_cboxBankBR);
		selectText(sel_cboxBankBR, BankBDSelA);
		WaitElement(sel_cboxBankAccountNoBR);
		selectText(sel_cboxBankAccountNoBR, BankAccountNoBDNewSelA);
		
		ScrollDownAruna();
		
		WaitElement(txt_DocNoSearchBRA);
		sendKeys(txt_DocNoSearchBRA, BANo);
		pressEnter(txt_DocNoSearchBRA);
		
		WaitClick(sel_DocSelBR.replace("DocNoBR", BANo));
		click(sel_DocSelBR.replace("DocNoBR", BANo));
		
		String ReconciliedAmountBR1= driver.findElement(By.xpath(txt_ReconciliedAmountBR)).getAttribute("value");
		//System.out.println(ReconciliedAmountBR);
		
		ScrollUpAruna();
		
		WaitElement(txt_StatementBalanceBR);
		sendKeys(txt_StatementBalanceBR, ReconciliedAmountBR1.replace("-", ""));
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Bank reconciliation Form"
					,"user can use Draft the Bank reconciliation Form"
					, "user can use Draft the Bank reconciliation Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Bank reconciliation Form"
					,"user can use Draft the Bank reconciliation Form"
					, "user can use Draft the Bank reconciliation Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String BRNo1 =trackCode;
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verifiy that user can able to create Bank reconciliation using Bank Adjustment"
					,"user can able to create Bank reconciliation using Bank Adjustment"
					, "user can able to create Bank reconciliation using Bank Adjustment Successfully", "pass");
		}else {
			writeTestResults("Verifiy that user can able to create Bank reconciliation using Bank Adjustment"
					,"user can able to create Bank reconciliation using Bank Adjustment"
					, "user can able to create Bank reconciliation using Bank Adjustment Fail", "fail");
		}
		
		//===================================================FIN_16.37
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_BankAdjustmentBtnA);
		click(btn_BankAdjustmentBtnA);
		WaitElement(btn_NewBankAdjustmentBtnA);
		click(btn_NewBankAdjustmentBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Adjustment - New")){
			writeTestResults("Navigation to Bank Adjustment - New page"
					,"User can Navigate Bank Adjustment - New page"
					, "User can Navigate Bank Adjustment - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Bank Adjustment - New page"
					,"User can Navigate Bank Adjustment - New page"
					, "User can Navigate Bank Adjustment - New page Fail", "fail");
		}
		
		WaitElement(sel_cboxBankBA);
		selectText(sel_cboxBankBA, BankBDSelA);
		WaitElement(sel_cboxBankAccountNoBA);
		selectText(sel_cboxBankAccountNoBA, BankAccountNoBDNewSelA);
		
		WaitElement(btn_GLAccountLookupBABtnA);
		click(btn_GLAccountLookupBABtnA);
		WaitElement(txt_GLAccTxtA);
		sendKeys(txt_GLAccTxtA, GLAccTxtA);
		pressEnter(txt_GLAccTxtA);
		WaitElement(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		doubleClick(sel_GLAccSelA.replace("GLName", GLAccTxtA));
		
		WaitElement(txt_txtAmountBATxtA);
		sendKeys(txt_txtAmountBATxtA, "100");
		
		WaitElement(btn_CheckoutBtnA);
		click(btn_CheckoutBtnA);
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Bank Adjustment Form"
					,"user can use Draft the Bank Adjustment Form"
					, "user can use Draft the Bank Adjustment Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Bank Adjustment Form"
					,"user can use Draft the Bank Adjustment Form"
					, "user can use Draft the Bank Adjustment Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(4000);
		trackCode= getText(txt_HeaderDocNoA);
		BANo =trackCode;
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verify that user can use Release the Bank Adjustment Form"
					,"user can use Release the Bank Adjustment Form"
					, "user can use Release the Bank Adjustment Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Release the Bank Adjustment Form"
					,"user can use Release the Bank Adjustment Form"
					, "user can use Release the Bank Adjustment Form Fail", "fail");
		}
		
		clickNavigation();
		ClickFinanceButton();
		
		WaitElement(btn_BankReconciliationBtnA);
		click(btn_BankReconciliationBtnA);
		WaitElement(btn_NewBankReconciliationBtnA);
		click(btn_NewBankReconciliationBtnA);
		
		if(getText(txt_PageHeaderA).contentEquals("Bank Reconciliation - New")){
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Successfully", "pass");
		}else {
			writeTestResults("Navigation to Bank Reconciliation - New page"
					,"User can Navigate Bank Reconciliation - New page"
					, "User can Navigate Bank Reconciliation - New page Fail", "fail");
		}
		
		WaitElement(sel_cboxBankBR);
		selectText(sel_cboxBankBR, BankBDSelA);
		WaitElement(sel_cboxBankAccountNoBR);
		selectText(sel_cboxBankAccountNoBR, BankAccountNoBDNewSelA);
		
		ScrollDownAruna();
		
		WaitElement(txt_DocNoSearchBRA);
		sendKeys(txt_DocNoSearchBRA, BANo);
		pressEnter(txt_DocNoSearchBRA);
		
		WaitClick(sel_DocSelBR.replace("DocNoBR", BANo));
		click(sel_DocSelBR.replace("DocNoBR", BANo));
		
		String ReconciliedAmountBR2= driver.findElement(By.xpath(txt_ReconciliedAmountBR)).getAttribute("value");
		//System.out.println(ReconciliedAmountBR);
		
		ScrollUpAruna();
		
		WaitElement(txt_StatementBalanceBR);
		sendKeys(txt_StatementBalanceBR, ReconciliedAmountBR2.replace("-", ""));
		
		WaitElement(btn_DraftBtnA);
		click(btn_DraftBtnA);
		
		WaitElement(txt_pageDraft2A);
		if(isDisplayed(txt_pageDraft2A)) {
			writeTestResults("Verify that user can use Draft the Bank reconciliation Form"
					,"user can use Draft the Bank reconciliation Form"
					, "user can use Draft the Bank reconciliation Form Successfully", "pass");
		}else {
			writeTestResults("Verify that user can use Draft the Bank reconciliation Form"
					,"user can use Draft the Bank reconciliation Form"
					, "user can use Draft the Bank reconciliation Form Fail", "fail");
		}
		
		WaitElement(btn_ReleaseBtnA);
		click(btn_ReleaseBtnA);
		Thread.sleep(2000);
		trackCode= getText(txt_HeaderDocNoA);
		String BRNo2 =trackCode;
		
		WaitElement(txt_pageRelease2A);
		if(isDisplayed(txt_pageRelease2A)) {
			writeTestResults("Verifiy that user can able to create Bank reconciliation using Bank Adjustment"
					,"user can able to create Bank reconciliation using Bank Adjustment"
					, "user can able to create Bank reconciliation using Bank Adjustment Successfully", "pass");
		}else {
			writeTestResults("Verifiy that user can able to create Bank reconciliation using Bank Adjustment"
					,"user can able to create Bank reconciliation using Bank Adjustment"
					, "user can able to create Bank reconciliation using Bank Adjustment Fail", "fail");
		}
		
		WaitElement(btn_BankReconciliationByPageBtnA);
		click(btn_BankReconciliationByPageBtnA);
		WaitElement(txt_BankReconciliationByPageSearchTxtA);
		sendKeys(txt_BankReconciliationByPageSearchTxtA, BRNo1);
		pressEnter(txt_BankReconciliationByPageSearchTxtA);
		WaitElement(sel_BankReconciliationByPageSearchSelA.replace("BRNo", BRNo1));
		doubleClick(sel_BankReconciliationByPageSearchSelA.replace("BRNo", BRNo1));
		
		Thread.sleep(2000);
		
		WaitElement(btn_ActionBtnA);
		click(btn_ActionBtnA);
		WaitElement(btn_ReverseBtnA);
		click(btn_ReverseBtnA);
		WaitElement(btn_YesBtnA);
		click(btn_YesBtnA);
		
		WaitElement(txt_BankReconciliationReverseValidatorA);
		if(isDisplayed(txt_BankReconciliationReverseValidatorA)) {
			writeTestResults("Verify that if there is another statement for a particular bank account for ahead statement date, then the current document cannot be reversed."
					,"if there is another statement for a particular bank account for ahead statement date, then the current document cannot be reversed."
					, "if there is another statement for a particular bank account for ahead statement date, then the current document cannot be reversed. Successfully", "pass");
		}else {
			writeTestResults("Verify that if there is another statement for a particular bank account for ahead statement date, then the current document cannot be reversed."
					,"if there is another statement for a particular bank account for ahead statement date, then the current document cannot be reversed."
					, "if there is another statement for a particular bank account for ahead statement date, then the current document cannot be reversed. Fail", "fail");
		}
	}
	
	//scrollDown
	public void ScrollDownAruna() throws Exception {
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,400)");
	}
	
	//scrollUp
	public void ScrollUpAruna() throws Exception {
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,-400)");
	}

	//WaitClick
	public void WaitClick(String Locator)  throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Locator)));
	}

	//WaitElement
	public void WaitElement(String Locator)  throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator)));
	}
}
