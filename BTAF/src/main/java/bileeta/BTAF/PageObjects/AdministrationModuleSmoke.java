package bileeta.BTAF.PageObjects;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.time.LocalTime;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.openqa.selenium.JavascriptExecutor;

import bileeta.BATF.Pages.AdministrationModuleDataSmoke;

public class AdministrationModuleSmoke extends AdministrationModuleDataSmoke {

	private static String email_TC_003;
	private static String password_TC_007;
	private static FileInputStream fis;
	private static Workbook wb;
	private static Sheet sh;
	private static Row row;
	private static Cell cell;
	private static Cell cell1;
	private static FileOutputStream fos;

	public void navigateToTheLoginPage() throws Exception {
		openPage(siteURL);

		writeTestResults("Verify that 'Entution' header is available on the page",
				" User should be navigate to the login page of the Entution", "login page is dislayed", "pass");

	}

	public void verifyTheLogo() throws Exception {
		if (isDisplayed(siteLogo)) {

			writeTestResults("Verify that 'Entution' header is available on the page",
					"user should be able to see the logo", "logo is dislayed", "pass");
		} else {
			writeTestResults("Verify that 'Entution' header is available on the page",
					"user should be able to see the logo", "logo is not dislayed", "fail");

		}
	}

	public void userLogin() throws Exception {
		sendKeys(txt_username, UserNameData);

		String pass = "Aws@"+AdministrationModule.readAdminData(0);
		sendKeys(txt_password, pass);

//		sendKeys(txt_password, PasswordData);

		click(btn_login);

		if (isDisplayed(lnk_home)) {

			writeTestResults("User login", "User should navigate to the Home page of Entution",
					"user navigated to the home page", "pass");
		}

		else {
			writeTestResults("User login", "User should navigate to the Home page of Entution",
					"user cant navigate to the home page", "fail");

		}
	}

	public void navigationmenu() throws Exception {

		click(btn_navigationmenu);

		if (isDisplayed(sidemenu)) {
			writeTestResults("Click on the top navigation menu", "System should be loaded the menu item successfully",
					"Menu item is displayed", "pass");
		} else {
			writeTestResults("Click on the top navigation menu", "System should not  loaded the menu item successfully",
					"Menu item is not displayed", "fail");
		}
	}

	public void btn_administration() throws Exception {

		click(btn_administration);

		if (isDisplayed(subsidemenu)) {

			writeTestResults("Click on the Administration module",
					"System should be loaded the administration module successfully", "subsidemenu is displayed",
					"pass");
		} else {

			writeTestResults("Click on the Administration module",
					"System should not loaded the administration module successfully", "subsidemenu is not displayed",
					"fail");
		}
	}

	public void clickuserinformation() throws Exception {
		click(btn_userinformation);

		if (isDisplayed(userinformationpage)) {

			writeTestResults("Click on the user information form",
					"User information page should be loaded successfully",
					"userinformationpage is displayed successfully ", "pass");
		} else {

			writeTestResults("Click on the user information form",
					".User information page should not loaded successfully", "userinformationpage is not displayed",
					"fail");

		}

	}

	public void clickbtnnewuser() throws Exception {

		click(btn_newuser);

		Thread.sleep(2000);

		if (isDisplayed(newuserpage)) {

			writeTestResults("Click on the 'New user'button", "New user creation page loaded",
					"New user creation page loaded sucessfully ", "pass");
		} else {

			writeTestResults(".Click on the 'New user'button", "New user creation page not loaded",
					"New user creation page not loaded sucessfully", "fail");
		}
	}

	public void createnewuser() throws Exception {

		Thread.sleep(4000);

		selectText(txt_cbox, "txt_Cbox");

		sendKeys(fullname, txt_FullName);

		sendKeys(displaynamefield, txt_DisplayNameField);

		// mail id
		email_TC_003 = txt_EmailNameField;
		sendKeys(emailnamefield, txt_EmailNameField);

		selectText(userlevelfield, txt_UserLevelField);

		selectText(usertypefield, txt_UserTypeField);

		Thread.sleep(4000);

		click(btn_draft);

	}

	public void createnewuserTC09() throws Exception {

		selectText(txt_cbox, "txt_Cbox");

		sendKeys(fullname, txt_FullName);

		sendKeys(displaynamefield, txt_DisplayNameField);

		// mail id
		email_TC_003 = txt_EmailNameField;

		sendKeys(emailnamefield, txt_EmailNameField);

		String EmailName = getText(txt_EmailNameField);
		Thread.sleep(2000);

		System.out.println(EmailName);

		Thread.sleep(2000);

		selectText(userlevelfield, txt_UserLevelField);

		selectText(usertypefield, txt_UserTypeField);

		Thread.sleep(4000);

		click(btn_draft);

		Thread.sleep(3000);

		click(btn_edit);

		if (isDisplayed(editpage)) {

			writeTestResults("Click on the 'Edit' button", "The edit mode should be enabled successfully",
					"edit mode should be enabled successfully", "pass");
		}

		else {

			writeTestResults("Click on the 'Edit' button", "The edit mode should not enabled successfully",
					"edit mode should not enabled successfully in DB", "fail");

		}

		Thread.sleep(3000);

		click(btn_addpermission);

		switchWindow();

		Thread.sleep(2000);

		sendKeys(balancinglevel, txt_BalancingLevel);

		Thread.sleep(2000);

		click(btn_form);

		Thread.sleep(2000);

		if (isDisplayed(formtab)) {

			writeTestResults("Click on the 'Forms'Tab", "The user should be on 'Forms' tab",
					"The user should be on 'Forms' tab sucessfully", "pass");
		}

		else {

			writeTestResults("Click on the 'Forms'Tab", "The user should not on 'Forms' tab",
					"The user should not on 'Forms' tab sucessfully", "fail");

		}

		selectText(modulename, txt_ModuleName);

		if (isDisplayed(modulename)) {

			writeTestResults(" Move to the  'Module Name' field", "The user should be on Moduke name area",
					"The user should be on Moduke name area sucessfully", "pass");
		}

		else {

			writeTestResults(" Move to the  'Module Name' field", "The user should not on Moduke name area",
					"The user should not on Moduke name area sucessfully", "fail");

		}

		click(btn_AllowTemplatetoall);

		click(btn_AllowPrinttoall);

		click(btn_AllowReversetoall);

		click(btn_AllowAllReports);

		click(btn_AllowReleasetoall);

		Thread.sleep(3000);

		click(btn_update);

		Thread.sleep(3000);

		pageRefersh();

		closeWindow();

		pageRefersh();

		Thread.sleep(3000);

		click(btn_update);

		Thread.sleep(5000);

		click("//a[contains(text(),'Release')]");

		Thread.sleep(6000);

		trackCode = email_TC_003;

		if (isDisplayed(releasedpage))

		{
			writeTestResults("Click on the 'Release' button", "The user should be released successfully",
					"The user released successfully", "pass");
		}

		else {

			writeTestResults("Click on the 'Release' button", "The user should be released successfully",
					"The user not released successfully", "fail");

		}

		switchWindow();

		click(btn_navigationmenu);

		if (isDisplayed(sidemenu)) {
			writeTestResults("Click on the top navigation menu", "System should be loaded the menu item successfully",
					"Menu item is displayed", "pass");
		} else {
			writeTestResults("Click on the top navigation menu", "System should not  loaded the menu item successfully",
					"Menu item is not displayed", "fail");
		}

		click(btn_administration);

		if (isDisplayed(subsidemenu)) {

			writeTestResults("Click on the Administration module",
					"System should be loaded the administration module successfully", "subsidemenu is displayed",
					"pass");
		} else {

			writeTestResults("Click on the Administration module",
					"System should not loaded the administration module successfully", "subsidemenu is not displayed",
					"fail");
		}

		click(btn_userinformation);

		if (isDisplayed(userinformationpage)) {

			writeTestResults("Click on the user information form",
					"User information page should be loaded successfully",
					"userinformationpage is displayed successfully ", "pass");
		} else {

			writeTestResults("Click on the user information form",
					".User information page should not loaded successfully", "userinformationpage is not displayed",
					"fail");

		}

		switchWindow();

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(searchuser, email_TC_003);

		Thread.sleep(3000);

		pressEnter(searchuser);

		Thread.sleep(4000);

		trackCode = email_TC_003;

		if (isDisplayed(releasedpage))

			if (isDisplayed(username.replace("username", txt_SearchUser))) {

				writeTestResults("Click on the serach button",
						"The search button should be clicked and data should be dispalyed",
						"user is displayed successfully", "pass");
			}

			else {

				writeTestResults("Click on the serach button",
						"The search button should be clicked and data should not dispalyed",
						"user is not displayed successfully", "fail");

			}

		click(btn_user);

		trackCode = email_TC_003;

		if (isDisplayed(releasedpage))

		{

			writeTestResults("Click on the user", "The user data should be dispalyed",
					"User information is displayed successfully", "pass");
		}

		else {

			writeTestResults("Click on the user", "The user data should not be dispalyed",
					"User information is not displayed successfully", "fail");

		}
	}

	// edit
	public void verifyeditbutton() throws Exception {

		click(btn_edit);

//		switchWindow();
//		
//		 Thread.sleep(3000);
//			    
//		 selectText("//select[@id='cobxUserLevel']", "Super");
//				
//		 Thread.sleep(2000);

		if (isDisplayed(editpage)) {

			Thread.sleep(3000);

			writeTestResults("Click on the 'Edit' button", "The edit mode should be enabled successfully",
					"edit mode should be enabled successfully", "pass");
		}

		else {
			writeTestResults("Click on the 'Edit' button", "The edit mode should not enabled successfully",
					"edit mode should not enabled successfully", "fail");

		}
	}

	// update
	public void clickupdatebutton() throws Exception {

		Thread.sleep(3000);

		click(btn_update);

		Thread.sleep(3000);

		if (isDisplayed(updatepage)) {

			writeTestResults("Click on the update button",
					"Update button should be clicked and data should be saved in DB", "Data saved successfully in DB",
					"pass");
		}

		else {

			writeTestResults("Click on the update button",
					"Update button should not clicked and data should not saved in DB",
					"Data not saved successfully in DB", "fail");

		}
	}

	// tc005
	public void givepermission() throws Exception {

		Thread.sleep(3000);

		click(btn_edit);

		if (isDisplayed(editpage)) {

			writeTestResults("Click on the 'Edit' button", "The edit mode should be enabled successfully",
					"edit mode should be enabled successfully", "pass");
		}

		else {

			writeTestResults("Click on the 'Edit' button", "The edit mode should not enabled successfully",
					"edit mode should not enabled successfully in DB", "fail");

		}

		Thread.sleep(3000);

		click(btn_addpermission);

		switchWindow();

		Thread.sleep(2000);

		sendKeys(balancinglevel, txt_BalancingLevel);

		Thread.sleep(2000);

		click(btn_form);
		Thread.sleep(2000);

		if (isDisplayed(formtab)) {

			writeTestResults("Click on the 'Forms'Tab", "The user should be on 'Forms' tab",
					"The user should be on 'Forms' tab sucessfully", "pass");
		}

		else {

			writeTestResults("Click on the 'Forms'Tab", "The user should not on 'Forms' tab",
					"The user should not on 'Forms' tab sucessfully", "fail");

		}

		selectText(modulename, txt_ModuleName);

		if (isDisplayed(modulename)) {

			writeTestResults(" Move to the  'Module Name' field", "The user should be on Moduke name area",
					"The user should be on Moduke name area sucessfully", "pass");
		}

		else {

			writeTestResults(" Move to the  'Module Name' field", "The user should not on Moduke name area",
					"The user should not on Moduke name area sucessfully", "fail");

		}

		click(btn_AllowTemplatetoall);

		click(btn_AllowPrinttoall);

		click(btn_AllowReversetoall);

		click(btn_AllowAllReports);

		click(btn_AllowReleasetoall);

		Thread.sleep(4000);

		click(btn_update);

		Thread.sleep(4000);

		pageRefersh();

		closeWindow();

		pageRefersh();

		Thread.sleep(3000);

	}

	public void clickreleasebutton() throws Exception {

		pageRefersh();

		Thread.sleep(4000);

		click(btn_edit);

		switchWindow();
		Thread.sleep(3000);

		selectText("//select[@id='cobxUserLevel']", "Super");

		Thread.sleep(4000);

		click(btn_update);

		Thread.sleep(4000);

		pageRefersh();

		switchWindow();

		Thread.sleep(2000);

		click("//a[contains(text(),'Release')]");

		Thread.sleep(6000);

		trackCode = email_TC_003;

		if (isDisplayed(releasedpage))

		{
			writeTestResults("Click on the 'Release' button", "The user should be released successfully",
					"The user released successfully", "pass");
		}

		else {

			writeTestResults("Click on the 'Release' button", "The user should be released successfully",
					"The user not released successfully", "fail");

		}
	}
	// configuration

	public void clickconfigurationbutton() throws Exception {

		click(btn_configuration);

		click(btn_changepassword);

		if (isDisplayed("//div[@id='dlgChangePassword']")) {

			writeTestResults("Select the 'Change password' option from the drop down",
					"The change password pop up screen should be loaded",
					"changepasswordpopup is displayed successfully", "pass");

		} else {

			writeTestResults("Select the 'Change password' option from the drop down",
					"The change password pop up screen should not loaded",
					"changepasswordpopup is not displayed  successfully", "fail");
		}
	}

	/* Write admin data */
	// Common
//			public void writeAdminData(String name, String variable, int data_row) throws InvalidFormatException, IOException{
//				fis = new FileInputStream("src/test/java/administrationModule/AdminData.xlsx");
//				wb = WorkbookFactory.create(fis);
//				sh = wb.getSheet("Sheet1");
//			/*	int noOfRows = sh.getLastRowNum();
//				System.out.println(noOfRows);*/
//				row = sh.createRow(data_row);
//				cell = row.createCell(1);
//				cell1 = row.createCell(0);
//				cell.setCellValue(variable);
//				cell1.setCellValue(name);
//				System.out.println(cell.getStringCellValue());
//				fos = new FileOutputStream("src/test/java/administrationModule/AdminData.xlsx");
//				wb.write(fos);
//				fos.flush();
//				fos.close();
//			}
//			
//			/* Read admin data */
//			public String readAdminData(int data_row) throws IOException, InvalidFormatException {
//				fis = new FileInputStream("src/test/java/administrationModule/AdminData.xlsx");
//				wb = WorkbookFactory.create(fis);
//				sh = wb.getSheet("Sheet1");
//				row = sh.getRow(data_row);
//				cell = row.getCell(1);
//
//				String employee_readed = cell.getStringCellValue();
//				return employee_readed;
//			}

	// change password
	public void changepassword() throws Exception {

		sendKeys(currentpassword, AdministrationModule.readAdminData(0));

		Thread.sleep(1000);

//				sendKeys(currentpassword, txt_CurrentPassword);

//				password_TC_007 = txt_NewPassword;

		int passInt = Integer.parseInt(AdministrationModule.readAdminData(0));
		int afterInt = passInt + 5;
		String pass_out = String.valueOf(afterInt);
//					
		AdministrationModule.writeAdminData("Password", pass_out, 0);

//					sendKeys(newpassword, readAdminData(0));

		sendKeys(newpassword, pass_out);

		sendKeys(confirmnewpassword, pass_out);
		Thread.sleep(1000);

		sendKeys(confirmnewpassword, txt_ConfirmNewPassword);

		Thread.sleep(1000);

		click(btn_change);

		Thread.sleep(6000);

		trackCode = "New Password = " + pass_out;

		if (isDisplayed(newhomepage)) {

			writeTestResults("Click on the 'Change' button", "The user made changes should be saved to the DB",
					"password changed successfully", "pass");

		} else {

			writeTestResults("Click on the 'Change' button", "The user made changes should not saved to the DB",
					"password not changed successfully", "fail");

		}

	}

//	search user		
	public void searchuser() throws Exception {

		switchWindow();

		selectText(template, txt_Template);

		sendKeys(searchuser, "rashi09@yopmail.com");

		pressEnter(searchuser);

		Thread.sleep(3000);

		if (isDisplayed(username.replace("username", txt_SearchUser))) {

			writeTestResults("Click on the serach button",
					"The search button should be clicked and data should be dispalyed",
					"user is displayed successfully", "pass");
		}

		else {

			writeTestResults("Click on the serach button",
					"The search button should be clicked and data should not dispalyed",
					"user is not displayed successfully", "fail");

		}

		click(btn_user);

	}

	//////

	public void searchuserforstatus() throws Exception {

		selectText(template, txt_Templatestatus);

		sendKeys(searchuser, txt_SearchUserstatus);

		pressEnter(searchuser);

		Thread.sleep(1000);

		if (isDisplayed(username.replace("username", txt_SearchUserstatus))) {

			writeTestResults("Click on the serach button",
					"The search button should be clicked and data should be dispalyed",
					"user is displayed successfully", "pass");
		}

		else {

			writeTestResults("Click on the serach button",
					"The search button should be clicked and data should not dispalyed",
					"user is not displayed successfully", "fail");

		}

		click(btn_user);

	}

	// forgot password
	public void clickforgotpassword() throws Exception {

		click(btn_forgotpassword);

		if (isDisplayed(resetpasswordpage)) {

			writeTestResults("Verify that 'Reset Your Password' header is availble",
					"The user should be re directed to the 'Reset Your Password' page",
					"resetpasswordpage is displayed successfully", "pass");
		}

		else {

			writeTestResults("Verify that 'Reset Your Password' header is availble",
					"The user should not re directed to the 'Reset Your Password' page",
					"resetpasswordpage is not displayed successfully", "fail");
		}

	}

	public void userLogintomail() throws Exception {
		sendKeys(emailaddress, txt_EmailAddress);

		click(btn_next);

	}

	public void navigateToTheMailPage() throws Exception {
		openPage(mailURL);

		writeTestResults("Then go to the email page", "The user should be on his email page",
				"The user should be on his email successfully", "pass");

	}

	public void verifyTheMail() throws Exception

	{

		// click("//ul[contains(@class,'h-c-header__cta-list
		// header__nav--ltr')]//a[contains(@class,'h-c-header__nav-li-link')][contains(text(),'Sign
		// in')]");

		if (isDisplayed(maillogo)) {

			writeTestResults("Verify that email logo is available on the page",
					"user should be able to see the mail logo", "mail logo is dislayed", "pass");
		} else {
			writeTestResults("Verify that email logo is available on the page",
					"user should not able to see the mail logo", "mail logo is not dislayed", "fail");

		}
	}

	public void uservalidate() throws Exception {
		sendKeys(mailid, "bileeta.automation@gmail.com");

		click(btn_next1);

		sendKeys(mailpassword, "Bileeta123");

		click(btn_next2);

		Thread.sleep(4000);

		switchWindow();

	}

	public void OpenMailRead() throws Exception {

		Thread.sleep(3000);

//            if (isDisplayed(mailpage)) {
//				
//    			writeTestResults("Then go to the email address", "The user should be on his email", "user navigated to the mail", "pass");
//    		}
//  			
//    			else
//   			{
//    				writeTestResults("Then go to the email address", "The user should not on his email", "user not navigate to the mail", "fail");
//
//    			}

//            click("");

		switchWindow();

		Thread.sleep(3000);

		IsPasswordResetSent();

		if ((IsPasswordResetSent())) {

			writeTestResults("Verify that there is email from the entution which is content the new password",
					"The system generated email should be on user provided email address", "view the mail successfully",
					"pass");
		}

		else {
			writeTestResults("Verify that there is email from the entution which is content the new password",
					"The system generated email should not on user provided email address",
					" not view the mail successfully", "fail");

		}

		closeWindow();

	}

	// terminate a user

	public void terminateuser() throws Exception {

		click(btn_user);

		click(btn_edit1);

		if (isDisplayed(editpage)) {

			writeTestResults("Click on the 'Edit' button", "The edit mode should be enabled successfully",
					"edit mode should be enabled successfully", "pass");
		}

		else {

			writeTestResults("Click on the 'Edit' button", "The edit mode should not enabled successfully",
					"edit mode should not enabled successfully in DB", "fail");

		}

		switchWindow();

		click(btn_active);

		selectText(status, txt_status);

//	    	 click (btn_changeperiod);
//	    	 
//	    	 click (btn_from);
//	    	 
//	    	 if(isDisplayed(calander)) {
//					
//					writeTestResults("Enable the change period", "The change period should be enabled","The change period  enabled successfully","pass" );}
//				
//				    else {
//						
//					writeTestResults("Enable the change period", "The change period should not enabled","The change period not enabled successfully","fail");
//					
//					}
//	    	 
//	    	 
//	    	 click (btn_fromdate);
//	    	 
//	    	 click (btn_to);
//	    	 
//	    	 
//	    	 if(isDisplayed(calander)) {
//					
//					writeTestResults("Enable the change period", "The change period should be enabled","The change period  enabled successfully","pass" );}
//				
//				    else {
//						
//					writeTestResults("Enable the change period", "The change period should not enabled","The change period not enabled successfully","fail");
//					
//					}
//	    	 
//	    	 click (btn_todate);
//	    	 

		click(btn_apply);

		if (isDisplayed(Assignuserpopup)) {

			writeTestResults("Click on the 'Apply button",
					"The 'Apply' button should be clicked and 'Assign users' pop up should be loaded successfully",
					"'Assign users' pop up should be loaded successfully", "pass");
		}

		else {

			writeTestResults("Click on the 'Apply button",
					"The 'Apply' button should be clicked and 'Assign users' pop up should not loaded successfully",
					"'Assign users' pop up should not loaded successfully", "fail");

		}

		click(btn_apply2);

		click(btn_update4);

		if (isDisplayed(updatepage)) {

			writeTestResults("Click on the update button",
					"Update button should be clicked and data should be saved in DB", "Data saved successfully in DB",
					"pass");
		}

		else {

			writeTestResults("Click on the update button",
					"Update button should not clicked and data should not saved in DB",
					"Data not saved successfully in DB", "fail");

		}

		switchWindow();

		String TerminateStatus = getText(TerminateStatusValue);

		Thread.sleep(4000);

		System.out.println(TerminateStatus);

		trackCode = TerminateStatus;

		if (isDisplayed(updatepage)) {

			writeTestResults("Click on the update button", "User should be on Terminated Status",
					"User in Terminated Status successfully", "pass");
		}

		else {

			writeTestResults("Click on the update button", "User should not on Terminated Status",
					"User not oin Terminated Status successfully", "fail");

		}

	}
	// .............................................................................
	// active a terminate user

	public void activeuser() throws Exception {

		click(btn_user);

		click(btn_edit1);

		if (isDisplayed(editpage)) {

			writeTestResults("Click on the 'Edit' button", "The edit mode should be enabled successfully",
					"edit mode should be enabled successfully", "pass");
		}

		else {

			writeTestResults("Click on the 'Edit' button", "The edit mode should not enabled successfully",
					"edit mode should not enabled successfully in DB", "fail");

		}

		switchWindow();

		click(btn_active);

		selectText(status, txt_status4);

//             click (btn_changeperiod);
//	    	 
//	    	 click (btn_from);

//	    	 if(isDisplayed(calander)) {
//					
//					writeTestResults("Enable the change period", "The change period should be enabled","The change period  enabled successfully","pass" );}
//				
//				    else {
//						
//					writeTestResults("Enable the change period", "The change period should not enabled","The change period not enabled successfully","fail");
//					
//					}

//	    	 click (btn_fromdate);
//	    	 
//	    	 click (btn_to);
//	    	 
//             click (btn_todate);

		click(btn_apply);

		Thread.sleep(3000);

		click(btn_update4);

		switchWindow();

		String TerminateStatus = getText(TerminateStatusValue);

		Thread.sleep(4000);

		System.out.println(TerminateStatus);

		trackCode = TerminateStatus;

		if (isDisplayed(updatepage)) {

			writeTestResults("Click on the update button", "User should be on Active Status",
					"User in Active Status successfully", "pass");
		}

		else {

			writeTestResults("Click on the update button", "User should not on Active Status",
					"User not oin Active Status successfully", "fail");

		}

	}
	// ..................................................
	// inactive user

	public void inactiveuser() throws Exception {

		click(btn_user);

		click(btn_edit1);

		if (isDisplayed(editpage)) {

			writeTestResults("Click on the 'Edit' button", "The edit mode should be enabled successfully",
					"edit mode should be enabled successfully", "pass");
		}

		else {

			writeTestResults("Click on the 'Edit' button", "The edit mode should not enabled successfully",
					"edit mode should not enabled successfully in DB", "fail");

		}

		switchWindow();

		click(btn_active);

		selectText(status, txt_status2);

		Thread.sleep(1000);

//             click (btn_changeperiod);
//	    	 
//	    	 click (btn_from);
//	    	  
//	    	 click (btn_fromdate);
//	    	 
//    	     click (btn_to);
//	    	 
//	    	 click (btn_todate);
//	    	 
//	    	 click (btn_apply);

		click(btn_apply);

//	    	 if(isDisplayed(Assignuserpopup)) {
//					
//					writeTestResults("Click on the 'Apply button", "The 'Apply' button should be clicked and 'Assign users' pop up should be loaded successfully","'Assign users' pop up should be loaded successfully","pass" );}
//				
//				    else {
//						
//					writeTestResults("Click on the 'Apply button", "The 'Apply' button should be clicked and 'Assign users' pop up should not loaded successfully","'Assign users' pop up should not loaded successfully","fail");
//					
//					}
//	    	 
		click(btn_apply2);

		Thread.sleep(3000);

		click(btn_update4);

		switchWindow();

		String TerminateStatus = getText(TerminateStatusValue);

		Thread.sleep(4000);

		System.out.println(TerminateStatus);

		trackCode = TerminateStatus;

		if (isDisplayed(updatepage)) {

			writeTestResults("Click on the update button", "User should be on Inactive Status",
					"User in Inactive Status successfully", "pass");
		}

		else {

			writeTestResults("Click on the update button", "User should not on Inactive  Status",
					"User not oin Inactive Status successfully", "fail");

		}

	}
	// ...............................
//block a user

	public void blockauser() throws Exception {

		click(btn_editblock);

		click(btn_activeblock);

		selectText(status, txt_status3);

//             click (btn_changeperiod);
//	    	 
//	    	 click (btn_from);
//	    	     	 
//	    	 
//	         click (btn_fromdate);
//	    	 
//	    	 click (btn_to);
//	    	 
//	    	 click (btn_todate);
//	    	 
		click(btn_apply);

		Thread.sleep(1000);

		click(btn_update4);

//	    	 if(isDisplayed(activeuserpage)) {
//					
//	    		 writeTestResults("Click on the update button", "Update button should be clicked and data should be saved in DB","Data saved successfully in DB","pass");}
//				
//				else{
//					
//				writeTestResults("Click on the update button", "Update button should not clicked and data should not saved in DB","Data not saved successfully in DB","fail");
//				
//				}

		switchWindow();

		String TerminateStatus = getText(TerminateStatusValue);

		Thread.sleep(4000);

		System.out.println(TerminateStatus);

		trackCode = TerminateStatus;

		if (isDisplayed(updatepage)) {

			writeTestResults("Click on the update button", "User should be on Blocked Status",
					"User in Blocked Status successfully", "pass");
		}

		else {

			writeTestResults("Click on the update button", "User should not on Blocked Status",
					"User not in Blocked Status successfully", "fail");

		}

	}

	// active a blocked user
	public void activeauser() throws Exception {
//	    	 click (btn_user);

		pageRefersh();

		Thread.sleep(1000);

		click(btn_editactive);

		if (isDisplayed(editpage)) {

			writeTestResults("Click on the 'Edit' button", "The edit mode should be enabled successfully",
					"edit mode should be enabled successfully", "pass");
		}

		else {

			writeTestResults("Click on the 'Edit' button", "The edit mode should not enabled successfully",
					"edit mode should not enabled successfully in DB", "fail");

		}

		switchWindow();

		click(btn_active);

		Thread.sleep(2000);

		selectText(status, txt_status4);

//            click (btn_changeperiod1);

//	    	 click (btn_from);
//	    	 
//	    	 if(isDisplayed(calander)) {
//					
//					writeTestResults("Enable the change period", "The change period should be enabled","The change period  enabled successfully","pass" );}
//				
//				    else {
//						
//					writeTestResults("Enable the change period", "The change period should not enabled","The change period not enabled successfully","fail");
//					
//					}
//	    	 
//	    	 
//	    	 click (btn_fromdate);
//	    	 
//	    	 click (btn_to);
//	    	 
//	    	 
//	    	 if(isDisplayed(calander)) {
//					
//					writeTestResults("Enable the change period", "The change period should be enabled","The change period  enabled successfully","pass" );}
//				
//				    else {
//						
//					writeTestResults("Enable the change period", "The change period should not enabled","The change period not enabled successfully","fail");
//					
//					}
//	    	 
//	    	 click (btn_todate);
//	    	 
		click(btn_apply);

//             click (btn_apply);
//	    	 
//
//	    	 if(isDisplayed(Assignuserpopup)) {
//					
//					writeTestResults("Click on the 'Apply button", "The 'Apply' button should be clicked and 'Assign users' pop up should be loaded successfully","'Assign users' pop up should be loaded successfully","pass" );}
//				
//				    else {
//						
//					writeTestResults("Click on the 'Apply button", "The 'Apply' button should be clicked and 'Assign users' pop up should not loaded successfully","'Assign users' pop up should not loaded successfully","fail");
//					
//					}
//	    	 
//	    	 click (btn_apply2);

		Thread.sleep(2000);

		click(btn_update4);

		switchWindow();

		String TerminateStatus = getText(TerminateStatusValue);

		Thread.sleep(4000);

		System.out.println(TerminateStatus);

		trackCode = TerminateStatus;

		if (isDisplayed(updatepage)) {

			writeTestResults("Click on the update button", "User should be on Active Status",
					"User in Active Status successfully", "pass");
		}

		else {

			writeTestResults("Click on the update button", "User should not on Active Status",
					"User not in Active Status successfully", "fail");

		}

	}

	// inactive a blocked user
	public void inactiveauser() throws Exception {

		click(btn_user);

		click(btn_edit1);

		if (isDisplayed(editpage)) {

			writeTestResults("Click on the 'Edit' button", "The edit mode should be enabled successfully",
					"edit mode should be enabled successfully", "pass");
		}

		else {

			writeTestResults("Click on the 'Edit' button", "The edit mode should not enabled successfully",
					"edit mode should not enabled successfully in DB", "fail");

		}

		// switchWindow();
		click(btn_active);

		selectText(status, txt_status2);

//             click (btn_changeperiod);
//	    	 
//	    	 click (btn_from);
//	    	 
//	    	 if(isDisplayed(calander)) {
//					
//					writeTestResults("Enable the change period", "The change period should be enabled","The change period  enabled successfully","pass" );}
//				
//				    else {
//						
//					writeTestResults("Enable the change period", "The change period should not enabled","The change period not enabled successfully","fail");
//					
//					}
//	    	 
//	    	 
//	    	 click (btn_fromdate);
//	    	 
//	    	 click (btn_to);
//	    	 
//	    	 
//	    	 if(isDisplayed(calander)) {
//					
//					writeTestResults("Enable the change period", "The change period should be enabled","The change period  enabled successfully","pass" );}
//				
//				    else {
//						
//					writeTestResults("Enable the change period", "The change period should not enabled","The change period not enabled successfully","fail");
//					
//					}
//	    	 
//	    	 click (btn_todate);
//	    	 
		click(btn_apply);

		click(btn_apply);

		if (isDisplayed(Assignuserpopup)) {

			writeTestResults("Click on the 'Apply button",
					"The 'Apply' button should be clicked and 'Assign users' pop up should be loaded successfully",
					"'Assign users' pop up should be loaded successfully", "pass");
		}

		else {

			writeTestResults("Click on the 'Apply button",
					"The 'Apply' button should be clicked and 'Assign users' pop up should not loaded successfully",
					"'Assign users' pop up should not loaded successfully", "fail");

		}

		click(btn_apply2);

		Thread.sleep(3000);

		click(btn_update4);

		switchWindow();

		String TerminateStatus = getText(TerminateStatusValue);

		Thread.sleep(4000);

		System.out.println(TerminateStatus);

		trackCode = TerminateStatus;

		if (isDisplayed(updatepage)) {

			writeTestResults("Click on the update button", "User should be on Inactive Status",
					"User in Inactive status successfully", "pass");
		}

		else {

			writeTestResults("Click on the update button", "User should not on Inactive Status",
					"User not oin Inactive Status successfully", "fail");

		}

	}

	// active a inactive user
	public void activeainactiveuser() throws Exception {

		click(btn_user);

		click(btn_edit1);

		if (isDisplayed(editpage)) {

			writeTestResults("Click on the 'Edit' button", "The edit mode should be enabled successfully",
					"edit mode should be enabled successfully", "pass");
		}

		else {

			writeTestResults("Click on the 'Edit' button", "The edit mode should not enabled successfully",
					"edit mode should not enabled successfully in DB", "fail");

		}

		switchWindow();

		click(btn_active);

		Thread.sleep(1000);

		selectText(status, txt_status5);

//             click (btn_changeperiod);
//	    	 
//	    	 click (btn_from);
//	    	  
//	    	 click (btn_fromdate);
//	    	 
//	    	 click (btn_to);
//	    	 
//	    	
//	    	 click (btn_todate);

//	    	 
		click(btn_apply);

		Thread.sleep(3000);

		Thread.sleep(3000);

		click(btn_update4);

		switchWindow();

		String TerminateStatus = getText(TerminateStatusValue);

		Thread.sleep(4000);

		System.out.println(TerminateStatus);

		trackCode = TerminateStatus;

		if (isDisplayed(updatepage)) {

			writeTestResults("Click on the update button", "User should be on Active Status",
					"User in Active Status successfully", "pass");
		}

		else {

			writeTestResults("Click on the update button", "User should not on Active Status",
					"User not oin Active Status successfully", "fail");

		}

	}

	// Terminate a inactive user
	public void terminateainactiveuser() throws Exception {
		click(btn_user);

		click(btn_edit1);

		if (isDisplayed(editpage)) {

			writeTestResults("Click on the 'Edit' button", "The edit mode should be enabled successfully",
					"edit mode should be enabled successfully", "pass");
		}

		else {

			writeTestResults("Click on the 'Edit' button", "The edit mode should not enabled successfully",
					"edit mode should not enabled successfully in DB", "fail");

		}

		switchWindow();

		click(btn_active);

		selectText(status, txt_status6);

//             click (btn_changeperiod);
//	    	 
//	    	 click (btn_from);
//	    	 
//	    	 if(isDisplayed(calander)) {
//					
//					writeTestResults("Enable the change period", "The change period should be enabled","The change period  enabled successfully","pass" );}
//				
//				    else {
//						
//					writeTestResults("Enable the change period", "The change period should not enabled","The change period not enabled successfully","fail");
//					
//					}
//	    	 
//	    	 
//	    	 click (btn_fromdate);
//	    	 
//	    	 click (btn_to);
//	    	 
//	    	 
//	    	 if(isDisplayed(calander)) {
//					
//					writeTestResults("Enable the change period", "The change period should be enabled","The change period  enabled successfully","pass" );}
//				
//				    else {
//						
//					writeTestResults("Enable the change period", "The change period should not enabled","The change period not enabled successfully","fail");
//					
//					}
//	    	 
//	    	 click (btn_todate);

//	    	 click (btn_apply);

		click(btn_apply);

		if (isDisplayed(Assignuserpopup)) {

			writeTestResults("Click on the 'Apply button",
					"The 'Apply' button should be clicked and 'Assign users' pop up should be loaded successfully",
					"'Assign users' pop up should be loaded successfully", "pass");
		}

		else {

			writeTestResults("Click on the 'Apply button",
					"The 'Apply' button should be clicked and 'Assign users' pop up should not loaded successfully",
					"'Assign users' pop up should not loaded successfully", "fail");

		}

		click(btn_apply2);

		Thread.sleep(3000);

		click(btn_update4);

		switchWindow();

		String TerminateStatus = getText(TerminateStatusValue);

		Thread.sleep(4000);

		System.out.println(TerminateStatus);

		trackCode = TerminateStatus;

		if (isDisplayed(updatepage)) {

			writeTestResults("Click on the update button", "User should be on Terminated Status",
					"User in Terminated Status successfully", "pass");
		}

		else {

			writeTestResults("Click on the update button", "User should not on Terminated Status",
					"User not oin Terminated Status successfully", "fail");

		}

	}

	// print template
	public void btn_printtemplatesetup() throws Exception {

		click(btn_printtemplatesetup);

		if (isDisplayed(printtemplatesetuppage)) {

			writeTestResults("Click on the print template set up", "The print template set up form should be loaded",
					"The print template set up form is displayed", "pass");
		} else {

			writeTestResults("Click on the print template set up", ".The print template set up form should not loaded",
					"The print template set up form not displayed", "fail");
		}
		click(btn_print);

		selectText(selectbalancinglevel, txt_selectbalancinglevel);

		click(btn_new);

		if (isDisplayed(addnewtemplatepage)) {

			writeTestResults("Click on the 'New' button", " The 'Add new Template' hedaer should be available",
					"Add new Template is displayed successfully", "pass");
		} else {

			writeTestResults(".Click on the 'New' button", " The 'Add new Template' hedaer should not available",
					"Add new Templatepage is not displayed", "fail");
		}

		LocalTime myObj = LocalTime.now();

		String a = txt_templatename + myObj;

		sendKeys(templatename, a);

		sendKeys(description, txt_description);

		selectText(form, txt_form);

		Thread.sleep(1000);

		selectText(datasourse, txt_datasourse);

		selectText(emailtemplate, txt_emailtemplate);

		Thread.sleep(1000);

		click(btn_update5);

		pageRefersh();

		Thread.sleep(2000);

		String val = Printtemplate.replace("Test1", a);

		System.out.println(val);

		Thread.sleep(4000);

		if (isDisplayed(val)) {

			writeTestResults("Click on the update button",
					"Update button should be clicked and data should be saved in DB", "Data saved successfully in DB",
					"pass");
		}

		else {

			writeTestResults("Click on the update button",
					"Update button should not clicked and data should not saved in DB",
					"Data not saved successfully in DB", "fail");

		}

	}
	// Cl

	public void btn_report() throws Exception {

		click(btn_printtemplatesetup);

		if (isDisplayed(printtemplatesetuppage)) {

			writeTestResults("verify printtemplatesetuppage", "view printtemplatesetuppage",
					"printtemplatesetuppage is displayed", "pass");
		} else {

			writeTestResults("verify printtemplatesetuppage", "view printtemplatesetuppage",
					"printtemplatesetuppage is not displayed", "fail");
		}
		click(btn_report);

		selectText(selectbalancinglevel, txt_selectbalancinglevel);

		selectText(datasourse1, txt_datasourse1);

		click(btn_new);

		if (isDisplayed(addnewtemplatepage)) {

			writeTestResults("verify addnewtemplatepage", "view addnewtemplatepage", "addnewtemplatepage is displayed",
					"pass");
		} else {

			writeTestResults("verify addnewtemplatepage", "view addnewtemplatepage",
					"addnewtemplatepage is not displayed", "fail");
		}

		LocalTime myObj = LocalTime.now();

		String a = txt_templatename + myObj;

		sendKeys(templatename, a);

		sendKeys(description, txt_description);

		click(btn_user1);

		click(btn_username2);

		click(btn_update5);

		Thread.sleep(2000);

		String val = Printtemplate.replace("Test1", a);

		System.out.println(val);

		Thread.sleep(3000);

		if (isDisplayed(val)) {

			writeTestResults("Click on the update button",
					"Update button should be clicked and data should be saved in DB", "Data saved successfully in DB",
					"pass");
		}

		else {

			writeTestResults("Click on the update button",
					"Update button should not clicked and data should not saved in DB",
					"Data not saved successfully in DB", "fail");

		}

	}

	public void workflowconfig() throws Exception {

		click(btn_workflowconfiguration);

		if (isDisplayed(workflowconfigurationpage)) {

			writeTestResults("verify workflowconfigurationpage", "view workflowconfigurationpage",
					"workflowconfigurationpage is displayed", "pass");
		} else {

			writeTestResults("verify workflowconfigurationpage", "view workflowconfigurationpage",
					"workflowconfigurationpage is not displayed", "fail");
		}

		click(btn_newworkflow);
		if (isDisplayed(newworkflowpage)) {

			writeTestResults("verify newworkflowpage", "view newworkflowpage", "newworkflowpage is displayed", "pass");
		} else {

			writeTestResults("verify newworkflowpage", "view wnewworkflowpage", "newworkflowpage is not displayed",
					"fail");
		}
		LocalTime myObj = LocalTime.now();
		sendKeys(workflowname, txt_workflowname + myObj);

		selectText(formname, txt_formname);

//		 	selectText(journeyname1,txt_journeyname1);
//		 	

		selectText(action, txt_action);

		selectText(fromaction, txt_fromaction);

		click(btn_create);

	}

	// schedule template

	public void btn_scheduletemplate() throws Exception {

		click(btn_scheduletemplate);

		if (isDisplayed(scheduletemplatepage)) {

			writeTestResults("verify scheduletemplatepage", "view scheduletemplatepage",
					"scheduletemplatepage is displayed", "pass");
		} else {

			writeTestResults("verify scheduletemplatepage", "view scheduletemplatepage",
					"scheduletemplatepage is not displayed", "fail");
		}

		click(btn_newscheduletemplate);

		if (isDisplayed(newscheduletemplatepage)) {

			writeTestResults("verify newscheduletemplatepage", "view newscheduletemplatepage",
					"newscheduletemplatepage is displayed", "pass");
		} else {

			writeTestResults("verify newscheduletemplatepage", "view newscheduletemplatepage",
					"newscheduletemplatepage is not displayed", "fail");
		}

		LocalTime myObj = LocalTime.now();

		sendKeys(templatecode, txt_templatecode + myObj);

		sendKeys(templatedescription, txt_templatedescription);

		click(btn_Recurringseriesoftask);

		click(btn_daily);

		click(btn_draft);

		if (isDisplayed(draftscheduletemplatepage)) {

			writeTestResults("verify drafted schedule template page", "view drafted schedule template page",
					"Drafted schedule template page is displayed", "pass");
		} else {

			writeTestResults("verify drafted schedule template page", "view drafted schedule template page",
					"Drafted schedule template page is not displayed", "fail");
		}

		Thread.sleep(3000);

		click(btn_releasest);

		Thread.sleep(6000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(releasesecheduletemplatepage)) {

			writeTestResults("Click on the Released", ".The document should be on Released status",
					"The document released successfully", "pass");
		} else {

			writeTestResults("Click on the Released", ".The document should be on Released status",
					"The document not released successfully", "fail");
		}
	}

	// journey config

	public void btn_journeyconfiguration() throws Exception {

		click(btn_journeyconfiguration);

		if (isDisplayed(journeyconfigurationpage)) {

			writeTestResults("Click on the Journey Configuration set up",
					"The 'Journey Configurations' form should be loaded", "The 'Journey Configurations' form displayed",
					"pass");

		} else {

			writeTestResults("Click on the Journey Configuration set up",
					"The 'Journey Configurations' form should not loaded",
					"The 'Journey Configurations' form is not displayed", "fail");

		}

		switchWindow();

	}

	public void btn_WIPrequest1() throws Exception {

		doubleClick(btn_WIPrequest);

		if (isDisplayed(WIPrequestpage)) {

			writeTestResults(".Find the WIP Requests (Production) journary",
					"The user should be on the WIP Requests (Production) journary",
					"WIP Requests (Production) journary is displayed successfully", "pass");
		} else {

			writeTestResults(".Find the WIP Requests (Production) journary",
					"The user should not on the WIP Requests (Production) journary", "WIPrequestpage is not displayed",
					"fail");
		}

		selectText(stockreservation, txt_stockreservation);

		click(btn_update6);

		if (isDisplayed(journeyupdatepage)) {

			writeTestResults("Click on the update button",
					"Update button should be clicked and data should be saved in DB", "Data saved successfully in DB",
					"pass");
		}

		else {

			writeTestResults("Click on the update button",
					"Update button should not clicked and data should not saved in DB",
					"Data not saved successfully in DB", "fail");

		}

	}

	// super user can send reset password link
	public void btn_action() throws Exception {

		Thread.sleep(3000);
		click(btn_action);

		Thread.sleep(2000);

		click(btn_resetpassword);

		if (isDisplayed(resetpasswordpopup)) {

			writeTestResults("Click on the 'Re set password' from the Actions drop down",
					" Can able to view resetpasswordpopup", "resetpasswordpopup is displayed successfully", "pass");
		} else {

			writeTestResults("Click on the 'Re set password' from the Actions drop down",
					"Cant able to view resetpasswordpopup", "resetpasswordpopup is not displayed", "fail");
		}
		click(btn_yes);
	}

	// sustitute
	public void maintainsubstitue() throws Exception {
		Thread.sleep(3000);
		click(btn_action);

		click(btn_maintainsubstitue);

		if (isDisplayed(vactionsubstituepopup)) {

			writeTestResults(" Click on the 'Maintain subsitiude' from the Actions drop down",
					"The maintain subsituite pop up should be loaded",
					"The maintain subsituite pop up should be loaded successfully", "pass");
		} else {

			writeTestResults(" Click on the 'Maintain subsitiude' from the Actions drop down",
					"The maintain subsituite pop up should not loaded",
					"The maintain subsituite pop up should not loaded successfully", "fail");
		}
		click(btn_isvaction);

		click(btn_search);
//		 		
//		 		if(isDisplayed(assignuserpopup)) {
//		 			
//		 			 writeTestResults("Find the user", "The user should be selected", "The user should be selected successfully", "pass");
//		 		}else {
//		 			
//		 			writeTestResults("Find the user", "The user should be selected", "The user should not selected", "fail");
//		 		}

	}

//	 	search user2		
	public void searchuser2() throws Exception {

		selectText(template, txt_Template);

		Thread.sleep(1000);

		sendKeys(searchuser01, "rashi09@yopmail");

		pressEnter(searchuser01);

		// Thread.sleep(1000);

//		 if(isDisplayed(assignuserpopup)) {
//	 			
// 			 writeTestResults("Find the user", "The subsituter should be selected", "The subsituter should be selected successfully", "pass");
// 		}else {
// 			
// 			writeTestResults("Find the user", "The subsituter should be selected", "The subsituter should not selected", "fail");
// 		}
// 		
		Thread.sleep(1000);

		doubleClick(btn_usernow);

		click(btn_startdate);

		click(btn_selectastartdate);

		click(btn_enddate);

		click(btn_selectenddate);

		Thread.sleep(1000);

		click(btn_applynow);

		if (isDisplayed(updatedUserinformationPage)) {

			writeTestResults(".Click on the 'Apply' button", "The data should be saved to the DB",
					"The data  saved to the DB successfully ", "pass");
		} else {

			writeTestResults(".Click on the 'Apply' button", "The data should not saved to the DB",
					"The data  not saved to the DB successfully ", "fail");
		}

	}

	// search work flow
	public void searchuserworkflow() throws Exception {

		click(btn_workflowconfiguration);

		if (isDisplayed(workflowconfigurationpage)) {

			writeTestResults("verify workflowconfigurationpage", "view workflowconfigurationpage",
					"workflowconfigurationpage is displayed", "pass");
		} else {

			writeTestResults("verify workflowconfigurationpage", "view workflowconfigurationpage",
					"workflowconfigurationpage is not displayed", "fail");
		}

		// selectText(workflowname,txt_workflowname);
//		 		LocalTime myObj = LocalTime.now();
//		 		sendKeys(workflowname,txt_workflowname+myObj);
		sendKeys(searchuserworkflow, txt_workflowname);

		click(btn_workflownameapper.replace("name", txt_workflowname));

		if (isDisplayed(workflowinformationpage)) {

			writeTestResults("verify workflowinformationpage", "view workflowinformation page",
					"workflowconfiguration page is displayed", "pass");
		} else {

			writeTestResults("verify workflowinformationpage", "view workflowinformation page",
					"workflowconfiguration page is not displayed", "fail");
		}

		click(btn_editworkflow);

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$('#svgWFDesigner').find('#1').click();");

		click(btn_newapprovalprocess);

		if (isDisplayed(addeditapprovalprocesspage)) {

			writeTestResults("Select the 'New approval process'",
					"The new approval process should be loaded successfully",
					"The new approval process  loaded successfully", "pass");

		} else {

			writeTestResults("Select the 'New approval process'",
					"The new approval process should be loaded successfully",
					"The new approval process loaded successfully", "fail");
		}

		Thread.sleep(1000);
		LocalTime myObj = LocalTime.now();
		sendKeys(displayname, txt_displayname + myObj);

		sendKeys(noofapprovals, txt_noofapprovals);

		click(btn_sendmail2);

		click(btn_lookup);
		Thread.sleep(2000);

		selectText(template, txt_Template);
		Thread.sleep(2000);
		sendKeys(searchuser, txt_SearchUserstatus);

		Thread.sleep(3000);

		pressEnter(searchuser);

		Thread.sleep(1000);

		if (isDisplayed(username.replace("username", txt_SearchUserstatus))) {

			writeTestResults("verify the user", "user should be displayed", "user is displayed successfully", "pass");
		}

		else {

			writeTestResults("verify the user", "user should not displayed", "user is not displayed", "fail");

		}
		// click(btn_user);

		Thread.sleep(1000);

		doubleClick(btn_user);

		Thread.sleep(1000);

		click(btn_udatetosendmail);

		if (isDisplayed(workflowupdatepage)) {

			writeTestResults("Click on the update button", "The workflow should be updated succesfully",
					"The workflow  updated succesfully", "pass");
		}

		else {

			writeTestResults("Click on the update button", "The workflow should not updated succesfully",
					"The workflow not updated succesfully ", "fail");
		}

		pageRefersh();

//			click(btn_workflowconfiguration);
//
//	 		if(isDisplayed(workflowconfigurationpage)) {
//	 			
//	 			 writeTestResults("verify workflowconfigurationpage", "view workflowconfigurationpage", "workflowconfigurationpage is displayed", "pass");
//	 		}else {
//	 			
//	 			writeTestResults("verify workflowconfigurationpage", "view workflowconfigurationpage", "workflowconfigurationpage is not displayed", "fail");
//	 		}
//    

		sendKeys(searchuserworkflow, txt_workflowname);

		click(btn_workflownameapper.replace("name", txt_workflowname));

		if (isDisplayed(workflowinformationpage)) {

			writeTestResults("verify workflowinformationpage", " workflowinformationpage should be open succesfully",
					"workflowconfigurationpage is displayed", "pass");

		} else {

			writeTestResults("verify workflowinformationpage", " workflowinformationpage should not open succesfully",
					"workflowconfigurationpage is not displayed", "fail");
		}

		click(btn_activeflow);

		if (isDisplayed(confirmationpopup)) {

			writeTestResults("Click on the activeflow button",
					"The workflowconfimation pop up should be open succesfully", "The workflow  updated succesfully",
					"pass");
		}

		else {

			writeTestResults("Click on activeflow button",
					"The workflowconfimation pop up  should not open succesfully",
					"The workflow not updated succesfully ", "fail");
		}

		click(btn_activeyes);

		Thread.sleep(2000);

		click(btn_navigationmenu);

		Thread.sleep(2000);

		click(btn_service);

		ServiceModule S2 = new ServiceModule();

		S2.navigationmenu();

		Thread.sleep(2000);

		S2.CreateNewCaseReversed();

		Thread.sleep(2000);

	}

	// search user for sustitute
	public void searchuserforsubstitute() throws Exception {

		selectText(template, txt_Template);

		sendKeys(searchuser, txt_SearchUser3);

		pressEnter(searchuser);

		Thread.sleep(1000);

		if (isDisplayed(username.replace("username", txt_SearchUser3))) {

			writeTestResults("Click on the search button",
					"The search button should be clicked and data should be dispalyed",
					"user is displayed successfully", "pass");
		}

		else {

			writeTestResults("Click on the search button",
					"The search button should be clicked and data should not dispalyed",
					"user is not displayed successfully", "fail");

		}
		click(btn_user);

	}
	/////////////////

	public void searchuserforstatus2() throws Exception {

		selectText(template, txt_Templatestatus);

		sendKeys(searchuser, txt_SearchUserstatus);

		pressEnter(searchuser);

		Thread.sleep(1000);

		if (isDisplayed(username.replace("username", txt_SearchUserstatus))) {

			writeTestResults("Click on the serach button",
					"The search button should be clicked and data should be dispalyed",
					"user is displayed successfully", "pass");
		}

		else {

			writeTestResults("Click on the serach button",
					"The search button should be clicked and data should not dispalyed",
					"user is not displayed successfully", "fail");

		}

		click(btn_user);

	}

	// user validate for tc 23
	public void uservalidate2() throws Exception {
		sendKeys(mailid, "bileeta.automation@gmail.com");

		click(btn_next1);

		if (isDisplayed(mailpasswordpopup)) {

			writeTestResults("Click on the btn_next1", "The mailpasswordpopup should be displayed",
					"mailpasswordpopup is displayed successfully", "pass");
		}

		else {

			writeTestResults("Click on the btn_next1", "The mailpasswordpopup should not displayed",
					"mailpasswordpopup is not displayed ", "fail");
		}

		// login to mail with password

		sendKeys(mailpassword, "Bileeta123");

		click(btn_next2);

		Thread.sleep(2000);

//            if (isDisplayed(mailpage)) {
//				
//    			writeTestResults("Then go to the email address", "The user should be on his email", "user navigated to the mail", "pass");
//    		}
//    			
//    			else
//   			{
//    				writeTestResults("Then go to the email address", "The user should not on his email", "user not navigate to the mail", "fail");
//
//    			}
//          
//            
//           if ((pendingApproval())) {
//				
//    			writeTestResults("Verify that user can received mails from activted workflow successfully", "The user have received mails from activted workflow successfully", "view the mail successfully", "pass");
//    		}
//    			
//    			else
//    			{
//    				writeTestResults("Verify that user can received mails from activted workflow successfully", "The user have  not received mails from activted workflow successfully", " unable to view the mail successfully", "fail");
//
//    			}
//            
	}

	///// service module

	public void btn_service1() throws Exception {

//	 		Thread.sleep(1000);

		click(btn_service);

		click(btn_case);

		if (isDisplayed(casepage)) {

			writeTestResults("Click on the user btn_case", "casepage page should be loaded successfully",
					"casepage is displayed successfully ", "pass");
		} else {

			writeTestResults("Click on the user btn_case", ".casepage page should not loaded successfully",
					"casepage is not displayed", "fail");

		}

	}

	public void searchuserforcase() throws Exception {

		selectText(template, txt_Template);

		sendKeys(searchuser, "CASE103");

		pressEnter(searchuser);

		Thread.sleep(1000);

		if (isDisplayed(username.replace("username", "CASE103"))) {

			writeTestResults("Click on the search button",
					"The search button should be clicked and data should be dispalyed",
					"user is displayed successfully", "pass");
		}

		else {

			writeTestResults("Click on the search button",
					"The search button should be clicked and data should not dispalyed",
					"user is not displayed successfully", "fail");

		}

	}

	public void userLogintotask() throws Exception {

		sendKeys(UserNameDatatask, "bileeta.automation@gmail.com");

		sendKeys(PasswordDatatask, "123456");

		click(btn_login);

		click(btn_task1);

		if (isDisplayed(taskpage)) {

			writeTestResults("Click on the tasks/ events tile", "The user should be on task form",
					"The user  on task form successfully", "pass");
		}

		else {
			writeTestResults("Click on the tasks/ events tile", "The user should not on task form",
					"The user not on task form successfully", "fail");

		}
		if (isDisplayed("//div[@id='divNotificationsContent'][text()='No Record(s) Found']")) {

			writeTestResults("Approval Process", "There is no approval", "There is no approval", "fail");
		}

		else {
			writeTestResults("Approval Process", "There is approval", "There is approval", "pass");
		}
		click(btn_Approval);

		Thread.sleep(1000);

		if (isDisplayed(approvalpage)) {

			writeTestResults("Click on the 'Approvals' tab", "The user should be on the 'Approvals' tab",
					"The user should  on the 'Approvals' tab successfully", "pass");
		}

		else {
			writeTestResults("Click on the 'Approvals' tab", "The user should not on the 'Approvals' tab",
					"The user not on the 'Approvals' tab successfully", "fail");

		}

		click(btn_arrow);

		if (isDisplayed(approvalrequestpage)) {

			writeTestResults("Clik on the 'Arrow' icon", "The relevent doucment should be opened successfully",
					"The relevent doucment viewed successfully", "pass");
		}

		else {
			writeTestResults("Clik on the 'Arrow' icon", "The relevent doucment should be opened successfully",
					"The relevent doucment viewed successfully", "fail");

		}
		sendKeys(remark, "txt_remark");

		click(btn_ok);
	}

//TC 27
	public void scheduletemplatetask() throws Exception {

		click(btn_entution);

		Thread.sleep(1000);

		click(btn_rolecentre);

		if (isDisplayed(rolecentrepage)) {

			writeTestResults("Click on Role centre", "The  user should be on Role centre page",
					"Role centre page is displayed successfully", "pass");
		}

		else {
			writeTestResults("Click on Role centre", "The  user should not on Role centre page",
					"Role centre page not displayed successfully", "fail");

		}

		click(btn_ScheduledJobs);

		Thread.sleep(3000);

		switchWindow();

		if (isDisplayed(ScheduledJobspage)) {

			writeTestResults("Move to the 'Scheduled Jobs'", "The user should be on 'Scheduled Jobs'",
					"Scheduled Jobs page is displayed successfully", "pass");
		}

		else {
			writeTestResults("Move to the 'Scheduled Jobs'", "The user should not on 'Scheduled Jobs'",
					"Scheduled Jobs page not displayed successfully", "fail");

		}

		click(btn_expand);

		Thread.sleep(1000);

		click(btn_selectall);

//	       click (btn_run);	 
//	       
//	       if (isDisplayed(message)) {
//				
//				writeTestResults(".Verify that user created shedule template is working when it used successfully'", "The schedule template should be worked successfully", "The schedule template  worked successfully", "pass");
//			}
//			
//			else
//			{
//				writeTestResults(".Verify that user created shedule template is not working when it used successfully", "The schedule template should be worked successfully", "The schedule template not worked successfully", "fail");
//
//			}

		Thread.sleep(3000);

	}

	// tc 25
	public void taskremoving() throws Exception {

		sendKeys(UserNameDatatask, "bileeta.automation@gmail.com");

		sendKeys(PasswordDatatask, "123456");

		click(btn_login);

		click(btn_task1);

		if (isDisplayed(taskpage)) {

			writeTestResults("Click on the tasks/ events tile", "The user should be on task form",
					"The user  on task form successfully", "pass");
		}

		else {
			writeTestResults("Click on the tasks/ events tile", "The user should not on task form",
					"The user not on task form successfully", "fail");

		}

		click(btn_Approval);

		Thread.sleep(1000);

		if (isDisplayed(approvalpage)) {

			writeTestResults("Click on the 'Approvals' tab", "The user should be on the 'Approvals' tab",
					"The user should  on the 'Approvals' tab successfully", "pass");
		}

		else {
			writeTestResults("Click on the 'Approvals' tab", "The user should not on the 'Approvals' tab",
					"The user not on the 'Approvals' tab successfully", "fail");

		}

//			 selectText(tasktemplate,txt_taskTemplate);

		Thread.sleep(1000);

		sendKeys(searchtask, txt_searchtask);

		if (isDisplayed(txt_searchtask)) {

			writeTestResults(
					"Verify that approval task is removing after approved, from the other same level approvals when 1 approval is required",
					"The system should not be removed the approval task is removing after approved, from the other same level approvals when 1 approval is required",
					"approval task is not removed successfully", "fail");
		}

		else {
			writeTestResults(
					"Verify that approval task is removing after approved, from the other same level approvals when 1 approval is required",
					"The system should be removed the approval task is removing after approved, from the other same level approvals when 1 approval is required",
					"approval task is removed successfully", "pass");
		}

	}

	// yopmail user
	public void searchuserforstatusnew() throws Exception {

		selectText(template, txt_Templatestatus);

		sendKeys(searchuser, txt_SearchUserstatusnew);

		pressEnter(searchuser);

		Thread.sleep(1000);

		if (isDisplayed(username.replace("username", txt_SearchUserstatusnew))) {

			writeTestResults("Click on the serach button",
					"The search button should be clicked and data should be dispalyed",
					"user is displayed successfully", "pass");
		}

		else {

			writeTestResults("Click on the serach button",
					"The search button should be clicked and data should not dispalyed",
					"user is not displayed successfully", "fail");

		}

		click(btn_user);

	}

	public void userLoginwithnewpassword() throws Exception {
		sendKeys(txt_username, UserNameData);

		sendKeys(txt_password, AdministrationModule.readAdminData(0));

//		sendKeys(txt_password, txt_NewPassword);

		click(btn_login);
		Thread.sleep(4000);
		if (isDisplayed(lnk_home)) {

			writeTestResults("User login", "User should navigate to the Home page of Entution",
					"user navigated to the home page", "pass");
		}

		else {
			writeTestResults("User login", "User should navigate to the Home page of Entution",
					"user cant navigate to the home page", "fail");

		}
	}

	public void CurrencyInformation() throws Exception {

		click(btn_CurrencyInformation);
		Thread.sleep(1000);

		if (isDisplayed(CurrencyInformationPage)) {

			writeTestResults("Click CurrencyInformation Button", "User should navigate to the CurrencyInformation page",
					"user navigated to the Currency Information page", "pass");
		}

		else {
			writeTestResults("Click CurrencyInformation Button", "User should navigate to the CurrencyInformation page",
					"user not navigated to the Currency Information page", "fail");

		}
		Thread.sleep(1000);

		click(ticktoactivate);

		Thread.sleep(1000);

		click(btn_updateCurrencyInformationpage);

		Thread.sleep(2000);

		if (isDisplayed("//*[@id=\"tblCurrency\"]/tbody/tr[1]/td[8]/input")) {

			writeTestResults("Click CurrencyInformation Update Button", "User should update the page",
					"User Updated the currencyinformation page", "pass");
		}

		else {
			writeTestResults("Click CurrencyInformation Update Button", "User should not update the page",
					"User cant Updated the currencyinformation page", "fail");

		}
	}

	public void ActionUserInformation() throws Exception {

		selectText(template, txt_Template);

		sendKeys(searchuser, "bileeta.automation@gmail.com");

		pressEnter(searchuser);

		Thread.sleep(3000);

		if (isDisplayed(username.replace("username", txt_SearchUser))) {

			writeTestResults("Click on the serach button",
					"The search button should be clicked and data should be dispalyed",
					"user is displayed successfully", "pass");
		}

		else {

			writeTestResults("Click on the serach button",
					"The search button should be clicked and data should not dispalyed",
					"user is not displayed successfully", "fail");

		}

		click(btn_user);

		Thread.sleep(2000);

		click(btn_action);

		click(btn_maintainsubstitue);

		if (isDisplayed(vactionsubstituepopup)) {

			writeTestResults(" Click on the 'Maintain subsitiude' from the Actions drop down",
					"The maintain subsituite pop up should be loaded",
					"The maintain subsituite pop up should be loaded successfully", "pass");
		} else {

			writeTestResults(" Click on the 'Maintain subsitiude' from the Actions drop down",
					"The maintain subsituite pop up should not loaded",
					"The maintain subsituite pop up should not loaded successfully", "fail");
		}

		Thread.sleep(2000);

		click(btn_closevactionsubstitute);

		click(btn_action);

		Thread.sleep(2000);

		click(btn_resetpassword);

		if (isDisplayed(resetpasswordpopup)) {

			writeTestResults("Click on the 'Re set password' from the Actions drop down",
					" Can able to view resetpasswordpopup", "resetpasswordpopup is displayed successfully", "pass");
		} else {

			writeTestResults("Click on the 'Re set password' from the Actions drop down",
					"Cant able to view resetpasswordpopup", "resetpasswordpopup is not displayed", "fail");
		}

		click(btn_closevactionsubstitute);
	}

	public void ActionUserPermission() throws Exception {

		click(btn_UserPermission);

		if (isDisplayed(UserPermissionPage)) {

			writeTestResults("Click on the UserPermission", " Can able to view UserPermission page",
					"resetpasswordpopup is displayed successfully", "pass");
		} else {

			writeTestResults("Click on the UserPermission' from the Actions drop down",
					"Cant able to view resetpasswordpopup", "resetpasswordpopup is not displayed", "fail");
		}

		click(btn_searchuser);

		sendKeys(user, "bileeta.automation@gmail.com");

		pressEnter(user);

		doubleClick("//*[@id=\"g1015-t\"]/table/tbody/tr/td[4]");

		selectText(modulename, txt_ModuleName);

		if (isDisplayed(modulename)) {

			writeTestResults(" Move to the  'Module Name' field", "The user should be on Moduke name area",
					"The user should be on Moduke name area sucessfully", "pass");
		}

		else {

			writeTestResults(" Move to the  'Module Name' field", "The user should not on Moduke name area",
					"The user should not on Moduke name area sucessfully", "fail");

		}

		Thread.sleep(3000);

		click(btn_AllowTemplatetoall);

		click(btn_AllowPrinttoall);

		click(btn_AllowReversetoall);

		click(btn_AllowAllReports);

		click(btn_AllowReleasetoall);

		Thread.sleep(4000);

		click(btn_update);

		Thread.sleep(4000);

		pageRefersh();

	}

	public void ActionReport() throws Exception {

		click(btn_Report);

		if (isDisplayed(UserReportPage)) {

			writeTestResults("Click on the Report", " Can able to view Report page",
					"Report page is displayed successfully", "pass");
		} else {

			writeTestResults("Click on the Report", "Cannot able to view Report page", "Report page is not displayed",
					"fail");
		}
	}

	public void AddressType() throws Exception {

		click(btn_Addresstype);

		if (isDisplayed(AddressTypePage)) {

			writeTestResults("Click on the Report", " Can able to view Report page",
					"Report page is displayed successfully", "pass");
		} else {

			writeTestResults("Click on the Report", "Cannot able to view Report page", "Report page is not displayed",
					"fail");
		}

		Thread.sleep(4000);

		click("//a[@class='button']");
	}

	public void IntegrationServiceSetup() throws Exception {

		click(btn_IntegrationServiceSetup);

		if (isDisplayed(IntegrationServiceSetupPage)) {

			writeTestResults("Click on the IntegrationServiceSetup", " Can able to view IntegrationServiceSetup page",
					"IntegrationServiceSetup is displayed successfully", "pass");
		} else {

			writeTestResults("Click on the IntegrationServiceSetup", "Cannot able to view IntegrationServiceSetup page",
					"IntegrationServiceSetup page is not displayed", "fail");
		}

		Thread.sleep(2000);

		click(btn_editpencil);

		Thread.sleep(2000);

		click(btn_IntegrationServiceSetupupdate);
	}

	public void PermissionGroup() throws Exception {

		click(btn_PermissionGroup);

		Thread.sleep(3000);

		switchWindow();

		click(btn_NewPermissionGroup);

		Thread.sleep(1000);

		if (isDisplayed(PermissionGroupPage)) {

			writeTestResults("Click on the PermissionGroup button", " Can able to view PermissionGroupPage",
					"PermissionGroupPage is displayed successfully", "pass");
		} else {

			writeTestResults("Click on the PermissionGroup button", "Cannot able to view PermissionGroupPage",
					"PermissionGroupPage  is not displayed", "fail");
		}

		LocalTime myObj = LocalTime.now();

		sendKeys(GroupCode, txt_Groupcode + myObj);

		sendKeys(GroupName, "new");

		click(btn_searchuserPermissionGroup);

		sendKeys(user, "bileeta.automation@gmail.com");

		pressEnter(user);

		doubleClick("//*[@id=\"g1015-t\"]/table/tbody/tr/td[4]");

		Thread.sleep(1000);

		click(btn_draft);

		if (isDisplayed(DraftedPermissionGroupPage)) {

			writeTestResults("Click on the Draft button", " Can able to view draft PermissionGroupPage",
					"Drafted PermissionGroupPage is displayed successfully", "pass");
		} else {

			writeTestResults("Click on the  Draft button", "Cannot able to view draft PermissionGroupPage",
					"Drafted PermissionGroupPage  is not displayed", "fail");
		}

		Thread.sleep(1000);

		click(btn_edit);

		sendKeys(GroupName, txt_GroupName2);

		click(btn_updateandnew);

		Thread.sleep(1000);

		click(btn_copyfrom);

		Thread.sleep(1000);

//	 		sendKeys(user,"IRE_PG");

		sendKeys("//input[@id='txtg1014']", "IRE_PG");

		pressEnter(user);

		doubleClick("//*[@id=\"g1015-t\"]/table/tbody/tr/td[4]");

		Thread.sleep(1000);

		LocalTime myObj1 = LocalTime.now();

		sendKeys(GroupCode, txt_Groupcode + myObj1);

		click(btn_draft);

	}

	public void Action_scheduletemplate() throws Exception {

		click(btn_scheduletemplate);

		if (isDisplayed(scheduletemplatepage)) {

			writeTestResults("verify scheduletemplatepage", "view scheduletemplatepage",
					"scheduletemplatepage is displayed", "pass");
		} else {

			writeTestResults("verify scheduletemplatepage", "view scheduletemplatepage",
					"scheduletemplatepage is not displayed", "fail");
		}

		click(btn_newscheduletemplate);

		if (isDisplayed(newscheduletemplatepage)) {

			writeTestResults("verify newscheduletemplatepage", "view newscheduletemplatepage",
					"newscheduletemplatepage is displayed", "pass");
		} else {

			writeTestResults("verify newscheduletemplatepage", "view newscheduletemplatepage",
					"newscheduletemplatepage is not displayed", "fail");
		}

		LocalTime myObj = LocalTime.now();

		sendKeys(templatecode, txt_templatecode + myObj);

		sendKeys(templatedescription, txt_templatedescription);

		click(btn_Recurringseriesoftask);

		click(btn_daily);

		click(btn_draftandnew);

		Thread.sleep(3000);

		if (isDisplayed(newscheduletemplatepage)) {

			writeTestResults("verify new schedule template page", "view new schedule template page",
					"New schedule template page is displayed", "pass");
		} else {

			writeTestResults("verify new schedule template page", "view new schedule template page",
					"New schedule template page is not displayed", "fail");
		}

		Thread.sleep(3000);

		LocalTime myObj1 = LocalTime.now();

		sendKeys(templatecode, txt_templatecode + myObj1);

		sendKeys(templatedescription, txt_templatedescription);

		click(btn_Recurringseriesoftask);

		click(btn_daily);

		Thread.sleep(2000);

		click(btn_draft);

		if (isDisplayed(draftscheduletemplatepage)) {

			writeTestResults("verify drafted schedule template page", "view drafted schedule template page",
					"Drafted schedule template page is displayed", "pass");
		} else {

			writeTestResults("verify drafted schedule template page", "view drafted schedule template page",
					"Drafted schedule template page is not displayed", "fail");
		}

		Thread.sleep(3000);

		click(btn_releasest);

		Thread.sleep(6000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(releasesecheduletemplatepage)) {

			writeTestResults("Click on the Released", ".The document should be on Released status",
					"The document released successfully", "pass");
		} else {

			writeTestResults("Click on the Released", ".The document should be on Released status",
					"The document not released successfully", "fail");
		}
	}

	public void Action_SystemPolicyConfigurationSetup() throws Exception {

		click(btn_SystemPolicyConfiguration);

		if (isDisplayed(SystemPolicyConfigurationSetuppage)) {

			writeTestResults("verify SystemPolicyConfigurationSetuppage", "view SystemPolicyConfigurationSetuppage",
					"SystemPolicyConfigurationSetup is displayed", "pass");
		} else {

			writeTestResults("verify SystemPolicyConfigurationSetuppage", "view SystemPolicyConfigurationSetuppage",
					"SystemPolicyConfigurationSetup is not displayed", "fail");
		}

		Thread.sleep(2000);

		click(btn_editpencil);

		Thread.sleep(2000);

		click(btn_viewpost);

		Thread.sleep(2000);

		click(btn_updatePolicyConfigurationSetuppage);

		Thread.sleep(2000);

		if (isDisplayed(UpdatedSystemPolicyConfigurationSetuppage)) {

			writeTestResults("verify Update button working", "Click update button",
					"Update button is working successfully", "pass");
		} else {

			writeTestResults("verify Update button working", "Click update button",
					"Update button is not working successfully", "fail");
		}

	}

	public void Action_BalancingLevelSetting() throws Exception {

		click(btn_BalancingLevelSettings);

		if (isDisplayed(BalancingLevelSettingPage)) {

			writeTestResults("verify BalancingLevelSettingaPage", "view BalancingLevelSettingaPage",
					"BalancingLevelSettingaPage is displayed", "pass");
		} else {

			writeTestResults("verify BalancingLevelSettingaPage", "view BalancingLevelSettingaPage",
					"BalancingLevelSettingaPage is not displayed", "fail");
		}

		Thread.sleep(2000);

		selectText(ProductCode, "Code and Description");

		Thread.sleep(2000);

		click(btn_productInformationupdate);

		Thread.sleep(2000);
	}

	public void Action_PasswordPolicies() throws Exception {

		click(btn_PasswordPolicies);

		if (isDisplayed(PasswordPoliciesPage)) {

			writeTestResults("verify PasswordPoliciesPage", "view PasswordPoliciesPage",
					"PasswordPoliciesPage is displayed", "pass");
		} else {

			writeTestResults("verify PasswordPoliciesPage", "view PasswordPoliciesPage",
					"PasswordPoliciesPage is not displayed", "fail");
		}

		Thread.sleep(2000);

	}

	public void Action_SharingGroup() throws Exception {

		click(btn_SharingGroup);

		if (isDisplayed(SharingGroupPage)) {

			writeTestResults("verify SharingGroupPage", "view SharingGroupPage", "SharingGroupPage is displayed",
					"pass");
		} else {

			writeTestResults("verify SharingGroupPage", "view SharingGroupPage", "SharingGroupPage is not displayed",
					"fail");
		}

		Thread.sleep(2000);

	}

	public void Action_TermsandCondition() throws Exception {

		Thread.sleep(2000);

		click(btn_TermsandConditions);

		if (isDisplayed(TermsandConditionsPage)) {

			writeTestResults("verify TermsandConditionsPage", "view TermsandConditionsPage",
					"TermsandConditionsPage is displayed", "pass");
		} else {

			writeTestResults("verify TermsandConditionsPage", "view TermsandConditionsPage",
					"TermsandConditionsPage is not displayed", "fail");
		}

		click(btn_NewTermsandCondition);

		Thread.sleep(2000);

		if (isDisplayed(NewTermsandConditionsPage)) {

			writeTestResults("verify NewTermsandConditionsPage", "view NewTermsandConditionsPage",
					"NewTermsandConditionsPage is displayed", "pass");
		} else {

			writeTestResults("verify NewTermsandConditionsPage", "view NewTermsandConditionsPage",
					"NewTermsandConditionsPage is not displayed", "fail");
		}

		selectText(BalancingLevel, "TRADE PROMOTERS (PVT) LTD");

		Thread.sleep(2000);

		LocalTime myObj1 = LocalTime.now();

		sendKeys(code, txt_code + myObj1);

		sendKeys(DescriptionCode, txt_DescriptionCode);

		Thread.sleep(2000);

		click(btn_UpdateandNew);

	}

}
