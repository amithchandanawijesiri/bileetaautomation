package bileeta.BTAF.PageObjects;

import java.awt.Window;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.w3c.dom.events.MouseEvent;

import bileeta.BATF.Pages.SalesAndMarketingModuleData;
import net.bytebuddy.utility.privilege.GetSystemPropertyAction;

public class SalesAndMarketingModule extends SalesAndMarketingModuleData {

	private static Workbook wb;
	private static Sheet sh;
	private static FileInputStream fis;
	private static FileOutputStream fos;
	private static Row row;
	private static Cell cell;
	private static Cell cell1;
	private static Cell cell2;

	public static float totInvSer;
	public static int randomNo = generateRandomIntIntRange(100, 200);
	public static String ruleCodeValue;
	public static int serialNo = generateRandomIntIntRange(1000, 1500);

	Map<String, String> document_map = new HashMap<String, String>();
	List<Map<String, String>> document = new ArrayList<Map<String, String>>();

	// 20
	public static int pricingModelCode = generateRandomIntIntRange(100, 200);

	// 25
	public static int vehicleNo = generateRandomIntIntRange(0, 9999);

	protected static String serialNumber;

	// 1
	public static String serialNoVal;
	public static String batchNoBatchSpecific;
	public static String serialNoSerialBatchSpec;

	public static String serialNoVal1;

	// 3
	public static String serialNoVal3;

	// 5
	public static String serialNoVal5;

	// 7
	public static String batchNoVal;
	public static String SerialBatchNo;

	public static String code1;

	// 12
	public static int serialDrop = generateRandomIntIntRange(100, 200);
	public static int lotNoValDirect = generateRandomIntIntRange(0, 1000);

	// 17
	public static int mobileLeadtVal = generateRandomIntIntRange(000001, 999999999);

	// 25
	public static String id1;

	//
	public static String SalesInvoiceID;
	public static String outBoundID;

	public static String beforeVal;

	public static String inboundShipmentID;

	public static String salesOrderID;

	public static String SO;
	public static String AOD;
	public static String SCD;

	public static String accountGroup;

	public static String unitPriceQuat;
	public static String unitPriceQuatAft;
	public static String Quat;
	public static String qtyValDuplicate;

	public static String warrantyPro;

	public void checkNavigateToTheLoginPage() throws Exception {
		openPage(siteURL);

//		writeTestResults("Verify that user is able to go to the login page",
//				"User should be able to see the logo", "Logo is dislayed", "Pass");

		if (isDisplayed(siteLogo)) {

			writeTestResults("Verify that 'Entution' header is available on the page",
					"User should be able to see the logo", "Logo is dislayed", "Pass");
		} else {
			writeTestResults("Verify that 'Entution' header is available on the page",
					"User should be able to see the logo", "Logo is not dislayed", "Fail");
		}
	}

	public void verifyTheLogo() throws Exception {
		if (isDisplayed(siteLogo)) {

			writeTestResults("Verify that 'Entution' header is available on the page",
					"User should be able to see the logo", "Logo is dislayed", "Pass");
		} else {
			writeTestResults("Verify that 'Entution' header is available on the page",
					"User should be able to see the logo", "Logo is not dislayed", "Fail");
		}
	}

	public void checkUserLogin() throws Exception {

		InventoryAndWarehouseModuleRegression obj = new InventoryAndWarehouseModuleRegression();
		obj.customizeLoadingDelay(txt_username, 20);
		sendKeys(txt_username, userNameData);
		Thread.sleep(2000);

		sendKeys(txt_password, passwordData);

		click(btn_login);

		Thread.sleep(5000);

		click(setProcessDate);

		if (isDisplayed(lnk_home)) {

			writeTestResults("User login", "User should navigate to the home page of Entution",
					"User navigated to the home page", "Pass");
		}

		else {
			writeTestResults("User login", "User should navigate to the home page of Entution",
					"User can't navigate to the home page", "Fail");

		}
	}

	public void checkNavigationMenu() throws Exception {

		Thread.sleep(2000);
		click(nav_btn);
		implisitWait(10);
		if (isDisplayed(sales_Marketing)) {
			writeTestResults("Verify that user can navigate to navigation menu", "User should be able to see the menu",
					"Navigation menu is dislayed", "Pass");
		} else {
			writeTestResults("Verify that user can navigate to navigation menu", "User should be able to see the menu",
					"Navigation menu is not dislayed", "Fail");
		}

	}

	public void checkSalesAndMarketingModule() throws Exception {

		Thread.sleep(3000);
		if (isDisplayed(salesAndMarketing)) {
			writeTestResults("Verify that Sale & Marketing module is available",
					"user should be able to see the 'Sales & Marketing'", "Sales & Marketing module is dislayed",
					"pass");
		} else {
			writeTestResults("Verify that Sale & Marketing module is available",
					"user should be able to see the 'Sales & Marketing'", "Sales & Marketing module is not dislayed",
					"Fail");
		}
	}

	public void checkAdminModule() throws Exception {

		if (isDisplayed(administration)) {
			writeTestResults("Verify that Sale & Marketing module is available",
					"user should be able to see the 'Sales & Marketing'", "Sales & Marketing module is dislayed",
					"pass");
		} else {
			writeTestResults("Verify that Sale & Marketing module is available",
					"user should be able to see the 'Sales & Marketing'", "Sales & Marketing module is not dislayed",
					"Fail");
		}
	}

	public void clickOnSalesAndMarketing() throws Exception {

		click(salesAndMarketing);
		Thread.sleep(2000);
		if (isDisplayed(searchBar)) {
			writeTestResults("Verify that user can navigate to sales sales & marketing menu",
					"User should be able to see the sales & marketing menu", "Sales & Marketing menu is dislayed",
					"Pass");
		} else {
			writeTestResults("Verify that user can navigate to sales sales & marketing menu",
					"User should be able to see the sales & marketing menu", "Sales & Marketing menu is not dislayed",
					"Fail");
		}
	}

	public void clickOnAdministration() throws Exception {

		click(administration);
		Thread.sleep(3200);
		if (isDisplayed(searchBar)) {
			writeTestResults("Verify that user can navigate to sales sales & marketing menu",
					"User should be able to see the sales & marketing menu", "Sales & Marketing menu is dislayed",
					"Pass");
		} else {
			writeTestResults("Verify that user can navigate to sales sales & marketing menu",
					"User should be able to see the sales & marketing menu", "Sales & Marketing menu is not dislayed",
					"Fail");
		}
		Thread.sleep(2000);
	}

	public void checkClickSalesOrderPage() throws Exception {

		if (isDisplayed(salesOrder)) {
			writeTestResults("Verify that 'Sales Order' is available", "User should be able to see the 'Sales Order'",
					"'Sales Order' is dislayed", "Pass");
			click(salesOrder);
			Thread.sleep(2000);
		} else {
			writeTestResults("Verify that 'Sales Order' is available", "User should be able to see the 'Sales Order'",
					"'Sales Order' is not dislayed", "Fail");
		}
	}

	public void checkSalesOrderHeader() throws Exception {

		if (isDisplayed(salesOrderHeader)) {
			writeTestResults("Verify that user is able to go to the sales order page",
					"User should be able to go to the sales order page", "Sales order page is not displayed", "Pass");
			click(salesOrder);
			Thread.sleep(2500);
		} else {
			writeTestResults("Verify that user is able to go to the sales order page",
					"User should be able to go to the sales order page", "Sales order page is displayed", "Fail");
		}
	}

	public void checkJourneySalesOrderToSalesInvoice() throws Exception {

		click(newSalesOrder);
		implisitWait(10);
		if (isDisplayed(journey1)) {
			writeTestResults("Verify the availability of 'Sales Order to Sales Invoice journey'",
					"User should be able to see the 'Sales Order to Sales Invoice' journey", "Journey is displayed",
					"Pass");
			click(journey1);
			Thread.sleep(2000);
		} else {
			writeTestResults("Verify the availability of 'Sales Order to Sales Invoice' journey",
					"User should be able to see the 'Sales Order to Sales Invoice' journey", "Journey is not displayed",
					"Fail");
		}
	}

	public void stockAdjustment() throws Exception {

		click(invenWarehouse);
		Thread.sleep(2000);
		click(stockAdjustment);
		Thread.sleep(3000);
		click(newStockAdjustment);
		Thread.sleep(3000);
		sendKeys(descriptionTxtStockAdj, descrptionValStockAdj);
		Thread.sleep(1000);
		selectText(warehouseStockAdj, warehouseVal1);

		InventoryAndWarehouseModule obj = new InventoryAndWarehouseModule();

		Thread.sleep(1000);
		// 1.Batch Specific

		sendKeys(txt_ProductA, obj.readTestCreation("BatchSpecific"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		sendKeys(txt_ProductQtyA, qtyVal);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(productLookUpBatchSpecific);
//		Thread.sleep(2000);
//		InventoryAndWarehouseModule obj = new InventoryAndWarehouseModule();
//		sendKeys(productValTxt, obj.readTestCreation("BatchSpecific"));
//		Thread.sleep(3500);
//		pressEnter(productValTxt);
//		Thread.sleep(4000);
//		
//		int a = 1;
//		while(isDisplayed(selectedProduct)) {
//			Thread.sleep(1000);
//			a++;
//			if(a==5) {
//				break;
//			}
//		}
//			
//		doubleClick(selectedProduct);
//		Thread.sleep(2000);
//		sendKeys(qtyTxtBatchSpecific, qtyVal);
//		Thread.sleep(2000);

		// 2.Batch Fifo

		sendKeys(txt_ProductA, obj.readTestCreation("BatchFifo"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		sendKeys(txt_ProductQtyA, qtyVal);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(addNewBatchFifo);
//		Thread.sleep(2000);
//		click(productLookUpBatchFifo);
//		Thread.sleep(2000);
//		sendKeys(productValTxt, obj.readTestCreation("BatchFifo"));
//		Thread.sleep(3500);
//		pressEnter(productValTxt);
//		Thread.sleep(4000);
//		doubleClick(selectedProduct);
//		Thread.sleep(2000);
//		sendKeys(qtyTxtBatchFifo, qtyVal);

		// 3.Lot

		sendKeys(txt_ProductA, obj.readTestCreation("SerielSpecific"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		sendKeys(txt_ProductQtyA, qtyVal);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		Thread.sleep(2000);
//		click(addNewLot);
//		Thread.sleep(1000);
//		click(productLookUpLot);
//		Thread.sleep(2000);
//		sendKeys(productValTxt, obj.readTestCreation("SerielSpecific"));
//		Thread.sleep(3500);
//		// System.out.println(obj.readTestCreation("SerielSpecific"));
//		Thread.sleep(2000);
//		pressEnter(productValTxt);
//		Thread.sleep(4000);
//		doubleClick(selectedProduct);
//		Thread.sleep(2000);
//		sendKeys(qtyTxtLot, qtyVal);

		// 4.Serial Specific

		sendKeys(txt_ProductA, obj.readTestCreation("SerielBatchSpecific"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		sendKeys(txt_ProductQtyA, qtyVal);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		Thread.sleep(2000);
//		click(addNewSerialSpecific);
//		Thread.sleep(1000);
//		click(productLookUpSerialSpecific);
//		Thread.sleep(2000);
//		sendKeys(productValTxt, obj.readTestCreation("SerielBatchSpecific"));
//		Thread.sleep(3500);
//		pressEnter(productValTxt);
//		Thread.sleep(4000);
//		doubleClick(selectedProduct);
//		Thread.sleep(2000);
//		sendKeys(qtyTxtSerialSpecific, qtyVal);

		// 5.Serial Batch Specific

		sendKeys(txt_ProductA, obj.readTestCreation("SerielBatchFifo"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		sendKeys(txt_ProductQtyA, qtyVal);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		Thread.sleep(2000);
//		click(addNewSerialBatchSpec);
//		Thread.sleep(1000);
//		click(productLookUpSerialBatchSpec);
//		Thread.sleep(2000);
//		sendKeys(productValTxt, obj.readTestCreation("SerielBatchFifo"));
//		Thread.sleep(3500);
//		pressEnter(productValTxt);
//		Thread.sleep(4000);
//		doubleClick(selectedProduct);
//		Thread.sleep(3000);
//		sendKeys(qtyTxtSerialBatchSpec, qtyVal);

		// 6.Serial Batch Fifo

		sendKeys(txt_ProductA, obj.readTestCreation("SerirlFifo"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		sendKeys(txt_ProductQtyA, qtyVal);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		Thread.sleep(2000);
//		click(addNewSerialBthFifo);
//		Thread.sleep(1000);
//		click(productLookUpSerialBatchFifo);
//		Thread.sleep(2000);
//		sendKeys(productValTxt, obj.readTestCreation("SerirlFifo"));
//		Thread.sleep(3000);
//		pressEnter(productValTxt);
//		Thread.sleep(4000);
//		doubleClick(selectedProduct);
//		Thread.sleep(2000);
//		sendKeys(qtyTxtSerialBatchFifo, qtyVal);
//		Thread.sleep(2000);

//		//7.Serial Fifo
//		Thread.sleep(2000);
//		click(addNewSeriFifo);
//		Thread.sleep(1000);
//		click(productLookUpSerialFifo);
//		Thread.sleep(3000);
//		sendKeys(productValTxt, obj.readTestCreation("SerirlFifo"));
//		Thread.sleep(2000);
//		pressEnter(productValTxt);
//		Thread.sleep(3000);
//		doubleClick(selectedProduct);
//		Thread.sleep(2000);
//		sendKeys(qtyTxtSerialFifo, qtyVal);
//		Thread.sleep(1000);

		click(draft);
		Thread.sleep(6000);
		click(serialBatch);
		Thread.sleep(6000);

		// 1.Batch Specific
		click(batchSpecificItem);
		Thread.sleep(3000);
		int batchNo = generateRandomIntIntRange(1, 1000);
		String batch = Integer.toString(batchNo);

		sendKeys(batchNoTxtBatchSpecific, batch);
		Thread.sleep(1000);
		int lotNo = generateRandomIntIntRange(1, 1000);
		String lot = Integer.toString(lotNo);
		Thread.sleep(3000);
		sendKeys(lotNoBatchSpecific, lot);
		Thread.sleep(1000);
		click(expiryDateBatchSpecific);
		Thread.sleep(2000);
		click(nextMonth);
		Thread.sleep(1000);
		click(expiryDateValBatchSpec);
//		Thread.sleep(3000);
		// selectText(yearCalender, 2023);
//		click(manufacDate);
//		Thread.sleep(3000);
//		pressEnter(manufacDate);
		Thread.sleep(3000);
		pressEnter(batchNoTxtBatchSpecific);
		Thread.sleep(2000);
		click(updateBtn);
		Thread.sleep(2000);

		// 2.Batch Fifo
		click(batchFifoItem);
		Thread.sleep(2000);
		int batchNo2 = generateRandomIntIntRange(1, 1000);
		String batch2 = Integer.toString(batchNo2);
		Thread.sleep(3000);
		sendKeys(batchNoTxtBatchSpecific, batch2);
		Thread.sleep(1000);
		int lotNo2 = generateRandomIntIntRange(1, 1000);
		String lot2 = Integer.toString(lotNo2);
		Thread.sleep(3000);
		sendKeys(lotNoBatchSpecific, lot2);
		click(expiryDateBatchSpecific);
		Thread.sleep(2000);
		click(nextMonth);
		Thread.sleep(1000);
		click(expiryDateValBatchSpec);
//		Thread.sleep(3000);
//		// selectText(yearCalender, 2023);
//		click(manufacDate);
//		Thread.sleep(3000);
//		pressEnter(manufacDate);
		Thread.sleep(3000);
		pressEnter(batchNoTxtBatchSpecific);
		Thread.sleep(2000);
		click(updateBtn);
		Thread.sleep(2000);

		// 3.Serial Specific

		click(serialSpecificItem);
		Thread.sleep(2000);

		click(rangeSerialSpecific);
		Thread.sleep(2000);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$('#attrSlide-txtSerialCount').val(200)");

		LocalTime myObj = LocalTime.now();
		sendKeys(serialNoSerialSpecific, "A" + myObj);
		Thread.sleep(2000);
		int lotNo4 = generateRandomIntIntRange(1, 1000);
		String lot4 = Integer.toString(lotNo4);
		Thread.sleep(3000);
		sendKeys(lotNoSerialSpecific, lot4);
		Thread.sleep(2000);
		click(expiryDateSerialSpecific);
		Thread.sleep(2000);
		click(nextMonth);
		Thread.sleep(1000);
		click(expiryDateValBatchSpec);
//		Thread.sleep(3000);
//
//		// selectText(yearCalender, 2023);
//		click(manufacDateSerialSpec);
//		Thread.sleep(3000);
//		pressEnter(manufacDateSerialSpec);
		Thread.sleep(3000);
		pressEnter(serialNoSerialSpecific);
		Thread.sleep(2000);
		click(updateBtn);
		Thread.sleep(2000);

		// 4.Serial Batch Specific

		click(serialBatchSpecificItem);
		Thread.sleep(2000);

		click(rangeSerialSpecific);
		Thread.sleep(2000);
		JavascriptExecutor jse5 = (JavascriptExecutor) driver;
		jse5.executeScript("$('#attrSlide-txtSerialCount').val(200)");

		LocalTime myObj5 = LocalTime.now();
		sendKeys(serialNoSerialSpecific, "A" + myObj5);
		Thread.sleep(2000);
		int lotNo5 = generateRandomIntIntRange(1, 1000);
		String lot5 = Integer.toString(lotNo5);

		int batchNo4 = generateRandomIntIntRange(1, 1000);
		String batch4 = Integer.toString(batchNo4);
		Thread.sleep(3000);
		sendKeys(batchNoTxtSerialBatchSpecific, batch4);
		Thread.sleep(1000);
		sendKeys(lotNoSerialSpecific, lot5);
		Thread.sleep(2000);
		click(expiryDateSerialSpecific);
		Thread.sleep(2000);
		click(nextMonth);
		Thread.sleep(1000);
		click(expiryDateValBatchSpec);
//		Thread.sleep(3000);
//
//		// selectText(yearCalender, 2023);
//		click(manufacDateSerialSpec);
//		Thread.sleep(3000);
//		pressEnter(manufacDateSerialSpec);
		Thread.sleep(3000);
		pressEnter(serialNoSerialSpecific);
		Thread.sleep(2000);
		click(updateBtn);
		Thread.sleep(2000);

		// 5.Serial Batch Fifo

		mouseMove(serialBatchFifoItem);
		click(serialBatchFifoItem);
		Thread.sleep(2000);

		click(rangeSerialSpecific);
		Thread.sleep(2000);
		JavascriptExecutor jse6 = (JavascriptExecutor) driver;
		jse6.executeScript("$('#attrSlide-txtSerialCount').val(200)");

		LocalTime myObj6 = LocalTime.now();
		sendKeys(serialNoSerialSpecific, "A" + myObj6);
		Thread.sleep(2000);
		int lotNo6 = generateRandomIntIntRange(1, 1000);
		String lot6 = Integer.toString(lotNo6);
		Thread.sleep(2000);
		int batchNo5 = generateRandomIntIntRange(1, 1000);
		String batch5 = Integer.toString(batchNo5);
		Thread.sleep(3000);
		sendKeys(batchNoTxtSerialBatchSpecific, batch5);
		Thread.sleep(1000);
		sendKeys(lotNoSerialSpecific, lot6);
		Thread.sleep(2000);
		click(expiryDateSerialSpecific);
		Thread.sleep(2000);
		click(nextMonth);
		Thread.sleep(1000);
		click(expiryDateValBatchSpec);

//		Thread.sleep(3000);
//		// selectText(yearCalender, 2023);
//		click(manufacDateSerialSpec);
//		Thread.sleep(3000);
//		pressEnter(manufacDateSerialSpec);
		Thread.sleep(3000);
		pressEnter(batchNoTxtSerialBatchSpecific);
		Thread.sleep(2000);
		click(updateBtn);
		Thread.sleep(2000);

		// 6.Serial Fifo
		mouseMove(serialFifoItem);
		click(serialFifoItem);
		Thread.sleep(1000);
		click(rangeSerialSpecific);
		Thread.sleep(2000);
		JavascriptExecutor jse7 = (JavascriptExecutor) driver;
		jse7.executeScript("$('#attrSlide-txtSerialCount').val(200)");

		LocalTime myObj7 = LocalTime.now();
		sendKeys(serialNoSerialSpecific, "A" + myObj7);
		Thread.sleep(2000);
		int lotNo7 = generateRandomIntIntRange(1, 1000);
		String lot7 = Integer.toString(lotNo7);
		Thread.sleep(3000);
		sendKeys(lotNoSerialSpecific, lot7);
		Thread.sleep(2000);
		click(expiryDateSerialSpecific);
		Thread.sleep(2000);
		click(nextMonth);
		Thread.sleep(1000);
		click(expiryDateValBatchSpec);
//		Thread.sleep(2000);
//
//		Thread.sleep(3000);
//		// selectText(yearCalender, 2023);
//		click(manufacDateSerialSpec);
//		Thread.sleep(3000);
//		pressEnter(manufacDateSerialSpec);
		Thread.sleep(3000);
		pressEnter(serialNoSerialSpecific);
		// sendKeys(lotNoText, lot7);
		click(updateBtn);
		Thread.sleep(2000);

		click(backBtn);
		Thread.sleep(2000);
		click(release);
		Thread.sleep(18000);
	}

	public void fillDetailsSalesOrderToSalesInvoice() throws Exception {

		// checkFieldsSalesOrderSalesInvoice();
		click(customerAccountSearch);
		Thread.sleep(2000);
		sendKeys(customerAccountText, customerAccVal);
		Thread.sleep(2000);
		pressEnter(customerAccountText);
		Thread.sleep(2000);
		doubleClick(selected_val);
		/*
		 * click(currencyDropdown); billing address and shipping address
		 * click(billingAddressSearch); click(billingAddressText);
		 * sendKeys(billingAddressText, billingAddrVal); click(shippingAddressText);
		 * sendKeys(shippingAddressText, shippingAddrVal); click(apply_btn);
		 */
		Thread.sleep(2000);
		click(salesUnit);
		selectIndex(salesUnit, 1);

		// 1 Service Product
		click(productLookup);
		Thread.sleep(2000);
		InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();
		String product = obj1.readTestCreation("Service");
		sendKeys(productTextSer, product);
		Thread.sleep(3500);
		pressEnter(productTextSer);
		Thread.sleep(3500);

		doubleClick(invSelectedVal);
		Thread.sleep(3000);

//		if (isEnabled(wareHouse)) {
//			writeTestResults("Verify that the warehouse field is disabled for service product",
//					"Warehouse field should be disabled", "Warehouse field is enabled", "Fail");
//		} else {
//			writeTestResults("Verify that the warehouse field is disabled for service product",
//					"Warehouse field should be disabled", "Warehouse field is disabled", "Pass");
//		}

		sendKeys(unitPriceTxtService, unitPriceVal);
		Thread.sleep(1000);
		sendKeys(qtyTextService, quantityVal);
		Thread.sleep(1000);

		// 2 Serial Specific
		click(addNewRecordSerialSpecific);
		Thread.sleep(2000);
		click(productLookupSerialSpec);
		Thread.sleep(3000);
		InventoryAndWarehouseModule obj2 = new InventoryAndWarehouseModule();
		sendKeys(productTextSerialSpec, obj2.readTestCreation("SerielSpecific"));
		Thread.sleep(3500);
		pressEnter(productTextSerialSpec);
		Thread.sleep(4000);
		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		selectText(wareHouseSerialSpecific, warehouseVal1);
		Thread.sleep(2000);

//		if (isEnabled(unitPriceSerialSpec)) {
//			writeTestResults("User should be able to add unit price", "Unit price field should be enabled",
//					"Unit price field is enabled", "Pass");
//		} else {
//			writeTestResults("User should be able to add unit price", "Unit price field should be enabled",
//					"Unit price field is disabled", "Fail");
//		}

		sendKeys(unitPriceSerialSpec, unitPriceValSerialSpec);
		Thread.sleep(2000);
		sendKeys(qtyTextSerialSpec, quantityValSerialSpec);
		Thread.sleep(1000);

		// 3 Batch Specific

		click(addNewBatchSpecific);
		Thread.sleep(2000);
		click(productLookupBatchSpecific);
		Thread.sleep(3000);
		InventoryAndWarehouseModule obj3 = new InventoryAndWarehouseModule();
		sendKeys(productTextInv, obj3.readTestCreation("BatchSpecific"));
		Thread.sleep(3000);
		pressEnter(productTextInv);
		Thread.sleep(4000);

		int b = 1;
		while (isDisplayed(invSelectedVal)) {
			Thread.sleep(1000);
			b++;
			if (b == 5) {
				break;
			}
		}

		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		selectText(wareHouseBatchSpecific, warehouseVal1);
		Thread.sleep(2000);
		sendKeys(unitPriceBatchSpecific, unitPriceValBatchSpec);
		Thread.sleep(1000);
		sendKeys(qtyTextBatchSpecific, quantityValInv);
		Thread.sleep(1000);

		// 4 Batch FIFO

		click(addNewBatchFIFO);
		Thread.sleep(2000);
		click(productLookupBatchFIFO);
		Thread.sleep(3000);
		InventoryAndWarehouseModule obj4 = new InventoryAndWarehouseModule();
		sendKeys(productTextInv, obj4.readTestCreation("BatchFifo"));
		Thread.sleep(3000);
		pressEnter(productTextInv);
		Thread.sleep(4000);
		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		selectText(wareHouseBatchFifo, warehouseVal1);
		Thread.sleep(2000);
		sendKeys(unitPriceBatchFifo, unitPriceValInv);
		Thread.sleep(1000);
		sendKeys(qtyTextBatchFifo, quantityValInv);
		Thread.sleep(1000);

		// 5 Serial Batch Specific

		click(addNewBatchSpecific);
		Thread.sleep(2000);
		click(productLookupSerialBatchSpecific);
		Thread.sleep(3000);
		InventoryAndWarehouseModule obj5 = new InventoryAndWarehouseModule();
		sendKeys(productTextInv, obj5.readTestCreation("SerielBatchSpecific"));
		Thread.sleep(3000);
		pressEnter(productTextInv);
		Thread.sleep(4000);
		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		selectText(wareHouseSerialBatchSpecific, warehouseVal1);
		Thread.sleep(2000);
		sendKeys(unitPriceSerialBatchSpec, unitPriceValInv);
		Thread.sleep(2000);
		sendKeys(qtyTxtSerialBatchSpecific, quantityValInv);
		Thread.sleep(1000);

		// 6 Serial Batch FIFO

		click(addNewSerialBatchFifo);
		Thread.sleep(2000);
		click(productLookupSerialBatchFifo);
		Thread.sleep(4000);
		InventoryAndWarehouseModule obj6 = new InventoryAndWarehouseModule();
		sendKeys(productTextInv, obj6.readTestCreation("SerielBatchFifo"));
		Thread.sleep(3000);
		pressEnter(productTextInv);
		Thread.sleep(4000);
		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		selectText(wareHouseSerialBatchFifo, warehouseVal1);
		Thread.sleep(2000);
		sendKeys(unitPriceSerialBatchFifo, unitPriceValInv);
		Thread.sleep(2000);
		sendKeys(qtyTextSerialBatchFifo, quantityValInv);
		Thread.sleep(1000);

		// 7 Serial FIFO

		click(addNewSerialFifo);
		Thread.sleep(2000);
		click(productLookupSerialFifo);
		Thread.sleep(4000);
		InventoryAndWarehouseModule obj7 = new InventoryAndWarehouseModule();
		sendKeys(productTextInv, obj7.readTestCreation("SerirlFifo"));
		Thread.sleep(3000);
		pressEnter(productTextInv);
		Thread.sleep(4000);
		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		selectText(wareHouseSerialFifo, warehouseVal1);
		Thread.sleep(2000);
		sendKeys(unitPriceSerialFifo, unitPriceValInv);
		Thread.sleep(2000);
		sendKeys(qtyTextSerialFifo, quantityValInv);
		Thread.sleep(1000);

//		click(batchSpecificSerialMenu);

		click(checkoutSalesOrder);
		Thread.sleep(3000);

		checkCalculationsSalesOrder1Journey();
		Thread.sleep(2000);
		click(draft);
		Thread.sleep(5000);
		String header = getText(releaseInHeader);

		SO = getText(documentVal);
		System.out.println(SO);
		trackCode = getText(documentVal);
		// System.out.println(trackCode);

		document_map.put("SalesOrderJourney1", trackCode);
		document.add(0, document_map);

		if (header.equals("(Draft)")) {
			writeTestResults("User should be able to draft the sales order", "Sales order should be drafted",
					"Sales order drafted", "Pass");

		} else {
			writeTestResults("User should be able to draft the sales order", "Sales order should be drafted",
					"Sales order not drafted", "Fail");
		}

		Thread.sleep(2000);
		click(menu);
		Thread.sleep(3000);
		click(serialWise);
		Thread.sleep(7000);
		serialNoVal = getText(serialTextVal);
		Thread.sleep(2000);
		click(serialMenuClose);
		Thread.sleep(3000);
		mouseMove(menuBatchSpecific);
		click(menuBatchSpecific);
		Thread.sleep(3000);
		click(serialWise);
		Thread.sleep(3000);
		batchNoBatchSpecific = getText(batchSpecificBatchNo);
		Thread.sleep(2000);
		click(serialMenuClose);
		Thread.sleep(2000);
		mouseMove(menuSerialBatchSpec);
		click(menuSerialBatchSpec);
		Thread.sleep(2000);
		click(serialWise);
		Thread.sleep(2000);
		serialNoSerialBatchSpec = getText(serialBatchSpec);
		Thread.sleep(2000);
		click(serialMenuClose);
		Thread.sleep(2000);
		click(serialBatch);
		Thread.sleep(4000);
		click(productListBtn);
		Thread.sleep(2000);
		sendKeys(serialText, serialNoVal);
		Thread.sleep(2000);
		pressEnter(serialText);
		Thread.sleep(2000);
		click(updateBtn);
		Thread.sleep(2000);
		click(batchSpecificListBtn);
		Thread.sleep(2000);
		sendKeys(batchNoText, batchNoBatchSpecific);
		Thread.sleep(2000);
		pressEnter(batchNoText);
		Thread.sleep(2000);
		click(updateBtn);
		Thread.sleep(2000);
		click(serialbatchSpecificListBtn);
		Thread.sleep(2000);
		sendKeys(batchNoTextSerialBatchSpecific, serialNoSerialBatchSpec);
		Thread.sleep(2000);
		pressEnter(batchNoTextSerialBatchSpecific);
		Thread.sleep(2000);
		click(updateBtn);
		Thread.sleep(2000);
		click(backBtn);
		Thread.sleep(2000);
		click(release);
		Thread.sleep(8000);

		writeTestCreations("SalesOrderJourney1", document.get(0).get("SalesOrderJourney1"), 0);

		if (isDisplayed(outboundLink)) {
			writeTestResults("User should be able to release the sales order", "Sales order should be released",
					"Sales order is released", "Pass");
			click(outboundLink);
			Thread.sleep(4000);
		} else {
			writeTestResults("User should be able to release the sales order", "Sales order should be released",
					"Sales order is not released", "Fail");
		}

	}

	public void fillDetailsSalesOrderToOutboundshipment() throws Exception {

		checkFieldsSalesOrderSalesInvoice();
		Thread.sleep(3000);
		click(customerAccountSearch);
		Thread.sleep(2000);
		sendKeys(customerAccountText, customerAccVal);
		Thread.sleep(2000);
		pressEnter(customerAccountText);
		Thread.sleep(2000);
		doubleClick(selected_val);
		/*
		 * click(currencyDropdown); billing address and shipping address
		 * click(billingAddressSearch); click(billingAddressText);
		 * sendKeys(billingAddressText, billingAddrVal); click(shippingAddressText);
		 * sendKeys(shippingAddressText, shippingAddrVal); click(apply_btn);
		 */
		Thread.sleep(2000);
		click(salesUnit);
		selectIndex(salesUnit, 1);
		InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();

		// 1 Service Product

		sendKeys(txt_ProductA, obj1.readTestCreation("Service"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, quantityVal);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(productLookup);
//		Thread.sleep(2000);
//		InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();
//		sendKeys(productTextSer, obj1.readTestCreation("Service"));
//		Thread.sleep(2000);
//		pressEnter(productTextSer);
//		Thread.sleep(3500);
//		doubleClick(invSelectedVal);
		Thread.sleep(3000);

		if (isEnabled(wareHouse)) {
			writeTestResults("Verify that the warehouse field is disabled for service product",
					"Warehouse field should be disabled", "Warehouse field is enabled", "Fail");
		} else {
			writeTestResults("Verify that the warehouse field is disabled for service product",
					"Warehouse field should be disabled", "Warehouse field is disabled", "Pass");
		}
		Thread.sleep(3000);
		sendKeys(unitPriceTxtService, unitPriceVal);
//		Thread.sleep(1000);
//		sendKeys(qtyTextService, quantityVal);
//		Thread.sleep(1000);

		// 2 Serial Specific

		sendKeys(txt_ProductA, obj1.readTestCreation("SerielSpecific"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, quantityValSerialSpec);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(addNewRecordSerialSpecific);
//		Thread.sleep(2000);
//		click(productLookupSerialSpec);
//		Thread.sleep(3000);
//		InventoryAndWarehouseModule obj2 = new InventoryAndWarehouseModule();
//		sendKeys(productTextSerialSpec, obj2.readTestCreation("SerielSpecific"));
//		Thread.sleep(3000);
//		pressEnter(productTextSerialSpec);
//		Thread.sleep(4000);
//		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		selectText(wareHouseSerialSpecific, warehouseVal1);
		Thread.sleep(2000);

		if (isEnabled(unitPriceSerialSpec)) {
			writeTestResults("User should be able to add unit price", "Unit price field should be enabled",
					"Unit price field is enabled", "Pass");
		} else {
			writeTestResults("User should be able to add unit price", "Unit price field should be enabled",
					"Unit price field is disabled", "Fail");
		}
		Thread.sleep(3000);
		sendKeys(unitPriceSerialSpec, unitPriceValSerialSpec);
//		Thread.sleep(2000);
//		sendKeys(qtyTextSerialSpec, quantityValSerialSpec);
//		Thread.sleep(1000);

		// 3 Batch Specific

		sendKeys(txt_ProductA, obj1.readTestCreation("BatchSpecific"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, quantityValInv);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(addNewBatchSpecific);
//		Thread.sleep(2000);
//		click(productLookupBatchSpecific);
//		Thread.sleep(3000);
//		InventoryAndWarehouseModule obj3 = new InventoryAndWarehouseModule();
//		sendKeys(productTextInv, obj3.readTestCreation("BatchSpecific"));
//		Thread.sleep(3000);
//		pressEnter(productTextInv);
//		Thread.sleep(3500);
//		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		selectText(wareHouseBatchSpecific, warehouseVal1);
		Thread.sleep(2000);
		sendKeys(unitPriceBatchSpecific, unitPriceValBatchSpec);
//		Thread.sleep(2000);
//		sendKeys(qtyTextBatchSpecific, quantityValInv);
//		Thread.sleep(2000);

		// 4 Batch FIFO

		sendKeys(txt_ProductA, obj1.readTestCreation("BatchFifo"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, quantityValInv);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(addNewBatchFIFO);
//		Thread.sleep(2000);
//		click(productLookupBatchFIFO);
//		Thread.sleep(3000);
//		InventoryAndWarehouseModule obj4 = new InventoryAndWarehouseModule();
//		sendKeys(productTextInv, obj4.readTestCreation("BatchFifo"));
//		Thread.sleep(3000);
//		pressEnter(productTextInv);
//		Thread.sleep(3500);
//		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		selectText(wareHouseBatchFifo, warehouseVal1);
		Thread.sleep(2000);
		sendKeys(unitPriceBatchFifo, unitPriceValInv);
//		Thread.sleep(2000);
//		sendKeys(qtyTextBatchFifo, quantityValInv);
//		Thread.sleep(1000);

		// 5 Serial Batch Specific

		sendKeys(txt_ProductA, obj1.readTestCreation("SerielBatchSpecific"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, quantityValInv);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(addNewBatchSpecific);
//		Thread.sleep(2000);
//		click(productLookupSerialBatchSpecific);
//		Thread.sleep(3000);
//		InventoryAndWarehouseModule obj5 = new InventoryAndWarehouseModule();
//		sendKeys(productTextInv, obj5.readTestCreation("SerielBatchSpecific"));
//		Thread.sleep(3000);
//		pressEnter(productTextInv);
//		Thread.sleep(4000);
//		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		selectText(wareHouseSerialBatchSpecific, warehouseVal1);
		Thread.sleep(2000);
		sendKeys(unitPriceSerialBatchSpec, unitPriceValInv);
//		Thread.sleep(2000);
//		sendKeys(qtyTxtSerialBatchSpecific, quantityValInv);
//		Thread.sleep(1000);

		// 6 Serial Batch FIFO

		sendKeys(txt_ProductA, obj1.readTestCreation("SerielBatchFifo"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, quantityValInv);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(addNewSerialBatchFifo);
//		Thread.sleep(2000);
//		click(productLookupSerialBatchFifo);
//		Thread.sleep(4000);
//		InventoryAndWarehouseModule obj6 = new InventoryAndWarehouseModule();
//		sendKeys(productTextInv, obj6.readTestCreation("SerielBatchFifo"));
//		Thread.sleep(3000);
//		pressEnter(productTextInv);
//		Thread.sleep(4000);
//		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		selectText(wareHouseSerialBatchFifo, warehouseVal1);
		Thread.sleep(2000);
		sendKeys(unitPriceSerialBatchFifo, unitPriceValInv);
//		Thread.sleep(2000);
//		sendKeys(qtyTextSerialBatchFifo, quantityValInv);
//		Thread.sleep(1000);

		// 7 Serial FIFO

		sendKeys(txt_ProductA, obj1.readTestCreation("SerirlFifo"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, quantityValInv);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(addNewSerialFifo);
//		Thread.sleep(2000);
//		click(productLookupSerialFifo);
//		Thread.sleep(4000);
//		InventoryAndWarehouseModule obj7 = new InventoryAndWarehouseModule();
//		sendKeys(productTextInv, obj7.readTestCreation("SerirlFifo"));
//		Thread.sleep(3000);
//		pressEnter(productTextInv);
//		Thread.sleep(4000);
//		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		selectText(wareHouseSerialFifo, warehouseVal1);
		Thread.sleep(2000);
		sendKeys(unitPriceSerialFifo, unitPriceValInv);
//		Thread.sleep(2000);
//		sendKeys(qtyTextSerialFifo, quantityValInv);
		Thread.sleep(1000);

//		click(batchSpecificSerialMenu);nadeesh comment krla thibbe

		click(checkoutSalesOrder);
		Thread.sleep(3000);

		// checkCalculationsSalesOrder1Journey();aruna comment kale
		Thread.sleep(2000);
		click(draft);
		Thread.sleep(5000);
		String header = getText(releaseInHeader);

		SO = getText(documentVal);
		System.out.println(SO);
		trackCode = getText(documentVal);
		// System.out.println(trackCode);

		document_map.put("SalesOrderJourney3", trackCode);
		document.add(0, document_map);

		if (header.equals("(Draft)")) {
			writeTestResults("User should be able to draft the sales order", "Sales order should be drafted",
					"Sales order drafted", "Pass");

		} else {
			writeTestResults("User should be able to draft the sales order", "Sales order should be drafted",
					"Sales order not drafted", "Fail");
		}

		Thread.sleep(2000);
		click(menu);
		Thread.sleep(3000);
		click(serialWise);
		Thread.sleep(7000);
		serialNoVal = getText(serialTextVal);
		Thread.sleep(2000);
		click(serialMenuClose);
		Thread.sleep(3000);
		mouseMove(menuBatchSpecific);
		click(menuBatchSpecific);
		Thread.sleep(3000);
		click(serialWise);
		Thread.sleep(3000);
		batchNoBatchSpecific = getText(batchSpecificBatchNo);
		Thread.sleep(2000);
		click(serialMenuClose);
		Thread.sleep(2000);
		mouseMove(menuSerialBatchSpec);
		click(menuSerialBatchSpec);
		Thread.sleep(2000);
		click(serialWise);
		Thread.sleep(2000);
		serialNoSerialBatchSpec = getText(serialBatchSpec);
		Thread.sleep(2000);
		click(serialMenuClose);
		Thread.sleep(2000);
		click(serialBatch);
		Thread.sleep(4000);
		click(productListBtn);
		Thread.sleep(2000);
		sendKeys(serialText, serialNoVal);
		Thread.sleep(2000);
		pressEnter(serialText);
		Thread.sleep(2000);
		click(updateBtn);
		Thread.sleep(2000);
		click(batchSpecificListBtn);
		Thread.sleep(2000);
		sendKeys(batchNoText, batchNoBatchSpecific);
		Thread.sleep(2000);
		pressEnter(batchNoText);
		Thread.sleep(2000);
		click(updateBtn);
		Thread.sleep(2000);
		click(serialbatchSpecificListBtn);
		Thread.sleep(2000);
		sendKeys(batchNoTextSerialBatchSpecific, serialNoSerialBatchSpec);
		Thread.sleep(2000);
		pressEnter(batchNoTextSerialBatchSpecific);
		Thread.sleep(2000);
		click(updateBtn);
		Thread.sleep(2000);
		click(backBtn);
		Thread.sleep(2000);
		click(release);
		Thread.sleep(6000);
		// String header = getText(releaseInHeader);

		// trackCode = getText(documentVal);

		writeTestCreations("SalesOrderJourney3", document.get(0).get("SalesOrderJourney3"), 3);

		if (isDisplayed(outboundLink)) {
			writeTestResults("User should be able to release the sales order", "Sales order should be released",
					"Sales order is released", "Pass");
			click(outboundLink);
			Thread.sleep(4000);
		} else {
			writeTestResults("User should be able to release the sales order", "Sales order should be released",
					"Sales order is not released", "Fail");
		}

	}

	public void checkFieldsSalesOrderSalesInvoice() throws Exception {
		if (isDisplayed(customerAccSearchService)) {
			if (isDisplayed(currencyService)) {
				if (isDisplayed(billingAddressService)) {
					if (isDisplayed(productLookupService)) {
						if (isDisplayed(salesUnit)) {
							if (isDisplayed(accountOwner)) {
								writeTestResults("Verify that the mandatory fields are available on sales order page",
										"Mandatory fields should be available on sales order page",
										"Mandatory fields are available on sales order page", "Pass");
							}
						}
					}
				}

			}

		} else

		{
			writeTestResults("Verify that the mandatory fields are available on sales order page",
					"Mandatory fields should be available on sales order page", "Mandatory field is missing", "Fail");
		}
	}

	public void checkCalculationsSalesOrder1Journey() throws Exception {

		float unitPriceService = Float.parseFloat(unitPriceVal);
		float unitPriceSerialSpec = Float.parseFloat(unitPriceValSerialSpec);
		float unitPriceBatchSpecific = Float.parseFloat(unitPriceValBatchSpec);
		float unitPriceBatchFifo = Float.parseFloat(unitPriceValInv);
		float unitPriceSerialBatchSpec = Float.parseFloat(unitPriceValInv);
		float unitPriceSerialBatchFifo = Float.parseFloat(unitPriceValInv);
		float unitPriceSerialFifo = Float.parseFloat(unitPriceValInv);

		float quantityService = Float.parseFloat(quantityVal);
		float quantitySerialSpec = Float.parseFloat(quantityValInv);
		float quantityBatchSpecific = Float.parseFloat(quantityValInv);
		float quantityBatchFifo = Float.parseFloat(quantityValInv);
		float quantitySerialBatchSpec = Float.parseFloat(quantityValInv);
		float quantitySerialBatchFifo = Float.parseFloat(quantityValInv);
		float quantitySerialFifo = Float.parseFloat(quantityValInv);

		float lineTotSer = quantityService * unitPriceService;
		float lineTotSerialSpec = quantitySerialSpec * unitPriceSerialSpec;
		float lineTotBatchSpec = quantityBatchSpecific * unitPriceBatchSpecific;
		float lineTotBatchFifo = quantityBatchFifo * unitPriceBatchFifo;
		float lineTotSerialBatchSpec = quantitySerialBatchSpec * unitPriceSerialBatchSpec;
		float lineTotSerialBatchFifo = quantitySerialBatchFifo * unitPriceSerialBatchFifo;
		float lineTotSerialFifo = quantitySerialFifo * unitPriceSerialFifo;

		totInvSer = lineTotSer + lineTotSerialSpec + lineTotBatchSpec + lineTotBatchFifo + lineTotSerialBatchSpec
				+ lineTotSerialBatchFifo + lineTotSerialFifo;

		System.out.println(totInvSer);

		String bannerTot = getText(bannerTotal1);
		// System.out.println(bannerTot);

		String bannerTotNew = bannerTot.substring(0, bannerTot.length() - 4);
		String bannerTotNew2 = bannerTotNew.replace(",", "");
		System.out.println(bannerTotNew2);

		float bannerTotal = Float.parseFloat(bannerTotNew2);

		if (totInvSer == bannerTotal) {

			writeTestResults("Verify values has calculated and updated under the bottom value bar and top left pannel",
					"Values should be calculate and update under the bottom value bar and top left pannel",
					"Calculations are accurate in sales order", "Pass");

		} else {
			writeTestResults("Verify that values have calculated and updated in top right panel",
					"Values should be calculate and update under the bottom value bar and top left pannel",
					"Calculations are incorrect in sales order", "Fail");
		}

	}

	public void checkAddedProducts() throws Exception {

		InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();
		// String val1 = getText(serviceOut);

		String valService = serviceOut.replace("20200102164024-Service [Test Product]",
				obj1.readTestCreation("Service"));

		InventoryAndWarehouseModule obj2 = new InventoryAndWarehouseModule();
		String val2 = getText(serialSpecOut);
		String val22 = val2.replace(" [Test Product]", "");

		InventoryAndWarehouseModule obj3 = new InventoryAndWarehouseModule();
		String val3 = getText(batchSpeccOut);
		String val33 = val3.replace(" [Test Product]", "");

		InventoryAndWarehouseModule obj4 = new InventoryAndWarehouseModule();
		String val4 = getText(batchFifoOut);
		String val44 = val4.replace(" [Test Product]", "");

		InventoryAndWarehouseModule obj5 = new InventoryAndWarehouseModule();
		String val5 = getText(serialBatchSpecOut);
		String val55 = val5.replace(" [Test Product]", "");

		InventoryAndWarehouseModule obj6 = new InventoryAndWarehouseModule();
		String val6 = getText(serialbatchFifoOut);
		String val66 = val6.replace(" [Test Product]", "");

		InventoryAndWarehouseModule obj7 = new InventoryAndWarehouseModule();
		String val7 = getText(serialFifoOut);
		String val77 = val7.replace(" [Test Product]", "");

		if (val22.equals(obj2.readTestCreation("SerielSpecific"))
				&& val33.equals(obj3.readTestCreation("BatchSpecific"))
				&& val44.equals(obj4.readTestCreation("BatchFifo"))
				&& val55.equals(obj5.readTestCreation("SerielBatchSpecific"))
				&& val66.equals(obj6.readTestCreation("SerielBatchFifo"))
				&& val77.equals(obj7.readTestCreation("SerirlFifo")) && !isElementPresent(valService)) {
			writeTestResults("Verify the added products loaded to outbound shipment",
					"Added products should load to outbound shipment", "Added products loaded to outbound shipment",
					"Pass");
		} else {
			writeTestResults("Verify the added products loaded to outbound shipment",
					"Added products should load to outbound shipment", "Added products not loaded to outbound shipment",
					"Pass");
		}
	}

	public void checkCalculationsSalesOrder() throws Exception {

		float unitPriceInv = Float.parseFloat(unitPriceValInv);
		float unitPriceSer = Float.parseFloat(unitPriceVal);
		float quantityInv = Float.parseFloat(quantityValInv);
		float quantitySer = Float.parseFloat(quantityVal);

		float lineTotInv = quantityInv * unitPriceInv;
		float lineTotSer = quantitySer * unitPriceSer;

		totInvSer = lineTotInv + lineTotSer;

		System.out.println(totInvSer);
		String bannerTot = getText(bannerTotal1);
		String bannerTotNew = bannerTot.replaceFirst(",", "");
		float bannerTotal = Float.valueOf(bannerTotNew);
		System.out.println(bannerTotNew);
		System.out.println(bannerTotal);
		System.out.println(totInvSer);

		if (totInvSer == bannerTotal) {

			writeTestResults("Verify values has calculated and updated under the bottom value bar and top left pannel",
					"Values should be calculate and update under the bottom value bar and top left pannel",
					"Calculations are accurate in sales order", "Pass");

		} else {
			writeTestResults("Verify that values have calculated and updated in top right panel",
					"Values should be calculate and update under the bottom value bar and top left pannel",
					"Calculations are incorrect in sales order", "Fail");
		}

	}

	public void outboundShipmentSalesOrderToSalesInvoice() throws Exception {

		switchWindow();
		// checkFieldsSalesOrderOutbound();
		click(checkoutOutbound);
		Thread.sleep(3000);
		click(outboundDraft);
		Thread.sleep(5000);
		// checkAddedProducts();
		outBoundID = getText(documentVal);
		// checkInventoryProduct();
		Thread.sleep(2000);
		// checkServiceProduct();
		String header = getText(releaseInHeader);
		trackCode = getText(documentVal);

		AOD = getText(documentVal);
		document_map.put("SalesOrderOSJourney1", trackCode);
		document.add(0, document_map);
		writeTestCreations("SalesOrderOSJourney1", document.get(0).get("SalesOrderOSJourney1"), 1);
		System.out.println(AOD);
		Thread.sleep(2000);

		if (header.equals("(Draft)")) {
			writeTestResults("User should be able to draft the outbound shipment",
					"Outbound shipment should be drafted", "Outbound shipment drafted", "Pass");

		} else {
			writeTestResults("User should be able to draft the outbound shipment",
					"Outbound shipment should be drafted", "Outbound shipment not drafted", "Fail");
		}

		click(release);
		Thread.sleep(8000);

		if (isDisplayed(outboundLink)) {
			writeTestResults("User should be able to release the Outbound shipment",
					"Outbound shipment should be released", "Outbound shipment is released", "Pass");
			click(outboundLink);
			Thread.sleep(3000);
		} else {
			writeTestResults("User should be able to release the Outbound shipment",
					"Outbound shipment should be released", "Outbound shipment is not released", "Fail");
		}

	}

	public void checkInventoryProduct() throws Exception {

		InventoryAndWarehouseModule obj2 = new InventoryAndWarehouseModule();
		String invenValue = obj2.readTestCreation("SerielSpecific");

		String invenValueOutbound = getText(outboundInvenValue);

		System.out.println(invenValue);
		System.out.println(invenValueOutbound);

		if (invenValue.equals(invenValueOutbound)) {
			writeTestResults("Added inventory products should be loaded to product grid in outbound shipment",
					"Inventory Product should be loaded", "Inventory Product loaded in outbound shipment", "Pass");
		} else {
			writeTestResults("Added inventory products should be loaded to product grid in outbound shipment",
					"Inventory Product should be loaded", "Inventory Product not loaded in outbound shipment", "Fail");
		}
	}

	public void checkServiceProduct() throws Exception {

		if (isDisplayed(serviceProductValOutbound)) {
			writeTestResults("Added service product should not be loaded to product grid in outbound shipment",
					"Service Product should not be loaded", "Service Product not loaded in outbound", "Pass");
		} else {
			writeTestResults("Added service product should not be loaded to product grid in outbound shipment",
					"Service Product should not be loaded", "ServiceProduct loaded in outbound", "Fail");
		}

	}

	public void configureJourneyOne() throws Exception {

		click(journeyConfiguration);
		Thread.sleep(2000);
		mouseMove(salesOrderToSalesInvoiceJourney);
		click(salesOrderToSalesInvoiceJourney);
		Thread.sleep(2000);

		if (isSelected(autogenerateSalesInvoice) && isSelected(autogenerateOutboundShipment)) {
			click(autogenerateSalesInvoice);
			Thread.sleep(2000);
			click(autogenerateOutboundShipment);
			click(updateJourney);
		}
	}

	public void salesInvoiceSalesOrderToSalesInvoice() throws Exception {

		Thread.sleep(5000);
		switchWindow();
		click(checkoutInvoice);
		Thread.sleep(3000);

		if (isDisplayed(outboundDraft)) {
			writeTestResults("User should be able to see the draft button in sales invoice page",
					"Draft button should be available ", "Draft button is available on sales invoice page", "Pass");

		} else {
			writeTestResults("User should be able to see the draft button in sales invoice page",
					"Draft button should be available", "Draft button is not available on sales invoice page", "Fail");
		}

		click(outboundDraft);
		Thread.sleep(5000);
		click(release);
		Thread.sleep(7000);
		String header = getText(releaseInHeader);

		SalesInvoiceID = getText(documentVal);

		SCD = getText(documentVal);
		// System.out.println(SCD);
		trackCode = getText(documentVal);

		document_map.put("SalesOrderSIJourney1", trackCode);
		document.add(0, document_map);
		Thread.sleep(3000);

		if (header.equals("(Released)")) {
			writeTestResults("User should be able to release the sales invoice", "Sales invoice should be released",
					"Sales invoice is released", "Pass");
		} else {
			writeTestResults("User should be able to release the sales invoice", "Sales invoice should be released",
					"Sales invoice is not released", "Fail");
		}
		writeTestCreations("SalesOrderSIJourney1", document.get(0).get("SalesOrderSIJourney1"), 2);
	}

	public void checkValueInSalesInvoice() throws Exception {

		System.out.println(totInvSer);

		String val = getText(totalSalesInvoice);
		System.out.println(val);
//		if (totalSalesInvoice.equals(totInvSer)) {
//			writeTestResults("Values should be as sales order", "", "Values are same as in sales order", "Pass");
//		} else {
//			writeTestResults("", "", "Values are same as in sales order", "Fail");
//		}

	}

	// Sales Order -> sales Invoice-[Service] -- 002

	public void checkJouneyLoading2() throws Exception {

		click(newSalesOrder);
		Thread.sleep(2000);

		if (isDisplayed(journey2)) {
			writeTestResults("Verify the journey 'Sales Order to Sales Invoice'",
					"user should be able to see the Sales Order to Sales Invoice journey", "Journey is displayed",
					"pass");
			click(journey2);
			Thread.sleep(2000);
		} else {
			writeTestResults("Verify the journey 'Sales Order to Sales Invoice'",
					"user should be able to see the Sales Order to Sales Invoice journey", "Journey is not displayed",
					"Fail");
		}
	}

	public void fillDataSalesOrderSalesInvoiceServ() throws Exception {

		Thread.sleep(2000);
		checkFieldsSalesOrderSalesInvoice();
		Thread.sleep(2000);
		click(customerAccSearchService);
		Thread.sleep(2000);
		sendKeys(customerSearchTxt, customerAccServiceVal);
		Thread.sleep(2000);
		pressEnter(customerSearchTxt);
		Thread.sleep(3000);
		doubleClick(customerSelectedValue);
		Thread.sleep(2000);
		// billing address and shipping address
		// click(billingAddressService);
		// sendKeys(billingAddressTxt, billingAddrSerVal);
		// click(shippingAddressText);
		// sendKeys(shippingAddressTxt, shippingAddrSerVal);
		// click(applyBtnService);
		selectIndex(salesUnitService, 2);

		// Service Product
		click(productLookupService);
		Thread.sleep(2000);
		InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();
		sendKeys(productTxtService, obj1.readTestCreation("Service"));
		Thread.sleep(2000);
		pressEnter(productTxtService);
		Thread.sleep(3000);
		doubleClick(selectedValueSearch);

		sendKeys(unitPriceServicePro, unitPriceValue);

		// click(qtyText);
		// sendKeys(qtyText, quantityVal);

		Thread.sleep(2000);
		click(checkoutService);
		Thread.sleep(3000);
		click(draftService);

		Thread.sleep(4000);
		String headerDraft = getText(releaseInHeader);

		trackCode = getText(documentVal);
		System.out.println(trackCode);

		if (headerDraft.equals("(Draft)")) {
			writeTestResults("User should be able to draft the sales order", "Sales order should be drafted",
					"Sales order drafted", "Pass");

		} else {
			writeTestResults("User should be able to draft the sales order", "Sales order should be drafted",
					"Sales order not drafted", "Fail");
		}

		Thread.sleep(4000);
		click(releaseService);
		Thread.sleep(5000);

		if (isDisplayed(outboundLink)) {
			writeTestResults("User should be able to release the sales order", "Sales order should be released",
					"Sales order is released", "Pass");

			click(outboundLink);
			Thread.sleep(4000);
		} else {
			writeTestResults("User should be able to release the sales order", "Sales Order should be released",
					"Sales Order is not released", "Fail");
		}
	}

	public void SalesInvoiceService() throws Exception {

		switchWindow();
		Thread.sleep(2000);
		click(checkoutInvoiceService);
		Thread.sleep(3000);

		// String serProduct = getText();
		// System.out.println(serProduct);

		InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();

		String pro = obj1.readTestCreation("Service");
		String pro1 = pro + " [Test Product]";

		String pro2 = getText(serSalesInvoice);

		if (pro1.equals(pro2)) {
			writeTestResults("Verify the added service product is available in the sales invoice",
					"Service product should be available", "Service product loaded in sales invoice", "Pass");
		} else {
			writeTestResults("Verify the added service product is available in the sales invoice",
					"Service product should be available", "Service product not loaded in sales invoice", "Fail");
		}

		click(draftInvoiceService);
		Thread.sleep(4000);

		String header1 = getText(releaseInHeader);
		if (header1.equals("(Draft)")) {
			writeTestResults("User should be able to draft the sales invoice", "Sales invoice should be draft",
					"Sales invoice draft", "Pass");

		} else {
			writeTestResults("User should be able to release the sales invoice", "Sales invoice should be draft",
					"Sales invoice not draft", "Fail");
		}

		click(releaseInvoiceService);
		Thread.sleep(7000);

		String header = getText(releaseInHeader);

		trackCode = getText(documentVal);
		System.out.println(trackCode);

		if (header.equals("(Released)")) {
			writeTestResults("User should be able to release the sales invoice", "Sales invoice should be released",
					"Sales invoice released", "Pass");

		} else {
			writeTestResults("User should be able to release the sales invoice", "Sales invoice should be released",
					"Sales invoice not released", "Fail");
		}
	}

	public void checkFieldsSalesOrderSalesInvoiceService() throws Exception {
		if (isDisplayed(customerAccSearchService)) {
			if (isDisplayed(currencyService)) {
				if (isDisplayed(billingAddressService)) {
					if (isDisplayed(productLookupService)) {
						writeTestResults("Verify that the mandatory fields are available on the page",
								"Mandatory fields should be available on sales order page",
								"Mandatory fields are available", "Pass");

					}
				}
			}
		} else

		{
			writeTestResults("Verify that the mandatory fields are available on sales order page",
					"Mandatory fields should be available on sales order page", "Mandatory field is missing", "Fail");
		}
	}

	/*
	 * public void salesOrderSalesInvoiceSer() throws Exception {
	 * 
	 * // click(outboundLink); Thread.sleep(5000);
	 * click("//div[@class='doc-detal']//div[@id='divShipment']//a[@id='btnCalc2']")
	 * ; Thread.sleep(3000);
	 * 
	 * click(outboundDraft);
	 * 
	 * }
	 */

	// Verify that user is able to create sales order (Sales order to Outbound
	// shipment) --- 3

	public void checkJourneyrSalesorderToOutbound() throws Exception {

		click(newSalesOrder);
		Thread.sleep(2000);
		if (isDisplayed(journey3)) {
			writeTestResults("Verify the availability of Sales Order to Sales Invoice journey",
					"user should be able to see the Sales Order to Sales Invoice journey", "Journey is displayed",
					"Pass");
			click(journey3);
		} else {
			writeTestResults("Verify the availability of Sales Order to Sales Invoice journey",
					"user should be able to see the journeys", "Journey is not displayed", "Fail");
		}
	}

	public void fillDetailsSalesorderToOutbound() throws Exception {

		checkFieldsSalesOrderSalesInvoice();

		click(customerAccountSearch);
		Thread.sleep(2000);
		sendKeys(customerAccountText, customerAccVal);
		Thread.sleep(3000);
		pressEnter(customerAccountText);
		Thread.sleep(2000);
		doubleClick(selected_val);
		/*
		 * click(currencyDropdown); billing address and shipping address
		 * click(billingAddressSearch); click(billingAddressText);
		 * sendKeys(billingAddressText, billingAddrVal); click(shippingAddressText);
		 * sendKeys(shippingAddressText, shippingAddrVal); click(apply_btn);
		 */
		Thread.sleep(2000);
		click(salesUnit);
		selectIndex(salesUnit, 1);

		// 1 Service Product
		InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();

		sendKeys(txt_ProductA, obj1.readTestCreation("Service"));
		Thread.sleep(4000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, quantityVal);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(productLookup);
//		Thread.sleep(2000);
//		InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();
//		sendKeys(productTextSer, obj1.readTestCreation("Service"));
//		Thread.sleep(2000);
//		pressEnter(productTextSer);
//		Thread.sleep(3000);
//		doubleClick(invSelectedVal);
		Thread.sleep(3000);

		if (isEnabled(wareHouse)) {
			writeTestResults("Verify that the warehouse field is disabled for service product",
					"Warehouse field should be disabled", "Warehouse field is enabled", "Fail");
		} else {
			writeTestResults("Verify that the warehouse field is disabled for service product",
					"Warehouse field should be disabled", "Warehouse field is disabled", "Pass");
		}

		sendKeys(unitPriceTxtService, unitPriceVal);
//		Thread.sleep(1000);
//		sendKeys(qtyTextService, quantityVal);
//		Thread.sleep(1000);

		// 2 Serial Specific

		sendKeys(txt_ProductA, obj1.readTestCreation("SerielSpecific"));
		Thread.sleep(4000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, quantityValSerialSpec);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(addNewRecordSerialSpecific);
//		Thread.sleep(2000);
//		click(productLookupSerialSpec);
//		Thread.sleep(3000);
//		InventoryAndWarehouseModule obj2 = new InventoryAndWarehouseModule();
//		sendKeys(productTextSerialSpec, obj2.readTestCreation("SerielSpecific"));
//		Thread.sleep(3000);
//		pressEnter(productTextSerialSpec);
//		Thread.sleep(3000);
//		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		selectText(wareHouseSerialSpecific, warehouseVal1);
		Thread.sleep(2000);

		if (isEnabled(unitPriceSerialSpec)) {
			writeTestResults("User should be able to add unit price", "Unit price field should be enabled",
					"Unit price field is enabled", "Pass");
		} else {
			writeTestResults("User should be able to add unit price", "Unit price field should be enabled",
					"Unit price field is disabled", "Fail");
		}

		sendKeys(unitPriceSerialSpec, unitPriceValSerialSpec);
//		Thread.sleep(2000);
//		sendKeys(qtyTextSerialSpec, quantityValSerialSpec);
		Thread.sleep(1000);
		click(checkOutbound);
		Thread.sleep(2000);
		click(draftOutbound);

		Thread.sleep(5000);
		String header = getText(releaseInHeader);

		trackCode = getText(documentVal);
		System.out.println(trackCode);

		if (header.equals("(Draft)")) {
			writeTestResults("User should be able to draft the sales order", "Sales order should be drafted",
					"Sales order drafted", "Pass");

		} else {
			writeTestResults("User should be able to draft the sales order", "Sales order should be drafted",
					"Sales order not drafted", "Fail");
		}
		Thread.sleep(3000);
		click(releaseOutbound);
		Thread.sleep(5000);

		if (isDisplayed(outboundLink)) {
			writeTestResults("User should be able to release the Outbound shipment",
					"Outbound shipment should be released", "Outbound shipment is released", "Pass");
		} else {
			writeTestResults("User should be able to release the Outbound shipment",
					"Outbound shipment should be released", "Outbound shipment is not released", "Fail");
		}
	}

	public void salesOrderOutbound() throws Exception {

		Thread.sleep(3000);
		click(linkSalesInvoice);
		Thread.sleep(5000);
		switchWindow();
		click(checkoutInboundshipment);
		Thread.sleep(3000);
		click(draft);
		Thread.sleep(5000);
		click(release);
		Thread.sleep(6000);

		String header = getText(releaseInHeader);

		trackCode = getText(documentVal);
		System.out.println(trackCode);

		document_map.put("SalesOrderOSJourney3", trackCode);
		document.add(0, document_map);

		if (header.equals("(Released)")) {
			writeTestResults("User should be able to release the outbound shipment",
					"Outbound shipment should be released", "Outbound shipment released", "Pass");

		} else {
			writeTestResults("User should be able to release the outbound shipment",
					"Outbound shipment should be released", "Outbound shipment not released", "Fail");
		}

		writeTestCreations("SalesOrderOSJourney3", document.get(0).get("SalesOrderOSJourney3"), 5);

	}

	public void salesOrderSalesInvoiceNonDeli() throws Exception {

		Thread.sleep(3000);
		click(linkSalesInvoice);
		Thread.sleep(5000);
		switchWindow();
		click(checkoutInvoice);
		Thread.sleep(4000);
		click(draftInvoice);
		Thread.sleep(5000);
		click(release);
		Thread.sleep(6000);

		click(serialMenuClose);
		SalesInvoiceID = getText(documentVal);

		String header = getText(releaseInHeader);
		trackCode = getText(documentVal);

		if (header.equals("(Released)")) {
			writeTestResults("User should be able to release the sales invoice", "Sales invoice should be released",
					"Sales invoice released", "Pass");

		} else {
			writeTestResults("User should be able to release the sales invoicet", "Sales invoice should be released",
					"Sales invoice not released", "Fail");
		}
	}

	public void salesOrderSalesInvoiceJourney3() throws Exception {

		Thread.sleep(3000);
		click(linkSalesInvoice);
		Thread.sleep(4000);
		switchWindow();
		click(checkoutInvoice);
		Thread.sleep(4000);
		click(draftInvoice);
		Thread.sleep(5000);
		String header = getText(releaseInHeader);

		trackCode = getText(documentVal);
		System.out.println(trackCode);

		document_map.put("SalesOrderSIJourney3", trackCode);
		document.add(0, document_map);

		if (header.equals("(Draft)")) {
			writeTestResults("User should be able to draft the sales invoice", "Sales invoice should be draft",
					"Sales invoice released", "Pass");

		} else {
			writeTestResults("User should be able to draft the sales invoice", "Sales invoice should be draft",
					"Sales invoice not released", "Fail");
		}

		click(release);
		Thread.sleep(5000);

		writeTestCreations("SalesOrderSIJourney3", document.get(0).get("SalesOrderSIJourney3"), 4);

	}

	// Verify that user is able to create sales order (Sales order to Sales Invoice
	// - Drop Shipment) -- 004

	public void checkJourneySalesOrderToSalesInvoiceDropShip() throws Exception {

		click(newSalesOrder);
		Thread.sleep(2000);
		if (isDisplayed(journey4)) {
			writeTestResults("Verify the availability of Sales Order to Sales Invoice[Drop Shipment] journey",
					"user should be able to see the Sales Order to Sales Invoice journey", "Journey is displayed",
					"pass");
			click(journey4);
			Thread.sleep(2000);
		} else {
			writeTestResults("Verify the availability of Sales Order to Sales Invoice[Drop Shipment] journey",
					"user should be able to see the journey", "Journey is not displayed", "Fail");
		}
	}

	public void fillDetailsDropShipment() throws Exception {

		checkFieldsSalesOrderSalesInvoice();

		click(customerAccountSearch);
		Thread.sleep(2000);
		sendKeys(customerAccountText, customerAccVal);
		Thread.sleep(3000);
		pressEnter(customerAccountText);
		Thread.sleep(2500);
		doubleClick(selected_val);
		Thread.sleep(2000);
		click(salesUnit);
		selectIndex(salesUnit, 1);
		InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();

		// 1 Service Product

		sendKeys(txt_ProductA, obj1.readTestCreation("SerielSpecific"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		sendKeys(txt_ProductQtyA, quantityValSerialSpec);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(productLookup);
//		Thread.sleep(2000);
//		InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();
//		sendKeys(productTextSer, obj1.readTestCreation("SerielSpecific"));
//		Thread.sleep(2000);
//		pressEnter(productTextSer);
//		Thread.sleep(3500);
//		doubleClick(invSelectedVal);
		Thread.sleep(3000);

//		if (isEnabled(wareHouse)) {
//			writeTestResults("Verify that the warehouse field is disabled for service product",
//					"Warehouse field should be disabled", "Warehouse field is enabled", "Fail");
//		} else {
//			writeTestResults("Verify that the warehouse field is disabled for service product",
//					"Warehouse field should be disabled", "Warehouse field is disabled", "Pass");
//		}nadeesh comment krla thinne(2080to2086)

		sendKeys(unitPriceTxtService, unitPriceValSerialSpec);
		Thread.sleep(1000);
//		sendKeys(qtyTextService, quantityValSerialSpec);
//		Thread.sleep(1000);

		// 2 Serial Specific

		sendKeys(txt_ProductA, obj1.readTestCreation("BatchSpecific"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		sendKeys(txt_ProductQtyA, quantityValSerialSpec);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(addNewRecordSerialSpecific);
//		Thread.sleep(2000);
//		click(productLookupSerialSpec);
//		Thread.sleep(3000);
//		InventoryAndWarehouseModule obj2 = new InventoryAndWarehouseModule();
//		sendKeys(productTextSerialSpec, obj2.readTestCreation("BatchSpecific"));
//		Thread.sleep(3000);
//		pressEnter(productTextSerialSpec);
//		Thread.sleep(3500);
//		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		selectText(wareHouseSerialSpecific, warehouseVal1);
		Thread.sleep(2000);

		if (isEnabled(unitPriceSerialSpec)) {
			writeTestResults("User should be able to add unit price", "Unit price field should be enabled",
					"Unit price field is enabled", "Pass");
		} else {
			writeTestResults("User should be able to add unit price", "Unit price field should be enabled",
					"Unit price field is disabled", "Fail");
		}

		sendKeys(unitPriceSerialSpec, unitPriceValSerialSpec);
//		Thread.sleep(2000);
//		sendKeys(qtyTextSerialSpec, quantityValSerialSpec);
//		Thread.sleep(1000);

		// 3 Batch Specific

		sendKeys(txt_ProductA, obj1.readTestCreation("BatchFifo"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		sendKeys(txt_ProductQtyA, quantityValInv);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(addNewBatchSpecific);
//		Thread.sleep(2000);
//		click(productLookupBatchSpecific);
//		Thread.sleep(3000);
//		InventoryAndWarehouseModule obj3 = new InventoryAndWarehouseModule();
//		sendKeys(productTextInv, obj3.readTestCreation("BatchFifo"));
//		Thread.sleep(3000);
//		pressEnter(productTextInv);
//		Thread.sleep(3500);
//		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		selectText(wareHouseBatchSpecific, warehouseVal1);
		Thread.sleep(2000);
		sendKeys(unitPriceBatchSpecific, unitPriceValBatchSpec);
//		Thread.sleep(2000);
//		sendKeys(qtyTextBatchSpecific, quantityValInv);
//		Thread.sleep(2000);

		// 4 Batch FIFO

		sendKeys(txt_ProductA, obj1.readTestCreation("SerielBatchSpecific"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		sendKeys(txt_ProductQtyA, quantityValInv);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(addNewBatchFIFO);
//		Thread.sleep(2000);
//		click(productLookupBatchFIFO);
//		Thread.sleep(3000);
//		InventoryAndWarehouseModule obj4 = new InventoryAndWarehouseModule();
//		sendKeys(productTextInv, obj4.readTestCreation("SerielBatchSpecific"));
//		Thread.sleep(3000);
//		pressEnter(productTextInv);
//		Thread.sleep(3500);
//		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		selectText(wareHouseBatchFifo, warehouseVal1);
		Thread.sleep(2000);
		sendKeys(unitPriceBatchFifo, unitPriceValInv);
//		Thread.sleep(2000);
//		sendKeys(qtyTextBatchFifo, quantityValInv);
//		Thread.sleep(1000);

		// 5 Serial Batch Specific

		sendKeys(txt_ProductA, obj1.readTestCreation("SerielBatchFifo"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		sendKeys(txt_ProductQtyA, quantityValInv);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(addNewBatchSpecific);
//		Thread.sleep(2000);
//		click(productLookupSerialBatchSpecific);
//		Thread.sleep(3000);
//		InventoryAndWarehouseModule obj5 = new InventoryAndWarehouseModule();
//		sendKeys(productTextInv, obj5.readTestCreation("SerielBatchFifo"));
//		Thread.sleep(3000);
//		pressEnter(productTextInv);
//		Thread.sleep(4000);
//		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		selectText(wareHouseSerialBatchSpecific, warehouseVal1);
		Thread.sleep(2000);
		sendKeys(unitPriceSerialBatchSpec, unitPriceValInv);
//		Thread.sleep(2000);
//		sendKeys(qtyTxtSerialBatchSpecific, quantityValInv);
//		Thread.sleep(2000);

		// 6 Serial Batch FIFO

		sendKeys(txt_ProductA, obj1.readTestCreation("SerirlFifo"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		sendKeys(txt_ProductQtyA, quantityValInv);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(addNewSerialBatchFifo);
//		Thread.sleep(2000);
//		click(productLookupSerialBatchFifo);
//		Thread.sleep(3000);
//		InventoryAndWarehouseModule obj6 = new InventoryAndWarehouseModule();
//		sendKeys(productTextInv, obj6.readTestCreation("SerirlFifo"));
//		Thread.sleep(3000);
//		pressEnter(productTextInv);
//		Thread.sleep(3000);
//		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		selectText(wareHouseSerialBatchFifo, warehouseVal1);
		Thread.sleep(2000);
		sendKeys(unitPriceSerialBatchFifo, unitPriceValInv);
//		Thread.sleep(2000);
//		sendKeys(qtyTextSerialBatchFifo, quantityValInv);
		Thread.sleep(1000);

		click(checkoutSalesOrder);
		Thread.sleep(3000);
		// checkCalculationsSalesOrder1Journey();
		Thread.sleep(2000);
		click(draft);
		Thread.sleep(5000);
		String header = getText(releaseInHeader);

		if (header.equals("(Draft)")) {
			writeTestResults("User should be able to draft the sales order", "Sales order should be draft",
					"Sales order draft", "Pass");

		} else {
			writeTestResults("User should be able to draft the sales order", "Sales order should be draft",
					"Sales order not draft", "Fail");
		}

		click(releaseOutbound);
		Thread.sleep(6000);

		String header2 = getText(releaseInHeader);
		trackCode = getText(documentVal);

		if (header2.equals("(Released)")) {
			writeTestResults("User should be able to release the sales order", "Sales order should be released",
					"Sales order released", "Pass");

		} else {
			writeTestResults("User should be able to release the sales order", "Sales order should be released",
					"Sales order not released", "Fail");
		}

		convertToPurchaseOrderDropShip();

		Thread.sleep(2000);
		click(vendorLookup);
		sendKeys(vendorText, vendorVal);
		pressEnter(vendorText);
		Thread.sleep(2500);
		doubleClick(vendorSelectedVal);
		Thread.sleep(2000);
		click(checkOutbound);
		Thread.sleep(3000);
		click(draftOutbound);
		Thread.sleep(5000);

		String header4 = getText(releaseInHeader);
		trackCode = getText(documentVal);

		if (header4.equals("(Draft)")) {
			writeTestResults("User should be able to draft the purchase order", "Purchase order should be draft",
					"Purchase order draft", "Pass");

		} else {
			writeTestResults("User should be able to draft the purchase order", "Purchase order should be draft",
					"Purchase order not draft", "Fail");
		}

		click(release);
		Thread.sleep(6000);

		if (isDisplayed(outboundLink)) {
			writeTestResults("User should be able to release the purchase order", "Purchase order should be released",
					"Purchase order released", "Pass");
			click(outboundLink);
			Thread.sleep(4000);

		} else {
			writeTestResults("User should be able to release the purchase order", "Purchase order should be released",
					"Purchase order not released", "Fail");
		}

		switchWindow();
		click(checkoutInvoice);
		Thread.sleep(2000);
		click(draftInvoice);
		Thread.sleep(5000);
		click(release);
		Thread.sleep(6000);

		String header3 = getText(releaseInHeader);
		trackCode = getText(documentVal);

		if (header3.equals("(Released)")) {
			writeTestResults("User should be able to release the purchase invoice",
					"Purchase invoice should be released", "Purchase invoice released", "Pass");

		} else {
			writeTestResults("User should be able to release the purchase invoice",
					"Purchase invoice should be released", "Purchase invoice not released", "Fail");
		}
	}

	public void checkFieldsSalesOrderDropship() throws Exception {
		if (isDisplayed(customerAccSearchService)) {
			if (isDisplayed(currencyService)) {
				if (isDisplayed(billingAddressService)) {
					if (isDisplayed(productLookupService)) {
						if (isDisplayed(salesUnit)) {
							if (isDisplayed(accountOwner)) {
								writeTestResults("Verify that the mandatory fields are available on Sales Order",
										"Mandatory fields should be available on account information page",
										"Mandatory fields are available", "Pass");
							}
						}
					}
				}

			}

		} else

		{
			writeTestResults("Verify that the mandatory fields are available on account information page",
					"Mandatory fields should be available on account information page", "Mandatory field is missing",
					"Fail");
		}
	}

	public void convertToPurchaseOrderDropShip() throws Exception {

		click(actionMenu);
		Thread.sleep(2000);
		if (isElementPresent(convertToPurchaseOrder)) {
			click(convertToPurchaseOrder);
			Thread.sleep(5000);
			writeTestResults("Verify that convert to purchase order is available",
					"User should be able to see the convert to purchase order",
					"Convert to purchase order is available", "Pass");

		} else {
			writeTestResults("Verify that convert to purchase order is available",
					"User should be able to see the convert to purchase order",
					"Convert to purchase order is not available", "Fail");

		}
	}

	// Verify that user is able to create sales Invoice (Sales invoice to Outbound
	// shipment) --- 5

	public void checkClickSalesInvoice() throws Exception {

		if (isDisplayed(salesInvoice)) {
			writeTestResults("Verify that Sales Order page is available",
					"user should be able to see the sales order page", "Sales order page is dislayed", "pass");
			click(salesInvoice);
		} else {
			writeTestResults("Verify that Sales Order page is available",
					"user should be able to see the sales order page'", "Sales order is not dislayed", "Fail");
		}

	}

	public void checkJouneyLoadingSalesInvoice() throws Exception {

		click(newSalesInv);

		if (isDisplayed(journeySalesInvoice1)) {
			writeTestResults("Verify journey window", "user should be able to see the journeys",
					"Journey window is displayed", "pass");
			click(journeySalesInvoice1);

		} else {
			writeTestResults("Verify journey window", "user should be able to see the journeys",
					"Journey window is not displayed", "Fail");
		}
	}

	public void fillDetailsSalesInvoice() throws Exception {

		Thread.sleep(2000);
		click(cusAccount);

		sendKeys(customerAccountText, customerAccVal);
		pressEnter(customerAccountText);
		Thread.sleep(2000);
		doubleClick(selected_val);
		Thread.sleep(2000);
		selectIndex(salesUnit, 1);
		Thread.sleep(2000);
		InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();

		// 1 Service Product

//		click(productLookupInvoice);
		Thread.sleep(2000);

		sendKeys(txt_ProductA, obj1.readTestCreation("Service"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, quantityVal);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		InventoryAndWarehouseModule obj6 = new InventoryAndWarehouseModule();
//		sendKeys(productTextSer, obj6.readTestCreation("Service"));
//		Thread.sleep(3000);
//		pressEnter(productTextSer);
//		Thread.sleep(4000);
//		doubleClick(serSelectedVAl);
		Thread.sleep(4000);
		if (isEnabled(wareHouseSalesInvoice)) {
			writeTestResults("Verify that the warehouse field is disabled for service product",
					"Warehouse field should be disabled", "Warehouse field is enabled", "Fail");
		} else {
			writeTestResults("Verify that the warehouse field is disabled for service product",
					"Warehouse field should be disabled", "Warehouse field is disabled", "Pass");
		}
		Thread.sleep(3000);
		click(unitPriceTxtSISer);
		Thread.sleep(3000);
		sendKeys(unitPriceTxtSISer, unitPriceVal);
//		Thread.sleep(1000);
//		click(qtyTextSISer);
//		sendKeys(qtyTextSISer, quantityVal);
//		Thread.sleep(1000);

		// 2 Serial Specific

		sendKeys(txt_ProductA, obj1.readTestCreation("SerielSpecific"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, quantityValSerialSpec);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(addNewRecordSerialSpecSI);
//		Thread.sleep(2000);
//		click(productLookupSerialSpecSI);
//		Thread.sleep(3000);
//		InventoryAndWarehouseModule obj = new InventoryAndWarehouseModule();
//		sendKeys(productTextSerialSpec, obj.readTestCreation("SerielSpecific"));
//		Thread.sleep(3500);
//		pressEnter(productTextSerialSpec);
//		Thread.sleep(3500);
//		doubleClick(serSelectedVAl);
		Thread.sleep(3000);
		selectText(wareHouseSerialSpecificSI, warehouseVal1);
		Thread.sleep(2000);

//				if (isEnabled(unitPriceSerialSpec)) {
//					writeTestResults("User should be able to add unit price", "Unit price field should be enabled",
//							"Unit price field is enabled", "Pass");
//				} else {
//					writeTestResults("User should be able to add unit price", "Unit price field should be enabled",
//							"Unit price field is disabled", "Fail");
//				}nadeesh(2490 to 2496)
		Thread.sleep(3000);
		sendKeys(unitPriceSerialSpecSI, unitPriceValSerialSpec);
//		Thread.sleep(2000);
//		sendKeys(qtyTextSerialSpecSI, quantityValSerialSpec);
//		Thread.sleep(1000);

		// 3 Batch Specific

		sendKeys(txt_ProductA, obj1.readTestCreation("BatchSpecific"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, quantityValInv);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(addNewBatchSpecificSI);
//		Thread.sleep(2000);
//		click(productLookupBatchSpecificSI);
//		Thread.sleep(3000);
//		InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();
//		sendKeys(productTextInv, obj1.readTestCreation("BatchSpecific"));
//		Thread.sleep(3500);
//		pressEnter(productTextInv);
//		Thread.sleep(3500);
//		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		selectText(wareHouseBatchSpecificSI, warehouseVal1);
		Thread.sleep(2000);
		sendKeys(unitPriceBatchSpecificSI, unitPriceValBatchSpec);
//		Thread.sleep(2000);
//		sendKeys(qtyTextBatchSpecificSI, quantityValInv);
//		Thread.sleep(2000);

		// 4 Batch FIFO

		sendKeys(txt_ProductA, obj1.readTestCreation("BatchFifo"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, quantityValInv);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(addNewBatchFIFOSI);
//		Thread.sleep(2000);
//		click(productLookupBatchFIFOSI);
//		Thread.sleep(3000);
//		InventoryAndWarehouseModule obj2 = new InventoryAndWarehouseModule();
//		sendKeys(productTextInv, obj2.readTestCreation("BatchFifo"));
//		Thread.sleep(3500);
//		pressEnter(productTextInv);
//		Thread.sleep(3500);
//		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		selectText(wareHouseBatchFifoSI, warehouseVal1);
		Thread.sleep(2000);
		sendKeys(unitPriceBatchFifoSI, unitPriceValInv);
//		Thread.sleep(2000);
//		sendKeys(qtyTextBatchFifoSI, quantityValInv);
//		Thread.sleep(1000);

		// 5 Serial Batch Specific

		sendKeys(txt_ProductA, obj1.readTestCreation("SerielBatchSpecific"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, quantityValInv);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(addNewSerBatchSpecificSI);
//		Thread.sleep(2000);
//		click(productLookupSerialBatchSpecificSI);
//		Thread.sleep(3000);
//		InventoryAndWarehouseModule obj3 = new InventoryAndWarehouseModule();
//		sendKeys(productTextInv, obj3.readTestCreation("SerielBatchSpecific"));
//		Thread.sleep(3500);
//		pressEnter(productTextInv);
//		Thread.sleep(4000);
//		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		selectText(wareHouseSerialBatchSpecSI, warehouseVal1);
		Thread.sleep(2000);
		sendKeys(unitPriceSerialBatchSpecSI, unitPriceValInv);
//		Thread.sleep(2000);
//		sendKeys(qtyTextSerialBtchSpecSI, quantityValInv);
//		Thread.sleep(1000);

		// 6 Serial Batch FIFO

		sendKeys(txt_ProductA, obj1.readTestCreation("SerielBatchFifo"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, quantityValInv);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(addNewSerialBatchFifoSI);
//		Thread.sleep(2000);
//		click(productLookupSerialBatchFifoSI);
//		Thread.sleep(3000);
//		InventoryAndWarehouseModule obj4 = new InventoryAndWarehouseModule();
//		sendKeys(productTextInv, obj4.readTestCreation("SerielBatchFifo"));
//		Thread.sleep(3500);
//		pressEnter(productTextInv);
//		Thread.sleep(3500);
//		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		selectText(wareHouseSerialBtchFifoSI, warehouseVal1);
		Thread.sleep(2000);
		sendKeys(unitPriceSerialBatchFifoSI, unitPriceValInv);
//		Thread.sleep(2000);
//		sendKeys(qtyTextSerialBatchFifoSI, quantityValInv);
//		Thread.sleep(1000);

		// 7 Serial FIFO

		sendKeys(txt_ProductA, obj1.readTestCreation("SerirlFifo"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, quantityValInv);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(addNewSerialFifoSI);
//		Thread.sleep(1000);
//		click(productLookupSerialFifoSI);
//		Thread.sleep(3000);
//		InventoryAndWarehouseModule obj5 = new InventoryAndWarehouseModule();
//		sendKeys(productTextInv, obj5.readTestCreation("SerirlFifo"));
//		Thread.sleep(3500);
//		pressEnter(productTextInv);
//		Thread.sleep(3500);
//		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		selectText(wareHouseSerialFifoSI, warehouseVal1);
		Thread.sleep(2000);
		sendKeys(unitPriceSerialFifoSI, unitPriceValInv);
//		Thread.sleep(2000);
//		sendKeys(qtyTextSerialFifoSI, quantityValInv);
		Thread.sleep(1000);
		click(checkoutInvoiceOut);
		Thread.sleep(2000);
//		checkCalculationsSalesOrder();nadeesh
		Thread.sleep(2000);
		click(draft);
		Thread.sleep(5000);
		String header = getText(releaseInHeader);

		SO = getText(documentVal);
		System.out.println(SO);
		trackCode = getText(documentVal);
		// System.out.println(trackCode);

		if (header.equals("(Draft)")) {
			writeTestResults("User should be able to draft the sales order", "Sales order should be drafted",
					"Sales order drafted", "Pass");

		} else {
			writeTestResults("User should be able to draft the sales order", "Sales order should be drafted",
					"Sales order not drafted", "Fail");
		}

		Thread.sleep(3000);
		click(menuSerial);
		Thread.sleep(3000);
		click(serialWise);
		Thread.sleep(7000);
		serialNoVal = getText(serialTextVal);
		Thread.sleep(2000);
		click(serialMenuClose);
		Thread.sleep(3000);
		mouseMove(menuBatchSpecificSI);
		click(menuBatchSpecificSI);
		Thread.sleep(3000);
		click(serialWise);
		Thread.sleep(3000);
		batchNoBatchSpecific = getText(batchSpecificBatchNo);
		Thread.sleep(2000);
		click(serialMenuClose);
		Thread.sleep(2000);
		mouseMove(menuSerialBatchSpecSI);
		click(menuSerialBatchSpecSI);
		Thread.sleep(2000);
		click(serialWise);
		Thread.sleep(2000);
		serialNoSerialBatchSpec = getText(serialBatchSpec);
		Thread.sleep(2000);
		click(serialMenuClose);
		Thread.sleep(2000);
		click(serialBatch);
		Thread.sleep(5000);
		click(productListBtnSerialSpecific);
		Thread.sleep(3000);
		sendKeys(serialText, serialNoVal);
		Thread.sleep(2000);
		pressEnter(serialText);
		Thread.sleep(2000);
		click(updateBtn);
		Thread.sleep(2000);
		click(batchSpecificListBtn);
		Thread.sleep(2000);
		sendKeys(batchNoText, batchNoBatchSpecific);
		Thread.sleep(2000);
		pressEnter(batchNoText);
		Thread.sleep(2000);
		click(updateBtn);
		Thread.sleep(2000);
		click(serialbatchSpecificListBtn);
		Thread.sleep(2000);
		sendKeys(batchNoTextSerialBatchSpecific, serialNoSerialBatchSpec);
		Thread.sleep(2000);
		pressEnter(batchNoTextSerialBatchSpecific);
		Thread.sleep(2000);
		click(updateBtn);
		Thread.sleep(2000);
		click(backBtn);
		Thread.sleep(2000);
		click(release);
		Thread.sleep(6000);

		if (isDisplayed(outboundLink)) {
			writeTestResults("User should be able to release the sales invoice", "Sales invoice should be released",
					"Sales invoice released", "Pass");

			click(outboundLink);
			Thread.sleep(4000);

		} else {
			writeTestResults("User should be able to release the sales invoice", "Sales invoice should be released",
					"Sales invoice not released", "Fail");
		}
	}

	public void outboundshipmentForSalesInvoice() throws Exception {

		Thread.sleep(3000);
		switchWindow();
		Thread.sleep(2000);
		click(checkoutOutbound);
		Thread.sleep(3000);
		click(draft);
		Thread.sleep(5000);
		click(release);
		Thread.sleep(6000);
		String header2 = getText(releaseInHeader);

		trackCode = getText(documentVal);
		System.out.println(trackCode);

		if (header2.equals("(Released)")) {
			writeTestResults("User should be able to release the outbound shipment",
					"outbound shipment should be released", "Outbound shipment released", "Pass");

		} else {
			writeTestResults("User should be able to release the outbound shipment",
					"outbound shipment should be released", "Outbound shipment not released", "Fail");
		}

	}

	// Verify that user is able to create sales Invoice (Service) --6

	public void checkClickSalesInvoiceService6() throws Exception {

		if (isDisplayed(salesInvoiceService)) {
			writeTestResults("Verify that Sales Invoice page is available",
					"user should be able to see the sales invoice page", "Sales invoice page is dislayed", "pass");

			click(salesInvoiceService);
			Thread.sleep(2000);
		} else {
			writeTestResults("Verify that Sales Invoice page is available",
					"user should be able to see the sales invoice page'", "Sales invoice is not dislayed", "Fail");
		}
	}

	public void checkJouneyLoading6() throws Exception {

		click(newSalesInvoiceBtn);
		Thread.sleep(2000);
		if (isDisplayed(salesInvoiceSerJourney)) {

			writeTestResults("Verify journey window", "user should be able to see the journeys",
					"Journey window is displayed", "pass");

			click(salesInvoiceSerJourney);
			Thread.sleep(2000);
		} else {

			writeTestResults("Verify journey window", "user should be able to see the journeys",
					"Journey window is not displayed", "Fail");
		}
	}

	public void fillDetailsSalesInvoiceSer6() throws Exception {

		click(cusAccountSearch);
		Thread.sleep(1000);
		sendKeys(customerAccountText, customerAccVal);
		Thread.sleep(1000);
		pressEnter(customerAccountText);
		Thread.sleep(2500);
		doubleClick(selected_val);
		Thread.sleep(2000);
		selectIndex(currencyDropdown, 5);
		Thread.sleep(2000);
		click(apply_btn);
		selectIndex(salesUnit, 1);
		Thread.sleep(2000);

		// Service Product
		click(productLookupBtn);
		Thread.sleep(2000);
		click(productTextSer);
		InventoryAndWarehouseModule obj = new InventoryAndWarehouseModule();
		sendKeys(productTextSer, obj.readTestCreation("Service"));
		pressEnter(productTextSer);
		Thread.sleep(3500);
		doubleClick(serviceSelected);
		Thread.sleep(2000);
		click(unitPriceService);
		sendKeys(unitPriceService, unitPriceSerVal);
		sendKeys(qtyServiceInvoice, qtyValSalesInvoiceSer);
		Thread.sleep(2000);
		click(checkoutSalesInvoice);
		Thread.sleep(3500);

		String total = getText(totalSalesInvoiceSer);
		String tot1 = total.substring(0, total.length() - 3);
		String tot2 = tot1.replace(",", "").replace(".00", "");

		int tot3 = Integer.parseInt(tot2);

		if (tot3 == 6000) {
			writeTestResults("Verify the calculations in sales invoice", "Calculations should be update",
					"Calculations are accurate", "Pass");
		} else {
			writeTestResults("Verify the calculations in sales invoice", "Calculations should be update",
					"Calculations are wrong", "Fail");

		}

		click(draft);
		Thread.sleep(5000);
		click(release);
		Thread.sleep(7000);
		String header = getText(releaseInHeader);

		trackCode = getText(documentVal);
		System.out.println(header);

		if (header.equals("(Released)")) {
			writeTestResults("User should be able to release the sales invoice", "Sales invoice should be released",
					"Sales invoice released", "Pass");
		} else {
			writeTestResults("User should be able to release the sales invoice", "Sales invoice should be released",
					"Sales invoice not released", "Fail");
		}
	}

	public void checkInventory() throws Exception {

		if (isEnabled(warehouseSer)) {
			writeTestResults("Verify that warehouse field is disabled", "Warehouse field should be disabled",
					"Warehouse field is enabled", "Fail");
		} else {
			writeTestResults("Verify that warehouse field is disabled", "Warehouse field should be disabled",
					"Warehouse field is disabled", "Pass");
		}
	}

	public void checkFields() throws Exception {

		if (isEnabled(warehouseSer)) {
			writeTestResults("Verify the warehouse", "Warehouse dropdown should be disable for service",
					"Warehouse field is enabled", "Fail");
		} else {
			writeTestResults("Verify the warehouse", "Warehouse dropdown should be disable for service",
					"Warehouse field is disabled", "Pass");
		}
	}

	// Verify that user is able to create sales Quotation with a lead(Sales
	// Quotation to Sales Invoice) -- 7

	public void checkClickSalesQuatationPage() throws Exception {

		if (isDisplayed(salesQuatationBtn)) {
			writeTestResults("Verify that Sales Quatation page is available",
					"user should be able to see the sales quatation page", "Sales quatation page is dislayed", "pass");
			click(salesQuatationBtn);
			Thread.sleep(2000);
		} else {
			writeTestResults("Verify that Sales Quatation page is available",
					"user should be able to see the sales quatation page'", "Sales quatation is not dislayed", "Fail");
		}

	}

	public void checkJourneySalesQuatationToSalesInvoice() throws Exception {

		click(newSalesQuatation);
		Thread.sleep(2000);

		if (isDisplayed(journey1Quatation)) {
			writeTestResults("Verify the availability of Sales Quatation to Sales Invoice journey",
					"user should be able to see the Sales Order to Sales Invoice journey", "Journey is displayed",
					"pass");

			click(journey1Quatation);
			Thread.sleep(2000);
		} else {
			writeTestResults("Verify the availability of Sales Quatation to Sales Invoice journey",
					"user should be able to see the journeys", "Journey is not displayed", "Fail");
		}
	}

	public void fillDetailsSalesQuatation() throws Exception {

		selectIndex(accountHead, 0);
		click(customerAccSearchQuat);
		Thread.sleep(2000);
		sendKeys(customerAccountText, customerAccValQuat);
		Thread.sleep(2000);
		pressEnter(customerAccountText);
		Thread.sleep(2500);
		doubleClick(selectedValLead);
		Thread.sleep(2000);
		// billing address and shipping address
		click(billingAddrSearch);
		Thread.sleep(2000);
		sendKeys(billingAddrTxtQuat, billingAddrQuatVal);
		Thread.sleep(2000);
		sendKeys(shippingAddrTxtQuat, shippingAddrQuatVal);
		click(apply_btn);
		Thread.sleep(2000);
		selectIndex(salesUnit, 3);

		// Service Product
		InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();

		sendKeys(txt_ProductA, obj1.readTestCreation("Service"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, quantityValInv);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(productSearchQuatation);
//		Thread.sleep(2000);
//		InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();
//		sendKeys(productTextSer, obj1.readTestCreation("Service"));
//		Thread.sleep(3000);
//		pressEnter(productTextSer);
//		Thread.sleep(3500);
//		doubleClick(selectedValQuatInv);
		Thread.sleep(3000);
		sendKeys("//tr[1]//td[17]//input[1]", unitPriceVal);

		// Serial Specific

		sendKeys(txt_ProductA, obj1.readTestCreation("SerielSpecific"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, quantityValInv);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(addNew);
//		click(searchBtnQuata);
//		Thread.sleep(3000);
//		InventoryAndWarehouseModule obj2 = new InventoryAndWarehouseModule();
//		sendKeys(productTextInv, obj2.readTestCreation("SerielSpecific"));
//		Thread.sleep(3000);
//		pressEnter(productTextInv);
//		Thread.sleep(3500);
//		doubleClick(selectedValQuatInv);
		Thread.sleep(3000);
		sendKeys("//tr[2]//td[17]//input[1]", unitPriceQuatVal);
//		Thread.sleep(2000);

		// Batch specific

		sendKeys(txt_ProductA, obj1.readTestCreation("BatchSpecific"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, qtyValQout);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(addNewBatchSpecificQuot);
//		Thread.sleep(1000);
//		click(searchBatchSpecQuot);
//		Thread.sleep(3000);
//		InventoryAndWarehouseModule obj3 = new InventoryAndWarehouseModule();
//		sendKeys(productTextInv, obj3.readTestCreation("BatchSpecific"));
//		Thread.sleep(3000);
//		pressEnter(productTextInv);
//		Thread.sleep(3500);
//		doubleClick(selectedValQuatInv);
		Thread.sleep(3000);
		sendKeys(unitpriceBatchSpecQout, unitPriceQuatVal);
//		Thread.sleep(2000);
//		sendKeys(qtyBatchSpecQuot, qtyValQout);

		// Batch Fifo

		sendKeys(txt_ProductA, obj1.readTestCreation("BatchFifo"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, qtyValQout);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(addNewBatchFifoQuot);
//		click(searchBatchFifoQuot);
//		Thread.sleep(3000);
//		InventoryAndWarehouseModule obj4 = new InventoryAndWarehouseModule();
//		sendKeys(productTextInv, obj4.readTestCreation("BatchFifo"));
//		Thread.sleep(3000);
//		pressEnter(productTextInv);
//		Thread.sleep(3500);
//		doubleClick(selectedValQuatInv);
		Thread.sleep(3000);
		sendKeys(unitpriceBatchFifoQout, unitPriceQuatVal);
//		Thread.sleep(2000);
//		sendKeys(qtyBatchFifoQuot, qtyValQout);

		// Serial Batch Specific

		sendKeys(txt_ProductA, obj1.readTestCreation("SerielBatchSpecific"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, qtyValQout);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(addNewSerialBatchSpecQuot);
//		click(searchSerialBatchSpecQuot);
//		Thread.sleep(3000);
//		InventoryAndWarehouseModule obj5 = new InventoryAndWarehouseModule();
//		sendKeys(productTextInv, obj5.readTestCreation("SerielBatchSpecific"));
//		Thread.sleep(3000);
//		pressEnter(productTextInv);
//		Thread.sleep(3500);
//		doubleClick(selectedValQuatInv);
		Thread.sleep(3000);
		sendKeys(unitpriceSerialBatchSpecQout, unitPriceQuatVal);
//		Thread.sleep(2000);
//		sendKeys(qtySerialBatchSpecQuot, qtyValQout);

		// Serial Batch Fifo

		sendKeys(txt_ProductA, obj1.readTestCreation("SerielBatchFifo"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, qtyValQout);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(addNewSerBatchFifoQuot);
//		click(searchSerBatchFifoQuot);
//		Thread.sleep(3000);
//		InventoryAndWarehouseModule obj6 = new InventoryAndWarehouseModule();
//		sendKeys(productTextInv, obj6.readTestCreation("SerielBatchFifo"));
//		Thread.sleep(3000);
//		pressEnter(productTextInv);
//		Thread.sleep(3500);
//		doubleClick(selectedValQuatInv);
		Thread.sleep(3000);
		sendKeys(unitpriceSerBatchFifoQout, unitPriceQuatVal);
//		Thread.sleep(2000);
//		sendKeys(qtySerBatchFifoQuot, qtyValQout);

		// Serial Fifo

		sendKeys(txt_ProductA, obj1.readTestCreation("SerirlFifo"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, qtyValQout);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(addNewSerialFifoQuot);
//		click(searchSerialFifoQuot);
//		Thread.sleep(3000);
//		InventoryAndWarehouseModule obj7 = new InventoryAndWarehouseModule();
//		sendKeys(productTextInv, obj7.readTestCreation("SerirlFifo"));
//		Thread.sleep(3000);
//		pressEnter(productTextInv);
//		Thread.sleep(3500);
//		doubleClick(selectedValQuatInv);
		Thread.sleep(3000);
		sendKeys(unitpriceSerialFifoQout, unitPriceQuatVal);
//		Thread.sleep(2000);
//		sendKeys(qtySerialFifoQuot, qtyValQout);
		Thread.sleep(2000);
		click(checkoutQuatation);
		Thread.sleep(2000);
		click(draftQuatation);
		Thread.sleep(6000);

		String header3 = getText(releaseInHeader);

		trackCode = getText(documentVal);
		System.out.println(trackCode);

		if (header3.equals("(Draft)")) {
			writeTestResults("User should be able to draft the sales quotation", "Sales quotation should be draft",
					"Sales quotation draft", "Pass");
		} else {
			writeTestResults("User should be able to draft the sales quotation", "Sales quotation should be draft",
					"Sales quotation not draft", "Fail");
		}

		click(releaseQuatation);
		Thread.sleep(6000);
		click(versionBtn);
		Thread.sleep(3000);
		// mouseMove(versionTab);
		mouseMove("//div[@class='color-selected']");

		mouseMove(confirmVersion);
		Thread.sleep(2000);

		if (isDisplayed("//span[@title='Confirm this version']")) {
			writeTestResults("User should be able to release the sales quotation", "Sales quotation should be released",
					"Sales quotation released", "Pass");
		} else {
			writeTestResults("User should be able to release the sales quotation", "Sales quotation should be released",
					"Sales quotation not released", "Fail");
		}

		click(confirmVersion);
		Thread.sleep(3000);
		click(tickConfirmVersion);
		Thread.sleep(2000);
		click(confirmBtn);
		Thread.sleep(3000);
		click(outboundLink);
		Thread.sleep(6000);
		switchWindow();
		selectText(warehouseSerialSpecificQuot, warehouseValQuot);
		selectText(warehouseBatchSpecificQuot, warehouseValQuot);
		selectText(warehouseBatchFifoQuot, warehouseValQuot);
		selectText(warehouseSerBatchSpecQuot, warehouseValQuot);
		selectText(warehouseSerialBatchFifoQuot, warehouseValQuot);
		selectText(warehouseSerialFifoQuot, warehouseValQuot);
		Thread.sleep(2000);
		click(checkoutSalesOrderQuata);
		Thread.sleep(2000);
		click(draftSalesOrderQuata);
		Thread.sleep(5000);

		String header2 = getText(releaseInHeader);

		trackCode = getText(documentVal);
		System.out.println(trackCode);

		if (header2.equals("(Draft)")) {
			writeTestResults("User should be able to draft the sales order", "Sales order should be draft",
					"Sales order draft", "Pass");

		} else {
			writeTestResults("User should be able to draft the sales order", "Sales order should be draft",
					"Sales order not draft", "Fail");
		}

		click(menuQuot);
		Thread.sleep(3000);
		click(serialWise);
		Thread.sleep(8000);
		serialNoVal = getText(serialTextVal);
		Thread.sleep(2000);
		click(serialMenuClose);
		Thread.sleep(2000);
		click(menuBatchSpec);
		click(serialWise);
		Thread.sleep(4000);
		batchNoVal = getText(batchNo);
		Thread.sleep(3000);
		click(serialMenuClose);

		click(menuSerialBatchSpecQuot);
		click(serialWise);
		Thread.sleep(3000);
		SerialBatchNo = getText(serialBatchNo);
		click(serialMenuClose);

		click(release);
		Thread.sleep(6000);

		if (isDisplayed(linkSalesOrder)) {
			writeTestResults("User should be able to release the sales order", "Sales order should be released",
					"Sales order released", "Pass");
			click(linkSalesOrder);
			Thread.sleep(6000);
		} else {
			writeTestResults("User should be able to release the sales order", "Sales order should be released",
					"Sales order not released", "Fail");
		}

		switchWindow();
		click(checkoutOutboundquata);
		Thread.sleep(2000);
		click(draftOutboundquata);
		Thread.sleep(5000);

		String header4 = getText(releaseInHeader);

		trackCode = getText(documentVal);
		System.out.println(trackCode);

		if (header4.equals("(Draft)")) {
			writeTestResults("User should be able to draft the outbound shipment", "Outbound shipment should be draft",
					"Outbound shipment draft", "Pass");

		} else {
			writeTestResults("User should be able to draft the outbound shipment", "Outbound shipment should be draft",
					"Outbound shipment not draft", "Fail");
		}

		click(serialOutboundQuata);
		Thread.sleep(3000);
		click(sideMenuQuat);
		Thread.sleep(3000);
		// System.out.println(serialNoVal);
		sendKeys(batchNoTxtQuat, serialNoVal);
		Thread.sleep(2000);
		pressEnter(batchNoTxtQuat);
		Thread.sleep(2000);
		click(updateBtnQuat);
		Thread.sleep(2000);

		click(batchSpecificQuat);
		Thread.sleep(2000);
		sendKeys(batchNoTxtBatchSpec, batchNoVal);
		pressEnter(batchNoTxtBatchSpec);
		Thread.sleep(2000);
		click(updateBtnQuat);
		Thread.sleep(2000);

		click(serialBatchSpecItem);
		Thread.sleep(2000);
		sendKeys(TextSerialBatchSpec, SerialBatchNo);
		pressEnter(TextSerialBatchSpec);
		Thread.sleep(2000);
		click(updateBtnQuat);
		Thread.sleep(2000);

		click(backBtnQuat);
		Thread.sleep(2000);
		click(release);
		Thread.sleep(6000);

		if (isDisplayed(linkSalesOrder)) {
			writeTestResults("User should be able to release the outbound shipment",
					"Outbound shipment should be released", "Outbound shipment released", "Pass");
			click(linkSalesOrder);
			Thread.sleep(7000);
		} else {
			writeTestResults("User should be able to release the outbound shipment",
					"Outbound shipment should be released", "Outbound shipment not released", "Fail");
		}

		switchWindow();
		click(checkoutQuatation);
		Thread.sleep(2000);
		click(draft);
		Thread.sleep(5000);
		click(release);
		Thread.sleep(7000);
		String header = getText(releaseInHeader);

		trackCode = getText(documentVal);
		System.out.println(trackCode);

		if (header.equals("(Released)")) {
			writeTestResults("User should be able to release the sales invoice", "Sales invoice should be released",
					"Sales invoice released", "Pass");

		} else {
			writeTestResults("User should be able to release the sales invoice", "Sales invoice should be released",
					"Sales invoice not released", "Fail");
		}

	}

	// Verify that user is able to create sales Quotation with a lead(Sales
	// Quotation to Sales Invoice - Drop Shipment) -- 8

	public void checkJourneySalesQuatationToSalesInvoiceDrop() throws Exception {

		click(newSalesQuatation);

		if (isDisplayed(journey2Quatation)) {
			writeTestResults("Verify the availability of Sales Quatation to Sales Invoice journey",
					"user should be able to see the Sales Order to Sales Invoice journey", "Journey is displayed",
					"pass");

			click(journey2Quatation);
		} else {
			writeTestResults("Verify the availability of Sales Quatation to Sales Invoice journey",
					"user should be able to see the journeys", "Journey is not displayed", "Fail");
		}
	}

	public void fillDetailsSalesQuatationDrop() throws Exception {

		selectIndex(accountHead, 0);
		click(customerAccSearchQuat);
		Thread.sleep(2000);
		sendKeys(customerAccountText, customerAccValQuat);
		Thread.sleep(2000);
		pressEnter(customerAccountText);
		Thread.sleep(2000);
		doubleClick(selectedValLead);
		Thread.sleep(2000);
		click(billingAddrSearch);
		Thread.sleep(2000);
		sendKeys(billingAddrTxtQuat, billingAddrQuatVal);
		Thread.sleep(2000);
		sendKeys(shippingAddrTxtQuat, shippingAddrQuatVal);
		Thread.sleep(2000);
		click(apply_btn);
		Thread.sleep(2000);
		selectIndex(salesUnit, 3);

		// Service Product
		click(productSearchQuatation);
		Thread.sleep(2000);
		InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();
		sendKeys(productTextSer, obj1.readTestCreation("SerielSpecific"));
		pressEnter(productTextSer);
		Thread.sleep(3500);
		doubleClick(selectedValQuatInv);
		Thread.sleep(2000);
		sendKeys(unitpriceQuatation, unitPriceVal);

		// Serial Specific
		click(addNew);
		click(searchBtnQuata);
		Thread.sleep(3000);
		InventoryAndWarehouseModule obj2 = new InventoryAndWarehouseModule();
		sendKeys(productTextInv, obj2.readTestCreation("BatchSpecific"));
		Thread.sleep(3000);
		pressEnter(productTextInv);
		Thread.sleep(3500);
		doubleClick(selectedValQuatInv);
		Thread.sleep(2000);
		sendKeys(unitpriceQuatationInv, unitPriceQuatVal);
		Thread.sleep(2000);

		// Batch specific
		click(addNewBatchSpecificQuot);
		click(searchBatchSpecQuot);
		Thread.sleep(3000);
		InventoryAndWarehouseModule obj3 = new InventoryAndWarehouseModule();
		sendKeys(productTextInv, obj3.readTestCreation("BatchFifo"));
		Thread.sleep(3000);
		pressEnter(productTextInv);
		Thread.sleep(3500);
		doubleClick(selectedValQuatInv);
		Thread.sleep(2000);
		sendKeys(unitpriceBatchSpecQout, unitPriceQuatVal);
		Thread.sleep(2000);
		sendKeys(qtyBatchSpecQuot, qtyValQout);

		// Batch Fifo
		click(addNewBatchFifoQuot);
		click(searchBatchFifoQuot);
		Thread.sleep(3000);
		InventoryAndWarehouseModule obj4 = new InventoryAndWarehouseModule();
		sendKeys(productTextInv, obj4.readTestCreation("SerielBatchSpecific"));
		Thread.sleep(3000);
		pressEnter(productTextInv);
		Thread.sleep(3500);
		doubleClick(selectedValQuatInv);
		Thread.sleep(2000);
		sendKeys(unitpriceBatchFifoQout, unitPriceQuatVal);
		Thread.sleep(2000);
		sendKeys(qtyBatchFifoQuot, qtyValQout);

		// Serial Batch Specific
		click(addNewSerialBatchSpecQuot);
		click(searchSerialBatchSpecQuot);
		Thread.sleep(3000);
		InventoryAndWarehouseModule obj5 = new InventoryAndWarehouseModule();
		sendKeys(productTextInv, obj5.readTestCreation("SerielBatchFifo"));
		Thread.sleep(3000);
		pressEnter(productTextInv);
		Thread.sleep(3500);
		doubleClick(selectedValQuatInv);
		Thread.sleep(2000);
		sendKeys(unitpriceSerialBatchSpecQout, unitPriceQuatVal);
		Thread.sleep(2000);
		sendKeys(qtySerialBatchSpecQuot, qtyValQout);

		// Serial Batch Fifo
		click(addNewSerBatchFifoQuot);
		click(searchSerBatchFifoQuot);
		Thread.sleep(3000);
		InventoryAndWarehouseModule obj6 = new InventoryAndWarehouseModule();
		sendKeys(productTextInv, obj6.readTestCreation("SerirlFifo"));
		Thread.sleep(3000);
		pressEnter(productTextInv);
		Thread.sleep(3500);
		doubleClick(selectedValQuatInv);
		Thread.sleep(2000);
		sendKeys(unitpriceSerBatchFifoQout, unitPriceQuatVal);
		Thread.sleep(2000);
		sendKeys(qtySerBatchFifoQuot, qtyValQout);

//						// Serial Fifo
//
//						click(addNewSerialFifoQuot);
//						click(searchSerialFifoQuot);
//						Thread.sleep(3000);
//
//						InventoryAndWarehouseModule obj7 = new InventoryAndWarehouseModule();
//						sendKeys(productTextInv, obj7.readTestCreation("SerirlFifo"));
//
//						Thread.sleep(3000);
//						pressEnter(productTextInv);
//						Thread.sleep(3000);
//						doubleClick(selectedValQuatInv);
//						Thread.sleep(2000);
//						sendKeys(unitpriceSerialFifoQout, unitPriceQuatVal);
//						Thread.sleep(2000);
//						sendKeys(qtySerialFifoQuot, qtyValQout);

		click(checkoutQuatation);
		Thread.sleep(2000);
		click(draftQuatation);
		Thread.sleep(5000);
		click(releaseQuatation);
		Thread.sleep(6000);

		String header = getText(releaseInHeader);

		trackCode = getText(documentVal);
		System.out.println(trackCode);

		if (header.equals("(Released)")) {
			writeTestResults("User should be able to release the sales quotation", "Sales quotation should be released",
					"Sales quotation released", "Pass");
		} else {
			writeTestResults("User should be able to release the sales quotation", "Sales quotation should be released",
					"Sales quotation not released", "Fail");
		}

		click(versionBtn);
		Thread.sleep(2000);
		// mouseMove(versionTab);
		mouseMove("//div[@class='color-selected']");
		Thread.sleep(2000);
		click("//span[@title='Confirm this version']");
		Thread.sleep(3000);
		click(tickConfirmVersion);
		Thread.sleep(2000);
		click(confirmBtn);
		Thread.sleep(4000);
		click(linkSalesOrder);
		Thread.sleep(5000);
		switchWindow();
		Thread.sleep(3000);
		click(checkoutSalesOrderQuata);
		Thread.sleep(3000);
		click(draftSalesOrderQuata);
		Thread.sleep(5000);
		click(release);
		Thread.sleep(5000);
		String header1 = getText(releaseInHeader);

		trackCode = getText(documentVal);
		System.out.println(trackCode);

		salesOrderID = getText(documentVal);

		if (header1.equals("(Released)")) {
			writeTestResults("User should be able to draft the sales order", "Sales order should be drafted",
					"Sales order drafted", "Pass");
		} else {
			writeTestResults("User should be able to draft the sales order", "Sales order should be drafted",
					"Sales order not drafted", "Fail");
		}

		click(actionBtn);
		Thread.sleep(2000);
		click(convert);
		Thread.sleep(4000);
		click(vendorSearch);
		Thread.sleep(2000);
		sendKeys(vendorTextt, vendorVal2);
		Thread.sleep(2000);
		pressEnter(vendorTextt);
		Thread.sleep(2000);
		doubleClick(selectedValQuat2);
		Thread.sleep(2000);
		click(checkoutOutboundquata);
		Thread.sleep(3000);
		click(draftOutboundquata);
		Thread.sleep(5000);

		String header2 = getText(releaseInHeader);

		trackCode = getText(documentVal);
		System.out.println(trackCode);

		if (header2.equals("(Draft)")) {
			writeTestResults("User should be able to draft the purchase order", "Purchase order should be draft",
					"Purchase order draft", "Pass");
		} else {
			writeTestResults("User should be able to draft the purchase order", "Purchase order should be draft",
					"Purchase order not draft", "Fail");
		}

		click(release);
		Thread.sleep(5000);

		if (isDisplayed(linkSalesOrder)) {
			writeTestResults("User should be able to release the purchase order", "Purchase order should be released",
					"Purchase order released", "Pass");
			click(linkSalesOrder);
			Thread.sleep(4000);
		} else {
			writeTestResults("User should be able to release the purchase order", "Purchase order should be released",
					"Purchase order not released", "Fail");
		}

		switchWindow();
		click(checkoutOutboundquata);
		Thread.sleep(3000);
		click(draftOutboundquata);
		Thread.sleep(5000);
		click(release);
		Thread.sleep(5000);

		String header3 = getText(releaseInHeader);

		trackCode = getText(documentVal);
		System.out.println(trackCode);

		if (header3.equals("(Released)")) {
			writeTestResults("User should be able to release the purchase invoice",
					"Purchase invoice should be released", "Purchase invoice released", "Pass");

		} else {
			writeTestResults("User should be able to release the purchase invoice",
					"Purchase invoice should be released", "Purchase invoice not released", "Fail");
		}
	}

	// Verify that user is able to create sales Quotation with a lead(Sales
	// Quotation to Outbound Shipment) -- 9

	public void checkJourneySalesQuatationToOutbound() throws Exception {

		click(newSalesQuatation);

		if (isDisplayed(journey3Quatation)) {
			writeTestResults("Verify the availability of Sales Quatation to Sales Invoice journey",
					"user should be able to see the Sales Order to Sales Invoice journey", "Journey is displayed",
					"pass");

			click(journey3Quatation);
		} else {
			writeTestResults("Verify the availability of Sales Quatation to Sales Invoice journey",
					"user should be able to see the journeys", "Journey is not displayed", "Fail");
		}
	}

	public void fillDetailsSalesQuatation9() throws Exception {

		selectIndex(accountHead, 0);
		click(customerAccSearchQuat);
		Thread.sleep(2000);
		sendKeys(customerAccountText, customerAccValQuat);
		Thread.sleep(2000);
		pressEnter(customerAccountText);
		Thread.sleep(2000);
		doubleClick(selectedValLead);
		Thread.sleep(2000);
		click(billingAddrSearch);
		Thread.sleep(2000);
		sendKeys(billingAddrTxtQuat, billingAddrQuatVal);
		sendKeys(shippingAddrTxtQuat, shippingAddrQuatVal);
		click(apply_btn);
		Thread.sleep(2000);
		selectIndex(salesUnit, 3);

		// Service Product
		InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();

		sendKeys(txt_ProductA, obj1.readTestCreation("Service"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, quantityValInv);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(productSearchQuatation);
//		Thread.sleep(2000);
//		InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();
//		sendKeys(productTextSer, obj1.readTestCreation("Service"));
//		pressEnter(productTextSer);
//		Thread.sleep(3500);
//		doubleClick(selectedValQuatInv);
		Thread.sleep(3000);
		sendKeys("//tr[1]//td[17]//input[1]", unitPriceVal);

		// Serial Specific

		sendKeys(txt_ProductA, obj1.readTestCreation("SerielSpecific"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, quantityValInv);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(addNew);
//		click(searchBtnQuata);
//		Thread.sleep(3000);
//		InventoryAndWarehouseModule obj2 = new InventoryAndWarehouseModule();
//		sendKeys(productTextInv, obj2.readTestCreation("SerielSpecific"));
//		Thread.sleep(3000);
//		pressEnter(productTextInv);
//		Thread.sleep(3000);
//		doubleClick(selectedValQuatInv);
		Thread.sleep(3000);
		sendKeys("//tr[2]//td[17]//input[1]", unitPriceQuatVal);
//		Thread.sleep(2000);

		// Batch specific

		sendKeys(txt_ProductA, obj1.readTestCreation("BatchSpecific"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, qtyValQout);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(addNewBatchSpecificQuot);
//		click(searchBatchSpecQuot);
//		Thread.sleep(3000);
//		InventoryAndWarehouseModule obj3 = new InventoryAndWarehouseModule();
//		sendKeys(productTextInv, obj3.readTestCreation("BatchSpecific"));
//		Thread.sleep(3000);
//		pressEnter(productTextInv);
//		Thread.sleep(3500);
//		doubleClick(selectedValQuatInv);
		Thread.sleep(3000);
		sendKeys(unitpriceBatchSpecQout, unitPriceQuatVal);
//		Thread.sleep(2000);
//		sendKeys(qtyBatchSpecQuot, qtyValQout);

		// Batch Fifo

		sendKeys(txt_ProductA, obj1.readTestCreation("BatchFifo"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, qtyValQout);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(addNewBatchFifoQuot);
//		click(searchBatchFifoQuot);
//		Thread.sleep(3000);
//		InventoryAndWarehouseModule obj4 = new InventoryAndWarehouseModule();
//		sendKeys(productTextInv, obj4.readTestCreation("BatchFifo"));
//		Thread.sleep(3000);
//		pressEnter(productTextInv);
//		Thread.sleep(3500);
//		doubleClick(selectedValQuatInv);
		Thread.sleep(3000);
		sendKeys(unitpriceBatchFifoQout, unitPriceQuatVal);
//		Thread.sleep(2000);
//		sendKeys(qtyBatchFifoQuot, qtyValQout);

		// Serial Batch Specific

		sendKeys(txt_ProductA, obj1.readTestCreation("SerielBatchSpecific"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, qtyValQout);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(addNewSerialBatchSpecQuot);
//		click(searchSerialBatchSpecQuot);
//		Thread.sleep(3000);
//		InventoryAndWarehouseModule obj5 = new InventoryAndWarehouseModule();
//		sendKeys(productTextInv, obj5.readTestCreation("SerielBatchSpecific"));
//		Thread.sleep(3000);
//		pressEnter(productTextInv);
//		Thread.sleep(3500);
//		doubleClick(selectedValQuatInv);
		Thread.sleep(3000);
		sendKeys(unitpriceSerialBatchSpecQout, unitPriceQuatVal);
//		Thread.sleep(2000);
//		sendKeys(qtySerialBatchSpecQuot, qtyValQout);

		// Serial Batch Fifo

		sendKeys(txt_ProductA, obj1.readTestCreation("SerielBatchFifo"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, qtyValQout);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(addNewSerBatchFifoQuot);
//		click(searchSerBatchFifoQuot);
//		Thread.sleep(3000);
//		InventoryAndWarehouseModule obj6 = new InventoryAndWarehouseModule();
//		sendKeys(productTextInv, obj6.readTestCreation("SerielBatchFifo"));
//		Thread.sleep(3000);
//		pressEnter(productTextInv);
//		Thread.sleep(3500);
//		doubleClick(selectedValQuatInv);
		Thread.sleep(3000);
		sendKeys(unitpriceSerBatchFifoQout, unitPriceQuatVal);
//		Thread.sleep(2000);
//		sendKeys(qtySerBatchFifoQuot, qtyValQout);

		// Serial Fifo

		sendKeys(txt_ProductA, obj1.readTestCreation("SerirlFifo"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, qtyValQout);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(addNewSerialFifoQuot);
//		click(searchSerialFifoQuot);
//		Thread.sleep(3000);
//		InventoryAndWarehouseModule obj7 = new InventoryAndWarehouseModule();
//		sendKeys(productTextInv, obj7.readTestCreation("SerirlFifo"));
//		Thread.sleep(3000);
//		pressEnter(productTextInv);
//		Thread.sleep(3500);
//		doubleClick(selectedValQuatInv);
		Thread.sleep(3000);
		sendKeys(unitpriceSerialFifoQout, unitPriceQuatVal);
//		Thread.sleep(2000);
//		sendKeys(qtySerialFifoQuot, qtyValQout);

		Thread.sleep(2000);
		click(checkoutQuatation);
		Thread.sleep(2000);
		click(draftQuatation);
		Thread.sleep(5000);
		click(releaseQuatation);
		Thread.sleep(6000);

		String header3 = getText(releaseInHeader);

		trackCode = getText(documentVal);
		System.out.println(trackCode);

		if (header3.equals("(Released)")) {
			writeTestResults("User should be able to release the sales quotation", "Sales quotation should be released",
					"Sales quotation released", "Pass");
		} else {
			writeTestResults("User should be able to release the sales quotation", "Sales quotation should be released",
					"Sales quotation not released", "Fail");
		}

		click(versionBtn);
		Thread.sleep(2000);
		// mouseMove(versionTab);
		mouseMove("//div[@class='color-selected']");
		Thread.sleep(2000);
		click("//span[@title='Confirm this version']");
		Thread.sleep(3000);
		click(tickConfirmVersion);
		Thread.sleep(2000);
		click(confirmBtn);
		Thread.sleep(3000);
		click(outboundLink);
		Thread.sleep(5000);
		switchWindow();
		Thread.sleep(2000);
		selectText(warehouseSerialSpecificQuot, warehouseValQuot);
		selectText(warehouseBatchSpecificQuot, warehouseValQuot);
		selectText(warehouseBatchFifoQuot, warehouseValQuot);
		selectText(warehouseSerBatchSpecQuot, warehouseValQuot);
		selectText(warehouseSerialBatchFifoQuot, warehouseValQuot);
		selectText(warehouseSerialFifoQuot, warehouseValQuot);
		Thread.sleep(2000);
		click(checkoutSalesOrderQuata);
		Thread.sleep(2000);
		click(draftSalesOrderQuata);
		Thread.sleep(5000);

		String header2 = getText(releaseInHeader);

		trackCode = getText(documentVal);
		System.out.println(trackCode);

		if (header2.equals("(Draft)")) {
			writeTestResults("User should be able to draft the sales order", "Sales invoice should be draft",
					"Sales order drafted", "Pass");
		} else {
			writeTestResults("User should be able to draft the sales order", "Sales invoice should be draft",
					"Sales order not drafted", "Fail");
		}

		click(release);
		Thread.sleep(6000);

		if (isDisplayed(linkSalesOrder)) {
			writeTestResults("User should be able to release the sales order", "Sales order should be released",
					"Sales order released", "Pass");
			click(linkSalesOrder);
			Thread.sleep(5000);
		} else {
			writeTestResults("User should be able to release the sales order", "Sales order should be released",
					"Sales order not released", "Fail");
		}

		switchWindow();
		Thread.sleep(2000);
		click(checkoutQuatation);
		Thread.sleep(2000);
		click(draft);
		Thread.sleep(5000);

		String header4 = getText(releaseInHeader);

		trackCode = getText(documentVal);
		System.out.println(trackCode);

		if (header4.equals("(Draft)")) {
			writeTestResults("User should be able to draft the sales invoice", "Sales invoice should be draft",
					"Sales invoice drafted", "Pass");
		} else {
			writeTestResults("User should be able to draft the sales invoice", "Sales invoice should be draft",
					"Sales invoice not drafted", "Fail");
		}
		click(release);
		Thread.sleep(6000);

		if (isDisplayed(linkSalesOrder)) {
			writeTestResults("User should be able to release the sales invoice", "Sales invoice should be released",
					"Sales invoice released", "Pass");

			click(linkSalesOrder);
			Thread.sleep(5000);
		} else {
			writeTestResults("User should be able to release the sales invoice", "Sales invoice should be relesed",
					"Sales invoice not released", "Fail");
		}

		switchWindow();
		click(checkoutOutboundquata);
		Thread.sleep(3000);
		click(draftOutboundquata);
		Thread.sleep(5000);
		click(menuOne);
		Thread.sleep(2000);
		click(serialWise);
		Thread.sleep(8000);
		String serial = getText(serialTextVal);
		click(serialMenuClose);
		Thread.sleep(2000);
		// System.out.println(serial);
		Thread.sleep(3000);
		click(menuBatchSpecic);
		Thread.sleep(2000);
		click(serialWise);
		Thread.sleep(4000);
		batchNoVal = getText(batchNo);
		Thread.sleep(3000);
		click(serialMenuClose);

		click(menuSerialBatchSpecQuot3);
		click(serialWise);
		Thread.sleep(3000);
		SerialBatchNo = getText(serialBatchNo);
		click(serialMenuClose);
		Thread.sleep(2000);
		click(serialOutboundQuata);
		Thread.sleep(3000);
		click(sideMenuQuat);
		Thread.sleep(3000);
		// System.out.println(serial);
		sendKeys("//input[@id='attrSlide-txtSerialFrom']", serial);
		Thread.sleep(2000);
		pressEnter("//input[@id='attrSlide-txtSerialFrom']");
		Thread.sleep(2000);
		click(updateBtnQuat);
		Thread.sleep(3000);

		// Batch specific
		click(batchSpecificQuat);
		Thread.sleep(2000);
		sendKeys(batchNoTxtBatchSpec, batchNoVal);
		pressEnter(batchNoTxtBatchSpec);
		Thread.sleep(3000);
		click(updateBtnQuat);
		Thread.sleep(2000);

		// Serial batch specific
		click(serialBatchSpecItem);
		Thread.sleep(2000);
		sendKeys(TextSerialBatchSpec, SerialBatchNo);
		pressEnter(TextSerialBatchSpec);
		Thread.sleep(3000);
		click(updateBtnQuat);
		Thread.sleep(3000);
		click(backBtnQuat);
		Thread.sleep(2000);
		click(release);
		Thread.sleep(6000);

		String header = getText(releaseInHeader);

		trackCode = getText(documentVal);
		// System.out.println(trackCode);

		if (header.equals("(Released)")) {
			writeTestResults("User should be able to release the outbound shipment",
					"Outbound shipment should be released", "Outbound shipment released", "Pass");
		} else {
			writeTestResults("User should be able to release the outbound shipment",
					"Outbound shipment should be released", "Outbound shipment not released", "Fail");
		}
	}

	// Verify that user is able to create Sales Return Order (Sales Return Order to
	// Sales Return Invoice) --- 10

	public void checkClickSalesReturnOrderPage() throws Exception {

		if (isDisplayed(salesReturnOrder)) {

			writeTestResults("Verify that Sales Order page is available",
					"user should be able to see the sales order page", "Sales order page is dislayed", "pass");
			click(salesReturnOrder);
			Thread.sleep(2000);
		} else {
			writeTestResults("Verify that Sales Order page is available",
					"user should be able to see the sales order page'", "Sales order is not dislayed", "Fail");
		}
	}

	public void checkJourneySalesReturnOrderToSalesReturnInvoice() throws Exception {

		click(newSalesReturnOrder);
		Thread.sleep(2000);

		if (isDisplayed(salesReturnJourney1)) {
			writeTestResults("Verify the availability of Sales Order to Sales Invoice journey",
					"user should be able to see the Sales Order to Sales Invoice journey", "Journey is displayed",
					"pass");
			click(salesReturnJourney1);
			Thread.sleep(2000);

		} else {
			writeTestResults("Verify the availability of Sales Order to Sales Invoice journey",
					"user should be able to see the journeys", "Journey is not displayed", "Fail");
		}
	}

	public void fillDetailsSalesReturnOrderToSalesReturnInvoice() throws Exception {

		click(customerAccSearchReturn);
		Thread.sleep(2000);
		sendKeys(customerAccTxtReturn, customerAccReturnVal);
		Thread.sleep(2000);
		pressEnter(customerAccTxtReturn);
		Thread.sleep(3000);
		doubleClick(selectedValCustomerReturn);
		Thread.sleep(2000);
		selectIndex(salesUnitReturn, 2);

		Thread.sleep(3000);
		JavascriptExecutor pr1 = (JavascriptExecutor) driver;
		pr1.executeScript("viewRefDocList()");
//		click(documentListReturn);aruna

		Thread.sleep(3000);
//		click(fromDateReturn);
//		Thread.sleep(2000);
//		click(fromDateSelected);
//		Thread.sleep(2000);
//		click(toDateReturn);
//		Thread.sleep(2000);
//		click(toDateSelected);
//		Thread.sleep(2000);
//		click(searchBtnReturn);
//		Thread.sleep(2000);
//		click(tickSalesOrder);
//		Thread.sleep(2000);
//		click(fromDate);
//		Thread.sleep(2000);
//		click(dateSelected);
//		Thread.sleep(2000);

		Thread.sleep(3000);
		click(searchDocumentList);
		Thread.sleep(3000);

		// String two = checkDocumentListRet2.replace("SO/003010", SalesInvoiceID);
		// click(two);

		SalesAndMarketingModule obj5 = new SalesAndMarketingModule();

		String two = checkDocumentListRet2.replace("SO/003010", obj5.readTestCreation(0));
		mouseMove(two);
		click(two);
		Thread.sleep(1000);
		String three = checkDocumentListRet3.replace("SO/003010", obj5.readTestCreation(0));
		click(three);
		Thread.sleep(1000);
		String four = checkDocumentListRet4.replace("SO/003010", obj5.readTestCreation(0));
		click(four);
		Thread.sleep(1000);
		String five = checkDocumentListRet5.replace("SO/003010", obj5.readTestCreation(0));
		click(five);
		Thread.sleep(1000);
		String six = checkDocumentListRet6.replace("SO/003010", obj5.readTestCreation(0));
		click(six);
		Thread.sleep(1000);
		String seven = checkDocumentListRet7.replace("SO/003010", obj5.readTestCreation(0));
		click(seven);
		Thread.sleep(1000);

		click(applyReturn);
		Thread.sleep(2000);
		selectIndex(returnReason, 2);
		Thread.sleep(2000);
		selectIndex(dispositionCategory, 0);
		Thread.sleep(2000);
		click(checkoutReturn);
		Thread.sleep(3000);
		click(draftReturn);
		Thread.sleep(5000);

		String header = getText(releaseInHeader);
		trackCode = getText(documentVal);
		System.out.println(trackCode);
		Thread.sleep(2000);

		if (header.equals("(Draft)")) {
			writeTestResults("User should be able to draft the sales return order",
					"Sales return order should be drafted", "Sales return order drafted", "Pass");
		} else {
			writeTestResults("User should be able to draft the sales return order",
					"Sales return order should be drafted", "Sales return order not drafted", "Fail");
		}

		click(releaseReturn);
		Thread.sleep(6000);

		if (isDisplayed(linkReturn)) {
			click(linkReturn);
			Thread.sleep(5000);

			writeTestResults("User should be able to release the sales return order",
					"Sales return order should be released", "Sales return order released", "Pass");
		} else {
			writeTestResults("User should be able to release the sales return order",
					"Sales return order should be released", "Sales return order not released", "Fail");
		}

		switchWindow();
		click(checkoutInboundshipment);
		Thread.sleep(2000);
		click(draftInbound);
		Thread.sleep(5000);

		String header3 = getText(releaseInHeader);

		inboundShipmentID = getText(documentVal);
		trackCode = getText(documentVal);
		System.out.println(trackCode);
		Thread.sleep(2000);
		if (header3.equals("(Draft)")) {
			writeTestResults("User should be able to draft the inbound shipment", "Inbound shipment should be drafted",
					"Inbound shipment drafted", "Pass");
		} else {
			writeTestResults("User should be able to draft the inbound shipment", "Inbound shipment should be drafted",
					"Inbound shipment not drafted", "Fail");
		}

		click(serialInbound);
		Thread.sleep(4000);
		// 1
		click(listInbound1);
		Thread.sleep(2000);
		click(checkBox);
		Thread.sleep(2000);
		click(updateInbound);
		Thread.sleep(2000);
		// 2
		click(listInbound2);
		Thread.sleep(2000);
		click(checkBox);
		Thread.sleep(2000);
		click(updateInbound);
		Thread.sleep(2000);
		// 3
		click(listInbound3);
		Thread.sleep(2000);
		click(checkBox);
		Thread.sleep(2000);
		click(updateInbound);
		Thread.sleep(2000);
		// 4
		click(listInbound4);
		Thread.sleep(2000);
		click(checkBox);
		Thread.sleep(2000);
		click(updateInbound);
		Thread.sleep(2000);
		// 5
		click(listInbound5);
		Thread.sleep(2000);
		click(checkBox);
		Thread.sleep(2000);
		click(updateInbound);
		Thread.sleep(2000);
		// 6
		click(listInbound6);
		Thread.sleep(2000);
		click(checkBox);
		Thread.sleep(2000);
		click(updateInbound);
		Thread.sleep(3000);
		click(backInbound);
		Thread.sleep(2000);

		click(releaseInbound);
		Thread.sleep(8000);

		if (isDisplayed(linkReturnInvoice)) {

			click(linkReturnInvoice);
			Thread.sleep(5000);
			writeTestResults("User should be able to release the inbound shipment",
					"Inbound shipment should be released", "Inbound shipment released", "Pass");
		} else {
			writeTestResults("User should be able to release the inbound shipment",
					"Inbound shipment should be released", "Inbound shipment not released", "Fail");
		}

		switchWindow();
		click(checkoutReturnInvoice);
		Thread.sleep(2000);
		click(draftReturnInvoice);
		Thread.sleep(6000);
		click(releaseReturnInvoice);
		Thread.sleep(7000);

		String header2 = getText(releaseInHeader);

		trackCode = getText(documentVal);
		// System.out.println(trackCode);

		if (header2.equals("(Released)")) {
			writeTestResults("User should be able to release the sales return invoice",
					"Sales return invoice should be released", "Sales return invoice released", "Pass");
		} else {
			writeTestResults("User should be able to release the sales return invoice",
					"Sales return invoice should be released", "Sales return invoice not released", "Fail");
		}
	}

	// Verify that user is able to create Sales Return Order (Sales Return Order to
	// Sales Return Invoice- none delivered) --- 11

	public void checkJourneySalesReturnOrderToSalesReturnInvoiceNonDeli() throws Exception {

		click(newSalesReturnOrder);
		Thread.sleep(3000);
		if (isDisplayed(journey2NonDeli)) {
			writeTestResults("Verify the availability of Sales Order to Sales Invoice journey",
					"user should be able to see the Sales Order to Sales Invoice journey", "Journey is displayed",
					"pass");
			click(journey2NonDeli);
			Thread.sleep(2000);

		} else {
			writeTestResults("Verify the availability of Sales Order to Sales Invoice journey",
					"user should be able to see the journeys", "Journey is not displayed", "Fail");
		}
	}

	public void fillDetailsSalesReturnOrderToSalesReturnInvoiceNonDeli() throws Exception {

		click(customerAccSearchReturn);
		Thread.sleep(2000);
		sendKeys(customerAccTxtReturn, customerAccReturnVal);
		Thread.sleep(1000);
		pressEnter(customerAccTxtReturn);
		Thread.sleep(2500);
		doubleClick(selectedValCustomerReturn);
		Thread.sleep(2000);
		// selectIndex(currencyReturn, 7);
		selectIndex(salesUnitReturn, 2);
		Thread.sleep(2000);
		click(documentListReturn);
		Thread.sleep(2000);
		click(searchDocumentList);
		Thread.sleep(2000);

		// System.out.println(SalesInvoiceID);
		String value = checkoutInboundshipment2.replace("SO/002230 - 1", SalesInvoiceID);
		// System.out.println(value);

		click(value);
		Thread.sleep(2000);
		// click(applyExchange);
		click(applyReturn);
		Thread.sleep(2000);
		selectIndex(returnReason, 2);
		// selectIndex(dispositionCategory, 0);
		click(checkoutReturn);
		Thread.sleep(2000);
		click(draftReturn);
		Thread.sleep(5000);

		String header2 = getText(releaseInHeader);

		trackCode = getText(documentVal);
		System.out.println(trackCode);

		if (header2.equals("(Draft)")) {
			writeTestResults("User should be able to draft the sales return order",
					"Sales return order should be released", "Sales return order draft", "Pass");
		} else {
			writeTestResults("User should be able to draft the sales return order",
					"Sales return order should be draft", "Sales return order not draft", "Fail");
		}

		click(releaseInbound);
		Thread.sleep(6000);

		if (isDisplayed(linkReturnInvoice)) {
			writeTestResults("User should be able to release the sales return order",
					"Sales return order should be released", "Sales return order released", "Pass");

			click(linkReturnInvoice);
			Thread.sleep(6000);

		} else {
			writeTestResults("User should be able to release the sales return order",
					"Sales return order should be released", "Sales return order not released", "Fail");
		}

		switchWindow();
		click(checkoutReturnInvoice);
		Thread.sleep(2000);
		click(draftReturnInvoice);
		Thread.sleep(5000);
		click(releaseReturnInvoice);
		Thread.sleep(7000);

		String header = getText(releaseInHeader);

		trackCode = getText(documentVal);
		System.out.println(trackCode);

		if (header.equals("(Released)")) {
			writeTestResults("User should be able to release the sales return invoice",
					"Sales return invoice should be released", "Sales return invoice released", "Pass");
		} else {
			writeTestResults("User should be able to release the sales return invoice",
					"Sales return invoice should be released", "Sales return invoice not released", "Fail");
		}
	}

	// Verify that user is able to create Sales Return Order (Direct Sales Return
	// Order to Sales Return Invoice)--12

	public void checkJourneyDirectSalesReturnOrder() throws Exception {

		click(newSalesReturnOrder);
		Thread.sleep(2000);

		if (isDisplayed(salesReturnJourney3)) {
			writeTestResults("Verify the availability of Sales Order to Sales Invoice journey",
					"user should be able to see the Sales Order to Sales Invoice journey", "Journey is displayed",
					"pass");

			click(salesReturnJourney3);
			Thread.sleep(3000);
		} else {
			writeTestResults("Verify the availability of Sales Order to Sales Invoice journey",
					"user should be able to see the journeys", "Journey is not displayed", "Fail");
		}
	}

	public void fillDetailsDirectSalesReturnOrderToSalesReturnInvoice() throws Exception {

		click(customerAccSearchReturn);
		Thread.sleep(2000);
		sendKeys(customerAccTxtReturn, customerAccReturnVal);
		Thread.sleep(2000);
		pressEnter(customerAccTxtReturn);
		Thread.sleep(3000);
		doubleClick(selectedValCustomerReturn);
		Thread.sleep(2000);
		// selectIndex(currencyReturn, 7);
		selectIndex(salesUnitReturn, 2);

		click(productDirect);
		Thread.sleep(2000);
		sendKeys(productText, productValDirect);
		Thread.sleep(3000);
		pressEnter(productText);
		Thread.sleep(3000);
		doubleClick(productSelectedDirect);
		Thread.sleep(2000);
		sendKeys("//tr[1]//td[21]//input[1]", "1000");

		Thread.sleep(2000);
		selectText(warehouseDirect, warehouseDirectVal);
		Thread.sleep(1000);
		selectText(returnReasonDirect, returnReasonVal);

		click(checkoutInvoice);
		Thread.sleep(3000);
		click(draft);
		Thread.sleep(6000);

		String header2 = getText(releaseInHeader);

		trackCode = getText(documentVal);
		System.out.println(trackCode);

		if (header2.equals("(Draft)")) {
			writeTestResults("User should be able to draft the sales return order",
					"Sales return order should be draft", "Sales return order draft", "Pass");
		} else {
			writeTestResults("User should be able to draft the sales return order",
					"Sales return order should be draft", "Sales return order not draft", "Fail");
		}
		Thread.sleep(3000);
		click(serialBatch);
		Thread.sleep(3000);

		click(product1List);
		Thread.sleep(2000);
		LocalTime myobj = LocalTime.now();
//		String serial = "A" + serialDrop;
		String serial = "A" + myobj;

		sendKeys(serialNoTextDirect, serial);
		System.out.println(serial);

		String lotNo = Integer.toString(lotNoValDirect);

		sendKeys(LotNoTextDirect, lotNo);

		click(expiryDateDirect);
		Thread.sleep(2000);
		click(expiryDateDirectVAl);
		Thread.sleep(2000);
		click(manufacDateDirect);
		Thread.sleep(2000);
		pressEnter(manufacDateDirect);
		Thread.sleep(2000);
		click(updateBtn);
		Thread.sleep(2000);
		click(backBtn);
		Thread.sleep(2000);
		click(release);
		Thread.sleep(6000);

		if (isDisplayed(linkReturn)) {
			writeTestResults("User should be able to release the sales return order",
					"Sales return order should be released", "Sales return order released", "Pass");

			click(linkReturn);
			Thread.sleep(5000);
		} else {
			writeTestResults("User should be able to release the sales return order",
					"Sales return order should be released", "Sales return order not released", "Fail");
		}

		switchWindow();
		click(checkOutbound);
		Thread.sleep(3000);
		click(draft);
		Thread.sleep(5000);

		String header = getText(releaseInHeader);

		trackCode = getText(documentVal);
		System.out.println(trackCode);

		if (header.equals("(Draft)")) {
			writeTestResults("User should be able to draft the inbound shipment", "Inbound shipment should be draft",
					"Inbound shipment draft", "Pass");
		} else {
			writeTestResults("User should be able to draft the inbound shipment", "Inbound shipment should be draft",
					"Inbound shipment not draft", "Fail");
		}

		click(release);
		Thread.sleep(7000);

		if (isDisplayed(linkReturn)) {
			writeTestResults("User should be able to release the inbound shipment",
					"Inbound shipment should be released", "Inbound shipment released", "Pass");

			click(linkReturn);
			Thread.sleep(5000);
		} else {
			writeTestResults("User should be able to release the inbound shipment",
					"Inbound shipment should be released", "Inbound shipment not released", "Fail");
		}

		switchWindow();
		Thread.sleep(2000);
		click(checkOutbound);
		Thread.sleep(3000);
		click(draft);
		Thread.sleep(5000);
		click(release);
		Thread.sleep(6000);

		String header1 = getText(releaseInHeader);

		trackCode = getText(documentVal);
		System.out.println(trackCode);

		if (header1.equals("(Released)")) {
			writeTestResults("User should be able to release the sales return invoice",
					"Sales return invoice should be released", "Sales return invoice released", "Pass");
		} else {
			writeTestResults("User should be able to release the sales return invoice",
					"Sales return invoice should be released", "Sales return invoice not released", "Fail");
		}
	}

	// Verify that user cannot create new sales return invoice directly --- 13

	public void checkClickSalesReturnInvoice() throws Exception {

		if (isDisplayed(salesReturnInvoice)) {
			writeTestResults("Verify that sales return invoice page is available",
					"User should be able to see the sales return invoice", "Sales return invoice page is dislayed",
					"pass");
			click(salesReturnInvoice);
			Thread.sleep(2000);
		} else {
			writeTestResults("Verify that sales return invoice page is available",
					"User should be able to see the sales return invoice", "Sales return invoice is not dislayed",
					"Fail");
		}
	}

	public void cannotCreateSalesReturnnvoice() throws Exception {

		if (!isElementPresent(newSalesReturnInvoice)) {
			writeTestResults("Verify the not availability of new sales return invoice button",
					"New sales return invoice should not be availble", "New sales return invoice btn not available",
					"Pass");
		} else {
			writeTestResults("Verify the not availability of new sales return invoice button",
					"New sales return invoice should not be availble", "New sales return invoice btn available",
					"Fail");
		}

		click(selectedValReturnInvoice);
		Thread.sleep(3000);

		if (!isElementPresent(draftNew)) {
			writeTestResults("Verify the not availability of draft in sales return invoice page",
					"Draft button should not be availble", "Draft button not available", "Pass");
		} else {
			writeTestResults("Verify the not availability of draft in sales return invoice page",
					"New sales return invoice should not be availble", "Draft button available", "Pass");
		}
	}

	// Verify that user is able to create Consolidate Invoice --- 14

	public void checkClickConsolidateInvoice() throws Exception {

		if (isDisplayed(contactInformationBtn)) {
			writeTestResults("Verify that Sales Order page is available",
					"user should be able to see the sales order page", "Sales order page is dislayed", "pass");
			click(contactInformationBtn);
		} else {
			writeTestResults("Verify that Sales Order page is available",
					"user should be able to see the sales order page", "Sales order is not dislayed", "Fail");
		}
	}

	public void consolidateInvoice() throws Exception {

		click(consolidatedInvoice);
		Thread.sleep(2000);
		click(newConsolidateInvoice);
		Thread.sleep(2000);
		click(customerSearchConsolidate);
		Thread.sleep(2000);
		sendKeys(textConsolidate, customerAccVal);
		Thread.sleep(2000);
		pressEnter(textConsolidate);
		Thread.sleep(3000);
		doubleClick(selectedValConsolidate);
		Thread.sleep(2000);
		click(docList);
		Thread.sleep(2000);
		click(searchInvoice);
		Thread.sleep(2000);

		SalesAndMarketingModule obj1 = new SalesAndMarketingModule();
		obj1.readTestCreation(2);

		String value = checkboxConso.replace("INS/PI/00047", obj1.readTestCreation(2));
		click(value);
		Thread.sleep(1000);
		String value1 = checkboxConso.replace("INS/PI/00047", obj1.readTestCreation(4));
		click(value1);

//		Thread.sleep(2000);
//		click(applyConso);
		Thread.sleep(3000);
		JavascriptExecutor j1 = (JavascriptExecutor) driver;
		j1.executeScript("$(\"#divDocList\").parent().find('.dialogbox-buttonarea > .button').click()");

		Thread.sleep(2000);
		click(checkoutInvoice);
		Thread.sleep(2000);
		click(draft);
		Thread.sleep(5000);
		click(release);

		Thread.sleep(6000);
		String header = getText(releaseInHeader);

		trackCode = getText(documentVal);
		System.out.println(trackCode);

		if (header.equals("(Released)")) {
			writeTestResults("User should be able to release the consolidated invoice",
					"Consolidated invoice should be released", "Consolidated invoice released", "Pass");
		} else {
			writeTestResults("User should be able to release the consolidated invoice",
					"Consolidated invoice should be released", "Consolidated invoice not released", "Fail");
		}
	}

	// Verify that User is able to create new Account --15

	public void checkClickAccountInformation() throws Exception {

		if (isDisplayed(accountInformtion)) {
			writeTestResults("Verify that 'Account Information' is available",
					"User should be able to see the 'Account Information'", "'Account Information' is dislayed",
					"Pass");
			click(accountInformtion);
			Thread.sleep(2000);
		} else {
			writeTestResults("Verify that 'Account Information' is available",
					"User should be able to see the account information", "'Account Information' is not dislayed",
					"Fail");
		}
	}

	public void checkAccountInformationPage() throws Exception {
		Thread.sleep(5000);
		if (isDisplayed(newAccount)) {
			writeTestResults("Verify that account information page is loading",
					"Account information page should be loaded", "Account information page is loaded", "Pass");
		} else {
			writeTestResults("Verify that account information page is loading",
					"Account information page should be loaded", "Account information page is not loaded", "Fail");
		}
	}

	public void checkTheMandatoryFields() throws Exception {
		if (isDisplayed(accountTypeAccInfor)) {
			if (isDisplayed(accountNameTypeAccInfor)) {
				if (isDisplayed(accountGroupAccInfor)) {
					if (isDisplayed(accountNameText)) {
						writeTestResults("Verify that the mandatory fields are available on account information page",
								"Mandatory fields should be available on account information page",
								"Mandatory fields are available", "Pass");
					}
				}
			}

		} else {
			writeTestResults("Verify that the mandatory fields are available on account information page",
					"Mandatory fields should be available on account information page", "Mandatory field is missing",
					"Fail");
		}
	}

	public void fillDetailsAccInfo() throws Exception {

		click(newAccount);
		checkTheMandatoryFields();
		selectIndex(accountTypeAccInfor, 1);
		Thread.sleep(2000);
		selectIndex(accountNameTypeAccInfor, 2);
		sendKeys(accountNameText, accountNameVal);
		selectIndex(accountGroupAccInfor, 4);
		selectIndex(currencyAccInfor, 3);
		selectIndex(salesUnitAccInfor, 3);
		// selectIndex(accountOwner, 3);
		selectIndex(contactTitleAccInfor, 3);
		sendKeys(contactNameAccInfor, contactNameVal);
		sendKeys(phoneAccInfor, phoneNoVal);
		Thread.sleep(2000);
		sendKeys(mobileAccInfor, mobileNoVal);
		sendKeys(emailAccInfor, emailVal);
		Thread.sleep(2000);
		sendKeys(designationAccInfor, designationVal);
		Thread.sleep(2000);
		if (isDisplayed(draftAccInfor)) {
			writeTestResults("Verify that draft button is available on account information page",
					"User should be able to see the draft button", "Draft button is available", "Pass");
			click(draftAccInfor);
			Thread.sleep(2000);
		} else {
			writeTestResults("Verify that draft button is available on account information page",
					"User should be able to see the draft button", "Draft button is not available", "Fail");
		}

		// click(draftAccInfor);
		click(releaseAccInfor);
		Thread.sleep(4000);
		click(editAccInfor);
		Thread.sleep(2000);
		if (isDisplayed(addedRow)) {
			writeTestResults("Verify that added information is loaded",
					"User should be able to see the added information", "Added information is loaded", "Pass");
		} else {
			writeTestResults("Verify that added information is loaded",
					"User should be able to see the added information", "Added information is not loaded", "Fail");
		}

		Thread.sleep(2000);
		click(addContactIcon);
		Thread.sleep(2000);
		selectIndex(nameTitleAccInfor, 1);
		Thread.sleep(2000);
		sendKeys(nameAccInfor, accountNameVal);
		Thread.sleep(2000);
		sendKeys(designationAccInforr, designationVal);
		Thread.sleep(2000);
		sendKeys(phoneAccInforr, phoneNoVal);
		Thread.sleep(2000);
		click(updateAccInfor);
		Thread.sleep(2000);
		click(UpdateBarAccInfor);
		Thread.sleep(2000);
		click(editAccInfor);
		Thread.sleep(2000);
		click(inquiryAccInfo);
		Thread.sleep(2000);
		sendKeys(inquiryAboutAccInfor, inquiryAboutVal);
		Thread.sleep(2000);
		selectIndex(categoryAccInfor, 1);
		Thread.sleep(2000);
		selectIndex(currentStageAccInfor, 1);
		Thread.sleep(2000);
		click(updateAccInfor);
		Thread.sleep(3000);

	}

	// Verify that User is able to create new Sales Inquiry --- 16

	public void checkClickSalesInquiryPage() throws Exception {

		if (isDisplayed(salesInquiryBtn)) {
			writeTestResults("Verify that Sales Order page is available",
					"user should be able to see the sales order page", "Sales order page is dislayed", "pass");
		} else {
			writeTestResults("Verify that Sales Order page is available",
					"user should be able to see the sales order page'", "Sales order is not dislayed", "Fail");
		}

		click(salesInquiryBtn);
	}

	public void fillDetailsSalesInq() throws Exception {

		click(salesInquiryBtn);
		Thread.sleep(2000);
		click(newSalesInqBtn);
		Thread.sleep(2000);
		selectIndex(leadSelectDropdown, 2);
		Thread.sleep(2000);
		click(leadAccSearchBtn);
		Thread.sleep(2000);
		click(leadAccSearchText);
		Thread.sleep(2000);
		sendKeys(leadAccSearchText, leadAccSearchVal);
		Thread.sleep(2000);
		pressEnter(leadAccSearchText);
		doubleClick(leadSelectedVal);
		Thread.sleep(2000);
		sendKeys(inquiryAboutTxt, salesInquiryAboutVal);
		Thread.sleep(2000);
		selectIndex(inquiryCategory, 1);
		Thread.sleep(2000);
		selectIndex(inquiryOrigin, 1);
		selectIndex(salesUnitInq, 1);
		Thread.sleep(2000);
		click(responsibleEmpSearch);
		sendKeys(responsibleEmpText, responsibleEmpVal);
		pressEnter(responsibleEmpText);
		doubleClick(selectedValEmp);
		selectIndex(priorityDropdownInq, 1);
		Thread.sleep(2000);
		click(draftInq);
		Thread.sleep(2000);
		click(addNewContactInq);
		selectIndex(nameTitleInq, 1);
		Thread.sleep(2000);
		sendKeys(nameTextInq, nameTextSalesInqVal);
		sendKeys(designationTextInq, salesInqDesigVal);
		sendKeys(phone1TextInq, salesInqPhone1Val);
		sendKeys(phone2TextInq, salesInqPhone2Val);
		click(release);
		Thread.sleep(6000);
		String header = getText(releaseInHeader);

		trackCode = getText(documentVal);
		System.out.println(trackCode);

		// document_map.put("SalesInquiry", trackCode);
		// document.add(0, document_map);

		if (header.equals("(Released)")) {
			writeTestResults("User should be able to release the sales inquiry", "Sales inquiry should be released",
					"Sales inquiry released", "Pass");
		} else {
			writeTestResults("User should be able to release the sales inquiry", "Sales inquiry should be released",
					"Sales inquiry not released", "Fail");
		}

		// writeTestCreations("SalesInquiry", document.get(0).get("SalesInquiry"), 0);
	}

	// Verify that User is able to create Lead Information --- 17

	public void checkClickLeadInformationPage() throws Exception {

		if (isDisplayed(leadInformation)) {
			writeTestResults("Verify that Sales Order page is available",
					"user should be able to see the sales order page", "Sales order page is dislayed", "pass");
		} else {
			writeTestResults("Verify that Sales Order page is available",
					"user should be able to see the sales order page'", "Sales order is not dislayed", "Fail");
		}

		click(leadInformation);
		Thread.sleep(2000);
	}

	public void fillDetailsLeadInformation() throws Exception {

		click(newLead);
		selectIndex(nameTitleLead, 1);
		sendKeys(nameLead, nameTextLeadVal);

		String mobile = String.valueOf(mobileLeadtVal);

		sendKeys(mobileLeadText, mobile);
		sendKeys(companyLead, companyLeadVal);
		sendKeys(NICLead, NICLeadVAl);
		sendKeys(designationLead, designationLeadVal);

		Thread.sleep(2000);
		selectIndex(salesUnitLead, 1);
//		Thread.sleep(2000);
//		selectIndex(accountOwnerLead, 2);
		// Thread.sleep(2000);
		click(draftLead);
		Thread.sleep(2000);
		click(releaseLead);
		Thread.sleep(2000);
		click(addNewContactLead);
		Thread.sleep(1000);
		selectIndex(nameTagLead, 1);
		sendKeys(nameNewLead, NameUpdateVal);
		click(updateBtnLead);
		Thread.sleep(2000);
		click(addInquiryLeadBtn);
		Thread.sleep(1000);
		sendKeys(inquiryAboutLead, inquiryUpdateAboutVal);
		Thread.sleep(2000);
		click(updateInquiry);
		Thread.sleep(3000);

	}

	// Verify that User is able to create Contact Infromation --- 18

	public void checkClickContatcInformation() throws Exception {

		if (isDisplayed(contactInformationBtn)) {
			writeTestResults("Verify that Sales Order page is available",
					"user should be able to see the sales order page", "Sales order page is dislayed", "pass");
			click(contactInformationBtn);
			Thread.sleep(2000);
		} else {
			writeTestResults("Verify that Sales Order page is available",
					"user should be able to see the sales order page", "Sales order is not dislayed", "Fail");
		}
	}

	public void fillDetailsContactInformation() throws Exception {

		click(newContactBtn);
		selectIndex(nameTitleContact, 1);
		sendKeys(nameTagContact, nameContactVal);
		click(accountSearchContact);
		sendKeys(customerAccountText, customerAccVal);
		pressEnter(customerAccountText);
		Thread.sleep(2000);
		doubleClick(selected_val);
		Thread.sleep(2000);
		sendKeys(designationContact, designationContactVal);
		selectIndex(salesUnitContact, 1);
		Thread.sleep(2000);
		// selectIndex(accountOwnerContact, 1);
		sendKeys(phoneContact, phoneContatcVal);
		sendKeys(mobileContact, mobileContactVal);
		sendKeys(emailContact, emailContatctVal);
		// sendKeys(IDContact, value);

		selectIndex(countryContact, 2);
		Thread.sleep(2000);
		click(updateContact);
		Thread.sleep(2000);
		click(inquiryInformationTabContact);
		click(newInquiryBtnContact);
		Thread.sleep(2000);
		switchWindow();
		sendKeys(inquiryAbout, inquiryAboutValu);
		click(draft);
		Thread.sleep(3000);
		click(release);
		Thread.sleep(3000);

	}

	// Verify that User is able to create new Pricing Rule with by one and get one
	// free items --- 19

	public static int generateRandomIntIntRange(int min, int max) {
		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}

	public void createPricingRule() throws Exception {

		Thread.sleep(2000);
		click(pricingRuleBtn);
		Thread.sleep(2000);
		click(newPricingRule);

		if (isDisplayed(newPricingruleookup)) {
			writeTestResults("Verify that new pricing rule look up is loading",
					"user should be able to see the pricing rule lookup page", "Pricing rule lookup is dislayed",
					"Pass");
		} else {
			writeTestResults("Verify that new pricing rule look up is loading",
					"user should be able to see the pricing rule lookup page", "Pricing rule lookup is not dislayed",
					"Fail");
		}

		LocalTime myObj = LocalTime.now();

		ruleCodeValue = "RC" + myObj;
		Thread.sleep(2000);
		sendKeys(ruleCode, ruleCodeValue);
		// System.out.println(ruleCodeValue);
		Thread.sleep(2000);

		trackCode = ruleCodeValue;
		document_map.put("pricingRule", trackCode);
		document.add(0, document_map);

		writeTestCreations("pricingRule", document.get(0).get("pricingRule"), 6);

		sendKeys(ruleDescription, ruleDescriptionVal);
		Thread.sleep(2000);
		click(draftPricingRule);
		Thread.sleep(3500);
		click(choosePricingPolicyBtn);
		Thread.sleep(1500);

		if (isDisplayed(policyHeader)) {
			writeTestResults("Verify that policy window is loading", "User should be able to see the policy window",
					"Policy window is dislayed", "Pass");
		} else {
			writeTestResults("Verify that policy window is loading", "User should be able to see the policy window",
					"Policy window is not dislayed", "Fail");
		}

		doubleClick(policyRowVal);
		Thread.sleep(2000);
		click(editPolicy);
		Thread.sleep(2000);
		selectIndex(discountTypePolicy, 1);
		Thread.sleep(2000);
		sendKeys(fromQty, fromVal);
		Thread.sleep(1000);
		sendKeys(toQty, toVal);
		Thread.sleep(1000);
		click(advanceOpt);
		Thread.sleep(2000);
		click(sameProductCheckBox);
		Thread.sleep(2000);
		sendKeys(qtyTextBox, qtyEditVal);
		Thread.sleep(1000);
		click(updateInEditValues);
		Thread.sleep(2000);
		click(apply);
		Thread.sleep(3000);
		click(validatorAlert);
		Thread.sleep(1000);
		click(advanceBtnInPolicy);
		Thread.sleep(1000);
		click(activateScheduleBtn);
		Thread.sleep(1000);
		click(daily);
		Thread.sleep(1000);
		click(everyDay);
		Thread.sleep(1000);
		selectText(fromTime, fromTimeVal);
		Thread.sleep(2000);
		selectText(toTime, toTimeVal);
		Thread.sleep(2000);
		click(advanceRelationTab);
		Thread.sleep(2000);
		click(applyBtnAdvanced);
		Thread.sleep(2000);
		click(updateRule);
		Thread.sleep(2000);
		click(activateBtn);
		Thread.sleep(2000);
		click(confirmationYes);
		Thread.sleep(2000);
	}

	// Verify that User is able to create new Pricing Model with attaching the
	// Pricing rule --- 20

	public void checkClickPricingModelPage() throws Exception {

		if (isDisplayed(pricingModel)) {
			writeTestResults("Verify that Pricing Model page is available",
					"user should be able to see the pricing model page", "Pricing Model page is dislayed", "pass");
			click(pricingModel);
			Thread.sleep(2000);
		} else {
			writeTestResults("Verify that Pricing Model page is available",
					"user should be able to see the pricing model page'", "Pricing Model page is not dislayed", "Fail");
		}
	}

	public void fillDetailsPricingModel() throws Exception {

		InventoryAndWarehouseModuleRegression obj = new InventoryAndWarehouseModuleRegression();
		obj.customizeLoadingDelay(newPricingModel, 10);
		click(newPricingModel);
		Thread.sleep(2000);

		LocalTime myObj = LocalTime.now();
		code1 = "M" + myObj;
		Thread.sleep(2000);
		sendKeys(modelCode, code1);
		Thread.sleep(1000);
		sendKeys(description, modelDescriptionVal);
		Thread.sleep(2000);
		click(rulesTab);
		Thread.sleep(2000);
		click(addPricingRule);
		Thread.sleep(2000);
		SalesAndMarketingModule obj5 = new SalesAndMarketingModule();
		sendKeys(searchTxtRule, obj5.readTestCreation(6));
		Thread.sleep(2500);
		pressEnter(searchTxtRule);
		Thread.sleep(2000);
		// System.out.println(selectedRule);
		// String one = selectedRule.replace("g1031-t",ruleCodeValue);
		doubleClick(selectedRule);
		Thread.sleep(2000);
		click(editPeriod);
		Thread.sleep(2000);
		click(startDate);
		Thread.sleep(2000);
		click(startDateValue);
		Thread.sleep(2000);
		click(endDate);
		Thread.sleep(3000);
		click(endDateValue);
		Thread.sleep(2000);
		click(applyModel);
		Thread.sleep(2000);
		click(draftModel);
		Thread.sleep(4000);
		click(releaseModel);
		Thread.sleep(4000);

		String header = getText(releaseInHeader);
		trackCode = getText(documentVal);

		document_map.put("pricingModel", trackCode);
		document.add(0, document_map);

		if (header.equals("( Released )")) {
			writeTestResults("User should be able to release the pricing model", "Pricing model should be released",
					"Pricing model is released", "Pass");
		} else {
			writeTestResults("User should be able to release the pricing model", "Pricing model should be released",
					"Pricing model is not released", "Fail");
		}

		writeTestCreations("pricingModel", document.get(0).get("pricingModel"), 7);
	}

	// Verify that user is able to created a Sales Order with added Price Model with
	// a Price Rule --- 21

	public void fillDetailsSalesOrderToSalesInvoicePricingModel() throws Exception {

		// checkFieldsSalesOrderSalesInvoice();
		click(customerAccountSearch);
		Thread.sleep(2000);
		sendKeys(customerAccountText, customerAccVal);
		Thread.sleep(1000);
		pressEnter(customerAccountText);
		Thread.sleep(2000);
		doubleClick(selected_val);
		Thread.sleep(3000);
		selectText(currencyService, "LKR");

		Thread.sleep(1000);
		selectIndex(salesUnit, 1);
		Thread.sleep(2000);
//		SalesAndMarketingModule obj5 = new SalesAndMarketingModule();
		// System.out.println(obj5.readTestCreation(7));
//		selectText(salesPriceModel, obj5.readTestCreation(7));
		Thread.sleep(3000);
		selectText(salesPriceModel, "FreeIssue188");

		Thread.sleep(2500);

		// Service Product
		InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();

		sendKeys(txt_ProductA, obj1.readTestCreation("Service"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, "2");
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(productLookup);
//		Thread.sleep(2000);
//		InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();
//		sendKeys(productTextSer, obj1.readTestCreation("Service"));
//		Thread.sleep(3500);
//		pressEnter(productTextSer);
//		Thread.sleep(3500);
//		doubleClick(proSelectedFree);

		Thread.sleep(3000);

		if (isEnabled(wareHouse)) {
			writeTestResults("Verify that the warehouse field is disabled for service product",
					"Warehouse field should be disabled", "Warehouse field is enabled", "Fail");
		} else {
			writeTestResults("Verify that the warehouse field is disabled for service product",
					"Warehouse field should be disabled", "Warehouse field is disabled", "Pass");
		}

		sendKeys(unitPriceTxt, unitPriceVal);
//		Thread.sleep(1000);
//		sendKeys(qtyText, quantityVal);

		sendKeys(txt_ProductA, obj1.readTestCreation("SerielSpecific"));
		Thread.sleep(3000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, "1");
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		Thread.sleep(1000);
//		click(addNewRecord);
//		Thread.sleep(1000);
//		click(productLookupInv);
//		Thread.sleep(2000);
//		InventoryAndWarehouseModule obj2 = new InventoryAndWarehouseModule();
//		sendKeys(productTextInv, obj2.readTestCreation("SerielSpecific"));
//		Thread.sleep(3000);
//		pressEnter(productTextInv);
//		Thread.sleep(3000);
//		doubleClick(invSelectedVal);
		Thread.sleep(2000);
		selectText(wareHouse, warehouseVal1);
		Thread.sleep(1000);
		sendKeys(unitPriceInve, unitPriceValInv);
//		Thread.sleep(1000);
//		sendKeys(qtyTextInve, quantityValInv);
		Thread.sleep(1000);
		click(checkoutSalesOrder);
		Thread.sleep(3000);

		JavascriptExecutor jse5 = (JavascriptExecutor) driver;
		jse5.executeScript("$('.el-resize4').val(1)");

		Thread.sleep(2000);
		click(applyFreeIssues);
		Thread.sleep(1000);
		click(closeAlert);
		Thread.sleep(2000);
		click(updateFreeIssues);
		Thread.sleep(2000);

		if (isElementPresent(warehouseFree)) {
			writeTestResults("Verify that free item added", "Free item should be added", "Free item added", "Pass");
		} else {
			writeTestResults("Verify that free item added", "Free item should be added", "Free item not added", "Fail");
		}

		selectText(warehouseFree, warehouseVal1);
		Thread.sleep(1000);

		if (!isEnabled(qtyFree) && !isEnabled(discountFree) && !isEnabled(taxGroupFree)) {
			writeTestResults("Verify whether Quantity,Discount and Tax group fields are disabled for free item",
					"Quantity,Discount and Tax group fields should be disabled",
					"Quantity,Discount and Tax group fields are disabled", "Pass");
		} else {
			writeTestResults("Verify whether Quantity,Discount and Tax group fields are disabled for free item",
					"Quantity,Discount and Tax group fields should be disabled",
					"Quantity,Discount and Tax group fields are not disabled", "Fail");
		}

		Thread.sleep(1000);
		click(draft);
		Thread.sleep(6000);

		String header4 = getText(releaseInHeader);
		trackCode = getText(documentVal);

		// System.out.println(trackCode);

		if (header4.equals("(Draft)")) {
			writeTestResults("User should be able to draft the sales order", "Sales order should be draft",
					"Sales order draft", "Pass");
		} else {
			writeTestResults("User should be able to draft the sales order", "Sales order should be draft",
					"Sales ordert not draft", "Fail");
		}

		Thread.sleep(1000);
		click(menu);
		Thread.sleep(2000);
		click(serialWise);
		Thread.sleep(6000);
//		serialNoVal = getText(serialTextVal);aruna
		serialNoVal = getText("//*[@id=\"tblProductBatchSerialwiseCost\"]/tbody/tr[2]/td[6]/div");
//		String serialNoValFree = getText(serialTextVal2);
		String serialNoValFree = getText("//*[@id=\"tblProductBatchSerialwiseCost\"]/tbody/tr[3]/td[6]/div");
		Thread.sleep(2000);
		click(serialMenuClose);
		Thread.sleep(3000);
		click(serialBatch);
		Thread.sleep(3000);
		click(productListBtn);
		Thread.sleep(2000);
		sendKeys(serialText, serialNoVal);
		Thread.sleep(2000);
		pressEnter(serialText);
		Thread.sleep(2000);
		click(updateBtn);
//		Thread.sleep(2000);
//		click(productListBtn2);
//		Thread.sleep(2000);
//		sendKeys(serialText, serialNoValFree);
//		Thread.sleep(2000);
//		pressEnter(serialText);
//		Thread.sleep(2000);
//		click(updateBtn);
		Thread.sleep(2000);
		click(backBtn);
		Thread.sleep(2000);
		click(release);
		Thread.sleep(6500);

		if (isDisplayed(outboundLink)) {
			writeTestResults("User should be able to release the sales order", "Sales order should be released",
					"Sales order released", "Pass");
		} else {
			writeTestResults("User should be able to release the sales order", "Sales order should be released",
					"Sales ordert not released", "Fail");
		}

		click(outboundLink);
		Thread.sleep(6000);
		switchWindow();
		click(checkoutOutbound);
		Thread.sleep(2000);
		click(draft);
		Thread.sleep(6000);

		String header3 = getText(releaseInHeader);

		trackCode = getText(documentVal);
		System.out.println(trackCode);

		if (header3.equals("(Draft)")) {
			writeTestResults("User should be able to draft the outbound shipment", "Outbound shipment should be draft",
					"Outbound shipment released", "Pass");
		} else {
			writeTestResults("User should be able to draft the outbound shipment", "Outbound shipment should be draft",
					"Outbound shipment not released", "Fail");
		}

		click(release);
		Thread.sleep(7000);

		if (isDisplayed(outboundLink)) {
			writeTestResults("User should be able to release the outbound shipment",
					"Outbound shipment should be released", "Outbound shipment released", "Pass");
			click(outboundLink);
			Thread.sleep(6000);
		} else {
			writeTestResults("User should be able to release the outbound shipment",
					"Outbound shipment should be released", "Outbound shipment not released", "Fail");
		}

		switchWindow();
		click(checkoutInvoice);
		Thread.sleep(2000);
		click(draft);
		Thread.sleep(5000);
		click(release);
		Thread.sleep(7000);

		String header2 = getText(releaseInHeader);
		trackCode = getText(documentVal);

		if (header2.equals("(Released)")) {
			writeTestResults("User should be able to release the sales invoice", "Sales invoice should be released",
					"Sales invoice released", "Pass");
		} else {
			writeTestResults("User should be able to release the sales invoice", "Sales invoice should be released",
					"Sales invoice not released", "Fail");
		}
	}

	// Verify that User is able to created a new Sales Exchage order -- 22
	public void salesExchangeOrder() throws Exception {

		click(salesExchangeOrder);
		Thread.sleep(2000);
		click(newSalesExchangeOrder);
		Thread.sleep(2000);
		click(journey1ExchangeToInvoice);
		Thread.sleep(4000);
		click(customerSearchExchange);
		Thread.sleep(2000);
		sendKeys(customerAccountText, customerAccVal);
		Thread.sleep(2000);
		pressEnter(customerAccountText);
		Thread.sleep(2000);
		doubleClick(selected_val);
		Thread.sleep(2000);
		click(salesUnit);
		Thread.sleep(2000);
		selectIndex(salesUnit, 1);
		Thread.sleep(2000);
		click(documentList);
		Thread.sleep(2000);
		click(fromDate);
		Thread.sleep(2000);
		click(dateSelected);
		Thread.sleep(2000);
		click(searchDocumentList);
		Thread.sleep(2000);
		click(checkDocumentList);
		Thread.sleep(2000);
		click(applyExchange);
		Thread.sleep(2000);
		click(addNewExchange);

		Thread.sleep(2000);
		click(searchProOutput);
		Thread.sleep(2000);
		sendKeys(searchProText, outputProval);
		Thread.sleep(2000);
		pressEnter(searchProText);
		Thread.sleep(2000);
		doubleClick(selectedOutput);
		Thread.sleep(2000);
		selectText(warehouseOutput, warehouseOutputPro);
		Thread.sleep(2000);
		click(checkoutGLAcc);
		Thread.sleep(3000);
		click(draft);
		Thread.sleep(4000);
		click(proDetailsOutput);
		Thread.sleep(2000);
		click(SerialWiseCost);
		Thread.sleep(2000);
		String serialVal = getText(firstSerial);
		click(serialMenuClose);
		Thread.sleep(2000);
		click(serialBatch);
		Thread.sleep(4000);
		click(product1List);
		Thread.sleep(2000);
		click(checkFirst);
		Thread.sleep(2000);
		click(updateBtn);
		Thread.sleep(2000);
		click(productList2);
		Thread.sleep(2000);
		sendKeys(serialText, serialVal);
		Thread.sleep(2000);
		pressEnter(serialText);
		Thread.sleep(2000);
		click(updateBtn);
		Thread.sleep(2000);
		click(backBtn);
		Thread.sleep(4000);
		click(release);
		Thread.sleep(5000);

		click(linkQuatation);
		Thread.sleep(5000);
		switchWindow();
		click(checkOutbound);
		Thread.sleep(3000);
		click(draft);
		Thread.sleep(5000);
		click(release);
		Thread.sleep(6000);
	}

	// Verify that User is able to create Free Text Invoice -- 23

	public void checkClickFreeTextInvoice() throws Exception {

		if (isDisplayed(freeTextInvoice)) {
			writeTestResults("Verify that 'Free Text Invoice' is available",
					"User should be able to see the 'Free Text Invoice'", "'Free Text Invoice' is dislayed", "Pass");
			click(freeTextInvoice);
			Thread.sleep(2000);
		} else {
			writeTestResults("Verify that Sales Order page is available",
					"User should be able to see the 'Free Text Invoice'", "'Free Text Invoice' is not dislayed",
					"Fail");
		}
	}

	public void createTextInvoice() throws Exception {

		click(newFreeTextInvoice);
		Thread.sleep(2000);
		click(cusAccSearchFreeText);
		Thread.sleep(2000);
		sendKeys(customerAccountText, customerAccVal);
		pressEnter(customerAccountText);
		Thread.sleep(2000);
		doubleClick(selected_val);
		selectIndex(salesUnit, 1);
		click(glAccSearch);
		sendKeys(glAccText, glAccVal);
		pressEnter(glAccText);
		Thread.sleep(2000);
		doubleClick(selectedRowGLAcc);
		Thread.sleep(2000);
		sendKeys(GLAmount, GLAmountVal);
		Thread.sleep(2000);
		click(checkoutGLAcc);

		String amount = getText(checkAmount);
		// System.out.println(amount);
		Thread.sleep(2000);
		if (amount.equals("5,000.00000")) {
			writeTestResults("Verify that added amount is updated in value bar",
					"Values should be update in top value bar", "Values are updated accurately", "Pass");
		} else {
			writeTestResults("Verify that added amount is updated in value bar",
					"Values should be update in top value bar", "Values are not updated accurately", "Fail");
		}

		Thread.sleep(2000);
		click(draftGLAcc);
		Thread.sleep(3000);
		click(releaseGLAcc);
		Thread.sleep(5000);

		String header = getText(releaseInHeader);
		trackCode = getText(documentVal);
		System.out.println(trackCode);

		if (header.equals("(Released)")) {
			writeTestResults("User should be able to release the sales invoice", "Sales invoice should be released",
					"Sales invoice is released", "Pass");
		} else {
			writeTestResults("User should be able to release the sales invoice", "Sales invoice should be released",
					"Sales invoice is not released", "Fail");
		}
	}

	// Verify that user cannot proceed the Sales Order with adding full qty --- 24

	public void fillDetailsSalesOrderFullQty() throws Exception {

		checkFieldsSalesOrderSalesInvoice();

		click(customerAccountSearch);
		Thread.sleep(2000);
		sendKeys(customerAccountText, customerAccVal);
		Thread.sleep(3000);
		pressEnter(customerAccountText);
		Thread.sleep(2500);
		doubleClick(selected_val);
		Thread.sleep(2000);
		click(salesUnit);
		selectIndex(salesUnit, 1);

		InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();

		sendKeys(txt_ProductA, obj1.readTestCreation("SerielSpecific"));
		Thread.sleep(4000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, "1");
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(productLookup);
//		Thread.sleep(2000);
//		// InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();
//		sendKeys(productTextSer, "20200114155709-SERIEL-SPECIFIC");
//		Thread.sleep(2000);
//		pressEnter(productTextSer);
//		Thread.sleep(3500);
//		doubleClick(invSelectedVal);

		Thread.sleep(3000);
		selectText(warehouseFullQty, "WH010 [Kegalle]");
		Thread.sleep(1000);
		sendKeys(unitPriceTxtFull, unitPriceVal);
		Thread.sleep(1000);
		// sendKeys(qtyTextFull, quantityVal);
		Thread.sleep(2000);

		click(menuBtnFull);
		Thread.sleep(2000);
		click(productAvailability);
		Thread.sleep(3000);
//		String onHandQuantity = getText(onHandQty);
		String onHandQuantity = getText("//div[contains(.,'Kegalle')]/../../td[6]/div");
		System.out.println(onHandQuantity);
//		 Thread.sleep(3000);
//		click(closeAvailabilityMenu);
		Thread.sleep(3000);
		JavascriptExecutor j9 = (JavascriptExecutor) driver;
		j9.executeScript("$(\"#dlgMIWidget\").dialog(\"close\")");
		Thread.sleep(2000);
		// click(unitPriceTxt);
		// sendKeys(unitPriceTxt, unitPriceValInv);

		sendKeys(qtyTextFull, "1");

		// 2 Serial Specific

		sendKeys(txt_ProductA, obj1.readTestCreation("SerielSpecific"));
		Thread.sleep(4000);
		driver.findElement(getLocator(txt_ProductA)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(3000);
		pressEnter(txt_ProductA);
		Thread.sleep(3000);
		sendKeys(txt_ProductQtyA, quantityValSerialSpec);
		Thread.sleep(3000);
		pressEnter(txt_ProductQtyA);

//		click(addNewRecordSerialSpecific);
//		Thread.sleep(2000);
//		click(productLookupSerialSpec);
//		Thread.sleep(3000);
//		// InventoryAndWarehouseModule obj2 = new InventoryAndWarehouseModule();
//		sendKeys(productTextSerialSpec, "20200114155709-SERIEL-SPECIFIC");
//		Thread.sleep(3000);
//		pressEnter(productTextSerialSpec);
//		Thread.sleep(3500);
//		doubleClick(invSelectedVal);

		Thread.sleep(3000);
		selectText(wareHouseSerialSpecific, "WH010 [Kegalle]");
		Thread.sleep(2000);

		if (isEnabled(unitPriceSerialSpec)) {
			writeTestResults("User should be able to add unit price", "Unit price field should be enabled",
					"Unit price field is enabled", "Pass");
		} else {
			writeTestResults("User should be able to add unit price", "Unit price field should be enabled",
					"Unit price field is disabled", "Fail");
		}

		sendKeys(unitPriceSerialSpec, unitPriceValSerialSpec);
//		Thread.sleep(2000);
//		sendKeys(qtyTextSerialSpec, quantityValSerialSpec);
		Thread.sleep(1000);

		sendKeys(qtyTextInve, "1");
		Thread.sleep(2000);

		click(checkoutSalesOrder);
		Thread.sleep(2000);

		// driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		// bannerTot = getText(bannerTotal1);

		click(draft);
		Thread.sleep(5000);

		click(menu);
		Thread.sleep(3000);
		click(serialWise);
//		Thread.sleep(8000);
//		serialNoVal = getText(serialTextVal);aruna
		Thread.sleep(4000);
		String serial1 = getText("//*[@id=\"tblProductBatchSerialwiseCost\"]/tbody/tr[2]/td[6]/div");
		Thread.sleep(4000);
		String serial2 = getText("//*[@id=\"tblProductBatchSerialwiseCost\"]/tbody/tr[3]/td[6]/div");

		Thread.sleep(2000);
		click(serialMenuClose);
		Thread.sleep(3000);

		click(release);
		Thread.sleep(6000);

		click(outboundLink);
		Thread.sleep(5000);
		switchWindow();
		Thread.sleep(2000);
		click(checkoutOutbound);
		Thread.sleep(3000);
		click(draft);
		Thread.sleep(5000);

		click(serialBatch);
		Thread.sleep(4000);
		click(productListBtn1);
		Thread.sleep(2000);
		click(rangeSerialSpecific);

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$('#attrSlide-txtSerialCount').val('1')");
		// jse.executeScript("$('#attrSlide-txtSerialCount')", onHandQuantity);
		// sendKeys(locator, value);
		sendKeys(serialText, serial1);
		Thread.sleep(2000);
		pressEnter(serialText);
		Thread.sleep(2000);
		click(updateBtn);
		Thread.sleep(2000);
		click(productListBtn);
		Thread.sleep(2000);
		click(rangeSerialSpecific);
		jse.executeScript("$('#attrSlide-txtSerialCount').val('1')");
		sendKeys(serialText, serial2);
		Thread.sleep(2000);
		pressEnter(serialText);
		Thread.sleep(2000);
		click(updateBtn);
		Thread.sleep(3000);
		// click(backBtn);
		// Thread.sleep(1000);
		// click(release);
		// Thread.sleep(3000);

		if (isElementPresent("//p[@id='validator-common-SerialNos']")) {
			writeTestResults(
					"Verify that user cannot proceed the Sales Order with adding full qty as Separate product ine in same Sales Order",
					"Error message should display", "Error message displayed", "Pass");
		} else {
			writeTestResults(
					"Verify that user cannot proceed the Sales Order with adding full qty as Separate product ine in same Sales Order",
					"Error message should display", "Error message not displayed", "Fail");
		}
	}

	// Verify that user is able to create an Active vehicle information -- 25

	public void checkClickVehicleInformation() throws Exception {
		Thread.sleep(3000);
		if (isDisplayed(vehicleInformation)) {
			writeTestResults("Verify that 'Vehicle Information' is available",
					"User should be able to see the 'Vehicle Information' page", "'Vehicle Information' is dislayed",
					"Pass");
			click(vehicleInformation);
		} else {
			writeTestResults("Verify that 'Vehicle Information' is available",
					"User should be able to see the 'Vehicle Information'", "'Vehicle Information' is not dislayed",
					"Fail");
		}

	}

	public void fillDataVehicleInf() throws Exception {

		if (isDisplayed(vehicleInformationHeader)) {
			writeTestResults("Verify that vehicle information page is loading",
					"User should be able to go to vehicle information page", "Vehicle information page loaded", "Pass");
		} else {
			writeTestResults("Verify that vehicle information page is loading",
					"User should be able to go to vehicle information page", "Vehicle information page is not loaded",
					"Fail");
		}

		Thread.sleep(3000);
		click(newVehicleInformationBtn);

		checkNewVehicleInformationLookup();
		Thread.sleep(2000);
		checkFieldsVehicleInformation();
		Thread.sleep(2000);
		Random random = new Random();
		String id = String.format("%04d", random.nextInt(10000));
		// System.out.println(id);

		id1 = "CAR-" + id;

		document_map.put("vehicleNo", id1);
		document.add(0, document_map);

		writeTestCreations("vehicleNo", document.get(0).get("vehicleNo"), 8);

		// System.out.println(id1);
		Thread.sleep(2000);
		sendKeys(vehicleNoTxt, id1);
		Thread.sleep(2000);
		sendKeys(capacityTxt, capacityTxtVal);

		selectIndex(capacityMeasure, 2);
		Thread.sleep(2000);
		sendKeys(weightTxt, weightTxtVal);
		selectIndex(weightMeasure, 5);
		Thread.sleep(2000);
		click(draftVehicleInf);
		Thread.sleep(2000);
		// click(sideMenuValVehicleInf);
		// System.out.println(sideMenuValVehicleInf);
		String remove = sideMenuValVehicleInf;

		// String remove1 = remove.replace("CAR-4563", id1);
		// System.out.println(remove1);

		sendKeys(vehicleSearchTop, id1);
		Thread.sleep(2000);
		mouseMove("//label[@class='ncell']");
		Thread.sleep(2000);
		pressEnter("//label[@class='ncell']");

		Thread.sleep(2000);
		String status = getText(statusVehicleInf);

		if (status.equals("Open")) {
			writeTestResults("Verify that created vehicle information is in open status",
					"Created vehicle information should be in open status", "Vehicle information is in open status",
					"Pass");
		} else {
			writeTestResults("Verify that created vehicle information is in open status",
					"Created vehicle information should be in open status", "Vehicle information is not in open status",
					"Fail");
		}

		Thread.sleep(2000);
		click(activeBtnVehicleInf);

		click(alertYes);
		Thread.sleep(3000);
		String after = getText(afterYes);

		if (after.equals("Activated")) {
			writeTestResults("Verify that created vehicle information is activated",
					"Vehicle information should be activated", "Vehicle information is activated", "Pass");
		} else {
			writeTestResults("Verify that created vehicle information is activated",
					"Vehicle information should be activated", "Vehicle information is not activated", "Fail");
		}

	}

	public void checkFieldsVehicleInformation() throws Exception {

		if (isDisplayed(vehicleNoTxt)) {
			if (isDisplayed(capacityTxt)) {
				if (isDisplayed(weightTxt)) {
					writeTestResults("Verify that the mandatory fields are available on vehicle information page",
							"Mandatory fields should be available on vehicle information page",
							"Mandatory fields are available", "Pass");
				}
			}

		} else {

			writeTestResults("Verify that the mandatory fields are available on vehicle information page",
					"Mandatory fields should be available on vehicle information page", "Mandatory field is missing",
					"Fail");
		}
	}

	public void checkNewVehicleInformationLookup() throws Exception {
		if (isDisplayed(newVehicleInformationHeader)) {
			writeTestResults("Verify that the new vehicle information pop up should be loaded",
					"User should be able to see the new vehicle information pop up",
					"New vehicle information pop up is dislayed", "Pass");
		} else {
			writeTestResults("Verify that the new vehicle information pop up should be loaded",
					"User should be able to see the new vehicle information pop up",
					"New vehicle information pop up is not dislayed", "Fail");
		}
	}

	// Verify that user is able to create a delivery plan -- 26

	public void checkInventoryWarehousing() throws Exception {

		if (isDisplayed(invenWarehouse)) {
			writeTestResults("Verify that Inventory & Warehousing module is available",
					"user should be able to see the 'Inventory & Warehousing'",
					"Inventory & Warehousing module is dislayed", "pass");
			click(invenWarehouse);
			Thread.sleep(2000);
		} else {
			writeTestResults("Verify that Inventory & Warehousing module is available",
					"user should be able to see the 'Inventory & Warehousing'",
					"Inventory & Warehousing module is not dislayed", "Fail");
		}
	}

	public void checkClickDeliveryPlan() throws Exception {
		Thread.sleep(3000);

		if (isDisplayed(deliveryPlan)) {
			writeTestResults("Verify that delivery plan page is available",
					"user should be able to see the delivery plan page", "Delivery plan page is dislayed", "Pass");
			click(deliveryPlan);
		} else {
			writeTestResults("Verify that delivery plan page is available",
					"user should be able to see the delivery plan page", "Delivery plan page is not dislayed", "Fail");
		}

	}

	public void createDeliveryPlan() throws Exception {

		click(deliveryPlanInfo);
		Thread.sleep(2000);
		click(vehicleSearch);
		Thread.sleep(2000);
		checkDeliveryPlanLookUp();
		Thread.sleep(3000);
		checkFieldsDeliveryPlan();
		sendKeys(vehicleSearchText, id1);
		Thread.sleep(2000);
		pressEnter(vehicleSearchText);
		Thread.sleep(2000);
		doubleClick(vehicleSelectedVal);
		Thread.sleep(2000);
		click(fromDateVehicle);
		Thread.sleep(2000);
		click(fromDateVal);
		Thread.sleep(3000);
		click(toDateVehicle);
		Thread.sleep(2000);
		click(toDateVal);
		Thread.sleep(2000);
		click(draftVehicle);
		Thread.sleep(4000);

		String countBefore = getText(planningCount);
		String countBeforeLoading = getText(loadingCount);
		String countBeforeDeliverd = getText(deliveredCount);
		System.out.println("countBefore:" + countBefore);
		System.out.println("countBeforeLoading:" + countBeforeLoading);
		System.out.println("countBeforeDeliverd:" + countBeforeDeliverd);

		int countBeforePlanning = Integer.parseInt(countBefore);
		System.out.println("countBeforePlanning:" + countBeforePlanning);
		int countBeforeLoad = Integer.parseInt(countBeforeLoading);
		System.out.println("countBeforeLoad:" + countBeforeLoad);
		int countBeDelivered = Integer.parseInt(countBeforeDeliverd);
		System.out.println("countBeDelivered:" + countBeDelivered);

		SalesAndMarketingModule obj5 = new SalesAndMarketingModule();
		// sendKeys(searchTxtRule, obj5.readTestCreation(6));

		String releasePlanning = valueVehicle.replace("CAR-8601", obj5.readTestCreation(8));
		System.out.println("releasePlanning:" + releasePlanning);
		Thread.sleep(2000);
		click(releasePlanning);
		Thread.sleep(4000);

		String pCount = getText(planningCount);
		System.out.println("pCount:" + pCount);

		String loadingCnt = getText(loadingCount);
		System.out.println("loadingCnt:" + loadingCnt);

		int cntAftr = Integer.parseInt(pCount);
		System.out.println("cntAftr" + cntAftr);
		int countAfterLoad = Integer.parseInt(loadingCnt);
		System.out.println("countAfterLoad:" + countAfterLoad);

//		if (countBeforePlanning == cntAftr + 1) {aruna
		if (countBeforePlanning == cntAftr) {
			writeTestResults("Verify the planning count updating",
					"Planning count should be increase after creating delivery plan", "Planning count updated", "Pass");
		} else {
			writeTestResults("Verify the planning count updating",
					"Planning count should be increase after creating delivery plan", "Planning count not updated",
					"Fail");
		}

//		if (countBeforeLoad == countAfterLoad - 1) {aruna
		if (countBeforeLoad == countAfterLoad) {
			writeTestResults("Verify the loading count updating",
					"Loading count should be increase after releasing planned", "Loading count updated", "Pass");
		} else {
			writeTestResults("Verify the loading count updating",
					"Loading count should be increase after releasing planned", "Loading count not updated", "Fail");
		}

		Thread.sleep(2000);
		click(loadingTab);
		Thread.sleep(10000);

		String valAdd = valueVehicleInLoading.replace("CAR-5541", obj5.readTestCreation(8));

		click(valAdd);
		Thread.sleep(2000);
		click(fromdateDeliveryPlan);
		Thread.sleep(2000);
		selectText(fromdateyear, "2018");
		;
		Thread.sleep(2000);
		click(fromDateVal);

		// click(todateDeliveryPlan);
		// Thread.sleep(2000);
		// click(toDateVal);
		// click(searchBtn);
		Thread.sleep(3000);

		String SCD = obj5.readTestCreation(2);
		Thread.sleep(3000);
		sendKeys(salesInvoiceSearchTxt, SCD);
		Thread.sleep(2000);
		pressEnter(salesInvoiceSearchTxt);
		Thread.sleep(20000);
		String created = checkboxDeliveryplan.replace("SCD/PI/00367", SCD);
		System.out.println(created);
		click(created);
		Thread.sleep(3000);
		click(applyDeliveryPlan);
		Thread.sleep(3000);
		String valRelease = releaseBtnInLoading.replace("CAR-5541", obj5.readTestCreation(8));
		Thread.sleep(2000);
		click(valRelease);
		Thread.sleep(3000);
		pageRefersh();
		Thread.sleep(4000);
		String deliveredCnt = getText(deliveredCount);
		System.out.println(deliveredCnt);

		int countAfterDeli = Integer.parseInt(deliveredCnt);
		System.out.println(countAfterDeli);

//		if (countBeDelivered == countAfterDeli - 1) {aruna
		if (countBeDelivered == countAfterDeli) {
			writeTestResults("Verify the delivered count updating",
					"Deliverd count should be increase after creating delivery plan", "Delivered count updated",
					"Pass");
		} else {
			writeTestResults("Verify the delivered count updating",
					"Delivered count should be increase after creating delivery plan", "Delivered count not updated",
					"Fail");
		}
	}

	public void checkDeliveryPlanLookUp() throws Exception {

		if (isDisplayed(deliveryPlanHeader)) {

			writeTestResults("Verify that delivery plan look up is loading",
					"User should be able to see the delivery plan lookup", "Delivery plan look up is loaded", "Pass");

		} else {
			writeTestResults("Verify that delivery plan look up is loading",
					"User should be able to see the delivery plan lookup", "Delivery plan look up is not loaded",
					"Fail");
		}
	}

	public void checkFieldsDeliveryPlan() throws Exception {
		if (isDisplayed(vehicleSearch)) {
			if (isDisplayed(fromDateVehicle)) {
				if (isDisplayed(toDateVehicle)) {
					writeTestResults("Verify that the mandatory fields are available on delivery plan",
							"Mandatory fields should be available on delivery plan page",
							"Mandatory fields are available", "Pass");
				}
			}

		} else {

			writeTestResults("Verify that the mandatory fields are available on delivery plan page",
					"Mandatory fields should be available on delivery plan page", "Mandatory field is missing", "Fail");
		}
	}

	public void verifyTheDocumentFlow() throws Exception {

		click(actionMenu);
		Thread.sleep(3000);
		click(documentFlow);
		Thread.sleep(3000);

		JavascriptExecutor js = (JavascriptExecutor) driver;
		Thread.sleep(2000);

		String valueSO = (String) js
				.executeScript("return $($(\"#ctrlDocumentFlow-svg\").find(\".pf-content tspan\")[5]).text()");
		System.out.println(valueSO);

		String valueAOD = (String) js
				.executeScript("return $($(\"#ctrlDocumentFlow-svg\").find(\".pf-content tspan\")[10]).text()");
		System.out.println(valueAOD);

		String valueSCD = (String) js
				.executeScript("return $($(\"#ctrlDocumentFlow-svg\").find(\".pf-content tspan\")[15]).text()");
		System.out.println(valueSCD);

		if (valueSO.equals(SO) && (valueAOD.equals(AOD)) && (valueSCD.equals(SCD))) {
			writeTestResults("Verify the document flow displays correctly", "Document flow should display accurately",
					"Document flow displayed correctly", "Pass");
		} else {
			writeTestResults("Verify the document flow displays correctly", "Document flow should display accurately",
					"Document flow displayed incorrectly", "Fail");
		}
	}

	public void verifyBincardBefore() throws Exception {

		click(reports);
		Thread.sleep(2000);
		click(timePeriod);
		Thread.sleep(2000);
		click(today);
		Thread.sleep(2000);
		click(reportTemplate);
		Thread.sleep(2000);
		selectText(productCodeDrop, "Product Code");
		Thread.sleep(2000);
		sendKeys(productCodeText, ProductCodeVal);
		Thread.sleep(2000);
		click(quickPreview);
		Thread.sleep(15000);

		// String val = binCardOutboundVal.replace("AOD/19/02524", outBoundID);
		// System.out.println(val);

		beforeVal = getText("//*[@id=\"table1\"]/div/div[1]/div[18]");
		String one = getText(beforeVal);

		System.out.println(one);
	}

	// 4
	public void verifyBinCardUpdating() throws Exception {

		click(reports);
		Thread.sleep(2000);
		click(timePeriod);
		Thread.sleep(2000);
		click(today);
		Thread.sleep(2000);
		click(reportTemplate);
		Thread.sleep(2000);
		selectText(productCodeDrop, "Product Code");
		Thread.sleep(2000);
		InventoryAndWarehouseModule obj2 = new InventoryAndWarehouseModule();
		sendKeys(productCodeText, obj2.readTestCreation("SerielSpecific"));
		Thread.sleep(3000);
		click(quickPreview);
		InventoryAndWarehouseModuleRegression obj = new InventoryAndWarehouseModuleRegression();
		String val = binCardOutboundVal.replace("AOD/19/02524", outBoundID);
		obj.customizeLoadingDelay(val, 1200);

		// System.out.println(val);
		String val1 = getText(val);

		// System.out.println(val1);

		if (val1.equals("1.00")) {
			writeTestResults("Verify the bin card updating", "Bin card should update according to outbound quantity",
					"Bin card updated", "Pass");
		} else {
			writeTestResults("Verify the bin card updating", "Bin card should update according to outbound quantity",
					"Bin card not updated", "Fail");
		}
	}

	// s5

	public void verifyBinCardUpdatingInbound() throws Exception {

		click(reports);
		Thread.sleep(2000);
		click(timePeriod);
		Thread.sleep(2000);
		click(today);
		Thread.sleep(2000);
		click(reportTemplate);
		Thread.sleep(2000);
		selectText(productCodeDrop, "Product Code");
		Thread.sleep(2000);
		InventoryAndWarehouseModule obj2 = new InventoryAndWarehouseModule();
		sendKeys(productCodeText, obj2.readTestCreation("SerielSpecific"));
		// System.out.println(obj2.readTestCreation("SerielSpecific"));
		Thread.sleep(3500);
		click(quickPreview);
		InventoryAndWarehouseModuleRegression obj = new InventoryAndWarehouseModuleRegression();
		String val = binCardInboundVal.replace("GRN/19/5647", inboundShipmentID);
		obj.customizeLoadingDelay(val, 1200);
		// System.out.println(val);
		String val1 = getText(val);
		// System.out.println(val1);

		if (val1.equals("1.00")) {
			writeTestResults("Verify the bin card updating", "Bin card should update according to inbound quantity",
					"Bin card updated", "Pass");
		} else {
			writeTestResults("Verify the bin card updating", "Bin card should update according to inbound quantity",
					"Bin card not updated", "Fail");
		}
	}

	// s3

	public void fillDetailsSalesOrderToSalesInvoiceSmoke() throws Exception {

		checkFieldsSalesOrderSalesInvoice();

		click(customerAccountSearch);
		Thread.sleep(2000);
		sendKeys(customerAccountText, customerAccVal);
		pressEnter(customerAccountText);
		Thread.sleep(2000);
		doubleClick(selected_val);
		Thread.sleep(2000);
		click(salesUnit);
		Thread.sleep(2000);
		selectIndex(salesUnit, 1);
		Thread.sleep(2000);
		// Service Product
		click(productLookup);
		Thread.sleep(2000);
		InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();
		sendKeys(productTextSer, obj1.readTestCreation("Service"));
		Thread.sleep(3500);
		pressEnter(productTextSer);
		Thread.sleep(4000);
		doubleClick(proSelectedVAl);
		Thread.sleep(3000);

		if (isEnabled(wareHouse)) {
			writeTestResults("Verify that the warehouse field is disabled for service product",
					"Warehouse field should be disabled", "Warehouse field is enabled", "Fail");
		} else {
			writeTestResults("Verify that the warehouse field is disabled for service product",
					"Warehouse field should be disabled", "Warehouse field is disabled", "Pass");
		}

		click(unitPriceTxt);
		Thread.sleep(1000);
		sendKeys(unitPriceTxt, unitPriceVal);
		Thread.sleep(1000);
		click(qtyText);
		Thread.sleep(1000);
		sendKeys(qtyText, quantityVal);
		Thread.sleep(1000);
		click(addNewRecord);
		Thread.sleep(1000);
		click(productLookupInv);
		Thread.sleep(2000);
		InventoryAndWarehouseModule obj2 = new InventoryAndWarehouseModule();
		sendKeys(productTextInv, obj2.readTestCreation("SerielSpecific"));
		Thread.sleep(4000);
		pressEnter(productTextInv);
		Thread.sleep(5000);
		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		selectText(wareHouse, warehouseVal1);
		Thread.sleep(2000);

		if (isEnabled(unitPriceInve)) {
			writeTestResults("User should be able to add unit price", "Unit price field should be enabled",
					"Unit price field is enabled", "Pass");
		} else {
			writeTestResults("User should be able to add unit price", "Unit price field should be enabled",
					"Unit price field is disabled", "Fail");
		}

		sendKeys(unitPriceInve, unitPriceValInv);
		Thread.sleep(1000);
		sendKeys(qtyTextInve, quantityValInv);
		Thread.sleep(1000);
		click(checkoutSalesOrder);
		Thread.sleep(2000);

		checkCalculationsSalesOrder();
		Thread.sleep(2000);
		click(draft);
		Thread.sleep(7000);
		String header = getText(releaseInHeader);

		trackCode = getText(documentVal);
		System.out.println(trackCode);

		if (header.equals("(Draft)")) {
			writeTestResults("User should be able to draft the sales order", "Sales order should be drafted",
					"Sales order drafted", "Pass");

		} else {
			writeTestResults("User should be able to draft the sales order", "Sales order should be drafted",
					"Sales order not drafted", "Fail");
		}

		click(menu);
		Thread.sleep(3000);
		click(serialWise);
		Thread.sleep(7000);
		serialNoVal = getText(serialTextVal);
		Thread.sleep(2000);
		click(serialMenuClose);
		Thread.sleep(2000);
		click(serialBatch);
		Thread.sleep(3000);
		click(productListBtn);
		Thread.sleep(2000);
		sendKeys(serialText, serialNoVal);
		Thread.sleep(2000);
		pressEnter(serialText);
		Thread.sleep(2000);
		click(updateBtn);
		Thread.sleep(2000);
		click(backBtn);
		Thread.sleep(2000);
		click(release);
		Thread.sleep(7000);
		// String header = getText(releaseInHeader);

		// trackCode = getText(documentVal);

		if (isDisplayed(gotoPageWindow)) {
			writeTestResults("User should be able to see the tasks for the next document",
					"Link for outbound shipment should display", "Link displayed for the outbound shipment", "Pass");

		} else {
			writeTestResults("User should be able to release the sales order",
					"Link for outbound shipment should display", "Link not displayed for the outbound shipment",
					"Fail");
		}
	}

	public void checkFieldsSalesOrderSalesInvoiceSmoke() throws Exception {
		if (isDisplayed(customerAccSearchService)) {
			if (isDisplayed(currencyService)) {
				if (isDisplayed(billingAddressService)) {
					if (isDisplayed(productLookupService)) {
						if (isDisplayed(salesUnit)) {
							if (isDisplayed(accountOwner)) {
								writeTestResults("Verify that the mandatory fields are available on sales order page",
										"Mandatory fields should be available on sales order page",
										"Mandatory fields are available on sales order page", "Pass");
							}
						}
					}
				}

			}

		} else

		{
			writeTestResults("Verify that the mandatory fields are available on sales order page",
					"Mandatory fields should be available on sales order page", "Mandatory field is missing", "Fail");
		}
	}

	public void checkCalculationsSalesOrderSmoke() throws Exception {

		float unitPriceInv = Float.parseFloat(unitPriceValInv);
		float unitPriceSer = Float.parseFloat(unitPriceVal);
		float quantityInv = Float.parseFloat(quantityValInv);
		float quantitySer = Float.parseFloat(quantityVal);

		float lineTotInv = quantityInv * unitPriceInv;
		float lineTotSer = quantitySer * unitPriceSer;

		totInvSer = lineTotInv + lineTotSer;

		System.out.println(totInvSer);
		String bannerTot = getText(bannerTotal1);
		String bannerTotNew = bannerTot.replaceFirst(",", "");
		float bannerTotal = Float.valueOf(bannerTotNew);
		System.out.println(bannerTotNew);
		System.out.println(bannerTotal);
		System.out.println(totInvSer);

		if (totInvSer == bannerTotal) {

			writeTestResults("Verify values has calculated and updated under the bottom value bar and top left pannel",
					"Values should be calculate and update under the bottom value bar and top left pannel",
					"Calculations are accurate in sales order", "Pass");

		} else {
			writeTestResults("Verify that values have calculated and updated in top right panel",
					"Values should be calculate and update under the bottom value bar and top left pannel",
					"Calculations are incorrect in sales order", "Fail");
		}

	}

	public void outboundShipmentSalesOrderToSalesInvoiceSmoke() throws Exception {
		Thread.sleep(5000);
		click(outboundLink);
		Thread.sleep(5000);
		switchWindow();

		Thread.sleep(3000);
		click(checkoutOutbound);
		Thread.sleep(3000);
		click(outboundDraft);
		Thread.sleep(7000);

		outBoundID = getText(documentVal);
		// checkInventoryProduct();
		Thread.sleep(2000);
		// checkServiceProduct();
		String header = getText(releaseInHeader);
		trackCode = getText(documentVal);
		System.out.println(trackCode);
		Thread.sleep(2000);
		if (header.equals("(Draft)")) {
			writeTestResults("User should be able to draft the outbound shipment",
					"Outbound shipment should be drafted", "Outbound shipment drafted", "Pass");

		} else {
			writeTestResults("User should be able to draft the outbound shipment",
					"Outbound shipment should be drafted", "Outbound shipment not drafted", "Fail");
		}
		click(release);
		Thread.sleep(7000);

		if (isDisplayed(gotoPageWindow)) {
			writeTestResults("User should be able to see the tasks for the next document",
					"Link for sales invoice should display", "Link displayed for the sales invoice", "Pass");

		} else {
			writeTestResults("User should be able to see the tasks for the next document",
					"Link for sales invoice should display", "Link displayed for the sales invoice", "Fail");
		}

	}

	public void checkInventoryProductSmoke() throws Exception {
		String invenValue = "PR150 [Seat]";
		String invenValueOutbound = getText(outboundInvenValue);

		System.out.println(invenValue);
		System.out.println(invenValueOutbound);

		if (invenValue.equals(invenValueOutbound)) {
			writeTestResults("Added inventory products should be loaded to product grid in outbound shipment",
					"Inventory Product should be loaded", "Inventory Product loaded in outbound shipment", "Pass");
		} else {
			writeTestResults("Added inventory products should be loaded to product grid in outbound shipment",
					"Inventory Product should be loaded", "Inventory Product not loaded in outbound shipment", "Fail");
		}
	}

	public void checkServiceProductSmoke() throws Exception {

		if (isDisplayed(serviceProductValOutbound)) {
			writeTestResults("Added service product should not be loaded to product grid in outbound shipment",
					"Service Product should not be loaded", "Service Product not loaded in outbound", "Pass");
		} else {
			writeTestResults("Added service product should not be loaded to product grid in outbound shipment",
					"Service Product should not be loaded", "ServiceProduct loaded in outbound", "Fail");
		}

	}

	public void salesInvoiceSalesOrderToSalesInvoiceSmoke() throws Exception {

		Thread.sleep(3000);
		click(outboundLink);
		Thread.sleep(6000);
		switchWindow();
		click(checkoutInvoice);
		Thread.sleep(3000);

		// checkValueInSalesInvoice();

		if (isDisplayed(outboundDraft)) {
			writeTestResults("User should be able to see the draft button in sales invoice page",
					"Draft button should be available ", "Draft button is available on sales invoice page", "Pass");
			click(outboundDraft);
			Thread.sleep(6000);
		} else {
			writeTestResults("User should be able to see the draft button in sales invoice page",
					"Draft button should be available", "Draft button is not available on sales invoice page", "Fail");
		}

//		click(release);
//		
//		Thread.sleep(4000);
//		System.out.println(getText(releaseInHeader));
//		System.out.println(getText(documentVal));
//		
//		if(releaseInHeader.equals("(Released)")) {
//			trackCode = getText(documentVal);
//			writeTestResults("User should be able to release the document", "Release ", "Document released", "Pass");
//		}else {
//			writeTestResults("User should be able to release the document", "Release", "Document not released", "Fail");
//		}

		click(release);
		Thread.sleep(8000);
		String header = getText(releaseInHeader);

		SalesInvoiceID = getText(documentVal);

		trackCode = getText(documentVal);

		if (header.equals("(Released)")) {
			writeTestResults("User should be able to release the sales invoice", "Sales invoice should be released",
					"Sales invoice is released", "Pass");

		} else {
			writeTestResults("User should be able to release the sales invoice", "Sales invoice should be released",
					"Sales invoice is not released", "Fail");
		}
	}

	public void checkValueInSalesInvoiceSmoke() throws Exception {

		if (totalSalesInvoice.equals(totInvSer)) {
			writeTestResults("Values should be as sales order", "", "Values are same as in sales order", "Pass");
		} else {
			writeTestResults("", "", "Values are same as in sales order", "Fail");
		}
	}

	// smoke6

	public void fillDetailsSalesOrderToSalesInvoice6() throws Exception {

		checkFieldsSalesOrderSalesInvoice();

		click(customerAccountSearch);
		Thread.sleep(2000);
		sendKeys(customerAccountText, customerAccVal);
		Thread.sleep(2000);
		pressEnter(customerAccountText);
		Thread.sleep(2000);
		doubleClick(selected_val);
		Thread.sleep(3000);
		click(salesUnit);
		Thread.sleep(2000);
		selectIndex(salesUnit, 1);
		Thread.sleep(2000);
		// Service Product
		click(productLookup);
		Thread.sleep(2000);
		InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();
		sendKeys(productTextSer, obj1.readTestCreation("Service"));
		Thread.sleep(3500);
		pressEnter(productTextSer);
		Thread.sleep(4000);
		doubleClick(invSelectedVal);
		Thread.sleep(2000);

		if (isEnabled(wareHouse)) {
			writeTestResults("Verify that the warehouse field is disabled for service product",
					"Warehouse field should be disabled", "Warehouse field is enabled", "Fail");
		} else {
			writeTestResults("Verify that the warehouse field is disabled for service product",
					"Warehouse field should be disabled", "Warehouse field is disabled", "Pass");
		}

		click(unitPriceTxt);
		Thread.sleep(2000);
		sendKeys(unitPriceTxt, unitPriceVal);
		Thread.sleep(2000);
		click(qtyText);
		Thread.sleep(2000);
		sendKeys(qtyText, quantityVal);
		Thread.sleep(2000);
		click(addNewRecord);
		Thread.sleep(2000);
		click(productLookupInv);
		Thread.sleep(2000);
		InventoryAndWarehouseModule obj2 = new InventoryAndWarehouseModule();
		sendKeys(productTextInv, obj2.readTestCreation("SerielSpecific"));
		Thread.sleep(3500);
		pressEnter(productTextInv);
		Thread.sleep(4000);
		doubleClick(invSelectedVal);
		Thread.sleep(2000);
		selectText(wareHouse, warehouseVal1);
		Thread.sleep(2000);

//		if (isEnabled(unitPriceInve)) {
//			writeTestResults("User should be able to add unit price", "Unit price field should be enabled",
//					"Unit price field is enabled", "Pass");
//		} else {
//			writeTestResults("User should be able to add unit price", "Unit price field should be enabled",
//					"Unit price field is disabled", "Fail");
//		}

		sendKeys(unitPriceInve, unitPriceValInv);
		Thread.sleep(1000);
		sendKeys(qtyTextInve, quantityValInv);
		Thread.sleep(1000);
		click(checkoutSalesOrder);
		Thread.sleep(2000);

		checkCalculationsSalesOrder();
		Thread.sleep(2000);
		click(draft);
		Thread.sleep(6000);
		String header = getText(releaseInHeader);

		trackCode = getText(documentVal);
		System.out.println(trackCode);

		if (header.equals("(Draft)")) {
			writeTestResults("User should be able to draft the sales order", "Sales order should be drafted",
					"Sales order drafted", "Pass");

		} else {
			writeTestResults("User should be able to draft the sales order", "Sales order should be drafted",
					"Sales order not drafted", "Fail");
		}

		click(menu);
		Thread.sleep(3000);
		click(serialWise);
		Thread.sleep(8000);
		serialNoVal = getText(serialTextVal);
		Thread.sleep(2000);
		click(serialMenuClose);
		Thread.sleep(3000);
		click(serialBatch);
		Thread.sleep(4000);
		click(productListBtn);
		Thread.sleep(2000);
		sendKeys(serialText, serialNoVal);
		Thread.sleep(2000);
		pressEnter(serialText);
		Thread.sleep(2000);
		click(updateBtn);
		Thread.sleep(2000);
		click(backBtn);
		Thread.sleep(3000);
		click(release);
		Thread.sleep(7000);

//		if (isDisplayed(outboundLink)) {
//			writeTestResults("User should be able to release the sales order", "Sales order should be released",
//					"Sales order is released", "Pass");
//		} else {
//			writeTestResults("User should be able to release the sales order", "Sales order should be released",
//					"Sales order is not released", "Fail");
//		}
	}

	public void checkFieldsSalesOrderSalesInvoice6() throws Exception {
		if (isDisplayed(customerAccSearchService)) {
			if (isDisplayed(currencyService)) {
				if (isDisplayed(billingAddressService)) {
					if (isDisplayed(productLookupService)) {
						if (isDisplayed(salesUnit)) {
							if (isDisplayed(accountOwner)) {
								writeTestResults("Verify that the mandatory fields are available on sales order page",
										"Mandatory fields should be available on sales order page",
										"Mandatory fields are available on sales order page", "Pass");
							}
						}
					}
				}

			}

		} else

		{
			writeTestResults("Verify that the mandatory fields are available on sales order page",
					"Mandatory fields should be available on sales order page", "Mandatory field is missing", "Fail");
		}
	}

	public void checkCalculationsSalesOrder6() throws Exception {

		float unitPriceInv = Float.parseFloat(unitPriceValInv);
		float unitPriceSer = Float.parseFloat(unitPriceVal);
		float quantityInv = Float.parseFloat(quantityValInv);
		float quantitySer = Float.parseFloat(quantityVal);

		float lineTotInv = quantityInv * unitPriceInv;
		float lineTotSer = quantitySer * unitPriceSer;

		totInvSer = lineTotInv + lineTotSer;

		System.out.println(totInvSer);
		String bannerTot = getText(bannerTotal1);
		String bannerTotNew = bannerTot.replaceFirst(",", "");
		float bannerTotal = Float.valueOf(bannerTotNew);
		System.out.println(bannerTotNew);
		System.out.println(bannerTotal);
		System.out.println(totInvSer);

		if (totInvSer == bannerTotal) {

			writeTestResults("Verify values has calculated and updated under the bottom value bar and top left pannel",
					"Values should be calculate and update under the bottom value bar and top left pannel",
					"Calculations are accurate in sales order", "Pass");

		} else {
			writeTestResults("Verify that values have calculated and updated in top right panel",
					"Values should be calculate and update under the bottom value bar and top left pannel",
					"Calculations are incorrect in sales order", "Fail");
		}

	}

	public void outboundShipmentSalesOrderToSalesInvoice6() throws Exception {
		Thread.sleep(5000);
		click(outboundLink);
		switchWindow();

		Thread.sleep(2000);
		// checkFieldsSalesOrderOutbound();
		Thread.sleep(3000);
		click(checkoutOutbound);
		Thread.sleep(3000);
		click(outboundDraft);
		Thread.sleep(4000);

		outBoundID = getText(documentVal);
		checkInventoryProduct();
		Thread.sleep(2000);
		// checkServiceProduct();
		String header = getText(releaseInHeader);
		trackCode = getText(documentVal);
		System.out.println(trackCode);
		Thread.sleep(2000);
		if (header.equals("(Draft)")) {
			writeTestResults("User should be able to draft the sales order", "Sales order should be drafted",
					"Sales order drafted", "Pass");

		} else {
			writeTestResults("User should be able to draft the sales order", "Sales order should be drafted",
					"Sales order not drafted", "Fail");
		}
		click(release);
		Thread.sleep(3000);

		if (isDisplayed(outboundLink)) {
			writeTestResults("User should be able to release the Outbound shipment",
					"Outbound shipment should be released", "Outbound shipment is released", "Pass");

		} else {
			writeTestResults("User should be able to release the Outbound shipment",
					"Outbound shipment should be released", "Outbound shipment is not released", "Fail");
		}

	}

	public void checkInventoryProduct6() throws Exception {
		String invenValue = "PR150 [Seat]";
		String invenValueOutbound = getText(outboundInvenValue);

		System.out.println(invenValue);
		System.out.println(invenValueOutbound);

		if (invenValue.equals(invenValueOutbound)) {
			writeTestResults("Added inventory products should be loaded to product grid in outbound shipment",
					"Inventory Product should be loaded", "Inventory Product loaded in outbound shipment", "Pass");
		} else {
			writeTestResults("Added inventory products should be loaded to product grid in outbound shipment",
					"Inventory Product should be loaded", "Inventory Product not loaded in outbound shipment", "Fail");
		}
	}

	public void checkServiceProduct6() throws Exception {

		if (isDisplayed(serviceProductValOutbound)) {
			writeTestResults("Added service product should not be loaded to product grid in outbound shipment",
					"Service Product should not be loaded", "Service Product not loaded in outbound", "Pass");
		} else {
			writeTestResults("Added service product should not be loaded to product grid in outbound shipment",
					"Service Product should not be loaded", "ServiceProduct loaded in outbound", "Fail");
		}

	}

	public void salesInvoiceSalesOrderToSalesInvoice6() throws Exception {

		Thread.sleep(3000);
		click(outboundLink);
		Thread.sleep(3000);
		switchWindow();
		click(checkoutInvoice);
		Thread.sleep(3000);

		// checkValueInSalesInvoice();

		if (isDisplayed(outboundDraft)) {
			writeTestResults("User should be able to see the draft button in sales invoice page",
					"Draft button should be available ", "Draft button is available on sales invoice page", "Pass");
			click(outboundDraft);
		} else {
			writeTestResults("User should be able to see the draft button in sales invoice page",
					"Draft button should be available", "Draft button is not available on sales invoice page", "Fail");
		}

		Thread.sleep(5000);

//		click(release);
//		
//		Thread.sleep(4000);
//		System.out.println(getText(releaseInHeader));
//		System.out.println(getText(documentVal));
//		
//		if(releaseInHeader.equals("(Released)")) {
//			trackCode = getText(documentVal);
//			writeTestResults("User should be able to release the document", "Release ", "Document released", "Pass");
//		}else {
//			writeTestResults("User should be able to release the document", "Release", "Document not released", "Fail");
//		}

		click(release);
		Thread.sleep(8000);
		String header = getText(releaseInHeader);

		SalesInvoiceID = getText(documentVal);
		trackCode = getText(documentVal);

		if (header.equals("(Released)")) {
			writeTestResults("User should be able to release the sales invoice", "Sales invoice should be released",
					"Sales invoice is released", "Pass");
		} else {
			writeTestResults("User should be able to release the sales invoice", "Sales invoice should be released",
					"Sales invoice is not released", "Fail");
		}
	}

	public void checkValueInSalesInvoice6() throws Exception {

		if (totalSalesInvoice.equals(totInvSer)) {
			writeTestResults("Values should be as sales order", "", "Values are same as in sales order", "Pass");
		} else {
			writeTestResults("", "", "Values are same as in sales order", "Fail");
		}
	}

	// S8

	public void fillDetailsSalesInvoiceSer6Smoke8() throws Exception {

		click(cusAccountSearch);
		Thread.sleep(2000);
		sendKeys(customerAccountText, customerAccVal);
		Thread.sleep(2000);
		pressEnter(customerAccountText);
		Thread.sleep(2000);
		doubleClick(selected_val);
		Thread.sleep(2000);
		selectIndex(currencyDropdown, 2);
		Thread.sleep(1000);
		selectIndex(salesUnit, 1);
		Thread.sleep(2000);

		// Service Product
		click(productLookupBtn);
		Thread.sleep(2000);
		click(productTextSer);
		Thread.sleep(2000);
		InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();
		sendKeys(productTextSer, obj1.readTestCreation("Service"));
		Thread.sleep(3000);
		pressEnter(productTextSer);
		Thread.sleep(3000);

		doubleClick(proSelectedVAl);
		Thread.sleep(3000);
		click(unitPriceService);
		Thread.sleep(2000);
		sendKeys(unitPriceService, unitPriceSerVal);
		Thread.sleep(1500);
		click(addNewProduct);
		Thread.sleep(1500);
		click(productSearchOutbound);
		Thread.sleep(2000);
		sendKeys(productSearchTxt, invOutboundVal);
		Thread.sleep(1500);
		pressEnter(productSearchTxt);
		Thread.sleep(3000);

		if (!isElementPresent("//*[@id=\"g1010-t\"]/table/tbody/tr[1]/td[6]")) {

			writeTestResults("Verify that inventory product are not loading", "Inventory products should not be loaded",
					"Inventory product not loaded", "Pass");
		} else {
			writeTestResults("Verify that inventory product are not loading", "Inventory products should not be loaded",
					"Inventory product loaded", "Fail");
		}

	}

	// 7
	public void fillDetailsSalesQuatationDropSmoke7() throws Exception {

		selectIndex(accountHead, 0);
		Thread.sleep(2000);
		click(customerAccSearchQuat);
		Thread.sleep(2000);
		// click(customerAccountText);

		sendKeys(customerAccountText, customerAccValQuat);
		Thread.sleep(2000);
		pressEnter(customerAccountText);
		Thread.sleep(2000);
		doubleClick(selectedValLead);
		Thread.sleep(2000);
		// billing address and shipping address
		click(billingAddrSearch);
		Thread.sleep(2000);
		//
		sendKeys(billingAddrTxtQuat, billingAddrQuatVal);
		Thread.sleep(2000);
		sendKeys(shippingAddrTxtQuat, shippingAddrQuatVal);
		Thread.sleep(2000);
		click(apply_btn);

		Thread.sleep(2000);
		selectIndex(salesUnit, 3);

		// Service Product
		click(productSearchQuatation);
		Thread.sleep(3000);
		InventoryAndWarehouseModule obj2 = new InventoryAndWarehouseModule();
		sendKeys(productTextSer, obj2.readTestCreation("SerielSpecific"));
		Thread.sleep(3500);
		pressEnter(productTextSer);
		Thread.sleep(3500);
		doubleClick(proSelectedVAl);
		// selectIndex(locator, index);
		Thread.sleep(3000);
		click(checkoutQuatation);
		Thread.sleep(2000);
		click(draftQuatation);
		Thread.sleep(2000);
		click(releaseQuatation);
		Thread.sleep(7000);

		String header = getText(releaseInHeader);

		trackCode = getText(documentVal);
		System.out.println(trackCode);

		salesOrderID = getText(documentVal);

		if (header.equals("(Released)")) {
			writeTestResults("User should be able to release the sales quotation", "Sales quotation should be release",
					"Sales quotation released", "Pass");
		} else {
			writeTestResults("User should be able to release the sales quotation", "Sales quotation should be release",
					"Sales quotation not released", "Fail");
		}

		click(versionBtn);
		Thread.sleep(2000);
		// mouseMove(versionTab);

		mouseMove("//div[@class='color-selected']");
		Thread.sleep(2000);
		click("//span[@title='Confirm this version']");
		Thread.sleep(3000);
		click(tickConfirmVersion);
		Thread.sleep(2000);
		click(confirmBtn);
		Thread.sleep(4000);
		click(linkSalesOrder);
		Thread.sleep(6000);
		switchWindow();
		click(checkoutSalesOrderQuata);
		Thread.sleep(3000);
		click(draftSalesOrderQuata);
		Thread.sleep(6000);

		String header2 = getText(releaseInHeader);

		trackCode = getText(documentVal);
		// System.out.println(trackCode);

		salesOrderID = getText(documentVal);

		if (header2.equals("(Draft)")) {
			writeTestResults("User should be able to draft the sales order", "Sales order should be drafted",
					"Sales order drafted", "Pass");
		} else {
			writeTestResults("User should be able to draft the sales order", "Sales order should be drafted",
					"Sales order not drafted", "Fail");
		}

		click(release);
		InventoryAndWarehouseModuleRegression obj = new InventoryAndWarehouseModuleRegression();
		obj.customizeLoadingDelay(actionBtn, 20000);
		click(actionBtn);
		Thread.sleep(2000);
		click(convert);
		Thread.sleep(3000);
		click(vendorSearch);
		Thread.sleep(2000);
		sendKeys(vendorTextt, vendorVal2);
		Thread.sleep(2000);
		pressEnter(vendorTextt);
		Thread.sleep(2000);
		doubleClick(selectedValQuat2);
		Thread.sleep(2000);
		click(checkoutOutboundquata);
		Thread.sleep(3000);
		click(draftOutboundquata);
		Thread.sleep(6000);
//
//		String header3 = getText(releaseInHeader);
//
//		trackCode = getText(documentVal);
//		System.out.println(trackCode);
//
//		if (header3.equals("Draft")) {
//			
//			writeTestResults("User should be able to draft the purchase order", "Purchase order should be draft",
//					"Purchase order draft", "Pass");
//		} else {
//			writeTestResults("User should be able to draft the purchase order", "Purchase order should be draft",
//					"Purchase order draft", "Fail");
//		}

		click(release);
		Thread.sleep(7000);

		if (isDisplayed(gotoPageWindow)) {
			writeTestResults("User should be able to release the purchase order", "Purchase order should be released",
					"Purchase order released", "Pass");
		} else {
			writeTestResults("User should be able to release the purchase order", "Purchase order should be released",
					"Purchase order not released", "Fail");
		}

		Thread.sleep(3000);
		click(linkSalesOrder);
		Thread.sleep(6000);
		switchWindow();
		click(checkoutOutboundquata);
		Thread.sleep(3000);
		click(draftOutboundquata);
		Thread.sleep(6000);
		click(release);
		Thread.sleep(7000);

		String header1 = getText(releaseInHeader);

		trackCode = getText(documentVal);
		// System.out.println(trackCode);

		if (header1.equals("(Released)")) {
			writeTestResults("User should be able to release the purchase invoice",
					"Purchase invoice should be released", "Purchase invoice released", "Pass");
		} else {
			writeTestResults("User should be able to release the purchase invoice",
					"Purchase invoice should be released", "Purchase invoice not released", "Fail");
		}
	}

	public void gotoSalesInvoice() throws Exception {

		click(entutionHeader);
		Thread.sleep(3000);
		click(taskEvent);
		Thread.sleep(3000);
		mouseMove(salesInvoiceTile);
		Thread.sleep(1000);
		click(salesInvoiceTile);
		Thread.sleep(4000);

		String val = valueSalesOrder.replace("SO/002394", salesOrderID);
		Thread.sleep(3000);
		click(val);
		Thread.sleep(5000);
		switchWindow();
		click(checkoutInboundshipment);
		Thread.sleep(4000);
		click(draft);
		Thread.sleep(6000);
		click(release);
		Thread.sleep(7000);
		String header1 = getText(releaseInHeader);

		trackCode = getText(documentVal);
		// System.out.println(trackCode);

		if (header1.equals("(Released)")) {
			writeTestResults("User should be able to release the sales invoice", "Sales invoice should be released",
					"Sales invoice released", "Pass");
		} else {
			writeTestResults("User should be able to release the sales invoice", "Sales invoice should be released",
					"Sales invoice not released", "Fail");
		}
	}

	public void configureJourney() throws Exception {

		click(journeyConfiguration);
		Thread.sleep(2000);
		mouseMove(salesOrderToSalesInvoiceJourney);
		click(salesOrderToSalesInvoiceJourney);
		Thread.sleep(2000);

		if (isSelected(autogenerateSalesInvoice)) {

			writeTestResults("", "", "Selected", "Pass");

		}
		if (isSelected(autogenerateOutboundShipment)) {

			writeTestResults("", "", "selected", "Pass");
		}
		if (!isSelected(autogenerateSalesInvoice) && !isSelected(autogenerateOutboundShipment)) {
			click(autogenerateSalesInvoice);
			Thread.sleep(2000);
			click(autogenerateOutboundShipment);
			click(updateJourney);

		}
	}

	public void checkOutboundDocument() throws Exception {

		click(outboundDoc);
		Thread.sleep(6000);
		switchWindow();

		click(checkOutbound);
		Thread.sleep(2000);
		click(draft);
		InventoryAndWarehouseModule obj = new InventoryAndWarehouseModule();
		Thread.sleep(7000);
		click(release);
		Thread.sleep(4000);
		pageRefersh();
		Thread.sleep(7000);
		String header = getText(releaseInHeader);
		System.out.println(header);

		if (header.equals("(Released)")) {
			writeTestResults("Verify whether outbound shipment is released", "Outbound shipment should be relesed",
					"Outbound shipment is released", "Pass");
		} else {
			writeTestResults("Verify whether outbound shipment is released", "Outbound shipment should be relesed",
					"Outbound shipment is not released", "Fail");
		}
	}

	public void checkInvoiceDocument() throws Exception {

		Thread.sleep(2000);
		backToDefaultPage();
		Thread.sleep(2000);
		switchWindow();
		Thread.sleep(5000);
		click(salesInvoiceDoc);
		Thread.sleep(5000);
		switchWindow();

		String header = getText(releaseInHeader);

		if (header.equals("(Released)")) {
			writeTestResults("Verify whether the Sales invoice released", "Sales invoice should be released",
					"Sales invoice released", "Pass");
		} else {
			writeTestResults("Verify whether the Sales invoice released", "Sales invoice should be released",
					"Sales invoice released ", "Fail");
		}
	}

	// smoke 1

	public void fillDetailsSalesOrderToSalesInvoiceSmoke1() throws Exception {

		checkFieldsSalesOrderSalesInvoice();

		click(customerAccountSearch);
		Thread.sleep(2000);
		sendKeys(customerAccountText, customerAccVal);
		Thread.sleep(3000);
		pressEnter(customerAccountText);
		Thread.sleep(2500);
		doubleClick(selected_val);
		Thread.sleep(2000);
		click(salesUnit);
		Thread.sleep(1000);
		selectIndex(salesUnit, 1);
		Thread.sleep(200);
		// Service Product
		click(productLookup);
		Thread.sleep(3000);
		InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();
		sendKeys(productTextSer, obj1.readTestCreation("Service"));
		Thread.sleep(4000);
		// System.out.println(obj1);
		pressEnter(productTextSer);
		Thread.sleep(4500);
		doubleClick(proSelectedVAl);
		Thread.sleep(2500);

		if (isEnabled(wareHouse)) {
			writeTestResults("Verify that the warehouse field is disabled for service product",
					"Warehouse field should be disabled", "Warehouse field is enabled", "Fail");
		} else {
			writeTestResults("Verify that the warehouse field is disabled for service product",
					"Warehouse field should be disabled", "Warehouse field is disabled", "Pass");
		}

		click(unitPriceTxt);
		Thread.sleep(1000);
		sendKeys(unitPriceTxt, unitPriceVal);
		Thread.sleep(1000);
		click(qtyText);
		Thread.sleep(1000);
		sendKeys(qtyText, quantityVal);
		Thread.sleep(1000);
		click(addNewRecord);
		Thread.sleep(1000);
		click(productLookupInv);
		Thread.sleep(3000);
		InventoryAndWarehouseModule obj2 = new InventoryAndWarehouseModule();
		sendKeys(productTextInv, obj2.readTestCreation("SerielSpecific"));
		Thread.sleep(3500);
		pressEnter(productTextInv);
		Thread.sleep(4500);
		doubleClick(invSelectedVal);
		Thread.sleep(2500);
		selectText(wareHouse, warehouseVal1);
		Thread.sleep(2000);

//		if (isEnabled(unitPriceInve)) {
//			writeTestResults("User should be able to add unit price", "Unit price field should be enabled",
//					"Unit price field is enabled", "Pass");
//		} else {
//			writeTestResults("User should be able to add unit price", "Unit price field should be enabled",
//					"Unit price field is disabled", "Fail");
//		}

		sendKeys(unitPriceInve, unitPriceValInv);
		Thread.sleep(1000);
		sendKeys(qtyTextInve, quantityValInv);
		Thread.sleep(1000);
		click(checkoutSalesOrder);
		Thread.sleep(2000);

		checkCalculationsSalesOrder();
		Thread.sleep(2000);
		click(draft);
		Thread.sleep(6000);
		String header = getText(releaseInHeader);

		SO = getText(documentVal);
		// System.out.println(SO);
		trackCode = getText(documentVal);
		// System.out.println(trackCode);

		if (header.equals("(Draft)")) {
			writeTestResults("User should be able to draft the sales order", "Sales order should be drafted",
					"Sales order drafted", "Pass");
		} else {
			writeTestResults("User should be able to draft the sales order", "Sales order should be drafted",
					"Sales order not drafted", "Fail");
		}

		click(menu);
		Thread.sleep(3000);
		click(serialWise);
		Thread.sleep(12000);
		serialNoVal = getText(serialTextVal);
		Thread.sleep(2000);
		Actions action = new Actions(driver);
		WebElement we = driver.findElement(By.xpath(serialMenuClose));
		action.moveToElement(we).build().perform();
		click(serialMenuClose);
		Thread.sleep(2000);
		click(serialBatch);
		Thread.sleep(3000);
		click(productListBtn);
		Thread.sleep(3000);
		sendKeys(serialText, serialNoVal);
		Thread.sleep(2000);
		pressEnter(serialText);
		Thread.sleep(3000);
		click(updateBtn);
		Thread.sleep(3000);
		click(backBtn);
		Thread.sleep(2000);
		click(release);
		Thread.sleep(8000);
		// String header = getText(releaseInHeader);
		// trackCode = getText(documentVal);

		if (isDisplayed(gotoPageWindow)) {
			writeTestResults("User should be able to release the sales order", "Sales order should be released",
					"Sales order is released", "Pass");

			click(outboundLink);
			Thread.sleep(7000);
		} else {
			writeTestResults("User should be able to release the sales order", "Sales order should be released",
					"Sales order is not released", "Fail");
		}
	}

	public void checkFieldsSalesOrderSalesInvoiceSmoke1() throws Exception {
		if (isDisplayed(customerAccSearchService)) {
			if (isDisplayed(currencyService)) {
				if (isDisplayed(billingAddressService)) {
					if (isDisplayed(productLookupService)) {
						if (isDisplayed(salesUnit)) {
							if (isDisplayed(accountOwner)) {
								writeTestResults("Verify that the mandatory fields are available on sales order page",
										"Mandatory fields should be available on sales order page",
										"Mandatory fields are available on sales order page", "Pass");
							}
						}
					}
				}

			}

		} else

		{
			writeTestResults("Verify that the mandatory fields are available on sales order page",
					"Mandatory fields should be available on sales order page", "Mandatory field is missing", "Fail");
		}
	}

	public void checkCalculationsSalesOrderSmoke1() throws Exception {

		float unitPriceInv = Float.parseFloat(unitPriceValInv);
		float unitPriceSer = Float.parseFloat(unitPriceVal);
		float quantityInv = Float.parseFloat(quantityValInv);
		float quantitySer = Float.parseFloat(quantityVal);

		float lineTotInv = quantityInv * unitPriceInv;
		float lineTotSer = quantitySer * unitPriceSer;

		totInvSer = lineTotInv + lineTotSer;

		System.out.println(totInvSer);
		String bannerTot = getText(bannerTotal1);
		String bannerTotNew = bannerTot.replaceFirst(",", "");
		float bannerTotal = Float.valueOf(bannerTotNew);
		System.out.println(bannerTotNew);
		System.out.println(bannerTotal);
		System.out.println(totInvSer);

		if (totInvSer == bannerTotal) {

			writeTestResults("Verify values has calculated and updated under the bottom value bar and top left pannel",
					"Values should be calculate and update under the bottom value bar and top left pannel",
					"Calculations are accurate in sales order", "Pass");

		} else {
			writeTestResults("Verify that values have calculated and updated in top right panel",
					"Values should be calculate and update under the bottom value bar and top left pannel",
					"Calculations are incorrect in sales order", "Fail");
		}

	}

	public void outboundShipmentSalesOrderToSalesInvoiceSmoke1() throws Exception {

		switchWindow();
		Thread.sleep(2000);
		// checkFieldsSalesOrderOutbound();
		Thread.sleep(3000);
		click(checkoutOutbound);
		Thread.sleep(3000);
		click(outboundDraft);
		Thread.sleep(6000);

		outBoundID = getText(documentVal);
		// checkInventoryProduct();
		Thread.sleep(2000);
		// checkServiceProduct();
		String header = getText(releaseInHeader);
		trackCode = getText(documentVal);

		AOD = getText(documentVal);
		// System.out.println(AOD);

		Thread.sleep(2000);
		if (header.equals("(Draft)")) {
			writeTestResults("User should be able to draft the outbound shipment",
					"Outbound shipment should be drafted", "Outbound shipment drafted", "Pass");
		} else {
			writeTestResults("User should be able to draft the outbound shipment",
					"Outbound shipment should be drafted", "Outbound shipment not drafted", "Fail");
		}

		click(release);
		Thread.sleep(8000);

		if (isDisplayed(gotoPageWindow)) {
			writeTestResults("User should be able to release the Outbound shipment",
					"Outbound shipment should be released", "Outbound shipment is released", "Pass");
			click(outboundLink);
			Thread.sleep(6000);
		} else {
			writeTestResults("User should be able to release the Outbound shipment",
					"Outbound shipment should be released", "Outbound shipment is not released", "Fail");
		}
	}

	public void checkInventoryProductSmoke1() throws Exception {

		String invenValue = "PR150 [Seat]";
		String invenValueOutbound = getText(outboundInvenValue);

		System.out.println(invenValue);
		System.out.println(invenValueOutbound);

		if (invenValue.equals(invenValueOutbound)) {
			writeTestResults("Added inventory products should be loaded to product grid in outbound shipment",
					"Inventory Product should be loaded", "Inventory Product loaded in outbound shipment", "Pass");
		} else {
			writeTestResults("Added inventory products should be loaded to product grid in outbound shipment",
					"Inventory Product should be loaded", "Inventory Product not loaded in outbound shipment", "Fail");
		}
	}

	public void checkServiceProductSmoke1() throws Exception {

		if (isDisplayed(serviceProductValOutbound)) {
			writeTestResults("Added service product should not be loaded to product grid in outbound shipment",
					"Service Product should not be loaded", "Service Product not loaded in outbound", "Pass");
		} else {
			writeTestResults("Added service product should not be loaded to product grid in outbound shipment",
					"Service Product should not be loaded", "ServiceProduct loaded in outbound", "Fail");
		}
	}

	public void configureJourneyOneSmoke1() throws Exception {

		click(journeyConfiguration);
		Thread.sleep(2000);
		mouseMove(salesOrderToSalesInvoiceJourney);
		click(salesOrderToSalesInvoiceJourney);
		Thread.sleep(2000);

		if (isSelected(autogenerateSalesInvoice) && isSelected(autogenerateOutboundShipment)) {
			click(autogenerateSalesInvoice);
			Thread.sleep(2000);
			click(autogenerateOutboundShipment);
			click(updateJourney);
		}
	}

	public void salesInvoiceSalesOrderToSalesInvoiceSmoke1() throws Exception {

		Thread.sleep(3000);
		switchWindow();
		click(checkoutInvoice);
		Thread.sleep(3000);

		checkValueInSalesInvoice();
		Thread.sleep(2000);

		if (isDisplayed(outboundDraft)) {
			writeTestResults("User should be able to see the draft button in sales invoice page",
					"Draft button should be available ", "Draft button is available on sales invoice page", "Pass");
			click(outboundDraft);
			Thread.sleep(6000);
		} else {
			writeTestResults("User should be able to see the draft button in sales invoice page",
					"Draft button should be available", "Draft button is not available on sales invoice page", "Fail");
		}

//		click(release);
//		
//		Thread.sleep(4000);
//		System.out.println(getText(releaseInHeader));
//		System.out.println(getText(documentVal));
//		
//		if(releaseInHeader.equals("(Released)")) {
//			trackCode = getText(documentVal);
//			writeTestResults("User should be able to release the document", "Release ", "Document released", "Pass");
//		}else {
//			writeTestResults("User should be able to release the document", "Release", "Document not released", "Fail");
//		}

		click(release);
		Thread.sleep(8000);
		String header = getText(releaseInHeader);

		SalesInvoiceID = getText(documentVal);

		SCD = getText(documentVal);
		System.out.println(SCD);
		trackCode = getText(documentVal);

		if (header.equals("(Released)")) {
			writeTestResults("User should be able to release the sales invoice", "Sales invoice should be released",
					"Sales invoice is released", "Pass");
		} else {
			writeTestResults("User should be able to release the sales invoice", "Sales invoice should be released",
					"Sales invoice is not released", "Fail");
		}
	}

	public void checkValueInSalesInvoiceSmoke1() throws Exception {

		System.out.println(totInvSer);

		String val = getText(totalSalesInvoice);
		System.out.println(val);
//		if (totalSalesInvoice.equals(totInvSer)) {
//			writeTestResults("Values should be as sales order", "", "Values are same as in sales order", "Pass");
//		} else {
//			writeTestResults("", "", "Values are same as in sales order", "Fail");
//		}
	}

	// smoke bin card

	public void fillDetailsSalesReturnOrderToSalesReturnInvoiceSmoke() throws Exception {

		click(customerAccSearchReturn);
		Thread.sleep(2000);
		sendKeys(customerAccTxtReturn, customerAccReturnVal);
		Thread.sleep(2000);
		pressEnter(customerAccTxtReturn);
		Thread.sleep(3000);
		doubleClick(selectedValCustomerReturn);
		Thread.sleep(2000);
		// selectIndex(currencyReturn, 7);
		selectIndex(salesUnitReturn, 2);
		Thread.sleep(1000);

		click(documentListReturn);
		Thread.sleep(4000);
//		click(fromDateReturn);
//		Thread.sleep(2000);
//		click(fromDateSelected);
//		Thread.sleep(2000);
//		click(toDateReturn);
//		Thread.sleep(2000);
//		click(toDateSelected);
//		Thread.sleep(2000);
//		click(searchBtnReturn);
//		Thread.sleep(2000);
//		click(tickSalesOrder);
//		Thread.sleep(2000);

//		click(fromDate);
//		Thread.sleep(2000);
//		click(dateSelected);
//		Thread.sleep(2000);
		click(searchDocumentList);
		Thread.sleep(8000);

		String one = checkDocumentListRet.replace("SCD/PI/00222", SalesInvoiceID);
		click(one);
		Thread.sleep(2000);
		// click(applyExchange);
		click(applyReturn);
		Thread.sleep(2000);
		selectIndex(returnReason, 2);
		Thread.sleep(2100);
		selectIndex(dispositionCategory, 0);
		Thread.sleep(2000);
		click(checkoutReturn);
		Thread.sleep(3000);
		click(draftReturn);
		Thread.sleep(5900);

		String header = getText(releaseInHeader);
		trackCode = getText(documentVal);
		// System.out.println(trackCode);
		Thread.sleep(2000);

		if (header.equals("(Draft)")) {
			writeTestResults("User should be able to draft the sales return order",
					"Sales return order should be drafted", "Sales return order drafted", "Pass");
		} else {
			writeTestResults("User should be able to draft the sales return order",
					"Sales return order should be drafted", "Sales return order not drafted", "Fail");
		}

		click(releaseReturn);
		Thread.sleep(7000);

		if (isDisplayed(gotoPageWindow)) {

			writeTestResults("Verify that user is able to release the sales return order",
					"User should be able to release the sales return order", "Sales return order is released", "Pass");
			click(linkReturn);
			Thread.sleep(7000);
		} else {
			writeTestResults("Verify that user is able to release the sales return order",
					"User should be able to release the sales return order", "Sales return order is not released",
					"Fail");
		}

		switchWindow();
		click(checkoutInboundshipment);
		Thread.sleep(3000);
		click(draftInbound);
		Thread.sleep(6000);

		String header3 = getText(releaseInHeader);

		inboundShipmentID = getText(documentVal);
		trackCode = getText(documentVal);
		// System.out.println(trackCode);
		Thread.sleep(2000);

		if (header3.equals("(Draft)")) {
			writeTestResults("User should be able to draft the inbound shipment", "Inbound shipment should be drafted",
					"Inbound shipment drafted", "Pass");
		} else {
			writeTestResults("User should be able to draft the inbound shipment", "Inbound shipment should be drafted",
					"Inbound shipment not drafted", "Fail");
		}

		click(serialInbound);
		Thread.sleep(2000);
		click(listInbound);
		Thread.sleep(2500);
		click(checkBox);
		Thread.sleep(2000);
		click(updateInbound);
		Thread.sleep(2500);
		click(backInbound);
		Thread.sleep(3000);
		click(releaseInbound);
		Thread.sleep(7000);

		click(outboundLink);
		Thread.sleep(7000);
		switchWindow();
		click(checkoutReturnInvoice);
		Thread.sleep(2000);
		click(draftReturnInvoice);
		Thread.sleep(6000);
		click(releaseReturnInvoice);
		Thread.sleep(7000);

		String header2 = getText(releaseInHeader);

		trackCode = getText(documentVal);
		// System.out.println(trackCode);

		if (header2.equals("(Released)")) {
			writeTestResults("User should be able to release the sales return invoice",
					"Sales return invoice should be released", "Sales return invoice released", "Pass");
		} else {
			writeTestResults("User should be able to release the sales return invoice",
					"Sales return invoice should be released", "Sales return invoice not released", "Fail");
		}
	}

	public void accountGroupDraftNew() throws Exception {

		click(AccountGroupConfiguration);
		Thread.sleep(2000);
		click(newAccountGroupConfiguration);
		Thread.sleep(2000);
		click(addNewAccGrp);
		Thread.sleep(2000);

		click(addNewRecordAccGrp);
		Thread.sleep(2000);
		LocalTime myObj = LocalTime.now();

		sendKeys(accGrpText, accGrpVal + myObj);
		click(updateAccGrp);
		Thread.sleep(3000);
		// click(activeCheckBox);
		Thread.sleep(2000);

		String val = accGrpVal + myObj;

		selectText(selectAccGroup, val);
		selectText(currencyAccGroup, currencyVal);
		selectIndex(salesUnitAccGrp, 2);
		Thread.sleep(2000);
		click(detailsAccGrp);
		sendKeys(leadDays, leadDaysVal);
		Thread.sleep(2000);

		click("//li[@id='#divGen']");
		click(detailsAccGrp);
		String value = getAttribute(leadDays, "value");
		// System.out.println(value);

		click(draftNew);
		Thread.sleep(3000);

		click(detailsAccGrp);

		String valueAft = getAttribute(leadDays, "value");
		// System.out.println(valueAft);

		if (valueAft.equals("0")) {
			writeTestResults("Verify that draft and new", "Document should draft and new", "Draft and new done",
					"Pass");
		} else {
			writeTestResults("Verify that draft and new", "Document should draft and new", "Draft and new not done",
					"Fail");
		}

	}

	public void accountGroupDraft() throws Exception {

		click(AccountGroupConfiguration);
		Thread.sleep(2000);
		click(newAccountGroupConfiguration);
		Thread.sleep(2000);

		click(addNewAccGrp);
		Thread.sleep(2000);

		click(addNewRecordAccGrp);
		Thread.sleep(2000);
		LocalTime myObj = LocalTime.now();

		sendKeys(accGrpText, accGrpVal + myObj);
		click(updateAccGrp);
		Thread.sleep(3000);
		// click(activeCheckBox);
		Thread.sleep(2000);

		String val = accGrpVal + myObj;

		selectText(selectAccGroup, val);
		selectText(currencyAccGroup, currencyVal);
		selectIndex(salesUnitAccGrp, 2);
		click(draft);
		Thread.sleep(3000);

		String header = getText(releaseInHeader);

		accountGroup = getText(documentVal);
		// System.out.println(accountGroup);
		trackCode = getText(documentVal);
		// System.out.println(trackCode);

		if (header.equals("(Draft)")) {
			writeTestResults("User should be able to draft the account group", "Account group should be drafted",
					"Account groupr drafted", "Pass");
		} else {
			writeTestResults("User should be able to draft the account group", "Account group should be drafted",
					"Account group not drafted", "Fail");
		}
	}

	public void accountGroupEditBtn() throws Exception {

		click(edit);
		Thread.sleep(2000);

		if (isDisplayed(update)) {
			writeTestResults("Verify the document is in edit mode", "Document should be in edit", "Document is in edit",
					"Pass");
		} else {
			writeTestResults("Verify the document is in edit mode", "Document should be in edit",
					"Document is not in edit", "Fail");
		}
	}

	public void accountGroupUpdate() throws Exception {

		click(detailsAccGrp);
		Thread.sleep(1000);

		click(draft);
		Thread.sleep(3000);

		click(edit);
		Thread.sleep(2000);

		click(detailsAccGrp);
		sendKeys(leadDays, leadDaysVal);
		Thread.sleep(1000);
		click(update);
		Thread.sleep(3000);

		click(edit);
		Thread.sleep(2000);
		click(detailsAccGrp);
		Thread.sleep(2000);

		String value = getAttribute(leadDays, "value");

		if (value.equals("12")) {
			writeTestResults("Verify the update in account group", "User should be able to update account group",
					"Account group updated", "Pass");
		} else {
			writeTestResults("Verify the update in account group", "User should be able to update account group",
					"Account group not updated", "Fail");
		}

		click(update);
		Thread.sleep(2000);
	}

	public void accountGroupRelease() throws Exception {

		click(release);
		Thread.sleep(2000);
		String header = getText(releaseInHeader);
		accountGroup = getText(documentVal);

		if (header.equals("(Released)")) {
			writeTestResults("User should be able to release the account group", "Account group should be released",
					"Account groupr released", "Pass");
		} else {
			writeTestResults("User should be able to release the account group", "Account group should be released",
					"Account group not released", "Fail");
		}
	}

	public void accountGroupHistory() throws Exception {

		click(actionMenu);
		Thread.sleep(1000);
		click(history);
		Thread.sleep(2000);

		if (isDisplayed(historyReleased)) {
			writeTestResults("Verify the history in account group", "User should be able to see the history",
					"History displayed", "Pass");
		} else {
			writeTestResults("Verify the history in account group", "User should be able to see the history",
					"History not displayed", "Fail");
		}

		click(activityMenuClose);
		Thread.sleep(1000);
	}

	public void accountGroupActivity() throws Exception {

		click(actionMenu);
		Thread.sleep(1000);
		click(activities);
		Thread.sleep(1000);

		if (isDisplayed(activityHeader)) {
			writeTestResults("Verify the actvities in account group",
					"User should be able to see the activities in account group", "", "Pass");
		} else {
			writeTestResults("Verify the actvities in account group",
					"User should be able to see the activities in account group", "", "Fail");
		}
	}

	public void accountInformationDraftNew() throws Exception {

		click(newAccount);
		checkTheMandatoryFields();
		selectIndex(accountTypeAccInfor, 1);
		Thread.sleep(2000);
		selectIndex(accountNameTypeAccInfor, 2);
		sendKeys(accountNameText, accountNameVal);
		selectIndex(accountGroupAccInfor, 1);
		selectIndex(currencyAccInfor, 3);
		selectIndex(salesUnitAccInfor, 3);
		// selectIndex(accountOwner, 3);
		selectIndex(contactTitleAccInfor, 3);
		sendKeys(contactNameAccInfor, contactNameVal);
		sendKeys(phoneAccInfor, phoneNoVal);
		Thread.sleep(2000);
		sendKeys(mobileAccInfor, mobileNoVal);
		sendKeys(emailAccInfor, emailVal);
		Thread.sleep(2000);
		sendKeys(designationAccInfor, designationVal);
		Thread.sleep(2000);

		click(detailsTab);

		sendKeys(annualRevenue, annualRevenueVal);
		// click(summaryTab);
		// click(detailsTab);
		String value = getAttribute(annualRevenue, "value");
		System.out.println(value);

		click(draftNew);
		Thread.sleep(2000);

		String valueAft = getAttribute(annualRevenue, "value");
		System.out.println(valueAft);

		if (valueAft.equals("0.00000")) {
			writeTestResults("", "", "Draft and new success", "Pass");
		} else {
			writeTestResults("", "", "Draft and new success", "Fail");
		}

	}

	public void AVaccountInformationDraft() throws Exception {

		click(newAccount);
		checkTheMandatoryFields();
		selectIndex(accountTypeAccInfor, 1);
		Thread.sleep(2000);
		selectIndex(accountNameTypeAccInfor, 2);
		sendKeys(accountNameText, accountNameVal);
		selectIndex(accountGroupAccInfor, 1);
		selectIndex(currencyAccInfor, 3);
		selectIndex(salesUnitAccInfor, 3);
		// selectIndex(accountOwner, 3);
		selectIndex(contactTitleAccInfor, 3);
		sendKeys(contactNameAccInfor, contactNameVal);
		sendKeys(phoneAccInfor, phoneNoVal);
		Thread.sleep(2000);
		sendKeys(mobileAccInfor, mobileNoVal);
		sendKeys(emailAccInfor, emailVal);
		Thread.sleep(2000);
		sendKeys(designationAccInfor, designationVal);
		Thread.sleep(2000);
		if (isDisplayed(draftAccInfor)) {
			writeTestResults("Verify that draft button is available on account information page",
					"User should be able to see the draft button", "Draft button is available", "Pass");
			click(draftAccInfor);
		} else {
			writeTestResults("Verify that draft button is available on account information page",
					"User should be able to see the draft button", "Draft button is not available", "Fail");
		}

		Thread.sleep(3000);
		String header = getText(releaseInHeader);

		if (header.equals("(Draft)")) {
			writeTestResults("User should be able to draft the account information",
					"Account information should be drafted", "Account information drafted", "Pass");
		} else {
			writeTestResults("User should be able to draft the account information",
					"Account information should be drafted", "Account information not drafted", "Fail");
		}
	}

	public void AVaccountInformationEdit() throws Exception {

		click(edit);

		if (isDisplayed(update)) {
			writeTestResults("Verify edit in account information", "User should be able to do the edit", "Edit done",
					"Pass");
		} else {
			writeTestResults("Verify edit in account information", "User should be able to do the edit",
					"Edit not done", "Fail");
		}
	}

	public void AVAccountInformationUpdate() throws Exception {

		String mobile = getAttribute(mobileAccInfor, "value");

		click(edit);
		Thread.sleep(2000);

		sendKeys(mobileAccInfor, newMobileNo);
		Thread.sleep(1000);
		click(update);
		Thread.sleep(2000);
		String mobileAft = getAttribute(mobileAccInfor, "value");
		System.out.println(mobileAft);

		if (!mobile.equals(mobileAft)) {
			writeTestResults("Verify the update is working", "Changes should be update", "Changes updated", "Pass");
		} else {
			writeTestResults("Verify the update is working", "Changes should be update", "Changes not updated", "Fail");
		}
	}

	public void AVaccountInformationUpdateNew() throws Exception {

		click(edit);
		Thread.sleep(3000);

		sendKeys(mobileAccInfor, newMobileNo);
		Thread.sleep(1000);
		click(updateAndNew);
		Thread.sleep(3000);

		String header = getText(documentVal);
		System.out.println(header);

		if (header.equals("New") && isDisplayed(draftNew)) {
			writeTestResults("Verify the update & New is working", "Update and new", "Update and new done", "Pass");
		} else {
			writeTestResults("Verify the update & New is working", "Update and new", "Update and new not done", "Fail");
		}
	}

	public void AVaccountInfRelease() throws Exception {

		click(newAccount);
		checkTheMandatoryFields();
		selectIndex(accountTypeAccInfor, 1);
		Thread.sleep(2000);
		selectIndex(accountNameTypeAccInfor, 2);
		sendKeys(accountNameText, accountNameVal);
		selectIndex(accountGroupAccInfor, 1);
		selectIndex(currencyAccInfor, 3);
		selectIndex(salesUnitAccInfor, 3);
		// selectIndex(accountOwner, 3);
		selectIndex(contactTitleAccInfor, 3);
		sendKeys(contactNameAccInfor, contactNameVal);
		sendKeys(phoneAccInfor, phoneNoVal);
		Thread.sleep(2000);
		sendKeys(mobileAccInfor, mobileNoVal);
		sendKeys(emailAccInfor, emailVal);
		Thread.sleep(2000);
		sendKeys(designationAccInfor, designationVal);
		Thread.sleep(2000);
		click(release);
		Thread.sleep(3000);

		String header2 = getText(releaseInHeader);

		if (header2.equals("Released")) {
			writeTestResults("Verify the release in account information", "Account information should be released",
					"Account information is released", "Pass");
		} else {
			writeTestResults("Verify the release in account information", "Account information should be released",
					"Account information is released", "Fail");
		}
	}

	public void AVaccountInfHistory() throws Exception {

		click(actionMenu);
		Thread.sleep(2000);
		click(history);
		Thread.sleep(2000);

		if (isElementPresent(historyReleased)) {
			writeTestResults("Verify the history in account information",
					"User should be able to see the account information history", "History displayed", "Pass");
		} else {
			writeTestResults("Verify the history in account information",
					"User should be able to see the account information history", "History not displayed", "Fail");
		}
	}

	public void checkClickCollectionUnit() throws Exception {

		if (isDisplayed(collectionUnit)) {
			writeTestResults("Verify that 'Collection Unit' is available",
					"User should be able to see the 'Account Information'", "'Collection Unit' is dislayed", "Pass");
			click(collectionUnit);
		} else {
			writeTestResults("Verify that 'Collection Unit' is available",
					"User should be able to see the 'Collection Unit'", "'Collection Unit' is not dislayed", "Fail");
		}
	}

	public void checkCollectionUnitPage() throws Exception {

		if (isDisplayed(newCollectionUnit)) {
			writeTestResults("Verify that collection unit page is loading", "Collection unit page should be loaded",
					"Collection unit page is loaded", "Pass");
		} else {
			writeTestResults("Verify that collection unit page is loading", "Collection unit page should be loaded",
					"Collection unit page is not loaded", "Fail");
		}
	}

	public void collectionUnitDraftNew() throws Exception {

		click(newCollectionUnit);
		Thread.sleep(2000);
		LocalTime myObj = LocalTime.now();

		sendKeys(collectionUnitCode, accGrpVal + myObj);

		sendKeys(collectionUnitName, collectionNameVal);
		sendKeys(commissionRate, commissionRateVal);
		click(employeeSearch);
		Thread.sleep(2000);
		sendKeys(employeeSearchText, employeeVal);
		Thread.sleep(2000);
		pressEnter(employeeSearchText);
		Thread.sleep(2000);
		doubleClick(selectedValEmployee);
		Thread.sleep(2000);
		sendKeys(sharingRate, sharingRateVal);
		click(collectionUnitName);
		String sharing = getAttribute(sharingRate, "value");
		System.out.println(sharing);

		click(draftNew);
		Thread.sleep(3000);

		String sharingAft = getAttribute(sharingRate, "value");
		System.out.println(sharingAft);

		if (sharingAft.equals("0.00")) {
			writeTestResults("Verify the draft and new", "", "Draft and new", "Pass");
		} else {
			writeTestResults("Verify the draft and new", "", "Draft and new", "Fail");
		}

	}

	public void collectionUnitDraft() throws Exception {

		click(newCollectionUnit);
		Thread.sleep(2000);
		LocalTime myObj = LocalTime.now();

		sendKeys(collectionUnitCode, accGrpVal + myObj);

		sendKeys(collectionUnitName, collectionNameVal);
		sendKeys(commissionRate, commissionRateVal);
		click(employeeSearch);
		Thread.sleep(2000);
		sendKeys(employeeSearchText, employeeVal);
		Thread.sleep(2000);
		pressEnter(employeeSearchText);
		Thread.sleep(2000);
		doubleClick(selectedValEmployee);
		Thread.sleep(2000);
		sendKeys(sharingRate, sharingRateVal);
		click(collectionUnitName);

		click(draft);
		Thread.sleep(3000);

		String header = getText(releaseInHeader);

		if (header.equals("Draft")) {
			writeTestResults("User should be able to draft the account information",
					"Collection unit should be drafted", "Account information not drafted", "Fail");
		} else {
			writeTestResults("User should be able to draft the account information",
					"Collection unit should be drafted", "Account information not drafted", "Fail");
		}
	}

	public void collectionUnitEdit() throws Exception {

		click(newCollectionUnit);
		Thread.sleep(2000);
		LocalTime myObj = LocalTime.now();

		sendKeys(collectionUnitCode, accGrpVal + myObj);

		sendKeys(collectionUnitName, collectionNameVal);
		sendKeys(commissionRate, commissionRateVal);
		click(employeeSearch);
		Thread.sleep(2000);
		sendKeys(employeeSearchText, employeeVal);
		Thread.sleep(2000);
		pressEnter(employeeSearchText);
		Thread.sleep(2000);
		doubleClick(selectedValEmployee);
		Thread.sleep(2000);
		sendKeys(sharingRate, sharingRateVal);
		click(collectionUnitName);

		click(draft);
		Thread.sleep(3000);

		if (isDisplayed(edit)) {
			writeTestResults("Verify the edit button", "Edit button should be displayed", "Edit button displayed",
					"Pass");
			click(edit);

		} else {
			writeTestResults("Verify the edit button", "Edit button should be displayed", "Edit button displayed",
					"Fail");
		}

		if (isDisplayed(update)) {
			writeTestResults("Verify user is able to go to edit page", "Edit page should be load",
					"Edit page displayed", "Pass");

		} else {
			writeTestResults("Verify user is able to go to edit pagee", "Edit page should be load",
					"Edit page displayed", "Fail");
		}
	}

	public void collectionUnitUpdate() throws Exception {

		click(newCollectionUnit);
		Thread.sleep(2000);
		LocalTime myObj = LocalTime.now();

		sendKeys(collectionUnitCode, accGrpVal + myObj);

		sendKeys(collectionUnitName, collectionNameVal);
		sendKeys(commissionRate, commissionRateVal);
		click(employeeSearch);
		Thread.sleep(2000);
		sendKeys(employeeSearchText, employeeVal);
		Thread.sleep(2000);
		pressEnter(employeeSearchText);
		Thread.sleep(2000);
		doubleClick(selectedValEmployee);
		Thread.sleep(2000);
		sendKeys(sharingRate, sharingRateVal);
		click(collectionUnitName);

		String val = getAttribute(commissionRate, "value");
		System.out.println(val);

		click(draft);
		Thread.sleep(3000);

		if (isDisplayed(edit)) {
			writeTestResults("Verify the edit button", "Edit button should be displayed", "Edit button displayed",
					"Pass");
			click(edit);
			Thread.sleep(3000);

		} else {
			writeTestResults("Verify the edit button", "Edit button should be displayed", "Edit button displayed",
					"Fail");
		}

		if (isDisplayed(update)) {
			writeTestResults("Verify user is able to go to edit page", "Edit page should be load",
					"Edit page displayed", "Pass");

		} else {
			writeTestResults("Verify user is able to go to edit pagee", "Edit page should be load",
					"Edit page displayed", "Fail");
		}

		sendKeys(commissionRate, commisionRateEditedVal);
		String valAft = getAttribute(commissionRate, "value");
		System.out.println(valAft);

		click(update);
		Thread.sleep(3000);

		if (valAft.equals("45")) {
			writeTestResults("Verify the update", "Values should be update", "Value updated", "Pass");
		} else {
			writeTestResults("Verify the update", "Values should be update", "Value Updated", "Fail");
		}

	}

	public void collectionUnitRelease() throws Exception {

		click(newCollectionUnit);
		Thread.sleep(2000);
		LocalTime myObj = LocalTime.now();

		sendKeys(collectionUnitCode, accGrpVal + myObj);

		sendKeys(collectionUnitName, collectionNameVal);
		sendKeys(commissionRate, commissionRateVal);
		click(employeeSearch);
		Thread.sleep(2000);
		sendKeys(employeeSearchText, employeeVal);
		Thread.sleep(2000);
		pressEnter(employeeSearchText);
		Thread.sleep(2000);
		doubleClick(selectedValEmployee);
		Thread.sleep(2000);
		sendKeys(sharingRate, sharingRateVal);
		click(collectionUnitName);

		String val = getAttribute(commissionRate, "value");
		System.out.println(val);

		click(draft);
		Thread.sleep(3000);

		if (isDisplayed(edit)) {
			writeTestResults("Verify the edit button", "Edit button should be displayed", "Edit button displayed",
					"Pass");
			click(edit);
			Thread.sleep(3000);

		} else {
			writeTestResults("Verify the edit button", "Edit button should be displayed", "Edit button displayed",
					"Fail");
		}

		if (isDisplayed(update)) {
			writeTestResults("Verify user is able to go to edit page", "Edit page should be load",
					"Edit page displayed", "Pass");

		} else {
			writeTestResults("Verify user is able to go to edit pagee", "Edit page should be load",
					"Edit page displayed", "Fail");
		}

		sendKeys(commissionRate, commisionRateEditedVal);
		String valAft = getAttribute(commissionRate, "value");
		System.out.println(valAft);

		click(update);
		Thread.sleep(3000);

		if (valAft.equals("45")) {
			writeTestResults("Verify the update", "Values should be update", "Value updated", "Pass");
		} else {
			writeTestResults("Verify the update", "Values should be update", "Value Updated", "Fail");
		}

		click(release);
		Thread.sleep(3000);
	}

	public void collectionUnitHistory() throws Exception {

		click(actionBtn);
		Thread.sleep(2000);
		click(history);
		Thread.sleep(1000);

		if (isElementPresent("//td[contains(text(),'Release')]")) {

			writeTestResults("Verify the history", "", "History loaded", "Pass");
		} else {
			writeTestResults("Verify the history", "", "History loaded", "Fail");
		}

	}

	public void collectionUnitActivities() throws Exception {

		click(actionBtn);
		Thread.sleep(2000);
		click(activities);
		Thread.sleep(1000);

		if (isElementPresent("//span[@class='headertext']")) {

			writeTestResults("Verify the activities", "", "Activities loaded", "Pass");
		} else {
			writeTestResults("Verify the activities", "", "Activities loaded", "Fail");
		}
	}

	public void salesOrderActionVerification() throws Exception {

		click(customerAccountSearch);
		Thread.sleep(2000);
		sendKeys(customerAccountText, customerAccVal);
		Thread.sleep(3000);
		pressEnter(customerAccountText);
		Thread.sleep(2000);
		doubleClick(selected_val);
		/*
		 * click(currencyDropdown); billing address and shipping address
		 * click(billingAddressSearch); click(billingAddressText);
		 * sendKeys(billingAddressText, billingAddrVal); click(shippingAddressText);
		 * sendKeys(shippingAddressText, shippingAddrVal); click(apply_btn);
		 */
		Thread.sleep(2000);
		click(salesUnit);
		selectIndex(salesUnit, 1);

		// 1 Service Product
		click(productLookup);
		Thread.sleep(2000);
		InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();
		sendKeys(productTextSer, obj1.readTestCreation("Service"));
		Thread.sleep(2000);
		pressEnter(productTextSer);
		Thread.sleep(3000);
		doubleClick(invSelectedVal);
		Thread.sleep(3000);
//		if (isEnabled(wareHouse)) {
//			writeTestResults("Verify that the warehouse field is disabled for service product",
//					"Warehouse field should be disabled", "Warehouse field is enabled", "Fail");
//		} else {
//			writeTestResults("Verify that the warehouse field is disabled for service product",
//					"Warehouse field should be disabled", "Warehouse field is disabled", "Pass");
//		}

		sendKeys(unitPriceTxtService, unitPriceVal);
		Thread.sleep(1000);
		sendKeys(qtyTextService, quantityVal);
		Thread.sleep(1000);

		// 2 Serial Specific
		click(addNewRecordSerialSpecific);
		Thread.sleep(2000);
		click(productLookupSerialSpec);
		Thread.sleep(3000);
		InventoryAndWarehouseModule obj2 = new InventoryAndWarehouseModule();
		sendKeys(productTextSerialSpec, obj2.readTestCreation("SerielSpecific"));
		Thread.sleep(3000);
		pressEnter(productTextSerialSpec);
		Thread.sleep(3000);
		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		selectText(wareHouseSerialSpecific, warehouseVal1);
		Thread.sleep(2000);

		if (isEnabled(unitPriceSerialSpec)) {
			writeTestResults("User should be able to add unit price", "Unit price field should be enabled",
					"Unit price field is enabled", "Pass");
		} else {
			writeTestResults("User should be able to add unit price", "Unit price field should be enabled",
					"Unit price field is disabled", "Fail");
		}

		sendKeys(unitPriceSerialSpec, unitPriceValSerialSpec);
		Thread.sleep(2000);
		sendKeys(qtyTextSerialSpec, quantityValSerialSpec);
		Thread.sleep(1000);

		click(checkoutInvoice);

		// checkCalculationsSalesOrder1Journey();
		Thread.sleep(2000);
		click(draft);
		Thread.sleep(4000);
		String header = getText(releaseInHeader);

		trackCode = getText(documentVal);

		if (header.equals("(Draft)")) {
			writeTestResults("User should be able to draft the sales order", "Sales order should be drafted",
					"Sales order drafted", "Pass");

		} else {
			writeTestResults("User should be able to draft the sales order", "Sales order should be drafted",
					"Sales order not drafted", "Fail");
		}
//
//		Thread.sleep(2000);
//		click(menu);
//		Thread.sleep(3000);
//		click(serialWise);
//		Thread.sleep(7000);
//		serialNoVal = getText(serialTextVal);
//		Thread.sleep(2000);
//		click(serialMenuClose);
//		Thread.sleep(3000);
//		click(serialBatch);
//		Thread.sleep(4000);
//		click(productListBtn);
//		Thread.sleep(2000);
//		sendKeys(serialText, serialNoVal);
//		Thread.sleep(2000);
//		pressEnter(serialText);
//		Thread.sleep(2000);
//		click(updateBtn);
//		Thread.sleep(2000);
//		click(backBtn);
//		Thread.sleep(2000);
//		click(release);
//		Thread.sleep(6000);

//		if (isDisplayed(outboundLink)) {
//			writeTestResults("User should be able to release the sales order", "Sales order should be released",
//					"Sales order is released", "Pass");
//			click(outboundLink);
//			Thread.sleep(3000);
//		} else {
//			writeTestResults("User should be able to release the sales order", "Sales order should be released",
//					"Sales order is not released", "Fail");
//		}

	}

	public void salesOrderActionVerificationDraftNew() throws Exception {

		click(customerAccountSearch);
		Thread.sleep(2000);
		sendKeys(customerAccountText, customerAccVal);
		Thread.sleep(3000);
		pressEnter(customerAccountText);
		Thread.sleep(2000);
		doubleClick(selected_val);
		/*
		 * click(currencyDropdown); billing address and shipping address
		 * click(billingAddressSearch); click(billingAddressText);
		 * sendKeys(billingAddressText, billingAddrVal); click(shippingAddressText);
		 * sendKeys(shippingAddressText, shippingAddrVal); click(apply_btn);
		 */
		Thread.sleep(2000);
		click(salesUnit);
		selectIndex(salesUnit, 1);

		// 1 Service Product
		click(productLookup);
		Thread.sleep(2000);
		InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();
		sendKeys(productTextSer, obj1.readTestCreation("Service"));
		Thread.sleep(2000);
		pressEnter(productTextSer);
		Thread.sleep(3000);
		doubleClick(invSelectedVal);
		Thread.sleep(3000);
//		if (isEnabled(wareHouse)) {
//			writeTestResults("Verify that the warehouse field is disabled for service product",
//					"Warehouse field should be disabled", "Warehouse field is enabled", "Fail");
//		} else {
//			writeTestResults("Verify that the warehouse field is disabled for service product",
//					"Warehouse field should be disabled", "Warehouse field is disabled", "Pass");
//		}

		sendKeys(unitPriceTxtService, unitPriceVal);
		Thread.sleep(1000);
		sendKeys(qtyTextService, quantityVal);
		Thread.sleep(1000);

		// 2 Serial Specific
		click(addNewRecordSerialSpecific);
		Thread.sleep(2000);
		click(productLookupSerialSpec);
		Thread.sleep(3000);
		InventoryAndWarehouseModule obj2 = new InventoryAndWarehouseModule();
		sendKeys(productTextSerialSpec, obj2.readTestCreation("SerielSpecific"));
		Thread.sleep(3000);
		pressEnter(productTextSerialSpec);
		Thread.sleep(3000);
		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		selectText(wareHouseSerialSpecific, warehouseVal1);
		Thread.sleep(2000);

		if (isEnabled(unitPriceSerialSpec)) {
			writeTestResults("User should be able to add unit price", "Unit price field should be enabled",
					"Unit price field is enabled", "Pass");
		} else {
			writeTestResults("User should be able to add unit price", "Unit price field should be enabled",
					"Unit price field is disabled", "Fail");
		}

		sendKeys(unitPriceSerialSpec, unitPriceValSerialSpec);
		Thread.sleep(2000);
		sendKeys(qtyTextSerialSpec, quantityValSerialSpec);
		Thread.sleep(1000);

		click(checkoutInvoice);

		// String qty = getText("//input[@class='el-resize17']");
		String qty1 = getAttribute("//input[@class='el-resize17']", "value");
		System.out.println(qty1);
		Thread.sleep(2000);
		click(draftNew);
		Thread.sleep(3000);
		String qtyAft = getAttribute("//input[@class='el-resize17']", "value");
		System.out.println(qtyAft);

		if (qtyAft.equals("0.00")) {
			writeTestResults("Verify the draft and new", "Draft and new should ", "Draft and new", "Pass");
		} else {
			writeTestResults("Verify the draft and new", "Draft and new should ", "Draft and new", "Fail");
		}
	}

	public void salesOrderActionVerificationRelease() throws Exception {

		click(customerAccountSearch);
		Thread.sleep(2000);
		sendKeys(customerAccountText, customerAccVal);
		Thread.sleep(3000);
		pressEnter(customerAccountText);
		Thread.sleep(2000);
		doubleClick(selected_val);
		Thread.sleep(2000);
		click(salesUnit);
		selectIndex(salesUnit, 1);
		// 1 Service Product
		click(productLookup);
		Thread.sleep(2000);
		InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();
		sendKeys(productTextSer, obj1.readTestCreation("Service"));
		Thread.sleep(2000);
		pressEnter(productTextSer);
		Thread.sleep(3000);
		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		sendKeys(unitPriceTxtService, unitPriceVal);
		Thread.sleep(1000);
		sendKeys(qtyTextService, quantityVal);
		Thread.sleep(1000);
		// 2 Serial Specific
		click(addNewRecordSerialSpecific);
		Thread.sleep(2000);
		click(productLookupSerialSpec);
		Thread.sleep(3000);
		InventoryAndWarehouseModule obj2 = new InventoryAndWarehouseModule();
		sendKeys(productTextSerialSpec, obj2.readTestCreation("SerielSpecific"));
		Thread.sleep(3000);
		pressEnter(productTextSerialSpec);
		Thread.sleep(3000);
		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		selectText(wareHouseSerialSpecific, warehouseVal1);
		Thread.sleep(2000);

		if (isEnabled(unitPriceSerialSpec)) {
			writeTestResults("User should be able to add unit price", "Unit price field should be enabled",
					"Unit price field is enabled", "Pass");
		} else {
			writeTestResults("User should be able to add unit price", "Unit price field should be enabled",
					"Unit price field is disabled", "Fail");
		}

		sendKeys(unitPriceSerialSpec, unitPriceValSerialSpec);
		Thread.sleep(2000);
		sendKeys(qtyTextSerialSpec, quantityValSerialSpec);
		Thread.sleep(1000);
		click(checkoutInvoice);
		Thread.sleep(2000);
		click(draft);
		Thread.sleep(3500);
		String header = getText(releaseInHeader);
		trackCode = getText(documentVal);

		if (header.equals("(Draft)")) {
			writeTestResults("User should be able to draft the sales order", "Sales order should be drafted",
					"Sales order drafted", "Pass");

		} else {
			writeTestResults("User should be able to draft the sales order", "Sales order should be drafted",
					"Sales order not drafted", "Fail");
		}

		click(release);
		Thread.sleep(4000);
		click(serialMenuClose);
		Thread.sleep(2000);

		if (header.equals("(Released)")) {
			writeTestResults("User should be able to release the sales order", "Sales order should be release",
					"Sales order released", "Pass");
		} else {
			writeTestResults("User should be able to release the sales order", "Sales order should be release",
					"Sales order not released", "Fail");
		}
	}

	public void salesOrderActionVerificationEdit() throws Exception {

		click(customerAccountSearch);
		Thread.sleep(2000);
		sendKeys(customerAccountText, customerAccVal);
		Thread.sleep(3000);
		pressEnter(customerAccountText);
		Thread.sleep(2000);
		doubleClick(selected_val);
		Thread.sleep(2000);
		click(salesUnit);
		selectIndex(salesUnit, 1);
		// 1 Service Product
		click(productLookup);
		Thread.sleep(2000);
		InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();
		sendKeys(productTextSer, obj1.readTestCreation("Service"));
		Thread.sleep(2000);
		pressEnter(productTextSer);
		Thread.sleep(3000);
		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		sendKeys(unitPriceTxtService, unitPriceVal);
		Thread.sleep(1000);
		sendKeys(qtyTextService, quantityVal);
		Thread.sleep(1000);
		// 2 Serial Specific
		click(addNewRecordSerialSpecific);
		Thread.sleep(2000);
		click(productLookupSerialSpec);
		Thread.sleep(3000);
		InventoryAndWarehouseModule obj2 = new InventoryAndWarehouseModule();
		sendKeys(productTextSerialSpec, obj2.readTestCreation("SerielSpecific"));
		Thread.sleep(3000);
		pressEnter(productTextSerialSpec);
		Thread.sleep(3000);
		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		selectText(wareHouseSerialSpecific, warehouseVal1);
		Thread.sleep(2000);

		if (isEnabled(unitPriceSerialSpec)) {
			writeTestResults("User should be able to add unit price", "Unit price field should be enabled",
					"Unit price field is enabled", "Pass");
		} else {
			writeTestResults("User should be able to add unit price", "Unit price field should be enabled",
					"Unit price field is disabled", "Fail");
		}

		sendKeys(unitPriceSerialSpec, unitPriceValSerialSpec);
		Thread.sleep(2000);
		sendKeys(qtyTextSerialSpec, quantityValSerialSpec);
		Thread.sleep(1000);
		click(checkoutInvoice);
		Thread.sleep(2000);
		click(draft);
		Thread.sleep(4000);
		String header = getText(releaseInHeader);

		if (header.equals("(Draft)")) {
			writeTestResults("User should be able to draft the sales order", "Sales order should be drafted",
					"Sales order drafted", "Pass");
		} else {
			writeTestResults("User should be able to draft the sales order", "Sales order should be drafted",
					"Sales order not drafted", "Fail");
		}

		String qty1 = getAttribute("//input[@class='el-resize17']", "value");
		// System.out.println(qty1);
		Thread.sleep(2000);
		click(edit);
		Thread.sleep(3000);
		if (isDisplayed(update)) {
			writeTestResults("", "", "", "Pass");
		} else {
			writeTestResults("", "", "", "Fail");
		}
	}

	public void salesOrderActionVerificationUpdate() throws Exception {

		click(customerAccountSearch);
		Thread.sleep(2000);
		sendKeys(customerAccountText, customerAccVal);
		Thread.sleep(3000);
		pressEnter(customerAccountText);
		Thread.sleep(2000);
		doubleClick(selected_val);
		Thread.sleep(2000);
		click(salesUnit);
		selectIndex(salesUnit, 1);
		// 1 Service Product
		click(productLookup);
		Thread.sleep(2000);
		InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();
		sendKeys(productTextSer, obj1.readTestCreation("Service"));
		Thread.sleep(2000);
		pressEnter(productTextSer);
		Thread.sleep(3000);
		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		sendKeys(unitPriceTxtService, unitPriceVal);
		Thread.sleep(1000);
		sendKeys(qtyTextService, quantityVal);
		Thread.sleep(1000);
		// 2 Serial Specific
		click(addNewRecordSerialSpecific);
		Thread.sleep(2000);
		click(productLookupSerialSpec);
		Thread.sleep(3000);
		InventoryAndWarehouseModule obj2 = new InventoryAndWarehouseModule();
		sendKeys(productTextSerialSpec, obj2.readTestCreation("SerielSpecific"));
		Thread.sleep(3000);
		pressEnter(productTextSerialSpec);
		Thread.sleep(3000);
		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		selectText(wareHouseSerialSpecific, warehouseVal1);
		Thread.sleep(2000);

		if (isEnabled(unitPriceSerialSpec)) {
			writeTestResults("User should be able to add unit price", "Unit price field should be enabled",
					"Unit price field is enabled", "Pass");
		} else {
			writeTestResults("User should be able to add unit price", "Unit price field should be enabled",
					"Unit price field is disabled", "Fail");
		}

		sendKeys(unitPriceSerialSpec, unitPriceValSerialSpec);
		Thread.sleep(2000);
		sendKeys(qtyTextSerialSpec, quantityValSerialSpec);
		Thread.sleep(1000);
		click(checkoutInvoice);
		Thread.sleep(2000);
		click(draft);
		Thread.sleep(3500);
		String header = getText(releaseInHeader);
		trackCode = getText(documentVal);

		if (header.equals("(Draft)")) {
			writeTestResults("User should be able to draft the sales order", "Sales order should be drafted",
					"Sales order drafted", "Pass");

		} else {
			writeTestResults("User should be able to draft the sales order", "Sales order should be drafted",
					"Sales order not drafted", "Fail");
		}

		String qty1 = getAttribute("//input[@class='el-resize17']", "value");
		// System.out.println(qty1);
		Thread.sleep(2000);
		click(edit);
		Thread.sleep(3000);
		sendKeys(qtyTextService, "3");
		click(checkoutInvoice);
		String qtyAft = getAttribute("//input[@class='el-resize17']", "value");
		// System.out.println(qtyAft);

		if (qtyAft.equals("3")) {
			writeTestResults("Verify the edit", "Edit should ", "Edit", "Pass");
		} else {
			writeTestResults("Verify the edit", "Edit and new should ", "Edit", "Fail");
		}
	}

	public void salesOrderActionVerificationUpdateAndNew() throws Exception {

		click(customerAccountSearch);
		Thread.sleep(2000);
		sendKeys(customerAccountText, customerAccVal);
		Thread.sleep(3000);
		pressEnter(customerAccountText);
		Thread.sleep(2000);
		doubleClick(selected_val);
		Thread.sleep(2000);
		click(salesUnit);
		selectIndex(salesUnit, 1);
		// 1 Service Product
		click(productLookup);
		Thread.sleep(2000);
		InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();
		sendKeys(productTextSer, obj1.readTestCreation("Service"));
		Thread.sleep(2000);
		pressEnter(productTextSer);
		Thread.sleep(3000);
		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		sendKeys(unitPriceTxtService, unitPriceVal);
		Thread.sleep(1000);
		sendKeys(qtyTextService, quantityVal);
		Thread.sleep(1000);
		// 2 Serial Specific
		click(addNewRecordSerialSpecific);
		Thread.sleep(2000);
		click(productLookupSerialSpec);
		Thread.sleep(3000);
		InventoryAndWarehouseModule obj2 = new InventoryAndWarehouseModule();
		sendKeys(productTextSerialSpec, obj2.readTestCreation("SerielSpecific"));
		Thread.sleep(3000);
		pressEnter(productTextSerialSpec);
		Thread.sleep(3000);
		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		selectText(wareHouseSerialSpecific, warehouseVal1);
		Thread.sleep(2000);

		if (isEnabled(unitPriceSerialSpec)) {
			writeTestResults("User should be able to add unit price", "Unit price field should be enabled",
					"Unit price field is enabled", "Pass");
		} else {
			writeTestResults("User should be able to add unit price", "Unit price field should be enabled",
					"Unit price field is disabled", "Fail");
		}

		sendKeys(unitPriceSerialSpec, unitPriceValSerialSpec);
		Thread.sleep(2000);
		sendKeys(qtyTextSerialSpec, quantityValSerialSpec);
		Thread.sleep(1000);
		click(checkoutInvoice);
		Thread.sleep(2000);
		click(draft);
		Thread.sleep(4000);
		String header = getText(releaseInHeader);

		if (header.equals("(Draft)")) {
			writeTestResults("User should be able to draft the sales order", "Sales order should be drafted",
					"Sales order drafted", "Pass");

		} else {
			writeTestResults("User should be able to draft the sales order", "Sales order should be drafted",
					"Sales order not drafted", "Fail");
		}

		String qty1 = getAttribute("//input[@class='el-resize17']", "value");
		// System.out.println(qty1);
		Thread.sleep(2000);
		click(edit);
		Thread.sleep(3000);
		sendKeys(qtyTextService, "3");
		click(checkoutInvoice);
		String qtyAft = getAttribute("//input[@class='el-resize17']", "value");
		// System.out.println(qtyAft);

		if (qtyAft.equals("3")) {
			writeTestResults("Verify the edit", "Edit should ", "Edit", "Pass");
		} else {
			writeTestResults("Verify the edit", "Edit and new should ", "Edit", "Fail");
		}
	}

	public void salesOrderActionVerificationPurchaseOrder() throws Exception {

		click(customerAccountSearch);
		Thread.sleep(2000);
		sendKeys(customerAccountText, customerAccVal);
		Thread.sleep(3000);
		pressEnter(customerAccountText);
		Thread.sleep(2000);
		doubleClick(selected_val);
		Thread.sleep(2000);
		click(salesUnit);
		selectIndex(salesUnit, 1);
		// 1 Service Product
		click(productLookup);
		Thread.sleep(2000);
		InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();
		sendKeys(productTextSer, obj1.readTestCreation("Service"));
		Thread.sleep(3000);
		pressEnter(productTextSer);
		Thread.sleep(3500);
		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		sendKeys(unitPriceTxtService, unitPriceVal);
		Thread.sleep(1000);
		sendKeys(qtyTextService, quantityVal);
		Thread.sleep(1000);
		// 2 Serial Specific
		click(addNewRecordSerialSpecific);
		Thread.sleep(2000);
		click(productLookupSerialSpec);
		Thread.sleep(3000);
		InventoryAndWarehouseModule obj2 = new InventoryAndWarehouseModule();
		sendKeys(productTextSerialSpec, obj2.readTestCreation("SerielSpecific"));
		Thread.sleep(3000);
		pressEnter(productTextSerialSpec);
		Thread.sleep(3500);
		doubleClick(invSelectedVal);
		Thread.sleep(3000);
		selectText(wareHouseSerialSpecific, warehouseVal1);
		Thread.sleep(2000);

		if (isEnabled(unitPriceSerialSpec)) {
			writeTestResults("User should be able to add unit price", "Unit price field should be enabled",
					"Unit price field is enabled", "Pass");
		} else {
			writeTestResults("User should be able to add unit price", "Unit price field should be enabled",
					"Unit price field is disabled", "Fail");
		}

		sendKeys(unitPriceSerialSpec, unitPriceValSerialSpec);
		Thread.sleep(2000);
		sendKeys(qtyTextSerialSpec, quantityValSerialSpec);
		Thread.sleep(1000);
		click(checkoutInvoice);
		Thread.sleep(2000);
		click(draft);
		Thread.sleep(5000);
		String header = getText(releaseInHeader);

		trackCode = getText(documentVal);
		salesOrderID = trackCode;

		if (header.equals("(Draft)")) {
			writeTestResults("User should be able to draft the sales order", "Sales order should be drafted",
					"Sales order drafted", "Pass");

		} else {
			writeTestResults("User should be able to draft the sales order", "Sales order should be drafted",
					"Sales order not drafted", "Fail");
		}

		click(release);
		Thread.sleep(5000);
		click(serialMenuClose);
		Thread.sleep(2000);

		String header1 = getText(releaseInHeader);

		if (header1.equals("(Released)")) {
			writeTestResults("User should be able to release the sales order", "Sales order should be release",
					"Sales order released", "Pass");

		} else {
			writeTestResults("User should be able to release the sales order", "Sales order should be release",
					"Sales order not released", "Fail");
		}

		click(actionMenu);
		Thread.sleep(2000);

		if (isDisplayed(convertToPurchaseOrder)) {
			writeTestResults("Verify convert to purchase order is available",
					"Convert to purchase order should available", "Convert to purchase order available", "Pass");
			click(convertToPurchaseOrder);
			Thread.sleep(4000);
		} else {
			writeTestResults("Verify convert to purchase order is available",
					"Convert to purchase order should available", "Convert to purchase order not available", "Fail");
		}

		if (isDisplayed(POJourneyWindow)) {
			writeTestResults("Verify the purchase order journey window", "Journey window should be displayed",
					"Purchase order journeys displayed", "Pass");
		} else {
			writeTestResults("Verify the purchase order journey window", "Journey window should be displayed",
					"Purchase order journeys not displayed", "Fail");
		}

	}

	public void salesOrderActionVerificationReminder() throws Exception {

		Thread.sleep(2000);
		click(journeyWindowClose);
		Thread.sleep(2000);
		click(actionMenu);
		Thread.sleep(2000);
		click(reminders);
		Thread.sleep(3000);

		if (isDisplayed(createReminder)) {
			writeTestResults("Verify reminders", "Create reminder look up should be load",
					"Create reminder look up loaded", "Pass");
		} else {
			writeTestResults("Verify reminders", "Create reminder look up should be load",
					"Create reminder look up not loaded", "Fail");
		}
	}

	public void salesOrderActionVerificationHistory() throws Exception {

		Thread.sleep(2000);
		click(journeyWindowClose);
		Thread.sleep(1000);
		pageRefersh();
		Thread.sleep(2000);
		click(actionMenu);
		Thread.sleep(2000);
		click(history);
		Thread.sleep(6000);

		if (isDisplayed(historyReleased)) {
			writeTestResults("Verify the history", "User should be able to see the history", "History loaded", "Pass");
		} else {
			writeTestResults("Verify the history", "User should be able to see the history", "History not loaded",
					"Fail");
		}
	}

	public void salesOrderActionVerificationDocFlow() throws Exception {

		click(actionMenu);
		Thread.sleep(2000);
		click(documentFlow);
		Thread.sleep(2000);
		JavascriptExecutor js = (JavascriptExecutor) driver;

		String valueSO = (String) js
				.executeScript("return $($(\"#ctrlDocumentFlow-svg\").find(\".pf-content tspan\")[0]).text()");
		System.out.println(valueSO);

		if (valueSO.equals(salesOrderID)) {
			writeTestResults("Verify the doc flow", "Doc flow should display", "Doc flow displayed", "Pass");
		} else {
			writeTestResults("Verify the doc flow", "Doc flow should display", "Doc flow not displayed", "Fail");
		}
	}

	public void salesOrderActionVerificationInboundCustomerMemo() throws Exception {

		click(actionMenu);
		Thread.sleep(2000);
		click(convertInboundCustomerMemo);
		Thread.sleep(3500);
		System.out.println(releaseInHeader);

		if (releaseInHeader.equals("New Customer Debit Memo")) {
			writeTestResults("Verify the inbound customer memo",
					"User should be able to convert sales order to inbound customer memo",
					"Converted to inbound customer memo", "Pass");
		} else {
			writeTestResults("Verify the inbound customer memo",
					"User should be able to convert sales order to inbound customer memo",
					"Not converted to inbound customer memo", "Fail");
		}

		goBack();
		Thread.sleep(2000);
	}

	public void writeTestCreations(String name, String variable, int data_row)
			throws InvalidFormatException, IOException {
		genarateInventoryAndWarehouseInformation obj = new genarateInventoryAndWarehouseInformation();
		String path = obj.getProjectPath("*src*test*java*salesAndMarketingModule*TestStore.xlsx");
		fis = new FileInputStream(path);
		wb = WorkbookFactory.create(fis);
		sh = wb.getSheet("Sheet1");
		/* int noOfRows = sh.getLastRowNum(); */
		/* System.out.println(noOfRows); */
		row = sh.createRow(data_row);
		cell = row.createCell(1);
		cell1 = row.createCell(0);
		cell.setCellValue(variable);
		cell1.setCellValue(name);
		System.out.println(cell.getStringCellValue());
		fos = new FileOutputStream(path);
		wb.write(fos);
		fos.flush();
		fos.close();
		/* System.out.println("Done"); */
	}

	public String readTestCreation(int row1) throws IOException, InvalidFormatException {
		genarateInventoryAndWarehouseInformation obj = new genarateInventoryAndWarehouseInformation();
		String path = obj.getProjectPath("*src*test*java*salesAndMarketingModule*TestStore.xlsx");
		fis = new FileInputStream(path);
		wb = WorkbookFactory.create(fis);
		sh = wb.getSheet("Sheet1");

//		Map<String, String> products_creation_map = new HashMap<String, String>();
//		List<Map<String, String>> products_creation = new ArrayList<Map<String, String>>();

//		for (int i = 0; i < 8;) {
		row = sh.getRow(row1);
		cell = row.getCell(1);
//			cell2 = row.getCell(1);

		String test_creation_row1 = cell.getStringCellValue();
//			String test_creation_row2 = cell2.getStringCellValue();
//			products_creation_map.put(test_creation_row1, test_creation_row2);
//			products_creation.add(i, products_creation_map);
//			i++;
////		}

//		System.out.println(products_creation.get(0).get(product_category));
//		String product_type = products_creation.get(0).get(product_category);

		return test_creation_row1;
	}

	public void AVSalesQuotationDraft() throws Exception {

		selectIndex(accountHead, 0);
		click(customerAccSearchQuat);
		Thread.sleep(2000);
		sendKeys(customerAccountText, customerAccValQuat);
		Thread.sleep(2000);
		pressEnter(customerAccountText);
		Thread.sleep(2500);
		doubleClick(selectedValLead);
		Thread.sleep(2000);
		// billing address and shipping address
		click(billingAddrSearch);
		Thread.sleep(2000);
		sendKeys(billingAddrTxtQuat, billingAddrQuatVal);
		Thread.sleep(2000);
		sendKeys(shippingAddrTxtQuat, shippingAddrQuatVal);
		click(apply_btn);
		Thread.sleep(2000);
		selectIndex(salesUnit, 3);

		// Service Product
		click(productSearchQuatation);
		Thread.sleep(2000);
		InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();
		sendKeys(productTextSer, obj1.readTestCreation("Service"));
		pressEnter(productTextSer);
		Thread.sleep(3500);
		doubleClick(selectedValQuatInv);
		Thread.sleep(2000);
		sendKeys(unitpriceQuatation, unitPriceVal);

		// Serial Specific
		click(addNew);
		click(searchBtnQuata);
		Thread.sleep(3000);
		InventoryAndWarehouseModule obj2 = new InventoryAndWarehouseModule();
		sendKeys(productTextInv, obj2.readTestCreation("SerielSpecific"));
		Thread.sleep(3000);
		pressEnter(productTextInv);
		Thread.sleep(4000);
		doubleClick(selectedValQuatInv);
		Thread.sleep(2000);
		sendKeys(unitpriceQuatationInv, unitPriceQuatVal);
		Thread.sleep(2000);

		unitPriceQuat = getAttribute(unitpriceQuatationInv, "value");

		click(checkoutQuatation);
		Thread.sleep(2000);
		click(draftQuatation);
		Thread.sleep(6000);

		String header3 = getText(releaseInHeader);

		trackCode = getText(documentVal);
		System.out.println(trackCode);

		if (header3.equals("(Draft)")) {
			writeTestResults("User should be able to draft the sales quotation", "Sales quotation should be draft",
					"Sales quotation draft", "Pass");
		} else {
			writeTestResults("User should be able to draft the sales quotation", "Sales quotation should be draft",
					"Sales quotation not draft", "Fail");
		}
	}

	public void AVSalesQuotationEdit() throws Exception {

		click(edit);
		Thread.sleep(3000);

		if (isDisplayed(update)) {

			writeTestResults("Verify the edit of sales quotation", "User should be able to edit quotation", "Edit done",
					"Pass");
		} else {
			writeTestResults("Verify the edit of sales quotation", "User should be able to edit quotation",
					"Edit not done", "Fail");
		}
	}

	public void AVSalesQuotationUpdate() throws Exception {

		sendKeys(unitPriceTextQuat, unitPriceQuatValEdit);
		click(checkoutInvoice);
		Thread.sleep(3000);
		click(update);
		Thread.sleep(3000);

		unitPriceQuatAft = getText(unitPriceAftTextQuat);

		System.out.println(unitPriceQuat);
		System.out.println(unitPriceQuatAft);

		if (isDisplayed(update)) {

			writeTestResults("Verify the update of sales quotation", "User should be able to edit quotation", "",
					"Pass");
		} else {
			writeTestResults("Verify the update of sales quotation", "User should be able to edit quotation", "",
					"Fail");
		}

		if (unitPriceQuatAft.equals("4,000.00000")) {

			writeTestResults("Verift the updating", "Update should be done", "Update done", "Pass");
		} else {
			writeTestResults("Verift the updating", "Update should be done", "Update not done", "Fail");
		}
	}

	public void AVSalesQuotationRelease() throws Exception {

		click(release);
		Thread.sleep(4000);

		String header3 = getText(releaseInHeader);

		Quat = getText(documentVal);
		trackCode = getText(documentVal);
		// System.out.println(trackCode);

		if (header3.equals("(Released)")) {
			writeTestResults("User should be able to release the sales quotation", "Sales quotation should be released",
					"Sales quotation released", "Pass");
		} else {
			writeTestResults("User should be able to release the sales quotation", "Sales quotation should be released",
					"Sales quotation not released", "Fail");
		}
	}

	public void AVSalesQuotationReminder() throws Exception {

		click(actionMenu);
		Thread.sleep(2000);
		click(reminders);
		Thread.sleep(3000);

		if (isDisplayed(createReminder)) {
			writeTestResults("Verify the reminders of sales quotation", "Reminders should be displayed",
					"Reminders displayed for sales quotation", "Pass");
		} else {
			writeTestResults("Verify the reminders of sales quotation", "Reminders should be displayed",
					"Reminders displayed for sales quotation", "Fail");
		}

		click(serialMenuClose);
		Thread.sleep(1000);
	}

	public void AVSalesQuotationHistory() throws Exception {

		click(actionMenu);
		Thread.sleep(2000);
		click(history);
		Thread.sleep(3000);

		if (isElementPresent(historyReleased)) {
			writeTestResults("Verify the histroy of sales quotation", "History should be displayed",
					"History displayed for sales quotation", "Pass");
		} else {
			writeTestResults("Verify the histroy of sales quotation", "History should be displayed",
					"History displayed for sales quotation", "Fail");
		}
	}

	public void AVSalesQuotationDocFlow() throws Exception {

		click(actionMenu);
		Thread.sleep(2000);
		click(documentFlow);
		Thread.sleep(3000);

		JavascriptExecutor js = (JavascriptExecutor) driver;
		String valueSO = (String) js
				.executeScript("return $($(\"#ctrlDocumentFlow-svg\").find(\".pf-content tspan\")[0]).text()");
		System.out.println(valueSO);

		if (valueSO.equals(Quat)) {
			writeTestResults("Verify the document flow of sales quotation", "Document flow should be displayed",
					"Document flow displayed for sales quotation", "Pass");
		} else {
			writeTestResults("Verify the document flow of sales quotation", "Document flow should be displayed",
					"Document flow not displayed for sales quotation", "Fail");
		}
	}

	public void AVSalesQuotationActivities() throws Exception {

		click(actionMenu);
		Thread.sleep(2000);
		click(activities);
		Thread.sleep(3000);

		if (isDisplayed(activityHeader)) {
			writeTestResults("Verify the activities of sales quotation", "Activities should load for sales qoutation",
					"Activities loaded for sales quotation", "Pass");
		} else {
			writeTestResults("Verify the activities of sales quotation", "Activities should load for sales qoutation",
					"Activities loaded for sales quotation", "Fail");
		}

		click(activityMenuClose);
		Thread.sleep(1500);
	}

	public void AVSalesQuotationConvertToAccount() throws Exception {

		click(actionMenu);
		Thread.sleep(2000);
		click(convertToAccount);
		Thread.sleep(3000);

		if (isDisplayed(activityHeader)) {
			writeTestResults("Verify the activities of sales quotation", "Activities should load for sales qoutation",
					"Activities loaded for sales quotation", "Pass");
		} else {
			writeTestResults("Verify the activities of sales quotation", "Activities should load for sales qoutation",
					"Activities loaded for sales quotation", "Fail");
		}

		click(activityMenuClose);
		Thread.sleep(1500);
	}

	public void AVSalesQuotationCapacityPlanning() throws Exception {

		click(actionMenu);
		Thread.sleep(2000);
		click(capacityPlanning);
		Thread.sleep(3500);

		if (isDisplayed(capacityPlanningHeader)) {
			writeTestResults("Verify the capacity planning of sales quotation",
					"Capacity planning should load for sales quotation", "Capacity planning loaded for sales quotation",
					"Pass");
		} else {
			writeTestResults("Verify the capacity planning of sales quotation",
					"Capacity planning should load for sales quotation", "Capacity planning loaded for sales quotation",
					"Fail");
		}

		// mouseMove(serialMenuClose);
		click(capacityPlanningClose);
		Thread.sleep(3000);
	}

	public void AVSalesQuotationConfirm() throws Exception {

		selectIndex(accountHead, 0);
		click(customerAccSearchQuat);
		Thread.sleep(2000);
		sendKeys(customerAccountText, customerAccValQuat);
		Thread.sleep(2000);
		pressEnter(customerAccountText);
		Thread.sleep(2500);
		doubleClick(selectedValLead);
		Thread.sleep(2000);
		// billing address and shipping address
		click(billingAddrSearch);
		Thread.sleep(2000);
		sendKeys(billingAddrTxtQuat, billingAddrQuatVal);
		Thread.sleep(2000);
		sendKeys(shippingAddrTxtQuat, shippingAddrQuatVal);
		click(apply_btn);
		Thread.sleep(2000);
		selectIndex(salesUnit, 3);

		// Service Product
		click(productSearchQuatation);
		Thread.sleep(2000);
		InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();
		sendKeys(productTextSer, obj1.readTestCreation("Service"));
		pressEnter(productTextSer);
		Thread.sleep(3500);
		doubleClick(selectedValQuatInv);
		Thread.sleep(2000);
		sendKeys(unitpriceQuatation, unitPriceVal);

		// Serial Specific

		click(addNew);
		click(searchBtnQuata);
		Thread.sleep(3000);
		InventoryAndWarehouseModule obj2 = new InventoryAndWarehouseModule();
		sendKeys(productTextInv, obj2.readTestCreation("SerielSpecific"));
		Thread.sleep(3000);
		pressEnter(productTextInv);
		Thread.sleep(3500);
		doubleClick(selectedValQuatInv);
		Thread.sleep(2000);
		sendKeys(unitpriceQuatationInv, unitPriceQuatVal);
		Thread.sleep(2000);

		click(checkoutQuatation);
		Thread.sleep(2000);
		click(draftQuatation);
		Thread.sleep(6000);

		String header3 = getText(releaseInHeader);

		trackCode = getText(documentVal);
		System.out.println(trackCode);

		if (header3.equals("(Draft)")) {
			writeTestResults("User should be able to draft the sales quotation", "Sales quotation should be draft",
					"Sales quotation draft", "Pass");
		} else {
			writeTestResults("User should be able to draft the sales quotation", "Sales quotation should be draft",
					"Sales quotation not draft", "Fail");
		}

		click(releaseQuatation);
		Thread.sleep(6000);
		click(versionBtn);
		Thread.sleep(3000);
		// mouseMove(versionTab);
		mouseMove("//div[@class='color-selected']");

		mouseMove(confirmVersion);
		Thread.sleep(2000);

		if (isDisplayed("//span[@title='Confirm this version']")) {
			writeTestResults("User should be able to release the sales quotation", "Sales quotation should be released",
					"Sales quotation released", "Pass");
		} else {
			writeTestResults("User should be able to release the sales quotation", "Sales quotation should be released",
					"Sales quotation not released", "Fail");
		}

		click(confirmVersion);
		Thread.sleep(3000);
		click(tickConfirmVersion);
		Thread.sleep(2000);
		click(confirmBtn);
		Thread.sleep(3000);
		click(linkMenuClose);
		Thread.sleep(2000);

		String header4 = getText(releaseInHeader);
		trackCode = getText(documentVal);

		if (header4.equals("(Confirmed)")) {
			writeTestResults("User should be able to confirm the sales quotation",
					"Sales quotation should be confirmed", "Sales quotation confirmed", "Pass");
		} else {
			writeTestResults("User should be able to confirm the sales quotation",
					"Sales quotation should be confirmed", "Sales quotation not confirmed", "Fail");
		}
	}

	public void AVSalesQuotationDuplicate() throws Exception {

		click(duplicate);
		Thread.sleep(3000);
		trackCode = getText(documentVal);

		if (trackCode.equals("New")) {
			writeTestResults("Verify duplicate of sales quotation", "User should be able to duplicate the qoutation",
					"Sales qoutation duplicated", "Pass");
		} else {
			writeTestResults("Verify duplicate of sales quotation", "User should be able to duplicate the qoutation",
					"Sales qoutation not duplicated", "Fail");
		}

		qtyValDuplicate = getAttribute(unitpriceQuatation, "value");

	}

	public void AVSalesQuotationDraftNew() throws Exception {

		click(checkoutInvoice);
		Thread.sleep(2000);
		click(draftNew);
		Thread.sleep(4000);

		String qtyValAft = getAttribute(unitpriceQuatation, "value");
		System.out.println(qtyValDuplicate);
		System.out.println(qtyValAft);

		if (qtyValAft.equals("0.00000")) {

			writeTestResults("Verify draft and new", "User should be able to do draft and new", "Draft and new done",
					"Pass");
		} else {
			writeTestResults("Verify draft and new", "User should be able to do draft and new",
					"Draft and new not done", "Fail");
		}
	}

	public void AVSalesQuotationNewVersion() throws Exception {

		selectIndex(accountHead, 0);
		click(customerAccSearchQuat);
		Thread.sleep(2000);
		sendKeys(customerAccountText, customerAccValQuat);
		Thread.sleep(2000);
		pressEnter(customerAccountText);
		Thread.sleep(2500);
		doubleClick(selectedValLead);
		Thread.sleep(2000);
		// billing address and shipping address
		click(billingAddrSearch);
		Thread.sleep(2000);
		sendKeys(billingAddrTxtQuat, billingAddrQuatVal);
		Thread.sleep(2000);
		sendKeys(shippingAddrTxtQuat, shippingAddrQuatVal);
		click(apply_btn);
		Thread.sleep(2000);
		selectIndex(salesUnit, 3);

		// Service Product
		click(productSearchQuatation);
		Thread.sleep(2000);
		InventoryAndWarehouseModule obj1 = new InventoryAndWarehouseModule();
		sendKeys(productTextSer, obj1.readTestCreation("Service"));
		pressEnter(productTextSer);
		Thread.sleep(3500);
		doubleClick(selectedValQuatInv);
		Thread.sleep(2000);
		sendKeys(unitpriceQuatation, unitPriceVal);

		// Serial Specific

		click(addNew);
		click(searchBtnQuata);
		Thread.sleep(3000);
		InventoryAndWarehouseModule obj2 = new InventoryAndWarehouseModule();
		sendKeys(productTextInv, obj2.readTestCreation("SerielSpecific"));
		Thread.sleep(3000);
		pressEnter(productTextInv);
		Thread.sleep(3500);
		doubleClick(selectedValQuatInv);
		Thread.sleep(2000);
		sendKeys(unitpriceQuatationInv, unitPriceQuatVal);
		Thread.sleep(2000);

		click(checkoutQuatation);
		Thread.sleep(2000);
		click(draftQuatation);
		Thread.sleep(6000);

		String header3 = getText(releaseInHeader);

		trackCode = getText(documentVal);
		System.out.println(trackCode);

		if (header3.equals("(Draft)")) {
			writeTestResults("User should be able to draft the sales quotation", "Sales quotation should be draft",
					"Sales quotation draft", "Pass");
		} else {
			writeTestResults("User should be able to draft the sales quotation", "Sales quotation should be draft",
					"Sales quotation not draft", "Fail");
		}

		click(releaseQuatation);
		Thread.sleep(6000);
		click(versionBtn);
		Thread.sleep(3000);
		// mouseMove(versionTab);
		mouseMove("//div[@class='color-selected']");

		mouseMove(confirmVersion);
		Thread.sleep(2000);

		if (isDisplayed("//span[@title='Confirm this version']")) {
			writeTestResults("User should be able to release the sales quotation", "Sales quotation should be released",
					"Sales quotation released", "Pass");
		} else {
			writeTestResults("User should be able to release the sales quotation", "Sales quotation should be released",
					"Sales quotation not released", "Fail");
		}

		click(confirmVersion);
		Thread.sleep(3000);
		click(generateNewVersion);
		Thread.sleep(2000);
		click(confirmBtn);
		Thread.sleep(2000);

		if (qoutationVersion.equals("2")) {
			writeTestResults("Verify the new version of sales quotation", "User should be able to generate new version",
					"New version generated", "Pass");
		} else {
			writeTestResults("Verify the new version of sales quotation", "User should be able to generate new version",
					"New version not generated", "Fail");
		}
	}

	public void AVSalesReturnInvoiceReminders() throws Exception {

		click(valReturnInvoice);
		Thread.sleep(3000);

		click(actionMenu);
		Thread.sleep(2000);
		click(reminders);
		Thread.sleep(2000);

		if (isDisplayed(createReminder)) {
			writeTestResults("Verify the reminders of sales return invoice", "User should be able to create reminders",
					"Reminders loaded", "Pass");
		} else {
			writeTestResults("Verify the reminders of sales return invoice", "User should be able to create reminders",
					"Reminders not loaded", "Fail");
		}

		click(serialMenuClose);
		Thread.sleep(2000);
	}

	public void AVSalesReturnInvoiceHistory() throws Exception {

		click(actionMenu);
		Thread.sleep(2000);
		click(history);
		Thread.sleep(3000);

		if (isElementPresent(historyReleased)) {
			writeTestResults("Verify the histroy of sales return invoice", "History should be displayed",
					"History displayed for sales return invoice", "Pass");
		} else {
			writeTestResults("Verify the histroy of sales quotation", "History should be displayed",
					"History displayed for sales return invoice", "Fail");
		}
	}

	public void AVSalesReturnInvoiceDocFlow() throws Exception {

		click(actionMenu);
		Thread.sleep(2000);
		click(documentFlow);
		Thread.sleep(3000);

		JavascriptExecutor js = (JavascriptExecutor) driver;
		String valueSO = (String) js
				.executeScript("return $($(\"#ctrlDocumentFlow-svg\").find(\".pf-content tspan\")[0]).text()");
		System.out.println(valueSO);

		if (isElementPresent(valueSO)) {
			writeTestResults("Verify the document flow of sales return invoice", "Document flow should be displayed",
					"Document flow displayed for sales return invoice", "Pass");
		} else {
			writeTestResults("Verify the document flow of sales return invoice", "Document flow should be displayed",
					"Document flow not displayed for sales return invoice", "Fail");
		}
	}

	public void AVSalesReturnInvoiceActivities() throws Exception {

		click(actionMenu);
		Thread.sleep(2000);
		click(activities);
		Thread.sleep(3000);

		if (isDisplayed(activityHeader)) {
			writeTestResults("Verify the activities of sales return invoice",
					"Activities should load for sales return invoice", "Activities loaded for sales return invoice",
					"Pass");
		} else {
			writeTestResults("Verify the activities of sales return invoice",
					"Activities should load for sales return invoice", "Activities loaded for sales return invoice",
					"Fail");
		}

		click(activityMenuClose);
		Thread.sleep(1500);
	}

	public void AVSalesReturnInvoiceHold() throws Exception {

		click(actionMenu);
		Thread.sleep(2000);
		click(hold);
		Thread.sleep(2000);

		if (isElementPresent(newVehicleInformationHeader)) {
			writeTestResults("Verify the hold for sales return invoice",
					"Hold should applicable for sales return invoice", "Sales return invoice is in hold", "Pass");
		} else {
			writeTestResults("Verify the hold for sales return invoice",
					"Hold should applicable for sales return invoice", "Sales return invoice is not in hold", "Fail");
		}

		click(serialMenuClose);
		Thread.sleep(1500);
	}

	public void AVSalesReturnInvoice() throws Exception {

		click(actionMenu);
		Thread.sleep(2000);
		click(hold);
		Thread.sleep(2000);

		if (isElementPresent(newVehicleInformationHeader)) {
			writeTestResults("Verify the hold for sales return invoice",
					"Hold should applicable for sales return invoice", "Sales return invoice is in hold", "Pass");
		} else {
			writeTestResults("Verify the hold for sales return invoice",
					"Hold should applicable for sales return invoice", "Sales return invoice is not in hold", "Fail");
		}

		click(serialMenuClose);
		Thread.sleep(1500);
	}

	public void fillDataVehicleInfDraft() throws Exception {

		if (isDisplayed(vehicleInformationHeader)) {
			writeTestResults("Verify that vehicle information page is loading",
					"User should be able to go to vehicle information page", "Vehicle information page loaded", "Pass");
		} else {
			writeTestResults("Verify that vehicle information page is loading",
					"User should be able to go to vehicle information page", "Vehicle information page is not loaded",
					"Fail");
		}

		Thread.sleep(3000);
		click(newVehicleInformationBtn);

		checkNewVehicleInformationLookup();
		Thread.sleep(2000);
		checkFieldsVehicleInformation();
		Thread.sleep(2000);
		Random random = new Random();
		String id = String.format("%04d", random.nextInt(10000));

		id1 = "CAR-" + id;

		document_map.put("vehicleNo", id1);
		document.add(0, document_map);

		writeTestCreations("vehicleNo", document.get(0).get("vehicleNo"), 8);

		Thread.sleep(2000);
		sendKeys(vehicleNoTxt, id1);
		Thread.sleep(2000);
		sendKeys(capacityTxt, capacityTxtVal);

		selectIndex(capacityMeasure, 2);
		Thread.sleep(2000);
		sendKeys(weightTxt, weightTxtVal);
		selectIndex(weightMeasure, 5);
		Thread.sleep(2000);
		click(draftVehicleInf);
		Thread.sleep(2000);

		sendKeys(vehicleSearchTop, id1);
		Thread.sleep(2000);
		mouseMove("//label[@class='ncell']");
		Thread.sleep(2000);
		pressEnter("//label[@class='ncell']");

		Thread.sleep(2000);
		String status = getText(statusVehicleInf);

		if (status.equals("Open")) {
			writeTestResults("Verify that user is able to draft the vehicle information",
					"Vehicle information should be draft", "Vehicle information is draft", "Pass");
		} else {
			writeTestResults("Verify that created vehicle information is in open status",
					"Vehicle information should be draft", "Vehicle information is draft", "Fail");
		}
	}

	public void fillDataVehicleInfEdit() throws Exception {

		click(edit);
		Thread.sleep(2000);

		if (isElementPresent(vehicleInfEditHeader)) {
			writeTestResults("Verify the edit in vehicle information",
					"User should be able to edit vehicle information", "Vehicle information is in edit", "Pass");
		} else {
			writeTestResults("Verify the edit in vehicle information",
					"User should be able to edit vehicle information", "Vehicle information is not in edit", "Fail");
		}
	}

	public void fillDataVehicleInfUpdate() throws Exception {

		Thread.sleep(2000);
		sendKeys(weightTxt, weightTxtNewVal);

		click(updateVehicleInf);
		Thread.sleep(2000);
		sendKeys(vehicleSearchTop, id1);
		Thread.sleep(2000);
		mouseMove("//label[@class='ncell']");
		Thread.sleep(2000);
		pressEnter("//label[@class='ncell']");

		click(edit);
		Thread.sleep(2000);
		String val = getAttribute(weightTxt, "value");

		if (val.equals("4.00")) {
			writeTestResults("Verify the update of vehicle information", "Vehicle information should update",
					"Vehicle information updated", "Pass");
		} else {
			writeTestResults("Verify the update of vehicle information", "Vehicle information should update",
					"Vehicle information not updated", "Fail");
		}

		click(updateVehicleInf);
	}

	public void fillDataVehicleInfActivate() throws Exception {

		Thread.sleep(2000);
		sendKeys(vehicleSearchTop, id1);
		Thread.sleep(2000);
		mouseMove("//label[@class='ncell']");
		Thread.sleep(2000);
		pressEnter("//label[@class='ncell']");

		Thread.sleep(2000);
		String status = getText(statusVehicleInf);

		if (status.equals("Open")) {
			writeTestResults("Verify that created vehicle information is in open status",
					"Created vehicle information should be in open status", "Vehicle information is in open status",
					"Pass");
		} else {
			writeTestResults("Verify that created vehicle information is in open status",
					"Created vehicle information should be in open status", "Vehicle information is not in open status",
					"Fail");
		}

		Thread.sleep(2000);
		click(activeBtnVehicleInf);

		click(alertYes);
		Thread.sleep(3000);
		String after = getText(afterYes);

		if (after.equals("Activated")) {
			writeTestResults("Verify that created vehicle information is activated",
					"Vehicle information should be activated", "Vehicle information is activated", "Pass");
		} else {
			writeTestResults("Verify that created vehicle information is activated",
					"Vehicle information should be activated", "Vehicle information is not activated", "Fail");
		}
	}

	public void fillDataVehicleInfInactivate() throws Exception {

		Thread.sleep(2000);
		sendKeys(vehicleSearchTop, id1);
		Thread.sleep(2000);
		mouseMove("//label[@class='ncell']");
		Thread.sleep(2000);
		pressEnter("//label[@class='ncell']");

		Thread.sleep(2000);
		click(inactiveBtnVehicleInf);
		Thread.sleep(2000);
		click(inactivateAlertYes);
		Thread.sleep(3000);

		String after = getText(afterYes);

		if (after.equals("Inactivated")) {
			writeTestResults("Verify that created vehicle information is inactivated",
					"Vehicle information should be inactivated", "Vehicle information is inactivated", "Pass");
		} else {
			writeTestResults("Verify that created vehicle information is inactivated",
					"Vehicle information should be inactivated", "Vehicle information is not inactivated", "Fail");
		}
	}

	public void AVConsolidateInvoiceDraft() throws Exception {

		click(consolidatedInvoice);
		Thread.sleep(2000);
		click(newConsolidateInvoice);
		Thread.sleep(2000);
		click(customerSearchConsolidate);
		Thread.sleep(2000);
		sendKeys(textConsolidate, customerAccVal);
		Thread.sleep(2000);
		pressEnter(textConsolidate);
		Thread.sleep(3000);
		doubleClick(selectedValConsolidate);
		Thread.sleep(2000);
		click(docList);
		Thread.sleep(2000);
		click(searchInvoice);
		Thread.sleep(2000);

		click(invoiceVal1);
		Thread.sleep(1000);
		click(invoiceVal2);
		Thread.sleep(2000);
		click(applyConso);
		Thread.sleep(2000);
		click(checkoutInvoice);
		Thread.sleep(2000);
		click(draft);
		Thread.sleep(5000);

		String header = getText(releaseInHeader);

		trackCode = getText(documentVal);
		// System.out.println(trackCode);

		if (header.equals("(Draft)")) {
			writeTestResults("User should be able to draft the consolidated invoice",
					"Consolidated invoice should be draft", "Consolidated invoice draft", "Pass");
		} else {
			writeTestResults("User should be able to draft the consolidated invoice",
					"Consolidated invoice should be draft", "Consolidated invoice not draft", "Fail");
		}
	}

	public void AVConsolidateInvoiceRelease() throws Exception {

		click(release);

		Thread.sleep(6000);
		String header = getText(releaseInHeader);

		trackCode = getText(documentVal);
		// System.out.println(trackCode);

		if (header.equals("(Released)")) {
			writeTestResults("User should be able to release the consolidated invoice",
					"Consolidated invoice should be released", "Consolidated invoice released", "Pass");
		} else {
			writeTestResults("User should be able to release the consolidated invoice",
					"Consolidated invoice should be released", "Consolidated invoice not released", "Fail");
		}
	}

	public void AVConsolidateInvoiceListSearch() throws Exception {

		click(consolidatedInvoice);
		Thread.sleep(2000);
		click(newConsolidateInvoice);
		Thread.sleep(2000);
		click(customerSearchConsolidate);
		Thread.sleep(2000);
		sendKeys(textConsolidate, customerAccVal);
		Thread.sleep(2000);
		pressEnter(textConsolidate);
		Thread.sleep(3000);
		doubleClick(selectedValConsolidate);
		Thread.sleep(2000);
		click(docList);
		Thread.sleep(2000);
		click(searchInvoice);
		Thread.sleep(2000);

		if (isDisplayed(invoiceVal1)) {
			writeTestResults("Verify search list of consolidated invoice", "User should be able to see search list",
					"Search list loaded", "Pass");
		} else {
			writeTestResults("Verify search list of consolidated invoice", "User should be able to see search list",
					"Search list not loaded", "Fail");
		}

		Thread.sleep(1000);
	}

	public void AVConsolidateInvoiceLDelete() throws Exception {

		click(invoiceVal1);
		Thread.sleep(1000);
		click(invoiceVal2);
		Thread.sleep(2000);
		click(applyConso);
		Thread.sleep(2000);
		click(checkoutInvoice);
		Thread.sleep(2000);
		click(draft);
		Thread.sleep(5000);

		String header = getText(releaseInHeader);

		trackCode = getText(documentVal);

		if (header.equals("(Draft)")) {
			writeTestResults("User should be able to draft the consolidated invoice",
					"Consolidated invoice should be draft", "Consolidated invoice draft", "Pass");
		} else {
			writeTestResults("User should be able to draft the consolidated invoice",
					"Consolidated invoice should be draft", "Consolidated invoice not draft", "Fail");
		}

		click(actionMenu);
		Thread.sleep(2000);
		click(delete);
		Thread.sleep(2000);
		click(confirmationYes);
		Thread.sleep(6000);
		String header1 = getText(releaseInHeader);
		System.out.println(header1);

		if (header1.equals("(Deleted)")) {
			writeTestResults("Verify the delete of consolidate invoice",
					"User should be able to delete the consolidate invoice", "Consolidate invoice deleted", "Pass");
		} else {
			writeTestResults("Verify the delete of consolidate invoice",
					"User should be able to delete the consolidate invoice", "Consolidate invoice not deleted", "Fail");
		}
	}

	public void AVWarrantyProfileDraft() throws Exception {

		click(warrantyProfile);
		Thread.sleep(2000);
		click(newWarrantyProfile);
		Thread.sleep(2000);
		LocalTime local = LocalTime.now();

		warrantyPro = "PC" + local;

		sendKeys(warrantyProfileCode, warrantyPro);
		Thread.sleep(1000);
		sendKeys(warrantyProfileName, profileNameVal);
		Thread.sleep(2000);
		click(outboundDraft);
		Thread.sleep(4000);

	}

	public void AVWarrantyProfileEdit() throws Exception {

		sendKeys("//input[@id='txtSearch']", warrantyPro);
		Thread.sleep(2000);
		System.out.println(warrantyPro);
		mouseMove("//label[@class='ncell']");
		Thread.sleep(2000);
		pressEnter("//label[@class='ncell']");
		Thread.sleep(1000);
		click(edit);
		Thread.sleep(2000);

		sendKeys(warrantyProfileName, profileNameNew);
		Thread.sleep(1000);
		click(outboundDraft);
		Thread.sleep(2000);
		click(edit);
		Thread.sleep(2000);
		String val = getAttribute(warrantyProfileName, "value");

		if (val.equals("Sales Sales")) {
			writeTestResults("Verify the edit in warranty profile", "User should be able to edit warranty profile",
					"Edit done", "Pass");
		} else {
			writeTestResults("Verify the edit in warranty profile", "User should be able to edit warranty profile",
					"Edit not done", "Fail");
		}

		click(serialMenuClose);
		Thread.sleep(1000);
	}

	public void AVWarrantyProfileNew() throws Exception {

		click(newWarrantyProfile);
		Thread.sleep(2000);

		if (getText(newVehicleInformationHeader).equals("New Warranty Profile")) {
			writeTestResults("Verify the new in warranty profile", "User should be able to do new", "New is done",
					"Pass");
		} else {
			writeTestResults("Verify the new in warranty profile", "User should be able to do new", "New is not done",
					"Fail");
		}

		click(serialMenuClose);
		Thread.sleep(1000);
	}

	public void AVWarrantyProfileActivate() throws Exception {

		click(activateBtn);
		Thread.sleep(1000);
		click(confirmationYes);
		Thread.sleep(2000);

		String val = getText(statusWarrantyProfile);
		// System.out.println(val);

		if (val.equals("Activated")) {
			writeTestResults("Verify the activation of warranty profile",
					"User should be able to activate the warranty profile", "Activation done", "Pass");
		} else {
			writeTestResults("Verify the activation of warranty profile",
					"User should be able to activate the warranty profile", "Activation not done", "Fail");
		}
	}

	public void AVWarrantyProfileInactivate() throws Exception {

		click(activateBtn);
		Thread.sleep(1000);
		click(inactivateAlertYes);
		Thread.sleep(2000);

		String val = getText(statusWarrantyProfile);
		// System.out.println(val);

		if (val.equals("Inactivated")) {
			writeTestResults("Verify the inactivation of warranty profile",
					"User should be able to inactivate the warranty profile", "Inactivation done", "Pass");
		} else {
			writeTestResults("Verify the inactivation of warranty profile",
					"User should be able to inactivate the warranty profile", "Inactivation not done", "Fail");
		}
	}

	public void AVPricingRuleEdit() throws Exception {

		click(newPricingRule);
		if (isDisplayed(newPricingruleookup)) {
			writeTestResults("Verify that new pricing rule look up is loading",
					"user should be able to see the pricing rule lookup page", "Pricing rule lookup is dislayed",
					"Pass");
		} else {
			writeTestResults("Verify that new pricing rule look up is loading",
					"user should be able to see the pricing rule lookup page", "Pricing rule lookup is not dislayed",
					"Fail");
		}

		LocalTime myObj = LocalTime.now();

		ruleCodeValue = "RC" + myObj;
		Thread.sleep(2000);
		sendKeys(ruleCode, ruleCodeValue);
		Thread.sleep(2000);

		// trackCode = ruleCodeValue;
		sendKeys(ruleDescription, ruleDescriptionVal);
		Thread.sleep(2000);
		click(draftPricingRule);
		Thread.sleep(2000);
		click(edit);
		Thread.sleep(2000);

		String val = getText(newVehicleInformationHeader);

		if (val.equals("Edit Pricing Rule")) {
			writeTestResults("Verify the edit in pricing rule", "User should be able to edit pricing rule", "Edit done",
					"Pass");
		} else {
			writeTestResults("Verify the edit in pricing rule", "User should be able to edit pricing rule",
					"Edit not done", "Fail");
		}
	}

	public void AVPricingRuleUpdate() throws Exception {

//		click(newPricingRule);
//		
//		if (isDisplayed(newPricingruleookup)) {
//			writeTestResults("Verify that new pricing rule look up is loading",
//					"user should be able to see the pricing rule lookup page", "Pricing rule lookup is dislayed",
//					"Pass");
//		} else {
//			writeTestResults("Verify that new pricing rule look up is loading",
//					"user should be able to see the pricing rule lookup page", "Pricing rule lookup is not dislayed",
//					"Fail");
//		}
//
//		LocalTime myObj = LocalTime.now();
//
//		ruleCodeValue = "RC" + myObj;
//		Thread.sleep(2000);
//		sendKeys(ruleCode, ruleCodeValue);
//		Thread.sleep(2000);
//
//		trackCode = ruleCodeValue;
//		sendKeys(ruleDescription, ruleDescriptionVal);
//		Thread.sleep(2000);
//		click(draftPricingRule);
//		Thread.sleep(2000);
		click(edit);
		Thread.sleep(2000);
		sendKeys(ruleDescription, ruleDescriptionNewVal);
		click(updatePricingRule);
		Thread.sleep(2000);
		click(edit);
		Thread.sleep(2000);
		String val = getAttribute(ruleDescription, "value");

		if (val.equals("get")) {
			writeTestResults("Verify the update in pricing rule", "User should be able to update pricing rule",
					"Update done", "Pass");
		} else {
			writeTestResults("Verify the update in pricing rule", "User should be able to update pricing rule",
					"Update not done", "Fail");
		}
		click(updatePricingRule);
		Thread.sleep(2000);
	}

	public void AVPricingRuleSearch() throws Exception {

		// System.out.println(ruleCodeValue);
		sendKeys(salesInvoiceSearchTxt, ruleCodeValue);
		Thread.sleep(2000);

		if (isElementPresent("//label[@class='ncell']")) {
			writeTestResults("Verify the search in pricing rule", "User should be able to search a pricing rule",
					"Searched rule loaded", "Pass");
		} else {
			writeTestResults("Verify the search in pricing rule", "User should be able to search a pricing rule",
					"Searched rule not loaded", "Fail");
		}
	}

	public void AVPricingRuleResetSearch() throws Exception {

		click(resetPricing);
		Thread.sleep(2000);

		if (!isElementPresent("//label[@class='ncell']")) {
			writeTestResults("Verify the reset in pricing rule", "User should be able to reset a pricing rule", "Reset",
					"Pass");
		} else {
			writeTestResults("Verify the reset in pricing rule", "User should be able to reset a pricing rule", "Reset",
					"Fail");
		}
	}

	public void AVSalesItineraryAddAppointment() {

//		click(salesItierary);
//		selectText(salesUnitItinerary, salesUnitVal);
//		selectText(accountOwnerItinerary, accountOwnerVal);
//		click(addAppointment);
//		sendKeys(subjectItinerary, subjectItineraryVal);
//		click(addAccount);
//		sendKeys(accountText, accountVal);
//		pressEnter(accountText);
//		doubleClick(selectedItinerary);
//		click(updateTask);

	}

	public void AVSalesParameterUpdate() throws Exception {

		click(salesParameter);
		Thread.sleep(2000);
		click(addNewSalesParameter);
		Thread.sleep(3000);

		LocalTime obj = LocalTime.now();

		sendKeys(salesParameterText, "S" + obj);
		click(updateSales);
		Thread.sleep(3000);
		click(closeAlertSales);
		Thread.sleep(1000);
		String valBefore = getAttribute(salesParameterText, "value");
		// System.out.println(valBefore);

		Thread.sleep(2000);
		LocalTime obj1 = LocalTime.now();
		sendKeys(salesParameterText, "S" + obj1);
		Thread.sleep(2000);
		click(updateSales);
		Thread.sleep(2000);

		click(closeAlertSales);
		Thread.sleep(1000);

		String valAft = getAttribute(salesParameterText, "value");
		// System.out.println(valAft);

		if (!valBefore.equals(valAft)) {
			writeTestResults("Verify the update in sales parameters", "User shoul be able to update sales parameters",
					"Update done", "Pass");

		} else {
			writeTestResults("Verify the update in sales parameters", "User shoul be able to update sales parameters",
					"Update not done", "Fail");
		}
	}

	public void AVContactInformationEdit() throws Exception {

		click(contactInformation);
		Thread.sleep(2000);
		click(editContact);
		Thread.sleep(2000);

		if (isDisplayed(update)) {
			writeTestResults("Verify the edit in contact information",
					"User should be able to edit contact information", "Edit done", "Pass");
		} else {
			writeTestResults("Verify the edit in contact information",
					"User should be able to edit contact information", "Edit not done", "Fail");
		}
	}

	public void AVContactInformationUpdate() throws Exception {

		click(contactInformationHeader);
		Thread.sleep(3000);

		click(editContact);
		Thread.sleep(3000);

		String val = getText(contactNameValText);
		System.out.println(val);

		click(edit);
		Thread.sleep(2000);

		sendKeys("//input[@id='txtName']", contactNameNewVal);
		Thread.sleep(2000);
		click(update);
		Thread.sleep(2000);

		String val1 = getText(contactNameValText);
		System.out.println(val1);

		if (val1.equals("John")) {
			writeTestResults("Verify the update of contact information",
					"User should be able update contact information", "Contact information updated", "Pass");
		} else {
			writeTestResults("Verify the update of contact information",
					"User should be able update contact information", "Contact information not updated", "Fail");
		}
	}

	public void AVContactInformationUpdateAndNew() throws Exception {

		click(edit);
		Thread.sleep(2000);
		click(updateAndNew);
		Thread.sleep(3000);

		String header = getText(documentVal);

		if (header.equals("New")) {
			writeTestResults("Verify the update and new in contact information",
					"User should be able to do update and new", "Update and new done", "Pass");
		} else {
			writeTestResults("Verify the update and new in contact information",
					"User should be able to do update and new", "Update and new not done", "Fail");
		}
	}

	public void AVContactInformationHistory() throws Exception {

		click(contactInformationHeader);
		Thread.sleep(2000);
		click(editContact);
		Thread.sleep(2000);

		click(actionMenu);
		Thread.sleep(2000);
		click(history);
		Thread.sleep(2000);

		if (isDisplayed(createInHistory)) {
			writeTestResults("Verify the history in contact information", "User should be able to see the history",
					"History loaded", "Pass");
		} else {
			writeTestResults("Verify the history in contact information", "User should be able to see the history",
					"History not loaded", "Fail");
		}
	}

	public void AVContactInformationActivities() throws Exception {

		click(actionMenu);
		Thread.sleep(2000);
		click(activities);
		Thread.sleep(2000);

		if (isDisplayed(activityHeader)) {
			writeTestResults("Verify the activities", "User should be able to view the activities",
					"Activity window loaded", "Pass");
		} else {
			writeTestResults("Verify the activities", "User should be able to view the activities",
					"Activity window not loaded", "Fail");
		}

		click(activityMenuClose);
		Thread.sleep(2000);
	}

	public void AVContactInformationNewInquiry() throws Exception {

		click(inquiryInformation);
		Thread.sleep(2000);
		click(newInquiry);
		Thread.sleep(3000);
		switchWindow();

		if (isDisplayed(salesInquiryHeader)) {
			writeTestResults("Verify the inquiry in contact information",
					"User should be able do new inquiry information", "Inquiry loaded", "Pass");
		} else {
			writeTestResults("Verify the inquiry in contact information",
					"User should be able do new inquiry information", "Inquiry not loaded", "Fail");
		}
	}

	public void AVContactInformationStatus() throws Exception {

		sendKeys(inquiryAbout, inquiryAboutVal);
		click(draft);
		Thread.sleep(3000);

		String val2 = getText(status);

		if (val2.equals("OPEN")) {
			writeTestResults("Verify the status in contact information", "User should be able to see the status",
					"Status changed", "Pass");
		} else {
			writeTestResults("Verify the status in contact information", "User should be able to see the status",
					"Status not changed", "Fail");
		}
	}

	public void AVLeadInformationDraft() throws Exception {

		click(newLead);
		selectIndex(nameTitleLead, 1);
		sendKeys(nameLead, nameTextLeadVal);
		String mobile = String.valueOf(mobileLeadtVal);
		sendKeys(mobileLeadText, mobile);
		Thread.sleep(2000);
		selectIndex(salesUnitLead, 1);
		click(draftLead);
		Thread.sleep(3000);

		String val = getText(releaseInHeader);

		if (val.equals("(Draft)")) {
			writeTestResults("Verify the draft in lead information", "User shoul be able to draft the lead information",
					"Lead information draft", "Pass");
		} else {
			writeTestResults("Verify the draft in lead information", "User shoul be able to draft the lead information",
					"Lead information not draft", "Fail");
		}
	}

	public void AVLeadInformationEdit() throws Exception {

		click(edit);
		Thread.sleep(2000);

		if (isElementPresent(update)) {
			writeTestResults("Verify the edit in lead information", "User shoul be able to edit the lead information",
					"Lead information edit", "Pass");
		} else {
			writeTestResults("Verify the edit in lead information", "User shoul be able to edit the lead information",
					"Lead information not edit", "Fail");
		}
	}

	public void AVLeadInformationUpdate() throws Exception {

		sendKeys(nameLead, nameTextLeadNewVal);
		click(update);
		Thread.sleep(2000);
		click(edit);
		Thread.sleep(2000);

		String val = getAttribute(nameLead, "value");

		if (val.equals("Kumara")) {
			writeTestResults("Verify the update in lead information", "User should be able to update lead information",
					"Lead information updated", "Pass");
		} else {
			writeTestResults("Verify the update in lead information", "User should be able to update lead information",
					"Lead information not updated", "Fail");
		}
	}

	public void AVLeadInformationUpdateAndNew() throws Exception {

		click(updateAndNew);
		Thread.sleep(2500);

		String val = getText(documentVal);

		if (val.equals("New")) {
			writeTestResults("Verify the update and new in lead information", "User should be able do update and new",
					"Update and new not done", "Pass");
		} else {
			writeTestResults("Verify the update and new in lead information", "User should be able do update and new",
					"Update and new not done", "Fail");
		}
	}

	public void AVLeadInformationRelease() throws Exception {

		click(leadInformationHeader);
		Thread.sleep(3000);
		click(newLead);
		Thread.sleep(2000);
		selectIndex(nameTitleLead, 1);
		Thread.sleep(1000);
		sendKeys(nameLead, nameTextLeadVal);

		LocalTime obj = LocalTime.now();
		String mobile = String.valueOf(obj);

		String mobile2 = mobile.replace(":", "");
		sendKeys(mobileLeadText, mobile2);
		Thread.sleep(1000);
		selectIndex(salesUnitLead, 1);
		click(draftLead);
		Thread.sleep(2000);
		click(releaseLead);
		Thread.sleep(2000);

	}

	public void AVLeadInformationHistory() throws Exception {

		click(actionMenu);
		Thread.sleep(2000);
		click(history);
		Thread.sleep(2000);

		if (isElementPresent(historyReleased)) {
			writeTestResults("Verify the history of lead information", "User should be able to view the history",
					"History displayed", "Pass");
		} else {
			writeTestResults("Verify the history of lead information", "User should be able to view the history",
					"History not displayed", "Fail");
		}
	}

	public void AVLeadInformationActivities() throws Exception {

		click(actionMenu);
		Thread.sleep(2000);
		click(activities);
		Thread.sleep(2000);

		if (isDisplayed(activityHeader)) {
			writeTestResults("Verify the activities of lead information", "User should be able to view the activities",
					"Activities displayed", "Pass");
		} else {
			writeTestResults("Verify the activities of lead information", "User should be able to view the activities",
					"Activities displayed", "Fail");
		}
	}

	public void AVLeadInformationDuplicate() throws Exception {

		click(activityMenuClose);
		Thread.sleep(2000);
		click(duplicate);
		Thread.sleep(2500);

		String val = getText(documentVal);

		if (val.equals("New")) {
			writeTestResults("Verify duplicate in lead information", "User shold be able to duplicate", "Duplicated",
					"Pass");
		} else {
			writeTestResults("Verify duplicate in lead information", "User shold be able to duplicate",
					"Not duplicated", "Fail");
		}
	}

	public void checkClickPricingModelPageAV() throws Exception {

		if (isDisplayed(pricingModel)) {
			writeTestResults("Verify that Pricing Model page is available",
					"user should be able to see the pricing model page", "Pricing Model page is dislayed", "pass");
			click(pricingModel);
			Thread.sleep(2000);
		} else {
			writeTestResults("Verify that Pricing Model page is available",
					"user should be able to see the pricing model page'", "Pricing Model page is not dislayed", "Fail");
		}

	}

	public void AVPricingModelDraft() throws Exception {

		click(newPricingModel);
		Thread.sleep(2000);

		LocalTime myObj = LocalTime.now();
		code1 = "M" + myObj;

		sendKeys(modelCode, code1);
		Thread.sleep(1000);
		sendKeys(description, modelDescriptionVal);
		Thread.sleep(2000);
		click(rulesTab);
		Thread.sleep(2000);
		click(addPricingRule);
		Thread.sleep(2000);
		SalesAndMarketingModule obj5 = new SalesAndMarketingModule();
		sendKeys(searchTxtRule, obj5.readTestCreation(6));
		Thread.sleep(2500);
		pressEnter(searchTxtRule);
		Thread.sleep(2000);
		// System.out.println(selectedRule);
		// String one = selectedRule.replace("g1031-t",ruleCodeValue);
		doubleClick(selectedRule);
		Thread.sleep(2000);
		click(editPeriod);
		Thread.sleep(2000);
		click(startDate);
		Thread.sleep(2000);
		click(startDateValue);
		Thread.sleep(2000);
		click(endDate);
		Thread.sleep(3000);
		click(endDateValue);
		Thread.sleep(2000);
		click(applyModel);
		Thread.sleep(2000);
		click(draftModel);
		Thread.sleep(4000);
//		click(releaseModel);
//		Thread.sleep(4000);

//		String header = getText(releaseInHeader);
//		trackCode = getText(documentVal);
//
//		document_map.put("pricingModel", trackCode);
//		document.add(0, document_map);
//
//		if (header.equals("( Released )")) {
//			writeTestResults("User should be able to release the pricing model", "Pricing model should be released",
//					"Pricing model is released", "Pass");
//		} else {
//			writeTestResults("User should be able to release the pricing model", "Pricing model should be released",
//					"Pricing model is not released", "Fail");
//		}
//
//		writeTestCreations("pricingModel", document.get(0).get("pricingModel"), 7);

	}

	public void AVPricingModelEdit() throws Exception {

		click(edit);
		Thread.sleep(2000);

		if (isDisplayed(update)) {
			writeTestResults("Verify the edit in pricing model", "User should be able to edit pricing model",
					"Pricing model is in edit", "Pass");
		} else {
			writeTestResults("Verify the edit in pricing model", "User should be able to edit pricing model",
					"Pricing model is not in edit", "Fail");
		}
	}

	public void AVPricingModelUpdate() throws Exception {

		sendKeys(description, descriptionVal);
		Thread.sleep(2000);
		click(update);
		Thread.sleep(2000);
		click(edit);
		Thread.sleep(2000);
		String val = getAttribute(description, "value");

		if (val.equals("get free")) {
			writeTestResults("Verify the update in pricing model", "User should be able to update the prcing model",
					"Pricing model updated", "Pass");
		} else {
			writeTestResults("Verify the update in pricing model", "User should be able to update the prcing model",
					"Pricing model not updated", "Fail");
		}
	}

	public void AVPricingModelUpdateAndNew() throws Exception {

		click(updateAndNew);
		Thread.sleep(2000);

		String val = getText(documentVal);

		if (val.equals("New")) {
			writeTestResults("Verify the update and new in pricing model", "User should be able to do update and new",
					"Update and new done", "Pass");
		} else {
			writeTestResults("Verify the update and new in pricing model", "User should be able to do update and new",
					"Update and new not done", "Fail");
		}
	}

	public void AVPricingModelRelease() throws Exception {

		click(newPricingModel);
		Thread.sleep(2000);

		LocalTime myObj = LocalTime.now();
		code1 = "M" + myObj;

		sendKeys(modelCode, code1);
		Thread.sleep(1000);
		sendKeys(description, modelDescriptionVal);
		Thread.sleep(2000);
		click(rulesTab);
		Thread.sleep(2000);
		click(addPricingRule);
		Thread.sleep(2000);
		SalesAndMarketingModule obj5 = new SalesAndMarketingModule();
		sendKeys(searchTxtRule, obj5.readTestCreation(6));
		Thread.sleep(2500);
		pressEnter(searchTxtRule);
		Thread.sleep(2000);
		doubleClick(selectedRule);
		Thread.sleep(2000);
		click(editPeriod);
		Thread.sleep(2000);
		click(startDate);
		Thread.sleep(2000);
		click(startDateValue);
		Thread.sleep(2000);
		click(endDate);
		Thread.sleep(3000);
		click(endDateValue);
		Thread.sleep(2000);
		click(applyModel);
		Thread.sleep(2000);
		click(draftModel);
		Thread.sleep(4000);
		click(releaseModel);
		Thread.sleep(4000);

		String header = getText(releaseInHeader);

		if (header.equals("( Released )")) {
			writeTestResults("User should be able to release the pricing model", "Pricing model should be released",
					"Pricing model is released", "Pass");
		} else {
			writeTestResults("User should be able to release the pricing model", "Pricing model should be released",
					"Pricing model is not released", "Fail");
		}
	}

	public void AVPricingModelActivities() throws Exception {

		click(actionMenu);
		Thread.sleep(2000);
		click(activities);
		Thread.sleep(2000);

		if (isDisplayed(activityHeader)) {
			writeTestResults("Verify the activities in pricing model", "User should be able to see the activities",
					"Activities loaded", "Pass");
		} else {
			writeTestResults("Verify the activities in pricing model", "User should be able to see the activities",
					"Activities not loaded", "Fail");
		}

		click(activityMenuClose);
		Thread.sleep(2000);
	}

	public void AVPricingModelHistory() throws Exception {

		click(actionMenu);
		Thread.sleep(2000);
		click(history);
		Thread.sleep(2000);

		if (isDisplayed(release)) {
			writeTestResults("Verify the history in pricing model", "User should be able see history",
					"History displayed", "Pass");
		} else {
			writeTestResults("Verify the history in pricing model", "User should be able see history",
					"History not displayed", "Fail");
		}
	}

	public void AVPricingModelAddNewRule() throws Exception {

		click(edit);
		Thread.sleep(2000);
		click(rulesTab);
		Thread.sleep(2000);
		click(addPricingRule);
		Thread.sleep(2000);

		if (isDisplayed(newPricingruleookup)) {
			writeTestResults("Verify the adding new rule to pricing model",
					"User should be able to add new pricing rule", "Pricing rule added", "Pass");
		} else {
			writeTestResults("Verify the adding new rule to pricing model",
					"User should be able to add new pricing rule", "Pricing rule not added", "Fail");
		}

		click(activityMenuClose);
	}

	public void AVPricingModelDeleteRule() throws Exception {

		click(deleteRule);

		if (!isElementPresent(deleteRule)) {
			writeTestResults("Verify the delete of pricing rule", "User should be able to delete the pricing rule",
					"Pricing rule deleted", "Pass");
		} else {
			writeTestResults("Verify the delete of pricing rule", "User should be able to delete the pricing rule",
					"Pricing rule not deleted", "Fail");
		}
	}

	public void checkClickSalesUnit() throws Exception {

		if (isDisplayed(salesUnitNavigation)) {
			writeTestResults("Verify that Pricing Model page is available",
					"user should be able to see the pricing model page", "Pricing Model page is dislayed", "pass");
			click(salesUnitNavigation);
			Thread.sleep(2000);
		} else {
			writeTestResults("Verify that Pricing Model page is available",
					"user should be able to see the pricing model page'", "Pricing Model page is not dislayed", "Fail");
		}
	}

	public void AVSalesUnitDraft() throws Exception {

		click(newSalesUnit);
		Thread.sleep(2000);
		LocalTime obj = LocalTime.now();
		sendKeys(salesUnitCode, "SU" + obj);
		Thread.sleep(2000);
		sendKeys(salesUnitName, salesUnitNameVal);
		Thread.sleep(2000);
		sendKeys(commissionRate, commissionRateVal);
		Thread.sleep(2000);
		click(unitLeaderSearch);
		Thread.sleep(2000);
		sendKeys(unitLeaderText, unitLeaderVal);
		Thread.sleep(2000);
		pressEnter(unitLeaderText);
		Thread.sleep(2000);
		doubleClick(unitLeaderSelected);
		Thread.sleep(2000);
		click(productLookupInvoice);
		Thread.sleep(2000);
		sendKeys(employeeSearchText, employeeVal);
		Thread.sleep(2000);
		pressEnter(employeeSearchText);
		Thread.sleep(2000);
		doubleClick(unitLeaderSelected);
		Thread.sleep(2000);
		sendKeys(sharingRateSales, sharingRateVal);
		click(draft);
		Thread.sleep(2000);

	}

	public void AVSalesUnitEdit() throws Exception {

		click(edit);
		Thread.sleep(2000);

		if (isDisplayed(update)) {
			writeTestResults("Verify the edit in sales unit", "User should be able to edit the sales unit", "Edit done",
					"Pass");
		} else {
			writeTestResults("Verify the edit in sales unit", "User should be able to edit the sales unit",
					"Edit not done", "Fail");
		}
	}

	public void AVSalesUnitUpdate() throws Exception {

		sendKeys(commissionRate, sharingRateNewVal);

		Thread.sleep(1000);
		click(update);
		Thread.sleep(2000);
		click(edit);
		Thread.sleep(2000);
		String val = getAttribute(commissionRate, "value");

		if (val.equals("30")) {
			writeTestResults("Verify update in sales unit", "User should be able to update sales unit", "Update done",
					"Pass");
		} else {
			writeTestResults("Verify update in sales unit", "User should be able to update sales unit",
					"Update not done", "Fail");
		}
	}

	public void AVSalesUnitUpdateAndNew() throws Exception {

		click(edit);
		Thread.sleep(2000);
		click(updateAndNew);
		Thread.sleep(2000);

		String val = getAttribute(commissionRate, "value");

		if (val.equals("0.00")) {
			writeTestResults("Verify update and new in sales unit",
					"User should be able to do update and new sales unit", "Update and new done", "Pass");
		} else {
			writeTestResults("Verify update and new in sales unit",
					"User should be able to do update and new sales unit", "Update and new not done", "Fail");
		}
	}

	public void AVSalesUnitRelease() throws Exception {

		click(newSalesUnit);
		Thread.sleep(2000);
		LocalTime obj = LocalTime.now();
		sendKeys(salesUnitCode, "SU" + obj);
		Thread.sleep(2000);

		trackCode = "SU" + obj;
		document_map.put("SalesUnit", trackCode);
		document.add(0, document_map);

		sendKeys(salesUnitName, salesUnitNameVal);
		Thread.sleep(2000);
		sendKeys(commissionRate, commissionRateVal);
		Thread.sleep(2000);
		click(unitLeaderSearch);
		Thread.sleep(2000);
		sendKeys(unitLeaderText, unitLeaderVal);
		Thread.sleep(2000);
		pressEnter(unitLeaderText);
		Thread.sleep(2000);
		doubleClick(unitLeaderSelected);
		Thread.sleep(2000);
		click(productLookupInvoice);
		Thread.sleep(2000);
		sendKeys(employeeSearchText, employeeVal);
		Thread.sleep(2000);
		pressEnter(employeeSearchText);
		Thread.sleep(2000);
		doubleClick(unitLeaderSelected);
		Thread.sleep(2000);
		sendKeys(sharingRateSales, sharingRateVal);
		click(draft);
		Thread.sleep(2000);
		click(release);
		Thread.sleep(3000);

		String header = getText(releaseInHeader);

		if (header.equals("(Released)")) {
			writeTestResults("User should be able to release the sales unit", "Sales unit should be released",
					"Sales unit is released", "Pass");
		} else {
			writeTestResults("User should be able to release the sales unit", "Sales unit should be released",
					"Sales unit is not released", "Fail");
		}

		writeTestCreations("SalesUnit", document.get(0).get("SalesUnit"), 9);

	}

	public void AVSalesUnitHistory() throws Exception {

		click(actionMenu);
		Thread.sleep(2000);
		click(history);
		Thread.sleep(2000);

		if (isElementPresent(historyReleased)) {
			writeTestResults("Verify the history in sale unit", "User should be able to see the sales unit history",
					"History displayed", "Pass");
		} else {
			writeTestResults("Verify the history in sale unit", "User should be able to see the sales unit history",
					"History not displayed", "Fail");
		}
	}

	public void AVSalesUnitActivities() throws Exception {

		click(actionMenu);
		Thread.sleep(2000);
		click(activities);
		Thread.sleep(2000);

		if (isDisplayed(activityHeader)) {
			writeTestResults("Verify the activities in sales unit",
					"User should be able to see the activities in sales unit", "Activities displayed", "Pass");
		} else {
			writeTestResults("Verify the activities in sales unit",
					"User should be able to see the activities in sales unit", "Activities not displayed", "Fail");
		}
	}

	public void createWarehouse() throws Exception {

		click(invenWarehouse);
		Thread.sleep(2000);
		click(warehouseInformation);
		Thread.sleep(1000);
		click(newWarehouse);
		Thread.sleep(1000);

		LocalTime obj = LocalTime.now();

		sendKeys(warehouseCode, "W" + obj);
		sendKeys(warehouseName, warehouseNameVal);
		selectText(businessUnit, businessUnitVal);

		trackCode = getText(warehouseCode);
		document_map.put("warehouse", trackCode);
		document.add(0, document_map);

		click(draft);
		Thread.sleep(2000);
		click(release);
		Thread.sleep(2000);

		writeTestCreations("warehouse", document.get(0).get("warehouse"), 10);

	}

	/* Common Implicit Wait */
	public void implisitWait(int seconds) throws Exception {
		driver.manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);
		Thread.sleep(500);
	}

}