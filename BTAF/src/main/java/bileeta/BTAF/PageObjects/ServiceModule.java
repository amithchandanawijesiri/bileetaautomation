package bileeta.BTAF.PageObjects;

import java.sql.Date;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.poi.ss.formula.functions.Replace;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.ClickAction;

import bileeta.BATF.Pages.ProcumentModuleData;
import bileeta.BATF.Pages.ServiceModuleData;
import io.appium.java_client.android.nativekey.PressesKey;
import io.appium.java_client.windows.PressesKeyCode;

public class ServiceModule extends ServiceModuleData {

	public static String no;
	public static String txt_employeecode;

	public void runjQuery() {
		String script = "const event = $.Event('keyup');\r\n" + "			 event.which = 13;\r\n"
				+ "			 event.keyCode = 13;\r\n" + "			 this.$('#attrSlide-txtSerialFrom').focus();\r\n"
				+ "			 this.$('#attrSlide-txtSerialFrom').trigger(event);";
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript(script);

	}

	public void navigateToTheLoginPage() throws Exception {
		openPage(siteURL);

		writeTestResults("Verify that 'Entution' header is available on the page",
				"User should be navigate to the login page of the Entution", "login page is displayed", "pass");

	}

	public void verifyTheLogo() throws Exception {
		if (isDisplayed(siteLogo)) {

			writeTestResults("Verify that 'Entution' header is available on the page",
					"user should be able to see the logo", "logo is displayed", "pass");
		} else {
			writeTestResults("Verify that 'Entution' header is available on the page",
					"user should be able to see the logo", "logo is not displayed", "fail");

		}
	}

	public void userLogin() throws Exception

	{
		Thread.sleep(2000);
		sendKeys(username, txt_UserNameDataService);
		sendKeys(password, txt_PasswordDataService);
		Thread.sleep(2000);
		click(btn_login);
		Thread.sleep(3000);
//		Thread.sleep(5000);
//		
//		driver.switchTo().alert().dismiss();

		if (isDisplayed(lnk_home)) {

			writeTestResults("User login", "User should navigate to the Home page of Entution",
					"user navigated to the home page", "pass");
		}

		else {
			writeTestResults("User login", "User should navigate to the Home page of Entution",
					"user cant navigate to the home page", "fail");

		}
	}

	public void navigationmenu() throws Exception {

		click(btn_navigationmenu);

		if (isDisplayed(sidemenu)) {
			writeTestResults("Click on the top navigation menu", "System should be loaded the menu item successfully",
					"Menu item is displayed", "pass");
		} else {
			writeTestResults("Click on the top navigation menu", "System should not  loaded the menu item successfully",
					"Menu item is not displayed", "fail");
		}
	}

	public void btn_service() throws Exception {

		Thread.sleep(3000);
		click(btn_servicemodule);

		Thread.sleep(2000);

		if (isDisplayed(subsidemenu)) {

			writeTestResults("Click on the Service module", "System should be loaded the Service module successfully",
					"subsidemenu is displayed", "pass");
		} else {

			writeTestResults("Click on the Service module", "System should not loaded the Service module successfully",
					"subsidemenu is not displayed", "fail");
		}
	}

	public void btn_ServiceJobOrder2() throws Exception {

		click(btn_ServiceJobOrder);

		if (isDisplayed(ServiceJobOrderPage)) {

			writeTestResults("Click on Service job order under menu",
					"Service job order should be available in the menu", "Service job order is displayed", "pass");
		} else {

			writeTestResults("Click on Service job order under menu",
					"Service job order should not available in the menu", "Service job order is not displayed", "fail");
		}

		click(btn_joborder);
	}

	public void btn_ServiceJobOrder() throws Exception {

		Thread.sleep(4000);

		click(btn_ServiceJobOrder);

		if (isDisplayed(ServiceJobOrderPage)) {

			writeTestResults("Click on Service job order under menu",
					"Service job order should be available in the menu", "Service job order is displayed", "pass");
		} else {

			writeTestResults("Click on Service job order under menu",
					"Service job order should not available in the menu", "Service job order is not displayed", "fail");
		}

		Thread.sleep(3000);

		click(btn_NewServiceJobOrder);

		if (isDisplayed(StartNewJourneyPage)) {

			writeTestResults(" Click on \"New Service job order\" button in By page",
					"Service job order journey look up should be open",
					"Service job order journey look up is displayed", "pass");
		} else {

			writeTestResults(" Click on \"New Service job order\" button in By page",
					"Service job order journey look up should be open",
					"Service job order journey look up is not displayed", "fail");
		}

		Thread.sleep(4000);

		click(btn_ServiceOrdertoInternalDispatchOrderExternal);

		if (isDisplayed(NewServiceJobOrderPage)) {

			writeTestResults("Select \"Service Order to Internal Dispatch Order (External) \" Journey",
					"Service job order New  page should be open", "Service job order New  page is displayed", "pass");
		} else {

			writeTestResults(" Select \"Service Order to Internal Dispatch Order (External) \" Journey",
					"Service job order New  page should not be open", "Service job order New page is  not displayed",
					"fail");
		}
		LocalTime myObj = LocalTime.now();

		sendKeys(ServiceJobOrderTitle, txt_ServiceJobOrderTitle + myObj);

//			sendKeys(ServiceJobOrderAccount,txt_ServiceJobOrderAccount);

		click(btn_accountsearch);

		if (isDisplayed(AccountLookUPPage)) {

			writeTestResults("Click account search", "AccountLookUPPage should be open",
					"AccountLookUPPage  page is displayed", "pass");
		} else {

			writeTestResults("Click account search ", "AccountLookUPPage should not be open",
					"AccountLookUPPage is not displayed", "fail");
		}

		selectText(template, txt_Template);

		sendKeys(Accountname, txt_Accountname);

		pressEnter(Accountname);

		Thread.sleep(2000);

		doubleClick(btn_accountuser);

		Thread.sleep(1000);

//			 click(btn_Productso);
//		 
//		    Thread.sleep(4000);
//			 
//			 click(btn_SerialNos);

		Thread.sleep(1000);

		selectText(ServiceGroup, "IDC-INTER DEPARTMENT JOBS");

		sendKeys(ServiceJobOrderDescription, txt_ServiceJobOrderDescription);

		Thread.sleep(2000);

		Thread.sleep(2000);

		click(btn_searchowner);

		Thread.sleep(1000);

		if (isDisplayed(OwnerLookUpPage)) {

			writeTestResults(" Click to search owner ", "OwnerLookUpPage should be open ",
					"OwnerLookUpPage viewed successfully", "pass");
		} else {

			writeTestResults(" Click to search owner ", " OwnerLookUpPage should not  be open",
					"OwnerLookUpPage not viewed successfully", "fail");
		}

		sendKeys(owner, "H V Hewa Vidana");

		pressEnter(owner);

		Thread.sleep(1000);

		doubleClick(btn_owner);

		Thread.sleep(1000);

		selectText(Priority, "High");

		Thread.sleep(1000);

		click(btn_PricingProfileSearch);

		if (isDisplayed(PricingProfileLookUpPage)) {

			writeTestResults(" Select a \"Pricing Profile\" ", " Able to select a pricing profile from the look up",
					"Able to select pricing profile successfully", "pass");
		} else {

			writeTestResults(" Select a \"Pricing Profile\" ", " Not Able to select a pricing profile from the look up",
					"Not able to select pricing profile successfully", "fail");
		}
		Thread.sleep(1000);

		doubleClick(btn_pricingprofile12);

	}

	public void searchlocation() throws Exception {

		click(btn_searchservicelocation);

		selectText(template, txt_Template);

		sendKeys(ServiceLocation, txt_ServiceLocation);

		pressEnter(ServiceLocation);

		Thread.sleep(3000);

		doubleClick(btn_location);

		Thread.sleep(2000);

		click(btn_searchShippingAddress);

		sendKeys(ShippingAddress, "09,Colombo,Srilanka");

		Thread.sleep(2000);

		click("//div[@class='dialogbox-buttonarea']//a[@class='button'][contains(text(),'Apply')]");

	}

	public void task() throws Exception {

		Thread.sleep(3000);

		click(btn_taskdetails);

		if (isDisplayed(TaskDetailsTab)) {

			writeTestResults(" Go to \"Task Details Tab\" ", " Task details tab should be open",
					"task details tab viewed successfully", "pass");
		} else {

			writeTestResults(" Go to \"Task Details Tab\" ", " Task details tab should not open",
					"task details tab not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		click(btn_searchserviceproductfirst);

		if (isDisplayed(ProductLookUpPage)) {

			writeTestResults(" Click search to product", "Should be able to view product look up page",
					"product look up page viewed successfully", "pass");
		} else {

			writeTestResults(" Click search to product", "Should not able to view product look up page",
					"product look up page not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, txt_serviceProduct);

		Thread.sleep(4000);

		pressEnter(Product);

		Thread.sleep(4000);

		doubleClick(btn_product);

		Thread.sleep(2000);

//			sendKeys(EstimatedQtyservice, txt_EstimatedQtyservice);
		sendKeys(EstimatedQtyservice, "5");

		Thread.sleep(1000);

		click("//input[@class='unchecked el-resize15']");

		Thread.sleep(3000);

		click(btn_expand);

//		selectText(tasktype,txt_taskktype);

		selectText(tasktype, "Input Product");

		click(btn_searchserialproduct);

		if (isDisplayed(ProductLookUpPage)) {

			writeTestResults(" Click search to product", "Should be able to view product look up page",
					"product look up page viewed successfully", "pass");
		} else {

			writeTestResults(" Click search to product", "Should not able to view product look up page",
					"product look up page not  viewed successfully", "fail");
		}

		Thread.sleep(4000);

		selectText(template, txt_Template);

		sendKeys(Product, txt_SerialSpecificProduct);

		Thread.sleep(4000);

		pressEnter(Product);

		Thread.sleep(4000);

		doubleClick(btn_product);

		sendKeys(EstimatedQtyserial, txt_EstimatedQtyserial);

		Thread.sleep(4000);

//		click("//input[@class='unchecked el-resize15']");

	}

	public void selectbatchproduct() throws Exception {

		Thread.sleep(3000);

		click(btn_expand2);

		Thread.sleep(3000);

		click(btn_searchbatchproduct);

		Thread.sleep(3000);

		if (isDisplayed(ProductLookUpPage)) {

			writeTestResults(" Click search to product", "Should be able to view product look up page",
					"product look up page viewed successfully", "pass");
		} else {

			writeTestResults(" Click search to product", "Should not able to view product look up page",
					"product look up page not  viewed successfully", "fail");
		}

		Thread.sleep(4000);

		selectText(template, txt_Template);

		sendKeys(Product, txt_BatchSpecificProduct);

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(3000);

//			sendKeys(EstimatedQty, txt_EstimatedQty);

//			sendKeys(EstimatedBatch, txt_EstimatedQtyBatch);
		sendKeys(EstimatedBatch, "5");
	}

	public void selectFIFOproduct() throws Exception {

		Thread.sleep(4000);

		click(btn_expand3);

		Thread.sleep(3000);

		click(btn_searchFIFOproduct);

		Thread.sleep(3000);

		if (isDisplayed(ProductLookUpPage)) {

			writeTestResults(" Click search to product", "Should be able to view product look up page",
					"product look up page viewed successfully", "pass");
		} else {

			writeTestResults(" Click search to product", "Should not able to view product look up page",
					"product look up page not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		Thread.sleep(3000);

		sendKeys(Product, txt_FIFOProduct);

		Thread.sleep(3000);

		pressEnter(Product);

		Thread.sleep(5000);

		doubleClick(btn_product);

		Thread.sleep(2000);

//			sendKeys(EstimatedQtyFIFO, txt_EstimatedQtyFIFO);
		sendKeys(EstimatedQtyFIFO, "5");

	}

	public void selectSERVICEproduct() throws Exception {

		Thread.sleep(3000);

		click(btn_expand4);

		Thread.sleep(3000);

		selectText(tasktype, "Services");

		Thread.sleep(3000);

		click(btn_searchserviceproduct);

		Thread.sleep(3000);

		if (isDisplayed(ProductLookUpPage)) {

			writeTestResults(" Click search to product", "Should be able to view product look up page",
					"product look up page viewed successfully", "pass");
		} else {

			writeTestResults(" Click search to product", "Should not able to view product look up page",
					"product look up page not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		Thread.sleep(3000);

		sendKeys(Product, txt_serviceProduct);

		Thread.sleep(3000);

		pressEnter(Product);

		Thread.sleep(4000);

		doubleClick(btn_product);

		Thread.sleep(3000);

//			sendKeys(EstimatedQtyservice, txt_EstimatedQtyservice);
		sendKeys(EstimatedQtyservice, "5");

		Thread.sleep(1000);

		sendKeys(EstimatedValueService, "100");
	}

	public void selectlabour() throws Exception {

		Thread.sleep(3000);

		click(btn_expand5);

		Thread.sleep(3000);

		selectText(tasktypelabour, "Labour");

		click(btn_searchlabouremployee);

//			if(isDisplayed(LabourOwnerLookUpPage)) {
//				
//				 writeTestResults(" Click search to Employee" , "Should be able to view owner look up page", "Owner look up page viewed successfully", "pass");
//			}else {
//				
//				writeTestResults(" Click search to Employee"  , "Should not able to view owner look up page", "Owner look up page not  viewed successfully", "fail");
//			}

		Thread.sleep(3000);

//			 selectText(template,txt_Template);

		sendKeys(LabourEmployee, "rashi");

		Thread.sleep(3000);

		pressEnter(LabourEmployee);

		Thread.sleep(3000);

		doubleClick(btn_LabourEmployee);

		Thread.sleep(3000);

		click(btn_searchlabour);

		Thread.sleep(3000);

//			if(isDisplayed(ProductLookUpPage)) {
//				
//				 writeTestResults(" Click search to product" , "Should be able to view product look up page", "product look up page viewed successfully", "pass");
//			}else {
//				
//				writeTestResults(" Click search to product"  , "Should not able to view product look up page", "product look up page not  viewed successfully", "fail");
//			}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		Thread.sleep(3000);

		sendKeys(Product, txt_serviceProduct);

		Thread.sleep(3000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(3000);

		// sendKeys(EstimatedQtyLabour, txt_EstimatedQtyLabour);
		sendKeys(EstimatedQtyLabour, "5");

	}

	public void selectsubcontract() throws Exception {

		Thread.sleep(3000);

		click(btn_expand6);

		Thread.sleep(2000);

		selectText(tasktypesubcontract, "Sub Contract");

		Thread.sleep(3000);

		click(btn_searchsubcontract);

		Thread.sleep(3000);

		if (isDisplayed(ProductLookUpPage)) {

			writeTestResults(" Click search to product", "Should be able to view product look up page",
					"product look up page viewed successfully", "pass");
		} else {

			writeTestResults(" Click search to product", "Should not able to view product look up page",
					"product look up page not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, txt_serviceProduct);

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(3000);

//			sendKeys(EstimatedQtysubcontract, txt_EstimatedQtysubcontract);
		sendKeys(EstimatedQtysubcontract, "5");

		sendKeys(EstimatedValuesubcontract, "100");

		Thread.sleep(3000);

	}

	public void selectexpense() throws Exception {

		Thread.sleep(3000);

		click(btn_expand7);

		Thread.sleep(1000);

		selectText(tasktypeexpense, "Expense");

		Thread.sleep(3000);

		click(btn_searchexpense);

		Thread.sleep(3000);

		if (isDisplayed(ProductLookUpPage)) {

			writeTestResults(" Click search to product", "Should be able to view product look up page",
					"product look up page viewed successfully", "pass");
		} else {

			writeTestResults(" Click search to product", "Should not able to view product look up page",
					"product look up page not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		Thread.sleep(3000);

		sendKeys(Product, txt_serviceProduct);

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(2000);

		sendKeys(EstimatedQtyExpense, "5");

		Thread.sleep(3000);

		selectText(Referencecode, txt_Referencecode);

//			sendKeys(EstimatedQtyExpense, txt_EstimatedQtyExpense);

//			sendKeys(EstimatedQtyExpense, "5");

		Thread.sleep(3000);

		sendKeys("//*[@id=\"commonTbl\"]/tbody/tr[8]/td[14]/input", "100");

		Thread.sleep(1000);

	}

	public void selectlotproduct() throws Exception {

		Thread.sleep(3000);

		click(btn_expand8);

		Thread.sleep(3000);

		click(btn_searchlotproduct);

		Thread.sleep(3000);

		if (isDisplayed(ProductLookUpPage)) {

			writeTestResults(" Click search to product", "Should be able to view product look up page",
					"product look up page viewed successfully", "pass");
		} else {

			writeTestResults(" Click search to product", "Should not able to view product look up page",
					"product look up page not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		Thread.sleep(3000);

		sendKeys(Product, txt_lotProduct);

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(2000);

		sendKeys(EstimatedQtyLOT, "5");

	}

	public void selectSerialBatchProduct() throws Exception {

		Thread.sleep(3000);

		click(btn_expand9);

		Thread.sleep(2000);

		click(btn_searchBatchSerialproduct);

		Thread.sleep(3000);

		if (isDisplayed(ProductLookUpPage)) {

			writeTestResults(" Click search to product", "Should be able to view product look up page",
					"product look up page viewed successfully", "pass");
		} else {

			writeTestResults(" Click search to product", "Should not able to view product look up page",
					"product look up page not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, txt_BatchSerialProduct);

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(2000);

		sendKeys(EstimatedSerialBatchQty, "5");

	}

	public void selectResources() throws Exception {

		Thread.sleep(3000);

		click(btn_expand10);

		Thread.sleep(2000);

		selectText(tasktyperesource, "Resource");

		Thread.sleep(3000);

		click(btn_searchResourcesReferenceCode);

		Thread.sleep(3000);

		if (isDisplayed(ResourceLookUpPage)) {

			writeTestResults(" Click search to resource", "Should be able to view resource look up page",
					"resource look up page viewed successfully", "pass");
		} else {

			writeTestResults(" Click search to resourse", "Should not able to view resource look up page",
					"resource look up page not  viewed successfully", "fail");
		}

		Thread.sleep(4000);

		selectText(template, txt_Template);

//			 sendKeys(Resource,txt_Resource);

		Thread.sleep(2000);

		sendKeys(Resource, "REC-test-001");

		Thread.sleep(3000);

		pressEnter(Resource);

		Thread.sleep(3000);

		doubleClick(btn_Resource);

		Thread.sleep(2000);

		click(btn_searchResources);

		Thread.sleep(3000);

		if (isDisplayed(ProductLookUpPage)) {

			writeTestResults(" Click search to product", "Should be able to view product look up page",
					"product look up page viewed successfully", "pass");
		} else {

			writeTestResults(" Click search to product", "Should not able to view product look up page",
					"product look up page not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		Thread.sleep(3000);

		sendKeys(Product, txt_serviceProduct);

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(2000);

//		sendKeys(EstimatedQtyResourse, txt_EstimatedQtyResourse);
		sendKeys(EstimatedQtyResourse, "5");

	}

	public void draftandrelease() throws Exception

	{
		Thread.sleep(5000);

		click(btn_DraftServiceJobOrder);

		Thread.sleep(6000);

		click(btn_ReleaseServiceJobOrder);

		Thread.sleep(6000);

		trackCode = getText(ServiceJobOrderReleaseNoTC);

		Thread.sleep(3000);

		if (isDisplayed(ReleaseServiceJobOrderPage)) {

			writeTestResults("Click on \"Release\" Button", "User should be able to Release the service job order",
					"service job order Released successfully", "pass");
		} else {

			writeTestResults(" Click on \"Release\" Button", "User should not able toRelease the service job order",
					"service job order not  Released successfully", "fail");
		}

	}

	public void purchasedraftandrelease() throws Exception

	{

//		click(btn_ch);

		Thread.sleep(5000);

		click(btn_DraftServiceJobOrder);

		Thread.sleep(6000);

		click(btn_ReleaseServiceJobOrder);

		Thread.sleep(6000);

		trackCode = getText(ServiceJobOrderReleaseNoTC);

		if (isDisplayed(ReleaseServiceJobOrderPage)) {

			writeTestResults("Click on \"Release\" Button", "User should be able to Release the service job order",
					"service job order Released successfully", "pass");
		} else {

			writeTestResults(" Click on \"Release\" Button", "User should not able toRelease the service job order",
					"service job order not  Released successfully", "fail");
		}

	}

	public void createinternalorder() throws Exception {

		Thread.sleep(4000);

		click(btn_action);

		Thread.sleep(2000);
		click(btn_CreateInternalOrder);

		if (isDisplayed(GernerateInternalOrderPage)) {

			writeTestResults("Click on Create Internal Order Action",
					"\"Generate Internal Order\" look up should be displayed with the input product in service job order",
					"Generate Internal Order\\\" look up viewed successfully", "pass");
		} else {

			writeTestResults(" Click on Create Internal Order Action",
					"\"Generate Internal Order\" look up should not displayed with the input product in service job order",
					"Generate Internal Order\\\" look up not  viewed successfully", "fail");
		}

		Thread.sleep(2000);

		click(btn_selectall);

		Thread.sleep(2000);

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript(btn_applyCommon);

//			 if(isDisplayed(NewInternalOrderPage)) {
//					
//				 writeTestResults("Click On \"Apply\" Button" , "New Inrernal Order should be open with the details which are added from the \"Generate internal order\" look Up", "New Inrernal Order should be open successfully", "pass");
//			}else {
//				
//				writeTestResults(" Click On \"Apply\" Button"  , "New Inrernal Order not open with the details which are added from the \"Generate internal order\" look Up", "New Inrernal Order should not open successfully", "fail");
//			}

//			  pageRefersh();

		Thread.sleep(3000);

		switchWindow();

		click(btn_DraftIO);

		Thread.sleep(4000);

		click(btn_releaseInternalNewOrder);

		Thread.sleep(4000);

		trackCode = getText(InternalOrderReleaseNoTC);

		Thread.sleep(3000);

		if (isDisplayed(InternalOrderInformationPopUp)) {

			writeTestResults("User should be able to Release the Internal Order",
					" User should be able to Release the Internal Order",
					" Inrernal Order should be released successfully", "pass");
		} else {

			writeTestResults(" User should be able to Release the Internal Order",
					" User should not able to Release the Internal Order",
					" Inrernal Order  not should  released successfully", "fail");
		}

		Thread.sleep(2000);

		click(btn_gotopage);

//			 if(isDisplayed(InternalDispatchOrderPage)) {
//					
//				 writeTestResults(" Click on \\\"\\\"Go to page\\\"\\\" Link" , " Internal Dispatch order with filled details shoould be open in a new tab", " Internal Dispatch order with filled details shoould be open successfully", "pass");
//			}else {
//				
//				writeTestResults("Click on \"\"Go to page\"\" Link "  , " Internal Dispatch order with filled details shoould not open in a new tab", " Internal Dispatch order with filled details shoould not open successfully", "fail");
//			}

		Thread.sleep(2000);

		switchWindow();
//			 
		sendKeys(InternalDispatchOrderShippingAddress, txt_InternalDispatchOrderShippingAddress);

		Thread.sleep(3000);

		click(btn_DraftInternalDispatchOrder);

		Thread.sleep(3000);

		click(menu1);

		Thread.sleep(2000);

		click(serialWise);

		Thread.sleep(4000);

		String serialNoVal = getText(serialTextVal2_Service_TC_001);

		Thread.sleep(2000);

		click(serialMenuClose);

		Thread.sleep(3000);

		click(btn_SerialBatchInternalDispatchOrder);

		Thread.sleep(3000);

		if (isDisplayed(SerialorBatchnoPage)) {

			writeTestResults("User should be able to click serial batch",
					" User should be able to view the Internal Order", " serial or batch no feild viewed successfully",
					"pass");
		} else {

			writeTestResults(" User should be able to click serial batch",
					" User should not able to view the Internal Order",
					" serial or batch no feild  not viewed successfully", "fail");
		}

		Thread.sleep(3000);

		click(btn_product2);

		Thread.sleep(2000);

		click(btn_alterslide);

		Thread.sleep(2000);

		if (isDisplayed(ExistingSerial_BatchNosPage)) {

			writeTestResults("User should be able to click alterslide",
					" User should be able to view the ExistingSerial/BatchNosPage",
					" ExistingSerial/BatchNosPage viewed successfully", "pass");
		} else {

			writeTestResults(" User should be able to click alterslide",
					" User should not able to view the ExistingSerial/BatchNosPage",
					" ExistingSerial/BatchNosPage  not viewed successfully", "fail");
		}
//			 
//			 switchWindow();

		click(btn_addbatchproduct);

		Thread.sleep(2000);

		click(btn_refresh);

		Thread.sleep(3000);

		click(btn_product1);

		click(btn_range);

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$('#attrSlide-txtSerialCount').val(5)");

		runjQuery();

		Thread.sleep(2000);

		sendKeys(SerialProductOutboundShipment, serialNoVal);

		pressEnter(SerialProductOutboundShipment);
//			 
////			 String val ="SERIAL-026";
//			 JavascriptExecutor jse2 = (JavascriptExecutor)driver;
//			 jse2.executeScript("$('#attrSlide-txtSerialFrom').val('SERIAL-310')");

		Thread.sleep(1000);

		click(btn_refresh);

		Thread.sleep(2000);

		click(btn_arrowtointernaldistbatchorder);

		Thread.sleep(3000);
//			 
		click(btn_releaseinternaldispatchorder);

		Thread.sleep(6000);

		trackCode = getText(internaldispatchorderReleaseTC);

		if (isDisplayed(internaldispatchorderpage)) {

			writeTestResults("User should be able to Release the Internal Dispatch Order",
					" User should be able to Release the Internal Dispatch Order",
					" Inrernal Dispatch  Order should be released successfully", "pass");
		} else {

			writeTestResults(" User should be able to Release the Internal Dispatch Order",
					" User should not able to Release the Internal Dispatch Order",
					" Inrernal Dispatch  Order  not should  released successfully", "fail");
		}

		Thread.sleep(5000);

		String Serialspecific = getText(SerialSpecific);
		Thread.sleep(2000);

		System.out.println(Serialspecific);

		Thread.sleep(3000);

		String SerialBatch = getText(SerialBatchNAME);
		Thread.sleep(2000);

		System.out.println(SerialBatch);

		Thread.sleep(2000);

		String LotAuto = getText(LotAutoNAME);

		Thread.sleep(3000);

		System.out.println(LotAuto);

		Thread.sleep(2000);

		String FifoBatch = getText(FifoBatchNAME);
		Thread.sleep(2000);

		System.out.println(FifoBatch);

		Thread.sleep(2000);

		String Batchspecific = getText(BatchspecificNAME);

		Thread.sleep(3000);

		System.out.println(Batchspecific);

		closeWindow();

		switchWindow();

		/*
		 * closeWindow();
		 * 
		 * switchWindow();
		 */

		Thread.sleep(3000);

		pageRefersh();

		Thread.sleep(3000);

		click(btn_overview);

		Thread.sleep(5000);

		String SerialspecificServiceJobOrder = getText(SerialspecificServiceJobOrderName);

		Thread.sleep(5000);

		System.out.println(SerialspecificServiceJobOrder);

		Thread.sleep(3000);

		if (Serialspecific.replace(" ", "").equals(SerialspecificServiceJobOrder.replace(" ", ""))) {
			writeTestResults(" Check products to internal order is avilable for dispatch input products\"",
					" Link for internal order should be availbale with the dispatched input products\"",
					" Same products viewed successfully", "pass");
		} else {

			writeTestResults(" Check link to internal order is avilable for dispatch input products\"",
					"  Link for internal order should be availbale with the dispatched input products\"",
					"Same products not viewed successfully", "fail");
		}

		Thread.sleep(3000);

		click(btn_OrderNo1);

		Thread.sleep(4000);

		trackCode = getText(internaldispatchorderReleaseTC);

		Thread.sleep(4000);

		if (isDisplayed(InternalOrderPage)) {

			writeTestResults(" Check link to internal order is avilable for dispatch input products\"",
					" Link for internal order should be availbale with the dispatched input products\"",
					" internal order link viewed successfully", "pass");
		} else {

			writeTestResults(" Check link to internal order is avilable for dispatch input products\"",
					"  Link for internal order should be availbale with the dispatched input products\"",
					"internal order link is not  viewed successfully", "fail");
		}

		Thread.sleep(2000);

		switchWindow();

		closeWindow();

		Thread.sleep(4000);

		String SerialBatchServiceJobOrder = getText(SerialBatchServiceJobOrderName);

		Thread.sleep(3000);

		System.out.println(SerialBatchServiceJobOrder);

		Thread.sleep(3000);

		if (SerialBatch.replace(" ", "").equals(SerialBatchServiceJobOrder.replace(" ", ""))) {
			writeTestResults(" Check products to internal order is avilable for dispatch input products\"",
					" Link for internal order should be availbale with the dispatched input products\"",
					" Same products viewed successfully", "pass");
		} else {

			writeTestResults(" Check link to internal order is avilable for dispatch input products\"",
					"  Link for internal order should be availbale with the dispatched input products\"",
					"Same products not viewed successfully", "fail");
		}

		Thread.sleep(4000);

		click(btn_OrderNo5);

		Thread.sleep(4000);

		trackCode = getText(internaldispatchorderReleaseTC);

		Thread.sleep(4000);

		if (isDisplayed(InternalOrderPage)) {

			writeTestResults(" Check link to internal order is avilable for dispatch input products\"",
					" Link for internal order should be availbale with the dispatched input products\"",
					" internal order link viewed successfully", "pass");
		} else {

			writeTestResults(" Check link to internal order is avilable for dispatch input products\"",
					"  Link for internal order should be availbale with the dispatched input products\"",
					"internal order link is not  viewed successfully", "fail");
		}

		Thread.sleep(1000);

		switchWindow();

		closeWindow();

		Thread.sleep(4000);

		String LotAutoServiceJobOrder = getText(LotAutoServiceJobOrderName);

		Thread.sleep(3000);

		System.out.println(LotAutoServiceJobOrder);

		Thread.sleep(3000);

		if (LotAuto.replace(" ", "").equals(LotAutoServiceJobOrder.replace(" ", ""))) {
			writeTestResults(" Check products to internal order is avilable for dispatch input products\"",
					" Link for internal order should be availbale with the dispatched input products\"",
					" Same products viewed successfully", "pass");
		} else {

			writeTestResults(" Check link to internal order is avilable for dispatch input products\"",
					"  Link for internal order should be availbale with the dispatched input products\"",
					"Same products not viewed successfully", "fail");
		}

		Thread.sleep(4000);

		click(btn_OrderNo4);

		Thread.sleep(4000);

		trackCode = getText(internaldispatchorderReleaseTC);

		Thread.sleep(4000);

		if (isDisplayed(InternalOrderPage)) {

			writeTestResults(" Check link to internal order is avilable for dispatch input products\"",
					" Link for internal order should be availbale with the dispatched input products\"",
					" internal order link viewed successfully", "pass");
		} else {

			writeTestResults(" Check link to internal order is avilable for dispatch input products\"",
					"  Link for internal order should be availbale with the dispatched input products\"",
					"internal order link is not  viewed successfully", "fail");
		}

		Thread.sleep(1000);

		switchWindow();

		closeWindow();

		Thread.sleep(4000);

		String FifoBatchServiceJobOrder = getText(FifoBatchServiceJobOrderName);

		Thread.sleep(3000);

		System.out.println(FifoBatchServiceJobOrder);

		Thread.sleep(3000);

		if (FifoBatch.replace(" ", "").equals(FifoBatchServiceJobOrder.replace(" ", ""))) {
			writeTestResults(" Check products to internal order is avilable for dispatch input products\"",
					" Link for internal order should be availbale with the dispatched input products\"",
					" Same products viewed successfully", "pass");
		} else {

			writeTestResults(" Check link to internal order is avilable for dispatch input products\"",
					"  Link for internal order should be availbale with the dispatched input products\"",
					"Same products not viewed successfully", "fail");
		}

		Thread.sleep(4000);

		click(btn_OrderNo5);

		Thread.sleep(4000);

		trackCode = getText(internaldispatchorderReleaseTC);

		Thread.sleep(4000);

		if (isDisplayed(InternalOrderPage)) {

			writeTestResults(" Check link to internal order is avilable for dispatch input products\"",
					" Link for internal order should be availbale with the dispatched input products\"",
					" internal order link viewed successfully", "pass");
		} else {

			writeTestResults(" Check link to internal order is avilable for dispatch input products\"",
					"  Link for internal order should be availbale with the dispatched input products\"",
					"internal order link is not  viewed successfully", "fail");
		}

		Thread.sleep(1000);

		Thread.sleep(1000);

		switchWindow();

		closeWindow();

		Thread.sleep(4000);

		String BatchspecificServiceJobOrder = getText(BatchspecificServiceJobOrderName);

		Thread.sleep(3000);

		System.out.println(BatchspecificServiceJobOrder);

		Thread.sleep(3000);

		if (Batchspecific.replace(" ", "").equals(BatchspecificServiceJobOrder.replace(" ", ""))) {
			writeTestResults(" Check products to internal order is avilable for dispatch input products\"",
					" Link for internal order should be availbale with the dispatched input products\"",
					" Same products viewed successfully", "pass");
		} else {

			writeTestResults(" Check link to internal order is avilable for dispatch input products\"",
					"  Link for internal order should be availbale with the dispatched input products\"",
					"Same products not viewed successfully", "fail");
		}

		Thread.sleep(4000);

		click(btn_OrderNo2);

		Thread.sleep(3000);

		trackCode = getText(internaldispatchorderReleaseTC);

		Thread.sleep(4000);

		if (isDisplayed(InternalOrderPage)) {

			writeTestResults(" Check link to internal order is avilable for dispatch input products\"",
					" Link for internal order should be availbale with the dispatched input products\"",
					" internal order link viewed successfully", "pass");
		} else {

			writeTestResults(" Check link to internal order is avilable for dispatch input products\"",
					"  Link for internal order should be availbale with the dispatched input products\"",
					"internal order link is not  viewed successfully", "fail");
		}

		Thread.sleep(2000);

	}

	public void abletoactualupdate() throws Exception {

		Thread.sleep(4000);

		pageRefersh();

		Thread.sleep(3000);

		switchWindow();

		closeWindow();

		Thread.sleep(3000);

		click(btn_action);

		Thread.sleep(3000);

		click(btn_ActualUpdate);

		if (isDisplayed(actualupdatepage)) {

			writeTestResults(" Click on \"Actual Update\" Action", "  Actual Update Action should be Available",
					" Actual Update Action should be viewed successfully", "pass");
		} else {

			writeTestResults("  Click on \"Actual Update\" Action", "  Actual Update Action should not Available",
					" Actual Update Action not viewed successfully", "fail");
		}

		Thread.sleep(3000);

		click(btn_OpenSerialNoSelection);
		Thread.sleep(3000);

		if (isDisplayed(InputProductDetailsPopUp)) {

			writeTestResults(" Click on \"OpenSerialNoSelection\" Action",
					"  InputProductDetailsPopUp should be Available",
					" AInputProductDetailsPopUpAction should be viewed successfully", "pass");

		} else {

			writeTestResults("  Click on \"OpenSerialNoSelection\" Action",
					"  InputProductDetailsPopUp should not Available",
					"InputProductDetailsPopUp not viewed successfully", "fail");
		}

		click(btn_serialproduct);

		click(btn_update);

		Thread.sleep(1000);

		click(btn_actualUpdateConfiamation);

		Thread.sleep(200);

//		click(btn_close);

		click(btn_OpenBatchNoSelection);

		click(btn_NoOfSerialProduct);

		sendKeys(availableqty, txt_quty);

		Thread.sleep(3000);

		click(btn_update);
		Thread.sleep(1000);

		click(btn_actualUpdateConfiamation);

		Thread.sleep(200);

//		click(btn_close);

		Thread.sleep(2000);

		sendKeys(FifoActualQty, txt_FifoActualQty);

		Thread.sleep(2000);

		click(btn_updateinputproducts);

		click(btn_closeInputProductDetails);

		pageRefersh();

//		click(btn_CostingSummaary);

//			 if(isDisplayed(CostingSummaryPage)) {
//					
//				 writeTestResults(" Click on the \"Costing Summary Tab\" " , "  User should be able to click on the costing summary tab", "  Costing summary tab  be viewed successfully", "pass");
//			
//			 }else {
//				
//				writeTestResults("  Click on the \"Costing Summary Tab\" "  , "   User should not able to click on the costing summary tab", "Costing summary tab  not viewed successfully", "fail");
//			}
//			 
//		Thread.sleep(2000);
//
//		click(btn_material);

		click(btn_overview);

		Thread.sleep(4000);

		String serialspecificAQ = getText(serialspecificAQValue);

		Thread.sleep(3000);

		System.out.println(serialspecificAQ);

		Thread.sleep(3000);

		if (serialspecificAQ.equals("1")) {
			writeTestResults("Verify actual update values are displayed ", " Actual updated values should be displayed",
					" Actual updated values displayed successfully", "pass");
		} else {

			writeTestResults(" Verify actual update values are displayed ",
					" Actual updated values should be displated", "Actual updated values not displayed successfully",
					"fail");
		}

		Thread.sleep(4000);

		String BatchspecificAQ = getText(BatchspecificAQValue);

		Thread.sleep(3000);

		System.out.println(BatchspecificAQ);

		Thread.sleep(3000);

		if (serialspecificAQ.equals("1")) {
			writeTestResults("Verify actual update values are displayed ", " Actual updated values should be displayed",
					" Actual updated values displayed successfully", "pass");
		} else {

			writeTestResults(" Verify actual update values are displayed ",
					" Actual updated values should be displated", "Actual updated values not displayed successfully",
					"fail");
		}

		Thread.sleep(4000);

		String FIFOBATCHAQ = getText(FIFOBATCHAQValue);

		Thread.sleep(3000);

		System.out.println(FIFOBATCHAQ);

		Thread.sleep(3000);

		if (serialspecificAQ.equals("1")) {
			writeTestResults("Verify actual update values are displayed ", " Actual updated values should be displayed",
					" Actual updated values displayed successfully", "pass");
		} else {

			writeTestResults(" Verify actual update values are displayed ",
					" Actual updated values should be displated", "Actual updated values not displayed successfully",
					"fail");
		}

		Thread.sleep(4000);

		String autotestingAQ = getText(autotestingAQValue);

		Thread.sleep(3000);

		System.out.println(autotestingAQ);

		Thread.sleep(3000);

		if (autotestingAQ.equals("5.00")) {
			writeTestResults("Verify actual update values are displayed ", " Actual updated values should be displayed",
					" Actual updated values displayed successfully", "pass");
		} else {

			writeTestResults(" Verify actual update values are displayed ",
					" Actual updated values should be displated", "Actual updated values not displayed successfully",
					"fail");
		}

		Thread.sleep(4000);

		String SerialBatchAQ = getText(SerialBatchAQValue);

		Thread.sleep(3000);

		System.out.println(SerialBatchAQ);

		Thread.sleep(5000);

		if (SerialBatchAQ.equals("5")) {
			writeTestResults("Verify actual update values are displayed ", " Actual updated values should be displayed",
					" Actual updated values displayed successfully", "pass");
		} else {

			writeTestResults(" Verify actual update values are displayed ",
					" Actual updated values should be displated", "Actual updated values not displayed successfully",
					"fail");
		}

	}

	public void abletoactualupdateresorce() throws Exception {

		pageRefersh();

		Thread.sleep(2000);

		click(btn_action);

		click(btn_ActualUpdate);

		click(btn_resource);

		click(btn_actualduration);

//		  sendKeys(btn_actualduration,"09");

		click(btn_date);

		Thread.sleep(1000);

		click(btn_okActualDuration);

		Thread.sleep(2000);

//		String EstimatedDuration = getText(EstimatedDurationValue);
//		
//		Thread.sleep(2000);
//		
//		System.out.println(EstimatedDuration);

		click(btn_updateActualDuration);

		click(btn_close);

		pageRefersh();

		Thread.sleep(3000);

		click(btn_overview);

		Thread.sleep(3000);

//		click(btn_resource);

		click(btn_ResourceOverview);

		Thread.sleep(3000);

		if (isDisplayed(ActualWorkpage)) {

			writeTestResults("Click on Resource Tab",
					" \"\"Actual WOrk\"\" column should be filled with the Actual Updated Qty of relavant Resource\"",
					" \"\"Actual WOrk\"\" column  filled with the Actual Updated Qty of relavant Resource\"  successfully",
					"pass");

		} else {

			writeTestResults("  Click on Resource Tab",
					"  \"\"Actual WOrk\"\" column not filled with the Actual Updated Qty of relavant Resource\"",
					"\"\"Actual WOrk\"\" column not filled with the Actual Updated Qty of relavant Resource\"", "fail");
		}

		Thread.sleep(1000);

//		click(btn_CostingSummaary);
//
//		Thread.sleep(2000);
//
//		click(btn_ResourseCost);
//		

	}

	// daily worksheet

	public void abletoLabourViaDailyWorkSheet() throws Exception {
		click(btn_navigationmenu);

		if (isDisplayed(sidemenu)) {
			writeTestResults("Click on the top navigation menu", "System should be loaded the menu item successfully",
					"Menu item is displayed", "pass");
		} else {
			writeTestResults("Click on the top navigation menu", "System should not  loaded the menu item successfully",
					"Menu item is not displayed", "fail");
		}

		click(btn_servicemodule);

		if (isDisplayed(subsidemenu)) {

			writeTestResults("Click on the Service module", "System should be loaded the Service module successfully",
					"subsidemenu is displayed", "pass");
		} else {

			writeTestResults("Click on the Service module", "System should not loaded the Service module successfully",
					"subsidemenu is not displayed", "fail");
		}

		click(btn_DailyWorkSheet);

		if (isDisplayed(DailyWorkSheetPage)) {

			writeTestResults(" Click on Daily Work Sheet form", "User should be able to click on the Daily wrk sheet",
					"Daily Worksheet tab displayed successfully", "pass");
		} else {

			writeTestResults(" Click on Daily Work Sheet form", "User should not able to click on the Daily wrk sheet",
					"Daily Worksheet tab is not  displayed successfully", "fail");
		}

		click(btn_NewDailyWorkSheet);

		if (isDisplayed(StartNewJourneyForm)) {

			writeTestResults(" Click on \"New Daily Worksheet\" Button", "Click on \"New Daily Worksheet\" Button",
					"New Journey form displayed successfully", "pass");
		} else {

			writeTestResults(" Click on \"New Daily Worksheet\" Button", "Click on \"New Daily Worksheet\" Button",
					"New Journey form is not  displayed successfully", "fail");
		}

		click(btn_DailyWorkSheetServiceJourney);

		Thread.sleep(2000);

		click(btn_addnewrecord);

		click(btn_SearchJobNo);

		Thread.sleep(3000);

		selectText(templateJOBNO, "My Template");

		Thread.sleep(3000);

		sendKeys(searchjobno, txt_searchjobno);

		pressEnter(searchjobno);

		Thread.sleep(3000);

		doubleClick(btn_jobno);

	}

//		 public void SearchJobNo() throws Exception 
//	     {
//		
//		 selectText(templateJOBNO,txt_templateJOBNO);
//		
//		 sendKeys(searchjobno,txt_searchjobno);
//		
//		 pressEnter(searchjobno);
//		 
//		 Thread.sleep(3000);
//
//		 if(isDisplayed(txt_searchjobno)) {
//			
//		  writeTestResults("Click on the serach button", "The search button should be clicked and data should be dispalyed","user is displayed successfully","pass");}
//		 
//		 else {
//			
//			writeTestResults("Click on the serach button", "The search button should be clicked and data should not dispalyed","user is not displayed successfully","fail");
//			
//			}
//		 
//		 click(btn_jobno);
//		 
//		 
//				
//	}

	public void SearchJobNo() throws Exception {

//		click(btn_searchservicelocation); 
//			
//		 selectText(templateJOBNO,txt_templateJOBNO);

		sendKeys(searchjobno, txt_searchjobno);

		pressEnter(searchjobno);

		Thread.sleep(3000);

		doubleClick(btn_jobno);
	}

	public void CreateNewEmployee() throws Exception {

		click(btn_navigationmenu);

		if (isDisplayed(sidemenu)) {
			writeTestResults("Click on the top navigation menu", "System should be loaded the menu item successfully",
					"Menu item is displayed", "pass");
		} else {
			writeTestResults("Click on the top navigation menu", "System should not  loaded the menu item successfully",
					"Menu item is not displayed", "fail");
		}

		click(btn_OrganizationManagement);

		click(btn_EmployeeInformation);

		click(btn_NewEmployee);

		Thread.sleep(3000);

		LocalTime myObj = LocalTime.now();
		sendKeys(employeecode, txt_employeecode + myObj);

		no = txt_employeecode + myObj;
		System.out.println(no);

		// EmployeeCode = getText(employeecode);

		sendKeys(employeename, txt_employeename);

		getText(employeecode);

		sendKeys(namewithintials, txt_namewithintials);

		selectText(employeegroup, txt_employeegroup);

		Thread.sleep(1000);

		click(btn_searchworkingcalander);

		Thread.sleep(1000);

		sendKeys(workingcalender, "WC_Resources");

		pressEnter(workingcalender);

		Thread.sleep(1000);

		doubleClick(workingcalendernew);

		click(btn_searchrateprofile);

		Thread.sleep(1000);

//			 pressEnter(btn_ratesearch);

		sendKeys(rateprofile, "600");

		Thread.sleep(1000);

		pressEnter(rateprofile);

		doubleClick(rateprofilevalue);

		click(payableaccount);

		Thread.sleep(1000);

		click(btn_DraftIO);

		Thread.sleep(2000);

		click(btn_ReleaseEmployee);

		Thread.sleep(2000);
	}

	public void SearchEmployee() throws Exception {

		click(btn_searchemployee);

		if (isDisplayed(employeelookup)) {

			writeTestResults("Click on the search button",
					"The search button should be clicked and  emplyee data should be dispalyed",
					"user is displayed successfully", "pass");
		}

		else {

			writeTestResults("Click on the search button",
					"The search button should be clicked and  employee data should not dispalyed",
					"user is not displayed successfully", "fail");

		}

		Thread.sleep(4000);

		// selectText(template,"My Template");

		sendKeys(searchemployee, no);

		Thread.sleep(2000);

		pressEnter(searchemployee);

		Thread.sleep(1000);

		doubleClick(btn_employee);

		Thread.sleep(3000);

//		 selectText(templateJOBNO,"My Template");
//		 
//		 sendKeys(searchjobno,txt_searchjobno);
//			
//		 pressEnter(searchjobno);
//		 
//		 Thread.sleep(3000);
//		
//		 doubleClick(btn_jobnoServiceJobOrder);	
//		
//		 Thread.sleep(3000);

		click(btn_timein);

		click(timein);

		Thread.sleep(1000);

		click(btn_oktime);

		Thread.sleep(1000);

		click(btn_timeout);

		Thread.sleep(1000);

		click(timeout);

		Thread.sleep(1000);

		click(btn_oktimeout);

		Thread.sleep(1000);

		click(btn_updateNewJob);

		Thread.sleep(2000);

		click(btn_WorkedHrsDraft);

		Thread.sleep(3000);

		click(btn_WorkedHrsRelease);

		Thread.sleep(5000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleasedPage)) {

			writeTestResults("Click on \"Release\" icon", "User should be able to Release the service job order",
					"ServiceJobOrder released successfully", "pass");
		}

		else {

			writeTestResults("Click on \"Release\" icon", "User should be able to Release the service job order",
					"ServiceJobOrder not  released successfully", "fail");

		}
	}

	public void ServiceJobOrderDailyWorksheet() throws Exception {

		Thread.sleep(2000);

		click(btn_navigationmenu);

		Thread.sleep(2000);

		if (isDisplayed(sidemenu)) {
			writeTestResults("Click on the top navigation menu", "System should be loaded the menu item successfully",
					"Menu item is displayed", "pass");
		} else {
			writeTestResults("Click on the top navigation menu", "System should not  loaded the menu item successfully",
					"Menu item is not displayed", "fail");
		}

		Thread.sleep(2000);

		click(btn_servicemodule);

		if (isDisplayed(subsidemenu)) {

			writeTestResults("Click on the Service module", "System should be loaded the Service module successfully",
					"subsidemenu is displayed", "pass");
		} else {

			writeTestResults("Click on the Service module", "System should not loaded the Service module successfully",
					"subsidemenu is not displayed", "fail");
		}

		click(btn_ServiceJobOrder);

		if (isDisplayed(ServiceJobOrderPage)) {

			writeTestResults("Click on Service job order under menu",
					"Service job order should be available in the menu", "Service job order is displayed", "pass");
		} else {

			writeTestResults("Click on Service job order under menu",
					"Service job order should not available in the menu", "Service job order is not displayed", "fail");
		}

		selectText(templateJOBNO, "My Template");

		sendKeys(searchjobno, txt_searchjobno);

		Thread.sleep(2000);

		pressEnter(searchjobno);

		Thread.sleep(3000);

		doubleClick(btn_jobnoServiceJobOrder);

		Thread.sleep(3000);

		click(btn_overview);

		Thread.sleep(2000);

		if (isDisplayed(OverviewTabPage)) {

			writeTestResults(".Click on Overview Tab", " User should be able to click on the Overview tab",
					"Overview page is displayed", "pass");
		} else {

			writeTestResults(".Click on Overview Tab", " User should not able to click on the Overview tab",
					"Overview page not displayed", "fail");
		}

		Thread.sleep(2000);

		click(btn_labour);

//				 if(isDisplayed(LabourTabPage)) {
//						
//					 writeTestResults("Click on \"\"LabourTab\"\" ", "\"\"Actual WOrk\"\" column should be filled with the Actual Updated Qty of relavant labour via Daily Work Sheet", "Actual Updated Qty is displayed", "pass");
//				}else {
//					
//					writeTestResults("Click on \"\"LabourTab\"\" ", "\"\"Actual WOrk\"\" column should not filled with the Actual Updated Qty of relavant labour via Daily Work Sheet", "Actual Updated Qty is not displayed", "fail");
//				}
//
//		Thread.sleep(3000);
//
//		click(btn_CostingSummaary);
//
//		Thread.sleep(2000);
//
//		click(btn_hour);

	}

	public void SubcontractOrderAction() throws Exception {

		Thread.sleep(3000);

		click(btn_action);

		Thread.sleep(2000);

//		click(btn_SubContractOrder);

		click("//*[@id=\"permissionBar\"]/div/a[11]");

		Thread.sleep(1000);

		if (isDisplayed(SubContractOrderPage)) {

			writeTestResults("Click on Sub Contract Orders Drop down ",
					"User should be able to select a vendor from vendor look up",
					"User  able to select a vendor from vendor look up successfully", "pass");
		} else {

			writeTestResults("Click on Sub Contract Orders Drop down",
					"User should not able to select a vendor from vendor look up",
					"User not able to select a vendor from vendor look up successfully", "fail");
		}

		Thread.sleep(1000);

		click(btn_searchvendor);

		selectText(template, txt_Template);
		Thread.sleep(1000);

		sendKeys(searchvendor, txt_searchvendor);

		Thread.sleep(1000);

		pressEnter(searchvendor);

		Thread.sleep(3000);

		doubleClick(btn_vendor);

		Thread.sleep(2000);

		click(btn_checkboxsubcontractorder);

		Thread.sleep(1000);

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript(btn_applyCommon);

		Thread.sleep(1000);

		if (isDisplayed(NewPurchaseOrderPage)) {

			writeTestResults(" Click On Apply",
					" Check the vendor details in Purchase order is same which was added via sub contract order look up",
					"vendor details in Purchase order is same which was added via sub contract order look up", "pass");
		} else {

			writeTestResults(" Click On Apply",
					" Check the vendor details in Purchase order not same which was added via sub contract order look up",
					"vendor details in Purchase order is not  same which was added via sub contract order look up",
					"fail");
		}

		Thread.sleep(3000);

		switchWindow();

		Thread.sleep(1000);

//		     sendKeys(VendorRefrenceNo, txt_VendorRefrenceNo);

		sendKeys(VendorRefrenceNo, "test-002");

		click(btn_SearchBillingAddress);
		Thread.sleep(2000);

		sendKeys(BillingAddress, txt_BillingAddress);

		js.executeScript(btn_applyCommon);
		Thread.sleep(2000);

		doubleClick(btn_CheckOutPurchaseOrder);

		Thread.sleep(3000);

		click(btn_DraftPurchaseOrder);

		Thread.sleep(2000);

		if (isDisplayed(DraftPurchaseOrderPage)) {

			writeTestResults(" Click on \"Draft\" Button", " User should be able to \"Release\" the purchase order",
					"User  able to \"Release\" the purchase order successfully", "pass");
		} else {

			writeTestResults(" Click on \"Draft\" Button", " User should not able to \"Release\" the purchase order",
					"User not able to \"Release\" the purchase order successfully", "fail");
		}

		Thread.sleep(4000);

		click(btn_ReleasePurchaseOrder);

		Thread.sleep(4000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleaseInvoiceProposalPage)) {

			writeTestResults("  Click on \"Release\" Button", "   User should be able to Release the PurchaseOrder",
					" User able to Release the PurchaseOrder successfully", "pass");
		} else {

			writeTestResults("  Click on \"Release\" Button", "    User should be able to Release the PurchaseOrder",
					"User not able to Release the PurchaseOrder successfully", "fail");
		}

		Thread.sleep(2000);

		click(btn_gotopagePurchaseOrder);

		Thread.sleep(2000);

		if (isDisplayed(NewPurchaseInvoice)) {

			writeTestResults("  Click on \"\"Go To page \"\" link",
					"  User should be able to click on the \"\"Go to page \"\" link",
					"User  able to  click on the \\\"\\\"Go to page \\\"\\\" link successfully", "pass");
		} else {

			writeTestResults("  Click on \"\"Go To page \"\" link",
					"  User should not able to click on the \"\"Go to page \"\" link",
					"User not able to  click on the \\\"\\\"Go to page \\\"\\\" link successfully", "fail");
		}

		Thread.sleep(2000);

		switchWindow();

		Thread.sleep(1000);

		click(btn_checkoutpurchaseinvoice);

		Thread.sleep(2000);

		click(btn_DraftPurchaseInvoice);

		Thread.sleep(3000);

		click(btn_ReleasePurchaseInvoice1);

		Thread.sleep(2000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleasePurchaseInvoicePage)) {

			writeTestResults("  Click on \"Release\" Button", "   User should be able to Release the PurchaseInvoice",
					" User able to Release the  PurchaseInvoice successfully", "pass");
		} else {

			writeTestResults("  Click on \"Release\" Button", "    User should be able to Release the PurchaseInvoice",
					"User not able to Release the  PurchaseInvoice successfully", "fail");
		}

		Thread.sleep(3000);

		closeWindow();

		Thread.sleep(1000);

//			closeWindow();
//			
//			Thread.sleep(1000);

		switchWindow();

		Thread.sleep(3000);

		pageRefersh();

		click(btn_overview);

		Thread.sleep(3000);

		click(btn_SubContractServices);

		Thread.sleep(3000);

		click(btn_PurchaseOrderLink);

		Thread.sleep(3000);

		if (isDisplayed(PurchaseOrderPage)) {

			writeTestResults(" Check the purchase order and purchase invoice clumns are filled with the links",
					"  Links for purchase order and purchase invocie should be avilable ",
					"User  able to  click on the link successfully", "pass");
		} else {

			writeTestResults("Check the purchase order and purchase invoice clumns are filled with the links",
					" Links for purchase order and purchase invocie should be avilable ",
					"User not able to  click on the link successfully", "fail");
		}

		Thread.sleep(2000);

		switchWindow();

		closeWindow();

		Thread.sleep(2000);

		click(btn_PurchaseInvoiceLink);

		if (isDisplayed(PurchaseInvoicePage)) {

			writeTestResults(
					" Check the purchase order and purchase invoice clumns are filled with the linksClick on \"\"Go To page \"\" link",
					"  Links for purchase order and purchase invocie should be avilable",
					"User  able to  click on the link successfully", "pass");
		} else {

			writeTestResults(" Check the purchase order and purchase invoice clumns are filled with the links",
					"  Links for purchase order and purchase invocie should be avilable",
					"User not able to  click on the link successfully", "fail");
		}

		switchWindow();

		Thread.sleep(2000);

		click(btn_CostingSummaary);

		click(btn_hour);
	}

	public void InvoiceProposal() throws Exception {

		Thread.sleep(3000);

		pageRefersh();

		Thread.sleep(3000);

		switchWindow();

		click(btn_action);

		Thread.sleep(4000);

		click(btn_CreateInvoiceProposal);

		Thread.sleep(3000);

		if (isDisplayed(InvoiceProposalPopup)) {

			writeTestResults(" Click on the Create invoice Proposal Action",
					"   \"Invoice Proposal\" look up should be displayed",
					" \\\"Invoice Proposal\\\" look up viewed successfully", "pass");
		} else {

			writeTestResults(" Click on the Create invoice Proposal Action",
					"   \\\"Invoice Proposal\\\" look up should not displayed",
					"\\\\\\\"Invoice Proposal\\\\\\\" look up is not viewed successfully", "fail");
		}

		Thread.sleep(2000);

//		click("//*[@id=\"comOrderTbl\"]/tbody/tr/td[11]/span");
//		
//		Thread.sleep(3000);
//			
//		 selectText(template,txt_Template);
//		 
//		 sendKeys(Product,txt_serviceProduct);
//		 
//		 Thread.sleep(2000);	
//		 
//		 pressEnter(Product);
//		 
//		 Thread.sleep(3000);
//		
//		doubleClick(btn_product);	

		Thread.sleep(2000);

		sendKeys(BillableQtyInputProduct, txt_BillableQtyInputProduct);

		click(btn_InputproductCreateInvoiceProposal);

		sendKeys(BillableQtyInvoiceResource, txt_BillableQtyInvoiceResource);

		click(btn_ResourceCreateInvoiceProposal);

		Thread.sleep(2000);

		click("//*[@id=\"divProjectActions\"]/div[2]/div/div[1]/div/input");

		Thread.sleep(2000);

		doubleClick(btn_checkoutCreateInvoiceProposal);

		Thread.sleep(3000);

		click(btn_applyCreateInvoiceProposal);

		Thread.sleep(2000);

		if (isDisplayed(NewInvoiceProposalPage)) {

			writeTestResults("  Click On Apply",
					"  User should be able to click on the checkout button in the invoice proposal look up",
					" \\\"Invoice Proposal\\\" page viewed successfully", "pass");
		} else {

			writeTestResults("  Click On Apply",
					"   User should not able to click on the checkout button in the invoice proposal look up",
					"\\\\\\\"Invoice Proposal\\\\\\\" page is not viewed successfully", "fail");
		}

		Thread.sleep(3000);

		switchWindow();

//		   switchWindow();

		selectText(SalesUnit, txt_SalesUnit);

		Thread.sleep(2000);

		click(btn_checkoutInvoiceProposal);

		Thread.sleep(4000);

		click(btn_DraftInvoiceProposal);

		Thread.sleep(5000);

		click(btn_ReleaseInvoiceProposal);

		Thread.sleep(6000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleasedInvoiceProposalPage)) {

			writeTestResults("  Click on \"Release\" Button", "   User should be able to Release the service job order",
					" User able to Release the InvoiceProposal successfully", "pass");
		} else {

			writeTestResults("  Click on \"Release\" Button",
					"    User should be able to Release the service job order",
					"User not able to Release the InvoiceProposal successfully", "fail");
		}

		Thread.sleep(3000);

		click(btn_GoToPageInvoiceProposal);

		Thread.sleep(3000);

		switchWindow();

		click(btn_ServiceDetails);

		Thread.sleep(3000);

//		 click(btn_checkoutServiceInvoice);

		click("//*[@id=\"btnCalc2\"]");

		Thread.sleep(4000);

		click(btn_DraftServiceInvoice);

		if (isDisplayed(DraftServiceInvoice)) {

			writeTestResults("  Click on \"Draft\" Button", "   User should be able to Draft the service invoice",
					" User able to Draft the Service Invoice successfully", "pass");
		} else {

			writeTestResults("  Click on \"Draft\" Button", "    User should be able to Draft the service invoice",
					"User not able to Draft the Service Invoice successfully", "fail");
		}

		Thread.sleep(4000);

		click(btn_ReleaseServiceInvoice);

		Thread.sleep(6000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleaseServiceInvoicePage)) {

			writeTestResults("  Click on \" Release \" Button",
					"   User should be able to Release  the service invoice",
					" User able to Release  the Service Invoice successfully", "pass");
		} else {

			writeTestResults("  Click on \" Release \" Button",
					"    User should be able to  Release  the service invoice",
					"User not able to  Release  the Service Invoice successfully", "fail");
		}

	}

	public void CreateServiceQuatation() throws Exception {
//			 click(btn_servicemodule);
//
//				if(isDisplayed(subsidemenu)) {
//					
//					 writeTestResults("Click on the Service module", "System should be loaded the Service module successfully", "subsidemenu is displayed", "pass");
//				}else {
//					
//					writeTestResults("Click on the Service module", "System should not loaded the Service module successfully", "subsidemenu is not displayed", "fail");
//				}
//				
//				click(btn_ServiceJobOrder);
//
//				if(isDisplayed(ServiceJobOrderPage)) {
//					
//					 writeTestResults("Click on Service job order under menu", "Service job order should be available in the menu", "Service job order is displayed", "pass");
//				}else {
//					
//					writeTestResults("Click on Service job order under menu", "Service job order should not available in the menu", "Service job order is not displayed", "fail");
//				}
//				
//				 Thread.sleep(3000);
//				 
//			 selectText(templateJOBNO,"My Template");
//			 
//			 sendKeys(searchjobno,txt_searchjobno);
//				
//			 pressEnter(searchjobno);
//			 
//			 Thread.sleep(3000);
//			
//			doubleClick(btn_jobnoServiceJobOrder);	
//			
		switchWindow();
		Thread.sleep(3000);

		click(btn_action);

		click(btn_CreateServiceQuatation);

		Thread.sleep(3000);

		switchWindow();

		click(btn_ServiceDetailsServiceQuatation);

		Thread.sleep(1000);

		sendKeys(UnitPriceInputSerial, txt_UnitPriceInputSerial);

//			 Thread.sleep(1000);

//			 click(UnitPriceSubContract);
//			 
//			 sendKeys(UnitPriceSubContract, txt_UnitPriceSubContract);
//			 
		sendKeys(UnitPriceSubContract, "100");

//			 Thread.sleep(1000);
//			 
		sendKeys(UnitPriceService, txt_UnitPriceService);

		sendKeys(UnitPriceBatch, txt_UnitPriceBatch);

		sendKeys(UnitPriceFifo, txt_UnitPriceFifo);

		sendKeys(UnitPriceLabour1, "100");

		sendKeys(UnitPriceLabour, "100");

		sendKeys(UnitPriceExpense, "100");

		Thread.sleep(2000);

		click(btn_checkoutServiceQuatation);

		Thread.sleep(1000);

		click(btn_DraftServiceQuatation);

		Thread.sleep(1000);

		if (isDisplayed(DraftServiceQuatationPage)) {

			writeTestResults("Click on \"Draft\" Button", "User should be able to Draft the service Quotationorder",
					"User  able to Draft  the service Quotation successfully", "pass");
		} else {

			writeTestResults("Click on \"Draft\" Button", "User should not able to Draft  the service Quotation",
					"User not able to Draft  the service Quotation successfully", "fail");
		}

		Thread.sleep(3000);

		click(btn_ReleaseServiceQuatation);

		Thread.sleep(3000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleasedServiceQuatationPage)) {

			writeTestResults("Click on \"Release\" Button", "User should be able to Release the service Quotationorder",
					"User  able to Release the service Quotation successfully", "pass");
		} else {

			writeTestResults("Click on \"Release\" Button", "User should not able to Release the service Quotation",
					"User not able to Release the service Quotation successfully", "fail");
		}

		Thread.sleep(1000);

		click(btn_version);

		mouseMove(version);

		click(btn_tick);

		Thread.sleep(1000);

		click(btn_confirm);

		Thread.sleep(1000);

		click(btn_gotopageServiceQuatation);

		Thread.sleep(1000);

		switchWindow();

		click(btn_ServiceDetailsServiceOrder);

		Thread.sleep(2000);

		click(btn_checkoutServiceOrder);

		Thread.sleep(2000);

		click(btn_draftServiceOrder);

		Thread.sleep(2000);

		click(btn_ReleaseServiceOrder);

		Thread.sleep(2000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleasedServiceOrderPage)) {

			writeTestResults("Click on \"Release\" Button", "User should be able to Release the ServiceOrder",
					"User  able to Release the ServiceOrder successfully", "pass");
		} else {

			writeTestResults("Click on \"Release\" Button", "User should not able to Release the ServiceOrder",
					"User not able to Release the ServiceOrder successfully", "fail");
		}

		Thread.sleep(2000);

		closeWindow();

		switchWindow();

		/*
		 * closeWindow();
		 * 
		 * switchWindow();
		 */

		Thread.sleep(2000);
		pageRefersh();

		Thread.sleep(2000);

		click(btn_overview);

		click(btn_QuotationDetails);

	}

	public void InternalReturnOrder() throws Exception {

		Thread.sleep(3000);

		pageRefersh();

		click(btn_action);

		click(btn_InternalReturnOrder);

		Thread.sleep(3000);

		click(btn_SearchInternalReturnOrder);

		Thread.sleep(3000);

		click(btn_productreturn);

		Thread.sleep(3000);

		click(btn_applytoreturn);

		Thread.sleep(3000);

		switchWindow();

//			 selectText(ToWareHouse, txt_ToWareHouse);

		selectText(ToWareHouse, "WH-test-001-Allo [Rashi-test-auto]");

		Thread.sleep(3000);

		click(btn_DraftInternalReturnOrder);

		Thread.sleep(3000);

		if (isDisplayed(DraftInternalReturnOrderPage)) {

			writeTestResults("Draft the document", " User should be able to draft the internal return order",
					"Internal return order drafted successfully", "pass");
		} else {

			writeTestResults("Draft the document", " User should not able to draft the internal return order",
					"Internal return order not drafted successfully", "fail");
		}

		Thread.sleep(2000);

		click(btn_SerialbatchInternalReturnOrder);

		Thread.sleep(3000);

		click(btn_serialproducttocapture);

		Thread.sleep(2000);

		click(btn_updateInternalReturnOrder);

		Thread.sleep(1000);

		click(btn_backInternalReturnOrder);

		Thread.sleep(2000);

		click(btn_ReleaseInternalReturnOrder);

		Thread.sleep(3000);

		trackCode = getText(InternalReturnOrderReleaseNoTC);

		Thread.sleep(3000);

		if (isDisplayed(ReleasedInternalReturnOrderPage)) {

			writeTestResults("Release the document", " User should be able to Release the internal return order",
					"Internal return order released successfully", "pass");
		} else {

			writeTestResults("Release the document", " User should not able to Release internal return order",
					"Internal return order not released successfully", "fail");
		}

		Thread.sleep(2000);

		click(btn_GoToPageInternalReturnOrder);

		Thread.sleep(2000);

		if (isDisplayed(NewInternalReceiptPage)) {

			writeTestResults("Click on Go to link", "  user should be able to go to the internal receipt form",
					"internal receipt form viewed successfully", "pass");
		} else {

			writeTestResults("Click on Go to link", "  user should not able to go to the internal receipt form",
					"internal receipt form not  viewed successfully successfully", "fail");
		}

		switchWindow();

		Thread.sleep(3000);

		click(btn_Draftinternalreceipt);

//			 if(isDisplayed(DraftedInternalReceiptPage)) {
//					
//				 writeTestResults("Draft the Internal Receipt", " User should be able to Draft the Internal Receipt internal receipt form", "internal receipt form drafted successfully", "pass");
//			}else {
//				
//				writeTestResults("Draft the Internal Receipt", " User should not able to Draft the Internal Receipt", "internal receipt form not  drafted successfully successfully", "fail");
//			}

		Thread.sleep(4000);

		click(btn_Releaseinternalreceipt);

		Thread.sleep(4000);

		trackCode = getText(ReleasedInternalReceiptTC);

		if (isDisplayed(ReleasedInternalReceipt)) {

			writeTestResults("Release the InternalReceipt", " User should be able to Release the InternalReceipt",
					"InternalReceipt released successfully", "pass");
		} else {

			writeTestResults("Release the InternalReceipt", " User should not able to Release InternalReceipt",
					"InternalReceipt not released successfully", "fail");
		}
//			 closeWindow();
//			 
//			 closeWindow();
//			 
//			 switchWindow();
//			 
//			 closeWindow();
//			 
//			 pageRefersh();

//			 Thread.sleep(2000);
//			 
//			 click(btn_overview);

	}

	public void Paymentvouchers() throws Exception {

		click(btn_Finance);

		if (isDisplayed(FinanceSubSideMenu)) {

			writeTestResults(" Click on Finance Module", "  User should be able to click on the Finance modle ",
					"user able to click finance module successfully", "pass");
		} else {

			writeTestResults(" Click on Finance Module", "  User should be not able to click on the Finance modle ",
					"user  not able to click finance module successfully", "fail");
		}

		click(btn_OutboundPaymentAdvice);

		if (isDisplayed(NewOutboundPaymentAdvicePage)) {

			writeTestResults(" Click on Outbount Payment Advice Form",
					"   Outbound Payment Advice form should be available ",
					"Outbound Payment Advice form should  available successfully", "pass");
		} else {

			writeTestResults(" Click on Outbount Payment Advice Form",
					"   Outbound Payment Advice form should not be available ",
					"Outbound Payment Advice form should not available successfully", "fail");
		}

		click(btn_NewOutboundPaymentAdvice);

		click(btn_downarrow);

		click(btn_PaymentVoucherJourney);

		Thread.sleep(2000);

		switchWindow();

//			 selectText(AnalysisCode, txt_AnalysisCode);
		selectText(AnalysisCode, "DUTY");

		Thread.sleep(1000);

		sendKeys(Amount, "0.1");

		Thread.sleep(3000);

		click(btn_CostAllocation);

//		if (isDisplayed(CostAllocationPopUp)) {
//
//			writeTestResults(" Click on Cost allocation Icon", "  Cost allocation look up should be open  ",
//					" Cost allocation look up viewed successfully", "pass");
//		} else {
//
//			writeTestResults(" Click on Cost allocation Icon", "  Cost allocation look up should not open  ",
//					"Cost allocation look up not viewed successfully ", "fail");
//		}

		Thread.sleep(1000);

		sendKeys(Percentage, "100");

		Thread.sleep(1000);

//			selectText(CostAssignmentType, txt_CostAssignmentType);
		selectText(CostAssignmentType, "Service Job Order");

		Thread.sleep(1000);

		click(btn_SearchCostObject);

		Thread.sleep(1000);

		selectText(templateJOBNO, "My Template");

		sendKeys(searchjobno, txt_searchjobno);

		pressEnter(searchjobno);

		Thread.sleep(3000);

		doubleClick(btn_jobnoServiceOrderLookup);

		Thread.sleep(3000);

		click(btn_AddRecordCostAllocation);

		Thread.sleep(3000);

		click(btn_UpdateCostAllocation);

		Thread.sleep(3000);

		click(btn_checkoutCostAllocation);

		Thread.sleep(3000);

		click(btn_DraftOutboundPaymentAdvice);

		Thread.sleep(3000);

		if (isDisplayed(DraftOutboundPaymentAdvicePage)) {

			writeTestResults(" Click on draft", "   Should be able to Draft the Acrual voucher  ",
					" able to Draft the Acrual voucher successfully", "pass");
		} else {

			writeTestResults(" Click on draft", "   Should be able to Draft the Acrual voucher  ",
					"able to Draft the Acrual voucher successfully", "fail");
		}

		Thread.sleep(3000);

		click(btn_ReleaseOutboundPaymentAdvicePage);

		Thread.sleep(4000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleasedOutboundPaymentAdvicePage)) {

			writeTestResults(" Click on Release", "   Should be able to Release the Acrual voucher  ",
					" able to Release the Acrual voucher successfully", "pass");
		} else {

			writeTestResults(" Click on Release", "   Should be able to Release the Acrual voucher  ",
					"able to Release the Acrual voucher successfully", "fail");
		}

		Thread.sleep(3000);

		click(btn_ActionOutboundPaymentAdvice);

		click(btn_ConverttoOutboundPayment);

		Thread.sleep(1000);

//			sendKeys(Payee, txt_Payee);
		sendKeys(Payee, "ABC");

		Thread.sleep(1000);

		click(btn_OutboundPaymentDetails);

		click(btn_CheckoutOutboundPaymentDetails);

		click(btn_DraftPaymentDetails);

		Thread.sleep(3000);

		if (isDisplayed(DraftOutboundPaymentPage)) {

			writeTestResults(" Click on draft", "   Should be able to Draft the OutboundPayment  ",
					" able to Draft the OutboundPaymentsuccessfully", "pass");
		} else {

			writeTestResults(" Click on draft", "   Should be able to Draft the AOutboundPayment  ",
					"able to Draft the OutboundPayment successfully", "fail");
		}

		Thread.sleep(2000);

		switchWindow();

		click(btnReleasePaymentDetails);
		Thread.sleep(3000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		Thread.sleep(3000);

		if (isDisplayed(ReleaseOutboundPaymentPage)) {

			writeTestResults(" Click on Release", "   Should be able to Release the OutboundPayment  ",
					" able to Release the OutboundPaymentsuccessfully", "pass");
		} else {

			writeTestResults(" Click on Release", "   Should be able to Release the AOutboundPayment  ",
					"able to Release the OutboundPayment successfully", "fail");
		}

		Thread.sleep(1000);

		pageRefersh();

	}

	public void PaymentvouchersServiceJobOder() throws Exception {

		switchWindow();

		Thread.sleep(2000);

		click(btn_navigationmenu);

		if (isDisplayed(sidemenu)) {
			writeTestResults("Click on the top navigation menu", "System should be loaded the menu item successfully",
					"Menu item is displayed", "pass");
		} else {
			writeTestResults("Click on the top navigation menu", "System should not  loaded the menu item successfully",
					"Menu item is not displayed", "fail");
		}

		click(btn_servicemodule);

		if (isDisplayed(subsidemenu)) {

			writeTestResults("Click on the Service module", "System should be loaded the Service module successfully",
					"subsidemenu is displayed", "pass");
		} else {

			writeTestResults("Click on the Service module", "System should not loaded the Service module successfully",
					"subsidemenu is not displayed", "fail");
		}

		click(btn_ServiceJobOrder);

		if (isDisplayed(ServiceJobOrderPage)) {

			writeTestResults("Click on Service job order under menu",
					"Service job order should be available in the menu", "Service job order is displayed", "pass");
		} else {

			writeTestResults("Click on Service job order under menu",
					"Service job order should not available in the menu", "Service job order is not displayed", "fail");
		}

		Thread.sleep(3000);

		selectText(templateJOBNO, "My Template");

		Thread.sleep(3000);

		sendKeys(searchjobno, txt_searchjobno);

		Thread.sleep(3000);

		pressEnter(searchjobno);

		Thread.sleep(3000);

		doubleClick(btn_jobnoServiceJobOrder);

		Thread.sleep(3000);

		click(btn_overview);

		Thread.sleep(2000);

		click(btn_expenses);

		Thread.sleep(4000);

		click(btn_OutboundPaymentAdviceLink);

		Thread.sleep(5000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		Thread.sleep(3000);

		if (isDisplayed(ReleaseOutboundPaymentPage)) {

			writeTestResults(" Click on OPA link", "   Should be able to view the OutboundPayment  ",
					" Able to view the OutboundPaymentsuccessfully", "pass");
		} else {

			writeTestResults(" Click on OPA link", "  Should be able to view the OutboundPayment ",
					" Able to view the OutboundPaymentsuccessfully", "fail");
		}

		Thread.sleep(3000);

		click(btn_CostingSummaary);

		Thread.sleep(3000);

		click(btn_Expense);

	}

	public void PettyCash() throws Exception {

		click(btn_Finance);

		if (isDisplayed(FinanceSubSideMenu)) {

			writeTestResults(" Click on Finance Module", "  User should be able to click on the Finance modle ",
					"user able to click finance module successfully", "pass");
		} else {

			writeTestResults(" Click on Finance Module", "  User should be not able to click on the Finance modle ",
					"user  not able to click finance module successfully", "fail");
		}

		click(btn_PettyCash);

		Thread.sleep(3000);

		if (isDisplayed(PettyCashPage)) {

			writeTestResults(" Click on Petty Cash form",
					"   User should be able to click on the New Petty cash form and petty cash form should be open in a new tab ",
					"user able to click on the New Petty cash form and petty cash form should be open in a new tab successfully",
					"pass");
		} else {

			writeTestResults(" Click on Petty Cash form",
					"   User should be able to click on the New Petty cash form and petty cash form should be open in a new tab ",
					"user  not able to click on the New Petty cash form and petty cash form should be open in a new tab successfully",
					"fail");
		}

		click(btn_NewPettyCash);

		Thread.sleep(1000);

		selectText(PettyCashAccount, txt_PettyCashAccount);

		Thread.sleep(3000);

		sendKeys(PettyCashDescription, txt_PettyCashDescription);

		click(btn_paidtoemployee);

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Employee, txt_Employee);

		Thread.sleep(3000);
		pressEnter(Employee);

		Thread.sleep(3000);

		doubleClick(btn_LabourEmployee);

		Thread.sleep(2000);

		selectText(TypePaymentInformation, txt_TypePaymentInformation);

		selectText(AnalysisCode, "DUTY");

		Thread.sleep(1000);

		sendKeys(Amount, "0.1");

		Thread.sleep(1000);

		click(btn_CostAllocation1);

		if (isDisplayed(CostAllocationPopUp)) {

			writeTestResults(" Click on Cost allocation Icon", "  Cost allocation look up should be open  ",
					" Cost allocation look up viewed successfully", "pass");
		} else {

			writeTestResults(" Click on Cost allocation Icon", "  Cost allocation look up should not open  ",
					"Cost allocation look up not viewed successfully ", "fail");
		}

		Thread.sleep(1000);

		sendKeys(Percentage, "100");

		Thread.sleep(1000);

//			selectText(CostAssignmentType, txt_CostAssignmentType);
		selectText(CostAssignmentType, "Service Job Order");

		Thread.sleep(1000);

		click(btn_SearchCostObject);

		Thread.sleep(1000);

		selectText(templateJOBNO, "My Template");

		Thread.sleep(2000);

		click("//b[contains(text(),'New Service Job Order')]");

//		if (isDisplayed(StartNewJourneyPage)) {
//
//			writeTestResults(" Click on \"New Service job order\" button in By page",
//					"Service job order journey look up should be open",
//					"Service job order journey look up is displayed", "pass");
//		} else {
//
//			writeTestResults(" Click on \"New Service job order\" button in By page",
//					"Service job order journey look up should be open",
//					"Service job order journey look up is not displayed", "fail");
//		}

		Thread.sleep(4000);

		click(btn_ServiceOrdertoInternalDispatchOrderExternal);

		if (isDisplayed(NewServiceJobOrderPage)) {

			writeTestResults("Select \"Service Order to Internal Dispatch Order (External) \" Journey",
					"Service job order New  page should be open", "Service job order New  page is displayed", "pass");
		} else {

			writeTestResults(" Select \"Service Order to Internal Dispatch Order (External) \" Journey",
					"Service job order New  page should not be open", "Service job order New page is  not displayed",
					"fail");
		}

		switchWindow();

		LocalTime myObj = LocalTime.now();

		sendKeys(ServiceJobOrderTitle, txt_ServiceJobOrderTitle + myObj);

//			sendKeys(ServiceJobOrderAccount,txt_ServiceJobOrderAccount);

		click(btn_accountsearch);

		if (isDisplayed(AccountLookUPPage)) {

			writeTestResults("Click account search", "AccountLookUPPage should be open",
					"AccountLookUPPage  page is displayed", "pass");
		} else {

			writeTestResults("Click account search ", "AccountLookUPPage should not be open",
					"AccountLookUPPage is not displayed", "fail");
		}

		selectText(template, txt_Template);

		sendKeys(Accountname, txt_Accountname);

		pressEnter(Accountname);

		Thread.sleep(2000);

		doubleClick(btn_accountuser);

		Thread.sleep(1000);

//			 click(btn_Productso);
//		 
//		    Thread.sleep(4000);
//			 
//			 click(btn_SerialNos);

		Thread.sleep(1000);

		selectText(ServiceGroup, "IDC-INTER DEPARTMENT JOBS");

		sendKeys(ServiceJobOrderDescription, txt_ServiceJobOrderDescription);

		Thread.sleep(2000);

		Thread.sleep(2000);

		click(btn_searchowner);

		Thread.sleep(1000);

		if (isDisplayed(OwnerLookUpPage)) {

			writeTestResults(" Click to search owner ", "OwnerLookUpPage should be open ",
					"OwnerLookUpPage viewed successfully", "pass");
		} else {

			writeTestResults(" Click to search owner ", " OwnerLookUpPage should not  be open",
					"OwnerLookUpPage not viewed successfully", "fail");
		}

		sendKeys(owner, "H V Hewa Vidana");

		pressEnter(owner);

		Thread.sleep(1000);

		doubleClick(btn_owner);

		Thread.sleep(1000);

		selectText(Priority, "High");

		Thread.sleep(1000);

		click(btn_PricingProfileSearch);

		if (isDisplayed(PricingProfileLookUpPage)) {

			writeTestResults(" Select a \"Pricing Profile\" ", " Able to select a pricing profile from the look up",
					"Able to select pricing profile successfully", "pass");
		} else {

			writeTestResults(" Select a \"Pricing Profile\" ", " Not Able to select a pricing profile from the look up",
					"Not able to select pricing profile successfully", "fail");
		}
		Thread.sleep(1000);

		doubleClick(btn_pricingprofile12);

	}

	public void ServiceJobOrder10() throws Exception {

		Thread.sleep(4000);

		String ServiceJobOrderNo = getText(ServiceJobOrderValue);

		Thread.sleep(2000);

		System.out.println(ServiceJobOrderNo);

		switchWindow();

		Thread.sleep(2000);

		sendKeys(searchjobno, ServiceJobOrderNo);

		Thread.sleep(2000);

		pressEnter(searchjobno);

		Thread.sleep(3000);

		doubleClick(btn_jobnoServiceOrderLookup);

		Thread.sleep(3000);

		click(btn_AddRecordCostAllocation);

		click(btn_updateCostAllocation_Service_TC_011);

		Thread.sleep(2000);

		click(btn_narration);

		Thread.sleep(1000);

//			sendKeys(ExtendNarration, txt_ExtendNarration);
		sendKeys(ExtendNarration, "test");

		Thread.sleep(1000);

		click(btn_narrationapply);

		Thread.sleep(2000);

		click(btn_DraftPettyCash);

		Thread.sleep(5000);

		click(btn_ReleasePettyCash);

		Thread.sleep(7000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleasedpettycashPage)) {

			writeTestResults(" Click on Release", "   Should be able to Release the PettyCash  ",
					" Able to Release the PettyCashsuccessfully", "pass");
		} else {

			writeTestResults(" Click on Release", "   Should be able to Release the PettyCash  ",
					"Able to Release the PettyCash successfully", "fail");
		}

		Thread.sleep(4000);

		String TotalValue = getText(TotalValueNo);

		Thread.sleep(3000);

		System.out.println(TotalValue);

		Thread.sleep(1000);

//		pageRefersh();
//		
//		switchWindow();

		Thread.sleep(2000);

		click(btn_navigationmenu);

		if (isDisplayed(sidemenu)) {
			writeTestResults("Click on the top navigation menu", "System should be loaded the menu item successfully",
					"Menu item is displayed", "pass");
		} else {
			writeTestResults("Click on the top navigation menu", "System should not  loaded the menu item successfully",
					"Menu item is not displayed", "fail");
		}

		Thread.sleep(2000);

		click(btn_servicemodule);

		Thread.sleep(2000);

		if (isDisplayed(subsidemenu)) {

			writeTestResults("Click on the Service module", "System should be loaded the Service module successfully",
					"subsidemenu is displayed", "pass");
		} else {

			writeTestResults("Click on the Service module", "System should not loaded the Service module successfully",
					"subsidemenu is not displayed", "fail");
		}

		click(btn_ServiceJobOrder);

		if (isDisplayed(ServiceJobOrderPage)) {

			writeTestResults("Click on Service job order under menu",
					"Service job order should be available in the menu", "Service job order is displayed", "pass");
		} else {

			writeTestResults("Click on Service job order under menu",
					"Service job order should not available in the menu", "Service job order is not displayed", "fail");
		}

		Thread.sleep(4000);

		selectText(templateJOBNO, "My Template");

		sendKeys(searchjobno, ServiceJobOrderNo);

		pressEnter(searchjobno);

		Thread.sleep(3000);

		doubleClick(btn_jobnoServiceJobOrder);

		Thread.sleep(3000);

		click(btn_overview);

		Thread.sleep(2000);

		click(btn_expenses);

		Thread.sleep(4000);

		click(btn_OutboundPaymentAdviceLink);

		Thread.sleep(5000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleaseOutboundPaymentPage)) {

			writeTestResults(" Click on PC link", "   Should be able to view the PettyCash page successfully  ",
					" Able to view the PettyCash page successfully", "pass");
		} else {

			writeTestResults(" Click on PC link", "  Should be able to view the PettyCash page successfully",
					" Able to view the PettyCash page successfully", "fail");
		}

		Thread.sleep(3000);

		switchWindow();

		switchWindow();

		Thread.sleep(3000);

		pageRefersh();

		Thread.sleep(3000);

		click(btn_CostingSummaary);

		Thread.sleep(3000);

		click(btn_Expense);

		String TotalExpence = getText(TotalExpenceValue);

		Thread.sleep(3000);

		System.out.println(TotalExpence);

		Thread.sleep(3000);

//		if(TotalValue.equals(TotalExpence)) {
//			
//			writeTestResults(" Verify added payment voucher details are displayed", " Petty cash  details should be displayed  ",
//					"Petty cash details  displayed successfully", "pass");
//		} else {
//
//			writeTestResults(" Verify added payment voucher details are displayed", "  Petty cash  details should not displayed  ",
//					"Petty cash  details not displayed", "fail");
//		}

	}

	public void AcrualVoucher() throws Exception {

		click(btn_Finance);

		if (isDisplayed(FinanceSubSideMenu)) {

			writeTestResults(" Click on Finance Module", "  User should be able to enter the amount to amount field ",
					"User able to click finance module successfully", "pass");
		} else {

			writeTestResults(" Click on Finance Module", "  User should be able to enter the amount to amount field ",
					"User  not able to click finance module successfully", "fail");
		}

		Thread.sleep(1000);

		click(btn_OutboundPaymentAdvice);

		if (isDisplayed(OutboundPaymentAdvicePage)) {

			writeTestResults(" Click on \"New Outbound PAyment Advice\" button",
					"   User should be able to click on the button ",
					"user able to click on the New Outboudpayment button and Outboudpayment form should be open in a new tab successfully",
					"pass");
		} else {

			writeTestResults(" Click on \"New Outbound PAyment Advice\" button",
					"   User should be able to click on the button ",
					"user  not able to click on the  New Outboudpayment and Outboudpayment form  form should be open in a new tab successfully",
					"fail");
		}

		click(btn_NewOutboundPaymentAdvice);

		Thread.sleep(1000);

		click(btn_AccuralVoucher);

		Thread.sleep(2000);

		click(btn_searchvendorAcrualVoucher);

		selectText(template, txt_Template);

		sendKeys(searchvendor, txt_searchvendor);

		pressEnter(searchvendor);

		Thread.sleep(3000);

		doubleClick(btn_vendor);

		Thread.sleep(2000);

		LocalTime myObj = LocalTime.now();
		sendKeys(VendorReferenceNo, "009" + myObj);

		selectText(AnalysisCode, "DUTY");

		Thread.sleep(1000);

		sendKeys(Amount, "0.1");

		Thread.sleep(1000);

		click(btn_CostAllocation1);

//		if (isDisplayed(CostAllocationPopUp)) {
//
//			writeTestResults(" Click on Cost allocation Icon", "  Cost allocation look up should be open  ",
//					" Cost allocation look up viewed successfully", "pass");
//		} else {
//
//			writeTestResults(" Click on Cost allocation Icon", "  Cost allocation look up should not open  ",
//					"Cost allocation look up not viewed successfully ", "fail");
//		}

		Thread.sleep(1000);

		sendKeys(Percentage, "100");

		Thread.sleep(1000);

		selectText(CostAssignmentType, "Service Job Order");

		Thread.sleep(1000);

		click(btn_SearchCostObject);

		Thread.sleep(1000);

		selectText(templateJOBNO, "My Template");

		Thread.sleep(2000);

		click("//b[contains(text(),'New Service Job Order')]");

		Thread.sleep(4000);

		click(btn_ServiceOrdertoInternalDispatchOrderExternal);

		if (isDisplayed(NewServiceJobOrderPage)) {

			writeTestResults("Select \"Service Order to Internal Dispatch Order (External) \" Journey",
					"Service job order New  page should be open", "Service job order New  page is displayed", "pass");
		} else {

			writeTestResults(" Select \"Service Order to Internal Dispatch Order (External) \" Journey",
					"Service job order New  page should not be open", "Service job order New page is  not displayed",
					"fail");
		}

		switchWindow();

		LocalTime myObj2 = LocalTime.now();

		sendKeys(ServiceJobOrderTitle, txt_ServiceJobOrderTitle + myObj2);

		click(btn_accountsearch);

		if (isDisplayed(AccountLookUPPage)) {

			writeTestResults("Click account search", "AccountLookUPPage should be open",
					"AccountLookUPPage  page is displayed", "pass");
		} else {

			writeTestResults("Click account search ", "AccountLookUPPage should not be open",
					"AccountLookUPPage is not displayed", "fail");
		}

		selectText(template, txt_Template);

		sendKeys(Accountname, txt_Accountname);

		pressEnter(Accountname);

		Thread.sleep(2000);

		doubleClick(btn_accountuser);

		Thread.sleep(1000);

		selectText(ServiceGroup, "IDC-INTER DEPARTMENT JOBS");

		sendKeys(ServiceJobOrderDescription, txt_ServiceJobOrderDescription);

		Thread.sleep(2000);

		Thread.sleep(2000);

		click(btn_searchowner);

		Thread.sleep(1000);

		if (isDisplayed(OwnerLookUpPage)) {

			writeTestResults(" Click to search owner ", "OwnerLookUpPage should be open ",
					"OwnerLookUpPage viewed successfully", "pass");
		} else {

			writeTestResults(" Click to search owner ", " OwnerLookUpPage should not  be open",
					"OwnerLookUpPage not viewed successfully", "fail");
		}

		sendKeys(owner, "H V Hewa Vidana");

		pressEnter(owner);

		Thread.sleep(1000);

		doubleClick(btn_owner);

		Thread.sleep(1000);

		selectText(Priority, "High");

		Thread.sleep(1000);

		click(btn_PricingProfileSearch);

		if (isDisplayed(PricingProfileLookUpPage)) {

			writeTestResults(" Select a \"Pricing Profile\" ", " Able to select a pricing profile from the look up",
					"Able to select pricing profile successfully", "pass");
		} else {

			writeTestResults(" Select a \"Pricing Profile\" ", " Not Able to select a pricing profile from the look up",
					"Not able to select pricing profile successfully", "fail");
		}
		Thread.sleep(1000);

		doubleClick(btn_pricingprofile12);

		Thread.sleep(3000);

	}

	public void AcrualVoucherServiceJobOrder() throws Exception {

		Thread.sleep(4000);

		String ServiceJobOrderNo = getText(ServiceJobOrderValue);

		Thread.sleep(2000);

		System.out.println(ServiceJobOrderNo);

		switchWindow();

		Thread.sleep(2000);

		sendKeys(searchjobno, ServiceJobOrderNo);

		Thread.sleep(2000);

		pressEnter(searchjobno);

		Thread.sleep(3000);

		doubleClick(btn_jobnoServiceOrderLookup);

		Thread.sleep(3000);

		click(btn_AddRecordCostAllocation);

		click(btn_updateCostAllocation_Serice_TC_012);

		Thread.sleep(2000);

		click(btn_CheckoutOutboundPaymentAdvice);

		Thread.sleep(2000);

		click(btn_DraftPettyCash);

		Thread.sleep(5000);

		click(btn_ReleasePettyCash);

		Thread.sleep(6000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleasedpettycashPage)) {

			writeTestResults(" Click on Release", "   Should be able to Release the OutboundPayment Advice   ",
					" Able to Release the OutboundPayment Advice successfully", "pass");
		} else {

			writeTestResults(" Click on Release", "   Should be able to Release the OutboundPayment Advice  ",
					"Able to Release the OutboundPayment Advice successfully", "fail");
		}

		Thread.sleep(1000);

		String OutboundPaymentValue = getText(OutboundPaymentValueNo);

		Thread.sleep(2000);

		System.out.println(OutboundPaymentValue);

		Thread.sleep(1000);

		switchWindow();

		Thread.sleep(2000);

		click(btn_navigationmenu);

		if (isDisplayed(sidemenu)) {
			writeTestResults("Click on the top navigation menu", "System should be loaded the menu item successfully",
					"Menu item is displayed", "pass");
		} else {
			writeTestResults("Click on the top navigation menu", "System should not  loaded the menu item successfully",
					"Menu item is not displayed", "fail");
		}

		click(btn_servicemodule);

		if (isDisplayed(subsidemenu)) {

			writeTestResults("Click on the Service module", "System should be loaded the Service module successfully",
					"subsidemenu is displayed", "pass");
		} else {

			writeTestResults("Click on the Service module", "System should not loaded the Service module successfully",
					"subsidemenu is not displayed", "fail");
		}

		click(btn_ServiceJobOrder);

		if (isDisplayed(ServiceJobOrderPage)) {

			writeTestResults("Click on Service job order under menu",
					"Service job order should be available in the menu", "Service job order is displayed", "pass");
		} else {

			writeTestResults("Click on Service job order under menu",
					"Service job order should not available in the menu", "Service job order is not displayed", "fail");
		}

		Thread.sleep(3000);

		selectText(templateJOBNO, "My Template");

		sendKeys(searchjobno, ServiceJobOrderNo);

		pressEnter(searchjobno);

		Thread.sleep(3000);

		doubleClick(btn_jobnoServiceJobOrder);

		Thread.sleep(3000);

		click(btn_overview);

		Thread.sleep(2000);

		click(btn_expenses);

		Thread.sleep(4000);

		click(btn_OutboundPaymentAdviceLink);

		Thread.sleep(5000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleaseOutboundPaymentPage)) {

			writeTestResults(" Click on OPA link", "   Should be able to view the OutboundPayment  ",
					" Able to view the OutboundPaymentsuccessfully", "pass");
		} else {

			writeTestResults(" Click on OPA link", "  Should be able to view the OutboundPayment ",
					" Able to view the OutboundPaymentsuccessfully", "fail");
		}

		Thread.sleep(3000);

		click(btn_CostingSummaary);

		Thread.sleep(3000);

		click(btn_Expense);

		Thread.sleep(3000);

		switchWindow();

		switchWindow();

		Thread.sleep(3000);

		pageRefersh();

		Thread.sleep(3000);

		click(btn_CostingSummaary);

		Thread.sleep(3000);

		click(btn_Expense);

		String TotalExpence = getText(TotalExpenceValue);

		Thread.sleep(3000);

		System.out.println(TotalExpence);

		Thread.sleep(3000);

		if (OutboundPaymentValue.equals(TotalExpence)) {

			writeTestResults(" Verify added  OutboundPayment details are displayed",
					" OutboundPayment  details should be displayed  ",
					"OutboundPayment details  displayed successfully", "pass");
		} else {

			writeTestResults(" Verify added OutboundPayment details are displayed",
					"  OutboundPayment  details should not displayed  ", "OutboundPayment  details not displayed",
					"fail");
		}

	}

	public void BankAdjustment() throws Exception {

		click(btn_Finance);

		if (isDisplayed(FinanceSubSideMenu)) {

			writeTestResults(" Click on Finance Module", "  User should be able to click on the Finance modle ",
					"user able to click finance module successfully", "pass");
		} else {

			writeTestResults(" Click on Finance Module", "  User should be not able to click on the Finance modle ",
					"user  not able to click finance module successfully", "fail");
		}

		Thread.sleep(1000);

		click(btn_BankAdjustment);

//			 if(isDisplayed(BankAdjustmentPage)) {
//					
//				 writeTestResults(" Click on Bank Adjustment form", "  User should be able to click on the New bank adjustment button ", "user able to click New bank adjustment successfully", "pass");
//			}else {
//				
//				writeTestResults(" Click on Bank Adjustment form", " User should notable to click on the New bank adjustment button ", "user  not able to click New bank adjustment successfully", "fail");
//			} 

		click(btn_NewBankAdjustment);

		Thread.sleep(1000);

		selectText(bank, txt_bank);

		Thread.sleep(1000);

		selectText(bankaccountno, "AC-000945632147");

		Thread.sleep(1000);

//             selectText(analysiscode, txt_analysiscode);

		selectText(analysiscode, "DUTY");

		Thread.sleep(1000);

		sendKeys(AmountBankAdjustment, txt_AmountBankAdjustment);

		Thread.sleep(1000);

		click(btn_costallocationbankadjustment);

		Thread.sleep(2000);

//		if (isDisplayed(CostAllocationPopUp)) {
//
//			writeTestResults(" Click on Cost allocation Icon", "  Cost allocation look up should be open  ",
//					" Cost allocation look up viewed successfully", "pass");
//		} else {
//
//			writeTestResults(" Click on Cost allocation Icon", "  Cost allocation look up should not open  ",
//					"Cost allocation look up not viewed successfully ", "fail");
//		}

		Thread.sleep(1000);

		sendKeys(Percentage, "100");

		Thread.sleep(1000);

//			selectText(CostAssignmentType, txt_CostAssignmentType);
		selectText(CostAssignmentType, "Service Job Order");

		Thread.sleep(1000);

		click(btn_SearchCostObject);

		Thread.sleep(1000);

		selectText(templateJOBNO, "My Template");

		Thread.sleep(2000);

		click("//b[contains(text(),'New Service Job Order')]");

//		if (isDisplayed(StartNewJourneyPage)) {
//
//			writeTestResults(" Click on \"New Service job order\" button in By page",
//					"Service job order journey look up should be open",
//					"Service job order journey look up is displayed", "pass");
//		} else {
//
//			writeTestResults(" Click on \"New Service job order\" button in By page",
//					"Service job order journey look up should be open",
//					"Service job order journey look up is not displayed", "fail");
//		}

		Thread.sleep(4000);

		click(btn_ServiceOrdertoInternalDispatchOrderExternal);

		if (isDisplayed(NewServiceJobOrderPage)) {

			writeTestResults("Select \"Service Order to Internal Dispatch Order (External) \" Journey",
					"Service job order New  page should be open", "Service job order New  page is displayed", "pass");
		} else {

			writeTestResults(" Select \"Service Order to Internal Dispatch Order (External) \" Journey",
					"Service job order New  page should not be open", "Service job order New page is  not displayed",
					"fail");
		}

		switchWindow();

		LocalTime myObj = LocalTime.now();

		sendKeys(ServiceJobOrderTitle, txt_ServiceJobOrderTitle + myObj);

//			sendKeys(ServiceJobOrderAccount,txt_ServiceJobOrderAccount);

		click(btn_accountsearch);

		if (isDisplayed(AccountLookUPPage)) {

			writeTestResults("Click account search", "AccountLookUPPage should be open",
					"AccountLookUPPage  page is displayed", "pass");
		} else {

			writeTestResults("Click account search ", "AccountLookUPPage should not be open",
					"AccountLookUPPage is not displayed", "fail");
		}

		selectText(template, txt_Template);

		sendKeys(Accountname, txt_Accountname);

		pressEnter(Accountname);

		Thread.sleep(2000);

		doubleClick(btn_accountuser);

		Thread.sleep(1000);

//			 click(btn_Productso);
//		 
//		    Thread.sleep(4000);
//			 
//			 click(btn_SerialNos);

		Thread.sleep(1000);

		selectText(ServiceGroup, "IDC-INTER DEPARTMENT JOBS");

		sendKeys(ServiceJobOrderDescription, txt_ServiceJobOrderDescription);

		Thread.sleep(2000);

		Thread.sleep(2000);

		click(btn_searchowner);

		Thread.sleep(1000);

		if (isDisplayed(OwnerLookUpPage)) {

			writeTestResults(" Click to search owner ", "OwnerLookUpPage should be open ",
					"OwnerLookUpPage viewed successfully", "pass");
		} else {

			writeTestResults(" Click to search owner ", " OwnerLookUpPage should not  be open",
					"OwnerLookUpPage not viewed successfully", "fail");
		}

		sendKeys(owner, "H V Hewa Vidana");

		pressEnter(owner);

		Thread.sleep(1000);

		doubleClick(btn_owner);

		Thread.sleep(1000);

		selectText(Priority, "High");

		Thread.sleep(1000);

		click(btn_PricingProfileSearch);

		if (isDisplayed(PricingProfileLookUpPage)) {

			writeTestResults(" Select a \"Pricing Profile\" ", " Able to select a pricing profile from the look up",
					"Able to select pricing profile successfully", "pass");
		} else {

			writeTestResults(" Select a \"Pricing Profile\" ", " Not Able to select a pricing profile from the look up",
					"Not able to select pricing profile successfully", "fail");
		}
		Thread.sleep(1000);

		doubleClick(btn_pricingprofile12);

	}

	public void BankAdujustmentServiceJoborder() throws Exception {

		Thread.sleep(4000);

		String ServiceJobOrderNo = getText(ServiceJobOrderValue);

		Thread.sleep(2000);

		System.out.println(ServiceJobOrderNo);

		switchWindow();

		Thread.sleep(2000);

		sendKeys(searchjobno, ServiceJobOrderNo);

		Thread.sleep(2000);

		pressEnter(searchjobno);

		Thread.sleep(3000);

		doubleClick(btn_jobnoServiceOrderLookup);

		Thread.sleep(3000);

		click(btn_AddRecordCostAllocation);

		Thread.sleep(3000);

		click(btn_UpdateCostAllocation1);

		Thread.sleep(2000);

		click(btn_narration);

		Thread.sleep(1000);

//			sendKeys(ExtendNarration, txt_ExtendNarration);
		sendKeys(ExtendNarration, "test");

		Thread.sleep(1000);

//		click(btn_narrationapply);

		click("//*[@id=\"inspect\"]/div[13]/div[3]/a");

		click("//*[@id=\"btnCalc2\"]");

		Thread.sleep(2000);

		click(btn_DraftPettyCash);

		Thread.sleep(5000);

		click(btn_ReleasePettyCash);

		Thread.sleep(6000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleasedpettycashPage)) {

			writeTestResults(" Click on Release", "   Should be able to Release the BankAdjustment  ",
					" Able to Release the BankAdjustment successfully", "pass");
		} else {

			writeTestResults(" Click on Release", "   Should be able to Release the BankAdjustment  ",
					"Able to Release the BankAdjustment successfully", "fail");
		}

		Thread.sleep(4000);

		String TotalValueBankAdjustment = getText(TotalValueNoBankAdjustment);

		Thread.sleep(3000);

		System.out.println(TotalValueBankAdjustment);

		Thread.sleep(1000);

//		pageRefersh();
//		
//		switchWindow();

		Thread.sleep(2000);

		click(btn_navigationmenu);

		if (isDisplayed(sidemenu)) {
			writeTestResults("Click on the top navigation menu", "System should be loaded the menu item successfully",
					"Menu item is displayed", "pass");
		} else {
			writeTestResults("Click on the top navigation menu", "System should not  loaded the menu item successfully",
					"Menu item is not displayed", "fail");
		}

		click(btn_servicemodule);

		if (isDisplayed(subsidemenu)) {

			writeTestResults("Click on the Service module", "System should be loaded the Service module successfully",
					"subsidemenu is displayed", "pass");
		} else {

			writeTestResults("Click on the Service module", "System should not loaded the Service module successfully",
					"subsidemenu is not displayed", "fail");
		}

		click(btn_ServiceJobOrder);

		if (isDisplayed(ServiceJobOrderPage)) {

			writeTestResults("Click on Service job order under menu",
					"Service job order should be available in the menu", "Service job order is displayed", "pass");
		} else {

			writeTestResults("Click on Service job order under menu",
					"Service job order should not available in the menu", "Service job order is not displayed", "fail");
		}

		Thread.sleep(4000);

		selectText(templateJOBNO, "My Template");

		sendKeys(searchjobno, ServiceJobOrderNo);

		pressEnter(searchjobno);

		Thread.sleep(4000);

		doubleClick(btn_jobnoServiceJobOrder);

		Thread.sleep(3000);

		pageRefersh();

		Thread.sleep(3000);

		click(btn_overview);

		Thread.sleep(3000);

		click(btn_expenses);

		Thread.sleep(4000);

		click(btn_BankAdjustmentLink);

		Thread.sleep(5000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleaseOutboundPaymentPage)) {

			writeTestResults(" Click on BankAdjustment link",
					"   Should be able to view the BankAdjustment page successfully  ",
					" Able to view the BankAdjustment page successfully", "pass");
		} else {

			writeTestResults(" Click on BankAdjustment link",
					"  Should be able to view the BankAdjustment page successfully",
					" Able to view the BankAdjustment page successfully", "fail");
		}

		Thread.sleep(3000);

		switchWindow();

		switchWindow();

		Thread.sleep(3000);

		pageRefersh();

		Thread.sleep(3000);

		click(btn_CostingSummaary);

		Thread.sleep(3000);

		click(btn_Expense);

		String TotalDuty = getText(TotalDutyValue);

		Thread.sleep(3000);

		System.out.println(TotalDuty);

		Thread.sleep(3000);

		if ((TotalValueBankAdjustment.replace("-", "")).equals(TotalDuty)) {

			writeTestResults(" Verify added BankAdjustment details are displayed",
					" BankAdjustment  details should be displayed  ", "BankAdjustment details  displayed successfully",
					"pass");
		} else {

			writeTestResults(" Verify added BankAdjustment details are displayed",
					"  BankAdjustment  details should not displayed  ", "BankAdjustment  details not displayed",
					"fail");
		}

	}

	public void VendorRefund() throws Exception {

		click(btn_Finance);

		if (isDisplayed(FinanceSubSideMenu)) {

			writeTestResults(" Click on Finance Module", "  User should be able to click on the Finance modle ",
					"user able to click finance module successfully", "pass");
		} else {

			writeTestResults(" Click on Finance Module", "  User should be not able to click on the Finance modle ",
					"user  not able to click finance module successfully", "fail");
		}

		Thread.sleep(1000);

		click(btn_InboundPaymentAdvice);

		Thread.sleep(1000);

		click(btn_NewInboundPaymentAdvice);

		click(btn_VendorRefund);

		Thread.sleep(1000);

		click(btn_searchvendorInboundPaymentAdvice);

		selectText(template, txt_Template);

		sendKeys(searchvendor, txt_searchvendor);

		pressEnter(searchvendor);

		Thread.sleep(3000);

		doubleClick(btn_vendor);

		Thread.sleep(2000);

		selectText(AnalysisCode, "DUTY");

		Thread.sleep(1000);

		sendKeys(AmountInboundPaymentAdvice, "100");

		Thread.sleep(1000);

		click(btn_CostAllocation2);

		if (isDisplayed(CostAllocationPopUp)) {

			writeTestResults(" Click on Cost allocation Icon", "  Cost allocation look up should be open  ",
					" Cost allocation look up viewed successfully", "pass");
		} else {

			writeTestResults(" Click on Cost allocation Icon", "  Cost allocation look up should not open  ",
					"Cost allocation look up not viewed successfully ", "fail");
		}

		Thread.sleep(1000);

		sendKeys(Percentage, "100");

		Thread.sleep(1000);

		selectText(CostAssignmentType, "Service Job Order");

		Thread.sleep(1000);

		click(btn_SearchCostObject);

		Thread.sleep(1000);

		selectText(templateJOBNO, "My Template");

		Thread.sleep(2000);

		click("//b[contains(text(),'New Service Job Order')]");

		Thread.sleep(4000);

		click(btn_ServiceOrdertoInternalDispatchOrderExternal);

		if (isDisplayed(NewServiceJobOrderPage)) {

			writeTestResults("Select \"Service Order to Internal Dispatch Order (External) \" Journey",
					"Service job order New  page should be open", "Service job order New  page is displayed", "pass");
		} else {

			writeTestResults(" Select \"Service Order to Internal Dispatch Order (External) \" Journey",
					"Service job order New  page should not be open", "Service job order New page is  not displayed",
					"fail");
		}

		switchWindow();

		LocalTime myObj = LocalTime.now();

		sendKeys(ServiceJobOrderTitle, txt_ServiceJobOrderTitle + myObj);

		click(btn_accountsearch);

		if (isDisplayed(AccountLookUPPage)) {

			writeTestResults("Click account search", "AccountLookUPPage should be open",
					"AccountLookUPPage  page is displayed", "pass");
		} else {

			writeTestResults("Click account search ", "AccountLookUPPage should not be open",
					"AccountLookUPPage is not displayed", "fail");
		}

		selectText(template, txt_Template);

		sendKeys(Accountname, txt_Accountname);

		pressEnter(Accountname);

		Thread.sleep(2000);

		doubleClick(btn_accountuser);

		Thread.sleep(1000);

		selectText(ServiceGroup, "IDC-INTER DEPARTMENT JOBS");

		sendKeys(ServiceJobOrderDescription, txt_ServiceJobOrderDescription);

		Thread.sleep(2000);

		click(btn_searchowner);

		Thread.sleep(1000);

		if (isDisplayed(OwnerLookUpPage)) {

			writeTestResults(" Click to search owner ", "OwnerLookUpPage should be open ",
					"OwnerLookUpPage viewed successfully", "pass");
		} else {

			writeTestResults(" Click to search owner ", " OwnerLookUpPage should not  be open",
					"OwnerLookUpPage not viewed successfully", "fail");
		}

		sendKeys(owner, "H V Hewa Vidana");

		pressEnter(owner);

		Thread.sleep(1000);

		doubleClick(btn_owner);

		Thread.sleep(1000);

		selectText(Priority, "High");

		Thread.sleep(1000);

		click(btn_PricingProfileSearch);

		if (isDisplayed(PricingProfileLookUpPage)) {

			writeTestResults(" Select a \"Pricing Profile\" ", " Able to select a pricing profile from the look up",
					"Able to select pricing profile successfully", "pass");
		} else {

			writeTestResults(" Select a \"Pricing Profile\" ", " Not Able to select a pricing profile from the look up",
					"Not able to select pricing profile successfully", "fail");
		}
		Thread.sleep(1000);

		doubleClick(btn_pricingprofile12);

	}

	public void VendorRefundServiceJobOrder() throws Exception {

		Thread.sleep(4000);

		String ServiceJobOrderNo = getText(ServiceJobOrderValue);

		Thread.sleep(2000);

		System.out.println(ServiceJobOrderNo);

		switchWindow();

		Thread.sleep(2000);

		sendKeys(searchjobno, ServiceJobOrderNo);

		Thread.sleep(3000);

		pressEnter(searchjobno);

		Thread.sleep(3000);

		doubleClick(btn_jobnoServiceOrderLookup);

		Thread.sleep(3000);

		click(btn_AddRecordCostAllocation);

		Thread.sleep(3000);

		click(btn_UpdateCostAllocation1);

		Thread.sleep(2000);

		click(btn_narration);

		Thread.sleep(1000);

//			sendKeys(ExtendNarration, txt_ExtendNarration);
		sendKeys(ExtendNarration, "test");

		Thread.sleep(1000);

//		click(btn_narrationapply);

		click("//*[@id=\"inspect\"]/div[13]/div[3]/a");

		Thread.sleep(2000);

		click("//*[@id=\"btnCalc2\"]");

		Thread.sleep(3000);

		click(btn_DraftPettyCash);

		Thread.sleep(5000);

		click(btn_ReleasePettyCash);

		Thread.sleep(6000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleasedpettycashPage)) {

			writeTestResults(" Click on Release", "   Should be able to Release the InboundPaymentAdvice  ",
					" Able to Release the InboundPaymentAdvice successfully", "pass");
		} else {

			writeTestResults(" Click on Release", "   Should be able to Release the InboundPaymentAdvice  ",
					"Able to Release the InboundPaymentAdvice successfully", "fail");
		}

		Thread.sleep(4000);

		String TotalValueVendorRefundvalue = getText(TotalValueVendorRefund);

		Thread.sleep(3000);

		System.out.println(TotalValueVendorRefundvalue);

		Thread.sleep(1000);

//		pageRefersh();
//		
//		switchWindow();

		Thread.sleep(2000);

		click(btn_navigationmenu);

		if (isDisplayed(sidemenu)) {
			writeTestResults("Click on the top navigation menu", "System should be loaded the menu item successfully",
					"Menu item is displayed", "pass");
		} else {
			writeTestResults("Click on the top navigation menu", "System should not  loaded the menu item successfully",
					"Menu item is not displayed", "fail");
		}

		click(btn_servicemodule);

		if (isDisplayed(subsidemenu)) {

			writeTestResults("Click on the Service module", "System should be loaded the Service module successfully",
					"subsidemenu is displayed", "pass");
		} else {

			writeTestResults("Click on the Service module", "System should not loaded the Service module successfully",
					"subsidemenu is not displayed", "fail");
		}

		click(btn_ServiceJobOrder);

		if (isDisplayed(ServiceJobOrderPage)) {

			writeTestResults("Click on Service job order under menu",
					"Service job order should be available in the menu", "Service job order is displayed", "pass");
		} else {

			writeTestResults("Click on Service job order under menu",
					"Service job order should not available in the menu", "Service job order is not displayed", "fail");
		}

		Thread.sleep(4000);

		selectText(templateJOBNO, "My Template");

		sendKeys(searchjobno, ServiceJobOrderNo);

		pressEnter(searchjobno);

		Thread.sleep(4000);

		doubleClick(btn_jobnoServiceJobOrder);

		Thread.sleep(3000);

		pageRefersh();

		Thread.sleep(3000);

		click(btn_overview);

		Thread.sleep(3000);

		click("//*[@id=\"#tabVendorRefund\"]");

		Thread.sleep(4000);

		click(btn_InboundPaymetAdviceLink);

		Thread.sleep(5000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleaseOutboundPaymentPage)) {

			writeTestResults(" Click on InboundPaymentAdvice link",
					"   Should be able to view the InboundPaymentAdvice page successfully  ",
					" Able to view the InboundPaymentAdvice page successfully", "pass");
		} else {

			writeTestResults(" Click on  InboundPaymentAdvice  link",
					"  Should be able to view the InboundPaymentAdvice page successfully",
					" Able to view the InboundPaymentAdvice page successfully", "fail");
		}

		Thread.sleep(3000);

		switchWindow();

		switchWindow();

		Thread.sleep(3000);

		pageRefersh();

		Thread.sleep(3000);

		click(btn_CostingSummaary);

		Thread.sleep(3000);

		click(btn_CostAdjustmentArrow);

		Thread.sleep(3000);

		String VendorAdjustment = getText(VendorAdjustmentValue);

		Thread.sleep(3000);

		System.out.println(VendorAdjustment);

		Thread.sleep(3000);

		if ((VendorAdjustment.replace("-", "")).equals(TotalValueVendorRefundvalue)) {

			writeTestResults(" Verify added InboundPaymentAdvice details are displayed",
					" InboundPaymentAdvice details should be displayed  ",
					"InboundPaymentAdvice details  displayed successfully", "pass");
		} else {

			writeTestResults(" Verify added InboundPaymentAdvice details are displayed",
					" InboundPaymentAdvice details should not displayed  ",
					"InboundPaymentAdvice details not displayed", "fail");
		}

	}

	public void CustomerDebitMemo() throws Exception {
		click(btn_Finance);

		if (isDisplayed(FinanceSubSideMenu)) {

			writeTestResults(" Click on Finance Module", "  User should be able to click on the Finance modle ",
					"user able to click finance module successfully", "pass");
		} else {

			writeTestResults(" Click on Finance Module", "  User should be not able to click on the Finance modle ",
					"user  not able to click finance module successfully", "fail");
		}

		Thread.sleep(1000);

		click(btn_InboundPaymentAdvice);

		Thread.sleep(1000);

		click(btn_NewInboundPaymentAdvice);

		click(btn_CustomerDebitMemo);

		click(btn_SearchCustomerDebitMemo);

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(customer, txt_customer);

		Thread.sleep(2000);

		pressEnter(customer);

		Thread.sleep(3000);

		doubleClick(btn_customer);

		Thread.sleep(2000);

		Thread.sleep(2000);

		selectText(AnalysisCode, "DUTY");

		Thread.sleep(1000);

		sendKeys(AmountInboundPaymentAdvice, "1");

		Thread.sleep(1000);

		click(btn_CostAllocation2);

		if (isDisplayed(CostAllocationPopUp)) {

			writeTestResults(" Click on Cost allocation Icon", "  Cost allocation look up should be open  ",
					" Cost allocation look up viewed successfully", "pass");
		} else {

			writeTestResults(" Click on Cost allocation Icon", "  Cost allocation look up should not open  ",
					"Cost allocation look up not viewed successfully ", "fail");
		}

		Thread.sleep(1000);

		sendKeys(Percentage, "100");

		Thread.sleep(1000);

//				selectText(CostAssignmentType, txt_CostAssignmentType);
		selectText(CostAssignmentType, "Service Job Order");

		Thread.sleep(1000);

		click(btn_SearchCostObject);

		Thread.sleep(1000);

		selectText(templateJOBNO, "My Template");

		Thread.sleep(2000);

		click("//b[contains(text(),'New Service Job Order')]");

		Thread.sleep(4000);

		click(btn_ServiceOrdertoInternalDispatchOrderExternal);

		if (isDisplayed(NewServiceJobOrderPage)) {

			writeTestResults("Select \"Service Order to Internal Dispatch Order (External) \" Journey",
					"Service job order New  page should be open", "Service job order New  page is displayed", "pass");
		} else {

			writeTestResults(" Select \"Service Order to Internal Dispatch Order (External) \" Journey",
					"Service job order New  page should not be open", "Service job order New page is  not displayed",
					"fail");
		}

		switchWindow();

		LocalTime myObj = LocalTime.now();

		sendKeys(ServiceJobOrderTitle, txt_ServiceJobOrderTitle + myObj);

		click(btn_accountsearch);

		if (isDisplayed(AccountLookUPPage)) {

			writeTestResults("Click account search", "AccountLookUPPage should be open",
					"AccountLookUPPage  page is displayed", "pass");
		} else {

			writeTestResults("Click account search ", "AccountLookUPPage should not be open",
					"AccountLookUPPage is not displayed", "fail");
		}

		selectText(template, txt_Template);

		sendKeys(Accountname, txt_Accountname);

		pressEnter(Accountname);

		Thread.sleep(2000);

		doubleClick(btn_accountuser);

		Thread.sleep(1000);

		selectText(ServiceGroup, "IDC-INTER DEPARTMENT JOBS");

		sendKeys(ServiceJobOrderDescription, txt_ServiceJobOrderDescription);

		Thread.sleep(2000);

		click(btn_searchowner);

		Thread.sleep(1000);

		if (isDisplayed(OwnerLookUpPage)) {

			writeTestResults(" Click to search owner ", "OwnerLookUpPage should be open ",
					"OwnerLookUpPage viewed successfully", "pass");
		} else {

			writeTestResults(" Click to search owner ", " OwnerLookUpPage should not  be open",
					"OwnerLookUpPage not viewed successfully", "fail");
		}

		sendKeys(owner, "H V Hewa Vidana");

		pressEnter(owner);

		Thread.sleep(1000);

		doubleClick(btn_owner);

		Thread.sleep(1000);

		selectText(Priority, "High");

		Thread.sleep(1000);

		click(btn_PricingProfileSearch);

		if (isDisplayed(PricingProfileLookUpPage)) {

			writeTestResults(" Select a \"Pricing Profile\" ", " Able to select a pricing profile from the look up",
					"Able to select pricing profile successfully", "pass");
		} else {

			writeTestResults(" Select a \"Pricing Profile\" ", " Not Able to select a pricing profile from the look up",
					"Not able to select pricing profile successfully", "fail");
		}
		Thread.sleep(1000);

		doubleClick(btn_pricingprofile12);
	}

	///////////////////////////////////////////////////////////////////////////////////

	public void CustomerDebitMemoServiceJobOrderNew() throws Exception {

		Thread.sleep(4000);

		String ServiceJobOrderNo = getText(ServiceJobOrderValue);

		Thread.sleep(2000);

		System.out.println(ServiceJobOrderNo);

		switchWindow();

		Thread.sleep(2000);

		sendKeys(searchjobno, ServiceJobOrderNo);

		Thread.sleep(2000);

		pressEnter(searchjobno);

		Thread.sleep(3000);

		doubleClick(btn_jobnoServiceOrderLookup);

		Thread.sleep(4000);

//			click(btn_AddRecordCostAllocation);

		click("//a[@id='btnApportionAddRecord']");

		Thread.sleep(3000);

//			click(btn_UpdateCostAllocation1);

		click("//a[contains(text(),'Update')]");

		Thread.sleep(2000);
//
//			click(btn_narration);
//			
//		
//			Thread.sleep(1000);

//				sendKeys(ExtendNarration, txt_ExtendNarration);
		// sendKeys(ExtendNarration, "test");

//			Thread.sleep(1000);
//
//			click(btn_narrationapply);
//			
//			click("//*[@id=\"inspect\"]/div[13]/div[3]/a");
//			
//			click("//*[@id=\"btnCalc2\"]");

		// Thread.sleep(2000);

		click(btn_DraftPettyCash);

		Thread.sleep(5000);

		click(btn_ReleasePettyCash);

		Thread.sleep(6000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleasedpettycashPage)) {

			writeTestResults(" Click on Release", "   Should be able to Release the BankAdjustment  ",
					" Able to Release the BankAdjustment successfully", "pass");
		} else {

			writeTestResults(" Click on Release", "   Should be able to Release the BankAdjustment  ",
					"Able to Release the BankAdjustment successfully", "fail");
		}

		Thread.sleep(4000);

		String TotalValueVendorRefundvalue = getText(TotalValueVendorRefund);

		Thread.sleep(3000);

		System.out.println(TotalValueVendorRefundvalue);

		Thread.sleep(1000);

//			pageRefersh();
//			
//			switchWindow();

		Thread.sleep(2000);

		click(btn_navigationmenu);

		if (isDisplayed(sidemenu)) {
			writeTestResults("Click on the top navigation menu", "System should be loaded the menu item successfully",
					"Menu item is displayed", "pass");
		} else {
			writeTestResults("Click on the top navigation menu", "System should not  loaded the menu item successfully",
					"Menu item is not displayed", "fail");
		}

		click(btn_servicemodule);

		if (isDisplayed(subsidemenu)) {

			writeTestResults("Click on the Service module", "System should be loaded the Service module successfully",
					"subsidemenu is displayed", "pass");
		} else {

			writeTestResults("Click on the Service module", "System should not loaded the Service module successfully",
					"subsidemenu is not displayed", "fail");
		}

		click(btn_ServiceJobOrder);

		if (isDisplayed(ServiceJobOrderPage)) {

			writeTestResults("Click on Service job order under menu",
					"Service job order should be available in the menu", "Service job order is displayed", "pass");
		} else {

			writeTestResults("Click on Service job order under menu",
					"Service job order should not available in the menu", "Service job order is not displayed", "fail");
		}

		Thread.sleep(4000);

		selectText(templateJOBNO, "My Template");

		sendKeys(searchjobno, ServiceJobOrderNo);

		pressEnter(searchjobno);

		Thread.sleep(4000);

		doubleClick(btn_jobnoServiceJobOrder);

		Thread.sleep(3000);

		pageRefersh();

		Thread.sleep(3000);

		click(btn_overview);

		Thread.sleep(3000);

		click(btn_RevenueAdjustment);

		Thread.sleep(5000);

		click(btn_InboundPaymetAdviceLinkRevenueAdjustment);

		Thread.sleep(5000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleaseOutboundPaymentPage)) {

			writeTestResults(" Click on InboundPaymentAdvice link",
					"   Should be able to view the InboundPaymentAdvice page successfully  ",
					" Able to view the InboundPaymentAdvice page successfully", "pass");
		} else {

			writeTestResults(" Click on  InboundPaymentAdvice  link",
					"  Should be able to view the InboundPaymentAdvice page successfully",
					" Able to view the InboundPaymentAdvice page successfully", "fail");
		}

		Thread.sleep(3000);

		switchWindow();

		switchWindow();

		Thread.sleep(3000);

		pageRefersh();

		Thread.sleep(3000);

		click(btn_CostingSummaary);

		Thread.sleep(3000);

//			click(btn_RevenueAdjustmentArrow);

		click("//*[@id=\"24\"]");

		Thread.sleep(3000);

		String CustomerAdjustment = getText(CustomerAdjustmentValue);

		Thread.sleep(3000);

		System.out.println(CustomerAdjustment);

		Thread.sleep(3000);

		if ((CustomerAdjustment.replace("-", "")).equals(TotalValueVendorRefundvalue)) {

			writeTestResults(" Verify added InboundPaymentAdvice details are displayed",
					" InboundPaymentAdvice details should be displayed  ",
					"InboundPaymentAdvice details  displayed successfully", "pass");
		} else {

			writeTestResults(" Verify added InboundPaymentAdvice details are displayed",
					" InboundPaymentAdvice details should not displayed  ",
					"InboundPaymentAdvice details not displayed", "fail");
		}

	}

	public void CustomerRefund() throws Exception {

		click(btn_Finance);

		if (isDisplayed(FinanceSubSideMenu)) {

			writeTestResults(" Click on Finance Module", "  User should be able to click on the Finance modle ",
					"user able to click finance module successfully", "pass");
		} else {

			writeTestResults(" Click on Finance Module", "  User should be not able to click on the Finance modle ",
					"user  not able to click finance module successfully", "fail");
		}

		click(btn_OutboundPaymentAdvice);

		if (isDisplayed(NewOutboundPaymentAdvicePage)) {

			writeTestResults(" Click on Outbount Payment Advice Form",
					"   Outbound Payment Advice form should be available ",
					"Outbound Payment Advice form should  available successfully", "pass");
		} else {

			writeTestResults(" Click on Outbount Payment Advice Form",
					"   Outbound Payment Advice form should not be available ",
					"Outbound Payment Advice form should not available successfully", "fail");
		}

		click(btn_NewOutboundPaymentAdvice);

		click(btn_downarrow);

		Thread.sleep(1000);

		click(btn_CustomerRefund);

		Thread.sleep(1000);

		click(btn_SearchCustomerRefund);

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(customer, txt_customer);

		Thread.sleep(2000);

		pressEnter(customer);

		Thread.sleep(3000);

		doubleClick(btn_customer);

		Thread.sleep(2000);

		Thread.sleep(2000);

		selectText(AnalysisCode, "DUTY");

		Thread.sleep(1000);

		sendKeys(AmountCustomerRefund, "1");

		Thread.sleep(1000);

		click(btn_CostAllocationCustomerRefund);

//		if (isDisplayed(CostAllocationPopUp)) {
//
//			writeTestResults(" Click on Cost allocation Icon", "  Cost allocation look up should be open  ",
//					" Cost allocation look up viewed successfully", "pass");
//		} else {
//
//			writeTestResults(" Click on Cost allocation Icon", "  Cost allocation look up should not open  ",
//					"Cost allocation look up not viewed successfully ", "fail");
//		}

		Thread.sleep(1000);

		sendKeys(Percentage, "100");

		Thread.sleep(1000);

//				selectText(CostAssignmentType, txt_CostAssignmentType);
		selectText(CostAssignmentType, "Service Job Order");

		Thread.sleep(1000);

		click(btn_SearchCostObject);

		Thread.sleep(1000);

		selectText(templateJOBNO, "My Template");

		selectText(templateJOBNO, "My Template");

		Thread.sleep(2000);

		click("//b[contains(text(),'New Service Job Order')]");

		Thread.sleep(4000);

		click(btn_ServiceOrdertoInternalDispatchOrderExternal);

		if (isDisplayed(NewServiceJobOrderPage)) {

			writeTestResults("Select \"Service Order to Internal Dispatch Order (External) \" Journey",
					"Service job order New  page should be open", "Service job order New  page is displayed", "pass");
		} else {

			writeTestResults(" Select \"Service Order to Internal Dispatch Order (External) \" Journey",
					"Service job order New  page should not be open", "Service job order New page is  not displayed",
					"fail");
		}

		switchWindow();

		LocalTime myObj = LocalTime.now();

		sendKeys(ServiceJobOrderTitle, txt_ServiceJobOrderTitle + myObj);

		click(btn_accountsearch);

		if (isDisplayed(AccountLookUPPage)) {

			writeTestResults("Click account search", "AccountLookUPPage should be open",
					"AccountLookUPPage  page is displayed", "pass");
		} else {

			writeTestResults("Click account search ", "AccountLookUPPage should not be open",
					"AccountLookUPPage is not displayed", "fail");
		}

		selectText(template, txt_Template);

		sendKeys(Accountname, txt_Accountname);

		pressEnter(Accountname);

		Thread.sleep(2000);

		doubleClick(btn_accountuser);

		Thread.sleep(1000);

		selectText(ServiceGroup, "IDC-INTER DEPARTMENT JOBS");

		sendKeys(ServiceJobOrderDescription, txt_ServiceJobOrderDescription);

		Thread.sleep(2000);

		click(btn_searchowner);

		Thread.sleep(1000);

		if (isDisplayed(OwnerLookUpPage)) {

			writeTestResults(" Click to search owner ", "OwnerLookUpPage should be open ",
					"OwnerLookUpPage viewed successfully", "pass");
		} else {

			writeTestResults(" Click to search owner ", " OwnerLookUpPage should not  be open",
					"OwnerLookUpPage not viewed successfully", "fail");
		}

		sendKeys(owner, "H V Hewa Vidana");

		pressEnter(owner);

		Thread.sleep(1000);

		doubleClick(btn_owner);

		Thread.sleep(1000);

		selectText(Priority, "High");

		Thread.sleep(1000);

		click(btn_PricingProfileSearch);

		if (isDisplayed(PricingProfileLookUpPage)) {

			writeTestResults(" Select a \"Pricing Profile\" ", " Able to select a pricing profile from the look up",
					"Able to select pricing profile successfully", "pass");
		} else {

			writeTestResults(" Select a \"Pricing Profile\" ", " Not Able to select a pricing profile from the look up",
					"Not able to select pricing profile successfully", "fail");
		}
		Thread.sleep(1000);

		doubleClick(btn_pricingprofile12);
	}

	///////////////////////////////////////////////////////////////////////////////////

	public void CustomerDebitMemoServiceJobOrder() throws Exception {

		Thread.sleep(4000);

		String ServiceJobOrderNo = getText(ServiceJobOrderValue);

		Thread.sleep(2000);

		System.out.println(ServiceJobOrderNo);

		switchWindow();

		Thread.sleep(2000);

		sendKeys(searchjobno, ServiceJobOrderNo);

		Thread.sleep(2000);

		pressEnter(searchjobno);

		Thread.sleep(3000);

		doubleClick(btn_jobnoServiceOrderLookup);

		Thread.sleep(4000);

//			click(btn_AddRecordCostAllocation);

		click("//*[@id=\"btnApportionAddRecord\"]");

		Thread.sleep(3000);

		click(btn_UpdateCostAllocation1);

		Thread.sleep(2000);

		click(btn_narration);

		Thread.sleep(1000);

//				sendKeys(ExtendNarration, txt_ExtendNarration);
		sendKeys(ExtendNarration, "test");

		Thread.sleep(1000);

//			click(btn_narrationapply);

		click("//*[@id=\"inspect\"]/div[13]/div[3]/a");

		click("//*[@id=\"btnCalc2\"]");

		Thread.sleep(2000);

		click(btn_DraftPettyCash);

		Thread.sleep(5000);

		click(btn_ReleasePettyCash);

		Thread.sleep(6000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleasedpettycashPage)) {

			writeTestResults(" Click on Release", "   Should be able to Release the OutboundPaymentAdvice  ",
					" Able to Release the OutboundPaymentAdvice successfully", "pass");
		} else {

			writeTestResults(" Click on Release", "   Should be able to Release the OutboundPaymentAdvice  ",
					"Able to Release the OutboundPaymentAdvice successfully", "fail");
		}

		Thread.sleep(4000);

		String TotalValueVendorRefundvalue = getText(TotalValueVendorRefund);

		Thread.sleep(3000);

		System.out.println(TotalValueVendorRefundvalue);

		Thread.sleep(1000);

//			pageRefersh();
//			
//			switchWindow();

		Thread.sleep(2000);

		click(btn_navigationmenu);

		Thread.sleep(2000);

		if (isDisplayed(sidemenu)) {
			writeTestResults("Click on the top navigation menu", "System should be loaded the menu item successfully",
					"Menu item is displayed", "pass");
		} else {
			writeTestResults("Click on the top navigation menu", "System should not  loaded the menu item successfully",
					"Menu item is not displayed", "fail");
		}

		Thread.sleep(2000);

		click(btn_servicemodule);
//
//			if (isDisplayed(subsidemenu)) {
//
//				writeTestResults("Click on the Service module", "System should be loaded the Service module successfully",
//						"subsidemenu is displayed", "pass");
//			} else {
//
//				writeTestResults("Click on the Service module", "System should not loaded the Service module successfully",
//						"subsidemenu is not displayed", "fail");
//			}

		Thread.sleep(2000);

		click(btn_ServiceJobOrder);

		if (isDisplayed(ServiceJobOrderPage)) {

			writeTestResults("Click on Service job order under menu",
					"Service job order should be available in the menu", "Service job order is displayed", "pass");
		} else {

			writeTestResults("Click on Service job order under menu",
					"Service job order should not available in the menu", "Service job order is not displayed", "fail");
		}

		Thread.sleep(4000);

		selectText(templateJOBNO, "My Template");

		sendKeys(searchjobno, ServiceJobOrderNo);

		pressEnter(searchjobno);

		Thread.sleep(4000);

		doubleClick(btn_jobnoServiceJobOrder);

		Thread.sleep(3000);

		pageRefersh();

		Thread.sleep(3000);

		click(btn_overview);

		Thread.sleep(3000);

		click(btn_RevenueAdjustment);

		Thread.sleep(5000);

		click(btn_InboundPaymetAdviceLinkRevenueAdjustment);

		Thread.sleep(5000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleaseOutboundPaymentPage)) {

			writeTestResults(" Click on OutboundPaymentAdvice link",
					"   Should be able to view theOutboundPaymentAdvice page successfully  ",
					" Able to view the OutboundPaymentAdvice page successfully", "pass");
		} else {

			writeTestResults(" Click on  OutboundPaymentAdvice  link",
					"  Should be able to view the OutboundPaymentAdvice page successfully",
					" Able to view the OutboundPaymentAdvice page successfully", "fail");
		}

		Thread.sleep(3000);

		switchWindow();

		switchWindow();

		Thread.sleep(3000);

		pageRefersh();

		Thread.sleep(3000);

		click(btn_CostingSummaary);

		Thread.sleep(3000);

//			click(btn_RevenueAdjustmentArrow);

		click("//*[@id=\"24\"]");

		Thread.sleep(3000);

		String CustomerAdjustment = getText(CustomerAdjustmentValue);

		Thread.sleep(3000);

		System.out.println(CustomerAdjustment);

		Thread.sleep(3000);

//			if((CustomerAdjustment.replace("-", "")).equals(TotalValueVendorRefundvalue)) {
//				
//				writeTestResults(" Verify added OutboundPaymentAdvice details are displayed", " OutboundPaymentAdvice details should be displayed  ",
//						"OutboundPaymentAdvice details  displayed successfully", "pass");
//			} else {
//
//				writeTestResults(" Verify added OutboundPaymentAdvice details are displayed", " OutboundPaymentAdvice details should not displayed  ",
//						"OutboundPaymentAdvice details not displayed", "fail");
//			}

	}

	public void CustomerDebitMemoServiceJobOrdernew() throws Exception {

		sendKeys(searchjobno, txt_searchjobno);

		pressEnter(searchjobno);

		Thread.sleep(3000);

		doubleClick(btn_jobnoServiceOrderLookup);

		Thread.sleep(3000);

		click(btn_AddRecordCostAllocation);

		click(btn_UpdateCustomerRefund);

		Thread.sleep(2000);

		click(btn_CheckoutInboundPaymentAdvice);

		click(btn_DraftInboundPaymentAdvice);

		Thread.sleep(2000);

		click(btn_ReleasePettyCash);

		pageRefersh();

	}

	public void CustomerRefundServiceJobOrder1() throws Exception {

		Thread.sleep(2000);

		click(btn_navigationmenu);

		if (isDisplayed(sidemenu)) {
			writeTestResults("Click on the top navigation menu", "System should be loaded the menu item successfully",
					"Menu item is displayed", "pass");
		} else {
			writeTestResults("Click on the top navigation menu", "System should not  loaded the menu item successfully",
					"Menu item is not displayed", "fail");
		}

		click(btn_servicemodule);

		if (isDisplayed(subsidemenu)) {

			writeTestResults("Click on the Service module", "System should be loaded the Service module successfully",
					"subsidemenu is displayed", "pass");
		} else {

			writeTestResults("Click on the Service module", "System should not loaded the Service module successfully",
					"subsidemenu is not displayed", "fail");
		}

		click(btn_ServiceJobOrder);

		if (isDisplayed(ServiceJobOrderPage)) {

			writeTestResults("Click on Service job order under menu",
					"Service job order should be available in the menu", "Service job order is displayed", "pass");
		} else {

			writeTestResults("Click on Service job order under menu",
					"Service job order should not available in the menu", "Service job order is not displayed", "fail");
		}

		Thread.sleep(3000);

		selectText(templateJOBNO, "My Template");

		sendKeys(searchjobno, txt_searchjobno);

		pressEnter(searchjobno);

		Thread.sleep(3000);

		doubleClick(btn_jobnoServiceJobOrder);

		Thread.sleep(3000);

		click(btn_overview);

		Thread.sleep(1000);

		click(btn_RevenueAdjustment);

		Thread.sleep(2000);

		click(btn_CostingSummaary);

		Thread.sleep(2000);

		click(btn_Adjustment);
	}

	public void VerifPurchaseinvoicejourney() throws Exception {

		Thread.sleep(1000);

		click(btn_procurement);

		if (isDisplayed(ProcuremnentSubSideMenu)) {

			writeTestResults(" Click on Procuement Module", "  User should be able to click on the Procuement module  ",
					"user able to click  Procuement module successfully", "pass");
		} else {

			writeTestResults(" Click on Procuement Module", "  User should be able to click on the Procuement  module ",
					"user  not able to click Procuement module successfully", "fail");
		}

		click(btn_PurchaseInvoice);

		click(btn_NewPurchaseInvoice);

		Thread.sleep(2000);

		click(btn_NewPurchaseInvoiceService);

		click(btn_searchvendorPurchaseinvoicejourney);

		selectText(template, txt_Template);

		sendKeys(searchvendor, txt_searchvendor);

		pressEnter(searchvendor);

		Thread.sleep(3000);

		doubleClick(btn_vendor);

		Thread.sleep(2000);

		click(btn_BillingAddress);

		Thread.sleep(2000);

//			 sendKeys(BillingAddressPurchaseInvoice, txt_BillingAddressPurchaseInvoice);
		sendKeys(BillingAddressPurchaseInvoice, "No-04, Colombo 05");

		Thread.sleep(1000);

		click(btn_ApplyBillingAddress);

		Thread.sleep(1000);

		click(btn_productPurchaseinvoicejourney);

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, txt_serviceProduct);

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(2000);

//			sendKeys(Discount, txt_Discount);
		sendKeys(Discount, "2");

		Thread.sleep(3000);

		click(btn_CheckoutPurchaseInvoice);

		Thread.sleep(1000);

		Thread.sleep(1000);

		click(btn_CostAllocationPurchaseInvoice);
//
//		if (isDisplayed(CostAllocationPopUp)) {
//
//			writeTestResults(" Click on Cost allocation Icon", "  Cost allocation look up should be open  ",
//					" Cost allocation look up viewed successfully", "pass");
//		} else {
//
//			writeTestResults(" Click on Cost allocation Icon", "  Cost allocation look up should not open  ",
//					"Cost allocation look up not viewed successfully ", "fail");
//		}

		Thread.sleep(1000);

//			selectText(Costcenter, txt_Costcenter);
		selectText(Costcenter, "FIN (FINANCE)");

		sendKeys(Percentage, txt_PercentagePurchaseInvoice);

		Thread.sleep(1000);

//			selectText(CostAssignmentType, txt_CostAssignmentType);
		selectText(CostAssignmentType, "Service Job Order");

		Thread.sleep(1000);

		click(btn_SearchCostObject);

		Thread.sleep(1000);

		selectText(templateJOBNO, "My Template");

		selectText(templateJOBNO, "My Template");

		Thread.sleep(2000);

		click("//b[contains(text(),'New Service Job Order')]");

		Thread.sleep(4000);

		click(btn_ServiceOrdertoInternalDispatchOrderExternal);

		if (isDisplayed(NewServiceJobOrderPage)) {

			writeTestResults("Select \"Service Order to Internal Dispatch Order (External) \" Journey",
					"Service job order New  page should be open", "Service job order New  page is displayed", "pass");
		} else {

			writeTestResults(" Select \"Service Order to Internal Dispatch Order (External) \" Journey",
					"Service job order New  page should not be open", "Service job order New page is  not displayed",
					"fail");
		}

		switchWindow();

		LocalTime myObj = LocalTime.now();

		sendKeys(ServiceJobOrderTitle, txt_ServiceJobOrderTitle + myObj);

		click(btn_accountsearch);

		if (isDisplayed(AccountLookUPPage)) {

			writeTestResults("Click account search", "AccountLookUPPage should be open",
					"AccountLookUPPage  page is displayed", "pass");
		} else {

			writeTestResults("Click account search ", "AccountLookUPPage should not be open",
					"AccountLookUPPage is not displayed", "fail");
		}

		selectText(template, txt_Template);

		sendKeys(Accountname, txt_Accountname);

		pressEnter(Accountname);

		Thread.sleep(2000);

		doubleClick(btn_accountuser);

		Thread.sleep(1000);

		selectText(ServiceGroup, "IDC-INTER DEPARTMENT JOBS");

		sendKeys(ServiceJobOrderDescription, txt_ServiceJobOrderDescription);

		Thread.sleep(2000);

		click(btn_searchowner);

		Thread.sleep(1000);

		if (isDisplayed(OwnerLookUpPage)) {

			writeTestResults(" Click to search owner ", "OwnerLookUpPage should be open ",
					"OwnerLookUpPage viewed successfully", "pass");
		} else {

			writeTestResults(" Click to search owner ", " OwnerLookUpPage should not  be open",
					"OwnerLookUpPage not viewed successfully", "fail");
		}

		sendKeys(owner, "H V Hewa Vidana");

		pressEnter(owner);

		Thread.sleep(1000);

		doubleClick(btn_owner);

		Thread.sleep(1000);

		selectText(Priority, "High");

		Thread.sleep(1000);

		click(btn_PricingProfileSearch);

		if (isDisplayed(PricingProfileLookUpPage)) {

			writeTestResults(" Select a \"Pricing Profile\" ", " Able to select a pricing profile from the look up",
					"Able to select pricing profile successfully", "pass");
		} else {

			writeTestResults(" Select a \"Pricing Profile\" ", " Not Able to select a pricing profile from the look up",
					"Not able to select pricing profile successfully", "fail");
		}
		Thread.sleep(1000);

		doubleClick(btn_pricingprofile12);
	}

	public void PurchaseInvoiceServiceJobOrder() throws Exception {

		Thread.sleep(4000);

		String ServiceJobOrderNo = getText(ServiceJobOrderValue);

		Thread.sleep(2000);

		System.out.println(ServiceJobOrderNo);

		switchWindow();

		Thread.sleep(2000);

		sendKeys(searchjobno, ServiceJobOrderNo);

		Thread.sleep(2000);

		pressEnter(searchjobno);

		Thread.sleep(3000);

		doubleClick(btn_jobnoServiceOrderLookup);

		Thread.sleep(4000);

		click(btn_AddRecordCostAllocation);

		Thread.sleep(3000);

		click(btn_deletePI);

		Thread.sleep(3000);

		click(btn_UpdateCostAllocation1);

		Thread.sleep(2000);

//		click(btn_narration);
//		
//	
//		Thread.sleep(1000);
//
//
//		sendKeys(ExtendNarration, "test");
//
//		Thread.sleep(1000);
//
////		click(btn_narrationapply);
//		
//		click("//*[@id=\"inspect\"]/div[13]/div[3]/a");
//		

		Thread.sleep(2000);

		click("//*[@id=\"btnCalc2\"]");

		Thread.sleep(2000);

		click(btn_DraftPettyCash);

		Thread.sleep(5000);

		click(btn_ReleasePettyCash);

		Thread.sleep(6000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleasedpettycashPage)) {

			writeTestResults(" Click on Release", "   Should be able to Release the Purchase Invoice ",
					" Able to Release the  Purchase Invoicesuccessfully", "pass");
		} else {

			writeTestResults(" Click on Release", "   Should be able to Release the  Purchase Invoice ",
					"Able to Release the  Purchase Invoice successfully", "fail");
		}

		Thread.sleep(4000);

		String TotalValueVendorRefundvalue = getText(TotalValueVendorRefund);

		Thread.sleep(3000);

		System.out.println(TotalValueVendorRefundvalue);

		Thread.sleep(1000);

//		pageRefersh();
//		
//		switchWindow();

		Thread.sleep(2000);

		click(btn_navigationmenu);

		if (isDisplayed(sidemenu)) {
			writeTestResults("Click on the top navigation menu", "System should be loaded the menu item successfully",
					"Menu item is displayed", "pass");
		} else {
			writeTestResults("Click on the top navigation menu", "System should not  loaded the menu item successfully",
					"Menu item is not displayed", "fail");
		}

		click(btn_servicemodule);

		if (isDisplayed(subsidemenu)) {

			writeTestResults("Click on the Service module", "System should be loaded the Service module successfully",
					"subsidemenu is displayed", "pass");
		} else {

			writeTestResults("Click on the Service module", "System should not loaded the Service module successfully",
					"subsidemenu is not displayed", "fail");
		}

		click(btn_ServiceJobOrder);

		if (isDisplayed(ServiceJobOrderPage)) {

			writeTestResults("Click on Service job order under menu",
					"Service job order should be available in the menu", "Service job order is displayed", "pass");
		} else {

			writeTestResults("Click on Service job order under menu",
					"Service job order should not available in the menu", "Service job order is not displayed", "fail");
		}

		Thread.sleep(4000);

		selectText(templateJOBNO, "My Template");

		sendKeys(searchjobno, ServiceJobOrderNo);

		pressEnter(searchjobno);

		Thread.sleep(4000);

		doubleClick(btn_jobnoServiceJobOrder);

		Thread.sleep(3000);

		pageRefersh();

		Thread.sleep(3000);

		click(btn_overview);

		Thread.sleep(3000);

		click(btn_Expense);

		Thread.sleep(5000);

		click(btn_PurchaseInvoiceLink);

		Thread.sleep(5000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleaseOutboundPaymentPage)) {

			writeTestResults(" Click on PurchaseInvoice link",
					"   Should be able to view the  PurchaseInvoice page successfully  ",
					" Able to view the  PurchaseInvoice page successfully", "pass");
		} else {

			writeTestResults(" Click on   PurchaseInvoice link",
					"  Should be able to view the PurchaseInvoice page successfully",
					" Able to view the  PurchaseInvoice page successfully", "fail");
		}

		Thread.sleep(3000);

		switchWindow();

		switchWindow();

		Thread.sleep(3000);

		pageRefersh();

		Thread.sleep(3000);

		click(btn_CostingSummaary);

		Thread.sleep(3000);

//		click(btn_RevenueAdjustmentArrow);

		click("//*[@id=\"5\"]");

		Thread.sleep(3000);

		String CustomerAdjustment = getText(CustomerAdjustmentValue);

		Thread.sleep(3000);

		System.out.println(CustomerAdjustment);

		Thread.sleep(3000);

		if ((CustomerAdjustment.contentEquals("100.00000"))) {

			writeTestResults(" Verify added  PurchaseInvoice details are displayed",
					" PurchaseInvoice details should be displayed  ",
					" PurchaseInvoice details  displayed successfully", "pass");
		} else {

			writeTestResults(" Verify added  PurchaseInvoicedetails are displayed",
					"  PurchaseInvoicedetails should not displayed  ", " PurchaseInvoice details not displayed",
					"fail");
		}
	}

	public void VerifyOutboundLoanOrder() throws Exception {

		click(btn_servicemodule);

		if (isDisplayed(subsidemenu)) {

			writeTestResults("Click on the Service module", "System should be loaded the Service module successfully",
					"subsidemenu is displayed", "pass");
		} else {

			writeTestResults("Click on the Service module", "System should not loaded the Service module successfully",
					"subsidemenu is not displayed", "fail");
		}

		click(btn_ServiceJobOrder);

		if (isDisplayed(ServiceJobOrderPage)) {

			writeTestResults("Click on Service job order under menu",
					"Service job order should be available in the menu", "Service job order is displayed", "pass");
		} else {

			writeTestResults("Click on Service job order under menu",
					"Service job order should not available in the menu", "Service job order is not displayed", "fail");
		}

		click(btn_NewServiceJobOrder);

		if (isDisplayed(StartNewJourneyPage)) {

			writeTestResults(" Click on \"New Service job order\" button in By page",
					"Service job order journey look up should be open",
					"Service job order journey look up is displayed", "pass");
		} else {

			writeTestResults(" Click on \"New Service job order\" button in By page",
					"Service job order journey look up should be open",
					"Service job order journey look up is not displayed", "fail");
		}

		click(btn_ServiceOrdertoInternalDispatchOrderExternal);

		if (isDisplayed(NewServiceJobOrderPage)) {

			writeTestResults("Select \"Service Order to Internal Dispatch Order (External) \" Journey",
					"Service job order New  page should be open", "Service job order New  page is displayed", "pass");
		} else {

			writeTestResults(" Select \"Service Order to Internal Dispatch Order (External) \" Journey",
					"Service job order New  page should not be open", "Service job order New page is  not displayed",
					"fail");
		}
		LocalTime myObj = LocalTime.now();
		sendKeys(ServiceJobOrderTitle, txt_ServiceJobOrderTitle + myObj);

		click(btn_accountsearch);

		if (isDisplayed(AccountLookUPPage)) {

			writeTestResults("Click account search", "AccountLookUPPage should be open",
					"AccountLookUPPage  page is displayed", "pass");
		} else {

			writeTestResults("Click account search ", "AccountLookUPPage should not be open",
					"AccountLookUPPage is not displayed", "fail");
		}

		selectText(template, txt_Template);

		sendKeys(Accountname, txt_Accountname);

		pressEnter(Accountname);

		Thread.sleep(2000);

		doubleClick(btn_accountuser);

		Thread.sleep(4000);

		click(btn_Productso);

		Thread.sleep(4000);

		click(btn_SerialNos);

		Thread.sleep(1000);

//			String tagnoSO = getText(TagNoSO);

		Thread.sleep(2000);

		selectText(ServiceGroup, "IDC-INTER DEPARTMENT JOBS");

		sendKeys(ServiceJobOrderDescription, txt_ServiceJobOrderDescription);

		click(btn_searchowner);

		Thread.sleep(1000);

		doubleClick(btn_owner);

		Thread.sleep(1000);

		selectText(Priority, txt_Priority);

		selectText(Priority, "High");

		Thread.sleep(1000);

		click(btn_PricingProfileSearch);

		if (isDisplayed(PricingProfileLookUpPage)) {

			writeTestResults(" Select a \"Pricing Profile\" ", " Able to select a pricing profile from the look up",
					"Able to select pricing profile successfully", "pass");
		} else {

			writeTestResults(" Select a \"Pricing Profile\" ", " Not Able to select a pricing profile from the look up",
					"Not able to select pricing profile successfully", "fail");
		}

		click(btn_PricingProfile);

		doubleClick(btn_PricingProfile);

		click(btn_searchservicelocation);

		selectText(template, txt_Template);

		sendKeys(ServiceLocation, txt_ServiceLocation);

		pressEnter(ServiceLocation);

		Thread.sleep(3000);

		doubleClick(btn_location);

		Thread.sleep(2000);

		click(btn_searchShippingAddress);

		sendKeys(ShippingAddress, "09,Colombo,Srilanka");

		Thread.sleep(2000);

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript(btn_applyCommon);

		Thread.sleep(2000);

		click(btn_taskdetails);

		if (isDisplayed(TaskDetailsTab)) {

			writeTestResults(" Go to \"Task Details Tab\" ", " Task details tab should be open",
					"task details tab viewed successfully", "pass");
		} else {

			writeTestResults(" Go to \"Task Details Tab\" ", " Task details tab should not open",
					"task details tab not  viewed successfully", "fail");
		}

		Thread.sleep(1000);

		click(btn_expand);

		selectText(tasktype, "Input Product");

		click(btn_searchserialproduct);

		Thread.sleep(3000);

		selectText(template, txt_Template);

		Thread.sleep(2000);

		sendKeys(Product, "newserial001");

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		sendKeys(EstimatedQtyserial, "1");

		Thread.sleep(3000);

		click(btn_DraftServiceJobOrder);

		Thread.sleep(3000);

		click(btn_ReleaseServiceJobOrder);

		Thread.sleep(2000);

		if (isDisplayed(ReleaseServiceJobOrderPage)) {

			writeTestResults("Click on \"Release\" Button", "User should be able to Release the service job order",
					"service job order Released successfully", "pass");
		} else {

			writeTestResults(" Click on \"Release\" Button", "User should not able toRelease the service job order",
					"service job order not  Released successfully", "fail");
		}

		Thread.sleep(4000);

		pageRefersh();

		switchWindow();

		click(btn_action);

		Thread.sleep(1000);

		click(btn_OutBoundloanOrder);

		Thread.sleep(1000);

		switchWindow();

		Thread.sleep(3000);

		click(btn_SearchSerialProduct);

		Thread.sleep(3000);

		selectText(template, txt_Template);

		Thread.sleep(2000);

		sendKeys(Product, "newserial001");

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick("//*[@id=\"g1010-t\"]/table/tbody/tr/td[6]");

		Thread.sleep(4000);

		click(btn_DraftOutboundLoanOrder2);

		Thread.sleep(5000);

		click(btn_ReleaseOutboundLoanOrder);

		Thread.sleep(6000);

		trackCode = getText(ServiceJobOrderReleaseNoTC);

		if (isDisplayed(ReleaseOutboundLoanOrderPage)) {

			writeTestResults("Click on \"Release\" Button", "User should be able to Release the OutboundLoanOrder",
					"OutboundLoanOrder Released successfully", "pass");
		} else {

			writeTestResults(" Click on \"Release\" Button", "User should not able toRelease the OutboundLoanOrder",
					"OutboundLoanOrder not  Released successfully", "fail");
		}

		Thread.sleep(3000);

		click(btn_GoToPageOutboundLoanOrder);

		if (isDisplayed(NewOutboundShipmentPage)) {

			writeTestResults("CLick on \"Go to link\" ", " User should be able to Go to Outbound shipment",
					"New Outbound shipment page  is displayed", "pass");
		} else {

			writeTestResults("CLick on \"Go to link\" ", " User should not able to Go to Outbound shipment",
					"New Outbound shipment page  is not  displayed", "fail");
		}

		Thread.sleep(2000);

		switchWindow();

		click(btn_DraftOutboundShipmentPage);

		Thread.sleep(4000);

		if (isDisplayed(DraftOutboundShipmentPage)) {

			writeTestResults("Click on \"Draft\"  button ", " User should be able to Click on \"Draft\"  button",
					" click draft button successsfully", "pass");
		} else {

			writeTestResults("Click on \"Draft\"  button ", " User should not able to Click on \"Draft\"  button",
					"unable to click draft button successsfully", "fail");
		}

		Thread.sleep(3000);

		click(menu);

		Thread.sleep(2000);

		click(serialWise);

		Thread.sleep(4000);

		String serialNoVal = getText(serialTextValOLO);

		Thread.sleep(2000);

		click(serialMenuClose);

		Thread.sleep(3000);

		click(btn_SerialBatchOutboundShipmentPage);

		Thread.sleep(2000);

		click(btn_ProductOutboundShipmentPage);

		Thread.sleep(2000);

		sendKeys(SerialProductOutboundShipment, serialNoVal);

//		 sendKeys(SerialProductOutboundShipment, "new011");

		Thread.sleep(2000);

		pressEnter(SerialProductOutboundShipment);

		click(btn_refresh);

		Thread.sleep(2000);

		click(btn_arrowtointernaldistbatchorder);

		Thread.sleep(4000);

		click(btn_releaseinternaldispatchorder);

		Thread.sleep(6000);

		trackCode = getText(ServiceJobOrderReleaseNoTC);

		if (isDisplayed(ReleaseServiceJobOrderPage)) {

			writeTestResults("Click on \"Release\" Button", "User should be able to Release the Outboundshipment",
					"Outboundshipment Released successfully", "pass");
		} else {

			writeTestResults(" Click on \"Release\" Button", "User should not able toRelease the Outboundshipment",
					"Outboundshipment not  Released successfully", "fail");
		}

		closeWindow();

		switchWindow();

	}

	public void ReplacetheWarranty() throws Exception {

		click(btn_InventoryandWareHousing);

		click(btn_OutBoundLoanOrder);

		if (isDisplayed(OutBoundLoanOrderOrderPage)) {

			writeTestResults("Open Outbound Loan Order Document",
					" User should be able to open the outbound loan order document",
					"Outbound loan order document displayed", "pass");
		} else {

			writeTestResults("Open Outbound Loan Order Document",
					" User should be able to open the outbound loan order document",
					"Outbound loan order document displayed", "fail");
		}

		doubleClick(btn_OutboundloanOrderDoc);

		Thread.sleep(2000);

		click(btn_action);

		click(btn_WarrantyReplacement);

		if (isDisplayed(WarrantyReplacementPopUp)) {

			writeTestResults("Click on \"Warranty Replacement\" Action",
					" User should be able to Click on \"Warranty Replacement\" Action",
					"WarrantyReplacementPopUp is  displayed", "pass");
		} else {

			writeTestResults("Click on \"Warranty Replacement\" Action",
					" User should be able to Click on \"Warranty Replacement\" Action",
					"WarrantyReplacementPopUp is displayed", "fail");
		}

		Thread.sleep(2000);
		// search
		click(btn_ReplacementSerialNo);

		Thread.sleep(2000);

		click(btn_SerialProductSelect);

		click(btn_apply);

		Thread.sleep(3000);

//	sendkeys(tagnoProductRegis,tagnoSO)

	}

	public void AbletocompletetheServicejoborder() throws Exception {

		click(btn_ServiceJobOrder);

		if (isDisplayed(ServiceJobOrderPage)) {

			writeTestResults("Click on Service job order under menu",
					"Service job order should be available in the menu", "Service job order is displayed", "pass");
		} else {

			writeTestResults("Click on Service job order under menu",
					"Service job order should not available in the menu", "Service job order is not displayed", "fail");
		}

		Thread.sleep(3000);

		selectText(templateJOBNO, "My Template");

		sendKeys(searchjobno, txt_searchjobnoOutboundLoanOrder);

		pressEnter(searchjobno);

		Thread.sleep(3000);

		doubleClick(btn_jobnoServiceJobOrder);

		Thread.sleep(3000);

		click(btn_action);

		Thread.sleep(1000);

		click(btn_stopServiceJobOrder);

		Thread.sleep(1000);

		if (isDisplayed(CompleteProcessPopup)) {

			writeTestResults("Click on Complete Action", "User should be able to Click on the Complete action",
					"User  able to Click on the Complete action sucessfully", "pass");
		} else {

			writeTestResults("Click on Complete Action", "User should not able to Click on the Complete action",
					"User not able to Click on the Complete action sucessfully", "fail");
		}

		Thread.sleep(1000);

		click(btn_CompleteServiceJobOrder);

	}

	public void checkdetails() throws Exception {

		click(btn_servicemodule);

		if (isDisplayed(subsidemenu)) {

			writeTestResults("Click on the Service module", "System should be loaded the Service module successfully",
					"subsidemenu is displayed", "pass");
		} else {

			writeTestResults("Click on the Service module", "System should not loaded the Service module successfully",
					"subsidemenu is not displayed", "fail");
		}

		click(btn_ServiceJobOrder);

		if (isDisplayed(ServiceJobOrderPage)) {

			writeTestResults("Click on Service job order under menu",
					"Service job order should be available in the menu", "Service job order is displayed", "pass");
		} else {

			writeTestResults("Click on Service job order under menu",
					"Service job order should not available in the menu", "Service job order is not displayed", "fail");
		}

		click(btn_NewServiceJobOrder);

		if (isDisplayed(StartNewJourneyPage)) {

			writeTestResults(" Click on \"New Service job order\" button in By page",
					"Service job order journey look up should be open",
					"Service job order journey look up is displayed", "pass");
		} else {

			writeTestResults(" Click on \"New Service job order\" button in By page",
					"Service job order journey look up should be open",
					"Service job order journey look up is not displayed", "fail");
		}

		click(btn_ServiceOrdertoInternalDispatchOrderExternal);

		if (isDisplayed(NewServiceJobOrderPage)) {

			writeTestResults("Select \"Service Order to Internal Dispatch Order (External) \" Journey",
					"Service job order New  page should be open", "Service job order New  page is displayed", "pass");
		} else {

			writeTestResults(" Select \"Service Order to Internal Dispatch Order (External) \" Journey",
					"Service job order New  page should not be open", "Service job order New page is  not displayed",
					"fail");
		}
		LocalTime myObj = LocalTime.now();

		sendKeys(ServiceJobOrderTitle, txt_ServiceJobOrderTitle + myObj);

		click(btn_accountsearch);

		if (isDisplayed(AccountLookUPPage)) {

			writeTestResults("Click account search", "AccountLookUPPage should be open",
					"AccountLookUPPage  page is displayed", "pass");
		} else {

			writeTestResults("Click account search ", "AccountLookUPPage should not be open",
					"AccountLookUPPage is not displayed", "fail");
		}

		selectText(template, txt_Template);

		sendKeys(Accountname, txt_Accountname);

		pressEnter(Accountname);

		Thread.sleep(2000);

		doubleClick(btn_accountuser);

		Thread.sleep(4000);

		click(btn_Productso);

		Thread.sleep(4000);

//		click(btn_SerialNos);

		Thread.sleep(3000);

		String tagnoSO = getText(TagNoSO);

		Thread.sleep(4000);

		click(btn_SerialNos);

		Thread.sleep(4000);

		System.out.println(tagnoSO);

		Thread.sleep(2000);

		selectText(ServiceGroup, "IDC-INTER DEPARTMENT JOBS");

		sendKeys(ServiceJobOrderDescription, txt_ServiceJobOrderDescription);

		click(btn_searchowner);

		Thread.sleep(1000);

		doubleClick(btn_owner);

		Thread.sleep(1000);

		selectText(Priority, txt_Priority);

		selectText(Priority, "High");

		Thread.sleep(1000);

		click(btn_PricingProfileSearch);

		if (isDisplayed(PricingProfileLookUpPage)) {

			writeTestResults(" Select a \"Pricing Profile\" ", " Able to select a pricing profile from the look up",
					"Able to select pricing profile successfully", "pass");
		} else {

			writeTestResults(" Select a \"Pricing Profile\" ", " Not Able to select a pricing profile from the look up",
					"Not able to select pricing profile successfully", "fail");
		}

		click(btn_PricingProfile);

		doubleClick(btn_PricingProfile);

		click(btn_searchservicelocation);

		selectText(template, txt_Template);

		sendKeys(ServiceLocation, txt_ServiceLocation);

		pressEnter(ServiceLocation);

		Thread.sleep(3000);

		doubleClick(btn_location);

		Thread.sleep(2000);

		click(btn_searchShippingAddress);

		sendKeys(ShippingAddress, "09,Colombo,Srilanka");

		Thread.sleep(2000);

		click("//*[@id=\"inspect\"]/div[22]/div[3]/a");

		Thread.sleep(2000);

		click(btn_taskdetails);

		if (isDisplayed(TaskDetailsTab)) {

			writeTestResults(" Go to \"Task Details Tab\" ", " Task details tab should be open",
					"task details tab viewed successfully", "pass");
		} else {

			writeTestResults(" Go to \"Task Details Tab\" ", " Task details tab should not open",
					"task details tab not  viewed successfully", "fail");
		}

		Thread.sleep(1000);

		click(btn_expand);

		selectText(tasktype, "Input Product");

		click(btn_searchserialproduct);

		Thread.sleep(3000);

		selectText(template, txt_Template);

		Thread.sleep(2000);

		sendKeys(Product, "newserial001");

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		sendKeys(EstimatedQtyserial, "1");

		Thread.sleep(3000);

		click(btn_DraftServiceJobOrder);

		Thread.sleep(3000);

		click(btn_ReleaseServiceJobOrder);

		Thread.sleep(6000);

		trackCode = getText(ServiceJobOrderReleaseNoTC);

		if (isDisplayed(ReleaseServiceJobOrderPage)) {

			writeTestResults("Click on \"Release\" Button", "User should be able to Release the service job order",
					"service job order Released successfully", "pass");
		} else {

			writeTestResults(" Click on \"Release\" Button", "User should not able toRelease the service job order",
					"service job order not  Released successfully", "fail");
		}

		Thread.sleep(4000);

		pageRefersh();

		switchWindow();

		click(btn_action);

		Thread.sleep(1000);

		click(btn_OutBoundloanOrder);

		Thread.sleep(1000);

		switchWindow();

		Thread.sleep(3000);

		click(btn_SearchSerialProduct);

		Thread.sleep(3000);

		selectText(template, txt_Template);

		Thread.sleep(2000);

		sendKeys(Product, "newserial001");

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick("//*[@id=\"g1010-t\"]/table/tbody/tr/td[6]");

		Thread.sleep(4000);

		click(btn_DraftOutboundLoanOrder2);

		Thread.sleep(4000);

		click(btn_ReleaseOutboundLoanOrder);

		Thread.sleep(6000);

		trackCode = getText(ServiceJobOrderReleaseNoTC);

		if (isDisplayed(ReleaseOutboundLoanOrderPage)) {

			writeTestResults("Click on \"Release\" Button", "User should be able to Release the OutboundLoanOrder",
					"OutboundLoanOrder Released successfully", "pass");
		} else {

			writeTestResults(" Click on \"Release\" Button", "User should not able toRelease the OutboundLoanOrder",
					"OutboundLoanOrder not  Released successfully", "fail");
		}

		Thread.sleep(3000);

		click(btn_GoToPageOutboundLoanOrder);

		if (isDisplayed(NewOutboundShipmentPage)) {

			writeTestResults("CLick on \"Go to link\" ", " User should be able to Go to Outbound shipment",
					"New Outbound shipment page  is displayed", "pass");
		} else {

			writeTestResults("CLick on \"Go to link\" ", " User should not able to Go to Outbound shipment",
					"New Outbound shipment page  is not  displayed", "fail");
		}

		Thread.sleep(2000);

		switchWindow();

		click(btn_DraftOutboundShipmentPage);

		Thread.sleep(3000);

		if (isDisplayed(DraftOutboundShipmentPage)) {

			writeTestResults("Click on \"Draft\"  button ", " User should be able to Click on \"Draft\"  button",
					" click draft button successsfully", "pass");
		} else {

			writeTestResults("Click on \"Draft\"  button ", " User should not able to Click on \"Draft\"  button",
					"unable to click draft button successsfully", "fail");
		}

		Thread.sleep(3000);

		click(menu);

		Thread.sleep(2000);

		click(serialWise);

		Thread.sleep(4000);

		String serialNoVal = getText(serialTextValOLO);

		Thread.sleep(2000);

		click(serialMenuClose);

		Thread.sleep(3000);

		click(btn_SerialBatchOutboundShipmentPage);

		Thread.sleep(2000);

		click(btn_ProductOutboundShipmentPage);

		Thread.sleep(2000);

		sendKeys(SerialProductOutboundShipment, serialNoVal);

//	 sendKeys(SerialProductOutboundShipment, "new011");

		pressEnter(SerialProductOutboundShipment);

		click(btn_refresh);

		Thread.sleep(2000);

		click(btn_arrowtointernaldistbatchorder);

		Thread.sleep(3000);

		click(btn_releaseinternaldispatchorder);

		Thread.sleep(6000);

		trackCode = getText(ServiceJobOrderReleaseNoTC);

		if (isDisplayed(ReleaseServiceJobOrderPage)) {

			writeTestResults("Click on \"Release\" Button", "User should be able to Release the Outboundshipment",
					"Outboundshipment Released successfully", "pass");
		} else {

			writeTestResults(" Click on \"Release\" Button", "User should not able toRelease the Outboundshipment",
					"Outboundshipment not  Released successfully", "fail");
		}

		closeWindow();

		switchWindow();

		click(btn_navigationmenu);

		click(btn_InventoryandWareHousing);

		click(btn_OutBoundLoanOrder);

		if (isDisplayed(OutBoundLoanOrderOrderPage)) {

			writeTestResults("Open Outbound Loan Order Document",
					" User should be able to open the outbound loan order document",
					"Outbound loan order document displayed", "pass");
		} else {

			writeTestResults("Open Outbound Loan Order Document",
					" User should be able to open the outbound loan order document",
					"Outbound loan order document displayed", "fail");
		}

		doubleClick(btn_OutboundloanOrderDoc);

		Thread.sleep(2000);

		click(btn_action);

		Thread.sleep(2000);

		click(btn_WarrantyReplacement);

		if (isDisplayed(WarrantyReplacementPopUp)) {

			writeTestResults("Click on \"Warranty Replacement\" Action",
					" User should be able to Click on \"Warranty Replacement\" Action",
					"WarrantyReplacementPopUp is  displayed", "pass");
		} else {

			writeTestResults("Click on \"Warranty Replacement\" Action",
					" User should be able to Click on \"Warranty Replacement\" Action",
					"WarrantyReplacementPopUp is displayed", "fail");
		}

		Thread.sleep(4000);
		// search
		click(btn_ReplacementSerialNo);

		Thread.sleep(2000);

		click(btn_SerialProductSelect);

		click(btn_apply);

		Thread.sleep(4000);

//	switchWindow();

		click(btn_navigationmenu1);

		Thread.sleep(3000);

//	click(btn_InventoryandWareHousing);

		click("//*[@id=\"6sideSch\"]");

		Thread.sleep(3000);

		click(btn_ServiceProductRegistration);

		Thread.sleep(5000);

		sendKeys(SearchTagNo, tagnoSO);

		Thread.sleep(3000);

		pressEnter(SearchTagNo);

		Thread.sleep(2000);

		doubleClick(btn_SpecificTagNo);

		Thread.sleep(3000);

		Thread.sleep(6000);

		trackCode = getText(ServiceJobOrderReleaseNoTC);

		if (isDisplayed(ProductRegistrationPage)) {

			writeTestResults("Click on \"Release\" Button", "User should be able to view the ProductRegistrationPage",
					"ProductRegistrationPage Viewd successfully", "pass");
		} else {

			writeTestResults(" Click on \"Release\" Button", "User should not able to view the ProductRegistrationPage",
					"ProductRegistrationPage not viewed successfully", "fail");
		}

		Thread.sleep(6000);

		String customeracc = getText(customeraccountvalue);

		Thread.sleep(3000);

		System.out.println(customeracc);

		Thread.sleep(3000);

		if (customeracc.equals("")) {
			writeTestResults("Verify customer details are remove ", " Customer details should be removed",
					" Customer details  removed successfully", "pass");
		} else {

			writeTestResults(" Verify customer details are remove ", "Customer details should be removed",
					"Customer details not removed successfully", "fail");
		}

		Thread.sleep(6000);

		String warrantyprofile = getText(warrantyprofilevalue);

		Thread.sleep(3000);

		System.out.println(warrantyprofile);

		Thread.sleep(3000);

		if (warrantyprofile.equals("")) {
			writeTestResults("Verify sales warranty details are remove  ", " Sales warranty details should be removed",
					"  Sales warranty details removed successfully", "pass");
		} else {

			writeTestResults(" Verify sales warranty details are remove ", " Sales warranty details should be removed",
					"Sales warranty details not removed successfully", "fail");
		}

	}

	public void ServiceContract() throws Exception {
		click(btn_ServiceContract);

		Thread.sleep(1000);

		//click(btn_NewServiceContractForm);
		click("//a[@id='btnUpdate']//b[(text()='New Service Contract')]");

		Thread.sleep(3000);

		if (isDisplayed("//label[@id='lblTemplateFormHeader'][text()='New']")) {

			writeTestResults(" Clcik on \"New Service Contract \" Form",
					"User should be able to click on the New Service Contractbutton",
					"User  able to click on the New Service Contractbutton sucessfully", "pass");
		} else {

			writeTestResults(" Clcik on \"New Service Contract \" Form",
					"User should be able to click on the New Service Contractbutton",
					"User not able to click on the New Service Contractbutton sucessfully", "fail");
		}
		sendKeys("//input[@id='txtContractNo']", "Test");
		sendKeys(ServiceContractTitle, txt_ServiceContractTitle);

		click(btn_SearchCustomerServiceContract);

		Thread.sleep(3000);

		selectText(template, txt_Template);
		Thread.sleep(2000);
		sendKeys(customer, txt_customer);

		Thread.sleep(2000);

		pressEnter(customer);

		Thread.sleep(3000);

		doubleClick(btn_customer);

		Thread.sleep(2000);

		click(btn_searchowner);

		selectText(template, txt_Template);
		Thread.sleep(2000);
		sendKeys(searchowner, "Nihal Peiris");

		Thread.sleep(2000);

		pressEnter(searchowner);

		Thread.sleep(3000);

		doubleClick(btn_owner);

		Thread.sleep(2000);

		click(btn_contractdate);
		Thread.sleep(2000);
		click(btn_beforedate);

		Thread.sleep(3000);

		click(btn_beforedate);

		Thread.sleep(3000);

		click(btn_selectcontractdate);

		Thread.sleep(2000);

		click(btn_EffectiveFrom);

//		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss");
//		LocalDateTime now = LocalDateTime.now();
//		
//		 String curDate = dtf.format(now);
//		 String d = curDate.substring(3, 5);
//		 String final_day =btn_selectRevercedate.replace("Day",d);
//		 
//		 if(curDate.substring(3, 4).equals("0")) {
//			 click(btn_selectRevercedate.replace("Day",curDate.substring(4, 5)));
//		 }else {
//			 click(final_day);
//		 }

		Thread.sleep(1000);
		click(btn_backdate);
		Thread.sleep(2000);
		click(btn_EffectiveFromDate);

		Thread.sleep(2000);
		click(btn_EffectiveTo);

		click(btn_nextMonthDatePlicker);
		Thread.sleep(1000);
		click(btn_EffectiveToDate);

		Thread.sleep(1000);

		selectText(ContractGroup, "Schindler Installation");

		Thread.sleep(1000);

		click(btn_ServiceLevelAgreement);

		Thread.sleep(2000);

//		 click(btn_ServiceLevelAgreement1);

		sendKeys(btn_ServiceLevelAgreement1, "SLA001");
		Thread.sleep(2000);
		pressEnter(btn_ServiceLevelAgreement1);

		Thread.sleep(2000);

		doubleClick(btn_agreementname);

		Thread.sleep(1000);

		sendKeys(Description, txt_Description);

		click(btn_searchserviceproduct1);

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, txt_serviceProduct );

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(2000);

		sendKeys(EstimatedCost, txt_EstimatedCost);

		Thread.sleep(2000);

		click(btn_SearchSceduleCode);

		Thread.sleep(2000);

		sendKeys(SceduleCode, txt_SceduleCode);

		pressEnter(SceduleCode);

		Thread.sleep(2000);

		doubleClick(btn_SceduleCode);

		Thread.sleep(2000);

		click(btn_responsibleuser);

		Thread.sleep(1000);

		selectText(template, txt_Template);
		Thread.sleep(1000);
		sendKeys(responsibleuser, "Nihal Peiris");

		Thread.sleep(2000);

		pressEnter(responsibleuser);

		Thread.sleep(3000);

		doubleClick(btn_responsibleusername);

		Thread.sleep(2000);

		click(btn_ContractLine);
		Thread.sleep(2000);
		selectText(tagno, txt_tagno);

		Thread.sleep(2000);

		click(btn_Summary);

		Thread.sleep(2000);

		click(btn_checkoutServiceContract);

		Thread.sleep(0300);

		click(btn_draftServiceContract);

		Thread.sleep(3000);

		click(btn_editServiceContract);

		Thread.sleep(3000);

		click(btn_PreventiveMaintenance);

		Thread.sleep(1000);

		click(btn_plus);

		sendKeys(Title, txt_title);

		Thread.sleep(1000);

		selectText(PMCategory, "Service");

		Thread.sleep(1000);

		selectText(PMinterval, txt_PMinterval);

//		click(btn_down);

		Thread.sleep(1000);

//		mouseMove(PMcount);

		sendKeys(PMcount, "2");

		Thread.sleep(1000);

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript(btn_applyCommon);

		Thread.sleep(1000);

		click(btn_Summary);

		Thread.sleep(2000);

		click(btn_checkoutServiceContract);

		Thread.sleep(1000);

		click(btn_updateServiceContract);

		Thread.sleep(1000);

		click(btn_PreventiveMaintenance);

		Thread.sleep(1000);

		click(btn_ChargeableProd);

		Thread.sleep(1000);

		click(btn_searchproducts);

		if (isDisplayed(ProductLookUpPage)) {

			writeTestResults(" Click search to product", "Should be able to view product look up page",
					"product look up page viewed successfully", "pass");
		} else {

			writeTestResults(" Click search to product", "Should not able to view product look up page",
					"product look up page not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, txt_SerialSpecificProduct);

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(1000);

		sendKeys(qty, "2");

		Thread.sleep(1000);

		js.executeScript(btn_applyCommon);

		Thread.sleep(2000);

		click(btn_ChargeableProd1);

		Thread.sleep(1000);

		click(btn_searchproducts);

		if (isDisplayed(ProductLookUpPage)) {

			writeTestResults(" Click search to product", "Should be able to view product look up page",
					"product look up page viewed successfully", "pass");
		} else {

			writeTestResults(" Click search to product", "Should not able to view product look up page",
					"product look up page not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, txt_SerialSpecificProduct);

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(3000);

		js.executeScript(btn_applyCommon);

		click(btn_UpdatePreventiveMaintenance);

		Thread.sleep(1000);

		click(btn_ReleaseServiceQuatation);

		Thread.sleep(1000);

		trackCode = getText(ServiceJobOrderReleaseNoTC);

		if (isDisplayed(ReleaseServiceContractPage)) {

			writeTestResults("Click on \"Release\" Button", "User should be able to Release the ServiceContract",
					"ServiceContract Released successfully", "pass");
		} else {

			writeTestResults(" Click on \"Release\" Button", "User should not able toRelease the ServiceContract",
					"ServiceContract not  Released successfully", "fail");
		}

		Thread.sleep(3000);

		pageRefersh();

		click(btn_action);

		click(btn_ConverttoServiceInvoice);

		Thread.sleep(3000);

		click(btn_ServiceDetails);

		Thread.sleep(1000);

		click(btn_checkoutServiceInvoice1);

		click(btn_draftServiceInvoice1);

		Thread.sleep(3000);

		click(btn_releaseServiceInvoice1);

		Thread.sleep(1000);

		trackCode = getText(ServiceJobOrderReleaseNoTC);

		if (isDisplayed(ReleasedServiceInvoice1page)) {

			writeTestResults("Click on \"Release\" Button", "User should be able to Release the ServiceInvoice",
					"ServiceInvoice Released successfully", "pass");
		} else {

			writeTestResults(" Click on \"Release\" Button", "User should not able toRelease the ServiceInvoice",
					"ServiceInvoice not  Released successfully", "fail");
		}

		Thread.sleep(2000);
//
//		click(btn_entution);
//
//		Thread.sleep(3000);
//
//		click(btn_Rolecenter);
//
//		Thread.sleep(3000);
//
//		if (isDisplayed(Dashboardpage)) {
//
//			writeTestResults("Click on \"Role Center Tile\"", "User should be able to Click on \"Role Center Tile\" ",
//					"Role Center Tile viewed successfully", "pass");
//		} else {
//
//			writeTestResults(" Click on \"Role Center Tile\"", "User should be able to Click on \"Role Center Tile\" ",
//					"Role Center Tile not  viewed successfully", "fail");
//		}
//
//		Thread.sleep(2000);
//
//		click(btn_ScheduledJobs);
//
//		Thread.sleep(2000);
//
//		if (isDisplayed(Dashboardpage)) {
//
//			writeTestResults("Click on \"Scheduled Jobs\"tile",
//					"User should be able to Click on \"Scheduled Jobs\"tile ", "Role Center Tile viewed successfully",
//					"pass");
//		} else {
//
//			writeTestResults(" Click on \"Scheduled Jobs\"tile",
//					"User should be able to Click on \"Role Center Tile\" ",
//					"Role Center Tile not  viewed successfully", "fail");
//		}
//
//		Thread.sleep(1000);
	}

	public void ReverceServiceContract() throws Exception {

		Thread.sleep(2000);

		switchWindow();

		click(btn_navigationmenu);

		if (isDisplayed(sidemenu)) {
			writeTestResults("Click on the top navigation menu", "System should be loaded the menu item successfully",
					"Menu item is displayed", "pass");
		} else {
			writeTestResults("Click on the top navigation menu", "System should not  loaded the menu item successfully",
					"Menu item is not displayed", "fail");
		}

		click(btn_servicemodule);

		if (isDisplayed(subsidemenu)) {

			writeTestResults("Click on the Service module", "System should be loaded the Service module successfully",
					"subsidemenu is displayed", "pass");
		} else {

			writeTestResults("Click on the Service module", "System should not loaded the Service module successfully",
					"subsidemenu is not displayed", "fail");
		}

		click(btn_ServiceContract);

		Thread.sleep(3000);

		click(btn_OldServiceContract);

		Thread.sleep(3000);

		click(btn_ServiceContractLine);

		Thread.sleep(1000);

		click(btn_ServiceInvoiceLink);

		Thread.sleep(1000);

		switchWindow();

		click(btn_action);

		Thread.sleep(1000);

		click(btn_reverce);

		Thread.sleep(1000);
		
		InventoryAndWarehouseModuleRegression obj = new InventoryAndWarehouseModuleRegression();
		obj.customizeLoadingDelay(RevercePopupPage, 20);
		if (isDisplayed(RevercePopupPage)) {

			writeTestResults("Click on \"btn_reverce\"", "User should be able to Click on \"btn_reverce\" ",
					"RevercePopupPage Tile viewed successfully", "pass");
		} else {

			writeTestResults(" Click on \"btn_reverce\"", "User should be able to Click on \"btn_reverce\" ",
					"RevercePopupPage Tile not  viewed successfully", "fail");
		}

		obj.customizeLoadingDelay(btn_Revercedate, 15);
		click(btn_Revercedate);

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();

		String curDate = dtf.format(now);
		String d = curDate.substring(3, 5);
		String final_day = btn_selectRevercedate.replace("Day", d);

		if (curDate.substring(3, 4).equals("0")) {
			click(btn_selectRevercedate.replace("Day", curDate.substring(4, 5)));
		} else {
			click(final_day);
		}
		Thread.sleep(1000);

		click(btn_reverceapply);

		Thread.sleep(2000);

		click(btn_yesServiceInvoice);

		Thread.sleep(3000);

		switchWindow();

		Thread.sleep(3000);

		click(btn_action);

		Thread.sleep(1000);

		click(btn_reverceservicecontract);

		Thread.sleep(2000);

		click(btn_yesServiceContract);

		pageRefersh();

	}

	public void ServiceContractFirstday() throws Exception {
		click(btn_ServiceContract);

		Thread.sleep(1000);

		click(btn_NewServiceContractForm);

		Thread.sleep(1000);

		if (isDisplayed(NewServiceContractFormPage)) {

			writeTestResults(" Click on \"New Service Contract \" Form",
					"User should be able to click on the New Service Contractbutton",
					"User  able to click on the New Service Contractbutton sucessfully", "pass");
		} else {

			writeTestResults(" Click on \"New Service Contract \" Form",
					"User should be able to click on the New Service Contractbutton",
					"User not able to click on the New Service Contractbutton sucessfully", "fail");
		}

		sendKeys(ServiceContractTitle, txt_ServiceContractTitle);

		click(btn_SearchCustomerServiceContract);

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(customer, txt_customer);

		Thread.sleep(2000);

		pressEnter(customer);

		Thread.sleep(3000);

		doubleClick(btn_customer);

		Thread.sleep(2000);

		click(btn_searchowner);

		selectText(template, txt_Template);

		sendKeys(searchowner, "Nihal Peiris");

		Thread.sleep(2000);

		pressEnter(searchowner);

		Thread.sleep(3000);

		doubleClick(btn_owner);

		Thread.sleep(2000);

		click(btn_contractdate);

		Thread.sleep(1000);

		click(btn_beforedate);

		Thread.sleep(1000);

		click(btn_beforedate);

		Thread.sleep(1000);

		click("//*[@id=\"ui-datepicker-div\"]/table/tbody/tr[2]/td[3]");

		Thread.sleep(1000);

		click(btn_EffectiveFrom);

		Thread.sleep(1000);

		click(btn_beforedate);

		Thread.sleep(1000);

		click(btn_beforedate);

		click("//*[@id=\"ui-datepicker-div\"]/table/tbody/tr[3]/td[3]/a");

		click(btn_EffectiveTo);

		Thread.sleep(1000);

		click(btn_beforedate);

		click("//*[@id=\"ui-datepicker-div\"]/table/tbody/tr[3]/td[6]");

		selectText(ContractGroup, "Schindler Installation");

		click(btn_ServiceLevelAgreement);

		Thread.sleep(2000);

//		 click(btn_ServiceLevelAgreement1);

		sendKeys(btn_ServiceLevelAgreement1, "SLA001");

		pressEnter(btn_ServiceLevelAgreement1);

		Thread.sleep(2000);

		doubleClick(btn_agreementname);

		Thread.sleep(1000);

		sendKeys(Description, txt_Description);

		click(btn_searchserviceproduct1);

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, txt_serviceProduct);

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(2000);

		sendKeys(EstimatedCost, txt_EstimatedCost);

		Thread.sleep(2000);

		click(btn_SearchSceduleCode);

		Thread.sleep(2000);

		sendKeys(SceduleCode, txt_SceduleCodeFirstDayFM);

		pressEnter(SceduleCode);

		Thread.sleep(2000);

		doubleClick(btn_SceduleCode);

		Thread.sleep(2000);

		click(btn_responsibleuser);

		Thread.sleep(1000);

		selectText(template, txt_Template);

		sendKeys(responsibleuser, "Nihal Peiris");

		Thread.sleep(2000);

		pressEnter(responsibleuser);

		Thread.sleep(3000);

		doubleClick(btn_responsibleusername);

		Thread.sleep(2000);

		click(btn_ContractLine);

		selectText(tagno, txt_tagno);

		Thread.sleep(2000);

		click(btn_Summary);

		Thread.sleep(2000);

		click(btn_checkoutServiceContract);

		click(btn_draftServiceContract);

		Thread.sleep(3000);

		click(btn_editServiceContract);

		Thread.sleep(3000);

		click(btn_PreventiveMaintenance);

		Thread.sleep(1000);

		click(btn_plus);

		sendKeys(Title, txt_title);

		Thread.sleep(1000);

		selectText(PMCategory, "Service");

		Thread.sleep(1000);

		selectText(PMinterval, txt_PMinterval);

//		click(btn_down);

		Thread.sleep(1000);

//		mouseMove(PMcount);

		sendKeys(PMcount, "20");

		Thread.sleep(1000);

		click(btn_apply);

		Thread.sleep(1000);

		click(btn_Summary);

		Thread.sleep(1000);

		click(btn_checkoutServiceContract);

		Thread.sleep(1000);

		click(btn_updateServiceContract);

		Thread.sleep(1000);

		click(btn_PreventiveMaintenance);

		Thread.sleep(1000);

		click(btn_ChargeableProd);

		Thread.sleep(1000);

		click(btn_searchproducts);

		if (isDisplayed(ProductLookUpPage)) {

			writeTestResults(" Click search to product", "Should be able to view product look up page",
					"product look up page viewed successfully", "pass");
		} else {

			writeTestResults(" Click search to product", "Should not able to view product look up page",
					"product look up page not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, txt_SerialSpecificProduct);

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(3000);

		click(btn_apply);

		click(btn_UpdatePreventiveMaintenance);

		Thread.sleep(1000);

		click(btn_ReleaseServiceQuatation);

		Thread.sleep(1000);

		trackCode = getText(ServiceJobOrderReleaseNoTC);

		if (isDisplayed(ReleaseServiceContractPage)) {

			writeTestResults("Click on \"Release\" Button", "User should be able to Release the ServiceContract",
					"ServiceContract Released successfully", "pass");
		} else {

			writeTestResults(" Click on \"Release\" Button", "User should not able toRelease the ServiceContract",
					"ServiceContract not  Released successfully", "fail");
		}

		Thread.sleep(3000);

		pageRefersh();

		click(btn_action);

		click(btn_ConverttoServiceInvoice);

		Thread.sleep(3000);

		click(btn_ServiceDetails);

		Thread.sleep(1000);

		click(btn_checkoutServiceInvoice1);

		click(btn_draftServiceInvoice1);

		Thread.sleep(1000);

		switchWindow();

		click(btn_releaseServiceInvoice1);

		Thread.sleep(1000);

		trackCode = getText(ServiceJobOrderReleaseNoTC);

		if (isDisplayed(ReleasedServiceInvoice1page)) {

			writeTestResults("Click on \"Release\" Button", "User should be able to Release the ServiceInvoice",
					"ServiceInvoice Released successfully", "pass");
		} else {

			writeTestResults(" Click on \"Release\" Button", "User should not able toRelease the ServiceInvoice",
					"ServiceInvoice not  Released successfully", "fail");
		}

		Thread.sleep(1000);

		click(btn_entution);

		Thread.sleep(1000);

		click(btn_Rolecenter);

		Thread.sleep(2000);

		if (isDisplayed(Dashboardpage)) {

			writeTestResults("Click on \"Role Center Tile\"", "User should be able to Click on \"Role Center Tile\" ",
					"Role Center Tile viewed successfully", "pass");
		} else {

			writeTestResults(" Click on \"Role Center Tile\"", "User should be able to Click on \"Role Center Tile\" ",
					"Role Center Tile not  viewed successfully", "fail");
		}

		Thread.sleep(1000);

		click(btn_ScheduledJobs);

		Thread.sleep(1000);
	}

	// smoke Tc004
	public void ServiceQuotation() throws Exception {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Thread.sleep(3000);

		click(btn_ServiceQuotation);

		if (isDisplayed(ServiceQuotationPage)) {

			writeTestResults("Clcik on Service Quotation Form",
					"User should be able to Click on Service Quotation Form ",
					"Service Quotation Form viewed successfully", "pass");
		} else {

			writeTestResults(" Clcik on Service Quotation Form",
					"User should be able to Click on Service Quotation Form ",
					"Service Quotation Form not  viewed successfully", "fail");
		}
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Thread.sleep(2000);

		click(btn_newServiceQuotation);

		Thread.sleep(1000);

		click(btn_newServiceQuotationtoServiceJobOrder);

		Thread.sleep(1000);

		click(btn_SearchCustomerServiceQuoatation);

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(customer, "C/0040");

		Thread.sleep(2000);

		pressEnter(customer);

		Thread.sleep(3000);

		doubleClick(btn_customer);

		Thread.sleep(2000);

		selectText(currency, txt_currency);

		click(btn_searchBillingAddress);

		sendKeys(BillingAddress1, txt_BillingAddress1);

		Thread.sleep(1000);

		sendKeys(ShipingAddress1, txt_ShipingAddress1);

		Thread.sleep(1000);

		click(btn_ApplyBillingAddress);

		selectText(salesunit, txt_salesunit);

		Thread.sleep(2000);

		click(btn_ServiceDetails1);

		Thread.sleep(1000);

		selectText(serviceproduct, "Services");

		Thread.sleep(1000);

		click(btn_searchServiceProduct);

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, txt_serviceProduct);

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(2000);

		sendKeys(QTY1, "5");

		sendKeys(unitprice, "200");

		Thread.sleep(1000);

		Thread.sleep(1000);

		// serial Product

		click(btn_plusSQ1);

		Thread.sleep(1000);

		click(btn_searchSerialProduct);

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, "SP-test-001");// serial specific

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		sendKeys(QTY2, "5");

		Thread.sleep(2000);

		// select labour

		click(btn_plus2);

		Thread.sleep(1000);

		selectText(typelabour, "Labour");

		click(btn_searchlabouremployeeSQ);

		Thread.sleep(3000);

		sendKeys(LabourEmployee, "m.rashi");

		Thread.sleep(2000);

		pressEnter(LabourEmployee);

		Thread.sleep(3000);

		doubleClick(btn_LabourEmployee);

		Thread.sleep(2000);

		Thread.sleep(1000);

		click(btn_searchlabourSQ);

		selectText(template, txt_Template);

		sendKeys(Product, txt_serviceProduct);

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(2000);

		sendKeys(QTY3, "5");

		sendKeys(unitpricelabour, "200");

		Thread.sleep(3000);

		click(btn_checkoutServiceQuatation1);

		Thread.sleep(1000);

		click(btn_draftSericeQuotationNew);

		Thread.sleep(3000);

		if (isDisplayed(DraftedSericeQuotationNewPage)) {

			writeTestResults("Click on \"Draft\" Button", "User should be able to Draft the SericeQuotation",
					"SericeQuotation Drafted successfully", "pass");
		} else {

			writeTestResults(" Click on \"(Draft\" Button", "User should not able to Draft the SericeQuotation",
					"SericeQuotation not  Drafted successfully", "fail");

		}
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Thread.sleep(4000);

		click(btn_ReleaseSericeQuotationNew);

		Thread.sleep(5000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleaseSericeQuotationNewPage)) {

			writeTestResults("Click on \"Release\" Button", "User should be able to Release the SericeQuotation",
					"SericeQuotation Released successfully", "pass");
		} else {

			writeTestResults(" Click on \"Release\" Button", "User should not able toRelease the SericeQuotation",
					"SericeQuotation not  Released successfully", "fail");
		}

		Thread.sleep(1000);

		click(btn_version);

		mouseMove(version);

		click(btn_tick);

		Thread.sleep(1000);

		click(btn_confirm);

		Thread.sleep(1000);

		click(btn_gotopageServiceQuatation);

		Thread.sleep(3000);

		switchWindow();

		Thread.sleep(2000);

		click(btn_ServiceDetails2);

		Thread.sleep(2000);

		click(btn_checkoutServiceOrder1);

		Thread.sleep(3000);

		click(btn_DraftServiceOrder1);

		if (isDisplayed(DraftedServiceOrderNewPage)) {

			writeTestResults("Click on \"Draft\" Button", "User should be able to Draft the ServiceOrder",
					"ServiceOrder Drafted successfully", "pass");
		} else {

			writeTestResults(" Click on \"(Draft\" Button", "User should not able to Draft the ServiceOrder",
					"ServiceOrder not  Drafted successfully", "fail");

		}

		Thread.sleep(5000);

		click(btn_ReleaseServiceOrder1);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Thread.sleep(5000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleaseSericeQuotationNewPage)) {

			writeTestResults("Click on \"Release\" Button", "User should be able to Release the ServiceOrder",
					"ServiceOrder Released successfully", "pass");

		} else {

			writeTestResults(" Click on \"Release\" Button", "User should not able toRelease the ServiceOrder",
					"ServiceOrder not  Released successfully", "fail");

		}

		pageRefersh();

		Thread.sleep(1000);

		click(btn_actionSO);

		Thread.sleep(1000);

		click(btn_CreateSJO);

		Thread.sleep(3000);

		LocalTime myObj = LocalTime.now();

		sendKeys(ServiceJobOrderTitle, txt_ServiceJobOrderTitle + myObj);

//			click(btn_accountsearch);
//				
//			if(isDisplayed(AccountLookUPPage)) {
//					
//			writeTestResults("Click account search", "AccountLookUPPage should be open", "AccountLookUPPage  page is displayed", "pass");
//			
//			}else {
//					
//			writeTestResults("Click account search ", "AccountLookUPPage should not be open", "AccountLookUPPage is not displayed", "fail");
//			}
//				
//				 selectText(template,txt_Template);
//					
//				 sendKeys(Accountname,txt_Accountname);
//				 
//				 pressEnter(Accountname);
//				 
//				 Thread.sleep(2000);
//				
//				 doubleClick(btn_accountuser);
//				
//				 Thread.sleep(1000);
//				 
//				 click(btn_Productso);
//			 
//			    Thread.sleep(4000);
//				 
//				 click(btn_SerialNos);

		Thread.sleep(3000);

		selectText(ServiceGroup, "IDC-INTER DEPARTMENT JOBS");

		sendKeys(ServiceJobOrderDescription, txt_ServiceJobOrderDescription);

		click(btn_searchowner);

		Thread.sleep(2000);

		if (isDisplayed(OwnerLookUpPage)) {

			writeTestResults(" Click to search owner ", "OwnerLookUpPage should be open ",
					"OwnerLookUpPage viewed successfully", "pass");
		} else {

			writeTestResults(" Click to search owner ", " OwnerLookUpPage should not  be open",
					"OwnerLookUpPage not viewed successfully", "fail");
		}

		sendKeys(owner, "H V Hewa Vidana");

		pressEnter(owner);

		Thread.sleep(2000);

		doubleClick(btn_owner);

		Thread.sleep(2000);

		selectText(Priority, "High");

		Thread.sleep(1000);

		click(btn_PricingProfileSearch);

		if (isDisplayed(PricingProfileLookUpPage)) {

			writeTestResults(" Select a \"Pricing Profile\" ", " Able to select a pricing profile from the look up",
					"Able to select pricing profile successfully", "pass");
		} else {

			writeTestResults(" Select a \"Pricing Profile\" ", " Not Able to select a pricing profile from the look up",
					"Not able to select pricing profile successfully", "fail");
		}
		Thread.sleep(1000);

		doubleClick(btn_pricingprofile12);

		click(btn_searchservicelocation);

		selectText(template, txt_Template);

		sendKeys(ServiceLocation, txt_ServiceLocation);

		pressEnter(ServiceLocation);

		Thread.sleep(3000);

		doubleClick(btn_location);

		Thread.sleep(2000);

		click(btn_taskdetails);

		if (isDisplayed(filledcoloums)) {

			writeTestResults(" click taskdetails ", " Able to view the products",
					"Able to view the products successfully", "pass");
		} else {

			writeTestResults(" click taskdetails ", " Not Able to view the products",
					"Not able to view the products successfully", "fail");
		}

		Thread.sleep(3000);

		click(btn_DraftServiceJobOrder);

		Thread.sleep(3000);

		if (isDisplayed("//*[@id=\"permissionBar\"]/a[2]")) {

			writeTestResults("Click on \"Draft\" Button", "User should be able to Draft the service job order",
					"service job order Drafted successfully", "pass");
		} else {

			writeTestResults(" Click on \"Draft\" Button", "User should not able to Draft the service job order",
					"service job order not  Drafted successfully", "fail");
		}

		Thread.sleep(3000);

		click(btn_ReleaseServiceJobOrder);

		// Thread.sleep(2000);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		trackCode = getText(ServiceJobOrderReleaseNoTC);

		if (isDisplayed(ReleaseServiceJobOrderPage)) {

			writeTestResults("Click on \"Release\" Button", "User should be able to Release the service job order",
					"service job order Released successfully", "pass");
		} else {

			writeTestResults(" Click on \"Release\" Button", "User should not able toRelease the service job order",
					"service job order not  Released successfully", "fail");
		}
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Thread.sleep(3000);

		click(btn_action);
		Thread.sleep(2000);
		click(btn_CreateInternalOrder);

		if (isDisplayed(GernerateInternalOrderPage)) {

			writeTestResults("Click on Create Internal Order Action",
					"\"Generate Internal Order\" look up should be displayed with the input product in service job order",
					"Generate Internal Order\" look up viewed successfully", "pass");
		} else {

			writeTestResults(" Click on Create Internal Order Action",
					"\"Generate Internal Order\" look up should not displayed with the input product in service job order",
					"Generate Internal Order\" look up not  viewed successfully", "fail");
		}

		click(btn_selectall);

		Thread.sleep(2000);

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript(btn_applyCommon);

		Thread.sleep(3000);

		switchWindow();

		//click(btn_DraftIO);
		click("//div[@id='permissionBar']//a[text()='Draft']");

		Thread.sleep(3000);

		if (isDisplayed("//label[@id='lbldocstatus'][text()='(Draft)']")) {

			writeTestResults("Click on \"Draft\" Button", "User should be able to Draft the Internal Order",
					"Internal Order Drafted successfully", "pass");
		} else {

			writeTestResults(" Click on \"Draft\" Button", "User should not able to Draft the Internal Order",
					"Internal Order not  Drafted successfully", "fail");
		}

		Thread.sleep(4000);

		click("//div[@id='permissionBar']//a[text()='Release']");

		Thread.sleep(4000);

//		trackCode = getText(InternalOrderReleaseNoTC);

		if (isDisplayed(InternalOrderInformationPopUp)) {

			writeTestResults("User should be able to Release the Internal Order",
					" User should be able to Release the Internal Order", " Inrernal Order released successfully",
					"pass");
		} else {

			writeTestResults(" User should be able to Release the Internal Order",
					" User should not able to Release the Internal Order", " Inrernal Order not released successfully",
					"fail");
		}

		Thread.sleep(2000);

		click(btn_gotopage);

		Thread.sleep(2000);

		switchWindow();

		sendKeys(InternalDispatchOrderShippingAddress, txt_InternalDispatchOrderShippingAddress);

		click(btn_DraftInternalDispatchOrder);

		Thread.sleep(3000);

		click(menu1);

		Thread.sleep(2000);

		click(serialWise);

		Thread.sleep(4000);

		String serialNoVal = getText(serialTextVal2);

		Thread.sleep(2000);

		click(serialMenuClose);

		Thread.sleep(3000);

		click(btn_SerialBatchInternalDispatchOrder);

		Thread.sleep(3000);

		if (isDisplayed(SerialorBatchnoPage)) {

			writeTestResults("User should be able to click serial batch",
					" User should be able to view the Internal Order", " Serial or batch no field viewed successfully",
					"pass");
		} else {

			writeTestResults(" User should be able to click serial batch",
					" User should not able to view the Internal Order",
					" Serial or batch no field  not viewed successfully", "fail");
		}

//					 Thread.sleep(3000); 
//					
//					 click(Productlist1);
//						
//					 Thread.sleep(2000); 
//					 
//					 click(btn_alterslide);
//					 
//					 Thread.sleep(2000); 
//					 
//					 if(isDisplayed(ExistingSerial_BatchNosPage)) {
//							
//						 writeTestResults("User should be able to click alterslide" , " User should be able to view the ExistingSerial/BatchNosPage", " ExistingSerial/BatchNosPage viewed successfully", "pass");
//					}else {
//						
//						writeTestResults(" User should be able to click alterslide"  , " User should not able to view the ExistingSerial/BatchNosPage", " ExistingSerial/BatchNosPage  not viewed successfully", "fail");
//					}
//
//					 
//					 click(btn_addbatchproduct);
//					 
//					 Thread.sleep(2000); 

		Thread.sleep(3000);

		click(Productlist1);

		Thread.sleep(3000);

		click(btn_range);

		Thread.sleep(2000);

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$('#attrSlide-txtSerialCount').val(5)");

		runjQuery();

		Thread.sleep(2000);

		sendKeys(SerialProductOutboundShipment, serialNoVal);

		pressEnter(SerialProductOutboundShipment);

		Thread.sleep(2000);

		click(btn_refresh);

		click(btn_arrowtointernaldistbatchorder);

		click(btn_releaseinternaldispatchorder);

		Thread.sleep(3000);

		trackCode = getText(internaldispatchorderReleaseTC);

		if (isDisplayed(internaldispatchorderpage)) {

			writeTestResults("User should be able to Release the Internal Dispatch Order",
					" User should be able to Release the Internal Dispatch Order",
					" Inrernal Dispatch Order released successfully", "pass");
		} else {

			writeTestResults(" User should be able to Release the Internal Dispatch Order",
					" User should not able to Release the Internal Dispatch Order",
					" Inrernal Dispatch Order not released successfully", "fail");
		}

		Thread.sleep(2000);

	}

	public void ServiceHireAgreement() throws Exception {

		Thread.sleep(1000);

		click(btn_HireAgreement);

		click(btn_NewHireAgreement);

		if (isDisplayed(NewHireAgreementpage)) {

			writeTestResults(" Click on the New Hire Agreement Button",
					" User should be able to click on the New Hire Agreement Button",
					" User viewed New Hire Agreement form successfully", "pass");
		} else {

			writeTestResults("  Click on the New Hire Agreement Button",
					" User should not be able to click on the New Hire Agreement Button",
					"  User not viewed New Hire Agreement form successfully", "fail");
		}

		LocalTime myObj = LocalTime.now();

		sendKeys(AgreementNo, txt_AgreementNo + myObj);

//		String HireAgreeNo = getText(AgreementNo);
//		
//		System.out.println(HireAgreeNo);

		sendKeys(TitleAgreement, "new");

//		 click(selectagreementdate);

		selectText(Contractgroup, "Common");
		Thread.sleep(700);
		click(btn_agreementdate);

		click(btn_beforedate);

		Thread.sleep(700);

		click(btn_beforedate);

		Thread.sleep(700);

		click(selectagreementdate);

		Thread.sleep(2000);

		click(btn_EffectiveFrom);

//		click(btn_beforedate);
		
		Thread.sleep(1000);
		click("1_Link");
//		click(btn_EffectiveFromDate);
		
		String from_date = getAttribute(btn_EffectiveFrom,"value");
		click(btn_EffectiveTo);

		Thread.sleep(1000);

//		click(btn_backInternalReturnOrder);

		Thread.sleep(1000);
		click(btn_nextMonthDatePlicker);

		click(btn_EffectiveToDate);

		Thread.sleep(1000);

		sendKeys(DescriptionHireAgreement, "new test");

		Thread.sleep(1000);

		selectText(salesunitHireAgreement, "test");

		Thread.sleep(2000);

		selectText(HireBasis, "Units");

		Thread.sleep(2000);

		click(btn_accountsearchHireAgreement);

//		if(isDisplayed(AccountLookUPPage)) {
//			
//			 writeTestResults("Click account search", "AccountLookUPPage should be open", "AccountLookUPPage  page is displayed", "pass");
//		}else {
//			
//			writeTestResults("Click account search ", "AccountLookUPPage should not be open", "AccountLookUPPage is not displayed", "fail");
//		}
//		
		selectText(template, txt_Template);

		sendKeys(Accountname, "Rashi");

		pressEnter(Accountname);

		Thread.sleep(2000);

		doubleClick(btn_accountuser);

		Thread.sleep(2000);

		click(btn_SearchSceduleCodeHireAgreement);

		Thread.sleep(2000);

		sendKeys(SceduleCode, "TESTDAILY2");

//			sendKeys(SceduleCode,"newauto");

		pressEnter(SceduleCode);

		Thread.sleep(3000);

		doubleClick(btn_SceduleCode);

		Thread.sleep(2000);

		click(btn_responsibleuserHireAgreement);

		Thread.sleep(1000);

		selectText(template, txt_Template);

		sendKeys(responsibleuser, "Nihal Peiris");

		Thread.sleep(2000);

		pressEnter(responsibleuser);

		Thread.sleep(3000);

		doubleClick(btn_responsibleusername);

		Thread.sleep(2000);

		click(btn_ResourceInformation);

		if (isDisplayed(RecourceInformationPage)) {

			writeTestResults("Go to \"Resource Information\" tab",
					"User should be able to Go to \"Resource Information\" tab",
					"User able to Go to \"Resource Information\" tab successfully", "pass");

		} else {

			writeTestResults("Go to \"Resource Information\" tab ",
					"User should not be able to Go to \"Resource Information\" tab",
					"User not able to Go to \\\"Resource Information\\\" tab successfully", "fail");

		}

		click(btn_searchbtnReferncecode);

		if (isDisplayed(ReferncecodePage)) {

			writeTestResults(" Click on search icon in the \" Referance Code\" column",
					"User should be able to Click on search icon in the \" Referance Code\" column",
					"ReferncecodePage viewed successfully", "pass");
		} else {

			writeTestResults(" Click on search icon in the \" Referance Code\" column",
					"User should not be able to Click on search icon in the \" Referance Code\" column",
					"ReferncecodePage not viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Referncecode, "HireAgResource [HireAgResource]");

		Thread.sleep(2000);

		pressEnter(Referncecode);

		Thread.sleep(3000);

		doubleClick(btn_Referncecode);

		Thread.sleep(3000);

		sendKeys(openingunit, "1");

		Thread.sleep(1000);

		/* sendKeys(Minimumunit, "1"); */

		click(btn_RateRatio);

		if (isDisplayed(RateRangeSetup)) {

			writeTestResults(" Click on \"Rate Ratio\" icon",
					"User should be able to Click on search icon in the \" Referance Code\" column",
					"ReferncecodePage viewed successfully", "pass");
		} else {

			writeTestResults(" Click on \"Rate Ratio\" icon",
					"User should not be able to Click on search icon in the \" Referance Code\" column",
					"ReferncecodePage not viewed successfully", "fail");
		}

		sendKeys(Start1, "1");

		sendKeys(End1, "10");

		sendKeys(Rate1, "100");

		Thread.sleep(1000);

		click(btn_plusRR);

		Thread.sleep(1000);

		sendKeys(Start2, "11");

		sendKeys(End2, "0");

		sendKeys(Rate2, "200");

		Thread.sleep(1000);

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript(btn_applyCommon);

		Thread.sleep(1000);

		click(btn_draftHireAgreement);

		if (isDisplayed(DraftedHireAgreement)) {

			writeTestResults(" Click on \"Draft\" button",
					"User should be able to Click on \"Draft\" button and document should be draft",
					"User drafted the HireAgreement successfully", "pass");
		} else {

			writeTestResults(" Click on \"Draft\" button",
					"User should not be able to Click on \"Draft\" button and document should be draft",
					"User not drafted the HireAgreement successfully", "fail");
		}

		Thread.sleep(4000);

		click(btn_ReleaseHireAgreement);

		Thread.sleep(5000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleasedHireAgreement)) {

			writeTestResults(" Click on \"Release\" button",
					" User should be able to Click on \"Release\" button and document should be Released",
					"User released the HireAgreement successfully", "pass");
		} else {

			writeTestResults("Click on \"Release\" button",
					" User should be able to Click on \"Release\" button and document should be Released",
					"User not released the HireAgreement successfully", "fail");
		}

		Thread.sleep(5000);
		
		String HireAgreeNo = getText(HireAgreeNumber);

		System.out.println(HireAgreeNo);

		Thread.sleep(3000);

		switchWindow();

		InventoryAndWarehouseModuleRegression obj = new InventoryAndWarehouseModuleRegression();
		obj.customizeLoadingDelay(btn_action, 25);
		click(btn_action);

		click(btn_ActualUpdate);

		Thread.sleep(300);

		sendKeys(actualunit, "1");

		Thread.sleep(3000);

		click(btn_checkoutActualUpdate);

		Thread.sleep(2000);

		js.executeScript(btn_applyCommon);

		Thread.sleep(2000);

		pageRefersh();

		click(btn_entution);

		Thread.sleep(3000);

		click(btn_Rolecenter);

		Thread.sleep(3000);

		if (isDisplayed(Dashboardpage)) {

			writeTestResults("Click on \"Role Center Tile\"", "User should be able to Click on \"Role Center Tile\" ",
					"Role Center Tile viewed successfully", "pass");
		} else {

			writeTestResults(" Click on \"Role Center Tile\"", "User should be able to Click on \"Role Center Tile\" ",
					"Role Center Tile not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		click(btn_ScheduledJobs);

		Thread.sleep(2000);

		if (isDisplayed(Dashboardpage)) {

			writeTestResults("Click on \"Scheduled Jobs\"tile",
					"User should be able to Click on \"Scheduled Jobs\"tile ", "Role Center Tile viewed successfully",
					"pass");
		} else {

			writeTestResults(" Click on \"Scheduled Jobs\"tile",
					"User should be able to Click on \"Role Center Tile\" ",
					"Role Center Tile not  viewed successfully", "fail");
		}

		Thread.sleep(3000);
		switchWindow();
		click(btn_expandFirstPlusMarkHireAgreementDaily);
		Thread.sleep(2500);
		click(btn_expandSecondPlusHireAgreement.replace("date", from_date));
		
		String val = SpecificHireagreementNo.replace("agreement_no", HireAgreeNo);

		Thread.sleep(3000);

		click(val);

		Thread.sleep(2000);
		
		System.out.println();
		mouseMove(btn_runDailyMethodHireAgreement.replace("from_date", from_date));

		Thread.sleep(3000);

		click(btn_runDailyMethodHireAgreement.replace("from_date", from_date));

		Thread.sleep(4000);

		click(btn_link);

		Thread.sleep(3000);

		switchWindow();

		Thread.sleep(3000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleasedServiceInvoice)) {

			writeTestResults(" Click on \"Release\" button",
					" User should  able to run the schedule job & relese the Service Invoice",
					"User released the ServiceInvoice successfully", "pass");
		} else {

			writeTestResults("Click on \"Release\" button",
					" User should  able to run the schedule job & relese the Service Invoice",
					"User not released the ServiceInvoice successfully", "fail");
		}

	}

	public void CreateNewCase() throws Exception {

		Thread.sleep(1000);

		click(btn_case);

		if (isDisplayed(CasePage)) {

			writeTestResults(" Click on \"Case\" button", " User should be able to viewed casepage sucessfully ",
					"User viewed case page successfully", "pass");
		} else {

			writeTestResults("Click on \"Case\" button", " User should not able to viewed casepage sucessfully",
					"User  not viewed case page successfully", "fail");
		}

		click(btn_NewCase);

		if (isDisplayed(NewCasePage)) {

			writeTestResults(" Click on \"NewCase\" button", " User should be able to view new case page ",
					"User viewed  new case page successfully", "pass");
		} else {

			writeTestResults("Click on \"NewCase\" button", " User should not able to view new case page",
					"User not viewd the new casepage successfully", "fail");
		}

		sendKeys(TitleNewCase, txt_TitleNewCase);

		selectText(Orgin, "Phone");

		Thread.sleep(2000);

		click(btn_accountsearchCase);

		selectText(template, txt_Template);

		Thread.sleep(2000);

		sendKeys(Accountname, "Rashi");

		Thread.sleep(3000);

		pressEnter(Accountname);

		Thread.sleep(2000);

		doubleClick(btn_accountuser);

		Thread.sleep(3000);

		click(btn_ProductsoCase);

		Thread.sleep(5000);

//		click(btn_SerialNosCase);

		click("//tr[3]//td//input[@onclick=\"commonServiceSerial.serialService(this)\"]");

		Thread.sleep(2000);

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript(btn_ApplyCase_Smoke_Service_004);

		selectText(TypeCase, "Problem");

		Thread.sleep(2000);

		selectText(ServiceGroup, "STC-STAFF TRAINING");

		Thread.sleep(3000);

		click("//*[@id=\"divGeneral\"]/div[2]/table[1]/tbody/tr[6]/td[2]/div/span[1]/span");

		sendKeys(BillingAddress1, txt_BillingAddress1);

		Thread.sleep(1000);

		sendKeys(ShipingAddress1, txt_ShipingAddress1);

		Thread.sleep(1000);

		js.executeScript(btn_ApplyCase_Smoke_Service_004);

		Thread.sleep(2000);
		js.executeScript(btn_draftcase);

		if (isDisplayed(DraftedCasePage)) {

			writeTestResults(" Click on \"Draft\" button",
					"User should be able to Click on \"Draft\" button and document should be draft",
					"User drafted the Case successfully", "pass");
		} else {

			writeTestResults(" Click on \"Draft\" button",
					"User should not be able to Click on \"Draft\" button and document should be draft",
					"User not drafted the Case successfully", "fail");
		}

		Thread.sleep(4000);

		click(btn_releasecase);

		Thread.sleep(4000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleasedHireAgreement)) {

			writeTestResults(" Click on \"Release\" button",
					" User should be able to Click on \"Release\" button and document should be Released",
					"User released the Case successfully", "pass");
		} else {

			writeTestResults("Click on \"Release\" button",
					" User should be able to Click on \"Release\" button and document should be Released",
					"User not released the Case successfully", "fail");
		}

	}

	public void CreateNewCaseReversed() throws Exception {

		Thread.sleep(2000);

		click(btn_case);

		if (isDisplayed(CasePage)) {

			writeTestResults(" Click on \"Case\" button", " User should be able to viewed casepage sucessfully ",
					"User viewed case page successfully", "pass");
		} else {

			writeTestResults("Click on \"Case\" button", " User should not able to viewed casepage sucessfully",
					"User  not viewed case page successfully", "fail");
		}

		click(btn_NewCase);

		if (isDisplayed(NewCasePage)) {

			writeTestResults(" Click on \"NewCase\" button", " User should be able to view new case page ",
					"User viewed  new case page successfully", "pass");
		} else {

			writeTestResults("Click on \"NewCase\" button", " User should not able to view new case page",
					"User not viewd the new casepage successfully", "fail");
		}

		sendKeys(TitleNewCase, txt_TitleNewCase);

		selectText(Orgin, "Phone");

		Thread.sleep(2000);

		click(btn_accountsearchCase);

		selectText(template, txt_Template);

		Thread.sleep(2000);

		sendKeys(Accountname, "Rashi");

		Thread.sleep(3000);

		pressEnter(Accountname);

		Thread.sleep(2000);

		doubleClick(btn_accountuser);

		Thread.sleep(2000);

		click(btn_ProductsoCase);

		Thread.sleep(5000);

//		click(btn_SerialNosCase);

		click("//tr[3]//td//input[@onclick='commonServiceSerial.serialService(this)']");

		Thread.sleep(2000);

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript(btn_ApplyCase_Smoke_Service_004);

		selectText(TypeCase, "Problem");

		Thread.sleep(2000);

		selectText(ServiceGroup, "STC-STAFF TRAINING");

		Thread.sleep(3000);

		click("//*[@id='divGeneral']/div[2]/table[1]/tbody/tr[6]/td[2]/div/span[1]/span");

		sendKeys(BillingAddress1, txt_BillingAddress1);

		Thread.sleep(2000);
		JavascriptExecutor js1 = (JavascriptExecutor) driver;
		js1.executeScript("$(\"#divAddress\").nextAll().find(\".pic16-apply\").click()");
		//click("//div[@class='dialogbox-buttonarea']//a[@class='button'][contains(text(),'Apply')]");
		Thread.sleep(2000);
		
		click("//div[@class='divlookuptext']//*[@title='Edit Address']");
		Thread.sleep(2000);
		
		sendKeys(ShipingAddress1, txt_ShipingAddress1);

		Thread.sleep(1000);
		// btn apply
		js1.executeScript("$(\"#divAddress\").nextAll().find(\".pic16-apply\").click()");
		//click("//div[@class='dialogbox-buttonarea']//a[@class='button'][contains(text(),'Apply')]");

		js.executeScript(btn_draftcase);

		if (isDisplayed(DraftedCasePage)) {

			writeTestResults(" Click on \"Draft\" button",
					"User should be able to Click on \"Draft\" button and document should be draft",
					"User drafted the Case successfully", "pass");
		} else {

			writeTestResults(" Click on \"Draft\" button",
					"User should not be able to Click on \"Draft\" button and document should be draft",
					"User not drafted the Case successfully", "fail");
		}

		Thread.sleep(4000);

		click(btn_releasecase);

		Thread.sleep(4000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleasedHireAgreement)) {

			writeTestResults(" Click on \"Release\" button",
					" User should be able to Click on \"Release\" button and document should be Released",
					"User released the Case successfully", "pass");
		} else {

			writeTestResults("Click on \"Release\" button",
					" User should be able to Click on \"Release\" button and document should be Released",
					"User not released the Case successfully", "fail");
		}

		Thread.sleep(5000);

		click(btn_action);

		Thread.sleep(2000);

		click(btn_reversecase);

		Thread.sleep(2000);

		click(btn_yesReverseCase);
		
		Thread.sleep(2000);

	}

	public void ExistingServiceProductRegstraion() throws Exception {
		Thread.sleep(2000);
		click(btn_InventoryandWareHousing);
		Thread.sleep(2000);
		click(btn_ServiceProductRegistration);
		Thread.sleep(2000);

		click(btn_NewServiceProductRegistration);
		Thread.sleep(2000);
		selectText(RegistrationType, "Existing");

		Thread.sleep(2000);

		click(btn_accountsearchServiceProductRegis);
		Thread.sleep(2000);
//			if(isDisplayed(AccountLookUPPage)) {
//				
//				 writeTestResults("Click account search", "AccountLookUPPage should be open", "AccountLookUPPage  page is displayed", "pass");
//			}else {
//				
//				writeTestResults("Click account search ", "AccountLookUPPage should not be open", "AccountLookUPPage is not displayed", "fail");
//			}
//		Thread.sleep(2000);
		selectText(template, txt_Template);
		Thread.sleep(2000);

		sendKeys(Accountname, "Rashi");

		pressEnter(Accountname);

		Thread.sleep(2000);

		doubleClick(btn_accountuser);

		Thread.sleep(2000);


		click(btn_Tagno);

		Thread.sleep(7000);

		click("//a[@id='A1'][text()='Search']");
		Thread.sleep(7000);
//		InventoryAndWarehouseModuleRegression obj = new InventoryAndWarehouseModuleRegression();
//		obj.customizeLoadingDelay(btn_Tagnos, 30);
		//click(btn_Tagnos);
		click("//div[@class='dialogbox color-selectedborder ui-draggable first']//tr[1]//td[2]//span[1]");

		Thread.sleep(7000);

//		obj.customizeLoadingDelay(btn_SearchWarrantyProfile, 30);
		click(btn_SearchWarrantyProfile);
		Thread.sleep(2000);

		selectText(template, txt_Template);

		Thread.sleep(2000);

		sendKeys(Warranty, "176_Warranty_profile");

		Thread.sleep(1000);

		pressEnter(Warranty);

		Thread.sleep(2000);

		doubleClick(btn_Warranty);

		Thread.sleep(2000);

		click(btn_WarrantyDate);
		Thread.sleep(2000);
		click(btn_selectWarrantyDate);

		Thread.sleep(1000);

		click(btn_warrantyProfileSales);

		selectText(template, txt_Template);

		Thread.sleep(3000);

		sendKeys("//*[@id=\"txtg1168\"]", "176_Warranty_profile");

		Thread.sleep(1000);

		pressEnter(Warranty);

		Thread.sleep(2000);

		doubleClick(btn_Warranty);

		Thread.sleep(2000);

		click(btn_WarrantyDateSales);

		Thread.sleep(2000);

		click(btn_selectWarrantyDate);

		Thread.sleep(4000);

		click(btn_DraftServiceProductReg);

		Thread.sleep(2000);

		if (isDisplayed(DraftedHireAgreement)) {

			writeTestResults(" Click on \"Draft\" button",
					"User should be able to Click on \"Draft\" button and document should be draft",
					"User drafted the Service ProductRegistration form successfully", "pass");
		} else {

			writeTestResults(" Click on \"Draft\" button",
					"User should not be able to Click on \"Draft\" button and document should be draft",
					"User not drafted the Service ProductRegistration form  successfully", "fail");
		}

		Thread.sleep(4000);

		click(btn_ReleaseServiceProductReg);

		Thread.sleep(5000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleasedHireAgreement)) {

			writeTestResults(" Click on \"Release\" button",
					" User should be able to Click on \"Release\" button and document should be Released",
					"User released the Service ProductRegistration form  successfully", "pass");
		} else {

			writeTestResults("Click on \"Release\" button",
					" User should be able to Click on \"Release\" button and document should be Released",
					"User not released the  Service ProductRegistration form  successfully", "fail");
		}

	}

	public void DirectServiceProductRegstraion() throws Exception {

		click(btn_InventoryandWareHousing);

		click(btn_ServiceProductRegistration);

		click(btn_NewServiceProductRegistration);

		selectText(RegistrationType, "Direct");

		click(btn_accountsearchServiceProductRegis);

//			if(isDisplayed(AccountLookUPPage)) {
//				
//				 writeTestResults("Click account search", "AccountLookUPPage should be open", "AccountLookUPPage  page is displayed", "pass");
//			}else {
//				
//				writeTestResults("Click account search ", "AccountLookUPPage should not be open", "AccountLookUPPage is not displayed", "fail");
//			}
//			
		selectText(template, txt_Template);

		sendKeys(Accountname, "Rashi");

		pressEnter(Accountname);

		Thread.sleep(2000);

		doubleClick(btn_accountuser);

		Thread.sleep(2000);

		LocalTime myObj = LocalTime.now();

		sendKeys(tagnoDirectSR, txt_tagnoDirectSR + myObj);

		Thread.sleep(1000);

		click(btn_searchReferenceCode);

		Thread.sleep(2000);

		if (isDisplayed(ProductLookUpPage)) {

			writeTestResults(" Click search to product", "Should be able to view product look up page",
					"product look up page viewed successfully", "pass");

		} else {

			writeTestResults(" Click search to product", "Should not able to view product look up page",
					"product look up page not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, txt_SerialSpecificProduct);

		Thread.sleep(2000);
		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		click(btn_SearchWarrantyProfile);

		selectText(template, txt_Template);

		Thread.sleep(2000);

		sendKeys(Warranty, "Task81448-1");

		Thread.sleep(1000);

		pressEnter(Warranty);

		Thread.sleep(2000);

		doubleClick(btn_Warranty);

		Thread.sleep(2000);

		click(btn_WarrantyDate);

		click(btn_selectWarrantyDate);

		Thread.sleep(1000);

		click(btn_warrantyProfileSales);

		selectText(template, txt_Template);

		Thread.sleep(2000);

		sendKeys(Warranty, "176_Warranty_profile");

		Thread.sleep(1000);

		pressEnter(Warranty);

		Thread.sleep(2000);

		doubleClick(btn_Warranty);

		Thread.sleep(2000);

		click(btn_WarrantyDateSales);

		Thread.sleep(1000);

		click(btn_selectWarrantyDate);

		Thread.sleep(2000);

		click(btn_DraftServiceProductReg);

		if (isDisplayed(DraftedHireAgreement)) {

			writeTestResults(" Click on \"Draft\" button",
					"User should be able to Click on \"Draft\" button and document should be draft",
					"User drafted the Service Product Registration successfully", "pass");
		} else {

			writeTestResults(" Click on \"Draft\" button",
					"User should not be able to Click on \"Draft\" button and document should be draft",
					"User not drafted the Service Product Registration successfully", "fail");
		}

		Thread.sleep(1000);

		click(btn_ReleaseServiceProductReg);

		Thread.sleep(4000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleasedHireAgreement)) {

			writeTestResults(" Click on \"Release\" button",
					" User should be able to Click on \"Release\" button and document should be Released",
					"User released the Service Product Registration successfully", "pass");
		} else {

			writeTestResults("Click on \"Release\" button",
					" User should be able to Click on \"Release\" button and document should be Released",
					"User not released the Service Product Registration successfully", "fail");
		}

	}

	public void ServiceCostEstimation() throws Exception {

		click(btn_ServiceCostEstimation);

		Thread.sleep(1000);

		click(btn_NewServiceCostEstimation);

		Thread.sleep(1000);

		sendKeys(DescriptionService, "new");

		Thread.sleep(1000);

		click(btn_SearchCustomerServiceCE);

		Thread.sleep(1000);

		if (isDisplayed(AccountLookUPPageSE)) {

			writeTestResults("Click account search", "AccountLookUPPage should be open",
					"AccountLookUPPage  page is displayed", "pass");
		} else {

			writeTestResults("Click account search ", "AccountLookUPPage should not be open",
					"AccountLookUPPage is not displayed", "fail");
		}

		selectText(template, txt_Template);

		sendKeys(Accountname, txt_Accountname);

		pressEnter(Accountname);

		Thread.sleep(2000);

		doubleClick(btn_accountuser);

		Thread.sleep(3000);

		click(btn_searchtagno);

		Thread.sleep(3000);

		click(btn_serialno);

		click(btn_PricingProfileSearchSE);

//		if(isDisplayed(PricingProfileLookUpPage)) {
//				
//				 writeTestResults(" Select a \"Pricing Profile\" " , " Able to select a pricing profile from the look up", "Able to select pricing profile successfully", "pass");
//			}else {
//				
//				writeTestResults(" Select a \"Pricing Profile\" " , " Not Able to select a pricing profile from the look up", "Not able to select pricing profile successfully", "fail");
//			}
		Thread.sleep(1000);

		sendKeys(pricingprofileSE, "Service");

		pressEnter(pricingprofileSE);

		Thread.sleep(1000);

		doubleClick("//*[@id=\"g1198-t\"]/table/tbody/tr[2]/td[3]");

		Thread.sleep(2000);

		click(btn_EstimationDetails);

		Thread.sleep(2000);

		click(btn_searchserviceproductfirst);

		if (isDisplayed(ProductLookUpPage)) {

			writeTestResults(" Click search to product", "Should be able to view product look up page",
					"product look up page viewed successfully", "pass");
		} else {

			writeTestResults(" Click search to product", "Should not able to view product look up page",
					"product look up page not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, "SP-test-001");

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(2000);

//				selectText(warehouse1, "WH-test-001-Allo[Rashi-test-auto]");
		selectText("/tr[@class='set-selected-row']//td[11]", txt_ToWareHouse);
//				
		Thread.sleep(3000);

		sendKeys(EstimatedQtyservice, "5");

		click(btn_billable1);

		Thread.sleep(1000);

		click(btn_expand2);

		Thread.sleep(1000);

		click(btn_searchbatchproduct2);

		if (isDisplayed(ProductLookUpPage)) {

			writeTestResults(" Click search to product", "Should be able to view product look up page",
					"product look up page viewed successfully", "pass");
		} else {

			writeTestResults(" Click search to product", "Should not able to view product look up page",
					"product look up page not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, txt_BatchSpecificProduct);

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(2000);

		click(btn_expand2);

		selectText("//*[@id=\"commonTbl\"]/tbody/tr[3]/td[6]/select", "Resource");

		Thread.sleep(2000);

		click(btn_searchResourcesReferenceCode3);

		if (isDisplayed(ResourceLookUpPage)) {

			writeTestResults(" Click search to resource", "Should be able to view resource look up page",
					"resource look up page viewed successfully", "pass");
		} else {

			writeTestResults(" Click search to resourse", "Should not able to view resource look up page",
					"resource look up page not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

//						 sendKeys(Resource,txt_Resource);

		sendKeys(Resource, "REC-test-001");

		Thread.sleep(2000);

		pressEnter(Resource);

		Thread.sleep(3000);

		doubleClick(btn_Resource);

		Thread.sleep(2000);

		click(btn_searchResources3);

		if (isDisplayed(ProductLookUpPage)) {

			writeTestResults(" Click search to product", "Should be able to view product look up page",
					"product look up page viewed successfully", "pass");
		} else {

			writeTestResults(" Click search to product", "Should not able to view product look up page",
					"product look up page not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, txt_serviceProduct);

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(2000);

		sendKeys("//*[@id=\"commonTbl\"]/tbody/tr[3]/td[14]/input", "5");

		Thread.sleep(3000);

		click(btn_expand3);

		selectText("//*[@id=\"commonTbl\"]/tbody/tr[4]/td[6]/select", "Labour");

		click("//*[@id=\"commonTbl\"]/tbody/tr[4]/td[8]/span");

//							if(isDisplayed(LabourOwnerLookUpPage)) {
//								
//								 writeTestResults(" Click search to Employee" , "Should be able to view owner look up page", "Owner look up page viewed successfully", "pass");
//							}else {
//								
//								writeTestResults(" Click search to Employee"  , "Should not able to view owner look up page", "Owner look up page not  viewed successfully", "fail");
//							}

		Thread.sleep(3000);

//							 selectText(template,txt_Template);

		sendKeys(LabourEmployee, "rashi");

		Thread.sleep(2000);

		pressEnter(LabourEmployee);

		Thread.sleep(3000);

		doubleClick(btn_LabourEmployee);

		Thread.sleep(2000);

		click("//*[@id=\"commonTbl\"]/tbody/tr[4]/td[9]/span");

//							if(isDisplayed(ProductLookUpPage)) {
//								
//								 writeTestResults(" Click search to product" , "Should be able to view product look up page", "product look up page viewed successfully", "pass");
//							}else {
//								
//								writeTestResults(" Click search to product"  , "Should not able to view product look up page", "product look up page not  viewed successfully", "fail");
//							}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, txt_serviceProduct);

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(2000);

		// sendKeys(EstimatedQtyLabour, txt_EstimatedQtyLabour);
		sendKeys("//*[@id=\"commonTbl\"]/tbody/tr[4]/td[14]/input", "5");

		Thread.sleep(2000);

		click(btn_expand4);

		selectText("//*[@id=\"commonTbl\"]/tbody/tr[5]/td[6]/select", "Services");

		click("//*[@id=\"commonTbl\"]/tbody/tr[5]/td[9]/span");

		if (isDisplayed(ProductLookUpPage)) {

			writeTestResults(" Click search to product", "Should be able to view product look up page",
					"product look up page viewed successfully", "pass");
		} else {

			writeTestResults(" Click search to product", "Should not able to view product look up page",
					"product look up page not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, txt_serviceProduct);

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(2000);

//								sendKeys(EstimatedQtyservice, txt_EstimatedQtyservice);
		sendKeys("//*[@id=\"commonTbl\"]/tbody/tr[5]/td[14]/input", "5");

		Thread.sleep(1000);

		sendKeys("//*[@id=\"commonTbl\"]/tbody/tr[5]/td[16]/input", "100");

		Thread.sleep(2000);

		click(btn_expand5);

		Thread.sleep(1000);

		selectText("//*[@id=\"commonTbl\"]/tbody/tr[6]/td[6]/select", "Expense");

		Thread.sleep(1000);

		selectText("//*[@id=\"commonTbl\"]/tbody/tr[6]/td[8]/select", txt_Referencecode);

		click("//*[@id=\"commonTbl\"]/tbody/tr[6]/td[9]/span");

		Thread.sleep(1000);

		if (isDisplayed(ProductLookUpPage)) {

			writeTestResults(" Click search to product", "Should be able to view product look up page",
					"product look up page viewed successfully", "pass");
		} else {

			writeTestResults(" Click search to product", "Should not able to view product look up page",
					"product look up page not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, txt_serviceProduct);

		Thread.sleep(2000);

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(2000);

		sendKeys("//*[@id=\"commonTbl\"]/tbody/tr[6]/td[16]/input", "200");

		Thread.sleep(1000);

//			 click(btn_Summary);

		click("//*[@id=\"#divTbSummary\"]");

		Thread.sleep(3000);

		switchWindow();

//			click(btn_checkoutServiceCostEstimation);
		click("//*[@id=\"divTbSummary\"]/div[2]/span");

		Thread.sleep(2000);

		click(btn_DraftEmployeeAdvanceRequest);

		Thread.sleep(2000);

		if (isDisplayed(DraftedHireAgreement)) {

			writeTestResults(" Click on \"Draft\" button",
					"User should be able to Click on \"Draft\" button and document should be draft",
					"User drafted the ServiceCostEstimation  successfully", "pass");

		} else {

			writeTestResults(" Click on \"Draft\" button",
					"User should not be able to Click on \"Draft\" button and document should be draft",
					"User not drafted the ServiceCostEstimation successfully", "fail");
		}

		Thread.sleep(5000);

		click(btn_ReleaseServiceProductReg);

		Thread.sleep(4000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleasedHireAgreement)) {

			writeTestResults(" Click on \"Release\" button",
					" User should be able to Click on \"Release\" button and document should be Released",
					"User released the ServiceCostEstimation successfully", "pass");
		} else {

			writeTestResults("Click on \"Release\" button",
					" User should be able to Click on \"Release\" button and document should be Released",
					"User not released the ServiceCostEstimation successfully", "fail");
		}

	}

	public void EmployeeAdvanceRequest() throws Exception {

		Thread.sleep(2000);

		click(btn_EmployeeAdvanceRequest);

		Thread.sleep(2000);

		click(btn_NewEmployeeAdvanceRequest);

		Thread.sleep(2000);

		click(btn_searchemployeeEmployeeAdvanceRequest);

		Thread.sleep(2000);

		selectText(template, txt_Template);

		sendKeys(Employee, txt_Employee);

		Thread.sleep(2000);

		pressEnter(Employee);

		Thread.sleep(3000);

		doubleClick(btn_LabourEmployee);

		Thread.sleep(2000);

		selectText(ExpenseType, "Business Trip");

		Thread.sleep(2000);

		sendKeys(Purpose, "new test ");

		Thread.sleep(3000);

		sendKeys(AdvanceAmount, "200");

		Thread.sleep(3000);

		click(btn_DraftEmployeeAdvanceRequest);

		Thread.sleep(3000);

		if (isDisplayed(DraftedHireAgreement)) {

			writeTestResults(" Click on \"Draft\" button",
					"User should be able to Click on \"Draft\" button and document should be draft",
					"User drafted the Employee Advance Request successfully", "pass");
		} else {

			writeTestResults(" Click on \"Draft\" button",
					"User should not be able to Click on \"Draft\" button and document should be draft",
					"User not drafted the  Employee Advance Request successfully", "fail");
		}

		Thread.sleep(3000);

		click(btn_ReleaseServiceProductReg);

		Thread.sleep(4000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleasedHireAgreement)) {

			writeTestResults(" Click on \"Release\" button",
					" User should be able to Click on \"Release\" button and document should be Released",
					"User released the  Employee Advance Request successfully", "pass");
		} else {

			writeTestResults("Click on \"Release\" button",
					" User should be able to Click on \"Release\" button and document should be Released",
					"User not released the Employee Advance Request successfully", "fail");
		}
		Thread.sleep(4000);

		click(btn_action);

		Thread.sleep(2000);

		click(btn_PayEmployeeAdvance);

		Thread.sleep(2000);

		if (isDisplayed(OutboundPaymentAdvancePage)) {

			writeTestResults(" Click on \"btn_PayEmployeeAdvance\" button",
					" User should be able to view OutboundPaymentAdvice Page",
					"User viewed OutboundPaymentAdvice successfully", "pass");
		} else {

			writeTestResults("Click on \"btn_PayEmployeeAdvance\" button",
					" User should not  be able to view OutboundPaymentAdvice Page",
					"User  not viewed OutboundPaymentAdvice successfully", "fail");
		}

		Thread.sleep(3000);

		click(btn_NewcheckoutOutboundPaymentAdvice);

		Thread.sleep(3000);

		click(btn_DraftOutboundPaymentAdvice);

		Thread.sleep(3000);

		if (isDisplayed(DraftOutboundPaymentAdvicePage)) {

			writeTestResults(" Click on \"Draft\" button",
					"User should be able to Click on \"Draft\" button and document should be draft",
					"User drafted the OutPaymentAdvice Page successfully", "pass");
		} else {

			writeTestResults(" Click on \"Draft\" button",
					"User should not be able to Click on \"Draft\" button and document should be draft",
					"User not drafted the OutPaymentAdvice page successfully", "fail");
		}

		Thread.sleep(4000);

		click(btn_ReleaseServiceProductReg);

		Thread.sleep(4000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleasedHireAgreement)) {

			writeTestResults(" Click on \"Release\" button",
					" User should be able to Click on \"Release\" button and document should be Released",
					"User released the OutPaymentAdvice successfully", "pass");
		} else {

			writeTestResults("Click on \"Release\" button",
					" User should be able to Click on \"Release\" button and document should be Released",
					"User not released the OutPaymentAdvice successfully", "fail");
		}

		Thread.sleep(3000);

		switchWindow();

		Thread.sleep(3000);

		click(btn_action);

		Thread.sleep(4000);

		click(btn_NewConvertToOutboundPayment);

		if (isDisplayed(OutboundPaymentPage)) {

			writeTestResults(" Click on \"btn_ConvertToOutboundPayment\" button",
					" User should be able to view OutboundPayment Page", "User viewed OutboundPayment successfully",
					"pass");
		} else {

			writeTestResults("Click on \"btn_ConvertToOutboundPayment\" button",
					" User should not  be able to view OutboundPayment Page",
					"User  not viewed OutboundPaymentsuccessfully", "fail");
		}

		Thread.sleep(2000);

		click(btn_details);

		Thread.sleep(2000);

		click(btn_putTick);

		click("//*[@id=\"btnCalc2\"]");

		Thread.sleep(2000);

		click(btn_DraftEmployeeAdvanceRequest);

		Thread.sleep(3000);

		if (isDisplayed(DraftedHireAgreement)) {

			writeTestResults(" Click on \"Draft\" button",
					"User should be able to Click on \"Draft\" button and document should be draft",
					"User drafted the Outboundpayment successfully", "pass");
		} else {

			writeTestResults(" Click on \"Draft\" button",
					"User should not be able to Click on \"Draft\" button and document should be draft",
					"User not drafted the  Outboundpayment successfully", "fail");
		}

		Thread.sleep(4000);

		click(btn_ReleaseServiceProductReg);

		Thread.sleep(4000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleasedHireAgreement)) {

			writeTestResults(" Click on \"Release\" button",
					" User should be able to Click on \"Release\" button and document should be Released",
					"User released the Outboundpayment successfully", "pass");
		} else {

			writeTestResults("Click on \"Release\" button",
					" User should be able to Click on \"Release\" button and document should be Released",
					"User not released the Outboundpayment successfully", "fail");
		}

	}

	public void ServiceContractnew() throws Exception {
		click(btn_ServiceContract);

		Thread.sleep(1000);

		click(btn_NewServiceContractForm);

		Thread.sleep(1000);

		if (isDisplayed(NewServiceContractFormPage)) {

			writeTestResults(" Clcik on \"New Service Contract \" Form",
					"User should be able to click on the New Service Contractbutton",
					"User  able to click on the New Service Contractbutton sucessfully", "pass");
		} else {

			writeTestResults(" Clcik on \"New Service Contract \" Form",
					"User should be able to click on the New Service Contractbutton",
					"User not able to click on the New Service Contractbutton sucessfully", "fail");
		}

		sendKeys(ServiceContractTitle, txt_ServiceContractTitle);

		click(btn_SearchCustomerServiceContract);

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(customer, txt_customer);

		Thread.sleep(2000);

		pressEnter(customer);

		Thread.sleep(3000);

		doubleClick(btn_customer);

		Thread.sleep(2000);

		click(btn_searchowner);

		selectText(template, txt_Template);

		sendKeys(searchowner, "Nihal Peiris");

		Thread.sleep(2000);

		pressEnter(searchowner);

		Thread.sleep(3000);

		doubleClick(btn_owner);

		Thread.sleep(2000);

		click(btn_contractdate);

		click(btn_beforedate);

		Thread.sleep(3000);

		click(btn_beforedate);

		Thread.sleep(3000);

		click(btn_selectcontractdate);

		Thread.sleep(2000);

		click(btn_EffectiveFrom);

		click(btn_beforedate);

		Thread.sleep(1000);

		click(btn_beforedate);

		Thread.sleep(1000);

		click("//*[@id=\"ui-datepicker-div\"]/table/tbody/tr[2]/td[4]");

		click(btn_EffectiveTo);

		click(btn_beforedate);

		Thread.sleep(1000);

		click("//*[@id=\"ui-datepicker-div\"]/table/tbody/tr[2]/td[6]/a");

//			click(btn_EffectiveToDate);

		selectText(ContractGroup, "Schindler Installation");

//	        Thread.sleep(2000);  
//			
//			click(btn_contractdate);
//			
//			 Thread.sleep(1000);
//			 
//			click(btn_beforedate);
//			
//			 Thread.sleep(3000);
//			
//			click(btn_selectcontractdate);
//			
//			Thread.sleep(2000);  

		click(btn_ServiceLevelAgreement);

		Thread.sleep(2000);

//			 click(btn_ServiceLevelAgreement1);

		sendKeys(btn_ServiceLevelAgreement1, "SLA001");

		pressEnter(btn_ServiceLevelAgreement1);

		Thread.sleep(2000);

		doubleClick(btn_agreementname);

		Thread.sleep(1000);

		sendKeys(Description, txt_Description);

		click(btn_searchserviceproduct1);

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, txt_serviceProduct);

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(2000);

		sendKeys(EstimatedCost, txt_EstimatedCost);

		Thread.sleep(2000);

		click(btn_SearchSceduleCode);

		Thread.sleep(2000);

		sendKeys(SceduleCode, txt_SceduleCode);

		pressEnter(SceduleCode);

		Thread.sleep(2000);

		doubleClick(btn_SceduleCode);

		Thread.sleep(2000);

		click(btn_responsibleuser);

		Thread.sleep(1000);

		selectText(template, txt_Template);

		sendKeys(responsibleuser, "Nihal Peiris");

		Thread.sleep(2000);

		pressEnter(responsibleuser);

		Thread.sleep(3000);

		doubleClick(btn_responsibleusername);

		Thread.sleep(2000);

		click(btn_ContractLine);

		selectText(tagno, txt_tagno);

		Thread.sleep(2000);

		click(btn_Summary);

		Thread.sleep(2000);

		click(btn_checkoutServiceContract);

		click(btn_draftServiceContract);

		Thread.sleep(3000);

		click(btn_editServiceContract);

		Thread.sleep(3000);

		click(btn_PreventiveMaintenance);

		Thread.sleep(1000);

		click(btn_plus);

		sendKeys(Title, txt_title);

		Thread.sleep(1000);

		selectText(PMCategory, "Service");

		Thread.sleep(1000);

		selectText(PMinterval, txt_PMinterval);

//			click(btn_down);

		Thread.sleep(1000);

//			mouseMove(PMcount);

		sendKeys(PMcount, "2");

		Thread.sleep(1000);

		click(btn_apply);

		Thread.sleep(1000);

		click(btn_Summary);

		Thread.sleep(2000);

		click(btn_checkoutServiceContract);

		Thread.sleep(1000);

		click(btn_updateServiceContract);

		Thread.sleep(1000);

		click(btn_PreventiveMaintenance);

		Thread.sleep(1000);

		click(btn_ChargeableProd);

		Thread.sleep(1000);

		click(btn_searchproducts);

		if (isDisplayed(ProductLookUpPage)) {

			writeTestResults(" Click search to product", "Should be able to view product look up page",
					"product look up page viewed successfully", "pass");
		} else {

			writeTestResults(" Click search to product", "Should not able to view product look up page",
					"product look up page not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, txt_SerialSpecificProduct);

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(1000);

		sendKeys(qty, "2");

		Thread.sleep(1000);

		click(btn_apply);

		Thread.sleep(2000);

		click(btn_ChargeableProd1);

		Thread.sleep(1000);

		click(btn_searchproducts);

		if (isDisplayed(ProductLookUpPage)) {

			writeTestResults(" Click search to product", "Should be able to view product look up page",
					"product look up page viewed successfully", "pass");
		} else {

			writeTestResults(" Click search to product", "Should not able to view product look up page",
					"product look up page not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, txt_SerialSpecificProduct);

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(3000);

		click(btn_apply);

		click(btn_UpdatePreventiveMaintenance);

		Thread.sleep(1000);

		click(btn_ReleaseServiceQuatation);

		Thread.sleep(1000);

		trackCode = getText(ServiceJobOrderReleaseNoTC);

		if (isDisplayed(ReleaseServiceContractPage)) {

			writeTestResults("Click on \"Release\" Button", "User should be able to Release the ServiceContract",
					"ServiceContract Released successfully", "pass");
		} else {

			writeTestResults(" Click on \"Release\" Button", "User should not able toRelease the ServiceContract",
					"ServiceContract not  Released successfully", "fail");
		}

		Thread.sleep(3000);

		pageRefersh();

		click(btn_action);

		click(btn_ConverttoServiceInvoice);

		Thread.sleep(3000);

		click(btn_ServiceDetails);

		Thread.sleep(1000);

		click(btn_checkoutServiceInvoice1);

		click(btn_draftServiceInvoice1);

		Thread.sleep(3000);

		click(btn_releaseServiceInvoice1);

		Thread.sleep(2000);

		trackCode = getText(ServiceJobOrderReleaseNoTC);

		if (isDisplayed(ReleasedServiceInvoice1page)) {

			writeTestResults("Click on \"Release\" Button", "User should be able to Release the ServiceInvoice",
					"ServiceInvoice Released successfully", "pass");
		} else {

			writeTestResults(" Click on \"Release\" Button", "User should not able toRelease the ServiceInvoice",
					"ServiceInvoice not  Released successfully", "fail");
		}

		Thread.sleep(2000);

		click(btn_entution);

		Thread.sleep(1000);

		click(btn_Rolecenter);

		Thread.sleep(2000);

		if (isDisplayed(Dashboardpage)) {

			writeTestResults("Click on \"Role Center Tile\"", "User should be able to Click on \"Role Center Tile\" ",
					"Role Center Tile viewed successfully", "pass");
		} else {

			writeTestResults(" Click on \"Role Center Tile\"", "User should be able to Click on \"Role Center Tile\" ",
					"Role Center Tile not  viewed successfully", "fail");
		}

		Thread.sleep(1000);

		click(btn_ScheduledJobs);

		Thread.sleep(2000);

		if (isDisplayed(Dashboardpage)) {

			writeTestResults("Click on \"Scheduled Jobs\"tile",
					"User should be able to Click on \"Scheduled Jobs\"tile ", "Role Center Tile viewed successfully",
					"pass");
		} else {

			writeTestResults(" Click on \"Scheduled Jobs\"tile",
					"User should be able to Click on \"Role Center Tile\" ",
					"Role Center Tile not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		switchWindow();

//					 click(btn_LastdayofMonthPlus);
		click("//*[text()='lastday of month : newauto']");

		Thread.sleep(2000);

		click(btn_ServiceContractLastdayofMonth);

		Thread.sleep(2000);

		click(btn_selectallServiceContractLastdayofMonth);

		Thread.sleep(4000);

		click(btn_runServiceContractLastdayofMonth);

	}

	public void LastdayServiceHireAgreement() throws Exception {

		Thread.sleep(1000);

		click(btn_HireAgreement);

		click(btn_NewHireAgreement);

		if (isDisplayed(NewHireAgreementpage)) {

			writeTestResults(" Click on the New Hire Agreement Button",
					" User should be able to click on the New Hire Agreement Button",
					" User viewed New Hire Agreement form successfully", "pass");
		} else {

			writeTestResults("  Click on the New Hire Agreement Button",
					" User should not be able to click on the New Hire Agreement Button",
					"  User not viewed New Hire Agreement form successfully", "fail");
		}

		LocalTime myObj = LocalTime.now();

		sendKeys(AgreementNo, txt_AgreementNo + myObj);

		sendKeys(TitleAgreement, "new");

//			 click(selectagreementdate);

		selectText(Contractgroup, "Common");

		click(btn_agreementdate);

		click(btn_beforedate);

		Thread.sleep(3000);
		
		click(btn_beforedate);

		Thread.sleep(3000);

		click(btn_beforedate);

		Thread.sleep(3000);
		
		click(selectagreementdate);

		Thread.sleep(2000);

		click(btn_EffectiveFrom);

		 click(btn_beforedate); 

		Thread.sleep(1000);
		
		click(btn_EffectiveFromDate);

		click(btn_EffectiveTo);

		Thread.sleep(1000);

//		click(btn_backInternalReturnOrder);
		
		Thread.sleep(1000);
		
		click(btn_EffectiveToDate);

		Thread.sleep(1000);

		sendKeys(DescriptionHireAgreement, "new test");

		Thread.sleep(1000);

		selectText(salesunitHireAgreement, "test");

		Thread.sleep(2000);

		selectText(HireBasis, "Units");

		Thread.sleep(2000);

		click(btn_accountsearchHireAgreement);

//			if(isDisplayed(AccountLookUPPage)) {
//				
//				 writeTestResults("Click account search", "AccountLookUPPage should be open", "AccountLookUPPage  page is displayed", "pass");
//			}else {
//				
//				writeTestResults("Click account search ", "AccountLookUPPage should not be open", "AccountLookUPPage is not displayed", "fail");
//			}
//			
		selectText(template, txt_Template);

		sendKeys(Accountname, "Rashi");

		pressEnter(Accountname);

		Thread.sleep(2000);

		doubleClick(btn_accountuser);

		Thread.sleep(2000);

		selectText(BillingBasis, "Monthly");

		click(btn_SearchSceduleCodeHireAgreement);

		Thread.sleep(2000);

//				sendKeys(SceduleCode,"TESTDAILY2");

		sendKeys(SceduleCode, "newauto");

		pressEnter(SceduleCode);

		Thread.sleep(2000);

		doubleClick(btn_SceduleCode);

		Thread.sleep(2000);

		click(btn_responsibleuserHireAgreement);

		Thread.sleep(1000);

		selectText(template, txt_Template);

		sendKeys(responsibleuser, "Nihal Peiris");

		Thread.sleep(2000);

		pressEnter(responsibleuser);

		Thread.sleep(3000);

		doubleClick(btn_responsibleusername);

		Thread.sleep(2000);

		click(btn_ResourceInformation);

		if (isDisplayed(RecourceInformationPage)) {

			writeTestResults("Go to \"Resource Information\" tab",
					"User should be able to Go to \"Resource Information\" tab",
					"User able to Go to \"Resource Information\" tab successfully", "pass");

		} else {

			writeTestResults("Go to \"Resource Information\" tab ",
					"User should not be able to Go to \"Resource Information\" tab",
					"User not able to Go to \\\"Resource Information\\\" tab successfully", "fail");

		}

		click(btn_searchbtnReferncecode);

		if (isDisplayed(ReferncecodePage)) {

			writeTestResults(" Click on search icon in the \" Referance Code\" column",
					"User should be able to Click on search icon in the \" Referance Code\" column",
					"ReferncecodePage viewed successfully", "pass");
		} else {

			writeTestResults(" Click on search icon in the \" Referance Code\" column",
					"User should not be able to Click on search icon in the \" Referance Code\" column",
					"ReferncecodePage not viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Referncecode, "HireAgResource [HireAgResource]");

		Thread.sleep(2000);

		pressEnter(Referncecode);

		Thread.sleep(3000);

		doubleClick(btn_Referncecode);

		Thread.sleep(3000);

		sendKeys(openingunit, "1");

		Thread.sleep(1000);

		sendKeys(Minimumunit, "1");

		click(btn_RateRatio);

		if (isDisplayed(RateRangeSetup)) {

			writeTestResults(" Click on \"Rate Ratio\" icon",
					"User should be able to Click on search icon in the \" Referance Code\" column",
					"ReferncecodePage viewed successfully", "pass");
		} else {

			writeTestResults(" Click on \"Rate Ratio\" icon",
					"User should not be able to Click on search icon in the \" Referance Code\" column",
					"ReferncecodePage not viewed successfully", "fail");
		}

		sendKeys(Start1, "1");

		sendKeys(End1, "10");

		sendKeys(Rate1, "100");

		Thread.sleep(1000);

		click(btn_plusRR);

		Thread.sleep(1000);

		sendKeys(Start2, "11");

		sendKeys(End2, "0");

		sendKeys(Rate2, "200");

		Thread.sleep(1000);

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript(btn_applyCommon);

		Thread.sleep(1000);

		click(btn_draftHireAgreement);

		if (isDisplayed(DraftedHireAgreement)) {

			writeTestResults(" Click on \"Draft\" button",
					"User should be able to Click on \"Draft\" button and document should be draft",
					"User drafted the HireAgreement successfully", "pass");
		} else {

			writeTestResults(" Click on \"Draft\" button",
					"User should not be able to Click on \"Draft\" button and document should be draft",
					"User not drafted the HireAgreement successfully", "fail");
		}

		Thread.sleep(4000);

		switchWindow();

		click(btn_ReleaseHireAgreement);

		Thread.sleep(4000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleasedHireAgreement)) {

			writeTestResults(" Click on \"Release\" button",
					" User should be able to Click on \"Release\" button and document should be Released",
					"User released the HireAgreement successfully", "pass");
		} else {

			writeTestResults("Click on \"Release\" button",
					" User should be able to Click on \"Release\" button and document should be Released",
					"User not released the HireAgreement successfully", "fail");
		}

		String HireAgreeNo = getText(HireAgreeNumber);

		System.out.println(HireAgreeNo);

		pageRefersh();

		Thread.sleep(4000);

		click(btn_action);

		Thread.sleep(2000);

		click(btn_ActualUpdate);

		Thread.sleep(3000);

		sendKeys(actualunit, "1");

		Thread.sleep(3000);

		click(btn_checkoutActualUpdate);

		Thread.sleep(2000);

		click(btn_applyActualUpdate);

		Thread.sleep(2000);

		pageRefersh();

		click(btn_entution);

		Thread.sleep(1000);

		click(btn_Rolecenter);

		Thread.sleep(2000);

		if (isDisplayed(Dashboardpage)) {

			writeTestResults("Click on \"Role Center Tile\"", "User should be able to Click on \"Role Center Tile\" ",
					"Role Center Tile viewed successfully", "pass");
		} else {

			writeTestResults(" Click on \"Role Center Tile\"", "User should be able to Click on \"Role Center Tile\" ",
					"Role Center Tile not  viewed successfully", "fail");
		}

		Thread.sleep(1000);

		click(btn_ScheduledJobs);

		Thread.sleep(2000);

		if (isDisplayed(Dashboardpage)) {

			writeTestResults("Click on \"Scheduled Jobs\"tile",
					"User should be able to Click on \"Scheduled Jobs\"tile ", "Role Center Tile viewed successfully",
					"pass");
		} else {

			writeTestResults(" Click on \"Scheduled Jobs\"tile",
					"User should be able to Click on \"Role Center Tile\" ",
					"Role Center Tile not  viewed successfully", "fail");
		}

		Thread.sleep(2000);

		switchWindow();

		click("//*[text()='lastday of month : newauto']");

		Thread.sleep(2000);

//		click(btn_ServiceContractLastdayofMonth);

		click("//a[contains(text(),'Hire Agreement')]");

		Thread.sleep(3000);

		String val = SpecificHireagreementNo.replace("agreement_no", HireAgreeNo);

		Thread.sleep(2000);

		click(val);

		Thread.sleep(2000);

		mouseMove(run);

		Thread.sleep(2000);

		click(run);

		Thread.sleep(6000);

		if (isDisplayed(MessagePopup)) {

			writeTestResults("Click on \"RUN\"  button", "User should be able to Click on \"RUN\"  button ",
					"Message Popup viewed successfully", "pass");
		} else {

			writeTestResults("Click on \"RUN\"  button", "User should not able to Click on \"RUN\"  button ",
					"Message Popup not viewed successfully", "fail");
		}

		Thread.sleep(2000);

		click(btn_serviceInvoicePath);

		Thread.sleep(3000);

//			 if(isDisplayed(ServiceInvoicePage)) {
//					
//				 writeTestResults(" Check the service invoice amount is same to the amount in the actual update look up" , "User should be able to Check the service invoice amound is same to the amount in the actual update look up ", "service invoice amount is same to the amount in the actual update look up", "pass");
//			}else {
//				
//				writeTestResults(" Check the service invoice amount is same to the amount in the actual update look up"  , "User should be able to Check the service invoice amound is same to the amount in the actual update look up", "service invoice amount is same to the amount in the actual update look up", "fail");
//			}

		switchWindow();

		Thread.sleep(4000);

		click(btn_navigationmenu);

		if (isDisplayed(sidemenu)) {
			writeTestResults("Click on the top navigation menu", "System should be loaded the menu item successfully",
					"Menu item is displayed", "pass");
		} else {
			writeTestResults("Click on the top navigation menu", "System should not  loaded the menu item successfully",
					"Menu item is not displayed", "fail");
		}

		click(btn_servicemodule);

		if (isDisplayed(subsidemenu)) {

			writeTestResults("Click on the Service module", "System should be loaded the Service module successfully",
					"subsidemenu is displayed", "pass");
		} else {

			writeTestResults("Click on the Service module", "System should not loaded the Service module successfully",
					"subsidemenu is not displayed", "fail");
		}

		Thread.sleep(1000);

		click(btn_HireAgreement);

		Thread.sleep(1000);

		click(btn_ServiceHireAgreement);

		Thread.sleep(4000);

	}

	public void FirstdayServiceHireAgreement() throws Exception {

		Thread.sleep(1000);

		click(btn_HireAgreement);

		click(btn_NewHireAgreement);

		if (isDisplayed(NewHireAgreementpage)) {

			writeTestResults(" Click on the New Hire Agreement Button",
					" User should be able to click on the New Hire Agreement Button",
					" User viewed New Hire Agreement form successfully", "pass");
		} else {

			writeTestResults("  Click on the New Hire Agreement Button",
					" User should not be able to click on the New Hire Agreement Button",
					"  User not viewed New Hire Agreement form successfully", "fail");
		}

		LocalTime myObj = LocalTime.now();

		sendKeys(AgreementNo, txt_AgreementNo + myObj);

		sendKeys(TitleAgreement, "new");

//			 click(selectagreementdate);

		selectText(Contractgroup, "Common");
		click(btn_agreementdate);

		click(btn_beforedate);

		Thread.sleep(3000);

		click(btn_beforedate);

		Thread.sleep(3000);

//		click(btn_beforedate);

		Thread.sleep(3000);

		click(selectagreementdate);

		Thread.sleep(2000);

		click(btn_EffectiveFrom);

		click(btn_beforedate);

		Thread.sleep(1000);

		click(btn_EffectiveFromDate);

		click(btn_EffectiveTo);

		Thread.sleep(1000);

//		click(btn_backInternalReturnOrder);

		Thread.sleep(1000);

		click(btn_EffectiveToDate);

		Thread.sleep(1000);

		sendKeys(DescriptionHireAgreement, "new test");

		Thread.sleep(1000);

		selectText(salesunitHireAgreement, "test");

		Thread.sleep(2000);

		selectText(HireBasis, "Units");

		Thread.sleep(2000);

		click(btn_accountsearchHireAgreement);

//			if(isDisplayed(AccountLookUPPage)) {
//				
//				 writeTestResults("Click account search", "AccountLookUPPage should be open", "AccountLookUPPage  page is displayed", "pass");
//			}else {
//				
//				writeTestResults("Click account search ", "AccountLookUPPage should not be open", "AccountLookUPPage is not displayed", "fail");
//			}
//			
		selectText(template, txt_Template);

		sendKeys(Accountname, "Rashi");

		pressEnter(Accountname);

		Thread.sleep(2000);

		doubleClick(btn_accountuser);

		Thread.sleep(2000);

		selectText(BillingBasis, "Monthly");

		click(btn_SearchSceduleCodeHireAgreement);

		Thread.sleep(2000);

//				sendKeys(SceduleCode,"TESTDAILY2");

		sendKeys(SceduleCode, txt_SceduleCodeFirstDayFM);

		pressEnter(SceduleCode);

		Thread.sleep(2000);

		doubleClick(btn_SceduleCode);

		Thread.sleep(2000);

		click(btn_responsibleuserHireAgreement);

		Thread.sleep(1000);

		selectText(template, txt_Template);

		sendKeys(responsibleuser, "Nihal Peiris");

		Thread.sleep(2000);

		pressEnter(responsibleuser);

		Thread.sleep(3000);

		doubleClick(btn_responsibleusername);

		Thread.sleep(2000);

		click(btn_ResourceInformation);

		if (isDisplayed(RecourceInformationPage)) {

			writeTestResults("Go to \"Resource Information\" tab",
					"User should be able to Go to \"Resource Information\" tab",
					"User able to Go to \"Resource Information\" tab successfully", "pass");

		} else {

			writeTestResults("Go to \"Resource Information\" tab ",
					"User should not be able to Go to \"Resource Information\" tab",
					"User not able to Go to \\\"Resource Information\\\" tab successfully", "fail");

		}

		click(btn_searchbtnReferncecode);

		if (isDisplayed(ReferncecodePage)) {

			writeTestResults(" Click on search icon in the \" Referance Code\" column",
					"User should be able to Click on search icon in the \" Referance Code\" column",
					"ReferncecodePage viewed successfully", "pass");
		} else {

			writeTestResults(" Click on search icon in the \" Referance Code\" column",
					"User should not be able to Click on search icon in the \" Referance Code\" column",
					"ReferncecodePage not viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Referncecode, "HireAgResource [HireAgResource]");

		Thread.sleep(2000);

		pressEnter(Referncecode);

		Thread.sleep(3000);

		doubleClick(btn_Referncecode);

		Thread.sleep(3000);

		sendKeys(openingunit, "1");

		Thread.sleep(1000);

		sendKeys(Minimumunit, "1");

		click(btn_RateRatio);

		if (isDisplayed(RateRangeSetup)) {

			writeTestResults(" Click on \"Rate Ratio\" icon",
					"User should be able to Click on search icon in the \" Referance Code\" column",
					"ReferncecodePage viewed successfully", "pass");
		} else {

			writeTestResults(" Click on \"Rate Ratio\" icon",
					"User should not be able to Click on search icon in the \" Referance Code\" column",
					"ReferncecodePage not viewed successfully", "fail");
		}

		sendKeys(Start1, "1");

		sendKeys(End1, "10");

		sendKeys(Rate1, "100");

		Thread.sleep(1000);

		click(btn_plusRR);

		Thread.sleep(1000);

		sendKeys(Start2, "11");

		sendKeys(End2, "0");

		sendKeys(Rate2, "200");

		Thread.sleep(1000);

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript(btn_applyCommon);

		Thread.sleep(1000);

		click(btn_draftHireAgreement);

		if (isDisplayed(DraftedHireAgreement)) {

			writeTestResults(" Click on \"Draft\" button",
					"User should be able to Click on \"Draft\" button and document should be draft",
					"User drafted the HireAgreement successfully", "pass");
		} else {

			writeTestResults(" Click on \"Draft\" button",
					"User should not be able to Click on \"Draft\" button and document should be draft",
					"User not drafted the HireAgreement successfully", "fail");
		}

		Thread.sleep(4000);

		click(btn_ReleaseHireAgreement);

		Thread.sleep(4000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleasedHireAgreement)) {

			writeTestResults(" Click on \"Release\" button",
					" User should be able to Click on \"Release\" button and document should be Released",
					"User released the HireAgreement successfully", "pass");
		} else {

			writeTestResults("Click on \"Release\" button",
					" User should be able to Click on \"Release\" button and document should be Released",
					"User not released the HireAgreement successfully", "fail");
		}

		String HireAgreeNo = getText(HireAgreeNumber);

		System.out.println(HireAgreeNo);

		Thread.sleep(4000);

		pageRefersh();

		Thread.sleep(4000);

		switchWindow();

		click(btn_action);

		Thread.sleep(2000);

		click(btn_ActualUpdate);

		Thread.sleep(3000);

		sendKeys(actualunit, "1");

		Thread.sleep(3000);

		click(btn_checkoutActualUpdate);

		Thread.sleep(2000);

		click(btn_applyActualUpdate);

		Thread.sleep(2000);

		Thread.sleep(2000);

		pageRefersh();

		click(btn_entution);

		Thread.sleep(1000);

		click(btn_Rolecenter);

		Thread.sleep(2000);

		if (isDisplayed(Dashboardpage)) {

			writeTestResults("Click on \"Role Center Tile\"", "User should be able to Click on \"Role Center Tile\" ",
					"Role Center Tile viewed successfully", "pass");
		} else {

			writeTestResults(" Click on \"Role Center Tile\"", "User should be able to Click on \"Role Center Tile\" ",
					"Role Center Tile not  viewed successfully", "fail");
		}

		Thread.sleep(1000);

		click(btn_ScheduledJobs);

		Thread.sleep(2000);

		if (isDisplayed(Dashboardpage)) {

			writeTestResults("Click on \"Scheduled Jobs\"tile",
					"User should be able to Click on \"Scheduled Jobs\"tile ", "Role Center Tile viewed successfully",
					"pass");
		} else {

			writeTestResults(" Click on \"Scheduled Jobs\"tile",
					"User should be able to Click on \"Role Center Tile\" ",
					"Role Center Tile not  viewed successfully", "fail");
		}

		Thread.sleep(2000);

		switchWindow();

		// click(btn_frstdayfirstmonth);

		click("//label[contains(text(),'firstdayfirstmonth : FDFM(test)')]");

		Thread.sleep(2000);

		click("//*[@id=\"overdueyear\"]/div[17]/div/div[1]/ul/li/ul/li[2]/a");

		Thread.sleep(2000);

		String val = SpecificHireagreementNo.replace("agreement_no", HireAgreeNo);

		System.out.println(SpecificHireagreementNo);

		Thread.sleep(2000);

		click(val);

		Thread.sleep(2000);

		mouseMove(
				"//label[text()='firstdayfirstmonth : FDFM(test)']/../..//div[@class='task-due-date-due']/..//label[text()='02/01/2020']/../../..//button[text()='Run']");

		Thread.sleep(2000);

		click("//label[text()='firstdayfirstmonth : FDFM(test)']/../..//div[@class='task-due-date-due']/..//label[text()='02/01/2020']/../../..//button[text()='Run']");

		Thread.sleep(6000);

		if (isDisplayed(MessagePopup)) {

			writeTestResults("Click on \"RUN\"  button", "User should be able to Click on \"RUN\"  button ",
					"Message Popup viewed successfully", "pass");
		} else {

			writeTestResults("Click on \"RUN\"  button", "User should not able to Click on \"RUN\"  button ",
					"Message Popup not viewed successfully", "fail");
		}

		Thread.sleep(2000);

		click(btn_serviceInvoicePath);

		Thread.sleep(3000);

//			 if(isDisplayed(ServiceInvoicePage)) {
//					
//				 writeTestResults(" Check the service invoice amount is same to the amount in the actual update look up" , "User should be able to Check the service invoice amound is same to the amount in the actual update look up ", "service invoice amount is same to the amount in the actual update look up", "pass");
//			}else {
//				
//				writeTestResults(" Check the service invoice amount is same to the amount in the actual update look up"  , "User should be able to Check the service invoice amound is same to the amount in the actual update look up", "service invoice amount is same to the amount in the actual update look up", "fail");
//			}

		switchWindow();

		Thread.sleep(4000);

	}

	public void btn_ServiceJobOrderOutbound() throws Exception {

		click(btn_ServiceJobOrder);

		if (isDisplayed(ServiceJobOrderPage)) {

			writeTestResults("Click on Service job order under menu",
					"Service job order should be available in the menu", "Service job order is displayed", "pass");
		} else {

			writeTestResults("Click on Service job order under menu",
					"Service job order should not available in the menu", "Service job order is not displayed", "fail");
		}

		Thread.sleep(3000);

		click(btn_NewServiceJobOrder);

		if (isDisplayed(StartNewJourneyPage)) {

			writeTestResults(" Click on \"New Service job order\" button in By page",
					"Service job order journey look up should be open",
					"Service job order journey look up is displayed", "pass");
		} else {

			writeTestResults(" Click on \"New Service job order\" button in By page",
					"Service job order journey look up should be open",
					"Service job order journey look up is not displayed", "fail");
		}

		Thread.sleep(4000);

		click(btn_ServiceOrdertoInternalDispatchOrderExternal);

		if (isDisplayed(NewServiceJobOrderPage)) {

			writeTestResults("Select \"Service Order to Internal Dispatch Order (External) \" Journey",
					"Service job order New  page should be open", "Service job order New  page is displayed", "pass");
		} else {

			writeTestResults(" Select \"Service Order to Internal Dispatch Order (External) \" Journey",
					"Service job order New  page should not be open", "Service job order New page is  not displayed",
					"fail");
		}
		LocalTime myObj = LocalTime.now();

		sendKeys(ServiceJobOrderTitle, txt_ServiceJobOrderTitle + myObj);

//			sendKeys(ServiceJobOrderAccount,txt_ServiceJobOrderAccount);

		click(btn_accountsearch);

		if (isDisplayed(AccountLookUPPage)) {

			writeTestResults("Click account search", "AccountLookUPPage should be open",
					"AccountLookUPPage  page is displayed", "pass");
		} else {

			writeTestResults("Click account search ", "AccountLookUPPage should not be open",
					"AccountLookUPPage is not displayed", "fail");
		}

		selectText(template, txt_Template);

		sendKeys(Accountname, txt_Accountname);

		pressEnter(Accountname);

		Thread.sleep(2000);

		doubleClick(btn_accountuser);

		Thread.sleep(1000);

		click(btn_Productso);

		Thread.sleep(4000);

		click(btn_SerialNos);

		Thread.sleep(1000);

		selectText(ServiceGroup, "IDC-INTER DEPARTMENT JOBS");

		sendKeys(ServiceJobOrderDescription, txt_ServiceJobOrderDescription);

		Thread.sleep(2000);
//			 
//			 click(btn_searchShippingAddress);
//			 
//			 sendKeys(ShippingAddress, "09,Colombo,Srilanka");
//			 
//			 Thread.sleep(2000);
//			 
//			 click(btn_ApplyAdress);

		Thread.sleep(2000);

		click(btn_searchowner);

		Thread.sleep(1000);

		if (isDisplayed(OwnerLookUpPage)) {

			writeTestResults(" Click to search owner ", "OwnerLookUpPage should be open ",
					"OwnerLookUpPage viewed successfully", "pass");
		} else {

			writeTestResults(" Click to search owner ", " OwnerLookUpPage should not  be open",
					"OwnerLookUpPage not viewed successfully", "fail");
		}

		sendKeys(owner, "H V Hewa Vidana");

		pressEnter(owner);

		Thread.sleep(1000);

		doubleClick(btn_owner);

		Thread.sleep(1000);

		selectText(Priority, "High");

		Thread.sleep(1000);

		click(btn_PricingProfileSearch);

		if (isDisplayed(PricingProfileLookUpPage)) {

			writeTestResults(" Select a \"Pricing Profile\" ", " Able to select a pricing profile from the look up",
					"Able to select pricing profile successfully", "pass");
		} else {

			writeTestResults(" Select a \"Pricing Profile\" ", " Not Able to select a pricing profile from the look up",
					"Not able to select pricing profile successfully", "fail");
		}
		Thread.sleep(1000);

		doubleClick(btn_pricingprofile12);

	}

	public void DirectServiceProductRegstraionRec() throws Exception {

		click(btn_InventoryandWareHousing);

		click(btn_ServiceProductRegistration);

		click(btn_NewServiceProductRegistration);

		selectText(RegistrationType, "Direct");

		click(btn_accountsearchServiceProductRegis);

//				if(isDisplayed(AccountLookUPPage)) {
//					
//					 writeTestResults("Click account search", "AccountLookUPPage should be open", "AccountLookUPPage  page is displayed", "pass");
//				}else {
//					
//					writeTestResults("Click account search ", "AccountLookUPPage should not be open", "AccountLookUPPage is not displayed", "fail");
//				}
//				
		selectText(template, txt_Template);

		sendKeys(Accountname, "Rashi");

		pressEnter(Accountname);

		Thread.sleep(2000);

		doubleClick(btn_accountuser);

		Thread.sleep(2000);

		LocalTime myObj = LocalTime.now();

		sendKeys(tagnoDirectSR, txt_tagnoDirectSR + myObj);

		Thread.sleep(1000);

		click(btn_searchReferenceCode);

		Thread.sleep(2000);

		if (isDisplayed(ProductLookUpPage)) {

			writeTestResults(" Click search to product", "Should be able to view product look up page",
					"product look up page viewed successfully", "pass");

		} else {

			writeTestResults(" Click search to product", "Should not able to view product look up page",
					"product look up page not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, txt_SerialSpecificProduct);

		Thread.sleep(2000);
		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		click(btn_SearchWarrantyProfile);

		selectText(template, txt_Template);

		Thread.sleep(2000);

		sendKeys(Warranty, "Task81448-1");

		Thread.sleep(1000);

		pressEnter(Warranty);

		Thread.sleep(2000);

		doubleClick(btn_Warranty);

		Thread.sleep(2000);

		click(btn_WarrantyDate);

		click(btn_selectWarrantyDate);

		Thread.sleep(1000);

		click(btn_warrantyProfileSales);

		selectText(template, txt_Template);

		Thread.sleep(2000);

		sendKeys(Warranty, "Task81448-1");

		Thread.sleep(1000);

		pressEnter(Warranty);

		Thread.sleep(2000);

		doubleClick(btn_Warranty);

		Thread.sleep(2000);

		click(btn_WarrantyDateSales);

		Thread.sleep(1000);

		click(btn_selectWarrantyDate);

		Thread.sleep(2000);

		click(btn_DraftServiceProductReg);

		if (isDisplayed(DraftedHireAgreement)) {

			writeTestResults(" Click on \"Draft\" button",
					"User should be able to Click on \"Draft\" button and document should be draft",
					"User drafted the HireAgreement successfully", "pass");
		} else {

			writeTestResults(" Click on \"Draft\" button",
					"User should not be able to Click on \"Draft\" button and document should be draft",
					"User not drafted the HireAgreement successfully", "fail");
		}

		Thread.sleep(1000);

		click(btn_ReleaseServiceProductReg);

		Thread.sleep(4000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleasedHireAgreement)) {

			writeTestResults(" Click on \"Release\" button",
					" User should be able to Click on \"Release\" button and document should be Released",
					"User released the HireAgreement successfully", "pass");
		} else {

			writeTestResults("Click on \"Release\" button",
					" User should be able to Click on \"Release\" button and document should be Released",
					"User not released the HireAgreement successfully", "fail");
		}

	}

	public void ProductRegistration() throws Exception {

		SalesAndMarketingModule sales = new SalesAndMarketingModule();
		sales.checkSalesAndMarketingModule();
		sales.clickOnSalesAndMarketing();
		sales.checkClickSalesOrderPage();
		sales.checkSalesOrderHeader();
		sales.checkJourneySalesOrderToSalesInvoice();
		sales.fillDetailsSalesOrderToSalesInvoice();
		sales.outboundShipmentSalesOrderToSalesInvoice();
		sales.salesInvoiceSalesOrderToSalesInvoice();

		click(serialBatchProductRegis);

		Thread.sleep(3000);

		click(productListBtn);

		Thread.sleep(4000);

		String serialNoValPR = getText(serialTextValuePR);

		System.out.println(serialNoValPR);

		Thread.sleep(3000);

		pageRefersh();

		Thread.sleep(4000);

		click(btn_action);

		Thread.sleep(2000);

		click(btn_ProductRegistration);

		Thread.sleep(2000);

		click(btn_okProductRegistration);

		click(btn_SearchWarrantyProfileProductRegis);

		selectText(template, txt_Template);

		Thread.sleep(2000);

		sendKeys(Warranty, "Task81448-1");

		Thread.sleep(1000);

		pressEnter(Warranty);

		Thread.sleep(2000);

		doubleClick(btn_Warranty);

		Thread.sleep(2000);

		click(btn_ApplyProductRegis);

		Thread.sleep(4000);

		click(btn_ProductRegistration);

		Thread.sleep(4000);

		click(btn_navigationmenu1);

		Thread.sleep(2000);

		click(btn_InventoryandWareHousing);

		click(btn_ServiceProductRegistration);

		Thread.sleep(4000);

		sendKeys(serialNoProductRegis, serialNoValPR);

		pressEnter(serialNoProductRegis);

		Thread.sleep(2000);

		click("//*[@id=\"g1171-t\"]/table/tbody/tr/td[4]/a");

		Thread.sleep(4000);

//			switchWindow();

		click(btn_SalesReferDoc);

		Thread.sleep(4000);

//			switchWindow();

		Thread.sleep(4000);
//		
//		    if(isDisplayed(SalesInvoicePage)) {
//				
//				 writeTestResults("  Click on the link in \" Doc No\" " , " User should be able to Click on the link in \" Doc No\" ", "User viewed the ServiceInvoice Page successfully", "pass");
//			}else {
//				
//				writeTestResults(" Click on the link in \" Doc No\" "  , " User should not able to Click on the link in \" Doc No\" ", "User not  viewed the ServiceInvoice Page successfully", "fail");
//			}

		switchWindow();
		switchWindow();

		Thread.sleep(4000);

		click(btn_action);

		Thread.sleep(3000);

		click(btn_AssosiateProduct);

		Thread.sleep(2000);

		click(btn_searchAssosiateProduct);

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, txt_SerialSpecificProduct);

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(2000);

		sendKeys(TagnoAssosiateProduct, "001");

		click(btn_searchWarrantyProfileAssosiateProduct);

		selectText(template, txt_Template);

		Thread.sleep(2000);

		sendKeys(Warranty, "1YEAR-SLS");

		Thread.sleep(1000);

		pressEnter(Warranty);

		Thread.sleep(2000);

		doubleClick("//*[@id=\"g1168-t\"]/table/tbody/tr[1]/td[3]");

		Thread.sleep(2000);

		click(btn_apply);

		Thread.sleep(3000);

		click(btn_AssosiateProductTab);

	}

	public void ChangeOwnershipInformation() throws Exception {

		click(btn_action);

		click(btn_ChangeButtonOwnerShipInformation);

		Thread.sleep(1000);

		click(btn_SerarchNewOwner);

		if (isDisplayed(AccountLookUPPage)) {

			writeTestResults("Click account search", "AccountLookUPPage should be open",
					"AccountLookUPPage  page is displayed", "pass");
		} else {

			writeTestResults("Click account search ", "AccountLookUPPage should not be open",
					"AccountLookUPPage is not displayed", "fail");
		}

		selectText(template, txt_Template);

		sendKeys(Accountname, "0001H [Chamara Roshan]");

		pressEnter(Accountname);

		Thread.sleep(2000);

		doubleClick(btn_accountuser);

		Thread.sleep(1000);

		click(btn_applyChangeOwnershipInformation);

		Thread.sleep(2000);

		pageRefersh();

	}

	public void ChangeProductRegisInfo() throws Exception {

		Thread.sleep(4000);

		click(btn_action);

		click(btn_ChangeProductRegisInformation);

		Thread.sleep(2000);

		click(btn_SearchEndUser);

		Thread.sleep(2000);

//		  if(isDisplayed(AccountLookUPPage)) {
//				
//				 writeTestResults("Click account search", "AccountLookUPPage should be open", "AccountLookUPPage  page is displayed", "pass");
//			}else {
//				
//				writeTestResults("Click account search ", "AccountLookUPPage should not be open", "AccountLookUPPage is not displayed", "fail");
//			}

		selectText(template, txt_Template);

		Thread.sleep(2000);

		sendKeys(Accountname, "00012-Madhushan [Madhushan_Entution[Fin_TC_018]]");

		pressEnter(Accountname);

		Thread.sleep(2000);

		doubleClick(btn_accountuser);

		Thread.sleep(1000);

		click(btn_ApplyProductRegisInformation);

		Thread.sleep(2000);

		pageRefersh();

	}

	public void SalesWarrantyExtend() throws Exception {

		Thread.sleep(2000);

		click(btn_action);

		click(btn_ExtendSalesWarranty);

		Thread.sleep(2000);

		click(btn_plusSalesWarrantyExtend);

		Thread.sleep(2000);

		click(btn_searchSalesWarrantyExtend);

		selectText(template, txt_Template);

		Thread.sleep(2000);

		sendKeys(Warranty, "1YEAR-SLS");

		Thread.sleep(1000);

		pressEnter(Warranty);

		Thread.sleep(2000);

		doubleClick(btn_Warranty);

		Thread.sleep(2000);

		click(btn_UpdateSalesWarranty);

		Thread.sleep(2000);

		pageRefersh();

	}

	public void InvoiceLiscenceRenewal() throws Exception {

		Thread.sleep(2000);

		click(btn_action);

		Thread.sleep(2000);

		click(btn_Renewal);

		Thread.sleep(4000);

//		  click(btn_yesRenewal);

		Thread.sleep(3000);

		switchWindow();

		click(btn_searchBillingAddress);

		sendKeys(BillingAddress1, txt_BillingAddress1);

		Thread.sleep(1000);

		sendKeys(ShipingAddress1, txt_ShipingAddress1);

		Thread.sleep(1000);
		// btn apply
		click("//*[@id=\"inspect\"]/div[8]/div[3]/a");

		Thread.sleep(3000);

		selectText("//*[@id=\"cboxSalesUnitPerson\"]", txt_SalesUnit);

		Thread.sleep(3000);

//		click(btn_ServiceDetails);

		click("//*[@id=\"#divDetailsTab\"]");

		Thread.sleep(3000);

//		click(btn_searchserviceproductfirst);

		// *[@id="commonTbl"]/tbody/tr/td[9]/span

		click("//*[@id=\"tblSerQuotation\"]/tbody/tr/td[16]/span");

		if (isDisplayed(ProductLookUpPage)) {

			writeTestResults(" Click search to product", "Should be able to view product look up page",
					"product look up page viewed successfully", "pass");

		} else {

			writeTestResults(" Click search to product", "Should not able to view product look up page",
					"product look up page not  viewed successfully", "fail");

		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, txt_SerialSpecificProduct);

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(2000);

//		sendKeys(EstimatedQtyservice, txt_EstimatedQtyservice);
		sendKeys(EstimatedQtyservice, "5");

		Thread.sleep(1000);

		click("//input[@class='unchecked el-resize15']");

		Thread.sleep(3000);

		click("//*[@id=\"tblSerQuotation\"]/tbody/tr/td[2]/span[1]");

		selectText(tasktype, "Input Product");

		click("//*[@id=\"tblSerQuotation\"]/tbody/tr[2]/td[16]/span");

		if (isDisplayed(ProductLookUpPage)) {

			writeTestResults(" Click search to product", "Should be able to view product look up page",
					"product look up page viewed successfully", "pass");
		} else {

			writeTestResults(" Click search to product", "Should not able to view product look up page",
					"product look up page not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, txt_SerialSpecificProduct);

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		sendKeys(EstimatedQtyserial, txt_EstimatedQtyserial);

		Thread.sleep(2000);

		Thread.sleep(1000);

		click(btn_expand2);

		Thread.sleep(1000);

		click("//*[@id=\"tblSerQuotation\"]/tbody/tr[3]/td[16]/span");

		if (isDisplayed(ProductLookUpPage)) {

			writeTestResults(" Click search to product", "Should be able to view product look up page",
					"product look up page viewed successfully", "pass");
		} else {

			writeTestResults(" Click search to product", "Should not able to view product look up page",
					"product look up page not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, txt_BatchSpecificProduct);

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(2000);

//			sendKeys(EstimatedQty, txt_EstimatedQty);

//			sendKeys(EstimatedBatch, txt_EstimatedQtyBatch);
		sendKeys("//*[@id=\"tblSerQuotation\"]/tbody/tr[3]/td[23]/input", "5");

		Thread.sleep(1000);

		sendKeys("//*[@id=\"tblSerQuotation\"]/tbody/tr[3]/td[24]/input", "1000");

		click("//*[@id=\"tblSerQuotation\"]/tbody/tr[3]/td[2]/span[1]");

		Thread.sleep(1000);

//		click(btn_searchFIFOproduct);

		click("//*[@id=\"tblSerQuotation\"]/tbody/tr[4]/td[16]/span");

		if (isDisplayed(ProductLookUpPage)) {

			writeTestResults(" Click search to product", "Should be able to view product look up page",
					"product look up page viewed successfully", "pass");
		} else {

			writeTestResults(" Click search to product", "Should not able to view product look up page",
					"product look up page not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, txt_FIFOProduct);

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(2000);

//			sendKeys(EstimatedQtyFIFO, txt_EstimatedQtyFIFO);
		sendKeys("//*[@id=\"tblSerQuotation\"]/tbody/tr[4]/td[23]/input", "5");

		Thread.sleep(1000);

		sendKeys("//*[@id=\"tblSerQuotation\"]/tbody/tr[4]/td[24]/input", "1000");

		Thread.sleep(1000);

		click("//*[@id=\"tblSerQuotation\"]/tbody/tr[4]/td[2]/span[1]");

		Thread.sleep(1000);

		selectText("//*[@id=\"tblSerQuotation\"]/tbody/tr[5]/td[13]/select", "Services");

		click("//*[@id=\"tblSerQuotation\"]/tbody/tr[5]/td[16]/span");

		if (isDisplayed(ProductLookUpPage)) {

			writeTestResults(" Click search to product", "Should be able to view product look up page",
					"product look up page viewed successfully", "pass");
		} else {

			writeTestResults(" Click search to product", "Should not able to view product look up page",
					"product look up page not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, txt_serviceProduct);

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(2000);

//			sendKeys(EstimatedQtyservice, txt_EstimatedQtyservice);
		sendKeys(EstimatedQtyservice, "5");

		Thread.sleep(1000);

		sendKeys(EstimatedValueService, "100");

		Thread.sleep(1000);

		click("//*[@id=\"tblSerQuotation\"]/tbody/tr[5]/td[2]/span[1]");

		Thread.sleep(1000);

		click("//*[@id=\"tblSerQuotation\"]/tbody/tr[5]/td[2]/span[1]");

		Thread.sleep(1000);

		selectText("//*[@id=\"tblSerQuotation\"]/tbody/tr[6]/td[13]/select", "Sub Contract");

		click("//*[@id=\"tblSerQuotation\"]/tbody/tr[6]/td[16]/span");

		if (isDisplayed(ProductLookUpPage)) {

			writeTestResults(" Click search to product", "Should be able to view product look up page",
					"product look up page viewed successfully", "pass");
		} else {

			writeTestResults(" Click search to product", "Should not able to view product look up page",
					"product look up page not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, txt_serviceProduct);

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(2000);

//			sendKeys(EstimatedQtysubcontract, txt_EstimatedQtysubcontract);
//		sendKeys("", "5");

		sendKeys("//*[@id=\"tblSerQuotation\"]/tbody/tr[6]/td[24]/input", "100");

		Thread.sleep(1000);

		click("//*[@id=\"tblSerQuotation\"]/tbody/tr[6]/td[2]/span[1]");

		Thread.sleep(2000);

		selectText("//*[@id=\"tblSerQuotation\"]/tbody/tr[7]/td[13]/select", "Resource");

		Thread.sleep(1000);

		click("//*[@id=\"tblSerQuotation\"]/tbody/tr[7]/td[15]/span");

		if (isDisplayed(ResourceLookUpPage)) {

			writeTestResults(" Click search to resource", "Should be able to view resource look up page",
					"resource look up page viewed successfully", "pass");
		} else {

			writeTestResults(" Click search to resourse", "Should not able to view resource look up page",
					"resource look up page not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

//			 sendKeys(Resource,txt_Resource);

		sendKeys(Resource, "REC-test-001");

		Thread.sleep(2000);

		pressEnter(Resource);

		Thread.sleep(3000);

		doubleClick(btn_Resource);

		Thread.sleep(2000);

		click("//*[@id=\"tblSerQuotation\"]/tbody/tr[7]/td[16]/span");

		if (isDisplayed(ProductLookUpPage)) {

			writeTestResults(" Click search to product", "Should be able to view product look up page",
					"product look up page viewed successfully", "pass");
		} else {

			writeTestResults(" Click search to product", "Should not able to view product look up page",
					"product look up page not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, txt_serviceProduct);

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(2000);

		Thread.sleep(2000);

		click(btn_checkoutServiceQuatation);

		Thread.sleep(1000);

		click(btn_DraftServiceQuatation);

		Thread.sleep(1000);

		if (isDisplayed(DraftServiceQuatationPage)) {

			writeTestResults("Click on \"Draft\" Button", "User should be able to Draft the service Quotationorder",
					"User  able to Draft  the service Quotation successfully", "pass");
		} else {

			writeTestResults("Click on \"Draft\" Button", "User should not able to Draft  the service Quotation",
					"User not able to Draft  the service Quotation successfully", "fail");
		}

		Thread.sleep(2000);

		click(btn_ReleaseServiceQuatation);

		Thread.sleep(2000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleasedServiceQuatationPage)) {

			writeTestResults("Click on \"Release\" Button", "User should be able to Release the service Quotationorder",
					"User  able to Release the service Quotation successfully", "pass");
		} else {

			writeTestResults("Click on \"Release\" Button", "User should not able to Release the service Quotation",
					"User not able to Release the service Quotation successfully", "fail");
		}

		Thread.sleep(1000);

		click(btn_version);

		mouseMove(version);

		click(btn_tick);

		Thread.sleep(1000);

		click(btn_confirm);

		Thread.sleep(1000);

		click(btn_gotopageServiceQuatation);

		Thread.sleep(1000);

		switchWindow();

		click(btn_ServiceDetailsServiceOrder);

		Thread.sleep(2000);

		click(btn_checkoutServiceOrder);

		Thread.sleep(2000);

		click(btn_draftServiceOrder);

		Thread.sleep(2000);

		click(btn_ReleaseServiceOrder);

		Thread.sleep(2000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleasedServiceOrderPage)) {

			writeTestResults("Click on \"Release\" Button", "User should be able to Release the ServiceOrder",
					"User  able to Release the ServiceOrder successfully", "pass");
		} else {

			writeTestResults("Click on \"Release\" Button", "User should not able to Release the ServiceOrder",
					"User not able to Release the ServiceOrder successfully", "fail");
		}

		Thread.sleep(2000);

		switchWindow();

		closeWindow();

		closeWindow();

		switchWindow();

		Thread.sleep(2000);

	}

	public void Renewal() throws Exception

	{

		click(btn_action);

		click(btn_Renewal2);

		click(btn_yesRenewal);

		Thread.sleep(4000);

		pageRefersh();

	}

	public void ReplaceProduct() throws Exception

	{

		click(btn_action);

		click(btn_ReplaceProducts);

		click(btn_SearchReplaceProduct);

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, "000000180_DPO [DPO ]");

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(2000);

		sendKeys(Replacetagno, "987");

		Thread.sleep(2000);

		click(btn_apply);

	}

	public void SalesInvoicetoOutboundShipment() throws Exception

	{
		SalesAndMarketingModule sales = new SalesAndMarketingModule();
		sales.checkNavigationMenu();
		sales.checkSalesAndMarketingModule();
		sales.clickOnSalesAndMarketing();
		sales.checkClickSalesInvoice();
		sales.checkJouneyLoadingSalesInvoice();
//		sales.fillDetailsInvoice();

	}

	public void InterDepartmant() throws Exception

	{
		ProjectModule p1 = new ProjectModule();
		p1.VerifyTheAbilityOfCreatingServiceJobOrder();

		Thread.sleep(2000);

		selectText(ServiceGroup, "IDC-INTER DEPARTMENT JOBS");

		Thread.sleep(2000);

		sendKeys(ServiceJobOrderDescription, txt_ServiceJobOrderDescription);

		Thread.sleep(2000);

		click(btn_searchowner);

		Thread.sleep(1000);

		if (isDisplayed(OwnerLookUpPage)) {

			writeTestResults(" Click to search owner ", "OwnerLookUpPage should be open ",
					"OwnerLookUpPage viewed successfully", "pass");
		} else {

			writeTestResults(" Click to search owner ", " OwnerLookUpPage should not  be open",
					"OwnerLookUpPage not viewed successfully", "fail");
		}

		sendKeys(owner, "H V Hewa Vidana");

		pressEnter(owner);

		Thread.sleep(1000);

		doubleClick(btn_owner);

		Thread.sleep(1000);

		click(btn_PricingProfileSearch);

		if (isDisplayed(PricingProfileLookUpPage)) {

			writeTestResults(" Select a \"Pricing Profile\" ", " Able to select a pricing profile from the look up",
					"Able to select pricing profile successfully", "pass");
		} else {

			writeTestResults(" Select a \"Pricing Profile\" ", " Not Able to select a pricing profile from the look up",
					"Not able to select pricing profile successfully", "fail");
		}

		Thread.sleep(2000);

		doubleClick(btn_pricingprofile12);

		click(btn_searchservicelocation);

		selectText(template, txt_Template);

		sendKeys(ServiceLocation, txt_ServiceLocation);

		Thread.sleep(2000);

		pressEnter(ServiceLocation);

		Thread.sleep(3000);

		doubleClick(btn_location);

		Thread.sleep(2000);

//        Thread.sleep(1000);

		selectText(Priority, "High");

//		click(btn_searchShippingAddress);
//
//		sendKeys(ShippingAddress, "09,Colombo,Srilanka");
//
//		Thread.sleep(2000);
//
//		click("//*[@id=\"inspect\"]/div[20]/div[3]/a");
//		

	}

	public void PurchaseinvoicetoInboundShipment41() throws Exception

	{
		ProcumentModule p2 = new ProcumentModule();

		p2.clickProbutton();
		p2.completePurchaseInvoiceToInboundShipment();
		Thread.sleep(2000);
		switchWindow();

		Thread.sleep(3000);
		pageRefersh();

		click(btn_action);

		click(btn_ProductRegistration);

		Thread.sleep(2000);

		click(btn_okProductRegistration);

		click(btn_SearchWarrantyProfileProductRegis);

		selectText(template, txt_Template);

		Thread.sleep(2000);

		sendKeys(Warranty, "Task81448-1");

		Thread.sleep(1000);

		pressEnter(Warranty);

		Thread.sleep(2000);

		doubleClick(btn_Warranty);

		Thread.sleep(2000);

		click(btn_ApplyProductRegis);

		Thread.sleep(4000);

		click(btn_ProductRegistration);

		Thread.sleep(4000);

		click(btn_navigationmenu1);

		Thread.sleep(2000);

		click(btn_InventoryandWareHousing);

		click(btn_ServiceProductRegistration);

		Thread.sleep(4000);

//		sendKeys(serialNoProductRegis, serialNoValPR);

		pressEnter(serialNoProductRegis);

		Thread.sleep(2000);

		click("//*[@id=\"g1171-t\"]/table/tbody/tr/td[4]/a");

		Thread.sleep(4000);

//			switchWindow();

		click(btn_SalesReferDoc);

		Thread.sleep(4000);

//			switchWindow();

		Thread.sleep(4000);

	}

	public void completePurchaseOrderToPurchaseInvoice42() throws Exception

	{
		ProcumentModule p3 = new ProcumentModule();

		p3.clickProbutton();
		p3.completePurchaseOrderToPurchaseInvoice();
		Thread.sleep(2000);

	}

	public void Verificationdraft() throws Exception

	{
		Thread.sleep(5000);

		click(btn_DraftServiceJobOrder);

		Thread.sleep(6000);

		trackCode = getText(ServiceJobOrderReleaseNoTC);

		if (isDisplayed(DraftServiceJobOrderPage)) {

			writeTestResults("Click on \"Draft\" Button", "User should be able to Draft the service job order",
					"service job order Drafted successfully", "pass");
		} else {

			writeTestResults(" Click on \"Draft\" Button", "User should not able to Draft the service job order",
					"service job order not  Drafted successfully", "fail");
		}

	}

	public void Edit() throws Exception

	{
		Thread.sleep(5000);

		click(btn_EditServiceJobOrder);

		Thread.sleep(6000);

		trackCode = getText(ServiceJobOrderReleaseNoTC);

		if (isDisplayed(EditServiceJobOrderPage)) {

			writeTestResults("Click on \"Edit\" Button", "User should be able to Edit the service job order",
					"service job order Edited successfully", "pass");
		} else {

			writeTestResults(" Click on \"Edit\" Button", "User should not able to Edit the service job order",
					"service job order not  Edited successfully", "fail");
		}

	}

	public void Update() throws Exception

	{
		Thread.sleep(5000);

		click(btn_EditServiceJobOrder);

		Thread.sleep(6000);

		trackCode = getText(ServiceJobOrderReleaseNoTC);

		if (isDisplayed(UpadatedServiceJobOrderPage)) {

			writeTestResults("Click on \"Update\" Button", "User should be able to Update the service job order",
					"service job order Updated successfully", "pass");
		} else {

			writeTestResults(" Click on \"Update\" Button", "User should not able to Update the service job order",
					"service job order not  Edited successfully", "fail");
		}

	}

	public void UpdateandNew() throws Exception

	{
		Thread.sleep(5000);

		click(btn_UpdateandNewServiceJobOrder);

		Thread.sleep(6000);

		trackCode = getText(ServiceJobOrderReleaseNoTC);

		if (isDisplayed(UpadatedServiceJobOrderPage)) {

			writeTestResults("Click on \"UpdateAndNew\" Button",
					"User should be able to UpdateAndNew the service job order",
					"Viewed new service job order page successfully", "pass");
		} else {

			writeTestResults(" Click on \"UpdateAndNew\" Button",
					"User should not able to Update the service job order",
					"Not viewed new service job order page successfully", "fail");
		}

	}

	public void draftandnew() throws Exception

	{
		Thread.sleep(5000);

		click(btn_DraftandNewServiceJobOrder);

		Thread.sleep(6000);

		trackCode = getText(ServiceJobOrderReleaseNoTC);

		if (isDisplayed(ReleaseServiceJobOrderPage)) {

			writeTestResults("Click on \"Draft and New\" Button",
					"User should be able to Draft and New the service job order",
					"New service job order viewed successfully", "pass");
		} else {

			writeTestResults(" Click on \"Draft and New\" Button",
					"User should not able to Draft and New the service job order",
					"New service job order not viewed successfully", "fail");
		}

	}

	public void btn_ServiceJobOrderhalf() throws Exception {

		LocalTime myObj = LocalTime.now();

		sendKeys(ServiceJobOrderTitle, txt_ServiceJobOrderTitle + myObj);

//			sendKeys(ServiceJobOrderAccount,txt_ServiceJobOrderAccount);

		click(btn_accountsearch);

		if (isDisplayed(AccountLookUPPage)) {

			writeTestResults("Click account search", "AccountLookUPPage should be open",
					"AccountLookUPPage  page is displayed", "pass");
		} else {

			writeTestResults("Click account search ", "AccountLookUPPage should not be open",
					"AccountLookUPPage is not displayed", "fail");
		}

		selectText(template, txt_Template);

		sendKeys(Accountname, txt_Accountname);

		pressEnter(Accountname);

		Thread.sleep(2000);

		doubleClick(btn_accountuser);

		Thread.sleep(1000);

//			 click(btn_Productso);
//		 
//		    Thread.sleep(4000);
//			 
//			 click(btn_SerialNos);

		Thread.sleep(1000);

		selectText(ServiceGroup, "IDC-INTER DEPARTMENT JOBS");

		sendKeys(ServiceJobOrderDescription, txt_ServiceJobOrderDescription);

		Thread.sleep(2000);

		Thread.sleep(2000);

		click(btn_searchowner);

		Thread.sleep(1000);

		if (isDisplayed(OwnerLookUpPage)) {

			writeTestResults(" Click to search owner ", "OwnerLookUpPage should be open ",
					"OwnerLookUpPage viewed successfully", "pass");
		} else {

			writeTestResults(" Click to search owner ", " OwnerLookUpPage should not  be open",
					"OwnerLookUpPage not viewed successfully", "fail");
		}

		sendKeys(owner, "H V Hewa Vidana");

		pressEnter(owner);

		Thread.sleep(1000);

		doubleClick(btn_owner);

		Thread.sleep(1000);

		selectText(Priority, "High");

		Thread.sleep(1000);

		click(btn_PricingProfileSearch);

		if (isDisplayed(PricingProfileLookUpPage)) {

			writeTestResults(" Select a \"Pricing Profile\" ", " Able to select a pricing profile from the look up",
					"Able to select pricing profile successfully", "pass");
		} else {

			writeTestResults(" Select a \"Pricing Profile\" ", " Not Able to select a pricing profile from the look up",
					"Not able to select pricing profile successfully", "fail");
		}
		Thread.sleep(1000);

		doubleClick(btn_pricingprofile12);

	}

	public void Release() throws Exception

	{

		click(btn_ReleaseServiceJobOrder);

		Thread.sleep(4000);

		trackCode = getText(ServiceJobOrderReleaseNoTC);

		if (isDisplayed(ReleaseServiceJobOrderPage)) {

			writeTestResults("Click on \"Release\" Button", "User should be able to Release the service job order",
					"service job order Released successfully", "pass");
		} else {

			writeTestResults(" Click on \"Release\" Button", "User should not able toRelease the service job order",
					"service job order not  Released successfully", "fail");
		}

	}

	public void ActualUpdate() throws Exception

	{
		click(btn_action);

		Thread.sleep(2000);

		click(btn_ActualUpdate);

		trackCode = getText(ServiceJobOrderReleaseNoTC);

		if (isDisplayed(ActualUpdatePage)) {

			writeTestResults("Click on \"ActualUpdate\" Button",
					"User should be able to ActualUpdate the service job order",
					"service job order can ActualUpdate successfully", "pass");
		} else {

			writeTestResults(" Click on \"ActualUpdate\" Button",
					"User should not able to ActualUpdate the service job order",
					"service job order cant ActualUpdate successfully", "fail");
		}

		Thread.sleep(2000);

		click(btn_closepopup);

	}

	public void AddProductTagNo() throws Exception

	{
		click(btn_action);

		Thread.sleep(2000);

		click(btn_AddProductTagNo);

		trackCode = getText(ServiceJobOrderReleaseNoTC);

		if (isDisplayed(AddProductTagNoPage)) {

			writeTestResults("Click on \"ActualUpdate\" Button",
					"User should be able to ActualUpdate the service job order",
					"service job order can ActualUpdate successfully", "pass");
		} else {

			writeTestResults(" Click on \"ActualUpdate\" Button",
					"User should not able to ActualUpdate the service job order",
					"service job order cant ActualUpdate successfully", "fail");
		}

		Thread.sleep(2000);

		click(btn_closepopup);

	}

	public void createInteralOrder() throws Exception

	{
		click(btn_action);

		Thread.sleep(2000);

		click(btn_CreateInternalOrder);

		trackCode = getText(ServiceJobOrderReleaseNoTC);

		if (isDisplayed(InternalOrderPage)) {

			writeTestResults("Click on \"CreateInternalOrder\" Button",
					"User should be able to CreateInternalOrder the service job order",
					"service job order can CreateInternalOrder  successfully", "pass");
		} else {

			writeTestResults(" Click on \"CreateInternalOrder\" Button",
					"User should not able to CreateInternalOrder the service job order",
					"service job order cant CreateInternalOrder successfully", "fail");
		}

		Thread.sleep(2000);

		click(btn_closepopup);

	}

	public void createInteralReturnOrder() throws Exception

	{
		click(btn_action);

		Thread.sleep(2000);

		click(btn_backInternalReturnOrder);

		trackCode = getText(ServiceJobOrderReleaseNoTC);

		if (isDisplayed(InternalOrderPage)) {

			writeTestResults("Click on \"CreateInternalReturnOrder\" Button",
					"User should be able to CreateInternalReturnOrder the service job order",
					"service job order can CreateInternalReturnOrder  successfully", "pass");
		} else {

			writeTestResults(" Click on \"CreateInternalReturnOrder\" Button",
					"User should not able to CreateInternalReturnOrder the service job order",
					"service job order cant CreateInternalReturnOrder successfully", "fail");
		}

		Thread.sleep(2000);

		click(btn_closepopup);

	}

	public void createInvoiceProposal() throws Exception

	{
		click(btn_action);

		Thread.sleep(2000);

		click(btn_CreateInvoiceProposal);

		trackCode = getText(ServiceJobOrderReleaseNoTC);

		if (isDisplayed(InvoiceProposalPage)) {

			writeTestResults("Click on \"InvoiceProposalPopup\" Button",
					"User should be able to create InvoiceProposal the service job order",
					" Can  create InvoiceProposal successfully", "pass");
		} else {

			writeTestResults(" Click on \"InvoiceProposalPopup\" Button",
					"User should not able to InvoiceProposalPopup the service job order",
					"Cant InvoiceProposalPopup successfully", "fail");
		}

		Thread.sleep(2000);

		click(btn_closepopup);

	}

	public void CreateServiceQuotation() throws Exception

	{
		click(btn_action);

		Thread.sleep(2000);

		click(btn_CreateServiceQuatation);

		Thread.sleep(2000);

		switchWindow();

		trackCode = getText(ServiceJobOrderReleaseNoTC);

		if (isDisplayed(ServiceQuotationPagePopup)) {

			writeTestResults("Click on \"ServiceQuotationPopup\" Button",
					"User should be able to create ServiceQuotationPopup the service job order",
					" Can  create ServiceQuotationPopup successfully", "pass");
		} else {

			writeTestResults(" Click on \"ServiceQuotationPopup\" Button",
					"User should not able to ServiceQuotationPopup the service job order",
					"Cant ServiceQuotationPopup successfully", "fail");
		}

		Thread.sleep(3000);

		closeWindow();

	}

	public void CustomerAdvance() throws Exception

	{
		click(btn_action);

		Thread.sleep(2000);

		click(btn_GenerateCustomerAdvance);

		trackCode = getText(ServiceJobOrderReleaseNoTC);

		if (isDisplayed(CustomerAdvancePage)) {

			writeTestResults("Click on \"GenerateCustomerAdvance\" Button",
					"User should be able to create GenerateCustomerAdvancePopup the service job order",
					" Can  create GenerateCustomerAdvancePopup successfully", "pass");
		} else {

			writeTestResults(" Click on \"GenerateCustomerAdvance\" Button",
					"User should not able to ServiceQuotationPopup the service job order",
					"Cant GenerateCustomerAdvancePopup successfully", "fail");
		}

		Thread.sleep(2000);

		click(btn_closepopup);

	}

	public void ResponceDetails() throws Exception

	{
		click(btn_action);

		Thread.sleep(3000);

		click("//div[@class='splitbtncontent']//a[contains(text(),'Response Detail')]");

		switchWindow();

		trackCode = getText(ServiceJobOrderReleaseNoTC);

		if (isDisplayed("//div[@class='dialogbox-header']//span[@class='headertext']")) {

			writeTestResults("Click on \"btn_GenerateResponceDetails\" Button",
					"User should be able to create ResponceDetailsPopup the service job order",
					" Can  create ResponceDetailsPopup successfully", "pass");
		} else {

			writeTestResults(" Click on \"btn_GenerateResponceDetails\" Button",
					"User should not able to ResponceDetailsPopup the service job order",
					"Cant ResponceDetailsPopup successfully", "fail");
		}

		Thread.sleep(3000);

		click("//span[@class='headerclose']");

	}

	public void RMA() throws Exception

	{
		click(btn_action);

		Thread.sleep(3000);

		click("//a[contains(text(),'RMA')]");

		switchWindow();

//    	 trackCode = getText(ServiceJobOrderReleaseNoTC);

		if (isDisplayed("//div[@class='dialogbox-header']//span[contains(text(),'RMA Details')]")) {

			writeTestResults("Click on \"btn_RMA\" Button",
					"User should be able to create ResponceDetailsPopup the service job order",
					" Can  create RMA Details Page successfully", "pass");
		} else {

			writeTestResults(" Click on \"btn_RMA\" Button",
					"User should not able to ResponceDetailsPopup the service job order",
					"Cant RMA Details Page  successfully", "fail");
		}

		Thread.sleep(2000);

		click(btn_closeRMA);

	}

	public void SubContractOrder() throws Exception

	{
		click(btn_action);

		Thread.sleep(2000);

		click(btn_SubContractOrder);

//    	 trackCode = getText(ServiceJobOrderReleaseNoTC);
//
//    		if (isDisplayed(SubContractOrderPage1)) {
//
//    			writeTestResults("Click on \"SubContractOrder\" Button", "User should be able to create SubContractOrderPopup the service job order",
//    					" SubContractOrder button working sucessfully", "pass");
//    		} else {
//
//    			writeTestResults(" Click on \"SubContractOrder\" Button", "User should not able to SubContractOrderPopup the service job order",
//    					"Cant RMA Details Page  successfully", "fail");
//    		}	

		Thread.sleep(2000);

		click(btn_closepopup);

	}

	public void Stop() throws Exception

	{

		Thread.sleep(2000);

		click(btn_action);

		Thread.sleep(2000);

		click(btn_stopServiceJobOrder);

//    	 trackCode = getText(ServiceJobOrderReleaseNoTC);
//
//    		if (isDisplayed(stopserviceOrderPage)) {
//
//    			writeTestResults("Click on \"SubContractOrder\" Button", "User should be able to create SubContractOrderPopup the service job order",
//    					" SubContractOrder button working sucessfully", "pass");
//    		} else {
//
//    			writeTestResults(" Click on \"SubContractOrder\" Button", "User should not able to SubContractOrderPopup the service job order",
//    					"Cant RMA Details Page  successfully", "fail");
//    		}	

		Thread.sleep(2000);

		click(btn_closepopup);

	}

	public void btn_ServiceJobOrderInternal() throws Exception {

		Thread.sleep(4000);

		click(btn_ServiceJobOrder);

		if (isDisplayed(ServiceJobOrderPage)) {

			writeTestResults("Click on Service job order under menu",
					"Service job order should be available in the menu", "Service job order is displayed", "pass");
		} else {

			writeTestResults("Click on Service job order under menu",
					"Service job order should not available in the menu", "Service job order is not displayed", "fail");
		}

		Thread.sleep(3000);

		click(btn_NewServiceJobOrder);

		if (isDisplayed(StartNewJourneyPage)) {

			writeTestResults(" Click on \"New Service job order\" button in By page",
					"Service job order journey look up should be open",
					"Service job order journey look up is displayed", "pass");
		} else {

			writeTestResults(" Click on \"New Service job order\" button in By page",
					"Service job order journey look up should be open",
					"Service job order journey look up is not displayed", "fail");
		}

		Thread.sleep(4000);

		click(btn_ServiceOrdertoInternalDispatchOrderInternal);

		if (isDisplayed(NewServiceJobOrderPage)) {

			writeTestResults("Select \"Service Order to Internal Dispatch Order (External) \" Journey",
					"Service job order New  page should be open", "Service job order New  page is displayed", "pass");
		} else {

			writeTestResults(" Select \"Service Order to Internal Dispatch Order (External) \" Journey",
					"Service job order New  page should not be open", "Service job order New page is  not displayed",
					"fail");
		}
		LocalTime myObj = LocalTime.now();

		sendKeys(ServiceJobOrderTitle, txt_ServiceJobOrderTitle + myObj);

// 			sendKeys(ServiceJobOrderAccount,txt_ServiceJobOrderAccount);

		click(btn_accountsearch);

//		if (isDisplayed(AccountLookUPPage)) {
//
//			writeTestResults("Click account search", "AccountLookUPPage should be open",
//					"AccountLookUPPage  page is displayed", "pass");
//		} else {
//
//			writeTestResults("Click account search ", "AccountLookUPPage should not be open",
//					"AccountLookUPPage is not displayed", "fail");
//		}

		selectText(template, txt_Template);

		sendKeys(Accountname, txt_Accountname);

		pressEnter(Accountname);

		Thread.sleep(2000);

		doubleClick(btn_accountuser);

		Thread.sleep(1000);

// 			 click(btn_Productso);
// 		 
// 		    Thread.sleep(4000);
// 			 
// 			 click(btn_SerialNos);

		Thread.sleep(1000);

		selectText(ServiceGroup, "IDC-INTER DEPARTMENT JOBS");

		sendKeys(ServiceJobOrderDescription, txt_ServiceJobOrderDescription);

		Thread.sleep(2000);

		Thread.sleep(2000);

		click(btn_searchowner);

		Thread.sleep(1000);

		if (isDisplayed(OwnerLookUpPage)) {

			writeTestResults(" Click to search owner ", "OwnerLookUpPage should be open ",
					"OwnerLookUpPage viewed successfully", "pass");
		} else {

			writeTestResults(" Click to search owner ", " OwnerLookUpPage should not  be open",
					"OwnerLookUpPage not viewed successfully", "fail");
		}

		sendKeys(owner, "H V Hewa Vidana");

		pressEnter(owner);

		Thread.sleep(1000);

		doubleClick(btn_owner);

		Thread.sleep(1000);

		selectText(Priority, "High");

		Thread.sleep(1000);

		click(btn_PricingProfileSearch);

		if (isDisplayed(PricingProfileLookUpPage)) {

			writeTestResults(" Select a \"Pricing Profile\" ", " Able to select a pricing profile from the look up",
					"Able to select pricing profile successfully", "pass");
		} else {

			writeTestResults(" Select a \"Pricing Profile\" ", " Not Able to select a pricing profile from the look up",
					"Not able to select pricing profile successfully", "fail");
		}
		Thread.sleep(1000);

		doubleClick(btn_pricingprofile12);

	}

	public void btn_ServiceInvoice() throws Exception {

		Thread.sleep(2000);

		click(btn_ServiceInvoice);

		Thread.sleep(2000);

		sendKeys(ServiceInvoice, "SI/0001167");

		Thread.sleep(2000);

		pressEnter(ServiceInvoice);

		Thread.sleep(2000);

		doubleClick(btn_selectServiceInvoice);

		click(btn_action);

		Thread.sleep(2000);

		click(btn_History);

		if (isDisplayed(ServiceInvoiceReversedPage)) {

			writeTestResults("Click button History ", " User should able to view ServiceInvoice history page",
					"Able to view ServiceInvoice history page successfully", "pass");
		} else {

			writeTestResults("Click button History ", " User should able to view ServiceInvoice history page",
					"User unable to view ServiceInvoice history page successfully", "fail");
		}

		Thread.sleep(2000);

		click(btn_action);

		Thread.sleep(2000);

		click(btn_docflow);

		Thread.sleep(2000);

		if (isDisplayed(ServiceDocFlowPage)) {

			writeTestResults("Click button Docflow ", " User should able to view Docflow page",
					"Able to view Docflow page successfully", "pass");
		} else {

			writeTestResults("Click button Docflow ", " User should able to view Docflow page",
					"User unable to view Docflow page successfully", "fail");
		}

		Thread.sleep(2000);

		click(btn_action);

		Thread.sleep(2000);

		click(btn_activities);

		Thread.sleep(2000);

		if (isDisplayed(ServiceInvoiceActivitiesPage)) {

			writeTestResults("Click button Activites ", " User should able to view ServiceInvoiceActivities page ",
					"Able to view ServiceInvoiceActivities page successfully", "pass");
		} else {

			writeTestResults("Click button Activites ", " User should able to view ServiceInvoiceActivities page",
					"User unable to view ServiceInvoiceActivities page successfully", "fail");
		}

		Thread.sleep(2000);

		switchWindow();

		click("//span[@class='headerclose']");

		Thread.sleep(2000);

		click(btn_action);

		Thread.sleep(2000);

		click(btn_journal);

		Thread.sleep(2000);

		if (isDisplayed(ServiceInvoiceJournalPage)) {

			writeTestResults("Click button journal", " User should able to view ServiceInvoicejournalPage ",
					"Able to view ServiceInvoicejournalPage successfully", "pass");
		} else {

			writeTestResults("Click button journal ", " User should able to view ServiceInvoicejournalPage",
					"User unable to view ServiceInvoicejournalPage successfully", "fail");
		}

	}

	public void btn_ServiceOrder() throws Exception {

		Thread.sleep(2000);

		click("//ul[@id='header2']//a[text()='Service Order']");

		Thread.sleep(2000);
		
		selectText("//select[@id='filterListId']", "All");
		sendKeys(ServiceOrderNo, "SO/00315");

		Thread.sleep(2000);

		pressEnter(ServiceOrderNo);

		Thread.sleep(2000);

		doubleClick(btn_selectServiceOrder);

		Thread.sleep(2000);

		click(btn_action);

		Thread.sleep(2000);

		click(btn_History);
		Thread.sleep(2000);
		if (isDisplayed(ServiceInvoiceReversedPage)) {

			writeTestResults("Click button History ", " User should able to view ServiceOder history page",
					"Able to view ServiceOder history page successfully", "pass");
		} else {

			writeTestResults("Click button History ", " User should able to view ServiceOder history page",
					"User unable to view ServiceOder history page successfully", "fail");
		}

		Thread.sleep(2000);

		click(btn_action);

		Thread.sleep(2000);

		click(btn_docflow);

		Thread.sleep(2000);

		if (isDisplayed(ServiceDocFlowPage)) {

			writeTestResults("Click button Docflow ", " User should able to view Docflow page",
					"Able to view Docflow page successfully", "pass");
		} else {

			writeTestResults("Click button Docflow ", " User should able to view Docflow page",
					"User unable to view Docflow page successfully", "fail");
		}

		Thread.sleep(2000);

		click(btn_action);

		Thread.sleep(2000);

		click(btn_activities);

		Thread.sleep(2000);

		if (isDisplayed(ServiceInvoiceActivitiesPage)) {

			writeTestResults("Click button Activites ", " User should able to view ServiceInvoiceActivities page ",
					"Able to view ServiceInvoiceActivities page successfully", "pass");
		} else {

			writeTestResults("Click button Activites ", " User should able to view ServiceInvoiceActivities page",
					"User unable to view ServiceInvoiceActivities page successfully", "fail");
		}

		Thread.sleep(2000);

		switchWindow();

//    	 click(btn_CloseActivitiesPage);

		click("//span[@class='headerclose']");
//    	 
		Thread.sleep(2000);

		click(btn_action);

		Thread.sleep(2000);

//         click("");
//    	 
//    	 Thread.sleep(2000);
//    	 
//    	 if (isDisplayed(ServiceInvoiceJournalPage)) {
//
//    			writeTestResults("Click button journal", " User should able to view ServiceInvoicejournalPage ",
//    					"Able to view ServiceInvoicejournalPage successfully", "pass");
//    		} else {
//
//    			writeTestResults("Click button journal ", " User should able to view ServiceInvoicejournalPage",
//    					"User unable to view ServiceInvoicejournalPage successfully", "fail");
//    		}
//    	 

	}

	public void btn_case() throws Exception {

		Thread.sleep(2000);

		click(btn_case);

		if (isDisplayed(CasePage)) {

			writeTestResults(" Click on \"Case\" button", " User should be able to viewed casepage sucessfully ",
					"User viewed case page successfully", "pass");
		} else {

			writeTestResults("Click on \"Case\" button", " User should not able to viewed casepage sucessfully",
					"User  not viewed case page successfully", "fail");
		}

		click(btn_NewCase);

		if (isDisplayed(NewCasePage)) {

			writeTestResults(" Click on \"NewCase\" button", " User should be able to view new case page ",
					"User viewed  new case page successfully", "pass");
		} else {

			writeTestResults("Click on \"NewCase\" button", " User should not able to view new case page",
					"User not viewd the new casepage successfully", "fail");
		}

		Thread.sleep(3000);

//	click(btn_CopyFrom);

		click("//a[contains(text(),'Copy From')]");

		Thread.sleep(2000);

		sendKeys(caseno, "SIQ/000237");

		Thread.sleep(2000);

		pressEnter(caseno);

		Thread.sleep(3000);

		doubleClick(btn_selectcaseno);

		Thread.sleep(3000);

		click(btn_DraftandNewServiceJobOrder);

		Thread.sleep(2000);

	}

	public void Action_ServiceCostEstimation() throws Exception {

		click(btn_ServiceCostEstimation);

		Thread.sleep(1000);

		click(btn_NewServiceCostEstimation);

		Thread.sleep(1000);

		click(btn_copyfromCostEstimation);

		Thread.sleep(1000);

		sendKeys(ServiceCostEstimationForm, "SCE/002781");

		Thread.sleep(1000);

		pressEnter(ServiceCostEstimationForm);

		Thread.sleep(1000);

		doubleClick(SelectServiceCoseEstimation);

		Thread.sleep(1000);

		click(btn_checkoutCostEstimation);

		Thread.sleep(1000);

		click(btn_DraftandNewEmployeeAdvanceRequest);

		Thread.sleep(1000);

		Thread.sleep(1000);

		sendKeys(DescriptionService, "new");

		Thread.sleep(1000);

		click(btn_SearchCustomerServiceCE);

		Thread.sleep(1000);

		if (isDisplayed(AccountLookUPPageSE)) {

			writeTestResults("Click account search", "AccountLookUPPage should be open",
					"AccountLookUPPage  page is displayed", "pass");
		} else {

			writeTestResults("Click account search ", "AccountLookUPPage should not be open",
					"AccountLookUPPage is not displayed", "fail");
		}

		selectText(template, txt_Template);

		sendKeys(Accountname, txt_Accountname);

		pressEnter(Accountname);

		Thread.sleep(2000);

		doubleClick(btn_accountuser);

		Thread.sleep(3000);

		click(btn_searchtagno);

		Thread.sleep(3000);

		click(btn_serialno);

		click(btn_PricingProfileSearchSE);

//		if(isDisplayed(PricingProfileLookUpPage)) {
//				
//				 writeTestResults(" Select a \"Pricing Profile\" " , " Able to select a pricing profile from the look up", "Able to select pricing profile successfully", "pass");
//			}else {
//				
//				writeTestResults(" Select a \"Pricing Profile\" " , " Not Able to select a pricing profile from the look up", "Not able to select pricing profile successfully", "fail");
//			}
		Thread.sleep(1000);

		sendKeys(pricingprofileSE, "Service");

		pressEnter(pricingprofileSE);

		Thread.sleep(1000);

		doubleClick("//*[@id=\"g1198-t\"]/table/tbody/tr[2]/td[3]");

		Thread.sleep(2000);

		click(btn_EstimationDetails);

		Thread.sleep(2000);

		click(btn_searchserviceproductfirst);

		if (isDisplayed(ProductLookUpPage)) {

			writeTestResults(" Click search to product", "Should be able to view product look up page",
					"product look up page viewed successfully", "pass");
		} else {

			writeTestResults(" Click search to product", "Should not able to view product look up page",
					"product look up page not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, "SP-test-001");

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(2000);

//				selectText(warehouse1, "WH-test-001-Allo[Rashi-test-auto]");
		selectText("/tr[@class='set-selected-row']//td[11]", txt_ToWareHouse);
//				
		Thread.sleep(3000);

		sendKeys(EstimatedQtyservice, "5");

		click(btn_billable1);

		Thread.sleep(1000);

		click(btn_expand2);

		Thread.sleep(1000);

		click(btn_searchbatchproduct2);

		if (isDisplayed(ProductLookUpPage)) {

			writeTestResults(" Click search to product", "Should be able to view product look up page",
					"product look up page viewed successfully", "pass");
		} else {

			writeTestResults(" Click search to product", "Should not able to view product look up page",
					"product look up page not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, txt_BatchSpecificProduct);

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(2000);

		click(btn_expand2);

		selectText("//*[@id=\"commonTbl\"]/tbody/tr[3]/td[6]/select", "Resource");

		Thread.sleep(2000);

		click(btn_searchResourcesReferenceCode3);

		if (isDisplayed(ResourceLookUpPage)) {

			writeTestResults(" Click search to resource", "Should be able to view resource look up page",
					"resource look up page viewed successfully", "pass");
		} else {

			writeTestResults(" Click search to resourse", "Should not able to view resource look up page",
					"resource look up page not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

//						 sendKeys(Resource,txt_Resource);

		sendKeys(Resource, "REC-test-001");

		Thread.sleep(2000);

		pressEnter(Resource);

		Thread.sleep(3000);

		doubleClick(btn_Resource);

		Thread.sleep(2000);

		click(btn_searchResources3);

		if (isDisplayed(ProductLookUpPage)) {

			writeTestResults(" Click search to product", "Should be able to view product look up page",
					"product look up page viewed successfully", "pass");
		} else {

			writeTestResults(" Click search to product", "Should not able to view product look up page",
					"product look up page not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, txt_serviceProduct);

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(2000);

		sendKeys("//*[@id=\"commonTbl\"]/tbody/tr[3]/td[14]/input", "5");

		Thread.sleep(3000);

		click(btn_expand3);

		selectText("//*[@id=\"commonTbl\"]/tbody/tr[4]/td[6]/select", "Labour");

		click("//*[@id=\"commonTbl\"]/tbody/tr[4]/td[8]/span");

//							if(isDisplayed(LabourOwnerLookUpPage)) {
//								
//								 writeTestResults(" Click search to Employee" , "Should be able to view owner look up page", "Owner look up page viewed successfully", "pass");
//							}else {
//								
//								writeTestResults(" Click search to Employee"  , "Should not able to view owner look up page", "Owner look up page not  viewed successfully", "fail");
//							}

		Thread.sleep(3000);

//							 selectText(template,txt_Template);

		sendKeys(LabourEmployee, "rashi");

		Thread.sleep(2000);

		pressEnter(LabourEmployee);

		Thread.sleep(3000);

		doubleClick(btn_LabourEmployee);

		Thread.sleep(2000);

		click("//*[@id=\"commonTbl\"]/tbody/tr[4]/td[9]/span");

//							if(isDisplayed(ProductLookUpPage)) {
//								
//								 writeTestResults(" Click search to product" , "Should be able to view product look up page", "product look up page viewed successfully", "pass");
//							}else {
//								
//								writeTestResults(" Click search to product"  , "Should not able to view product look up page", "product look up page not  viewed successfully", "fail");
//							}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, txt_serviceProduct);

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(2000);

		// sendKeys(EstimatedQtyLabour, txt_EstimatedQtyLabour);
		sendKeys("//*[@id=\"commonTbl\"]/tbody/tr[4]/td[14]/input", "5");

		Thread.sleep(2000);

		click(btn_expand4);

		selectText("//*[@id=\"commonTbl\"]/tbody/tr[5]/td[6]/select", "Services");

		click("//*[@id=\"commonTbl\"]/tbody/tr[5]/td[9]/span");

		if (isDisplayed(ProductLookUpPage)) {

			writeTestResults(" Click search to product", "Should be able to view product look up page",
					"product look up page viewed successfully", "pass");
		} else {

			writeTestResults(" Click search to product", "Should not able to view product look up page",
					"product look up page not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, txt_serviceProduct);

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(2000);

//								sendKeys(EstimatedQtyservice, txt_EstimatedQtyservice);
		sendKeys("//*[@id=\"commonTbl\"]/tbody/tr[5]/td[14]/input", "5");

		Thread.sleep(1000);

		sendKeys("//*[@id=\"commonTbl\"]/tbody/tr[5]/td[16]/input", "100");

		Thread.sleep(2000);

		click(btn_expand5);

		Thread.sleep(1000);

		selectText("//*[@id=\"commonTbl\"]/tbody/tr[6]/td[6]/select", "Expense");

		Thread.sleep(1000);

		selectText("//*[@id=\"commonTbl\"]/tbody/tr[6]/td[8]/select", txt_Referencecode);

		click("//*[@id=\"commonTbl\"]/tbody/tr[6]/td[9]/span");

		Thread.sleep(1000);

		if (isDisplayed(ProductLookUpPage)) {

			writeTestResults(" Click search to product", "Should be able to view product look up page",
					"product look up page viewed successfully", "pass");
		} else {

			writeTestResults(" Click search to product", "Should not able to view product look up page",
					"product look up page not  viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Product, txt_serviceProduct);

		Thread.sleep(2000);

		Thread.sleep(2000);

		pressEnter(Product);

		Thread.sleep(3000);

		doubleClick(btn_product);

		Thread.sleep(2000);

		sendKeys("//*[@id=\"commonTbl\"]/tbody/tr[6]/td[16]/input", "200");

		Thread.sleep(1000);

//			 click(btn_Summary);

		click("//*[@id=\"#divTbSummary\"]");

		Thread.sleep(3000);

		switchWindow();

//			click(btn_checkoutServiceCostEstimation);
		click("//*[@id=\"divTbSummary\"]/div[2]/span");

		Thread.sleep(2000);

		click(btn_DraftEmployeeAdvanceRequest);

		Thread.sleep(2000);

		if (isDisplayed(DraftedHireAgreement)) {

			writeTestResults(" Click on \"Draft\" button",
					"User should be able to Click on \"Draft\" button and document should be draft",
					"User drafted the ServiceCostEstimation  successfully", "pass");

		} else {

			writeTestResults(" Click on \"Draft\" button",
					"User should not be able to Click on \"Draft\" button and document should be draft",
					"User not drafted the ServiceCostEstimation successfully", "fail");
		}

		Thread.sleep(5000);

		click(btn_ReleaseServiceProductReg);

		Thread.sleep(4000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleasedHireAgreement)) {

			writeTestResults(" Click on \"Release\" button",
					" User should be able to Click on \"Release\" button and document should be Released",
					"User released the ServiceCostEstimation successfully", "pass");
		} else {

			writeTestResults("Click on \"Release\" button",
					" User should be able to Click on \"Release\" button and document should be Released",
					"User not released the ServiceCostEstimation successfully", "fail");
		}

		Thread.sleep(2000);

		click(btn_duplicateServiceCostEstimation);

		Thread.sleep(2000);

//		click("//*[@id=\"divTbSummary\"]/div[2]/span");
//
//		Thread.sleep(2000);

		click("//*[@id=\"btnCalc2\"]");

		Thread.sleep(2000);

		if (isDisplayed(DraftedHireAgreement)) {

			writeTestResults(" Click on \"Draft\" button",
					"User should be able to Click on \"Draft\" button and document should be draft",
					"User drafted the ServiceCostEstimation  successfully", "pass");

		} else {

			writeTestResults(" Click on \"Draft\" button",
					"User should not be able to Click on \"Draft\" button and document should be draft",
					"User not drafted the ServiceCostEstimation successfully", "fail");
		}

		Thread.sleep(5000);

		click("//*[@id=\"permissionBar\"]/a[2]");

		Thread.sleep(4000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleasedHireAgreement)) {

			writeTestResults(" Click on \"Release\" button",
					" User should be able to Click on \"Release\" button and document should be Released",
					"User released the ServiceCostEstimation successfully", "pass");
		} else {

			writeTestResults("Click on \"Release\" button",
					" User should be able to Click on \"Release\" button and document should be Released",
					"User not released the ServiceCostEstimation successfully", "fail");
		}

	}

	public void Action_ServiceHireagreement() throws Exception {

		Thread.sleep(1000);

		click(btn_HireAgreement);

		click(btn_NewHireAgreement);

		if (isDisplayed(NewHireAgreementpage)) {

			writeTestResults(" Click on the New Hire Agreement Button",
					" User should be able to click on the New Hire Agreement Button",
					" User viewed New Hire Agreement form successfully", "pass");
		} else {

			writeTestResults("  Click on the New Hire Agreement Button",
					" User should not be able to click on the New Hire Agreement Button",
					"  User not viewed New Hire Agreement form successfully", "fail");
		}

		Thread.sleep(1000);

		click(btn_CopyFromHireAgreement);

		sendKeys(HireAgreementNo, "ser00111:52:53.590");

		Thread.sleep(1000);

		pressEnter(HireAgreementNo);

		Thread.sleep(1000);

		doubleClick(btn_selectHireagreement);

		Thread.sleep(1000);

		LocalTime myObj = LocalTime.now();

		sendKeys(AgreementNo, txt_AgreementNo + myObj);

//   			String HireAgreeNo = getText(AgreementNo);
//   			
//   			System.out.println(HireAgreeNo);

		sendKeys(TitleAgreement, "new");

//   			 click(selectagreementdate);

		selectText(Contractgroup, "Common");

		click(btn_agreementdate);

		click(btn_beforedate);

		Thread.sleep(3000);

		click(btn_beforedate);

		Thread.sleep(3000);

//   			click(btn_beforedate);

		Thread.sleep(3000);

		click(selectagreementdate);

		Thread.sleep(2000);

		click(btn_EffectiveFrom);

		click(btn_beforedate);

		Thread.sleep(1000);

		click(btn_EffectiveFromDate);

		click(btn_EffectiveTo);

		Thread.sleep(1000);

//   			click(btn_backInternalReturnOrder);

		Thread.sleep(1000);

		click(btn_EffectiveToDate);

		Thread.sleep(1000);

		click(btn_draftnewHireAgreement);

		LocalTime myObj1 = LocalTime.now();

		sendKeys(AgreementNo, txt_AgreementNo + myObj1);

//		String HireAgreeNo = getText(AgreementNo);
//		
//		System.out.println(HireAgreeNo);

		sendKeys(TitleAgreement, "new");

//		 click(selectagreementdate);

		selectText(Contractgroup, "Common");

		click(btn_agreementdate);

		click(btn_beforedate);

		Thread.sleep(3000);

		click(btn_beforedate);

		Thread.sleep(3000);

//		click(btn_beforedate);

		Thread.sleep(3000);

		click(selectagreementdate);

		Thread.sleep(2000);

		click(btn_EffectiveFrom);

		click(btn_beforedate);

		Thread.sleep(1000);

		click(btn_EffectiveFromDate);

		click(btn_EffectiveTo);

		Thread.sleep(1000);

//		click(btn_backInternalReturnOrder);

		Thread.sleep(1000);

		click(btn_EffectiveToDate);

		Thread.sleep(1000);

		sendKeys(DescriptionHireAgreement, "new test");

		Thread.sleep(1000);

		selectText(salesunitHireAgreement, "test");

		Thread.sleep(2000);

		selectText(HireBasis, "Units");

		Thread.sleep(2000);

		click(btn_accountsearchHireAgreement);

//		if(isDisplayed(AccountLookUPPage)) {
//			
//			 writeTestResults("Click account search", "AccountLookUPPage should be open", "AccountLookUPPage  page is displayed", "pass");
//		}else {
//			
//			writeTestResults("Click account search ", "AccountLookUPPage should not be open", "AccountLookUPPage is not displayed", "fail");
//		}
//		
		selectText(template, txt_Template);

		sendKeys(Accountname, "Rashi");

		pressEnter(Accountname);

		Thread.sleep(2000);

		doubleClick(btn_accountuser);

		Thread.sleep(2000);

		click(btn_SearchSceduleCodeHireAgreement);

		Thread.sleep(2000);

		sendKeys(SceduleCode, "TESTDAILY2");

//			sendKeys(SceduleCode,"newauto");

		pressEnter(SceduleCode);

		Thread.sleep(3000);

		doubleClick(btn_SceduleCode);

		Thread.sleep(2000);

		click(btn_responsibleuserHireAgreement);

		Thread.sleep(1000);

		selectText(template, txt_Template);

		sendKeys(responsibleuser, "Nihal Peiris");

		Thread.sleep(2000);

		pressEnter(responsibleuser);

		Thread.sleep(3000);

		doubleClick(btn_responsibleusername);

		Thread.sleep(2000);

		click(btn_ResourceInformation);

		if (isDisplayed(RecourceInformationPage)) {

			writeTestResults("Go to \"Resource Information\" tab",
					"User should be able to Go to \"Resource Information\" tab",
					"User able to Go to \"Resource Information\" tab successfully", "pass");

		} else {

			writeTestResults("Go to \"Resource Information\" tab ",
					"User should not be able to Go to \"Resource Information\" tab",
					"User not able to Go to \\\"Resource Information\\\" tab successfully", "fail");

		}

		click(btn_searchbtnReferncecode);

		if (isDisplayed(ReferncecodePage)) {

			writeTestResults(" Click on search icon in the \" Referance Code\" column",
					"User should be able to Click on search icon in the \" Referance Code\" column",
					"ReferncecodePage viewed successfully", "pass");
		} else {

			writeTestResults(" Click on search icon in the \" Referance Code\" column",
					"User should not be able to Click on search icon in the \" Referance Code\" column",
					"ReferncecodePage not viewed successfully", "fail");
		}

		Thread.sleep(3000);

		selectText(template, txt_Template);

		sendKeys(Referncecode, "HireAgResource [HireAgResource]");

		Thread.sleep(2000);

		pressEnter(Referncecode);

		Thread.sleep(3000);

		doubleClick(btn_Referncecode);

		Thread.sleep(3000);

		sendKeys(openingunit, "1");

		Thread.sleep(1000);

		sendKeys(Minimumunit, "1");

		click(btn_RateRatio);

		if (isDisplayed(RateRangeSetup)) {

			writeTestResults(" Click on \"Rate Ratio\" icon",
					"User should be able to Click on search icon in the \" Referance Code\" column",
					"ReferncecodePage viewed successfully", "pass");
		} else {

			writeTestResults(" Click on \"Rate Ratio\" icon",
					"User should not be able to Click on search icon in the \" Referance Code\" column",
					"ReferncecodePage not viewed successfully", "fail");
		}

		sendKeys(Start1, "1");

		sendKeys(End1, "10");

		sendKeys(Rate1, "100");

		Thread.sleep(1000);

		click(btn_plusRR);

		Thread.sleep(1000);

		sendKeys(Start2, "11");

		sendKeys(End2, "0");

		sendKeys(Rate2, "200");

		Thread.sleep(1000);

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript(btn_applyCommon);

		Thread.sleep(1000);

		click(btn_draftHireAgreement);

		if (isDisplayed(DraftedHireAgreement)) {

			writeTestResults(" Click on \"Draft\" button",
					"User should be able to Click on \"Draft\" button and document should be draft",
					"User drafted the HireAgreement successfully", "pass");
		} else {

			writeTestResults(" Click on \"Draft\" button",
					"User should not be able to Click on \"Draft\" button and document should be draft",
					"User not drafted the HireAgreement successfully", "fail");
		}

		Thread.sleep(4000);

		click(btn_ReleaseHireAgreement);

		Thread.sleep(5000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleasedHireAgreement)) {

			writeTestResults(" Click on \"Release\" button",
					" User should be able to Click on \"Release\" button and document should be Released",
					"User released the HireAgreement successfully", "pass");
		} else {

			writeTestResults("Click on \"Release\" button",
					" User should be able to Click on \"Release\" button and document should be Released",
					"User not released the HireAgreement successfully", "fail");
		}

		Thread.sleep(3000);

		click(btn_action);

//		click(btn_ActualUpdate);
//    		
//		if (isDisplayed(actualupdatepage)) {
//
//			writeTestResults(" Click on \"Actual Update\" Action", "  Actual Update Action should be Available",
//					" Actual Update Action should be viewed successfully", "pass");
//		} else {
//
//			writeTestResults("  Click on \"Actual Update\" Action", "  Actual Update Action should not Available",
//					" Actual Update Action not viewed successfully", "fail");
//		}

		Thread.sleep(3000);

		click("//*[@id=\"inspect\"]/div[7]/div[1]/span[2]");

	}

	public void Action_ServiceGroupConfiguration() throws Exception {

		Thread.sleep(1000);

		click(btn_ServiceGroupconfig);

		Thread.sleep(1000);

		if (isDisplayed(ServiceGroupconfigpage)) {

			writeTestResults(" Click on \"ServiceGroupconfig\" button", "  Service configuration button shoud be work",
					"Service configuration page should be viewed successfully", "pass");
		} else {

			writeTestResults("  Click on \"ServiceGroupconfig\" button", "  Service configuration button shoud be work",
					" Service configuration page should not  viewed successfully", "fail");
		}

		click(btn_newServiceConfiguration);

		Thread.sleep(1000);

		if (isDisplayed(NewServiceGroupconfigpage)) {

			writeTestResults(" Click on \"NewServiceGroupconfigpage\" button",
					" NewServiceGroupconfig button shoud be work",
					"Service configuration page should be viewed successfully", "pass");
		} else {

			writeTestResults("  Click on \"NewServiceGroupconfigpage\" button",
					"  NewServiceGroupconfig  button shoud be work",
					" Service configuration page should not  viewed successfully", "fail");
		}

// 		selectText(SelectServiceGroup, "IDC-INTER DEPARTMENT JOBS");
// 		
// 		Thread.sleep(1000);
// 		
// 		sendKeys(Doctitle, txt_Doctile);

		click(btn_copyfromServiceGroupConfiguaration);

		Thread.sleep(1000);

		click(btn_searchServiceGroupCongig);

// 		sendKeys(serviceConfig, "WSC-WORK SHOP JOBS");

//		pressEnter(serviceConfig);
// 		
// 		Thread.sleep(2000);
// 		
		doubleClick(btn_specificServiceConfig);

		Thread.sleep(2000);

		LocalTime myObj = LocalTime.now();

		sendKeys(Doctitle, txt_Doctile + myObj);

		Thread.sleep(2000);

		click(btn_DraftandNewServiceCongig);

		Thread.sleep(2000);

	}

	public void Action_ServiceGroupConfigurationDraft() throws Exception {

		Thread.sleep(1000);

		click(btn_ServiceGroupconfig);

		Thread.sleep(1000);

		if (isDisplayed(ServiceGroupconfigpage)) {

			writeTestResults(" Click on \"ServiceGroupconfig\" button", "  Service configuration button shoud be work",
					"Service configuration page should be viewed successfully", "pass");
		} else {

			writeTestResults("  Click on \"ServiceGroupconfig\" button", "  Service configuration button shoud be work",
					" Service configuration page should not  viewed successfully", "fail");
		}

		click(btn_newServiceConfiguration);

		Thread.sleep(1000);

		if (isDisplayed(NewServiceGroupconfigpage)) {

			writeTestResults(" Click on \"NewServiceGroupconfigpage\" button",
					" NewServiceGroupconfig button shoud be work",
					"Service configuration page should be viewed successfully", "pass");
		} else {

			writeTestResults("  Click on \"NewServiceGroupconfigpage\" button",
					"  NewServiceGroupconfig  button shoud be work",
					" Service configuration page should not  viewed successfully", "fail");
		}

		Thread.sleep(1000);

		selectText(SelectServiceGroup, "IDC-INTER DEPARTMENT JOBS");

		Thread.sleep(1000);

		LocalTime myObj = LocalTime.now();

		sendKeys(Doctitle, txt_Doctile + myObj);

		Thread.sleep(2000);

		click(btn_searchEmployeeHiearchy);

		Thread.sleep(2000);

		sendKeys(searchhiearchy, "H1");

		pressEnter(searchhiearchy);

		Thread.sleep(2000);

		doubleClick(btn_specificsearchhiearchy);

		Thread.sleep(2000);

		click(btn_draftSericeQuotation);

		Thread.sleep(2000);

		if (isDisplayed(DraftedServiceOrderNewPage)) {

			writeTestResults("Click on \"Draft\" Button", "User should be able to Draft the ServiceGroupConfiguration",
					"ServiceGroupConfiguration Drafted successfully", "pass");
		} else {

			writeTestResults(" Click on \"(Draft\" Button",
					"User should not able to Draft the ServiceGroupConfiguration",
					"ServiceGroupConfiguration not  Drafted successfully", "fail");

		}

		Thread.sleep(5000);

		click(btn_ReleaseServiceOrder1);

		Thread.sleep(5000);

		trackCode = getText("//*[@id=\"lblTemplateFormHeader\"]");

		if (isDisplayed(ReleaseSericeQuotationNewPage)) {

			writeTestResults("Click on \"Release\" Button",
					"User should be able to Release the ServiceGroupConfiguration",
					"ServiceGroupConfiguration Released successfully", "pass");

		} else {

			writeTestResults(" Click on \"Release\" Button",
					"User should not able toRelease the ServiceGroupConfiguration",
					"ServiceGroupConfiguration not  Released successfully", "fail");

		}

	}

	public void Action_ServiceControl() throws Exception {

		Thread.sleep(1000);

		click(btn_ServiceControl);

		Thread.sleep(1000);

		if (isDisplayed(ServiceControlpage)) {

			writeTestResults(" Click on \"ServiceControl\" button", "  ServiceControl button shoud be work",
					"ServiceControl page should be viewed successfully", "pass");
		} else {

			writeTestResults("  Click on \"ServiceControl\" button", " ServiceControl button shoud be work",
					" ServiceControl page should not  viewed successfully", "fail");
		}

		Thread.sleep(1000);

		click(btn_casedetails);

		Thread.sleep(1000);

		click(btn_gernerateServiceOrder);

		Thread.sleep(3000);
	}

	public void ActionBankAdjustment() throws Exception {

		click(btn_Finance);

		if (isDisplayed(FinanceSubSideMenu)) {

			writeTestResults(" Click on Finance Module", "  User should be able to click on the Finance modle ",
					"user able to click finance module successfully", "pass");
		} else {

			writeTestResults(" Click on Finance Module", "  User should be not able to click on the Finance modle ",
					"user  not able to click finance module successfully", "fail");
		}

		Thread.sleep(1000);

		click(btn_BankAdjustment);

//    	 			 if(isDisplayed(BankAdjustmentPage)) {
//    	 					
//    	 				 writeTestResults(" Click on Bank Adjustment form", "  User should be able to click on the New bank adjustment button ", "user able to click New bank adjustment successfully", "pass");
//    	 			}else {
//    	 				
//    	 				writeTestResults(" Click on Bank Adjustment form", " User should notable to click on the New bank adjustment button ", "user  not able to click New bank adjustment successfully", "fail");
//    	 			} 

		click(btn_NewBankAdjustment);

		Thread.sleep(1000);

//    	 		click(btn_CopyfromBankAdjustment);

		click("//a[contains(text(),'Copy From')]");

		Thread.sleep(2000);

		sendKeys(BankAdjustment, "BA/05943");

		Thread.sleep(1000);

		pressEnter(BankAdjustment);

		doubleClick(btn_selectBankAdjustment);

		Thread.sleep(1000);

		click(btn_draftandnewBankAdjustment);

		Thread.sleep(1000);

	 			 if(isDisplayed("//div[@id='divHeader']//a[contains(text(),'Draft & New')]")) {
					
				 writeTestResults(" Click on Draft and New button", "  User should be able to click on the Draft and New button button ", "user able to click New bank adjustment successfully", "pass");
			
	 			 }else {
				
				writeTestResults(" Click on Draft and New button", " User should notable to click on the Draft and New button button ", "user  not able to click New bank adjustment successfully", "fail");
			} 
//     
		Thread.sleep(1000);

		click("//a[contains(text(),'Copy From')]");

		Thread.sleep(2000);

		sendKeys(BankAdjustment, "BA/05943");

		Thread.sleep(1000);

		pressEnter(BankAdjustment);

		doubleClick(btn_selectBankAdjustment);

		Thread.sleep(3000);

		click("//div[@id='permissionBar']//a[text()='Draft']");

		if (isDisplayed(DraftedBankAdjustmentPage)) {

			writeTestResults(" Click on Draft button", "  User should be able to click on the Draft button ",
					"User able to click Drafted bank adjustment successfully", "pass");

		} else {

			writeTestResults(" Click on Draft button", " User should notable to click on the Draft button ",
					"User  not able to click Drafted bank adjustment successfully", "fail");
		}

		Thread.sleep(1000);

		click(btn_ReleaseBankAdjustmentPage);

		Thread.sleep(1000);

		if (isDisplayed(ReleasedBankAdjustmentPage)) {

			writeTestResults(" Click on Release button", "  User should be able to click on the Release button ",
					"User able to click Released bank adjustment successfully", "pass");

		} else {

			writeTestResults(" Click on Release button", " User should notable to click on the Release button ",
					"User  not able to click Released bank adjustment successfully", "fail");
		}

	}

	public void ActionVerifyPettyCash() throws Exception {

		click(btn_Finance);

		if (isDisplayed(FinanceSubSideMenu)) {

			writeTestResults(" Click on Finance Module", "  User should be able to click on the Finance modle ",
					"user able to click finance module successfully", "pass");
		} else {

			writeTestResults(" Click on Finance Module", "  User should be not able to click on the Finance modle ",
					"user  not able to click finance module successfully", "fail");
		}

		click(btn_PettyCash);

		Thread.sleep(3000);

		if (isDisplayed(PettyCashPage)) {

			writeTestResults(" Click on Petty Cash form",
					"   User should be able to click on the New Petty cash form and petty cash form should be open in a new tab ",
					"user able to click on the New Petty cash form and petty cash form should be open in a new tab successfully",
					"pass");
		} else {

			writeTestResults(" Click on Petty Cash form",
					"   User should be able to click on the New Petty cash form and petty cash form should be open in a new tab ",
					"user  not able to click on the New Petty cash form and petty cash form should be open in a new tab successfully",
					"fail");
		}

		click(btn_NewPettyCash);

		Thread.sleep(1000);

		click(btn_CopyfromPettyCash);

		Thread.sleep(1000);

		sendKeys(pettycashName, "PC/19/003995");

		Thread.sleep(1000);

		pressEnter(pettycashName);

		Thread.sleep(1000);

		doubleClick("//span[@class='pic16 pic16-act-release']");

		Thread.sleep(1000);

		click(btn_draftandnewBankAdjustment);

		Thread.sleep(1000);

		click(btn_CopyfromPettyCash);

		Thread.sleep(1000);

		sendKeys(pettycashName, "PC/19/003995");

		Thread.sleep(1000);

		pressEnter(pettycashName);

		Thread.sleep(1000);

		doubleClick("//span[@class='pic16 pic16-act-release']");

		Thread.sleep(1000);

		click(btn_draftpettycash);

		Thread.sleep(1000);

		if (isDisplayed(DraftedPettyCashPage)) {

			writeTestResults(" Click on Draft button", "   User should be able to click on the Draft button ",
					"user able to view drafted pettycash page successfully", "pass");
		} else {

			writeTestResults(" Click on Draft button", "User should be able to click on the Draft button ",
					"user unable to view drafted pettycash page successfully", "fail");
		}

	}

	public void Action_VendorRefund() throws Exception {

		click(btn_Finance);

		if (isDisplayed(FinanceSubSideMenu)) {

			writeTestResults(" Click on Finance Module", "  User should be able to click on the Finance modle ",
					"user able to click finance module successfully", "pass");
		} else {

			writeTestResults(" Click on Finance Module", "  User should be not able to click on the Finance modle ",
					"user  not able to click finance module successfully", "fail");
		}

		Thread.sleep(1000);

		click(btn_InboundPaymentAdvice);

		Thread.sleep(1000);

		click(btn_NewInboundPaymentAdvice);

		click(btn_VendorRefund);

		Thread.sleep(1000);

		click(btn_copyfromInboundPaymentAdvice);

		Thread.sleep(1000);

		sendKeys(InboundAdviseNo, "IPA/22017");

		pressEnter(InboundAdviseNo);

		doubleClick(selectInboundAdvice);

		Thread.sleep(1000);
//
//    	 		click(btn_searchvendorInboundPaymentAdvice);
//
//    	 		selectText(template, txt_Template);
//
//    	 		sendKeys(searchvendor, txt_searchvendor);
//
//    	 		pressEnter(searchvendor);
//
//    	 		Thread.sleep(3000);
//
//    	 		doubleClick(btn_vendor);
//
//    	 		Thread.sleep(2000);
//
//    	 		selectText(AnalysisCode, "DUTY");
//
//    	 		Thread.sleep(1000);
//
//    	 		sendKeys(AmountInboundPaymentAdvice, "100");
//
//    	 		Thread.sleep(1000);
//
//    	 		click(btn_CostAllocation2);
//
//    	 		if (isDisplayed(CostAllocationPopUp)) {
//
//    	 			writeTestResults(" Click on Cost allocation Icon", "  Cost allocation look up should be open  ",
//    	 					" Cost allocation look up viewed successfully", "pass");
//    	 		} else {
//
//    	 			writeTestResults(" Click on Cost allocation Icon", "  Cost allocation look up should not open  ",
//    	 					"Cost allocation look up not viewed successfully ", "fail");
//    	 		}
//
//    	 		Thread.sleep(1000);
//
//    	 		sendKeys(Percentage, "100");
//
//    	 		Thread.sleep(1000);
//    	 		
//    	 		selectText(CostAssignmentType, "Service Job Order");
//
//    	 		Thread.sleep(1000);
//
//    	 		click(btn_SearchCostObject);
//
//    	 		Thread.sleep(1000);
//
//    	 		selectText(templateJOBNO, "My Template");
//    	 		
//    	 	    Thread.sleep(2000);
//    	 		
//    	 		click("//b[contains(text(),'New Service Job Order')]");
//
//
//    	 		Thread.sleep(4000);
//
//    	 		click(btn_ServiceOrdertoInternalDispatchOrderExternal);
//
//    	 		if (isDisplayed(NewServiceJobOrderPage)) {
//
//    	 			writeTestResults("Select \"Service Order to Internal Dispatch Order (External) \" Journey",
//    	 					"Service job order New  page should be open", "Service job order New  page is displayed", "pass");
//    	 		} else {
//
//    	 			writeTestResults(" Select \"Service Order to Internal Dispatch Order (External) \" Journey",
//    	 					"Service job order New  page should not be open", "Service job order New page is  not displayed",
//    	 					"fail");
//    	 		}
//    	 		
//    	 		switchWindow();
//    	 		
//    	 		LocalTime myObj = LocalTime.now();
//
//    	 		sendKeys(ServiceJobOrderTitle, txt_ServiceJobOrderTitle + myObj);
//
//    	 		click(btn_accountsearch);
//
//    	 		if (isDisplayed(AccountLookUPPage)) {
//
//    	 			writeTestResults("Click account search", "AccountLookUPPage should be open",
//    	 					"AccountLookUPPage  page is displayed", "pass");
//    	 		} else {
//
//    	 			writeTestResults("Click account search ", "AccountLookUPPage should not be open",
//    	 					"AccountLookUPPage is not displayed", "fail");
//    	 		}
//
//    	 		selectText(template, txt_Template);
//
//    	 		sendKeys(Accountname, txt_Accountname);
//
//    	 		pressEnter(Accountname);
//
//    	 		Thread.sleep(2000);
//
//    	 		doubleClick(btn_accountuser);
//
//    	 		Thread.sleep(1000);
//
//
//    	 		selectText(ServiceGroup, "IDC-INTER DEPARTMENT JOBS");
//
//    	 		sendKeys(ServiceJobOrderDescription, txt_ServiceJobOrderDescription);
//
//    	 		Thread.sleep(2000);
//
//    	 		click(btn_searchowner);
//
//    	 		Thread.sleep(1000);
//
//    	 		if (isDisplayed(OwnerLookUpPage)) {
//
//    	 			writeTestResults(" Click to search owner ", "OwnerLookUpPage should be open ",
//    	 					"OwnerLookUpPage viewed successfully", "pass");
//    	 		} else {
//
//    	 			writeTestResults(" Click to search owner ", " OwnerLookUpPage should not  be open",
//    	 					"OwnerLookUpPage not viewed successfully", "fail");
//    	 		}
//
//    	 		sendKeys(owner, "H V Hewa Vidana");
//
//    	 		pressEnter(owner);
//
//    	 		Thread.sleep(1000);
//
//    	 		doubleClick(btn_owner);
//
//    	 		Thread.sleep(1000);
//
//    	 		selectText(Priority, "High");
//
//    	 		Thread.sleep(1000);
//
//    	 		click(btn_PricingProfileSearch);
//
//    	 		if (isDisplayed(PricingProfileLookUpPage)) {
//
//    	 			writeTestResults(" Select a \"Pricing Profile\" ", " Able to select a pricing profile from the look up",
//    	 					"Able to select pricing profile successfully", "pass");
//    	 		} else {
//
//    	 			writeTestResults(" Select a \"Pricing Profile\" ", " Not Able to select a pricing profile from the look up",
//    	 					"Not able to select pricing profile successfully", "fail");
//    	 		}
//    	 		Thread.sleep(1000);
//
//    	 		doubleClick(btn_pricingprofile12);

	}

	public void CreateNewEmployees() throws Exception {

		click(btn_navigationmenu);

		if (isDisplayed(sidemenu)) {
			writeTestResults("Click on the top navigation menu", "System should be loaded the menu item successfully",
					"Menu item is displayed", "pass");
		} else {
			writeTestResults("Click on the top navigation menu", "System should not  loaded the menu item successfully",
					"Menu item is not displayed", "fail");
		}

		click(btn_OrganizationManagement);

		click(btn_EmployeeInformation);

		click(btn_NewEmployee);

		Thread.sleep(3000);

		LocalTime myObj = LocalTime.now();
		sendKeys(employeecode, txt_employeecode + myObj);

		no = txt_employeecode + myObj;
		System.out.println(no);

		// EmployeeCode = getText(employeecode);

		sendKeys(employeename, txt_employeename);

		getText(employeecode);

		sendKeys(namewithintials, txt_namewithintials);

		selectText(employeegroup, txt_employeegroup);

		Thread.sleep(1000);

		click(btn_searchworkingcalander);

		Thread.sleep(1000);

		sendKeys(workingcalender, "WC_Resources");

		pressEnter(workingcalender);

		Thread.sleep(1000);

		doubleClick(workingcalendernew);

		click(btn_searchrateprofile);

		Thread.sleep(1000);

//    	 			 pressEnter(btn_ratesearch);

		sendKeys(rateprofile, "600");

		Thread.sleep(1000);

		pressEnter(rateprofile);

		doubleClick(rateprofilevalue);

		click(payableaccount);

		Thread.sleep(1000);

		click(btn_DraftIO);

		Thread.sleep(2000);

		click(btn_ReleaseEmployee);

		Thread.sleep(2000);
	}

	public void CreateNewWarrantyProfile() throws Exception {

		click(btn_navigationmenu);

		if (isDisplayed(sidemenu)) {
			writeTestResults("Click on the top navigation menu", "System should be loaded the menu item successfully",
					"Menu item is displayed", "pass");
		} else {
			writeTestResults("Click on the top navigation menu", "System should not  loaded the menu item successfully",
					"Menu item is not displayed", "fail");
		}

		click(btn_SalesMarkting);

		Thread.sleep(2000);

		click(btn_WarrantyProfile);

		Thread.sleep(2000);

		click(Newbtn_WarrantyProfile);

		LocalTime myObj1 = LocalTime.now();

		sendKeys(profilecode, "new001" + myObj1);

		sendKeys(profilename, "new");

		Thread.sleep(2000);

		click(btn_DraftWarrantyProfile);

		Thread.sleep(2000);

	}

	/* common wait untill displayed */
	public void waitUntillQuickIsDisplayed(String locatior, int rounds) throws Exception {
		int count = 0;
		while ((!isDisplayedQuickCheck(locatior)) || count >= rounds) {
			Thread.sleep(1000);
		}
	}

	/* common quick is displayed */
	public boolean isDisplayedQuickCheck(String locator) throws Exception {
		boolean found = false;

		try {
			if (driver.findElement(getLocator(locator)).isDisplayed()) {
				found = true; // FOUND IT
			}
		} catch (Exception e) {
		}
		return found;
	}
}
