package bileeta.BTAF.PageObjects;

import java.time.LocalTime;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import bileeta.BATF.Pages.FixedAssetData;

public class FixedAsset extends FixedAssetData {

	public static String segmentNo;

	public static String taskId;

	public static String projectCodeNo;

	public static String binCardNo;

	public static String docValue;

	public static String stockValue;

	public static String fxInfoNo;

	public void navigateToTheLoginPage() throws Exception {
		openPage(siteURL);

		writeTestResults("Verify that 'Entution' header is available on the page",
				"user should be able to see the logo", "logo is displayed", "pass");

	}

	public void verifyTheLogo() throws Exception {
		
		WaitElement(siteLogo);
		if (isDisplayed(siteLogo)) {

			writeTestResults("Verify that 'Entution' header is available on the page",
					"user should be able to see the logo", "logo is displayed", "pass");
		} else {
			writeTestResults("Verify that 'Entution' header is available on the page",
					"user should be able to see the logo", "logo is not displayed", "fail");

		}
	}

	public void userLogin() throws Exception {

		WaitElement(txt_username);
		sendKeys(txt_username, UserNameData);
		WaitElement(txt_password);
		sendKeys(txt_password, PasswordData);

		WaitClick(btn_login);
		click(btn_login);
		
		WaitElement(lnk_home);
		if (isDisplayed(lnk_home)) {

			writeTestResults("User login", "User should navigate to the Home page of Entution",
					"user navigated to the home page", "pass");
		}

		else {
			writeTestResults("User login", "User should navigate to the Home page of Entution",
					"user cant navigate to the home page", "fail");

		}
	}

	public void navigateToSideBar() throws Exception {

		WaitClick(navigateMenu);
		click(navigateMenu);

		WaitElement(sideNavBar);
		if (isDisplayed(sideNavBar)) {

			writeTestResults("Verify that user can successfully navigate to side menu",
					"User successfully navigate to side menu", "Side menu displayed", "pass");

		} else {

			writeTestResults("Verify that user can successfully navigate to side menu",
					"User can't successfully navigate to side menu", "Side menu is not displayed", "fail");
		}
	}

	public void navigateToFixedAssetMenu() throws Exception {

		click(fixedAsset_btn);

		if (isDisplayed(fixedAssetLogo)) {

			writeTestResults("Verify that user can view fixed asset logo", "User can view the logo", "logo viewed",
					"pass");

		}

		else {

			writeTestResults("Verify that  user can view fixed asset logo", "User can't view the logo",
					"logo is not viewed", "fail");
		}

	}

	/* TestCase - 01 */
	// Verify that Fixed asset can be created through generating Purchase order

	public void navigateToProcurement() throws Exception {

		click(btn_procurement);

		if (isDisplayed(label_procurement)) {

			writeTestResults("Verify that user can view procurement label", "User can view the logo", "Label viewed",
					"pass");
		}

		else {

			writeTestResults("Verify that user can view procurement label", "User can view the logo",
					"Label can't be viewed", "fail");
		}

	}

	public void navigateToPurchaseOrder() throws Exception {
		click(btn_purchaseOrder);

		if (isDisplayed(label_PurchaseOrder)) {

			writeTestResults("Verify that user can view purchase order logo", "User can view the logo",
					"Logo displayed", "pass");
		}

		else {

			writeTestResults("Verify that user can view purchase order logo", "User can view the logo",
					"Logo can't be displayed", "fail");
		}
	}

	public void navigateToJourneyTab() throws Exception {

		click(btn_newPurchaseOrder);

		if (isDisplayed(tab_journey)) {

			writeTestResults(" Verify that the user can navigate to journey view", "List Journey should be displayed",
					"journeys are loaded", "pass");
		} else {

			writeTestResults(" Verify that the user can navigate to journey view", "List Journey should be displayed",
					"journeys are not loaded", "fail");
		}
	}

	public void navigateToNewPurchaseOrder() throws Exception {

		click(btn_newServiceJourney);

		if (isDisplayed(label_PurchaseOrder1)) {
			writeTestResults(" Verify that the user can navigate to Purchase Order by-page",
					"User can navigate to purchase order", "purchase order page is loaded", "pass");
		} else {
			writeTestResults("Verify that the user can navigate to Purchase Order by-page",
					"User can navigate to purchase order", "purchase order page is not loaded", "fail");
		}
	}

	public void fillingPurchaseOrderForm() throws Exception {
		// Select Vendor
		click(btn_vendorSearch);

		sendKeys(txt_vendorSearch, vendorData);

		pressEnter(txt_vendorSearch);

		Thread.sleep(2000);
		doubleClick(doubleClickVendor);

		// Select Vendor ref No
		sendKeys(txt_venderRefNo, vendorRefData);

		// Select from Product lookup

		click(btn_productSearch);

		Thread.sleep(2000);

		sendKeys(txt_productSearch, productsData);

		pressEnter(txt_productSearch);

		Thread.sleep(3000);

		doubleClick(doubleClickProduct);

		sendKeys(txt_qty, qtyData);

		sendKeys(txt_unitPrice, unitPriceData);

	}

	// loading advance page
	public void advancedSettings() throws Exception {

		click(btn_advance);

		Thread.sleep(2000);

		if (isDisplayed(label_advance)) {
			writeTestResults(" Verify that the user can navigate Advance page ", "User can navigate to Advance page",
					"Advance page is displayed", "pass");
		} else {
			writeTestResults("Verify that the user can navigate Advance page ", "User can navigate to Advance page",
					"Advance page is not displayed", "fail");
		}
	}

	public void fixedAsset() throws Exception {

		changeCSS("//div[@id='message-back-bd']", "z-index:-9999");

		click(btn_fixedAss);

		Thread.sleep(2000);

		if (isDisplayed(label_assetGrp)) {
			writeTestResults(" Verify that the user can navigate to fixed asset page ",
					"User can navigate to fixed asset page", "Fixed Asset page is displayed", "pass");
		} else {
			writeTestResults("Verify that the user can navigate to fixed asset page ",
					"User can navigate to fixed asset page", "Fixed Asset page is not displayed", "fail");
		}

	}

	public void assetGrp() throws Exception {

		selectText(btn_assetGrp, assetGrpData);

		click(btn_apply);

		Thread.sleep(2000);

		if (isDisplayed(label_purchaseOrderPage)) {
			writeTestResults(" Verify that the user will be able to save advance page and back to purchase order page",
					"User can navigate to purchase order page", "purchase order page displayed again", "pass");
		} else {
			writeTestResults(" Verify that the user will be able to save advance page and back to purchase order page ",
					"User can navigate to purchase order page", "purchase order page is not displayed", "fail");
		}
	}

	public void checkOutProcurement() throws Exception {

		click(btn_checkout);

		Thread.sleep(2000);

		if (isDisplayed(label_purchaseOrderPageNew)) {
			writeTestResults(" Verify that the user Should be able to click on checkout",
					"User can navigate to purchase order page", "purchase order page displayed again", "pass");
		} else {
			writeTestResults("Verify that the user Should be able to click on checkout",
					"User can navigate to purchase order page", "purchase order page is not displayed again", "fail");
		}
	}

	public void draftNreleaseProcurement() throws Exception {

		click(btn_draft);

		Thread.sleep(2000);
		explicitWait(btn_releaseProc, 40);
		trackCode = getText("//label[@id='lblTemplateFormHeader']");
		click(btn_releaseProc);

		Thread.sleep(2000);
		explicitWait(label_gotopage, 40);
		if (isDisplayed(label_gotopage)) {
			writeTestResults(" Verify that the user Should be able to click on draft and release",
					"Should be able draft & release the purchase order", "purchase order released successfully",
					"pass");
		} else {
			writeTestResults("Verify that the user Should be able to click on draft and release",
					"Should be able draft & release the purchase order", "purchase order is not released successfully",
					"fail");
		}
	}

	public void purchaseInvoicePage() throws Exception {

		click(btn_goToPageInternal);

		switchWindow();

		click(btn_checkoutPro);

		click(btn_draftPro);

		Thread.sleep(2000);

		if (isDisplayed(label_purchaseInvoicePage)) {

			writeTestResults(" Verify that the user Should be able to click on purchase invoice button",
					"Able to click on Convert to Purchase Invoice & page should navigate to PI where fields were auto filled",
					"purchase invoice drafted", "pass");
		} else {
			writeTestResults("Verify that the user Should be able to click on purchase invoice button",
					"Able to click on Convert to Purchase Invoice & page should navigate to PI where fields were auto filled",
					"purchase invoice not drafted", "fail");
		}
	}

	public void purchaseInvoiceRelease() throws Exception {

		Thread.sleep(2000);

		click(btn_releaseButton);
		
		explicitWait(label_purchaseInvoicePageReleasebtn, 30);
		Thread.sleep(5000);
		trackCode = getText("//label[@id='lblTemplateFormHeader']");
		if (isDisplayed(label_purchaseInvoicePageReleasebtn)) {

			writeTestResults(" Verify that the user Should be able to click on purchase invoice button",
					"Able to release the purchase Invoice successfully", "purchase invoice released successfully",
					"pass");
		} else {
			writeTestResults("Verify that the user Should be able to click on purchase invoice button",
					"Able to release the purchase Invoice successfully",
					"purchase invoice is not released successfully", "fail");
		}

	}

	public void purchaseInvoiceJournalEntry() throws Exception {
		
		explicitWaitUntillClickable(btn_actionPro, 50);
		Thread.sleep(2000);
		explicitWaitUntillClickable(btn_actionPro, 50);
		Thread.sleep(2000);
		explicitWaitUntillClickable(btn_actionPro, 50);
		Thread.sleep(3000);
		click(btn_actionPro);

		explicitWaitUntillClickable(btn_journal, 50);

		Thread.sleep(2000);
		click(btn_journal);
		Thread.sleep(2000);

		explicitWait(label_journalEntryDetails, 50);
		if (isDisplayed(label_journalEntryDetails)) {

			writeTestResults(" Verify that the user Should be able to click on journal entry",
					"Journal entry should be displayed", "FA page displayed", "pass");
		} else {
			writeTestResults("Verify that the user Should be able to click on journal entry",
					"Journal entry should be displayed", "FA page displayed is not displayed again", "fail");

		}

	}

	public void purchaseInvoiceFAPage() throws Exception {

		explicitWait(txt_FA, 50);
		click(txt_FA);

		switchWindow();

		Thread.sleep(4000);

		click(btn_ok);

		click(btn_fixedAssSegment);

		segmentNo = getText(txt_segmentNo);

		click(btn_close);

		Thread.sleep(4000);

		if (isDisplayed(label_falabel)) {

			writeTestResults(" Verify that the user Should be able to click on FA",
					"Created draft FA information number will be displayed", "Journal entry page displayed", "pass");
		} else {
			writeTestResults("Verify that the user Should be able to click on FA",
					"Created draft FA information number will be displayed", "Journal entry page is not displayed",
					"fail");
		}

	}

	public void releaseFABook() throws Exception {

		Thread.sleep(2000);

		click(navigateMenu);
		waitImplicitBase(fixedAsset_btn, 3);
		click(fixedAsset_btn);
		waitImplicitBase(btn_fixedAssetInfo, 3);

		click(btn_fixedAssetInfo);

		waitImplicitBase(txt_fixedAssetInfo, 3);

		sendKeys(txt_fixedAssetInfo, segmentNo);

		waitImplicitBase(txt_fixedAssetInfo, 3);

		pressEnter(txt_fixedAssetInfo);
		Thread.sleep(3000);
		click(doubleClickSegmentNo.replace("number", segmentNo));
		click("//div[@id='permissionBar']//a[text()='Edit']");
		Thread.sleep(2000);
		click("//ul[@id='divFixedAsInfo-tabs']//li[@id='#divAssetBook']");
		Thread.sleep(2000);
		click("//span[@class='pic16 pic16-newwin-alt picalign']");
		Thread.sleep(2000);
		click("//input[@id='txtTemAcquisitiondate']");
		Thread.sleep(2000);
		click("//div[@id='ui-datepicker-div']//a[contains(text(),'28')]");
		Thread.sleep(2000);
		click("//li[@id='#divTemDep']");
		Thread.sleep(2000);
		JavascriptExecutor js1 = (JavascriptExecutor) driver;
		js1.executeScript("$('#txtTemschedulecode').nextAll().find('.pic16-search').click()");

		Thread.sleep(2000);
		String searchText = "//input[@id='txtg1138']";
		sendKeys(searchText, "YEARLY-1st of Jan");
		Thread.sleep(2000);
		click("//a[@id='A17']//i[contains(@class,'fa fa-search')]");
		Thread.sleep(2000);
		doubleClick("//td[contains(text(),'YEARLY-1st of Jan')]");
		Thread.sleep(2000);
		JavascriptExecutor js2 = (JavascriptExecutor) driver;
		js2.executeScript("$('#txtTemResponsibleUser').nextAll().find('.pic16-search').click()");
		Thread.sleep(2000);
		String serachText1 = "//input[@id='txtg1015']";
		Thread.sleep(2000);
		sendKeys(serachText1, "Ajith Kumarasiri");
		pressEnter(serachText1);
		Thread.sleep(2000);
		doubleClick("//td[text()='Ajith Kumarasiri']");
		Thread.sleep(2000);
		JavascriptExecutor js3 = (JavascriptExecutor) driver;
		js3.executeScript("$('#ui-datepicker-div').nextAll().find('.pic16-apply').click()");
		Thread.sleep(4000);
		click("//div[@id='permissionBar']//a[text()='Update']");
		Thread.sleep(2000);
		click(btn_release);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		Thread.sleep(5000);

		if (isDisplayed(label_fixedAssetInformationRelease)) {

			writeTestResults(" Verify that the user Should be able to click on fixed asset information",
					" User should be able to release the created FA Book Information",
					"fixed asset information page displayed again", "pass");
		} else {
			writeTestResults("Verify that the user Should be able to click on fixed asset information",
					" User should be able to release the created FA Book Information",
					"fixed asset information page is not displayed again", "fail");
		}

	}

	/* TestCase - 02 */
	// Verify that Fixed asset can be created through generating Internal order

	public void navigateInventoy() throws Exception {

		click(btn_inventory);

		Thread.sleep(2000);
		if (isDisplayed(label_inventory)) {

			writeTestResults("Verify that user can view inventory label",
					" Inventory & Warehousing should be clicked successfully", "Inventory and warehousing Label viewed",
					"pass");
		}

		else {

			writeTestResults("Verify that user can view inventory label",
					"Inventory & Warehousing should be clicked successfully",
					"Inventory and warehousing Label can't be viewed", "fail");
		}

	}

	public void navigateToInternalOrder() throws Exception {
		click(btn_internalOrder);
		Thread.sleep(2000);

		if (isDisplayed(label_InernalOrder)) {

			writeTestResults("Verify that user can navigate to Internal Order by-page",
					"Navigate to Internal Order by-page", "Internal order Logo displayed", "pass");
		}

		else {

			writeTestResults("Verify that user can navigate to Internal Order by-page",
					"Navigate to Internal Order by-page", "Internal order Logo can't be displayed", "fail");
		}
	}

	public void navigateToInternalOrderJourneyTab() throws Exception {

		click(btn_newInternalOrder);
		Thread.sleep(2000);

		if (isDisplayed(tab_internalOrderjourney)) {

			writeTestResults(" Verify that the user can navigate to Journeys", "List Journey should be displayed",
					"journeys are loaded", "pass");
		} else {

			writeTestResults(" Verify that the user can navigate to journeys", "List Journey should be displayed",
					"journeys are not loaded", "fail");
		}
	}

	public void navigateToNewInternalOrder() throws Exception {

		click(btn_CapitalWIPRequestJourney);
		Thread.sleep(2000);

		if (isDisplayed(label_InternalOrder1)) {
			writeTestResults(" Verify that the user can navigate to Internal Order by-page",
					"Internal Order page should be displayed", "internal order page is loaded", "pass");
		} else {
			writeTestResults("Verify that the user can navigate to Internal Order by-page",
					"Internal Order page should be displayed", "internal order page is not loaded", "fail");
		}
	}

	public void fillingInternalOrderForm() throws Exception {

		sendKeys(txt_title, titleData);

		click(btn_requester);

		sendKeys(txt_requesterSearch, requesterData);

		pressEnter(txt_requesterSearch);

		// Thread.sleep(3000);

		doubleClick(doubleClickRequester);

		click(btn_date);

		// Select from Product lookup

		click(btn_internalProductSearch);

		Thread.sleep(2000);

		sendKeys(txt_internalProductSearch, productData1);

		pressEnter(txt_internalProductSearch);

		Thread.sleep(3000);

		doubleClick(doubleClickinternalProduct);

		sendKeys(txt_internalqty, internalqtyData);

		if (isDisplayed(label_InternalOrder)) {
			writeTestResults(" Verify that the user can fill all the mandatory fields",
					"Should be able to fill all mandatory fields", "internal order page displayed again", "pass");
		} else {
			writeTestResults("Verify that the user can fill all the mandatory fields",
					"Should be able to fill all mandatory fields", "internal order page is not displayed again",
					"fail");
		}

	}

	// loading advance page
	public void advancedSettingsInventory() throws Exception {

		Thread.sleep(2000);
		click(btn_advanceInventory);
		Thread.sleep(5000);

		if (isDisplayed(label_advanceInventory)) {

			writeTestResults(" Verify that the user can navigate Advance page ", "User can navigate to Advance page",
					"Advance page is displayed", "pass");

		} else {

			writeTestResults("Verify that the user can navigate Advance page ", "User can navigate to Advance page",
					"Advance page is not displayed", "fail");
		}
	}

	public void fixedAssetInternal() throws Exception {

		changeCSS("//div[@id='message-back-bd']", "z-index:-9999");

		click(btn_fixedAssInternal);
		Thread.sleep(2000);

		if (isDisplayed(label_assetGrpInternal)) {

			writeTestResults(" Verify that the user can navigate to fixed asset page ",
					"User can navigate to fixed asset page", "Fixed Asset page is displayed", "pass");

		} else {

			writeTestResults("Verify that the user can navigate to fixed asset page ",
					"User can navigate to fixed asset page", "Fixed Asset page is not displayed", "fail");
		}

	}

	public void assetGrpInternal() throws Exception {

		selectText(btn_assetGrpInternal, assetGroupData);

		click(btn_applyInternal);
		Thread.sleep(2000);

		if (isDisplayed(label_InternalOrder)) {

			writeTestResults(" Verify that the user can navigate to internal order page again ",
					" Able to save advance page", "internal order page is displayed again", "pass");

		} else {

			writeTestResults("Verify that the user can navigate to internal order page again ",
					" Able to save advance page", "internal order page is not displayed again", "fail");
		}

	}

	public void draftNreleaseInventory() throws Exception {

		click(btn_draft);

		Thread.sleep(2000);

		click(btn_release);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		Thread.sleep(4000);

		if (isDisplayed(goToPageInternal)) {

			writeTestResults(" Verify that the user can release the order and navigate to internal order page again ",
					"Should be able draft & release the Internal order", "internal order page is displayed again",
					"pass");

		} else {

			writeTestResults("Verify that the user can release the order and navigate to internal order page again ",
					" Should be able draft & release the Internal order", "internal order page is not displayed again",
					"fail");
		}
	}

	public void navigateToDispatchOrder() throws Exception {

		click(btn_goToPageInternal);

		Thread.sleep(3000);

		switchWindow();
		sendKeys(txt_shipping, shippingData);

	}

	public void completeNreleaseInventory() throws Exception {

		click(btn_draftdispatch);

		Thread.sleep(2000);

		click(btn_releasedispatch);

		binCardNo = getText(txt_dispatchOrderNo);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		Thread.sleep(4000);

		if (isDisplayed(label_InternalDispatchOrder)) {

			writeTestResults(" Verify that the user can successfully release internal dispatch order",
					"Able to release the document successfully", "internal dispatch order page displayed again",
					"pass");

		} else {

			writeTestResults("Verify that the user can successfully release internal dispatch order",
					"Able to release the document successfully", "internal dispatch order page is not displayed again",
					"fail");
		}

	}

	public void displayJournalEntryInventory() throws Exception {

		Thread.sleep(5000);
		click(btn_action);

		click(btn_journalEntry);

		click(txt_FA);

		switchWindow();

		Thread.sleep(4000);

		click(btn_ok);

		docValue = getText(txt_docValue);

		click(btn_fixedAssSegment);

		segmentNo = getText(txt_segmentNo);

		click(btn_close);

		Thread.sleep(3000);

		if (isDisplayed(label_journalEntry)) {

			writeTestResults(" Verify that the user can successfully navigate to journal entry page",
					"Should be navigate to journal entry page", "journal entry page displayed successfully", "pass");

		} else {

			writeTestResults("Verify that the user can successfully navigate to journal entry page",
					"Should be navigate to journal entry page", "journal entry page is not displayed successfully",
					"fail");
		}

	}

	public void navigateTofixedAssetInfo() throws Exception {

		click(navigateMenu);

		click(fixedAsset_btn);

		click(btn_fixedAssetInfo);

		sendKeys(txt_fixedAssetInfo, segmentNo);

		pressEnter(txt_fixedAssetInfo);

		Thread.sleep(3000);
		click(doubleClickSegmentNo.replace("number", segmentNo));

		Thread.sleep(3000);

		click(btn_endRelease);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		Thread.sleep(2000);

		if (isDisplayed(label_fixedAssetInformationRelease)) {

			writeTestResults(" Verify that the user can successfully release the created FA Book Information",
					"User should be able to release the created FA Book Information",
					"fixed asset information page displayed ", "pass");

		} else {

			writeTestResults("Verify that the user can successfully release the created FA Book Information",
					"User should be able to release the created FA Book Information",
					"fixed asset information page is not displayed ", "fail");
		}
	}

	public void navigateToBinCard() throws Exception {

		Thread.sleep(3000);

		click(navigateMenu);

		click(btn_inventory);

		click(btn_reports);

		click(btn_templatebtn);

		selectIndex(btn_reportTmpWiz, 6);

		Thread.sleep(3000);

		sendKeys(txt_productCode, binCardNo);

		click(btn_quickPreview);

		Thread.sleep(20000);

		stockValue = getText(txt_stockValue);

		Thread.sleep(2000);

		if (docValue.equals(stockValue)) {
			writeTestResults(" Verify that the doc value and stock value are equal",
					"stock value and doc value should be equal", "stock value and doc value are equal", "pass");

		} else {
			writeTestResults(" Verify that the doc value and stock value are equal",
					"stock value and doc value should be equal", "stock value and doc value are not equal", "fail");
		}

	}

	/* TestCase - 03 */
	// Verify that Fixed asset can be created through generating Project

	public void navigateToProject() throws Exception {

		click(btn_project);

		if (isDisplayed(label_project)) {

			writeTestResults("Verify that user can view project logo", " Project module should be clicked successfully",
					"project logo viewed", "pass");

		}

		else {

			writeTestResults("Verify that user can view project logo", " Project module should be clicked successfully",
					"project logo is not viewed", "fail");
		}

	}

	public void navigateToAddProject() throws Exception {
		click(btn_subProject);

		if (isDisplayed(label_subProject)) {

			writeTestResults("Verify that  user can Navigate to Project by-page", "Navigate to Project by-page",
					"Project Logo by page displayed", "pass");
		}

		else {

			writeTestResults("Verify that  user can Navigate to Project by-page", "Navigate to Project by-page",
					"Project Logo by page can't be displayed", "fail");
		}
	}

	public void navigateToNewProject() throws Exception {

		click(btn_newProject);

		if (isDisplayed(txt_project)) {

			writeTestResults("Verify that user can view new project logo", " Project page should be displayed",
					"New Project Logo displayed", "pass");
		}

		else {

			writeTestResults("Verify that user can view new project logo", " Project page should be displayed",
					"New Project Logo can't be displayed", "fail");
		}
	}

	public void fillingProjectForm() throws Exception {

		// create instance of Random class
		Random rand = new Random();

		// Generate random integers in range 0 to 999
		int rand_int1 = rand.nextInt(10000000);

		projectCodeData = projectCodeData + Integer.toString(rand_int1);

		sendKeys(txt_projectCode, projectCodeData);

		click(btn_responsiblePersonSearch);

		if (isDisplayed(label_responsiblePerson)) {

			writeTestResults("Verify that user can add responsible person",
					" responsible person lookup should be displayed", "responsible person lookup Logo displayed",
					"pass");
		}

		else {

			writeTestResults("Verify that user can add responsible person",
					" responsible person lookup should be displayed",
					"responsible person lookup Logo can't be displayed", "fail");
		}

		sendKeys(txt_responsiblePersonSearch, responsiblePerson);

		pressEnter(txt_responsiblePersonSearch);

		doubleClick(doubleClickresponsiblePerson);

		sendKeys(txt_projectTitle, projectTitleData);

		selectText(txt_projectGroup, projectGroupData);

		selectText(txt_postingMethod, postingMethodData);

		selectText(txt_projectType, projectTypeData);

		selectText(txt_assetGroup, assetGroupData);

		selectText(txt_bussinessUnit, bussinessUnitData);
		Thread.sleep(2000);
		click(btn_pricingProfile);
		Thread.sleep(2000);
		if (isDisplayed(label_pricingProfile)) {

			writeTestResults("Verify that user can add pricing profile", " pricing profile should be displayed",
					"pricing profile Logo displayed", "pass");
		}

		else {

			writeTestResults("Verify that user can add pricing profile", " pricing profile should be displayed",
					"pricing profile Logo can't be displayed", "fail");
		}

		sendKeys(txt_pricingProfile, pricingProfileData);
		Thread.sleep(2000);
		pressEnter(txt_pricingProfile);
		Thread.sleep(2000);
		doubleClick(doubleClickPricingProfile);

		click(txt_startDate);
		doubleClick(doubleClick_startDate);

		click(txt_endDate);
		doubleClick(doubleClick_endDate);

		Thread.sleep(5000);

		click(btn_projectLocation);

		if (isDisplayed(label_projectLocation)) {

			writeTestResults("Verify that user can add project location", " project location should be displayed",
					"project location Logo displayed", "pass");
		}

		else {

			writeTestResults("Verify that user can view new project logo", " project location should be displayed",
					"project location Logo can't be displayed", "fail");
		}

		sendKeys(txt_projectLocation, projectLocationData);

		pressEnter(txt_projectLocation);

		doubleClick(doubleClickProjectLocation);

		selectText(txt_inputWarehouse, inputwareData);

		selectText(txt_WIPWarehouse, WIPWareData);
	}

	public void draftNReleaseProject() throws Exception {

		click(btn_draft);

		Thread.sleep(2000);

		click(btn_release);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(label_projectAgain)) {

			writeTestResults("Verify that user can draft and release project",
					" Project page should be displayed again", "Project Logo displayed again", "pass");
		}

		else {

			writeTestResults("Verify that user can view new project logo", " Project page should be displayed again",
					"Project Logo can't be displayed again", "fail");
		}
	}

	public void updateProject() throws Exception {

		Thread.sleep(2000);

		click(btn_edit);

		click(btn_workBreakdown);

		click(btn_plus);

		if (isDisplayed(label_projecttask)) {

			writeTestResults("Verify that user can add a task", " Task lookup should be displayed",
					"Task lookup displayed", "pass");
		}

		else {

			writeTestResults("Verify that user can add a task", "Task looup should be displayed",
					"Task lookup can't be displayed", "fail");
		}
	}

	public void taskProject() throws Exception {

		sendKeys(txt_taskName, taskNameData);

		click(btn_update);

		click(btn_expand);

		click(btn_releaseTask);

		click(btn_updateMain);

		if (isDisplayed(label_projectAgain)) {

			writeTestResults("Verify that user can update project successfully",
					" Project page should be displayed again", "Project Logo displayed again", "pass");
		}

		else {

			writeTestResults("Verify that user can update project successfully",
					" Project page should be displayed again", "Project Logo can't be displayed again", "fail");
		}

		switchWindow();
	}

	public void navigateToOutboundPaymentAdvice() throws Exception {

		click(navigateMenu);
		Thread.sleep(3000);
		click(btn_finance);
		Thread.sleep(3000);
		click(btn_outboundPaymentAdvice);
		Thread.sleep(3000);
		if (isDisplayed(label_outboundPaymentAdvice)) {

			writeTestResults("Verify that user can navigate to outbound payment advice in finance module",
					"Outbound payment advice page should be displayed again", "Outbound payment advice page displayed",
					"pass");
		}

		else {

			writeTestResults("Verify that user can navigate to outbound payment advice in finance module",
					"Outbound payment advice page should be displayed again",
					"Outbound payment advice page can't be displayed", "fail");
		}
	}

	public void navigateToNewOutboundPaymentAdvice() throws Exception {

		click(btn_newOutboundPaymentAdvice);
		Thread.sleep(3000);
		if (isDisplayed(label_newJourney)) {

			writeTestResults("Verify that user can select accrual voucher journey",
					"Should able to create accrual voucher", "Accrual voucher journey displayed", "pass");
		}

		else {

			writeTestResults("Verify that user can select accrual voucher journey",
					"Should able to create accrual voucher", "Accrual voucher journey  can't be displayed", "fail");
		}
	}

	public void journalAccrualVoucher() throws Exception {

		click(btn_accrualVoucher);

		if (isDisplayed(label_outboundPaymentnew)) {

			writeTestResults("Verify that user can navigate to outbound payment advice page again",
					" outbound payment advice page should be displayed again",
					"outbound payment advice page displayed again", "pass");
		}

		else {

			writeTestResults("Verify that user can navigate to outbound payment advice page again",
					" outbound payment advice page should be displayed again",
					"outbound payment advice page can't be displayed again", "fail");
		}

	}

	public void vendorDetailsLookup() throws Exception {

		click(btn_vendorDetails);

		if (isDisplayed(label_vendorDetails)) {

			writeTestResults("Verify that user can add vendor from lookup", "Vendor lookup should be displayed again",
					"Vendor lookup displayed", "pass");
		}

		else {

			writeTestResults("Verify that user can add vendor from lookup", "Vendor lookup should be displayed again",
					"Vendor lookup can't be displayed", "fail");
		}
		sendKeys(txt_vendorDetails, vendorDetailsData);

		pressEnter(txt_vendorDetails);

		Thread.sleep(2000);

		doubleClick(doubleClickvendorDetails);

	}

	public void GLACCLookup() throws Exception {

		Random rand1 = new Random();
		int rand_int2 = rand1.nextInt(100000);

		vendorRefNoData = vendorRefNoData + Integer.toString(rand_int2);

		sendKeys(txt_vendorRefNo, vendorRefNoData);

		click(btn_GLAcc);

		if (isDisplayed(label_GLAcc)) {

			writeTestResults("Verify that user can add a GL account",
					" add a GL account lookup should be displayed again", "GL account lookup displayed", "pass");
		}

		else {

			writeTestResults("Verify that user can add a GL account", "GL account lookup should be displayed again",
					"GL account lookup can't be displayed", "fail");
		}

		sendKeys(txt_GLAcc, GLAccData);

		pressEnter(txt_GLAcc);

		doubleClick(doubleClickGLAcc);

	}

	public void costAllocationProject() throws Exception {

		sendKeys(txt_amount, amountData);
		Thread.sleep(3000);
		click(btn_costAllocation);
		Thread.sleep(2000);
		if (isDisplayed(label_costAllocation)) {

			writeTestResults("Verify that user can add cost allocation", " Able to re-direct to Cost Allocation page.",
					"cost allocation logo displayed", "pass");
		}

		else {

			writeTestResults("Verify that user can add cost allocation", " Able to re-direct to Cost Allocation page.",
					"cost allocation Logo can't be displayed", "fail");
		}
	}

	public void endCostAllocation() throws Exception {

		selectText(txt_costCenter, costCenterData);

		sendKeys(txt_percentage, percentageData);

		selectText(txt_costAssignmentType, costAssignmentData);

		click(btn_costObject);

		sendKeys(txt_costObject, projectCodeData);

		pressEnter(txt_costObject);

		Thread.sleep(5000);

		doubleClick(doubleClickCostObject);

		click(btn_addRecord);

		click(btn_updateCostAllocations);

		if (isDisplayed(label_outboundPaymentlabel)) {

			writeTestResults(
					"Verify that user able to add record to the table in cost allocation & another new allocation can be generate up to Percentage = 100% and Cost allocation has updated as requirement",
					" Cost allocation has updated as requirement", "Outbound payement advice page displayed", "pass");
		}

		else {

			writeTestResults(
					"Verify that user able to add record to the table in cost allocation & another new allocation can be generate up to Percentage = 100% and Cost allocation has updated as requirement",
					"Cost allocation has updated as requirement", "Outbound payement advice page can't be displayed",
					"fail");
		}
	}

	public void draftNReleaseOutBoundPaymentAdvice() throws Exception {

		click(btn_checkoutCost);
		Thread.sleep(3000);
		click(btn_draftCost);

		Thread.sleep(3000);

		click(btn_releaseCost);

		Thread.sleep(4000);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(label_outboundPayment)) {

			writeTestResults("Verify that user can draft and released outbound payment advice",
					"outbound payment advice draft and released", "outbound payment advice displayed", "pass");
		}

		else {

			writeTestResults("Verify that user can draft and released outbound payment advice",
					"outbound payment advice draft and released", "outbound payment advice can't be displayed", "fail");
		}
	}

	public void navigateToProjectAgain() throws Exception {

		click(navigateMenu);
		Thread.sleep(2000);
		click(btn_project);
		Thread.sleep(2000);
		click(btn_submenuProject);

		Thread.sleep(3000);

		if (isDisplayed(label_subProject)) {

			writeTestResults("Verify that  user can Navigate to Project page", "Navigate to Project page",
					"Project Logo displayed", "pass");
		}

		else {

			writeTestResults("Verify that  user can Navigate to Project page", "Navigate to Project page",
					"Project Logo can't be displayed", "fail");
		}
	}

	public void ProjectRelease() throws Exception {

		sendKeys(txt_projecttxt, projectCodeData);

		pressEnter(txt_projecttxt);

		Thread.sleep(3000);
		click(clickProjectCode.replace("no", projectCodeData));

		click(btn_actionProject);

		click(btn_updateTaskPOC);

		selectIndex(txt_task, 1);

		sendKeys(txt_POC, POCData);

		click(btn_applyTask);

		Thread.sleep(3000);

		click(btn_actionProject);

		click(btn_complete);

		click(btn_yes);

		Thread.sleep(2000);

		if (isDisplayed(label_taskCompletion)) {

			writeTestResults("Verify that  user can navigate to Task completion page",
					"User will navigate to Task completion page", "Task completion page displayed", "pass");
		}

		else {

			writeTestResults("Verify that  user can navigate to Task completion page",
					"User will navigate to Task completion page", "Task completion page can't be displayed", "fail");
		}
	}

	public void ProjectCompletion() throws Exception {

		click(btn_postDate);

		click(doubleClick_postDate);

		Thread.sleep(2000);

		click(btn_SelectCheck);

		click(btn_applytaskCompletion);

		Thread.sleep(2000);

		if (isDisplayed(label_Project)) {

			writeTestResults("Verify that  user can change project task to completed stage",
					"Project task changed to Completed stage", "Project Logo displayed", "pass");
		}

		else {

			writeTestResults("Verify that  user can change project task to completed stage",
					"Project task changed to Completed stage", "Project Logo can't be displayed", "fail");
		}

	}

	public void ProjectJournalEntry() throws Exception {

		Thread.sleep(5000);

		click(btn_actionCompletedProject);

		click(btn_journalCompletedProject);

		Thread.sleep(2000);

		click(txt_FABookProject);

		switchWindow();

		Thread.sleep(5000);

		click(btn_fixedAssSegmentNo);

		segmentNo = getText(txt_segmentNo);

		click(btn_close);

		Thread.sleep(3000);

		if (isDisplayed(label_projectJournal)) {

			writeTestResults("Verify that  user can Navigate to journal entry page",
					" Journal entry should be displayed", "journal entry page displayed", "pass");
		}

		else {

			writeTestResults("Verify that  user can Navigate to journal entry page",
					" Journal entry should be displayed", "journal entry page can't be displayed", "fail");
		}
	}

	public void ProjectReleaseAssetInfo() throws Exception {

		Thread.sleep(2000);

		click(navigateMenu);

		click(fixedAsset_btn);

		click(btn_fixedAssetInfo);

		sendKeys(txt_fixedAssetInfo, segmentNo);

		pressEnter(txt_fixedAssetInfo);

		Thread.sleep(3000);
		click(doubleClickSegmentNo.replace("number", segmentNo));

		click(btn_endRelease);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		Thread.sleep(2000);

		if (isDisplayed(label_fixedAssetInformationlabel)) {

			writeTestResults("Verify that User should be able to release the created FA Book Information",
					"Able to do necessary changes and can check for accuracy of FA  page",
					"Fixed asset information page displayed", "pass");
		}

		else {

			writeTestResults("Verify that User should be able to release the created FA Book Information",
					"Able to do necessary changes and can check for accuracy of FA  page",
					"Fixed asset information page can't be displayed", "fail");
		}

	}

	/* TestCase - 04 */
	// Verify that user can depreciate asset

	public void navigateToFixedAssetCreation() throws Exception {

		click(btn_fixedAssetBook);

		if (isDisplayed(txt_fixedAssetBook)) {

			writeTestResults("Navigate to Fixed Asset book by-page", "User can view the fixed asset logo",
					"fixed asset Logo displayed", "pass");
		}

		else {

			writeTestResults("Navigate to Fixed Asset book by-page", "User can view the fixed asset logo",
					"fixed asset Logo can't be displayed", "fail");
		}

	}

	public void viewAssetBook() throws Exception {

		selectText(btn_template, templateData);

		sendKeys(txt_fixedAssetBookNo, fixedAssetBookNoData);

		pressEnter(txt_fixedAssetBookNo);

		doubleClick(doubleClickFixedAssetBookNo);

		if (isDisplayed(label_fixedAssetInformationMain)) {

			writeTestResults("Navigate to Fixed Asset book", "Should be able to view asset book",
					"fixed asset information Logo displayed", "pass");
		}

		else {

			writeTestResults("Navigate to Fixed Asset book", "Should be able to view asset book",
					"fixed asset information Logo can't be displayed", "fail");
		}

	}

	public void navigateToFixedAssetDepreciation() throws Exception {

		click(btn_action);

		click(btn_depreciation);

		if (isDisplayed(label_fixedAssetInformationMain)) {

			writeTestResults("user should successfully depreciate",
					"should be navigate to fixed asset depreciation page", "fixed asset information Logo displayed",
					"pass");
		}

		else {

			writeTestResults("user should successfully depreciate",
					"should be navigate to fixed asset depreciation page",
					"fixed asset information Logo can't be displayed", "fail");
		}

	}

	public void flagDepreciation() throws Exception {

		click(btn_selectAll);

		if (isDisplayed(label_fixedAssetInformationMain)) {

			writeTestResults("User can successfully flag the assets", "Able to flag the assets to depreciate",
					"fixed asset information Logo displayed again", "pass");
		}

		else {

			writeTestResults("User can successfully flag the assets", "Able to flag the assets to depreciate",
					"fixed asset information Logo can't be displayed again", "fail");
		}

	}

	public void releaseDepreciation() throws Exception {

		click(btn_releaseBook);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(label_fixedAssetInformationMain)) {

			writeTestResults("User can successfully release the Fixed Asset book",
					"Able to do depreciation for selected assets and those asset information net book value and accumilated depreciation value should be updated with latest values",
					"fixed asset information Logo displayed", "pass");
		}

		else {

			writeTestResults("User can successfully release the Fixed Asset book",
					"Able to do depreciation for selected assets and those asset information net book value and accumilated depreciation value should be updated with latest values",
					"fixed asset information Logo can't be displayed", "fail");
		}

	}

	/* After TestCase -01 run TestCase - 05 */
	// Verify that fixed asset can be revalue
	public void navigateToFixedAssetInfoByRevaluation() throws Exception {

		Thread.sleep(5000);

		click(btn_action);

		click(btn_Revaluation);

	}

	public void navigateToRevaluationPage() throws Exception {

		selectIndex(btn_AssetBook, 1);

		sendKeys(txt_revalueAmount, RevalueAmountData);

		Thread.sleep(3000);
		sendKeys(txt_usefulLife, usefulLifeData);

		Thread.sleep(2000);

		click(btn_applyAssetInfo);

		click(btn_AssetBooks);

		Thread.sleep(2000);
		String netBookValue1 = getText(txt_netBookNo1);
		String btn_FABookdotandzeroReplcaed = netBookValue1.replace(".00000", "");
		String btn_FABookdotandzeroReplcaed_commaReplaced = btn_FABookdotandzeroReplcaed.replaceAll(",", "");
		System.out.println(btn_FABookdotandzeroReplcaed_commaReplaced);
		click(btn_FABook);
		Thread.sleep(2000);
		String netBookValue2 = getText(txt_netBookNo2);

		Thread.sleep(2000);
		if (btn_FABookdotandzeroReplcaed_commaReplaced.equals(netBookValue2)) {

			writeTestResults("Asset book net value and FA book net value", "Should be able to equal net values",
					"FABook and assetbook values are equal", "pass");

			Thread.sleep(2000);
		}

		else {
			writeTestResults("Asset book net value and FA book net value", "Should be able to equal net values",
					"FABook and assetbook values are not equal", "fail");
		}

	}

	public void equalCreditValueandDebitValue() throws Exception {

		Thread.sleep(5000);

		click(btn_action);

		click(btn_journal);

		String creditValue = getText(txt_credit);
		String debitValue = getText(txt_debit);

		Thread.sleep(2000);
		if (creditValue.equals(debitValue)) {
			writeTestResults("Credit and Debit values should be equal",
					"Should be able to equal credit and debit values", "credit and debit are equal", "pass");
		}

		else {
			writeTestResults("Credit and Debit values should be equal",
					"Should be able to equal credit and debit values", "credit and debit are not equal", "fail");
		}
	}

	/* After TestCase -01 run TestCase - 06 */
	// Verify that fixed asset can be split and can transfer as new asset

	public void navigateToFixedAssetInfoBySplit() throws Exception {

		Thread.sleep(5000);

		click(btn_action);

		click(btn_split);

		if (isDisplayed(label_fixedAssetSplit)) {

			writeTestResults("User can successfully navigate to fixed asset split page",
					"Should navigate to fixed asset split page", "fixed asset split page displayed", "pass");
		}

		else {

			writeTestResults("User can successfully navigate to fixed asset split page",
					"Should navigate to fixed asset split page", "fixed asset split page can't be displayed", "fail");
		}

	}

	public void navigateToSplitnewAssetPage() throws Exception {

		click(btn_createNewwAsset);

		selectIndex(btn_AssetBookSplit1, 1);

		sendKeys(txt_assetDescription, assetDescriptionData);

		sendKeys(txt_usefulLifeSplit1, usefulLifeDataSplit1);

		Thread.sleep(2000);

		click(btn_applyAssetInfo);

		if (isDisplayed(label_fixedAssetSplit)) {

			writeTestResults("User can successfully navigate to create a new fixed asset split",
					"Should display the page for split the asset and to create new asset",
					"splited and created a new asset", "pass");
		}

		else {

			writeTestResults("User can successfully navigate to create a new fixed asset split",
					"Should display the page for split the asset and to create new asset",
					"can't split and creat a new asset", "fail");
		}
	}

	/* After TestCase -01 run TestCase - 07 */
	// Verify that fixed asset can be split and can transfer to existing Asset

	public void navigateToFixedAssetInfoBySplitPage() throws Exception {

		Thread.sleep(5000);

		click(btn_action);

		click(btn_split);

		if (isDisplayed(label_fixedAssetSplit)) {

			writeTestResults("User can successfully navigate to fixed asset split page",
					"Should navigate to fixed asset split page", "fixed asset split page displayed", "pass");
		}

		else {

			writeTestResults("User can successfully navigate to fixed asset split page",
					"Should navigate to fixed asset split page", "fixed asset split page can't be displayed", "fail");
		}
	}

	public void navigateToFixedAssetInfoBySplitExisting() throws Exception {

		click(btn_existingBook);

		selectIndex(btn_AssetBook, 1);

		click(btn_assetInfoSearch);

		sendKeys(txt_assetInfoLookup, assetInfoLookupData);

		Thread.sleep(2000);

		pressEnter(txt_assetInfoLookup);

		doubleClick(doubleClickAssetInfoLookup);

		Thread.sleep(2000);

		if (isDisplayed(label_fixedAssetSplit)) {

			writeTestResults("User can successfully navigate to create an existing fixed asset split",
					"Should display the page for split the asset to existing asset", "fixed asset split page displayed",
					"pass");
		}

		else {

			writeTestResults("User can successfully navigate to create an existing fixed asset split",
					"Should display the page for split the asset to existing asset",
					"fixed asset split page can't be displayed", "fail");
		}
	}

	public void ApplySplitExisting() throws Exception {

		click(btn_applyAssetInfo);

		click(click_close);

		if (isDisplayed(label_fixedAssetInformationSplit)) {

			writeTestResults("User can successfully apply an existing splitted fixed asset information",
					"Should be able to split successfully. splitted asset  & the existing asset which received split asset should be update with new net book value & journal entry should be update as it is.",
					"fixed asset Information Logo displayed", "pass");
		}

		else {

			writeTestResults("User can successfully apply an existing splitted fixed asset information",
					"Should be able to split successfully. splitted asset  & the existing asset which received split asset should be update with new net book value & journal entry should be update as it is.",
					"fixed asset Information Logo can't be displayed", "fail");
		}
	}

	/* After TestCase -01 run TestCase - 08 */
	// Verify that Asset can be dispose by write-off

	public void navigateToFixedAssetInfoByDisposal() throws Exception {

		Thread.sleep(5000);

		click(btn_action);

		click(btn_disposal);

		if (isDisplayed(label_fixedAssetSplit)) {

			writeTestResults("User can successfully navigate to fixed asset disposal page",
					" Should be able to navigate to Fixed asset disposal page", "fixed asset disposal page displayed",
					"pass");
		}

		else {

			writeTestResults("User can successfully navigate to fixed asset disposal page",
					" Should be able to navigate to Fixed asset disposal page",
					"fixed asset disposal page can't be displayed", "fail");
		}
	}

	public void addWriteOffDisposal() throws Exception {

		click(txt_disposalDate);

		doubleClick(doubleClick_disposalDate);

		selectText(txt_disposalMode, disposalModeDataWriteOff);

		click(btn_applyAssetInfoDisposal);

	}

	/* After TestCase -01 run TestCase - 09 */
	// Verify that Asset can be dispose by Selling an asset
	public void navigateToFixedAssetInfoByDisposalSellingAsset() throws Exception {

		Thread.sleep(5000);

		click(btn_action);

		click(btn_disposal);

		if (isDisplayed(label_fixedAssetSplit)) {

			writeTestResults("User can successfully navigate to fixed asset disposal page",
					" Should be able to navigate to Fixed asset disposal page", "fixed asset disposal page displayed",
					"pass");
		}

		else {

			writeTestResults("User can successfully navigate to fixed asset disposal page",
					" Should be able to navigate to Fixed asset disposal page",
					"fixed asset disposal page can't be displayed", "fail");
		}

	}

	public void addSellingAnAssetDisposal() throws Exception {

		click(txt_disposalDateSelling);

		doubleClick(doubleClick_disposalDateSelling);

		selectIndex(txt_disposalModeSelling, 2);

	}

	public void ApplyDisposalSelling() throws Exception {

		click(btn_applyAssetInfoSelling);

		if (isDisplayed(label_fixedAssetInformationSplit)) {

			writeTestResults("User can successfully apply a selling disposal fixed asset information",
					"Should be able to dispose asset by selling ", "fixed asset Information Logo displayed", "pass");
		}

		else {

			writeTestResults("User can successfully apply a selling disposal fixed asset information",
					"Should be able to dispose asset by selling ", "fixed asset Information Logo can't be displayed",
					"fail");
		}
	}

	public void ApplyFreetextInvoice() throws Exception {

		click(btn_action);

		click(txt_freeTextInvoice);

		click(btn_customer);

		sendKeys(txt_customer, customerData);

		pressEnter(txt_customer);

		doubleClick(doubleClickCustomer);

		selectText(txt_salesUnit, salesUnitData);

		Thread.sleep(2000);

		sendKeys(txt_amountText, amountDataText);

		click(btn_checkout);

		click(btn_draft);

		Thread.sleep(2000);

		click(btn_release);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		Thread.sleep(3000);

		if (isDisplayed(label_freeTextInvoice)) {

			writeTestResults("User can successfully apply a selling disposal fixed asset information",
					"Should be able to dispose asset by selling and user should be able to access to create free text invoice.",
					"freeTextInvoice Logo displayed", "pass");
		}

		else {

			writeTestResults("User can successfully apply a selling disposal fixed asset information",
					"Should be able to dispose asset by selling and user should be able to access to create free text invoice.",
					"fixed asset Information Logo can't be displayed", "fail");
		}

	}

	// SMOKE Test cases
	/* Smoke_Fixed_Asset_01 */
	// Verify the ability of creating a new fixed asset book

	public void creatingNewFixedAssetBook() throws Exception {

		click(btn_fixedAssetBookCreation);

		click(btn_newfixedAssetBook);

		Random rand1 = new Random();
		int rand_int2 = rand1.nextInt(1000000);

		assetBookCodeDataCreate = assetBookCodeDataCreate + Integer.toString(rand_int2);

		sendKeys(txt_assetBookCode, assetBookCodeDataCreate);

		click(btn_desc);

		sendKeys(txt_desc, descData);

		click(btn_applyBook);

		click(btn_dateA);

		click(btn_aquisitionDate);

		click(btn_depreciationClick);

		click(btn_depreciationStartDate);

		click(btn_dateB);

		click(btn_scheduleCode);

		sendKeys(txt_scheduleCode, scheduleCodeData);

		pressEnter(txt_scheduleCode);

		Thread.sleep(3000);
		doubleClick(doubleClickscheduleCode);

		click(btn_responsiblePerson);

		sendKeys(txt_responsiblePerson, responsiblePersonData);

		pressEnter(txt_responsiblePerson);

		doubleClick(doubleClickresponsiblePerson1);

		sendKeys(txt_usefulLife1, usefulLifeData1);

		click(btn_draft);

		click(btn_release);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		Thread.sleep(2000);

		if (isDisplayed(label_fixedAssetBook)) {

			writeTestResults("Verify the ability of creating a new fixed asset book.",
					"User should be able to release a fixed asset book.", "new fixed asset book released", "pass");
		}

		else {

			writeTestResults("Verify the ability of creating a new fixed asset book.",
					"User should be able to release a fixed asset book.", "new fixed asset book not released", "fail");
		}

	}

	/* Smoke_Fixed_Asset_02 */
	// Verify the ability of creating new asset group parameter
	public void creatingNewFixedAssetParameter() throws Exception {

		click(btn_fixedAssetGroupConfig);

		click(btn_newFixedAssetGroupConfig);

		click(btn_plus1);

//		click(btn_plus2);

		int rowCount = driver.findElements(By.xpath(assetGroup_tbl)).size();

		for (int i = 1; i <= rowCount; i++) {
			// row count
		}

		String btn_asset_group_plus_mark_second = btn_plus2.replace("row", String.valueOf(rowCount));
		click(btn_asset_group_plus_mark_second);

		int row_second_count = rowCount + 1;
		String txt_group_replaced = txt_group.replace("row", String.valueOf(row_second_count));

		Random rand2 = new Random();
		int rand_int3 = rand2.nextInt(1000);

		sendKeys(txt_group_replaced, groupData);

		if (isDisplayed(txt_group_replaced)) {

			writeTestResults("Verify the ability of creating new asset group parameter",
					"User should be able to release a asset group parameter",
					"new fixed asset group parameter released", "pass");
		}

		else {

			writeTestResults("Verify the ability of creating new asset group parameter",
					"User should be able to release a asset group parameter",
					"new fixed asset group parameter not released", "fail");
		}

		click(btn_update);

		Thread.sleep(3000);

		selectText(txt_GroupName, groupData);

	}

	// Smoke_Fixed_Asset_03
	// Verify the ability of creating a new fixed asset group

	public void creatingNewFixedAssetGroup() throws Exception {

		click(btn_fixedAssetGroupConfig);
		Thread.sleep(3000);
		click(btn_newFixedAssetGroupConfig);
		Thread.sleep(3000);
		click(btn_plus1);

//		click(btn_plus2);

		int rowCount = driver.findElements(By.xpath(assetGroup_tbl)).size();

		for (int i = 1; i <= rowCount; i++) {
			// row count
		}

		String btn_asset_group_plus_mark_second = btn_plus2.replace("row", String.valueOf(rowCount));
		click(btn_asset_group_plus_mark_second);
		Thread.sleep(3000);
		int row_second_count = rowCount + 1;
		String txt_group_replaced = txt_group.replace("row", String.valueOf(row_second_count));

		Random rand3 = new Random();
		int rand_int4 = rand3.nextInt(1000);
		int rand_int5 = rand3.nextInt(1000);
		groupData = groupData + Integer.toString(rand_int4)+Integer.toString(rand_int5);

		sendKeys(txt_group_replaced, groupData);

		click(btn_update);

		Thread.sleep(3000);

		selectText(txt_GroupName, groupData);

		selectIndex(txt_Book, 3);

		click(btn_fixedAssetBooks);
		Thread.sleep(3000);
		sendKeys(txt_fixedAssetBooks, assetBookCodeData);

		pressEnter(txt_fixedAssetBooks);
		Thread.sleep(3000);
		doubleClick(doubleClickfixedAssetBook);
		Thread.sleep(3000);
		click(btn_post);
		Thread.sleep(3000);
		click(btn_bookView);

		sendKeys(txt_aquisitionValue, quisitionData);

		click(btn_applyGroup);
		Thread.sleep(3000);
		click(btn_draft);

		Thread.sleep(2000);

		click(btn_release);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		Thread.sleep(2000);

		if (isDisplayed(label_GroupConfig)) {

			writeTestResults("Verify the ability of creating new asset group parameter",
					"User should be able to release a asset group parameter",
					"new fixed asset group parameter released", "pass");
		}

		else {

			writeTestResults("Verify the ability of creating new asset group parameter",
					"User should be able to release a asset group parameter",
					"new fixed asset group parameter not released", "fail");
		}

	}

	// 3 n 4 should run together
	// Smoke_Fixed_Asset_04
	// Verify the ability of creating a new fixed asset information

	public void creatingNewFixedAssetInformation() throws Exception {

		click(btn_fixedAssetInformationCreate);
		Thread.sleep(3000);
		click(btn_newFixedAssetInformation);
		Thread.sleep(3000);
		sendKeys(txt_InfoDescription, InfoDescriptionData);

		Thread.sleep(2000);

		selectText(txt_AssetGroupName, groupData);

		sendKeys(txt_quantity, quantityData);

		sendKeys(txt_perUnitCost, perUnitCostData);

		selectIndex(txt_barcodeBook, 0);

		click(btn_assetBookTab);
		Thread.sleep(3000);
		click(btn_fixedAssetBooksearch);

		sendKeys(txt_fixedAssetBooks, assetBookCodeData);

		pressEnter(txt_fixedAssetBooks);
		Thread.sleep(3000);
		doubleClick(doubleClickfixedAssetBook);
		Thread.sleep(3000);
		click(btn_bookView);

		sendKeys(txt_aquisitionValue, quisitionData);

		click(btn_disposalAndDepreciation);
		Thread.sleep(3000);
		click(btn_scheduleCodeSearch);
		Thread.sleep(3000);
		sendKeys(txt_scheduleCode, scheduleCodeData);

		pressEnter(txt_scheduleCode);
		Thread.sleep(3000);
		doubleClick(doubleClickscheduleCodeInfo);
		Thread.sleep(3000);
		click(btn_responsiblePerson);

		sendKeys(txt_responsiblePerson, responsiblePersonData);

		pressEnter(txt_responsiblePerson);
		Thread.sleep(3000);
		doubleClick(doubleClickresponsiblePerson);
		Thread.sleep(3000);
		click(btn_applyInfo);

//		click(btn_post);

		click(btn_draft);
		Thread.sleep(3000);
		Thread.sleep(2000);

		click(btn_release);
		Thread.sleep(3000);
		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		Thread.sleep(2000);

		if (isDisplayed(label_fixedAssetInfoRelease)) {

			writeTestResults("Verify the ability of creating a new fixed asset information",
					"User should be able to release a fixed asset.", "new fixed asset information released", "pass");
		}

		else {

			writeTestResults("Verify the ability of creating a new fixed asset information",
					"User should be able to release a fixed asset.", "new fixed asset information not released",
					"fail");
		}

	}

	// Action Verification Test Cases

	// Action verification fixed asset group

	public void actiondeleteFixedAssetGroup() throws Exception {

		click(btn_fixedAssetGroupConfig);

		click(btn_newFixedAssetGroupConfig);
		Thread.sleep(2000);

		click(btn_plus1);

//			click(btn_plus2);

		int rowCount = driver.findElements(By.xpath(assetGroup_tbl)).size();

		for (int i = 1; i <= rowCount; i++) {
			// row count
		}

		String btn_asset_group_plus_mark_second = btn_plus2.replace("row", String.valueOf(rowCount));
		click(btn_asset_group_plus_mark_second);

		int row_second_count = rowCount + 1;
		String txt_group_replaced = txt_group.replace("row", String.valueOf(row_second_count));

		Random rand4 = new Random();
		int rand_int5 = rand4.nextInt(1000);

		groupData = groupData + Integer.toString(rand_int5);

		sendKeys(txt_group_replaced, groupData);

		click(btn_update);

		Thread.sleep(3000);

		selectText(txt_GroupName, groupData);

		selectIndex(txt_Book, 3);

		click(btn_fixedAssetBooks);

		sendKeys(txt_fixedAssetBooks, assetBookCodeData);

		pressEnter(txt_fixedAssetBooks);

		Thread.sleep(2000);

		doubleClick(doubleClickfixedAssetBook);

		click(btn_post);

		click(btn_bookView);

		sendKeys(txt_aquisitionValue, quisitionData);

		click(btn_applyGroup);

		click(btn_draft);

		if (isDisplayed(txt_draft)) {

			writeTestResults("Verify that the user can draft the asset group successfully", "asset group drafted",
					"asset group drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the asset group successfully", "asset group drafted",
					"asset group not drafted", "fail");
		}

		Thread.sleep(2000);

		click(btn_action);

		click(btn_Delete);

		click(btn_yes);

		if (isDisplayed(txt_delete)) {

			writeTestResults("Verify that the user can delete the asset group successfully", "asset group deleted",
					"asset group deleted", "pass");
		} else {

			writeTestResults("Verify that the user can release the asset group successfully", "asset group deleted",
					"asset group not deleted", "fail");
		}

		Thread.sleep(2000);

		click(btn_duplicate);

		if (isDisplayed(txt_duplicate)) {

			writeTestResults("Verify that the user can duplicate the asset group successfully",
					"asset group duplicated", "asset group duplicated", "pass");
		} else {

			writeTestResults("Verify that the user can duplicate the fixed asset book successfully",
					"asset group duplicated", "asset group not duplicated", "fail");
		}

		Thread.sleep(2000);

		click(btn_plus1);

//		click(btn_plus2);

		int rowCount1 = driver.findElements(By.xpath(assetGroup_tbl)).size();

		for (int i = 1; i <= rowCount1; i++) {
			// row count
		}

		String btn_asset_group_plus_marksecond = btn_plus2.replace("row", String.valueOf(rowCount1));
		click(btn_asset_group_plus_marksecond);

		int row_secondcount = rowCount1 + 1;
		String txt_groupreplaced = txt_group.replace("row", String.valueOf(row_secondcount));

		Random rand = new Random();
		int rand1_int2 = rand.nextInt(1000);

		groupData = groupData + Integer.toString(rand1_int2);

		sendKeys(txt_groupreplaced, groupData);

		click(btn_update);

		Thread.sleep(3000);

		selectText(txt_GroupName, groupData);

		Thread.sleep(2000);

		click(btn_draft);

		Thread.sleep(3000);

		if (isDisplayed(txt_draft)) {

			writeTestResults("Verify that the user can draft asset group successfully", "fixed asset book drafted",
					"asset group drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the asset group successfully", "asset group drafted",
					"asset group not drafted", "fail");
		}

		Thread.sleep(2000);

		click(btn_edit);

		if (isDisplayed(txt_edit)) {

			writeTestResults("Verify that the user can edit the asset group successfully", "asset group edited",
					"asset group edited", "pass");
		} else {

			writeTestResults("Verify that the user can edit the asset group successfully", "asset group edited",
					"asset group not edited", "fail");
		}

		Thread.sleep(3000);

		selectText(txt_GroupName, groupData);

		selectIndex(txt_Book, 3);

		click(btn_fixedAssetBooks);

		sendKeys(txt_fixedAssetBooks, assetBookCodeData);

		pressEnter(txt_fixedAssetBooks);

		doubleClick(doubleClickfixedAssetBook);

		click(btn_bookView);

		sendKeys(txt_aquisitionValue, quisitionData);

		click(btn_applyGroup);

		Thread.sleep(2000);

		click(btn_updateNNew);

		Thread.sleep(2000);

		if (isDisplayed(txt_updateNNewBOM)) {

			writeTestResults("Verify that the user can update asset group and create new successfully",
					"asset group updated and create a new", "asset group updated and create a new", "pass");
		} else {

			writeTestResults("Verify that the user can update the asset group and create new successfully",
					"asset group updated and create a new", "asset group not updated and create a new", "fail");
		}

		Thread.sleep(2000);

		click(btn_fixedAssetBooks);

		sendKeys(txt_fixedAssetBooks, assetBookCodeData);

		pressEnter(txt_fixedAssetBooks);

		doubleClick(doubleClickfixedAssetBook);

		click(btn_post);

		click(btn_bookView);

		sendKeys(txt_aquisitionValue, quisitionData);

		click(btn_applyGroup);

		click(btn_plus1);

//		click(btn_plus2);

		int rowCount5 = driver.findElements(By.xpath(assetGroup_tbl)).size();

		for (int i = 1; i <= rowCount5; i++) {
			// row count
		}

		String btn_second = btn_plus2.replace("row", String.valueOf(rowCount5));
		click(btn_second);

		int row_count1 = rowCount5 + 1;
		String txt_replace = txt_group.replace("row", String.valueOf(row_count1));

		Random rand5 = new Random();
		int rand_int1 = rand5.nextInt(1000);

		groupData = groupData + Integer.toString(rand_int1);

		sendKeys(txt_replace, groupData);

		click(btn_update);

		selectText(txt_GroupName, groupData);

		Thread.sleep(2000);

		click(btn_draft);

		Thread.sleep(2000);

		click(btn_release);

		Thread.sleep(2000);

		if (isDisplayed(label_GroupConfig)) {

			writeTestResults("Verify the ability of creating new asset group parameter",
					"User should be able to release a asset group parameter",
					"new fixed asset group parameter released", "pass");
		}

		else {

			writeTestResults("Verify the ability of creating new asset group parameter",
					"User should be able to release a asset group parameter",
					"new fixed asset group parameter not released", "fail");
		}

	}

	public void actioneditFixedAssetGroup() throws Exception {

		click(btn_fixedAssetGroupConfig);

		click(btn_newFixedAssetGroupConfig);
		Thread.sleep(2000);

		click(btn_plus1);

//			click(btn_plus2);

		int rowCount = driver.findElements(By.xpath(assetGroup_tbl)).size();

		for (int i = 1; i <= rowCount; i++) {
			// row count
		}

		String btn_asset_group_plus_mark_second = btn_plus2.replace("row", String.valueOf(rowCount));
		click(btn_asset_group_plus_mark_second);

		int row_second_count = rowCount + 1;
		String txt_group_replaced = txt_group.replace("row", String.valueOf(row_second_count));

		Random rand1 = new Random();
		int rand_int2 = rand1.nextInt(100000);

		groupData = groupData + Integer.toString(rand_int2);

		sendKeys(txt_group_replaced, groupData);

		click(btn_update);

		Thread.sleep(3000);

		selectText(txt_GroupName, groupData);

		selectIndex(txt_Book, 3);

		click(btn_fixedAssetBooks);

		sendKeys(txt_fixedAssetBooks, assetBookCodeData);

		pressEnter(txt_fixedAssetBooks);

		doubleClick(doubleClickfixedAssetBook);

		click(btn_post);

		click(btn_bookView);

		sendKeys(txt_aquisitionValue, quisitionData);

		click(btn_applyGroup);

		click(btn_draft);

		Thread.sleep(3000);

		if (isDisplayed(txt_draft)) {

			writeTestResults("Verify that the user can draft the fixed asset group successfully", "asset group drafted",
					"asset group drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the asset group successfully", "asset group drafted",
					"asset group not drafted", "fail");
		}

		Thread.sleep(3000);

		click(btn_edit);

		Thread.sleep(2000);

		if (isDisplayed(txt_edit)) {

			writeTestResults("Verify that the user can edit the asset group successfully", "asset group edited",
					"asset group edited", "pass");
		} else {

			writeTestResults("Verify that the user can edit the asset group successfully", "asset group edited",
					"asset group not edited", "fail");
		}

		Thread.sleep(3000);

		selectText(txt_GroupName, groupData);

		selectIndex(txt_Book, 3);

		click(btn_fixedAssetBooks);

		Thread.sleep(2000);

		sendKeys(txt_fixedAssetBooks, assetBookCodeData);

		pressEnter(txt_fixedAssetBooks);

		Thread.sleep(3000);

		doubleClick(doubleClickfixedAssetBook);

		Thread.sleep(3000);

		click(btn_bookView);

		sendKeys(txt_aquisitionValue, quisitionData);

		click(btn_applyGroup);

		Thread.sleep(2000);

		click(btn_Update);

		Thread.sleep(2000);
		if (isDisplayed(txt_update)) {

			writeTestResults("Verify that the user can update the asset group successfully", "asset group updated",
					"asset group updated", "pass");
		} else {

			writeTestResults("Verify that the user can update the asset group successfully", "asset group updated",
					"asset group not updated", "fail");
		}

		click(btn_duplicate);

		Thread.sleep(2000);

		if (isDisplayed(txt_duplicate)) {

			writeTestResults("Verify that the user can duplicate the asset group successfully",
					"asset group duplicated", "asset group duplicated", "pass");
		} else {

			writeTestResults("Verify that the user can duplicate the asset group successfully",
					"asset group duplicated", "asset group not duplicated", "fail");
		}

		Thread.sleep(2000);
		click(btn_plus1);

//		click(btn_plus2);

		int rowCount6 = driver.findElements(By.xpath(assetGroup_tbl)).size();

		for (int i = 1; i <= rowCount6; i++) {
			// row count
		}

		String btn_group_plus_mark_second = btn_plus2.replace("row", String.valueOf(rowCount6));
		click(btn_group_plus_mark_second);

		int rowsecond_count = rowCount6 + 1;
		String txt_replaced = txt_group.replace("row", String.valueOf(rowsecond_count));

		Random ran1 = new Random();
		int ranint2 = ran1.nextInt(1000);

		groupData = groupData + Integer.toString(ranint2);

		sendKeys(txt_replaced, groupData);

		click(btn_update);

		Thread.sleep(5000);

		selectText(txt_GroupName, groupData);

		click(btn_draftNNew);

		Thread.sleep(2000);

		if (isDisplayed(txt_draftNNew)) {

			writeTestResults("Verify that the user can draft the asset group and create new successfully",
					"asset group drafted and create a new", "asset group drafted and create a new", "pass");
		} else {

			writeTestResults("Verify that the user can draft the asset group and create new successfully",
					"asset group drafted and create a new", "asset group not drafted and create a new", "fail");
		}

		click(btn_plus1);

//		click(btn_plus2);

		int rowCount7 = driver.findElements(By.xpath(assetGroup_tbl)).size();

		for (int i = 1; i <= rowCount7; i++) {
			// row count
		}

		String btnasset_group_plus_mark_second = btn_plus2.replace("row", String.valueOf(rowCount7));
		click(btnasset_group_plus_mark_second);

		int rowSecond_count = rowCount7 + 1;
		String txtGroup_replaced = txt_group.replace("row", String.valueOf(rowSecond_count));

		Random randd = new Random();
		int ran1d_int2 = randd.nextInt(100000);

		groupData = groupData + Integer.toString(ran1d_int2);

		sendKeys(txtGroup_replaced, groupData);

		click(btn_update);

		Thread.sleep(3000);

		selectText(txt_GroupName, groupData);

		selectIndex(txt_Book, 3);

		click(btn_fixedAssetBooks);

		sendKeys(txt_fixedAssetBooks, assetBookCodeData);

		pressEnter(txt_fixedAssetBooks);

		doubleClick(doubleClickfixedAssetBook);

		click(btn_post);

		click(btn_bookView);

		sendKeys(txt_aquisitionValue, quisitionData);

		click(btn_applyGroup);

		click(btn_copyFrom);

		if (isDisplayed(txt_copyFrom)) {

			writeTestResults("Verify that the user can take a copy from the previously created asset group",
					"take a copy from the previously created asset group",
					"take a copy from the previously created asset group", "pass");
		} else {

			writeTestResults("Verify that the user can take a copy from the previously created asset group",
					"take a copy from the previously created asset group",
					"can't take a copy from the previously created asset group", "fail");
		}

		Thread.sleep(2000);

		sendKeys(txt_fxGroup, fxGroupData);

		pressEnter(txt_fxGroup);
		Thread.sleep(2000);

		doubleClick(doublfxGroup);

		Thread.sleep(2000);

		click(btn_plus1);

		Thread.sleep(3000);

//		click(btn_plus2);

		int rowCount9 = driver.findElements(By.xpath(assetGroup_tbl)).size();

		for (int i = 1; i <= rowCount9; i++) {
			// row count
		}

		String btnassetgroup_plus_mark_second = btn_plus2.replace("row", String.valueOf(rowCount9));
		click(btnassetgroup_plus_mark_second);

		int row_count = rowCount9 + 1;
		String txtreplaced = txt_group.replace("row", String.valueOf(row_count));

		Random rand9 = new Random();
		int ran1dint2 = rand9.nextInt(100000);

		groupData = groupData + Integer.toString(ran1dint2);

		sendKeys(txtreplaced, groupData);

		click(btn_update);

		Thread.sleep(3000);

		selectText(txt_GroupName, groupData);

		selectIndex(txt_book, 1);

		click(btn_draft);

		Thread.sleep(3000);

		click(btn_release);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(txt_fxBook)) {

			writeTestResults("Verify that the user can release the asset group successfully", "asset group released",
					"asset group released", "pass");
		} else {

			writeTestResults("Verify that the user can release the asset group successfully", "asset group released",
					"asset group not released", "fail");
		}

	}

	// Action verification fixed asset book

	public void actiondeleteFixedAssetBook() throws Exception {

		click(btn_fixedAssetBookCreation);

		click(btn_newfixedAssetBook);

		Random rand1 = new Random();
		int rand_int2 = rand1.nextInt(10000);

		assetBookCodeData = assetBookCodeData + Integer.toString(rand_int2);

		sendKeys(txt_assetBookCode, assetBookCodeData);

		click(btn_desc);

		sendKeys(txt_desc, descData);

		click(btn_applyBook);

		click(btn_dateA);

		click(btn_aquisitionDate);

		click(btn_depreciationClick);

		click(btn_depreciationStartDate);

		click(btn_dateB);

		click(btn_scheduleCode);

		sendKeys(txt_scheduleCode, scheduleCodeData);

		pressEnter(txt_scheduleCode);

		doubleClick(doubleClickscheduleCode);

		click(btn_responsiblePerson);

		sendKeys(txt_responsiblePerson, responsiblePersonData);

		pressEnter(txt_responsiblePerson);

		doubleClick(doubleClickresponsiblePerson1);

		sendKeys(txt_usefulLife1, usefulLifeData1);

		click(btn_draft);

		if (isDisplayed(label_fixedAssetBookdraft)) {

			writeTestResults("Verify the ability of creating a new fixed asset book.",
					"User should be able to release a fixed asset book.", "new fixed asset book released", "pass");
		}

		else {

			writeTestResults("Verify the ability of creating a new fixed asset book.",
					"User should be able to release a fixed asset book.", "new fixed asset book not released", "fail");
		}

		Thread.sleep(2000);

		click(btn_action);

		click(btn_Delete);

		click(btn_yes);

		if (isDisplayed(txt_delete)) {

			writeTestResults("Verify that the user can delete the fixed asset book successfully",
					"bill of materials deleted", "fixed asset book deleted", "pass");
		} else {

			writeTestResults("Verify that the user can release the fixed asset book successfully",
					"fixed asset book deleted", "fixed asset book not deleted", "fail");
		}

		Thread.sleep(2000);

		click(btn_duplicate);

		if (isDisplayed(txt_duplicate)) {

			writeTestResults("Verify that the user can duplicate the fixed asset book successfully",
					"fixed asset book duplicated", "fixed asset book duplicated", "pass");
		} else {

			writeTestResults("Verify that the user can duplicate the fixed asset book successfully",
					"fixed asset book duplicated", "fixed asset book not duplicated", "fail");
		}

		Thread.sleep(2000);

		Random rand3 = new Random();
		int rand_int = rand3.nextInt(100000);

		assetBookCodeData = assetBookCodeData + Integer.toString(rand_int);

		sendKeys(txt_assetBookCode, assetBookCodeData);

		click(btn_draft);

		if (isDisplayed(txt_draft)) {

			writeTestResults("Verify that the user can draft fixed asset book successfully", "fixed asset book drafted",
					"fixed asset book drafted", "pass");
		} else {

			writeTestResults("Verify that the user can draft the fixed asset book successfully",
					"fixed asset book drafted", "fixed asset book not drafted", "fail");
		}

		Thread.sleep(2000);

		click(btn_edit);

		if (isDisplayed(txt_edit)) {

			writeTestResults("Verify that the user can edit the fixed asset book successfully",
					"fixed asset book edited", "fixed asset book edited", "pass");
		} else {

			writeTestResults("Verify that the user can edit the fixed asset book successfully",
					"fixed asset book edited", "fixed asset book not edited", "fail");
		}

		Thread.sleep(3000);

		click(btn_updateNNew);

		if (isDisplayed(txt_updateNNewBOM)) {

			writeTestResults("Verify that the user can update fixed asset book and create new successfully",
					"fixed asset book updated and create a new", "fixed asset book updated and create a new", "pass");
		} else {

			writeTestResults("Verify that the user can update the fixed asset book and create new successfully",
					"fixed asset book updated and create a new", "fixed asset book not updated and create a new",
					"fail");
		}

		Random rand = new Random();
		int rand_int5 = rand.nextInt(10000);

		assetBookCodeData1 = assetBookCodeData1 + Integer.toString(rand_int5);

		sendKeys(txt_assetBookCode, assetBookCodeData1);

		click(btn_desc);

		sendKeys(txt_desc, descData);

		click(btn_applyBook);

		click(btn_dateA);

		click(btn_aquisitionDate);

		click(btn_depreciationClick);

		click(btn_depreciationStartDate);

		click(btn_dateB);

		click(btn_scheduleCode);

		sendKeys(txt_scheduleCode, scheduleCodeData);

		pressEnter(txt_scheduleCode);

		doubleClick(doubleClickscheduleCode);

		click(btn_responsiblePerson);

		sendKeys(txt_responsiblePerson, responsiblePersonData);

		pressEnter(txt_responsiblePerson);

		doubleClick(doubleClickresponsiblePerson1);

		sendKeys(txt_usefulLife1, usefulLifeData1);

		click(btn_draft);

		Thread.sleep(2000);

		click(btn_release);

		Thread.sleep(2000);

		if (isDisplayed(label_fixedAssetBook)) {

			writeTestResults("Verify the ability of creating a new fixed asset book.",
					"User should be able to release a fixed asset book.", "new fixed asset book released", "pass");
		}

		else {

			writeTestResults("Verify the ability of creating a new fixed asset book.",
					"User should be able to release a fixed asset book.", "new fixed asset book not released", "fail");
		}
	}

	public void actioneditFixedAssetBook() throws Exception {

		click(btn_fixedAssetBookCreation);

		click(btn_newfixedAssetBook);

		Random rand1 = new Random();
		int rand_int2 = rand1.nextInt(10000);

		assetBookCodeData = assetBookCodeData + Integer.toString(rand_int2);

		sendKeys(txt_assetBookCode, assetBookCodeData);

		click(btn_desc);

		sendKeys(txt_desc, descData);

		click(btn_applyBook);

		click(btn_dateA);

		click(btn_aquisitionDate);

		click(btn_depreciationClick);

		click(btn_depreciationStartDate);

		click(btn_dateB);

		click(btn_scheduleCode);

		sendKeys(txt_scheduleCode, scheduleCodeData);

		pressEnter(txt_scheduleCode);

		doubleClick(doubleClickscheduleCode);

		click(btn_responsiblePerson);

		sendKeys(txt_responsiblePerson, responsiblePersonData);

		pressEnter(txt_responsiblePerson);

		doubleClick(doubleClickresponsiblePerson1);

		sendKeys(txt_usefulLife1, usefulLifeData1);

		click(btn_draft);

		Thread.sleep(3000);

		if (isDisplayed(label_fixedAssetBookdraft)) {

			writeTestResults("Verify the ability of creating a new fixed asset book.",
					"User should be able to draft a fixed asset book.", "new fixed asset book drafted", "pass");
		}

		else {

			writeTestResults("Verify the ability of creating a new fixed asset book.",
					"User should be able to draft a fixed asset book.", "new fixed asset book not drafted", "fail");
		}

		click(btn_edit);

		Thread.sleep(2000);

		if (isDisplayed(txt_edit)) {

			writeTestResults("Verify that the user can edit the fixed asset book successfully",
					"fixed asset book edited", "fixed asset book edited", "pass");
		} else {

			writeTestResults("Verify that the user can edit the fixed asset book successfully",
					"fixed asset book edited", "fixed asset book not edited", "fail");
		}

		Random rand5 = new Random();
		int rand5_int2 = rand5.nextInt(10000);

		assetBookCodeData = assetBookCodeData + Integer.toString(rand5_int2);

		sendKeys(txt_assetBookCode, assetBookCodeData);

		click(btn_Update);

		Thread.sleep(2000);
		if (isDisplayed(txt_update)) {

			writeTestResults("Verify that the user can update the fixed asset book successfully",
					"fixed asset book updated", "fixed asset book updated", "pass");
		} else {

			writeTestResults("Verify that the user can update the fixed asset book successfully",
					"fixed asset book updated", "fixed asset book not updated", "fail");
		}

		click(btn_duplicate);

		Thread.sleep(2000);

		if (isDisplayed(txt_duplicate)) {

			writeTestResults("Verify that the user can duplicate the fixed asset book successfully",
					"fixed asset book duplicated", "fixed asset book duplicated", "pass");
		} else {

			writeTestResults("Verify that the user can duplicate the fixed asset book successfully",
					"fixed asset book duplicated", "fixed asset book not duplicated", "fail");
		}

		Random rand8 = new Random();
		int rand8_int2 = rand8.nextInt(10000);

		assetBookCodeData1 = assetBookCodeData1 + Integer.toString(rand8_int2);

		sendKeys(txt_assetBookCode, assetBookCodeData1);

		click(btn_draftNNew);

		Thread.sleep(2000);

		if (isDisplayed(txt_draftNNew)) {

			writeTestResults("Verify that the user can draft the fixed asset book and create new successfully",
					"fixed asset book drafted and create a new", "fixed asset book drafted and create a new", "pass");
		} else {

			writeTestResults("Verify that the user can draft the fixed asset book and create new successfully",
					"fixed asset book drafted and create a new", "fixed asset book not drafted and create a new",
					"fail");
		}

		Random rand9 = new Random();
		int rand9_int2 = rand9.nextInt(10000);

		assetBookCodeData1 = assetBookCodeData1 + Integer.toString(rand9_int2);

		sendKeys(txt_assetBookCode, assetBookCodeData1);

		click(btn_copyFrom);

		if (isDisplayed(txt_copyFromBook)) {

			writeTestResults("Verify that the user can take a copy from the previously created fixed asset book",
					"take a copy from the previously created fixed asset book",
					"take a copy from the previously created fixed asset book", "pass");
		} else {

			writeTestResults("Verify that the user can take a copy from the previously created fixed asset book",
					"take a copy from the previously created fixed asset book",
					"can't take a copy from the previously created fixed asset book", "fail");
		}

		Thread.sleep(2000);

		sendKeys(txt_fxcopy, fxcopyData);

		pressEnter(txt_fxcopy);
		Thread.sleep(2000);

		doubleClick(doublfxcopy);

		Thread.sleep(2000);

		Random rand3 = new Random();
		int rand3_int2 = rand3.nextInt(10000);

		assetBookCodeData1 = assetBookCodeData1 + Integer.toString(rand3_int2);

		sendKeys(txt_assetBookCode, assetBookCodeData1);

		click(btn_draft);

		Thread.sleep(3000);

		click(btn_release);

		trackCode = getText("//label[@id='lblTemplateFormHeader']");

		if (isDisplayed(txt_fxBook)) {

			writeTestResults("Verify that the user can release the fixed asset book successfully",
					"fixed asset book released", "fixed asset book released", "pass");
		} else {

			writeTestResults("Verify that the user can release the fixed asset book successfully",
					"fixed asset book released", "fixed asset book not released", "fail");
		}

	}

	// Action verification fixed asset information

	public void actiondeleteFixedAssetInfo() throws Exception {

		click(btn_fixedAssetInfo);

		click(btn_newfixedAssetInfo);

		sendKeys(txt_fixedAssetInfoDesc, fixedAssetInfoDescData);

		click(btn_plusMark);

		Thread.sleep(3000);

		int rowCount9 = driver.findElements(By.xpath(assetGroup_tbl)).size();

		for (int i = 1; i <= rowCount9; i++) {
			// row count
		}

		String btnassetgroup_plus_mark_second = btn_plus2.replace("row", String.valueOf(rowCount9));
		click(btnassetgroup_plus_mark_second);

		int row_count = rowCount9 + 1;
		String txtreplaced = txt_group.replace("row", String.valueOf(row_count));

		Random rand9 = new Random();
		int ran1dint2 = rand9.nextInt(100000);

		groupData = groupData + Integer.toString(ran1dint2);

		sendKeys(txtreplaced, groupData);

		click(btn_update);

		Thread.sleep(4000);

		selectText(txt_GroupName, groupData);

		selectIndex(txt_barcodeBook, 0);

		click(btn_assetBookTab);

		click(btn_fixedAssetBooksearch);

		sendKeys(txt_fixedAssetBooks, assetBookCodeData);

		pressEnter(txt_fixedAssetBooks);

		Thread.sleep(2000);

		doubleClick(doubleClickfixedAssetBook);

		click(btn_post);

		click(btn_bookView);

		sendKeys(txt_aquisitionValue, quisitionData);

		click(btn_disposalAndDepreciation);

		click(btn_scheduleCodeSearch);

		sendKeys(txt_scheduleCode, scheduleCodeData);

		pressEnter(txt_scheduleCode);

		Thread.sleep(2000);

		doubleClick(doubleClickschedulecode);

		click(btn_responsiblePerson);

		sendKeys(txt_responsiblePerson, responsiblePersonData);

		pressEnter(txt_responsiblePerson);

		doubleClick(doubleClickresponsiblePerson);

		click(btn_applyInfo);

		click(btn_draft);

		Thread.sleep(2000);

		click(btn_action);

		click(btn_Delete);

		click(btn_yes);

		Thread.sleep(3000);

		if (isDisplayed(txt_delete)) {

			writeTestResults("Verify that the user can delete the asset information successfully",
					"asset information deleted", "asset information deleted", "pass");
		} else {

			writeTestResults("Verify that the user can release the asset information successfully",
					"asset information deleted", "asset information not deleted", "fail");
		}

	}

	public void actionupdateFixedAssetInfo() throws Exception {

		click(btn_fixedAssetInfo);

		click(btn_newfixedAssetInfo);

		sendKeys(txt_fixedAssetInfoDesc, fixedAssetInfoDescData);

		click(btn_plusMark);

		Thread.sleep(3000);

		int rowCount9 = driver.findElements(By.xpath(assetGroup_tbl)).size();

		for (int i = 1; i <= rowCount9; i++) {
			// row count
		}

		String btnassetgroup_plus_mark_second = btn_plus2.replace("row", String.valueOf(rowCount9));
		click(btnassetgroup_plus_mark_second);

		int row_count = rowCount9 + 1;
		String txtreplaced = txt_group.replace("row", String.valueOf(row_count));

		Random rand9 = new Random();
		int ran1dint2 = rand9.nextInt(100000);

		groupData = groupData + Integer.toString(ran1dint2);

		sendKeys(txtreplaced, groupData);

		click(btn_update);

		Thread.sleep(4000);

		selectText(txt_GroupName, groupData);

		selectIndex(txt_barcodeBook, 0);

		click(btn_assetBookTab);

		click(btn_fixedAssetBooksearch);

		sendKeys(txt_fixedAssetBooks, assetBookCodeData);

		pressEnter(txt_fixedAssetBooks);

		Thread.sleep(2000);

		doubleClick(doubleClickfixedAssetBook);

		click(btn_post);

		click(btn_bookView);

		sendKeys(txt_aquisitionValue, quisitionData);

		click(btn_disposalAndDepreciation);

		click(btn_scheduleCodeSearch);

		sendKeys(txt_scheduleCode, scheduleCodeData);

		pressEnter(txt_scheduleCode);

		Thread.sleep(2000);

		doubleClick(doubleClickschedulecode);

		click(btn_responsiblePerson);

		sendKeys(txt_responsiblePerson, responsiblePersonData);

		pressEnter(txt_responsiblePerson);

		doubleClick(doubleClickresponsiblePerson);

		click(btn_applyInfo);

		click(btn_draft);

		click(btn_release);

		fxInfoNo = getText(txt_fxInfoNo);

		Thread.sleep(4000);

		click(btn_action);

		click(btn_resource);

		Random random = new Random();
		int randint2 = random.nextInt(100000);

		resourceCodeData = resourceCodeData + Integer.toString(randint2);

		sendKeys(txt_resourceCode, resourceCodeData);

		sendKeys(txt_resourceDesc, resourceDescData);

		selectIndex(txt_resourceGroup, 1);

		click(btn_draft);

		click(btn_release);

	}

	public void actionBarcode() throws Exception {

		Thread.sleep(3000);

		click(navigateMenu);

		click(fixedAsset_btn);

		click(btn_fixedAssetInfo);

		sendKeys(txt_fixedAssetInfo, fxInfoNo);

		pressEnter(txt_fixedAssetInfo);
		Thread.sleep(3000);
		click(doubleClickSegmentNo.replace("number", fxInfoNo));

		Thread.sleep(3000);
		click(btn_action);

		click(btn_barCode);

		Thread.sleep(3000);
		if (isDisplayed(txt_barcode)) {

			writeTestResults("Verify that the user can click barcode button successfully", "barcode button clicked",
					"barcode button clicked", "pass");
		} else {

			writeTestResults("Verify that the user can click barcode button successfully", "barcode button clicked",
					"barcode button not clicked", "fail");
		}

		pageRefersh();

		Thread.sleep(3000);

		click(navigateMenu);

		click(fixedAsset_btn);

		click(btn_fixedAssetInfo);

		sendKeys(txt_fixedAssetInfo, fxInfoNo);

		pressEnter(txt_fixedAssetInfo);
		Thread.sleep(3000);
		click(doubleClickSegmentNo.replace("number", fxInfoNo));

		click(btn_action);

		click(btn_transfer);

		if (isDisplayed(txt_transfer)) {

			writeTestResults("Verify that the user can click transfer button successfully", "transfer button clicked",
					"transfer button clicked", "pass");
		} else {

			writeTestResults("Verify that the user can click transfer button successfully", "transfer button clicked",
					"transfer button not clicked", "fail");
		}

		click("//span[@class='headerclose']");

		click(btn_action);

		click(btn_disposal);

		click(txt_disposalDate);

		doubleClick(doubleClick_disposalDate);

		selectText(txt_disposalMode, disposalModeDataWriteOff);

		click(btn_applyAssetInfoDisposal);

		click("//span[@class='headerclose']");

		click(btn_action);

		click(btn_Revaluation);

		selectIndex(btn_AssetBook, 1);

		sendKeys(txt_revalueAmount, RevalueAmountData);

		Thread.sleep(3000);
		sendKeys(txt_usefulLife, usefulLifeData);

		Thread.sleep(2000);

		click(btn_applyAssetInfo);

	}

	/* New vendor creation */
	public void vendorInfo() throws Exception {

		click(btn_procurement);

		click(btn_vendorInfo);

		click(newVendor);

		selectIndex(txt_vendorType, 1);

		selectIndex(txt_vendorName, 2);

		sendKeys(txt_vendorName1, vendorName1Data);

		selectIndex(txt_vendorGroup, 3);

		click(btn_draft);

		click(btn_release);
	}

	/* New warehouse creation */
	public void warehouseInfo() throws Exception {

		click(btn_inventory);

		click(btn_warehouseInfo);

		click(btn_newWarehouse);

		Random rand = new Random();

		// Generate random integers in range 0 to 999
		int rand_int1 = rand.nextInt(10000000);

		warehouseCodeData = warehouseCodeData + Integer.toString(rand_int1);

		sendKeys(txt_warehouseCode, warehouseCodeData);

		sendKeys(txt_wraehousename, warehousenameData);

		click(btn_draft);

		click(btn_release);
	}
	
	//FA_TC_010to011
	public void VerifyThatUserCanDeleteDraftedFixedAssetInformationWhichIsGeneratdThroughPOtoPIServiceJourney() throws Exception {
		
		WaitClick(btn_procurement);
		click(btn_procurement);
		WaitClick(btn_purchaseOrder);
		click(btn_purchaseOrder);

		WaitElement(label_PurchaseOrder);
		if (isDisplayed(label_PurchaseOrder)) {

			writeTestResults("Verify that user can view purchase order page", 
					"user can view purchase order page",
					"user can view purchase order page successfully", "pass");
		}

		else {

			writeTestResults("Verify that user can view purchase order page", 
					"user can view purchase order page",
					"user can view purchase order page Fail", "fail");
		}
		
		WaitClick(btn_newPurchaseOrder);
		click(btn_newPurchaseOrder);

		WaitElement(tab_journey);
		if (isDisplayed(tab_journey)) {

			writeTestResults(" Verify that the user can navigate to journey view", 
					"user can navigate to journey view",
					"user can navigate to journey view Successfully", "pass");
		} else {

			writeTestResults(" Verify that the user can navigate to journey view", 
					"user can navigate to journey view",
					"user can navigate to journey view Fail", "fail");
		}

		WaitClick(btn_newServiceJourney);
		click(btn_newServiceJourney);

		if (isDisplayed(label_PurchaseOrder1)) {
			writeTestResults(" Verify that the user can navigate to Purchase Order by-page",
					"User can navigate to purchase order", "purchase order page is loaded", "pass");
		} else {
			writeTestResults("Verify that the user can navigate to Purchase Order by-page",
					"User can navigate to purchase order", "purchase order page is not loaded", "fail");
		}
		
		// Select Vendor
		WaitClick(btn_vendorSearch);
		click(btn_vendorSearch);
		sendKeys(txt_vendorSearch, vendorData);
		pressEnter(txt_vendorSearch);
		WaitElement(sel_vendorSearch.replace("VenName", vendorData));
		doubleClick(sel_vendorSearch.replace("VenName", vendorData));
		
		// Select Vendor ref No
		sendKeys(txt_venderRefNo, vendorRefData);
		
		ScrollDownAruna();
		
		WaitElement(txt_Product);
		sendKeys(txt_Product, Product1);
		Thread.sleep(4000);
		driver.findElement(getLocator(txt_Product)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(4000);
		pressEnter(txt_Product);
		sendKeys(txt_ProductQty, "1");
		Thread.sleep(4000);
		pressEnter(txt_ProductQty);
		
		WaitClick(btn_advance);
		click(btn_advance);

		WaitElement(label_advance);
		if (isDisplayed(label_advance)) {
			writeTestResults("Verify that the user can navigate Advance Popup", 
					"User can navigate Advance Popup",
					"User can navigate Advance Popup Successfully", "pass");
		} else {
			writeTestResults("Verify that the user can navigate Advance Popup", 
					"User can navigate to Advance page",
					"User can navigate Advance Popup Fail", "fail");
		}
		
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$(closeMessageBox()).click()");
		
		WaitClick(btn_fixedAss);
		click(btn_fixedAss);
		WaitElement(btn_assetGrp);
		selectText(btn_assetGrp, assetGrpData);
		WaitClick(btn_apply);
		click(btn_apply);
		
		WaitClick(btn_CheckoutBtn);
		click(btn_CheckoutBtn);
		
		WaitClick(btn_DraftBtn);
		click(btn_DraftBtn);
		
		WaitElement(txt_pageDraft);
		if (isDisplayed(txt_pageDraft)){

			writeTestResults("Verify Purchase Order Document Draft",
					"Purchase Order Document Draft", 
					"Purchase Order Document is Draft", "pass");
		} else {
			writeTestResults("Verify Purchase Order Document Draft",
					"Purchase Order Document Draft", 
					"Purchase Order Document is not Draft", "fail");
		}
		
		WaitClick(btn_release);
		click(btn_release);
		Thread.sleep(2000);
		trackCode= getText(txt_DocHeader);
		
		WaitElement(btn_GoToPageBtn);
		if (isDisplayed(btn_GoToPageBtn)){

			writeTestResults("Verify Purchase Order Document Released",
					"Purchase Order Document Released", 
					"Purchase Order Document is Released", "pass");
		} else {
			writeTestResults("Verify Purchase Order Document Released",
					"Purchase Order Document Released", 
					"Purchase Order Document is not Released", "fail");
		}
		
		WaitClick(btn_GoToPageBtn);
		click(btn_GoToPageBtn);
		
		switchWindow();
		
		WaitClick(btn_CheckoutBtn);
		click(btn_CheckoutBtn);
		
		WaitClick(btn_DraftBtn);
		click(btn_DraftBtn);
		
		WaitElement(txt_pageDraft);
		if (isDisplayed(txt_pageDraft)){

			writeTestResults("Verify Purchase Invoice Document Draft",
					"Purchase Invoice Document Draft", 
					"Purchase Invoice Document is Draft", "pass");
		} else {
			writeTestResults("Verify Purchase Invoice Document Draft",
					"Purchase Invoice Document Draft", 
					"Purchase Invoice Document is not Draft", "fail");
		}
		
		Thread.sleep(3000);
		WaitClick(btn_release);
		click(btn_release);
		Thread.sleep(2000);
		trackCode= getText(txt_DocHeader);
		
		WaitElement(txt_pageRelease);
		if (isDisplayed(txt_pageRelease)){

			writeTestResults("Verify Purchase Invoice Document Released",
					"Purchase Invoice Document Released", 
					"Purchase Invoice Document is Released", "pass");
		} else {
			writeTestResults("Verify Purchase Invoice Document Released",
					"Purchase Invoice Document Released", 
					"Purchase Invoice Document is not Released", "fail");
		}
		
		//======================================================FA_TC_010
		
		WaitClick(btn_actionPro);
		click(btn_actionPro);
		WaitClick(btn_journal);
		click(btn_journal);
		WaitElement(label_journalEntryDetails);
		if (isDisplayed(label_journalEntryDetails)) {

			writeTestResults(" Verify that user Should be able to click on journal entry",
					"Journal entry should be displayed", 
					"Journal entry displayed", "pass");
		} else {
			writeTestResults("Verify that user Should be able to click on journal entry",
					"Journal entry should be displayed", 
					"Journal entry is not displayed", "fail");
		}
		
		WaitClick(txt_FA);
		click(txt_FA);
		
		switchWindow();
		
		Thread.sleep(4000);

		WaitClick(btn_fixedAssSegment);
		click(btn_fixedAssSegment);
		WaitElement(txt_segmentNo);
		segmentNo = getText(txt_segmentNo);
		WaitClick(btn_close);
		click(btn_close);
		
		navigateToSideBar();
		WaitClick(fixedAsset_btn);
		click(fixedAsset_btn);
		WaitClick(btn_fixedAssetInfo);
		click(btn_fixedAssetInfo);
		
		WaitElement(txt_fixedAssetInfo);
		sendKeys(txt_fixedAssetInfo, segmentNo);
		pressEnter(txt_fixedAssetInfo);
		WaitElement(doubleClickSegmentNo.replace("number", segmentNo));
		doubleClick(doubleClickSegmentNo.replace("number", segmentNo));
		
		WaitClick(btn_ActionBtn);
		click(btn_ActionBtn);
		WaitClick(btn_Delete);
		click(btn_Delete);
		WaitClick(btn_yes);
		click(btn_yes);
		
		WaitElement(txt_pageDelete);
		if (isDisplayed(txt_pageDelete)) {

			writeTestResults("Verify that user can delete drafted fixed asset information which is generatd through PO > PI (service ) journey",
					"user can delete drafted fixed asset information which is generatd through PO > PI (service ) journey", 
					"user can delete drafted fixed asset information which is generatd through PO > PI (service ) journey Successfully", "pass");
		} else {
			writeTestResults("Verify that user can delete drafted fixed asset information which is generatd through PO > PI (service ) journey",
					"user can delete drafted fixed asset information which is generatd through PO > PI (service ) journey", 
					"user can delete drafted fixed asset information which is generatd through PO > PI (service ) journey Fail", "fail");
		}
		
		//======================================================FA_TC_011
		
		switchWindow();
		
		pageRefersh();
		WaitClick(btn_ActionBtn);
		click(btn_ActionBtn);
		WaitClick(btn_ReverseBtn);
		click(btn_ReverseBtn);
		WaitClick(btn_ReversePopupBtn);
		click(btn_ReversePopupBtn);
		WaitClick(btn_yes);
		click(btn_yes);		
		
		WaitElement(txt_pageReverse);
		if (isDisplayed(txt_pageReverse)) {

			writeTestResults("Verify that user can reverse the purchase invoice, which respective attached fixed asset information has been deleted",
					"user can reverse the purchase invoice, which respective attached fixed asset information has been deleted", 
					"user can reverse the purchase invoice, which respective attached fixed asset information has been deleted Successfully", "pass");
		} else {
			writeTestResults("Verify that user can reverse the purchase invoice, which respective attached fixed asset information has been deleted",
					"user can reverse the purchase invoice, which respective attached fixed asset information has been deleted", 
					"user can reverse the purchase invoice, which respective attached fixed asset information has been deleted Fail", "fail");
		}
	}
	
	//FA_TC_012to013
	public void VerifyThatUserCanDeleteDraftedFixedAssetInformationWhichIsGeneratdThroughIOIDOWIPAssetJourney() throws Exception {
		
		WaitClick(btn_inventory);
		click(btn_inventory);
		
		WaitClick(btn_internalOrder);
		click(btn_internalOrder);
		
		WaitElement(label_InernalOrder);
		if (isDisplayed(label_InernalOrder)) {
			writeTestResults("Verify that user can navigate to Internal Order page",
					"Navigate to Internal Order page", 
					"Navigate to Internal Order page Successfully", "pass");
		}else {
			writeTestResults("Verify that user can navigate to Internal Order by-page",
					"Navigate to Internal Order page", 
					"Navigate to Internal Order page Fail", "fail");
		}
		
		WaitClick(btn_newInternalOrder);
		click(btn_newInternalOrder);

		WaitElement(tab_internalOrderjourney);
		if (isDisplayed(tab_internalOrderjourney)) {
			writeTestResults(" Verify that the user can navigate to Journeys", 
					"List Journey should be displayed",
					"journeys are loaded", "pass");
		} else {
			writeTestResults(" Verify that the user can navigate to journeys", 
					"List Journey should be displayed",
					"journeys are not loaded", "fail");
		}

		WaitClick(btn_CapitalWIPRequestJourney);
		click(btn_CapitalWIPRequestJourney);
		
		WaitElement(txt_title);
		LocalTime Obj1 = LocalTime.now();
		sendKeys(txt_title, "Title"+Obj1);
		
		WaitClick(btn_requester);
		click(btn_requester);
		WaitElement(txt_requesterSearch);
		sendKeys(txt_requesterSearch, requesterData);
		pressEnter(txt_requesterSearch);
		WaitElement(sel_requesterSel.replace("ReqName", requesterData));
		doubleClick(sel_requesterSel.replace("ReqName", requesterData));
		
		ScrollDownAruna();
		
		WaitElement(txt_Product);
		sendKeys(txt_Product, Product2);
		Thread.sleep(4000);
		driver.findElement(getLocator(txt_Product)).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(4000);
		pressEnter(txt_Product);
		sendKeys(txt_ProductQty, "1");
		Thread.sleep(4000);
		pressEnter(txt_ProductQty);
		Thread.sleep(4000);
		WaitClick(btn_advanceInventory);
		click(btn_advanceInventory);

		WaitElement(label_advanceInventory);
		if (isDisplayed(label_advanceInventory)) {
			writeTestResults("Verify that the user can navigate Advance Popup", 
					"User can navigate Advance Popup",
					"User can navigate Advance Popup Successfully", "pass");
		} else {
			writeTestResults("Verify that the user can navigate Advance Popup", 
					"User can navigate to Advance page",
					"User can navigate Advance Popup Fail", "fail");
		}
		
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$(closeMessageBox()).click()");
		
		Thread.sleep(4000);
		WaitClick(btn_fixedAss);
		click(btn_fixedAss);
		WaitElement(btn_assetGrp);
		selectText(btn_assetGrp, assetGrpData);
		WaitClick(btn_apply);
		click(btn_apply);
		
		WaitClick(btn_DraftBtn);
		click(btn_DraftBtn);
		
		WaitElement(txt_pageDraft);
		if (isDisplayed(txt_pageDraft)){

			writeTestResults("Verify Internal Order Document Draft",
					"Internal Order Document Draft", 
					"Internal Order Document is Draft", "pass");
		} else {
			writeTestResults("Verify Internal Order Document Draft",
					"Internal Order Document Draft", 
					"Internal Order Document is not Draft", "fail");
		}
		
		WaitClick(btn_release);
		click(btn_release);
		Thread.sleep(2000);
		trackCode= getText(txt_DocHeader);
		
		WaitElement(btn_GoToPageBtn);
		if (isDisplayed(btn_GoToPageBtn)){

			writeTestResults("Verify Internal Order Document Released",
					"Internal Order Document Released", 
					"Internal Order Document is Released", "pass");
		} else {
			writeTestResults("Verify Internal Order Document Released",
					"Internal Order Document Released", 
					"Internal Order Document is not Released", "fail");
		}
		
		WaitClick(btn_GoToPageBtn);
		click(btn_GoToPageBtn);
		
		switchWindow();
		
		WaitElement(txt_shipping);
		sendKeys(txt_shipping, "18/B/77/2,Dirumpitiya,Gatahaththa");
		
		WaitClick(btn_DraftBtn);
		click(btn_DraftBtn);
		
		WaitElement(txt_pageDraft);
		if (isDisplayed(txt_pageDraft)){

			writeTestResults("Verify Internal Dispatch Order Document Draft",
					"Internal Dispatch Order Document Draft", 
					"Internal Dispatch Order Document is Draft", "pass");
		} else {
			writeTestResults("Verify Internal Dispatch Order Document Draft",
					"Internal Dispatch Order Document Draft", 
					"Internal Dispatch Order Document is not Draft", "fail");
		}
		
		Thread.sleep(3000);
		WaitClick(btn_release);
		click(btn_release);
		Thread.sleep(2000);
		trackCode= getText(txt_DocHeader);
		
		WaitElement(txt_pageRelease);
		if (isDisplayed(txt_pageRelease)){

			writeTestResults("Verify Internal Dispatch Order Document Released",
					"Internal Dispatch Order Document Released", 
					"Internal Dispatch Order Document is Released", "pass");
		} else {
			writeTestResults("Verify Internal Dispatch Order Document Released",
					"Internal Dispatch Order Document Released", 
					"Internal Dispatch Order Document is not Released", "fail");
		}
		
		//======================================================FA_TC_012
		
		WaitClick(btn_actionPro);
		click(btn_actionPro);
		WaitClick(btn_journal);
		click(btn_journal);
		WaitElement(label_journalEntryDetails);
		if (isDisplayed(label_journalEntryDetails)) {

			writeTestResults(" Verify that user Should be able to click on journal entry",
					"Journal entry should be displayed", 
					"Journal entry displayed", "pass");
		} else {
			writeTestResults("Verify that user Should be able to click on journal entry",
					"Journal entry should be displayed", 
					"Journal entry is not displayed", "fail");
		}
		
		WaitClick(txt_FA);
		click(txt_FA);
		
		switchWindow();
		
		Thread.sleep(4000);

		WaitClick(btn_fixedAssSegment);
		click(btn_fixedAssSegment);
		WaitElement(txt_segmentNo);
		segmentNo = getText(txt_segmentNo);
		WaitClick(btn_close);
		click(btn_close);
		
		navigateToSideBar();
		WaitClick(fixedAsset_btn);
		click(fixedAsset_btn);
		WaitClick(btn_fixedAssetInfo);
		click(btn_fixedAssetInfo);
		
		WaitElement(txt_fixedAssetInfo);
		sendKeys(txt_fixedAssetInfo, segmentNo);
		pressEnter(txt_fixedAssetInfo);
		WaitElement(doubleClickSegmentNo.replace("number", segmentNo));
		doubleClick(doubleClickSegmentNo.replace("number", segmentNo));
		
		WaitClick(btn_ActionBtn);
		click(btn_ActionBtn);
		WaitClick(btn_Delete);
		click(btn_Delete);
		WaitClick(btn_yes);
		click(btn_yes);
		
		WaitElement(txt_pageDelete);
		if (isDisplayed(txt_pageDelete)) {

			writeTestResults("Verify that user can delete drafted fixed asset information which is generatd through IO > IDO (WIP Asset) journey",
					"user can delete drafted fixed asset information which is generatd through IO > IDO (WIP Asset) journey", 
					"user can delete drafted fixed asset information which is generatd through IO > IDO (WIP Asset) journey Successfully", "pass");
		} else {
			writeTestResults("Verify that user can delete drafted fixed asset information which is generatd through IO > IDO (WIP Asset) journey",
					"user can delete drafted fixed asset information which is generatd through IO > IDO (WIP Asset) journey", 
					"user can delete drafted fixed asset information which is generatd through IO > IDO (WIP Asset) journey Fail", "fail");
		}
		
		//======================================================FA_TC_013
		
		switchWindow();
		
		pageRefersh();
		WaitClick(btn_ActionBtn);
		click(btn_ActionBtn);
		WaitClick(btn_ReverseBtn);
		click(btn_ReverseBtn);
		JavascriptExecutor j7 = (JavascriptExecutor)driver;
		j7.executeScript("$(\"#txtrdcmnReverseDate\").datepicker(\"setDate\", new Date())");
		WaitClick(btn_ReversePopupBtn);
		click(btn_ReversePopupBtn);
		WaitClick(btn_yes);
		click(btn_yes);		
		
		WaitElement(txt_pageReverse);
		if (isDisplayed(txt_pageReverse)) {

			writeTestResults("Verify that user can reverse the Internal Dispatch Order, which  respective attached fixed asset information has been deleted",
					"user can reverse the Internal Dispatch Order, which  respective attached fixed asset information has been deleted", 
					"user can reverse the Internal Dispatch Order, which  respective attached fixed asset information has been deleted Successfully", "pass");
		} else {
			writeTestResults("Verify that user can reverse the Internal Dispatch Order, which  respective attached fixed asset information has been deleted",
					"user can reverse the Internal Dispatch Order, which  respective attached fixed asset information has been deleted", 
					"user can reverse the Internal Dispatch Order, which  respective attached fixed asset information has been deleted Fail", "fail");
		}
	}

	//scrollDown
	public void ScrollDownAruna() throws Exception {
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,400)");
	}
	
	//scrollUp
	public void ScrollUpAruna() throws Exception {
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,-400)");
	}

	//WaitClick
	public void WaitClick(String Locator)  throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Locator)));
	}

	//WaitElement
	public void WaitElement(String Locator)  throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator)));
	}
}
