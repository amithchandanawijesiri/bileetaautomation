package bileeta.BTAF.PageObjects;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.ClickAction;

import bileeta.BATF.Pages.ProjectModuleDataSmoke;

public class ProjectModuleSmoke extends ProjectModuleDataSmoke {
	/* Common Objects */
	InventoryAndWarehouseModule invObj = new InventoryAndWarehouseModule();
	FinanceModule finObj = new FinanceModule();

	/* Variables */
	String projectQuatationTotlValue;
	String serviceInvoiceInvoiceProposals;
	/* write production data */
	private static Workbook wb;
	private static Sheet sh;
	private static FileInputStream fis;
	private static FileOutputStream fos;
	private static Row row;
	private static Cell cell;
	private static Cell cell1;
	private static Cell cell2;

	/* Common Methods */
	public void writeProjectData(String name, String variable, int data_row)
			throws InvalidFormatException, IOException {
		String path = "";
		String location = System.getProperty("user.dir");

		String real_project_path = "*src*test*java*projectModule*projectData.xlsx".replace("*", "\\");
		path = location + real_project_path;

		fis = new FileInputStream(path);
		wb = WorkbookFactory.create(fis);
		sh = wb.getSheet("Sheet1");
		row = sh.createRow(data_row);
		cell = row.createCell(1);
		cell1 = row.createCell(0);
		cell.setCellValue(variable);
		cell1.setCellValue(name);
		/* System.out.println(cell.getStringCellValue()); */
		fos = new FileOutputStream(path);
		wb.write(fos);
		fos.flush();
		fos.close();
	}

	public String readProjectData(String name) throws IOException, InvalidFormatException {
		String path = "";
		String location = System.getProperty("user.dir");

		String real_project_path = "*src*test*java*projectModule*projectData.xlsx".replace("*", "\\");
		path = location + real_project_path;

		fis = new FileInputStream(path);
		wb = WorkbookFactory.create(fis);
		sh = wb.getSheet("Sheet1");

		Map<String, String> iw_creation_map = new HashMap<String, String>();
		List<Map<String, String>> iw_creation = new ArrayList<Map<String, String>>();

		int totRows = sh.getLastRowNum();
		for (i = 0; i <= totRows; i++) {
			row = sh.getRow(i);
			cell = row.getCell(0);
			cell2 = row.getCell(1);

			String test_creation_row1 = cell.getStringCellValue();
			String test_creation_row2 = cell2.getStringCellValue();

			iw_creation_map.put(test_creation_row1, test_creation_row2);
			iw_creation.add(i, iw_creation_map);

		}

		String iw_info = iw_creation.get(0).get(name);

		return iw_info;
	}

	public void setInvObj(InventoryAndWarehouseModule invObj) {
		this.invObj = invObj;
	}

	public void loginProjectSmoke() throws Exception {
		openPage(entutionURL);
		Thread.sleep(2000);
		explicitWait(txt_username, 40);
		sendKeys(txt_username, automationTenantProjectSmokeUsername);
		Thread.sleep(1000);
		sendKeys(txt_password, automationTenantProjectSmokePassword);
		Thread.sleep(2000);
		click(btn_login);
		explicitWait(btn_navigationPane, 40);
		if (isDisplayedQuickCheck(div_loginVerification)) {
			openPage(siteURL);
			Thread.sleep(2000);
			explicitWait(txt_username, 40);
			sendKeys(txt_username, automationTenantProjectSmokeUsername);
			Thread.sleep(1000);
			sendKeys(txt_password, automationTenantProjectSmokePassword);
			Thread.sleep(2000);
			click(btn_login);
			explicitWait(btn_navigationPane, 40);
		}
	}

	/* Smoke_Project_001 */
	public void projectQuatation1_Smoke_Project_001() throws Exception {
		loginProjectSmoke();
		click(btn_navigationPane);
		explicitWait(btn_projectModule, 10);
		if (isDisplayed(btn_projectModule)) {
			writeTestResults("Verify that Project module is displayed", "Project module should be display",
					"Project module successfully displayed", "pass");
		} else {
			writeTestResults("Verify that Project module is displayed", "Project module should be display",
					"Project module doesn't displayed", "fail");
		}

		click(btn_projectModule);
		explicitWait(btn_projectQuatation, 10);
		if (isDisplayed(btn_projectQuatation)) {
			writeTestResults("Verify that Project Quatation option is displayed",
					"Project Quatation option should be display", "Project Quatation option successfully displayed",
					"pass");
		} else {
			writeTestResults("Verify that Project Quatation option is displayed",
					"Project Quatation option should be display", "Project Quatation option doesn't displayed", "fail");
		}

		click(btn_projectQuatation);
		explicitWait(header_projectQuatationByPage, 10);
		if (isDisplayed(header_projectQuatationByPage)) {
			writeTestResults("Verify that user able to navigate Project Quatation by-page",
					"User should be able to navigate Project Quatation by-page",
					"User successfully navigate Project Quatation by-page", "pass");
		} else {
			writeTestResults("Verify that user able to navigate Project Quatation by-page",
					"User should be able to navigate Project Quatation by-page",
					"User doesn't navigate Project Quatation by-page", "fail");
		}

		click(btn_newProjectQuatation);
		explicitWait(header_newProjectQuatation, 10);
		if (isDisplayed(header_newProjectQuatation)) {
			writeTestResults("Verify that user able to navigate Project Quatation new form",
					"User should be able to navigate Project Quatation new form",
					"User successfully navigate Project Quatation new form", "pass");
		} else {
			writeTestResults("Verify that user able to navigate Project Quatation new form",
					"User should be able to navigate Project Quatation new form",
					"User doesn't navigate Project Quatation new form", "fail");
		}

		click(btn_customerLookup);
		sendKeysLookup(txt_searchCustomer, customer);
		pressEnter(txt_searchCustomer);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", customer), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", customer));

		Thread.sleep(1500);
		String selectedCustomer = getAttribute(txt_customerAccountFrontPageProjectQuatation, "value");
		if (selectedCustomer.contains(customer)) {
			writeTestResults("Verify that user able to select the Customer",
					"User should be able to select the Customer", "User successfully select the Customer", "pass");
		} else {
			writeTestResults("Verify that user able to select the Customer",
					"User should be able to select the Customer", "User doesn't select the Customer", "fail");

		}

		selectText(drop_salesUnitProjectQuatation, salesUnit);
		Thread.sleep(1000);
		String selecteSalesUnit = getSelectedOptionInDropdown(drop_salesUnitProjectQuatation);
		if (selecteSalesUnit.equals(salesUnit)) {
			writeTestResults("Verify that user able to select the Sales Unit",
					"User should be able to select the Sales Unit", "User successfully select the Sales Unit", "pass");
		} else {
			writeTestResults("Verify that user able to select the Sales Unit",
					"User should be able to select the Sales Unit", "User doesn't select the Sales Unit", "fail");

		}

		/* Line 01 */
		selectText(drop_typeOnGridProjectQuatationRowReplace.replace("row", "1"), typeInputProducts);
		Thread.sleep(1000);
		String selectedType = getSelectedOptionInDropdown(
				drop_typeOnGridProjectQuatationRowReplace.replace("row", "1"));
		if (selectedType.equals(typeInputProducts)) {
			writeTestResults("Verify that user able to select the type as Input Product",
					"User should be able to select the type as Input Product",
					"User successfully select the type as Input Product", "pass");
		} else {
			writeTestResults("Verify that user able to select the type as Input Product",
					"User should be able to select the type as Input Product",
					"User doesn't select the type as Input Product", "fail");

		}

		click(btn_productLookupOnGridRowReplace.replace("row", "1"));
		sendKeysLookup(txt_searchProduct, batchFifoProduct);
		pressEnter(txt_searchProduct);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", batchFifoProduct), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", batchFifoProduct));

		Thread.sleep(1500);
		String selectedProduct = getText(lbl_productOnGridProductQuatationRowReplace.replace("row", "1"));
		if (selectedProduct.contains(batchFifoProduct)) {
			writeTestResults("Verify that user able to select the Input Product",
					"User should be able to select the Input Product", "User successfully select the Input Product",
					"pass");
		} else {
			writeTestResults("Verify that user able to select the Input Product",
					"User should be able to select the Input Product", "User doesn't select the Input Product", "fail");

		}

		String ohQuantity = getAttribute(lbl_OHOnGridProductQuatation, "value");
		if (ohQuantity.equals("0")) {
			writeTestResults("Verify that Input Product quantity is available",
					"Input Product quantity should be available", "Input Product quantity not available", "fail");
		}

		sendKeys(txt_quantityOnGridProductQuatationRowReplace.replace("row", "1"), quantityFive);
		String enteredQty = getAttribute(txt_quantityOnGridProductQuatationRowReplace.replace("row", "1"), "value");
		if (enteredQty.equals(quantityFive)) {
			writeTestResults("Verify user can enter the input product Qty",
					"User should be able to enter the input product Qty",
					"User successfully enter the input product Qty", "pass");
		} else {
			writeTestResults("Verify user can enter the input product Qty",
					"User should be able to enter the input product Qty", "User doesn't enter the input product Qty",
					"fail");
		}

		sendKeys(txt_unitPriceProductQuatationRowReplace.replace("row", "1"), inputProductUnitPrice);
		String enteredUnitPriceInputProduct = getAttribute(txt_unitPriceProductQuatationRowReplace.replace("row", "1"),
				"value");
		if (enteredUnitPriceInputProduct.equals(inputProductUnitPrice)) {
			writeTestResults("Verify user can enter the input product Unit Price",
					"User should be able to enter the input product Unit Price",
					"User successfully enter the input product Unit Price", "pass");
		} else {
			writeTestResults("Verify user can enter the input product Unit Price",
					"User should be able to enter the input product Unit Price",
					"User doesn't enter the input product Unit Price", "fail");
		}

		/* Line 02 */
		click(btn_addNewRecordGrid);

		explicitWait(drop_typeOnGridProjectQuatationRowReplace.replace("row", "2"), 6);
		selectText(drop_typeOnGridProjectQuatationRowReplace.replace("row", "2"), typeLabour);
		Thread.sleep(1000);
		String selectedTypeRowTwo = getSelectedOptionInDropdown(
				drop_typeOnGridProjectQuatationRowReplace.replace("row", "2"));
		if (selectedTypeRowTwo.equals(typeLabour)) {
			writeTestResults("Verify that user able to select the type as Labour",
					"User should be able to select the type as Labour", "User successfully select the type as Labour",
					"pass");
		} else {
			writeTestResults("Verify that user able to select the type as Labour",
					"User should be able to select the type as Labour", "User doesn't select the type as Labour",
					"fail");

		}

		selectText(drop_referenceGrouopOnGridRowReplce.replace("row", "2"), referenceGroupLabour);
		Thread.sleep(1000);
		String selectedReferenceGroupRow2 = getSelectedOptionInDropdown(
				drop_referenceGrouopOnGridRowReplce.replace("row", "2"));
		if (selectedReferenceGroupRow2.equals(referenceGroupLabour)) {
			writeTestResults("Verify that user able to select the Reference Group",
					"User should be able to select the Reference Group", "User successfully select the Reference Group",
					"pass");
		} else {
			writeTestResults("Verify that user able to select the Reference Group",
					"User should be able to select the Reference Group", "User doesn't select the Reference Group",
					"fail");

		}

		click(btn_labourLookupProductQuatation);
		sendKeysLookup(txt_labourSearch, employee);
		pressEnter(txt_labourSearch);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", employee), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", employee));

		Thread.sleep(1500);
		String selectedLabour = getText(txt_employeeOnGridProductQuatation);
		if (selectedLabour.equals(employeeName)) {
			writeTestResults("Verify that user able to select the Labour", "User should be able to select the Labour",
					"User successfully select the Labour", "pass");
		} else {
			writeTestResults("Verify that user able to select the Labour", "User should be able to select the Labour",
					"User doesn't select the Labour", "fail");

		}

		click(btn_productLookupOnGridRowReplace.replace("row", "2"));
		sendKeysLookup(txt_searchProduct, serviceProduct);
		pressEnter(txt_searchProduct);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", serviceProduct), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", serviceProduct));

		Thread.sleep(1500);
		String selectedServiceProductLabourLine = getText(
				lbl_productOnGridProductQuatationRowReplace.replace("row", "2"));
		if (selectedServiceProductLabourLine.contains(serviceProduct)) {
			writeTestResults("Verify that user able to select the Service Product for the labour line",
					"User should be able to select the Service Product for the labour line",
					"User successfully select the Service Product for the labour line", "pass");
		} else {
			writeTestResults("Verify that user able to select the Service Product for the labour line",
					"User should be able to select the Service Product for the labour line",
					"User doesn't select the Service Product for the labour line", "fail");

		}

		sendKeys(txt_quantityOnGridProductQuatationRowReplace.replace("row", "2"), quantityFive);
		String enteredLaboourHours = getAttribute(txt_quantityOnGridProductQuatationRowReplace.replace("row", "2"),
				"value");
		if (enteredLaboourHours.equals(quantityFive)) {
			writeTestResults("Verify user can enter the Labour Hours", "User should be able to enter the Labour Hours",
					"User successfully enter the Labour Hours", "pass");
		} else {
			writeTestResults("Verify user can enter the Labour Hours", "User should be able to enter the Labour Hours",
					"User doesn't enter the Labour Hours", "fail");
		}

		sendKeys(txt_unitPriceProductQuatationRowReplace.replace("row", "2"), labourUnitPrice);
		String enteredUnitPriceLabour = getAttribute(txt_unitPriceProductQuatationRowReplace.replace("row", "2"),
				"value");
		if (enteredUnitPriceLabour.equals(labourUnitPrice)) {
			writeTestResults("Verify user can enter the labour Unit Price",
					"User should be able to enter the labour Unit Price",
					"User successfully enter the labour Unit Price", "pass");
		} else {
			writeTestResults("Verify user can enter the labour Unit Price",
					"User should be able to enter the labour Unit Price", "User doesn't enter the labour Unit Price",
					"fail");
		}

	}

	public void projectQuatation2_Smoke_Project_001() throws Exception {

		/* Line 03 */
		click(btn_addNewRecordGrid);

		explicitWait(drop_typeOnGridProjectQuatationRowReplace.replace("row", "3"), 6);
		selectText(drop_typeOnGridProjectQuatationRowReplace.replace("row", "3"), typeResource);
		Thread.sleep(1000);
		String selectedTypeRowThree = getSelectedOptionInDropdown(
				drop_typeOnGridProjectQuatationRowReplace.replace("row", "3"));
		if (selectedTypeRowThree.equals(typeResource)) {
			writeTestResults("Verify that user able to select the type as Resourse",
					"User should be able to select the type as Resourse",
					"User successfully select the type as Resourse", "pass");
		} else {
			writeTestResults("Verify that user able to select the type as Resourse",
					"User should be able to select the type as Resourse", "User doesn't select the type as Resourse",
					"fail");

		}

		selectText(drop_referenceGrouopOnGridRowReplce.replace("row", "3"), referenceGroupMotorVehicle);
		Thread.sleep(1000);
		String selectedReferenceGroupRow3 = getSelectedOptionInDropdown(
				drop_referenceGrouopOnGridRowReplce.replace("row", "3"));
		if (selectedReferenceGroupRow3.equals(referenceGroupMotorVehicle)) {
			writeTestResults("Verify that user able to select the Reference Group",
					"User should be able to select the Reference Group", "User successfully select the Reference Group",
					"pass");
		} else {
			writeTestResults("Verify that user able to select the Reference Group",
					"User should be able to select the Reference Group", "User doesn't select the Reference Group",
					"fail");

		}

		click(btn_resourceLookupProductQuatation);
		sendKeysLookup(txt_searchResource, resource);
		pressEnter(txt_searchResource);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", resource), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", resource));

		Thread.sleep(1500);
		String selectedResource = getText(txt_resourseOnGridProductQuatation);
		if (selectedResource.contains(resource)) {
			writeTestResults("Verify that user able to select the Resourse",
					"User should be able to select the Resourse", "User successfully select the Resourse", "pass");
		} else {
			writeTestResults("Verify that user able to select the Resourse",
					"User should be able to select the Resourse", "User doesn't select the Resourse", "fail");

		}

		click(btn_productLookupOnGridRowReplace.replace("row", "3"));
		sendKeysLookup(txt_searchProduct, serviceProduct2);
		pressEnter(txt_searchProduct);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", serviceProduct2), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", serviceProduct2));

		Thread.sleep(1500);
		String selectedServiceProductResorseLine = getText(
				lbl_productOnGridProductQuatationRowReplace.replace("row", "3"));
		if (selectedServiceProductResorseLine.contains(serviceProduct2)) {
			writeTestResults("Verify that user able to select the Service Product for the resourse line",
					"User should be able to select the Service Product for the resourse line",
					"User successfully select the Service Product for the resourse line", "pass");
		} else {
			writeTestResults("Verify that user able to select the Service Product for the resourse line",
					"User should be able to select the Service Product for the resourse line",
					"User doesn't select the Service Product for the resourse line", "fail");

		}

		sendKeys(txt_quantityOnGridProductQuatationRowReplace.replace("row", "3"), quantityFive);
		String enteredResourseHours = getAttribute(txt_quantityOnGridProductQuatationRowReplace.replace("row", "3"),
				"value");
		if (enteredResourseHours.equals(quantityFive)) {
			writeTestResults("Verify user can enter the Resourse Hours",
					"User should be able to enter the Resourse Hours", "User successfully enter the Resourse Hours",
					"pass");
		} else {
			writeTestResults("Verify user can enter the Resourse Hours",
					"User should be able to enter the Resourse Hours", "User doesn't enter the Resourse Hours", "fail");
		}

		sendKeys(txt_unitPriceProductQuatationRowReplace.replace("row", "3"), resourceUnitPrice);
		String enteredUnitPriceResourse = getAttribute(txt_unitPriceProductQuatationRowReplace.replace("row", "3"),
				"value");
		if (enteredUnitPriceResourse.equals(resourceUnitPrice)) {
			writeTestResults("Verify user can enter the resourse Unit Price",
					"User should be able to enter the resourse Unit Price",
					"User successfully enter the resourse Unit Price", "pass");
		} else {
			writeTestResults("Verify user can enter the resourse Unit Price",
					"User should be able to enter the resourse Unit Price",
					"User doesn't enter the resourse Unit Price", "fail");
		}

		/* Line 04 */
		click(btn_addNewRecordGrid);

		explicitWait(drop_typeOnGridProjectQuatationRowReplace.replace("row", "4"), 6);
		selectText(drop_typeOnGridProjectQuatationRowReplace.replace("row", "4"), typeExpence);
		Thread.sleep(1000);
		String selectedTypeRowFour = getSelectedOptionInDropdown(
				drop_typeOnGridProjectQuatationRowReplace.replace("row", "4"));
		if (selectedTypeRowFour.equals(typeExpence)) {
			writeTestResults("Verify that user able to select the type as Expence",
					"User should be able to select the type as Expence", "User successfully select the type as Expence",
					"pass");
		} else {
			writeTestResults("Verify that user able to select the type as Expence",
					"User should be able to select the type as Expence", "User doesn't select the type as Expence",
					"fail");

		}

		selectIndex(drop_referenceCodeExpence.replace("row", "4"), 1);
		Thread.sleep(1000);
		String selectedReferenceCodeRow4 = getSelectedOptionInDropdown(drop_referenceCodeExpence.replace("row", "4"));
		if (!selectedReferenceCodeRow4.equals("--None--")) {
			writeTestResults("Verify that user able to select the Reference Code",
					"User should be able to select the Reference Code", "User successfully select the Reference Code",
					"pass");
		} else {
			writeTestResults("Verify that user able to select the Reference Code",
					"User should be able to select the Reference Code", "User doesn't select the Reference Code",
					"fail");

		}

		click(btn_productLookupOnGridRowReplace.replace("row", "4"));
		sendKeysLookup(txt_searchProduct, serviceProduct3);
		pressEnter(txt_searchProduct);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", serviceProduct3), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", serviceProduct3));

		Thread.sleep(1500);
		String selectedServiceProductExpenceLine = getText(
				lbl_productOnGridProductQuatationRowReplace.replace("row", "4"));
		if (selectedServiceProductExpenceLine.contains(serviceProduct3)) {
			writeTestResults("Verify that user able to select the Service Product for the expence line",
					"User should be able to select the Service Product for the expence line",
					"User successfully select the Service Product for the expence line", "pass");
		} else {
			writeTestResults("Verify that user able to select the Service Product for the expence line",
					"User should be able to select the Service Product for the expence line",
					"User doesn't select the Service Product for the expence line", "fail");

		}

		sendKeys(txt_unitPriceProductQuatationRowReplace.replace("row", "4"), expenceUnitPrice);
		String enteredUnitPriceExpence = getAttribute(txt_unitPriceProductQuatationRowReplace.replace("row", "4"),
				"value");
		if (enteredUnitPriceExpence.equals(expenceUnitPrice)) {
			writeTestResults("Verify user can enter the expence Unit Price",
					"User should be able to enter the expence Unit Price",
					"User successfully enter the expence Unit Price", "pass");
		} else {
			writeTestResults("Verify user can enter the expence Unit Price",
					"User should be able to enter the expence Unit Price", "User doesn't enter the expence Unit Price",
					"fail");
		}

		/* Line 05 */
		click(btn_addNewRecordGrid);

		explicitWait(drop_typeOnGridProjectQuatationRowReplace.replace("row", "5"), 6);
		selectText(drop_typeOnGridProjectQuatationRowReplace.replace("row", "5"), typeServices);
		Thread.sleep(1000);
		String selectedTypeRowFive = getSelectedOptionInDropdown(
				drop_typeOnGridProjectQuatationRowReplace.replace("row", "5"));
		if (selectedTypeRowFive.equals(typeServices)) {
			writeTestResults("Verify that user able to select the type as Services",
					"User should be able to select the type as Services",
					"User successfully select the type as Services", "pass");
		} else {
			writeTestResults("Verify that user able to select the type as Services",
					"User should be able to select the type as Services", "User doesn't select the type as Services",
					"fail");

		}

		click(btn_productLookupOnGridRowReplace.replace("row", "5"));
		sendKeysLookup(txt_searchProduct, serviceProduct4);
		pressEnter(txt_searchProduct);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", serviceProduct4), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", serviceProduct4));

		Thread.sleep(1500);
		String selectedServiceProductServicesLine = getText(
				lbl_productOnGridProductQuatationRowReplace.replace("row", "5"));
		if (selectedServiceProductServicesLine.contains(serviceProduct4)) {
			writeTestResults("Verify that user able to select the Service Product for the services line",
					"User should be able to select the Service Product for the services line",
					"User successfully select the Service Product for the services line", "pass");
		} else {
			writeTestResults("Verify that user able to select the Service Product for the services line",
					"User should be able to select the Service Product for the services line",
					"User doesn't select the Service Product for the services line", "fail");

		}

		sendKeys(txt_unitPriceProductQuatationRowReplace.replace("row", "5"), servicesUnitPrice);
		String enteredUnitPriceServices = getAttribute(txt_unitPriceProductQuatationRowReplace.replace("row", "5"),
				"value");
		if (enteredUnitPriceServices.equals(servicesUnitPrice)) {
			writeTestResults("Verify user can enter the services Unit Price",
					"User should be able to enter the services Unit Price",
					"User successfully enter the services Unit Price", "pass");
		} else {
			writeTestResults("Verify user can enter the services Unit Price",
					"User should be able to enter the services Unit Price",
					"User doesn't enter the services Unit Price", "fail");
		}

		click(btn_checkout);
		Thread.sleep(3000);

		String gridLabourLineTotal = getAttribute(lbl_gridLineTotalLabour, "value");
		String checkoutBarLabourTotal = getText(lbl_labourTotalCheckoutBar);
		if (gridLabourLineTotal.equals(checkoutBarLabourTotal)) {
			writeTestResults("Verify that labour total is displayed correctly",
					"Labour total should be display correctly", "Labour total correctly displayed", "pass");
		} else {
			writeTestResults("Verify that labour total is displayed correctly",
					"Labour total should be display correctly", "Labour total doesn't correctly displayed", "fail");
		}

		String gridPartsLineTotal = getAttribute(lbl_gridLineTotalParts, "value");
		String checkoutBarPartsTotal = getText(lbl_partsTotalCheckoutBar);
		if (gridPartsLineTotal.equals(checkoutBarPartsTotal)) {
			writeTestResults("Verify that parts total is displayed correctly",
					"parts total should be display correctly", "parts total correctly displayed", "pass");
		} else {
			writeTestResults("Verify that parts total is displayed correctly",
					"parts total should be display correctly", "parts total doesn't correctly displayed", "fail");
		}

		click(btn_draft);
		explicitWait(header_draftedProjectQuatation, 50);
		if (isDisplayed(header_draftedProjectQuatation)) {
			writeTestResults("Verify user able to draft the Project Quatation",
					"User should be able to draft the Project Quatation",
					"User successfully draft the Project Quatation", "pass");
		} else {
			writeTestResults("Verify user able to draft the Project Quatation",
					"User should be able to draft the Project Quatation", "User doesn't draft the Project Quatation",
					"fail");

		}

		click(btn_release);
		explicitWait(header_releasedProjectQuatation, 50);
		trackCode = getText(lbl_docNumber);
		if (isDisplayed(header_releasedProjectQuatation)) {
			writeTestResults("Verify user able to release the Project Quatation",
					"User should be able to release the Project Quatation",
					"User successfully release the Project Quatation", "pass");
		} else {
			writeTestResults("Verify user able to release the Project Quatation",
					"User should be able to release the Project Quatation",
					"User doesn't release the Project Quatation", "fail");

		}

		click(btn_versionProductQuatation);
		Thread.sleep(1000);
		mouseMove(div_versionPopup);
		explicitWait(btn_confirmVersion, 10);
		click(btn_confirmVersion);
		explicitWait(btn_confirmQuatation, 10);
		click(btn_confirmQuatation);

		explicitWait(header_confirmedProjectQuatation, 50);
		if (isDisplayed(header_confirmedProjectQuatation)) {
			writeTestResults("Verify user able to confirm the Project Quatation",
					"User should be able to confirm the Project Quatation",
					"User successfully confirm the Project Quatation", "pass");
		} else {
			writeTestResults("Verify user able to confirm the Project Quatation",
					"User should be able to confirm the Project Quatation",
					"User doesn't confirm the Project Quatation", "fail");

		}

		projectQuatationTotlValue = getText(lbl_totalCheckoutBarProjectQuatation);

	}

	public void convertToProject_Smoke_Project_001() throws Exception {
		click(btn_action);
		explicitWait(btn_convertToProject, 50);
		if (isDisplayed(btn_convertToProject)) {
			writeTestResults("Verify that 'Convert to Project' action is displayed",
					"'Convert to Project' action should display", "'Convert to Project' action successfully display",
					"pass");
		} else {
			writeTestResults("Verify that 'Convert to Project' action is displayed",
					"'Convert to Project' action should display", "'Convert to Project' action doesn't display",
					"fail");

		}
		click(btn_convertToProject);
		Thread.sleep(2000);
		switchWindow();
		explicitWait(header_newProject, 50);
		if (isDisplayed(header_newProject)) {
			writeTestResults("Verify that user able to navigate Project new form",
					"User should be able to navigate Project new form", "User successfully navigate Project new form",
					"pass");
		} else {
			writeTestResults("Verify that user able to navigate Project new form",
					"User should be able to navigate Project new form", "User doesn't navigate Project new form",
					"fail");
		}

		explicitWait(txt_projectCode, 30);
		String projectCode = invObj.currentTimeAndDate().replace("/", "").replace(":", "");
		sendKeys(txt_projectCode, projectCode);
		String enteredProjectCode = getAttribute(txt_projectCode, "value");
		if (enteredProjectCode.equals(projectCode)) {
			writeTestResults("Verify user can enter the Project Code", "User should be able to enter the Project Code",
					"User successfully enter the Project Code", "pass");
		} else {
			writeTestResults("Verify user can enter the Project Code", "User should be able to enter the Project Code",
					"User doesn't enter the Project Code", "fail");
		}

		sendKeys(txt_projectTitle, projectTitle);
		String enteredProjectTitle = getAttribute(txt_projectTitle, "value");
		if (enteredProjectTitle.equals(projectTitle)) {
			writeTestResults("Verify user can enter the Project Title",
					"User should be able to enter the Project Title", "User successfully enter the Project Title",
					"pass");
		} else {
			writeTestResults("Verify user can enter the Project Title",
					"User should be able to enter the Project Title", "User doesn't enter the Project Title", "fail");
		}

		selectIndex(drop_projectGroup, 1);
		Thread.sleep(1000);
		String selectedProjectGroup = getSelectedOptionInDropdown(drop_projectGroup);
		if (!selectedProjectGroup.equals("--None--")) {
			writeTestResults("Verify that user able to select the Project Group",
					"User should be able to select the Project Group", "User successfully select the Project Group",
					"pass");
		} else {
			writeTestResults("Verify that user able to select the Project Group",
					"User should be able to select the Project Group", "User doesn't select the Project Group", "fail");

		}

		String autoSelectedCustomer = getAttribute(txt_customerAccountFrontPageProject, "value");
		if (autoSelectedCustomer.contains("000013")) {
			writeTestResults("Verify that relevent customer is auto selected",
					"Relevent customer should be auto selected", "Relevent customer successfully auto selected",
					"pass");
		} else {
			writeTestResults("Verify that relevent customer is auto selected",
					"Relevent customer should be auto selected", "Relevent customer doesn't auto selected", "fail");
		}

		click(btn_lookupResponsiblePerson);
		sendKeysLookup(txt_labourSearch, employee);
		pressEnter(txt_labourSearch);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", employee), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", employee));

		Thread.sleep(1500);
		String selectedResponsiblePerson = getAttribute(txt_responsiblePersonFrontPageProject, "value");
		if (selectedResponsiblePerson.contains(employee)) {
			writeTestResults("Verify that user able to select the Responsible Person",
					"User should be able to select the Responsible Person",
					"User successfully select the Responsible Person", "pass");
		} else {
			writeTestResults("Verify that user able to select the Responsible Person",
					"User should be able to select the Responsible Person",
					"User doesn't select the Responsible Person", "fail");

		}

		String projectValueOnProject = getAttribute(txt_projectValue, "value");
		if (projectValueOnProject.equals(projectQuatationTotlValue)) {
			writeTestResults("Verify that project value is same as Project Quatation",
					"Project value should be same as Project Quatation", "Project value is same as Project Quatation",
					"pass");
		} else {
			writeTestResults("Verify that project value is same as Project Quatation",
					"Project value should be same as Project Quatation",
					"Project value is not same as Project Quatation", "fail");

		}

		click(btn_pricingProfileLookup);
		sendKeysLookup(txt_searchPricingProfile, pricingProfile);
		pressEnter(txt_searchPricingProfile);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", pricingProfile), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", pricingProfile));

		Thread.sleep(1500);
		String selectedPricingProfile = getAttribute(txt_frontPagePricingProfileProject, "value");
		if (selectedPricingProfile.equals(pricingProfile)) {
			writeTestResults("Verify that user able to select the Pricing Profile",
					"User should be able to select the Pricing Profile", "User successfully select the Pricing Profile",
					"pass");
		} else {
			writeTestResults("Verify that user able to select the Pricing Profile",
					"User should be able to select the Pricing Profile", "User doesn't select the Pricing Profile",
					"fail");

		}

		click(txt_requestDateProject);
		String date = finObj.currentDay() + "_Link";
		click(date);
		String selectedDate = getAttribute(txt_requestDateProject, "value");
		String currentDate = invObj.currentTimeAndDate().substring(0, 10);
		if (selectedDate.equals(currentDate)) {
			writeTestResults("Verify that user able to select the current date as Request Start Date",
					"User should be able to select the current date as Request Start Date",
					"User successfully select the current date as Request Start Date", "pass");
		} else {
			writeTestResults("Verify that user able to select the current date as Request Start Date",
					"User should be able to select the current date as Request Start Date",
					"User doesn't select the current date as Request Start Date", "fail");

		}

		click(btn_locationInformationLookup);
		sendKeysLookup(txt_searchLocation, location);
		pressEnter(txt_searchLocation);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", location), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", location));

		Thread.sleep(1500);
		String selectedLocation = getAttribute(txt_projectLocationFrontPageProject, "value");
		if (selectedLocation.equals(location)) {
			writeTestResults("Verify that user able to select the Location",
					"User should be able to select the Location", "User successfully select the Location", "pass");
		} else {
			writeTestResults("Verify that user able to select the Location",
					"User should be able to select the Location", "User doesn't select the Location", "fail");

		}

		click(btn_draft);
		explicitWait(header_draftedProject, 50);
		if (isDisplayed(header_draftedProject)) {
			writeTestResults("Verify user able to draft the Project", "User should be able to draft the Project",
					"User successfully draft the Project", "pass");
		} else {
			writeTestResults("Verify user able to draft the Project", "User should be able to draft the Project",
					"User doesn't draft the Project", "fail");

		}

		click(btn_release);
		explicitWait(header_releasedProject, 50);
		trackCode = getText(lbl_docNumber);
		if (isDisplayed(header_releasedProject)) {
			writeTestResults("Verify user able to release the Project", "User should be able to release the Project",
					"User successfully release the Project", "pass");
		} else {
			writeTestResults("Verify user able to release the Project", "User should be able to release the Project",
					"User doesn't release the Project", "fail");

		}

		click(btn_edit);
		explicitWait(btn_update, 30);
		if (isDisplayed(header_releasedProject)) {
			writeTestResults("Verify that the document is go to edit mode", "The document should go to edit mode",
					"The document successfully go to edit mode", "pass");
		} else {
			writeTestResults("Verify that the document is go to edit mode", "The document should go to edit mode",
					"The document doesn't go to edit mode", "fail");

		}

		click(tab_workBreackdown);
		explicitWait(btn_expandProjectTask, 20);
		click(btn_expandProjectTask);
		if (isDisplayed(lbl_draftedTaskWorkBreackdownProject)) {
			writeTestResults("Task doesn't display with the draft status",
					"Task should be display with the draft status", "Task successfully display with the draft status",
					"pass");
		} else {
			writeTestResults("Task doesn't display with the draft status",
					"Task should be display with the draft status", "Task doesn't display with the draft status",
					"fail");

		}

		click(btn_releaseTaskWorkBreackdown);
		explicitWait(para_taskReleaseValidationWorkBreackdown, 20);
		if (isDisplayed(para_taskReleaseValidationWorkBreackdown)) {
			writeTestResults("Verify that task release successfull validation is appear",
					"Task release successfull validation should appear",
					"Task release successfull validation is appear", "pass");
		} else {
			writeTestResults("Verify that task release successfull validation is appear",
					"Task release successfull validation should appear",
					"Task release successfull validation doesn't appear", "fail");

		}

		Thread.sleep(3000);
		click(btn_update);
		explicitWait(btn_edit, 30);
		if (isDisplayed(btn_edit)) {
			writeTestResults("Verify that project is updated", "Project should be updated",
					"Project successfully updated", "pass");
		} else {
			writeTestResults("Verify that project is updated", "Project should be updated", "Project doesn't updated",
					"fail");

		}
	}

	/* Smoke_Project_002 */
	public void createProject_Smoke_Project_002() throws Exception {
		loginProjectSmoke();
		click(btn_navigationPane);
		explicitWait(btn_projectModule, 10);
		if (isDisplayed(btn_projectModule)) {
			writeTestResults("Verify that Project module is displayed", "Project module should be display",
					"Project module successfully displayed", "pass");
		} else {
			writeTestResults("Verify that Project module is displayed", "Project module should be display",
					"Project module doesn't displayed", "fail");
		}

		click(btn_projectModule);
		explicitWait(btn_project, 10);
		if (isDisplayed(btn_project)) {
			writeTestResults("Verify that Project option is displayed", "Project option should be display",
					"Project option successfully displayed", "pass");
		} else {
			writeTestResults("Verify that Project option is displayed", "Project option should be display",
					"Project option doesn't displayed", "fail");
		}

		click(btn_project);
		explicitWait(header_projectByPage, 10);
		if (isDisplayed(header_projectByPage)) {
			writeTestResults("Verify that user able to navigate Project by-page",
					"User should be able to navigate Project by-page", "User successfully navigate Project by-page",
					"pass");
		} else {
			writeTestResults("Verify that user able to navigate Project by-page",
					"User should be able to navigate Project by-page", "User doesn't navigate Project by-page", "fail");
		}

		click(btn_newProject);
		explicitWait(header_newProject, 50);
		if (isDisplayed(header_newProject)) {
			writeTestResults("Verify that user able to navigate Project new form",
					"User should be able to navigate Project new form", "User successfully navigate Project new form",
					"pass");
		} else {
			writeTestResults("Verify that user able to navigate Project new form",
					"User should be able to navigate Project new form", "User doesn't navigate Project new form",
					"fail");
		}

		explicitWait(txt_projectCode, 30);
		String projectCode = invObj.currentTimeAndDate().replace("/", "").replace(":", "");
		sendKeys(txt_projectCode, projectCode);
		String enteredProjectCode = getAttribute(txt_projectCode, "value");
		if (enteredProjectCode.equals(projectCode)) {
			writeTestResults("Verify user can enter the Project Code", "User should be able to enter the Project Code",
					"User successfully enter the Project Code", "pass");
		} else {
			writeTestResults("Verify user can enter the Project Code", "User should be able to enter the Project Code",
					"User doesn't enter the Project Code", "fail");
		}

		sendKeys(txt_projectTitle, projectTitle);
		String enteredProjectTitle = getAttribute(txt_projectTitle, "value");
		if (enteredProjectTitle.equals(projectTitle)) {
			writeTestResults("Verify user can enter the Project Title",
					"User should be able to enter the Project Title", "User successfully enter the Project Title",
					"pass");
		} else {
			writeTestResults("Verify user can enter the Project Title",
					"User should be able to enter the Project Title", "User doesn't enter the Project Title", "fail");
		}

		selectIndex(drop_projectGroup, 1);
		Thread.sleep(1000);
		String selectedProjectGroup = getSelectedOptionInDropdown(drop_projectGroup);
		if (!selectedProjectGroup.equals("--None--")) {
			writeTestResults("Verify that user able to select the Project Group",
					"User should be able to select the Project Group", "User successfully select the Project Group",
					"pass");
		} else {
			writeTestResults("Verify that user able to select the Project Group",
					"User should be able to select the Project Group", "User doesn't select the Project Group", "fail");

		}

		click(btn_customerLookupProject);
		sendKeysLookup(txt_searchCustomer, customer);
		pressEnter(txt_searchCustomer);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", customer), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", customer));

		Thread.sleep(1500);
		String selectedCustomer = getAttribute(txt_customerAccountFrontPageProject, "value");
		if (selectedCustomer.contains("000013")) {
			writeTestResults("Verify that user able to select the Customer",
					"User should be able to select the Customer", "User successfully select the Customer", "pass");
		} else {
			writeTestResults("Verify that user able to select the Customer",
					"User should be able to select the Customer", "User doesn't select the Customer", "fail");

		}

		pageScrollUpToTop();
		click(btn_lookupResponsiblePerson);
		sendKeysLookup(txt_labourSearch, employee);
		pressEnter(txt_labourSearch);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", employee), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", employee));

		Thread.sleep(1500);
		String selectedResponsiblePerson = getAttribute(txt_responsiblePersonFrontPageProject, "value");
		if (selectedResponsiblePerson.contains(employee)) {
			writeTestResults("Verify that user able to select the Responsible Person",
					"User should be able to select the Responsible Person",
					"User successfully select the Responsible Person", "pass");
		} else {
			writeTestResults("Verify that user able to select the Responsible Person",
					"User should be able to select the Responsible Person",
					"User doesn't select the Responsible Person", "fail");

		}

		selectText(drop_businessUnitProject, businessUnit);
		Thread.sleep(1000);
		String selectedBusinessUnit = getSelectedOptionInDropdown(drop_businessUnitProject);
		if (selectedBusinessUnit.equals(businessUnit)) {
			writeTestResults("Verify that user able to select the Business Unit",
					"User should be able to select the Business Unit", "User successfully select the Business Unit",
					"pass");
		} else {
			writeTestResults("Verify that user able to select the Business Unit",
					"User should be able to select the Business Unit", "User doesn't select the Business Unit", "fail");

		}

		click(btn_pricingProfileLookup);
		sendKeysLookup(txt_searchPricingProfile, pricingProfile);
		pressEnter(txt_searchPricingProfile);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", pricingProfile), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", pricingProfile));

		Thread.sleep(1500);
		String selectedPricingProfile = getAttribute(txt_frontPagePricingProfileProject, "value");
		if (selectedPricingProfile.equals(pricingProfile)) {
			writeTestResults("Verify that user able to select the Pricing Profile",
					"User should be able to select the Pricing Profile", "User successfully select the Pricing Profile",
					"pass");
		} else {
			writeTestResults("Verify that user able to select the Pricing Profile",
					"User should be able to select the Pricing Profile", "User doesn't select the Pricing Profile",
					"fail");

		}

		click(txt_requestDateProject);
		click("1_Link");
		String selectedDate = getAttribute(txt_requestDateProject, "value");
		String firstDateOfCurrentMonth = invObj.currentTimeAndDate().substring(0, 8) + "01";
		if (selectedDate.equals(firstDateOfCurrentMonth)) {
			writeTestResults("Verify that user able to select the firsy date of the month as Request Start Date",
					"User should be able to select the firsy date of the month as Request Start Date",
					"User successfully select the firsy date of the month as Request Start Date", "pass");
		} else {
			writeTestResults("Verify that user able to select the firsy date of the month as Request Start Date",
					"User should be able to select the firsy date of the month as Request Start Date",
					"User doesn't select the firsy date of the month as Request Start Date", "fail");

		}

		click(btn_locationInformationLookup);
		sendKeysLookup(txt_searchLocation, location);
		pressEnter(txt_searchLocation);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", location), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", location));

		Thread.sleep(1500);
		String selectedLocation = getAttribute(txt_projectLocationFrontPageProject, "value");
		if (selectedLocation.equals(location)) {
			writeTestResults("Verify that user able to select the Location",
					"User should be able to select the Location", "User successfully select the Location", "pass");
		} else {
			writeTestResults("Verify that user able to select the Location",
					"User should be able to select the Location", "User doesn't select the Location", "fail");

		}

		click(btn_draft);
		explicitWait(header_draftedProject, 50);
		if (isDisplayed(header_draftedProject)) {
			writeTestResults("Verify user able to draft the Project", "User should be able to draft the Project",
					"User successfully draft the Project", "pass");
		} else {
			writeTestResults("Verify user able to draft the Project", "User should be able to draft the Project",
					"User doesn't draft the Project", "fail");

		}

		click(btn_release);
		explicitWait(header_releasedProject, 50);
		trackCode = getText(lbl_docNumber);
		System.out.println("***********"+trackCode+"***********");
		writeProjectData("releasedProjectUrl_Smoke_Project_002", getCurrentUrl(), 0);
		writeProjectData("ReleasedProjectID_Smoke_Project_002", trackCode, 1);
		if (isDisplayed(header_releasedProject)) {
			writeTestResults("Verify user able to release the Project", "User should be able to release the Project",
					"User successfully release the Project", "pass");
		} else {
			writeTestResults("Verify user able to release the Project", "User should be able to release the Project",
					"User doesn't release the Project", "fail");

		}

	}

	/* Smoke_Project_003 */
	public void addTasks1_Smoke_Project_003() throws Exception {
		loginProjectSmoke();
		openPage(readProjectData("releasedProjectUrl_Smoke_Project_002"));
		explicitWait(btn_edit, 50);
		click(btn_edit);
		explicitWait(tab_workBreackdown, 40);
		click(tab_workBreackdown);
		explicitWait(btn_expandProjectTask, 10);
		click(btn_expandProjectTask);
		explicitWait(btn_addTask, 10);
		/* Task 01 */
		click(btn_addTask);
		explicitWait(header_taskWindow, 15);
		if (isDisplayed(header_taskWindow)) {
			writeTestResults("Verify that task window is open", "Task window should be open",
					"Task window successfully open", "pass");
		} else {
			writeTestResults("Verify that task window is open", "Task window should be open",
					"Task window doesn't open", "fail");
		}

		explicitWait(txt_taskNameTaskWindow, 20);
		sendKeys(txt_taskNameTaskWindow, task);
		click(btn_inputProductsTabTaskWindow);
		explicitWait(btn_inputProductsLookupTaskWindow, 10);
		click(btn_inputProductsLookupTaskWindow);
		sendKeysLookup(txt_searchProduct, batchFifoProduct);
		pressEnter(txt_searchProduct);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", batchFifoProduct), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", batchFifoProduct));

		Thread.sleep(1500);
		String selectedProduct = getText(div_selectedInputProductTaskWindow);
		if (selectedProduct.contains(batchFifoProduct)) {
			writeTestResults("Verify that user able to select the Input Product",
					"User should be able to select the Input Product", "User successfully select the Input Product",
					"pass");
		} else {
			writeTestResults("Verify that user able to select the Input Product",
					"User should be able to select the Input Product", "User doesn't select the Input Product", "fail");

		}

		sendKeys(txt_quantityInputProductsTaskWindow, quantityFive);

		click(chk_billableTaskWindowInputProducts);

		click(tab_labourTaskWindow);

		explicitWait(btn_lookupProductTaskWindowLabourTab, 10);
		click(btn_lookupProductTaskWindowLabourTab);
		sendKeysLookup(txt_searchProduct, serviceProduct);
		pressEnter(txt_searchProduct);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", serviceProduct), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", serviceProduct));

		Thread.sleep(1500);
		String selectedServiceProductLAbourTabProduct = getText(div_selectedServiceLabourTabTaskWindow);
		if (selectedServiceProductLAbourTabProduct.contains(serviceProduct)) {
			writeTestResults("Verify that user able to select the Service Product",
					"User should be able to select the Service Product", "User successfully select the Service Product",
					"pass");
		} else {
			writeTestResults("Verify that user able to select the Service Product",
					"User should be able to select the Service Product", "User doesn't select the Service Product",
					"fail");

		}

		click(txt_hoursLabourTabTaskWindow);
		click(btn_fiveLabourHoursTaskWindow);
		click(btn_okLabourHoursTaskWindow);

		click(chk_billableLabourHoursTaskWindow);

		click(tab_resourseTaskWindow);

		explicitWait(drop_resourseGroupTaskWindow, 10);
		selectText(drop_resourseGroupTaskWindow, referenceGroupMotorVehicle);

		click(btn_lookupResourseTaskWindow);
		sendKeysLookup(txt_searchResource, resource);
		pressEnter(txt_searchResource);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", resource), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", resource));

		Thread.sleep(1500);
		String selectedResource = getText(div_selectedResourseTaskWindow);
		if (selectedResource.contains(resource)) {
			writeTestResults("Verify that user able to select the Resourse",
					"User should be able to select the Resourse", "User successfully select the Resourse", "pass");
		} else {
			writeTestResults("Verify that user able to select the Resourse",
					"User should be able to select the Resourse", "User doesn't select the Resourse", "fail");

		}

		click(btn_lookupServiceTaskWindowResorseTab);
		sendKeysLookup(txt_searchProduct, serviceProduct2);
		pressEnter(txt_searchProduct);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", serviceProduct2), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", serviceProduct2));

		Thread.sleep(1500);
		String selectedServiceProductResourseTabProduct = getText(div_serviceProductResourseTabTaskWindow);
		if (selectedServiceProductResourseTabProduct.contains(serviceProduct2)) {
			writeTestResults("Verify that user able to select the Service Product",
					"User should be able to select the Service Product", "User successfully select the Service Product",
					"pass");
		} else {
			writeTestResults("Verify that user able to select the Service Product",
					"User should be able to select the Service Product", "User doesn't select the Service Product",
					"fail");

		}

		click(txt_hoursResourseTabTaskWindow);
		click(btn_fiveResourseHoursTaskWindow);
		click(btn_okResourseHoursTaskWindow);

		click(chk_billableResourseTabTaskWindow);

		click(tab_expenceTaskWindow);

		explicitWait(drop_analysisCodeExpenceTabTaskWindow, 10);
		selectIndex(drop_analysisCodeExpenceTabTaskWindow, 1);

		click(btn_nraationExpenceTabTaskWindow);

		explicitWait(txt_narrationTaskWindowExpenceTab, 10);

		sendKeys(txt_narrationTaskWindowExpenceTab, naraation);

		click(btn_applyNarrationExpence);
		Thread.sleep(1500);

		click(btn_serviceLoockupExpenceTabTaskWindow);

		sendKeysLookup(txt_searchProduct, serviceProduct3);
		pressEnter(txt_searchProduct);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", serviceProduct3), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", serviceProduct3));

		Thread.sleep(1500);
		String selectedServiceProductExpenceTabProduct = getText(div_selectedServiceExpenceTabTaskWindow);
		if (selectedServiceProductExpenceTabProduct.contains(serviceProduct3)) {
			writeTestResults("Verify that user able to select the Service Product",
					"User should be able to select the Service Product", "User successfully select the Service Product",
					"pass");
		} else {
			writeTestResults("Verify that user able to select the Service Product",
					"User should be able to select the Service Product", "User doesn't select the Service Product",
					"fail");

		}

		sendKeys(txt_estimatedExpenceTaskWindowExpenceTab, expenceEstimatedAmount);

		click(chk_billableExpenceTabTaskWindow);

		click(tab_overheadTaskWindow);

		String autoLoadedOverhead = getText(div_autoLoadedOverhead);
		if (autoLoadedOverhead.contains(overhead)) {
			writeTestResults("Verify that overhead is autoloadede", "Overhead should be loaded automatically",
					"Overhead successfully loaded automatically", "pass");
		} else {
			writeTestResults("Verify that overhead is autoloadede", "Overhead should be loaded automatically",
					"Overhead doesn't loaded automatically", "fail");
		}

		click(chk_billableOverheadTabTaskWindow);

		click(btn_updateTaskWindow);

		/* Task 02 */
		explicitWait(btn_addTask, 20);
		Thread.sleep(3000);
		click(btn_addTask);
		explicitWait(header_taskWindow, 15);
		if (isDisplayed(header_taskWindow)) {
			writeTestResults("Verify that task window is open", "Task window should be open",
					"Task window successfully open", "pass");
		} else {
			writeTestResults("Verify that task window is open", "Task window should be open",
					"Task window doesn't open", "fail");
		}

		explicitWait(txt_taskNameTaskWindow, 20);
		sendKeys(txt_taskNameTaskWindow, taskSubContract);

		selectText(drop_contractTypeTaskWindow, contractTypeSubContract);

		Thread.sleep(1000);
		String contractTypeSubContract = getSelectedOptionInDropdown(drop_contractTypeTaskWindow);
		if (contractTypeSubContract.equals(contractTypeSubContract)) {
			writeTestResults("Verify that user able to select the cntract type as Sub Contract",
					"User should be able to select the cntract type as Sub Contract",
					"User successfully select the cntract type as Sub Contract", "pass");
		} else {
			writeTestResults("Verify that user able to select the cntract type as Sub Contract",
					"User should be able to select the cntract type as Sub Contract",
					"User doesn't select the cntract type as Sub Contract", "fail");

		}

		click(tab_subContractServices);

		explicitWaitUntillClickable(btn_vendorLookup, 20);
		click(btn_vendorLookup);
		sendKeysLookup(txt_vendorSearch, "00020");
		pressEnter(txt_vendorSearch);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", "00020"), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", "00020"));
		Thread.sleep(1500);
		String selectedVendort = getAttribute(txt_vendorSubContractTabTaskWindow, "value");
		if (selectedVendort.contains("00020")) {
			writeTestResults("Verify that user able to select the Vendor", "User should be able to select the Vendor",
					"User successfully select the Vendor", "pass");
		} else {
			writeTestResults("Verify that user able to select the Vendor", "User should be able to select the Vendor",
					"User doesn't select the Vendor", "fail");

		}

		click(btn_serviceProductLookupRowReplaceSubContractTab.replace("row", "1"));
		sendKeysLookup(txt_searchProduct, serviceProduct4);
		pressEnter(txt_searchProduct);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", serviceProduct4), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", serviceProduct4));

		Thread.sleep(1500);
		String selectedServiceProductSubContractLineOne = getText(
				div_selectedServiceSubContractTabTaskWindowRowReplace.replace("row", "1"));
		if (selectedServiceProductSubContractLineOne.contains(serviceProduct4)) {
			writeTestResults("Verify that user able to select the Service Product(Line 1)",
					"User should be able to select the Service Product(Line 1)",
					"User successfully select the Service Product(Line 1)", "pass");
		} else {
			writeTestResults("Verify that user able to select the Service Product(Line 1)",
					"User should be able to select the Service Product(Line 1)",
					"User doesn't select the Service Product(Line 1)", "fail");

		}

		sendKeys(txt_estimatedAmountServiceOneSubContractTypeTaskRowReplace.replace("row", "1"),
				serviceOneEstimatedAmountSubContractTypeTask);

		click(btn_addNewRowSubContractTaskTaskWindow);
		Thread.sleep(1500);
		click(btn_serviceProductLookupRowReplaceSubContractTab.replace("row", "2"));
		sendKeysLookup(txt_searchProduct, serviceProduct5);
		pressEnter(txt_searchProduct);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", serviceProduct5), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", serviceProduct5));

		Thread.sleep(2000);
		String selectedServiceProductSubContractLineTwo = getText(
				div_selectedServiceSubContractTabTaskWindowRowReplace.replace("row", "2"));
		if (selectedServiceProductSubContractLineTwo.contains(serviceProduct5)) {
			writeTestResults("Verify that user able to select the Service Product(Line 2)",
					"User should be able to select the Service Product(Line 2)",
					"User successfully select the Service Product(Line 2)", "pass");
		} else {
			writeTestResults("Verify that user able to select the Service Product(Line 2)",
					"User should be able to select the Service Product(Line 2)",
					"User doesn't select the Service Product(Line 2)", "fail");

		}

		sendKeys(txt_estimatedAmountServiceOneSubContractTypeTaskRowReplace.replace("row", "2"),
				serviceTwoEstimatedAmountSubContractTypeTask);

		click(chk_billableServicesSubContractTypeTaskRowReplace.replace("row", "1"));
		click(chk_billableServicesSubContractTypeTaskRowReplace.replace("row", "2"));
		click(btn_updateTaskWindow);

	}

	public void addTasks2_Smoke_Project_003() throws Exception {
		/* Task 03 */
		explicitWaitUntillClickable(btn_addTask, 20);
		Thread.sleep(3000);
		click(btn_addTask);
		explicitWait(header_taskWindow, 15);
		if (isDisplayed(header_taskWindow)) {
			writeTestResults("Verify that task window is open", "Task window should be open",
					"Task window successfully open", "pass");
		} else {
			writeTestResults("Verify that task window is open", "Task window should be open",
					"Task window doesn't open", "fail");
		}

		explicitWait(txt_taskNameTaskWindow, 20);
		sendKeys(txt_taskNameTaskWindow, taskNameInterdepartment);

		selectText(drop_contractTypeTaskWindow, contractTypeInterDepartment);
		Thread.sleep(1000);
		String contractTypeInterdepartmental = getSelectedOptionInDropdown(drop_contractTypeTaskWindow);
		if (contractTypeInterdepartmental.equals(contractTypeInterDepartment)) {
			writeTestResults("Verify that user able to select the cntract type as Interdepartment",
					"User should be able to select the cntract type as Interdepartment",
					"User successfully select the cntract type as Interdepartment", "pass");
		} else {
			writeTestResults("Verify that user able to select the cntract type as Interdepartment",
					"User should be able to select the cntract type as Interdepartment",
					"User doesn't select the cntract type as Interdepartment", "fail");

		}

		String autoLoadedEmployee = getAttribute(txt_loadedEmployeeNameSummarTabTaskWindowInterdepartmentalTask,
				"value");
		if (autoLoadedEmployee.contains(employeeName)) {
			writeTestResults("Verify that employee is autoloaded", "Employee should be autoloaded",
					"Employee successfully autoloaded", "pass");
		} else {
			writeTestResults("Verify that employee is autoloaded", "Employee should be autoloaded",
					"Employee doesn't autoloaded", "fail");
		}

		click(tab_interdepartmentTaskWindow);

		explicitWait(btn_ownerLookupTaskWindow, 15);
		click(btn_ownerLookupTaskWindow);
		sendKeysLookup(txt_labourSearch, employee);
		pressEnter(txt_labourSearch);
		Thread.sleep(2000);
		explicitWait(lnk_employeeTaskWindowInterdepartmentalTabEmpReplace.replace("emp", employee), 20);
		Thread.sleep(2000);
		doubleClick(lnk_employeeTaskWindowInterdepartmentalTabEmpReplace.replace("emp", employee));

		Thread.sleep(1500);
		String selectedOwner = getAttribute(txt_selectedOwnerInterdepartmentTabTaskWindow, "value");
		if (selectedOwner.contains(employee)) {
			writeTestResults("Verify that user able to select the Owner", "User should be able to select the Owner",
					"User successfully select the Owner", "pass");
		} else {
			writeTestResults("Verify that user able to select the Owner", "User should be able to select the Owner",
					"User doesn't select the Owner", "fail");

		}
		
		click(btn_lookupPerformer);
		sendKeysLookup(txt_labourSearch, employee);
		pressEnter(txt_labourSearch);
		Thread.sleep(2000);
		explicitWait(lnk_employeeTaskWindowInterdepartmentalTabEmpReplace.replace("emp", employee), 20);
		Thread.sleep(2000);
		doubleClick(lnk_employeeTaskWindowInterdepartmentalTabEmpReplace.replace("emp", employee));

		Thread.sleep(1500);
		String selectedPerformer = getAttribute(txt_performerInterDepartmentTabWorkBreackdown, "value");
		if (selectedPerformer.contains(employee)) {
			writeTestResults("Verify that user able to select the Performer", "User should be able to select the Performer",
					"User successfully select the Performer", "pass");
		} else {
			writeTestResults("Verify that user able to select the Performer", "User should be able to select the Performer",
					"User doesn't select the Performer", "fail");

		}

		sendKeys(txt_estimatedCostTaskWindowInterdepartmentalTask, estimatedCostInterdepartmentalTask);

		click(txt_estimatedDateInterdepartmentTaskWindow);

		String date = finObj.currentDay();
		click(btn_dayClickTaskWindow.replace("day", date));

		click(chk_billableOverheadTabTaskWindow);

		click(btn_updateTaskWindow);

		Thread.sleep(2000);
		click(btn_update);
		explicitWait(btn_edit, 50);

		click(tab_workBreackdown);
		Thread.sleep(1500);
		explicitWait(btn_expandProjectTask, 20);
		click(btn_expandProjectTask);
		Thread.sleep(1000);
		click(btn_releaseTaskWorkBreackdownPendingFirst);

		explicitWait(tab_workBreackdown, 40);
		Thread.sleep(3500);
		click(tab_workBreackdown);
		explicitWait(btn_expandProjectTask, 20);
		click(btn_expandProjectTask);
		Thread.sleep(1000);
		if (isDisplayed(lbl_releasedTaskWorkBreackdowntaskNoReplace.replace("taskNo", "1"))) {
			writeTestResults("Verify that Task 01 is released successfully", "Task 01 should be released successfully",
					"Task 01 releases successfully", "pass");
		} else {
			writeTestResults("Verify that Task 01 is released successfully", "Task 01 should be released successfully",
					"Task 01 doesn't releases successfully", "fail");

		}

		click(btn_releaseTaskWorkBreackdownPendingFirst);

		explicitWait(tab_workBreackdown, 40);
		Thread.sleep(3500);
		click(tab_workBreackdown);
		explicitWait(btn_expandProjectTask, 20);
		click(btn_expandProjectTask);
		Thread.sleep(1000);
		if (isDisplayed(lbl_releasedTaskWorkBreackdowntaskNoReplace.replace("taskNo", "2"))) {
			writeTestResults("Verify that Task 02 is released successfully", "Task 02 should be released successfully",
					"Task 02 releases successfully", "pass");
		} else {
			writeTestResults("Verify that Task 02 is released successfully", "Task 02 should be released successfully",
					"Task 02 doesn't releases successfully", "fail");

		}

		click(btn_releaseTaskWorkBreackdownPendingFirst);
		explicitWait(tab_workBreackdown, 40);
		Thread.sleep(3500);
		click(tab_workBreackdown);
		explicitWait(btn_expandProjectTask, 20);
		click(btn_expandProjectTask);
		Thread.sleep(1000);
		if (isDisplayed(lbl_releasedTaskWorkBreackdowntaskNoReplace.replace("taskNo", "3"))) {
			writeTestResults("Verify that Task 03 is released successfully", "Task 03 should be released successfully",
					"Task 03 releases successfully", "pass");
		} else {
			writeTestResults("Verify that Task 03 is released successfully", "Task 03 should be released successfully",
					"Task 03 doesn't releases successfully", "fail");

		}

		click(tab_costingSummary);
		Thread.sleep(2000);

		String allocatedExpence = getText(lbl_allocationExpenceCostingTab);
		String allocatedREsoureseCose = getText(lbl_allocationResourseCostCostingTab);
		String allocatedHours = getText(lbl_allocationHourseCostCostingTab);
		String allocatedMaterials = getText(lbl_allocationMaterialsCostCostingTab);
		String allocatedSubContract = getText(lbl_allocationSubContractCostCostingTab);
		String allocatedOverhead = getText(lbl_allocationOverheadCostCostingTab);
		String allocatedInterdepartmental = getText(lbl_allocationInterdepartmentalCostCostingTab);

		if (allocatedExpence.equals("2,000.000000")) {
			writeTestResults("Verify that expence allocated correctly", "Expence should be allocated correctly",
					"Expence allocated correctly", "pass");
		} else {
			writeTestResults("Verify that expence allocated correctly", "Expence should be allocated correctly",
					"Expence doesn't allocated correctly", "fail");
		}

		if (allocatedREsoureseCose.equals("250.000000")) {
			writeTestResults("Verify that resourse allocated correctly", "resourse should be allocated correctly",
					"resourse allocated correctly", "pass");
		} else {
			writeTestResults("Verify that resourse allocated correctly", "resourse should be allocated correctly",
					"resourse doesn't allocated correctly", "fail");
		}

		if (allocatedHours.equals("250.000000")) {
			writeTestResults("Verify that hours allocated correctly", "hours should be allocated correctly",
					"hours allocated correctly", "pass");
		} else {
			writeTestResults("Verify that hours allocated correctly", "hours should be allocated correctly",
					"hours doesn't allocated correctly", "fail");
		}

		if (allocatedMaterials.equals("2,500.000000")) {
			writeTestResults("Verify that materials allocated correctly", "materials should be allocated correctly",
					"materials allocated correctly", "pass");
		} else {
			writeTestResults("Verify that materials allocated correctly", "materials should be allocated correctly",
					"materials doesn't allocated correctly", "fail");
		}

		if (allocatedSubContract.equals("2,200.000000")) {
			writeTestResults("Verify that sub contract allocated correctly",
					"sub contract should be allocated correctly", "sub contract allocated correctly", "pass");
		} else {
			writeTestResults("Verify that sub contract allocated correctly",
					"sub contract should be allocated correctly", "sub contract doesn't allocated correctly", "fail");
		}

		if (allocatedOverhead.equals("15.000000")) {
			writeTestResults("Verify that overhead allocated correctly", "overhead should be allocated correctly",
					"overhead allocated correctly", "pass");
		} else {
			writeTestResults("Verify that overhead allocated correctly", "overhead should be allocated correctly",
					"overhead doesn't allocated correctly", "fail");
		}

		if (allocatedInterdepartmental.equals("0.000000")) {
			writeTestResults("Verify that inter department allocation is zero",
					"inter department allocation should be zero", "Interdepartment allocation is zero", "pass");
		} else {
			writeTestResults("Verify that inter department allocation is zero",
					"inter department allocation should be zero", "Interdepartment allocation is not zero", "fail");
		}
	}

	/* Smoke_Project_004 */
	public void releaseInternalAndInternalDispatchOrders_Smoke_Project_004()
			throws InvalidFormatException, IOException, Exception {
		loginProjectSmoke();
		openPage(readProjectData("releasedProjectUrl_Smoke_Project_002"));
		explicitWait(btn_action, 50);
		click(btn_action);
		explicitWait(btn_createInternalOrder, 50);
		click(btn_createInternalOrder);
		explicitWait(header_genarateInternalOrder, 20);
		if (isDisplayed(header_genarateInternalOrder)) {
			writeTestResults("Verify that Genarate Internal Orders window is open",
					"Genarate Internal Orders window should be open",
					"Genarate Internal Orders window successfully open", "pass");
		} else {
			writeTestResults("Verify that Genarate Internal Orders window is open",
					"Genarate Internal Orders window should be open", "Genarate Internal Orders window doesn't open",
					"fail");
		}

		selectText(drop_taskInternalOrder, taskAll);

		explicitWait(div_productsGenarateInternalOrderWindowProductReplace.replace("product", batchFifoProduct), 10);
		if (isDisplayed(header_genarateInternalOrder)) {
			writeTestResults("Verify that relevent product is loaded", "Relevent product should be loaded",
					"Relevent product successfully loaded", "pass");
		} else {
			writeTestResults("Verify that relevent product is loaded", "Relevent product should be loaded",
					"Relevent product doesn't loaded", "fail");
		}

		click(chk_productGenarateInternalOrder);

		click(btn_applyGenarateInternalOrder);

		Thread.sleep(2000);
		switchWindow();
		explicitWait(header_newInternalOrder, 40);

		click(btn_draft);
		explicitWait(header_draftedInternalOrder, 10);
		if (isDisplayed(header_draftedInternalOrder)) {
			writeTestResults("Verify user able to draft the Internal Order",
					"User should be able to draft the Internal Order", "User successfully draft the Internal Order",
					"pass");
		} else {
			writeTestResults("Verify user able to draft the Internal Order",
					"User should be able to draft the Internal Order", "User doesn't draft the Internal Order", "fail");
		}

		click(btn_release);

		explicitWait(lnk_goToPage, 30);
		click(lnk_goToPage);
		Thread.sleep(2000);
		pageRefersh();
		explicitWait(header_releasedInternalOrder, 10);
		trackCode = getText(lbl_docNumber);
		String internalOrderNp = trackCode;
		if (isDisplayed(header_releasedInternalOrder)) {
			writeTestResults("Verify user able to release the Internal Order",
					"User should be able to release the Internal Order", "User successfully release the Internal Order",
					"pass");
		} else {
			writeTestResults("Verify user able to release the Internal Order",
					"User should be able to release the Internal Order", "User doesn't release the Internal Order",
					"fail");
		}
		switchWindow();
		explicitWait(header_newInternalDispatchOrder, 40);

		click(btn_draft);
		explicitWait(header_draftedInternalDispatchOrder, 10);
		if (isDisplayed(header_draftedInternalDispatchOrder)) {
			writeTestResults("Verify user able to draft the Internal Dispatch Order",
					"User should be able to draft the Internal Dispatch Order",
					"User successfully draft the Internal Dispatch Order", "pass");
		} else {
			writeTestResults("Verify user able to draft the Internal Dispatch Order",
					"User should be able to draft the Internal Dispatch Order",
					"User doesn't draft the Internal Dispatch Order", "fail");
		}

		click(btn_release);
		explicitWait(header_releasedInternalDispatchOrder, 10);
		trackCode = getText(lbl_docNumber);
		writeProjectData("IO_Smoke_Project_004", trackCode, 2);
		if (isDisplayed(header_releasedInternalDispatchOrder)) {
			writeTestResults("Verify user able to release the Internal Dispatch Order",
					"User should be able to release the Internal Dispatch Order",
					"User successfully release the Internal Dispatch Order", "pass");
		} else {
			writeTestResults("Verify user able to release the Internal Dispatch Order",
					"User should be able to release the Internal Dispatch Order",
					"User doesn't release the Internal Dispatch Order", "fail");
		}

		switchWindow();
		closeWindow();
		switchWindow();

		pageRefersh();
		explicitWait(tab_overviewProject, 50);
		click(tab_overviewProject);

		explicitWait(tab_inputProductsOverviewTab, 6);
		click(tab_inputProductsOverviewTab);

		explicitWait(lbl_internalOrderOverviewTabDocNoReplace.replace("doc_no", internalOrderNp), 15);
		if (isDisplayed(lbl_internalOrderOverviewTabDocNoReplace.replace("doc_no", internalOrderNp))) {
			writeTestResults("Verify user able to see Internal Order number under input products tab",
					"User should be able to see Internal Order number under input products tab",
					"User successfully able to see Internal Order number under input products tab", "pass");
		} else {
			writeTestResults("Verify user able to see Internal Order number under input products tab",
					"User should be able to see Internal Order number under input products tab",
					"User not able to see Internal Order number under input products tab", "fail");
		}

	}

	/* Smoke_Project_005 */
	public void releasesDailyWorkSheet_Smoke_Project_005() throws Exception {
		loginProjectSmoke();
		click(btn_navigationPane);
		explicitWait(btn_service, 10);
		click(btn_service);
		explicitWait(btn_dailyWorkSheet, 10);
		click(btn_dailyWorkSheet);
		explicitWait(btn_newDailyWorkSheet, 30);
		click(btn_newDailyWorkSheet);
		explicitWait(btn_dailyWorksheetProjectJourney, 10);
		click(btn_dailyWorksheetProjectJourney);
		explicitWait(header_newDailyWorkSheet, 40);
		if (isDisplayed(header_newDailyWorkSheet)) {
			writeTestResults("Verify user able to navigate to New Daily Work Sheet",
					"User should be able to navigate to New Daily Work Sheet",
					"User successfully navigate to New Daily Work Sheet", "pass");
		} else {
			writeTestResults("Verify user able to navigate to New Daily Work Sheet",
					"User should be able to navigate to New Daily Work Sheet",
					"User doesn't navigate to New Daily Work Sheet", "fail");
		}

		click(btn_addNewDailyWorsheet);
		explicitWait(header_AddNewJobWindow, 40);
		if (isDisplayed(header_AddNewJobWindow)) {
			writeTestResults("Verify user able to navigate to Add New Job window",
					"User should be able to navigate to Add New Job window",
					"User successfully navigate to Add New Job window", "pass");
		} else {
			writeTestResults("Verify user able to navigate to Add New Job window",
					"User should be able to navigate to Add New Job window",
					"User doesn't navigate to Add New Job window", "fail");
		}

		click(btn_addProjectTaskLookup);
		sendKeysLookup(txt_serachProjectTask, readProjectData("ReleasedProjectID_Smoke_Project_002"));
		pressEnter(txt_serachProjectTask);
		Thread.sleep(2000);
		explicitWait(lnl_releventTaskProjectNoReplace.replace("proj_no",
				readProjectData("ReleasedProjectID_Smoke_Project_002")), 20);
		Thread.sleep(2000);
		doubleClick(lnl_releventTaskProjectNoReplace.replace("proj_no",
				readProjectData("ReleasedProjectID_Smoke_Project_002")));

		Thread.sleep(1500);

		click(btn_lookupEmployyeAddNewJobWindow);
		sendKeysLookup(txt_labourSearch, employee);
		pressEnter(txt_labourSearch);
		Thread.sleep(2000);
		explicitWait(lnk_employeeProjectEmpReplace.replace("emp", employee), 20);
		Thread.sleep(2000);
		doubleClick(lnk_employeeProjectEmpReplace.replace("emp", employee));

		Thread.sleep(1500);

		click(txt_inTimeAddNewJob);

		click(btn_eightIntimeHoursAddNEwJob);

		click(btn_okAddNewwJobHoursInTime);

		click(txt_outTimeAddNewJob);

		click(btn_fiveOuttimeHoursAddNEwJob);

		click(btn_okAddNewwJobHoursOutTime);

		click(btn_updateAddnewJob);

		explicitWait(header_newDailyWorkSheet, 30);
		click(btn_draft);
		explicitWait(header_draftedDailyWorkSheet, 10);
		if (isDisplayed(header_draftedDailyWorkSheet)) {
			writeTestResults("Verify user able to draft the Daily Work Sheet",
					"User should be able to draft the Daily Work Sheet", "User successfully draft the Daily Work Sheet",
					"pass");
		} else {
			writeTestResults("Verify user able to draft the Daily Work Sheet",
					"User should be able to draft the Daily Work Sheet", "User doesn't draft the Daily Work Sheet",
					"fail");
		}

		click(btn_release);
		explicitWait(header_releasedDailyWorkSheet, 10);
		trackCode = getText(lbl_docNumber);
		if (isDisplayed(header_releasedDailyWorkSheet)) {
			writeTestResults("Verify user able to release the Daily Work Sheet",
					"User should be able to release the Daily Work Sheet",
					"User successfully release the Daily Work Sheet", "pass");
		} else {
			writeTestResults("Verify user able to release the Daily Work Sheet",
					"User should be able to release the Daily Work Sheet", "User doesn't release the Daily Work Sheet",
					"fail");
		}

		openPage(readProjectData("releasedProjectUrl_Smoke_Project_002"));
		explicitWait(tab_costingSummary, 40);
		click(tab_costingSummary);
		Thread.sleep(2000);

		String actualHours = getText(lbl_actualHourseCostCostingTab);

		if ((!(actualHours == null)) && (!actualHours.equals("0.000000"))) {
			writeTestResults("Verify that actual hours were updated", "Actual hours should be updated",
					"Actual hours successfully updated", "pass");
		} else {
			writeTestResults("Verify that actual hours were updated", "Actual hours should be updated",
					"Actual hours doesn't updated", "fail");
		}
	}

	/* Smoke_Project_006 */
	public void releaseSubContractPurchaseOrderAndInvoice_Smoke_Project_006() throws Exception {
		loginProjectSmoke();
		openPage(readProjectData("releasedProjectUrl_Smoke_Project_002"));
		explicitWait(btn_action, 50);
		click(btn_action);
		explicitWait(btn_subContractOrderActions, 50);
		click(btn_subContractOrderActions);
		explicitWait(header_subContractWindow, 20);
		if (isDisplayed(header_subContractWindow)) {
			writeTestResults("Verify that Sub Contract Order window is open",
					"Sub Contract Order window should be open", "Sub Contract Order window successfully open", "pass");
		} else {
			writeTestResults("Verify that Sub Contract Order window is open",
					"Sub Contract Order window should be open", "Sub Contract Order window doesn't open", "fail");
		}

		selectText(drop_task, taskAll);

		explicitWaitUntillClickable(btn_vendorLookup, 60);
		click(btn_vendorLookup);
		sendKeysLookup(txt_vendorSearch, "00020");
		pressEnter(txt_vendorSearch);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", "00020"), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", "00020"));

		Thread.sleep(1500);

		Thread.sleep(2500);
		click(chk_selectAllWindowComeFromProjectAction);

		click(btn_applySucContractOrders);

		Thread.sleep(2000);

		switchWindow();
		explicitWait(header_newPurchaseOrder, 40);
		click(btn_checkout);
		Thread.sleep(2000);
		click(btn_draft);
		explicitWait(header_draftedPurchaseOrder, 10);
		if (isDisplayed(header_draftedPurchaseOrder)) {
			writeTestResults("Verify user able to draft the Purchase Order",
					"User should be able to draft the Purchase Order", "User successfully draft the Purchase Order",
					"pass");
		} else {
			writeTestResults("Verify user able to draft the Purchase Order",
					"User should be able to draft the Purchase Order", "User doesn't draft the Purchase Order", "fail");
		}

		click(btn_release);

		explicitWait(lnk_goToPage, 30);
		click(lnk_goToPage);
		Thread.sleep(2000);
		pageRefersh();
		explicitWait(header_releasedPurchaseOrder, 10);
		trackCode = getText(lbl_docNumber);
		String purchaseOrderNo = trackCode;
		if (isDisplayed(header_releasedPurchaseOrder)) {
			writeTestResults("Verify user able to release the Purchase Order",
					"User should be able to release the Purchase Order", "User successfully release the Purchase Order",
					"pass");
		} else {
			writeTestResults("Verify user able to release the Purchase Order",
					"User should be able to release the Purchase Order", "User doesn't release the Purchase Order",
					"fail");
		}
		switchWindow();
		explicitWait(header_newPurchaseInvoice, 40);
		click(btn_checkout);
		Thread.sleep(2000);
		click(btn_draft);
		explicitWait(header_draftedPurchaseInvoice, 30);
		if (isDisplayed(header_draftedPurchaseInvoice)) {
			writeTestResults("Verify user able to draft the Purchase Invoice",
					"User should be able to draft the Purchase Invoice", "User successfully draft the Purchase Invoice",
					"pass");
		} else {
			writeTestResults("Verify user able to draft the Purchase Invoice",
					"User should be able to draft the Purchase Invoice", "User doesn't draft the Purchase Invoice",
					"fail");
		}

		explicitWait(btn_release, 30);
		Thread.sleep(2000);
		click(btn_release);
		explicitWait(header_releasedPurchaseInvoice, 30);
		trackCode = getText(lbl_docNumber);
		if (isDisplayed(header_releasedPurchaseInvoice)) {
			writeTestResults("Verify user able to release the Purchase Invoice",
					"User should be able to release the Purchase Invoice",
					"User successfully release the Purchase Invoice", "pass");
		} else {
			writeTestResults("Verify user able to release the Purchase Invoice",
					"User should be able to release the Purchase Invoice", "User doesn't release the Purchase Invoice",
					"fail");
		}

		switchWindow();
		closeWindow();
		switchWindow();

		pageRefersh();
		explicitWait(tab_overviewProject, 50);
		click(tab_overviewProject);

		explicitWait(tab_subContractServicesOverviewTab, 6);
		click(tab_subContractServicesOverviewTab);

		explicitWait(lbl_purchaseOrderOverviewTabDocNoReplace.replace("doc_no", purchaseOrderNo), 15);

		int countPoDisplyed = getCount(lbl_purchaseOrderOverviewTabDocNoReplace.replace("doc_no", purchaseOrderNo));
		if (countPoDisplyed == 2) {
			writeTestResults("Verify user able to see Purchase Order number under sub contract tab",
					"User should be able to see Purchase Order number under sub contract tab",
					"User successfully able to see Purchase Order number under sub contract tab", "pass");
		} else {
			writeTestResults("Verify user able to see Purchase Order number under sub contract tab",
					"User should be able to see Purchase Order number under sub contract tab",
					"User not able to see Purchase Order number under sub contract tab", "fail");
		}

		click(tab_costingSummary);
		Thread.sleep(2000);

		String actualSubContractCost = getText(lbl_actualSubContractCostCostingTab);

		if (actualSubContractCost.equals("2,200.000000")) {
			writeTestResults("Verify that actual of sub contract were updated",
					"Actual of sub contract should be updated", "Actual of sub contract successfully updated", "pass");
		} else {
			writeTestResults("Verify that actual of sub contract were updated",
					"Actual of sub contract should be updated", "Actual of sub contract doesn't updated", "fail");
		}

	}

	/* Smoke_Project_007 */
	public void actualUpdate_Smoke_Project_007() throws Exception {
		loginProjectSmoke();
		openPage(readProjectData("releasedProjectUrl_Smoke_Project_002"));
		explicitWait(btn_action, 50);
		click(btn_action);
		explicitWait(btn_actualUpdate, 50);
		click(btn_actualUpdate);

		explicitWait(txt_actualUpdateQuantity, 30);
		sendKeys(txt_actualUpdateQuantity, actualUpdateQuantity);

		click(btn_updateActualUpdate);

		Thread.sleep(3000);

		click(tab_resourseActualUpdateWindow);

		explicitWait(txt_actualResourseHours, 10);
		click(txt_actualResourseHours);

		click(btn_fiveActualResourseHours);

		click(btn_okActualResourseHours);

		click(btn_updateActualUpdate);
		Thread.sleep(2500);
		pageRefersh();

		explicitWait(tab_costingSummary, 40);
		click(tab_costingSummary);
		Thread.sleep(2000);

		String actualMaterials = getText(lbl_actualMaterialCostCostingTab);
		String actualResourse = getText(lbl_actualResourseCostCostingTab);
		String actualOverhead = getText(lbl_actualOverheadCostCostingTab);

		if (actualMaterials.equals("600.000000")) {
			writeTestResults("Verify that material actual cost were updated", "material actual cost should be updated",
					"material actual cost successfully updated", "pass");
		} else {
			writeTestResults("Verify that material actual cost were updated", "material actual cost should be updated",
					"material actual cost doesn't updated", "fail");
		}

		if (actualResourse.equals("250.000000")) {
			writeTestResults("Verify that resourse actual cost were updated", "resourse actual cost should be updated",
					"resourse actual cost successfully updated", "pass");
		} else {
			writeTestResults("Verify that resourse actual cost were updated", "resourse actual cost should be updated",
					"resourse actual cost doesn't updated", "fail");
		}

		if (actualOverhead.equals("15.000000")) {
			writeTestResults("Verify that overhead actual cost were updated", "overhead actual cost should be updated",
					"overhead actual cost successfully updated", "pass");
		} else {
			writeTestResults("Verify that overhead actual cost were updated", "overhead actual cost should be updated",
					"overhead actual cost doesn't updated", "fail");
		}

	}

	/* Smoke_Project_008 */
	public void internelReturnOrder_Smoke_Project_008() throws Exception {
		loginProjectSmoke();
		click(btn_navigationPane);
		explicitWait(btn_inventoryAndWarehouseModule, 10);
		click(btn_inventoryAndWarehouseModule);
		explicitWait(btn_internalReturnOrder, 10);
		click(btn_internalReturnOrder);
		explicitWait(btn_newInternalReturnOrder, 40);
		click(btn_newInternalReturnOrder);
		explicitWait(btn_journeyWIPReturns, 40);
		click(btn_journeyWIPReturns);
		explicitWait(header_newInternalReturnOrder, 40);
		if (isDisplayed(header_newInternalReturnOrder)) {
			writeTestResults("Verify user able to navigate to new Internal Return Order",
					"User should be able to navigate to new Internal Return Order",
					"User successfully navigate to new Internal Return Order", "pass");
		} else {
			writeTestResults("Verify user able to navigate to new Internal Return Order",
					"User should be able to navigate to new Internal Return Order",
					"User doesn't navigate to new Internal Return Order", "fail");
		}

		selectText(drop_warehouseInternalReturnOrder, outptuWarehouse + " [Negombo]");

		click(btn_docIconInternalReturnOrder);

		sendKeys(txt_docSearchInternalReturnOrder, readProjectData("IO_Smoke_Project_004"));

		click(btn_searchInternalReturnOrder);

		explicitWait(chk_documentFirstInternalReturnOrder, 30);
		Thread.sleep(2000);
		click(chk_documentFirstInternalReturnOrder);

		click(btn_applyInternalReturnOrder);
		explicitWait(btn_draft, 10);
		Thread.sleep(2000);
		click(btn_draft);
		explicitWait(header_draftedIRO, 10);
		if (isDisplayed(header_draftedIRO)) {
			writeTestResults("Verify user able to draft the Internal Return Order",
					"User should be able to draft the Internal Return Order",
					"User successfully draft the Internal Return Order", "pass");
		} else {
			writeTestResults("Verify user able to draft the Internal Return Order",
					"User should be able to draft the Internal Return Order",
					"User doesn't draft the Internal Return Order", "fail");
		}

		click(btn_release);

		explicitWait(lnk_goToPage, 30);
		click(lnk_goToPage);
		Thread.sleep(2000);
		pageRefersh();
		explicitWait(header_releasedIRO, 10);
		trackCode = getText(lbl_docNumber);
		if (isDisplayed(header_releasedIRO)) {
			writeTestResults("Verify user able to release the Internal Return Order",
					"User should be able to release the Internal Return Order",
					"User successfully release the Internal Return Order", "pass");
		} else {
			writeTestResults("Verify user able to release the Internal Return Order",
					"User should be able to release the Internal Return Order",
					"User doesn't release the Internal Return Order", "fail");
		}
		switchWindow();
		explicitWait(btn_draft, 40);

		click(btn_draft);
		explicitWait(header_draftedInternalRecipt, 10);
		if (isDisplayed(header_draftedInternalRecipt)) {
			writeTestResults("Verify user able to draft the Internal Receipt",
					"User should be able to draft the Internal Receipt", "User successfully draft the Internal Receipt",
					"pass");
		} else {
			writeTestResults("Verify user able to draft the Internal Receipt",
					"User should be able to draft the Internal Receipt", "User doesn't draft the Internal Receipt",
					"fail");
		}

		click(btn_serialBatchCapture);
		explicitWait(btn_itemNumberOneSerielBatchCapture, 20);
		click(btn_itemNumberOneSerielBatchCapture);
		doubleClick(div_capturedCountOnGrdSerialBatchCaptureInternalRecipt);
		sendKeys(txt_captureQuantityOnGridSerilBatchCaptureInternalRecipt, remainingQuntityForReturn);
		click(btn_updateSerielBatchCapture);
		explicitWait(btn_serialBatchCaptureBackButton, 20);
		click(btn_serialBatchCaptureBackButton);

		click(btn_release);
		explicitWait(header_releasedInternalReceipt, 10);
		trackCode = getText(lbl_docNumber);
		if (isDisplayed(header_releasedInternalReceipt)) {
			writeTestResults("Verify user able to release the Internal Receipt",
					"User should be able to release the Internal Receipt",
					"User successfully release the Internal Receipt", "pass");
		} else {
			writeTestResults("Verify user able to release the Internal Receipt",
					"User should be able to release the Internal Receipt", "User doesn't release the Internal Receipt",
					"fail");
		}

	}

	/* Smoke_Project_009 */
	public void accruelVoucher_Smoke_Project_009() throws Exception {
		loginProjectSmoke();
		click(btn_navigationPane);
		explicitWait(btn_finance, 10);
		click(btn_finance);
		explicitWait(btn_outboundPaymentAdvice, 10);
		click(btn_outboundPaymentAdvice);
		explicitWait(btn_newOutboundPaymentAdvice, 40);
		click(btn_newOutboundPaymentAdvice);
		explicitWait(btn_accruelVoucherJourney, 40);
		click(btn_accruelVoucherJourney);

		explicitWait(header_newOutboundPaymentAdvice, 40);
		if (isDisplayed(header_newOutboundPaymentAdvice)) {
			writeTestResults("Verify user able to navigate to new Outbound Payment Advice",
					"User should be able to navigate to new Outbound Payment Advice",
					"User successfully navigate to new Outbound Payment Advice", "pass");
		} else {
			writeTestResults("Verify user able to navigate to new Outbound Payment Advice",
					"User should be able to navigate to new Outbound Payment Advice",
					"User doesn't navigate to new Outbound Payment Advice", "fail");
		}

		explicitWait(btn_vendorLookupOutboundPaymentAdvice, 30);
		click(btn_vendorLookupOutboundPaymentAdvice);
		sendKeysLookup(txt_vendorSearch, "00020");
		pressEnter(txt_vendorSearch);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", "00020"), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", "00020"));

		Thread.sleep(1500);

		selectIndex(drop_anlyzeCodeAccruelVoucher, 1);

		sendKeys(txt_rowDescriptionAccruelVoucher, acruelVoucherRowDescription);

		sendKeys(txt_amountAccruelVoucher, acruelVoucherAmount);

		click(btn_costAllocationWidgetAccruelVoucher);

		sendKeys(txt_costinfAllocationPresentageAccruelVoucher, costAllocationPresentageAccruelVoucher);

		selectText(drop_costAssignmentType, costAssignmentTypeProjectTask);

		explicitWait(btn_addProjectTaskLookup, 10);
		click(btn_addProjectTaskLookup);
		sendKeysLookup(txt_serachProjectTask, readProjectData("ReleasedProjectID_Smoke_Project_002"));
		pressEnter(txt_serachProjectTask);
		Thread.sleep(2000);
		explicitWait(lnl_releventTaskProjectNoReplace.replace("proj_no",
				readProjectData("ReleasedProjectID_Smoke_Project_002")), 20);
		Thread.sleep(2000);
		doubleClick(lnl_releventTaskProjectNoReplace.replace("proj_no",
				readProjectData("ReleasedProjectID_Smoke_Project_002")));

		Thread.sleep(1500);
		
		click(btn_addRecordCostAllocationOPA);
		
		Thread.sleep(2000);
		click(btn_updateCostAllocationAccruelVoucher);

		Thread.sleep(2000);
		click(btn_checkout);
		Thread.sleep(2000);
		click(btn_draft);
		explicitWait(header_draftedOutboundPaymentAdvice, 10);
		if (isDisplayed(header_draftedOutboundPaymentAdvice)) {
			writeTestResults("Verify user able to draft the Outbound Payment Advice",
					"User should be able to draft the Outbound Payment Advice",
					"User successfully draft the Outbound Payment Advice", "pass");
		} else {
			writeTestResults("Verify user able to draft the Outbound Payment Advice",
					"User should be able to draft the Outbound Payment Advice",
					"User doesn't draft the Outbound Payment Advice", "fail");
		}

		click(btn_release);
		explicitWait(header_releasedOutboundPaymentAdvice, 10);
		trackCode = getText(lbl_docNumber);
		if (isDisplayed(header_releasedOutboundPaymentAdvice)) {
			writeTestResults("Verify user able to release the Outbound Payment Advice",
					"User should be able to release the Outbound Payment Advice",
					"User successfully release the Outbound Payment Advice", "pass");
		} else {
			writeTestResults("Verify user able to release the Outbound Payment Advice",
					"User should be able to release the Outbound Payment Advice",
					"User doesn't release the Outbound Payment Advice", "fail");
		}

		openPage(readProjectData("releasedProjectUrl_Smoke_Project_002"));
		explicitWait(tab_costingSummary, 40);
		click(tab_costingSummary);
		Thread.sleep(2000);

		String actualExpenceAfterAccruelVoucher = getText(lbl_actualCostExpence);

		if (actualExpenceAfterAccruelVoucher.equals("1,000.000000")) {
			writeTestResults("Verify that actual expence were updated", "Actual expence should be updated",
					"Actual expence successfully updated", "pass");
		} else {
			writeTestResults("Verify that actual expence were updated", "Actual expence should be updated",
					"Actual expence doesn't updated", "fail");
		}
	}

	/* Smoke_Project_010 */
	public void invoiceProposal_Smoke_Project_010() throws Exception {
		loginProjectSmoke();
		openPage(readProjectData("releasedProjectUrl_Smoke_Project_002"));
		explicitWait(btn_action, 50);
		click(btn_action);
		explicitWait(btn_createInvoiceProposals, 50);
		click(btn_createInvoiceProposals);
		explicitWait(header_invoiceProposalWindow, 20);
		if (isDisplayed(header_invoiceProposalWindow)) {
			writeTestResults("Verify that Invoice Proposal window is open", "Invoice Proposal window should be open",
					"Invoice Proposal window successfully open", "pass");
		} else {
			writeTestResults("Verify that Invoice Proposal window is open", "Invoice Proposal window should be open",
					"Invoice Proposal window doesn't open", "fail");
		}

		selectText(drop_invoiceProposalFilter, inoiceProposalFilterTask);
		Thread.sleep(3000);

		click(chk_selectAllInvoiceProposalWindow);

		click(btn_productLoockupInvoiceProposalWindowRowReplace.replace("row", "1"));
		sendKeysLookup(txt_searchProduct, serviceProduct6);
		pressEnter(txt_searchProduct);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", serviceProduct6), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", serviceProduct6));

		Thread.sleep(2000);

		click(btn_productLoockupInvoiceProposalWindowRowReplace.replace("row", "2"));
		sendKeysLookup(txt_searchProduct, serviceProduct7);
		pressEnter(txt_searchProduct);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", serviceProduct7), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", serviceProduct7));

		Thread.sleep(2000);

		sendKeys(txt_quantitySecondRowInvoiceProposalTask, invoiceProposalSecondRowQuantiry);
		click(btn_checkout);
		Thread.sleep(1500);
		click(btn_applyInvoiceProposalWindow);
		Thread.sleep(2000);
		switchWindow();
		explicitWait(header_newInvoiceProposal, 40);
		click(btn_checkout);
		Thread.sleep(2000);
		click(btn_draft);
		explicitWait(header_draftedInvoiceProposal, 10);
		if (isDisplayed(header_draftedInvoiceProposal)) {
			writeTestResults("Verify user able to draft the Invoice Proposal",
					"User should be able to draft the Invoice Proposal", "User successfully draft the Invoice Proposal",
					"pass");
		} else {
			writeTestResults("Verify user able to draft the Invoice Proposal",
					"User should be able to draft the Invoice Proposal", "User doesn't draft the Invoice Proposal",
					"fail");
		}

		click(btn_release);

		explicitWait(lnk_goToPage, 30);
		click(lnk_goToPage);
		Thread.sleep(2000);
		pageRefersh();
		explicitWait(header_releasedInvoiceProposal, 10);
		trackCode = getText(lbl_docNumber);
		if (isDisplayed(header_releasedInvoiceProposal)) {
			writeTestResults("Verify user able to release the Invoice Proposal",
					"User should be able to release the Invoice Proposal",
					"User successfully release the Invoice Proposal", "pass");
		} else {
			writeTestResults("Verify user able to release the Invoice Proposal",
					"User should be able to release the Invoice Proposal", "User doesn't release the Invoice Proposal",
					"fail");
		}

		switchWindow();
		explicitWait(header_newServiceInvoice, 40);
		
		click(tab_serviceDeatailsServiceInvoice);
		explicitWait(btn_checkout, 10);
		click(btn_checkout);
		Thread.sleep(2000);

		click(btn_draft);
		explicitWait(header_draftedServiceInvoice, 10);
		if (isDisplayed(header_draftedServiceInvoice)) {
			writeTestResults("Verify user able to draft the Service Invoice",
					"User should be able to draft the Service Invoice", "User successfully draft the Service Invoice",
					"pass");
		} else {
			writeTestResults("Verify user able to draft the Service Invoice",
					"User should be able to draft the Service Invoice", "User doesn't draft the Service Invoice",
					"fail");
		}

		click(btn_release);
		explicitWait(header_releasedServiceInvoice, 10);
		Thread.sleep(2000);
		trackCode = getText(lbl_docNumber);
		serviceInvoiceInvoiceProposals = trackCode;
		if (isDisplayed(header_releasedServiceInvoice)) {
			writeTestResults("Verify user able to release the Service Invoice",
					"User should be able to release the Service Invoice",
					"User successfully release the Service Invoice", "pass");
		} else {
			writeTestResults("Verify user able to release the Service Invoice",
					"User should be able to release the Service Invoice", "User doesn't release the Service Invoice",
					"fail");
		}

		openPage(readProjectData("releasedProjectUrl_Smoke_Project_002"));
		explicitWait(tab_overviewProject, 50);
		click(tab_overviewProject);

		explicitWait(tab_invoiceDetailsOverviewTab, 6);
		click(tab_invoiceDetailsOverviewTab);

		explicitWait(
				div_salesInvoiceInviceDetailsTabOverviewDocReplace.replace("doc_no", serviceInvoiceInvoiceProposals),
				15);
		if (isDisplayed(
				div_salesInvoiceInviceDetailsTabOverviewDocReplace.replace("doc_no", serviceInvoiceInvoiceProposals))) {
			writeTestResults("Verify user able to see Sales Invoice number under invoice details tab",
					"User should be able to see Sales Invoice number under invoice details tab",
					"User successfully able to see Sales Invoice number under invoice details tab", "pass");
		} else {
			writeTestResults("Verify user able to see Sales Invoice number under invoice details tab",
					"User should be able to see Sales Invoice number under invoice details tab",
					"User not able to see Sales Invoice number under invoice details tab", "fail");
		}

	}

	/* Smoke_Project_011 */
	public void serviceJobOrderRelease_Smoke_Project_011() throws Exception {
		loginProjectSmoke();
		click(btn_navigationPane);
		explicitWait(btn_service, 10);
		click(btn_service);
		explicitWait(btn_serviceCalender, 10);
		click(btn_serviceCalender);
		explicitWait(btn_interdepartmntalTaskOnWorkingCalender.replace("proj_no", readProjectData("ReleasedProjectID_Smoke_Project_002")), 30);
		click(btn_interdepartmntalTaskOnWorkingCalender.replace("proj_no", readProjectData("ReleasedProjectID_Smoke_Project_002")));
		Thread.sleep(2000);
		switchWindow();
		explicitWait(header_newServiceJobOrder, 40);
		if (isDisplayed(header_newServiceJobOrder)) {
			writeTestResults("Verify user able to navigate to new Service Job Order",
					"User should be able to navigate to new Service Job Order",
					"User successfully navigate to new Service Job Order", "pass");
		} else {
			writeTestResults("Verify user able to navigate to new Service Job Order",
					"User should be able to navigate to new Service Job Order",
					"User doesn't navigate to new Service Job Order", "fail");
		}

		selectIndex(drop_serviceGroupServiceJobOrder, 1);

		selectIndex(drop_priorityServiceJobOrder, 1);

		click(btn_pricingProfileLookup);
		sendKeysLookup(txt_searchPricingProfile, "");
		pressEnter(txt_searchPricingProfile);
		Thread.sleep(2000);
		explicitWait(lnk_firstRecordPricingProfile, 20);
		doubleClick(lnk_firstRecordPricingProfile);

		Thread.sleep(1500);

		click(btn_lookupServiceLocation);
		sendKeysLookup(txt_searchLocation, location);
		pressEnter(txt_searchLocation);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", location), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", location));

		Thread.sleep(1500);

		click(btn_draft);
		explicitWait(header_draftedServiceJobOrder, 50);
		if (isDisplayed(header_draftedServiceJobOrder)) {
			writeTestResults("Verify user able to draft the Service Job Order",
					"User should be able to draft the Service Job Order",
					"User successfully draft the Service Job Order", "pass");
		} else {
			writeTestResults("Verify user able to draft the Service Job Order",
					"User should be able to draft the Service Job Order", "User doesn't draft the Service Job Order",
					"fail");

		}

		click(btn_release);
		explicitWait(header_releasedServiceJobOrder, 50);
		trackCode = getText(lbl_docNumber);
		writeProjectData("releasedSJOUrl_Smoke_Project_002", getCurrentUrl(), 3);
		writeProjectData("releasedSJODocNum_Smoke_Project_011", trackCode, 4);
		if (isDisplayed(header_releasedServiceJobOrder)) {
			writeTestResults("Verify user able to release the Service Job Order",
					"User should be able to release the Service Job Order",
					"User successfully release the Service Job Order", "pass");
		} else {
			writeTestResults("Verify user able to release the Service Job Order",
					"User should be able to release the Service Job Order",
					"User doesn't release the Service Job Order", "fail");

		}

	}

	/* Smoke_Project_012 */
	public void completeSericeJobOrder_Smoke_Project_012() throws Exception {
		loginProjectSmoke();
		openPage(readProjectData("releasedSJOUrl_Smoke_Project_002"));
		explicitWait(btn_action, 40);
		click(btn_action);
		explicitWait(btn_addProductTagNoActions, 40);
		click(btn_addProductTagNoActions);
		explicitWait(heaser_addProductTagNoPopup, 10);
		if (isDisplayed(heaser_addProductTagNoPopup)) {
			writeTestResults("Verify that \"Add Product / Tag No\" popup is popup",
					"\"Add Product / Tag No\" popup should popup", "\"Add Product / Tag No\" popup successfully popup",
					"pass");
		} else {
			writeTestResults("Verify that \"Add Product / Tag No\" popup is popup",
					"\"Add Product / Tag No\" popup should popup", "\"Add Product / Tag No\" popup doesn't popup",
					"fail");
		}

		click(btn_addProductWidgetAddProductPopup);

		click(chk_firstRegSerielAddProductPopup);

		Thread.sleep(2000);

		click(btn_applyAssProductTagNoPopup);

		explicitWait(btn_action, 40);
		click(btn_action);
		explicitWait(btn_stopActionsSJO, 40);
		click(btn_stopActionsSJO);
		explicitWait(header_popupCompleteProcessSJO, 10);
		if (isDisplayed(header_popupCompleteProcessSJO)) {
			writeTestResults("Verify that Complete Process popup is popup", "Complete Process popup should popup",
					"Complete Process popup successfully popup", "pass");
		} else {
			writeTestResults("Verify that Complete Process popup is popup", "Complete Process popup should popup",
					"Complete Process popup doesn't popup", "fail");
		}

		click(btn_completeCompleteProcessSJP);

		((JavascriptExecutor) driver).executeScript("window.open('about:blank','_blank');");
		Thread.sleep(2000);
		switchWindow();

		openPage(readProjectData("releasedProjectUrl_Smoke_Project_002"));
		explicitWait(tab_workBreackdown, 40);
		click(tab_workBreackdown);

		click(btn_expandProjectTask);

		String poc = getText(labl_pocInterdepartmentTaskWorkBreackdown);
		String sjo = getText(labl_sjoNumberInterdepartmentTaskWorkBreackdown);

		if (poc.equals("100")) {
			writeTestResults("Verify that POC is display as 100", "POC should be display as 100",
					"POC successfully diaplay as 100", "pass");
		} else {
			if (poc.equals("100")) {
				writeTestResults("Verify that POC is display as 100", "POC should be display as 100",
						"POC doesn't diaplay as 100", "fail");
			}
		}

		if (sjo.equals(readProjectData("releasedSJODocNum_Smoke_Project_011"))) {
			writeTestResults("Verify that Service Job Order number is display",
					"Service Job Order number successfully display", "Service Job Order number successfully display",
					"pass");
		} else {
			writeTestResults("Verify that Service Job Order number is display",
					"Service Job Order number successfully display", "Service Job Order number doesn't display",
					"fail");
		}

		switchWindow();

		click(btn_action);

		explicitWait(btn_completeSJO, 10);

		click(btn_completeSJO);

		explicitWait(btn_yesCompleteStatusSJO, 10);

		click(btn_yesCompleteStatusSJO);

		explicitWait(txt_serviceJobOrderCompletionPostDate, 10);

		click(txt_serviceJobOrderCompletionPostDate);

		click(btn_dayReplaceServiceJobOrderCompleteDate.replace("day", finObj.currentDay()));

		click(btn_applySJOCompletionDate);

		explicitWait(header_completedServiceJobOrder, 40);
		if (isDisplayed(header_completedServiceJobOrder)) {
			writeTestResults("Verify that Service Job Order is completed", "Service Job Order should be completed",
					"Service Job Order successfully completed", "pass");
		} else {
			writeTestResults("Verify that Service Job Order is completed", "Service Job Order should be completed",
					"Service Job Order doesn't completed", "fail");
		}

	}

	/* Smoke_Project_013 */
	public void pocUpdate_Smoke_Project_013() throws Exception {
		loginProjectSmoke();
		openPage(readProjectData("releasedProjectUrl_Smoke_Project_002"));
		explicitWait(btn_action, 50);
		click(btn_action);
		explicitWait(btn_updateTaskPocActtions, 50);
		click(btn_updateTaskPocActtions);

		explicitWait(drop_taskPOCUpdate, 30);

		selectIndex(drop_taskPOCUpdate, 1);

		sendKeys(txt_presentagePOC, pocPresentage);

		click(btn_applyPOC);

		Thread.sleep(3000);

		explicitWait(btn_action, 50);
		click(btn_action);
		explicitWait(btn_updateTaskPocActtions, 50);
		click(btn_updateTaskPocActtions);

		explicitWait(drop_taskPOCUpdate, 30);

		selectIndex(drop_taskPOCUpdate, 1);

		sendKeys(txt_presentagePOC, pocPresentage);

		click(btn_applyPOC);

		Thread.sleep(3000);

		explicitWait(tab_workBreackdown, 40);
		click(tab_workBreackdown);
		explicitWait(btn_expandProjectTask, 10);
		click(btn_expandProjectTask);
		Thread.sleep(2000);
		String pocTask01 = getText(lbl_pocPresenatgeTaskOneWorkBreackdown);
		String pocTaskSubContract = getText(lbl_pocPresenatgeTaskSubContractWorkBreackdown);

		if (pocTask01.equals("100") && pocTaskSubContract.equals("100")) {
			writeTestResults("Verify that POC is updated for the Task 01 and Sub Conract Task",
					"POC should be updated for the Task 01 and Sub Conract Task",
					"POC successfully updated for the Task 01 and Sub Conract Task", "pass");
		} else {
			writeTestResults("Verify that POC is updated for the Task 01 and Sub Conract Task",
					"POC should be updated for the Task 01 and Sub Conract Task",
					"POC doesn't updated for the Task 01 and/or Sub Conract Task", "fail");
		}

	}

	/* Smoke_Project_014 */
	public void projectCostEstimation_Smoke_Project_014() throws Exception {
		loginProjectSmoke();
		click(btn_navigationPane);
		explicitWait(btn_projectModule, 10);
		click(btn_projectModule);
		explicitWait(btn_projectCostEstimation, 10);
		click(btn_projectCostEstimation);

		explicitWait(btn_newProjectCostEstimation, 40);
		click(btn_newProjectCostEstimation);

		explicitWait(header_newProjectCostEstimation, 40);
		if (isDisplayed(header_newProjectCostEstimation)) {
			writeTestResults("Verify user able to navigate to new Project Cost Estimation",
					"User should be able to navigate to new Project Cost Estimation",
					"User successfully navigate to new Project Cost Estimation", "pass");
		} else {
			writeTestResults("Verify user able to navigate to new Project Cost Estimation",
					"User should be able to navigate to new Project Cost Estimation",
					"User doesn't navigate to new Project Cost Estimation", "fail");
		}

		sendKeys(txt_descriptionCostEstimation, description);

		click(btn_pricingProfileLookupCostEstimation);
		sendKeysLookup(txt_searchPricingProfile, pricingProfile);
		pressEnter(txt_searchPricingProfile);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", pricingProfile), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", pricingProfile));

		Thread.sleep(1500);

		click(tab_estimationDetails);

		click(btn_productLookupRowReplaceCostEstimation.replace("row", "1"));

		sendKeysLookup(txt_searchProduct, batchFifoProduct);
		pressEnter(txt_searchProduct);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", batchFifoProduct), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", batchFifoProduct));

		Thread.sleep(1500);

		selectIndex(drop_warehouseCostEstimation, 1);

		sendKeys(txt_quantityInputProdyctsPCE, inputProductQuantityProjectCostEstimation);

		click(chk_billablePCERowReplace.replace("row", "1"));

		click(btn_addNewRecordGridCommon);

		Thread.sleep(1500);

		selectText(drop_typePCE, typeExpence);

		selectIndex(drop_refrenceCodePCE, 1);

		click(btn_productLookupRowReplaceCostEstimation.replace("row", "2"));

		sendKeysLookup(txt_searchProduct, serviceProduct);
		pressEnter(txt_searchProduct);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", serviceProduct), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", serviceProduct));

		Thread.sleep(1500);

		sendKeys(txt_unitPricePCERowReplace.replace("row", "2"), unitCostServicseProductsPCE);

		click(chk_billablePCERowReplace.replace("row", "2"));
		
		click(btn_tabSummaryCostEstimation);
		
		explicitWaitUntillClickable(btn_checkout, 20);
		click(btn_checkout);
		Thread.sleep(3000);
		
		click(btn_draft);
		explicitWait(header_draftedProjectCostEstimation, 10);
		if (isDisplayed(header_draftedProjectCostEstimation)) {
			writeTestResults("Verify user able to draft the Project Cost Estimation",
					"User should be able to draft the Project Cost Estimation",
					"User successfully draft the Project Cost Estimation", "pass");
		} else {
			writeTestResults("Verify user able to draft the Project Cost Estimation",
					"User should be able to draft the Project Cost Estimation",
					"User doesn't draft the Project Cost Estimation", "fail");
		}

		click(btn_release);
		explicitWait(header_releasedProjectCostEstimation, 10);
		trackCode = getText(lbl_docNumber);
		if (isDisplayed(header_releasedProjectCostEstimation)) {
			writeTestResults("Verify user able to release the Project Cost Estimation",
					"User should be able to release the Project Cost Estimation",
					"User successfully release the Project Cost Estimation", "pass");
		} else {
			writeTestResults("Verify user able to release the Project Cost Estimation",
					"User should be able to release the Project Cost Estimation",
					"User doesn't release the Project Cost Estimation", "fail");
		}

	}

	/* Smoke_Project_015 */
	public void projectModel_Smoke_Project_015() throws Exception {
		loginProjectSmoke();
		click(btn_navigationPane);
		explicitWait(btn_projectModule, 10);
		click(btn_projectModule);
		explicitWait(btn_projectModel, 10);
		click(btn_projectModel);

		explicitWait(btn_newProjectModel, 40);
		click(btn_newProjectModel);

		explicitWait(header_newProjectModel, 40);
		if (isDisplayed(header_newProjectModel)) {
			writeTestResults("Verify user able to navigate to new Project Model",
					"User should be able to navigate to new Project Model",
					"User successfully navigate to new Project Model", "pass");
		} else {
			writeTestResults("Verify user able to navigate to new Project Model",
					"User should be able to navigate to new Project Model",
					"User doesn't navigate to new Project Model", "fail");
		}

		String projectCode = invObj.currentTimeAndDate().replace("/", "").replace(":", "");
		sendKeys(txt_projectCodeProjectModel, projectCode);

		sendKeys(txt_projectModelName, projectName);
		
		sendKeys(txt_daysCountSummaryTabProductionModel, durationOfDaysProdjectModel);
		
		click(btn_lookupResponsiblePerson);
		sendKeysLookup(txt_labourSearch, employee);
		pressEnter(txt_labourSearch);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", employee), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", employee));

		Thread.sleep(1500);

		selectIndex(drop_inputWarehouseProjectModel, 2);
		selectIndex(drop_outputWarehouseProjectModel, 3);

		click(btn_draft);
		explicitWait(header_draftedProjectModel, 40);
		if (isDisplayed(header_draftedProjectModel)) {
			writeTestResults("Verify user able to draft Project Model", "User should be able to draft Project Model",
					"User successfully draft Project Model", "pass");
		} else {
			writeTestResults("Verify user able to draft Project Model", "User should be able to draft Project Model",
					"User doesn't draft Project Model", "fail");
		}

		click(btn_edit);

		explicitWait(btn_update, 20);

		explicitWait(tab_workBreackDownProjectModel, 40);
		click(tab_workBreackDownProjectModel);
		explicitWait(btn_expandProjectTask, 10);
		click(btn_expandProjectTask);
		explicitWait(btn_addTask, 10);
		/* Task 01 */
		click(btn_addTask);
		explicitWait(header_taskWindow, 15);
		if (isDisplayed(header_taskWindow)) {
			writeTestResults("Verify that task window is open", "Task window should be open",
					"Task window successfully open", "pass");
		} else {
			writeTestResults("Verify that task window is open", "Task window should be open",
					"Task window doesn't open", "fail");
		}

		explicitWait(txt_taskNameTaskWindow, 20);
		sendKeys(txt_taskNameTaskWindow, task);
		sendKeys(txt_daysCountSummaryTabProductionModel, durationOfDaysProdjectModel);
		click(btn_inputProductsTabTaskWindow);
		explicitWait(btn_inputProductsLookupTaskWindow, 10);
		click(btn_inputProductsLookupTaskWindow);
		sendKeysLookup(txt_searchProduct, batchFifoProduct);
		pressEnter(txt_searchProduct);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", batchFifoProduct), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", batchFifoProduct));

		Thread.sleep(1500);
		String selectedProduct = getText(div_selectedInputProductTaskWindow);
		if (selectedProduct.contains(batchFifoProduct)) {
			writeTestResults("Verify that user able to select the Input Product",
					"User should be able to select the Input Product", "User successfully select the Input Product",
					"pass");
		} else {
			writeTestResults("Verify that user able to select the Input Product",
					"User should be able to select the Input Product", "User doesn't select the Input Product", "fail");

		}

		sendKeys(txt_quantityInputProductsTaskWindow, quantityFive);

		click(chk_billableTaskWindowInputProducts);

		click(tab_labourTaskWindow);

		explicitWait(btn_lookupProductTaskWindowLabourTab, 10);
		click(btn_lookupProductTaskWindowLabourTab);
		sendKeysLookup(txt_searchProduct, serviceProduct);
		pressEnter(txt_searchProduct);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", serviceProduct), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", serviceProduct));

		Thread.sleep(1500);
		String selectedServiceProductLAbourTabProduct = getText(div_selectedServiceLabourTabTaskWindow);
		if (selectedServiceProductLAbourTabProduct.contains(serviceProduct)) {
			writeTestResults("Verify that user able to select the Service Product",
					"User should be able to select the Service Product", "User successfully select the Service Product",
					"pass");
		} else {
			writeTestResults("Verify that user able to select the Service Product",
					"User should be able to select the Service Product", "User doesn't select the Service Product",
					"fail");

		}

		click(txt_hoursLabourTabTaskWindow);
		click(btn_fiveLabourHoursTaskWindow);
		click(btn_okLabourHoursTaskWindow);

		click(chk_billableLabourHoursTaskWindow);

		click(tab_resourseTaskWindow);

		explicitWait(drop_resourseGroupTaskWindow, 10);
		selectText(drop_resourseGroupTaskWindow, referenceGroupMotorVehicle);

		click(btn_lookupResourseTaskWindow);
		sendKeysLookup(txt_searchResource, resource);
		pressEnter(txt_searchResource);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", resource), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", resource));

		Thread.sleep(1500);
		String selectedResource = getText(div_selectedResourseTaskWindow);
		if (selectedResource.contains(resource)) {
			writeTestResults("Verify that user able to select the Resourse",
					"User should be able to select the Resourse", "User successfully select the Resourse", "pass");
		} else {
			writeTestResults("Verify that user able to select the Resourse",
					"User should be able to select the Resourse", "User doesn't select the Resourse", "fail");

		}

		click(btn_lookupServiceTaskWindowResorseTab);
		sendKeysLookup(txt_searchProduct, serviceProduct2);
		pressEnter(txt_searchProduct);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", serviceProduct2), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", serviceProduct2));

		Thread.sleep(1500);
		String selectedServiceProductResourseTabProduct = getText(div_serviceProductResourseTabTaskWindow);
		if (selectedServiceProductResourseTabProduct.contains(serviceProduct2)) {
			writeTestResults("Verify that user able to select the Service Product",
					"User should be able to select the Service Product", "User successfully select the Service Product",
					"pass");
		} else {
			writeTestResults("Verify that user able to select the Service Product",
					"User should be able to select the Service Product", "User doesn't select the Service Product",
					"fail");

		}

		click(txt_hoursResourseTabTaskWindow);
		click(btn_fiveResourseHoursTaskWindow);
		click(btn_okResourseHoursTaskWindow);

		click(chk_billableResourseTabTaskWindow);

		click(tab_expenceTaskWindow);

		explicitWait(drop_analysisCodeExpenceTabTaskWindow, 10);
		selectIndex(drop_analysisCodeExpenceTabTaskWindow, 1);

		click(btn_nraationExpenceTabTaskWindow);

		explicitWait(txt_narrationTaskWindowExpenceTab, 10);

		sendKeys(txt_narrationTaskWindowExpenceTab, naraation);

		click(btn_applyNarrationExpence);
		Thread.sleep(1500);

		click(btn_serviceLoockupExpenceTabTaskWindow);

		sendKeysLookup(txt_searchProduct, serviceProduct3);
		pressEnter(txt_searchProduct);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info", serviceProduct3), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info", serviceProduct3));

		Thread.sleep(1500);
		String selectedServiceProductExpenceTabProduct = getText(div_selectedServiceExpenceTabTaskWindow);
		if (selectedServiceProductExpenceTabProduct.contains(serviceProduct3)) {
			writeTestResults("Verify that user able to select the Service Product",
					"User should be able to select the Service Product", "User successfully select the Service Product",
					"pass");
		} else {
			writeTestResults("Verify that user able to select the Service Product",
					"User should be able to select the Service Product", "User doesn't select the Service Product",
					"fail");

		}

		sendKeys(txt_estimatedExpenceTaskWindowExpenceTab, expenceEstimatedAmount);

		click(chk_billableExpenceTabTaskWindow);

		click(tab_overheadTaskWindow);

		String autoLoadedOverhead = getText(div_autoLoadedOverhead);
		if (autoLoadedOverhead.contains(overhead)) {
			writeTestResults("Verify that overhead is autoloadede", "Overhead should be loaded automatically",
					"Overhead successfully loaded automatically", "pass");
		} else {
			writeTestResults("Verify that overhead is autoloadede", "Overhead should be loaded automatically",
					"Overhead doesn't loaded automatically", "fail");
		}

		click(chk_billableOverheadTabTaskWindow);

		click(btn_updateTaskWindow);
		
		Thread.sleep(1000);
		explicitWait(btn_update, 25);
		Thread.sleep(2500);
		click(btn_update);

		explicitWait(btn_edit, 40);

		click(btn_release);

		explicitWait(header_releasedProjectModel, 40);
		trackCode = getText(lbl_docNumber);
		if (isDisplayed(header_releasedProjectModel)) {
			writeTestResults("Verify user able to release Project Model",
					"User should be able to release Project Model", "User successfully release Project Model", "pass");
		} else {
			writeTestResults("Verify user able to release Project Model",
					"User should be able to release Project Model", "User doesn't release Project Model", "fail");
		}

	}

}
