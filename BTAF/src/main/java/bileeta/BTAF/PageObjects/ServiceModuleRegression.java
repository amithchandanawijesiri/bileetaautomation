package bileeta.BTAF.PageObjects;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import bileeta.BATF.Pages.ServiceModuleData;

public class ServiceModuleRegression extends ServiceModuleData {
	
	public static String ProjectCode;

	public void navigateToTheLoginPage() throws Exception {
		openPage(siteURL);

		writeTestResults("Verify that 'Entution' page is available", "user should be able to see 'Entution' page",
				"'Entution' page is dislayed", "pass");

	}

	public void verifyTheLogo() throws Exception {
		if (isDisplayed(siteLogo)) {

			writeTestResults("Verify that 'Entution' header is available on the page",
					"user should be able to see the logo", "logo is dislayed", "pass");
		} else {
			writeTestResults("Verify that 'Entution' header is available on the page",
					"user should be able to see the logo", "logo is not dislayed", "fail");

		}
	}

	public void userLogin() throws Exception {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		sendKeys(username, usernamedataA);
		sendKeys(password, passworddataA);
		click(btn_login);

		if (isDisplayed(lnk_home)) {

			writeTestResults("User login", "User should navigate to the Home page of Entution",
					"user navigated to the home page", "pass");
		}

		else {
			writeTestResults("User login", "User should navigate to the Home page of Entution",
					"user cant navigate to the home page", "fail");

		}
	}

	public void clickNavigation() throws Exception {

		WaitClick(btn_navigationmenu);
		click(btn_navigationmenu);

		WaitElement(sidemenu);
		if (isDisplayed(sidemenu)) {
			writeTestResults("verify sidemenu", "view sidemenu", "sidemenu is display", "pass");
		} else {
			writeTestResults("verify sidemenu", "view sidemenu", "sidemenu is not display", "fail");
		}

		WaitClick(btn_servicemodule);
		click(btn_servicemodule);
		WaitElement(subsidemenu);
		if (isDisplayed(subsidemenu)) {
			writeTestResults("verify sub side menu", "view sub side menu", "sub side menu is display", "pass");
		} else {
			writeTestResults("verify sub side menu", "view sub side menu", "sub side menu is not display", "fail");
		}
	}

	// Service_Case_001
	public void navigationCaseForm() throws Exception {

		Thread.sleep(3000);
		click(btn_FormCaseA);
		Thread.sleep(3000);
		click(btn_FormNewCaseA);
		Thread.sleep(3000);
		if (getText(txt_pageHeaderA).contentEquals("Case - New")) {
			writeTestResults("Verify that user can navigate to the Case form", "user can navigate to the Case form",
					"user can navigate to the Case form Successfully", "pass");
		} else {
			writeTestResults("Verify that user can navigate to the Case form", "user can navigate to the Case form",
					"user can navigate to the Case form Fail", "fail");
		}
	}

	// Service_Case_002
	public void VerifyThatFollowingFieldsAreDisplayedAsTheMandatoryFields() throws Exception {

		navigationCaseForm();

		Thread.sleep(3000);
		if (isDisplayed(txt_TitleA)) {
			writeTestResults("Verify Title fields is displayed as the mandatory fields",
					"Title fields is displayed as the mandatory fields",
					"Title fields is displayed as the mandatory fields Successfully", "pass");
		} else {
			writeTestResults("Verify Title fields is displayed as the mandatory fields",
					"Title fields is displayed as the mandatory fields",
					"Title fields is displayed as the mandatory fields Fail", "fail");
		}

		if (isDisplayed(txt_OriginA)) {
			writeTestResults("Verify Origin fields is displayed as the mandatory fields",
					"Origin fields is displayed as the mandatory fields",
					"Origin fields is displayed as the mandatory fields Successfully", "pass");
		} else {
			writeTestResults("Verify Origin fields is displayed as the mandatory fields",
					"Origin fields is displayed as the mandatory fields",
					"Origin fields is displayed as the mandatory fields Fail", "fail");
		}

		if (isDisplayed(txt_CustomerAccountA)) {
			writeTestResults("Verify Customer Account fields is displayed as the mandatory fields",
					"Customer Account fields is displayed as the mandatory fields",
					"Customer Account fields is displayed as the mandatory fields Successfully", "pass");
		} else {
			writeTestResults("Verify Customer Account fields is displayed as the mandatory fields",
					"Customer Account fields is displayed as the mandatory fields",
					"Customer Account fields is displayed as the mandatory fields Fail", "fail");
		}

		if (isDisplayed(txt_ProductA)) {
			writeTestResults("Verify Product fields is displayed as the mandatory fields",
					"Product fields is displayed as the mandatory fields",
					"Product fields is displayed as the mandatory fields Successfully", "pass");
		} else {
			writeTestResults("Verify Product fields is displayed as the mandatory fields",
					"Product fields is displayed as the mandatory fields",
					"Product fields is displayed as the mandatory fields Fail", "fail");
		}

		if (isDisplayed(txt_TagNoA)) {
			writeTestResults("Verify Tag No fields is displayed as the mandatory fields",
					"Tag No fields is displayed as the mandatory fields",
					"Tag No fields is displayed as the mandatory fields Successfully", "pass");
		} else {
			writeTestResults("Verify Tag No fields is displayed as the mandatory fields",
					"Tag No fields is displayed as the mandatory fields",
					"Tag No fields is displayed as the mandatory fields Fail", "fail");
		}

		if (isDisplayed(txt_TypeA)) {
			writeTestResults("Verify Type fields is displayed as the mandatory fields",
					"Type fields is displayed as the mandatory fields",
					"Type fields is displayed as the mandatory fields Successfully", "pass");
		} else {
			writeTestResults("Verify Type fields is displayed as the mandatory fields",
					"Type fields is displayed as the mandatory fields",
					"Type fields is displayed as the mandatory fields Fail", "fail");
		}

		if (isDisplayed(txt_ServiceGroupA)) {
			writeTestResults("Verify Service Group fields is displayed as the mandatory fields",
					"Service Group fields is displayed as the mandatory fields",
					"Service Group fields is displayed as the mandatory fields Successfully", "pass");
		} else {
			writeTestResults("Verify Service Group fields is displayed as the mandatory fields",
					"Service Group fields is displayed as the mandatory fields",
					"Service Group fields is displayed as the mandatory fields Fail", "fail");
		}

		if (isDisplayed(txt_PriorityA)) {
			writeTestResults("Verify Priority fields is displayed as the mandatory fields",
					"Priority fields is displayed as the mandatory fields",
					"Priority fields is displayed as the mandatory fields Successfully", "pass");
		} else {
			writeTestResults("Verify Priority fields is displayed as the mandatory fields",
					"Priority fields is displayed as the mandatory fields",
					"Priority fields is displayed as the mandatory fields Fail", "fail");
		}

		if (isDisplayed(txt_CurrencyA)) {
			writeTestResults("Verify Currency fields is displayed as the mandatory fields",
					"Currency fields is displayed as the mandatory fields",
					"Currency fields is displayed as the mandatory fields Successfully", "pass");
		} else {
			writeTestResults("Verify Currency fields is displayed as the mandatory fields",
					"Currency fields is displayed as the mandatory fields",
					"Currency fields is displayed as the mandatory fields Fail", "fail");
		}

		if (isDisplayed(txt_BillingAddressA)) {
			writeTestResults("Verify Billing Address fields is displayed as the mandatory fields",
					"Billing Address fields is displayed as the mandatory fields",
					"Billing Address fields is displayed as the mandatory fields Successfully", "pass");
		} else {
			writeTestResults("Verify Billing Address fields is displayed as the mandatory fields",
					"Billing Address fields is displayed as the mandatory fields",
					"Billing Address fields is displayed as the mandatory fields Fail", "fail");
		}

		if (isDisplayed(txt_ShippingAddressA)) {
			writeTestResults("Verify Shipping Address fields is displayed as the mandatory fields",
					"Shipping Address fields is displayed as the mandatory fields",
					"Shipping Address fields is displayed as the mandatory fields Successfully", "pass");
		} else {
			writeTestResults("Verify Shipping Address fields is displayed as the mandatory fields",
					"Shipping Address fields is displayed as the mandatory fields",
					"Shipping Address fields is displayed as the mandatory fields Fail", "fail");
		}
	}

	// Service_Case_003
	public void VerifyThatMandatoryFieldsValidationAreFiredForTheFollowingMandatoryFields() throws Exception {

		navigationCaseForm();
		Thread.sleep(3000);
		click(btn_DraftA);
		Thread.sleep(3000);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("$(closeMessageBox()).click()");

		if (isDisplayed(txt_TitleValidatorA) && isDisplayed(txt_OriginValidatorA) && isDisplayed(txt_CustomerAccountValidatorA)
				 && isDisplayed(txt_ProductValidatorA) && isDisplayed(txt_TagNoValidatorA) && isDisplayed(txt_TypeValidatorA) && isDisplayed(txt_ServiceGroupValidatorA)) {
			writeTestResults("Verify that mandatory fields validation are fired for the following mandatory fields",
					"mandatory fields validation are fired for the following mandatory fields",
					"mandatory fields validation are fired for the following mandatory fields Successfully", "pass");
		} else {
			writeTestResults("Verify that mandatory fields validation are fired for the following mandatory fields",
					"mandatory fields validation are fired for the following mandatory fields",
					"mandatory fields validation are fired for the following mandatory fields Fail", "fail");
		}
	}

	// Service_Case_004
	public void VerifyThatAbleToDraftTheCaseViaMainMenu() throws Exception {

		navigationCaseForm();
		Thread.sleep(1000);
		LocalTime obj = LocalTime.now();
		sendKeys(txt_TitleTextA, "Case" + obj);
		selectText(txt_OriginTextA, OriginTextFieldA);

		click(btn_CustomerAccountButtonA);
		sendKeys(txt_CustomerAccountTextA, CustomerAccountTextA);
		pressEnter(txt_CustomerAccountTextA);
		WaitElement(sel_CustomerAccountSelA);
		doubleClick(sel_CustomerAccountSelA);

		WaitClick(btn_newWindowProdA);
		click(btn_newWindowProdA);
		WaitElement(sel_newWindowProdSelA);
		click(sel_newWindowProdSelA);
		Thread.sleep(3000);
		JavascriptExecutor j1 = (JavascriptExecutor) driver;
		j1.executeScript("$(\"#divServiceSerial\").parent().find('.dialogbox-buttonarea > .button').click()");

		selectText(txt_TypeTextA, TypeTextFieldA);
		selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);
		selectText(txt_PriorityTextA, PriorityTextFieldA);

		WaitClick(btn_DraftA);;
		click(btn_DraftA);
		WaitElement(txt_PageDraft1A);
		if (isDisplayed(txt_PageDraft1A)) {
			writeTestResults("Verify that able to draft the Case via main menu", "able to draft the Case via main menu",
					"able to draft the Case via main menu Successfully", "pass");
		} else {
			writeTestResults("Verify that able to draft the Case via main menu", "able to draft the Case via main menu",
					"able to draft the Case via main menu Fail", "fail");
		}
	}

	// Service_Case_005
	public void VerifyThatAbleToReleaseTheCaseCreatedViaMenu() throws Exception {

		VerifyThatAbleToDraftTheCaseViaMainMenu();

		Thread.sleep(3000);
		click(btn_ReleaseA);
		Thread.sleep(3000);
		trackCode = getText(txt_trackCodeA);
		WaitElement(txt_PageRelease1A);
		if (isDisplayed(txt_PageRelease1A)) {
			writeTestResults("Verify that able to Release the Case via main menu",
					"able to Release the Case via main menu", "able to Release the Case via main menu Successfully",
					"pass");
		} else {
			writeTestResults("Verify that able to Release the Case via main menu",
					"able to Release the Case via main menu", "able to Release the Case via main menu Fail", "fail");
		}
	}

	// Service_Case_006
	public void VerifyThatUserAbleToReleaseaCaseViaRolecenter() throws Exception {

		Thread.sleep(3000);
		click(btn_RolecenterA);

		Thread.sleep(3000);
		click(btn_searchTagA);
		sendKeys(txt_searchTagtxtA, "s1.1");
		Thread.sleep(3000);
		pressEnter(txt_searchTagtxtA);
		Thread.sleep(3000);
		click(btn_searchTagarrowA);

		switchWindow();

		Thread.sleep(3000);
		LocalTime obj = LocalTime.now();
		sendKeys(txt_TitleTextA, "Case" + obj);
		selectText(txt_OriginTextA, OriginTextFieldA);

		selectText(txt_TypeTextA, TypeTextFieldA);
		selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);
		selectText(txt_PriorityTextA, PriorityTextFieldA);

		Thread.sleep(3000);
		click(btn_DraftA);
		Thread.sleep(5000);
		if (isDisplayed(txt_PageDraft1A)) {
			writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
					"User Can draft the Case Form Successfully", "pass");
		} else {
			writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
					"User Can draft the Case Form Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_ReleaseA);
		Thread.sleep(3000);
		trackCode = getText(txt_trackCodeA);
		Thread.sleep(5000);
		if (isDisplayed(txt_PageRelease1A)) {
			writeTestResults("Verify that User able to release a case Via Role center",
					"User able to release a case Via Role center",
					"User able to release a case Via Role center Successfully", "pass");
		} else {
			writeTestResults("Verify that User able to release a case Via Role center",
					"User able to release a case Via Role center", "User able to release a case Via Role center Fail",
					"fail");
		}

	}

	// Service_Case_007
	public void VerifyThatFollowingActionsAreAvailable() throws Exception {

		navigationCaseForm();
		Thread.sleep(1000);
		LocalTime obj = LocalTime.now();
		sendKeys(txt_TitleTextA, "Case" + obj);
		selectText(txt_OriginTextA, OriginTextFieldA);

		click(btn_CustomerAccountButtonA);
		sendKeys(txt_CustomerAccountTextA, CustomerAccountTextA);
		Thread.sleep(3000);
		pressEnter(txt_CustomerAccountTextA);
		Thread.sleep(3000);
		doubleClick(sel_CustomerAccountSelA);

		Thread.sleep(3000);
		click(btn_newWindowProdA);
		Thread.sleep(3000);
		click(sel_newWindowProdSelA);
		Thread.sleep(3000);
		JavascriptExecutor j1 = (JavascriptExecutor) driver;
		j1.executeScript("$(\"#divServiceSerial\").parent().find('.dialogbox-buttonarea > .button').click()");

		selectText(txt_TypeTextA, TypeTextFieldA);
		selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);
		selectText(txt_PriorityTextA, PriorityTextFieldA);

		Thread.sleep(3000);
		click(btn_DraftA);
		Thread.sleep(5000);
		if (isDisplayed(txt_PageDraft1A)) {
			writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
					"User Can draft the Case Form Successfully", "pass");
		} else {
			writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
					"User Can draft the Case Form Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_ReleaseA);
		Thread.sleep(3000);
		trackCode = getText(txt_trackCodeA);
		Thread.sleep(5000);
		if (isDisplayed(txt_PageRelease1A)) {
			writeTestResults("Verify that User Can Release the Case Form", "User Can Release the Case Form",
					"User Can Release the Case Form Successfully", "pass");
		} else {
			writeTestResults("Verify that User Can Release the Case Form", "User Can Release the Case Form",
					"User Can Release the Case Form Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_ActionBtnA);

		if (isDisplayed(btn_CreateServiceQuotationBtnA)) {
			writeTestResults("Verify that Create Service Quotation actions are available",
					"Create Service Quotation actions are available",
					"Create Service Quotation actions are available Successfully", "pass");
		} else {
			writeTestResults("Verify that Create Service Quotation actions are available",
					"Create Service Quotation actions are available",
					"Create Service Quotation actions are available Fail", "fail");
		}

		if (isDisplayed(btn_DroptoServiceCentreBtnA)) {
			writeTestResults("Verify that Drop to Service Centre actions are available",
					"Drop to Service Centre actions are available",
					"Drop to Service Centre actions are available Successfully", "pass");
		} else {
			writeTestResults("Verify that Drop to Service Centre actions are available",
					"Drop to Service Centre actions are available", "Drop to Service Centre actions are available Fail",
					"fail");
		}

		if (isDisplayed(btn_ResponseDetailBtnA)) {
			writeTestResults("Verify that Response Detail actions are available",
					"Response Detail actions are available", "Response Detail actions are available Successfully",
					"pass");
		} else {
			writeTestResults("Verify that Response Detail actions are available",
					"Response Detail actions are available", "Response Detail actions are available Fail", "fail");
		}
	}

	// Service_Case_008
	public void VerificationOfCaseCanByDuplicatingAnExistingCase() throws Exception {

		navigationCaseForm();
		Thread.sleep(1000);
		LocalTime obj = LocalTime.now();
		sendKeys(txt_TitleTextA, "Case" + obj);

		selectText(txt_OriginTextA, OriginTextFieldA);

		click(btn_CustomerAccountButtonA);
		sendKeys(txt_CustomerAccountTextA, CustomerAccountTextA);
		Thread.sleep(3000);
		pressEnter(txt_CustomerAccountTextA);
		Thread.sleep(3000);
		doubleClick(sel_CustomerAccountSelA);

		Thread.sleep(3000);
		click(btn_newWindowProdA);
		Thread.sleep(3000);
		click(sel_newWindowProdSelA);
		Thread.sleep(3000);
		JavascriptExecutor j1 = (JavascriptExecutor) driver;
		j1.executeScript("$(\"#divServiceSerial\").parent().find('.dialogbox-buttonarea > .button').click()");

		selectText(txt_TypeTextA, TypeTextFieldA);

		selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);

		selectText(txt_PriorityTextA, PriorityTextFieldA);

		Thread.sleep(3000);
		click(btn_DraftA);
		Thread.sleep(5000);
		if (isDisplayed(txt_PageDraft1A)) {
			writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
					"User Can draft the Case Form Successfully", "pass");
		} else {
			writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
					"User Can draft the Case Form Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_ReleaseA);
		Thread.sleep(3000);
		trackCode = getText(txt_trackCodeA);
		Thread.sleep(5000);
		if (isDisplayed(txt_PageRelease1A)) {
			writeTestResults("Verify that User Can Release the Case Form", "User Can Release the Case Form",
					"User Can Release the Case Form Successfully", "pass");
		} else {
			writeTestResults("Verify that User Can Release the Case Form", "User Can Release the Case Form",
					"User Can Release the Case Form Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_DuplicateA);

		Thread.sleep(3000);
		if (getText(txt_pageHeaderA).contentEquals("Case - New")) {
			writeTestResults("Verify that user can navigate to the Case form", "user can navigate to the Case form",
					"user can navigate to the Case form Successfully", "pass");
		} else {
			writeTestResults("Verify that user can navigate to the Case form", "user can navigate to the Case form",
					"user can navigate to the Case form Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_DraftA);
		Thread.sleep(5000);
		if (isDisplayed(txt_PageDraft1A)) {
			writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
					"User Can draft the Case Form Successfully", "pass");
		} else {
			writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
					"User Can draft the Case Form Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_ReleaseA);
		Thread.sleep(3000);
		trackCode = getText(txt_trackCodeA);
		Thread.sleep(5000);
		if (isDisplayed(txt_PageRelease1A)) {
			writeTestResults("Verify that User Can Release the duplicating an existing Case Form", "User Can Release the duplicating an existing Case Form",
					"User Can Release the duplicating an existing Case Form Successfully", "pass");
		} else {
			writeTestResults("Verify that User Can Release the duplicating an existing Case Form", "User Can Release the duplicating an existing Case Form",
					"User Can Release the duplicating an existing Case Form Fail", "fail");
		}
	}

	// Service_Case_009
	public void VerifyThatAbleToDeleteTheDraftCase() throws Exception {

		navigationCaseForm();
		Thread.sleep(1000);
		LocalTime obj = LocalTime.now();
		sendKeys(txt_TitleTextA, "Case" + obj);

		selectText(txt_OriginTextA, OriginTextFieldA);

		click(btn_CustomerAccountButtonA);
		sendKeys(txt_CustomerAccountTextA, CustomerAccountTextA);
		Thread.sleep(3000);
		pressEnter(txt_CustomerAccountTextA);
		Thread.sleep(3000);
		doubleClick(sel_CustomerAccountSelA);

		Thread.sleep(3000);
		click(btn_newWindowProdA);
		Thread.sleep(3000);
		click(sel_newWindowProdSelA);
		Thread.sleep(3000);
		JavascriptExecutor j1 = (JavascriptExecutor) driver;
		j1.executeScript("$(\"#divServiceSerial\").parent().find('.dialogbox-buttonarea > .button').click()");

		selectText(txt_TypeTextA, TypeTextFieldA);

		selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);

		selectText(txt_PriorityTextA, PriorityTextFieldA);

		Thread.sleep(3000);
		click(btn_DraftA);
		Thread.sleep(5000);
		if (isDisplayed(txt_PageDraft1A)) {
			writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
					"User Can draft the Case Form Successfully", "pass");
		} else {
			writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
					"User Can draft the Case Form Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_ActionBtnA);
		Thread.sleep(3000);
		click(btn_DeleteBtnA);
		Thread.sleep(3000);
		j1.executeScript("$('#divConfirmAction').nextAll().find('.pic16-apply').click()");

		Thread.sleep(5000);
		if (isDisplayed(txt_pageDeletedA)) {
			writeTestResults("Verify that able to delete the Draft case", "User Can delete the Draft Case Form",
					"User Can delete the Draft Case Form Successfully", "pass");
		} else {
			writeTestResults("Verify that able to delete the Draft case", "User Can delete the Draft Case Form",
					"User Can delete the Draft Case Form Fail", "fail");
		}
	}

	// Service_Case_010
	public void VerifyThatAbleToEditUpdateTheCase() throws Exception {

		navigationCaseForm();
		Thread.sleep(1000);
		LocalTime obj = LocalTime.now();
		sendKeys(txt_TitleTextA, "Case" + obj);

		selectText(txt_OriginTextA, OriginTextFieldA);

		click(btn_CustomerAccountButtonA);
		sendKeys(txt_CustomerAccountTextA, CustomerAccountTextA);
		Thread.sleep(3000);
		pressEnter(txt_CustomerAccountTextA);
		Thread.sleep(3000);
		doubleClick(sel_CustomerAccountSelA);

		Thread.sleep(3000);
		click(btn_newWindowProdA);
		Thread.sleep(3000);
		click(sel_newWindowProdSelA);
		Thread.sleep(3000);
		JavascriptExecutor j1 = (JavascriptExecutor) driver;
		j1.executeScript("$(\"#divServiceSerial\").parent().find('.dialogbox-buttonarea > .button').click()");

		selectText(txt_TypeTextA, TypeTextFieldA);

		selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);

		selectText(txt_PriorityTextA, PriorityTextFieldA);

		Thread.sleep(3000);
		click(btn_DraftA);
		Thread.sleep(5000);
		if (isDisplayed(txt_PageDraft1A)) {
			writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
					"User Can draft the Case Form Successfully", "pass");
		} else {
			writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
					"User Can draft the Case Form Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_EditA);

		Thread.sleep(3000);
		selectText(txt_OriginTextA, "E-mail");
		selectText(txt_TypeTextA, "Question");

		Thread.sleep(3000);
		click(btn_UpdateA);

		Thread.sleep(3000);
		String Origin = getText(txt_OriginTextFieldA);
		String Type = getText(txt_TypeTextFieldA);

		if (Origin.contentEquals("E-mail") && Type.contentEquals("Question")) {
			writeTestResults("Verify that able to Edit-Update the Case", "User Can Edit-Update the Case Form",
					"User Can Edit-Update the Case Form Successfully", "pass");
		} else {
			writeTestResults("Verify that able to Edit-Update the Case", "User Can Edit-Update the Case Form",
					"User Can Edit-Update the Case Form Fail", "fail");
		}
	}

	// Service_Case_011
	public void VerificationOfNewCaseCanBeCreateViaUpdateAndNew() throws Exception {

		navigationCaseForm();
		Thread.sleep(1000);
		LocalTime obj = LocalTime.now();
		sendKeys(txt_TitleTextA, "Case" + obj);

		selectText(txt_OriginTextA, OriginTextFieldA);

		click(btn_CustomerAccountButtonA);
		sendKeys(txt_CustomerAccountTextA, CustomerAccountTextA);
		Thread.sleep(3000);
		pressEnter(txt_CustomerAccountTextA);
		Thread.sleep(3000);
		doubleClick(sel_CustomerAccountSelA);

		Thread.sleep(3000);
		click(btn_newWindowProdA);
		Thread.sleep(3000);
		click(sel_newWindowProdSelA);
		Thread.sleep(3000);
		JavascriptExecutor j1 = (JavascriptExecutor) driver;
		j1.executeScript("$(\"#divServiceSerial\").parent().find('.dialogbox-buttonarea > .button').click()");

		selectText(txt_TypeTextA, TypeTextFieldA);

		selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);

		selectText(txt_PriorityTextA, PriorityTextFieldA);

		Thread.sleep(3000);
		click(btn_DraftA);
		Thread.sleep(5000);
		if (isDisplayed(txt_PageDraft1A)) {
			writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
					"User Can draft the Case Form Successfully", "pass");
		} else {
			writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
					"User Can draft the Case Form Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_EditA);

		Thread.sleep(3000);
		selectText(txt_OriginTextA, "E-mail");
		selectText(txt_TypeTextA, "Question");

		Thread.sleep(3000);
		click(btn_UpdateAndNewA);
		Thread.sleep(3000);
		if (getText(txt_pageHeaderA).contentEquals("Case - New")) {
			writeTestResults("Verification of New case can be create via Update and New",
					"Case-New form is displayed After doing Update and New",
					"Case-New form is displayed After doing Update and New Successfully", "pass");
		} else {
			writeTestResults("Verification of New case can be create via Update and New",
					"Case-New form is displayed After doing Update and New",
					"Case-New form is displayed After doing Update and New Fail", "fail");
		}
	}

	// Service_Case_012
	public void CreateNewCaseViaDraftAndNew() throws Exception {

		navigationCaseForm();
		Thread.sleep(1000);
		LocalTime obj = LocalTime.now();
		sendKeys(txt_TitleTextA, "Case" + obj);

		selectText(txt_OriginTextA, OriginTextFieldA);

		click(btn_CustomerAccountButtonA);
		sendKeys(txt_CustomerAccountTextA, CustomerAccountTextA);
		Thread.sleep(3000);
		pressEnter(txt_CustomerAccountTextA);
		Thread.sleep(3000);
		doubleClick(sel_CustomerAccountSelA);

		Thread.sleep(3000);
		click(btn_newWindowProdA);
		Thread.sleep(3000);
		click(sel_newWindowProdSelA);
		Thread.sleep(3000);
		JavascriptExecutor j1 = (JavascriptExecutor) driver;
		j1.executeScript("$(\"#divServiceSerial\").parent().find('.dialogbox-buttonarea > .button').click()");

		selectText(txt_TypeTextA, TypeTextFieldA);

		selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);

		selectText(txt_PriorityTextA, PriorityTextFieldA);

		Thread.sleep(3000);
		click(btn_DraftAndNewA);
		Thread.sleep(3000);
		if (getText(txt_pageHeaderA).contentEquals("Case - New")) {
			writeTestResults("Verification of New case can be create via Draft and New",
					"Case-New form is displayed After doing Draft and New",
					"Case-New form is displayed After doing Draft and New Successfully", "pass");
		} else {
			writeTestResults("Verification of New case can be create via Draft and New",
					"Case-New form is displayed After doing Draft and New",
					"Case-New form is displayed After doing Draft and New Fail", "fail");
		}

		click(btn_CaseFormByPageA);
		Thread.sleep(3000);
		click(sel_DraftedCaseDocA);
		Thread.sleep(5000);
		if (isDisplayed(txt_PageDraft1A)) {
			writeTestResults("Verify that Drafted document is available", "Drafted document is available",
					"Drafted document is available Successfully", "pass");
		} else {
			writeTestResults("Verify that Drafted document is available", "Drafted document is available",
					"Drafted document is available Fail", "fail");
		}
	}

	// Service_Case_013
	public void CreateNewCaseWithCopyingExistingCaseCopyFrom() throws Exception {

		navigationCaseForm();

		Thread.sleep(3000);
		click(btn_CopyFromA);
		sendKeys(txt_CopyFromSerchtxtA, CopyFromSerchtxtA);
		Thread.sleep(3000);
		pressEnter(txt_CopyFromSerchtxtA);
		WaitElement(sel_CopyFromSerchselA);
		doubleClick(sel_CopyFromSerchselA);

		Thread.sleep(3000);
		click(btn_DraftA);
		Thread.sleep(5000);
		if (isDisplayed(txt_PageDraft1A)) {
			writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
					"User Can draft the Case Form Successfully", "pass");
		} else {
			writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
					"User Can draft the Case Form Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_ReleaseA);
		Thread.sleep(3000);
		trackCode = getText(txt_trackCodeA);
		Thread.sleep(5000);
		if (isDisplayed(txt_PageRelease1A)) {
			writeTestResults("Verify that User Can Release the Case Form", "User Can Release the Case Form",
					"User Can Release the Case Form Successfully", "pass");
		} else {
			writeTestResults("Verify that User Can Release the Case Form", "User Can Release the Case Form",
					"User Can Release the Case Form Fail", "fail");
		}

		Thread.sleep(3000);
		if (getText(txt_TitleTextFieldA).contentEquals(CopyFromSerchtitleA)) {
			writeTestResults("Verify that copied details are displayed for Title Text Field",
					"copied details are displayed for Title Text Field",
					"copied details are displayed for Title Text Field Successfully", "pass");
		} else {
			writeTestResults("Verify that copied details are displayed for Title Text Field",
					"copied details are displayed for Title Text Field",
					"copied details are displayed for Title Text Field Fail", "fail");
		}

		if (getText(txt_OriginTextFieldA).contentEquals(OriginTextFieldA)) {
			writeTestResults("Verify that copied details are displayed for Origin Text Field",
					"copied details are displayed for Origin Text Field",
					"copied details are displayed for Origin Text Field Successfully", "pass");
		} else {
			writeTestResults("Verify that copied details are displayed for Origin Text Field",
					"copied details are displayed for Origin Text Field",
					"copied details are displayed for Origin Text Field Fail", "fail");
		}

		if (getText(txt_CustomerAccountTextFieldA).contentEquals(CustomerAccountCodeAndDicriptionA)) {
			writeTestResults("Verify that copied details are displayed for Customer Account Text Field",
					"copied details are displayed for Customer Account Text Field",
					"copied details are displayed for Customer Account Text Field Successfully", "pass");
		} else {
			writeTestResults("Verify that copied details are displayed for Customer Account Text Field",
					"copied details are displayed for Customer Account Text Field",
					"copied details are displayed for Customer Account Text Field Fail", "fail");
		}

		if (getText(txt_ProductTextFieldA).contentEquals(ProductTextFieldA)) {
			writeTestResults("Verify that copied details are displayed for Product Text Field",
					"copied details are displayed for Product Text Field",
					"copied details are displayed for Product Text Field Successfully", "pass");
		} else {
			writeTestResults("Verify that copied details are displayed for Product Text Field",
					"copied details are displayed for Product Text Field",
					"copied details are displayed for Product Text Field Fail", "fail");
		}

		if (getText(txt_TagNoTextFieldA).contentEquals(TagNoTextFieldA)) {
			writeTestResults("Verify that copied details are displayed for Tag No Text Field",
					"copied details are displayed for Tag No Text Field",
					"copied details are displayed for Tag No Text Field Successfully", "pass");
		} else {
			writeTestResults("Verify that copied details are displayed for Tag No Text Field",
					"copied details are displayed for Tag No Text Field",
					"copied details are displayed for Tag No Text Field Fail", "fail");
		}

		if (getText(txt_TypeTextFieldA).contentEquals(TypeTextFieldA)) {
			writeTestResults("Verify that copied details are displayed for Type Text Field",
					"copied details are displayed for Type Text Field",
					"copied details are displayed for Type Text Field Successfully", "pass");
		} else {
			writeTestResults("Verify that copied details are displayed for Type Text Field",
					"copied details are displayed for Type Text Field",
					"copied details are displayed for Type Text Field Fail", "fail");
		}

		if (getText(txt_ServiceGroupTextFieldA).contentEquals(ServiceGroupTextFieldA)) {
			writeTestResults("Verify that copied details are displayed for Service Group Text Field",
					"copied details are displayed for Service Group Text Field",
					"copied details are displayed for Service Group Text Field Successfully", "pass");
		} else {
			writeTestResults("Verify that copied details are displayed for Service Group Text Field",
					"copied details are displayed for Service Group Text Field",
					"copied details are displayed for Service Group Text Field Fail", "fail");
		}

		if (getText(txt_PriorityTextFieldA).contentEquals(PriorityTextFieldA)) {
			writeTestResults("Verify that copied details are displayed for Priority Text Field",
					"copied details are displayed for Priority Text Field",
					"copied details are displayed for Priority Text Field Successfully", "pass");
		} else {
			writeTestResults("Verify that copied details are displayed for Priority Text Field",
					"copied details are displayed for Priority Text Field",
					"copied details are displayed for Priority Text Field Fail", "fail");
		}
	}

	// Service_Case_014
	public void VerifyThatAbleToReverseTheDocument() throws Exception {

		VerifyThatUserAbleToReleaseaCaseViaRolecenter();
		Thread.sleep(3000);
		click(btn_ActionBtnA);
		Thread.sleep(3000);
		click(btn_ReverseBtnA);
		Thread.sleep(3000);
		JavascriptExecutor j1 = (JavascriptExecutor) driver;
		j1.executeScript("$('#divConfirmAction').nextAll().find('.pic16-apply').click()");

		Thread.sleep(5000);
		if (isDisplayed(txt_pageReversedA)) {
			writeTestResults("Verify that able to reverse the document", "User Can reverse the Case Form",
					"User Can reverse the Case Form Successfully", "pass");
		} else {
			writeTestResults("Verify that able to reverse the document", "User Can reverse the Case Form",
					"User Can reverse the Case Form Fail", "fail");
		}
	}

	// Service_Case_015
	public void VerifyThatTheFormMandatoryFieldsWhenDocumentInDraftMode() throws Exception {

		navigationCaseForm();
		Thread.sleep(1000);
		LocalTime obj = LocalTime.now();
		sendKeys(txt_TitleTextA, "Case" + obj);

		selectText(txt_OriginTextA, OriginTextFieldA);

		click(btn_CustomerAccountButtonA);
		sendKeys(txt_CustomerAccountTextA, CustomerAccountTextA);
		Thread.sleep(3000);
		pressEnter(txt_CustomerAccountTextA);
		Thread.sleep(3000);
		doubleClick(sel_CustomerAccountSelA);

		Thread.sleep(3000);
		click(btn_newWindowProdA);
		Thread.sleep(3000);
		click(sel_newWindowProdSelA);
		Thread.sleep(3000);
		JavascriptExecutor j1 = (JavascriptExecutor) driver;
		j1.executeScript("$(\"#divServiceSerial\").parent().find('.dialogbox-buttonarea > .button').click()");

		selectText(txt_TypeTextA, TypeTextFieldA);

		selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);

		selectText(txt_PriorityTextA, PriorityTextFieldA);

		Thread.sleep(3000);
		click(btn_DraftA);
		Thread.sleep(5000);
		if (isDisplayed(txt_PageDraft1A)) {
			writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
					"User Can draft the Case Form Successfully", "pass");
		} else {
			writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
					"User Can draft the Case Form Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_EditA);

		Thread.sleep(3000);
		sendKeys(txt_TitleTextA, "");
		selectText(txt_OriginTextA, "--None--");
		
		selectText(txt_TypeTextA, "--None--");
		selectText(txt_ServiceGroupTextA, "--None--");
		
		Thread.sleep(3000);
		click(btn_BillingAddressDeleteA);
		click(btn_ShippingAddressDeleteA);

		Thread.sleep(3000);
		click(btn_UpdateA);
		Thread.sleep(5000);
		if (isDisplayed(txt_TitleValidatorA) && isDisplayed(txt_OriginValidatorA) && isDisplayed(txt_TypeValidatorA) 
				&& isDisplayed(txt_ServiceGroupValidatorA) && isDisplayed(txt_BillingAddressValidatorA) && isDisplayed(txt_ShippingAddressValidatorA)) {
			writeTestResults(" Verify required fields validations are displayed",
					"required fields validations are displayed",
					"required fields validations are displayed Successfully", "pass");
		} else {
			writeTestResults(" Verify required fields validations are displayed",
					"required fields validations are displayed", "required fields validations are displayed Fail",
					"fail");
		}

	}

	// Service_Case_016
	public void VerifyThatStageAndStatusOfRreleasedCaseIsDisplayedAsOpen() throws Exception {

		VerifyThatAbleToReleaseTheCaseCreatedViaMenu();
		Thread.sleep(3000);
		String StageFieldA = new Select(driver.findElement(By.xpath(txt_StageFieldA))).getFirstSelectedOption()
				.getText();
		Thread.sleep(3000);
		click(btn_StatusFieldArrowA);
		String StatusFieldA = getText(txt_StatusFieldTxtA);

		Thread.sleep(5000);
		if (StageFieldA.contentEquals("Open") && StatusFieldA.contentEquals("Open")) {
			writeTestResults("Verify that \"Stage\" and \"Status\" of released case is displayed as \"Open\"",
					"\"Stage\" and \"Status\" of released case is displayed as \"Open\"",
					"\"Stage\" and \"Status\" of released case is displayed as \"Open\" Successfully", "pass");
		} else {
			writeTestResults("Verify that \"Stage\" and \"Status\" of released case is displayed as \"Open\"",
					"\"Stage\" and \"Status\" of released case is displayed as \"Open\"",
					"\"Stage\" and \"Status\" of released case is displayed as \"Open\" Fail", "fail");
		}
	}

	// Service_Case_017
	public void VerifyThatAbleToCreateServiceQuotationUsingaCaseWhichCreatedFromMenu() throws Exception {

		navigationCaseForm();
		Thread.sleep(1000);
		LocalTime obj = LocalTime.now();
		sendKeys(txt_TitleTextA, "Case" + obj);
		selectText(txt_OriginTextA, OriginTextFieldA);

		click(btn_CustomerAccountButtonA);
		sendKeys(txt_CustomerAccountTextA, CustomerAccountTextA);
		Thread.sleep(3000);
		pressEnter(txt_CustomerAccountTextA);
		Thread.sleep(3000);
		doubleClick(sel_CustomerAccountSelA);

		Thread.sleep(3000);
		click(btn_newWindowProdA);
		Thread.sleep(3000);
		click(sel_newWindowProdSelA);
		Thread.sleep(3000);
		JavascriptExecutor j1 = (JavascriptExecutor) driver;
		j1.executeScript("$(\"#divServiceSerial\").parent().find('.dialogbox-buttonarea > .button').click()");

		selectText(txt_TypeTextA, TypeTextFieldA);
		selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);
		selectText(txt_PriorityTextA, PriorityTextFieldA);

		Thread.sleep(3000);
		click(btn_DraftA);
		Thread.sleep(5000);
		if (isDisplayed(txt_PageDraft1A)) {
			writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
					"User Can draft the Case Form Successfully", "pass");
		} else {
			writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
					"User Can draft the Case Form Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_ReleaseA);
		Thread.sleep(3000);
		trackCode = getText(txt_trackCodeA);
		Thread.sleep(5000);
		if (isDisplayed(txt_PageRelease1A)) {
			writeTestResults("Verify that User Can Release the Case Form", "User Can Release the Case Form",
					"User Can Release the Case Form Successfully", "pass");
		} else {
			writeTestResults("Verify that User Can Release the Case Form", "User Can Release the Case Form",
					"User Can Release the Case Form Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_ActionBtnA);
		click(btn_CreateServiceQuotationBtnA);

		switchWindow();
		Thread.sleep(3000);
		selectText(txt_SalesUnitA, SalesUnitA);
		Thread.sleep(3000);
		click(btn_ServiceDetailsA);

		Thread.sleep(3000);
		click(btn_Plus1A);
		click(btn_Plus2A);
		click(btn_Plus3A);
		click(btn_Plus4A);
		click(btn_Plus5A);

		selectText(txt_Type1A, InputProductA);
		selectText(txt_Type2A, "Labour");
		selectText(txt_Type3A, "Resource");
		selectText(txt_Type4A, "Expense");
		selectText(txt_Type5A, "Sub Contract");
		selectText(txt_Type6A, "Services");

		click(btn_ProSearch1A);
		Thread.sleep(3000);
		sendKeys(txt_ProtxtA, InputProductA);
		Thread.sleep(3000);
		pressEnter(txt_ProtxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProselA);

		click(btn_ProSearch2A);
		Thread.sleep(3000);
		sendKeys(txt_ProtxtA, LabourProductA);
		Thread.sleep(3000);
		pressEnter(txt_ProtxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProselA);

		click(btn_ProSearch3A);
		sendKeys(txt_ProtxtA, ResourceProductA);
		Thread.sleep(3000);
		pressEnter(txt_ProtxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProselA);

		click(btn_ProSearch4A);
		Thread.sleep(3000);
		sendKeys(txt_ProtxtA, ExpenseProductA);
		Thread.sleep(3000);
		pressEnter(txt_ProtxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProselA);

		click(btn_ProSearch5A);
		Thread.sleep(3000);
		sendKeys(txt_ProtxtA, SubContractProductA);
		Thread.sleep(3000);
		pressEnter(txt_ProtxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProselA);

		click(btn_ProSearch6A);
		Thread.sleep(3000);
		sendKeys(txt_ProtxtA, ServiceProductA);
		Thread.sleep(3000);
		pressEnter(txt_ProtxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProselA);

		Thread.sleep(3000);
		sendKeys(txt_UnitPrice1A, "100");
		sendKeys(txt_UnitPrice2A, "100");
		sendKeys(txt_UnitPrice3A, "100");
		sendKeys(txt_UnitPrice4A, "100");
		sendKeys(txt_UnitPrice5A, "100");
		sendKeys(txt_UnitPrice6A, "100");

		click(btn_ReferenceCodeLabourA);
		Thread.sleep(3000);
		sendKeys(txt_ReferenceCodeLabourtxtA, ReferenceCodeLabourtxtA);
		Thread.sleep(3000);
		pressEnter(txt_ReferenceCodeLabourtxtA);
		Thread.sleep(3000);
		doubleClick(sel_ReferenceCodeLabourselA);

		click(btn_ReferenceCodeResourceA);
		Thread.sleep(3000);
		sendKeys(txt_ReferenceCodeResourcetxtA, ReferenceCodeResourcetxtA);
		Thread.sleep(3000);
		pressEnter(txt_ReferenceCodeResourcetxtA);
		Thread.sleep(3000);
		doubleClick(sel_ReferenceCodeResourceselA);

		selectText(txt_ReferenceCodeExpenseA, ReferenceCodeExpenseA);

		click(btn_CheckoutA);

		Thread.sleep(3000);
		click(btn_DraftA);
		Thread.sleep(5000);
		if (isDisplayed(txt_PageDraft1A)) {
			writeTestResults("Verify that User Can draft the Service Quotation Form",
					"User Can draft the Service Quotation Form",
					"User Can draft the Service Quotation Form Successfully", "pass");
		} else {
			writeTestResults("Verify that User Can draft the Service Quotation Form",
					"User Can draft the Service Quotation Form", "User Can draft the Service Quotation Form Fail",
					"fail");
		}

		Thread.sleep(3000);
		click(btn_ReleaseA);
		Thread.sleep(3000);
		trackCode = getText(txt_trackCodeA);
		Thread.sleep(5000);
		if (isDisplayed(txt_PageRelease1A)) {
			writeTestResults("Verify that User Can Release the Service Quotation Form",
					"User Can Release the Service Quotation Form",
					"User Can Release the Service Quotation Form Successfully", "pass");
		} else {
			writeTestResults("Verify that User Can Release the Service Quotation Form",
					"User Can Release the Service Quotation Form", "User Can Release the Service Quotation Form Fail",
					"fail");
		}
		
		Thread.sleep(3000);
		doubleClick(btn_VersionA);
		Thread.sleep(3000);
		mouseMove(btn_colorSelectedA);
		Thread.sleep(3000);
		click(btn_ConfirmTikA);
		Thread.sleep(3000);
		click(btn_ConfirmbtnA);
		Thread.sleep(3000);
		click(btn_GoToPageLinkA);
		
		switchWindow();
		Thread.sleep(3000);
		click(btn_ServiceDetailsSOA);
		click(btn_CheckoutA);
		
		Thread.sleep(3000);
		click(btn_DraftA);
		Thread.sleep(5000);
		if (isDisplayed(txt_PageDraft1A)) {
			writeTestResults("Verify that User Can draft the Service Order Form",
					"User Can draft the Service Order Form",
					"User Can draft the Service Order Form Successfully", "pass");
		} else {
			writeTestResults("Verify that User Can draft the Service Order Form",
					"User Can draft the Service Order Form", "User Can draft the Service Order Form Fail",
					"fail");
		}

		Thread.sleep(3000);
		click(btn_ReleaseA);
		Thread.sleep(3000);
		trackCode = getText(txt_trackCodeA);
		Thread.sleep(5000);
		if (isDisplayed(txt_PageRelease1A)) {
			writeTestResults("Verify that User Can Release the Service Order Form",
					"User Can Release the Service Order Form",
					"User Can Release the Service Order Form Successfully", "pass");
		} else {
			writeTestResults("Verify that User Can Release the Service Order Form",
					"User Can Release the Service Order Form", "User Can Release the Service Order Form Fail",
					"fail");
		}
	}
	
	// Service_Case_018
	public void VerifyThatAbleToCreateServiceQuotationUsingCaseWhichCreatedFromRoleCenter() throws Exception {
		
		VerifyThatUserAbleToReleaseaCaseViaRolecenter();
		
		Thread.sleep(3000);
		click(btn_ActionBtnA);
		click(btn_CreateServiceQuotationBtnA);

		switchWindow();
		Thread.sleep(3000);
		selectText(txt_SalesUnitA, SalesUnitA);
		Thread.sleep(3000);
		click(btn_ServiceDetailsA);

		Thread.sleep(3000);
		click(btn_Plus1A);
		click(btn_Plus2A);
		click(btn_Plus3A);
		click(btn_Plus4A);
		click(btn_Plus5A);

		selectText(txt_Type1A, "Input Product");
		selectText(txt_Type2A, "Labour");
		selectText(txt_Type3A, "Resource");
		selectText(txt_Type4A, "Expense");
		selectText(txt_Type5A, "Sub Contract");
		selectText(txt_Type6A, "Services");

		click(btn_ProSearch1A);
		Thread.sleep(3000);
		sendKeys(txt_ProtxtA, InputProductA);
		Thread.sleep(3000);
		pressEnter(txt_ProtxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProselA);

		click(btn_ProSearch2A);
		Thread.sleep(3000);
		sendKeys(txt_ProtxtA, LabourProductA);
		Thread.sleep(3000);
		pressEnter(txt_ProtxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProselA);

		click(btn_ProSearch3A);
		sendKeys(txt_ProtxtA, ResourceProductA);
		Thread.sleep(3000);
		pressEnter(txt_ProtxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProselA);

		click(btn_ProSearch4A);
		Thread.sleep(3000);
		sendKeys(txt_ProtxtA, ExpenseProductA);
		Thread.sleep(3000);
		pressEnter(txt_ProtxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProselA);

		click(btn_ProSearch5A);
		Thread.sleep(3000);
		sendKeys(txt_ProtxtA, SubContractProductA);
		Thread.sleep(3000);
		pressEnter(txt_ProtxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProselA);

		click(btn_ProSearch6A);
		Thread.sleep(3000);
		sendKeys(txt_ProtxtA, ServiceProductA);
		Thread.sleep(3000);
		pressEnter(txt_ProtxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProselA);

		Thread.sleep(3000);
		sendKeys(txt_UnitPrice1A, "100");
		sendKeys(txt_UnitPrice2A, "100");
		sendKeys(txt_UnitPrice3A, "100");
		sendKeys(txt_UnitPrice4A, "100");
		sendKeys(txt_UnitPrice5A, "100");
		sendKeys(txt_UnitPrice6A, "100");

		click(btn_ReferenceCodeLabourA);
		Thread.sleep(3000);
		sendKeys(txt_ReferenceCodeLabourtxtA, ReferenceCodeLabourtxtA);
		Thread.sleep(3000);
		pressEnter(txt_ReferenceCodeLabourtxtA);
		Thread.sleep(3000);
		doubleClick(sel_ReferenceCodeLabourselA);

		click(btn_ReferenceCodeResourceA);
		Thread.sleep(3000);
		sendKeys(txt_ReferenceCodeResourcetxtA, ReferenceCodeResourcetxtA);
		Thread.sleep(3000);
		pressEnter(txt_ReferenceCodeResourcetxtA);
		Thread.sleep(3000);
		doubleClick(sel_ReferenceCodeResourceselA);

		selectText(txt_ReferenceCodeExpenseA, ReferenceCodeExpenseA);

		click(btn_CheckoutA);

		Thread.sleep(3000);
		click(btn_DraftA);
		Thread.sleep(5000);
		if (isDisplayed(txt_PageDraft1A)) {
			writeTestResults("Verify that User Can draft the Service Quotation Form",
					"User Can draft the Service Quotation Form",
					"User Can draft the Service Quotation Form Successfully", "pass");
		} else {
			writeTestResults("Verify that User Can draft the Service Quotation Form",
					"User Can draft the Service Quotation Form", "User Can draft the Service Quotation Form Fail",
					"fail");
		}

		Thread.sleep(3000);
		click(btn_ReleaseA);
		Thread.sleep(3000);
		trackCode = getText(txt_trackCodeA);
		Thread.sleep(5000);
		if (isDisplayed(txt_PageRelease1A)) {
			writeTestResults("Verify that  able to create Service Quotation using Case which created from Role Center",
					"create Service Quotation using Case which created from Role Center",
					"create Service Quotation using Case which created from Role Center Successfully", "pass");
		} else {
			writeTestResults("Verify that  able to create Service Quotation using Case which created from Role Center",
					"create Service Quotation using Case which created from Role Center", "create Service Quotation using Case which created from Role Center Fail",
					"fail");
		}
		
		Thread.sleep(3000);
		doubleClick(btn_VersionA);
		Thread.sleep(3000);
		mouseMove(btn_colorSelectedA);
		Thread.sleep(3000);
		click(btn_ConfirmTikA);
		Thread.sleep(3000);
		click(btn_ConfirmbtnA);
		Thread.sleep(3000);
		click(btn_GoToPageLinkA);
		
		switchWindow();
		Thread.sleep(3000);
		click(btn_ServiceDetailsSOA);
		click(btn_CheckoutA);
		
		Thread.sleep(3000);
		click(btn_DraftA);
		Thread.sleep(5000);
		if (isDisplayed(txt_PageDraft1A)) {
			writeTestResults("Verify that User Can draft the Service Order Form",
					"User Can draft the Service Order Form",
					"User Can draft the Service Order Form Successfully", "pass");
		} else {
			writeTestResults("Verify that User Can draft the Service Order Form",
					"User Can draft the Service Order Form", "User Can draft the Service Order Form Fail",
					"fail");
		}

		Thread.sleep(3000);
		click(btn_ReleaseA);
		Thread.sleep(3000);
		trackCode = getText(txt_trackCodeA);
		Thread.sleep(5000);
		if (isDisplayed(txt_PageRelease1A)) {
			writeTestResults("Verify that User Can Release the Service Order Form",
					"User Can Release the Service Order Form",
					"User Can Release the Service Order Form Successfully", "pass");
		} else {
			writeTestResults("Verify that User Can Release the Service Order Form",
					"User Can Release the Service Order Form", "User Can Release the Service Order Form Fail",
					"fail");
		}
	}
	
	// Service_Case_019
	public void VerifyThatStatusOfStatusAndStageHeadersAreChangedWhenCreatingAnServiceQuotation() throws Exception {
		
		Thread.sleep(3000);
		click(btn_RolecenterA);

		Thread.sleep(3000);
		click(btn_searchTagA);
		sendKeys(txt_searchTagtxtA, "s1.1");
		Thread.sleep(3000);
		pressEnter(txt_searchTagtxtA);
		Thread.sleep(3000);
		click(btn_searchTagarrowA);

		switchWindow();

		Thread.sleep(3000);
		LocalTime obj = LocalTime.now();
		sendKeys(txt_TitleTextA, "Case" + obj);
		selectText(txt_OriginTextA, OriginTextFieldA);

		selectText(txt_TypeTextA, TypeTextFieldA);
		selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);
		selectText(txt_PriorityTextA, PriorityTextFieldA);

		Thread.sleep(3000);
		click(btn_DraftA);
		Thread.sleep(5000);
		if (isDisplayed(txt_PageDraft1A)) {
			writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
					"User Can draft the Case Form Successfully", "pass");
		} else {
			writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
					"User Can draft the Case Form Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_ReleaseA);
		Thread.sleep(3000);
		trackCode = getText(txt_trackCodeA);
		String CNo= trackCode;
		System.out.println(CNo);
		
		Thread.sleep(5000);
		if (isDisplayed(txt_PageRelease1A)) {
			writeTestResults("Verify that User able to release a case Via Role center",
					"User able to release a case Via Role center",
					"User able to release a case Via Role center Successfully", "pass");
		} else {
			writeTestResults("Verify that User able to release a case Via Role center",
					"User able to release a case Via Role center", "User able to release a case Via Role center Fail",
					"fail");
		}
		
		Thread.sleep(3000);
		click(btn_ActionBtnA);
		click(btn_CreateServiceQuotationBtnA);

		switchWindow();
		Thread.sleep(3000);
		selectText(txt_SalesUnitA, SalesUnitA);
		Thread.sleep(3000);
		click(btn_ServiceDetailsA);

		Thread.sleep(3000);
		click(btn_Plus1A);
		click(btn_Plus2A);
		click(btn_Plus3A);
		click(btn_Plus4A);
		click(btn_Plus5A);

		selectText(txt_Type1A, "Input Product");
		selectText(txt_Type2A, "Labour");
		selectText(txt_Type3A, "Resource");
		selectText(txt_Type4A, "Expense");
		selectText(txt_Type5A, "Sub Contract");
		selectText(txt_Type6A, "Services");

		click(btn_ProSearch1A);
		Thread.sleep(3000);
		sendKeys(txt_ProtxtA, InputProductA);
		Thread.sleep(3000);
		pressEnter(txt_ProtxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProselA);

		click(btn_ProSearch2A);
		Thread.sleep(3000);
		sendKeys(txt_ProtxtA, LabourProductA);
		Thread.sleep(3000);
		pressEnter(txt_ProtxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProselA);

		click(btn_ProSearch3A);
		sendKeys(txt_ProtxtA, ResourceProductA);
		Thread.sleep(3000);
		pressEnter(txt_ProtxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProselA);

		click(btn_ProSearch4A);
		Thread.sleep(3000);
		sendKeys(txt_ProtxtA, ExpenseProductA);
		Thread.sleep(3000);
		pressEnter(txt_ProtxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProselA);

		click(btn_ProSearch5A);
		Thread.sleep(3000);
		sendKeys(txt_ProtxtA, SubContractProductA);
		Thread.sleep(3000);
		pressEnter(txt_ProtxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProselA);

		click(btn_ProSearch6A);
		Thread.sleep(3000);
		sendKeys(txt_ProtxtA, ServiceProductA);
		Thread.sleep(3000);
		pressEnter(txt_ProtxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProselA);

		Thread.sleep(3000);
		sendKeys(txt_UnitPrice1A, "100");
		sendKeys(txt_UnitPrice2A, "100");
		sendKeys(txt_UnitPrice3A, "100");
		sendKeys(txt_UnitPrice4A, "100");
		sendKeys(txt_UnitPrice5A, "100");
		sendKeys(txt_UnitPrice6A, "100");

		click(btn_ReferenceCodeLabourA);
		Thread.sleep(3000);
		sendKeys(txt_ReferenceCodeLabourtxtA, ReferenceCodeLabourtxtA);
		Thread.sleep(3000);
		pressEnter(txt_ReferenceCodeLabourtxtA);
		Thread.sleep(3000);
		doubleClick(sel_ReferenceCodeLabourselA);

		click(btn_ReferenceCodeResourceA);
		Thread.sleep(3000);
		sendKeys(txt_ReferenceCodeResourcetxtA, ReferenceCodeResourcetxtA);
		Thread.sleep(3000);
		pressEnter(txt_ReferenceCodeResourcetxtA);
		Thread.sleep(3000);
		doubleClick(sel_ReferenceCodeResourceselA);

		selectText(txt_ReferenceCodeExpenseA, ReferenceCodeExpenseA);

		click(btn_CheckoutA);

		Thread.sleep(3000);
		click(btn_DraftA);
		Thread.sleep(5000);
		if (isDisplayed(txt_PageDraft1A)) {
			writeTestResults("Verify that User Can draft the Service Quotation Form",
					"User Can draft the Service Quotation Form",
					"User Can draft the Service Quotation Form Successfully", "pass");
		} else {
			writeTestResults("Verify that User Can draft the Service Quotation Form",
					"User Can draft the Service Quotation Form", "User Can draft the Service Quotation Form Fail",
					"fail");
		}

		Thread.sleep(3000);
		click(btn_ReleaseA);
		Thread.sleep(3000);
		trackCode = getText(txt_trackCodeA);
		Thread.sleep(5000);
		if (isDisplayed(txt_PageRelease1A)) {
			writeTestResults("Verify that  able to create Service Quotation using Case which created from Role Center",
					"create Service Quotation using Case which created from Role Center",
					"create Service Quotation using Case which created from Role Center Successfully", "pass");
		} else {
			writeTestResults("Verify that  able to create Service Quotation using Case which created from Role Center",
					"create Service Quotation using Case which created from Role Center", "create Service Quotation using Case which created from Role Center Fail",
					"fail");
		}
		
		Thread.sleep(3000);
		doubleClick(btn_VersionA);
		Thread.sleep(3000);
		mouseMove(btn_colorSelectedA);
		Thread.sleep(3000);
		click(btn_ConfirmTikA);
		Thread.sleep(3000);
		click(btn_ConfirmbtnA);
		Thread.sleep(3000);
		click(btn_GoToPageLinkA);
		
		switchWindow();
		Thread.sleep(3000);
		click(btn_ServiceDetailsSOA);
		click(btn_CheckoutA);
		
		Thread.sleep(3000);
		click(btn_DraftA);
		Thread.sleep(5000);
		if (isDisplayed(txt_PageDraft1A)) {
			writeTestResults("Verify that User Can draft the Service Order Form",
					"User Can draft the Service Order Form",
					"User Can draft the Service Order Form Successfully", "pass");
		} else {
			writeTestResults("Verify that User Can draft the Service Order Form",
					"User Can draft the Service Order Form", "User Can draft the Service Order Form Fail",
					"fail");
		}

		Thread.sleep(3000);
		click(btn_ReleaseA);
		Thread.sleep(3000);
		trackCode = getText(txt_trackCodeA);
		Thread.sleep(5000);
		if (isDisplayed(txt_PageRelease1A)) {
			writeTestResults("Verify that User Can Release the Service Order Form",
					"User Can Release the Service Order Form",
					"User Can Release the Service Order Form Successfully", "pass");
		} else {
			writeTestResults("Verify that User Can Release the Service Order Form",
					"User Can Release the Service Order Form", "User Can Release the Service Order Form Fail",
					"fail");
		}
		
		Thread.sleep(6000);
		clickNavigation();
		Thread.sleep(3000);
		click(btn_FormCaseA);
		
		WaitElement(txt_CopyFromSerchtxtA);
		sendKeys(txt_CopyFromSerchtxtA, CNo);
		pressEnter(txt_CopyFromSerchtxtA);
		WaitElement(sel_CaseSerchSelNewA.replace("CaseTitle", "Case" + obj));
		doubleClick(sel_CaseSerchSelNewA.replace("CaseTitle", "Case" + obj));
		
		Thread.sleep(3000);
		String StageFieldA = new Select(driver.findElement(By.xpath(txt_StageFieldA))).getFirstSelectedOption().getText();
		Thread.sleep(3000);
		click(btn_StatusFieldArrowA);
		String StatusFieldA = getText(txt_StatusFieldTxtA);

		Thread.sleep(5000);
		if (StageFieldA.contentEquals("Open") && StatusFieldA.contentEquals("Estimated")) {
			writeTestResults("Verify that status of 'Status' and 'Stage' headers are changed to \"Open\" and \"Estimated\"when creating an service Quotation",
					"status of 'Status' and 'Stage' headers are changed to \"Open\" and \"Estimated\"when creating an service Quotation",
					"status of 'Status' and 'Stage' headers are changed to \"Open\" and \"Estimated\"when creating an service Quotation Successfully", "pass");
		} else {
			writeTestResults("Verify that status of 'Status' and 'Stage' headers are changed to \"Open\" and \"Estimated\"when creating an service Quotation",
					"status of 'Status' and 'Stage' headers are changed to \"Open\" and \"Estimated\"when creating an service Quotation",
					"status of 'Status' and 'Stage' headers are changed to \"Open\" and \"Estimated\"when creating an service Quotation Fail", "fail");
		}
	}
	

    // Service_Case_020
	public void VerifyThatStatusColumnInTheBypageIsChagedToEstimatedAfterReleasingTheServiceQuotation() throws Exception {
				
				Thread.sleep(3000);
				click(btn_RolecenterA);

				Thread.sleep(3000);
				click(btn_searchTagA);
				sendKeys(txt_searchTagtxtA, "s1.1");
				Thread.sleep(3000);
				pressEnter(txt_searchTagtxtA);
				Thread.sleep(3000);
				click(btn_searchTagarrowA);

				switchWindow();

				Thread.sleep(3000);
				LocalTime obj = LocalTime.now();
				sendKeys(txt_TitleTextA, "Case" + obj);
				selectText(txt_OriginTextA, OriginTextFieldA);

				selectText(txt_TypeTextA, TypeTextFieldA);
				selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);
				selectText(txt_PriorityTextA, PriorityTextFieldA);

				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
							"User Can draft the Case Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
							"User Can draft the Case Form Fail", "fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String CNo= trackCode;
				System.out.println(CNo);
				
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that User able to release a case Via Role center",
							"User able to release a case Via Role center",
							"User able to release a case Via Role center Successfully", "pass");
				} else {
					writeTestResults("Verify that User able to release a case Via Role center",
							"User able to release a case Via Role center", "User able to release a case Via Role center Fail",
							"fail");
				}
				
				Thread.sleep(3000);
				click(btn_ActionBtnA);
				click(btn_CreateServiceQuotationBtnA);

				switchWindow();
				Thread.sleep(3000);
				selectText(txt_SalesUnitA, SalesUnitA);
				Thread.sleep(3000);
				click(btn_ServiceDetailsA);

				Thread.sleep(3000);
				click(btn_Plus1A);
				click(btn_Plus2A);
				click(btn_Plus3A);
				click(btn_Plus4A);
				click(btn_Plus5A);

				selectText(txt_Type1A, "Input Product");
				selectText(txt_Type2A, "Labour");
				selectText(txt_Type3A, "Resource");
				selectText(txt_Type4A, "Expense");
				selectText(txt_Type5A, "Sub Contract");
				selectText(txt_Type6A, "Services");

				click(btn_ProSearch1A);
				Thread.sleep(3000);
				sendKeys(txt_ProtxtA, InputProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				click(btn_ProSearch2A);
				Thread.sleep(3000);
				sendKeys(txt_ProtxtA, LabourProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				click(btn_ProSearch3A);
				sendKeys(txt_ProtxtA, ResourceProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				click(btn_ProSearch4A);
				Thread.sleep(3000);
				sendKeys(txt_ProtxtA, ExpenseProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				click(btn_ProSearch5A);
				Thread.sleep(3000);
				sendKeys(txt_ProtxtA, SubContractProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				click(btn_ProSearch6A);
				Thread.sleep(3000);
				sendKeys(txt_ProtxtA, ServiceProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				Thread.sleep(3000);
				sendKeys(txt_UnitPrice1A, "100");
				sendKeys(txt_UnitPrice2A, "100");
				sendKeys(txt_UnitPrice3A, "100");
				sendKeys(txt_UnitPrice4A, "100");
				sendKeys(txt_UnitPrice5A, "100");
				sendKeys(txt_UnitPrice6A, "100");

				click(btn_ReferenceCodeLabourA);
				Thread.sleep(3000);
				sendKeys(txt_ReferenceCodeLabourtxtA, ReferenceCodeLabourtxtA);
				Thread.sleep(3000);
				pressEnter(txt_ReferenceCodeLabourtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ReferenceCodeLabourselA);

				click(btn_ReferenceCodeResourceA);
				Thread.sleep(3000);
				sendKeys(txt_ReferenceCodeResourcetxtA, ReferenceCodeResourcetxtA);
				Thread.sleep(3000);
				pressEnter(txt_ReferenceCodeResourcetxtA);
				Thread.sleep(3000);
				doubleClick(sel_ReferenceCodeResourceselA);

				selectText(txt_ReferenceCodeExpenseA, ReferenceCodeExpenseA);

				click(btn_CheckoutA);

				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Service Quotation Form",
							"User Can draft the Service Quotation Form",
							"User Can draft the Service Quotation Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Service Quotation Form",
							"User Can draft the Service Quotation Form", "User Can draft the Service Quotation Form Fail",
							"fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that  able to create Service Quotation using Case which created from Role Center",
							"create Service Quotation using Case which created from Role Center",
							"create Service Quotation using Case which created from Role Center Successfully", "pass");
				} else {
					writeTestResults("Verify that  able to create Service Quotation using Case which created from Role Center",
							"create Service Quotation using Case which created from Role Center", "create Service Quotation using Case which created from Role Center Fail",
							"fail");
				}
				
				Thread.sleep(3000);
				doubleClick(btn_VersionA);
				Thread.sleep(3000);
				mouseMove(btn_colorSelectedA);
				Thread.sleep(3000);
				click(btn_ConfirmTikA);
				Thread.sleep(3000);
				click(btn_ConfirmbtnA);
				Thread.sleep(3000);
				click(btn_GoToPageLinkA);
				
				switchWindow();
				Thread.sleep(3000);
				click(btn_ServiceDetailsSOA);
				click(btn_CheckoutA);
				
				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Service Order Form",
							"User Can draft the Service Order Form",
							"User Can draft the Service Order Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Service Order Form",
							"User Can draft the Service Order Form", "User Can draft the Service Order Form Fail",
							"fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that User Can Release the Service Order Form",
							"User Can Release the Service Order Form",
							"User Can Release the Service Order Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can Release the Service Order Form",
							"User Can Release the Service Order Form", "User Can Release the Service Order Form Fail",
							"fail");
				}
				
				Thread.sleep(6000);
				clickNavigation();
				Thread.sleep(3000);
				click(btn_FormCaseA);
				
				Thread.sleep(3000);
				sendKeys(txt_CopyFromSerchtxtA, CNo);
				Thread.sleep(3000);
				pressEnter(txt_CopyFromSerchtxtA);
				
				Thread.sleep(5000);
				if (getText(txt_CaseSerchSelStatusA).contentEquals("Estimated")) {
					writeTestResults("Verify that Status column in the bypage is chaged to \"Estimated\" after releasing the service Quotation",
							"Status column in the bypage is chaged to \"Estimated\" after releasing the service Quotation",
							"Status column in the bypage is chaged to \"Estimated\" after releasing the service Quotation Successfully", "pass");
				} else {
					writeTestResults("Verify that Status column in the bypage is chaged to \"Estimated\" after releasing the service Quotation",
							"Status column in the bypage is chaged to \"Estimated\" after releasing the service Quotation",
							"Status column in the bypage is chaged to \"Estimated\" after releasing the service Quotation Fail", "fail");
				}
			}
	
    // Service_Case_021
	public void VerifyThatNotificationIsDispalyedAfterReleasingTheServiceQuotationAndServiceOrder() throws Exception {
		
		Thread.sleep(3000);
		click(btn_RolecenterA);

		Thread.sleep(3000);
		click(btn_searchTagA);
		sendKeys(txt_searchTagtxtA, "s1.1");
		Thread.sleep(3000);
		pressEnter(txt_searchTagtxtA);
		Thread.sleep(3000);
		click(btn_searchTagarrowA);

		switchWindow();

		Thread.sleep(3000);
		LocalTime obj = LocalTime.now();
		sendKeys(txt_TitleTextA, "Case" + obj);
		selectText(txt_OriginTextA, OriginTextFieldA);

		selectText(txt_TypeTextA, TypeTextFieldA);
		selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);
		selectText(txt_PriorityTextA, PriorityTextFieldA);

		Thread.sleep(3000);
		click(btn_DraftA);
		Thread.sleep(5000);
		if (isDisplayed(txt_PageDraft1A)) {
			writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
					"User Can draft the Case Form Successfully", "pass");
		} else {
			writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
					"User Can draft the Case Form Fail", "fail");
		}

		Thread.sleep(3000);
		click(btn_ReleaseA);
		Thread.sleep(3000);
		trackCode = getText(txt_trackCodeA);
		String CNo= trackCode;
		System.out.println(CNo);
		
		Thread.sleep(5000);
		if (isDisplayed(txt_PageRelease1A)) {
			writeTestResults("Verify that User able to release a case Via Role center",
					"User able to release a case Via Role center",
					"User able to release a case Via Role center Successfully", "pass");
		} else {
			writeTestResults("Verify that User able to release a case Via Role center",
					"User able to release a case Via Role center", "User able to release a case Via Role center Fail",
					"fail");
		}
		
		Thread.sleep(3000);
		click(btn_ActionBtnA);
		click(btn_CreateServiceQuotationBtnA);

		switchWindow();
		Thread.sleep(3000);
		selectText(txt_SalesUnitA, SalesUnitA);
		Thread.sleep(3000);
		click(btn_ServiceDetailsA);

		Thread.sleep(3000);
		click(btn_Plus1A);
		click(btn_Plus2A);
		click(btn_Plus3A);
		click(btn_Plus4A);
		click(btn_Plus5A);

		selectText(txt_Type1A, "Input Product");
		selectText(txt_Type2A, "Labour");
		selectText(txt_Type3A, "Resource");
		selectText(txt_Type4A, "Expense");
		selectText(txt_Type5A, "Sub Contract");
		selectText(txt_Type6A, "Services");

		click(btn_ProSearch1A);
		Thread.sleep(3000);
		sendKeys(txt_ProtxtA, InputProductA);
		Thread.sleep(3000);
		pressEnter(txt_ProtxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProselA);

		click(btn_ProSearch2A);
		Thread.sleep(3000);
		sendKeys(txt_ProtxtA, LabourProductA);
		Thread.sleep(3000);
		pressEnter(txt_ProtxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProselA);

		click(btn_ProSearch3A);
		sendKeys(txt_ProtxtA, ResourceProductA);
		Thread.sleep(3000);
		pressEnter(txt_ProtxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProselA);

		click(btn_ProSearch4A);
		Thread.sleep(3000);
		sendKeys(txt_ProtxtA, ExpenseProductA);
		Thread.sleep(3000);
		pressEnter(txt_ProtxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProselA);

		click(btn_ProSearch5A);
		Thread.sleep(3000);
		sendKeys(txt_ProtxtA, SubContractProductA);
		Thread.sleep(3000);
		pressEnter(txt_ProtxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProselA);

		click(btn_ProSearch6A);
		Thread.sleep(3000);
		sendKeys(txt_ProtxtA, ServiceProductA);
		Thread.sleep(3000);
		pressEnter(txt_ProtxtA);
		Thread.sleep(3000);
		doubleClick(sel_ProselA);

		Thread.sleep(3000);
		sendKeys(txt_UnitPrice1A, "100");
		sendKeys(txt_UnitPrice2A, "100");
		sendKeys(txt_UnitPrice3A, "100");
		sendKeys(txt_UnitPrice4A, "100");
		sendKeys(txt_UnitPrice5A, "100");
		sendKeys(txt_UnitPrice6A, "100");

		click(btn_ReferenceCodeLabourA);
		Thread.sleep(3000);
		sendKeys(txt_ReferenceCodeLabourtxtA, ReferenceCodeLabourtxtA);
		Thread.sleep(3000);
		pressEnter(txt_ReferenceCodeLabourtxtA);
		Thread.sleep(3000);
		doubleClick(sel_ReferenceCodeLabourselA);

		click(btn_ReferenceCodeResourceA);
		Thread.sleep(3000);
		sendKeys(txt_ReferenceCodeResourcetxtA, ReferenceCodeResourcetxtA);
		Thread.sleep(3000);
		pressEnter(txt_ReferenceCodeResourcetxtA);
		Thread.sleep(3000);
		doubleClick(sel_ReferenceCodeResourceselA);

		selectText(txt_ReferenceCodeExpenseA, ReferenceCodeExpenseA);

		click(btn_CheckoutA);

		Thread.sleep(3000);
		click(btn_DraftA);
		Thread.sleep(5000);
		if (isDisplayed(txt_PageDraft1A)) {
			writeTestResults("Verify that User Can draft the Service Quotation Form",
					"User Can draft the Service Quotation Form",
					"User Can draft the Service Quotation Form Successfully", "pass");
		} else {
			writeTestResults("Verify that User Can draft the Service Quotation Form",
					"User Can draft the Service Quotation Form", "User Can draft the Service Quotation Form Fail",
					"fail");
		}

		Thread.sleep(3000);
		click(btn_ReleaseA);
		Thread.sleep(3000);
		trackCode = getText(txt_trackCodeA);
		Thread.sleep(5000);
		if (isDisplayed(txt_PageRelease1A)) {
			writeTestResults("Verify that  able to create Service Quotation using Case which created from Role Center",
					"create Service Quotation using Case which created from Role Center",
					"create Service Quotation using Case which created from Role Center Successfully", "pass");
		} else {
			writeTestResults("Verify that  able to create Service Quotation using Case which created from Role Center",
					"create Service Quotation using Case which created from Role Center", "create Service Quotation using Case which created from Role Center Fail",
					"fail");
		}
		
		Thread.sleep(3000);
		doubleClick(btn_VersionA);
		Thread.sleep(3000);
		mouseMove(btn_colorSelectedA);
		Thread.sleep(3000);
		click(btn_ConfirmTikA);
		Thread.sleep(3000);
		click(btn_ConfirmbtnA);
		Thread.sleep(3000);
		click(btn_GoToPageLinkA);
		
		switchWindow();
		Thread.sleep(3000);
		click(btn_ServiceDetailsSOA);
		click(btn_CheckoutA);
		
		Thread.sleep(3000);
		click(btn_DraftA);
		Thread.sleep(5000);
		if (isDisplayed(txt_PageDraft1A)) {
			writeTestResults("Verify that User Can draft the Service Order Form",
					"User Can draft the Service Order Form",
					"User Can draft the Service Order Form Successfully", "pass");
		} else {
			writeTestResults("Verify that User Can draft the Service Order Form",
					"User Can draft the Service Order Form", "User Can draft the Service Order Form Fail",
					"fail");
		}

		Thread.sleep(3000);
		click(btn_ReleaseA);
		Thread.sleep(3000);
		trackCode = getText(txt_trackCodeA);
		Thread.sleep(5000);
		if (isDisplayed(txt_PageRelease1A)) {
			writeTestResults("Verify that User Can Release the Service Order Form",
					"User Can Release the Service Order Form",
					"User Can Release the Service Order Form Successfully", "pass");
		} else {
			writeTestResults("Verify that User Can Release the Service Order Form",
					"User Can Release the Service Order Form", "User Can Release the Service Order Form Fail",
					"fail");
		}
		
		Thread.sleep(6000);
		clickNavigation();
		Thread.sleep(3000);
		click(btn_FormCaseA);
		
		WaitElement(txt_CopyFromSerchtxtA);
		sendKeys(txt_CopyFromSerchtxtA, CNo);
		pressEnter(txt_CopyFromSerchtxtA);
		WaitElement(sel_CaseSerchSelNewA.replace("CaseTitle", "Case" + obj));
		doubleClick(sel_CaseSerchSelNewA.replace("CaseTitle", "Case" + obj));
		
		Thread.sleep(5000);
		if (isDisplayed(txt_ConfirmNotificationTxt1A)&&isDisplayed(txt_ConfirmNotificationTxt1A)) {
			writeTestResults("Verify that \"Notification - The Order has been confirmed, please proceed the job\" is dispalyed after releasing the service quotation and service order",
					"\"Notification - The Order has been confirmed, please proceed the job\" is dispalyed after releasing the service quotation and service order",
					"\"Notification - The Order has been confirmed, please proceed the job\" is dispalyed after releasing the service quotation and service order Successfully", "pass");
		} else {
			writeTestResults("Verify that \"Notification - The Order has been confirmed, please proceed the job\" is dispalyed after releasing the service quotation and service order",
					"\"Notification - The Order has been confirmed, please proceed the job\" is dispalyed after releasing the service quotation and service order", 
					"\"Notification - The Order has been confirmed, please proceed the job\" is dispalyed after releasing the service quotation and service order Fail",
					"fail");
		}
	}
	
	   // Service_Case_022
		public void VerifyThatCreateServiceQuotationActionIsRemovedFromTheActionListAfterCompletingTheServiceQuotationJourney() throws Exception {
			
			navigationCaseForm();
			Thread.sleep(1000);
			LocalTime obj = LocalTime.now();
			sendKeys(txt_TitleTextA, "Case" + obj);
			selectText(txt_OriginTextA, OriginTextFieldA);

			click(btn_CustomerAccountButtonA);
			sendKeys(txt_CustomerAccountTextA, CustomerAccountTextA);
			Thread.sleep(3000);
			pressEnter(txt_CustomerAccountTextA);
			Thread.sleep(3000);
			doubleClick(sel_CustomerAccountSelA);

			Thread.sleep(3000);
			click(btn_newWindowProdA);
			Thread.sleep(3000);
			click(sel_newWindowProdSelA);
			Thread.sleep(3000);
			JavascriptExecutor j1 = (JavascriptExecutor) driver;
			j1.executeScript("$(\"#divServiceSerial\").parent().find('.dialogbox-buttonarea > .button').click()");

			selectText(txt_TypeTextA, TypeTextFieldA);
			selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);
			selectText(txt_PriorityTextA, PriorityTextFieldA);

			Thread.sleep(3000);
			click(btn_DraftA);
			Thread.sleep(5000);
			if (isDisplayed(txt_PageDraft1A)) {
				writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
						"User Can draft the Case Form Successfully", "pass");
			} else {
				writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
						"User Can draft the Case Form Fail", "fail");
			}

			Thread.sleep(3000);
			click(btn_ReleaseA);
			Thread.sleep(3000);
			trackCode = getText(txt_trackCodeA);
			String CNo=trackCode;
			Thread.sleep(5000);
			if (isDisplayed(txt_PageRelease1A)) {
				writeTestResults("Verify that User Can Release the Case Form", "User Can Release the Case Form",
						"User Can Release the Case Form Successfully", "pass");
			} else {
				writeTestResults("Verify that User Can Release the Case Form", "User Can Release the Case Form",
						"User Can Release the Case Form Fail", "fail");
			}

			Thread.sleep(3000);
			click(btn_ActionBtnA);
			click(btn_CreateServiceQuotationBtnA);

			switchWindow();
			Thread.sleep(3000);
			selectText(txt_SalesUnitA, SalesUnitA);
			Thread.sleep(3000);
			click(btn_ServiceDetailsA);

			Thread.sleep(3000);
			click(btn_Plus1A);
			click(btn_Plus2A);
			click(btn_Plus3A);
			click(btn_Plus4A);
			click(btn_Plus5A);

			selectText(txt_Type1A, "Input Product");
			selectText(txt_Type2A, "Labour");
			selectText(txt_Type3A, "Resource");
			selectText(txt_Type4A, "Expense");
			selectText(txt_Type5A, "Sub Contract");
			selectText(txt_Type6A, "Services");

			click(btn_ProSearch1A);
			Thread.sleep(3000);
			sendKeys(txt_ProtxtA, InputProductA);
			Thread.sleep(3000);
			pressEnter(txt_ProtxtA);
			Thread.sleep(3000);
			doubleClick(sel_ProselA);

			click(btn_ProSearch2A);
			Thread.sleep(3000);
			sendKeys(txt_ProtxtA, LabourProductA);
			Thread.sleep(3000);
			pressEnter(txt_ProtxtA);
			Thread.sleep(3000);
			doubleClick(sel_ProselA);

			click(btn_ProSearch3A);
			sendKeys(txt_ProtxtA, ResourceProductA);
			Thread.sleep(3000);
			pressEnter(txt_ProtxtA);
			Thread.sleep(3000);
			doubleClick(sel_ProselA);

			click(btn_ProSearch4A);
			Thread.sleep(3000);
			sendKeys(txt_ProtxtA, ExpenseProductA);
			Thread.sleep(3000);
			pressEnter(txt_ProtxtA);
			Thread.sleep(3000);
			doubleClick(sel_ProselA);

			click(btn_ProSearch5A);
			Thread.sleep(3000);
			sendKeys(txt_ProtxtA, SubContractProductA);
			Thread.sleep(3000);
			pressEnter(txt_ProtxtA);
			Thread.sleep(3000);
			doubleClick(sel_ProselA);

			click(btn_ProSearch6A);
			Thread.sleep(3000);
			sendKeys(txt_ProtxtA, ServiceProductA);
			Thread.sleep(3000);
			pressEnter(txt_ProtxtA);
			Thread.sleep(3000);
			doubleClick(sel_ProselA);

			Thread.sleep(3000);
			sendKeys(txt_UnitPrice1A, "100");
			sendKeys(txt_UnitPrice2A, "100");
			sendKeys(txt_UnitPrice3A, "100");
			sendKeys(txt_UnitPrice4A, "100");
			sendKeys(txt_UnitPrice5A, "100");
			sendKeys(txt_UnitPrice6A, "100");

			click(btn_ReferenceCodeLabourA);
			Thread.sleep(3000);
			sendKeys(txt_ReferenceCodeLabourtxtA, ReferenceCodeLabourtxtA);
			Thread.sleep(3000);
			pressEnter(txt_ReferenceCodeLabourtxtA);
			Thread.sleep(3000);
			doubleClick(sel_ReferenceCodeLabourselA);

			click(btn_ReferenceCodeResourceA);
			Thread.sleep(3000);
			sendKeys(txt_ReferenceCodeResourcetxtA, ReferenceCodeResourcetxtA);
			Thread.sleep(3000);
			pressEnter(txt_ReferenceCodeResourcetxtA);
			Thread.sleep(3000);
			doubleClick(sel_ReferenceCodeResourceselA);

			selectText(txt_ReferenceCodeExpenseA, ReferenceCodeExpenseA);

			click(btn_CheckoutA);

			Thread.sleep(3000);
			click(btn_DraftA);
			Thread.sleep(5000);
			if (isDisplayed(txt_PageDraft1A)) {
				writeTestResults("Verify that User Can draft the Service Quotation Form",
						"User Can draft the Service Quotation Form",
						"User Can draft the Service Quotation Form Successfully", "pass");
			} else {
				writeTestResults("Verify that User Can draft the Service Quotation Form",
						"User Can draft the Service Quotation Form", "User Can draft the Service Quotation Form Fail",
						"fail");
			}

			Thread.sleep(3000);
			click(btn_ReleaseA);
			Thread.sleep(3000);
			trackCode = getText(txt_trackCodeA);
			Thread.sleep(5000);
			if (isDisplayed(txt_PageRelease1A)) {
				writeTestResults("Verify that User Can Release the Service Quotation Form",
						"User Can Release the Service Quotation Form",
						"User Can Release the Service Quotation Form Successfully", "pass");
			} else {
				writeTestResults("Verify that User Can Release the Service Quotation Form",
						"User Can Release the Service Quotation Form", "User Can Release the Service Quotation Form Fail",
						"fail");
			}
			
			Thread.sleep(6000);
			clickNavigation();
			Thread.sleep(3000);
			click(btn_FormCaseA);
			
			Thread.sleep(3000);
			sendKeys(txt_CopyFromSerchtxtA, CNo);
			Thread.sleep(3000);
			pressEnter(txt_CopyFromSerchtxtA);
			Thread.sleep(3000);
			doubleClick(sel_CaseSerchSelA);
			
			Thread.sleep(3000);
			click(btn_ActionBtnA);
			Thread.sleep(5000);
			if (!isDisplayed(btn_CreateServiceQuotationBtnA)) {
				writeTestResults("Verify that \"Create service Quotation \" Action is removed from the action list after completing the service quotation journey",
						"\"Create service Quotation\" action is not available",
						" \"Create Service Quotation\" action is not available Successfully", "pass");
			} else {
				writeTestResults("Verify that \"Create service Quotation \" Action is removed from the action list after completing the service quotation journey",
						"\"Create service Quotation\" action is not available", 
						" \"Create Service Quotation\" action is not available Fail",
						"fail");
			}
		}
		
		   // Service_Case_023
		   public void VerifyThatAbleToCreateServiceQuotationUsingCaseWhichCreatedByDuplicatingAnExcitingCase() throws Exception {
			   
			   VerificationOfCaseCanByDuplicatingAnExistingCase();
			   
				Thread.sleep(3000);
				click(btn_ActionBtnA);
				click(btn_CreateServiceQuotationBtnA);

				switchWindow();
				Thread.sleep(3000);
				selectText(txt_SalesUnitA, SalesUnitA);
				Thread.sleep(3000);
				click(btn_ServiceDetailsA);

				Thread.sleep(3000);
				click(btn_Plus1A);
				click(btn_Plus2A);
				click(btn_Plus3A);
				click(btn_Plus4A);
				click(btn_Plus5A);

				selectText(txt_Type1A, "Input Product");
				selectText(txt_Type2A, "Labour");
				selectText(txt_Type3A, "Resource");
				selectText(txt_Type4A, "Expense");
				selectText(txt_Type5A, "Sub Contract");
				selectText(txt_Type6A, "Services");

				click(btn_ProSearch1A);
				Thread.sleep(3000);
				sendKeys(txt_ProtxtA, InputProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				click(btn_ProSearch2A);
				Thread.sleep(3000);
				sendKeys(txt_ProtxtA, LabourProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				click(btn_ProSearch3A);
				sendKeys(txt_ProtxtA, ResourceProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				click(btn_ProSearch4A);
				Thread.sleep(3000);
				sendKeys(txt_ProtxtA, ExpenseProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				click(btn_ProSearch5A);
				Thread.sleep(3000);
				sendKeys(txt_ProtxtA, SubContractProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				click(btn_ProSearch6A);
				Thread.sleep(3000);
				sendKeys(txt_ProtxtA, ServiceProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				Thread.sleep(3000);
				sendKeys(txt_UnitPrice1A, "100");
				sendKeys(txt_UnitPrice2A, "100");
				sendKeys(txt_UnitPrice3A, "100");
				sendKeys(txt_UnitPrice4A, "100");
				sendKeys(txt_UnitPrice5A, "100");
				sendKeys(txt_UnitPrice6A, "100");

				click(btn_ReferenceCodeLabourA);
				Thread.sleep(3000);
				sendKeys(txt_ReferenceCodeLabourtxtA, ReferenceCodeLabourtxtA);
				Thread.sleep(3000);
				pressEnter(txt_ReferenceCodeLabourtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ReferenceCodeLabourselA);

				click(btn_ReferenceCodeResourceA);
				Thread.sleep(3000);
				sendKeys(txt_ReferenceCodeResourcetxtA, ReferenceCodeResourcetxtA);
				Thread.sleep(3000);
				pressEnter(txt_ReferenceCodeResourcetxtA);
				Thread.sleep(3000);
				doubleClick(sel_ReferenceCodeResourceselA);

				selectText(txt_ReferenceCodeExpenseA, ReferenceCodeExpenseA);

				click(btn_CheckoutA);

				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Service Quotation Form",
							"User Can draft the Service Quotation Form",
							"User Can draft the Service Quotation Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Service Quotation Form",
							"User Can draft the Service Quotation Form", "User Can draft the Service Quotation Form Fail",
							"fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that User Can Release the Service Quotation using Case which created by duplicating an exciting Case",
							"User Can Release the Service Quotation using Case which created by duplicating an exciting Case",
							"User Can Release the Service Quotation using Case which created by duplicating an exciting Case Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can Release the Service Quotation using Case which created by duplicating an exciting Case",
							"User Can Release the Service Quotation using Case which created by duplicating an exciting Case", "User Can Release the Service Quotation using Case which created by duplicating an exciting Case Fail",
							"fail");
				}
				
				Thread.sleep(3000);
				doubleClick(btn_VersionA);
				Thread.sleep(3000);
				mouseMove(btn_colorSelectedA);
				Thread.sleep(3000);
				click(btn_ConfirmTikA);
				Thread.sleep(3000);
				click(btn_ConfirmbtnA);
				Thread.sleep(3000);
				click(btn_GoToPageLinkA);
				
				switchWindow();
				Thread.sleep(3000);
				click(btn_ServiceDetailsSOA);
				click(btn_CheckoutA);
				
				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Service Order Form",
							"User Can draft the Service Order Form",
							"User Can draft the Service Order Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Service Order Form",
							"User Can draft the Service Order Form", "User Can draft the Service Order Form Fail",
							"fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that User Can Release the Service Order Form",
							"User Can Release the Service Order Form",
							"User Can Release the Service Order Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can Release the Service Order Form",
							"User Can Release the Service Order Form", "User Can Release the Service Order Form Fail",
							"fail");
				}
		   }
		   
		   
		   
		   // Service_Case_024
		   public void VerifyThatAbleToCreateTheServiceJobOrderByConvertTheCaseWhichCreatedFromMenu() throws Exception {
			   
		
			   navigationCaseForm();
				Thread.sleep(1000);
				LocalTime obj = LocalTime.now();
				sendKeys(txt_TitleTextA, "Case" + obj);
				selectText(txt_OriginTextA, OriginTextFieldA);

				click(btn_CustomerAccountButtonA);
				sendKeys(txt_CustomerAccountTextA, CustomerAccountTextA);
				Thread.sleep(3000);
				pressEnter(txt_CustomerAccountTextA);
				Thread.sleep(3000);
				doubleClick(sel_CustomerAccountSelA);

				Thread.sleep(3000);
				click(btn_newWindowProdA);
				Thread.sleep(3000);
				click(sel_newWindowProdSelA);
				Thread.sleep(3000);
				JavascriptExecutor j1 = (JavascriptExecutor) driver;
				j1.executeScript("$(\"#divServiceSerial\").parent().find('.dialogbox-buttonarea > .button').click()");

				selectText(txt_TypeTextA, TypeTextFieldA);
				selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);
				selectText(txt_PriorityTextA, PriorityTextFieldA);

				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
							"User Can draft the Case Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
							"User Can draft the Case Form Fail", "fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String CNo = trackCode;
				
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that User Can Release the Case Form", "User Can Release the Case Form",
							"User Can Release the Case Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can Release the Case Form", "User Can Release the Case Form",
							"User Can Release the Case Form Fail", "fail");
				}

				Thread.sleep(3000);
				click(btn_ActionBtnA);
				click(btn_CreateServiceQuotationBtnA);

				switchWindow();
				Thread.sleep(3000);
				selectText(txt_SalesUnitA, SalesUnitA);
				Thread.sleep(3000);
				click(btn_ServiceDetailsA);

				Thread.sleep(3000);
				click(btn_Plus1A);
				click(btn_Plus2A);
				click(btn_Plus3A);
				click(btn_Plus4A);
				click(btn_Plus5A);

				selectText(txt_Type1A, "Input Product");
				selectText(txt_Type2A, "Labour");
				selectText(txt_Type3A, "Resource");
				selectText(txt_Type4A, "Expense");
				selectText(txt_Type5A, "Sub Contract");
				selectText(txt_Type6A, "Services");

				click(btn_ProSearch1A);
				Thread.sleep(3000);
				sendKeys(txt_ProtxtA, InputProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				click(btn_ProSearch2A);
				Thread.sleep(3000);
				sendKeys(txt_ProtxtA, LabourProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				click(btn_ProSearch3A);
				sendKeys(txt_ProtxtA, ResourceProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				click(btn_ProSearch4A);
				Thread.sleep(3000);
				sendKeys(txt_ProtxtA, ExpenseProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				click(btn_ProSearch5A);
				Thread.sleep(3000);
				sendKeys(txt_ProtxtA, SubContractProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				click(btn_ProSearch6A);
				Thread.sleep(3000);
				sendKeys(txt_ProtxtA, ServiceProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				Thread.sleep(3000);
				sendKeys(txt_UnitPrice1A, "100");
				sendKeys(txt_UnitPrice2A, "100");
				sendKeys(txt_UnitPrice3A, "100");
				sendKeys(txt_UnitPrice4A, "100");
				sendKeys(txt_UnitPrice5A, "100");
				sendKeys(txt_UnitPrice6A, "100");

				click(btn_ReferenceCodeLabourA);
				Thread.sleep(3000);
				sendKeys(txt_ReferenceCodeLabourtxtA, ReferenceCodeLabourtxtA);
				Thread.sleep(3000);
				pressEnter(txt_ReferenceCodeLabourtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ReferenceCodeLabourselA);

				click(btn_ReferenceCodeResourceA);
				Thread.sleep(3000);
				sendKeys(txt_ReferenceCodeResourcetxtA, ReferenceCodeResourcetxtA);
				Thread.sleep(3000);
				pressEnter(txt_ReferenceCodeResourcetxtA);
				Thread.sleep(3000);
				doubleClick(sel_ReferenceCodeResourceselA);

				selectText(txt_ReferenceCodeExpenseA, ReferenceCodeExpenseA);

				click(btn_CheckoutA);

				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Service Quotation Form",
							"User Can draft the Service Quotation Form",
							"User Can draft the Service Quotation Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Service Quotation Form",
							"User Can draft the Service Quotation Form", "User Can draft the Service Quotation Form Fail",
							"fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that User Can Release the Service Quotation Form",
							"User Can Release the Service Quotation Form",
							"User Can Release the Service Quotation Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can Release the Service Quotation Form",
							"User Can Release the Service Quotation Form", "User Can Release the Service Quotation Form Fail",
							"fail");
				}
				
				Thread.sleep(3000);
				doubleClick(btn_VersionA);
				Thread.sleep(3000);
				mouseMove(btn_colorSelectedA);
				Thread.sleep(3000);
				click(btn_ConfirmTikA);
				Thread.sleep(3000);
				click(btn_ConfirmbtnA);
				Thread.sleep(3000);
				click(btn_GoToPageLinkA);
				
				switchWindow();
				Thread.sleep(3000);
				click(btn_ServiceDetailsSOA);
				click(btn_CheckoutA);
				
				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Service Order Form",
							"User Can draft the Service Order Form",
							"User Can draft the Service Order Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Service Order Form",
							"User Can draft the Service Order Form", "User Can draft the Service Order Form Fail",
							"fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that User Can Release the Service Order Form",
							"User Can Release the Service Order Form",
							"User Can Release the Service Order Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can Release the Service Order Form",
							"User Can Release the Service Order Form", "User Can Release the Service Order Form Fail",
							"fail");
				}
				
				Thread.sleep(6000);
				clickNavigation();
				Thread.sleep(3000);
				click(btn_FormCaseA);
				
				WaitElement(txt_CopyFromSerchtxtA);
				sendKeys(txt_CopyFromSerchtxtA, CNo);
				pressEnter(txt_CopyFromSerchtxtA);
				WaitElement(sel_CaseSerchSelNewA.replace("CaseTitle", "Case" + obj));
				doubleClick(sel_CaseSerchSelNewA.replace("CaseTitle", "Case" + obj));
				
				
				pageRefersh();
				Thread.sleep(3000);
				click(btn_ActionBtnA);
				Thread.sleep(3000);
				click(btn_DroptoServiceCentreBtnAfterSQA);
				Thread.sleep(3000);
				click(btn_EntutionHeaderA);
				Thread.sleep(3000);
				click(btn_RolecenterA);
				
				WaitForEstimatedServiceJobsNoA();

				Thread.sleep(3000);
				click(btn_EstimatedServiceJobsA);
				Thread.sleep(5000);
				click(btn_ServiceJobArrowA.replace("CNo", CNo));
				
				
				switchWindow();
				
				Thread.sleep(3000);
				click(btn_OwnerBtnA);
				sendKeys(txt_OwnerTxtA, OwnerTxtA);
				Thread.sleep(3000);
				pressEnter(txt_OwnerTxtA);
				Thread.sleep(3000);
				doubleClick(sel_OwnerSelA);
				
				Thread.sleep(3000);
				click(btn_PricingProfileBtnA);
				sendKeys(txt_PricingProfileTxtA, PricingProfileTxtA);
				Thread.sleep(3000);
				pressEnter(txt_PricingProfileTxtA);
				Thread.sleep(3000);
				doubleClick(sel_PricingProfileSelA);
				
				Thread.sleep(3000);
				click(btn_ServiceLocationBtnA);
				sendKeys(txt_ServiceLocationTxtA, ServiceLocationTxtA);
				Thread.sleep(3000);
				pressEnter(txt_ServiceLocationTxtA);
				Thread.sleep(3000);
				doubleClick(sel_ServiceLocationSelA);
				
				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form", "User Can draft the Service Job Order Form Fail",
							"fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that  able to create the service Job Order by convert the Case which created from Menu",
							"User Can Release created service Job Order by convert the Case which created from Menu",
							"User Can Release created service Job Order by convert the Case which created from Menu Successfully", "pass");
				} else {
					writeTestResults("Verify that  able to create the service Job Order by convert the Case which created from Menu",
							"User Can Release created service Job Order by convert the Case which created from Menu", "User Can Release created service Job Order by convert the Case which created from Menu Fail",
							"fail");
				}
				
		   }
		   
		   // Service_Case_025
		   public void VerifyThatAbleToCreateServiceJobOrderCreationByConvertTheCaseWhichCreatedFromRoleCenter() throws Exception {
			   
				Thread.sleep(3000);
				click(btn_RolecenterA);

				Thread.sleep(3000);
				click(btn_searchTagA);
				sendKeys(txt_searchTagtxtA, "s1.1");
				Thread.sleep(3000);
				pressEnter(txt_searchTagtxtA);
				Thread.sleep(3000);
				click(btn_searchTagarrowA);

				switchWindow();

				Thread.sleep(3000);
				LocalTime obj = LocalTime.now();
				sendKeys(txt_TitleTextA, "Case" + obj);
				selectText(txt_OriginTextA, OriginTextFieldA);

				selectText(txt_TypeTextA, TypeTextFieldA);
				selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);
				selectText(txt_PriorityTextA, PriorityTextFieldA);

				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
							"User Can draft the Case Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
							"User Can draft the Case Form Fail", "fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String CNo = trackCode;
				
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that User able to release a case Via Role center",
							"User able to release a case Via Role center",
							"User able to release a case Via Role center Successfully", "pass");
				} else {
					writeTestResults("Verify that User able to release a case Via Role center",
							"User able to release a case Via Role center", "User able to release a case Via Role center Fail",
							"fail");
				}
				
				Thread.sleep(3000);
				click(btn_ActionBtnA);
				click(btn_CreateServiceQuotationBtnA);

				switchWindow();
				Thread.sleep(3000);
				selectText(txt_SalesUnitA, SalesUnitA);
				Thread.sleep(3000);
				click(btn_ServiceDetailsA);

				Thread.sleep(3000);
				click(btn_Plus1A);
				click(btn_Plus2A);
				click(btn_Plus3A);
				click(btn_Plus4A);
				click(btn_Plus5A);

				selectText(txt_Type1A, "Input Product");
				selectText(txt_Type2A, "Labour");
				selectText(txt_Type3A, "Resource");
				selectText(txt_Type4A, "Expense");
				selectText(txt_Type5A, "Sub Contract");
				selectText(txt_Type6A, "Services");

				click(btn_ProSearch1A);
				Thread.sleep(3000);
				sendKeys(txt_ProtxtA, InputProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				click(btn_ProSearch2A);
				Thread.sleep(3000);
				sendKeys(txt_ProtxtA, LabourProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				click(btn_ProSearch3A);
				sendKeys(txt_ProtxtA, ResourceProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				click(btn_ProSearch4A);
				Thread.sleep(3000);
				sendKeys(txt_ProtxtA, ExpenseProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				click(btn_ProSearch5A);
				Thread.sleep(3000);
				sendKeys(txt_ProtxtA, SubContractProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				click(btn_ProSearch6A);
				Thread.sleep(3000);
				sendKeys(txt_ProtxtA, ServiceProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				Thread.sleep(3000);
				sendKeys(txt_UnitPrice1A, "100");
				sendKeys(txt_UnitPrice2A, "100");
				sendKeys(txt_UnitPrice3A, "100");
				sendKeys(txt_UnitPrice4A, "100");
				sendKeys(txt_UnitPrice5A, "100");
				sendKeys(txt_UnitPrice6A, "100");

				click(btn_ReferenceCodeLabourA);
				Thread.sleep(3000);
				sendKeys(txt_ReferenceCodeLabourtxtA, ReferenceCodeLabourtxtA);
				Thread.sleep(3000);
				pressEnter(txt_ReferenceCodeLabourtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ReferenceCodeLabourselA);

				click(btn_ReferenceCodeResourceA);
				Thread.sleep(3000);
				sendKeys(txt_ReferenceCodeResourcetxtA, ReferenceCodeResourcetxtA);
				Thread.sleep(3000);
				pressEnter(txt_ReferenceCodeResourcetxtA);
				Thread.sleep(3000);
				doubleClick(sel_ReferenceCodeResourceselA);

				selectText(txt_ReferenceCodeExpenseA, ReferenceCodeExpenseA);

				click(btn_CheckoutA);

				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Service Quotation Form",
							"User Can draft the Service Quotation Form",
							"User Can draft the Service Quotation Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Service Quotation Form",
							"User Can draft the Service Quotation Form", "User Can draft the Service Quotation Form Fail",
							"fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that  able to create Service Quotation using Case which created from Role Center",
							"create Service Quotation using Case which created from Role Center",
							"create Service Quotation using Case which created from Role Center Successfully", "pass");
				} else {
					writeTestResults("Verify that  able to create Service Quotation using Case which created from Role Center",
							"create Service Quotation using Case which created from Role Center", "create Service Quotation using Case which created from Role Center Fail",
							"fail");
				}
				
				Thread.sleep(3000);
				doubleClick(btn_VersionA);
				Thread.sleep(3000);
				mouseMove(btn_colorSelectedA);
				Thread.sleep(3000);
				click(btn_ConfirmTikA);
				Thread.sleep(3000);
				click(btn_ConfirmbtnA);
				Thread.sleep(3000);
				click(btn_GoToPageLinkA);
				
				switchWindow();
				Thread.sleep(3000);
				click(btn_ServiceDetailsSOA);
				click(btn_CheckoutA);
				
				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Service Order Form",
							"User Can draft the Service Order Form",
							"User Can draft the Service Order Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Service Order Form",
							"User Can draft the Service Order Form", "User Can draft the Service Order Form Fail",
							"fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that User Can Release the Service Order Form",
							"User Can Release the Service Order Form",
							"User Can Release the Service Order Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can Release the Service Order Form",
							"User Can Release the Service Order Form", "User Can Release the Service Order Form Fail",
							"fail");
				}
				
				Thread.sleep(6000);
				clickNavigation();
				Thread.sleep(3000);
				click(btn_FormCaseA);
				
				WaitElement(txt_CopyFromSerchtxtA);
				sendKeys(txt_CopyFromSerchtxtA, CNo);
				pressEnter(txt_CopyFromSerchtxtA);
				WaitElement(sel_CaseSerchSelNewA.replace("CaseTitle", "Case" + obj));
				doubleClick(sel_CaseSerchSelNewA.replace("CaseTitle", "Case" + obj));
				
				
				pageRefersh();
				Thread.sleep(3000);
				click(btn_ActionBtnA);
				Thread.sleep(3000);
				click(btn_DroptoServiceCentreBtnAfterSQA);
				Thread.sleep(3000);
				click(btn_EntutionHeaderA);
				Thread.sleep(3000);
				click(btn_RolecenterA);
				
				WaitForEstimatedServiceJobsNoA();
				
				Thread.sleep(3000);
				click(btn_EstimatedServiceJobsA);
				Thread.sleep(5000);
				click(btn_ServiceJobArrowA.replace("CNo", CNo));
				
				switchWindow();
				
				Thread.sleep(3000);
				click(btn_OwnerBtnA);
				sendKeys(txt_OwnerTxtA, OwnerTxtA);
				Thread.sleep(3000);
				pressEnter(txt_OwnerTxtA);
				Thread.sleep(3000);
				doubleClick(sel_OwnerSelA);
				
				Thread.sleep(3000);
				click(btn_PricingProfileBtnA);
				sendKeys(txt_PricingProfileTxtA, PricingProfileTxtA);
				Thread.sleep(3000);
				pressEnter(txt_PricingProfileTxtA);
				Thread.sleep(3000);
				doubleClick(sel_PricingProfileSelA);
				
				Thread.sleep(3000);
				click(btn_ServiceLocationBtnA);
				sendKeys(txt_ServiceLocationTxtA, ServiceLocationTxtA);
				Thread.sleep(3000);
				pressEnter(txt_ServiceLocationTxtA);
				Thread.sleep(3000);
				doubleClick(sel_ServiceLocationSelA);
				
				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form", "User Can draft the Service Job Order Form Fail",
							"fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that  able to create the service Job Order by convert the Case which created from  Role Center",
							"User Can Release created service Job Order by convert the Case which created from  Role Center",
							"User Can Release created service Job Order by convert the Case which created from  Role Center Successfully", "pass");
				} else {
					writeTestResults("Verify that  able to create the service Job Order by convert the Case which created from  Role Center",
							"User Can Release created service Job Order by convert the Case which created from  Role Center", "User Can Release created service Job Order by convert the Case which created from  Role Center Fail",
							"fail");
				}
		   }
		   
		   // Service_Case_026
		   public void VerifyThatDropToServiceJobOrderActionIsRemovedOnceClickedIt() throws Exception {
			   

			   navigationCaseForm();
				Thread.sleep(1000);
				LocalTime obj = LocalTime.now();
				sendKeys(txt_TitleTextA, "Case" + obj);
				selectText(txt_OriginTextA, OriginTextFieldA);

				click(btn_CustomerAccountButtonA);
				sendKeys(txt_CustomerAccountTextA, CustomerAccountTextA);
				Thread.sleep(3000);
				pressEnter(txt_CustomerAccountTextA);
				Thread.sleep(3000);
				doubleClick(sel_CustomerAccountSelA);

				Thread.sleep(3000);
				click(btn_newWindowProdA);
				Thread.sleep(3000);
				click(sel_newWindowProdSelA);
				Thread.sleep(3000);
				JavascriptExecutor j1 = (JavascriptExecutor) driver;
				j1.executeScript("$(\"#divServiceSerial\").parent().find('.dialogbox-buttonarea > .button').click()");

				selectText(txt_TypeTextA, TypeTextFieldA);
				selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);
				selectText(txt_PriorityTextA, PriorityTextFieldA);

				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
							"User Can draft the Case Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
							"User Can draft the Case Form Fail", "fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String CNo = trackCode;
				
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that User Can Release the Case Form", "User Can Release the Case Form",
							"User Can Release the Case Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can Release the Case Form", "User Can Release the Case Form",
							"User Can Release the Case Form Fail", "fail");
				}

				Thread.sleep(3000);
				click(btn_ActionBtnA);
				click(btn_CreateServiceQuotationBtnA);

				switchWindow();
				Thread.sleep(3000);
				selectText(txt_SalesUnitA, SalesUnitA);
				Thread.sleep(3000);
				click(btn_ServiceDetailsA);

				Thread.sleep(3000);
				click(btn_Plus1A);
				click(btn_Plus2A);
				click(btn_Plus3A);
				click(btn_Plus4A);
				click(btn_Plus5A);

				selectText(txt_Type1A, "Input Product");
				selectText(txt_Type2A, "Labour");
				selectText(txt_Type3A, "Resource");
				selectText(txt_Type4A, "Expense");
				selectText(txt_Type5A, "Sub Contract");
				selectText(txt_Type6A, "Services");

				click(btn_ProSearch1A);
				Thread.sleep(3000);
				sendKeys(txt_ProtxtA, InputProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				click(btn_ProSearch2A);
				Thread.sleep(3000);
				sendKeys(txt_ProtxtA, LabourProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				click(btn_ProSearch3A);
				sendKeys(txt_ProtxtA, ResourceProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				click(btn_ProSearch4A);
				Thread.sleep(3000);
				sendKeys(txt_ProtxtA, ExpenseProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				click(btn_ProSearch5A);
				Thread.sleep(3000);
				sendKeys(txt_ProtxtA, SubContractProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				click(btn_ProSearch6A);
				Thread.sleep(3000);
				sendKeys(txt_ProtxtA, ServiceProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				Thread.sleep(3000);
				sendKeys(txt_UnitPrice1A, "100");
				sendKeys(txt_UnitPrice2A, "100");
				sendKeys(txt_UnitPrice3A, "100");
				sendKeys(txt_UnitPrice4A, "100");
				sendKeys(txt_UnitPrice5A, "100");
				sendKeys(txt_UnitPrice6A, "100");

				click(btn_ReferenceCodeLabourA);
				Thread.sleep(3000);
				sendKeys(txt_ReferenceCodeLabourtxtA, ReferenceCodeLabourtxtA);
				Thread.sleep(3000);
				pressEnter(txt_ReferenceCodeLabourtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ReferenceCodeLabourselA);

				click(btn_ReferenceCodeResourceA);
				Thread.sleep(3000);
				sendKeys(txt_ReferenceCodeResourcetxtA, ReferenceCodeResourcetxtA);
				Thread.sleep(3000);
				pressEnter(txt_ReferenceCodeResourcetxtA);
				Thread.sleep(3000);
				doubleClick(sel_ReferenceCodeResourceselA);

				selectText(txt_ReferenceCodeExpenseA, ReferenceCodeExpenseA);

				click(btn_CheckoutA);

				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Service Quotation Form",
							"User Can draft the Service Quotation Form",
							"User Can draft the Service Quotation Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Service Quotation Form",
							"User Can draft the Service Quotation Form", "User Can draft the Service Quotation Form Fail",
							"fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that User Can Release the Service Quotation Form",
							"User Can Release the Service Quotation Form",
							"User Can Release the Service Quotation Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can Release the Service Quotation Form",
							"User Can Release the Service Quotation Form", "User Can Release the Service Quotation Form Fail",
							"fail");
				}
				
				Thread.sleep(3000);
				doubleClick(btn_VersionA);
				Thread.sleep(3000);
				mouseMove(btn_colorSelectedA);
				Thread.sleep(3000);
				click(btn_ConfirmTikA);
				Thread.sleep(3000);
				click(btn_ConfirmbtnA);
				Thread.sleep(3000);
				click(btn_GoToPageLinkA);
				
				switchWindow();
				Thread.sleep(3000);
				click(btn_ServiceDetailsSOA);
				click(btn_CheckoutA);
				
				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Service Order Form",
							"User Can draft the Service Order Form",
							"User Can draft the Service Order Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Service Order Form",
							"User Can draft the Service Order Form", "User Can draft the Service Order Form Fail",
							"fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that User Can Release the Service Order Form",
							"User Can Release the Service Order Form",
							"User Can Release the Service Order Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can Release the Service Order Form",
							"User Can Release the Service Order Form", "User Can Release the Service Order Form Fail",
							"fail");
				}
				
				Thread.sleep(6000);
				clickNavigation();
				WaitClick(btn_FormCaseA);
				click(btn_FormCaseA);
				
				WaitElement(txt_CopyFromSerchtxtA);
				sendKeys(txt_CopyFromSerchtxtA, CNo);
				pressEnter(txt_CopyFromSerchtxtA);
				Thread.sleep(5000);
				doubleClick(sel_CaseSerchSelA);
				
				
				pageRefersh();
				Thread.sleep(3000);
				click(btn_ActionBtnA);
				Thread.sleep(3000);
				click(btn_DroptoServiceCentreBtnAfterSQA);
				
//				Thread.sleep(3000);
//				click(btn_EntutionHeaderA);
//				Thread.sleep(3000);
//				click(btn_RolecenterA);
//				
//				WaitForEstimatedServiceJobsNoA();
//				
//				Thread.sleep(3000);
//				click(btn_EstimatedServiceJobsA);
//				Thread.sleep(3000);
//				click(btn_ServiceJobArrowA.replace("CNo", CNo));
//				
//				
//				switchWindow();
//				
//				Thread.sleep(3000);
//				click(btn_OwnerBtnA);
//				sendKeys(txt_OwnerTxtA, OwnerTxtA);
//				Thread.sleep(3000);
//				pressEnter(txt_OwnerTxtA);
//				Thread.sleep(3000);
//				doubleClick(sel_OwnerSelA);
//				
//				Thread.sleep(3000);
//				click(btn_PricingProfileBtnA);
//				sendKeys(txt_PricingProfileTxtA, PricingProfileTxtA);
//				Thread.sleep(3000);
//				pressEnter(txt_PricingProfileTxtA);
//				Thread.sleep(3000);
//				doubleClick(sel_PricingProfileSelA);
//				
//				Thread.sleep(3000);
//				click(btn_ServiceLocationBtnA);
//				sendKeys(txt_ServiceLocationTxtA, ServiceLocationTxtA);
//				Thread.sleep(3000);
//				pressEnter(txt_ServiceLocationTxtA);
//				Thread.sleep(3000);
//				doubleClick(sel_ServiceLocationSelA);
//				
//				Thread.sleep(3000);
//				click(btn_DraftA);
//				Thread.sleep(5000);
//				if (isDisplayed(txt_PageDraft1A)) {
//					writeTestResults("Verify that User Can draft the Service Job Order Form",
//							"User Can draft the Service Job Order Form",
//							"User Can draft the Service Job Order Form Successfully", "pass");
//				} else {
//					writeTestResults("Verify that User Can draft the Service Job Order Form",
//							"User Can draft the Service Job Order Form", "User Can draft the Service Job Order Form Fail",
//							"fail");
//				}
//
//				Thread.sleep(3000);
//				click(btn_ReleaseA);
//				Thread.sleep(3000);
//				trackCode = getText(txt_trackCodeA);
//				Thread.sleep(5000);
//				if (isDisplayed(txt_PageRelease1A)) {
//					writeTestResults("Verify that  able to create the service Job Order by convert the Case which created from Menu",
//							"User Can Release created service Job Order by convert the Case which created from Menu",
//							"User Can Release created service Job Order by convert the Case which created from Menu Successfully", "pass");
//				} else {
//					writeTestResults("Verify that  able to create the service Job Order by convert the Case which created from Menu",
//							"User Can Release created service Job Order by convert the Case which created from Menu", "User Can Release created service Job Order by convert the Case which created from Menu Fail",
//							"fail");
//				}
//				
//				Thread.sleep(6000);
//				clickNavigation();
//				Thread.sleep(3000);
//				click(btn_FormCaseA);
//				
//				Thread.sleep(3000);
//				sendKeys(txt_CopyFromSerchtxtA, CNo);
//				Thread.sleep(3000);
//				pressEnter(txt_CopyFromSerchtxtA);
//				Thread.sleep(3000);
//				doubleClick(sel_CaseSerchSelA);
				
				pageRefersh();
				
				Thread.sleep(3000);
				click(btn_ActionBtnA);
				Thread.sleep(5000);
				if (!isDisplayed(btn_DroptoServiceCentreBtnA)) {
					writeTestResults("Verify that \"Drop to service job order\" action is removed once clicked it",
							"\"Drop to service center\" action is not available",
							"\"Drop to service center\" action should not available Successfully", "pass");
				} else {
					writeTestResults("Verify that \"Drop to service job order\" action is removed once clicked it",
							"\"Drop to service center\" action is not available", "\"Drop to service center\" action should not available Fail",
							"fail");
				}
		   }
		   
		   // Service_Case_027_028
		   public void VerifyThatStageHeaderIsChangedToJobStartedAndStatusHeaderIsChangedToInProgress() throws Exception {
			   
			   navigationCaseForm();
				Thread.sleep(1000);
				LocalTime obj = LocalTime.now();
				sendKeys(txt_TitleTextA, "Case" + obj);
				selectText(txt_OriginTextA, OriginTextFieldA);

				click(btn_CustomerAccountButtonA);
				sendKeys(txt_CustomerAccountTextA, CustomerAccountTextA);
				Thread.sleep(3000);
				pressEnter(txt_CustomerAccountTextA);
				Thread.sleep(3000);
				doubleClick(sel_CustomerAccountSelA);

				Thread.sleep(3000);
				click(btn_newWindowProdA);
				Thread.sleep(3000);
				click(sel_newWindowProdSelA);
				Thread.sleep(3000);
				JavascriptExecutor j1 = (JavascriptExecutor) driver;
				j1.executeScript("$(\"#divServiceSerial\").parent().find('.dialogbox-buttonarea > .button').click()");

				selectText(txt_TypeTextA, TypeTextFieldA);
				selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);
				selectText(txt_PriorityTextA, PriorityTextFieldA);

				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
							"User Can draft the Case Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
							"User Can draft the Case Form Fail", "fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String CNo = trackCode;
				
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that User Can Release the Case Form", "User Can Release the Case Form",
							"User Can Release the Case Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can Release the Case Form", "User Can Release the Case Form",
							"User Can Release the Case Form Fail", "fail");
				}

				Thread.sleep(3000);
				click(btn_ActionBtnA);
				click(btn_CreateServiceQuotationBtnA);

				switchWindow();
				Thread.sleep(3000);
				selectText(txt_SalesUnitA, SalesUnitA);
				Thread.sleep(3000);
				click(btn_ServiceDetailsA);

				Thread.sleep(3000);
				click(btn_Plus1A);
				click(btn_Plus2A);
				click(btn_Plus3A);
				click(btn_Plus4A);
				click(btn_Plus5A);

				selectText(txt_Type1A, "Input Product");
				selectText(txt_Type2A, "Labour");
				selectText(txt_Type3A, "Resource");
				selectText(txt_Type4A, "Expense");
				selectText(txt_Type5A, "Sub Contract");
				selectText(txt_Type6A, "Services");

				click(btn_ProSearch1A);
				Thread.sleep(3000);
				sendKeys(txt_ProtxtA, InputProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				click(btn_ProSearch2A);
				Thread.sleep(3000);
				sendKeys(txt_ProtxtA, LabourProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				click(btn_ProSearch3A);
				sendKeys(txt_ProtxtA, ResourceProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				click(btn_ProSearch4A);
				Thread.sleep(3000);
				sendKeys(txt_ProtxtA, ExpenseProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				click(btn_ProSearch5A);
				Thread.sleep(3000);
				sendKeys(txt_ProtxtA, SubContractProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				click(btn_ProSearch6A);
				Thread.sleep(3000);
				sendKeys(txt_ProtxtA, ServiceProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				Thread.sleep(3000);
				sendKeys(txt_UnitPrice1A, "100");
				sendKeys(txt_UnitPrice2A, "100");
				sendKeys(txt_UnitPrice3A, "100");
				sendKeys(txt_UnitPrice4A, "100");
				sendKeys(txt_UnitPrice5A, "100");
				sendKeys(txt_UnitPrice6A, "100");

				click(btn_ReferenceCodeLabourA);
				Thread.sleep(3000);
				sendKeys(txt_ReferenceCodeLabourtxtA, ReferenceCodeLabourtxtA);
				Thread.sleep(3000);
				pressEnter(txt_ReferenceCodeLabourtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ReferenceCodeLabourselA);

				click(btn_ReferenceCodeResourceA);
				Thread.sleep(3000);
				sendKeys(txt_ReferenceCodeResourcetxtA, ReferenceCodeResourcetxtA);
				Thread.sleep(3000);
				pressEnter(txt_ReferenceCodeResourcetxtA);
				Thread.sleep(3000);
				doubleClick(sel_ReferenceCodeResourceselA);

				selectText(txt_ReferenceCodeExpenseA, ReferenceCodeExpenseA);

				click(btn_CheckoutA);

				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Service Quotation Form",
							"User Can draft the Service Quotation Form",
							"User Can draft the Service Quotation Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Service Quotation Form",
							"User Can draft the Service Quotation Form", "User Can draft the Service Quotation Form Fail",
							"fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that User Can Release the Service Quotation Form",
							"User Can Release the Service Quotation Form",
							"User Can Release the Service Quotation Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can Release the Service Quotation Form",
							"User Can Release the Service Quotation Form", "User Can Release the Service Quotation Form Fail",
							"fail");
				}
				
				Thread.sleep(3000);
				doubleClick(btn_VersionA);
				Thread.sleep(3000);
				mouseMove(btn_colorSelectedA);
				Thread.sleep(3000);
				click(btn_ConfirmTikA);
				Thread.sleep(3000);
				click(btn_ConfirmbtnA);
				Thread.sleep(3000);
				click(btn_GoToPageLinkA);
				
				switchWindow();
				Thread.sleep(3000);
				click(btn_ServiceDetailsSOA);
				click(btn_CheckoutA);
				
				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Service Order Form",
							"User Can draft the Service Order Form",
							"User Can draft the Service Order Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Service Order Form",
							"User Can draft the Service Order Form", "User Can draft the Service Order Form Fail",
							"fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that User Can Release the Service Order Form",
							"User Can Release the Service Order Form",
							"User Can Release the Service Order Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can Release the Service Order Form",
							"User Can Release the Service Order Form", "User Can Release the Service Order Form Fail",
							"fail");
				}
				
				Thread.sleep(6000);
				clickNavigation();
				Thread.sleep(3000);
				click(btn_FormCaseA);
				
				Thread.sleep(3000);
				sendKeys(txt_CopyFromSerchtxtA, CNo);
				pressEnter(txt_CopyFromSerchtxtA);
				Thread.sleep(5000);
				doubleClick(sel_CaseSerchSelA);
				
				
				pageRefersh();
				Thread.sleep(3000);
				click(btn_ActionBtnA);
				Thread.sleep(3000);
				click(btn_DroptoServiceCentreBtnAfterSQA);
				Thread.sleep(3000);
				click(btn_EntutionHeaderA);
				Thread.sleep(3000);
				click(btn_RolecenterA);
				
				WaitForEstimatedServiceJobsNoA();
				
				Thread.sleep(3000);
				click(btn_EstimatedServiceJobsA);
				Thread.sleep(5000);
				click(btn_ServiceJobArrowA.replace("CNo", CNo));
				
				
				switchWindow();
				
				Thread.sleep(3000);
				click(btn_OwnerBtnA);
				sendKeys(txt_OwnerTxtA, OwnerTxtA);
				Thread.sleep(3000);
				pressEnter(txt_OwnerTxtA);
				Thread.sleep(3000);
				doubleClick(sel_OwnerSelA);
				
				Thread.sleep(3000);
				click(btn_PricingProfileBtnA);
				sendKeys(txt_PricingProfileTxtA, PricingProfileTxtA);
				Thread.sleep(3000);
				pressEnter(txt_PricingProfileTxtA);
				Thread.sleep(3000);
				doubleClick(sel_PricingProfileSelA);
				
				Thread.sleep(3000);
				click(btn_ServiceLocationBtnA);
				sendKeys(txt_ServiceLocationTxtA, ServiceLocationTxtA);
				Thread.sleep(3000);
				pressEnter(txt_ServiceLocationTxtA);
				Thread.sleep(3000);
				doubleClick(sel_ServiceLocationSelA);
				
				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form", "User Can draft the Service Job Order Form Fail",
							"fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that  able to create the service Job Order by convert the Case which created from Menu",
							"User Can Release created service Job Order by convert the Case which created from Menu",
							"User Can Release created service Job Order by convert the Case which created from Menu Successfully", "pass");
				} else {
					writeTestResults("Verify that  able to create the service Job Order by convert the Case which created from Menu",
							"User Can Release created service Job Order by convert the Case which created from Menu", "User Can Release created service Job Order by convert the Case which created from Menu Fail",
							"fail");
				}
				
				Thread.sleep(6000);
				clickNavigation();
				Thread.sleep(3000);
				click(btn_FormCaseA);
				
				Thread.sleep(3000);
				sendKeys(txt_CopyFromSerchtxtA, CNo);
				pressEnter(txt_CopyFromSerchtxtA);
				Thread.sleep(5000);
				doubleClick(sel_CaseSerchSelA);
				
				Thread.sleep(3000);
				String StageFieldA = new Select(driver.findElement(By.xpath(txt_StageFieldA))).getFirstSelectedOption().getText();
				Thread.sleep(3000);
				click(btn_StatusFieldArrowA);
				String StatusFieldA = getText(txt_StatusFieldTxtA);

				Thread.sleep(5000);
				if (StageFieldA.contentEquals("Job Started") && StatusFieldA.contentEquals("In Progress")) {
					writeTestResults("Verify that status of 'Status' and 'Stage' headers are changed to \"In-Progress\" and \"Job Started\"when creating an service Quotation",
							"status of 'Status' and 'Stage' headers are changed to \"In-Progress\" and \"Job Started\"when creating an service Quotation",
							"status of 'Status' and 'Stage' headers are changed to \"In-Progress\" and \"Job Started\"when creating an service Quotation Successfully", "pass");
				} else {
					writeTestResults("Verify that status of 'Status' and 'Stage' headers are changed to \"In-Progress\" and \"Job Started\"when creating an service Quotation",
							"status of 'Status' and 'Stage' headers are changed to \"In-Progress\" and \"Job Started\"when creating an service Quotation",
							"status of 'Status' and 'Stage' headers are changed to \"In-Progress\" and \"Job Started\"when creating an service Quotation Fail", "fail");
				}
		   }


		   
		   // Service_Case_029
		   public void VerifyThatStatusColumnInTheBypageIsChagedToInProgressAfterReleasingTheServiceQuotation() throws Exception {
			 
			   navigationCaseForm();
				Thread.sleep(1000);
				LocalTime obj = LocalTime.now();
				sendKeys(txt_TitleTextA, "Case" + obj);
				selectText(txt_OriginTextA, OriginTextFieldA);

				click(btn_CustomerAccountButtonA);
				sendKeys(txt_CustomerAccountTextA, CustomerAccountTextA);
				Thread.sleep(3000);
				pressEnter(txt_CustomerAccountTextA);
				Thread.sleep(3000);
				doubleClick(sel_CustomerAccountSelA);

				Thread.sleep(3000);
				click(btn_newWindowProdA);
				Thread.sleep(3000);
				click(sel_newWindowProdSelA);
				Thread.sleep(3000);
				JavascriptExecutor j1 = (JavascriptExecutor) driver;
				j1.executeScript("$(\"#divServiceSerial\").parent().find('.dialogbox-buttonarea > .button').click()");

				selectText(txt_TypeTextA, TypeTextFieldA);
				selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);
				selectText(txt_PriorityTextA, PriorityTextFieldA);

				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
							"User Can draft the Case Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
							"User Can draft the Case Form Fail", "fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String CNo = trackCode;
				
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that User Can Release the Case Form", "User Can Release the Case Form",
							"User Can Release the Case Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can Release the Case Form", "User Can Release the Case Form",
							"User Can Release the Case Form Fail", "fail");
				}

				Thread.sleep(3000);
				click(btn_ActionBtnA);
				click(btn_CreateServiceQuotationBtnA);

				switchWindow();
				Thread.sleep(3000);
				selectText(txt_SalesUnitA, SalesUnitA);
				Thread.sleep(3000);
				click(btn_ServiceDetailsA);

				Thread.sleep(3000);
				click(btn_Plus1A);
				click(btn_Plus2A);
				click(btn_Plus3A);
				click(btn_Plus4A);
				click(btn_Plus5A);

				selectText(txt_Type1A, "Input Product");
				selectText(txt_Type2A, "Labour");
				selectText(txt_Type3A, "Resource");
				selectText(txt_Type4A, "Expense");
				selectText(txt_Type5A, "Sub Contract");
				selectText(txt_Type6A, "Services");

				click(btn_ProSearch1A);
				Thread.sleep(3000);
				sendKeys(txt_ProtxtA, InputProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				click(btn_ProSearch2A);
				Thread.sleep(3000);
				sendKeys(txt_ProtxtA, LabourProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				click(btn_ProSearch3A);
				sendKeys(txt_ProtxtA, ResourceProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				click(btn_ProSearch4A);
				Thread.sleep(3000);
				sendKeys(txt_ProtxtA, ExpenseProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				click(btn_ProSearch5A);
				Thread.sleep(3000);
				sendKeys(txt_ProtxtA, SubContractProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				click(btn_ProSearch6A);
				Thread.sleep(3000);
				sendKeys(txt_ProtxtA, ServiceProductA);
				Thread.sleep(3000);
				pressEnter(txt_ProtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ProselA);

				Thread.sleep(3000);
				sendKeys(txt_UnitPrice1A, "100");
				sendKeys(txt_UnitPrice2A, "100");
				sendKeys(txt_UnitPrice3A, "100");
				sendKeys(txt_UnitPrice4A, "100");
				sendKeys(txt_UnitPrice5A, "100");
				sendKeys(txt_UnitPrice6A, "100");

				click(btn_ReferenceCodeLabourA);
				Thread.sleep(3000);
				sendKeys(txt_ReferenceCodeLabourtxtA, ReferenceCodeLabourtxtA);
				Thread.sleep(3000);
				pressEnter(txt_ReferenceCodeLabourtxtA);
				Thread.sleep(3000);
				doubleClick(sel_ReferenceCodeLabourselA);

				click(btn_ReferenceCodeResourceA);
				Thread.sleep(3000);
				sendKeys(txt_ReferenceCodeResourcetxtA, ReferenceCodeResourcetxtA);
				Thread.sleep(3000);
				pressEnter(txt_ReferenceCodeResourcetxtA);
				Thread.sleep(3000);
				doubleClick(sel_ReferenceCodeResourceselA);

				selectText(txt_ReferenceCodeExpenseA, ReferenceCodeExpenseA);

				click(btn_CheckoutA);

				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Service Quotation Form",
							"User Can draft the Service Quotation Form",
							"User Can draft the Service Quotation Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Service Quotation Form",
							"User Can draft the Service Quotation Form", "User Can draft the Service Quotation Form Fail",
							"fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that User Can Release the Service Quotation Form",
							"User Can Release the Service Quotation Form",
							"User Can Release the Service Quotation Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can Release the Service Quotation Form",
							"User Can Release the Service Quotation Form", "User Can Release the Service Quotation Form Fail",
							"fail");
				}
				
				Thread.sleep(3000);
				doubleClick(btn_VersionA);
				Thread.sleep(3000);
				mouseMove(btn_colorSelectedA);
				Thread.sleep(3000);
				click(btn_ConfirmTikA);
				Thread.sleep(3000);
				click(btn_ConfirmbtnA);
				Thread.sleep(3000);
				click(btn_GoToPageLinkA);
				
				switchWindow();
				Thread.sleep(3000);
				click(btn_ServiceDetailsSOA);
				click(btn_CheckoutA);
				
				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Service Order Form",
							"User Can draft the Service Order Form",
							"User Can draft the Service Order Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Service Order Form",
							"User Can draft the Service Order Form", "User Can draft the Service Order Form Fail",
							"fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that User Can Release the Service Order Form",
							"User Can Release the Service Order Form",
							"User Can Release the Service Order Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can Release the Service Order Form",
							"User Can Release the Service Order Form", "User Can Release the Service Order Form Fail",
							"fail");
				}
				
				Thread.sleep(6000);
				clickNavigation();
				Thread.sleep(3000);
				click(btn_FormCaseA);
				
				Thread.sleep(3000);
				sendKeys(txt_CopyFromSerchtxtA, CNo);
				pressEnter(txt_CopyFromSerchtxtA);
				Thread.sleep(5000);
				doubleClick(sel_CaseSerchSelA);
				
				
				pageRefersh();
				Thread.sleep(3000);
				click(btn_ActionBtnA);
				Thread.sleep(3000);
				click(btn_DroptoServiceCentreBtnAfterSQA);
				Thread.sleep(3000);
				click(btn_EntutionHeaderA);
				Thread.sleep(3000);
				click(btn_RolecenterA);
				
				WaitForEstimatedServiceJobsNoA();
				
				Thread.sleep(3000);
				click(btn_EstimatedServiceJobsA);
				Thread.sleep(5000);
				click(btn_ServiceJobArrowA.replace("CNo", CNo));
				
				
				switchWindow();
				
				Thread.sleep(3000);
				click(btn_OwnerBtnA);
				sendKeys(txt_OwnerTxtA, OwnerTxtA);
				Thread.sleep(3000);
				pressEnter(txt_OwnerTxtA);
				Thread.sleep(3000);
				doubleClick(sel_OwnerSelA);
				
				Thread.sleep(3000);
				click(btn_PricingProfileBtnA);
				sendKeys(txt_PricingProfileTxtA, PricingProfileTxtA);
				Thread.sleep(3000);
				pressEnter(txt_PricingProfileTxtA);
				Thread.sleep(3000);
				doubleClick(sel_PricingProfileSelA);
				
				Thread.sleep(3000);
				click(btn_ServiceLocationBtnA);
				sendKeys(txt_ServiceLocationTxtA, ServiceLocationTxtA);
				Thread.sleep(3000);
				pressEnter(txt_ServiceLocationTxtA);
				Thread.sleep(3000);
				doubleClick(sel_ServiceLocationSelA);
				
				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form", "User Can draft the Service Job Order Form Fail",
							"fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that  able to create the service Job Order by convert the Case which created from Menu",
							"User Can Release created service Job Order by convert the Case which created from Menu",
							"User Can Release created service Job Order by convert the Case which created from Menu Successfully", "pass");
				} else {
					writeTestResults("Verify that  able to create the service Job Order by convert the Case which created from Menu",
							"User Can Release created service Job Order by convert the Case which created from Menu", "User Can Release created service Job Order by convert the Case which created from Menu Fail",
							"fail");
				}
				
				Thread.sleep(6000);
				clickNavigation();
				Thread.sleep(3000);
				click(btn_FormCaseA);
				
				Thread.sleep(3000);
				sendKeys(txt_CopyFromSerchtxtA, CNo);
				Thread.sleep(3000);
				pressEnter(txt_CopyFromSerchtxtA);
				
				Thread.sleep(5000);
				if (getText(txt_CaseSerchSelStatusA).contentEquals("In Progress")) {
					writeTestResults("Verify that Status column in the bypage is chaged to \"In-Progress\" after releasing the service Quotation",
							"\"status\" column is changed to \"In-Progress\"",
							"\"status\" column is changed to \"In-Progress\" Successfully", "pass");
				} else {
					writeTestResults("Verify that Status column in the bypage is chaged to \"In-Progress\" after releasing the service Quotation",
							"\"status\" column is changed to \"In-Progress\"",
							"\"status\" column is changed to \"In-Progress\" Fail", "fail");
				}
		   }
		   
		   // Service_Case_030
		   public void VerifyThatAbleToCreateTheServiceJobOrderByConvertTheCaseWhichCreatedFromMenuWithoutServiceQuotation() throws Exception {
			   
			   navigationCaseForm();
				Thread.sleep(1000);
				LocalTime obj = LocalTime.now();
				sendKeys(txt_TitleTextA, "Case" + obj);
				selectText(txt_OriginTextA, OriginTextFieldA);

				click(btn_CustomerAccountButtonA);
				sendKeys(txt_CustomerAccountTextA, CustomerAccountTextA);
				Thread.sleep(3000);
				pressEnter(txt_CustomerAccountTextA);
				Thread.sleep(3000);
				doubleClick(sel_CustomerAccountSelA);

				Thread.sleep(3000);
				click(btn_newWindowProdA);
				Thread.sleep(3000);
				click(sel_newWindowProdSelA);
				Thread.sleep(3000);
				JavascriptExecutor j1 = (JavascriptExecutor) driver;
				j1.executeScript("$(\"#divServiceSerial\").parent().find('.dialogbox-buttonarea > .button').click()");

				selectText(txt_TypeTextA, TypeTextFieldA);
				selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);
				selectText(txt_PriorityTextA, PriorityTextFieldA);

				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that able to draft the Case via main menu", "able to draft the Case via main menu",
							"able to draft the Case via main menu Successfully", "pass");
				} else {
					writeTestResults("Verify that able to draft the Case via main menu", "able to draft the Case via main menu",
							"able to draft the Case via main menu Fail", "fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String CNo = trackCode;
				
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that able to Release the Case via main menu",
							"able to Release the Case via main menu", "able to Release the Case via main menu Successfully",
							"pass");
				} else {
					writeTestResults("Verify that able to Release the Case via main menu",
							"able to Release the Case via main menu", "able to Release the Case via main menu Fail", "fail");
				}
				
				pageRefersh();
				Thread.sleep(3000);
				click(btn_ActionBtnA);
				Thread.sleep(3000);
				click(btn_DroptoServiceCentreBtnA);
				Thread.sleep(3000);
				click(btn_EntutionHeaderA);
				Thread.sleep(3000);
				click(btn_RolecenterA);
				
				WaitForOpenServiceJobsNoA();
				
				Thread.sleep(3000);
				click(btn_OpenServiceJobsA);
				Thread.sleep(3000);
				click(btn_ServiceJobArrowA.replace("CNo", CNo));
				
				
				switchWindow();
				
				Thread.sleep(3000);
				click(btn_OwnerBtnA);
				sendKeys(txt_OwnerTxtA, OwnerTxtA);
				Thread.sleep(3000);
				pressEnter(txt_OwnerTxtA);
				Thread.sleep(3000);
				doubleClick(sel_OwnerSelA);
				
				Thread.sleep(3000);
				click(btn_PricingProfileBtnA);
				sendKeys(txt_PricingProfileTxtA, PricingProfileTxtA);
				Thread.sleep(3000);
				pressEnter(txt_PricingProfileTxtA);
				Thread.sleep(3000);
				doubleClick(sel_PricingProfileSelA);
				
				Thread.sleep(3000);
				click(btn_ServiceLocationBtnA);
				sendKeys(txt_ServiceLocationTxtA, ServiceLocationTxtA);
				Thread.sleep(3000);
				pressEnter(txt_ServiceLocationTxtA);
				Thread.sleep(3000);
				doubleClick(sel_ServiceLocationSelA);
				
				Thread.sleep(3000);
				click(btn_TaskDetailsA);
				Thread.sleep(3000);
				click(btn_TaskDetailsProBtnA);
				sendKeys(txt_TaskDetailsProTxtA, ServiceProductA);
				Thread.sleep(3000);
				pressEnter(txt_TaskDetailsProTxtA);
				Thread.sleep(3000);
				doubleClick(sel_TaskDetailsProSelA);
				
				
				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form", "User Can draft the Service Job Order Form Fail",
							"fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that  able to create the service Job Order by convert the Case which created from Menu(Without service quotation)",
							"User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation)",
							"User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation) Successfully", "pass");
				} else {
					writeTestResults("Verify that  able to create the service Job Order by convert the Case which created from Menu(Without service quotation)",
							"User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation)", "User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation) Fail",
							"fail");
				}
		   }
		   
		   // Service_Case_031
		   public void VerifyThatAbleToCreateTheServiceJobOrderByConvertTheCaseWhichCreatedFromRoleCenterWithoutServiceQuotation() throws Exception {
			   
				Thread.sleep(3000);
				click(btn_RolecenterA);

				Thread.sleep(3000);
				click(btn_searchTagA);
				sendKeys(txt_searchTagtxtA, "s1.1");
				Thread.sleep(3000);
				pressEnter(txt_searchTagtxtA);
				Thread.sleep(3000);
				click(btn_searchTagarrowA);

				switchWindow();

				Thread.sleep(3000);
				LocalTime obj = LocalTime.now();
				sendKeys(txt_TitleTextA, "Case" + obj);
				selectText(txt_OriginTextA, OriginTextFieldA);

				selectText(txt_TypeTextA, TypeTextFieldA);
				selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);
				selectText(txt_PriorityTextA, PriorityTextFieldA);

				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
							"User Can draft the Case Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
							"User Can draft the Case Form Fail", "fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String CNo = trackCode;
				
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that User able to release a case Via Role center",
							"User able to release a case Via Role center",
							"User able to release a case Via Role center Successfully", "pass");
				} else {
					writeTestResults("Verify that User able to release a case Via Role center",
							"User able to release a case Via Role center", "User able to release a case Via Role center Fail",
							"fail");
				}
				
				pageRefersh();
				Thread.sleep(3000);
				click(btn_ActionBtnA);
				Thread.sleep(3000);
				click(btn_DroptoServiceCentreBtnA);
				Thread.sleep(3000);
				click(btn_EntutionHeaderA);
				Thread.sleep(3000);
				click(btn_RolecenterA);
				
				WaitForOpenServiceJobsNoA();
				
				Thread.sleep(3000);
				click(btn_OpenServiceJobsA);
				Thread.sleep(3000);
				click(btn_ServiceJobArrowA.replace("CNo", CNo));
				
				
				switchWindow();
				
				Thread.sleep(3000);
				click(btn_OwnerBtnA);
				sendKeys(txt_OwnerTxtA, OwnerTxtA);
				Thread.sleep(3000);
				pressEnter(txt_OwnerTxtA);
				Thread.sleep(3000);
				doubleClick(sel_OwnerSelA);
				
				Thread.sleep(3000);
				click(btn_PricingProfileBtnA);
				sendKeys(txt_PricingProfileTxtA, PricingProfileTxtA);
				Thread.sleep(3000);
				pressEnter(txt_PricingProfileTxtA);
				Thread.sleep(3000);
				doubleClick(sel_PricingProfileSelA);
				
				Thread.sleep(3000);
				click(btn_ServiceLocationBtnA);
				sendKeys(txt_ServiceLocationTxtA, ServiceLocationTxtA);
				Thread.sleep(3000);
				pressEnter(txt_ServiceLocationTxtA);
				Thread.sleep(3000);
				doubleClick(sel_ServiceLocationSelA);
				
				Thread.sleep(3000);
				click(btn_TaskDetailsA);
				Thread.sleep(3000);
				click(btn_TaskDetailsProBtnA);
				sendKeys(txt_TaskDetailsProTxtA, ServiceProductA);
				Thread.sleep(3000);
				pressEnter(txt_TaskDetailsProTxtA);
				Thread.sleep(3000);
				doubleClick(sel_TaskDetailsProSelA);
				
				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form", "User Can draft the Service Job Order Form Fail",
							"fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that  able to create the service Job Order by convert the Case which created from Role Center(Without service quotation)",
							"User Can Release created service Job Order by convert the Case which created from Role Center(Without service quotation)",
							"User Can Release created service Job Order by convert the Case which created from Role Center(Without service quotation) Successfully", "pass");
				} else {
					writeTestResults("Verify that  able to create the service Job Order by convert the Case which created from Role Center(Without service quotation)",
							"User Can Release created service Job Order by convert the Case which created from Role Center(Without service quotation)", "User Can Release created service Job Order by convert the Case which created from Role Center(Without service quotation) Fail",
							"fail");
				}
		   }
		   
		   // Service_Case_032
		   public void VerifyThatNotificationCaseHasBeenDroppedToServiceCenter() throws Exception {
			   
			   navigationCaseForm();
				Thread.sleep(1000);
				LocalTime obj = LocalTime.now();
				sendKeys(txt_TitleTextA, "Case" + obj);
				selectText(txt_OriginTextA, OriginTextFieldA);

				click(btn_CustomerAccountButtonA);
				sendKeys(txt_CustomerAccountTextA, CustomerAccountTextA);
				Thread.sleep(3000);
				pressEnter(txt_CustomerAccountTextA);
				Thread.sleep(3000);
				doubleClick(sel_CustomerAccountSelA);

				Thread.sleep(3000);
				click(btn_newWindowProdA);
				Thread.sleep(3000);
				click(sel_newWindowProdSelA);
				Thread.sleep(3000);
				JavascriptExecutor j1 = (JavascriptExecutor) driver;
				j1.executeScript("$(\"#divServiceSerial\").parent().find('.dialogbox-buttonarea > .button').click()");

				selectText(txt_TypeTextA, TypeTextFieldA);
				selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);
				selectText(txt_PriorityTextA, PriorityTextFieldA);

				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
							"User Can draft the Case Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
							"User Can draft the Case Form Fail", "fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String CNo = trackCode;
				
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that User Can Release the Case Form", "User Can Release the Case Form",
							"User Can Release the Case Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can Release the Case Form", "User Can Release the Case Form",
							"User Can Release the Case Form Fail", "fail");
				}
				
				pageRefersh();
				Thread.sleep(3000);
				click(btn_ActionBtnA);
				Thread.sleep(3000);
				click(btn_DroptoServiceCentreBtnA);
				
				Thread.sleep(5000);
				if (isDisplayed(txt_ConfirmNotificationTxt1A)&&isDisplayed(txt_DropNotificationTxt3A)) {
					writeTestResults("Verify that \"Notification - Case has been dropped to service center.\" is dispalyed after Clicking dropped to service center Button",
							"\"Notification - Case has been dropped to service center.\" is dispalyed after Clicking dropped to service center Button",
							"\"Notification - Case has been dropped to service center.\" is dispalyed after Clicking dropped to service center Button Successfully", "pass");
				} else {
					writeTestResults("Verify that \"Notification - Case has been dropped to service center.\" is dispalyed after Clicking dropped to service center Button",
							"\"Notification - Case has been dropped to service center.\" is dispalyed after Clicking dropped to service center Button", 
							"\"Notification - Case has been dropped to service center.\" is dispalyed after Clicking dropped to service center Button Fail",
							"fail");
				}
			   
		   }
		   
		   // Service_Case_033
		   public void VerifyThatAbleToCloseTheCase() throws Exception {
			   
			   VerifyThatAbleToReleaseTheCaseCreatedViaMenu();
			   
			   WaitElement(btn_ActionBtnA);
			   WaitClick(btn_ActionBtnA);
			   click(btn_ActionBtnA);
			   WaitClick(btn_CloseBtnA);
			   click(btn_CloseBtnA);
			   WaitElement(txt_cboxPOPSatisfactionA);
			   selectText(txt_cboxPOPSatisfactionA, "5");
			   
			   Thread.sleep(3000);
			   JavascriptExecutor j1 = (JavascriptExecutor)driver;
			   j1.executeScript("$(\"#dlgChgSatisfaction\").parent().find('.dialogbox-buttonarea > .button').click()");
			   
				WaitElement(txt_PageClosedA);
				if (isDisplayed(txt_PageClosedA)) {
					writeTestResults("Verify that able to \"Close\" the case", "docuemnt status is changed to \"Closed\"",
							"docuemnt status is changed to \"Closed\" Successfully", "pass");
				} else {
					writeTestResults("Verify that able to \"Close\" the case", "docuemnt status is changed to \"Closed\"",
							"docuemnt status is changed to \"Closed\" Fail", "fail");
				}
		   }
		   
		   // Service_Case_034
		   public void VerifyThatViewHistoryLinkInCaseNew() throws Exception {
			   
			    navigationCaseForm();

			    Thread.sleep(3000);
				click(btn_CustomerAccountButtonA);
				sendKeys(txt_CustomerAccountTextA, CustomerAccountTextA);
				Thread.sleep(3000);
				pressEnter(txt_CustomerAccountTextA);
				Thread.sleep(3000);
				doubleClick(sel_CustomerAccountSelA);
				
				Thread.sleep(3000);
				click(btn_ViewHistoryBtnA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PendingCasesTileHeaderA) && isDisplayed(txt_CompletedCasesTileHeaderA) && isDisplayed(txt_RegisteredProductsTileHeaderA)) {
					writeTestResults("Verify that 'View History' Link in 'Case - New'", "Customer History Window  pop up with following tiles Pending cases,Completed cases,Registered Products",
							"Customer History Window  pop up with following tiles Pending cases,Completed cases,Registered Products Successfully", "pass");
				} else {
					writeTestResults("Verify that 'View History' Link in 'Case - New'", "Customer History Window  pop up with following tiles Pending cases,Completed cases,Registered Products",
							"Customer History Window  pop up with following tiles Pending cases,Completed cases,Registered Products Fail", "fail");
				}
		   }
		   
		   
		   // Service_Case_035
		   public void 	VerifyThatCaseCanBeHOLD() throws Exception {
			   
			    navigationCaseForm();
				Thread.sleep(1000);
				LocalTime obj = LocalTime.now();
				sendKeys(txt_TitleTextA, "Case" + obj);
				selectText(txt_OriginTextA, OriginTextFieldA);

				click(btn_CustomerAccountButtonA);
				sendKeys(txt_CustomerAccountTextA, CustomerAccountTextA);
				Thread.sleep(3000);
				pressEnter(txt_CustomerAccountTextA);
				Thread.sleep(3000);
				doubleClick(sel_CustomerAccountSelA);

				Thread.sleep(3000);
				click(btn_newWindowProdA);
				Thread.sleep(3000);
				click(sel_newWindowProdSelA);
				Thread.sleep(3000);
				JavascriptExecutor j1 = (JavascriptExecutor) driver;
				j1.executeScript("$(\"#divServiceSerial\").parent().find('.dialogbox-buttonarea > .button').click()");

				selectText(txt_TypeTextA, TypeTextFieldA);
				selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);
				selectText(txt_PriorityTextA, PriorityTextFieldA);

				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that able to draft the Case via main menu", "able to draft the Case via main menu",
							"able to draft the Case via main menu Successfully", "pass");
				} else {
					writeTestResults("Verify that able to draft the Case via main menu", "able to draft the Case via main menu",
							"able to draft the Case via main menu Fail", "fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String CNo = trackCode;
				
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that able to Release the Case via main menu",
							"able to Release the Case via main menu", "able to Release the Case via main menu Successfully",
							"pass");
				} else {
					writeTestResults("Verify that able to Release the Case via main menu",
							"able to Release the Case via main menu", "able to Release the Case via main menu Fail", "fail");
				}
				
				pageRefersh();
				Thread.sleep(3000);
				click(btn_ActionBtnA);
				Thread.sleep(3000);
				click(btn_DroptoServiceCentreBtnA);
				Thread.sleep(3000);
				click(btn_EntutionHeaderA);
				Thread.sleep(3000);
				click(btn_RolecenterA);
				
				WaitForOpenServiceJobsNoA();
				
				Thread.sleep(3000);
				click(btn_OpenServiceJobsA);
				Thread.sleep(5000);
				click(btn_ServiceJobArrowA.replace("CNo", CNo));
				
				
				switchWindow();
				
				Thread.sleep(3000);
				click(btn_OwnerBtnA);
				sendKeys(txt_OwnerTxtA, OwnerTxtA);
				Thread.sleep(3000);
				pressEnter(txt_OwnerTxtA);
				Thread.sleep(3000);
				doubleClick(sel_OwnerSelA);
				
				Thread.sleep(3000);
				click(btn_PricingProfileBtnA);
				sendKeys(txt_PricingProfileTxtA, PricingProfileTxtA);
				Thread.sleep(3000);
				pressEnter(txt_PricingProfileTxtA);
				Thread.sleep(3000);
				doubleClick(sel_PricingProfileSelA);
				
				Thread.sleep(3000);
				click(btn_ServiceLocationBtnA);
				sendKeys(txt_ServiceLocationTxtA, ServiceLocationTxtA);
				Thread.sleep(3000);
				pressEnter(txt_ServiceLocationTxtA);
				Thread.sleep(3000);
				doubleClick(sel_ServiceLocationSelA);
				
				Thread.sleep(3000);
				click(btn_TaskDetailsA);
				Thread.sleep(3000);
				click(btn_TaskDetailsProBtnA);
				sendKeys(txt_TaskDetailsProTxtA, ServiceProductA);
				Thread.sleep(3000);
				pressEnter(txt_TaskDetailsProTxtA);
				Thread.sleep(3000);
				doubleClick(sel_TaskDetailsProSelA);
				
				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form", "User Can draft the Service Job Order Form Fail",
							"fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that  able to create the service Job Order by convert the Case which created from Menu(Without service quotation)",
							"User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation)",
							"User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation) Successfully", "pass");
				} else {
					writeTestResults("Verify that  able to create the service Job Order by convert the Case which created from Menu(Without service quotation)",
							"User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation)", "User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation) Fail",
							"fail");
				}
				
				Thread.sleep(6000);
				clickNavigation();
				Thread.sleep(3000);
				click(btn_FormCaseA);
				
				WaitElement(txt_CopyFromSerchtxtA);
				sendKeys(txt_CopyFromSerchtxtA, CNo);
				pressEnter(txt_CopyFromSerchtxtA);
				WaitElement(sel_CaseSerchSelNewA.replace("CaseTitle", "Case" + obj));
				doubleClick(sel_CaseSerchSelNewA.replace("CaseTitle", "Case" + obj));
			   
			   Thread.sleep(3000);
			   click(btn_ActionBtnA);
			   Thread.sleep(3000);
			   click(btn_HoldBtnA);
			   Thread.sleep(3000);
			   sendKeys(txt_HoldReasonA, "Testing");
			   
			   Thread.sleep(3000);
			   j1.executeScript("$(\"#dlgChgActionStatus\").parent().find('.dialogbox-buttonarea > .button').click();");
			   
				Thread.sleep(5000);
				if (isDisplayed(txt_PageHoldA)) {
					writeTestResults("Verify that case can be \"HOLD\"", "docuemnt status is changed to \"HOLD\"",
							"docuemnt status is changed to \"HOLD\" Successfully", "pass");
				} else {
					writeTestResults("Verify that case can be \"HOLD\"", "docuemnt status is changed to \"HOLD\"",
							"docuemnt status is changed to \"HOLD\" Fail", "fail");
				}
		   }
		   
		   // Service_Case_036
		   public void 	VerifyThatDocuemntStatusIconInTheByPageIsChanagedWhenUserHOLDTheDocumentProcess() throws Exception {
			   
			    navigationCaseForm();
				Thread.sleep(1000);
				LocalTime obj = LocalTime.now();
				sendKeys(txt_TitleTextA, "Case" + obj);
				selectText(txt_OriginTextA, OriginTextFieldA);

				click(btn_CustomerAccountButtonA);
				sendKeys(txt_CustomerAccountTextA, CustomerAccountTextA);
				Thread.sleep(3000);
				pressEnter(txt_CustomerAccountTextA);
				Thread.sleep(3000);
				doubleClick(sel_CustomerAccountSelA);

				Thread.sleep(3000);
				click(btn_newWindowProdA);
				Thread.sleep(3000);
				click(sel_newWindowProdSelA);
				Thread.sleep(3000);
				JavascriptExecutor j1 = (JavascriptExecutor) driver;
				j1.executeScript("$(\"#divServiceSerial\").parent().find('.dialogbox-buttonarea > .button').click()");

				selectText(txt_TypeTextA, TypeTextFieldA);
				selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);
				selectText(txt_PriorityTextA, PriorityTextFieldA);

				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that able to draft the Case via main menu", "able to draft the Case via main menu",
							"able to draft the Case via main menu Successfully", "pass");
				} else {
					writeTestResults("Verify that able to draft the Case via main menu", "able to draft the Case via main menu",
							"able to draft the Case via main menu Fail", "fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String CNo = trackCode;
				
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that able to Release the Case via main menu",
							"able to Release the Case via main menu", "able to Release the Case via main menu Successfully",
							"pass");
				} else {
					writeTestResults("Verify that able to Release the Case via main menu",
							"able to Release the Case via main menu", "able to Release the Case via main menu Fail", "fail");
				}
				
				pageRefersh();
				Thread.sleep(3000);
				click(btn_ActionBtnA);
				Thread.sleep(3000);
				click(btn_DroptoServiceCentreBtnA);
				Thread.sleep(3000);
				click(btn_EntutionHeaderA);
				Thread.sleep(3000);
				click(btn_RolecenterA);
				
				WaitForOpenServiceJobsNoA();
				
				Thread.sleep(3000);
				click(btn_OpenServiceJobsA);
				Thread.sleep(5000);
				click(btn_ServiceJobArrowA.replace("CNo", CNo));
				
				
				switchWindow();
				
				Thread.sleep(3000);
				click(btn_OwnerBtnA);
				sendKeys(txt_OwnerTxtA, OwnerTxtA);
				Thread.sleep(3000);
				pressEnter(txt_OwnerTxtA);
				Thread.sleep(3000);
				doubleClick(sel_OwnerSelA);
				
				Thread.sleep(3000);
				click(btn_PricingProfileBtnA);
				sendKeys(txt_PricingProfileTxtA, PricingProfileTxtA);
				Thread.sleep(3000);
				pressEnter(txt_PricingProfileTxtA);
				Thread.sleep(3000);
				doubleClick(sel_PricingProfileSelA);
				
				Thread.sleep(3000);
				click(btn_ServiceLocationBtnA);
				sendKeys(txt_ServiceLocationTxtA, ServiceLocationTxtA);
				Thread.sleep(3000);
				pressEnter(txt_ServiceLocationTxtA);
				Thread.sleep(3000);
				doubleClick(sel_ServiceLocationSelA);
				
				Thread.sleep(3000);
				click(btn_TaskDetailsA);
				Thread.sleep(3000);
				click(btn_TaskDetailsProBtnA);
				sendKeys(txt_TaskDetailsProTxtA, ServiceProductA);
				Thread.sleep(3000);
				pressEnter(txt_TaskDetailsProTxtA);
				Thread.sleep(3000);
				doubleClick(sel_TaskDetailsProSelA);
				
				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form", "User Can draft the Service Job Order Form Fail",
							"fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that  able to create the service Job Order by convert the Case which created from Menu(Without service quotation)",
							"User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation)",
							"User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation) Successfully", "pass");
				} else {
					writeTestResults("Verify that  able to create the service Job Order by convert the Case which created from Menu(Without service quotation)",
							"User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation)", "User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation) Fail",
							"fail");
				}
				
				Thread.sleep(6000);
				clickNavigation();
				Thread.sleep(3000);
				click(btn_FormCaseA);
				
				Thread.sleep(3000);
				sendKeys(txt_CopyFromSerchtxtA, CNo);
				pressEnter(txt_CopyFromSerchtxtA);
				Thread.sleep(5000);
				doubleClick(sel_CaseSerchSelA);
			   
			   Thread.sleep(3000);
			   click(btn_ActionBtnA);
			   Thread.sleep(3000);
			   click(btn_HoldBtnA);
			   Thread.sleep(3000);
			   sendKeys(txt_HoldReasonA, "Testing");
			   
			   Thread.sleep(3000);
			   j1.executeScript("$(\"#dlgChgActionStatus\").parent().find('.dialogbox-buttonarea > .button').click();");
			   
				Thread.sleep(5000);
				if (isDisplayed(txt_PageHoldA)) {
					writeTestResults("Verify that case can be \"HOLD\"", "docuemnt status is changed to \"HOLD\"",
							"docuemnt status is changed to \"HOLD\" Successfully", "pass");
				} else {
					writeTestResults("Verify that case can be \"HOLD\"", "docuemnt status is changed to \"HOLD\"",
							"docuemnt status is changed to \"HOLD\" Fail", "fail");
				}
				
				
				Thread.sleep(3000);
				click(btn_CaseFormByPageA);
				
				Thread.sleep(3000);
				sendKeys(txt_CopyFromSerchtxtA, CNo);
				Thread.sleep(3000);
				pressEnter(txt_CopyFromSerchtxtA);
				
				Thread.sleep(5000);
				if (isDisplayed(icn_HoldIconA)) {
					writeTestResults("Verify that docuemnt status icon in the by page is chanaged when user HOLD the document process", 
							"docuemnt status icon in the by page is chanaged when user HOLD the document process",
							"docuemnt status icon in the by page is chanaged when user HOLD the document process Successfully", "pass");
				} else {
					writeTestResults("Verify that docuemnt status icon in the by page is chanaged when user HOLD the document process", 
							"docuemnt status icon in the by page is chanaged when user HOLD the document process",
							"docuemnt status icon in the by page is chanaged when user HOLD the document process Fail", "fail");
				}
		   }
		   
		   // Service_Case_037
		   public void 	VerifyThatUserCanBeUNHOLDTheCase() throws Exception {
			   
			   VerifyThatCaseCanBeHOLD();
			   
			   Thread.sleep(3000);
			   click(btn_ActionBtnA);
			   Thread.sleep(3000);
			   click(btn_UnHoldBtnA);
			   Thread.sleep(3000);
			   sendKeys(txt_HoldReasonA, "Testing");
			   
			   Thread.sleep(3000);
			   JavascriptExecutor j1 = (JavascriptExecutor) driver;
			   j1.executeScript("$(\"#dlgChgActionStatus\").parent().find('.dialogbox-buttonarea > .button').click();");
			   
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that User can be \"UNHOLD\" the case", "User can be \"UNHOLD\" the case",
							"User can be \"UNHOLD\" the case Successfully", "pass");
				} else {
					writeTestResults("Verify that User can be \"UNHOLD\" the case", "User can be \"UNHOLD\" the case",
							"User can be \"UNHOLD\" the case Fail", "fail");
				}
		   }
		   
		   // Service_Case_038
		   public void 	VerifyThatDocumentStatusIconInTheByPageIsChanagedWhenUserUNHOLDTheDocument() throws Exception {
			   
			   navigationCaseForm();
				Thread.sleep(1000);
				LocalTime obj = LocalTime.now();
				sendKeys(txt_TitleTextA, "Case" + obj);
				selectText(txt_OriginTextA, OriginTextFieldA);

				click(btn_CustomerAccountButtonA);
				sendKeys(txt_CustomerAccountTextA, CustomerAccountTextA);
				Thread.sleep(3000);
				pressEnter(txt_CustomerAccountTextA);
				Thread.sleep(3000);
				doubleClick(sel_CustomerAccountSelA);

				Thread.sleep(3000);
				click(btn_newWindowProdA);
				Thread.sleep(3000);
				click(sel_newWindowProdSelA);
				Thread.sleep(3000);
				JavascriptExecutor j1 = (JavascriptExecutor) driver;
				j1.executeScript("$(\"#divServiceSerial\").parent().find('.dialogbox-buttonarea > .button').click()");

				selectText(txt_TypeTextA, TypeTextFieldA);
				selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);
				selectText(txt_PriorityTextA, PriorityTextFieldA);

				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that able to draft the Case via main menu", "able to draft the Case via main menu",
							"able to draft the Case via main menu Successfully", "pass");
				} else {
					writeTestResults("Verify that able to draft the Case via main menu", "able to draft the Case via main menu",
							"able to draft the Case via main menu Fail", "fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String CNo = trackCode;
				
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that able to Release the Case via main menu",
							"able to Release the Case via main menu", "able to Release the Case via main menu Successfully",
							"pass");
				} else {
					writeTestResults("Verify that able to Release the Case via main menu",
							"able to Release the Case via main menu", "able to Release the Case via main menu Fail", "fail");
				}
				
				pageRefersh();
				Thread.sleep(3000);
				click(btn_ActionBtnA);
				Thread.sleep(3000);
				click(btn_DroptoServiceCentreBtnA);
				Thread.sleep(3000);
				click(btn_EntutionHeaderA);
				Thread.sleep(3000);
				click(btn_RolecenterA);
				
				WaitForOpenServiceJobsNoA();
				
				Thread.sleep(3000);
				click(btn_OpenServiceJobsA);
				Thread.sleep(5000);
				click(btn_ServiceJobArrowA.replace("CNo", CNo));
				
				
				switchWindow();
				
				Thread.sleep(3000);
				click(btn_OwnerBtnA);
				sendKeys(txt_OwnerTxtA, OwnerTxtA);
				Thread.sleep(3000);
				pressEnter(txt_OwnerTxtA);
				Thread.sleep(3000);
				doubleClick(sel_OwnerSelA);
				
				Thread.sleep(3000);
				click(btn_PricingProfileBtnA);
				sendKeys(txt_PricingProfileTxtA, PricingProfileTxtA);
				Thread.sleep(3000);
				pressEnter(txt_PricingProfileTxtA);
				Thread.sleep(3000);
				doubleClick(sel_PricingProfileSelA);
				
				Thread.sleep(3000);
				click(btn_ServiceLocationBtnA);
				sendKeys(txt_ServiceLocationTxtA, ServiceLocationTxtA);
				Thread.sleep(3000);
				pressEnter(txt_ServiceLocationTxtA);
				Thread.sleep(3000);
				doubleClick(sel_ServiceLocationSelA);
				
				Thread.sleep(3000);
				click(btn_TaskDetailsA);
				Thread.sleep(3000);
				click(btn_TaskDetailsProBtnA);
				sendKeys(txt_TaskDetailsProTxtA, ServiceProductA);
				Thread.sleep(3000);
				pressEnter(txt_TaskDetailsProTxtA);
				Thread.sleep(3000);
				doubleClick(sel_TaskDetailsProSelA);
				
				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form", "User Can draft the Service Job Order Form Fail",
							"fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that  able to create the service Job Order by convert the Case which created from Menu(Without service quotation)",
							"User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation)",
							"User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation) Successfully", "pass");
				} else {
					writeTestResults("Verify that  able to create the service Job Order by convert the Case which created from Menu(Without service quotation)",
							"User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation)", "User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation) Fail",
							"fail");
				}
				
				Thread.sleep(6000);
				clickNavigation();
				Thread.sleep(3000);
				click(btn_FormCaseA);
				
				Thread.sleep(3000);
				sendKeys(txt_CopyFromSerchtxtA, CNo);
				pressEnter(txt_CopyFromSerchtxtA);
				Thread.sleep(5000);
				doubleClick(sel_CaseSerchSelA);
			   
			   Thread.sleep(3000);
			   click(btn_ActionBtnA);
			   Thread.sleep(3000);
			   click(btn_HoldBtnA);
			   Thread.sleep(3000);
			   sendKeys(txt_HoldReasonA, "Testing");
			   
			   Thread.sleep(3000);
			   j1.executeScript("$(\"#dlgChgActionStatus\").parent().find('.dialogbox-buttonarea > .button').click();");
			   
				Thread.sleep(5000);
				if (isDisplayed(txt_PageHoldA)) {
					writeTestResults("Verify that case can be \"HOLD\"", "docuemnt status is changed to \"HOLD\"",
							"docuemnt status is changed to \"HOLD\" Successfully", "pass");
				} else {
					writeTestResults("Verify that case can be \"HOLD\"", "docuemnt status is changed to \"HOLD\"",
							"docuemnt status is changed to \"HOLD\" Fail", "fail");
				}
				
				   Thread.sleep(3000);
				   click(btn_ActionBtnA);
				   Thread.sleep(3000);
				   click(btn_UnHoldBtnA);
				   Thread.sleep(3000);
				   sendKeys(txt_HoldReasonA, "Testing");
				   
				   Thread.sleep(3000);
				   j1.executeScript("$(\"#dlgChgActionStatus\").parent().find('.dialogbox-buttonarea > .button').click();");
				   
					Thread.sleep(5000);
					if (isDisplayed(txt_PageRelease1A)) {
						writeTestResults("Verify that User can be \"UNHOLD\" the case", "User can be \"UNHOLD\" the case",
								"User can be \"UNHOLD\" the case Successfully", "pass");
					} else {
						writeTestResults("Verify that User can be \"UNHOLD\" the case", "User can be \"UNHOLD\" the case",
								"User can be \"UNHOLD\" the case Fail", "fail");
					}
					
					Thread.sleep(3000);
					click(btn_CaseFormByPageA);
					
					Thread.sleep(3000);
					sendKeys(txt_CopyFromSerchtxtA, CNo);
					Thread.sleep(3000);
					pressEnter(txt_CopyFromSerchtxtA);
					
					Thread.sleep(5000);
					if (isDisplayed(icn_UnHoldIconA)) {
						writeTestResults("Verify that docuemnt status icon in the by page is chanaged when user UNHOLD the document process", 
								"docuemnt status icon in the by page is chanaged when user UNHOLD the document process",
								"docuemnt status icon in the by page is chanaged when user UNHOLD the document process Successfully", "pass");
					} else {
						writeTestResults("Verify that docuemnt status icon in the by page is chanaged when user UNHOLD the document process", 
								"docuemnt status icon in the by page is chanaged when user UNHOLD the document process",
								"docuemnt status icon in the by page is chanaged when user UNHOLD the document process Fail", "fail");
					}
		   }
		   
		   // Service_Case_039
		   public void 	VerifyThatUserCannotBeEditFollowingFieldsInDocumentDetailsDocumentDateBookNODocumentNO() throws Exception {
			   
			   VerifyThatAbleToDraftTheCaseViaMainMenu();
			   
				Thread.sleep(3000);
				click(btn_EditA);
				Thread.sleep(3000);
				String BookNoTxt = getAttribute(txt_BookNoTxtA, "disabled");
				String DocumentDateTxtA = getAttribute(txt_DocumentDateTxtA, "disabled");
				
				if(BookNoTxt.equals("true") && DocumentDateTxtA.equals("true")&&isDisplayed(txt_DocumentNoTxtA)){
					writeTestResults("Verify that  user cannot be edit  following fields in document details Document date,Book NO,Document NO", 
							"user cannot be edit  following fields in document details Document date,Book NO,Document NO",
							"user cannot be edit  following fields in document details Document date,Book NO,Document NO Successfully", "pass");
				} else {
					writeTestResults("Verify that  user cannot be edit  following fields in document details Document date,Book NO,Document NO", 
							"user cannot be edit  following fields in document details Document date,Book NO,Document NO",
							"user cannot be edit  following fields in document details Document date,Book NO,Document NO Fail", "fail");
				}
		   }
		   
		   // Service_Case_040
		   public void VerifyResponseDetailsSubHeaderInSummaryTabIsUpdatedAccordingToTheResponcesComingFromTheActionResponseDetails() throws Exception {
			   
			   VerifyThatAbleToReleaseTheCaseCreatedViaMenu();
			   Thread.sleep(3000);
			   click(btn_ActionBtnA);
			   Thread.sleep(3000);
			   click(btn_ResponseDetailBtnA);
			   
			   Thread.sleep(3000);
			   sendKeys(txt_ResolutionDescriptionA, "Test");
			   Thread.sleep(3000);
			   selectText(txt_ResponseTypeA, "90%");
			   
			   Thread.sleep(3000);
			   click(btn_ResponseDateA);
			   Thread.sleep(2000);
			   selectText(txt_NextYearA, "2030");
			   Thread.sleep(3000);
			   doubleClick(txt_SelectDateA);
			   
			   selectText(txt_SatisfactionA, "7");
			   
			   Thread.sleep(3000);
			   click(btn_ResponseDetailsUpdateA);
			   
			   Thread.sleep(3000);
			   String StageFieldA = new Select(driver.findElement(By.xpath(txt_StageFieldA))).getFirstSelectedOption().getText();

			   Thread.sleep(5000);
			   if (StageFieldA.contentEquals("90% Completed")) {
			   writeTestResults("Verify 'Response Details' sub header in summary tab is updated according to the Responces coming from the Action ' Response Details'",
							"added response details are displayed in the case under the \"Response details - Actual\" header",
							"added response details are displayed in the case under the\"Response details - Actual\" header Successfully", "pass");
			   } else {
					writeTestResults("Verify 'Response Details' sub header in summary tab is updated according to the Responces coming from the Action ' Response Details'",
							"added response details are displayed in the case under the\"Response details - Actual\" header",
							"added response details are displayed in the case under the\"Response details - Actual\" header Fail", "fail");
			   }
		   }
		   
		   // Service_Case_041_042
		   public void VerifyThatResolutionOfaCaseCanBeViewedAsaSameResolutionInaServiceJobOrderWhichWasCreatedUsingaCase() throws Exception {
			   
			   navigationCaseForm();
				Thread.sleep(1000);
				LocalTime obj = LocalTime.now();
				sendKeys(txt_TitleTextA, "Case" + obj);
				selectText(txt_OriginTextA, OriginTextFieldA);

				click(btn_CustomerAccountButtonA);
				sendKeys(txt_CustomerAccountTextA, CustomerAccountTextA);
				Thread.sleep(3000);
				pressEnter(txt_CustomerAccountTextA);
				Thread.sleep(3000);
				doubleClick(sel_CustomerAccountSelA);

				Thread.sleep(3000);
				click(btn_newWindowProdA);
				Thread.sleep(3000);
				click(sel_newWindowProdSelA);
				Thread.sleep(3000);
				JavascriptExecutor j1 = (JavascriptExecutor) driver;
				j1.executeScript("$(\"#divServiceSerial\").parent().find('.dialogbox-buttonarea > .button').click()");

				selectText(txt_TypeTextA, TypeTextFieldA);
				selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);
				selectText(txt_PriorityTextA, PriorityTextFieldA);
				
				Thread.sleep(3000);
				click(btn_ResolutionTabA);
				sendKeys(txt_ResolutionTextA, "Test Service");

				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that able to draft the Case via main menu", "able to draft the Case via main menu",
							"able to draft the Case via main menu Successfully", "pass");
				} else {
					writeTestResults("Verify that able to draft the Case via main menu", "able to draft the Case via main menu",
							"able to draft the Case via main menu Fail", "fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String CNo = trackCode;		

				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that able to Release the Case via main menu",
							"able to Release the Case via main menu", "able to Release the Case via main menu Successfully",
							"pass");
				} else {
					writeTestResults("Verify that able to Release the Case via main menu",
							"able to Release the Case via main menu", "able to Release the Case via main menu Fail", "fail");
				}
			   
				Thread.sleep(3000);
				click(btn_ActionBtnA);
				Thread.sleep(3000);
				click(btn_DroptoServiceCentreBtnA);
				Thread.sleep(3000);
				click(btn_EntutionHeaderA);
				Thread.sleep(3000);
				click(btn_RolecenterA);
				
				WaitForOpenServiceJobsNoA();
				
				Thread.sleep(3000);
				click(btn_OpenServiceJobsA);
				Thread.sleep(5000);
				click(btn_ServiceJobArrowA.replace("CNo", CNo));
				
				
				switchWindow();
				
				Thread.sleep(3000);
				click(btn_OwnerBtnA);
				sendKeys(txt_OwnerTxtA, OwnerTxtA);
				Thread.sleep(3000);
				pressEnter(txt_OwnerTxtA);
				Thread.sleep(3000);
				doubleClick(sel_OwnerSelA);
				
				Thread.sleep(3000);
				click(btn_PricingProfileBtnA);
				sendKeys(txt_PricingProfileTxtA, PricingProfileTxtA);
				Thread.sleep(3000);
				pressEnter(txt_PricingProfileTxtA);
				Thread.sleep(3000);
				doubleClick(sel_PricingProfileSelA);
				
				Thread.sleep(3000);
				click(btn_ServiceLocationBtnA);
				sendKeys(txt_ServiceLocationTxtA, ServiceLocationTxtA);
				Thread.sleep(3000);
				pressEnter(txt_ServiceLocationTxtA);
				Thread.sleep(3000);
				doubleClick(sel_ServiceLocationSelA);
				
				Thread.sleep(3000);
				click(btn_TaskDetailsA);
				Thread.sleep(3000);
				click(btn_TaskDetailsProBtnA);
				sendKeys(txt_TaskDetailsProTxtA, ServiceProductA);
				Thread.sleep(3000);
				pressEnter(txt_TaskDetailsProTxtA);
				Thread.sleep(3000);
				doubleClick(sel_TaskDetailsProSelA);
				
				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form", "User Can draft the Service Job Order Form Fail",
							"fail");
				}
				
				Thread.sleep(3000);
				click(btn_ResolutionTabSJOA);
				
				Thread.sleep(5000);
				if (getText(txt_ResolutionTextSJOA).contentEquals("Test Service")) {
					writeTestResults("Veriy that resolution of a Case can be viewed as a same resolution in a Service Job Order which was created using a case",
							"resolution of a Case can be viewed as a same resolution in a Service Job Order which was created using a case",
							"resolution of a Case can be viewed as a same resolution in a Service Job Order which was created using a case Successfully", "pass");
				} else {
					writeTestResults("Veriy that resolution of a Case can be viewed as a same resolution in a Service Job Order which was created using a case",
							"resolution of a Case can be viewed as a same resolution in a Service Job Order which was created using a case", 
							"resolution of a Case can be viewed as a same resolution in a Service Job Order which was created using a case Fail","fail");
				}
				
				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA); 
				
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that User Can Release the Service Job Order Form",
							"User Can Release the Service Job Order Form",
							"User Can Release the Service Job Order Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can Release the Service Job Order Form",
							"User Can Release the Service Job Order Form", "User Can Release the Service Job Order Form Fail",
							"fail");
				}
				
				  Thread.sleep(3000);
				   click(btn_ActionBtnA);
				   Thread.sleep(3000);
				   click(btn_ResponseDetailBtnSJOA);
				   
				   Thread.sleep(3000);
				   sendKeys(txt_ResolutionDescriptionA, "Test-Service Job Order");
				   Thread.sleep(3000);
				   selectText(txt_ResponseTypeSJOA, "90%");
				   
				   Thread.sleep(3000);
				   click(btn_ResponseDateA);
				   Thread.sleep(2000);
				   selectText(txt_NextYearA, "2030");
				   Thread.sleep(3000);
				   doubleClick(txt_SelectDateA);
				   
				   selectText(txt_SatisfactionA, "7");
				   
				   Thread.sleep(3000);
				   click(btn_ResponseDetailsUpdateA);
				   
					Thread.sleep(6000);
					clickNavigation();
					Thread.sleep(3000);
					click(btn_FormCaseA);
					
					Thread.sleep(3000);
					sendKeys(txt_CopyFromSerchtxtA, CNo);
					pressEnter(txt_CopyFromSerchtxtA);
					Thread.sleep(5000);
					doubleClick(sel_CaseSerchSelA);
					
					Thread.sleep(3000);
					String StageFieldA = new Select(driver.findElement(By.xpath(txt_StageFieldA))).getFirstSelectedOption().getText();

					Thread.sleep(3000);
					click(btn_ResolutionTabA);
					
					Thread.sleep(5000);
					if (StageFieldA.contentEquals("90% Completed")&& getText(txt_ResolutionTextSJOA).contentEquals("Test-Service Job Order")) {
						writeTestResults("Verify that resolution changed via a service job order is displayed in the case as well",
									"resolution changed via a service job order is displayed in the case as well",
									"resolution changed via a service job order is displayed in the case as well Successfully", "pass");
					} else {
						writeTestResults("Verify that resolution changed via a service job order is displayed in the case as well",
									"resolution changed via a service job order is displayed in the case as well",
									"resolution changed via a service job order is displayed in the case as well Fail", "fail");
					}
				
		   }
		   
		   // Service_Case_043
		   public void VerifyThatSTATUSIsInOPENAfterDuplicatingAnExistingCase() throws Exception {
			   
			   VerificationOfCaseCanByDuplicatingAnExistingCase();
			   
				Thread.sleep(3000);
				click(btn_StatusFieldArrowA);
				String StatusFieldA = getText(txt_StatusFieldTxtA);

				Thread.sleep(5000);
				if (StatusFieldA.contentEquals("Open")) {
					writeTestResults("Verify that 'STATUS' is in 'OPEN' after duplicating an existing case",
							"'STATUS' is in 'OPEN' after duplicating an existing case",
							"'STATUS' is in 'OPEN' after duplicating an existing case Successfully", "pass");
				} else {
					writeTestResults("Verify that 'STATUS' is in 'OPEN' after duplicating an existing case",
							"'STATUS' is in 'OPEN' after duplicating an existing case",
							"'STATUS' is in 'OPEN' after duplicating an existing case Fail", "fail");
				}
			   
		   }
		   
		   // Service_Case_044
		   public void VerifyThatWarrentyClosureIsUpdatingWhenProductChangeInEdit() throws Exception {
			   
				navigationCaseForm();
				Thread.sleep(1000);
				LocalTime obj = LocalTime.now();
				sendKeys(txt_TitleTextA, "Case" + obj);

				selectText(txt_OriginTextA, OriginTextFieldA);

				click(btn_CustomerAccountButtonA);
				sendKeys(txt_CustomerAccountTextA, CustomerAccountTextA);
				Thread.sleep(3000);
				pressEnter(txt_CustomerAccountTextA);
				Thread.sleep(3000);
				doubleClick(sel_CustomerAccountSelA);

				Thread.sleep(3000);
				click(btn_newWindowProdA);
				Thread.sleep(3000);
				click(sel_newWindowProdSelA);
				Thread.sleep(3000);
				JavascriptExecutor j1 = (JavascriptExecutor) driver;
				j1.executeScript("$(\"#divServiceSerial\").parent().find('.dialogbox-buttonarea > .button').click()");

				selectText(txt_TypeTextA, TypeTextFieldA);

				selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);

				selectText(txt_PriorityTextA, PriorityTextFieldA);

				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
							"User Can draft the Case Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
							"User Can draft the Case Form Fail", "fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that User Can Release the Case Form", "User Can Release the Case Form",
							"User Can Release the Case Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can Release the Case Form", "User Can Release the Case Form",
							"User Can Release the Case Form Fail", "fail");
				}

				Thread.sleep(3000);
				click(btn_DuplicateA);

				Thread.sleep(3000);
				if (getText(txt_pageHeaderA).contentEquals("Case - New")) {
					writeTestResults("Verify that user can navigate to the Case form", "user can navigate to the Case form",
							"user can navigate to the Case form Successfully", "pass");
				} else {
					writeTestResults("Verify that user can navigate to the Case form", "user can navigate to the Case form",
							"user can navigate to the Case form Fail", "fail");
				}

				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
							"User Can draft the Case Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
							"User Can draft the Case Form Fail", "fail");
				}
				
				Thread.sleep(3000);
				click(btn_EditA);
				
				click(btn_CustomerAccountButtonA);
				sendKeys(txt_CustomerAccountTextA, CustomerAccountTextA);
				Thread.sleep(3000);
				pressEnter(txt_CustomerAccountTextA);
				Thread.sleep(3000);
				doubleClick(sel_CustomerAccountSelA);

				Thread.sleep(3000);
				click(btn_newWindowProdA);
				Thread.sleep(3000);
				click(sel_newWindowProdSelA);
				
				String WarrantyStart= getText(txt_WarrantyStartA);
				String WarrantyEnd= getText(txt_WarrantyEndA);
				
				Thread.sleep(3000);
				j1.executeScript("$(\"#divServiceSerial\").parent().find('.dialogbox-buttonarea > .button').click()");
				
				JavascriptExecutor jse = (JavascriptExecutor)driver;
				String WarrantyStartDate =  (String) jse.executeScript("return $(\"#txtWarryDate\").val()");
				String WarrantyEndDate =  (String) jse.executeScript("return $(\"#txtWarryEndDate\").val()");
				
				if (WarrantyStart.contentEquals(WarrantyStartDate) && WarrantyEnd.contentEquals(WarrantyEndDate)) {
					writeTestResults("Verify that warrenty closure is updating when product change in Edit", "warrenty closure is updating when product change in Edit",
							"warrenty closure is updating when product change in Edit Successfully", "pass");
				}else {
					writeTestResults("Verify that warrenty closure is updating when product change in Edit", "warrenty closure is updating when product change in Edit",
							"warrenty closure is updating when product change in Edit Fail", "fail");
				}

				Thread.sleep(3000);
				click(btn_UpdateA);
				
				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that User Can Release the duplicating an existing Case Form", "User Can Release the duplicating an existing Case Form",
							"User Can Release the duplicating an existing Case Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can Release the duplicating an existing Case Form", "User Can Release the duplicating an existing Case Form",
							"User Can Release the duplicating an existing Case Form Fail", "fail");
				}
		   }
		   
		   // Service_Case_045_046
		   public void VerifyThatServiceContractIsUpdatingWhenProductChangeInEdit() throws Exception {
			   
			   VerifyThatAbleToDraftTheCaseViaMainMenu();
			   
			   Thread.sleep(3000);
			   click(btn_EditA);
			   
			   click(btn_ServiceContractBtnA);
			   Thread.sleep(3000);
			   sendKeys(txt_ServiceContractTxtA, ServiceContractTxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ServiceContractTxtA);
			   
				Thread.sleep(3000);
				if (isDisplayed(sel_ServiceContractselA)) {
					writeTestResults("Verify that Service Contract is updating when product change in Edit", "Service Contract is updating when product change in Edit",
							"Service Contract is updating when product change in Edit Successfully", "pass");
				} else {
					writeTestResults("Verify that Service Contract is updating when product change in Edit", "Service Contract is updating when product change in Edit",
							"Service Contract is updating when product change in Edit Fail", "fail");
				}
				
			   Thread.sleep(3000);
			   doubleClick(sel_ServiceContractselA);
			   
			   JavascriptExecutor jse = (JavascriptExecutor)driver;
			   String ServiceAgreement =  (String) jse.executeScript("return $(\"#txtServAgreement\").val()");
			   
			   Thread.sleep(3000);
			   if (ServiceAgreement.contentEquals(ServiceAgreementTxtA)) {
					writeTestResults("Verify that \"Service Level Agreement \"  in the service contract is displayed in the case form", "\"Service Level Agreement \"  in the service contract is displayed in the case form",
							"\"Service Level Agreement \"  in the service contract is displayed in the case form Successfully", "pass");
			   } else {
					writeTestResults("Verify that \"Service Level Agreement \"  in the service contract is displayed in the case form", "\"Service Level Agreement \"  in the service contract is displayed in the case form",
							"\"Service Level Agreement \"  in the service contract is displayed in the case form Fail", "fail");
			   }
		   }
		   
		   // Service_Case_047
		   public void VerifyThatKnowledgeBaseIsFunctioningProperly() throws Exception {
			   
			   navigationCaseForm();
			   Thread.sleep(3000);
			   click(btn_knowledgeBaseBtnA);
			   
			   switchWindow();
			   
			   selectText(txt_InquiryTypeA, "Problem");
			   Thread.sleep(3000);
		       if (isDisplayed(txt_ProblemTxtA)) {
					writeTestResults("Verify that 'Knowledge Base' Is functioning properly for Problem Section", "'Knowledge Base' Is functioning properly for Problem Section",
							"'Knowledge Base' Is functioning properly for Problem Section Successfully", "pass");
			   } else {
					writeTestResults("Verify that 'Knowledge Base' Is functioning properly for Problem Section", "'Knowledge Base' Is functioning properly for Problem Section",
							"'Knowledge Base' Is functioning properly for Problem Section Fail", "fail");
			   }
			   
			   selectText(txt_InquiryTypeA, "Question");
			   Thread.sleep(3000);
		       if (isDisplayed(txt_QuestionTxtA)) {
					writeTestResults("Verify that 'Knowledge Base' Is functioning properly for Question Section", "'Knowledge Base' Is functioning properly for Question Section",
							"'Knowledge Base' Is functioning properly for Question Section Successfully", "pass");
			   } else {
					writeTestResults("Verify that 'Knowledge Base' Is functioning properly for Question Section", "'Knowledge Base' Is functioning properly for Question Section",
							"'Knowledge Base' Is functioning properly for Question Section Fail", "fail");
			   }
			   
			   selectText(txt_InquiryTypeA, "Request");
			   Thread.sleep(3000);
		       if (isDisplayed(txt_RequestTxtA)) {
					writeTestResults("Verify that 'Knowledge Base' Is functioning properly for Request Section", "'Knowledge Base' Is functioning properly for Request Section",
							"'Knowledge Base' Is functioning properly for Request Section Successfully", "pass");
			   } else {
					writeTestResults("Verify that 'Knowledge Base' Is functioning properly for Request Section", "'Knowledge Base' Is functioning properly for Request Section",
							"'Knowledge Base' Is functioning properly for Request Section Fail", "fail");
			   }
		   }
		   
		   // Service_Case_048_049
		   public void VerifyThatDocumentStatusIconInTheByPageIsChanagedWhenUserDRAFTTheDocument() throws Exception {
			   
			    navigationCaseForm();
				Thread.sleep(1000);
				LocalTime obj = LocalTime.now();
				sendKeys(txt_TitleTextA, "Case" + obj);
				selectText(txt_OriginTextA, OriginTextFieldA);

				click(btn_CustomerAccountButtonA);
				sendKeys(txt_CustomerAccountTextA, CustomerAccountTextA);
				Thread.sleep(3000);
				pressEnter(txt_CustomerAccountTextA);
				Thread.sleep(3000);
				doubleClick(sel_CustomerAccountSelA);

				Thread.sleep(3000);
				click(btn_newWindowProdA);
				Thread.sleep(3000);
				click(sel_newWindowProdSelA);
				Thread.sleep(3000);
				JavascriptExecutor j1 = (JavascriptExecutor) driver;
				j1.executeScript("$(\"#divServiceSerial\").parent().find('.dialogbox-buttonarea > .button').click()");

				selectText(txt_TypeTextA, TypeTextFieldA);
				selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);
				selectText(txt_PriorityTextA, PriorityTextFieldA);

				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String CNo = trackCode;
				
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that able to draft the Case via main menu", "able to draft the Case via main menu",
							"able to draft the Case via main menu Successfully", "pass");
				} else {
					writeTestResults("Verify that able to draft the Case via main menu", "able to draft the Case via main menu",
							"able to draft the Case via main menu Fail", "fail");
				}
				
				Thread.sleep(6000);
				clickNavigation();
				Thread.sleep(3000);
				click(btn_FormCaseA);
				
				WaitElement(txt_CopyFromSerchtxtA);
				sendKeys(txt_CopyFromSerchtxtA, CNo);
				pressEnter(txt_CopyFromSerchtxtA);

				Thread.sleep(5000);
				if (isDisplayed(icn_DraftIconA)) {
					writeTestResults("Verify that document status icon in the by page is chanaged when user \"DRAFT\" the document ",
							"document status icon in the by page is chanaged when user \"DRAFT\" the document ",
							"document status icon in the by page is chanaged when user \"DRAFT\" the document  Successfully", "pass");
				} else {
					writeTestResults("Verify that document status icon in the by page is chanaged when user \"DRAFT\" the document ",
							"document status icon in the by page is chanaged when user \"DRAFT\" the document ",
							"document status icon in the by page is chanaged when user \"DRAFT\" the document  Fail", "fail");
				}
				
				WaitElement(sel_CaseSerchSelNewA.replace("CaseTitle", "Case" + obj));
				doubleClick(sel_CaseSerchSelNewA.replace("CaseTitle", "Case" + obj));
				
				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that able to Release the Case via main menu",
							"able to Release the Case via main menu", "able to Release the Case via main menu Successfully",
							"pass");
				} else {
					writeTestResults("Verify that able to Release the Case via main menu",
							"able to Release the Case via main menu", "able to Release the Case via main menu Fail", "fail");
				}
				
				Thread.sleep(6000);
				clickNavigation();
				Thread.sleep(3000);
				click(btn_FormCaseA);
				
				Thread.sleep(3000);
				sendKeys(txt_CopyFromSerchtxtA, CNo);
				Thread.sleep(3000);
				pressEnter(txt_CopyFromSerchtxtA);
				
				Thread.sleep(5000);
				if (isDisplayed(icn_ReleasedIconA)) {
					writeTestResults("Verify that document status icon in the by page is chanaged when user \"Released\" the document ",
							"document status icon in the by page is chanaged when user \"Released\" the document ",
							"document status icon in the by page is chanaged when user \"Released\" the document  Successfully", "pass");
				} else {
					writeTestResults("Verify that document status icon in the by page is chanaged when user \"Released\" the document ",
							"document status icon in the by page is chanaged when user \"Released\" the document ",
							"document status icon in the by page is chanaged when user \"Released\" the document  Fail", "fail");
				}
		   }
		   
		   // Service_Case_050
		   public void VerifyThatDocumentStatusIconInTheByPageIsChanagedWhenUserReverseTheDocument() throws Exception {
			   
				Thread.sleep(3000);
				click(btn_RolecenterA);

				Thread.sleep(3000);
				click(btn_searchTagA);
				sendKeys(txt_searchTagtxtA, "s1.1");
				Thread.sleep(3000);
				pressEnter(txt_searchTagtxtA);
				Thread.sleep(3000);
				click(btn_searchTagarrowA);

				switchWindow();

				Thread.sleep(3000);
				LocalTime obj = LocalTime.now();
				sendKeys(txt_TitleTextA, "Case" + obj);
				selectText(txt_OriginTextA, OriginTextFieldA);

				selectText(txt_TypeTextA, TypeTextFieldA);
				selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);
				selectText(txt_PriorityTextA, PriorityTextFieldA);

				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
							"User Can draft the Case Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
							"User Can draft the Case Form Fail", "fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String CNo = trackCode;
				
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that User able to release a case Via Role center",
							"User able to release a case Via Role center",
							"User able to release a case Via Role center Successfully", "pass");
				} else {
					writeTestResults("Verify that User able to release a case Via Role center",
							"User able to release a case Via Role center", "User able to release a case Via Role center Fail",
							"fail");
				}

				Thread.sleep(3000);
				click(btn_ActionBtnA);
				Thread.sleep(3000);
				click(btn_ReverseBtnA);
				Thread.sleep(3000);
				JavascriptExecutor j1 = (JavascriptExecutor) driver;
				j1.executeScript("$('#divConfirmAction').nextAll().find('.pic16-apply').click()");

				Thread.sleep(5000);
				if (isDisplayed(txt_pageReversedA)) {
					writeTestResults("Verify that able to reverse the document", "User Can reverse the Case Form",
							"User Can reverse the Case Form Successfully", "pass");
				} else {
					writeTestResults("Verify that able to reverse the document", "User Can reverse the Case Form",
							"User Can reverse the Case Form Fail", "fail");
				}
				
				Thread.sleep(6000);
				clickNavigation();
				Thread.sleep(3000);
				click(btn_FormCaseA);
				
				Thread.sleep(3000);
				sendKeys(txt_CopyFromSerchtxtA, CNo);
				Thread.sleep(3000);
				pressEnter(txt_CopyFromSerchtxtA);
				
				Thread.sleep(5000);
				if (isDisplayed(icn_ReverseIconA)) {
					writeTestResults("Verify that document status icon in the by page is chanaged when user \"Reverse\" the document ",
							"document status icon in the by page is chanaged when user \"Reverse\" the document ",
							"document status icon in the by page is chanaged when user \"Reverse\" the document  Successfully", "pass");
				} else {
					writeTestResults("Verify that document status icon in the by page is chanaged when user \"Reverse\" the document ",
							"document status icon in the by page is chanaged when user \"Reverse\" the document ",
							"document status icon in the by page is chanaged when user \"Reverse\" the document  Fail", "fail");
				}
		   }
		   
		   // Service_Case_051
		   public void VerifyThatDocumentStatusIconInTheByPageIsChanagedWhenUserDeleteTheDocument() throws Exception {
			   
			   navigationCaseForm();
				Thread.sleep(1000);
				LocalTime obj = LocalTime.now();
				sendKeys(txt_TitleTextA, "Case" + obj);

				selectText(txt_OriginTextA, OriginTextFieldA);

				click(btn_CustomerAccountButtonA);
				sendKeys(txt_CustomerAccountTextA, CustomerAccountTextA);
				Thread.sleep(3000);
				pressEnter(txt_CustomerAccountTextA);
				Thread.sleep(3000);
				doubleClick(sel_CustomerAccountSelA);

				Thread.sleep(3000);
				click(btn_newWindowProdA);
				Thread.sleep(3000);
				click(sel_newWindowProdSelA);
				Thread.sleep(3000);
				JavascriptExecutor j1 = (JavascriptExecutor) driver;
				j1.executeScript("$(\"#divServiceSerial\").parent().find('.dialogbox-buttonarea > .button').click()");

				selectText(txt_TypeTextA, TypeTextFieldA);

				selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);

				selectText(txt_PriorityTextA, PriorityTextFieldA);

				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String CNo = trackCode;
				
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
							"User Can draft the Case Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Case Form", "User Can draft the Case Form",
							"User Can draft the Case Form Fail", "fail");
				}

				Thread.sleep(3000);
				click(btn_ActionBtnA);
				Thread.sleep(3000);
				click(btn_DeleteBtnA);
				Thread.sleep(3000);
				j1.executeScript("$('#divConfirmAction').nextAll().find('.pic16-apply').click()");

				Thread.sleep(5000);
				if (isDisplayed(txt_pageDeletedA)) {
					writeTestResults("Verify that able to delete the Draft case", "User Can delete the Draft Case Form",
							"User Can delete the Draft Case Form Successfully", "pass");
				} else {
					writeTestResults("Verify that able to delete the Draft case", "User Can delete the Draft Case Form",
							"User Can delete the Draft Case Form Fail", "fail");
				}
				
				Thread.sleep(6000);
				clickNavigation();
				Thread.sleep(3000);
				click(btn_FormCaseA);
				
				Thread.sleep(3000);
				sendKeys(txt_CopyFromSerchtxtA, CNo);
				Thread.sleep(3000);
				pressEnter(txt_CopyFromSerchtxtA);
				
				Thread.sleep(5000);
				if (isDisplayed(icn_DeleteIconA)) {
					writeTestResults("Verify that document status icon in the by page is chanaged when user \"Delete\" the document ",
							"document status icon in the by page is chanaged when user \"Delete\" the document ",
							"document status icon in the by page is chanaged when user \"Delete\" the document  Successfully", "pass");
				} else {
					writeTestResults("Verify that document status icon in the by page is chanaged when user \"Delete\" the document ",
							"document status icon in the by page is chanaged when user \"Delete\" the document ",
							"document status icon in the by page is chanaged when user \"Delete\" the document  Fail", "fail");
				}
		   }
		   
		   // Service_Case_052_053
		   public void VerifyThatAbleToEnterNewOriginParameterViaCaseForm() throws Exception {
			   
			   navigationCaseForm();
			   
			   Thread.sleep(3000);
			   click(btn_OriginAddBtnA);
			   Thread.sleep(3000);
			   click(btn_OriginAddPlusBtnA);
			   
			   LocalTime Obj1 = LocalTime.now();
			   sendKeys(txt_OriginAddTextA, "OriginTest"+Obj1);
			   
			   Thread.sleep(3000);
			   JavascriptExecutor j1 = (JavascriptExecutor) driver;
			   j1.executeScript("$(\"#divPara\").parent().find('.dialogbox-buttonarea > .button').click()");
			   
			   pageRefersh();
			   
			   selectText(txt_OriginTextA, "OriginTest"+Obj1);
			   
				Thread.sleep(3000);
				String selectedOption1 = new Select(driver.findElement(By.xpath(txt_OriginTextA))).getFirstSelectedOption().getText();
				
				Thread.sleep(5000);
				if (selectedOption1.contentEquals("OriginTest"+Obj1)) {
					writeTestResults("Verify that able to enter new origin parameter via case form",
							"User Can enter new origin parameter via case form",
							"User Can enter new origin parameter via case form Successfully", "pass");
				} else {
					writeTestResults("Verify that able to enter new origin parameter via case form",
							"User Can enter new origin parameter via case form",
							"User Can enter new origin parameter via case form Fail", "fail");
				}
				
				Thread.sleep(3000);
				click(btn_OriginAddBtnA);
				Thread.sleep(3000);
				click(btn_OriginInactiveA);
				
				Thread.sleep(3000);
				j1.executeScript("$(\"#divPara\").parent().find('.dialogbox-buttonarea > .button').click()");
				
				pageRefersh();
				   
				selectText(txt_OriginTextA, "OriginTest"+Obj1);
				
				Thread.sleep(3000);
				String selectedOption2 = new Select(driver.findElement(By.xpath(txt_OriginTextA))).getFirstSelectedOption().getText();
				
				Thread.sleep(5000);
				if (selectedOption2.contentEquals("--None--")) {
					writeTestResults("Verify that inactivated origin parameters are not displayed in the list",
							"inactivated origin parameters are not displayed in the list",
							"inactivated origin parameters are not displayed in the list Successfully", "pass");
				} else {
					writeTestResults("Verify that inactivated origin parameters are not displayed in the list",
							"inactivated origin parameters are not displayed in the list",
							"inactivated origin parameters are not displayed in the list Fail", "fail");
				}
		   }
		   
		   // Service_Case_054
		   public void VerifyThatExistingSerialsForaCustomerIsDisplayedInTheSerialNosLookUp() throws Exception {
			   
			    navigationCaseForm();
				Thread.sleep(1000);
				LocalTime obj = LocalTime.now();
				sendKeys(txt_TitleTextA, "Case" + obj);
				selectText(txt_OriginTextA, OriginTextFieldA);

				click(btn_CustomerAccountButtonA);
				sendKeys(txt_CustomerAccountTextA, CustomerAccountTextA);
				Thread.sleep(3000);
				pressEnter(txt_CustomerAccountTextA);
				Thread.sleep(3000);
				doubleClick(sel_CustomerAccountSelA);

				Thread.sleep(3000);
				click(btn_newWindowProdA);
				
		          List<WebElement> links = driver.findElements(By.xpath(sel_SerialNosSelA));
		          
		          int linkCount = links.size();
		          
		          System.out.println("Total Number of link count on webpage = "  + linkCount);
				
				Thread.sleep(5000);
				if (isDisplayed(sel_SerialNosSelA) && linkCount==10) {
					writeTestResults("Verify that existing serials for a customer is displayed in the \"Serial Nos\" look up",
							"existing serials for a customer is displayed in the \"Serial Nos\" look up",
							"existing serials for a customer is displayed in the \"Serial Nos\" look up Successfully", "pass");
				} else {
					writeTestResults("Verify that existing serials for a customer is displayed in the \"Serial Nos\" look up",
							"existing serials for a customer is displayed in the \"Serial Nos\" look up",
							"existing serials for a customer is displayed in the \"Serial Nos\" look up Fail", "fail");
				}
		   }
		   
		   // Service_Case_055
		   public void VerifyThatParentCasesForaSpecificSerialIsDisplayedInTheParenetCaseLokUp() throws Exception {
			   
				navigationCaseForm();
				Thread.sleep(1000);
				LocalTime obj = LocalTime.now();
				sendKeys(txt_TitleTextA, "Case" + obj);
				selectText(txt_OriginTextA, OriginTextFieldA);

				click(btn_CustomerAccountButtonA);
				sendKeys(txt_CustomerAccountTextA, CustomerAccountTextA);
				Thread.sleep(3000);
				pressEnter(txt_CustomerAccountTextA);
				Thread.sleep(3000);
				doubleClick(sel_CustomerAccountSelA);

				Thread.sleep(3000);
				click(btn_newWindowProdA);
				Thread.sleep(3000);
				click(sel_newWindowProdSelA);
				Thread.sleep(3000);
				JavascriptExecutor j1 = (JavascriptExecutor) driver;
				j1.executeScript("$(\"#divServiceSerial\").parent().find('.dialogbox-buttonarea > .button').click()");

				selectText(txt_TypeTextA, TypeTextFieldA);
				selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);
				selectText(txt_PriorityTextA, PriorityTextFieldA);

				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that able to draft the Case via main menu", "able to draft the Case via main menu",
							"able to draft the Case via main menu Successfully", "pass");
				} else {
					writeTestResults("Verify that able to draft the Case via main menu", "able to draft the Case via main menu",
							"able to draft the Case via main menu Fail", "fail");
				}
				
				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String CNo = trackCode;
				
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that able to Release the Case via main menu",
							"able to Release the Case via main menu", "able to Release the Case via main menu Successfully",
							"pass");
				} else {
					writeTestResults("Verify that able to Release the Case via main menu",
							"able to Release the Case via main menu", "able to Release the Case via main menu Fail", "fail");
				}
				
				Thread.sleep(3000);
				click(btn_AddNewCasePlusA);
				
				switchWindow();
				
				click(btn_CustomerAccountButtonA);
				sendKeys(txt_CustomerAccountTextA, CustomerAccountTextA);
				Thread.sleep(3000);
				pressEnter(txt_CustomerAccountTextA);
				Thread.sleep(3000);
				doubleClick(sel_CustomerAccountSelA);

				Thread.sleep(3000);
				click(btn_newWindowProdA);
				Thread.sleep(3000);
				click(sel_newWindowProdSelA);
				Thread.sleep(3000);
				j1.executeScript("$(\"#divServiceSerial\").parent().find('.dialogbox-buttonarea > .button').click()");
				
				Thread.sleep(3000);
				click(btn_ParentCaseSerchBtnA);
				sendKeys(txt_ParentCaseSerchTxtA, CNo);
				Thread.sleep(3000);
				pressEnter(txt_ParentCaseSerchTxtA);
				
				Thread.sleep(5000);
				if (isDisplayed(sel_ParentCaseSerchSelA.replace("CNo", "Case" + obj))) {
					writeTestResults("Verify that Parent cases for a specific serial is displayed in the \"Parenet Case\" lok up",
							"Parent cases for a specific serial is displayed in the \"Parenet Case\" lok up", "Parent cases for a specific serial is displayed in the \"Parenet Case\" lok up Successfully",
							"pass");
				} else {
					writeTestResults("Verify that Parent cases for a specific serial is displayed in the \"Parenet Case\" lok up",
							"Parent cases for a specific serial is displayed in the \"Parenet Case\" lok up", "Parent cases for a specific serial is displayed in the \"Parenet Case\" lok up Fail", "fail");
				}
		   }
		   
		   // Service_Case_056_057
		   public void VerifyThatAbleToAddTheNewInquiryTypeParameters() throws Exception {
			   
			   navigationCaseForm();
			   
			   Thread.sleep(3000);
			   click(btn_TypeAddBtnA);
			   Thread.sleep(3000);
			   click(btn_TypeAddPlusA);
			   
			   LocalTime Obj1 = LocalTime.now();
			   sendKeys(txt_TypeAddTextA, "TypeTest"+Obj1);
			   
			   Thread.sleep(3000);
			   JavascriptExecutor j1 = (JavascriptExecutor) driver;
			   j1.executeScript("$(\"#divPara\").parent().find('.dialogbox-buttonarea > .button').click()");
			   
			   pageRefersh();
			   
			   selectText(txt_TypeTextA, "TypeTest"+Obj1);
			   
				Thread.sleep(3000);
				String selectedOption1 = new Select(driver.findElement(By.xpath(txt_TypeTextA))).getFirstSelectedOption().getText();
				
				Thread.sleep(5000);
				if (selectedOption1.contentEquals("TypeTest"+Obj1)) {
					writeTestResults("Verify that able to add the new \"Inquiry Type\" Parameters",
							"User Can add the new \"Inquiry Type\" Parameters",
							"User Can add the new \"Inquiry Type\" Parameters Successfully", "pass");
				} else {
					writeTestResults("Verify that able to add the new \"Inquiry Type\" Parameters",
							"User Can add the new \"Inquiry Type\" Parameters",
							"User Can add the new \"Inquiry Type\" Parameters Fail", "fail");
				}
				
				Thread.sleep(3000);
				click(btn_TypeAddBtnA);
				Thread.sleep(3000);
				click(btn_TypeInactiveA);
				
				Thread.sleep(3000);
				j1.executeScript("$(\"#divPara\").parent().find('.dialogbox-buttonarea > .button').click()");
				
				pageRefersh();
				   
				selectText(txt_TypeTextA, "TypeTest"+Obj1);
				
				Thread.sleep(3000);
				String selectedOption2 = new Select(driver.findElement(By.xpath(txt_TypeTextA))).getFirstSelectedOption().getText();
				
				Thread.sleep(5000);
				if (selectedOption2.contentEquals("--None--")) {
					writeTestResults("Verify that inactivated inquiry type parameters are not displayed in the list",
							"inactivated inquiry type parameters are not displayed in the list",
							"inactivated inquiry type parameters are not displayed in the list Successfully", "pass");
				} else {
					writeTestResults("Verify that inactivated inquiry type parameters are not displayed in the list",
							"inactivated inquiry type parameters are not displayed in the list",
							"inactivated inquiry type parameters are not displayed in the list Fail", "fail");
				}
		   }
		   
		   // Service_Case_058_059
		   public void VerifyThatAbleToAddTheNewServiceGroupParameters() throws Exception {
			   
			   navigationCaseForm();
			   
			   Thread.sleep(3000);
			   click(btn_ServiceGroupAddBtnA);
			   Thread.sleep(3000);
			   click(btn_ServiceGroupAddPlusA);
			   
			   LocalTime Obj1 = LocalTime.now();
			   sendKeys(txt_ServiceGroupAddTextA, "ServiceGroup"+Obj1);
			   
			   Thread.sleep(3000);
			   JavascriptExecutor j1 = (JavascriptExecutor) driver;
			   j1.executeScript("$(\"#divPara\").parent().find('.dialogbox-buttonarea > .button').click()");
			   
			   pageRefersh();
			   
			   selectText(txt_ServiceGroupTextA, "ServiceGroup"+Obj1);
			   
				Thread.sleep(3000);
				String selectedOption1 = new Select(driver.findElement(By.xpath(txt_ServiceGroupTextA))).getFirstSelectedOption().getText();
				
				Thread.sleep(5000);
				if (selectedOption1.contentEquals("ServiceGroup"+Obj1)) {
					writeTestResults("Verify that able to add the new \"Service Group\" Parameters",
							"User can add the new \"Service Group\" Parameters",
							"User can add the new \"Service Group\" Parameters Successfully", "pass");
				} else {
					writeTestResults("Verify that able to add the new \"Service Group\" Parameters",
							"User can add the new \"Service Group\" Parameters",
							"User can add the new \"Service Group\" Parameters Fail", "fail");
				}
				
				Thread.sleep(3000);
				click(btn_ServiceGroupAddBtnA);
				Thread.sleep(3000);
				click(btn_ServiceGroupInactiveA);
				
				Thread.sleep(3000);
				j1.executeScript("$(\"#divPara\").parent().find('.dialogbox-buttonarea > .button').click()");
				
				pageRefersh();
				   
				selectText(txt_ServiceGroupTextA, "ServiceGroup"+Obj1);
				
				Thread.sleep(3000);
				String selectedOption2 = new Select(driver.findElement(By.xpath(txt_ServiceGroupTextA))).getFirstSelectedOption().getText();
				
				Thread.sleep(5000);
				if (selectedOption2.contentEquals("--None--")) {
					writeTestResults("Verify that inactivated service group parameters are not displayed in the list",
							"inactivated service group parameters are not displayed in the list",
							"inactivated service group parameters are not displayed in the list Successfully", "pass");
				} else {
					writeTestResults("Verify that inactivated service group parameters are not displayed in the list",
							"inactivated service group parameters are not displayed in the list",
							"inactivated service group parameters are not displayed in the list Fail", "fail");
				}
		   }
		   
		   // Service_Case_060
		   public void VerifyThatCustomerAccountIsDisplayedAccordingToTheBalancingLevelSettingCodeAndDescription() throws Exception {
			   
				Thread.sleep(3000);
				click(btn_AdministrationBtnA);
				Thread.sleep(3000);
				click(btn_BalancingLevelSettingsBtnA);
				Thread.sleep(3000);
				click(btn_SalesAndMarketingBtnA);
				
				selectText(txt_cboxAccountCodeNameA, "Code and Description");
				Thread.sleep(3000);
				click(btn_SalesAndMarketingUpdateBtnA);
				
				pageRefersh();
				
				Thread.sleep(6000);
				clickNavigation();
				navigationCaseForm();

				click(btn_CustomerAccountButtonA);
				sendKeys(txt_CustomerAccountTextA, CustomerAccountCodeAndDicriptionA);
				Thread.sleep(3000);
				pressEnter(txt_CustomerAccountTextA);
				Thread.sleep(3000);
				doubleClick(sel_CustomerAccountSelA);
				
				JavascriptExecutor jse = (JavascriptExecutor)driver;
				String CustomerAccount =  (String) jse.executeScript("return $(\"#txtCusAcc\").val()");

				Thread.sleep(5000);
				if (CustomerAccount.contentEquals(CustomerAccountCodeAndDicriptionA)) {
					writeTestResults("Verify that \"Customer Account\" is displayed according to the balancing level setting \"Code and Description\" ",
							"\"Customer Account\" is displayed according to the balancing level setting \"Code and Description\"",
							"\"Customer Account\" is displayed according to the balancing level setting \"Code and Description\" Successfully", "pass");
				} else {
					writeTestResults("Verify that \"Customer Account\" is displayed according to the balancing level setting \"Code and Description\" ",
							"\"Customer Account\" is displayed according to the balancing level setting \"Code and Description\"",
							"\"Customer Account\" is displayed according to the balancing level setting \"Code and Description\" Fail", "fail");
				}
				
				Thread.sleep(6000);
				clickNavigation();
				Thread.sleep(3000);
				click(btn_AdministrationBtnA);
				Thread.sleep(3000);
				click(btn_BalancingLevelSettingsBtnA);
				Thread.sleep(3000);
				click(btn_SalesAndMarketingBtnA);
				
				selectText(txt_cboxAccountCodeNameA, "Code and Description");
				Thread.sleep(3000);
				click(btn_SalesAndMarketingUpdateBtnA);
		   }
		   
		   
		   // Service_Case_061
		   public void VerifyThatCustomerAccountIsDisplayedAccordingToTheBalancingLevelSettingCodeOnly() throws Exception {
			   
				Thread.sleep(3000);
				click(btn_AdministrationBtnA);
				Thread.sleep(3000);
				click(btn_BalancingLevelSettingsBtnA);
				Thread.sleep(3000);
				click(btn_SalesAndMarketingBtnA);
				
				selectText(txt_cboxAccountCodeNameA, "Code Only");
				Thread.sleep(3000);
				click(btn_SalesAndMarketingUpdateBtnA);
				
				pageRefersh();
				
				Thread.sleep(6000);
				clickNavigation();
				navigationCaseForm();

				click(btn_CustomerAccountButtonA);
				sendKeys(txt_CustomerAccountTextA, CustomerAccountCodeA);
				Thread.sleep(3000);
				pressEnter(txt_CustomerAccountTextA);
				Thread.sleep(3000);
				doubleClick(sel_CustomerAccountSelCodeA);
				
				JavascriptExecutor jse = (JavascriptExecutor)driver;
				String CustomerAccount =  (String) jse.executeScript("return $(\"#txtCusAcc\").val()");

				Thread.sleep(5000);
				if (CustomerAccount.contentEquals(CustomerAccountCodeA)) {
					writeTestResults("Verify that \"Customer Account\" is displayed according to the balancing level setting \"Code only\" ",
							"\"Customer Account\" is displayed according to the balancing level setting \"Code only\"",
							"\"Customer Account\" is displayed according to the balancing level setting \"Code only\" Successfully", "pass");
				} else {
					writeTestResults("Verify that \"Customer Account\" is displayed according to the balancing level setting \"Code only\" ",
							"\"Customer Account\" is displayed according to the balancing level setting \"Code only\"",
							"\"Customer Account\" is displayed according to the balancing level setting \"Code only\" Fail", "fail");
				}
				
				Thread.sleep(6000);
				clickNavigation();
				Thread.sleep(3000);
				click(btn_AdministrationBtnA);
				Thread.sleep(3000);
				click(btn_BalancingLevelSettingsBtnA);
				Thread.sleep(3000);
				click(btn_SalesAndMarketingBtnA);
				
				selectText(txt_cboxAccountCodeNameA, "Code and Description");
				Thread.sleep(3000);
				click(btn_SalesAndMarketingUpdateBtnA);
		   }
		   
		   // Service_Case_062
		   public void VerifyThatCustomerAccountIsDisplayedAccordingToTheBalancingLevelSettingNameOnly() throws Exception {
			   
				Thread.sleep(3000);
				click(btn_AdministrationBtnA);
				Thread.sleep(3000);
				click(btn_BalancingLevelSettingsBtnA);
				Thread.sleep(3000);
				click(btn_SalesAndMarketingBtnA);
				
				selectText(txt_cboxAccountCodeNameA, "Name Only");
				Thread.sleep(3000);
				click(btn_SalesAndMarketingUpdateBtnA);
				
				pageRefersh();
				
				Thread.sleep(6000);
				clickNavigation();
				navigationCaseForm();

				click(btn_CustomerAccountButtonA);
				sendKeys(txt_CustomerAccountTextA, CustomerAccountDicriptionA);
				Thread.sleep(3000);
				pressEnter(txt_CustomerAccountTextA);
				Thread.sleep(3000);
				doubleClick(sel_CustomerAccountSelNameA);
				
				JavascriptExecutor jse = (JavascriptExecutor)driver;
				String CustomerAccount =  (String) jse.executeScript("return $(\"#txtCusAcc\").val()");

				Thread.sleep(5000);
				if (CustomerAccount.contentEquals(CustomerAccountDicriptionA)) {
					writeTestResults("Verify that \"Customer Account\" is displayed according to the balancing level setting \"Name only\" ",
							"\"Customer Account\" is displayed according to the balancing level setting \"Name only\"",
							"\"Customer Account\" is displayed according to the balancing level setting \"Name only\" Successfully", "pass");
				} else {
					writeTestResults("Verify that \"Customer Account\" is displayed according to the balancing level setting \"Name only\" ",
							"\"Customer Account\" is displayed according to the balancing level setting \"Name only\"",
							"\"Customer Account\" is displayed according to the balancing level setting \"Name only\" Fail", "fail");
				}
				
				Thread.sleep(6000);
				clickNavigation();
				Thread.sleep(3000);
				click(btn_AdministrationBtnA);
				Thread.sleep(3000);
				click(btn_BalancingLevelSettingsBtnA);
				Thread.sleep(3000);
				click(btn_SalesAndMarketingBtnA);
				
				selectText(txt_cboxAccountCodeNameA, "Code and Description");
				Thread.sleep(3000);
				click(btn_SalesAndMarketingUpdateBtnA);
		   }
		   
		   // Service_Case_063
		   public void VerifyThatProductIsDisplayedAccordingToTheBalancingLevelSettingCodeAndDescription() throws Exception {
			   
			    Thread.sleep(3000);
				click(btn_AdministrationBtnA);
				Thread.sleep(3000);
				click(btn_BalancingLevelSettingsBtnA);
				Thread.sleep(3000);
				click(btn_InventoryBtnA);
				
				selectText(txt_cboxProductCodeNameA, "Code and Description");
				Thread.sleep(3000);
				click(btn_InventoryUpdateBtnA);
				
				pageRefersh();
				
				Thread.sleep(6000);
				clickNavigation();
				navigationCaseForm();

				click(btn_CustomerAccountButtonA);
				sendKeys(txt_CustomerAccountTextA, CustomerAccountCodeAndDicriptionA);
				Thread.sleep(3000);
				pressEnter(txt_CustomerAccountTextA);
				Thread.sleep(3000);
				doubleClick(sel_CustomerAccountSelA);
				
				Thread.sleep(3000);
				click(btn_newWindowProdA);
				Thread.sleep(3000);
				click(sel_newWindowProdSelA);
				Thread.sleep(3000);
				JavascriptExecutor j1 = (JavascriptExecutor) driver;
				j1.executeScript("$(\"#divServiceSerial\").parent().find('.dialogbox-buttonarea > .button').click()");
				
				JavascriptExecutor jse = (JavascriptExecutor)driver;
				String Product =  (String) jse.executeScript("return $(\"#txtProduct\").val()");
				
				Thread.sleep(5000);
				if (Product.contentEquals(ProductCodeAndDicriptionA)) {
					writeTestResults("Verify that \"Product\" is displayed according to the balancing level setting \"Code and Description\" ",
							"\"Product\" is displayed according to the balancing level setting \"Code and Description\"",
							"\"Product\" is displayed according to the balancing level setting \"Code and Description\" Successfully", "pass");
				} else {
					writeTestResults("Verify that \"Product\" is displayed according to the balancing level setting \"Code and Description\" ",
							"\"Product\" is displayed according to the balancing level setting \"Code and Description\"",
							"\"Product\" is displayed according to the balancing level setting \"Code and Description\" Fail", "fail");
				}
				
				Thread.sleep(6000);
				clickNavigation();
			    Thread.sleep(3000);
				click(btn_AdministrationBtnA);
				Thread.sleep(3000);
				click(btn_BalancingLevelSettingsBtnA);
				Thread.sleep(3000);
				click(btn_InventoryBtnA);
				
				selectText(txt_cboxProductCodeNameA, "Code and Description");
				Thread.sleep(3000);
				click(btn_InventoryUpdateBtnA);
		   }
		   
		   // Service_Case_064
		   public void VerifyThatProductIsDisplayedAccordingToTheBalancingLevelSettingCodeOnly() throws Exception {
			   
			    Thread.sleep(3000);
				click(btn_AdministrationBtnA);
				Thread.sleep(3000);
				click(btn_BalancingLevelSettingsBtnA);
				Thread.sleep(3000);
				click(btn_InventoryBtnA);
				
				selectText(txt_cboxProductCodeNameA, "Code Only");
				Thread.sleep(3000);
				click(btn_InventoryUpdateBtnA);
				
				pageRefersh();
				
				Thread.sleep(6000);
				clickNavigation();
				navigationCaseForm();

				click(btn_CustomerAccountButtonA);
				sendKeys(txt_CustomerAccountTextA, CustomerAccountCodeAndDicriptionA);
				Thread.sleep(3000);
				pressEnter(txt_CustomerAccountTextA);
				Thread.sleep(3000);
				doubleClick(sel_CustomerAccountSelA);
				
				Thread.sleep(3000);
				click(btn_newWindowProdA);
				Thread.sleep(3000);
				click(sel_newWindowProdSelA);
				Thread.sleep(3000);
				JavascriptExecutor j1 = (JavascriptExecutor) driver;
				j1.executeScript("$(\"#divServiceSerial\").parent().find('.dialogbox-buttonarea > .button').click()");
				
				JavascriptExecutor jse = (JavascriptExecutor)driver;
				String Product =  (String) jse.executeScript("return $(\"#txtProduct\").val()");
				
				Thread.sleep(5000);
				if (Product.contentEquals(ProductCodeA)) {
					writeTestResults("Verify that \"Product\" is displayed according to the balancing level setting \"Code only\" ",
							"\"Product\" is displayed according to the balancing level setting \"Code only\"",
							"\"Product\" is displayed according to the balancing level setting \"Code only\" Successfully", "pass");
				} else {
					writeTestResults("Verify that \"Product\" is displayed according to the balancing level setting \"Code only\" ",
							"\"Product\" is displayed according to the balancing level setting \"Code only\"",
							"\"Product\" is displayed according to the balancing level setting \"Code only\" Fail", "fail");
				}
				
				
				Thread.sleep(6000);
				clickNavigation();
			    Thread.sleep(3000);
				click(btn_AdministrationBtnA);
				Thread.sleep(3000);
				click(btn_BalancingLevelSettingsBtnA);
				Thread.sleep(3000);
				click(btn_InventoryBtnA);
				
				selectText(txt_cboxProductCodeNameA, "Code and Description");
				Thread.sleep(3000);
				click(btn_InventoryUpdateBtnA);
				
		   }
		   
		   // Service_Case_065
		   public void VerifyThatProductIsDisplayedAccordingToTheBalancingLevelSettingNameOnly() throws Exception {
			   
			    Thread.sleep(3000);
				click(btn_AdministrationBtnA);
				Thread.sleep(3000);
				click(btn_BalancingLevelSettingsBtnA);
				Thread.sleep(3000);
				click(btn_InventoryBtnA);
				
				selectText(txt_cboxProductCodeNameA, "Name Only");
				Thread.sleep(3000);
				click(btn_InventoryUpdateBtnA);
				
				pageRefersh();
				
				Thread.sleep(6000);
				clickNavigation();
				navigationCaseForm();

				click(btn_CustomerAccountButtonA);
				sendKeys(txt_CustomerAccountTextA, CustomerAccountCodeAndDicriptionA);
				Thread.sleep(3000);
				pressEnter(txt_CustomerAccountTextA);
				Thread.sleep(3000);
				doubleClick(sel_CustomerAccountSelA);
				
				Thread.sleep(3000);
				click(btn_newWindowProdA);
				Thread.sleep(3000);
				click(sel_newWindowProdSelA);
				Thread.sleep(3000);
				JavascriptExecutor j1 = (JavascriptExecutor) driver;
				j1.executeScript("$(\"#divServiceSerial\").parent().find('.dialogbox-buttonarea > .button').click()");
				
				JavascriptExecutor jse = (JavascriptExecutor)driver;
				String Product =  (String) jse.executeScript("return $(\"#txtProduct\").val()");
				
				Thread.sleep(5000);
				if (Product.contentEquals(ProductDicriptionA)) {
					writeTestResults("Verify that \"Product\" is displayed according to the balancing level setting \"Name only\" ",
							"\"Product\" is displayed according to the balancing level setting \"Name only\"",
							"\"Product\" is displayed according to the balancing level setting \"Name only\" Successfully", "pass");
				} else {
					writeTestResults("Verify that \"Product\" is displayed according to the balancing level setting \"Name only\" ",
							"\"Product\" is displayed according to the balancing level setting \"Name only\"",
							"\"Product\" is displayed according to the balancing level setting \"Name only\" Fail", "fail");
				}
				
				
				Thread.sleep(6000);
				clickNavigation();
			    Thread.sleep(3000);
				click(btn_AdministrationBtnA);
				Thread.sleep(3000);
				click(btn_BalancingLevelSettingsBtnA);
				Thread.sleep(3000);
				click(btn_InventoryBtnA);
				
				selectText(txt_cboxProductCodeNameA, "Code and Description");
				Thread.sleep(3000);
				click(btn_InventoryUpdateBtnA);
				
		   }
		   
		   // Service_Case_066
		   public void VerifyThatCaseIsCompletedWhenServiceJobOrderIsCompleted() throws Exception {
			   
			    navigationCaseForm();
				Thread.sleep(1000);
				LocalTime obj = LocalTime.now();
				sendKeys(txt_TitleTextA, "Case" + obj);
				selectText(txt_OriginTextA, OriginTextFieldA);

				click(btn_CustomerAccountButtonA);
				sendKeys(txt_CustomerAccountTextA, CustomerAccountTextA);
				Thread.sleep(3000);
				pressEnter(txt_CustomerAccountTextA);
				Thread.sleep(3000);
				doubleClick(sel_CustomerAccountSelA);

				Thread.sleep(3000);
				click(btn_newWindowProdA);
				Thread.sleep(3000);
				click(sel_newWindowProdSelA);
				Thread.sleep(3000);
				JavascriptExecutor j1 = (JavascriptExecutor) driver;
				j1.executeScript("$(\"#divServiceSerial\").parent().find('.dialogbox-buttonarea > .button').click()");

				selectText(txt_TypeTextA, TypeTextFieldA);
				selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);
				selectText(txt_PriorityTextA, PriorityTextFieldA);

				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that able to draft the Case via main menu", "able to draft the Case via main menu",
							"able to draft the Case via main menu Successfully", "pass");
				} else {
					writeTestResults("Verify that able to draft the Case via main menu", "able to draft the Case via main menu",
							"able to draft the Case via main menu Fail", "fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String CNo = trackCode;
				
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that able to Release the Case via main menu",
							"able to Release the Case via main menu", "able to Release the Case via main menu Successfully",
							"pass");
				} else {
					writeTestResults("Verify that able to Release the Case via main menu",
							"able to Release the Case via main menu", "able to Release the Case via main menu Fail", "fail");
				}
				
				pageRefersh();
				Thread.sleep(3000);
				click(btn_ActionBtnA);
				Thread.sleep(3000);
				click(btn_DroptoServiceCentreBtnA);
				Thread.sleep(3000);
				click(btn_EntutionHeaderA);
				Thread.sleep(3000);
				click(btn_RolecenterA);
				
				WaitForOpenServiceJobsNoA();
				
				Thread.sleep(3000);
				click(btn_OpenServiceJobsA);
				Thread.sleep(5000);
				click(btn_ServiceJobArrowA.replace("CNo", CNo));
				
				
				switchWindow();
				
				Thread.sleep(3000);
				click(btn_OwnerBtnA);
				sendKeys(txt_OwnerTxtA, OwnerTxtA);
				Thread.sleep(3000);
				pressEnter(txt_OwnerTxtA);
				Thread.sleep(3000);
				doubleClick(sel_OwnerSelA);
				
				Thread.sleep(3000);
				click(btn_PricingProfileBtnA);
				sendKeys(txt_PricingProfileTxtA, PricingProfileTxtA);
				Thread.sleep(3000);
				pressEnter(txt_PricingProfileTxtA);
				Thread.sleep(3000);
				doubleClick(sel_PricingProfileSelA);
				
				Thread.sleep(3000);
				click(btn_ServiceLocationBtnA);
				sendKeys(txt_ServiceLocationTxtA, ServiceLocationTxtA);
				Thread.sleep(3000);
				pressEnter(txt_ServiceLocationTxtA);
				Thread.sleep(3000);
				doubleClick(sel_ServiceLocationSelA);
				
				Thread.sleep(3000);
				click(btn_TaskDetailsA);
				Thread.sleep(3000);
				click(btn_TaskDetailsProBtnA);
				sendKeys(txt_TaskDetailsProTxtA, ServiceProductA);
				Thread.sleep(3000);
				pressEnter(txt_TaskDetailsProTxtA);
				Thread.sleep(3000);
				doubleClick(sel_TaskDetailsProSelA);
				
				
				Thread.sleep(3000);
				click(btn_DraftA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form", "User Can draft the Service Job Order Form Fail",
							"fail");
				}

				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that  able to create the service Job Order by convert the Case which created from Menu(Without service quotation)",
							"User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation)",
							"User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation) Successfully", "pass");
				} else {
					writeTestResults("Verify that  able to create the service Job Order by convert the Case which created from Menu(Without service quotation)",
							"User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation)", "User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation) Fail",
							"fail");
				}
				
				WaitClick(btn_ActionBtnA);
				click(btn_ActionBtnA);
				WaitClick(btn_StopBtnA);
			    click(btn_StopBtnA);
			    WaitClick(btn_CompleteProcessBtnA);
			    click(btn_CompleteProcessBtnA);
			    
			    pageRefersh();
			    
			    WaitClick(btn_ActionBtnA);
				click(btn_ActionBtnA);
				WaitClick(btn_CompleteBtnA);
			    click(btn_CompleteBtnA);
			    WaitClick(btn_CompleteYesBtnA);
			    click(btn_CompleteYesBtnA);
			    
				Thread.sleep(3000);
				JavascriptExecutor j7 = (JavascriptExecutor)driver;
				j7.executeScript("$(\"#txtServiceJOCompPostDate\").datepicker(\"setDate\", new Date())");
				WaitClick(btn_PostDateApplyA);
				click(btn_PostDateApplyA);
				
				WaitElement(txt_pageCompletedA);
				if (isDisplayed(txt_pageCompletedA)) {
					writeTestResults("Verify that User Can Completed the Service Job Order Form",
							"User Can Completed the Service Job Order Form",
							"User Can Completed the Service Job Order Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can Completed the Service Job Order Form",
							"User Can Completed the Service Job Order Form",
							"User Can Completed the Service Job Order Form Fail", "fail");
				}
				
				Thread.sleep(6000);
				clickNavigation();
				Thread.sleep(3000);
				click(btn_FormCaseA);
				
				Thread.sleep(3000);
				sendKeys(txt_CopyFromSerchtxtA, CNo);
				pressEnter(txt_CopyFromSerchtxtA);
				Thread.sleep(5000);
				doubleClick(sel_CaseSerchSelA);
				
				Thread.sleep(3000);
				String StageFieldA = new Select(driver.findElement(By.xpath(txt_StageFieldA))).getFirstSelectedOption().getText();
				Thread.sleep(3000);
				click(btn_StatusFieldArrowA);
				String StatusFieldA = getText(txt_StatusFieldTxtA);

				Thread.sleep(5000);
				if (StageFieldA.contentEquals("Completed") && StatusFieldA.contentEquals("Completed")) {
					writeTestResults("Verify that case is completed when service job order is completed",
							"case is completed when service job order is completed",
							"case is completed when service job order is completed Successfully", "pass");
				} else {
					writeTestResults("Verify that case is completed when service job order is completed",
							"case is completed when service job order is completed",
							"case is completed when service job order is completed Fail", "fail");
				}
		   }
		   
		   // Service_HA_001
		   public void VerifyThatUserCanNavigateToTheHireAgreementForm() throws Exception {
			   
			   Thread.sleep(3000);
			   click(btn_HireAgreementBtnA);
			   Thread.sleep(3000);
			   click(btn_NewHireAgreementBtnA);
				Thread.sleep(3000);
				if (getText(txt_pageHeaderA).contentEquals("Hire Agreement - New")) {
					writeTestResults("Verify that user can navigate to the Hire Agreement form", "user can navigate to the Hire Agreement form",
							"user can navigate to the Hire Agreement form Successfully", "pass");
				} else {
					writeTestResults("Verify that user can navigate to the Hire Agreement form", "user can navigate to the Hire Agreement form",
							"user can navigate to the Hire Agreement form Fail", "fail");
				}
		   }
		   
		   // Service_HA_002
		   public void VerifyThatFollowingSystemDefinedRequiredFieldsAreDisplayedInTheHireAgreementNewForm() throws Exception {
			   
			   VerifyThatUserCanNavigateToTheHireAgreementForm();
			   
				Thread.sleep(4000);
				if (isDisplayed(txt_AgreementNoStarA)) {
					writeTestResults("Verify Agreement No field is displayed as the mandatory fields",
							"Agreement No field is displayed as the mandatory fields",
							"Agreement No fields is displayed as the mandatory fields Successfully", "pass");
				} else {
					writeTestResults("Verify Agreement Noe field is displayed as the mandatory fields",
							"Agreement No field is displayed as the mandatory fields",
							"Agreement No field is displayed as the mandatory fields Fail", "fail");
				}
				
				if (isDisplayed(txt_TitleStarA)) {
					writeTestResults("Verify Title field is displayed as the mandatory fields",
							"Title field is displayed as the mandatory fields",
							"Title field is displayed as the mandatory fields Successfully", "pass");
				} else {
					writeTestResults("Verify Title field is displayed as the mandatory fields",
							"Title field is displayed as the mandatory fields",
							"Title field is displayed as the mandatory fields Fail", "fail");
				}
				
				if (isDisplayed(txt_AgreementDateStarA)) {
					writeTestResults("Verify Agreement Date field is displayed as the mandatory fields",
							"Agreement Date field is displayed as the mandatory fields",
							"Agreement Date field is displayed as the mandatory fields Successfully", "pass");
				} else {
					writeTestResults("Verify Agreement Date field is displayed as the mandatory fields",
							"Agreement Date field is displayed as the mandatory fields",
							"Agreement Date field is displayed as the mandatory fields Fail", "fail");
				}
				
				if (isDisplayed(txt_ContractGroupStarA)) {
					writeTestResults("Verify Contract Group field is displayed as the mandatory fields",
							"Contract Group field is displayed as the mandatory fields",
							"Contract Group field is displayed as the mandatory fields Successfully", "pass");
				} else {
					writeTestResults("Verify Contract Group field is displayed as the mandatory fields",
							"Contract Group field is displayed as the mandatory fields",
							"Contract Group field is displayed as the mandatory fields Fail", "fail");
				}
				
				if (isDisplayed(txt_ValidityPeriodForStarA)) {
					writeTestResults("Verify Validity Period For field is displayed as the mandatory fields",
							"Validity Period For field is displayed as the mandatory fields",
							"Validity Period For field is displayed as the mandatory fields Successfully", "pass");
				} else {
					writeTestResults("Verify Validity Period For field is displayed as the mandatory fields",
							"Validity Period For field is displayed as the mandatory fields",
							"Validity Period For field is displayed as the mandatory fields Fail", "fail");
				}
				
				if (isDisplayed(txt_EffectiveFromStarA)) {
					writeTestResults("Verify Effective From field is displayed as the mandatory fields",
							"Effective From field is displayed as the mandatory fields",
							"Effective From field is displayed as the mandatory fields Successfully", "pass");
				} else {
					writeTestResults("Verify Effective From field is displayed as the mandatory fields",
							"Validity Effective From field is displayed as the mandatory fields",
							"Validity Effective From field is displayed as the mandatory fields Fail", "fail");
				}
				
				if (isDisplayed(txt_EffectiveToStarA)) {
					writeTestResults("Verify Effective To field is displayed as the mandatory fields",
							"Effective To field is displayed as the mandatory fields",
							"Effective To field is displayed as the mandatory fields Successfully", "pass");
				} else {
					writeTestResults("Verify Effective To field is displayed as the mandatory fields",
							"Effective To field is displayed as the mandatory fields",
							"Effective To field is displayed as the mandatory fields Fail", "fail");
				}
				
				if (isDisplayed(txt_DescriptionStarA)) {
					writeTestResults("Verify Description field is displayed as the mandatory fields",
							"Description field is displayed as the mandatory fields",
							"Description field is displayed as the mandatory fields Successfully", "pass");
				} else {
					writeTestResults("Verify Description field is displayed as the mandatory fields",
							"Description field is displayed as the mandatory fields",
							"Description field is displayed as the mandatory fields Fail", "fail");
				}
				
				if (isDisplayed(txt_SalesUnitStarA)) {
					writeTestResults("Verify Sales Unit field is displayed as the mandatory fields",
							"Sales Unit field is displayed as the mandatory fields",
							"Sales Unit field is displayed as the mandatory fields Successfully", "pass");
				} else {
					writeTestResults("Verify Sales Unit field is displayed as the mandatory fields",
							"Sales Unit field is displayed as the mandatory fields",
							"Sales Unit field is displayed as the mandatory fields Fail", "fail");
				}
				
				if (isDisplayed(txt_AccountOwnerStarA)) {
					writeTestResults("Verify Account Owner field is displayed as the mandatory fields",
							"Account Owner field is displayed as the mandatory fields",
							"Account Owner field is displayed as the mandatory fields Successfully", "pass");
				} else {
					writeTestResults("Verify Account Owner field is displayed as the mandatory fields",
							"Account Owner field is displayed as the mandatory fields",
							"Account Owner field is displayed as the mandatory fields Fail", "fail");
				}
				
				if (isDisplayed(txt_CustomerAccountStarA)) {
					writeTestResults("Verify Customer Account field is displayed as the mandatory fields",
							"Customer Account field is displayed as the mandatory fields",
							"Customer Account field is displayed as the mandatory fields Successfully", "pass");
				} else {
					writeTestResults("Verify Customer Account field is displayed as the mandatory fields",
							"Customer Account field is displayed as the mandatory fields",
							"Customer Account field is displayed as the mandatory fields Fail", "fail");
				}
				
				if (isDisplayed(txt_CurrencyStarA)) {
					writeTestResults("Verify Currency field is displayed as the mandatory fields",
							"Currency field is displayed as the mandatory fields",
							"Currency field is displayed as the mandatory fields Successfully", "pass");
				} else {
					writeTestResults("Verify Currency field is displayed as the mandatory fields",
							"Currency field is displayed as the mandatory fields",
							"Currency field is displayed as the mandatory fields Fail", "fail");
				}
				
				if (isDisplayed(txt_ScheduleCodeStarA)) {
					writeTestResults("Verify Schedule Code is displayed as the mandatory fields",
							"Schedule Code is displayed as the mandatory fields",
							"Schedule Code is displayed as the mandatory fields Successfully", "pass");
				} else {
					writeTestResults("Verify Schedule Code is displayed as the mandatory fields",
							"Schedule Code is displayed as the mandatory fields",
							"Schedule Code is displayed as the mandatory fields Fail", "fail");
				}
				
		   }
		   
		   // Service_HA_003
		   public void VerifyThatCannotDraftTheDocumentWithoutFillingTheRequiredFields() throws Exception {
			   
			    VerifyThatUserCanNavigateToTheHireAgreementForm();
			    Thread.sleep(3000);
			    click(btn_DraftA);
				Thread.sleep(3000);
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("$(closeMessageBox()).click()");
				
				if (isDisplayed(txt_AgreementNoValidationA) && isDisplayed(txt_TitleValidationA) && isDisplayed(txt_AgreementDateValidationA)
						&& isDisplayed(txt_ContractGroupValidationA) && isDisplayed(txt_EffectiveFromValidationA) && isDisplayed(txt_EffectiveToValidationA)
						 && isDisplayed(txt_DescriptionValidationA) && isDisplayed(txt_SalesUnitValidationA) && isDisplayed(txt_AccountOwnerValidationA)
						 && isDisplayed(txt_CustomerAccountValidationA) && isDisplayed(txt_ScheduleCodeValidationA)) {
					writeTestResults("Verify that cannot Draft the document without filling the required fields",
							"user cannot Draft the document without filling the required fields",
							"user cannot Draft the document without filling the required fields Successfully", "pass");
				} else {
					writeTestResults("Verify that cannot Draft the document without filling the required fields",
							"user cannot Draft the document without filling the required fields",
							"user cannot Draft the document without filling the required fields Fail", "fail");
				}
		   }
		   
		   // Service_HA_004
		   public void VerifyThatAbleToDraftTheHireAgreement() throws Exception {
			   
			   VerifyThatUserCanNavigateToTheHireAgreementForm();
			   
			   Thread.sleep(3000);
			   LocalTime obj1 = LocalTime.now();
			   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj1);
			   sendKeys(txt_TitleTxtA, "Testing Hire Agreement");
			   
			   Thread.sleep(3000);
			   JavascriptExecutor j7 = (JavascriptExecutor)driver;
			   j7.executeScript("$(\"#txtContractDate\").datepicker(\"setDate\", new Date())");
			   
			   
			   selectText(txt_ContractGroupTxtA, "ContractGroupService");
			   
			   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker(\"setDate\", new Date())");
			   j7.executeScript("$(\"#txtEffectiveTo\").datepicker(\"setDate\", new Date())");
			   
			   sendKeys(txt_DescriptionTxtA, "Testing Hire Agreement");
			   
			   Thread.sleep(3000);
			   click(btn_CustomerAccountHABtnA);
			   sendKeys(txt_CustomerAccountHATxtA, CustomerAccountTextA);
			   Thread.sleep(3000);
			   pressEnter(txt_CustomerAccountHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_CustomerAccountHASelA);
			   
			   selectText(txt_SalesUnitHAA, SalesUnitA);
			   
			   Thread.sleep(3000);
			   click(btn_ScheduleCodeHABtnA);
			   sendKeys(txt_ScheduleCodeHATxtA, ScheduleCodeHATxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ScheduleCodeHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ScheduleCodeHASelA);
			   
			   Thread.sleep(3000);
			   click(btn_ResponsibleUserHABtnA);
			   sendKeys(txt_ResponsibleUserHATxtA, ResponsibleUserHATxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ResponsibleUserHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ResponsibleUserHASelA);
			   Thread.sleep(3000);
			   selectText(txt_HireBasisA, "Units");
			   
			   Thread.sleep(3000);
			   click(btn_ResourceInformationTabA);
			   selectText(txt_TypeHAA, "Resource");
			   Thread.sleep(3000);
			   click(btn_ReferenceCodeHABtnA);
			   sendKeys(txt_ReferenceCodeHATxtA, ReferenceCodeHA1TxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ReferenceCodeHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ReferenceCodeHASelA);
			   
			   sendKeys(txt_OpeningUnitA, "10");
			   Thread.sleep(3000);
			   click(btn_RateRangeA);
			   sendKeys(txt_MinimumUnitA, "5");
			   sendKeys(txt_Rate1A, "5");
			   Thread.sleep(3000);
			   click(btn_RateRangeSetupPlusA);
			   sendKeys(txt_RateRangeSetupStartA, "6");
			   sendKeys(txt_RateRangeSetupEndA, "0");
			   sendKeys(txt_Rate2A, "10");
			   Thread.sleep(3000);
			   click(btn_RateRangeSetupApplyA);
			   
			   Thread.sleep(3000);
			   click(btn_DraftA);
			   Thread.sleep(5000);
			   if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
							"User Can draft the Hire Agreement Form Successfully", "pass");
			   } else {
					writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
							"User Can draft the Hire Agreement Form Fail", "fail");
			   }
		   }
		   
		   // Service_HA_005
		   public void VerifyThatCannotDraftTheHireAgreementWithoutAddingTheResourceInformations() throws Exception {
			   
			   VerifyThatUserCanNavigateToTheHireAgreementForm();
			   
			   Thread.sleep(3000);
			   LocalTime obj1 = LocalTime.now();
			   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj1);
			   sendKeys(txt_TitleTxtA, "Testing Hire Agreement");
			   
			   Thread.sleep(3000);
			   JavascriptExecutor j7 = (JavascriptExecutor)driver;
			   j7.executeScript("$(\"#txtContractDate\").datepicker(\"setDate\", new Date())");
			   
			   
			   selectText(txt_ContractGroupTxtA, "ContractGroupService");
			   
			   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker(\"setDate\", new Date())");
			   j7.executeScript("$(\"#txtEffectiveTo\").datepicker(\"setDate\", new Date())");
			   
			   sendKeys(txt_DescriptionTxtA, "Testing Hire Agreement");
			   
			   Thread.sleep(3000);
			   click(btn_CustomerAccountHABtnA);
			   sendKeys(txt_CustomerAccountHATxtA, CustomerAccountTextA);
			   Thread.sleep(3000);
			   pressEnter(txt_CustomerAccountHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_CustomerAccountHASelA);
			   
			   selectText(txt_SalesUnitHAA, SalesUnitA);
			   
			   Thread.sleep(3000);
			   click(btn_ScheduleCodeHABtnA);
			   sendKeys(txt_ScheduleCodeHATxtA, ScheduleCodeHATxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ScheduleCodeHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ScheduleCodeHASelA);
			   
			   Thread.sleep(3000);
			   click(btn_ResponsibleUserHABtnA);
			   sendKeys(txt_ResponsibleUserHATxtA, ResponsibleUserHATxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ResponsibleUserHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ResponsibleUserHASelA);
			   Thread.sleep(3000);
			   selectText(txt_HireBasisA, "Units");
			  
			   Thread.sleep(3000);
			   click(btn_DraftA);

			   Thread.sleep(3000);
			   JavascriptExecutor jse = (JavascriptExecutor) driver;
		       jse.executeScript("$(closeMessageBox()).click()");
		       
			   Thread.sleep(5000);
			   click(btn_ErrorBtnA);
			   if (isDisplayed(txt_ReferenceCodeErrorA) && isDisplayed(txt_ServiceProductErrorA) && isDisplayed(txt_SelectaServiceProductErrorA) 
					   && isDisplayed(txt_SelectaResourceProductErrorA) && isDisplayed(txt_EnterOpeningUnitsErrorA)) {
					writeTestResults("Verify that cannot draft the hire agreement without adding the Resource informations", "cannot draft the hire agreement without adding the Resource informations",
							"cannot draft the hire agreement without adding the Resource informations Successfully", "pass");
			   } else {
					writeTestResults("Verify that cannot draft the hire agreement without adding the Resource informations", "cannot draft the hire agreement without adding the Resource informations",
							"cannot draft the hire agreement without adding the Resource informations Fail", "fail");
			   }
		   }
		   
		   // Service_HA_006
		   public void VerifyThatAbleToEditUpdateTheDraftedHireAgreement() throws Exception {
			   
			   VerifyThatAbleToDraftTheHireAgreement();
			   
			   Thread.sleep(3000);
			   click(btn_EditA);
			   
			   Thread.sleep(3000);
			   sendKeys(txt_TitleTxtA, "Test Hire Agreement");
			   
			   Thread.sleep(3000);
			   click(btn_UpdateA);
			   
			   Thread.sleep(5000);
			   if (getText(txt_TitleFieldA).contentEquals("Test Hire Agreement")) {
					writeTestResults("Verify that able to \" Edit-Update \" the Drafted Hire agreement", "User can \" Edit-Update \" the Drafted Hire agreement",
							"User can \" Edit-Update \" the Drafted Hire agreement Successfully", "pass");
			   } else {
					writeTestResults("Verify that able to \" Edit-Update \" the Drafted Hire agreement", "User can \" Edit-Update \" the Drafted Hire agreement",
							"User can \" Edit-Update \" the Drafted Hire agreement Fail", "fail");
			   }
			 
		   }
		   
		   // Service_HA_007
		   public void VerifyThatAbleToDeleteTheDraftedHireAgreement() throws Exception {
			   
			   VerifyThatAbleToDraftTheHireAgreement();
			   
			   Thread.sleep(3000);
			   click(btn_ActionBtnA);
			   Thread.sleep(3000);
			   click(btn_DeleteHABtnA);
			   Thread.sleep(3000);
			   click(btn_YesBtnA);
			   
			   Thread.sleep(5000);
			   if (isDisplayed(txt_pageDeletedA)) {
					writeTestResults("Verify that able to delete the Drafted hire agreement", "User can delete the Drafted hire agreement",
							"User can delete the Drafted hire agreement Successfully", "pass");
			   } else {
					writeTestResults("Verify that able to delete the Drafted hire agreement", "User can delete the Drafted hire agreement",
							"User can delete the Drafted hire agreement Fail", "fail");
			   }
		   }
		   
		   // Service_HA_008
		   public void VerifyThatAbleToReleaseTheHireAgreement() throws Exception {
			   
			   VerifyThatAbleToDraftTheHireAgreement();
			   
				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
						writeTestResults("Verify that able to Release the Hire Agreement Form", "User Can Release the Hire Agreement Form",
							"User Can Release the Hire Agreement Form Successfully", "pass");
				} else {
						writeTestResults("Verify that able to Release the Hire Agreement Form", "User Can Release the Hire Agreement Form",
								"User Can Release the Hire Agreement Form Fail", "fail");
				}
		   }
		   
		   // Service_HA_009
		   public void VerifyThatFollowingActionsAreAvailableAfterReleasedTheDocument() throws Exception {
			   
			   VerifyThatAbleToReleaseTheHireAgreement();

			   Thread.sleep(3000);
		       click(btn_ActionBtnA);
		       
			   Thread.sleep(5000);
		       if (isDisplayed(btn_ReverseHABtnA) && isDisplayed(btn_ActivitiesHABtnA) && isDisplayed(btn_ActualUpdateHABtnA) 
		    		   && isDisplayed(btn_CloseAgreementHABtnA) && isDisplayed(btn_ConvertToAcquireDepositHABtnA)) {
						writeTestResults("Verify that Following actions are available after Released the document", "following Actions are displayed under the list Reverse,Activities,Actual Update,Close Agreement,Convert to Aquire Deposite,Edit Resource Information",
							"following Actions are displayed under the list Reverse,Activities,Actual Update,Close Agreement,Convert to Aquire Deposite,Edit Resource Information Successfully", "pass");
		       } else {
						writeTestResults("Verify that Following actions are available after Released the document", "following Actions are displayed under the list Reverse,Activities,Actual Update,Close Agreement,Convert to Aquire Deposite,Edit Resource Information",
								"following Actions are displayed under the list Reverse,Activities,Actual Update,Close Agreement,Convert to Aquire Deposite,Edit Resource Information Fail", "fail");
		       }
		       
		   }
		   
		   // Service_HA_010
		   public void VerifyThatAbleToReverseTheReleaseDocument() throws Exception {
			   
			   VerifyThatAbleToReleaseTheHireAgreement();

			   Thread.sleep(3000);
		       click(btn_ActionBtnA);
			   Thread.sleep(3000);
		       click(btn_ReverseHABtnA);
			   Thread.sleep(3000);
		       click(btn_YesBtnA);
		       
			   Thread.sleep(5000);
			   if (isDisplayed(txt_pageReversedA)) {
					writeTestResults("Verify that able to reverse the document", "User Can reverse the Hire Agreement Form",
							"User Can reverse the Hire Agreement Form Successfully", "pass");
			   } else {
					writeTestResults("Verify that able to reverse the document", "User Can reverse the Hire Agreement Form",
							"User Can reverse the Hire Agreement Form Fail", "fail");
			   }
		   }
		   
		   // Service_HA_011
		   public void VerifyThatAbleToReleaseaHireAgreementByDuplicatingAnExistingDraftHireAgreement() throws Exception {
			   
			   VerifyThatUserCanNavigateToTheHireAgreementForm();
			   
			   Thread.sleep(3000);
			   LocalTime obj1 = LocalTime.now();
			   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj1);
			   sendKeys(txt_TitleTxtA, "Testing Hire Agreement");
			   
			   Thread.sleep(3000);
			   JavascriptExecutor j7 = (JavascriptExecutor)driver;
			   j7.executeScript("$(\"#txtContractDate\").datepicker(\"setDate\", new Date())");
			   
			   
			   selectText(txt_ContractGroupTxtA, "ContractGroupService");
			   
			   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker(\"setDate\", new Date())");
			   j7.executeScript("$(\"#txtEffectiveTo\").datepicker(\"setDate\", new Date())");
			   
			   sendKeys(txt_DescriptionTxtA, "Testing Hire Agreement");
			   
			   Thread.sleep(3000);
			   click(btn_CustomerAccountHABtnA);
			   sendKeys(txt_CustomerAccountHATxtA, CustomerAccountTextA);
			   Thread.sleep(3000);
			   pressEnter(txt_CustomerAccountHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_CustomerAccountHASelA);
			   
			   selectText(txt_SalesUnitHAA, SalesUnitA);
			   
			   Thread.sleep(3000);
			   click(btn_ScheduleCodeHABtnA);
			   sendKeys(txt_ScheduleCodeHATxtA, ScheduleCodeHATxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ScheduleCodeHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ScheduleCodeHASelA);
			   
			   Thread.sleep(3000);
			   click(btn_ResponsibleUserHABtnA);
			   sendKeys(txt_ResponsibleUserHATxtA, ResponsibleUserHATxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ResponsibleUserHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ResponsibleUserHASelA);
			   Thread.sleep(3000);
			   selectText(txt_HireBasisA, "Units");
			   
			   Thread.sleep(3000);
			   click(btn_ResourceInformationTabA);
			   selectText(txt_TypeHAA, "Resource");
			   Thread.sleep(3000);
			   click(btn_ReferenceCodeHABtnA);
			   sendKeys(txt_ReferenceCodeHATxtA, ReferenceCodeHA1TxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ReferenceCodeHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ReferenceCodeHASelA);
			   
			   sendKeys(txt_OpeningUnitA, "10");
			   Thread.sleep(3000);
			   click(btn_RateRangeA);
			   sendKeys(txt_MinimumUnitA, "5");
			   sendKeys(txt_Rate1A, "5");
			   Thread.sleep(3000);
			   click(btn_RateRangeSetupPlusA);
			   sendKeys(txt_RateRangeSetupStartA, "6");
			   sendKeys(txt_RateRangeSetupEndA, "0");
			   sendKeys(txt_Rate2A, "10");
			   Thread.sleep(3000);
			   click(btn_RateRangeSetupApplyA);
			   
			   Thread.sleep(3000);
			   click(btn_DraftA);
			   Thread.sleep(3000);
			   trackCode = getText(txt_trackCodeA);
			   String HANo = trackCode;
			   Thread.sleep(5000);
			   if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
							"User Can draft the Hire Agreement Form Successfully", "pass");
			   } else {
					writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
							"User Can draft the Hire Agreement Form Fail", "fail");
			   }
			   
			   Thread.sleep(3000);
			   click(btn_HireAgreementByPageBtnA);
			   Thread.sleep(3000);
			   sendKeys(txt_HireAgreementSerchTxtA, HANo);
			   Thread.sleep(3000);
			   pressEnter(txt_HireAgreementSerchTxtA);
			   Thread.sleep(3000);
			   doubleClick(sel_HireAgreementSerchSelA.replace("HANo", HANo));
			   
			   Thread.sleep(3000);
			   click(btn_DuplicateHABtnA);
			   
			   Thread.sleep(3000);
			   LocalTime obj2 = LocalTime.now();
			   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj2);
			   
			   Thread.sleep(3000);
			   j7.executeScript("$(\"#txtContractDate\").datepicker(\"setDate\", new Date())");
			   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker(\"setDate\", new Date())");
			   j7.executeScript("$(\"#txtEffectiveTo\").datepicker(\"setDate\", new Date())");
			   
			   Thread.sleep(3000);
			   click(btn_DraftA);
			   Thread.sleep(5000);
			   if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that able to draft the Duplicated Hire Agreement Form", "User Can draft the Duplicated Hire Agreement Form",
							"User Can draft the Duplicated Hire Agreement Form Successfully", "pass");
			   } else {
					writeTestResults("Verify that able to draft the Duplicated Hire Agreement Form", "User Can draft the Duplicated Hire Agreement Form",
							"User Can draft the Duplicated Hire Agreement Form Fail", "fail");
			   }
			   
				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
						writeTestResults("Verify that able to Release a Hire agreement by duplicating an existing \"Draft\" hire agreement", "User Can Release a Hire agreement by duplicating an existing \"Draft\" hire agreement",
							"User Can Release a Hire agreement by duplicating an existing \"Draft\" hire agreement Successfully", "pass");
				} else {
						writeTestResults("Verify that able to Release a Hire agreement by duplicating an existing \"Draft\" hire agreement", "User Can Release a Hire agreement by duplicating an existing \"Draft\" hire agreement",
								"User Can Release a Hire agreement by duplicating an existing \"Draft\" hire agreement Fail", "fail");
				}
		   }
		   
		   // Service_HA_012
		   public void VerifyThatAbleToReleaseaHireAgreementByDuplicatingAnExistingReleasedHireAgreement() throws Exception {
			   
			    VerifyThatAbleToDraftTheHireAgreement();
			   
				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String HANo = trackCode;
				
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
						writeTestResults("Verify that able to Release the Hire Agreement Form", "User Can Release the Hire Agreement Form",
							"User Can Release the Hire Agreement Form Successfully", "pass");
				} else {
						writeTestResults("Verify that able to Release the Hire Agreement Form", "User Can Release the Hire Agreement Form",
								"User Can Release the Hire Agreement Form Fail", "fail");
				}
				
				   Thread.sleep(3000);
				   click(btn_HireAgreementByPageBtnA);
				   Thread.sleep(3000);
				   sendKeys(txt_HireAgreementSerchTxtA, HANo);
				   Thread.sleep(3000);
				   pressEnter(txt_HireAgreementSerchTxtA);
				   Thread.sleep(3000);
				   doubleClick(sel_HireAgreementSerchSelA.replace("HANo", HANo));
				   
				   Thread.sleep(3000);
				   click(btn_DuplicateHABtnAfterReleaseA);
				   
				   Thread.sleep(3000);
				   LocalTime obj2 = LocalTime.now();
				   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj2);
				   
				   Thread.sleep(3000);
				   JavascriptExecutor j7 = (JavascriptExecutor)driver;
				   j7.executeScript("$(\"#txtContractDate\").datepicker(\"setDate\", new Date())");
				   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker(\"setDate\", new Date())");
				   j7.executeScript("$(\"#txtEffectiveTo\").datepicker(\"setDate\", new Date())");
				   
				   Thread.sleep(3000);
				   click(btn_DraftA);
				   Thread.sleep(5000);
				   if (isDisplayed(txt_PageDraft1A)) {
						writeTestResults("Verify that able to draft the Duplicated Hire Agreement Form", "User Can draft the Duplicated Hire Agreement Form",
								"User Can draft the Duplicated Hire Agreement Form Successfully", "pass");
				   } else {
						writeTestResults("Verify that able to draft the Duplicated Hire Agreement Form", "User Can draft the Duplicated Hire Agreement Form",
								"User Can draft the Duplicated Hire Agreement Form Fail", "fail");
				   }
				   
					Thread.sleep(3000);
					click(btn_ReleaseA);
					Thread.sleep(3000);
					trackCode = getText(txt_trackCodeA);
					Thread.sleep(5000);
					if (isDisplayed(txt_PageRelease1A)) {
							writeTestResults("Verify that able to Release a Hire agreement by duplicating an existing \"Released\" hire agreement", "User Can Release a Hire agreement by duplicating an existing \"Released\" hire agreement",
								"User Can Release a Hire agreement by duplicating an existing \"Released\" hire agreement Successfully", "pass");
					} else {
							writeTestResults("Verify that able to Release a Hire agreement by duplicating an existing \"Released\" hire agreement", "User Can Release a Hire agreement by duplicating an existing \"Released\" hire agreement",
									"User Can Release a Hire agreement by duplicating an existing \"Released\" hire agreement Fail", "fail");
					}
		   }
		   
		   
		   // Service_HA_013
		   public void VerifyThatAbleToReleaseaHireAgreementByDuplicatingAnExistingDeletedHireAgreement() throws Exception {
			   
               VerifyThatUserCanNavigateToTheHireAgreementForm();
			   
			   Thread.sleep(3000);
			   LocalTime obj1 = LocalTime.now();
			   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj1);
			   sendKeys(txt_TitleTxtA, "Testing Hire Agreement");
			   
			   Thread.sleep(3000);
			   JavascriptExecutor j7 = (JavascriptExecutor)driver;
			   j7.executeScript("$(\"#txtContractDate\").datepicker(\"setDate\", new Date())");
			   
			   
			   selectText(txt_ContractGroupTxtA, "ContractGroupService");
			   
			   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker(\"setDate\", new Date())");
			   j7.executeScript("$(\"#txtEffectiveTo\").datepicker(\"setDate\", new Date())");
			   
			   sendKeys(txt_DescriptionTxtA, "Testing Hire Agreement");
			   
			   Thread.sleep(3000);
			   click(btn_CustomerAccountHABtnA);
			   sendKeys(txt_CustomerAccountHATxtA, CustomerAccountTextA);
			   Thread.sleep(3000);
			   pressEnter(txt_CustomerAccountHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_CustomerAccountHASelA);
			   
			   selectText(txt_SalesUnitHAA, SalesUnitA);
			   
			   Thread.sleep(3000);
			   click(btn_ScheduleCodeHABtnA);
			   sendKeys(txt_ScheduleCodeHATxtA, ScheduleCodeHATxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ScheduleCodeHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ScheduleCodeHASelA);
			   
			   Thread.sleep(3000);
			   click(btn_ResponsibleUserHABtnA);
			   sendKeys(txt_ResponsibleUserHATxtA, ResponsibleUserHATxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ResponsibleUserHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ResponsibleUserHASelA);
			   Thread.sleep(3000);
			   selectText(txt_HireBasisA, "Units");
			   
			   Thread.sleep(3000);
			   click(btn_ResourceInformationTabA);
			   selectText(txt_TypeHAA, "Resource");
			   Thread.sleep(3000);
			   click(btn_ReferenceCodeHABtnA);
			   sendKeys(txt_ReferenceCodeHATxtA, ReferenceCodeHA1TxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ReferenceCodeHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ReferenceCodeHASelA);
			   
			   sendKeys(txt_OpeningUnitA, "10");
			   Thread.sleep(3000);
			   click(btn_RateRangeA);
			   sendKeys(txt_MinimumUnitA, "5");
			   sendKeys(txt_Rate1A, "5");
			   Thread.sleep(3000);
			   click(btn_RateRangeSetupPlusA);
			   sendKeys(txt_RateRangeSetupStartA, "6");
			   sendKeys(txt_RateRangeSetupEndA, "0");
			   sendKeys(txt_Rate2A, "10");
			   Thread.sleep(3000);
			   click(btn_RateRangeSetupApplyA);
			   
			   Thread.sleep(3000);
			   click(btn_DraftA);
			   Thread.sleep(3000);
		       trackCode = getText(txt_trackCodeA);
			   String HANo = trackCode;
			   
			   Thread.sleep(5000);
			   if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
							"User Can draft the Hire Agreement Form Successfully", "pass");
			   } else {
					writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
							"User Can draft the Hire Agreement Form Fail", "fail");
			   }

			   Thread.sleep(3000);
			   click(btn_ActionBtnA);
			   Thread.sleep(3000);
			   click(btn_DeleteHABtnA);
			   Thread.sleep(3000);
			   click(btn_YesBtnA);
			   
			   Thread.sleep(5000);
			   if (isDisplayed(txt_pageDeletedA)) {
					writeTestResults("Verify that able to delete the Drafted hire agreement", "User can delete the Drafted hire agreement",
							"User can delete the Drafted hire agreement Successfully", "pass");
			   } else {
					writeTestResults("Verify that able to delete the Drafted hire agreement", "User can delete the Drafted hire agreement",
							"User can delete the Drafted hire agreement Fail", "fail");
			   }
			   
			   Thread.sleep(3000);
			   click(btn_HireAgreementByPageBtnA);
			   Thread.sleep(3000);
			   sendKeys(txt_HireAgreementSerchTxtA, HANo);
			   Thread.sleep(3000);
			   pressEnter(txt_HireAgreementSerchTxtA);
			   Thread.sleep(3000);
			   doubleClick(sel_HireAgreementSerchSelA.replace("HANo", HANo));
			   
			   Thread.sleep(3000);
			   click(btn_DuplicateHABtnAfterReleaseA);
			   
			   Thread.sleep(3000);
			   LocalTime obj2 = LocalTime.now();
			   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj2);
			   
			   Thread.sleep(3000);
			   j7.executeScript("$(\"#txtContractDate\").datepicker(\"setDate\", new Date())");
			   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker(\"setDate\", new Date())");
			   j7.executeScript("$(\"#txtEffectiveTo\").datepicker(\"setDate\", new Date())");
			   
			   Thread.sleep(3000);
			   click(btn_DraftA);
			   Thread.sleep(5000);
			   if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that able to draft the Duplicated Hire Agreement Form", "User Can draft the Duplicated Hire Agreement Form",
							"User Can draft the Duplicated Hire Agreement Form Successfully", "pass");
			   } else {
					writeTestResults("Verify that able to draft the Duplicated Hire Agreement Form", "User Can draft the Duplicated Hire Agreement Form",
							"User Can draft the Duplicated Hire Agreement Form Fail", "fail");
			   }
			   
				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
						writeTestResults("Verify that able to Release a Hire agreement by duplicating an existing \"Deleted\" hire agreement", "User Can Release a Hire agreement by duplicating an existing \"Deleted\" hire agreement",
							"User Can Release a Hire agreement by duplicating an existing \"Deleted\" hire agreement Successfully", "pass");
				} else {
						writeTestResults("Verify that able to Release a Hire agreement by duplicating an existing \"Deleted\" hire agreement", "User Can Release a Hire agreement by duplicating an existing \"Deleted\" hire agreement",
								"User Can Release a Hire agreement by duplicating an existing \"Deleted\" hire agreement Fail", "fail");
				}
		   }
		   
		   // Service_HA_014
		   public void VerifyThatAbleToReleaseaHireAgreementByDuplicatingAnExistingReversedHireAgreement() throws Exception {
			   
			   VerifyThatUserCanNavigateToTheHireAgreementForm();
			   
			   Thread.sleep(3000);
			   LocalTime obj1 = LocalTime.now();
			   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj1);
			   sendKeys(txt_TitleTxtA, "Testing Hire Agreement");
			   
			   Thread.sleep(3000);
			   JavascriptExecutor j7 = (JavascriptExecutor)driver;
			   j7.executeScript("$(\"#txtContractDate\").datepicker(\"setDate\", new Date())");
			   
			   
			   selectText(txt_ContractGroupTxtA, "ContractGroupService");
			   
			   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker(\"setDate\", new Date())");
			   j7.executeScript("$(\"#txtEffectiveTo\").datepicker(\"setDate\", new Date())");
			   
			   sendKeys(txt_DescriptionTxtA, "Testing Hire Agreement");
			   
			   Thread.sleep(3000);
			   click(btn_CustomerAccountHABtnA);
			   sendKeys(txt_CustomerAccountHATxtA, CustomerAccountTextA);
			   Thread.sleep(3000);
			   pressEnter(txt_CustomerAccountHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_CustomerAccountHASelA);
			   
			   selectText(txt_SalesUnitHAA, SalesUnitA);
			   
			   Thread.sleep(3000);
			   click(btn_ScheduleCodeHABtnA);
			   sendKeys(txt_ScheduleCodeHATxtA, ScheduleCodeHATxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ScheduleCodeHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ScheduleCodeHASelA);
			   
			   Thread.sleep(3000);
			   click(btn_ResponsibleUserHABtnA);
			   sendKeys(txt_ResponsibleUserHATxtA, ResponsibleUserHATxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ResponsibleUserHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ResponsibleUserHASelA);
			   Thread.sleep(3000);
			   selectText(txt_HireBasisA, "Units");
			   
			   Thread.sleep(3000);
			   click(btn_ResourceInformationTabA);
			   selectText(txt_TypeHAA, "Resource");
			   Thread.sleep(3000);
			   click(btn_ReferenceCodeHABtnA);
			   sendKeys(txt_ReferenceCodeHATxtA, ReferenceCodeHA1TxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ReferenceCodeHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ReferenceCodeHASelA);
			   
			   sendKeys(txt_OpeningUnitA, "10");
			   Thread.sleep(3000);
			   click(btn_RateRangeA);
			   sendKeys(txt_MinimumUnitA, "5");
			   sendKeys(txt_Rate1A, "5");
			   Thread.sleep(3000);
			   click(btn_RateRangeSetupPlusA);
			   sendKeys(txt_RateRangeSetupStartA, "6");
			   sendKeys(txt_RateRangeSetupEndA, "0");
			   sendKeys(txt_Rate2A, "10");
			   Thread.sleep(3000);
			   click(btn_RateRangeSetupApplyA);
			   
			   Thread.sleep(3000);
			   click(btn_DraftA);
			   Thread.sleep(5000);
			   if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
							"User Can draft the Hire Agreement Form Successfully", "pass");
			   } else {
					writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
							"User Can draft the Hire Agreement Form Fail", "fail");
			   }

			   Thread.sleep(3000);
			   click(btn_ReleaseA);
		       Thread.sleep(3000);
		       trackCode = getText(txt_trackCodeA);
		       String HANo = trackCode;
			   Thread.sleep(5000);
			   if (isDisplayed(txt_PageRelease1A)) {
				        writeTestResults("Verify that able to Release the Hire Agreement Form", "User Can Release the Hire Agreement Form",
							"User Can Release the Hire Agreement Form Successfully", "pass");
		           } else {
					writeTestResults("Verify that able to Release the Hire Agreement Form", "User Can Release the Hire Agreement Form",
								"User Can Release the Hire Agreement Form Fail", "fail");
			   }

			   Thread.sleep(3000);
		       click(btn_ActionBtnA);
			   Thread.sleep(3000);
		       click(btn_ReverseHABtnA);
			   Thread.sleep(3000);
		       click(btn_YesBtnA);
		       
			   Thread.sleep(5000);
			   if (isDisplayed(txt_pageReversedA)) {
					writeTestResults("Verify that able to reverse the document", "User Can reverse the Hire Agreement Form",
							"User Can reverse the Hire Agreement Form Successfully", "pass");
			   } else {
					writeTestResults("Verify that able to reverse the document", "User Can reverse the Hire Agreement Form",
							"User Can reverse the Hire Agreement Form Fail", "fail");
			   }
			   
			   Thread.sleep(3000);
			   click(btn_HireAgreementByPageBtnA);
			   Thread.sleep(3000);
			   sendKeys(txt_HireAgreementSerchTxtA, HANo);
			   Thread.sleep(3000);
			   pressEnter(txt_HireAgreementSerchTxtA);
			   Thread.sleep(3000);
			   doubleClick(sel_HireAgreementSerchSelA.replace("HANo", HANo));
			   
			   Thread.sleep(3000);
			   click(btn_DuplicateHABtnAfterReleaseA);
			   
			   Thread.sleep(3000);
			   LocalTime obj2 = LocalTime.now();
			   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj2);
			   
			   Thread.sleep(3000);
			   j7.executeScript("$(\"#txtContractDate\").datepicker(\"setDate\", new Date())");
			   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker(\"setDate\", new Date())");
			   j7.executeScript("$(\"#txtEffectiveTo\").datepicker(\"setDate\", new Date())");
			   
			   Thread.sleep(3000);
			   click(btn_DraftA);
			   Thread.sleep(5000);
			   if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that able to draft the Duplicated Hire Agreement Form", "User Can draft the Duplicated Hire Agreement Form",
							"User Can draft the Duplicated Hire Agreement Form Successfully", "pass");
			   } else {
					writeTestResults("Verify that able to draft the Duplicated Hire Agreement Form", "User Can draft the Duplicated Hire Agreement Form",
							"User Can draft the Duplicated Hire Agreement Form Fail", "fail");
			   }
			   
				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
						writeTestResults("Verify that able to Release a Hire agreement by duplicating an existing \"Reversed\" hire agreement", "User Can Release a Hire agreement by duplicating an existing \"Reversed\" hire agreement",
							"User Can Release a Hire agreement by duplicating an existing \"Reversed\" hire agreement Successfully", "pass");
				} else {
						writeTestResults("Verify that able to Release a Hire agreement by duplicating an existing \"Reversed\" hire agreement", "User Can Release a Hire agreement by duplicating an existing \"Reversed\" hire agreement",
								"User Can Release a Hire agreement by duplicating an existing \"Reversed\" hire agreement Fail", "fail");
				}
		   }
		   
		   // Service_HA_015
		   public void VerifyThatRateRangeIconIsEnableWhenUserSelectingTheUnitsAsHireBasis() throws Exception {
			   
			   
			   VerifyThatUserCanNavigateToTheHireAgreementForm();

			   Thread.sleep(3000);
			   selectText(txt_HireBasisA, "Units");
			   
			   Thread.sleep(3000);
			   click(btn_ResourceInformationTabA);
			   
			   Thread.sleep(5000);
			   if (isDisplayed(btn_RateRangeA)) {
						writeTestResults("Verify that \"Rate Range\" Icon is enable when user selecting the Units as Hire Basis", "\"Rate Range\" Icon is enable when user selecting the Units as Hire Basis",
							"\"Rate Range\" Icon is enable when user selecting the Units as Hire Basis Successfully", "pass");
			   } else {
						writeTestResults("Verify that \"Rate Range\" Icon is enable when user selecting the Units as Hire Basis", "\"Rate Range\" Icon is enable when user selecting the Units as Hire Basis",
								"\"Rate Range\" Icon is enable when user selecting the Units as Hire Basis Fail", "fail");
			   }
		   }
		   
		   // Service_HA_016
		   public void VerifyThatHireAgreemntCanBeCreateByAddingMultipleResources() throws Exception {
			   
			   VerifyThatUserCanNavigateToTheHireAgreementForm();
			   
			   Thread.sleep(3000);
			   LocalTime obj1 = LocalTime.now();
			   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj1);
			   sendKeys(txt_TitleTxtA, "Testing Hire Agreement");
			   
			   Thread.sleep(3000);
			   JavascriptExecutor j7 = (JavascriptExecutor)driver;
			   j7.executeScript("$(\"#txtContractDate\").datepicker(\"setDate\", new Date())");
			   
			   
			   selectText(txt_ContractGroupTxtA, "ContractGroupService");
			   
			   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker(\"setDate\", new Date())");
			   j7.executeScript("$(\"#txtEffectiveTo\").datepicker(\"setDate\", new Date())");
			   
			   sendKeys(txt_DescriptionTxtA, "Testing Hire Agreement");
			   
			   Thread.sleep(3000);
			   click(btn_CustomerAccountHABtnA);
			   sendKeys(txt_CustomerAccountHATxtA, CustomerAccountTextA);
			   Thread.sleep(3000);
			   pressEnter(txt_CustomerAccountHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_CustomerAccountHASelA);
			   
			   selectText(txt_SalesUnitHAA, SalesUnitA);
			   
			   Thread.sleep(3000);
			   click(btn_ScheduleCodeHABtnA);
			   sendKeys(txt_ScheduleCodeHATxtA, ScheduleCodeHATxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ScheduleCodeHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ScheduleCodeHASelA);
			   
			   Thread.sleep(3000);
			   click(btn_ResponsibleUserHABtnA);
			   sendKeys(txt_ResponsibleUserHATxtA, ResponsibleUserHATxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ResponsibleUserHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ResponsibleUserHASelA);
			   Thread.sleep(3000);
			   selectText(txt_HireBasisA, "Units");
			   
			   Thread.sleep(3000);
			   click(btn_ResourceInformationTabA);
			   
			   
			   selectText(txt_TypeHAA, "Resource");
			   Thread.sleep(3000);
			   click(btn_ReferenceCodeHABtnA);
			   sendKeys(txt_ReferenceCodeHATxtA, ReferenceCodeHA1TxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ReferenceCodeHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ReferenceCodeHASelA);
			   
			   sendKeys(txt_OpeningUnitA, "10");
			   Thread.sleep(3000);
			   click(btn_RateRangeA);
			   sendKeys(txt_MinimumUnitA, "5");
			   sendKeys(txt_Rate1A, "5");
			   Thread.sleep(3000);
			   click(btn_RateRangeSetupPlusA);
			   sendKeys(txt_RateRangeSetupStartA, "6");
			   sendKeys(txt_RateRangeSetupEndA, "0");
			   sendKeys(txt_Rate2A, "10");
			   Thread.sleep(3000);
			   click(btn_RateRangeSetupApplyA);
			   
			   Thread.sleep(3000);
			   click(btn_ResourcePlusBtnA);
			   Thread.sleep(3000);
			   click(btn_ReferenceCodeHABtn2A);
			   sendKeys(txt_ReferenceCodeHATxtA, ReferenceCodeHA2TxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ReferenceCodeHATxtA);
			   Thread.sleep(3000);
			   doubleClick(sel_ReferenceCodeHASel2A);
			   
			   sendKeys(txt_OpeningUnit2A, "10");
			   Thread.sleep(3000);
			   click(btn_RateRange2A);
			   sendKeys(txt_MinimumUnitA, "5");
			   sendKeys(txt_Rate1A, "5");
			   Thread.sleep(3000);
			   click(btn_RateRangeSetupPlusA);
			   sendKeys(txt_RateRangeSetupStartA, "6");
			   sendKeys(txt_RateRangeSetupEndA, "0");
			   sendKeys(txt_Rate2A, "10");
			   Thread.sleep(3000);
			   click(btn_RateRangeSetupApplyA);

			   Thread.sleep(3000);
			   if (isDisplayed(txt_Resource1A) && isDisplayed(txt_Resource2A)){
			        writeTestResults("Verify that  hire agreemnt can be create by adding multiple resources", "hire agreemnt can be create by adding multiple resources",
						"hire agreemnt can be create by adding multiple resources Successfully", "pass");
	           } else {
	        	    writeTestResults("Verify that  hire agreemnt can be create by adding multiple resources", "hire agreemnt can be create by adding multiple resources",
							"hire agreemnt can be create by adding multiple resources Fail", "fail");
	           }
			   
			   Thread.sleep(3000);
			   click(btn_DraftA);
			   Thread.sleep(5000);
			   if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
							"User Can draft the Hire Agreement Form Successfully", "pass");
			   } else {
					writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
							"User Can draft the Hire Agreement Form Fail", "fail");
			   }

			   Thread.sleep(3000);
			   click(btn_ReleaseA);
		       Thread.sleep(3000);
		       trackCode = getText(txt_trackCodeA);
			   Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that able to Release the Hire Agreement Form", "User Can Release the Hire Agreement Form",
						"User Can Release the Hire Agreement Form Successfully", "pass");
				} else {
					writeTestResults("Verify that able to Release the Hire Agreement Form", "User Can Release the Hire Agreement Form",
							"User Can Release the Hire Agreement Form Fail", "fail");
				}
		   }
		   
		   // Service_HA_017
		   public void VerifyThatUserAbleToCreateAnHireAgreementViaCopyFromOption() throws Exception {
			   
			   VerifyThatUserCanNavigateToTheHireAgreementForm();
			   Thread.sleep(3000);
			   click(btn_CopyFromA);
			   Thread.sleep(3000);
			   sendKeys(txt_CopyFromSerchHATxtA, HireAgreementNoA);
			   Thread.sleep(3000);
			   pressEnter(txt_CopyFromSerchHATxtA);
			   Thread.sleep(3000);
			   doubleClick(sel_CopyFromSerchHASelA);
			   
			   Thread.sleep(3000);
			   LocalTime obj1 = LocalTime.now();
			   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj1);
			   
			   Thread.sleep(3000);
			   JavascriptExecutor j7 = (JavascriptExecutor)driver;
			   j7.executeScript("$(\"#txtContractDate\").datepicker(\"setDate\", new Date())");
			   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker(\"setDate\", new Date())");
			   j7.executeScript("$(\"#txtEffectiveTo\").datepicker(\"setDate\", new Date())");
			   
			   Thread.sleep(3000);
			   click(btn_DraftA);
			   Thread.sleep(5000);
			   if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
							"User Can draft the Hire Agreement Form Successfully", "pass");
			   } else {
					writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
							"User Can draft the Hire Agreement Form Fail", "fail");
			   }
			   
				Thread.sleep(3000);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
						writeTestResults("Verify that user able to create an Hire Agreement via Copy From Option", "User Can create an Hire Agreement via Copy From Option",
							"User Can create an Hire Agreement via Copy From Option Successfully", "pass");
				} else {
						writeTestResults("Verify that user able to create an Hire Agreement via Copy From Option", "User Can create an Hire Agreement via Copy From Option",
								"User Can create an Hire Agreement via Copy From Option Fail", "fail");
				}
		   }
		   
		   // Service_HA_018
		   public void VerifyThatResourceDropDownFieldInTheActulUpdateLookUpIsDisplayedTheAllTheResourcesInHireAgreement() throws Exception {
			   
			   VerifyThatHireAgreemntCanBeCreateByAddingMultipleResources();
			   Thread.sleep(3000);
			   click(btn_ActionBtnA);
			   Thread.sleep(3000);
			   click(btn_ActualUpdateHABtnA);
			   
			   Thread.sleep(3000);
			    Select dropdown = new Select(driver.findElement(By.xpath(txt_cboxActualResourcesA)));

			    //Get all options
			    List<WebElement> elements = dropdown.getOptions();

				String ReferenceCodeHA1A=elements.get(0).getText();
				String ReferenceCodeHA2A=elements.get(1).getText();
				
				Thread.sleep(5000);
				if (ReferenceCodeHA1A.contentEquals(ReferenceCodeHA1TxtA) && ReferenceCodeHA2A.contentEquals(ReferenceCodeHA2TxtA)) {
						writeTestResults("Verify that Resource drop down field in the \"Actul Update\" look up is displayed the all the resources in Hire Agreement", "Resource drop down field in the \"Actul Update\" look up is displayed the all the resources in Hire Agreement",
							"Resource drop down field in the \"Actul Update\" look up is displayed the all the resources in Hire Agreement Successfully", "pass");
				} else {
						writeTestResults("Verify that Resource drop down field in the \"Actul Update\" look up is displayed the all the resources in Hire Agreement", "Resource drop down field in the \"Actul Update\" look up is displayed the all the resources in Hire Agreement",
								"Resource drop down field in the \"Actul Update\" look up is displayed the all the resources in Hire Agreement Fail", "fail");
				}
		   }
		   
		   // Service_HA_019_020_021
		   public void VerifyThatUserAbleToActualUpdateTheAllResourcesInTheHireAgreement() throws Exception {
			   
			   VerifyThatHireAgreemntCanBeCreateByAddingMultipleResources();
			   Thread.sleep(3000);
			   click(btn_ActionBtnA);
			   Thread.sleep(3000);
			   click(btn_ActualUpdateHABtnA);
			   
			   selectText(txt_cboxActualResourcesA, ReferenceCodeHA1TxtA);
			   sendKeys(txt_ActualUnitA, "2");
			   Thread.sleep(3000);
			   click(btn_CheckoutHAA);
			   
			   Thread.sleep(3000);
			   JavascriptExecutor j1 = (JavascriptExecutor) driver;
			   j1.executeScript("$(\"#divTaskcusAd\").parent().find('.dialogbox-buttonarea > .button').click()");
			   
			   Thread.sleep(3000);
			   click(btn_ActionBtnA);
			   Thread.sleep(3000);
			   click(btn_ActualUpdateHABtnA);
			   
			   selectText(txt_cboxActualResourcesA, ReferenceCodeHA2TxtA);
			   sendKeys(txt_ActualUnitA, "2");
			   Thread.sleep(3000);
			   click(btn_CheckoutHAA);
			   
			   Thread.sleep(3000);
			   j1.executeScript("$(\"#divTaskcusAd\").parent().find('.dialogbox-buttonarea > .button').click()");
			   
			   Thread.sleep(3000);
			   click(btn_ActionBtnA);
			   Thread.sleep(3000);
			   click(btn_ActualUpdateHABtnA);
			   
			   selectText(txt_cboxActualResourcesA, ReferenceCodeHA1TxtA);
			   Thread.sleep(3000);
//			   String ReferenceCodeHA1ActualUnitA = getAttribute(txt_ActualUnitA, "disabled");
			   String ReferenceCodeHA1ActualUnitA= driver.findElement(By.xpath(txt_ActualUnitA)).getAttribute("value");
			   selectText(txt_cboxActualResourcesA, ReferenceCodeHA2TxtA);
			   Thread.sleep(3000);
//			   String ReferenceCodeHA2ActualUnitA = getAttribute(txt_ActualUnitA, "disabled");
			   String ReferenceCodeHA2ActualUnitA= driver.findElement(By.xpath(txt_ActualUnitA)).getAttribute("value");
			   
			   Thread.sleep(3000);
				if(ReferenceCodeHA1ActualUnitA.equals("2.000000") && ReferenceCodeHA2ActualUnitA.equals("2.000000")){
					writeTestResults("Verify that User able to Actual Update the All resources in the Hire agreement ", 
							"User Can Actual Update the All resources in the Hire agreement",
							"User Can Actual Update the All resources in the Hire agreement Successfully", "pass");
				} else {
					writeTestResults("Verify that User able to Actual Update the All resources in the Hire agreement ", 
							"User Can Actual Update the All resources in the Hire agreement",
							"User Can Actual Update the All resources in the Hire agreement Fail", "fail");
				}
				
				selectText(txt_cboxActualResourcesA, ReferenceCodeHA1TxtA);
				Thread.sleep(3000);
				String ReferenceCodeHA1ActualUpdateStatusA = getText(txt_ActualUpdateStatusA);
				
				selectText(txt_cboxActualResourcesA, ReferenceCodeHA2TxtA);
				Thread.sleep(3000);
				String ReferenceCodeHA2ActualUpdateStatusA = getText(txt_ActualUpdateStatusA);
				
				Thread.sleep(3000);
				if(ReferenceCodeHA1ActualUpdateStatusA.contentEquals("Pending") && ReferenceCodeHA2ActualUpdateStatusA.contentEquals("Pending")){
					writeTestResults("Verify that 'Status' column in the actual update look up is displayed as the 'Pending' just after the actual update", 
							"'Status' column in the actual update look up is displayed as the 'Pending' just after the actual update",
							"'Status' column in the actual update look up is displayed as the 'Pending' just after the actual update Successfully", "pass");
				} else {
					writeTestResults("Verify that 'Status' column in the actual update look up is displayed as the 'Pending' just after the actual update", 
							"'Status' column in the actual update look up is displayed as the 'Pending' just after the actual update",
							"'Status' column in the actual update look up is displayed as the 'Pending' just after the actual update Fail", "fail");
				}
			   pageRefersh();
			   Thread.sleep(3000);
			   click(btn_ActionBtnA);
			   Thread.sleep(3000);
			   if(!isDisplayed(btn_ReverseHABtnA)){
					writeTestResults("Verification of Hire Agreement cannot reverse when having an actual update record", 
							"Hire Agreement cannot reverse when having an actual update record",
							"Hire Agreement cannot reverse when having an actual update record Successfully", "pass");
			   } else {
					writeTestResults("Verification of Hire Agreement cannot reverse when having an actual update record", 
							"Hire Agreement cannot reverse when having an actual update record",
							"Hire Agreement cannot reverse when having an actual update record Fail", "fail");
			   }
		   }
		   
		   // Service_HA_022
		   public void VerifyThatNextBillingDateFieldIsUpdatingAccordingToTheSelectedScheduleTemplateAssumeScheduleTemplateIsaLastDateOfTheMonth() throws Exception {
			   
			   VerifyThatUserCanNavigateToTheHireAgreementForm();
			   
			   Thread.sleep(3000);
			   LocalTime obj1 = LocalTime.now();
			   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj1);
			   sendKeys(txt_TitleTxtA, "Testing Hire Agreement");
			   
			   Thread.sleep(3000);
			   JavascriptExecutor j7 = (JavascriptExecutor)driver;
			   j7.executeScript("$(\"#txtContractDate\").datepicker('setDate', \"2020/07/01\")");
			   
			   
			   selectText(txt_ContractGroupTxtA, "ContractGroupService");
			   
			   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker('setDate', \"2020/07/01\")");
			   j7.executeScript("$(\"#txtEffectiveTo\").datepicker('setDate', \"2020/07/31\")");
			   
			   sendKeys(txt_DescriptionTxtA, "Testing Hire Agreement");
			   
			   Thread.sleep(3000);
			   click(btn_CustomerAccountHABtnA);
			   sendKeys(txt_CustomerAccountHATxtA, CustomerAccountTextA);
			   Thread.sleep(3000);
			   pressEnter(txt_CustomerAccountHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_CustomerAccountHASelA);
			   
			   selectText(txt_SalesUnitHAA, SalesUnitA);
			   Thread.sleep(3000);
			   selectText(txt_HireBasisA, "Units");
			   
			   selectText(txt_BillingBasisA, "Monthly");
			   
			   Thread.sleep(3000);
			   click(btn_ScheduleCodeHABtnA);
			   sendKeys(txt_ScheduleCodeHATxtA, ScheduleCodeHA2TxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ScheduleCodeHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ReferenceCodeHA2SelA);
			   
			   Thread.sleep(3000);
			   click(btn_ResponsibleUserHABtnA);
			   sendKeys(txt_ResponsibleUserHATxtA, ResponsibleUserHATxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ResponsibleUserHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ResponsibleUserHASelA);

			   
			   Thread.sleep(3000);
			   click(btn_ResourceInformationTabA);
			   selectText(txt_TypeHAA, "Resource");
			   Thread.sleep(3000);
			   click(btn_ReferenceCodeHABtnA);
			   sendKeys(txt_ReferenceCodeHATxtA, ReferenceCodeHA1TxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ReferenceCodeHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ReferenceCodeHASelA);
			   
			   sendKeys(txt_OpeningUnitA, "10");
			   Thread.sleep(3000);
			   click(btn_RateRangeA);
			   sendKeys(txt_MinimumUnitA, "5");
			   sendKeys(txt_Rate1A, "5");
			   Thread.sleep(3000);
			   click(btn_RateRangeSetupPlusA);
			   sendKeys(txt_RateRangeSetupStartA, "6");
			   sendKeys(txt_RateRangeSetupEndA, "0");
			   sendKeys(txt_Rate2A, "10");
			   Thread.sleep(3000);
			   click(btn_RateRangeSetupApplyA);
			   
			   Thread.sleep(3000);
			   click(btn_DraftA);
			   Thread.sleep(5000);
			   if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
							"User Can draft the Hire Agreement Form Successfully", "pass");
			   } else {
					writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
							"User Can draft the Hire Agreement Form Fail", "fail");
			   }

			   Thread.sleep(3000);
			   click(btn_ReleaseA);
			   Thread.sleep(3000);
			   trackCode = getText(txt_trackCodeA);
			   Thread.sleep(5000);
			   if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that able to Release the Hire Agreement Form", "User Can Release the Hire Agreement Form",
							"User Can Release the Hire Agreement Form Successfully", "pass");
			   } else {
					writeTestResults("Verify that able to Release the Hire Agreement Form", "User Can Release the Hire Agreement Form",
								"User Can Release the Hire Agreement Form Fail", "fail");
			   }
			   
			   Thread.sleep(3000);
			   String NextBillingDateA = getText(txt_NextBillingDateA);
			   String EffectiveToDateA = getText(txt_EffectiveToDateA);
			   
			   Thread.sleep(5000);
			   if (EffectiveToDateA.contentEquals(NextBillingDateA)) {
					writeTestResults("Verify that \"Next Billing Date\" field is updating according to the selected schedule template  (Assume schedule template is a last date of the month)", 
							"\"Next Billing Date\" field is updating according to the selected schedule template  (Assume schedule template is a last date of the month)",
							"\"Next Billing Date\" field is updating according to the selected schedule template  (Assume schedule template is a last date of the month) Successfully", "pass");
			   } else {
					writeTestResults("Verify that \"Next Billing Date\" field is updating according to the selected schedule template  (Assume schedule template is a last date of the month)",
							"\"Next Billing Date\" field is updating according to the selected schedule template  (Assume schedule template is a last date of the month)",
								"\"Next Billing Date\" field is updating according to the selected schedule template  (Assume schedule template is a last date of the month) Fail", "fail");
			   }
		   }
		   
		   // Service_HA_023
		   public void VerifyThatNextBillingDateFieldIsUpdatingAccordingToTheSelectedScheduleTemplateAssumeScheduleTemplateIsaFirstDateOfTheMonth() throws Exception {
			   
			   VerifyThatUserCanNavigateToTheHireAgreementForm();
			   
			   Thread.sleep(3000);
			   LocalTime obj1 = LocalTime.now();
			   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj1);
			   sendKeys(txt_TitleTxtA, "Testing Hire Agreement");
			   
			   Thread.sleep(3000);
			   JavascriptExecutor j7 = (JavascriptExecutor)driver;
			   j7.executeScript("$(\"#txtContractDate\").datepicker('setDate', \"2020/07/01\")");
			   
			   
			   selectText(txt_ContractGroupTxtA, "ContractGroupService");
			   
			   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker('setDate', \"2020/07/01\")");
			   j7.executeScript("$(\"#txtEffectiveTo\").datepicker('setDate', \"2020/08/31\")");
			   
			   sendKeys(txt_DescriptionTxtA, "Testing Hire Agreement");
			   
			   Thread.sleep(3000);
			   click(btn_CustomerAccountHABtnA);
			   sendKeys(txt_CustomerAccountHATxtA, CustomerAccountTextA);
			   Thread.sleep(3000);
			   pressEnter(txt_CustomerAccountHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_CustomerAccountHASelA);
			   
			   selectText(txt_SalesUnitHAA, SalesUnitA);
			   Thread.sleep(3000);
			   selectText(txt_HireBasisA, "Units");
			   
			   selectText(txt_BillingBasisA, "Monthly");
			   
			   Thread.sleep(3000);
			   click(btn_ScheduleCodeHABtnA);
			   sendKeys(txt_ScheduleCodeHATxtA, ScheduleCodeHA3TxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ScheduleCodeHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ReferenceCodeHA3SelA);
			   
			   Thread.sleep(3000);
			   click(btn_ResponsibleUserHABtnA);
			   sendKeys(txt_ResponsibleUserHATxtA, ResponsibleUserHATxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ResponsibleUserHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ResponsibleUserHASelA);

			   
			   Thread.sleep(3000);
			   click(btn_ResourceInformationTabA);
			   selectText(txt_TypeHAA, "Resource");
			   Thread.sleep(3000);
			   click(btn_ReferenceCodeHABtnA);
			   sendKeys(txt_ReferenceCodeHATxtA, ReferenceCodeHA1TxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ReferenceCodeHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ReferenceCodeHASelA);
			   
			   sendKeys(txt_OpeningUnitA, "10");
			   Thread.sleep(3000);
			   click(btn_RateRangeA);
			   sendKeys(txt_MinimumUnitA, "5");
			   sendKeys(txt_Rate1A, "5");
			   Thread.sleep(3000);
			   click(btn_RateRangeSetupPlusA);
			   sendKeys(txt_RateRangeSetupStartA, "6");
			   sendKeys(txt_RateRangeSetupEndA, "0");
			   sendKeys(txt_Rate2A, "10");
			   Thread.sleep(3000);
			   click(btn_RateRangeSetupApplyA);
			   
			   Thread.sleep(3000);
			   click(btn_DraftA);
			   Thread.sleep(5000);
			   if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
							"User Can draft the Hire Agreement Form Successfully", "pass");
			   } else {
					writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
							"User Can draft the Hire Agreement Form Fail", "fail");
			   }

			   Thread.sleep(3000);
			   click(btn_ReleaseA);
			   Thread.sleep(3000);
			   trackCode = getText(txt_trackCodeA);
			   Thread.sleep(5000);
			   if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that able to Release the Hire Agreement Form", "User Can Release the Hire Agreement Form",
							"User Can Release the Hire Agreement Form Successfully", "pass");
			   } else {
					writeTestResults("Verify that able to Release the Hire Agreement Form", "User Can Release the Hire Agreement Form",
								"User Can Release the Hire Agreement Form Fail", "fail");
			   }
			   
			   Thread.sleep(3000);
			   String NextBillingDateA = getText(txt_NextBillingDateA);
			   
			   Thread.sleep(5000);
			   if (NextBillingDateA.contentEquals("2020/08/01")) {
					writeTestResults("Verify that \"Next Billing Date\" field is updating according to the selected schedule template (Assume schedule template is a first date of the month)", 
							"\"Next Billing Date\" field is updating according to the selected schedule template (Assume schedule template is a first date of the month)",
							"\"Next Billing Date\" field is updating according to the selected schedule template (Assume schedule template is a first date of the month) Successfully", "pass");
			   } else {
					writeTestResults("Verify that \"Next Billing Date\" field is updating according to the selected schedule template (Assume schedule template is a first date of the month)",
							"\"Next Billing Date\" field is updating according to the selected schedule template (Assume schedule template is a first date of the month)",
								"\"Next Billing Date\" field is updating according to the selected schedule template (Assume schedule template is a first date of the month) Fail", "fail");
			   }
		   }
		   
		   // Service_HA_024
		   public void VerifyThatNextBillingDateFieldIsUpdatingAccordingToTheSelectedScheduleTemplateAssumeScheduleTemplateIsDaily() throws Exception {
			   
			   VerifyThatUserCanNavigateToTheHireAgreementForm();
			   
			   Thread.sleep(3000);
			   LocalTime obj1 = LocalTime.now();
			   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj1);
			   sendKeys(txt_TitleTxtA, "Testing Hire Agreement");
			   
			   Thread.sleep(3000);
			   JavascriptExecutor j7 = (JavascriptExecutor)driver;
			   j7.executeScript("$(\"#txtContractDate\").datepicker('setDate', \"2020/07/01\")");
			   
			   
			   selectText(txt_ContractGroupTxtA, "ContractGroupService");
			   
			   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker('setDate', \"2020/07/01\")");
			   j7.executeScript("$(\"#txtEffectiveTo\").datepicker('setDate', \"2020/07/31\")");
			   
			   sendKeys(txt_DescriptionTxtA, "Testing Hire Agreement");
			   
			   Thread.sleep(3000);
			   click(btn_CustomerAccountHABtnA);
			   sendKeys(txt_CustomerAccountHATxtA, CustomerAccountTextA);
			   Thread.sleep(3000);
			   pressEnter(txt_CustomerAccountHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_CustomerAccountHASelA);
			   
			   selectText(txt_SalesUnitHAA, SalesUnitA);
			   Thread.sleep(3000);
			   selectText(txt_HireBasisA, "Units");
			   
			   selectText(txt_BillingBasisA, "Daily");
			   
			   Thread.sleep(3000);
			   click(btn_ScheduleCodeHABtnA);
			   sendKeys(txt_ScheduleCodeHATxtA, ScheduleCodeHA4TxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ScheduleCodeHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ReferenceCodeHA4SelA);
			   
			   Thread.sleep(3000);
			   click(btn_ResponsibleUserHABtnA);
			   sendKeys(txt_ResponsibleUserHATxtA, ResponsibleUserHATxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ResponsibleUserHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ResponsibleUserHASelA);

			   
			   Thread.sleep(3000);
			   click(btn_ResourceInformationTabA);
			   selectText(txt_TypeHAA, "Resource");
			   Thread.sleep(3000);
			   click(btn_ReferenceCodeHABtnA);
			   sendKeys(txt_ReferenceCodeHATxtA, ReferenceCodeHA1TxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ReferenceCodeHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ReferenceCodeHASelA);
			   
			   sendKeys(txt_OpeningUnitA, "10");
			   Thread.sleep(3000);
			   click(btn_RateRangeA);
			   sendKeys(txt_MinimumUnitA, "5");
			   sendKeys(txt_Rate1A, "5");
			   Thread.sleep(3000);
			   click(btn_RateRangeSetupPlusA);
			   sendKeys(txt_RateRangeSetupStartA, "6");
			   sendKeys(txt_RateRangeSetupEndA, "0");
			   sendKeys(txt_Rate2A, "10");
			   Thread.sleep(3000);
			   click(btn_RateRangeSetupApplyA);
			   
			   Thread.sleep(3000);
			   click(btn_DraftA);
			   Thread.sleep(5000);
			   if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
							"User Can draft the Hire Agreement Form Successfully", "pass");
			   } else {
					writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
							"User Can draft the Hire Agreement Form Fail", "fail");
			   }

			   Thread.sleep(3000);
			   click(btn_ReleaseA);
			   Thread.sleep(3000);
			   trackCode = getText(txt_trackCodeA);
			   Thread.sleep(5000);
			   if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that able to Release the Hire Agreement Form", "User Can Release the Hire Agreement Form",
							"User Can Release the Hire Agreement Form Successfully", "pass");
			   } else {
					writeTestResults("Verify that able to Release the Hire Agreement Form", "User Can Release the Hire Agreement Form",
								"User Can Release the Hire Agreement Form Fail", "fail");
			   }
			   
			   Thread.sleep(3000);
			   String NextBillingDateA = getText(txt_NextBillingDateA);
			   String EffectiveFromDateA = getText(txt_EffectiveFromDateAfterReleaseA);
			   
			   Thread.sleep(5000);
			   if (NextBillingDateA.contentEquals(EffectiveFromDateA)) {
					writeTestResults("Verify that \"Next Billing Date\" field is updating according to the selected schedule template  (Assume schedule template is Daily)", 
							"\"Next Billing Date\" field is updating according to the selected schedule template  (Assume schedule template is Daily)",
							"\"Next Billing Date\" field is updating according to the selected schedule template  (Assume schedule template is Daily) Successfully", "pass");
			   } else {
					writeTestResults("Verify that \"Next Billing Date\" field is updating according to the selected schedule template  (Assume schedule template is Daily)",
							"\"Next Billing Date\" field is updating according to the selected schedule template  (Assume schedule template is Daily)",
								"\"Next Billing Date\" field is updating according to the selected schedule template  (Assume schedule template is Daily) Fail", "fail");
			   }
		   }
		   
		   // Service_HA_025_026
		   public void VerifyThatScheduleJobTaskIsAvailableForTheUserWhoIsSelectedInTheFieldCalledResponsibleUser() throws Exception {
			   
			   VerifyThatUserCanNavigateToTheHireAgreementForm();
			   
			   Thread.sleep(3000);
			   LocalTime obj1 = LocalTime.now();
			   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj1);
			   sendKeys(txt_TitleTxtA, "Testing Hire Agreement");
			   
			   Thread.sleep(3000);
			   JavascriptExecutor j7 = (JavascriptExecutor)driver;
			   j7.executeScript("$(\"#txtContractDate\").datepicker(\"setDate\", new Date())");
			   
			   
			   selectText(txt_ContractGroupTxtA, "ContractGroupService");
			   
			   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker(\"setDate\", new Date())");
			   j7.executeScript("$(\"#txtEffectiveTo\").datepicker(\"setDate\", new Date())");
			   
			   sendKeys(txt_DescriptionTxtA, "Testing Hire Agreement");
			   
			   Thread.sleep(3000);
			   click(btn_CustomerAccountHABtnA);
			   sendKeys(txt_CustomerAccountHATxtA, CustomerAccountTextA);
			   Thread.sleep(3000);
			   pressEnter(txt_CustomerAccountHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_CustomerAccountHASelA);
			   
			   selectText(txt_SalesUnitHAA, SalesUnitA);
			   
			   Thread.sleep(3000);
			   click(btn_ScheduleCodeHABtnA);
			   sendKeys(txt_ScheduleCodeHATxtA, ScheduleCodeHATxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ScheduleCodeHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ScheduleCodeHASelA);
			   
			   Thread.sleep(3000);
			   click(btn_ResponsibleUserHABtnA);
			   sendKeys(txt_ResponsibleUserHATxtA, ResponsibleUserHA2TxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ResponsibleUserHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ResponsibleUserHA2SelA);
			   Thread.sleep(3000);
			   selectText(txt_HireBasisA, "Units");
			   
			   Thread.sleep(3000);
			   click(btn_ResourceInformationTabA);
			   selectText(txt_TypeHAA, "Resource");
			   Thread.sleep(3000);
			   click(btn_ReferenceCodeHABtnA);
			   sendKeys(txt_ReferenceCodeHATxtA, ReferenceCodeHA1TxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ReferenceCodeHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ReferenceCodeHASelA);
			   
			   sendKeys(txt_OpeningUnitA, "10");
			   Thread.sleep(3000);
			   click(btn_RateRangeA);
			   sendKeys(txt_MinimumUnitA, "5");
			   sendKeys(txt_Rate1A, "5");
			   Thread.sleep(3000);
			   click(btn_RateRangeSetupPlusA);
			   sendKeys(txt_RateRangeSetupStartA, "6");
			   sendKeys(txt_RateRangeSetupEndA, "0");
			   sendKeys(txt_Rate2A, "10");
			   Thread.sleep(3000);
			   click(btn_RateRangeSetupApplyA);
			   
			   Thread.sleep(3000);
			   click(btn_DraftA);
			   Thread.sleep(5000);
			   if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
							"User Can draft the Hire Agreement Form Successfully", "pass");
			   } else {
					writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
							"User Can draft the Hire Agreement Form Fail", "fail");
			   }

			   Thread.sleep(3000);
			   click(btn_ReleaseA);
			   Thread.sleep(3000);
			   trackCode = getText(txt_trackCodeA);
			   String HANo = trackCode;
			   
			   Thread.sleep(5000);
			   if (isDisplayed(txt_PageRelease1A)) {
						writeTestResults("Verify that able to Release the Hire Agreement Form", "User Can Release the Hire Agreement Form",
							"User Can Release the Hire Agreement Form Successfully", "pass");
			   } else {
						writeTestResults("Verify that able to Release the Hire Agreement Form", "User Can Release the Hire Agreement Form",
								"User Can Release the Hire Agreement Form Fail", "fail");
			   }
				
			   Thread.sleep(3000);
			   click(btn_ActionBtnA);
			   Thread.sleep(3000);
			   click(btn_ActualUpdateHABtnA);
			   
			   selectText(txt_cboxActualResourcesA, ReferenceCodeHA1TxtA);
			   sendKeys(txt_ActualUnitA, "2");
			   Thread.sleep(3000);
			   click(btn_CheckoutHAA);
			   
			   Thread.sleep(3000);
			   JavascriptExecutor j1 = (JavascriptExecutor) driver;
			   j1.executeScript("$(\"#divTaskcusAd\").parent().find('.dialogbox-buttonarea > .button').click()");
			   
			   pageRefersh();
			   
			   WaitElement(btn_LogOutBtnA);
			   WaitClick(btn_LogOutBtnA);
			   click(btn_LogOutBtnA);
			   WaitElement(btn_SignOutBtnA);
			   WaitClick(btn_SignOutBtnA);
			   click(btn_SignOutBtnA);
			   
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				sendKeys(username, NewUserNameA);
				sendKeys(password, NewPasswordA);
				click(btn_login);

				if (isDisplayed(lnk_home)) {

					writeTestResults("User login", "User should navigate to the Home page of Entution",
							"user navigated to the home page", "pass");
				}

				else {
					writeTestResults("User login", "User should navigate to the Home page of Entution",
							"user cant navigate to the home page", "fail");

				}
				
				Thread.sleep(3000);
				click(btn_RolecenterA);
				Thread.sleep(3000);
				click(btn_ScheduledJobsA);
				switchWindow();
				Thread.sleep(3000);
				click(btn_scheduleCodePlusA);
				Thread.sleep(3000);
				click(btn_HireAgreementPlusA);
			   
				if (isDisplayed(txt_HANoA.replace("HANo", HANo))) {

					writeTestResults("Verify that Schedule job task is available for the user who is selected in the field called \" Responsible User\" ", 
							"Schedule job task is available for the user who is selected in the field called \" Responsible User\" ",
							"Schedule job task is available for the user who is selected in the field called \" Responsible User\" Successfully", "pass");
				}

				else {
					writeTestResults("Verify that Schedule job task is available for the user who is selected in the field called \" Responsible User\" ", 
							"Schedule job task is available for the user who is selected in the field called \" Responsible User\" ",
							"Schedule job task is available for the user who is selected in the field called \" Responsible User\" Fail", "fail");
				}
				
				Thread.sleep(3000);
				click(btn_checkboxRelevantHireAgreementA.replace("HANo", HANo));
				Thread.sleep(3000);
				click(btn_RunButtonRelevantHireAgreementA);
				Thread.sleep(3000);
				if (isDisplayed(btn_LinkReleasedServiceInvoiceA)) {

					writeTestResults("Verify that message look up is displayed with the link for released service invoice", 
							"message look up is displayed with the link for released service invoice",
							"message look up is displayed with the link for released service invoice Successfully", "pass");
					click(btn_LinkReleasedServiceInvoiceA);
				}

				else {
					writeTestResults("Verify that message look up is displayed with the link for released service invoice", 
							"message look up is displayed with the link for released service invoice",
							"message look up is displayed with the link for released service invoice Fail", "fail");
				}
				
				switchWindow();
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
							writeTestResults("Verify that Service Invoice get Released after runing the schedule job", 
									"Service Invoice get Released after runing the schedule job",
								"Service Invoice get Released after runing the schedule job Successfully", "pass");
				} else {
							writeTestResults("Verify that Service Invoice get Released after runing the schedule job", 
									"Service Invoice get Released after runing the schedule job",
									"Service Invoice get Released after runing the schedule job Fail", "fail");
				}
		   }
		   
		   // Service_HA_027
		   public void VerifyThatStatusColumnOfTheActualUpdateLookUpIsChangingToPendingToBilledAfterRunningTheServiceInvoice() throws Exception {
			   
			   VerifyThatUserCanNavigateToTheHireAgreementForm();
			   
			   Thread.sleep(3000);
			   LocalTime obj1 = LocalTime.now();
			   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj1);
			   sendKeys(txt_TitleTxtA, "Testing Hire Agreement");
			   
			   Thread.sleep(3000);
			   JavascriptExecutor j7 = (JavascriptExecutor)driver;
			   j7.executeScript("$(\"#txtContractDate\").datepicker(\"setDate\", new Date())");
			   
			   
			   selectText(txt_ContractGroupTxtA, "ContractGroupService");
			   
			   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker(\"setDate\", new Date())");
			   j7.executeScript("$(\"#txtEffectiveTo\").datepicker('setDate', \"2023/07/31\")");
			   
			   sendKeys(txt_DescriptionTxtA, "Testing Hire Agreement");
			   
			   Thread.sleep(3000);
			   click(btn_CustomerAccountHABtnA);
			   sendKeys(txt_CustomerAccountHATxtA, CustomerAccountTextA);
			   Thread.sleep(3000);
			   pressEnter(txt_CustomerAccountHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_CustomerAccountHASelA);
			   
			   selectText(txt_SalesUnitHAA, SalesUnitA);
			   
			   Thread.sleep(3000);
			   click(btn_ScheduleCodeHABtnA);
			   sendKeys(txt_ScheduleCodeHATxtA, ScheduleCodeHATxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ScheduleCodeHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ScheduleCodeHASelA);
			   
			   Thread.sleep(3000);
			   click(btn_ResponsibleUserHABtnA);
			   sendKeys(txt_ResponsibleUserHATxtA, ResponsibleUserHATxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ResponsibleUserHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ResponsibleUserHASelA);
			   Thread.sleep(3000);
			   selectText(txt_HireBasisA, "Units");
			   
			   Thread.sleep(3000);
			   click(btn_ResourceInformationTabA);
			   selectText(txt_TypeHAA, "Resource");
			   Thread.sleep(3000);
			   click(btn_ReferenceCodeHABtnA);
			   sendKeys(txt_ReferenceCodeHATxtA, ReferenceCodeHA1TxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ReferenceCodeHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ReferenceCodeHASelA);
			   
			   sendKeys(txt_OpeningUnitA, "10");
			   Thread.sleep(3000);
			   click(btn_RateRangeA);
			   sendKeys(txt_MinimumUnitA, "5");
			   sendKeys(txt_Rate1A, "5");
			   Thread.sleep(3000);
			   click(btn_RateRangeSetupPlusA);
			   sendKeys(txt_RateRangeSetupStartA, "6");
			   sendKeys(txt_RateRangeSetupEndA, "0");
			   sendKeys(txt_Rate2A, "10");
			   Thread.sleep(3000);
			   click(btn_RateRangeSetupApplyA);
			   
			   Thread.sleep(3000);
			   click(btn_DraftA);
			   Thread.sleep(5000);
			   if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
							"User Can draft the Hire Agreement Form Successfully", "pass");
			   } else {
					writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
							"User Can draft the Hire Agreement Form Fail", "fail");
			   }

			   Thread.sleep(3000);
			   click(btn_ReleaseA);
			   Thread.sleep(3000);
			   trackCode = getText(txt_trackCodeA);
			   String HANo = trackCode;
			   
			   Thread.sleep(5000);
			   if (isDisplayed(txt_PageRelease1A)) {
						writeTestResults("Verify that able to Release the Hire Agreement Form", "User Can Release the Hire Agreement Form",
							"User Can Release the Hire Agreement Form Successfully", "pass");
			   } else {
						writeTestResults("Verify that able to Release the Hire Agreement Form", "User Can Release the Hire Agreement Form",
								"User Can Release the Hire Agreement Form Fail", "fail");
			   }
				
			   Thread.sleep(3000);
			   click(btn_ActionBtnA);
			   Thread.sleep(3000);
			   click(btn_ActualUpdateHABtnA);
			   
			   selectText(txt_cboxActualResourcesA, ReferenceCodeHA1TxtA);
			   sendKeys(txt_ActualUnitA, "2");
			   Thread.sleep(3000);
			   click(btn_CheckoutHAA);
			   
			   Thread.sleep(3000);
			   JavascriptExecutor j1 = (JavascriptExecutor) driver;
			   j1.executeScript("$(\"#divTaskcusAd\").parent().find('.dialogbox-buttonarea > .button').click()");
			   
			   Thread.sleep(3000);
			   click(btn_EntutionHeaderA);
			   Thread.sleep(3000);
			   click(btn_RolecenterA);
			   Thread.sleep(3000);
			   click(btn_ScheduledJobsA);
			   switchWindow();
			   Thread.sleep(3000);
			   click(btn_scheduleCodePlusA);
			   Thread.sleep(3000);
			   click(btn_HireAgreementPlusA);
			   
			   if (isDisplayed(txt_HANoA.replace("HANo", HANo))) {

					writeTestResults("Verify that Schedule job task is available for the user who is selected in the field called \" Responsible User\" ", 
							"Schedule job task is available for the user who is selected in the field called \" Responsible User\" ",
							"Schedule job task is available for the user who is selected in the field called \" Responsible User\" Successfully", "pass");
			   }

			   else {
					writeTestResults("Verify that Schedule job task is available for the user who is selected in the field called \" Responsible User\" ", 
							"Schedule job task is available for the user who is selected in the field called \" Responsible User\" ",
							"Schedule job task is available for the user who is selected in the field called \" Responsible User\" Fail", "fail");
			   }
				
			   Thread.sleep(3000);
			   click(btn_checkboxRelevantHireAgreementA.replace("HANo", HANo));
			   mouseMove(btn_RunButtonRelevantHireAgreementA);
			   WaitClick(btn_RunButtonRelevantHireAgreementA);
			   click(btn_RunButtonRelevantHireAgreementA);
			   Thread.sleep(3000);
			   if (isDisplayed(btn_LinkReleasedServiceInvoiceA)) {

					writeTestResults("Verify that message look up is displayed with the link for released service invoice", 
							"message look up is displayed with the link for released service invoice",
							"message look up is displayed with the link for released service invoice Successfully", "pass");
					click(btn_LinkReleasedServiceInvoiceA);
			   }

			   else {
					writeTestResults("Verify that message look up is displayed with the link for released service invoice", 
							"message look up is displayed with the link for released service invoice",
							"message look up is displayed with the link for released service invoice Fail", "fail");
			   }
				
			   switchWindow();
			   Thread.sleep(3000);
			   trackCode = getText(txt_trackCodeA);
				
			   Thread.sleep(5000);
			   if (isDisplayed(txt_PageRelease1A)) {
							writeTestResults("Verify that Service Invoice get Released after runing the schedule job", 
									"Service Invoice get Released after runing the schedule job",
								"Service Invoice get Released after runing the schedule job Successfully", "pass");
			   } else {
							writeTestResults("Verify that Service Invoice get Released after runing the schedule job", 
									"Service Invoice get Released after runing the schedule job",
									"Service Invoice get Released after runing the schedule job Fail", "fail");
			   }
			   
			   Thread.sleep(6000);
			   clickNavigation();
			   Thread.sleep(3000);
			   click(btn_HireAgreementBtnA);
			   Thread.sleep(3000);
			   sendKeys(txt_HireAgreementSerchTxtA, HANo);
			   Thread.sleep(3000);
			   pressEnter(txt_HireAgreementSerchTxtA);
			   Thread.sleep(3000);
			   doubleClick(sel_HireAgreementSerchSelA.replace("HANo", HANo));
			   
			   Thread.sleep(3000);
			   click(btn_ActionBtnA);
			   Thread.sleep(3000);
			   click(btn_ActualUpdateHABtnA);
			   
				selectText(txt_cboxActualResourcesA, ReferenceCodeHA1TxtA);
				Thread.sleep(3000);
				String ReferenceCodeHA1ActualUpdateStatusA = getText(txt_ActualUpdateStatusA);
				
				Thread.sleep(3000);
				if(ReferenceCodeHA1ActualUpdateStatusA.contentEquals("Billed")){
					writeTestResults("Verify that status column of the Actual update look up is changing to pending to \"Billed\", after running the service invoice", 
							"status column of the Actual update look up is changing to pending to \"Billed\", after running the service invoice",
							"status column of the Actual update look up is changing to pending to \"Billed\", after running the service invoice Successfully", "pass");
				} else {
					writeTestResults("Verify that status column of the Actual update look up is changing to pending to \"Billed\", after running the service invoice", 
							"status column of the Actual update look up is changing to pending to \"Billed\", after running the service invoice",
							"status column of the Actual update look up is changing to pending to \"Billed\", after running the service invoice Fail", "fail");
				}
		   }
		   
		   // Service_HA_028
		   public void VerifyThatActualUpdatedAmountIsDisplayedInTheServiceInvoiceTotalHeader() throws Exception {
			   
			   VerifyThatUserCanNavigateToTheHireAgreementForm();
			   
			   Thread.sleep(3000);
			   LocalTime obj1 = LocalTime.now();
			   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj1);
			   sendKeys(txt_TitleTxtA, "Testing Hire Agreement");
			   
			   Thread.sleep(3000);
			   JavascriptExecutor j7 = (JavascriptExecutor)driver;
			   j7.executeScript("$(\"#txtContractDate\").datepicker(\"setDate\", new Date())");
			   
			   
			   selectText(txt_ContractGroupTxtA, "ContractGroupService");
			   
			   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker(\"setDate\", new Date())");
			   j7.executeScript("$(\"#txtEffectiveTo\").datepicker(\"setDate\", new Date())");
			   
			   sendKeys(txt_DescriptionTxtA, "Testing Hire Agreement");
			   
			   Thread.sleep(3000);
			   click(btn_CustomerAccountHABtnA);
			   sendKeys(txt_CustomerAccountHATxtA, CustomerAccountTextA);
			   Thread.sleep(3000);
			   pressEnter(txt_CustomerAccountHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_CustomerAccountHASelA);
			   
			   selectText(txt_SalesUnitHAA, SalesUnitA);
			   
			   Thread.sleep(3000);
			   click(btn_ScheduleCodeHABtnA);
			   sendKeys(txt_ScheduleCodeHATxtA, ScheduleCodeHATxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ScheduleCodeHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ScheduleCodeHASelA);
			   
			   Thread.sleep(3000);
			   click(btn_ResponsibleUserHABtnA);
			   sendKeys(txt_ResponsibleUserHATxtA, ResponsibleUserHA2TxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ResponsibleUserHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ResponsibleUserHA2SelA);
			   Thread.sleep(3000);
			   selectText(txt_HireBasisA, "Units");
			   
			   Thread.sleep(3000);
			   click(btn_ResourceInformationTabA);
			   selectText(txt_TypeHAA, "Resource");
			   Thread.sleep(3000);
			   click(btn_ReferenceCodeHABtnA);
			   sendKeys(txt_ReferenceCodeHATxtA, ReferenceCodeHA1TxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ReferenceCodeHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ReferenceCodeHASelA);
			   
			   sendKeys(txt_OpeningUnitA, "10");
			   Thread.sleep(3000);
			   click(btn_RateRangeA);
			   sendKeys(txt_MinimumUnitA, "5");
			   sendKeys(txt_Rate1A, "5");
			   Thread.sleep(3000);
			   click(btn_RateRangeSetupPlusA);
			   sendKeys(txt_RateRangeSetupStartA, "6");
			   sendKeys(txt_RateRangeSetupEndA, "0");
			   sendKeys(txt_Rate2A, "10");
			   Thread.sleep(3000);
			   click(btn_RateRangeSetupApplyA);
			   
			   Thread.sleep(3000);
			   click(btn_DraftA);
			   Thread.sleep(5000);
			   if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
							"User Can draft the Hire Agreement Form Successfully", "pass");
			   } else {
					writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
							"User Can draft the Hire Agreement Form Fail", "fail");
			   }

			   Thread.sleep(3000);
			   click(btn_ReleaseA);
			   Thread.sleep(3000);
			   trackCode = getText(txt_trackCodeA);
			   String HANo = trackCode;
			   
			   Thread.sleep(5000);
			   if (isDisplayed(txt_PageRelease1A)) {
						writeTestResults("Verify that able to Release the Hire Agreement Form", "User Can Release the Hire Agreement Form",
							"User Can Release the Hire Agreement Form Successfully", "pass");
			   } else {
						writeTestResults("Verify that able to Release the Hire Agreement Form", "User Can Release the Hire Agreement Form",
								"User Can Release the Hire Agreement Form Fail", "fail");
			   }
				
			   Thread.sleep(3000);
			   click(btn_ActionBtnA);
			   Thread.sleep(3000);
			   click(btn_ActualUpdateHABtnA);
			   
			   selectText(txt_cboxActualResourcesA, ReferenceCodeHA1TxtA);
			   sendKeys(txt_ActualUnitA, "2");
			   Thread.sleep(3000);
			   click(btn_CheckoutHAA);
			   Thread.sleep(3000);
			   String ActualUpdatedAmountA= driver.findElement(By.xpath(txt_ActualUpdatedAmountA)).getAttribute("value");
			   Thread.sleep(3000);
			   JavascriptExecutor j1 = (JavascriptExecutor) driver;
			   j1.executeScript("$(\"#divTaskcusAd\").parent().find('.dialogbox-buttonarea > .button').click()");
			   
			   Thread.sleep(3000);
			   click(btn_LogOutBtnA);
			   Thread.sleep(3000);
			   click(btn_SignOutBtnA);
			   
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				sendKeys(username, NewUserNameA);
				sendKeys(password, NewPasswordA);
				click(btn_login);

				if (isDisplayed(lnk_home)) {

					writeTestResults("User login", "User should navigate to the Home page of Entution",
							"user navigated to the home page", "pass");
				}

				else {
					writeTestResults("User login", "User should navigate to the Home page of Entution",
							"user cant navigate to the home page", "fail");

				}
				
				Thread.sleep(3000);
				click(btn_RolecenterA);
				Thread.sleep(3000);
				click(btn_ScheduledJobsA);
				switchWindow();
				Thread.sleep(3000);
				click(btn_scheduleCodePlusA);
				Thread.sleep(3000);
				click(btn_HireAgreementPlusA);
			   
				if (isDisplayed(txt_HANoA.replace("HANo", HANo))) {

					writeTestResults("Verify that Schedule job task is available for the user who is selected in the field called \" Responsible User\" ", 
							"Schedule job task is available for the user who is selected in the field called \" Responsible User\" ",
							"Schedule job task is available for the user who is selected in the field called \" Responsible User\" Successfully", "pass");
				}

				else {
					writeTestResults("Verify that Schedule job task is available for the user who is selected in the field called \" Responsible User\" ", 
							"Schedule job task is available for the user who is selected in the field called \" Responsible User\" ",
							"Schedule job task is available for the user who is selected in the field called \" Responsible User\" Fail", "fail");
				}
				
				Thread.sleep(3000);
				click(btn_checkboxRelevantHireAgreementA.replace("HANo", HANo));
				Thread.sleep(3000);
				click(btn_RunButtonRelevantHireAgreementA);
				Thread.sleep(3000);
				if (isDisplayed(btn_LinkReleasedServiceInvoiceA)) {

					writeTestResults("Verify that message look up is displayed with the link for released service invoice", 
							"message look up is displayed with the link for released service invoice",
							"message look up is displayed with the link for released service invoice Successfully", "pass");
					click(btn_LinkReleasedServiceInvoiceA);
				}

				else {
					writeTestResults("Verify that message look up is displayed with the link for released service invoice", 
							"message look up is displayed with the link for released service invoice",
							"message look up is displayed with the link for released service invoice Fail", "fail");
				}
				
				switchWindow();
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				
				Thread.sleep(5000);
				if (isDisplayed(txt_PageRelease1A)) {
							writeTestResults("Verify that Service Invoice get Released after runing the schedule job", 
									"Service Invoice get Released after runing the schedule job",
								"Service Invoice get Released after runing the schedule job Successfully", "pass");
				} else {
							writeTestResults("Verify that Service Invoice get Released after runing the schedule job", 
									"Service Invoice get Released after runing the schedule job",
									"Service Invoice get Released after runing the schedule job Fail", "fail");
				}
				Thread.sleep(3000);
				String ServiceInvoiceTotalHeaderA = getText(txt_ServiceInvoiceTotalHeaderA);
				
				if (ServiceInvoiceTotalHeaderA.contentEquals(ActualUpdatedAmountA)) {
					writeTestResults("Verify that Actual  updated amount is displayed in the service invoice \"TOTAL\" Header", 
							"Actual  updated amount is displayed in the service invoice \"TOTAL\" Header",
						"Actual  updated amount is displayed in the service invoice \"TOTAL\" Header Successfully", "pass");
				} else {
					writeTestResults("Verify that Actual  updated amount is displayed in the service invoice \"TOTAL\" Header", 
							"Actual  updated amount is displayed in the service invoice \"TOTAL\" Header",
							"Actual  updated amount is displayed in the service invoice \"TOTAL\" Header Fail", "fail");
				}
		   }
		   
		   // Service_HA_029
		   public void VerificationOfServiceInvoiceOfHireAgreementAvailableWithTotalAmountInTheActualUpdate() throws Exception {
			   
			   VerifyThatUserCanNavigateToTheHireAgreementForm();
			   
			   Thread.sleep(3000);
			   LocalTime obj1 = LocalTime.now();
			   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj1);
			   sendKeys(txt_TitleTxtA, "Testing Hire Agreement");
			   
			   Thread.sleep(3000);
			   JavascriptExecutor j7 = (JavascriptExecutor)driver;
			   j7.executeScript("$(\"#txtContractDate\").datepicker(\"setDate\", new Date())");
			   
			   
			   selectText(txt_ContractGroupTxtA, "ContractGroupService");
			   
			   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker(\"setDate\", new Date())");
			   j7.executeScript("$(\"#txtEffectiveTo\").datepicker('setDate', \"2023/07/31\")");
			   
			   sendKeys(txt_DescriptionTxtA, "Testing Hire Agreement");
			   
			   Thread.sleep(3000);
			   click(btn_CustomerAccountHABtnA);
			   sendKeys(txt_CustomerAccountHATxtA, CustomerAccountTextA);
			   Thread.sleep(3000);
			   pressEnter(txt_CustomerAccountHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_CustomerAccountHASelA);
			   
			   selectText(txt_SalesUnitHAA, SalesUnitA);
			   
			   Thread.sleep(3000);
			   click(btn_ScheduleCodeHABtnA);
			   sendKeys(txt_ScheduleCodeHATxtA, ScheduleCodeHATxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ScheduleCodeHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ScheduleCodeHASelA);
			   
			   Thread.sleep(3000);
			   click(btn_ResponsibleUserHABtnA);
			   sendKeys(txt_ResponsibleUserHATxtA, ResponsibleUserHATxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ResponsibleUserHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ResponsibleUserHASelA);
			   Thread.sleep(3000);
			   selectText(txt_HireBasisA, "Units");
			   
			   Thread.sleep(3000);
			   click(btn_ResourceInformationTabA);
			   selectText(txt_TypeHAA, "Resource");
			   Thread.sleep(3000);
			   click(btn_ReferenceCodeHABtnA);
			   sendKeys(txt_ReferenceCodeHATxtA, ReferenceCodeHA1TxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ReferenceCodeHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ReferenceCodeHASelA);
			   
			   sendKeys(txt_OpeningUnitA, "10");
			   Thread.sleep(3000);
			   click(btn_RateRangeA);
			   sendKeys(txt_MinimumUnitA, "5");
			   sendKeys(txt_Rate1A, "5");
			   Thread.sleep(3000);
			   click(btn_RateRangeSetupPlusA);
			   sendKeys(txt_RateRangeSetupStartA, "6");
			   sendKeys(txt_RateRangeSetupEndA, "0");
			   sendKeys(txt_Rate2A, "10");
			   Thread.sleep(3000);
			   click(btn_RateRangeSetupApplyA);
			   
			   Thread.sleep(3000);
			   click(btn_DraftA);
			   Thread.sleep(5000);
			   if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
							"User Can draft the Hire Agreement Form Successfully", "pass");
			   } else {
					writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
							"User Can draft the Hire Agreement Form Fail", "fail");
			   }

			   Thread.sleep(3000);
			   click(btn_ReleaseA);
			   Thread.sleep(3000);
			   trackCode = getText(txt_trackCodeA);
			   String HANo = trackCode;
			   
			   Thread.sleep(5000);
			   if (isDisplayed(txt_PageRelease1A)) {
						writeTestResults("Verify that able to Release the Hire Agreement Form", "User Can Release the Hire Agreement Form",
							"User Can Release the Hire Agreement Form Successfully", "pass");
			   } else {
						writeTestResults("Verify that able to Release the Hire Agreement Form", "User Can Release the Hire Agreement Form",
								"User Can Release the Hire Agreement Form Fail", "fail");
			   }
				
			   Thread.sleep(3000);
			   click(btn_ActionBtnA);
			   Thread.sleep(3000);
			   click(btn_ActualUpdateHABtnA);
			   
			   selectText(txt_cboxActualResourcesA, ReferenceCodeHA1TxtA);
			   sendKeys(txt_ActualUnitA, "2");
			   Thread.sleep(3000);
			   click(btn_CheckoutHAA);
			   Thread.sleep(3000);
			   String ActualUpdatedAmount1A= driver.findElement(By.xpath(txt_ActualUpdatedAmountA)).getAttribute("value");
			   int ActualAmount1A = Integer.parseInt(ActualUpdatedAmount1A.replace(".000000", ""));
			   //System.out.println(ActualAmount1A);
			   
			   Thread.sleep(3000);
			   JavascriptExecutor j1 = (JavascriptExecutor) driver;
			   j1.executeScript("$(\"#divTaskcusAd\").parent().find('.dialogbox-buttonarea > .button').click()");
			   
			   Thread.sleep(3000);
			   click(btn_ActionBtnA);
			   Thread.sleep(3000);
			   click(btn_ActualUpdateHABtnA);
			   
			   selectText(txt_cboxActualResourcesA, ReferenceCodeHA1TxtA);
			   sendKeys(txt_ActualUnit2A, "2");
			   Thread.sleep(3000);
			   click(btn_CheckoutHAA);
			   Thread.sleep(3000);
			   String ActualUpdatedAmount2A= driver.findElement(By.xpath(txt_ActualUpdatedAmount2A)).getAttribute("value");
			   int ActualAmount2A = Integer.parseInt(ActualUpdatedAmount2A.replace(".000000", ""));
			   //System.out.println(ActualAmount2A);
			   
			   Thread.sleep(2000);
			   String TotalAmountA = String.valueOf(ActualAmount1A+ActualAmount2A);
			   //System.out.println(TotalAmountA);
			   
			   Thread.sleep(3000);
			   j1.executeScript("$(\"#divTaskcusAd\").parent().find('.dialogbox-buttonarea > .button').click()");
			   
			   Thread.sleep(3000);
			   click(btn_EntutionHeaderA);
			   Thread.sleep(3000);
			   click(btn_RolecenterA);
			   Thread.sleep(3000);
			   click(btn_ScheduledJobsA);
			   switchWindow();
			   Thread.sleep(3000);
			   click(btn_scheduleCodePlusA);
			   Thread.sleep(3000);
			   click(btn_HireAgreementPlusA);
			   
			   if (isDisplayed(txt_HANoA.replace("HANo", HANo))) {

					writeTestResults("Verify that Schedule job task is available for the user who is selected in the field called \" Responsible User\" ", 
							"Schedule job task is available for the user who is selected in the field called \" Responsible User\" ",
							"Schedule job task is available for the user who is selected in the field called \" Responsible User\" Successfully", "pass");
			   }

			   else {
					writeTestResults("Verify that Schedule job task is available for the user who is selected in the field called \" Responsible User\" ", 
							"Schedule job task is available for the user who is selected in the field called \" Responsible User\" ",
							"Schedule job task is available for the user who is selected in the field called \" Responsible User\" Fail", "fail");
			   }
				
			   Thread.sleep(3000);
			   click(btn_checkboxRelevantHireAgreementA.replace("HANo", HANo));
			   mouseMove(btn_RunButtonRelevantHireAgreementA);
			   WaitClick(btn_RunButtonRelevantHireAgreementA);
			   click(btn_RunButtonRelevantHireAgreementA);
			   Thread.sleep(3000);
			   if (isDisplayed(btn_LinkReleasedServiceInvoiceA)) {

					writeTestResults("Verify that message look up is displayed with the link for released service invoice", 
							"message look up is displayed with the link for released service invoice",
							"message look up is displayed with the link for released service invoice Successfully", "pass");
					click(btn_LinkReleasedServiceInvoiceA);
			   }

			   else {
					writeTestResults("Verify that message look up is displayed with the link for released service invoice", 
							"message look up is displayed with the link for released service invoice",
							"message look up is displayed with the link for released service invoice Fail", "fail");
			   }
				
			   switchWindow();
			   Thread.sleep(3000);
			   trackCode = getText(txt_trackCodeA);
				
			   Thread.sleep(5000);
			   if (isDisplayed(txt_PageRelease1A)) {
							writeTestResults("Verify that Service Invoice get Released after runing the schedule job", 
									"Service Invoice get Released after runing the schedule job",
								"Service Invoice get Released after runing the schedule job Successfully", "pass");
			   } else {
							writeTestResults("Verify that Service Invoice get Released after runing the schedule job", 
									"Service Invoice get Released after runing the schedule job",
									"Service Invoice get Released after runing the schedule job Fail", "fail");
			   }
			   
			   Thread.sleep(3000);
			   String ServiceInvoiceTotalHeaderA = getText(txt_ServiceInvoiceTotalHeaderA);
				
			   if (ServiceInvoiceTotalHeaderA.contentEquals(TotalAmountA+".000000")) {
					writeTestResults("Verify that Actual  updated amount is displayed in the service invoice \"TOTAL\" Header", 
							"Actual  updated amount is displayed in the service invoice \"TOTAL\" Header",
						"Actual  updated amount is displayed in the service invoice \"TOTAL\" Header Successfully", "pass");
			   } else {
					writeTestResults("Verify that Actual  updated amount is displayed in the service invoice \"TOTAL\" Header", 
							"Actual  updated amount is displayed in the service invoice \"TOTAL\" Header",
							"Actual  updated amount is displayed in the service invoice \"TOTAL\" Header Fail", "fail");
			   }
				
			   Thread.sleep(3000);
			   click(btn_ServiceDetailsTabA);
			   Thread.sleep(3000);
			   String ServiceInvoiceSubTotalA=driver.findElement(By.xpath(txt_ServiceInvoiceSubTotalA)).getAttribute("value");
			   //System.out.println(ServiceInvoiceSubTotalA);
			   
			   if (ServiceInvoiceSubTotalA.contentEquals(TotalAmountA+".000000")) {
					writeTestResults("Verification of Service Invoice of Hire Agreement; available with Total Amount in the Actual update", 
							"Service Invoice of Hire Agreement; available with Total Amount in the Actual update",
						"Service Invoice of Hire Agreement; available with Total Amount in the Actual update Successfully", "pass");
			   } else {
					writeTestResults("Verification of Service Invoice of Hire Agreement; available with Total Amount in the Actual update", 
							"Service Invoice of Hire Agreement; available with Total Amount in the Actual update",
							"Service Invoice of Hire Agreement; available with Total Amount in the Actual update Fail", "fail");
			   }
				
		   }
		   
		   // Service_HA_030_031
		   public void VerifyThatServiceInvoiceCanBeGenerateForTheMultipleResouresInaHireAgreement() throws Exception {
			   
			   VerifyThatUserCanNavigateToTheHireAgreementForm();
			   
			   Thread.sleep(3000);
			   LocalTime obj1 = LocalTime.now();
			   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj1);
			   sendKeys(txt_TitleTxtA, "Testing Hire Agreement");
			   
			   Thread.sleep(3000);
			   JavascriptExecutor j7 = (JavascriptExecutor)driver;
			   j7.executeScript("$(\"#txtContractDate\").datepicker(\"setDate\", new Date())");
			   
			   
			   selectText(txt_ContractGroupTxtA, "ContractGroupService");
			   
			   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker(\"setDate\", new Date())");
			   j7.executeScript("$(\"#txtEffectiveTo\").datepicker(\"setDate\", new Date())");
			   
			   sendKeys(txt_DescriptionTxtA, "Testing Hire Agreement");
			   
			   Thread.sleep(3000);
			   click(btn_CustomerAccountHABtnA);
			   sendKeys(txt_CustomerAccountHATxtA, CustomerAccountTextA);
			   Thread.sleep(3000);
			   pressEnter(txt_CustomerAccountHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_CustomerAccountHASelA);
			   
			   selectText(txt_SalesUnitHAA, SalesUnitA);
			   
			   Thread.sleep(3000);
			   click(btn_ScheduleCodeHABtnA);
			   sendKeys(txt_ScheduleCodeHATxtA, ScheduleCodeHATxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ScheduleCodeHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ScheduleCodeHASelA);
			   
			   Thread.sleep(3000);
			   click(btn_ResponsibleUserHABtnA);
			   sendKeys(txt_ResponsibleUserHATxtA, ResponsibleUserHATxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ResponsibleUserHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ResponsibleUserHASelA);
			   Thread.sleep(3000);
			   selectText(txt_HireBasisA, "Units");
			   
			   Thread.sleep(3000);
			   click(btn_ResourceInformationTabA);
			   
			   
			   selectText(txt_TypeHAA, "Resource");
			   Thread.sleep(3000);
			   click(btn_ReferenceCodeHABtnA);
			   sendKeys(txt_ReferenceCodeHATxtA, ReferenceCodeHA1TxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ReferenceCodeHATxtA);
			   Thread.sleep(3000);
			   doubleClick(Sel_ReferenceCodeHASelA);
			   
			   sendKeys(txt_OpeningUnitA, "1");
			   Thread.sleep(3000);
			   click(btn_RateRangeA);
			   sendKeys(txt_MinimumUnitA, "0");
			   sendKeys(txt_RateRangeSetupEnd1A, "5");
			   sendKeys(txt_Rate1A, "5");
			   Thread.sleep(3000);
			   click(btn_RateRangeSetupPlusA);
			   sendKeys(txt_RateRangeSetupStartA, "6");
			   sendKeys(txt_RateRangeSetupEndA, "0");
			   sendKeys(txt_Rate2A, "10");
			   Thread.sleep(3000);
			   click(btn_RateRangeSetupApplyA);
			   
			   Thread.sleep(3000);
			   click(btn_ResourcePlusBtnA);
			   Thread.sleep(3000);
			   click(btn_ReferenceCodeHABtn2A);
			   sendKeys(txt_ReferenceCodeHATxtA, ReferenceCodeHA2TxtA);
			   Thread.sleep(3000);
			   pressEnter(txt_ReferenceCodeHATxtA);
			   Thread.sleep(3000);
			   doubleClick(sel_ReferenceCodeHASel2A);
			   
			   sendKeys(txt_OpeningUnit2A, "1");
			   Thread.sleep(3000);
			   click(btn_RateRange2A);
			   sendKeys(txt_MinimumUnitA, "0");
			   sendKeys(txt_RateRangeSetupEnd1A, "5");
			   sendKeys(txt_Rate1A, "5");
			   Thread.sleep(3000);
			   click(btn_RateRangeSetupPlusA);
			   sendKeys(txt_RateRangeSetupStartA, "6");
			   sendKeys(txt_RateRangeSetupEndA, "0");
			   sendKeys(txt_Rate2A, "10");
			   Thread.sleep(3000);
			   click(btn_RateRangeSetupApplyA);
			   
			   Thread.sleep(3000);
			   click(btn_DraftA);
			   Thread.sleep(5000);
			   if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
							"User Can draft the Hire Agreement Form Successfully", "pass");
			   } else {
					writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
							"User Can draft the Hire Agreement Form Fail", "fail");
			   }

			   Thread.sleep(3000);
			   click(btn_ReleaseA);
		       Thread.sleep(3000);
		       trackCode = getText(txt_trackCodeA);
		       String HANo = trackCode;
		       
			   Thread.sleep(5000);
			   if (isDisplayed(txt_PageRelease1A)) {
				        writeTestResults("Verify that  hire agreemnt can be create by adding multiple resources", "hire agreemnt can be create by adding multiple resources",
							"hire agreemnt can be create by adding multiple resources Successfully", "pass");
		           } else {
		        	    writeTestResults("Verify that  hire agreemnt can be create by adding multiple resources", "hire agreemnt can be create by adding multiple resources",
								"hire agreemnt can be create by adding multiple resources Fail", "fail");
			   }
			   
			   Thread.sleep(3000);
			   click(btn_ActionBtnA);
			   Thread.sleep(3000);
			   click(btn_ActualUpdateHABtnA);
			   
			   selectText(txt_cboxActualResourcesA, ReferenceCodeHA1TxtA);
			   sendKeys(txt_ActualUnitA, "2");
			   Thread.sleep(3000);
			   click(btn_CheckoutHAA);
			   Thread.sleep(3000);
			   String ActualUpdatedAmount1A= driver.findElement(By.xpath(txt_ActualUpdatedAmountA)).getAttribute("value");
			   int ActualAmount1A = Integer.parseInt(ActualUpdatedAmount1A.replace(".000000", ""));
			   //System.out.println(ActualAmount1A);
			   
			   Thread.sleep(3000);
			   JavascriptExecutor j1 = (JavascriptExecutor) driver;
			   j1.executeScript("$(\"#divTaskcusAd\").parent().find('.dialogbox-buttonarea > .button').click()");
			   
			   Thread.sleep(3000);
			   click(btn_ActionBtnA);
			   Thread.sleep(3000);
			   click(btn_ActualUpdateHABtnA);
			   
			   selectText(txt_cboxActualResourcesA, ReferenceCodeHA2TxtA);
			   sendKeys(txt_ActualUnitA, "2");
			   Thread.sleep(3000);
			   click(btn_CheckoutHAA);
			   String ActualUpdatedAmount2A= driver.findElement(By.xpath(txt_ActualUpdatedAmountA)).getAttribute("value");
			   int ActualAmount2A = Integer.parseInt(ActualUpdatedAmount2A.replace(".000000", ""));
			   //System.out.println(ActualAmount2A);
			   
			   Thread.sleep(3000);
			   j1.executeScript("$(\"#divTaskcusAd\").parent().find('.dialogbox-buttonarea > .button').click()");
			   
			   Thread.sleep(2000);
			   String TotalAmountA = String.valueOf(ActualAmount1A+ActualAmount2A);
			   //System.out.println(TotalAmountA);
			   
			   Thread.sleep(3000);
			   click(btn_EntutionHeaderA);
			   Thread.sleep(3000);
			   click(btn_RolecenterA);
			   Thread.sleep(3000);
			   click(btn_ScheduledJobsA);
			   switchWindow();
			   Thread.sleep(3000);
			   click(btn_scheduleCodePlusA);
			   Thread.sleep(3000);
			   click(btn_HireAgreementPlusA);
			   
			   if (isDisplayed(txt_HANoA.replace("HANo", HANo))) {

					writeTestResults("Verify that Schedule job task is available for the user who is selected in the field called \" Responsible User\" ", 
							"Schedule job task is available for the user who is selected in the field called \" Responsible User\" ",
							"Schedule job task is available for the user who is selected in the field called \" Responsible User\" Successfully", "pass");
			   }

			   else {
					writeTestResults("Verify that Schedule job task is available for the user who is selected in the field called \" Responsible User\" ", 
							"Schedule job task is available for the user who is selected in the field called \" Responsible User\" ",
							"Schedule job task is available for the user who is selected in the field called \" Responsible User\" Fail", "fail");
			   }
				
			   Thread.sleep(3000);
			   click(btn_checkboxRelevantHireAgreementA.replace("HANo", HANo));
			   mouseMove(btn_RunButtonRelevantHireAgreementA);
			   WaitClick(btn_RunButtonRelevantHireAgreementA);
			   click(btn_RunButtonRelevantHireAgreementA);
			   Thread.sleep(3000);
			   if (isDisplayed(btn_LinkReleasedServiceInvoiceA)) {
					writeTestResults("Verify that message look up is displayed with the link for released service invoice", 
							"message look up is displayed with the link for released service invoice",
							"message look up is displayed with the link for released service invoice Successfully", "pass");
					click(btn_LinkReleasedServiceInvoiceA);
			   }

			   else {
					writeTestResults("Verify that message look up is displayed with the link for released service invoice", 
							"message look up is displayed with the link for released service invoice",
							"message look up is displayed with the link for released service invoice Fail", "fail");
			   }
				
			   switchWindow();
			   Thread.sleep(3000);
			   trackCode = getText(txt_trackCodeA);
				
			   Thread.sleep(5000);
			   if (isDisplayed(txt_PageRelease1A)) {
							writeTestResults("Verify that Service Invoice get Released after runing the schedule job", 
									"Service Invoice get Released after runing the schedule job",
								"Service Invoice get Released after runing the schedule job Successfully", "pass");
			   } else {
							writeTestResults("Verify that Service Invoice get Released after runing the schedule job", 
									"Service Invoice get Released after runing the schedule job",
									"Service Invoice get Released after runing the schedule job Fail", "fail");
			   }
			   
			   Thread.sleep(3000);
			   String ServiceInvoiceTotalHeaderA = getText(txt_ServiceInvoiceTotalHeaderA);
				
			   if (ServiceInvoiceTotalHeaderA.contentEquals(TotalAmountA+".000000")) {
					writeTestResults("Verify that Actual  updated amount is displayed in the service invoice \"TOTAL\" Header", 
							"Actual  updated amount is displayed in the service invoice \"TOTAL\" Header",
						"Actual  updated amount is displayed in the service invoice \"TOTAL\" Header Successfully", "pass");
			   } else {
					writeTestResults("Verify that Actual  updated amount is displayed in the service invoice \"TOTAL\" Header", 
							"Actual  updated amount is displayed in the service invoice \"TOTAL\" Header",
							"Actual  updated amount is displayed in the service invoice \"TOTAL\" Header Fail", "fail");
			   }
				
			   Thread.sleep(3000);
			   click(btn_ServiceDetailsTabA);
			   Thread.sleep(3000);
			   String ResourceUnitPrice1A = getText(txt_ResourceUnitPrice1A);
			   String ResourceUnitPrice2A = getText(txt_ResourceUnitPrice2A);
			   
			   if (ResourceUnitPrice1A.contentEquals(ActualUpdatedAmount1A) && ResourceUnitPrice2A.contentEquals(ActualUpdatedAmount2A)) {
					writeTestResults("Verify that service invoice can be generate for the multiple Resoures in a hire agreement", 
							"service invoice can be generate for the multiple Resoures in a hire agreement",
						"service invoice can be generate for the multiple Resoures in a hire agreement Successfully", "pass");
			   } else {
					writeTestResults("Verify that service invoice can be generate for the multiple Resoures in a hire agreement", 
							"service invoice can be generate for the multiple Resoures in a hire agreement",
							"service invoice can be generate for the multiple Resoures in a hire agreement Fail", "fail");
			   }
			   
			   Thread.sleep(3000);
			   click(btn_ActionBtnA);
			   Thread.sleep(3000);
			   click(btn_ReverseBtnA);
			   j7.executeScript("$(\"#txtrdcmnReverseDate\").datepicker(\"setDate\", new Date())");
			   j1.executeScript("$(\"#dlgReverseDateCMN\").parent().find('.dialogbox-buttonarea > .button').click()");
			   Thread.sleep(3000);
			   click(btn_YesBtnA);
			   
			   Thread.sleep(3000);
			   if (isDisplayed(txt_ReverceValidatorA)) {
					writeTestResults("Verification of Service Invoice cannot be reverse", 
							"Service Invoice cannot be reverse",
							"Service Invoice cannot be reverse Successfully", "pass");
					click(btn_LinkReleasedServiceInvoiceA);
			   }

			   else {
					writeTestResults("Verification of Service Invoice cannot be reverse", 
							"Service Invoice cannot be reverse",
							"Service Invoice cannot be reverse Fail", "fail");
			   }
			   
		   }
		   
		    // Service_HA_032
		    public void VerifyThatLastBillingDateIsUpdatingAfterRunnigTheInvoice() throws Exception {
		    	
				   VerifyThatUserCanNavigateToTheHireAgreementForm();
				   
				   Thread.sleep(3000);
				   LocalTime obj1 = LocalTime.now();
				   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj1);
				   sendKeys(txt_TitleTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   JavascriptExecutor j7 = (JavascriptExecutor)driver;
				   j7.executeScript("$(\"#txtContractDate\").datepicker(\"setDate\", \"2020/06/01\")");
				   
				   
				   selectText(txt_ContractGroupTxtA, "ContractGroupService");
				   
				   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker(\"setDate\", \"2020/06/01\")");
				   j7.executeScript("$(\"#txtEffectiveTo\").datepicker(\"setDate\", \"2020/07/31\")");
				   
				   sendKeys(txt_DescriptionTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   click(btn_CustomerAccountHABtnA);
				   sendKeys(txt_CustomerAccountHATxtA, CustomerAccountTextA);
				   Thread.sleep(3000);
				   pressEnter(txt_CustomerAccountHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_CustomerAccountHASelA);
				   
				   selectText(txt_SalesUnitHAA, SalesUnitA);
				   
				   Thread.sleep(3000);
				   click(btn_ScheduleCodeHABtnA);
				   sendKeys(txt_ScheduleCodeHATxtA, ScheduleCodeHATxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ScheduleCodeHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ScheduleCodeHASelA);
				   
				   Thread.sleep(3000);
				   click(btn_ResponsibleUserHABtnA);
				   sendKeys(txt_ResponsibleUserHATxtA, ResponsibleUserHA2TxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ResponsibleUserHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ResponsibleUserHA2SelA);
				   Thread.sleep(3000);
				   selectText(txt_HireBasisA, "Units");
				   
				   Thread.sleep(3000);
				   click(btn_ResourceInformationTabA);
				   selectText(txt_TypeHAA, "Resource");
				   Thread.sleep(3000);
				   click(btn_ReferenceCodeHABtnA);
				   sendKeys(txt_ReferenceCodeHATxtA, ReferenceCodeHA1TxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ReferenceCodeHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ReferenceCodeHASelA);
				   
				   sendKeys(txt_OpeningUnitA, "10");
				   Thread.sleep(3000);
				   click(btn_RateRangeA);
				   sendKeys(txt_MinimumUnitA, "5");
				   sendKeys(txt_Rate1A, "5");
				   Thread.sleep(3000);
				   click(btn_RateRangeSetupPlusA);
				   sendKeys(txt_RateRangeSetupStartA, "6");
				   sendKeys(txt_RateRangeSetupEndA, "0");
				   sendKeys(txt_Rate2A, "10");
				   Thread.sleep(3000);
				   click(btn_RateRangeSetupApplyA);
				   
				   Thread.sleep(3000);
				   click(btn_DraftA);
				   Thread.sleep(5000);
				   if (isDisplayed(txt_PageDraft1A)) {
						writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
								"User Can draft the Hire Agreement Form Successfully", "pass");
				   } else {
						writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
								"User Can draft the Hire Agreement Form Fail", "fail");
				   }

				   Thread.sleep(3000);
				   click(btn_ReleaseA);
				   Thread.sleep(3000);
				   trackCode = getText(txt_trackCodeA);
				   String HANo = trackCode;
				   
				   Thread.sleep(5000);
				   if (isDisplayed(txt_PageRelease1A)) {
							writeTestResults("Verify that able to Release the Hire Agreement Form", "User Can Release the Hire Agreement Form",
								"User Can Release the Hire Agreement Form Successfully", "pass");
				   } else {
							writeTestResults("Verify that able to Release the Hire Agreement Form", "User Can Release the Hire Agreement Form",
									"User Can Release the Hire Agreement Form Fail", "fail");
				   }
					
				   Thread.sleep(3000);
				   click(btn_ActionBtnA);
				   Thread.sleep(3000);
				   click(btn_ActualUpdateHABtnA);
				   
				   selectText(txt_cboxActualResourcesA, ReferenceCodeHA1TxtA);
				   sendKeys(txt_ActualUnitA, "2");
				   Thread.sleep(3000);
				   click(btn_CheckoutHAA);
				   
				   Thread.sleep(3000);
				   JavascriptExecutor j1 = (JavascriptExecutor) driver;
				   j1.executeScript("$(\"#divTaskcusAd\").parent().find('.dialogbox-buttonarea > .button').click()");
				   
				   Thread.sleep(3000);
				   click(btn_LogOutBtnA);
				   Thread.sleep(3000);
				   click(btn_SignOutBtnA);
				   
					driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
					sendKeys(username, NewUserNameA);
					sendKeys(password, NewPasswordA);
					click(btn_login);

					if (isDisplayed(lnk_home)) {

						writeTestResults("User login", "User should navigate to the Home page of Entution",
								"user navigated to the home page", "pass");
					}

					else {
						writeTestResults("User login", "User should navigate to the Home page of Entution",
								"user cant navigate to the home page", "fail");

					}
					
					Thread.sleep(3000);
					click(btn_RolecenterA);
					Thread.sleep(3000);
					click(btn_ScheduledJobsA);
					switchWindow();
					Thread.sleep(3000);
					click(btn_scheduleCodePlusA);
					Thread.sleep(3000);
					click(btn_HireAgreementPlusNewA);
				   
					if (isDisplayed(txt_HANoA.replace("HANo", HANo))) {

						writeTestResults("Verify that Schedule job task is available for the user who is selected in the field called \" Responsible User\" ", 
								"Schedule job task is available for the user who is selected in the field called \" Responsible User\" ",
								"Schedule job task is available for the user who is selected in the field called \" Responsible User\" Successfully", "pass");
					}

					else {
						writeTestResults("Verify that Schedule job task is available for the user who is selected in the field called \" Responsible User\" ", 
								"Schedule job task is available for the user who is selected in the field called \" Responsible User\" ",
								"Schedule job task is available for the user who is selected in the field called \" Responsible User\" Fail", "fail");
					}
					
					Thread.sleep(3000);
					click(btn_checkboxRelevantHireAgreementA.replace("HANo", HANo));
					Thread.sleep(3000);
					click(btn_RunButtonRelevantHireAgreementNewA);
					Thread.sleep(3000);
					if (isDisplayed(btn_LinkReleasedServiceInvoiceA)) {

						writeTestResults("Verify that message look up is displayed with the link for released service invoice", 
								"message look up is displayed with the link for released service invoice",
								"message look up is displayed with the link for released service invoice Successfully", "pass");
						click(btn_LinkReleasedServiceInvoiceA);
					}

					else {
						writeTestResults("Verify that message look up is displayed with the link for released service invoice", 
								"message look up is displayed with the link for released service invoice",
								"message look up is displayed with the link for released service invoice Fail", "fail");
					}
					
					switchWindow();
					Thread.sleep(3000);
					trackCode = getText(txt_trackCodeA);
					
					Thread.sleep(5000);
					if (isDisplayed(txt_PageRelease1A)) {
								writeTestResults("Verify that Service Invoice get Released after runing the schedule job", 
										"Service Invoice get Released after runing the schedule job",
									"Service Invoice get Released after runing the schedule job Successfully", "pass");
					} else {
								writeTestResults("Verify that Service Invoice get Released after runing the schedule job", 
										"Service Invoice get Released after runing the schedule job",
										"Service Invoice get Released after runing the schedule job Fail", "fail");
					}
					
					Thread.sleep(3000);
					click(btn_LogOutBtnA);
					Thread.sleep(3000);
					click(btn_SignOutBtnA);
					   
					driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
					sendKeys(username, usernamedataA);
					sendKeys(password, passworddataA);
					click(btn_login);
					
					
					Thread.sleep(6000);
					clickNavigation();
					Thread.sleep(3000);
					click(btn_HireAgreementBtnA);
					Thread.sleep(3000);
					sendKeys(txt_HireAgreementSerchTxtA, HANo);
					Thread.sleep(3000);
					pressEnter(txt_HireAgreementSerchTxtA);
					Thread.sleep(3000);
					doubleClick(sel_HireAgreementSerchSelA.replace("HANo", HANo));
					
					String LastBillDateA = getText(txt_LastBillDateA);
					String EffectiveFromDateAfterReleaseA = getText(txt_EffectiveFromDateAfterReleaseA);
					
					Thread.sleep(5000);
					if (LastBillDateA.contentEquals(EffectiveFromDateAfterReleaseA)) {
								writeTestResults("Verify that Last billing date is updating after runnig the invoice", 
										"Last billing date is updating after runnig the invoice",
									"Last billing date is updating after runnig the invoice Successfully", "pass");
					} else {
								writeTestResults("Verify that Last billing date is updating after runnig the invoice", 
										"Last billing date is updating after runnig the invoice",
										"Last billing date is updating after runnig the invoice Fail", "fail");
					}
		    }
		    
		    // Service_HA_035_036_037
		    public void VerifyThatUserCannotCloseTheHireAgreementWithoutRunningTheSchedule() throws Exception {
		    	
				   VerifyThatUserCanNavigateToTheHireAgreementForm();
				   
				   Thread.sleep(3000);
				   LocalTime obj1 = LocalTime.now();
				   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj1);
				   sendKeys(txt_TitleTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   JavascriptExecutor j7 = (JavascriptExecutor)driver;
				   j7.executeScript("$(\"#txtContractDate\").datepicker(\"setDate\", new Date())");
				   
				   
				   selectText(txt_ContractGroupTxtA, "ContractGroupService");
				   
				   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker(\"setDate\", new Date())");
				   j7.executeScript("$(\"#txtEffectiveTo\").datepicker(\"setDate\", new Date())");
				   
				   sendKeys(txt_DescriptionTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   click(btn_CustomerAccountHABtnA);
				   sendKeys(txt_CustomerAccountHATxtA, CustomerAccountTextA);
				   Thread.sleep(3000);
				   pressEnter(txt_CustomerAccountHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_CustomerAccountHASelA);
				   
				   selectText(txt_SalesUnitHAA, SalesUnitA);
				   
				   Thread.sleep(3000);
				   click(btn_ScheduleCodeHABtnA);
				   sendKeys(txt_ScheduleCodeHATxtA, ScheduleCodeHATxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ScheduleCodeHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ScheduleCodeHASelA);
				   
				   Thread.sleep(3000);
				   click(btn_ResponsibleUserHABtnA);
				   sendKeys(txt_ResponsibleUserHATxtA, ResponsibleUserHATxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ResponsibleUserHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ResponsibleUserHASelA);
				   Thread.sleep(3000);
				   selectText(txt_HireBasisA, "Units");
				   
				   Thread.sleep(3000);
				   click(btn_ResourceInformationTabA);
				   selectText(txt_TypeHAA, "Resource");
				   Thread.sleep(3000);
				   click(btn_ReferenceCodeHABtnA);
				   sendKeys(txt_ReferenceCodeHATxtA, ReferenceCodeHA1TxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ReferenceCodeHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ReferenceCodeHASelA);
				   
				   sendKeys(txt_OpeningUnitA, "10");
				   Thread.sleep(3000);
				   click(btn_RateRangeA);
				   sendKeys(txt_MinimumUnitA, "5");
				   sendKeys(txt_Rate1A, "5");
				   Thread.sleep(3000);
				   click(btn_RateRangeSetupPlusA);
				   sendKeys(txt_RateRangeSetupStartA, "6");
				   sendKeys(txt_RateRangeSetupEndA, "0");
				   sendKeys(txt_Rate2A, "10");
				   Thread.sleep(3000);
				   click(btn_RateRangeSetupApplyA);
				   
				   Thread.sleep(3000);
				   click(btn_DraftA);
				   Thread.sleep(5000);
				   if (isDisplayed(txt_PageDraft1A)) {
						writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
								"User Can draft the Hire Agreement Form Successfully", "pass");
				   } else {
						writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
								"User Can draft the Hire Agreement Form Fail", "fail");
				   }

				   Thread.sleep(3000);
				   click(btn_ReleaseA);
				   Thread.sleep(3000);
				   trackCode = getText(txt_trackCodeA);
				   String HANo = trackCode;
				   
				   Thread.sleep(5000);
				   if (isDisplayed(txt_PageRelease1A)) {
							writeTestResults("Verify that able to Release the Hire Agreement Form", "User Can Release the Hire Agreement Form",
								"User Can Release the Hire Agreement Form Successfully", "pass");
				   } else {
							writeTestResults("Verify that able to Release the Hire Agreement Form", "User Can Release the Hire Agreement Form",
									"User Can Release the Hire Agreement Form Fail", "fail");
				   }
					
				   Thread.sleep(3000);
				   click(btn_ActionBtnA);
				   Thread.sleep(3000);
				   click(btn_ActualUpdateHABtnA);
				   
				   selectText(txt_cboxActualResourcesA, ReferenceCodeHA1TxtA);
				   sendKeys(txt_ActualUnitA, "2");
				   Thread.sleep(3000);
				   click(btn_CheckoutHAA);
				   
				   Thread.sleep(3000);
				   JavascriptExecutor j1 = (JavascriptExecutor) driver;
				   j1.executeScript("$(\"#divTaskcusAd\").parent().find('.dialogbox-buttonarea > .button').click()");
				   
				   Thread.sleep(3000);
				   click(btn_ActionBtnA);
				   Thread.sleep(3000);
				   click(btn_CloseAgreementHABtnA);
				   j7.executeScript("$(\"#txtCmpDate\").datepicker(\"setDate\", new Date())");
				   Thread.sleep(3000);
				   click(btn_OkBtnA);
				   
				   Thread.sleep(5000);
				   if (isDisplayed(txt_CloseValidatorA)) {
							writeTestResults("Verify that user cannot close the Hire agreement without running the schedule", "user cannot close the Hire agreement without running the schedule",
								"user cannot close the Hire agreement without running the schedule Successfully", "pass");
				   } else {
							writeTestResults("Verify that user cannot close the Hire agreement without running the schedule", "user cannot close the Hire agreement without running the schedule",
									"user cannot close the Hire agreement without running the schedule Fail", "fail");
				   }
				   
				   pageRefersh();
				   
				   	Thread.sleep(3000);
				    click(btn_EntutionHeaderA);
					Thread.sleep(3000);
					click(btn_RolecenterA);
					Thread.sleep(3000);
					click(btn_ScheduledJobsA);
					switchWindow();
					Thread.sleep(3000);
					click(btn_scheduleCodePlusA);
					Thread.sleep(3000);
					click(btn_HireAgreementPlusA);
				   
					if (isDisplayed(txt_HANoA.replace("HANo", HANo))) {

						writeTestResults("Verify that Schedule job task is available for the user who is selected in the field called \" Responsible User\" ", 
								"Schedule job task is available for the user who is selected in the field called \" Responsible User\" ",
								"Schedule job task is available for the user who is selected in the field called \" Responsible User\" Successfully", "pass");
					}

					else {
						writeTestResults("Verify that Schedule job task is available for the user who is selected in the field called \" Responsible User\" ", 
								"Schedule job task is available for the user who is selected in the field called \" Responsible User\" ",
								"Schedule job task is available for the user who is selected in the field called \" Responsible User\" Fail", "fail");
					}
					
					Thread.sleep(3000);
					click(btn_checkboxRelevantHireAgreementA.replace("HANo", HANo));
					mouseMove(btn_RunButtonRelevantHireAgreementA);
					WaitClick(btn_RunButtonRelevantHireAgreementA);
					click(btn_RunButtonRelevantHireAgreementA);
					Thread.sleep(3000);
					if (isDisplayed(btn_LinkReleasedServiceInvoiceA)) {

						writeTestResults("Verify that message look up is displayed with the link for released service invoice", 
								"message look up is displayed with the link for released service invoice",
								"message look up is displayed with the link for released service invoice Successfully", "pass");
						click(btn_LinkReleasedServiceInvoiceA);
					}

					else {
						writeTestResults("Verify that message look up is displayed with the link for released service invoice", 
								"message look up is displayed with the link for released service invoice",
								"message look up is displayed with the link for released service invoice Fail", "fail");
					}
					
					switchWindow();
					Thread.sleep(3000);
					trackCode = getText(txt_trackCodeA);
					
					Thread.sleep(5000);
					if (isDisplayed(txt_PageRelease1A)) {
								writeTestResults("Verify that Service Invoice get Released after runing the schedule job", 
										"Service Invoice get Released after runing the schedule job",
									"Service Invoice get Released after runing the schedule job Successfully", "pass");
					} else {
								writeTestResults("Verify that Service Invoice get Released after runing the schedule job", 
										"Service Invoice get Released after runing the schedule job",
										"Service Invoice get Released after runing the schedule job Fail", "fail");
					}
					
					Thread.sleep(6000);
					clickNavigation();
					Thread.sleep(3000);
					click(btn_HireAgreementBtnA);
					Thread.sleep(3000);
					sendKeys(txt_HireAgreementSerchTxtA, HANo);
					Thread.sleep(3000);
					pressEnter(txt_HireAgreementSerchTxtA);
					Thread.sleep(3000);
					doubleClick(sel_HireAgreementSerchSelA.replace("HANo", HANo));
					
					Thread.sleep(3000);
					click(btn_ActionBtnA);
					Thread.sleep(3000);
					click(btn_CloseAgreementBtnAfterServiceInvoiceA);
					j7.executeScript("$(\"#txtCmpDate\").datepicker(\"setDate\", new Date())");
					Thread.sleep(3000);
					click(btn_OkBtnA);
					   
					Thread.sleep(5000);
					if (isDisplayed(txt_pageCompletedA)) {
								writeTestResults("Verify that user able to close the expired Agreeement", "user able to close the expired Agreeement",
									"user able to close the expired Agreeement Successfully", "pass");
					} else {
								writeTestResults("Verify that user able to close the expired Agreeement", "user able to close the expired Agreeement",
										"user able to close the expired Agreeement Fail", "fail");
					}
					
					Thread.sleep(3000);
					click(btn_HireAgreementByPageBtnA);
					Thread.sleep(3000);
					sendKeys(txt_HireAgreementSerchTxtA, HANo);
					Thread.sleep(3000);
					pressEnter(txt_HireAgreementSerchTxtA);
					
					Thread.sleep(5000);
					if (getText(txt_HireAgreementStatusByPageA).contentEquals("Completed")) {
								writeTestResults("Verify that Status \"Completed\" is displayed in the Hire Agreement By page", "Status \"Completed\" is displayed in the Hire Agreement By page",
									"Status \"Completed\" is displayed in the Hire Agreement By page Successfully", "pass");
					} else {
								writeTestResults("Verify that Status \"Completed\" is displayed in the Hire Agreement By page", "Status \"Completed\" is displayed in the Hire Agreement By page",
										"Status \"Completed\" is displayed in the Hire Agreement By page Fail", "fail");
					}
		    }
		    
		    // Service_HA_038_039
		    public void VerifyThatAmountColumnInTheActualUpdateLookUpIsUpdateAccordingToTheRateRage() throws Exception {
		    	
				   VerifyThatUserCanNavigateToTheHireAgreementForm();
				   
				   Thread.sleep(3000);
				   LocalTime obj1 = LocalTime.now();
				   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj1);
				   sendKeys(txt_TitleTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   JavascriptExecutor j7 = (JavascriptExecutor)driver;
				   j7.executeScript("$(\"#txtContractDate\").datepicker(\"setDate\", new Date())");
				   
				   
				   selectText(txt_ContractGroupTxtA, "ContractGroupService");
				   
				   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker(\"setDate\", new Date())");
				   j7.executeScript("$(\"#txtEffectiveTo\").datepicker(\"setDate\", new Date())");
				   
				   sendKeys(txt_DescriptionTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   click(btn_CustomerAccountHABtnA);
				   sendKeys(txt_CustomerAccountHATxtA, CustomerAccountTextA);
				   Thread.sleep(3000);
				   pressEnter(txt_CustomerAccountHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_CustomerAccountHASelA);
				   
				   selectText(txt_SalesUnitHAA, SalesUnitA);
				   
				   Thread.sleep(3000);
				   click(btn_ScheduleCodeHABtnA);
				   sendKeys(txt_ScheduleCodeHATxtA, ScheduleCodeHATxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ScheduleCodeHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ScheduleCodeHASelA);
				   
				   Thread.sleep(3000);
				   click(btn_ResponsibleUserHABtnA);
				   sendKeys(txt_ResponsibleUserHATxtA, ResponsibleUserHATxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ResponsibleUserHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ResponsibleUserHASelA);
				   Thread.sleep(3000);
				   selectText(txt_HireBasisA, "Units");
				   
				   Thread.sleep(3000);
				   click(btn_ResourceInformationTabA);
				   
				   
				   selectText(txt_TypeHAA, "Resource");
				   Thread.sleep(3000);
				   click(btn_ReferenceCodeHABtnA);
				   sendKeys(txt_ReferenceCodeHATxtA, ReferenceCodeHA1TxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ReferenceCodeHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ReferenceCodeHASelA);
				   
				   sendKeys(txt_OpeningUnitA, "1");
				   Thread.sleep(3000);
				   click(btn_RateRangeA);
				   sendKeys(txt_MinimumUnitA, "1");
				   sendKeys(txt_RateRangeSetupEnd1A, "10");
				   sendKeys(txt_Rate1A, "10");
				   Thread.sleep(3000);
				   click(btn_RateRangeSetupPlusA);
				   sendKeys(txt_RateRangeSetupStartA, "11");
				   sendKeys(txt_RateRangeSetupEndA, "0");
				   sendKeys(txt_Rate2A, "20");
				   Thread.sleep(3000);
				   click(btn_RateRangeSetupApplyA);
				   
				   Thread.sleep(3000);
				   click(btn_DraftA);
				   Thread.sleep(5000);
				   if (isDisplayed(txt_PageDraft1A)) {
						writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
								"User Can draft the Hire Agreement Form Successfully", "pass");
				   } else {
						writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
								"User Can draft the Hire Agreement Form Fail", "fail");
				   }

				   Thread.sleep(3000);
				   click(btn_ReleaseA);
			       Thread.sleep(3000);
			       trackCode = getText(txt_trackCodeA);
			       String HANo = trackCode;
			       
				   Thread.sleep(5000);
				   if (isDisplayed(txt_PageRelease1A)) {
						writeTestResults("Verify that able to Release the Hire Agreement Form", "User Can Release the Hire Agreement Form",
							"User Can Release the Hire Agreement Form Successfully", "pass");
				   } else {
						writeTestResults("Verify that able to Release the Hire Agreement Form", "User Can Release the Hire Agreement Form",
								"User Can Release the Hire Agreement Form Fail", "fail");
				   }
				   
				   Thread.sleep(3000);
				   click(btn_ActionBtnA);
				   Thread.sleep(3000);
				   click(btn_ActualUpdateHABtnA);
				   
				   selectText(txt_cboxActualResourcesA, ReferenceCodeHA1TxtA);
				   sendKeys(txt_ActualUnitA, "100");
				   Thread.sleep(3000);
				   click(btn_CheckoutHAA);
				   Thread.sleep(3000);
				   String ActualUpdatedAmount1A= driver.findElement(By.xpath(txt_ActualUpdatedAmountA)).getAttribute("value");
				   //System.out.println(ActualUpdatedAmount1A);
				   
				   if (ActualUpdatedAmount1A.contentEquals("1,900.000000")) {
						writeTestResults("Verify that Amount column in the Actual update look up is update according to the Rate rage", "Amount column in the Actual update look up is update according to the Rate rage",
							"Amount column in the Actual update look up is update according to the Rate rage Successfully", "pass");
				   } else {
						writeTestResults("Verify that Amount column in the Actual update look up is update according to the Rate rage", "Amount column in the Actual update look up is update according to the Rate rage",
								"Amount column in the Actual update look up is update according to the Rate rage Fail", "fail");
				   }
				   
				   Thread.sleep(3000);
				   JavascriptExecutor j1 = (JavascriptExecutor) driver;
				   j1.executeScript("$(\"#divTaskcusAd\").parent().find('.dialogbox-buttonarea > .button').click()");
				   
				   Thread.sleep(3000);
				   click(btn_ActionBtnA);
				   Thread.sleep(3000);
				   click(btn_ActualUpdateHABtnA);
				   
				   Thread.sleep(3000);
				   String BaseAmountA= driver.findElement(By.xpath(txt_BaseAmountA)).getAttribute("value");
				   //System.out.println(BaseAmountA);
				   
				   if (BaseAmountA.contentEquals("1,900.000000")) {
						writeTestResults("Verify that Base Amount column in the Actual update look up is update according to the Amount",
								"Base Amount column in the Actual update look up is update according to the Amount",
							"Base Amount column in the Actual update look up is update according to the Amount Successfully", "pass");
				   } else {
						writeTestResults("Verify that Base Amount column in the Actual update look up is update according to the Amount",
								"Base Amount column in the Actual update look up is update according to the Amount",
								"Base Amount column in the Actual update look up is update according to the Amount Fail", "fail");
				   }
		    }
		    
		    // Service_HA_040
		    public void VerifyThatEffectiveFromDateCannotBeaBackDateOfTheAgreementDate() throws Exception {
		    	
				   VerifyThatUserCanNavigateToTheHireAgreementForm();
				   
				   Thread.sleep(3000);
				   LocalTime obj1 = LocalTime.now();
				   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj1);
				   sendKeys(txt_TitleTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   JavascriptExecutor j7 = (JavascriptExecutor)driver;
				   j7.executeScript("$(\"#txtContractDate\").datepicker(\"setDate\", new Date())");
				   
				   
				   selectText(txt_ContractGroupTxtA, "ContractGroupService");
				   
				   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker(\"setDate\", \"2020/07/01\")");
				   j7.executeScript("$(\"#txtEffectiveTo\").datepicker('setDate', \"2023/07/31\")");
				   
				   sendKeys(txt_DescriptionTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   click(btn_CustomerAccountHABtnA);
				   sendKeys(txt_CustomerAccountHATxtA, CustomerAccountTextA);
				   Thread.sleep(3000);
				   pressEnter(txt_CustomerAccountHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_CustomerAccountHASelA);
				   
				   selectText(txt_SalesUnitHAA, SalesUnitA);
				   
				   Thread.sleep(3000);
				   click(btn_ScheduleCodeHABtnA);
				   sendKeys(txt_ScheduleCodeHATxtA, ScheduleCodeHATxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ScheduleCodeHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ScheduleCodeHASelA);
				   
				   Thread.sleep(3000);
				   click(btn_ResponsibleUserHABtnA);
				   sendKeys(txt_ResponsibleUserHATxtA, ResponsibleUserHATxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ResponsibleUserHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ResponsibleUserHASelA);
				   Thread.sleep(3000);
				   selectText(txt_HireBasisA, "Units");
				   
				   Thread.sleep(3000);
				   click(btn_ResourceInformationTabA);
				   selectText(txt_TypeHAA, "Resource");
				   Thread.sleep(3000);
				   click(btn_ReferenceCodeHABtnA);
				   sendKeys(txt_ReferenceCodeHATxtA, ReferenceCodeHA1TxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ReferenceCodeHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ReferenceCodeHASelA);
				   
				   sendKeys(txt_OpeningUnitA, "10");
				   Thread.sleep(3000);
				   click(btn_RateRangeA);
				   sendKeys(txt_MinimumUnitA, "5");
				   sendKeys(txt_Rate1A, "5");
				   Thread.sleep(3000);
				   click(btn_RateRangeSetupPlusA);
				   sendKeys(txt_RateRangeSetupStartA, "6");
				   sendKeys(txt_RateRangeSetupEndA, "0");
				   sendKeys(txt_Rate2A, "10");
				   Thread.sleep(3000);
				   click(btn_RateRangeSetupApplyA);
				   
				   Thread.sleep(3000);
				   click(btn_DraftA);
				   
				   Thread.sleep(3000);
				   JavascriptExecutor jse = (JavascriptExecutor) driver;
			       jse.executeScript("$(closeMessageBox()).click()");
				   Thread.sleep(3000);
				   if (isDisplayed(txt_AgreementDateValidationEffectiveFromDateA)) {
						writeTestResults("Verify that Effective From date cannot be a back date of the \"Agreement date\" ", "Effective From date cannot be a back date of the \"Agreement date\" ",
								"Effective From date cannot be a back date of the \"Agreement date\"  Successfully", "pass");
				   } else {
						writeTestResults("Verify that Effective From date cannot be a back date of the \"Agreement date\" ", "Effective From date cannot be a back date of the \"Agreement date\" ",
								"Effective From date cannot be a back date of the \"Agreement date\"  Fail", "fail");
				   }
		    }
		    
		    // Service_HA_041
		    public void VerifyThatEffectiveToDateCannotBeaBackDateOfEffectiveFromDate() throws Exception {
		    	
				   VerifyThatUserCanNavigateToTheHireAgreementForm();
				   
				   Thread.sleep(3000);
				   LocalTime obj1 = LocalTime.now();
				   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj1);
				   sendKeys(txt_TitleTxtA, "Testing Hire Agreement");
				   
//				   Thread.sleep(3000);
//				   JavascriptExecutor j7 = (JavascriptExecutor)driver;
//				   j7.executeScript("$(\"#txtContractDate\").datepicker('setDate', \"2020/08/04\")");
				   sendKeys(txt_AgreementDateA, "2020/08/04");
				   
				   
				   selectText(txt_ContractGroupTxtA, "ContractGroupService");
				   
//				   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker('setDate', \"2020/08/04\")");
				   
				   sendKeys(txt_EffectiveFromDateA, "2020/08/04");
				   
				   WaitClick(txt_EffectiveToA);
				   click(txt_EffectiveToA);
				   
				   WaitElement(txt_EffectiveToMonthA);
				   selectText(txt_EffectiveToMonthA, "Aug");
				   WaitElement(txt_EffectiveToYearA);
				   selectText(txt_EffectiveToYearA, "2020");
				   WaitClick(txt_EffectiveToDatePreA.replace("Date", "1"));
				   click(txt_EffectiveToDatePreA.replace("Date", "1"));
				   
//					Thread.sleep(3000);
//					String EffectiveToDatePre1A = getAttribute(txt_EffectiveToDatePreA.replace("Date", "1"), "disabled");
//					String EffectiveToDatePre2A = getAttribute(txt_EffectiveToDatePreA.replace("Date", "2"), "disabled");
//					String EffectiveToDatePre3A = getAttribute(txt_EffectiveToDatePreA.replace("Date", "3"), "disabled");
				   
				   Thread.sleep(3000);
				   String txtEffectiveTo= driver.findElement(By.xpath(btn_EffectiveTo)).getAttribute("value");
				   //System.out.println(txtEffectiveTo);
				   
				   Thread.sleep(3000);
				   if (txtEffectiveTo.isEmpty()) {
						writeTestResults("Verify that Effective To date cannot be a back date of \"Effective From Date\"", "Effective To date cannot be a back date of \"Effective From Date\"",
								"Effective To date cannot be a back date of \"Effective From Date\" Successfully", "pass");
				   } else {
						writeTestResults("Verify that Effective To date cannot be a back date of \"Effective From Date\"", "Effective To date cannot be a back date of \"Effective From Date\"",
								"Effective To date cannot be a back date of \"Effective From Date\" Fail", "fail");
				   }
		    }
		    
		    // Service_HA_042
		    public void VerifyThatOverDueFieldCountDisplayedAsZeroBeforeRunTheFirstScheduledJob() throws Exception {
		    	
		    	VerifyThatAbleToReleaseTheHireAgreement();
		    	String OverDueTxtA = getText(txt_OverDueTxtA);
		    	
				Thread.sleep(3000);
				if (OverDueTxtA.contentEquals("0")) {
						writeTestResults("Verify that \"Over due\" field count displayed as \"0\" before run the 1st scheduled job", "\"Over due\" field count displayed as \"0\" before run the 1st scheduled job",
								"\"Over due\" field count displayed as \"0\" before run the 1st scheduled job Successfully", "pass");
				} else {
						writeTestResults("Verify that \"Over due\" field count displayed as \"0\" before run the 1st scheduled job", "\"Over due\" field count displayed as \"0\" before run the 1st scheduled job",
								"\"Over due\" field count displayed as \"0\" before run the 1st scheduled job Fail", "fail");
				}
		    }
		    
		    // Service_HA_043
		    public void VerifyThatOverdueDateFieldCountIsAutoIncreasingAccodingToTheDueDatesUptoSystemDateFromNextBillingDate() throws Exception {
		    	
				   VerifyThatUserCanNavigateToTheHireAgreementForm();
				   
				   Thread.sleep(3000);
				   LocalTime obj1 = LocalTime.now();
				   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj1);
				   sendKeys(txt_TitleTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   JavascriptExecutor j7 = (JavascriptExecutor)driver;
				   j7.executeScript("$('#txtContractDate').datepicker('setDate', '2020/06/01')");
				   
				   
				   selectText(txt_ContractGroupTxtA, "ContractGroupService");
				   
				   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker(\"setDate\", \"2020/06/01\")");
				   j7.executeScript("$(\"#txtEffectiveTo\").datepicker(\"setDate\", \"2020/07/31\")");
				   
					String dateFrom = "2020-06-01";
					
					DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
					Date date = new Date();
					String dateTo = dateFormat.format(date).replace("/", "-");
						
					//Parsing the date
					LocalDate DateFrom = LocalDate.parse(dateFrom);
					LocalDate DateTo = LocalDate.parse(dateTo);
						
					//calculating number of days in between
					long noOfDaysBetween = ChronoUnit.DAYS.between(DateFrom, DateTo);
					int OverDueDays = (int) noOfDaysBetween;
					String OverDueDaysCount = String.valueOf(OverDueDays+1);
				   
				   sendKeys(txt_DescriptionTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   click(btn_CustomerAccountHABtnA);
				   sendKeys(txt_CustomerAccountHATxtA, CustomerAccountTextA);
				   Thread.sleep(3000);
				   pressEnter(txt_CustomerAccountHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_CustomerAccountHASelA);
				   
				   selectText(txt_SalesUnitHAA, SalesUnitA);
				   
				   Thread.sleep(3000);
				   click(btn_ScheduleCodeHABtnA);
				   sendKeys(txt_ScheduleCodeHATxtA, ScheduleCodeHATxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ScheduleCodeHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ScheduleCodeHASelA);
				   
				   Thread.sleep(3000);
				   click(btn_ResponsibleUserHABtnA);
				   sendKeys(txt_ResponsibleUserHATxtA, ResponsibleUserHATxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ResponsibleUserHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ResponsibleUserHASelA);
				   Thread.sleep(3000);
				   selectText(txt_HireBasisA, "Units");
				   
				   Thread.sleep(3000);
				   click(btn_ResourceInformationTabA);
				   selectText(txt_TypeHAA, "Resource");
				   Thread.sleep(3000);
				   click(btn_ReferenceCodeHABtnA);
				   sendKeys(txt_ReferenceCodeHATxtA, ReferenceCodeHA1TxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ReferenceCodeHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ReferenceCodeHASelA);
				   
				   sendKeys(txt_OpeningUnitA, "10");
				   Thread.sleep(3000);
				   click(btn_RateRangeA);
				   sendKeys(txt_MinimumUnitA, "5");
				   sendKeys(txt_Rate1A, "5");
				   Thread.sleep(3000);
				   click(btn_RateRangeSetupPlusA);
				   sendKeys(txt_RateRangeSetupStartA, "6");
				   sendKeys(txt_RateRangeSetupEndA, "0");
				   sendKeys(txt_Rate2A, "10");
				   Thread.sleep(3000);
				   click(btn_RateRangeSetupApplyA);
				   
				   Thread.sleep(3000);
				   click(btn_DraftA);
				   Thread.sleep(5000);
				   if (isDisplayed(txt_PageDraft1A)) {
						writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
								"User Can draft the Hire Agreement Form Successfully", "pass");
				   } else {
						writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
								"User Can draft the Hire Agreement Form Fail", "fail");
				   }

				   Thread.sleep(3000);
				   click(btn_ReleaseA);
				   Thread.sleep(3000);
				   trackCode = getText(txt_trackCodeA);
				   String HANo = trackCode;
				   
				   Thread.sleep(5000);
				   if (isDisplayed(txt_PageRelease1A)) {
							writeTestResults("Verify that able to Release the Hire Agreement Form", "User Can Release the Hire Agreement Form",
								"User Can Release the Hire Agreement Form Successfully", "pass");
				   } else {
							writeTestResults("Verify that able to Release the Hire Agreement Form", "User Can Release the Hire Agreement Form",
									"User Can Release the Hire Agreement Form Fail", "fail");
				   }
					
				   Thread.sleep(3000);
				   click(btn_ActionBtnA);
				   Thread.sleep(3000);
				   click(btn_ActualUpdateHABtnA);
				   
				   selectText(txt_cboxActualResourcesA, ReferenceCodeHA1TxtA);
				   sendKeys(txt_ActualUnitA, "2");
				   Thread.sleep(3000);
				   click(btn_CheckoutHAA);
				   
				   Thread.sleep(3000);
				   JavascriptExecutor j1 = (JavascriptExecutor) driver;
				   j1.executeScript("$(\"#divTaskcusAd\").parent().find('.dialogbox-buttonarea > .button').click()");
					
					Thread.sleep(3000);
					click(btn_EntutionHeaderA);
					Thread.sleep(3000);
					click(btn_RolecenterA);
					Thread.sleep(3000);
					click(btn_ScheduledJobsA);
					switchWindow();
					Thread.sleep(3000);
					click(btn_scheduleCodePlusA);
					Thread.sleep(3000);
					click(btn_HireAgreementPlusNewA);
				   
					if (isDisplayed(txt_HANoA.replace("HANo", HANo))) {

						writeTestResults("Verify that Schedule job task is available for the user who is selected in the field called \" Responsible User\" ", 
								"Schedule job task is available for the user who is selected in the field called \" Responsible User\" ",
								"Schedule job task is available for the user who is selected in the field called \" Responsible User\" Successfully", "pass");
					}

					else {
						writeTestResults("Verify that Schedule job task is available for the user who is selected in the field called \" Responsible User\" ", 
								"Schedule job task is available for the user who is selected in the field called \" Responsible User\" ",
								"Schedule job task is available for the user who is selected in the field called \" Responsible User\" Fail", "fail");
					}
					
					Thread.sleep(3000);
					click(btn_checkboxRelevantHireAgreementA.replace("HANo", HANo));
					Thread.sleep(3000);
					click(btn_RunButtonRelevantHireAgreementNewA);
					Thread.sleep(3000);
					if (isDisplayed(btn_LinkReleasedServiceInvoiceA)) {

						writeTestResults("Verify that message look up is displayed with the link for released service invoice", 
								"message look up is displayed with the link for released service invoice",
								"message look up is displayed with the link for released service invoice Successfully", "pass");
						click(btn_LinkReleasedServiceInvoiceA);
					}

					else {
						writeTestResults("Verify that message look up is displayed with the link for released service invoice", 
								"message look up is displayed with the link for released service invoice",
								"message look up is displayed with the link for released service invoice Fail", "fail");
					}
					
					switchWindow();
					Thread.sleep(3000);
					trackCode = getText(txt_trackCodeA);
					
					Thread.sleep(5000);
					if (isDisplayed(txt_PageRelease1A)) {
								writeTestResults("Verify that Service Invoice get Released after runing the schedule job", 
										"Service Invoice get Released after runing the schedule job",
									"Service Invoice get Released after runing the schedule job Successfully", "pass");
					} else {
								writeTestResults("Verify that Service Invoice get Released after runing the schedule job", 
										"Service Invoice get Released after runing the schedule job",
										"Service Invoice get Released after runing the schedule job Fail", "fail");
					}
					
					Thread.sleep(6000);
					clickNavigation();
					Thread.sleep(3000);
					click(btn_HireAgreementBtnA);
					Thread.sleep(3000);
					sendKeys(txt_HireAgreementSerchTxtA, HANo);
					Thread.sleep(3000);
					pressEnter(txt_HireAgreementSerchTxtA);
					Thread.sleep(3000);
					doubleClick(sel_HireAgreementSerchSelA.replace("HANo", HANo));
					
			    	String OverDueTxtA = getText(txt_OverDueTxtA);
			    	
					Thread.sleep(3000);
					if (OverDueTxtA.contentEquals(OverDueDaysCount)) {
							writeTestResults("Verify that \"Overdue date\" field count is auto increasing, accoding to the due dates (upto system date from next billing date)", 
									"\"Overdue date\" field count is auto increasing, accoding to the due dates (upto system date from next billing date)",
									"\"Overdue date\" field count is auto increasing, accoding to the due dates (upto system date from next billing date) Successfully", "pass");
					} else {
							writeTestResults("Verify that \"Overdue date\" field count is auto increasing, accoding to the due dates (upto system date from next billing date)", 
									"\"Overdue date\" field count is auto increasing, accoding to the due dates (upto system date from next billing date)",
									"\"Overdue date\" field count is auto increasing, accoding to the due dates (upto system date from next billing date) Fail", "fail");
					}
		    }
		    
		    // Service_HA_044
		    public void VerifyThatUserCannotCreateTwoOrMoreHireAgreementsWithSameAgreementNo() throws Exception {
		    	
				   VerifyThatUserCanNavigateToTheHireAgreementForm();
				   
				   Thread.sleep(3000);
				   LocalTime obj1 = LocalTime.now();
				   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj1);
				   sendKeys(txt_TitleTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   JavascriptExecutor j7 = (JavascriptExecutor)driver;
				   j7.executeScript("$(\"#txtContractDate\").datepicker(\"setDate\", new Date())");
				   
				   
				   selectText(txt_ContractGroupTxtA, "ContractGroupService");
				   
				   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker(\"setDate\", new Date())");
				   j7.executeScript("$(\"#txtEffectiveTo\").datepicker(\"setDate\", new Date())");
				   
				   sendKeys(txt_DescriptionTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   click(btn_CustomerAccountHABtnA);
				   sendKeys(txt_CustomerAccountHATxtA, CustomerAccountTextA);
				   Thread.sleep(3000);
				   pressEnter(txt_CustomerAccountHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_CustomerAccountHASelA);
				   
				   selectText(txt_SalesUnitHAA, SalesUnitA);
				   
				   Thread.sleep(3000);
				   click(btn_ScheduleCodeHABtnA);
				   sendKeys(txt_ScheduleCodeHATxtA, ScheduleCodeHATxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ScheduleCodeHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ScheduleCodeHASelA);
				   
				   Thread.sleep(3000);
				   click(btn_ResponsibleUserHABtnA);
				   sendKeys(txt_ResponsibleUserHATxtA, ResponsibleUserHATxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ResponsibleUserHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ResponsibleUserHASelA);
				   Thread.sleep(3000);
				   selectText(txt_HireBasisA, "Units");
				   
				   Thread.sleep(3000);
				   click(btn_ResourceInformationTabA);
				   selectText(txt_TypeHAA, "Resource");
				   Thread.sleep(3000);
				   click(btn_ReferenceCodeHABtnA);
				   sendKeys(txt_ReferenceCodeHATxtA, ReferenceCodeHA1TxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ReferenceCodeHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ReferenceCodeHASelA);
				   
				   sendKeys(txt_OpeningUnitA, "10");
				   Thread.sleep(3000);
				   click(btn_RateRangeA);
				   sendKeys(txt_MinimumUnitA, "5");
				   sendKeys(txt_Rate1A, "5");
				   Thread.sleep(3000);
				   click(btn_RateRangeSetupPlusA);
				   sendKeys(txt_RateRangeSetupStartA, "6");
				   sendKeys(txt_RateRangeSetupEndA, "0");
				   sendKeys(txt_Rate2A, "10");
				   Thread.sleep(3000);
				   click(btn_RateRangeSetupApplyA);
				   
				   Thread.sleep(3000);
				   click(btn_DraftA);
				   Thread.sleep(5000);
				   if (isDisplayed(txt_PageDraft1A)) {
						writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
								"User Can draft the Hire Agreement Form Successfully", "pass");
				   } else {
						writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
								"User Can draft the Hire Agreement Form Fail", "fail");
				   }

				   Thread.sleep(3000);
				   click(btn_ReleaseA);
				   Thread.sleep(3000);
				   trackCode = getText(txt_trackCodeA);
				   String HANo = trackCode;
				   
				   
				   Thread.sleep(5000);
				   if (isDisplayed(txt_PageRelease1A)) {
							writeTestResults("Verify that able to Release the Hire Agreement Form", "User Can Release the Hire Agreement Form",
								"User Can Release the Hire Agreement Form Successfully", "pass");
				   } else {
							writeTestResults("Verify that able to Release the Hire Agreement Form", "User Can Release the Hire Agreement Form",
									"User Can Release the Hire Agreement Form Fail", "fail");
				   }
				   
				   Thread.sleep(3000);
				   click(btn_DuplicateHireAgreementA);
				   
				   Thread.sleep(3000);
				   sendKeys(txt_AgreementNoTxtA, HANo);
				   sendKeys(txt_TitleTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   j7.executeScript("$(\"#txtContractDate\").datepicker(\"setDate\", new Date())");
				   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker(\"setDate\", new Date())");
				   j7.executeScript("$(\"#txtEffectiveTo\").datepicker(\"setDate\", new Date())");
				   
				   Thread.sleep(3000);
				   click(btn_DraftA);
				   Thread.sleep(3000);
				   if (isDisplayed(txt_HireAgreementDuplicateValidatorA)) {
						writeTestResults("Verify that user cannot create 2 or more hire agreements with same agreement NO", "user cannot create 2 or more hire agreements with same agreement NO",
								"user cannot create 2 or more hire agreements with same agreement NO Successfully", "pass");
				   } else {
						writeTestResults("Verify that user cannot create 2 or more hire agreements with same agreement NO", "user cannot create 2 or more hire agreements with same agreement NO",
								"user cannot create 2 or more hire agreements with same agreement NO Fail", "fail");
				   }
		    }
		    
		    // Service_HA_045
		    public void VerifyThatUserShouldNotBeAbleToReleaseTheServiceHireAgreementWithoutAnyResourceInformation() throws Exception {
		    	
				   VerifyThatUserCanNavigateToTheHireAgreementForm();
				   
				   Thread.sleep(3000);
				   LocalTime obj1 = LocalTime.now();
				   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj1);
				   sendKeys(txt_TitleTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   JavascriptExecutor j7 = (JavascriptExecutor)driver;
				   j7.executeScript("$(\"#txtContractDate\").datepicker(\"setDate\", new Date())");
				   
				   
				   selectText(txt_ContractGroupTxtA, "ContractGroupService");
				   
				   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker(\"setDate\", new Date())");
				   j7.executeScript("$(\"#txtEffectiveTo\").datepicker(\"setDate\", new Date())");
				   
				   sendKeys(txt_DescriptionTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   click(btn_CustomerAccountHABtnA);
				   sendKeys(txt_CustomerAccountHATxtA, CustomerAccountTextA);
				   Thread.sleep(3000);
				   pressEnter(txt_CustomerAccountHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_CustomerAccountHASelA);
				   
				   selectText(txt_SalesUnitHAA, SalesUnitA);
				   
				   Thread.sleep(3000);
				   click(btn_ScheduleCodeHABtnA);
				   sendKeys(txt_ScheduleCodeHATxtA, ScheduleCodeHATxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ScheduleCodeHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ScheduleCodeHASelA);
				   
				   Thread.sleep(3000);
				   click(btn_ResponsibleUserHABtnA);
				   sendKeys(txt_ResponsibleUserHATxtA, ResponsibleUserHATxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ResponsibleUserHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ResponsibleUserHASelA);
				   Thread.sleep(3000);
				   selectText(txt_HireBasisA, "Units");
				  
				   Thread.sleep(3000);
				   click(btn_DraftA);

				   Thread.sleep(3000);
				   JavascriptExecutor jse = (JavascriptExecutor) driver;
			       jse.executeScript("$(closeMessageBox()).click()");
			       
				   Thread.sleep(5000);
				   if (isDisplayed(txt_ReferenceCodeErrorA) && isDisplayed(txt_ServiceProductErrorA)) {
						writeTestResults("Verify that user should not be able to release the service hire Agreement without any resource information", "user should not be able to release the service hire Agreement without any resource information",
								"user should not be able to release the service hire Agreement without any resource information Successfully", "pass");
				   } else {
						writeTestResults("Verify that user should not be able to release the service hire Agreement without any resource information", "user should not be able to release the service hire Agreement without any resource information",
								"user should not be able to release the service hire Agreement without any resource information Fail", "fail");
				   }
		    }
		    
		    // Service_HA_046
		    public void VerifyThatResoponsibleUserFieldIsMarkedAsMandatoryFieldAfterSelectingaScheduleCode() throws Exception {
		    	
				   VerifyThatUserCanNavigateToTheHireAgreementForm();
				   
				   Thread.sleep(3000);
				   LocalTime obj1 = LocalTime.now();
				   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj1);
				   sendKeys(txt_TitleTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   JavascriptExecutor j7 = (JavascriptExecutor)driver;
				   j7.executeScript("$(\"#txtContractDate\").datepicker(\"setDate\", new Date())");
				   
				   
				   selectText(txt_ContractGroupTxtA, "ContractGroupService");
				   
				   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker(\"setDate\", new Date())");
				   j7.executeScript("$(\"#txtEffectiveTo\").datepicker(\"setDate\", new Date())");
				   
				   sendKeys(txt_DescriptionTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   click(btn_CustomerAccountHABtnA);
				   sendKeys(txt_CustomerAccountHATxtA, CustomerAccountTextA);
				   Thread.sleep(3000);
				   pressEnter(txt_CustomerAccountHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_CustomerAccountHASelA);
				   
				   selectText(txt_SalesUnitHAA, SalesUnitA);
				   
				   Thread.sleep(3000);
				   click(btn_ScheduleCodeHABtnA);
				   sendKeys(txt_ScheduleCodeHATxtA, ScheduleCodeHATxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ScheduleCodeHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ScheduleCodeHASelA);

				   Thread.sleep(3000);
				   if (isDisplayed(txt_ResponsibleUserValidatorA)) {
						writeTestResults("Verify that \"Resoponsible user\" field is marked as Mandatory field after selecting a schedule code", "\"Resoponsible user\" field is marked as Mandatory field after selecting a schedule code",
								"\"Resoponsible user\" field is marked as Mandatory field after selecting a schedule code Successfully", "pass");
				   } else {
						writeTestResults("Verify that \"Resoponsible user\" field is marked as Mandatory field after selecting a schedule code", "\"Resoponsible user\" field is marked as Mandatory field after selecting a schedule code",
								"\"Resoponsible user\" field is marked as Mandatory field after selecting a schedule code Fail", "fail");
				   }
		    }
		    
		    // Service_HA_047
		    public void VerifyThatValueInBillingBasisFieldIsChangedAccordingToTheValueSelectingToTheFieldHireBasis() throws Exception {
		    	
				   VerifyThatUserCanNavigateToTheHireAgreementForm();
				   
				   Thread.sleep(3000);
				   LocalTime obj1 = LocalTime.now();
				   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj1);
				   sendKeys(txt_TitleTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   JavascriptExecutor j7 = (JavascriptExecutor)driver;
				   j7.executeScript("$(\"#txtContractDate\").datepicker('setDate', new Date())");
				   
				   
				   selectText(txt_ContractGroupTxtA, "ContractGroupService");
				   
				   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker('setDate', new Date())");
				   j7.executeScript("$(\"#txtEffectiveTo\").datepicker('setDate', new Date())");
				   
				   sendKeys(txt_DescriptionTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   click(btn_CustomerAccountHABtnA);
				   sendKeys(txt_CustomerAccountHATxtA, CustomerAccountTextA);
				   Thread.sleep(3000);
				   pressEnter(txt_CustomerAccountHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_CustomerAccountHASelA);
				   
				   selectText(txt_SalesUnitHAA, SalesUnitA);
				   Thread.sleep(3000);
				   selectText(txt_HireBasisA, "Monthly");
				   
				   Thread.sleep(3000);
				   String BillingBasis1A = new Select(driver.findElement(By.xpath(txt_BillingBasisA))).getFirstSelectedOption().getText();
				   
				   Thread.sleep(3000);
				   selectText(txt_HireBasisA, "Daily");
				   
				   Thread.sleep(3000);
				   String BillingBasis2A = new Select(driver.findElement(By.xpath(txt_BillingBasisA))).getFirstSelectedOption().getText();
				   
				   Thread.sleep(3000);
				   if (BillingBasis1A.contentEquals("Monthly") && BillingBasis2A.contentEquals("Daily")) {
						writeTestResults("Verify that value in \"Billing Basis\" field is changed according to the value selecting to the field \"Hire Basis\"", "value in \"Billing Basis\" field is changed according to the value selecting to the field \"Hire Basis\"",
								"value in \"Billing Basis\" field is changed according to the value selecting to the field \"Hire Basis\" Successfully", "pass");
				   } else {
						writeTestResults("Verify that value in \"Billing Basis\" field is changed according to the value selecting to the field \"Hire Basis\"", "value in \"Billing Basis\" field is changed according to the value selecting to the field \"Hire Basis\"",
								"value in \"Billing Basis\" field is changed according to the value selecting to the field \"Hire Basis\" Fail", "fail");
				   }
		    }
		    
		    // Service_HA_048
		    public void VerifyThatUnableToReverseTheHireAgreementAfterActualUpdate() throws Exception {
		    	
				   VerifyThatHireAgreemntCanBeCreateByAddingMultipleResources();
				   Thread.sleep(3000);
				   click(btn_ActionBtnA);
				   Thread.sleep(3000);
				   click(btn_ActualUpdateHABtnA);
				   
				   selectText(txt_cboxActualResourcesA, ReferenceCodeHA1TxtA);
				   sendKeys(txt_ActualUnitA, "2");
				   Thread.sleep(3000);
				   click(btn_CheckoutHAA);
				   
				   Thread.sleep(3000);
				   JavascriptExecutor j1 = (JavascriptExecutor) driver;
				   j1.executeScript("$(\"#divTaskcusAd\").parent().find('.dialogbox-buttonarea > .button').click()");
				   
				   Thread.sleep(3000);
				   click(btn_ActionBtnA);
				   Thread.sleep(3000);
				   click(btn_ActualUpdateHABtnA);
				   
				   selectText(txt_cboxActualResourcesA, ReferenceCodeHA2TxtA);
				   sendKeys(txt_ActualUnitA, "2");
				   Thread.sleep(3000);
				   click(btn_CheckoutHAA);
				   
				   Thread.sleep(3000);
				   j1.executeScript("$(\"#divTaskcusAd\").parent().find('.dialogbox-buttonarea > .button').click()");
	
				   pageRefersh();
				   Thread.sleep(3000);
				   click(btn_ActionBtnA);
				   Thread.sleep(3000);
				   if(!isDisplayed(btn_ReverseHABtnA)){
						writeTestResults("Verify that unable to reverse the hire agreement after actual update", 
								"unable to reverse the hire agreement after actual update",
								"unable to reverse the hire agreement after actual update Successfully", "pass");
				   } else {
						writeTestResults("Verify that unable to reverse the hire agreement after actual update", 
								"unable to reverse the hire agreement after actual update",
								"unable to reverse the hire agreement after actual update Fail", "fail");
				   }
		    }
		    
		    // Service_HA_049
		    public void VerifyThatScheduleCodeLookUpOnlyDisplayedTheCodesWhichAreRelatedToTheBillingBasisBillingBasisDaily() throws Exception {
		    	
				   VerifyThatUserCanNavigateToTheHireAgreementForm();
				   
				   Thread.sleep(3000);
				   LocalTime obj1 = LocalTime.now();
				   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj1);
				   sendKeys(txt_TitleTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   JavascriptExecutor j7 = (JavascriptExecutor)driver;
				   j7.executeScript("$(\"#txtContractDate\").datepicker('setDate', new Date())");
				   
				   
				   selectText(txt_ContractGroupTxtA, "ContractGroupService");
				   
				   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker('setDate', new Date())");
				   j7.executeScript("$(\"#txtEffectiveTo\").datepicker('setDate', new Date())");
				   
				   sendKeys(txt_DescriptionTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   click(btn_CustomerAccountHABtnA);
				   sendKeys(txt_CustomerAccountHATxtA, CustomerAccountTextA);
				   Thread.sleep(3000);
				   pressEnter(txt_CustomerAccountHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_CustomerAccountHASelA);
				   
				   selectText(txt_SalesUnitHAA, SalesUnitA);
				   Thread.sleep(3000);
				   selectText(txt_HireBasisA, "Units");
				   
				   selectText(txt_BillingBasisA, "Daily");
				   
				   Thread.sleep(3000);
				   click(btn_ScheduleCodeHABtnA);
				   pressEnter(txt_ScheduleCodeHATxtA);
				   Thread.sleep(3000);
				   if(!isDisplayed(txt_ScheduleCodeNoA.replace("SCS", ScheduleCodeHA2TxtA)) && !isDisplayed(txt_ScheduleCodeNoA.replace("SCS", ScheduleCodeHA3TxtA))){
						writeTestResults("Verify that Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Daily)", 
								"Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Daily)",
								"Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Daily) Successfully", "pass");
				   } else {
						writeTestResults("Verify that Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Daily)", 
								"Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Daily)",
								"Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Daily) Fail", "fail");
				   }
				   
				   WaitElement(txt_ScheduleCodeHATxtA);
				   sendKeys(txt_ScheduleCodeHATxtA, ScheduleCodeHA4TxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ScheduleCodeHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ReferenceCodeHA4SelA);
				   
				   
				   Thread.sleep(3000);
				   String ScheduleCodeA= driver.findElement(By.xpath(txt_ScheduleCodeA)).getAttribute("value");
				   
				   Thread.sleep(3000);
				   if(ScheduleCodeA.contentEquals(ScheduleCodeHA4TxtA)){
						writeTestResults("Verify that Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Daily)", 
								"Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Daily)",
								"Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Daily) Successfully", "pass");
				   } else {
						writeTestResults("Verify that Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Daily)", 
								"Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Daily)",
								"Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Daily) Fail", "fail");
				   }
		    }
		    
		    // Service_HA_050
		    public void VerifyThatScheduleCodeLookUpOnlyDisplayedTheCodesWhichAreRelatedToTheBillingBasisBillingBasisMonthly() throws Exception {
		    	
		    	
				   VerifyThatUserCanNavigateToTheHireAgreementForm();
				   
				   Thread.sleep(3000);
				   LocalTime obj1 = LocalTime.now();
				   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj1);
				   sendKeys(txt_TitleTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   JavascriptExecutor j7 = (JavascriptExecutor)driver;
				   j7.executeScript("$(\"#txtContractDate\").datepicker('setDate', new Date())");
				   
				   
				   selectText(txt_ContractGroupTxtA, "ContractGroupService");
				   
				   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker('setDate', new Date())");
				   j7.executeScript("$(\"#txtEffectiveTo\").datepicker('setDate', new Date())");
				   
				   sendKeys(txt_DescriptionTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   click(btn_CustomerAccountHABtnA);
				   sendKeys(txt_CustomerAccountHATxtA, CustomerAccountTextA);
				   Thread.sleep(3000);
				   pressEnter(txt_CustomerAccountHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_CustomerAccountHASelA);
				   
				   selectText(txt_SalesUnitHAA, SalesUnitA);
				   Thread.sleep(3000);
				   selectText(txt_HireBasisA, "Units");
				   
				   selectText(txt_BillingBasisA, "Monthly");
				   
				   Thread.sleep(3000);
				   click(btn_ScheduleCodeHABtnA);
				   pressEnter(txt_ScheduleCodeHATxtA);
				   Thread.sleep(3000);
				   if(!isDisplayed(txt_ScheduleCodeNoA.replace("SCS", ScheduleCodeHATxtA)) && !isDisplayed(txt_ScheduleCodeNoA.replace("SCS", ScheduleCodeHA4TxtA))
						   && !isDisplayed(txt_ScheduleCodeNoA.replace("SCS", ScheduleCodeHA5TxtA))){
						writeTestResults("Verify that Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Monthly)", 
								"Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Monthly)",
								"Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Monthly) Successfully", "pass");
				   } else {
						writeTestResults("Verify that Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Monthly)", 
								"Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Monthly)",
								"Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Monthly) Fail", "fail");
				   }
				   
				   WaitElement(txt_ScheduleCodeHATxtA);
				   sendKeys(txt_ScheduleCodeHATxtA, ScheduleCodeHA2TxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ScheduleCodeHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ReferenceCodeHA2SelA);
				   
				   
				   Thread.sleep(3000);
				   String ScheduleCodeA= driver.findElement(By.xpath(txt_ScheduleCodeA)).getAttribute("value");
				   
				   Thread.sleep(3000);
				   if(ScheduleCodeA.contentEquals(ScheduleCodeHA2TxtA)){
						writeTestResults("Verify that Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Monthly)", 
								"Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Monthly)",
								"Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Monthly) Successfully", "pass");
				   } else {
						writeTestResults("Verify that Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Monthly)", 
								"Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Monthly)",
								"Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Monthly) Fail", "fail");
				   }
		    }
		    
		    // Service_HA_051
		    public void VerifyThatScheduleCodeLookUpOnlyDisplayedTheCodesWhichAreRelatedToTheBillingBasisBillingBasisWeekly() throws Exception {
		    	
		    	
				   VerifyThatUserCanNavigateToTheHireAgreementForm();
				   
				   Thread.sleep(3000);
				   LocalTime obj1 = LocalTime.now();
				   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj1);
				   sendKeys(txt_TitleTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   JavascriptExecutor j7 = (JavascriptExecutor)driver;
				   j7.executeScript("$(\"#txtContractDate\").datepicker('setDate', new Date())");
				   
				   
				   selectText(txt_ContractGroupTxtA, "ContractGroupService");
				   
				   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker('setDate', new Date())");
				   j7.executeScript("$(\"#txtEffectiveTo\").datepicker('setDate', new Date())");
				   
				   sendKeys(txt_DescriptionTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   click(btn_CustomerAccountHABtnA);
				   sendKeys(txt_CustomerAccountHATxtA, CustomerAccountTextA);
				   Thread.sleep(3000);
				   pressEnter(txt_CustomerAccountHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_CustomerAccountHASelA);
				   
				   selectText(txt_SalesUnitHAA, SalesUnitA);
				   Thread.sleep(3000);
				   selectText(txt_HireBasisA, "Units");
				   
				   selectText(txt_BillingBasisA, "Weekly");
				   
				   Thread.sleep(3000);
				   click(btn_ScheduleCodeHABtnA);
				   pressEnter(txt_ScheduleCodeHATxtA);
				   Thread.sleep(3000);
				   if(!isDisplayed(txt_ScheduleCodeNoA.replace("SCS", ScheduleCodeHATxtA)) && !isDisplayed(txt_ScheduleCodeNoA.replace("SCS", ScheduleCodeHA4TxtA))
						   && !isDisplayed(txt_ScheduleCodeNoA.replace("SCS", ScheduleCodeHA3TxtA)) && !isDisplayed(txt_ScheduleCodeNoA.replace("SCS", ScheduleCodeHA2TxtA))){
						writeTestResults("Verify that Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Weekly)", 
								"Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Weekly)",
								"Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Weekly) Successfully", "pass");
				   } else {
						writeTestResults("Verify that Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Weekly)", 
								"Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Weekly)",
								"Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Weekly) Fail", "fail");
				   }
				   
				   WaitElement(txt_ScheduleCodeHATxtA);
				   sendKeys(txt_ScheduleCodeHATxtA, ScheduleCodeHA5TxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ScheduleCodeHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ReferenceCodeHA5SelA);
				   
				   
				   Thread.sleep(3000);
				   String ScheduleCodeA= driver.findElement(By.xpath(txt_ScheduleCodeA)).getAttribute("value");
				   
				   Thread.sleep(3000);
				   if(ScheduleCodeA.contentEquals(ScheduleCodeHA5TxtA)){
						writeTestResults("Verify that Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Weekly)", 
								"Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Weekly)",
								"Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Weekly) Successfully", "pass");
				   } else {
						writeTestResults("Verify that Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Weekly)", 
								"Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Weekly)",
								"Schedule code look up only displayed the codes which are related to the billing basis (Billing basis = Weekly) Fail", "fail");
				   }
		    }
		    
		    // Service_HA_052
		    public void VerifyThatPrepareFieldIsDisableFromEditing() throws Exception {
		    	
		    	VerifyThatUserCanNavigateToTheHireAgreementForm();
		    	
				Thread.sleep(3000);
				String PreparerA = getAttribute(txt_PreparerA, "disabled");
				Thread.sleep(3000);
				if(PreparerA.equals("true")){
						writeTestResults("Verify that prepare field is disable from editing", 
								"prepare field is disable from editing",
								"prepare field is disable from editing Successfully", "pass");
				} else {
						writeTestResults("Verify that prepare field is disable from editing", 
								"prepare field is disable from editing",
								"prepare field is disable from editing Fail", "fail");
					}
		    }
		    
		    // Service_HA_053
		    public void VerifyThatPrepareFieldIsFilledWithTheUserNameOfTheSystemLoggedUser() throws Exception {
		    	
		    	VerifyThatUserCanNavigateToTheHireAgreementForm();
				Thread.sleep(3000);
				String PreparerA= driver.findElement(By.xpath(txt_PreparerA)).getAttribute("value");
				Thread.sleep(3000);
				if(PreparerA.contentEquals(ResponsibleUserHATxtA)){
						writeTestResults("Verify that Prepare field is filled with the user name of the system logged user", 
								"Prepare field is filled with the user name of the system logged user",
								"Prepare field is filled with the user name of the system logged user Successfully", "pass");
				} else {
						writeTestResults("Verify that Prepare field is filled with the user name of the system logged user", 
								"Prepare field is filled with the user name of the system logged user",
								"Prepare field is filled with the user name of the system logged user Fail", "fail");
				}
		    }
		    
		    // Service_HA_054
		    public void VerifyThatAgreedExRateFieldIsDisableForTheCurrencyLKR() throws Exception {
		    	
		    	VerifyThatUserCanNavigateToTheHireAgreementForm();
		    	
		    	selectText(txt_CurrencyCboxA, "LKR");
				Thread.sleep(3000);
				String AgreedRateA = getAttribute(txt_AgreedRateA, "disabled");
				Thread.sleep(3000);
				if(AgreedRateA.equals("true")){
						writeTestResults("Verify that \"Agreed Ex. Rate\" field is disable for the Currency LKR", 
								"\"Agreed Ex. Rate\" field is disable for the Currency LKR",
								"\"Agreed Ex. Rate\" field is disable for the Currency LKR Successfully", "pass");
				} else {
						writeTestResults("Verify that \"Agreed Ex. Rate\" field is disable for the Currency LKR", 
								"\"Agreed Ex. Rate\" field is disable for the Currency LKR",
								"\"Agreed Ex. Rate\" field is disable for the Currency LKR Fail", "fail");
					}
		    }
		    
		    // Service_HA_055
		    public void VerifyThatAgreedExRateFieldIsEditableForTheOtherCurrenciesOtherThanTheLKR() throws Exception {
		    	
		    	VerifyThatUserCanNavigateToTheHireAgreementForm();
		    	
		    	selectText(txt_CurrencyCboxA, "USD");
				Thread.sleep(3000);
				boolean AgreedRateA = driver.findElement(By.xpath(txt_AgreedRateA)).isEnabled();
				Thread.sleep(3000);
				if(AgreedRateA==true){
						writeTestResults("Verify that \" Agreed Ex. Rate\" field is editable for the other currencies other than the LKR", 
								"\" Agreed Ex. Rate\" field is editable for the other currencies other than the LKR",
								"\" Agreed Ex. Rate\" field is editable for the other currencies other than the LKR Successfully", "pass");
				} else {
						writeTestResults("Verify that \" Agreed Ex. Rate\" field is editable for the other currencies other than the LKR", 
								"\" Agreed Ex. Rate\" field is editable for the other currencies other than the LKR",
								"\" Agreed Ex. Rate\" field is editable for the other currencies other than the LKR Fail", "fail");
					}
		    }
		    
		    // Service_HA_056
		    public void VerifyThatObjectReferanceErrorIsNotDisplayed() throws Exception {
		    	
				   VerifyThatUserCanNavigateToTheHireAgreementForm();
				   
				   Thread.sleep(3000);
				   LocalTime obj1 = LocalTime.now();
				   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj1);
				   sendKeys(txt_TitleTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   JavascriptExecutor j7 = (JavascriptExecutor)driver;
				   j7.executeScript("$(\"#txtContractDate\").datepicker(\"setDate\", new Date())");
				   
				   
				   selectText(txt_ContractGroupTxtA, "ContractGroupService");
				   
				   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker(\"setDate\", new Date())");
				   j7.executeScript("$(\"#txtEffectiveTo\").datepicker(\"setDate\", new Date())");
				   
				   sendKeys(txt_DescriptionTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   click(btn_CustomerAccountHABtnA);
				   sendKeys(txt_CustomerAccountHATxtA, CustomerAccountTextA);
				   Thread.sleep(3000);
				   pressEnter(txt_CustomerAccountHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_CustomerAccountHASelA);
				   
				   Thread.sleep(3000);
				   click(btn_ScheduleCodeHABtnA);
				   sendKeys(txt_ScheduleCodeHATxtA, ScheduleCodeHATxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ScheduleCodeHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ScheduleCodeHASelA);
				   
				   Thread.sleep(3000);
				   click(btn_ResponsibleUserHABtnA);
				   sendKeys(txt_ResponsibleUserHATxtA, ResponsibleUserHATxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ResponsibleUserHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ResponsibleUserHASelA);
				   Thread.sleep(3000);
				   selectText(txt_HireBasisA, "Units");
				   
				   Thread.sleep(3000);
				   click(btn_ResourceInformationTabA);
				   selectText(txt_TypeHAA, "Resource");
				   Thread.sleep(3000);
				   click(btn_ReferenceCodeHABtnA);
				   sendKeys(txt_ReferenceCodeHATxtA, ReferenceCodeHA1TxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ReferenceCodeHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ReferenceCodeHASelA);
				   
				   sendKeys(txt_OpeningUnitA, "10");
				   Thread.sleep(3000);
				   click(btn_RateRangeA);
				   sendKeys(txt_MinimumUnitA, "5");
				   sendKeys(txt_Rate1A, "5");
				   Thread.sleep(3000);
				   click(btn_RateRangeSetupPlusA);
				   sendKeys(txt_RateRangeSetupStartA, "6");
				   sendKeys(txt_RateRangeSetupEndA, "0");
				   sendKeys(txt_Rate2A, "10");
				   Thread.sleep(3000);
				   click(btn_RateRangeSetupApplyA);
				   
				   Thread.sleep(3000);
				   click(btn_DraftA);
				   Thread.sleep(3000);
				   JavascriptExecutor jse = (JavascriptExecutor) driver;
			       jse.executeScript("$(closeMessageBox()).click()");
			       
				   Thread.sleep(3000);
				   if (isDisplayed(txt_SalesUnitValidationA)&&isDisplayed(txt_AccountOwnerValidationA)) {
						writeTestResults("Verify that Object Referance error is not displayed", "Object Referance error is not displayed",
								"Object Referance error is not displayed Successfully", "pass");
				   } else {
						writeTestResults("Verify that Object Referance error is not displayed", "Object Referance error is not displayed",
								"Object Referance error is not displayed Fail", "fail");
				   }
		    }
		    
		    // Service_HA_057
		    public void VerifyThatScheduleCodeFieldIsClearedAfterChangingTheBillingBasis() throws Exception {
		    	
				   VerifyThatUserCanNavigateToTheHireAgreementForm();
				   
				   Thread.sleep(3000);
				   LocalTime obj1 = LocalTime.now();
				   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj1);
				   sendKeys(txt_TitleTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   JavascriptExecutor j7 = (JavascriptExecutor)driver;
				   j7.executeScript("$(\"#txtContractDate\").datepicker(\"setDate\", new Date())");
				   
				   
				   selectText(txt_ContractGroupTxtA, "ContractGroupService");
				   
				   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker(\"setDate\", new Date())");
				   j7.executeScript("$(\"#txtEffectiveTo\").datepicker(\"setDate\", new Date())");
				   
				   sendKeys(txt_DescriptionTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   click(btn_CustomerAccountHABtnA);
				   sendKeys(txt_CustomerAccountHATxtA, CustomerAccountTextA);
				   Thread.sleep(3000);
				   pressEnter(txt_CustomerAccountHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_CustomerAccountHASelA);
				   
				   selectText(txt_SalesUnitHAA, SalesUnitA);
				   
				   
				   selectText(txt_HireBasisA, "Units");
				   selectText(txt_BillingBasisA, "Daily");
				   
				   Thread.sleep(3000);
				   click(btn_ScheduleCodeHABtnA);
				   sendKeys(txt_ScheduleCodeHATxtA, ScheduleCodeHATxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ScheduleCodeHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ScheduleCodeHASelA);


				   selectText(txt_BillingBasisA, "Monthly");
				   Thread.sleep(3000);
//				   if (!getText(txt_ScheduleCodeA).contentEquals(ScheduleCodeHATxtA)) {
				   if (getText(txt_ScheduleCodeA).isEmpty()) {
						writeTestResults("Verify that Schedule code field is cleared after changing the \"Billing Basis\"", "Schedule code field is cleared after changing the \"Billing Basis\"",
								"Schedule code field is cleared after changing the \"Billing Basis\" Successfully", "pass");
				   } else {
						writeTestResults("Verify that Schedule code field is cleared after changing the \"Billing Basis\"", "Schedule code field is cleared after changing the \"Billing Basis\"",
								"Schedule code field is cleared after changing the \"Billing Basis\" Fail", "fail");
				   }
		    }
		    
		    // Service_HA_058
		    public void VerifyThatUserAbleToChangeThePostOrganizationUnitBeforDraftTheHireHgreement() throws Exception {
		    	
				   VerifyThatUserCanNavigateToTheHireAgreementForm();
				   
				   Thread.sleep(3000);
				   LocalTime obj1 = LocalTime.now();
				   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj1);
				   sendKeys(txt_TitleTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   JavascriptExecutor j7 = (JavascriptExecutor)driver;
				   j7.executeScript("$(\"#txtContractDate\").datepicker(\"setDate\", new Date())");
				   
				   
				   selectText(txt_ContractGroupTxtA, "ContractGroupService");
				   
				   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker(\"setDate\", new Date())");
				   j7.executeScript("$(\"#txtEffectiveTo\").datepicker(\"setDate\", new Date())");
				   
				   sendKeys(txt_DescriptionTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   click(btn_CustomerAccountHABtnA);
				   sendKeys(txt_CustomerAccountHATxtA, CustomerAccountTextA);
				   Thread.sleep(3000);
				   pressEnter(txt_CustomerAccountHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_CustomerAccountHASelA);
				   
				   selectText(txt_SalesUnitHAA, SalesUnitA);
				   
				   Thread.sleep(3000);
				   click(btn_ScheduleCodeHABtnA);
				   sendKeys(txt_ScheduleCodeHATxtA, ScheduleCodeHATxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ScheduleCodeHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ScheduleCodeHASelA);
				   
				   Thread.sleep(3000);
				   click(btn_ResponsibleUserHABtnA);
				   sendKeys(txt_ResponsibleUserHATxtA, ResponsibleUserHATxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ResponsibleUserHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ResponsibleUserHASelA);
				   Thread.sleep(3000);
				   selectText(txt_HireBasisA, "Units");
				   
				   Thread.sleep(3000);
				   click(btn_ResourceInformationTabA);
				   selectText(txt_TypeHAA, "Resource");
				   Thread.sleep(3000);
				   click(btn_ReferenceCodeHABtnA);
				   sendKeys(txt_ReferenceCodeHATxtA, ReferenceCodeHA1TxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ReferenceCodeHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ReferenceCodeHASelA);
				   
				   sendKeys(txt_OpeningUnitA, "10");
				   Thread.sleep(3000);
				   click(btn_RateRangeA);
				   sendKeys(txt_MinimumUnitA, "5");
				   sendKeys(txt_Rate1A, "5");
				   Thread.sleep(3000);
				   click(btn_RateRangeSetupPlusA);
				   sendKeys(txt_RateRangeSetupStartA, "6");
				   sendKeys(txt_RateRangeSetupEndA, "0");
				   sendKeys(txt_Rate2A, "10");
				   Thread.sleep(3000);
				   click(btn_RateRangeSetupApplyA);
				   Thread.sleep(3000);
				   click(txt_PostBusinessUnitTxtA);
				   selectText(sel_PostBusinessUnitSelA, PostBusinessUnitTxtA);
				   
				   Thread.sleep(3000);
				   click(btn_DraftA);
				   Thread.sleep(5000);
				   if (isDisplayed(txt_PageDraft1A)) {
						writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
								"User Can draft the Hire Agreement Form Successfully", "pass");
				   } else {
						writeTestResults("Verify that able to draft the Hire Agreement Form", "User Can draft the Hire Agreement Form",
								"User Can draft the Hire Agreement Form Fail", "fail");
				   }

				   Thread.sleep(3000);
				   click(btn_ReleaseA);
				   Thread.sleep(3000);
				   trackCode = getText(txt_trackCodeA);
				   Thread.sleep(5000);
				   if (isDisplayed(txt_PageRelease1A)) {
						writeTestResults("Verify that able to Release the Hire Agreement Form", "User Can Release the Hire Agreement Form",
								"User Can Release the Hire Agreement Form Successfully", "pass");
				   } else {
						writeTestResults("Verify that able to Release the Hire Agreement Form", "User Can Release the Hire Agreement Form",
									"User Can Release the Hire Agreement Form Fail", "fail");
				   }
				   
				   if (getText(txt_PostBusinessUnitTxtAfterReleaseA).contentEquals(PostBusinessUnitAfterReleaseTxtA)) {
						writeTestResults("Verify that user able to change the Post organization Unit befor draft the hire agreement", "user able to change the Post organization Unit befor draft the hire agreement",
								"user able to change the Post organization Unit befor draft the hire agreement Successfully", "pass");
				   } else {
						writeTestResults("Verify that user able to change the Post organization Unit befor draft the hire agreement", "user able to change the Post organization Unit befor draft the hire agreement",
									"user able to change the Post organization Unit befor draft the hire agreement Fail", "fail");
				   }
				   
		    }
		    
		    // Service_HA_059
		    public void VerifyThatResourceCodeAndDescriptionIsDisplayed() throws Exception {
		    	
		    	   Thread.sleep(3000);
		    	   click(btn_navigationmenu);
				   Thread.sleep(3000);
				   click(btn_AdministrationBtnA);
				   click(btn_BalancingLevelSettingsBtnA);
				   Thread.sleep(3000);
				   click(btn_GeneralBtnA);
				   selectText(btn_cboxConfigResourceA, "Code and Description");
				   click(btn_GeneralUpdateA);
				   
				   Thread.sleep(3000);
				   clickNavigation();
				   VerifyThatUserCanNavigateToTheHireAgreementForm();
				   
				   Thread.sleep(3000);
				   LocalTime obj1 = LocalTime.now();
				   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj1);
				   sendKeys(txt_TitleTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   JavascriptExecutor j7 = (JavascriptExecutor)driver;
				   j7.executeScript("$(\"#txtContractDate\").datepicker(\"setDate\", new Date())");
				   
				   
				   selectText(txt_ContractGroupTxtA, "ContractGroupService");
				   
				   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker(\"setDate\", new Date())");
				   j7.executeScript("$(\"#txtEffectiveTo\").datepicker(\"setDate\", new Date())");
				   
				   sendKeys(txt_DescriptionTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   click(btn_CustomerAccountHABtnA);
				   sendKeys(txt_CustomerAccountHATxtA, CustomerAccountTextA);
				   Thread.sleep(3000);
				   pressEnter(txt_CustomerAccountHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_CustomerAccountHASelA);
				   
				   selectText(txt_SalesUnitHAA, SalesUnitA);
				   
				   Thread.sleep(3000);
				   click(btn_ScheduleCodeHABtnA);
				   sendKeys(txt_ScheduleCodeHATxtA, ScheduleCodeHATxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ScheduleCodeHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ScheduleCodeHASelA);
				   
				   Thread.sleep(3000);
				   click(btn_ResponsibleUserHABtnA);
				   sendKeys(txt_ResponsibleUserHATxtA, ResponsibleUserHATxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ResponsibleUserHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ResponsibleUserHASelA);
				   Thread.sleep(3000);
				   selectText(txt_HireBasisA, "Units");
				   
				   Thread.sleep(3000);
				   click(btn_ResourceInformationTabA);
				   selectText(txt_TypeHAA, "Resource");
				   Thread.sleep(3000);
				   click(btn_ReferenceCodeHABtnA);
				   sendKeys(txt_ReferenceCodeHATxtA, ResourceCodeAndDescriptionA);
				   Thread.sleep(3000);
				   pressEnter(txt_ReferenceCodeHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ReferenceCodeHASelA);
				   
				   Thread.sleep(3000);
				   if (isDisplayed(txt_ResourceCodeAndDescriptionA)){
				        writeTestResults("Verify that Resource code and Description is displayed", "Resource code and Description is displayed",
							"Resource code and Description is displayed Successfully", "pass");
		           } else {
		        	    writeTestResults("Verify that Resource code and Description is displayed", "Resource code and Description is displayed",
								"Resource code and Description is displayed Fail", "fail");
		           }
				   
		    	   Thread.sleep(6000);
		    	   click(btn_navigationmenu);
				   Thread.sleep(3000);
				   click(btn_AdministrationBtnA);
				   click(btn_BalancingLevelSettingsBtnA);
				   Thread.sleep(3000);
				   click(btn_GeneralBtnA);
				   selectText(btn_cboxConfigResourceA, "Code and Description");
				   click(btn_GeneralUpdateA);
		    }
		    
		    // Service_HA_060
		    public void VerifyThatResourceCodeIsDisplayed() throws Exception {
		    	
		    	
		    	   Thread.sleep(3000);
		    	   click(btn_navigationmenu);
				   Thread.sleep(3000);
				   click(btn_AdministrationBtnA);
				   click(btn_BalancingLevelSettingsBtnA);
				   Thread.sleep(3000);
				   click(btn_GeneralBtnA);
				   selectText(btn_cboxConfigResourceA, "Code Only");
				   click(btn_GeneralUpdateA);
				   
				   Thread.sleep(3000);
				   clickNavigation();
				   VerifyThatUserCanNavigateToTheHireAgreementForm();
				   
				   Thread.sleep(3000);
				   LocalTime obj1 = LocalTime.now();
				   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj1);
				   sendKeys(txt_TitleTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   JavascriptExecutor j7 = (JavascriptExecutor)driver;
				   j7.executeScript("$(\"#txtContractDate\").datepicker(\"setDate\", new Date())");
				   
				   
				   selectText(txt_ContractGroupTxtA, "ContractGroupService");
				   
				   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker(\"setDate\", new Date())");
				   j7.executeScript("$(\"#txtEffectiveTo\").datepicker(\"setDate\", new Date())");
				   
				   sendKeys(txt_DescriptionTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   click(btn_CustomerAccountHABtnA);
				   sendKeys(txt_CustomerAccountHATxtA, CustomerAccountTextA);
				   Thread.sleep(3000);
				   pressEnter(txt_CustomerAccountHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_CustomerAccountHASelA);
				   
				   selectText(txt_SalesUnitHAA, SalesUnitA);
				   
				   Thread.sleep(3000);
				   click(btn_ScheduleCodeHABtnA);
				   sendKeys(txt_ScheduleCodeHATxtA, ScheduleCodeHATxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ScheduleCodeHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ScheduleCodeHASelA);
				   
				   Thread.sleep(3000);
				   click(btn_ResponsibleUserHABtnA);
				   sendKeys(txt_ResponsibleUserHATxtA, ResponsibleUserHATxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ResponsibleUserHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ResponsibleUserHASelA);
				   Thread.sleep(3000);
				   selectText(txt_HireBasisA, "Units");
				   
				   Thread.sleep(3000);
				   click(btn_ResourceInformationTabA);
				   selectText(txt_TypeHAA, "Resource");
				   Thread.sleep(3000);
				   click(btn_ReferenceCodeHABtnA);
				   sendKeys(txt_ReferenceCodeHATxtA, ResourceCodeA);
				   Thread.sleep(3000);
				   pressEnter(txt_ReferenceCodeHATxtA);
				   Thread.sleep(3000);
				   doubleClick(sel_ResourceCodeHASel2A);
				   
				   Thread.sleep(3000);
				   if (isDisplayed(txt_ResourceCodeA)){
				        writeTestResults("Verify that Resource code is displayed", "Resource code is displayed",
							"Resource code is displayed Successfully", "pass");
		           } else {
		        	    writeTestResults("Verify that Resource code is displayed", "Resource code is displayed",
								"Resource code is displayed Fail", "fail");
		           } 
				   
		    	   Thread.sleep(6000);
		    	   click(btn_navigationmenu);
				   Thread.sleep(3000);
				   click(btn_AdministrationBtnA);
				   click(btn_BalancingLevelSettingsBtnA);
				   Thread.sleep(3000);
				   click(btn_GeneralBtnA);
				   selectText(btn_cboxConfigResourceA, "Code and Description");
				   click(btn_GeneralUpdateA);
		    }
		    
		    // Service_HA_061
		    public void VerifyThatResourceNameIsDisplayed() throws Exception {
		    	
		    	
		    	   Thread.sleep(3000);
		    	   click(btn_navigationmenu);
				   Thread.sleep(3000);
				   click(btn_AdministrationBtnA);
				   click(btn_BalancingLevelSettingsBtnA);
				   Thread.sleep(3000);
				   click(btn_GeneralBtnA);
				   selectText(btn_cboxConfigResourceA, "Name Only");
				   click(btn_GeneralUpdateA);
				   
				   Thread.sleep(3000);
				   clickNavigation();
				   VerifyThatUserCanNavigateToTheHireAgreementForm();
				   
				   Thread.sleep(3000);
				   LocalTime obj1 = LocalTime.now();
				   sendKeys(txt_AgreementNoTxtA, "AgreeNo:"+obj1);
				   sendKeys(txt_TitleTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   JavascriptExecutor j7 = (JavascriptExecutor)driver;
				   j7.executeScript("$(\"#txtContractDate\").datepicker(\"setDate\", new Date())");
				   
				   
				   selectText(txt_ContractGroupTxtA, "ContractGroupService");
				   
				   j7.executeScript("$(\"#txtEffectiveFrom\").datepicker(\"setDate\", new Date())");
				   j7.executeScript("$(\"#txtEffectiveTo\").datepicker(\"setDate\", new Date())");
				   
				   sendKeys(txt_DescriptionTxtA, "Testing Hire Agreement");
				   
				   Thread.sleep(3000);
				   click(btn_CustomerAccountHABtnA);
				   sendKeys(txt_CustomerAccountHATxtA, CustomerAccountTextA);
				   Thread.sleep(3000);
				   pressEnter(txt_CustomerAccountHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_CustomerAccountHASelA);
				   
				   selectText(txt_SalesUnitHAA, SalesUnitA);
				   
				   Thread.sleep(3000);
				   click(btn_ScheduleCodeHABtnA);
				   sendKeys(txt_ScheduleCodeHATxtA, ScheduleCodeHATxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ScheduleCodeHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ScheduleCodeHASelA);
				   
				   Thread.sleep(3000);
				   click(btn_ResponsibleUserHABtnA);
				   sendKeys(txt_ResponsibleUserHATxtA, ResponsibleUserHATxtA);
				   Thread.sleep(3000);
				   pressEnter(txt_ResponsibleUserHATxtA);
				   Thread.sleep(3000);
				   doubleClick(Sel_ResponsibleUserHASelA);
				   Thread.sleep(3000);
				   selectText(txt_HireBasisA, "Units");
				   
				   Thread.sleep(3000);
				   click(btn_ResourceInformationTabA);
				   selectText(txt_TypeHAA, "Resource");
				   Thread.sleep(3000);
				   click(btn_ReferenceCodeHABtnA);
				   sendKeys(txt_ReferenceCodeHATxtA, ResourceDescriptionA);
				   Thread.sleep(3000);
				   pressEnter(txt_ReferenceCodeHATxtA);
				   Thread.sleep(3000);
				   doubleClick(sel_ResourceCodeHASel3A);
				   
				   Thread.sleep(3000);
				   if (isDisplayed(txt_ResourceDescriptionA)){
				        writeTestResults("Verify that Resource Name is displayed", "Resource Name is displayed",
							"Resource Name is displayed Successfully", "pass");
		           } else {
		        	    writeTestResults("Verify that Resource Name is displayed", "Resource Name is displayed",
								"Resource Name is displayed Fail", "fail");
		           } 
				   
		    	   Thread.sleep(6000);
		    	   click(btn_navigationmenu);
				   Thread.sleep(3000);
				   click(btn_AdministrationBtnA);
				   click(btn_BalancingLevelSettingsBtnA);
				   Thread.sleep(3000);
				   click(btn_GeneralBtnA);
				   selectText(btn_cboxConfigResourceA, "Code and Description");
				   click(btn_GeneralUpdateA);
		    }
		    
		    // Service_EAR_001
		    public void VerifyThatEmployeeAdvanceRequestFormIsAvailableInTheServiceModule() throws Exception {
		    	
				   WaitElement(btn_EmployeeAdvanceRequestBtnA);
				   if (isDisplayed(btn_EmployeeAdvanceRequestBtnA)){
				        writeTestResults("Verify that \"Employee Advance Request\" form is available in the service module", "\"Employee Advance Request\" form is available in the service module",
							"\"Employee Advance Request\" form is available in the service module Successfully", "pass");
		           } else {
		        	    writeTestResults("Verify that \"Employee Advance Request\" form is available in the service module", "\"Employee Advance Request\" form is available in the service module",
								"\"Employee Advance Request\" form is available in the service module Fail", "fail");
		           } 
		    }
		    
		    // Service_EAR_002
		    public void VerifyThatEmployeeAdvanceRequestFormByPageIsOpenAfterClickOnTheForm() throws Exception {
		    	
				   WaitClick(btn_EmployeeAdvanceRequestBtnA);
				   click(btn_EmployeeAdvanceRequestBtnA);
				   WaitElement(txt_EmployeeAdvanceRequestByPageA);
				   if (isDisplayed(txt_EmployeeAdvanceRequestByPageA)){
				        writeTestResults("Verify that \"Employee Advance Request\" form by page is open after click on the form", "\"Employee Advance Request\" form by page is open after click on the form",
							"\"Employee Advance Request\" form by page is open after click on the form Successfully", "pass");
		           } else {
		        	    writeTestResults("Verify that \"Employee Advance Request\" form by page is open after click on the form", "\"Employee Advance Request\" form by page is open after click on the form",
								"\"Employee Advance Request\" form by page is open after click on the form Fail", "fail");
		           } 
		    }
		    
		    // Service_EAR_003
		    public void VerifyThatFollowingColumnHeadersAreAvailabaleInTheByPage() throws Exception {
		    	
		    	VerifyThatEmployeeAdvanceRequestFormByPageIsOpenAfterClickOnTheForm();
				Thread.sleep(3000);
				if (isDisplayed(txt_DocNoLableA) && isDisplayed(txt_DocDateLableA) && isDisplayed(txt_EmployeeCodeLableA) && isDisplayed(txt_ExpenseTypeLableA)
						 && isDisplayed(txt_PurposeLableA) && isDisplayed(txt_PostBusinessUnitCodeLableA) && isDisplayed(txt_PostBusinessUnitNameLableA)){
				        writeTestResults("Verify that following column headers are availabale in the by page", "following column headers are availabale in the by page",
							"following column headers are availabale in the by page Successfully", "pass");
		        } else {
		        	    writeTestResults("Verify that following column headers are availabale in the by page", "following column headers are availabale in the by page",
								"following column headers are availabale in the by page Fail", "fail");
		        } 
		    }
		    
		    // Service_EAR_004
		    public void VerifyThatNewEmployeeAdvanceRequestButtonIsNavigatedToEmployeeAdvanceRequestNewForm() throws Exception {
		    	
		    	VerifyThatEmployeeAdvanceRequestFormByPageIsOpenAfterClickOnTheForm();
		    	click(btn_NewEmployeeAdvanceRequestBtnA);
				Thread.sleep(3000);
				if (getText(txt_NewEmployeeAdvanceRequestPageHeaderA).contentEquals("Employee Advance Request - New")){
				        writeTestResults("Verify that \"New Employee Advance Request\" button is navigated to \"Employee advance request - New\" form", 
				        		"\"New Employee Advance Request\" button is navigated to \"Employee advance request - New\" form",
							"\"New Employee Advance Request\" button is navigated to \"Employee advance request - New\" form Successfully", "pass");
		        } else {
		        	    writeTestResults("Verify that \"New Employee Advance Request\" button is navigated to \"Employee advance request - New\" form", 
		        	    		"\"New Employee Advance Request\" button is navigated to \"Employee advance request - New\" form",
								"\"New Employee Advance Request\" button is navigated to \"Employee advance request - New\" form Fail", "fail");
		        } 
		    }
		    
		    // Service_EAR_005
		    public void VerifyThatFollowingFieldsAreAvailabaleInTheEmployeeAdvanceRequestNewForm() throws Exception {
		    	
		    	VerifyThatNewEmployeeAdvanceRequestButtonIsNavigatedToEmployeeAdvanceRequestNewForm();
				Thread.sleep(3000);
				if (isDisplayed(txt_EmployeeTextFieldA) && isDisplayed(txt_ExpenseTypeTextFieldA) && isDisplayed(txt_PurposeTextFieldA) 
						&& isDisplayed(txt_AdditionalRemarkTextFieldA) && isDisplayed(txt_CurrencyTextFieldA) && isDisplayed(txt_AmountTextFieldA))
				{
				        writeTestResults("Verify that following fields are availabale in the \"Employee Advance Request - New\" form", 
				        		"following fields are availabale in the \"Employee Advance Request - New\" form",
							"following fields are availabale in the \"Employee Advance Request - New\" form Successfully", "pass");
		        } else {
		        	    writeTestResults("Verify that following fields are availabale in the \"Employee Advance Request - New\" form", 
		        	    		"following fields are availabale in the \"Employee Advance Request - New\" form",
								"following fields are availabale in the \"Employee Advance Request - New\" form Fail", "fail");
		        } 
		    	
		    }
		    
		    // Service_EAR_006
		    public void VerifyThatFollowingFieldsAreMarkedAsTheMandatoryFields() throws Exception {
		    	
		    	VerifyThatNewEmployeeAdvanceRequestButtonIsNavigatedToEmployeeAdvanceRequestNewForm();
				Thread.sleep(3000);
				if (isDisplayed(txt_ExpenseTypeTextFieldValidatorA) && isDisplayed(txt_PurposeTextFieldValidatorA) && isDisplayed(txt_AmountTextFieldValidatorA))
				{
				        writeTestResults("Verify that following fields are marked as the \"Mandatory fields\"", 
				        		"following fields are marked as the \"Mandatory fields\"",
							"following fields are marked as the \"Mandatory fields\" Successfully", "pass");
		        } else {
		        	    writeTestResults("Verify that following fields are marked as the \"Mandatory fields\"", 
		        	    		"following fields are marked as the \"Mandatory fields\"",
								"following fields are marked as the \"Mandatory fields\" Fail", "fail");
		        }
		    }
		    
		    // Service_EAR_007
		    public void VerifyThatRequiredFieldValidationsAreFired() throws Exception {
		    	
		    	VerifyThatNewEmployeeAdvanceRequestButtonIsNavigatedToEmployeeAdvanceRequestNewForm();
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("$(closeMessageBox()).click()");
				
				Thread.sleep(3000);
				if (isDisplayed(txt_PurposeTextFieldDraftValidatorA) && isDisplayed(txt_AmountTextFieldDraftValidatorA))
				{
				        writeTestResults("Verify that required field validations are fired", 
				        		"required field validations are fired",
							"required field validations are fired Successfully", "pass");
		        } else {
		        	    writeTestResults("Verify that required field validations are fired", 
		        	    		"required field validations are fired",
								"required field validations are fired Fail", "fail");
		        }
		    }
		    
		    // Service_EAR_008
		    public void VerifyThatAbleToDraftTheDocument() throws Exception {
		    	
		    	VerifyThatNewEmployeeAdvanceRequestButtonIsNavigatedToEmployeeAdvanceRequestNewForm();
		    	
		    	WaitClick(btn_EmployeeBtnEARA);
		    	click(btn_EmployeeBtnEARA);
		    	sendKeys(txt_EmployeeSearchTextEARA, EmployeeSearchTextEAR1A);
		    	Thread.sleep(3000);
		    	pressEnter(txt_EmployeeSearchTextEARA);
		    	WaitElement(sel_EmployeeSearchSelEARA.replace("Employee", EmployeeSearchTextEAR1A));
		    	doubleClick(sel_EmployeeSearchSelEARA.replace("Employee", EmployeeSearchTextEAR1A));
		    	
		    	selectText(txt_ExpenseTypeTextFieldA, "Business Trip");
		    	sendKeys(txt_PurposeTextFieldA, "Advance");
		    	sendKeys(txt_AdditionalRemarkTextFieldA, "Advance");
		    	selectText(txt_CurrencyTextFieldA, "LKR");
		    	sendKeys(txt_AmountTextFieldA, "120000");
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that able to draft the Employee Advance Request document", "User Can draft the Employee Advance Request document",
								"User Can draft the Employee Advance Request document Successfully", "pass");
				} else {
						writeTestResults("Verify that able to draft the Employee Advance Request document", "User Can draft the Employee Advance Request document",
								"User Can draft the Employee Advance Request document Fail", "fail");
				}
		    }
		    
		    // Service_EAR_009_010
		    public void VerifyUserCanEditTheDraftedEmployeeAdvanceRequestForm() throws Exception {
		    	
		    	VerifyThatAbleToDraftTheDocument();
		    	WaitClick(btn_EditA);
		    	click(btn_EditA);
		    	
		    	WaitClick(btn_EmployeeBtnEARA);
		    	click(btn_EmployeeBtnEARA);
		    	sendKeys(txt_EmployeeSearchTextEARA, EmployeeSearchTextEAR2A);
		    	Thread.sleep(3000);
		    	pressEnter(txt_EmployeeSearchTextEARA);
		    	WaitElement(sel_EmployeeSearchSelEARA.replace("Employee", EmployeeSearchTextEAR2A));
		    	doubleClick(sel_EmployeeSearchSelEARA.replace("Employee", EmployeeSearchTextEAR2A));
		    	
		    	selectText(txt_ExpenseTypeTextFieldA, "Recipt Only");
		    	sendKeys(txt_PurposeTextFieldA, "Advance Request");
		    	sendKeys(txt_AdditionalRemarkTextFieldA, "Advance Request");
		    	selectText(txt_CurrencyTextFieldA, "USD");
		    	sendKeys(txt_AmountTextFieldA, "130000");
		    	
		    	
		    	String EmployeeTextFieldA= driver.findElement(By.xpath(txt_EmployeeTextFieldA)).getAttribute("value");
		    	String ExpenseTypeTextFieldA = new Select(driver.findElement(By.xpath(txt_ExpenseTypeTextFieldA))).getFirstSelectedOption().getText();
		    	String PurposeTextFieldA= driver.findElement(By.xpath(txt_PurposeTextFieldA)).getAttribute("value");
		    	String AdditionalRemarkTextFieldA= driver.findElement(By.xpath(txt_AdditionalRemarkTextFieldA)).getAttribute("value");
		    	String CurrencyTextFieldA = new Select(driver.findElement(By.xpath(txt_CurrencyTextFieldA))).getFirstSelectedOption().getText();
		    	String AmountTextFieldA= driver.findElement(By.xpath(txt_AmountTextFieldA)).getAttribute("value");
		    	
		    	Thread.sleep(3000);
				if (EmployeeTextFieldA.contentEquals("W H M Dilshan") && ExpenseTypeTextFieldA.contentEquals("Recipt Only")
						&& PurposeTextFieldA.contentEquals("Advance Request") && AdditionalRemarkTextFieldA.contentEquals("Advance Request")
						 && CurrencyTextFieldA.contentEquals("USD") && AmountTextFieldA.contentEquals("130000")) {
					writeTestResults("Verify user can edit the drafted employee advance request form", "user can edit the drafted employee advance request form",
							"user can edit the drafted employee advance request form Successfully", "pass");
				} else {
					writeTestResults("Verify user can edit the drafted employee advance request form", "user can edit the drafted employee advance request form",
							"user can edit the drafted employee advance request form Fail", "fail");
				}
		    	
		    	WaitClick(btn_UpdateA);
		    	click(btn_UpdateA);
		    	
		    	Thread.sleep(3000);
				if (getText(txt_EmployeeTextFieldAfterValueA).contentEquals("130000 [W H M Dilshan]") && getText(txt_ExpenseTypeTextFieldAfterValueA).contentEquals("Recipt Only")
						&& getText(txt_PurposeTextFieldAfterValueA).contentEquals("Advance Request") && getText(txt_AdditionalRemarkTextFieldAfterValueA).contentEquals("Advance Request")
						 && getText(txt_CurrencyTextFieldAfterValueA).contentEquals("USD") && getText(txt_AmountTextFieldAfterValueA).contentEquals("130,000.000000")) {
					writeTestResults("Verify user can Update the Drafted details", "user can Update the Drafted details",
							"user can Update the Drafted details Successfully", "pass");
				} else {
					writeTestResults("Verify user can Update the Drafted details", "user can Update the Drafted details",
							"user can Update the Drafted details Fail", "fail");
				}
		    }
		    
		    // Service_EAR_011
		    public void VerifyThatEmployeeAdvanceRequestCanBeRelease() throws Exception {
		    	
		    	VerifyThatNewEmployeeAdvanceRequestButtonIsNavigatedToEmployeeAdvanceRequestNewForm();
		    	
		    	WaitClick(btn_EmployeeBtnEARA);
		    	click(btn_EmployeeBtnEARA);
		    	sendKeys(txt_EmployeeSearchTextEARA, EmployeeSearchTextEAR1A);
		    	Thread.sleep(3000);
		    	pressEnter(txt_EmployeeSearchTextEARA);
		    	WaitElement(sel_EmployeeSearchSelEARA.replace("Employee", EmployeeSearchTextEAR1A));
		    	doubleClick(sel_EmployeeSearchSelEARA.replace("Employee", EmployeeSearchTextEAR1A));
		    	
		    	selectText(txt_ExpenseTypeTextFieldA, "Business Trip");
		    	sendKeys(txt_PurposeTextFieldA, "Advance");
		    	sendKeys(txt_AdditionalRemarkTextFieldA, "Advance");
		    	selectText(txt_CurrencyTextFieldA, "LKR");
		    	sendKeys(txt_AmountTextFieldA, "120000");
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that able to draft the Employee Advance Request document", "User Can draft the Employee Advance Request document",
								"User Can draft the Employee Advance Request document Successfully", "pass");
				} else {
						writeTestResults("Verify that able to draft the Employee Advance Request document", "User Can draft the Employee Advance Request document",
								"User Can draft the Employee Advance Request document Fail", "fail");
				}
				
				WaitClick(btn_ReleaseA);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				WaitElement(txt_PageRelease2A);
				if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that employee advance request can be \"Release\"", "User Can Release the Employee Advance Request document",
							"User Can Release the Employee Advance Request document Successfully", "pass");
				} else {
						writeTestResults("Verify that employee advance request can be \"Release\"", "User Can Release the Employee Advance Request document",
								"User Can Release the Employee Advance Request document Fail", "fail");
				}
		    }
		    
		    // Service_EAR_012
		    public void VerifyThatDraftedDocumentCanBeDelete() throws Exception {
		    	
		    	VerifyThatAbleToDraftTheDocument();
				WaitClick(btn_ActionBtnA);
				click(btn_ActionBtnA);
				WaitClick(btn_DeleteBtnA);
				click(btn_DeleteBtnA);
				WaitClick(btn_YesBtnA);
				click(btn_YesBtnA);
				   
				WaitElement(txt_pageDeletedNewA);
				if (isDisplayed(txt_pageDeletedNewA)) {
						writeTestResults("Verify that drafted document can be delete", "drafted document can be delete",
								"drafted document can be delete Successfully", "pass");
				} else {
						writeTestResults("Verify that drafted document can be delete", "drafted document can be delete",
								"drafted document can be delete Fail", "fail");
				}
		    }
		    
		    // Service_EAR_013
		    public void VerifyThatAbleToReverseTheReleasedEmployeeAdvanceRequest() throws Exception {
		    	
		    	VerifyThatEmployeeAdvanceRequestCanBeRelease();
				WaitClick(btn_ActionBtnA);
			    click(btn_ActionBtnA);
			    WaitClick(btn_ReverseBtnA);
			    click(btn_ReverseBtnA);
				Thread.sleep(2000);
				JavascriptExecutor j7 = (JavascriptExecutor)driver;
				j7.executeScript("$(\"#txtrdcmnReverseDate\").datepicker(\"setDate\", new Date())");
				WaitElement(btn_reverceApplyA);
			    click(btn_reverceApplyA);
				WaitClick(btn_YesBtnA);
			    click(btn_YesBtnA);
			       
				WaitElement(txt_pageReversedNewA);
				if (isDisplayed(txt_pageReversedNewA)) {
						writeTestResults("Verify that able to reverse the released employee advance request", "User Can reverse the Employee Advance Request document",
								"User Can reverse the Employee Advance Request document Successfully", "pass");
				} else {
						writeTestResults("Verify that able to reverse the released employee advance request", "User Can reverse the Employee Advance Request document",
								"User Can reverse the Employee Advance Request document Fail", "fail");
				}
		    }
		    
		    // Service_EAR_014_015_016_017
		    public void VerifyThatFollowingActionIsAvilableUnderTheReleasedDocumentPayEmployeeAdvance() throws Exception {
		    	
		    	VerifyThatNewEmployeeAdvanceRequestButtonIsNavigatedToEmployeeAdvanceRequestNewForm();
		    	
		    	WaitClick(btn_EmployeeBtnEARA);
		    	click(btn_EmployeeBtnEARA);
		    	sendKeys(txt_EmployeeSearchTextEARA, EmployeeSearchTextEAR1A);
		    	Thread.sleep(3000);
		    	pressEnter(txt_EmployeeSearchTextEARA);
		    	WaitElement(sel_EmployeeSearchSelEARA.replace("Employee", EmployeeSearchTextEAR1A));
		    	doubleClick(sel_EmployeeSearchSelEARA.replace("Employee", EmployeeSearchTextEAR1A));
		    	
		    	selectText(txt_ExpenseTypeTextFieldA, "Business Trip");
		    	sendKeys(txt_PurposeTextFieldA, "Advance");
		    	sendKeys(txt_AdditionalRemarkTextFieldA, "Advance");
		    	selectText(txt_CurrencyTextFieldA, "LKR");
		    	sendKeys(txt_AmountTextFieldA, "120000");
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that able to draft the Employee Advance Request document", "User Can draft the Employee Advance Request document",
								"User Can draft the Employee Advance Request document Successfully", "pass");
				} else {
						writeTestResults("Verify that able to draft the Employee Advance Request document", "User Can draft the Employee Advance Request document",
								"User Can draft the Employee Advance Request document Fail", "fail");
				}
				
				WaitClick(btn_ReleaseA);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String EARNo = trackCode;
				
				WaitElement(txt_PageRelease2A);
				if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that employee advance request can be \"Release\"", "User Can Release the Employee Advance Request document",
							"User Can Release the Employee Advance Request document Successfully", "pass");
				} else {
						writeTestResults("Verify that employee advance request can be \"Release\"", "User Can Release the Employee Advance Request document",
								"User Can Release the Employee Advance Request document Fail", "fail");
				}
				
				WaitClick(btn_ActionBtnA);
			    click(btn_ActionBtnA);
			    
				WaitElement(btn_PayEmployeeAdvanceA);
				if (isDisplayed(btn_PayEmployeeAdvanceA)) {
						writeTestResults("Verify that Following Action is avilable under the released document * Pay Employee Advance", "Following Action is avilable under the released document * Pay Employee Advance",
								"Following Action is avilable under the released document * Pay Employee Advance Successfully", "pass");
						click(btn_PayEmployeeAdvanceA);
				} else {
						writeTestResults("Verify that Following Action is avilable under the released document * Pay Employee Advance", "Following Action is avilable under the released document * Pay Employee Advance",
								"Following Action is avilable under the released document * Pay Employee Advance Fail", "fail");
				}
				
				Thread.sleep(3000);
				if (getText(txt_NewOutboundPaymentAdvicePageHeaderA).contentEquals("Outbound Payment Advice - New")){
				        writeTestResults("Verify that able to navigated to the \"Outbound Payment Advice\" form", 
				        		"navigated to the \"Outbound Payment Advice\" form",
							"navigated to the \"Outbound Payment Advice\" form Successfully", "pass");
		        } else {
		        	    writeTestResults("Verify that able to navigated to the \"Outbound Payment Advice\" form", 
		        	    		"navigated to the \"Outbound Payment Advice\" form",
								"navigated to the \"Outbound Payment Advice\" form Fail", "fail");
		        }
				String CustomerAccount= driver.findElement(By.xpath(txt_CustomerAccountOPAA)).getAttribute("value");
				String ReferenceType= driver.findElement(By.xpath(txt_ReferenceTypeOPAA)).getAttribute("value");
				String RefDocNo= driver.findElement(By.xpath(txt_RefDocNoOPAA)).getAttribute("value");
				String Description= driver.findElement(By.xpath(txt_DescriptionOPAA)).getAttribute("value");
				String Currency= new Select(driver.findElement(By.xpath(txt_cboxCurrencyOPAA))).getFirstSelectedOption().getText();
				String Amount= driver.findElement(By.xpath(txt_AmountOPAA)).getAttribute("value");
				
		    	Thread.sleep(3000);
				if (CustomerAccount.contentEquals("120000") && ReferenceType.contentEquals("Employee Advance Request")
						&& RefDocNo.contentEquals(EARNo) && Description.contentEquals("Advance")
						 && Currency.contentEquals("LKR") && Amount.contentEquals("120,000.000000")) {
					writeTestResults("Verify that following fields in the outbound payment advice is filled with the details from employee advance request", 
							"following fields in the outbound payment advice is filled with the details from employee advance request",
							"following fields in the outbound payment advice is filled with the details from employee advance request Successfully", "pass");
				} else {
					writeTestResults("Verify that following fields in the outbound payment advice is filled with the details from employee advance request", 
							"following fields in the outbound payment advice is filled with the details from employee advance request",
							"following fields in the outbound payment advice is filled with the details from employee advance request Fail", "fail");
				}
				
		    	WaitClick(btn_CheckoutA);
		    	click(btn_CheckoutA);
				
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that able to draft the outbound payment advice", "User Can draft the outbound payment advice document",
								"User Can draft the outbound payment advice document Successfully", "pass");
				} else {
						writeTestResults("Verify that able to draft the outbound payment advice", "User Can draft the outbound payment advice document",
								"User Can draft the outbound payment advice document Fail", "fail");
				}
				
				WaitClick(btn_ReleaseA);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				
				WaitElement(txt_PageRelease2A);
				if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that able to release the outbound payment advice", "User Can Release the outbound payment advice document",
							"User Can Release the outbound payment advice document Successfully", "pass");
				} else {
						writeTestResults("Verify that able to release the outbound payment advice", "User Can Release the outbound payment advice document",
								"User Can Release the outbound payment advice document Fail", "fail");
				}
		    }
		    
		    // Service_EAR_018_019_020
		    public void VerifyThatReferenceDetailsTabInTheReleasedEmployeeAdvanceRequestFormIsEmpty() throws Exception {
		    	
		    	VerifyThatEmployeeAdvanceRequestCanBeRelease();
		    	WaitClick(btn_ReferenceDetailsTabA);
		    	click(btn_ReferenceDetailsTabA);
		    	
				if (getText(txt_OutboundPaymentAdviceRecord1A).isEmpty()) {
					writeTestResults("Verify that \"Reference Details\" tab in the released employee advance request form is empty ", 
							"\"Reference Details\" tab in the released employee advance request form is empty ",
						"\"Reference Details\" tab in the released employee advance request form is empty  Successfully", "pass");
				} else {
					writeTestResults("Verify that \"Reference Details\" tab in the released employee advance request form is empty ", 
							"\"Reference Details\" tab in the released employee advance request form is empty ",
							"\"Reference Details\" tab in the released employee advance request form is empty  Fail", "fail");
				}
				
				WaitClick(btn_ActionBtnA);
			    click(btn_ActionBtnA);
			    
				WaitElement(btn_PayEmployeeAdvanceA);
				if (isDisplayed(btn_PayEmployeeAdvanceA)) {
						writeTestResults("Verify that Following Action is avilable under the released document * Pay Employee Advance", "Following Action is avilable under the released document * Pay Employee Advance",
								"Following Action is avilable under the released document * Pay Employee Advance Successfully", "pass");
						click(btn_PayEmployeeAdvanceA);
				} else {
						writeTestResults("Verify that Following Action is avilable under the released document * Pay Employee Advance", "Following Action is avilable under the released document * Pay Employee Advance",
								"Following Action is avilable under the released document * Pay Employee Advance Fail", "fail");
				}
				
				Thread.sleep(3000);
				if (getText(txt_NewOutboundPaymentAdvicePageHeaderA).contentEquals("Outbound Payment Advice - New")){
				        writeTestResults("Verify that able to navigated to the \"Outbound Payment Advice\" form", 
				        		"navigated to the \"Outbound Payment Advice\" form",
							"navigated to the \"Outbound Payment Advice\" form Successfully", "pass");
		        } else {
		        	    writeTestResults("Verify that able to navigated to the \"Outbound Payment Advice\" form", 
		        	    		"navigated to the \"Outbound Payment Advice\" form",
								"navigated to the \"Outbound Payment Advice\" form Fail", "fail");
		        }

		    	WaitClick(btn_CheckoutA);
		    	click(btn_CheckoutA);
				
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	WaitElement(txt_PageDraft2A);
		    	if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that able to draft the outbound payment advice", "User Can draft the outbound payment advice document",
								"User Can draft the outbound payment advice document Successfully", "pass");
		    	} else {
						writeTestResults("Verify that able to draft the outbound payment advice", "User Can draft the outbound payment advice document",
								"User Can draft the outbound payment advice document Fail", "fail");
		    	}
				
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
		    	Thread.sleep(3000);
		    	trackCode = getText(txt_trackCodeA);
		    	String OPANo = trackCode.replace(" Employee Advance Advice", "");
				
		    	WaitElement(txt_PageRelease2A);
		    	if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that able to release the outbound payment advice", "User Can Release the outbound payment advice document",
							"User Can Release the outbound payment advice document Successfully", "pass");
		    	} else {
						writeTestResults("Verify that able to release the outbound payment advice", "User Can Release the outbound payment advice document",
								"User Can Release the outbound payment advice document Fail", "fail");
		    	}
		    	
		    	driver.navigate().back();
		    	driver.navigate().back();
		    	
		    	Thread.sleep(3000);
		    	
		    	WaitClick(btn_ReferenceDetailsTabA);
		    	click(btn_ReferenceDetailsTabA);
		    	Thread.sleep(3000);
		    	String OutboundPaymentAdviceRecord1A= getText(txt_OutboundPaymentAdviceRecord1A);
		    	
				if (OutboundPaymentAdviceRecord1A.contentEquals(OPANo)) {
					writeTestResults("Verify that \"Reference Details \" tab is filled with a record when user release the outbound payment advice", 
							"\"Reference Details \" tab is filled with a record when user release the outbound payment advice",
						"\"Reference Details \" tab is filled with a record when user release the outbound payment advice Successfully", "pass");
				} else {
					writeTestResults("Verify that \"Reference Details \" tab is filled with a record when user release the outbound payment advice", 
							"\"Reference Details \" tab is filled with a record when user release the outbound payment advice",
							"\"Reference Details \" tab is filled with a record when user release the outbound payment advice Fail", "fail");
				}
				
				driver.navigate().forward();
				driver.navigate().forward();
				
				Thread.sleep(3000);
				
				WaitClick(btn_ActionBtnA);
			    click(btn_ActionBtnA);
			    WaitClick(btn_ReverseBtnA);
			    click(btn_ReverseBtnA);
			    click(btn_reverceApplyA);
				WaitClick(btn_YesBtnA);
			    click(btn_YesBtnA);
			       
				WaitElement(txt_pageReversedNewA);
				if (isDisplayed(txt_pageReversedNewA)) {
						writeTestResults("Verify that able to reverse the released outbound payment advice", "User Can reverse the outbound payment advice document",
								"User Can reverse the outbound payment advice document Successfully", "pass");
				} else {
						writeTestResults("Verify that able to reverse the released outbound payment advice", "User Can reverse the outbound payment advice document",
								"User Can reverse the outbound payment advice document Fail", "fail");
				}
				
		    	driver.navigate().back();
		    	driver.navigate().back();
		    	
		    	Thread.sleep(3000);
		    	
		    	WaitClick(btn_ReferenceDetailsTabA);
		    	click(btn_ReferenceDetailsTabA);

				if (getText(txt_OutboundPaymentAdviceRecord1A).isEmpty()) {
					writeTestResults("Verify that reference details tab is cleared when reverse the outbound payment advice", 
							"reference details tab is cleared when reverse the outbound payment advice",
						"reference details tab is cleared when reverse the outbound payment advice Successfully", "pass");
				} else {
					writeTestResults("Verify that reference details tab is cleared when reverse the outbound payment advice", 
							"reference details tab is cleared when reverse the outbound payment advice",
							"reference details tab is cleared when reverse the outbound payment advice Fail", "fail");
				}
		    }
		    
		    // Service_EAR_021_022_023_024_025
		    public void VerifyThatUserCanDoTheOutboundPaymentForEmployeeAdvanceRequestForm() throws Exception {
		    	
		    	VerifyThatNewEmployeeAdvanceRequestButtonIsNavigatedToEmployeeAdvanceRequestNewForm();
		    	
		    	WaitClick(btn_EmployeeBtnEARA);
		    	click(btn_EmployeeBtnEARA);
		    	sendKeys(txt_EmployeeSearchTextEARA, EmployeeSearchTextEAR1A);
		    	Thread.sleep(3000);
		    	pressEnter(txt_EmployeeSearchTextEARA);
		    	WaitElement(sel_EmployeeSearchSelEARA.replace("Employee", EmployeeSearchTextEAR1A));
		    	doubleClick(sel_EmployeeSearchSelEARA.replace("Employee", EmployeeSearchTextEAR1A));
		    	
		    	selectText(txt_ExpenseTypeTextFieldA, "Business Trip");
		    	sendKeys(txt_PurposeTextFieldA, "Advance");
		    	sendKeys(txt_AdditionalRemarkTextFieldA, "Advance");
		    	selectText(txt_CurrencyTextFieldA, "LKR");
		    	sendKeys(txt_AmountTextFieldA, "120000");
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that able to draft the Employee Advance Request document", "User Can draft the Employee Advance Request document",
								"User Can draft the Employee Advance Request document Successfully", "pass");
				} else {
						writeTestResults("Verify that able to draft the Employee Advance Request document", "User Can draft the Employee Advance Request document",
								"User Can draft the Employee Advance Request document Fail", "fail");
				}
				
				WaitClick(btn_ReleaseA);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String EARNo = trackCode;
				
				WaitElement(txt_PageRelease2A);
				if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that employee advance request can be \"Release\"", "User Can Release the Employee Advance Request document",
							"User Can Release the Employee Advance Request document Successfully", "pass");
				} else {
						writeTestResults("Verify that employee advance request can be \"Release\"", "User Can Release the Employee Advance Request document",
								"User Can Release the Employee Advance Request document Fail", "fail");
				}
				
				WaitClick(btn_ActionBtnA);
			    click(btn_ActionBtnA);
			    
				WaitElement(btn_PayEmployeeAdvanceA);
				if (isDisplayed(btn_PayEmployeeAdvanceA)) {
						writeTestResults("Verify that Following Action is avilable under the released document * Pay Employee Advance", "Following Action is avilable under the released document * Pay Employee Advance",
								"Following Action is avilable under the released document * Pay Employee Advance Successfully", "pass");
						click(btn_PayEmployeeAdvanceA);
				} else {
						writeTestResults("Verify that Following Action is avilable under the released document * Pay Employee Advance", "Following Action is avilable under the released document * Pay Employee Advance",
								"Following Action is avilable under the released document * Pay Employee Advance Fail", "fail");
				}
				
				Thread.sleep(3000);
				if (getText(txt_NewOutboundPaymentAdvicePageHeaderA).contentEquals("Outbound Payment Advice - New")){
				        writeTestResults("Verify that able to navigated to the \"Outbound Payment Advice\" form", 
				        		"navigated to the \"Outbound Payment Advice\" form",
							"navigated to the \"Outbound Payment Advice\" form Successfully", "pass");
		        } else {
		        	    writeTestResults("Verify that able to navigated to the \"Outbound Payment Advice\" form", 
		        	    		"navigated to the \"Outbound Payment Advice\" form",
								"navigated to the \"Outbound Payment Advice\" form Fail", "fail");
		        }
				
		    	WaitClick(btn_CheckoutA);
		    	click(btn_CheckoutA);
				
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that able to draft the outbound payment advice", "User Can draft the outbound payment advice document",
								"User Can draft the outbound payment advice document Successfully", "pass");
				} else {
						writeTestResults("Verify that able to draft the outbound payment advice", "User Can draft the outbound payment advice document",
								"User Can draft the outbound payment advice document Fail", "fail");
				}
				
				WaitClick(btn_ReleaseA);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				
				WaitElement(txt_PageRelease2A);
				if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that able to release the outbound payment advice", "User Can Release the outbound payment advice document",
							"User Can Release the outbound payment advice document Successfully", "pass");
				} else {
						writeTestResults("Verify that able to release the outbound payment advice", "User Can Release the outbound payment advice document",
								"User Can Release the outbound payment advice document Fail", "fail");
				}
				
				WaitClick(btn_ActionBtnA);
			    click(btn_ActionBtnA);
				WaitClick(btn_ConvertToOutboundPaymentBtnA);
			    click(btn_ConvertToOutboundPaymentBtnA);
			    //System.out.println(getText(txt_NewOutboundPaymentPageHeaderA));
			    
				Thread.sleep(3000);
				if (getText(txt_NewOutboundPaymentPageHeaderA).contentEquals("Outbound Payment - New Employee Payment")){
				        writeTestResults("Verify Outbound payment form is open in a new tab", 
				        		"Outbound payment form is open in a new tab",
							"Outbound payment form is open in a new tab Successfully", "pass");
		        } else {
		        	    writeTestResults("Verify Outbound payment form is open in a new tab", 
		        	    		"Outbound payment form is open in a new tab",
								"Outbound payment form is open in a new tab Fail", "fail");
		        } 
	
				String CustomerAccount= driver.findElement(By.xpath(txt_CustomerAccountOPAA)).getAttribute("value");
				String Currency= new Select(driver.findElement(By.xpath(txt_cboxCurrencyOPAA))).getFirstSelectedOption().getText();
				
				WaitClick(btn_DetailsTabA);
				click(btn_DetailsTabA);
				String Amount= getText(txt_AmountOPA).replace(",", "");
				
				if (CustomerAccount.contentEquals("120000 [W H M Aruna]") && Currency.contentEquals("LKR") 
						&& Amount.contentEquals("120000.000000")) {
					writeTestResults("Verify details in the Employee advance request and Outbound payment advice are displayed in the Outbound payment also", 
							"details in the Employee advance request and Outbound payment advice are displayed in the Outbound payment also",
						"details in the Employee advance request and Outbound payment advice are displayed in the Outbound payment also Successfully", "pass");
				} else {
					writeTestResults("Verify details in the Employee advance request and Outbound payment advice are displayed in the Outbound payment also", 
							"details in the Employee advance request and Outbound payment advice are displayed in the Outbound payment also",
							"details in the Employee advance request and Outbound payment advice are displayed in the Outbound payment also Fail", "fail");
				}
				
				sendKeys(txt_PaidAmountA, "120000");
				WaitClick(btn_CheckoutA);
				click(btn_CheckoutA);
				
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that able to draft the outbound payment", "User Can draft the outbound payment document",
								"User Can draft the outbound payment document Successfully", "pass");
				} else {
						writeTestResults("Verify that able to draft the outbound payment", "User Can draft the outbound payment document",
								"User Can draft the outbound payment document Fail", "fail");
				}
				
				WaitClick(btn_ReleaseA);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String OPNo = trackCode.replace(" Employee Payment", "");
				
				WaitElement(txt_PageRelease2A);
				if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that able to release the outbound payment", "User Can Release the outbound payment document",
							"User Can Release the outbound payment document Successfully", "pass");
				} else {
						writeTestResults("Verify that able to release the outbound payment", "User Can Release the outbound payment document",
								"User Can Release the outbound payment document Fail", "fail");
				}
				
		    	driver.navigate().back();
		    	driver.navigate().back();
		    	driver.navigate().back();
		    	driver.navigate().back();
		    	
		    	Thread.sleep(3000);
		    	
		    	WaitClick(btn_ReferenceDetailsTabA);
		    	click(btn_ReferenceDetailsTabA);
		    	Thread.sleep(3000);
		    	String OutboundPaymentAdviceRecord2A= getText(txt_OutboundPaymentAdviceRecord2A);
		    	
				if (OutboundPaymentAdviceRecord2A.contentEquals(OPNo)) {
					writeTestResults("Verify Record for Outbound payment also displayed in the Employee advance request \"Reference Details\" tab", 
							"Record for Outbound payment also displayed in the Employee advance request \"Reference Details\" tab",
						"Record for Outbound payment also displayed in the Employee advance request \"Reference Details\" tab Successfully", "pass");
				} else {
					writeTestResults("Verify Record for Outbound payment also displayed in the Employee advance request \"Reference Details\" tab", 
							"Record for Outbound payment also displayed in the Employee advance request \"Reference Details\" tab",
							"Record for Outbound payment also displayed in the Employee advance request \"Reference Details\" tab Fail", "fail");
				}
				
		    	WaitClick(btn_SummaryTabA);
		    	click(btn_SummaryTabA);
		    	
		    	String BalanceAmountA= getText(txt_BalanceAmountA).replace(",", "");
		    	
				if (BalanceAmountA.contentEquals(Amount)) {
					writeTestResults("Verify that \"Balance Amount\" field in the employee advance request is changed after releasing the full payment through the Outbound payment", 
							"\"Balance Amount\" field in the employee advance request is changed after releasing the full payment through the Outbound payment",
						"\"Balance Amount\" field in the employee advance request is changed after releasing the full payment through the Outbound payment Successfully", "pass");
				} else {
					writeTestResults("Verify that \"Balance Amount\" field in the employee advance request is changed after releasing the full payment through the Outbound payment", 
							"\"Balance Amount\" field in the employee advance request is changed after releasing the full payment through the Outbound payment",
							"\"Balance Amount\" field in the employee advance request is changed after releasing the full payment through the Outbound payment Fail", "fail");
				}
				
				String DueAmountA= getText(txt_DueAmountA).replace(",", "");
				
				if (DueAmountA.contentEquals("0.000000")) {
					writeTestResults("Verify that \"Due Amount\" field in the emloyee advance request form is \"0\" when user paid the full amount via outbound payment", 
							"\"Due Amount\" field in the emloyee advance request form is \"0\" when user paid the full amount via outbound payment",
						"\"Due Amount\" field in the emloyee advance request form is \"0\" when user paid the full amount via outbound payment Successfully", "pass");
				} else {
					writeTestResults("Verify that \"Due Amount\" field in the emloyee advance request form is \"0\" when user paid the full amount via outbound payment", 
							"\"Due Amount\" field in the emloyee advance request form is \"0\" when user paid the full amount via outbound payment",
							"\"Due Amount\" field in the emloyee advance request form is \"0\" when user paid the full amount via outbound payment Fail", "fail");
				}
				
				String PaidAmountEARA= getText(txt_PaidAmountEARA).replace(",", "");
				
				if (PaidAmountEARA.contentEquals(Amount)) {
					writeTestResults("Verify that \"Paid Amount\" field in the Employee advance Request is filled with the amount paid via the outbound payment (When paid the full Amount)", 
							"\"Paid Amount\" field in the Employee advance Request is filled with the amount paid via the outbound payment (When paid the full Amount)",
						"\"Paid Amount\" field in the Employee advance Request is filled with the amount paid via the outbound payment (When paid the full Amount) Successfully", "pass");
				} else {
					writeTestResults("Verify that \"Paid Amount\" field in the Employee advance Request is filled with the amount paid via the outbound payment (When paid the full Amount)", 
							"\"Paid Amount\" field in the Employee advance Request is filled with the amount paid via the outbound payment (When paid the full Amount)",
							"\"Paid Amount\" field in the Employee advance Request is filled with the amount paid via the outbound payment (When paid the full Amount) Fail", "fail");
				}
		    }
		    
		    // Service_EAR_026
		    public void VerifyThatDueAmountFieldInTheEmployeeAdvanceRequestDisplayedTheAdavanceAmountAsSameBeforeDoThePayment() throws Exception {
		    	
		    	VerifyThatEmployeeAdvanceRequestCanBeRelease();
		    	
				String DueAmountA= getText(txt_DueAmountA).replace(",", "");
				
				if (DueAmountA.contentEquals("120000.000000")) {
					writeTestResults("Verify that Due Amount field in the Employee Advance Request displayed the Adavance amount as same before do the payment", 
							"Due Amount field in the Employee Advance Request displayed the Adavance amount as same before do the payment",
						"Due Amount field in the Employee Advance Request displayed the Adavance amount as same before do the payment Successfully", "pass");
				} else {
					writeTestResults("Verify that Due Amount field in the Employee Advance Request displayed the Adavance amount as same before do the payment", 
							"Due Amount field in the Employee Advance Request displayed the Adavance amount as same before do the payment",
							"Due Amount field in the Employee Advance Request displayed the Adavance amount as same before do the payment Fail", "fail");
				}
		    }
		    
		    // Service_EAR_027
		    public void VerifyThatUserShouldNotBeAbleToDoThePartialPaymentsForTheEmployeeAdvanceRequest() throws Exception {
		    	
		    	VerifyThatNewEmployeeAdvanceRequestButtonIsNavigatedToEmployeeAdvanceRequestNewForm();
		    	
		    	WaitClick(btn_EmployeeBtnEARA);
		    	click(btn_EmployeeBtnEARA);
		    	sendKeys(txt_EmployeeSearchTextEARA, EmployeeSearchTextEAR1A);
		    	Thread.sleep(3000);
		    	pressEnter(txt_EmployeeSearchTextEARA);
		    	WaitElement(sel_EmployeeSearchSelEARA.replace("Employee", EmployeeSearchTextEAR1A));
		    	doubleClick(sel_EmployeeSearchSelEARA.replace("Employee", EmployeeSearchTextEAR1A));
		    	
		    	selectText(txt_ExpenseTypeTextFieldA, "Business Trip");
		    	sendKeys(txt_PurposeTextFieldA, "Advance");
		    	sendKeys(txt_AdditionalRemarkTextFieldA, "Advance");
		    	selectText(txt_CurrencyTextFieldA, "LKR");
		    	sendKeys(txt_AmountTextFieldA, "120000");
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that able to draft the Employee Advance Request document", "User Can draft the Employee Advance Request document",
								"User Can draft the Employee Advance Request document Successfully", "pass");
				} else {
						writeTestResults("Verify that able to draft the Employee Advance Request document", "User Can draft the Employee Advance Request document",
								"User Can draft the Employee Advance Request document Fail", "fail");
				}
				
				WaitClick(btn_ReleaseA);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String EARNo = trackCode;
				
				WaitElement(txt_PageRelease2A);
				if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that employee advance request can be \"Release\"", "User Can Release the Employee Advance Request document",
							"User Can Release the Employee Advance Request document Successfully", "pass");
				} else {
						writeTestResults("Verify that employee advance request can be \"Release\"", "User Can Release the Employee Advance Request document",
								"User Can Release the Employee Advance Request document Fail", "fail");
				}
				
				WaitClick(btn_ActionBtnA);
			    click(btn_ActionBtnA);
			    
				WaitElement(btn_PayEmployeeAdvanceA);
				if (isDisplayed(btn_PayEmployeeAdvanceA)) {
						writeTestResults("Verify that Following Action is avilable under the released document * Pay Employee Advance", "Following Action is avilable under the released document * Pay Employee Advance",
								"Following Action is avilable under the released document * Pay Employee Advance Successfully", "pass");
						click(btn_PayEmployeeAdvanceA);
				} else {
						writeTestResults("Verify that Following Action is avilable under the released document * Pay Employee Advance", "Following Action is avilable under the released document * Pay Employee Advance",
								"Following Action is avilable under the released document * Pay Employee Advance Fail", "fail");
				}
				
				Thread.sleep(3000);
				if (getText(txt_NewOutboundPaymentAdvicePageHeaderA).contentEquals("Outbound Payment Advice - New")){
				        writeTestResults("Verify that able to navigated to the \"Outbound Payment Advice\" form", 
				        		"navigated to the \"Outbound Payment Advice\" form",
							"navigated to the \"Outbound Payment Advice\" form Successfully", "pass");
		        } else {
		        	    writeTestResults("Verify that able to navigated to the \"Outbound Payment Advice\" form", 
		        	    		"navigated to the \"Outbound Payment Advice\" form",
								"navigated to the \"Outbound Payment Advice\" form Fail", "fail");
		        }
				
		    	WaitClick(btn_CheckoutA);
		    	click(btn_CheckoutA);
				
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that able to draft the outbound payment advice", "User Can draft the outbound payment advice document",
								"User Can draft the outbound payment advice document Successfully", "pass");
				} else {
						writeTestResults("Verify that able to draft the outbound payment advice", "User Can draft the outbound payment advice document",
								"User Can draft the outbound payment advice document Fail", "fail");
				}
				
				WaitClick(btn_ReleaseA);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				
				WaitElement(txt_PageRelease2A);
				if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that able to release the outbound payment advice", "User Can Release the outbound payment advice document",
							"User Can Release the outbound payment advice document Successfully", "pass");
				} else {
						writeTestResults("Verify that able to release the outbound payment advice", "User Can Release the outbound payment advice document",
								"User Can Release the outbound payment advice document Fail", "fail");
				}
				
				WaitClick(btn_ActionBtnA);
			    click(btn_ActionBtnA);
				WaitClick(btn_ConvertToOutboundPaymentBtnA);
			    click(btn_ConvertToOutboundPaymentBtnA);
			    //System.out.println(getText(txt_NewOutboundPaymentPageHeaderA));
			    
				Thread.sleep(3000);
				if (getText(txt_NewOutboundPaymentPageHeaderA).contentEquals("Outbound Payment - New Employee Payment")){
				        writeTestResults("Verify Outbound payment form is open in a new tab", 
				        		"Outbound payment form is open in a new tab",
							"Outbound payment form is open in a new tab Successfully", "pass");
		        } else {
		        	    writeTestResults("Verify Outbound payment form is open in a new tab", 
		        	    		"Outbound payment form is open in a new tab",
								"Outbound payment form is open in a new tab Fail", "fail");
		        } 
				
				WaitClick(btn_DetailsTabA);
				click(btn_DetailsTabA);
				
				WaitClick(btn_OutboundPaymentCheckBoxA);
				click(btn_OutboundPaymentCheckBoxA);
				
				sendKeys(txt_PaidAmountA, "100000");
				WaitClick(btn_CheckoutA);
				click(btn_CheckoutA);
				
				WaitClick(btn_OutboundPaymentRowErrorA);
				click(btn_OutboundPaymentRowErrorA);
				
				WaitElement(txt_PartialPaymentErrorA);
				if (isDisplayed(txt_PartialPaymentErrorA)) {
						writeTestResults("Verify that user should not be able to do the partial payments for the Employee advance Request", "user should not be able to do the partial payments for the Employee advance Request",
								"user should not be able to do the partial payments for the Employee advance Request Successfully", "pass");
				} else {
						writeTestResults("Verify that user should not be able to do the partial payments for the Employee advance Request", "user should not be able to do the partial payments for the Employee advance Request",
								"user should not be able to do the partial payments for the Employee advance Request Fail", "fail");
				}
		    }
		    
		    // Service_EAR_028_029_030
		    public void VerifyThatUserAbleToDoThePaymentForMultipleAdvanceRequestsAtOneOutboundPayment() throws Exception {
		    	
		    	VerifyThatNewEmployeeAdvanceRequestButtonIsNavigatedToEmployeeAdvanceRequestNewForm();
		    	
		    	WaitClick(btn_EmployeeBtnEARA);
		    	click(btn_EmployeeBtnEARA);
		    	sendKeys(txt_EmployeeSearchTextEARA, EmployeeSearchTextEAR1A);
		    	Thread.sleep(3000);
		    	pressEnter(txt_EmployeeSearchTextEARA);
		    	WaitElement(sel_EmployeeSearchSelEARA.replace("Employee", EmployeeSearchTextEAR1A));
		    	doubleClick(sel_EmployeeSearchSelEARA.replace("Employee", EmployeeSearchTextEAR1A));
		    	
		    	selectText(txt_ExpenseTypeTextFieldA, "Business Trip");
		    	sendKeys(txt_PurposeTextFieldA, "Advance");
		    	sendKeys(txt_AdditionalRemarkTextFieldA, "Advance");
		    	selectText(txt_CurrencyTextFieldA, "LKR");
		    	sendKeys(txt_AmountTextFieldA, "120000");
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that able to draft the Employee Advance Request First document", "User Can draft the Employee Advance Request First document",
								"User Can draft the Employee Advance Request First document Successfully", "pass");
				} else {
						writeTestResults("Verify that able to draft the Employee Advance Request First document", "User Can draft the Employee Advance Request First document",
								"User Can draft the Employee Advance Request First document Fail", "fail");
				}
				
				WaitClick(btn_ReleaseA);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String EARNo1 = trackCode;
				
				WaitElement(txt_PageRelease2A);
				if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that employee advance request can be \"Release\"", "User Can Release the Employee Advance Request First document",
							"User Can Release the Employee Advance Request First document Successfully", "pass");
				} else {
						writeTestResults("Verify that employee advance request can be \"Release\"", "User Can Release the Employee Advance Request First document",
								"User Can Release the Employee Advance Request First document Fail", "fail");
				}
				
				WaitClick(btn_ActionBtnA);
			    click(btn_ActionBtnA);
			    
				WaitElement(btn_PayEmployeeAdvanceA);
				if (isDisplayed(btn_PayEmployeeAdvanceA)) {
						writeTestResults("Verify that Following Action is avilable under the released document * Pay Employee Advance", "Following Action is avilable under the released document * Pay Employee Advance",
								"Following Action is avilable under the released document * Pay Employee Advance Successfully", "pass");
						click(btn_PayEmployeeAdvanceA);
				} else {
						writeTestResults("Verify that Following Action is avilable under the released document * Pay Employee Advance", "Following Action is avilable under the released document * Pay Employee Advance",
								"Following Action is avilable under the released document * Pay Employee Advance Fail", "fail");
				}
				
				Thread.sleep(3000);
				if (getText(txt_NewOutboundPaymentAdvicePageHeaderA).contentEquals("Outbound Payment Advice - New")){
				        writeTestResults("Verify that able to navigated to the \"Outbound Payment Advice\" form", 
				        		"navigated to the \"Outbound Payment Advice\" form",
							"navigated to the \"Outbound Payment Advice\" form Successfully", "pass");
		        } else {
		        	    writeTestResults("Verify that able to navigated to the \"Outbound Payment Advice\" form", 
		        	    		"navigated to the \"Outbound Payment Advice\" form",
								"navigated to the \"Outbound Payment Advice\" form Fail", "fail");
		        }
				
		    	WaitClick(btn_CheckoutA);
		    	click(btn_CheckoutA);
				
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that able to draft the outbound payment advice", "User Can draft the outbound payment advice First document",
								"User Can draft the outbound payment advice First document Successfully", "pass");
				} else {
						writeTestResults("Verify that able to draft the outbound payment advice", "User Can draft the outbound payment advice First document",
								"User Can draft the outbound payment advice First document Fail", "fail");
				}
				
				WaitClick(btn_ReleaseA);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String OPANo1 = trackCode.replace(" Employee Advance Advice", "");
				
				WaitElement(txt_PageRelease2A);
				if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that able to release the outbound payment advice", "User Can Release the outbound payment advice First document",
							"User Can Release the outbound payment advice First document Successfully", "pass");
				} else {
						writeTestResults("Verify that able to release the outbound payment advice", "User Can Release the outbound payment advice First document",
								"User Can Release the outbound payment advice First document Fail", "fail");
				}
				
				clickNavigation();
				
		    	VerifyThatNewEmployeeAdvanceRequestButtonIsNavigatedToEmployeeAdvanceRequestNewForm();
		    	
		    	WaitClick(btn_EmployeeBtnEARA);
		    	click(btn_EmployeeBtnEARA);
		    	sendKeys(txt_EmployeeSearchTextEARA, EmployeeSearchTextEAR1A);
		    	Thread.sleep(3000);
		    	pressEnter(txt_EmployeeSearchTextEARA);
		    	WaitElement(sel_EmployeeSearchSelEARA.replace("Employee", EmployeeSearchTextEAR1A));
		    	doubleClick(sel_EmployeeSearchSelEARA.replace("Employee", EmployeeSearchTextEAR1A));
		    	
		    	selectText(txt_ExpenseTypeTextFieldA, "Business Trip");
		    	sendKeys(txt_PurposeTextFieldA, "Advance");
		    	sendKeys(txt_AdditionalRemarkTextFieldA, "Advance");
		    	selectText(txt_CurrencyTextFieldA, "LKR");
		    	sendKeys(txt_AmountTextFieldA, "120000");
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that able to draft the Employee Advance Request second document", "User Can draft the Employee Advance Request second document",
								"User Can draft the Employee Advance Request second document Successfully", "pass");
				} else {
						writeTestResults("Verify that able to draft the Employee Advance Request second document", "User Can draft the Employee Advance Request second document",
								"User Can draft the Employee Advance Request second document Fail", "fail");
				}
				
				WaitClick(btn_ReleaseA);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String EARNo2 = trackCode;
				
				WaitElement(txt_PageRelease2A);
				if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that employee advance request can be \"Release\"", "User Can Release the Employee Advance Request second document",
							"User Can Release the Employee Advance Request second document Successfully", "pass");
				} else {
						writeTestResults("Verify that employee advance request can be \"Release\"", "User Can Release the Employee Advance Request second document",
								"User Can Release the Employee Advance Request second document Fail", "fail");
				}
				
				WaitClick(btn_ActionBtnA);
			    click(btn_ActionBtnA);
			    
				WaitElement(btn_PayEmployeeAdvanceA);
				if (isDisplayed(btn_PayEmployeeAdvanceA)) {
						writeTestResults("Verify that Following Action is avilable under the released document * Pay Employee Advance", "Following Action is avilable under the released document * Pay Employee Advance",
								"Following Action is avilable under the released document * Pay Employee Advance Successfully", "pass");
						click(btn_PayEmployeeAdvanceA);
				} else {
						writeTestResults("Verify that Following Action is avilable under the released document * Pay Employee Advance", "Following Action is avilable under the released document * Pay Employee Advance",
								"Following Action is avilable under the released document * Pay Employee Advance Fail", "fail");
				}
				
				Thread.sleep(3000);
				if (getText(txt_NewOutboundPaymentAdvicePageHeaderA).contentEquals("Outbound Payment Advice - New")){
				        writeTestResults("Verify that able to navigated to the \"Outbound Payment Advice\" form", 
				        		"navigated to the \"Outbound Payment Advice\" form",
							"navigated to the \"Outbound Payment Advice\" form Successfully", "pass");
		        } else {
		        	    writeTestResults("Verify that able to navigated to the \"Outbound Payment Advice\" form", 
		        	    		"navigated to the \"Outbound Payment Advice\" form",
								"navigated to the \"Outbound Payment Advice\" form Fail", "fail");
		        }
				
		    	WaitClick(btn_CheckoutA);
		    	click(btn_CheckoutA);
				
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that able to draft the outbound payment advice", "User Can draft the outbound payment advice second document",
								"User Can draft the outbound payment advice second document Successfully", "pass");
				} else {
						writeTestResults("Verify that able to draft the outbound payment advice", "User Can draft the outbound payment advice second document",
								"User Can draft the outbound payment advice second document Fail", "fail");
				}
				
				WaitClick(btn_ReleaseA);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String OPANo2 = trackCode.replace(" Employee Advance Advice", "");
				
				WaitElement(txt_PageRelease2A);
				if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that able to release the outbound payment advice", "User Can Release the outbound payment advice second document",
							"User Can Release the outbound payment advice second document Successfully", "pass");
				} else {
						writeTestResults("Verify that able to release the outbound payment advice", "User Can Release the outbound payment advice second document",
								"User Can Release the outbound payment advice second document Fail", "fail");
				}
				
				WaitClick(btn_ActionBtnA);
			    click(btn_ActionBtnA);
				WaitClick(btn_ConvertToOutboundPaymentBtnA);
			    click(btn_ConvertToOutboundPaymentBtnA);
			    //System.out.println(getText(txt_NewOutboundPaymentPageHeaderA));
			    
				Thread.sleep(3000);
				if (getText(txt_NewOutboundPaymentPageHeaderA).contentEquals("Outbound Payment - New Employee Payment")){
				        writeTestResults("Verify Outbound payment form is open in a new tab", 
				        		"Outbound payment form is open in a new tab",
							"Outbound payment form is open in a new tab Successfully", "pass");
		        } else {
		        	    writeTestResults("Verify Outbound payment form is open in a new tab", 
		        	    		"Outbound payment form is open in a new tab",
								"Outbound payment form is open in a new tab Fail", "fail");
		        } 
				
				WaitClick(btn_DetailsTabA);
				click(btn_DetailsTabA);
				WaitClick(btn_ViewAllPendingBtnA);
				click(btn_ViewAllPendingBtnA);

				WaitClick(btn_OutboundPaymentAdviceRecordCheckboxA.replace("OPANo", OPANo1));
				click(btn_OutboundPaymentAdviceRecordCheckboxA.replace("OPANo", OPANo1));
				WaitClick(btn_OutboundPaymentAdviceRecordCheckboxA.replace("OPANo", OPANo2));
				click(btn_OutboundPaymentAdviceRecordCheckboxA.replace("OPANo", OPANo2));
				
				WaitClick(btn_CheckoutA);
				click(btn_CheckoutA);
				
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that able to draft the outbound payment", "User Can draft the outbound payment document",
								"User Can draft the outbound payment document Successfully", "pass");
				} else {
						writeTestResults("Verify that able to draft the outbound payment", "User Can draft the outbound payment document",
								"User Can draft the outbound payment document Fail", "fail");
				}
				
				WaitClick(btn_ReleaseA);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String OPNo = trackCode.replace(" Employee Payment", "");
				
				WaitElement(txt_PageRelease2A);
				if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that user able to do the payment for multiple advance requests at one outbound payment", 
								"user able to do the payment for multiple advance requests at one outbound payment",
							"user able to do the payment for multiple advance requests at one outbound payment Successfully", "pass");
				} else {
						writeTestResults("Verify that user able to do the payment for multiple advance requests at one outbound payment", 
								"user able to do the payment for multiple advance requests at one outbound payment",
								"user able to do the payment for multiple advance requests at one outbound payment Fail", "fail");
				}
		    	
		    	for(int i =1;i<=7;i++) {
		    		driver.navigate().back();
		    	}
		    	
//		    	System.out.println(EARNo1);
//		    	System.out.println(EARNo2);
		    	
		    	sendKeys(txt_EmployeeAdvanceRequestSearchTxtA, EARNo1);
		    	WaitClick(btn_EmployeeAdvanceRequestSearchBtnA);
		    	click(btn_EmployeeAdvanceRequestSearchBtnA);
		    	WaitClick(sel_EmployeeAdvanceRequestSearchSelA.replace("EARNo", EARNo1));
		    	click(sel_EmployeeAdvanceRequestSearchSelA.replace("EARNo", EARNo1));
		    	
		    	WaitElement(txt_BalanceAmountA);
		    	String BalanceAmount1A= getText(txt_BalanceAmountA).replace(",", "");
		    	//System.out.println(BalanceAmount1A);
		    	
		    	WaitClick(btn_ReferenceDetailsTabA);
		    	click(btn_ReferenceDetailsTabA);
		    	
		    	Thread.sleep(3000);
		    	String OutboundPaymentAdviceRecord1A= getText(txt_OutboundPaymentAdviceRecord1A);
		    	String OutboundPaymentRecord1A= getText(txt_OutboundPaymentAdviceRecord2A);
		    	
		    	driver.navigate().back();
		    	
		    	sendKeys(txt_EmployeeAdvanceRequestSearchTxtA, EARNo2);
		    	WaitClick(btn_EmployeeAdvanceRequestSearchBtnA);
		    	click(btn_EmployeeAdvanceRequestSearchBtnA);
		    	WaitClick(sel_EmployeeAdvanceRequestSearchSelA.replace("EARNo", EARNo2));
		    	click(sel_EmployeeAdvanceRequestSearchSelA.replace("EARNo", EARNo2));
		    	
		    	WaitElement(txt_BalanceAmountA);
		    	String BalanceAmount2A= getText(txt_BalanceAmountA).replace(",", "");
		    	//System.out.println(BalanceAmount2A);
		    	
		    	WaitClick(btn_ReferenceDetailsTabA);
		    	click(btn_ReferenceDetailsTabA);
		    	
		    	Thread.sleep(3000);
		    	String OutboundPaymentAdviceRecord2A= getText(txt_OutboundPaymentAdviceRecord1A);
		    	String OutboundPaymentRecord2A= getText(txt_OutboundPaymentAdviceRecord2A);
		    	
				if (OutboundPaymentAdviceRecord1A.contentEquals(OPANo1) && OutboundPaymentRecord1A.contentEquals(OPNo) && BalanceAmount1A.contentEquals("120000.000000")
						 && OutboundPaymentAdviceRecord2A.contentEquals(OPANo2) && OutboundPaymentRecord2A.contentEquals(OPNo) && BalanceAmount2A.contentEquals("120000.000000")) {
					writeTestResults("Verify that only the relvant amount is displayed in the \"Balance Amount\" field when user pay for the multiple advance requests via one outbound payment", 
							"only the relvant amount is displayed in the \"Balance Amount\" field when user pay for the multiple advance requests via one outbound payment",
						"only the relvant amount is displayed in the \"Balance Amount\" field when user pay for the multiple advance requests via one outbound payment Successfully", "pass");
				} else {
					writeTestResults("Verify that only the relvant amount is displayed in the \"Balance Amount\" field when user pay for the multiple advance requests via one outbound payment", 
							"only the relvant amount is displayed in the \"Balance Amount\" field when user pay for the multiple advance requests via one outbound payment",
							"only the relvant amount is displayed in the \"Balance Amount\" field when user pay for the multiple advance requests via one outbound payment Fail", "fail");
				}
				
		    }
		    
		    //Service_EAR_031to037
		    public void VerifyThatUserAbleToCreateTheMultipleOutboundPaymentAdvicesForAnEmployeeAdvancerRequest() throws Exception {
		    	
		    	VerifyThatEmployeeAdvanceRequestCanBeRelease();
		    	
				WaitClick(btn_ActionBtnA);
			    click(btn_ActionBtnA);
			    
				WaitElement(btn_PayEmployeeAdvanceA);
				if (isDisplayed(btn_PayEmployeeAdvanceA)) {
						writeTestResults("Verify that Following Action is avilable under the released document * Pay Employee Advance", "Following Action is avilable under the released document * Pay Employee Advance",
								"Following Action is avilable under the released document * Pay Employee Advance Successfully", "pass");
						click(btn_PayEmployeeAdvanceA);
				} else {
						writeTestResults("Verify that Following Action is avilable under the released document * Pay Employee Advance", "Following Action is avilable under the released document * Pay Employee Advance",
								"Following Action is avilable under the released document * Pay Employee Advance Fail", "fail");
				}
				
				Thread.sleep(3000);
				if (getText(txt_NewOutboundPaymentAdvicePageHeaderA).contentEquals("Outbound Payment Advice - New")){
				        writeTestResults("Verify that able to navigated to the \"Outbound Payment Advice\" form", 
				        		"navigated to the \"Outbound Payment Advice\" form",
							"navigated to the \"Outbound Payment Advice\" form Successfully", "pass");
		        } else {
		        	    writeTestResults("Verify that able to navigated to the \"Outbound Payment Advice\" form", 
		        	    		"navigated to the \"Outbound Payment Advice\" form",
								"navigated to the \"Outbound Payment Advice\" form Fail", "fail");
		        }
				
		    	WaitClick(btn_CheckoutA);
		    	click(btn_CheckoutA);
				
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that able to draft the outbound payment advice", "User Can draft the outbound payment advice First document",
								"User Can draft the outbound payment advice First document Successfully", "pass");
				} else {
						writeTestResults("Verify that able to draft the outbound payment advice", "User Can draft the outbound payment advice First document",
								"User Can draft the outbound payment advice First document Fail", "fail");
				}
				
				WaitClick(btn_ReleaseA);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String OPANo1 = trackCode.replace(" Employee Advance Advice", "");
				
				WaitElement(txt_PageRelease2A);
				if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that able to release the outbound payment advice", "User Can Release the outbound payment advice First document",
							"User Can Release the outbound payment advice First document Successfully", "pass");
				} else {
						writeTestResults("Verify that able to release the outbound payment advice", "User Can Release the outbound payment advice First document",
								"User Can Release the outbound payment advice First document Fail", "fail");
				}
				
		    	for(int i =1;i<=2;i++) {
		    		driver.navigate().back();
		    	}
		    	
				WaitClick(btn_ActionBtnA);
			    click(btn_ActionBtnA);
			    
				WaitElement(btn_PayEmployeeAdvanceA);
				if (isDisplayed(btn_PayEmployeeAdvanceA)) {
						writeTestResults("Verify that Following Action is avilable under the released document * Pay Employee Advance", "Following Action is avilable under the released document * Pay Employee Advance",
								"Following Action is avilable under the released document * Pay Employee Advance Successfully", "pass");
						click(btn_PayEmployeeAdvanceA);
				} else {
						writeTestResults("Verify that Following Action is avilable under the released document * Pay Employee Advance", "Following Action is avilable under the released document * Pay Employee Advance",
								"Following Action is avilable under the released document * Pay Employee Advance Fail", "fail");
				}
				
				Thread.sleep(3000);
				if (getText(txt_NewOutboundPaymentAdvicePageHeaderA).contentEquals("Outbound Payment Advice - New")){
				        writeTestResults("Verify that able to navigated to the \"Outbound Payment Advice\" form", 
				        		"navigated to the \"Outbound Payment Advice\" form",
							"navigated to the \"Outbound Payment Advice\" form Successfully", "pass");
		        } else {
		        	    writeTestResults("Verify that able to navigated to the \"Outbound Payment Advice\" form", 
		        	    		"navigated to the \"Outbound Payment Advice\" form",
								"navigated to the \"Outbound Payment Advice\" form Fail", "fail");
		        }
				
		    	WaitClick(btn_CheckoutA);
		    	click(btn_CheckoutA);
				
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that able to draft the outbound payment advice", "User Can draft the outbound payment advice Second document",
								"User Can draft the outbound payment advice Second document Successfully", "pass");
				} else {
						writeTestResults("Verify that able to draft the outbound payment advice", "User Can draft the outbound payment advice Second document",
								"User Can draft the outbound payment advice Second document Fail", "fail");
				}
				
				WaitClick(btn_ReleaseA);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String OPANo2 = trackCode.replace(" Employee Advance Advice", "");
				
				WaitElement(txt_PageRelease2A);
				if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that able to release the outbound payment advice", "User Can Release the outbound payment advice Second document",
							"User Can Release the outbound payment advice Second document Successfully", "pass");
				} else {
						writeTestResults("Verify that able to release the outbound payment advice", "User Can Release the outbound payment advice Second document",
								"User Can Release the outbound payment advice Second document Fail", "fail");
				}
				
		    	for(int i =1;i<=2;i++) {
		    		driver.navigate().back();
		    	}
		    	
		    	WaitClick(btn_ReferenceDetailsTabA);
		    	click(btn_ReferenceDetailsTabA);
		    	
		    	Thread.sleep(3000);
		    	String OutboundPaymentAdviceRecord1A= getText(txt_OutboundPaymentAdviceRecord1A);
		    	String OutboundPaymentAdviceRecord2A= getText(txt_OutboundPaymentAdviceRecord2A);
		    	
				if (OutboundPaymentAdviceRecord1A.contentEquals(OPANo1) && OutboundPaymentAdviceRecord2A.contentEquals(OPANo2)) {
					writeTestResults("Verify that all the outbound payment advices are displayed in the reference details tab of the employee advance request form", 
							"all the outbound payment advices are displayed in the reference details tab of the employee advance request form",
						"all the outbound payment advices are displayed in the reference details tab of the employee advance request form Successfully", "pass");
				} else {
					writeTestResults("Verify that all the outbound payment advices are displayed in the reference details tab of the employee advance request form", 
							"all the outbound payment advices are displayed in the reference details tab of the employee advance request form",
							"all the outbound payment advices are displayed in the reference details tab of the employee advance request form Fail", "fail");
				}
				
				driver.navigate().forward();
				
				WaitClick(btn_OutboundPaymentAdviceByPageHeaderA);
				click(btn_OutboundPaymentAdviceByPageHeaderA);
				
				sendKeys(txt_OutboundPaymentAdviceSearchTxtA, OPANo1);
				WaitClick(btn_OutboundPaymentAdviceSearchBtnA);
				click(btn_OutboundPaymentAdviceSearchBtnA);
				WaitClick(sel_OutboundPaymentAdviceSearchSelA.replace("OPANo", OPANo1));
				click(sel_OutboundPaymentAdviceSearchSelA.replace("OPANo", OPANo1));
				
				WaitClick(btn_ActionBtnA);
			    click(btn_ActionBtnA);
				WaitClick(btn_ConvertToOutboundPaymentBtnA);
			    click(btn_ConvertToOutboundPaymentBtnA);
			    //System.out.println(getText(txt_NewOutboundPaymentPageHeaderA));
			    
				Thread.sleep(3000);
				if (getText(txt_NewOutboundPaymentPageHeaderA).contentEquals("Outbound Payment - New Employee Payment")){
				        writeTestResults("Verify Outbound payment form is open in a new tab", 
				        		"Outbound payment form is open in a new tab",
							"Outbound payment form is open in a new tab Successfully", "pass");
		        } else {
		        	    writeTestResults("Verify Outbound payment form is open in a new tab", 
		        	    		"Outbound payment form is open in a new tab",
								"Outbound payment form is open in a new tab Fail", "fail");
		        }
				
				WaitClick(btn_DetailsTabA);
				click(btn_DetailsTabA);
				
				WaitClick(btn_OutboundPaymentCheckBoxA);
				click(btn_OutboundPaymentCheckBoxA);
				
				WaitClick(btn_CheckoutA);
				click(btn_CheckoutA);
				
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that able to draft the outbound payment", "User Can draft the outbound payment First document",
								"User Can draft the outbound payment First document Successfully", "pass");
				} else {
						writeTestResults("Verify that able to draft the outbound payment", "User Can draft the outbound payment First document",
								"User Can draft the outbound payment First document Fail", "fail");
				}
				
				WaitClick(btn_ReleaseA);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String OPNo1 = trackCode.replace(" Employee Payment", "");
				String OPTotal1 = getText(txt_OutboundPaymentHeaderTotalA).replace(".000000", "");
				int Total1= Integer.parseInt(OPTotal1.replace(",", ""));
				//System.out.println(OPTotal1);
				
		    	WaitElement(txt_PageRelease2A);
		    	if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that able to release the outbound payment advice", "User Can Release the outbound payment advice First document",
							"User Can Release the outbound payment advice First document Successfully", "pass");
		    	} else {
						writeTestResults("Verify that able to release the outbound payment advice", "User Can Release the outbound payment advice First document",
								"User Can Release the outbound payment advice First document Fail", "fail");
		    	}
		    	
		    	for(int i =1;i<=2;i++) {
		    		driver.navigate().back();
		    	}
		    	
				WaitClick(btn_OutboundPaymentAdviceByPageHeaderA);
				click(btn_OutboundPaymentAdviceByPageHeaderA);
				
				sendKeys(txt_OutboundPaymentAdviceSearchTxtA, OPANo2);
				WaitClick(btn_OutboundPaymentAdviceSearchBtnA);
				click(btn_OutboundPaymentAdviceSearchBtnA);
				WaitClick(sel_OutboundPaymentAdviceSearchSelA.replace("OPANo", OPANo2));
				click(sel_OutboundPaymentAdviceSearchSelA.replace("OPANo", OPANo2));
				
				WaitClick(btn_ActionBtnA);
			    click(btn_ActionBtnA);
				WaitClick(btn_ConvertToOutboundPaymentBtnA);
			    click(btn_ConvertToOutboundPaymentBtnA);
			    //System.out.println(getText(txt_NewOutboundPaymentPageHeaderA));
			    
				Thread.sleep(3000);
				if (getText(txt_NewOutboundPaymentPageHeaderA).contentEquals("Outbound Payment - New Employee Payment")){
				        writeTestResults("Verify Outbound payment form is open in a new tab", 
				        		"Outbound payment form is open in a new tab",
							"Outbound payment form is open in a new tab Successfully", "pass");
		        } else {
		        	    writeTestResults("Verify Outbound payment form is open in a new tab", 
		        	    		"Outbound payment form is open in a new tab",
								"Outbound payment form is open in a new tab Fail", "fail");
		        }
				
				WaitClick(btn_DetailsTabA);
				click(btn_DetailsTabA);
				
				WaitClick(btn_OutboundPaymentCheckBoxA);
				click(btn_OutboundPaymentCheckBoxA);
				
				WaitClick(btn_CheckoutA);
				click(btn_CheckoutA);
				
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that able to draft the outbound payment", "User Can draft the outbound payment Second document",
								"User Can draft the outbound payment Second document Successfully", "pass");
				} else {
						writeTestResults("Verify that able to draft the outbound payment", "User Can draft the outbound payment Second document",
								"User Can draft the outbound payment Second document Fail", "fail");
				}
				
				WaitClick(btn_ReleaseA);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String OPNo2 = trackCode.replace(" Employee Payment", "");
				String OPTotal2 = getText(txt_OutboundPaymentHeaderTotalA).replace(".000000", "");
				int Total2= Integer.parseInt(OPTotal2.replace(",", ""));
				int SubTotal= Total1+Total2;
				//System.out.println(OPTotal2);
				
		    	WaitElement(txt_PageRelease2A);
		    	if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that able to release the outbound payment advice", "User Can Release the outbound payment Second document",
							"User Can Release the outbound payment Second document Successfully", "pass");
		    	} else {
						writeTestResults("Verify that able to release the outbound payment advice", "User Can Release the outbound payment Second document",
								"User Can Release the outbound payment Second document Fail", "fail");
		    	}
		    	
		    	for(int i =1;i<=7;i++) {
		    		driver.navigate().back();
		    	}
		    	
		    	WaitClick(btn_ReferenceDetailsTabA);
		    	click(btn_ReferenceDetailsTabA);
		    	
		    	Thread.sleep(3000);
		    	String OutboundPaymentRecord1A= getText(txt_OutboundPaymentRecord1A);
		    	String OutboundPaymentRecord2A= getText(txt_OutboundPaymentRecord2A);
		    	
				if (OutboundPaymentRecord1A.contentEquals(OPNo1) && OutboundPaymentRecord2A.contentEquals(OPNo2)) {
					writeTestResults("Verify that all the outbound payment documents details are recorded in the reference details tab of employee advance request", 
							"all the outbound payment documents details are recorded in the reference details tab of employee advance request",
						"all the outbound payment documents details are recorded in the reference details tab of employee advance request Successfully", "pass");
				} else {
					writeTestResults("Verify that all the outbound payment documents details are recorded in the reference details tab of employee advance request", 
							"all the outbound payment documents details are recorded in the reference details tab of employee advance request",
							"all the outbound payment documents details are recorded in the reference details tab of employee advance request Fail", "fail");
				}
				
				WaitClick(btn_SummaryTabA);
				click(btn_SummaryTabA);
				
		    	WaitElement(txt_AmountTextFieldAfterValueA);
		    	String AmountTextFieldAfterValueA= getText(txt_AmountTextFieldAfterValueA).replace(".000000", "");
		    	int SubAmount = Integer.parseInt(AmountTextFieldAfterValueA.replace(",", ""));
		    	
		    	WaitElement(txt_PaidAmountEARA);
		    	String PaidAmountEARA= getText(txt_PaidAmountEARA).replace(".000000", "");
		    	int SubPaidAmount = Integer.parseInt(PaidAmountEARA.replace(",", ""));
		    	
		    	WaitElement(txt_BalanceAmountA);
		    	String BalanceAmountA= getText(txt_BalanceAmountA).replace(".000000", "");
		    	int SubBalanceAmount = Integer.parseInt(BalanceAmountA.replace(",", ""));
		    	
				if (SubAmount==SubTotal) {
					writeTestResults("Verify that advance amount field is updated according to the paid amount via outbound payment", 
							"advance amount field is updated according to the paid amount via outbound payment",
						"advance amount field is updated according to the paid amount via outbound payment Successfully", "pass");
				} else {
					writeTestResults("Verify that advance amount field is updated according to the paid amount via outbound payment", 
							"advance amount field is updated according to the paid amount via outbound payment",
							"advance amount field is updated according to the paid amount via outbound payment Fail", "fail");
				}
				
				if (SubPaidAmount==SubTotal) {
					writeTestResults("Verify that \"Paid Amount\" field is updated according to the payed amount via outbound payment", 
							"\"Paid Amount\" field is updated according to the payed amount via outbound payment",
						"\"Paid Amount\" field is updated according to the payed amount via outbound payment Successfully", "pass");
				} else {
					writeTestResults("Verify that \"Paid Amount\" field is updated according to the payed amount via outbound payment", 
							"\"Paid Amount\" field is updated according to the payed amount via outbound payment",
							"\"Paid Amount\" field is updated according to the payed amount via outbound payment Fail", "fail");
				}
				
				if (SubBalanceAmount==SubTotal) {
					writeTestResults("Verify that \"Balance amount\" field is updated according to the payed amount via outbound payment", 
							"\"Balance amount\" field is updated according to the payed amount via outbound payment",
						"\"Balance amount\" field is updated according to the payed amount via outbound payment Successfully", "pass");
				} else {
					writeTestResults("Verify that \"Balance amount\" field is updated according to the payed amount via outbound payment", 
							"\"Balance amount\" field is updated according to the payed amount via outbound payment",
							"\"Balance amount\" field is updated according to the payed amount via outbound payment Fail", "fail");
				}
		    }
		    
		    //Service_EAR_038
		    public void VerifyThatReverseActionIsNotAvailbaleInTheEmployeeAdvanceRequestFormWhereUserReleaseTheOutboundPaymnetAdviceAndOutboundPayment() throws Exception {
		    	
		    	VerifyThatEmployeeAdvanceRequestCanBeRelease();
		    	
				WaitClick(btn_ActionBtnA);
			    click(btn_ActionBtnA);
			    
				WaitElement(btn_PayEmployeeAdvanceA);
				if (isDisplayed(btn_PayEmployeeAdvanceA)) {
						writeTestResults("Verify that Following Action is avilable under the released document * Pay Employee Advance", "Following Action is avilable under the released document * Pay Employee Advance",
								"Following Action is avilable under the released document * Pay Employee Advance Successfully", "pass");
						click(btn_PayEmployeeAdvanceA);
				} else {
						writeTestResults("Verify that Following Action is avilable under the released document * Pay Employee Advance", "Following Action is avilable under the released document * Pay Employee Advance",
								"Following Action is avilable under the released document * Pay Employee Advance Fail", "fail");
				}
				
				Thread.sleep(3000);
				if (getText(txt_NewOutboundPaymentAdvicePageHeaderA).contentEquals("Outbound Payment Advice - New")){
				        writeTestResults("Verify that able to navigated to the \"Outbound Payment Advice\" form", 
				        		"navigated to the \"Outbound Payment Advice\" form",
							"navigated to the \"Outbound Payment Advice\" form Successfully", "pass");
		        } else {
		        	    writeTestResults("Verify that able to navigated to the \"Outbound Payment Advice\" form", 
		        	    		"navigated to the \"Outbound Payment Advice\" form",
								"navigated to the \"Outbound Payment Advice\" form Fail", "fail");
		        }
				
		    	WaitClick(btn_CheckoutA);
		    	click(btn_CheckoutA);
				
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that able to draft the outbound payment advice", "User Can draft the outbound payment advice document",
								"User Can draft the outbound payment advice document Successfully", "pass");
				} else {
						writeTestResults("Verify that able to draft the outbound payment advice", "User Can draft the outbound payment advice document",
								"User Can draft the outbound payment advice document Fail", "fail");
				}
				
				WaitClick(btn_ReleaseA);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				
				WaitElement(txt_PageRelease2A);
				if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that able to release the outbound payment advice", "User Can Release the outbound payment advice document",
							"User Can Release the outbound payment advice document Successfully", "pass");
				} else {
						writeTestResults("Verify that able to release the outbound payment advice", "User Can Release the outbound payment advice document",
								"User Can Release the outbound payment advice document Fail", "fail");
				}

				WaitClick(btn_ActionBtnA);
			    click(btn_ActionBtnA);
				WaitClick(btn_ConvertToOutboundPaymentBtnA);
			    click(btn_ConvertToOutboundPaymentBtnA);
			    
				Thread.sleep(3000);
				if (getText(txt_NewOutboundPaymentPageHeaderA).contentEquals("Outbound Payment - New Employee Payment")){
				        writeTestResults("Verify Outbound payment form is open in a new tab", 
				        		"Outbound payment form is open in a new tab",
							"Outbound payment form is open in a new tab Successfully", "pass");
		        } else {
		        	    writeTestResults("Verify Outbound payment form is open in a new tab", 
		        	    		"Outbound payment form is open in a new tab",
								"Outbound payment form is open in a new tab Fail", "fail");
		        }
				
				WaitClick(btn_DetailsTabA);
				click(btn_DetailsTabA);
				
				WaitClick(btn_OutboundPaymentCheckBoxA);
				click(btn_OutboundPaymentCheckBoxA);
				
				WaitClick(btn_CheckoutA);
				click(btn_CheckoutA);
				
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that able to draft the outbound payment", "User Can draft the outbound payment document",
								"User Can draft the outbound payment document Successfully", "pass");
				} else {
						writeTestResults("Verify that able to draft the outbound payment", "User Can draft the outbound payment document",
								"User Can draft the outbound payment document Fail", "fail");
				}
				
				WaitClick(btn_ReleaseA);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				
		    	WaitElement(txt_PageRelease2A);
		    	if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that able to release the outbound payment advice", "User Can Release the outbound payment advice document",
							"User Can Release the outbound payment advice document Successfully", "pass");
		    	} else {
						writeTestResults("Verify that able to release the outbound payment advice", "User Can Release the outbound payment advice document",
								"User Can Release the outbound payment advice document Fail", "fail");
		    	}
		    	
		    	for(int i =1;i<=4;i++) {
		    		driver.navigate().back();
		    	}
		    	
				WaitClick(btn_ActionBtnA);
			    click(btn_ActionBtnA);
			    
		    	if (!isDisplayed(btn_ReverseBtnA)) {
						writeTestResults("Verify that Reverse Action is not availbale in the Employee advance request form where user release the Outbound paymnet advice and outbound payment", 
								"Reverse Action is not availbale in the Employee advance request form where user release the Outbound paymnet advice and outbound payment",
							"Reverse Action is not availbale in the Employee advance request form where user release the Outbound paymnet advice and outbound payment Successfully", "pass");
		    	} else {
						writeTestResults("Verify that Reverse Action is not availbale in the Employee advance request form where user release the Outbound paymnet advice and outbound payment", 
								"Reverse Action is not availbale in the Employee advance request form where user release the Outbound paymnet advice and outbound payment",
								"Reverse Action is not availbale in the Employee advance request form where user release the Outbound paymnet advice and outbound payment Fail", "fail");
		    	}
		    }
		    
		    //Service_DWS_001
		    public void VerifyThatDailyWorkSheetIsAvailableUnderTheServiceModule() throws Exception {
		    	
		    	WaitElement(btn_DailyWorkSheetBtnA);
		    	if (isDisplayed(btn_DailyWorkSheetBtnA)) {
						writeTestResults("Verify that \"Daily work sheet\" is available under the service module", 
								"\"Daily work sheet\" is available under the service module",
							"\"Daily work sheet\" is available under the service module Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"Daily work sheet\" is available under the service module", 
								"\"Daily work sheet\" is available under the service module",
								"\"Daily work sheet\" is available under the service module Fail", "fail");
		    	}
		    }
		    
		    //Service_DWS_002
		    public void VerifyThatDailyWorkSheetByPageIsDisplayed() throws Exception {
		    	
		    	WaitElement(btn_DailyWorkSheetBtnA);
		    	click(btn_DailyWorkSheetBtnA);
		    	
		    	WaitElement(txt_DailyWorksheetByPageA);
		    	if (isDisplayed(txt_DailyWorksheetByPageA)) {
						writeTestResults("Verify that daily work sheet by page is displayed", 
								"daily work sheet by page is displayed",
							"daily work sheet by page is displayed Successfully", "pass");
		    	} else {
						writeTestResults("Verify that daily work sheet by page is displayed", 
								"daily work sheet by page is displayed",
								"daily work sheet by page is displayed Fail", "fail");
		    	}
		    }
		    
		    //Service_DWS_003
		    public void VerifyThatFollowingColumnsAreAvailableInTheDailyWorkSheetByPage() throws Exception {
		    	
		    	WaitElement(btn_DailyWorkSheetBtnA);
		    	click(btn_DailyWorkSheetBtnA);
		    	
		    	if (isDisplayed(txt_DailyWorkSheetNoColumnA) && isDisplayed(txt_ReferenceNoColumnA) && isDisplayed(txt_DocDateColumnA) && isDisplayed(txt_CreatedUserColumnA)
		    			 && isDisplayed(txt_CreatedDateColumnA) && isDisplayed(txt_PostedDateColumnA) && isDisplayed(txt_PostBusinessUnitColumnA)) {
						writeTestResults("Verify that following columns are available in the daily work sheet by page", 
								"following columns are available in the daily work sheet by page",
							"following columns are available in the daily work sheet by page Successfully", "pass");
		    	} else {
						writeTestResults("Verify that following columns are available in the daily work sheet by page", 
								"following columns are available in the daily work sheet by page",
								"following columns are available in the daily work sheet by page Fail", "fail");
		    	}
		    }
		    
		    //Service_DWS_004_005_006
		    public void VerifyThatNewDailyWorkSheetButtonIsAvailableInTheByPage() throws Exception {
		    	
		    	WaitElement(btn_DailyWorkSheetBtnA);
		    	click(btn_DailyWorkSheetBtnA);
		    	
		    	WaitElement(btn_NewDailyWorksheetBtnA);
		    	if (isDisplayed(btn_NewDailyWorksheetBtnA)) {
						writeTestResults("Verify that \"New Daily Work sheet\" Button is available in the by page", 
								"\"New Daily Work sheet\" Button is available in the by page",
							"\"New Daily Work sheet\" Button is available in the by page Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"New Daily Work sheet\" Button is available in the by page", 
								"\"New Daily Work sheet\" Button is available in the by page",
								"\"New Daily Work sheet\" Button is available in the by page Fail", "fail");
		    	}
		    	
		    	WaitElement(btn_NewDailyWorksheetBtnA);
		    	click(btn_NewDailyWorksheetBtnA);
		    	
		    	WaitElement(txt_StartNewJourneyHeaderA);
		    	if (isDisplayed(txt_StartNewJourneyHeaderA)) {
						writeTestResults("Verify that \"Start New journey\" look up is displayed when click on the \"New Daily Work Sheet \" button", 
								"\"Start New journey\" look up is displayed when click on the \"New Daily Work Sheet \" button",
							"\"Start New journey\" look up is displayed when click on the \"New Daily Work Sheet \" button Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"Start New journey\" look up is displayed when click on the \"New Daily Work Sheet \" button", 
								"\"Start New journey\" look up is displayed when click on the \"New Daily Work Sheet \" button",
								"\"Start New journey\" look up is displayed when click on the \"New Daily Work Sheet \" button Fail", "fail");
		    	}
		    	
		    	if (isDisplayed(btn_DailyWorkSheetServiceBtnA) && isDisplayed(btn_DailyWorkSheetProjectBtnA)) {
						writeTestResults("Verify that Following journeys are displayed in the \"Start New journey\" look up, Daily Work Sheet (Service),Daily Work Sheet (Project)", 
								"Following journeys are displayed in the \"Start New journey\" look up, Daily Work Sheet (Service),Daily Work Sheet (Project)",
							"Following journeys are displayed in the \"Start New journey\" look up, Daily Work Sheet (Service),Daily Work Sheet (Project) Successfully", "pass");
		    	} else {
						writeTestResults("Verify that Following journeys are displayed in the \"Start New journey\" look up, Daily Work Sheet (Service),Daily Work Sheet (Project)", 
								"Following journeys are displayed in the \"Start New journey\" look up, Daily Work Sheet (Service),Daily Work Sheet (Project)",
								"Following journeys are displayed in the \"Start New journey\" look up, Daily Work Sheet (Service),Daily Work Sheet (Project) Fail", "fail");
		    	}
		    }
		    
		    //Service_DWS_007to012
		    public void VerifyThatUserAbleToNavigatedToTheWorkedHoursNewFormWhenClickOnTheDailyWorkSheetServiceJourney() throws Exception {
		    	
		    	WaitElement(btn_DailyWorkSheetBtnA);
		    	click(btn_DailyWorkSheetBtnA);
		    	
		    	WaitElement(btn_NewDailyWorksheetBtnA);
		    	click(btn_NewDailyWorksheetBtnA);
		    	
		    	WaitElement(btn_DailyWorkSheetServiceBtnA);
		    	click(btn_DailyWorkSheetServiceBtnA);
		    	
		    	if (getText(txt_WorkedHoursServiceNewPageHeaderA).contentEquals("Worked Hours - New")) {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
							"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Fail", "fail");
		    	}
		    	
		    	if (isDisplayed(btn_DraftA) && isDisplayed(btn_DraftAndNewA)) {
						writeTestResults("Verify that following buttons are availble in the  \"Worked Hours - New\" form, Draft,Draft and New", 
								"following buttons are availble in the  \"Worked Hours - New\" form, Draft,Draft and New",
							"following buttons are availble in the  \"Worked Hours - New\" form, Draft,Draft and New Successfully", "pass");
		    	} else {
						writeTestResults("Verify that following buttons are availble in the  \"Worked Hours - New\" form, Draft,Draft and New", 
								"following buttons are availble in the  \"Worked Hours - New\" form, Draft,Draft and New",
								"following buttons are availble in the  \"Worked Hours - New\" form, Draft,Draft and New Fail", "fail");
		    	}
		    	
		    	if (getText(txt_JobNoFieldA).isEmpty()) {
						writeTestResults("Verify that empty line avilable in the form", 
								"empty line avilable in the form",
							"empty line avilable in the form Successfully", "pass");
		    	} else {
						writeTestResults("Verify that empty line avilable in the form", 
								"empty line avilable in the form",
								"empty line avilable in the form Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitElement(txt_AddNewJobHeaderA);
		    	if (isDisplayed(txt_AddNewJobHeaderA)) {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
							"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Fail", "fail");
		    	}
		    	
		    	if (isDisplayed(txt_JobNoStarA) && isDisplayed(txt_EmployeeStarA)) {
						writeTestResults("Verify that following fields are marked as the required fields of \"Add New Job look up\" , Job No,Employee", 
								"following fields are marked as the required fields of \"Add New Job look up\" , Job No,Employee",
							"following fields are marked as the required fields of \"Add New Job look up\" , Job No,Employee Successfully", "pass");
		    	} else {
						writeTestResults("Verify that following fields are marked as the required fields of \"Add New Job look up\" , Job No,Employee", 
								"following fields are marked as the required fields of \"Add New Job look up\" , Job No,Employee",
								"following fields are marked as the required fields of \"Add New Job look up\" , Job No,Employee Fail", "fail");
		    	}
		    	
		    	WaitElement(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);
		    	
		    	WaitClick(btn_EmployeeBtnErrorA);
		    	click(btn_EmployeeBtnErrorA);
		    	
		    	if (isDisplayed(txt_EmployeeIsRequiredErrorA) && isDisplayed(txt_JobNoValidatorA)) {
						writeTestResults("Verify that required field validation is displayed for the following fields, Job No,Employee", 
								"required field validation is displayed for the following fields, Job No,Employee",
							"required field validation is displayed for the following fields, Job No,Employee Successfully", "pass");
		    	} else {
						writeTestResults("Verify that required field validation is displayed for the following fields, Job No,Employee", 
								"required field validation is displayed for the following fields, Job No,Employee",
								"required field validation is displayed for the following fields, Job No,Employee Fail", "fail");
		    	}
		    }
		    
		    //Service_DWS_013to019
		    public void VerifyThatDateFieldInTheAddNewJobLookUpIsAutoFilledWithTheCurrentDate() throws Exception {
		    	
		    	WaitElement(btn_DailyWorkSheetBtnA);
		    	click(btn_DailyWorkSheetBtnA);
		    	
		    	WaitElement(btn_NewDailyWorksheetBtnA);
		    	click(btn_NewDailyWorksheetBtnA);
		    	
		    	WaitElement(btn_DailyWorkSheetServiceBtnA);
		    	click(btn_DailyWorkSheetServiceBtnA);
		    	
		    	if (getText(txt_WorkedHoursServiceNewPageHeaderA).contentEquals("Worked Hours - New")) {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
							"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
				Date currentDate = new Date();

		        // convert date to calendar
		        Calendar cal = Calendar.getInstance();
		        cal.setTime(currentDate);
		        
		        //get Today date to String
		        String TodayDate = dateFormat.format(currentDate);
		        String AddNewJobDate = driver.findElement(By.xpath(txt_AddNewJobDateA)).getAttribute("value");
//
//		        System.out.println(TodayDate);
		    	
		    	WaitElement(txt_AddNewJobDateA);
		    	if (AddNewJobDate.contentEquals(TodayDate)) {
						writeTestResults("Verify that \"Date\" field in the \"Add New Job\" look up is auto filled with the current date", 
								"\"Date\" field in the \"Add New Job\" look up is auto filled with the current date",
							"\"Date\" field in the \"Add New Job\" look up is auto filled with the current date Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"Date\" field in the \"Add New Job\" look up is auto filled with the current date", 
								"\"Date\" field in the \"Add New Job\" look up is auto filled with the current date",
								"\"Date\" field in the \"Add New Job\" look up is auto filled with the current date Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_ServiceJobOrderSearchBtnA);
		    	click(btn_ServiceJobOrderSearchBtnA);
		    	
		    	WaitElement(txt_ServiceJobOrderLookupHeaderA);
		    	if (isDisplayed(txt_ServiceJobOrderLookupHeaderA)) {
						writeTestResults("Verify that service job order look up is displayed when click on the search icon in the \"Job No\" field", 
								"service job order look up is displayed when click on the search icon in the \"Job No\" field",
							"service job order look up is displayed when click on the search icon in the \"Job No\" field Successfully", "pass");
		    	} else {
						writeTestResults("Verify that service job order look up is displayed when click on the search icon in the \"Job No\" field", 
								"service job order look up is displayed when click on the search icon in the \"Job No\" field",
								"service job order look up is displayed when click on the search icon in the \"Job No\" field Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_ServiceJobOrderLookupSearchBtnA);
		    	click(btn_ServiceJobOrderLookupSearchBtnA);
		    	
		    	if (isDisplayed(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A)) 
		    			&& isDisplayed(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos2A))
		    			&& isDisplayed(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos3A))) {
						writeTestResults("Verify that all the released service job orders are displayed in the Service job order look up", 
								"all the released service job orders are displayed in the Service job order look up",
							"all the released service job orders are displayed in the Service job order look up Successfully", "pass");
		    	} else {
						writeTestResults("Verify that all the released service job orders are displayed in the Service job order look up", 
								"all the released service job orders are displayed in the Service job order look up",
								"all the released service job orders are displayed in the Service job order look up Fail", "fail");
		    	}
		    	
		    	if (!isDisplayed(txt_ReleseServiceJobOrderNosA.replace("SJONo", RevercedServiceJobOrderNos1A)) 
		    			&& !isDisplayed(txt_ReleseServiceJobOrderNosA.replace("SJONo", RevercedServiceJobOrderNos2A))
		    			&& !isDisplayed(txt_ReleseServiceJobOrderNosA.replace("SJONo", RevercedServiceJobOrderNos3A))) {
						writeTestResults("Verify that reversed service job orders are not displayed in the service job order look up", 
								"reversed service job orders are not displayed in the service job order look up",
							"reversed service job orders are not displayed in the service job order look up Successfully", "pass");
		    	} else {
						writeTestResults("Verify that reversed service job orders are not displayed in the service job order look up", 
								"reversed service job orders are not displayed in the service job order look up",
								"reversed service job orders are not displayed in the service job order look up Fail", "fail");
		    	}
		    	
		    	if (!isDisplayed(txt_ReleseServiceJobOrderNosA.replace("SJONo", DeletedServiceJobOrderNos1A)) 
		    			&& !isDisplayed(txt_ReleseServiceJobOrderNosA.replace("SJONo", DeletedServiceJobOrderNos2A))
		    			&& !isDisplayed(txt_ReleseServiceJobOrderNosA.replace("SJONo", DeletedServiceJobOrderNos3A))) {
						writeTestResults("Verify that deleted service job orders are not displayed in the service job order look up", 
								"deleted service job orders are not displayed in the service job order look up",
							"deleted service job orders are not displayed in the service job order look up Successfully", "pass");
		    	} else {
						writeTestResults("Verify that deleted service job orders are not displayed in the service job order look up", 
								"deleted service job orders are not displayed in the service job order look up",
								"deleted service job orders are not displayed in the service job order look up Fail", "fail");
		    	}
		    	
		    	WaitElement(txt_ServiceJobOrderLookupSearchTxtA);
		    	sendKeys(txt_ServiceJobOrderLookupSearchTxtA, ReleseServiceJobOrderNos1A);
		    	
		    	WaitClick(btn_ServiceJobOrderLookupSearchBtnA);
		    	click(btn_ServiceJobOrderLookupSearchBtnA);
		    	
		    	WaitClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));
		    	doubleClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));
		    	
		    	WaitElement(txt_JobNoTextFieldA);
		    	String JobNoText = driver.findElement(By.xpath(txt_JobNoTextFieldA)).getAttribute("value");
		    	
		    	if (JobNoText.contentEquals(ReleseServiceJobOrderNos1A)) {
						writeTestResults("Verify that selected service job order is displayed in the \"JOB No\" field", 
								"selected service job order is displayed in the \"JOB No\" field",
							"selected service job order is displayed in the \"JOB No\" field Successfully", "pass");
		    	} else {
						writeTestResults("Verify that selected service job order is displayed in the \"JOB No\" field", 
								"selected service job order is displayed in the \"JOB No\" field",
								"selected service job order is displayed in the \"JOB No\" field Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelA);
		    	click(sel_TimeInSelA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelA);
		    	click(sel_TimeOutSelA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);
		    	
		    	WaitClick(btn_EmployeeBtnErrorA);
		    	click(btn_EmployeeBtnErrorA);
		    	
		    	if (isDisplayed(txt_EmployeeIsRequiredErrorA)) {
						writeTestResults("Verify that user cannot add the record without an employee", 
								"user cannot add the record without an employee",
							"user cannot add the record without an employee Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user cannot add the record without an employee", 
								"user cannot add the record without an employee",
								"user cannot add the record without an employee Fail", "fail");
		    	}
		    	
		    }
		    
		    //Service_DWS_020to025
		    public void VerifyThatEmployeeLookUpIsDisplayedWhenClickOnTheSearchIconOfEmployeeField() throws Exception {
		    	
		    	WaitElement(btn_DailyWorkSheetBtnA);
		    	click(btn_DailyWorkSheetBtnA);
		    	
		    	WaitElement(btn_NewDailyWorksheetBtnA);
		    	click(btn_NewDailyWorksheetBtnA);
		    	
		    	WaitElement(btn_DailyWorkSheetServiceBtnA);
		    	click(btn_DailyWorkSheetServiceBtnA);
		    	
		    	if (getText(txt_WorkedHoursServiceNewPageHeaderA).contentEquals("Worked Hours - New")) {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
							"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);
		    	
		    	WaitElement(txt_EmployeeLookupHeaderA);
		    	if (isDisplayed(txt_EmployeeLookupHeaderA)) {
						writeTestResults("Verify that \"Employee\" look up is displayed when click on the search icon of \"Employee\" field", 
								"\"Employee\" look up is displayed when click on the search icon of \"Employee\" field",
							"\"Employee\" look up is displayed when click on the search icon of \"Employee\" field Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"Employee\" look up is displayed when click on the search icon of \"Employee\" field", 
								"\"Employee\" look up is displayed when click on the search icon of \"Employee\" field",
								"\"Employee\" look up is displayed when click on the search icon of \"Employee\" field Fail", "fail");
		    	}
		    	
		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	
		    	if (isDisplayed(txt_ReleseEmployeeNamesA.replace("EName", ReleseEmployeeNames1A)) 
		    			&& isDisplayed(txt_ReleseEmployeeNamesA.replace("EName", ReleseEmployeeNames2A))) {
						writeTestResults("Verify that all the active employees are displayed in the \"Employee\" look up", 
								"all the active employees are displayed in the \"Employee\" look up",
							"all the active employees are displayed in the \"Employee\" look up Successfully", "pass");
		    	} else {
						writeTestResults("Verify that all the active employees are displayed in the \"Employee\" look up", 
								"all the active employees are displayed in the \"Employee\" look up",
								"all the active employees are displayed in the \"Employee\" look up Fail", "fail");
		    	}
		    	
		    	if (!isDisplayed(txt_ReleseEmployeeNamesA.replace("EName", InactiveEmployeeNames1A )) 
		    			&& !isDisplayed(txt_ReleseEmployeeNamesA.replace("EName", InactiveEmployeeNames2A))) {
						writeTestResults("Verify that Inactive Users are not displayed in the Employee look up", 
								"Inactive Users are not displayed in the Employee look up",
							"Inactive Users are not displayed in the Employee look up Successfully", "pass");
		    	} else {
						writeTestResults("Verify that Inactive Users are not displayed in the Employee look up", 
								"Inactive Users are not displayed in the Employee look up",
								"Inactive Users are not displayed in the Employee look up Fail", "fail");
		    	}
		    	
		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, ReleseEmployeeNames1A);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
//		    	WaitClick(btn_EmployeeLookupSearchBtnA);
//		    	click(btn_EmployeeLookupSearchBtnA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", ReleseEmployeeNames1A));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", ReleseEmployeeNames1A));
		    	
		    	WaitElement(txt_EmployeeTextFieldDWA);
		    	String EmployeeTextFieldDW = getText(txt_EmployeeTextFieldDWA);
		    	
		    	if (EmployeeTextFieldDW.contentEquals(ReleseEmployeeNames1A)) {
						writeTestResults("Verify that user able to add the employees to field", 
								"user able to add the employees to field",
							"user able to add the employees to field Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to add the employees to field", 
								"user able to add the employees to field",
								"user able to add the employees to field Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);
		    	
//		    	WaitClick(btn_EmployeeLookupSearchBtnA);
//		    	click(btn_EmployeeLookupSearchBtnA);
		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", ReleseEmployeeNames1A));
		    	click(txt_ReleseEmployeeNamesA.replace("EName", ReleseEmployeeNames1A));
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", ReleseEmployeeNames2A));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", ReleseEmployeeNames2A));
		    	
		    	if (isDisplayed(txt_EmployeeTextFieldDWValuesA.replace("EName", ReleseEmployeeNames1A))
		    			&& isDisplayed(txt_EmployeeTextFieldDWValuesA.replace("EName", ReleseEmployeeNames2A))) {
						writeTestResults("Verify that user able to select multiple employees at once and Verify that employee code and name with initials are displayed in the field", 
								"user able to select multiple employees at once and employee code and name with initials are displayed in the field",
							"user able to select multiple employees at once and employee code and name with initials are displayed in the field Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to select multiple employees at once and Verify that employee code and name with initials are displayed in the field", 
								"user able to select multiple employees at once and employee code and name with initials are displayed in the field",
								"user able to select multiple employees at once and employee code and name with initials are displayed in the field Fail", "fail");
		    	}
		    	
		    }
		    
		    //Service_DWS_026to028
		    public void VerifyThatPleaseDefineaValidRateProfileForTheSelectedEmployeeErrorMessageIsDisplayedWhenSelectingTheEmployeeWhoDoesNotHaveTheValidRateProfile() throws Exception {
		    	
		    	WaitElement(btn_DailyWorkSheetBtnA);
		    	click(btn_DailyWorkSheetBtnA);
		    	
		    	WaitElement(btn_NewDailyWorksheetBtnA);
		    	click(btn_NewDailyWorksheetBtnA);
		    	
		    	WaitElement(btn_DailyWorkSheetServiceBtnA);
		    	click(btn_DailyWorkSheetServiceBtnA);
		    	
		    	if (getText(txt_WorkedHoursServiceNewPageHeaderA).contentEquals("Worked Hours - New")) {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
							"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitClick(btn_ServiceJobOrderSearchBtnA);
		    	click(btn_ServiceJobOrderSearchBtnA);
		    	
		    	WaitElement(txt_ServiceJobOrderLookupSearchTxtA);
		    	sendKeys(txt_ServiceJobOrderLookupSearchTxtA, ReleseServiceJobOrderNos1A);
		    	pressEnter(txt_ServiceJobOrderLookupSearchTxtA);
		    	
//		    	WaitClick(btn_ServiceJobOrderLookupSearchBtnA);
//		    	click(btn_ServiceJobOrderLookupSearchBtnA);
		    	
		    	WaitElement(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));
		    	doubleClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));
		    	
		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);
		    	
		    	WaitElement(txt_EmployeeLookupHeaderA);
		    	if (isDisplayed(txt_EmployeeLookupHeaderA)) {
						writeTestResults("Verify that \"Employee\" look up is displayed when click on the search icon of \"Employee\" field", 
								"\"Employee\" look up is displayed when click on the search icon of \"Employee\" field",
							"\"Employee\" look up is displayed when click on the search icon of \"Employee\" field Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"Employee\" look up is displayed when click on the search icon of \"Employee\" field", 
								"\"Employee\" look up is displayed when click on the search icon of \"Employee\" field",
								"\"Employee\" look up is displayed when click on the search icon of \"Employee\" field Fail", "fail");
		    	}
		    	
		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, ReleseEmployeeNames1A);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
//		    	WaitClick(btn_EmployeeLookupSearchBtnA);
//		    	click(btn_EmployeeLookupSearchBtnA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", ReleseEmployeeNames1A));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", ReleseEmployeeNames1A));
		    	
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelA);
		    	click(sel_TimeInSelA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelA);
		    	click(sel_TimeOutSelA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);
		    	
		    	WaitClick(btn_EmployeeBtnErrorA);
		    	click(btn_EmployeeBtnErrorA);
		    	
		    	WaitElement(txt_DefineaValidRateProfileErrorA);
		    	if (isDisplayed(txt_DefineaValidRateProfileErrorA)) {
						writeTestResults("Verify that \"Please define a valid Rate Profile for the selected employee ..\" error message is displayed when selecting the employee who does not have the valid rate profile", 
								"\"Please define a valid Rate Profile for the selected employee ..\" error message is displayed when selecting the employee who does not have the valid rate profile",
							"\"Please define a valid Rate Profile for the selected employee ..\" error message is displayed when selecting the employee who does not have the valid rate profile Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"Please define a valid Rate Profile for the selected employee ..\" error message is displayed when selecting the employee who does not have the valid rate profile", 
								"\"Please define a valid Rate Profile for the selected employee ..\" error message is displayed when selecting the employee who does not have the valid rate profile",
								"\"Please define a valid Rate Profile for the selected employee ..\" error message is displayed when selecting the employee who does not have the valid rate profile Fail", "fail");
		    	}
		    	
		    	WaitElement(btn_EmployeeDeleteBtnA);
		    	if (isDisplayed(btn_EmployeeDeleteBtnA)) {
						writeTestResults("Verify that \"Delete\" button is available in the \"Employee\" field", 
								"\"Delete\" button is available in the \"Employee\" field",
							"\"Delete\" button is available in the \"Employee\" field Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"Delete\" button is available in the \"Employee\" field", 
								"\"Delete\" button is available in the \"Employee\" field",
								"\"Delete\" button is available in the \"Employee\" field Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_EmployeeDeleteBtnA);
		    	click(btn_EmployeeDeleteBtnA);
		    	
		    	WaitElement(txt_EmployeeTextFieldDWA);
		    	String EmployeeTextFieldDW = getText(txt_EmployeeTextFieldDWA);
		    	
		    	if (EmployeeTextFieldDW.isEmpty()) {
						writeTestResults("Verify that user able to delete the added employee from the field", 
								"user able to delete the added employee from the field",
							"user able to delete the added employee from the field Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to delete the added employee from the field", 
								"user able to delete the added employee from the field",
								"user able to delete the added employee from the field Fail", "fail");
		    	}
		    }
		    
		    //Service_DWS_029to041
		    public void VerifyThatPlusIconIsAvailbleInTheEmployeeField() throws Exception {
		    	
		    	WaitElement(btn_DailyWorkSheetBtnA);
		    	click(btn_DailyWorkSheetBtnA);
		    	
		    	WaitElement(btn_NewDailyWorksheetBtnA);
		    	click(btn_NewDailyWorksheetBtnA);
		    	
		    	WaitElement(btn_DailyWorkSheetServiceBtnA);
		    	click(btn_DailyWorkSheetServiceBtnA);
		    	
		    	if (getText(txt_WorkedHoursServiceNewPageHeaderA).contentEquals("Worked Hours - New")) {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
							"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitElement(txt_AddNewJobHeaderA);
		    	if (isDisplayed(txt_AddNewJobHeaderA)) {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
							"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Fail", "fail");
		    	}
		    	
		    	WaitElement(btn_EmployeeAddPlusBtnA);
		    	if (isDisplayed(btn_EmployeeAddPlusBtnA)) {
						writeTestResults("Verify that \"+\" icon is availble in the Employee field", 
								"\"+\" icon is availble in the Employee field",
							"\"+\" icon is availble in the Employee field Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"+\" icon is availble in the Employee field", 
								"\"+\" icon is availble in the Employee field",
								"\"+\" icon is availble in the Employee field Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_EmployeeAddPlusBtnA);
		    	click(btn_EmployeeAddPlusBtnA);
		    	
		    	WaitElement(txt_EmployeeAddedNewLineA);
		    	if (getText(txt_EmployeeAddedNewLineA).isEmpty()) {
						writeTestResults("Verify that empty line is added to the field when click on the \"+\" icon", 
								"empty line is added to the field when click on the \"+\" icon",
							"empty line is added to the field when click on the \"+\" icon Successfully", "pass");
		    	} else {
						writeTestResults("Verify that empty line is added to the field when click on the \"+\" icon", 
								"empty line is added to the field when click on the \"+\" icon",
								"empty line is added to the field when click on the \"+\" icon Fail", "fail");
		    	}
		  
		    	WaitClick(btn_JobDateA);
		    	click(btn_JobDateA);
		    	
		    	WaitElement(txt_JobDatePickerA);
		    	if (isDisplayed(txt_JobDatePickerA)) {
						writeTestResults("Verify that Date picker is available in the field \"job Date\"", 
								"Date picker is available in the field \"job Date\"",
							"Date picker is available in the field \"job Date\" Successfully", "pass");
		    	} else {
						writeTestResults("Verify that Date picker is available in the field \"job Date\"", 
								"Date picker is available in the field \"job Date\"",
								"Date picker is available in the field \"job Date\" Fail", "fail");
		    	}
		    	
				SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd");
				SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy/MM/dd");
						
				Date currentDate1 = new Date();
				Date currentDate2 = new Date();
				
				String CurrentDate1 = dateFormat1.format(currentDate1).replace("0", "");
				String CurrentDate2 = dateFormat2.format(currentDate2);
				
//				System.out.println(CurrentDate1);
//				System.out.println(CurrentDate2);
				
				WaitClick(sel_JobDateSelA.replace("JobDate", CurrentDate1));
				click(sel_JobDateSelA.replace("JobDate", CurrentDate1));
				
				String JobDate= driver.findElement(By.xpath(btn_JobDateA)).getAttribute("value");
//				System.out.println(JobDate);
		    	
		    	if (JobDate.contentEquals(CurrentDate2)) {
						writeTestResults("Verify that selected date is displayed in the \"job Date\" field", 
								"selected date is displayed in the \"job Date\" field",
							"selected date is displayed in the \"job Date\" field Successfully", "pass");
		    	} else {
						writeTestResults("Verify that selected date is displayed in the \"job Date\" field", 
								"selected date is displayed in the \"job Date\" field",
								"selected date is displayed in the \"job Date\" field Fail", "fail");
		    	}
				
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	
		    	WaitElement(txt_TimeInTimePickerA);
		    	if (isDisplayed(txt_TimeInTimePickerA)) {
						writeTestResults("Verify that Time picker is availble for the field \"Time In\"", 
								"Time picker is availble for the field \"Time In\"",
							"Time picker is availble for the field \"Time In\" Successfully", "pass");
		    	} else {
						writeTestResults("Verify that Time picker is availble for the field \"Time In\"", 
								"Time picker is availble for the field \"Time In\"",
								"Time picker is availble for the field \"Time In\" Fail", "fail");
		    	}
		    	
		    	WaitClick(sel_TimeInSelA);
		    	click(sel_TimeInSelA);
		    	WaitClick(btn_TimeInSelOkA);
		    	click(btn_TimeInSelOkA);
		    	
				String TimeIn= driver.findElement(By.xpath(btn_TimeInA)).getAttribute("value");
				int TimeInVal = Integer.parseInt(TimeIn);
//		    	System.out.println(TimeIn);
		    	
		    	if (TimeIn.contentEquals("01")) {
						writeTestResults("Verify that selected time is displayed in the field", 
								"selected time is displayed in the field",
							"selected time is displayed in the field Successfully", "pass");
		    	} else {
						writeTestResults("Verify that selected time is displayed in the field", 
								"selected time is displayed in the field",
								"selected time is displayed in the field Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	
		    	WaitElement(txt_TimeOutTimePickerA);
		    	if (isDisplayed(txt_TimeOutTimePickerA)) {
						writeTestResults("Verify that Time picker is availble for the field \"Time OUT\"", 
								"Time picker is availble for the field \"Time OUT\"",
							"Time picker is availble for the field \"Time OUT\" Successfully", "pass");
		    	} else {
						writeTestResults("Verify that Time picker is availble for the field \"Time OUT\"", 
								"Time picker is availble for the field \"Time OUT\"",
								"Time picker is availble for the field \"Time OUT\" Fail", "fail");
		    	}
		    	
		    	WaitClick(sel_TimeOutSelA);
		    	click(sel_TimeOutSelA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
				String TimeOut= driver.findElement(By.xpath(btn_TimeOutA)).getAttribute("value");
				int TimeOutVal = Integer.parseInt(TimeOut);
				//System.out.println(TimeOut);
		    	
		    	if (TimeOut.contentEquals("02")) {
						writeTestResults("Verify that selected time is displayed in the field Time OUT", 
								"selected time is displayed in the field Time OUT",
							"selected time is displayed in the field Time OUT Successfully", "pass");
		    	} else {
						writeTestResults("Verify that selected time is displayed in the field Time OUT", 
								"selected time is displayed in the field Time OUT",
								"selected time is displayed in the field Time OUT Fail", "fail");
		    	}
		    	
				int WorkedHoursVal = TimeOutVal - TimeInVal;
				
				String WorkedHours = String.valueOf(WorkedHoursVal);
		    	//System.out.println(WorkedHours);
		    	
		    	String WorkedHoursValue= driver.findElement(By.xpath(txt_WorkedHoursA)).getAttribute("value").replace("0", "");
		    	//System.out.println(WorkedHoursValue);
		    	
		    	String WorkedHoursTextField = getAttribute(txt_WorkedHoursA, "disabled");
		    	
		    	if (WorkedHoursTextField.contentEquals("true")) {
						writeTestResults("Verify that \"Worked Hours\" field is disable from editing", 
								"\"Worked Hours\" field is disable from editing",
							"\"Worked Hours\" field is disable from editing Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"Worked Hours\" field is disable from editing", 
								"\"Worked Hours\" field is disable from editing",
								"\"Worked Hours\" field is disable from editing Fail", "fail");
		    	}
		    	
		    	if (WorkedHoursValue.contentEquals(WorkedHours)) {
						writeTestResults("Verify that \"Worked Hours\" field is auto filled with the time difference of the Time In and Time out", 
								"\"Worked Hours\" field is auto filled with the time difference of the Time In and Time out",
							"\"Worked Hours\" field is auto filled with the time difference of the Time In and Time out Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"Worked Hours\" field is auto filled with the time difference of the Time In and Time out", 
								"\"Worked Hours\" field is auto filled with the time difference of the Time In and Time out",
								"\"Worked Hours\" field is auto filled with the time difference of the Time In and Time out Fail", "fail");
		    	}
		    	
		    }
		    
		    //Service_DWS_042to045
		    public void VerifyThatUserShouldNotBeAbleToUpdateTheJobWithoutHavingTheWorkedHours() throws Exception {
		    	
		    	WaitElement(btn_DailyWorkSheetBtnA);
		    	click(btn_DailyWorkSheetBtnA);
		    	
		    	WaitElement(btn_NewDailyWorksheetBtnA);
		    	click(btn_NewDailyWorksheetBtnA);
		    	
		    	WaitElement(btn_DailyWorkSheetServiceBtnA);
		    	click(btn_DailyWorkSheetServiceBtnA);
		    	
		    	if (getText(txt_WorkedHoursServiceNewPageHeaderA).contentEquals("Worked Hours - New")) {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
							"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitElement(txt_AddNewJobHeaderA);
		    	if (isDisplayed(txt_AddNewJobHeaderA)) {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
							"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Fail", "fail");
		    	}

		    	WaitClick(btn_ServiceJobOrderSearchBtnA);
		    	click(btn_ServiceJobOrderSearchBtnA);

		    	WaitElement(txt_ServiceJobOrderLookupSearchTxtA);
		    	sendKeys(txt_ServiceJobOrderLookupSearchTxtA, ReleseServiceJobOrderNos1A);
		    	
		    	WaitClick(btn_ServiceJobOrderLookupSearchBtnA);
		    	click(btn_ServiceJobOrderLookupSearchBtnA);
		    	
		    	WaitClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));
		    	doubleClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, ReleseEmployeeNames1A);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", ReleseEmployeeNames1A));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", ReleseEmployeeNames1A));
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);
		    	
		    	WaitElement(txt_workedHoursValidatorA);
		    	if (isDisplayed(txt_workedHoursValidatorA)) {
						writeTestResults("Verify that user should not be able to update the job without having the \"Worked Hours\"", 
								"user should not be able to update the job without having the \"Worked Hours\"",
							"user should not be able to update the job without having the \"Worked Hours\" Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user should not be able to update the job without having the \"Worked Hours\"", 
								"user should not be able to update the job without having the \"Worked Hours\"",
								"user should not be able to update the job without having the \"Worked Hours\" Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelA);
		    	click(sel_TimeInSelA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelA);
		    	click(sel_TimeOutSelA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj1 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj1);
		    	
		    	if (isDisplayed(btn_UpdateAndNewDWSBtnA) && isDisplayed(btn_UpdateDWSBtnA) && isDisplayed(btn_CloseDWSBtnA)) {
						writeTestResults("Verify that Following buttons are available in the \"Add Now Job\" look up Update,Update and New,Cancel Buttons", 
								"Following buttons are available in the \"Add Now Job\" look up Update,Update and New,Cancel Buttons",
							"Following buttons are available in the \"Add Now Job\" look up Update,Update and New,Cancel Buttons Successfully", "pass");
		    	} else {
						writeTestResults("Verify that Following buttons are available in the \"Add Now Job\" look up Update,Update and New,Cancel Buttons", 
								"Following buttons are available in the \"Add Now Job\" look up Update,Update and New,Cancel Buttons",
								"Following buttons are available in the \"Add Now Job\" look up Update,Update and New,Cancel Buttons Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);
		    	
		    	WaitElement(txt_ReferenceNoDWSTextA);
		    	String ReferenceNoDWSTextA= driver.findElement(By.xpath(txt_ReferenceNoDWSTextA)).getAttribute("value");
		    	String EmployeeNameDWSTextA= driver.findElement(By.xpath(txt_EmployeeNameDWSTextA)).getAttribute("value");
		    	String JobNoDWSTextA= driver.findElement(By.xpath(txt_JobNoDWSTextA)).getAttribute("value");
		    	
		    	if (ReferenceNoDWSTextA.contentEquals("REF"+Obj1)) {
						writeTestResults("Verify that user able to add the reference No for the job", 
								"user able to add the reference No for the job",
							"user able to add the reference No for the job Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to add the reference No for the job", 
								"user able to add the reference No for the job",
								"user able to add the reference No for the job Fail", "fail");
		    	}
		    	
		    	if (EmployeeNameDWSTextA.contentEquals(ReleseEmployeeNames1A) && JobNoDWSTextA.contentEquals(ReleseServiceJobOrderNos1A)) {
						writeTestResults("Verify that user able to add a New Job", 
								"user able to add a New Job",
							"user able to add a New Job Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to add a New Job", 
								"user able to add a New Job",
								"user able to add a New Job Fail", "fail");
		    	}
		    }
		    
		    //Service_DWS_046to047
		    public void VerifyThatUpdateAndNewFunctionOfTheAddNowJobLookUp() throws Exception {
		    	
		    	WaitElement(btn_DailyWorkSheetBtnA);
		    	click(btn_DailyWorkSheetBtnA);
		    	
		    	WaitElement(btn_NewDailyWorksheetBtnA);
		    	click(btn_NewDailyWorksheetBtnA);
		    	
		    	WaitElement(btn_DailyWorkSheetServiceBtnA);
		    	click(btn_DailyWorkSheetServiceBtnA);
		    	
		    	if (getText(txt_WorkedHoursServiceNewPageHeaderA).contentEquals("Worked Hours - New")) {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
							"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitElement(txt_AddNewJobHeaderA);
		    	if (isDisplayed(txt_AddNewJobHeaderA)) {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
							"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Fail", "fail");
		    	}

		    	WaitClick(btn_ServiceJobOrderSearchBtnA);
		    	click(btn_ServiceJobOrderSearchBtnA);

		    	WaitElement(txt_ServiceJobOrderLookupSearchTxtA);
		    	sendKeys(txt_ServiceJobOrderLookupSearchTxtA, ReleseServiceJobOrderNos1A);
		    	
		    	WaitClick(btn_ServiceJobOrderLookupSearchBtnA);
		    	click(btn_ServiceJobOrderLookupSearchBtnA);
		    	
		    	WaitClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));
		    	doubleClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, ReleseEmployeeNames1A);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", ReleseEmployeeNames1A));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", ReleseEmployeeNames1A));
		    	
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelA);
		    	click(sel_TimeInSelA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelA);
		    	click(sel_TimeOutSelA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj1 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj1);
		    	
		    	WaitClick(btn_UpdateAndNewDWSBtnA);
		    	click(btn_UpdateAndNewDWSBtnA);
		    	
		    	String EmployeeNameDWSTextA= driver.findElement(By.xpath(txt_EmployeeNameDWSTextA)).getAttribute("value");
		    	String JobNoDWSTextA= driver.findElement(By.xpath(txt_JobNoDWSTextA)).getAttribute("value");
		    	
		    	if (EmployeeNameDWSTextA.contentEquals(ReleseEmployeeNames1A) && JobNoDWSTextA.contentEquals(ReleseServiceJobOrderNos1A) 
		    			&& isDisplayed(txt_AddNewJobHeaderA)) {
						writeTestResults("Verify that \"Update and New\" function of the \"Add Now Job\" Look up", 
								"new row is added to the form and \"Add new job\" look up is still displayed the filled details",
							"new row is added to the form and \"Add new job\" look up is still displayed the filled details Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"Update and New\" function of the \"Add Now Job\" Look up", 
								"new row is added to the form and \"Add new job\" look up is still displayed the filled details",
								"new row is added to the form and \"Add new job\" look up is still displayed the filled details Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_CloseDWSBtnA);
		    	click(btn_CloseDWSBtnA);
		    	if (!isDisplayed(txt_AddNewJobHeaderA)) {
						writeTestResults("Verify that \"Add New Job\" look up is closed when click on the \"Close\" button", 
								"\"Add New Job\" look up is closed when click on the \"Close\" button",
							"\"Add New Job\" look up is closed when click on the \"Close\" button Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"Add New Job\" look up is closed when click on the \"Close\" button", 
								"\"Add New Job\" look up is closed when click on the \"Close\" button",
								"\"Add New Job\" look up is closed when click on the \"Close\" button Fail", "fail");
		    	}
		    	
		    }
		    
		    //Service_DWS_048to050
		    public void VerifyThatDuplicateEmployeeValidationMessageIsDisplayedWhenSameEmployeeAddedTwise() throws Exception {
		    	
		    	//========================================================================================================Service_DWS_048
		    	WaitElement(btn_DailyWorkSheetBtnA);
		    	click(btn_DailyWorkSheetBtnA);
		    	
		    	WaitElement(btn_NewDailyWorksheetBtnA);
		    	click(btn_NewDailyWorksheetBtnA);
		    	
		    	WaitElement(btn_DailyWorkSheetServiceBtnA);
		    	click(btn_DailyWorkSheetServiceBtnA);
		    	
		    	if (getText(txt_WorkedHoursServiceNewPageHeaderA).contentEquals("Worked Hours - New")) {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
							"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitElement(txt_AddNewJobHeaderA);
		    	if (isDisplayed(txt_AddNewJobHeaderA)) {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
							"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Fail", "fail");
		    	}

		    	WaitClick(btn_ServiceJobOrderSearchBtnA);
		    	click(btn_ServiceJobOrderSearchBtnA);

		    	WaitElement(txt_ServiceJobOrderLookupSearchTxtA);
		    	sendKeys(txt_ServiceJobOrderLookupSearchTxtA, ReleseServiceJobOrderNos1A);
		    	
		    	WaitClick(btn_ServiceJobOrderLookupSearchBtnA);
		    	click(btn_ServiceJobOrderLookupSearchBtnA);
		    	
		    	WaitClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));
		    	doubleClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, ReleseEmployeeNames1A);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", ReleseEmployeeNames1A));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", ReleseEmployeeNames1A));
		    	
		    	WaitClick(btn_EmployeeAddPlusBtnA);
		    	click(btn_EmployeeAddPlusBtnA);
		    	
		    	WaitClick(btn_EmployeeSearchBtn2A);
		    	click(btn_EmployeeSearchBtn2A);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, ReleseEmployeeNames1A);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", ReleseEmployeeNames1A));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", ReleseEmployeeNames1A));
		    	
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelA);
		    	click(sel_TimeInSelA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelA);
		    	click(sel_TimeOutSelA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj1 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj1);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);
		    	
		    	WaitClick(btn_EmployeeBtnErrorA);
		    	click(btn_EmployeeBtnErrorA);
		    	
		    	WaitElement(txt_EmployeeDuplicateErrorA);
		    	if (isDisplayed(txt_EmployeeDuplicateErrorA)) {
						writeTestResults("Verify that \"Duplicate Employee\" validation message is displayed when same Employee added twise", 
								"\"Duplicate Employee\" validation message is displayed when same Employee added twise",
							"\"Duplicate Employee\" validation message is displayed when same Employee added twise Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"Duplicate Employee\" validation message is displayed when same Employee added twise", 
								"\"Duplicate Employee\" validation message is displayed when same Employee added twise",
								"\"Duplicate Employee\" validation message is displayed when same Employee added twise Fail", "fail");
		    	}
		    	//========================================================================================================Service_DWS_049
		    	pageRefersh();
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);

		    	WaitClick(btn_ServiceJobOrderSearchBtnA);
		    	click(btn_ServiceJobOrderSearchBtnA);

		    	WaitElement(txt_ServiceJobOrderLookupSearchTxtA);
		    	sendKeys(txt_ServiceJobOrderLookupSearchTxtA, ReleseServiceJobOrderNos1A);
		    	
		    	WaitClick(btn_ServiceJobOrderLookupSearchBtnA);
		    	click(btn_ServiceJobOrderLookupSearchBtnA);
		    	
		    	WaitClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));
		    	doubleClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, ReleseEmployeeNames1A);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", ReleseEmployeeNames1A));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", ReleseEmployeeNames1A));

		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelA);
		    	click(sel_TimeInSelA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelA);
		    	click(sel_TimeOutSelA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj2 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj2);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);
		    	//===========================================================================================================
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);

		    	WaitClick(btn_ServiceJobOrderSearchBtnA);
		    	click(btn_ServiceJobOrderSearchBtnA);

		    	WaitElement(txt_ServiceJobOrderLookupSearchTxtA);
		    	sendKeys(txt_ServiceJobOrderLookupSearchTxtA, ReleseServiceJobOrderNos1A);
		    	pressEnter(txt_ServiceJobOrderLookupSearchTxtA);
		    	
		    	WaitClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));
		    	doubleClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, ReleseEmployeeNames1A);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", ReleseEmployeeNames1A));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", ReleseEmployeeNames1A));

		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelA);
		    	click(sel_TimeInSelA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelA);
		    	click(sel_TimeOutSelA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj3 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj3);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);
		    	
		    	WaitClick(btn_EmployeeBtnErrorA);
		    	click(btn_EmployeeBtnErrorA);
		    	
		    	WaitElement(txt_EmployeeAllocatedErrorA);
		    	if (getText(txt_EmployeeAllocatedErrorA).contentEquals("Employee is already allocated for "+ReleseServiceJobOrderNos1A)) {
						writeTestResults("Verify that same time in and out cannot be add to an employee", 
								"same time in and out cannot be add to an employee",
							"same time in and out cannot be add to an employee Successfully", "pass");
		    	} else {
						writeTestResults("Verify that same time in and out cannot be add to an employee", 
								"same time in and out cannot be add to an employee",
								"same time in and out cannot be add to an employee Fail", "fail");
		    	}

		    	//========================================================================================================Service_DWS_050
		    	pageRefersh();
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);

		    	WaitClick(btn_ServiceJobOrderSearchBtnA);
		    	click(btn_ServiceJobOrderSearchBtnA);

		    	WaitElement(txt_ServiceJobOrderLookupSearchTxtA);
		    	sendKeys(txt_ServiceJobOrderLookupSearchTxtA, ReleseServiceJobOrderNos1A);
		    	
		    	WaitClick(btn_ServiceJobOrderLookupSearchBtnA);
		    	click(btn_ServiceJobOrderLookupSearchBtnA);
		    	
		    	WaitClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));
		    	doubleClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, ReleseEmployeeNames1A);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", ReleseEmployeeNames1A));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", ReleseEmployeeNames1A));

		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelA);
		    	click(sel_TimeInSelA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelA);
		    	click(sel_TimeOutSelA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj4 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj4);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);
		    	//===========================================================================================================
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);

		    	WaitClick(btn_ServiceJobOrderSearchBtnA);
		    	click(btn_ServiceJobOrderSearchBtnA);

		    	WaitElement(txt_ServiceJobOrderLookupSearchTxtA);
		    	sendKeys(txt_ServiceJobOrderLookupSearchTxtA, ReleseServiceJobOrderNos2A);
		    	pressEnter(txt_ServiceJobOrderLookupSearchTxtA);
		    	
		    	WaitClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos2A));
		    	doubleClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos2A));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, ReleseEmployeeNames1A);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", ReleseEmployeeNames1A));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", ReleseEmployeeNames1A));

		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelA);
		    	click(sel_TimeInSelA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelA);
		    	click(sel_TimeOutSelA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj5 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj5);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);
		    	
		    	WaitClick(btn_EmployeeBtnErrorA);
		    	click(btn_EmployeeBtnErrorA);
		    	
		    	WaitElement(txt_EmployeeAllocatedErrorA);
		    	if (getText(txt_EmployeeAllocatedErrorA).contentEquals("Employee is already allocated for "+ReleseServiceJobOrderNos1A)) {
						writeTestResults("Verify that same employee cannot be added to the 2 different job orders when  timeIn and time out are same", 
								"same employee cannot be added to the 2 different job orders when  timeIn and time out are same",
							"same employee cannot be added to the 2 different job orders when  timeIn and time out are same Successfully", "pass");
		    	} else {
						writeTestResults("Verify that same employee cannot be added to the 2 different job orders when  timeIn and time out are same", 
								"same employee cannot be added to the 2 different job orders when  timeIn and time out are same",
								"same employee cannot be added to the 2 different job orders when  timeIn and time out are same Fail", "fail");
		    	}
		    }
		    
		    //Service_DWS_051to053
		    public void VerifyThatSameEmployeeCanBeAddedToTwoDifferentJobOrdersWhenTimeInAndTimeOutAreDifferent() throws Exception {
		    	
		    	WaitElement(btn_DailyWorkSheetBtnA);
		    	click(btn_DailyWorkSheetBtnA);
		    	
		    	WaitElement(btn_NewDailyWorksheetBtnA);
		    	click(btn_NewDailyWorksheetBtnA);
		    	
		    	WaitElement(btn_DailyWorkSheetServiceBtnA);
		    	click(btn_DailyWorkSheetServiceBtnA);
		    	
		    	if (getText(txt_WorkedHoursServiceNewPageHeaderA).contentEquals("Worked Hours - New")) {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
							"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitElement(txt_AddNewJobHeaderA);
		    	if (isDisplayed(txt_AddNewJobHeaderA)) {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
							"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Fail", "fail");
		    	}
		    	//=======================================================================================================================
		    	
		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);
		    	
		    	WaitClick(btn_NewEmployeeBtnA);
		    	click(btn_NewEmployeeBtnA);
		    	
		    	switchWindow();
		    	
		    	LocalTime emp = LocalTime.now();
		    	
		    	sendKeys(txt_EmployeeCodeA, ""+emp);
		    	sendKeys(txt_EmployeeNoA, ""+emp);
		    	sendKeys(txt_FullNameA, "EMP");
		    	sendKeys(txt_NameWithInitialsA, "EMP");
		    	selectText(txt_EmployeeGroupA, "Test Service");
		    	
		    	WaitClick(btn_RateProfileSearchBtnA);
		    	click(btn_RateProfileSearchBtnA);
		    	WaitElement(txt_RateProfileSearchTxtA);
		    	sendKeys(txt_RateProfileSearchTxtA, "Project");
		    	pressEnter(txt_RateProfileSearchTxtA);
		    	WaitClick(sel_RateProfileSearchSelA);
		    	doubleClick(sel_RateProfileSearchSelA);
		    	
		    	WaitClick(btn_WorkingCalendarSearchBtnA);
		    	click(btn_WorkingCalendarSearchBtnA);
		    	WaitElement(txt_WorkingCalendarSearchTxtA);
		    	sendKeys(txt_WorkingCalendarSearchTxtA, "General Calender");
		    	pressEnter(txt_WorkingCalendarSearchTxtA);
		    	WaitClick(sel_WorkingCalendarSearchSelA);
		    	doubleClick(sel_WorkingCalendarSearchSelA);
		    
		    	WaitClick(btn_PayableAccountTickA);
		    	click(btn_PayableAccountTickA);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				String EMP = emp+" [EMP]";
				
				closeWindow();
		    	
		    	//=======================================================================================================================
				
				pageRefersh();
				
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);

		    	WaitClick(btn_ServiceJobOrderSearchBtnA);
		    	click(btn_ServiceJobOrderSearchBtnA);

		    	WaitElement(txt_ServiceJobOrderLookupSearchTxtA);
		    	sendKeys(txt_ServiceJobOrderLookupSearchTxtA, ReleseServiceJobOrderNos1A);
		    	
		    	WaitClick(btn_ServiceJobOrderLookupSearchBtnA);
		    	click(btn_ServiceJobOrderLookupSearchBtnA);
		    	
		    	WaitClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));
		    	doubleClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP));

		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelA);
		    	click(sel_TimeInSelA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelA);
		    	click(sel_TimeOutSelA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj1 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj1);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);
		    	
		    	//===================================================================================
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitClick(btn_ServiceJobOrderSearchBtnA);
		    	click(btn_ServiceJobOrderSearchBtnA);

		    	WaitElement(txt_ServiceJobOrderLookupSearchTxtA);
		    	sendKeys(txt_ServiceJobOrderLookupSearchTxtA, ReleseServiceJobOrderNos2A);
		    	pressEnter(txt_ServiceJobOrderLookupSearchTxtA);
		    	
		    	WaitClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos2A));
		    	doubleClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos2A));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP));
		    	
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelNewA);
		    	click(sel_TimeInSelNewA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelNewA);
		    	click(sel_TimeOutSelNewA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj2 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj2);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);
		    	
		    	WaitElement(txt_EmployeeNameDWSTextA);
		    	String EmployeeNameDWSTextA= driver.findElement(By.xpath(txt_EmployeeNameDWSTextA)).getAttribute("value");
		    	String JobNoDWSTextA= driver.findElement(By.xpath(txt_JobNoDWSTextA)).getAttribute("value");
		    	String EmployeeNameDWSTextNewA= driver.findElement(By.xpath(txt_EmployeeNameDWSTextNewA)).getAttribute("value");
		    	String JobNoDWSTextNewA= driver.findElement(By.xpath(txt_JobNoDWSTextNewA)).getAttribute("value");
		    	
		    	
		    	if (EmployeeNameDWSTextA.contentEquals(EMP) && EmployeeNameDWSTextNewA.contentEquals(EMP) 
		    			&& JobNoDWSTextA.contentEquals(ReleseServiceJobOrderNos1A) && JobNoDWSTextNewA.contentEquals(ReleseServiceJobOrderNos2A)) {
						writeTestResults("Verify that same employee can be added to 2 different job orders when  timeIn and Time out are different", 
								"same employee can be added to 2 different job orders when  timeIn and Time out are different",
							"same employee can be added to 2 different job orders when  timeIn and Time out are different Successfully", "pass");
		    	} else {
						writeTestResults("Verify that same employee can be added to 2 different job orders when  timeIn and Time out are different", 
								"same employee can be added to 2 different job orders when  timeIn and Time out are different",
								"same employee can be added to 2 different job orders when  timeIn and Time out are different Fail", "fail");
		    	}
		    	//===========================================================================
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that able to draft the work sheet which having 2 different job orders with same employee but different worked hours", 
								"User Can draft the work sheet which having 2 different job orders with same employee but different worked hours",
								"User Can draft the work sheet which having 2 different job orders with same employee but different worked hours Successfully", "pass");
				} else {
						writeTestResults("Verify that able to draft the work sheet which having 2 different job orders with same employee but different worked hours", 
								"User Can draft the work sheet which having 2 different job orders with same employee but different worked hours",
								"User Can draft the work sheet which having 2 different job orders with same employee but different worked hours Fail", "fail");
				}
				
				//=====================================================================================
				
				WaitClick(btn_ReleaseA);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				WaitElement(txt_PageRelease2A);
				if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that able to Release the work sheet which having 2 different job orders with same employee but different worked hours", 
								"User Can Release the work sheet which having 2 different job orders with same employee but different worked hours",
							"User Can Release the work sheet which having 2 different job orders with same employee but different worked hours Successfully", "pass");
				} else {
						writeTestResults("Verify that able to Release the work sheet which having 2 different job orders with same employee but different worked hours",
								"User Can Release the work sheet which having 2 different job orders with same employee but different worked hours",
								"User Can Release the work sheet which having 2 different job orders with same employee but different worked hours Fail", "fail");
				}
		    }
		    
		    //Service_DWS_054to056
		    public void VerifyThatSameEmployeeCanBeAddTwiseToTheSameServiceJobOrderWithDifferentTimeInAndTimeOut() throws Exception {
		    	
		    	WaitElement(btn_DailyWorkSheetBtnA);
		    	click(btn_DailyWorkSheetBtnA);
		    	
		    	WaitElement(btn_NewDailyWorksheetBtnA);
		    	click(btn_NewDailyWorksheetBtnA);
		    	
		    	WaitElement(btn_DailyWorkSheetServiceBtnA);
		    	click(btn_DailyWorkSheetServiceBtnA);
		    	
		    	if (getText(txt_WorkedHoursServiceNewPageHeaderA).contentEquals("Worked Hours - New")) {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
							"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitElement(txt_AddNewJobHeaderA);
		    	if (isDisplayed(txt_AddNewJobHeaderA)) {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
							"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Fail", "fail");
		    	}
		    	//=======================================================================================================================
		    	
		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);
		    	
		    	WaitClick(btn_NewEmployeeBtnA);
		    	click(btn_NewEmployeeBtnA);
		    	
		    	switchWindow();
		    	
		    	LocalTime emp = LocalTime.now();
		    	
		    	sendKeys(txt_EmployeeCodeA, ""+emp);
		    	sendKeys(txt_EmployeeNoA, ""+emp);
		    	sendKeys(txt_FullNameA, "EMP");
		    	sendKeys(txt_NameWithInitialsA, "EMP");
		    	selectText(txt_EmployeeGroupA, "Test Service");
		    	
		    	WaitClick(btn_RateProfileSearchBtnA);
		    	click(btn_RateProfileSearchBtnA);
		    	WaitElement(txt_RateProfileSearchTxtA);
		    	sendKeys(txt_RateProfileSearchTxtA, "Project");
		    	pressEnter(txt_RateProfileSearchTxtA);
		    	WaitClick(sel_RateProfileSearchSelA);
		    	doubleClick(sel_RateProfileSearchSelA);
		    	
		    	WaitClick(btn_WorkingCalendarSearchBtnA);
		    	click(btn_WorkingCalendarSearchBtnA);
		    	WaitElement(txt_WorkingCalendarSearchTxtA);
		    	sendKeys(txt_WorkingCalendarSearchTxtA, "General Calender");
		    	pressEnter(txt_WorkingCalendarSearchTxtA);
		    	WaitClick(sel_WorkingCalendarSearchSelA);
		    	doubleClick(sel_WorkingCalendarSearchSelA);
		    
		    	WaitClick(btn_PayableAccountTickA);
		    	click(btn_PayableAccountTickA);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				String EMP = emp+" [EMP]";
				
				closeWindow();
		    	
		    	//=======================================================================================================================
				
				pageRefersh();
				
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);

		    	WaitClick(btn_ServiceJobOrderSearchBtnA);
		    	click(btn_ServiceJobOrderSearchBtnA);

		    	WaitElement(txt_ServiceJobOrderLookupSearchTxtA);
		    	sendKeys(txt_ServiceJobOrderLookupSearchTxtA, ReleseServiceJobOrderNos1A);
		    	
		    	WaitClick(btn_ServiceJobOrderLookupSearchBtnA);
		    	click(btn_ServiceJobOrderLookupSearchBtnA);
		    	
		    	WaitClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));
		    	doubleClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP));

		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelA);
		    	click(sel_TimeInSelA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelA);
		    	click(sel_TimeOutSelA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj1 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj1);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);
		    	
		    	//===================================================================================
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitClick(btn_ServiceJobOrderSearchBtnA);
		    	click(btn_ServiceJobOrderSearchBtnA);

		    	WaitElement(txt_ServiceJobOrderLookupSearchTxtA);
		    	sendKeys(txt_ServiceJobOrderLookupSearchTxtA, ReleseServiceJobOrderNos1A);
		    	pressEnter(txt_ServiceJobOrderLookupSearchTxtA);
		    	
		    	WaitClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));
		    	doubleClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP));
		    	
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelNewA);
		    	click(sel_TimeInSelNewA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelNewA);
		    	click(sel_TimeOutSelNewA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj2 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj2);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);
		    	
		    	WaitElement(txt_EmployeeNameDWSTextA);
		    	String EmployeeNameDWSTextA= driver.findElement(By.xpath(txt_EmployeeNameDWSTextA)).getAttribute("value");
		    	String JobNoDWSTextA= driver.findElement(By.xpath(txt_JobNoDWSTextA)).getAttribute("value");
		    	String EmployeeNameDWSTextNewA= driver.findElement(By.xpath(txt_EmployeeNameDWSTextNewA)).getAttribute("value");
		    	String JobNoDWSTextNewA= driver.findElement(By.xpath(txt_JobNoDWSTextNewA)).getAttribute("value");
		    	
		    	
		    	if (EmployeeNameDWSTextA.contentEquals(EMP) && EmployeeNameDWSTextNewA.contentEquals(EMP) 
		    			&& JobNoDWSTextA.contentEquals(ReleseServiceJobOrderNos1A) && JobNoDWSTextNewA.contentEquals(ReleseServiceJobOrderNos1A)) {
						writeTestResults("Verify that same employee can be add twise to the same service job order with different \"Time In\" and \"Time out\"", 
								"same employee can be add twise to the same service job order with different \"Time In\" and \"Time out\"",
							"same employee can be add twise to the same service job order with different \"Time In\" and \"Time out\" Successfully", "pass");
		    	} else {
						writeTestResults("Verify that same employee can be add twise to the same service job order with different \"Time In\" and \"Time out\"", 
								"same employee can be add twise to the same service job order with different \"Time In\" and \"Time out\"",
								"same employee can be add twise to the same service job order with different \"Time In\" and \"Time out\" Fail", "fail");
		    	}
		    	//===========================================================================
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that User able to draft the daily work sheet which having same employee twise in a job order with different worked hours", 
								"User Can draft the daily work sheet which having same employee twise in a job order with different worked hours",
								"User Can draft the daily work sheet which having same employee twise in a job order with different worked hours Successfully", "pass");
				} else {
						writeTestResults("Verify that User able to draft the daily work sheet which having same employee twise in a job order with different worked hours", 
								"User Can draft the daily work sheet which having same employee twise in a job order with different worked hours",
								"User Can draft the daily work sheet which having same employee twise in a job order with different worked hours Fail", "fail");
				}
				
				//=====================================================================================
				
				WaitClick(btn_ReleaseA);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				WaitElement(txt_PageRelease2A);
				if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that User able to release the daily work sheet which having same employee twise in a job order with different worked hours", 
								"User Can release the daily work sheet which having same employee twise in a job order with different worked hours",
							"User Can release the daily work sheet which having same employee twise in a job order with different worked hours Successfully", "pass");
				} else {
						writeTestResults("Verify that User able to release the daily work sheet which having same employee twise in a job order with different worked hours",
								"User Can release the daily work sheet which having same employee twise in a job order with different worked hours",
								"User Can release the daily work sheet which having same employee twise in a job order with different worked hours Fail", "fail");
				}
		    }
		    
		    //Service_DWS_057to059
		    public void VerifyThatAbleToAddTheDifferentEmployeesForaServiceJobOrderWithTheSameTimeInAndTimeOut() throws Exception {
		    	
		    	WaitElement(btn_DailyWorkSheetBtnA);
		    	click(btn_DailyWorkSheetBtnA);
		    	
		    	WaitElement(btn_NewDailyWorksheetBtnA);
		    	click(btn_NewDailyWorksheetBtnA);
		    	
		    	WaitElement(btn_DailyWorkSheetServiceBtnA);
		    	click(btn_DailyWorkSheetServiceBtnA);
		    	
		    	if (getText(txt_WorkedHoursServiceNewPageHeaderA).contentEquals("Worked Hours - New")) {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
							"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitElement(txt_AddNewJobHeaderA);
		    	if (isDisplayed(txt_AddNewJobHeaderA)) {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
							"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Fail", "fail");
		    	}
		    	//=======================================================================================================================
		    	
		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);
		    	
		    	WaitClick(btn_NewEmployeeBtnA);
		    	click(btn_NewEmployeeBtnA);
		    	
		    	switchWindow();
		    	
		    	LocalTime emp1 = LocalTime.now();
		    	
		    	sendKeys(txt_EmployeeCodeA, ""+emp1);
		    	sendKeys(txt_EmployeeNoA, ""+emp1);
		    	sendKeys(txt_FullNameA, "EMP");
		    	sendKeys(txt_NameWithInitialsA, "EMP");
		    	selectText(txt_EmployeeGroupA, "Test Service");
		    	
		    	WaitClick(btn_RateProfileSearchBtnA);
		    	click(btn_RateProfileSearchBtnA);
		    	WaitElement(txt_RateProfileSearchTxtA);
		    	sendKeys(txt_RateProfileSearchTxtA, "Project");
		    	pressEnter(txt_RateProfileSearchTxtA);
		    	WaitClick(sel_RateProfileSearchSelA);
		    	doubleClick(sel_RateProfileSearchSelA);
		    	
		    	WaitClick(btn_WorkingCalendarSearchBtnA);
		    	click(btn_WorkingCalendarSearchBtnA);
		    	WaitElement(txt_WorkingCalendarSearchTxtA);
		    	sendKeys(txt_WorkingCalendarSearchTxtA, "General Calender");
		    	pressEnter(txt_WorkingCalendarSearchTxtA);
		    	WaitClick(sel_WorkingCalendarSearchSelA);
		    	doubleClick(sel_WorkingCalendarSearchSelA);
		    
		    	WaitClick(btn_PayableAccountTickA);
		    	click(btn_PayableAccountTickA);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				String EMP1 = emp1+" [EMP]";
				
				WaitClick(btn_DuplicateA);
				click(btn_DuplicateA);
				
				WaitElement(txt_EmployeeCodeA);
		    	LocalTime emp2 = LocalTime.now();
		    	
		    	sendKeys(txt_EmployeeCodeA, ""+emp2);
		    	sendKeys(txt_EmployeeNoA, ""+emp2);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				String EMP2 = emp2+" [EMP]";
				
				closeWindow();
		    	
		    	//=======================================================================================================================
				
				pageRefersh();
				
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);

		    	WaitClick(btn_ServiceJobOrderSearchBtnA);
		    	click(btn_ServiceJobOrderSearchBtnA);

		    	WaitElement(txt_ServiceJobOrderLookupSearchTxtA);
		    	sendKeys(txt_ServiceJobOrderLookupSearchTxtA, ReleseServiceJobOrderNos1A);
		    	
		    	WaitClick(btn_ServiceJobOrderLookupSearchBtnA);
		    	click(btn_ServiceJobOrderLookupSearchBtnA);
		    	
		    	WaitClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));
		    	doubleClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));

//		    	WaitClick(btn_EmployeeSearchBtnA);
//		    	click(btn_EmployeeSearchBtnA);
//		    	WaitElement(txt_EmployeeLookupSearchTxtA);
//		    	pressEnter(txt_EmployeeLookupSearchTxtA);
//		    	
//		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP1));
//		    	click(txt_ReleseEmployeeNamesA.replace("EName", EMP1));
//		    	
//		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP2));
//		    	click(txt_ReleseEmployeeNamesA.replace("EName", EMP2));
//		    	WaitClick(txt_ReleseEmployeeNamesA.replace("EName", EMP2));
//		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP2));
		    	
		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP1);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP1));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP1));
		    	
		    	WaitClick(btn_EmployeeAddPlusBtnA);
		    	click(btn_EmployeeAddPlusBtnA);
		    	
		    	WaitClick(btn_EmployeeSearchBtn2A);
		    	click(btn_EmployeeSearchBtn2A);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP2);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP2));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP2));

		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelA);
		    	click(sel_TimeInSelA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelA);
		    	click(sel_TimeOutSelA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj1 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj1);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);
		    	
		    	WaitElement(txt_EmployeeNameDWSTextA);
		    	String EmployeeNameDWSTextA= driver.findElement(By.xpath(txt_EmployeeNameDWSTextA)).getAttribute("value");
		    	String JobNoDWSTextA= driver.findElement(By.xpath(txt_JobNoDWSTextA)).getAttribute("value");
		    	String EmployeeNameDWSTextNewA= driver.findElement(By.xpath(txt_EmployeeNameDWSTextNewA)).getAttribute("value");
		    	String JobNoDWSTextNewA= driver.findElement(By.xpath(txt_JobNoDWSTextNewA)).getAttribute("value");
		    	
		    	
		    	if (EmployeeNameDWSTextA.contentEquals(EMP1) && EmployeeNameDWSTextNewA.contentEquals(EMP2) 
		    			&& JobNoDWSTextA.contentEquals(ReleseServiceJobOrderNos1A) && JobNoDWSTextNewA.contentEquals(ReleseServiceJobOrderNos1A)) {
						writeTestResults("Verify that able to add the different employees for a service job order with the same \"Time In\" and \"Time out\"", 
								"add the different employees for a service job order with the same \"Time In\" and \"Time out\"",
							"add the different employees for a service job order with the same \"Time In\" and \"Time out\" Successfully", "pass");
		    	} else {
						writeTestResults("Verify that able to add the different employees for a service job order with the same \"Time In\" and \"Time out\"", 
								"add the different employees for a service job order with the same \"Time In\" and \"Time out\"",
								"add the different employees for a service job order with the same \"Time In\" and \"Time out\" Fail", "fail");
		    	}
		    	//===========================================================================
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that User able to draft the daily work sheet which having different employees in a job order with same worked hours", 
								"User Can draft the daily work sheet which having different employees in a job order with same worked hours",
								"User Can draft the daily work sheet which having different employees in a job order with same worked hours Successfully", "pass");
				} else {
						writeTestResults("Verify that User able to draft the daily work sheet which having different employees in a job order with same worked hours", 
								"User Can draft the daily work sheet which having different employees in a job order with same worked hours",
								"User Can draft the daily work sheet which having different employees in a job order with same worked hours Fail", "fail");
				}
				
				//=====================================================================================
				
				WaitClick(btn_ReleaseA);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				WaitElement(txt_PageRelease2A);
				if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that User able to release the daily work sheet which having different employees in a job order with same worked hours", 
								"User Can release the daily work sheet which having different employees in a job order with same worked hours",
							"User Can release the daily work sheet which having different employees in a job order with same worked hours Successfully", "pass");
				} else {
						writeTestResults("Verify that User able to release the daily work sheet which having different employees in a job order with same worked hours",
								"User Can release the daily work sheet which having different employees in a job order with same worked hours",
								"User Can release the daily work sheet which having different employees in a job order with same worked hours Fail", "fail");
				}
		    }
		    
		    //Service_DWS_060to062
		    public void VerifyThatUserAbleToAddDifferentEmployeesForaServiceJobOrderWithDifferentTimeInAndTimeOut() throws Exception {
		    	
		    	WaitElement(btn_DailyWorkSheetBtnA);
		    	click(btn_DailyWorkSheetBtnA);
		    	
		    	WaitElement(btn_NewDailyWorksheetBtnA);
		    	click(btn_NewDailyWorksheetBtnA);
		    	
		    	WaitElement(btn_DailyWorkSheetServiceBtnA);
		    	click(btn_DailyWorkSheetServiceBtnA);
		    	
		    	if (getText(txt_WorkedHoursServiceNewPageHeaderA).contentEquals("Worked Hours - New")) {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
							"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitElement(txt_AddNewJobHeaderA);
		    	if (isDisplayed(txt_AddNewJobHeaderA)) {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
							"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Fail", "fail");
		    	}
		    	//=======================================================================================================================
		    	
		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);
		    	
		    	WaitClick(btn_NewEmployeeBtnA);
		    	click(btn_NewEmployeeBtnA);
		    	
		    	switchWindow();
		    	
		    	LocalTime emp1 = LocalTime.now();
		    	
		    	sendKeys(txt_EmployeeCodeA, ""+emp1);
		    	sendKeys(txt_EmployeeNoA, ""+emp1);
		    	sendKeys(txt_FullNameA, "EMP");
		    	sendKeys(txt_NameWithInitialsA, "EMP");
		    	selectText(txt_EmployeeGroupA, "Test Service");
		    	
		    	WaitClick(btn_RateProfileSearchBtnA);
		    	click(btn_RateProfileSearchBtnA);
		    	WaitElement(txt_RateProfileSearchTxtA);
		    	sendKeys(txt_RateProfileSearchTxtA, "Project");
		    	pressEnter(txt_RateProfileSearchTxtA);
		    	WaitClick(sel_RateProfileSearchSelA);
		    	doubleClick(sel_RateProfileSearchSelA);
		    	
		    	WaitClick(btn_WorkingCalendarSearchBtnA);
		    	click(btn_WorkingCalendarSearchBtnA);
		    	WaitElement(txt_WorkingCalendarSearchTxtA);
		    	sendKeys(txt_WorkingCalendarSearchTxtA, "General Calender");
		    	pressEnter(txt_WorkingCalendarSearchTxtA);
		    	WaitClick(sel_WorkingCalendarSearchSelA);
		    	doubleClick(sel_WorkingCalendarSearchSelA);
		    
		    	WaitClick(btn_PayableAccountTickA);
		    	click(btn_PayableAccountTickA);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				String EMP1 = emp1+" [EMP]";
				
				WaitClick(btn_DuplicateA);
				click(btn_DuplicateA);
				
				WaitElement(txt_EmployeeCodeA);
		    	LocalTime emp2 = LocalTime.now();
		    	
		    	sendKeys(txt_EmployeeCodeA, ""+emp2);
		    	sendKeys(txt_EmployeeNoA, ""+emp2);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				String EMP2 = emp2+" [EMP]";
				
				closeWindow();
		    	
		    	//=======================================================================================================================
				
				pageRefersh();
				
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);

		    	WaitClick(btn_ServiceJobOrderSearchBtnA);
		    	click(btn_ServiceJobOrderSearchBtnA);

		    	WaitElement(txt_ServiceJobOrderLookupSearchTxtA);
		    	sendKeys(txt_ServiceJobOrderLookupSearchTxtA, ReleseServiceJobOrderNos1A);
		    	
		    	WaitClick(btn_ServiceJobOrderLookupSearchBtnA);
		    	click(btn_ServiceJobOrderLookupSearchBtnA);
		    	
		    	WaitClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));
		    	doubleClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP1);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP1));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP1));

		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelA);
		    	click(sel_TimeInSelA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelA);
		    	click(sel_TimeOutSelA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj1 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj1);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);
		    	
		    	//===================================================================================
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitClick(btn_ServiceJobOrderSearchBtnA);
		    	click(btn_ServiceJobOrderSearchBtnA);

		    	WaitElement(txt_ServiceJobOrderLookupSearchTxtA);
		    	sendKeys(txt_ServiceJobOrderLookupSearchTxtA, ReleseServiceJobOrderNos1A);
		    	pressEnter(txt_ServiceJobOrderLookupSearchTxtA);
		    	
		    	WaitClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));
		    	doubleClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP2);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP2));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP2));
		    	
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelNewA);
		    	click(sel_TimeInSelNewA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelNewA);
		    	click(sel_TimeOutSelNewA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj2 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj2);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);
		    	
		    	WaitElement(txt_EmployeeNameDWSTextA);
		    	String EmployeeNameDWSTextA= driver.findElement(By.xpath(txt_EmployeeNameDWSTextA)).getAttribute("value");
		    	String JobNoDWSTextA= driver.findElement(By.xpath(txt_JobNoDWSTextA)).getAttribute("value");
		    	String EmployeeNameDWSTextNewA= driver.findElement(By.xpath(txt_EmployeeNameDWSTextNewA)).getAttribute("value");
		    	String JobNoDWSTextNewA= driver.findElement(By.xpath(txt_JobNoDWSTextNewA)).getAttribute("value");
		    	
		    	
		    	if (EmployeeNameDWSTextA.contentEquals(EMP1) && EmployeeNameDWSTextNewA.contentEquals(EMP2) 
		    			&& JobNoDWSTextA.contentEquals(ReleseServiceJobOrderNos1A) && JobNoDWSTextNewA.contentEquals(ReleseServiceJobOrderNos1A)) {
						writeTestResults("Verify that user able to add different employees for a service job order with different \"Time In\" and \"Time Out\"", 
								"user able to add different employees for a service job order with different \"Time In\" and \"Time Out\"",
							"user able to add different employees for a service job order with different \"Time In\" and \"Time Out\" Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to add different employees for a service job order with different \"Time In\" and \"Time Out\"", 
								"user able to add different employees for a service job order with different \"Time In\" and \"Time Out\"",
								"user able to add different employees for a service job order with different \"Time In\" and \"Time Out\" Fail", "fail");
		    	}
		    	//===========================================================================
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that User able to draft the daily work sheet which having different employees in a job order with different worked hours", 
								"User Can draft the daily work sheet which having different employees in a job order with different worked hours",
								"User Can draft the daily work sheet which having different employees in a job order with different worked hours Successfully", "pass");
				} else {
						writeTestResults("Verify that User able to draft the daily work sheet which having different employees in a job order with different worked hours", 
								"User Can draft the daily work sheet which having different employees in a job order with different worked hours",
								"User Can draft the daily work sheet which having different employees in a job order with different worked hours Fail", "fail");
				}
				
				//=====================================================================================
				
				WaitClick(btn_ReleaseA);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				WaitElement(txt_PageRelease2A);
				if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that User able to release the daily work sheet which having different employees in a job order with different worked hours", 
								"User Can release the daily work sheet which having different employees in a job order with different worked hours",
							"User Can release the daily work sheet which having different employees in a job order with different worked hours Successfully", "pass");
				} else {
						writeTestResults("Verify that User able to release the daily work sheet which having different employees in a job order with different worked hours",
								"User Can release the daily work sheet which having different employees in a job order with different worked hours",
								"User Can release the daily work sheet which having different employees in a job order with different worked hours Fail", "fail");
				}
		    }
		    
		    //Service_DWS_063to065
		    public void VerifyThatUserShouldNotBeAbleToEditTheRecordAfterAddedToWorkSheet() throws Exception {
		    	
		    	WaitElement(btn_DailyWorkSheetBtnA);
		    	click(btn_DailyWorkSheetBtnA);
		    	
		    	WaitElement(btn_NewDailyWorksheetBtnA);
		    	click(btn_NewDailyWorksheetBtnA);
		    	
		    	WaitElement(btn_DailyWorkSheetServiceBtnA);
		    	click(btn_DailyWorkSheetServiceBtnA);
		    	
		    	if (getText(txt_WorkedHoursServiceNewPageHeaderA).contentEquals("Worked Hours - New")) {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
							"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitElement(txt_AddNewJobHeaderA);
		    	if (isDisplayed(txt_AddNewJobHeaderA)) {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
							"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Fail", "fail");
		    	}
		    	//=======================================================================================================================
		    	
		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);
		    	
		    	WaitClick(btn_NewEmployeeBtnA);
		    	click(btn_NewEmployeeBtnA);
		    	
		    	switchWindow();
		    	
		    	LocalTime emp1 = LocalTime.now();
		    	
		    	sendKeys(txt_EmployeeCodeA, ""+emp1);
		    	sendKeys(txt_EmployeeNoA, ""+emp1);
		    	sendKeys(txt_FullNameA, "EMP");
		    	sendKeys(txt_NameWithInitialsA, "EMP");
		    	selectText(txt_EmployeeGroupA, "Test Service");
		    	
		    	WaitClick(btn_RateProfileSearchBtnA);
		    	click(btn_RateProfileSearchBtnA);
		    	WaitElement(txt_RateProfileSearchTxtA);
		    	sendKeys(txt_RateProfileSearchTxtA, "Project");
		    	pressEnter(txt_RateProfileSearchTxtA);
		    	WaitClick(sel_RateProfileSearchSelA);
		    	doubleClick(sel_RateProfileSearchSelA);
		    	
		    	WaitClick(btn_WorkingCalendarSearchBtnA);
		    	click(btn_WorkingCalendarSearchBtnA);
		    	WaitElement(txt_WorkingCalendarSearchTxtA);
		    	sendKeys(txt_WorkingCalendarSearchTxtA, "General Calender");
		    	pressEnter(txt_WorkingCalendarSearchTxtA);
		    	WaitClick(sel_WorkingCalendarSearchSelA);
		    	doubleClick(sel_WorkingCalendarSearchSelA);
		    
		    	WaitClick(btn_PayableAccountTickA);
		    	click(btn_PayableAccountTickA);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				String EMP1 = emp1+" [EMP]";
				
				WaitClick(btn_DuplicateA);
				click(btn_DuplicateA);
				
				WaitElement(txt_EmployeeCodeA);
		    	LocalTime emp2 = LocalTime.now();
		    	
		    	sendKeys(txt_EmployeeCodeA, ""+emp2);
		    	sendKeys(txt_EmployeeNoA, ""+emp2);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				String EMP2 = emp2+" [EMP]";
				
				closeWindow();
		    	
		    	//=======================================================================================================================
				
				pageRefersh();
				
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);

		    	WaitClick(btn_ServiceJobOrderSearchBtnA);
		    	click(btn_ServiceJobOrderSearchBtnA);

		    	WaitElement(txt_ServiceJobOrderLookupSearchTxtA);
		    	sendKeys(txt_ServiceJobOrderLookupSearchTxtA, ReleseServiceJobOrderNos1A);
		    	
		    	WaitClick(btn_ServiceJobOrderLookupSearchBtnA);
		    	click(btn_ServiceJobOrderLookupSearchBtnA);
		    	
		    	WaitClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));
		    	doubleClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP1);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP1));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP1));

		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelA);
		    	click(sel_TimeInSelA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelA);
		    	click(sel_TimeOutSelA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj1 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj1);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);
		    	
		    	//===================================================================================
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitClick(btn_ServiceJobOrderSearchBtnA);
		    	click(btn_ServiceJobOrderSearchBtnA);

		    	WaitElement(txt_ServiceJobOrderLookupSearchTxtA);
		    	sendKeys(txt_ServiceJobOrderLookupSearchTxtA, ReleseServiceJobOrderNos1A);
		    	pressEnter(txt_ServiceJobOrderLookupSearchTxtA);
		    	
		    	WaitClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));
		    	doubleClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP2);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP2));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP2));
		    	
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelNewA);
		    	click(sel_TimeInSelNewA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelNewA);
		    	click(sel_TimeOutSelNewA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj2 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj2);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);

		    	
		    	WaitElement(txt_EmployeeNameDWSTextA);
		    	String EmployeeNameDWSTextA= driver.findElement(By.xpath(txt_EmployeeNameDWSTextA)).getAttribute("value");
		    	String JobNoDWSTextA= driver.findElement(By.xpath(txt_JobNoDWSTextA)).getAttribute("value");
		    	
		    	WaitClick(btn_DeleteRecordRaw2A);
		    	click(btn_DeleteRecordRaw2A);
		    	WaitClick(btn_YesBtnA);
		    	click(btn_YesBtnA);
		    	
				String EmployeeNameTextA = getAttribute(txt_EmployeeNameDWSTextA, "disabled");
				String JobNoTextA = getAttribute(txt_JobNoDWSTextA, "disabled");
				
		    	if (EmployeeNameTextA.contentEquals("true") && JobNoTextA.contentEquals("true")) {
						writeTestResults("Verify that user should not be able to edit the record after added to work sheet", 
								"user should not be able to edit the record after added to work sheet",
							"user should not be able to edit the record after added to work sheet Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user should not be able to edit the record after added to work sheet", 
								"user should not be able to edit the record after added to work sheet",
								"user should not be able to edit the record after added to work sheet Fail", "fail");
		    	}
		    	
		    	if (EmployeeNameDWSTextA.contentEquals(EMP1) && JobNoDWSTextA.contentEquals(ReleseServiceJobOrderNos1A)
		    			&& !isDisplayed(txt_EmployeeNameDWSTextNewA) && !isDisplayed(txt_JobNoDWSTextNewA)) {
						writeTestResults("Verify user should be able to delete the records in the daily work sheet before draft", 
								"user should be able to delete the records in the daily work sheet before draft",
							"user should be able to delete the records in the daily work sheet before draft Successfully", "pass");
		    	} else {
						writeTestResults("Verify user should be able to delete the records in the daily work sheet before draft", 
								"user should be able to delete the records in the daily work sheet before draft",
								"user should be able to delete the records in the daily work sheet before draft Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_DeleteRecordRaw1A);
		    	click(btn_DeleteRecordRaw1A);
		    	WaitClick(btn_YesBtnA);
		    	click(btn_YesBtnA);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
				Thread.sleep(3000);
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("$(closeMessageBox()).click()");
				
		    	WaitClick(btn_RecordErrorBtnA);
		    	click(btn_RecordErrorBtnA);
		    	
		    	if (isDisplayed(txt_EmployeeNameIsRequiredErrorA) && isDisplayed(txt_JobDateIsRequiredErrorA)
		    			 && isDisplayed(txt_JobNoIsRequiredErrorA)) {
						writeTestResults("Verify that User should not be able to draft the daily work sheet without having atleast 1 record", 
								"User should not be able to draft the daily work sheet without having atleast 1 record",
							"User should not be able to draft the daily work sheet without having atleast 1 record Successfully", "pass");
		    	} else {
						writeTestResults("Verify that User should not be able to draft the daily work sheet without having atleast 1 record", 
								"User should not be able to draft the daily work sheet without having atleast 1 record",
								"User should not be able to draft the daily work sheet without having atleast 1 record Fail", "fail");
		    	}
		    	
		    }
		    
		    //Service_DWS_066to069
		    public void VerifyDraftAndNewOptionOfDailyWorkSheetForm() throws Exception {
		    	
		    	WaitElement(btn_DailyWorkSheetBtnA);
		    	click(btn_DailyWorkSheetBtnA);
		    	
		    	WaitElement(btn_NewDailyWorksheetBtnA);
		    	click(btn_NewDailyWorksheetBtnA);
		    	
		    	WaitElement(btn_DailyWorkSheetServiceBtnA);
		    	click(btn_DailyWorkSheetServiceBtnA);
		    	
		    	if (getText(txt_WorkedHoursServiceNewPageHeaderA).contentEquals("Worked Hours - New")) {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
							"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitElement(txt_AddNewJobHeaderA);
		    	if (isDisplayed(txt_AddNewJobHeaderA)) {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
							"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Fail", "fail");
		    	}
		    	//=======================================================================================================================
		    	
		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);
		    	
		    	WaitClick(btn_NewEmployeeBtnA);
		    	click(btn_NewEmployeeBtnA);
		    	
		    	switchWindow();
		    	
		    	LocalTime emp1 = LocalTime.now();
		    	
		    	sendKeys(txt_EmployeeCodeA, ""+emp1);
		    	sendKeys(txt_EmployeeNoA, ""+emp1);
		    	sendKeys(txt_FullNameA, "EMP");
		    	sendKeys(txt_NameWithInitialsA, "EMP");
		    	selectText(txt_EmployeeGroupA, "Test Service");
		    	
		    	WaitClick(btn_RateProfileSearchBtnA);
		    	click(btn_RateProfileSearchBtnA);
		    	WaitElement(txt_RateProfileSearchTxtA);
		    	sendKeys(txt_RateProfileSearchTxtA, "Project");
		    	pressEnter(txt_RateProfileSearchTxtA);
		    	WaitClick(sel_RateProfileSearchSelA);
		    	doubleClick(sel_RateProfileSearchSelA);
		    	
		    	WaitClick(btn_WorkingCalendarSearchBtnA);
		    	click(btn_WorkingCalendarSearchBtnA);
		    	WaitElement(txt_WorkingCalendarSearchTxtA);
		    	sendKeys(txt_WorkingCalendarSearchTxtA, "General Calender");
		    	pressEnter(txt_WorkingCalendarSearchTxtA);
		    	WaitClick(sel_WorkingCalendarSearchSelA);
		    	doubleClick(sel_WorkingCalendarSearchSelA);
		    
		    	WaitClick(btn_PayableAccountTickA);
		    	click(btn_PayableAccountTickA);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				String EMP1 = emp1+" [EMP]";
				
				closeWindow();
		    	
		    	//=======================================================================================================================
				
				pageRefersh();
				
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);

		    	WaitClick(btn_ServiceJobOrderSearchBtnA);
		    	click(btn_ServiceJobOrderSearchBtnA);

		    	WaitElement(txt_ServiceJobOrderLookupSearchTxtA);
		    	sendKeys(txt_ServiceJobOrderLookupSearchTxtA, ReleseServiceJobOrderNos1A);
		    	
		    	WaitClick(btn_ServiceJobOrderLookupSearchBtnA);
		    	click(btn_ServiceJobOrderLookupSearchBtnA);
		    	
		    	WaitClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));
		    	doubleClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP1);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP1));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP1));

		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelA);
		    	click(sel_TimeInSelA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelA);
		    	click(sel_TimeOutSelA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj1 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj1);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);
		    	
		    	WaitClick(btn_DraftAndNewA);
		    	click(btn_DraftAndNewA);
		    	
		    	WaitElement(txt_WorkedHoursServiceNewPageHeaderA);
		    	if (getText(txt_WorkedHoursServiceNewPageHeaderA).contentEquals("Worked Hours - New")) {
						writeTestResults("Verify \"Draft and New\" Option of Daily Work Sheet Form", 
								"\"Draft and New\" Option of Daily Work Sheet Form",
							"\"Draft and New\" Option of Daily Work Sheet Form Successfully", "pass");
		    	} else {
						writeTestResults("Verify \"Draft and New\" Option of Daily Work Sheet Form", 
								"\"Draft and New\" Option of Daily Work Sheet Form",
								"\"Draft and New\" Option of Daily Work Sheet Form Fail", "fail");
		    	}
		    	
		    	//===================================================================================

		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);
		    	
		    	WaitClick(btn_NewEmployeeBtnA);
		    	click(btn_NewEmployeeBtnA);
		    	
		    	switchWindow();
		    	
		    	LocalTime emp2 = LocalTime.now();
		    	
		    	sendKeys(txt_EmployeeCodeA, ""+emp2);
		    	sendKeys(txt_EmployeeNoA, ""+emp2);
		    	sendKeys(txt_FullNameA, "EMP");
		    	sendKeys(txt_NameWithInitialsA, "EMP");
		    	selectText(txt_EmployeeGroupA, "Test Service");
		    	
		    	WaitClick(btn_RateProfileSearchBtnA);
		    	click(btn_RateProfileSearchBtnA);
		    	WaitElement(txt_RateProfileSearchTxtA);
		    	sendKeys(txt_RateProfileSearchTxtA, "Project");
		    	pressEnter(txt_RateProfileSearchTxtA);
		    	WaitClick(sel_RateProfileSearchSelA);
		    	doubleClick(sel_RateProfileSearchSelA);
		    	
		    	WaitClick(btn_WorkingCalendarSearchBtnA);
		    	click(btn_WorkingCalendarSearchBtnA);
		    	WaitElement(txt_WorkingCalendarSearchTxtA);
		    	sendKeys(txt_WorkingCalendarSearchTxtA, "General Calender");
		    	pressEnter(txt_WorkingCalendarSearchTxtA);
		    	WaitClick(sel_WorkingCalendarSearchSelA);
		    	doubleClick(sel_WorkingCalendarSearchSelA);
		    
		    	WaitClick(btn_PayableAccountTickA);
		    	click(btn_PayableAccountTickA);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				String EMP2 = emp2+" [EMP]";
				
				closeWindow();
				
				pageRefersh();
				
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);

		    	WaitClick(btn_ServiceJobOrderSearchBtnA);
		    	click(btn_ServiceJobOrderSearchBtnA);

		    	WaitElement(txt_ServiceJobOrderLookupSearchTxtA);
		    	sendKeys(txt_ServiceJobOrderLookupSearchTxtA, ReleseServiceJobOrderNos1A);
		    	
		    	WaitClick(btn_ServiceJobOrderLookupSearchBtnA);
		    	click(btn_ServiceJobOrderLookupSearchBtnA);
		    	
		    	WaitClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));
		    	doubleClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP2);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP2));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP2));

		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelA);
		    	click(sel_TimeInSelA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelA);
		    	click(sel_TimeOutSelA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj2 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj2);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
		    	WaitClick(btn_EditA);
		    	click(btn_EditA);
		    	
		    	WaitClick(btn_DeleteRecordRaw1A);
		    	click(btn_DeleteRecordRaw1A);
		    	WaitClick(btn_YesBtnA);
		    	click(btn_YesBtnA);
		    	
		    	if (getText(txt_EmployeeNameDWSTextA).isEmpty() && getText(txt_JobNoDWSTextA).isEmpty()) {
						writeTestResults("Verify that able to delete the recordes in Draft Edit mode", 
								"User able to delete the recordes in Draft Edit mode",
							"User able to delete the recordes in Draft Edit mode Successfully", "pass");
		    	} else {
						writeTestResults("Verify that able to delete the recordes in Draft Edit mode", 
								"User able to delete the recordes in Draft Edit mode",
								"User able to delete the recordes in Draft Edit mode Fail", "fail");
		    	}
		    	
		    	//=======================================================================================
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);
		    	
		    	WaitClick(btn_NewEmployeeBtnA);
		    	click(btn_NewEmployeeBtnA);
		    	
		    	switchWindow();
		    	
		    	LocalTime emp3 = LocalTime.now();
		    	
		    	sendKeys(txt_EmployeeCodeA, ""+emp3);
		    	sendKeys(txt_EmployeeNoA, ""+emp3);
		    	sendKeys(txt_FullNameA, "EMP");
		    	sendKeys(txt_NameWithInitialsA, "EMP");
		    	selectText(txt_EmployeeGroupA, "Test Service");
		    	
		    	WaitClick(btn_RateProfileSearchBtnA);
		    	click(btn_RateProfileSearchBtnA);
		    	WaitElement(txt_RateProfileSearchTxtA);
		    	sendKeys(txt_RateProfileSearchTxtA, "Project");
		    	pressEnter(txt_RateProfileSearchTxtA);
		    	WaitClick(sel_RateProfileSearchSelA);
		    	doubleClick(sel_RateProfileSearchSelA);
		    	
		    	WaitClick(btn_WorkingCalendarSearchBtnA);
		    	click(btn_WorkingCalendarSearchBtnA);
		    	WaitElement(txt_WorkingCalendarSearchTxtA);
		    	sendKeys(txt_WorkingCalendarSearchTxtA, "General Calender");
		    	pressEnter(txt_WorkingCalendarSearchTxtA);
		    	WaitClick(sel_WorkingCalendarSearchSelA);
		    	doubleClick(sel_WorkingCalendarSearchSelA);
		    
		    	WaitClick(btn_PayableAccountTickA);
		    	click(btn_PayableAccountTickA);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				String EMP3 = emp3+" [EMP]";
				
				closeWindow();
				
				pageRefersh();
				
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);

		    	WaitClick(btn_ServiceJobOrderSearchBtnA);
		    	click(btn_ServiceJobOrderSearchBtnA);

		    	WaitElement(txt_ServiceJobOrderLookupSearchTxtA);
		    	sendKeys(txt_ServiceJobOrderLookupSearchTxtA, ReleseServiceJobOrderNos1A);
		    	
		    	WaitClick(btn_ServiceJobOrderLookupSearchBtnA);
		    	click(btn_ServiceJobOrderLookupSearchBtnA);
		    	
		    	WaitClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));
		    	doubleClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP3);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP3));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP3));

		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelA);
		    	click(sel_TimeInSelA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelA);
		    	click(sel_TimeOutSelA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj3 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj3);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);
		    	
		    	WaitClick(btn_UpdateA);
		    	click(btn_UpdateA);
		    	
		    	WaitClick(btn_EditA);
		    	click(btn_EditA);
		    	
				String EmployeeNameTextA = getAttribute(txt_EmployeeNameDWSTextA, "disabled");
				String JobNoTextA = getAttribute(txt_JobNoDWSTextA, "disabled");
				
		    	if (EmployeeNameTextA.contentEquals("true") && JobNoTextA.contentEquals("true")) {
						writeTestResults("Verify that unable to edit the recordes in \"Draft Edit\" mode", 
								"User unable to edit the recordes in \"Draft Edit\" mode",
							"User unable to edit the recordes in \"Draft Edit\" mode Successfully", "pass");
		    	} else {
						writeTestResults("Verify that unable to edit the recordes in \"Draft Edit\" mode", 
								"User unable to edit the recordes in \"Draft Edit\" mode",
								"User unable to edit the recordes in \"Draft Edit\" mode Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_UpdateA);
		    	click(btn_UpdateA);
		    	//================================================================================
		    	
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				WaitElement(txt_PageRelease2A);
				if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that User able to release the daily work sheet Form", 
								"User Can release the daily work sheet Form",
							"User Can release the daily work sheet Form Successfully", "pass");
				} else {
						writeTestResults("Verify that User able to release the daily work sheet Form",
								"User Can release the daily work sheet Form",
								"User Can release the daily work sheet Form Fail", "fail");
				}
				
				String EmployeeNameTextAfterA = getAttribute(txt_EmployeeNameDWSTextA, "disabled");
				String JobNoTextAfterA = getAttribute(txt_JobNoDWSTextA, "disabled");
				
		    	if (EmployeeNameTextAfterA.contentEquals("true") && JobNoTextAfterA.contentEquals("true")) {
						writeTestResults("Verify that records cannot be edit after the \"Release\"", 
								"records cannot be edit after the \"Release\"",
							"records cannot be edit after the \"Release\" Successfully", "pass");
		    	} else {
						writeTestResults("Verify that records cannot be edit after the \"Release\"", 
								"records cannot be edit after the \"Release\"",
								"records cannot be edit after the \"Release\" Fail", "fail");
		    	}
		    }
		    
		    //Service_DWS_070to071
		    public void VerifyThatFollowingActionsAreAvailbleInTheActionDropDownAfterDraftTheDocumentDeleteHistoryActivitiesReminders() throws Exception {
		    	
		    	WaitElement(btn_DailyWorkSheetBtnA);
		    	click(btn_DailyWorkSheetBtnA);
		    	
		    	WaitElement(btn_NewDailyWorksheetBtnA);
		    	click(btn_NewDailyWorksheetBtnA);
		    	
		    	WaitElement(btn_DailyWorkSheetServiceBtnA);
		    	click(btn_DailyWorkSheetServiceBtnA);
		    	
		    	if (getText(txt_WorkedHoursServiceNewPageHeaderA).contentEquals("Worked Hours - New")) {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
							"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitElement(txt_AddNewJobHeaderA);
		    	if (isDisplayed(txt_AddNewJobHeaderA)) {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
							"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Fail", "fail");
		    	}
		    	//=======================================================================================================================
		    	
		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);
		    	
		    	WaitClick(btn_NewEmployeeBtnA);
		    	click(btn_NewEmployeeBtnA);
		    	
		    	switchWindow();
		    	
		    	LocalTime emp1 = LocalTime.now();
		    	
		    	sendKeys(txt_EmployeeCodeA, ""+emp1);
		    	sendKeys(txt_EmployeeNoA, ""+emp1);
		    	sendKeys(txt_FullNameA, "EMP");
		    	sendKeys(txt_NameWithInitialsA, "EMP");
		    	selectText(txt_EmployeeGroupA, "Test Service");
		    	
		    	WaitClick(btn_RateProfileSearchBtnA);
		    	click(btn_RateProfileSearchBtnA);
		    	WaitElement(txt_RateProfileSearchTxtA);
		    	sendKeys(txt_RateProfileSearchTxtA, "Project");
		    	pressEnter(txt_RateProfileSearchTxtA);
		    	WaitClick(sel_RateProfileSearchSelA);
		    	doubleClick(sel_RateProfileSearchSelA);
		    	
		    	WaitClick(btn_WorkingCalendarSearchBtnA);
		    	click(btn_WorkingCalendarSearchBtnA);
		    	WaitElement(txt_WorkingCalendarSearchTxtA);
		    	sendKeys(txt_WorkingCalendarSearchTxtA, "General Calender");
		    	pressEnter(txt_WorkingCalendarSearchTxtA);
		    	WaitClick(sel_WorkingCalendarSearchSelA);
		    	doubleClick(sel_WorkingCalendarSearchSelA);
		    
		    	WaitClick(btn_PayableAccountTickA);
		    	click(btn_PayableAccountTickA);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				String EMP1 = emp1+" [EMP]";
				
				closeWindow();
		    	
		    	//=======================================================================================================================
				
				pageRefersh();
				
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);

		    	WaitClick(btn_ServiceJobOrderSearchBtnA);
		    	click(btn_ServiceJobOrderSearchBtnA);

		    	WaitElement(txt_ServiceJobOrderLookupSearchTxtA);
		    	sendKeys(txt_ServiceJobOrderLookupSearchTxtA, ReleseServiceJobOrderNos1A);
		    	
		    	WaitClick(btn_ServiceJobOrderLookupSearchBtnA);
		    	click(btn_ServiceJobOrderLookupSearchBtnA);
		    	
		    	WaitClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));
		    	doubleClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP1);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP1));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP1));

		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelA);
		    	click(sel_TimeInSelA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelA);
		    	click(sel_TimeOutSelA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj1 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj1);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that User able to draft the daily work sheet Form", 
								"User Can draft the daily work sheet Form",
								"User Can draft the daily work sheet Form Successfully", "pass");
				} else {
						writeTestResults("Verify that User able to draft the daily work sheet Form", 
								"User Can draft the daily work sheet Form",
								"User Can draft the daily work sheet Form Fail", "fail");
				}
		    	
		    	WaitClick(btn_ActionBtnA);
		    	click(btn_ActionBtnA);
		    	
		    	WaitElement(btn_RemindersBtnA);
		    	if (isDisplayed(btn_RemindersBtnA) && isDisplayed(btn_DeleteBtnA) 
		    			&& isDisplayed(btn_HistoryBtnA) && isDisplayed(btn_ActivitiesBtnA)) {
						writeTestResults("Verify that Following Actions are availble in the Action Drop down after draft the document Delete,History,Activities,Reminders", 
								"Following Actions are availble in the Action Drop down after draft the document Delete,History,Activities,Reminders",
							"Following Actions are availble in the Action Drop down after draft the document Delete,History,Activities,Reminders Successfully", "pass");
		    	} else {
						writeTestResults("Verify that Following Actions are availble in the Action Drop down after draft the document Delete,History,Activities,Reminders", 
								"Following Actions are availble in the Action Drop down after draft the document Delete,History,Activities,Reminders",
								"Following Actions are availble in the Action Drop down after draft the document Delete,History,Activities,Reminders Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				WaitElement(txt_PageRelease2A);
				if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that User able to release the daily work sheet Form", 
								"User Can release the daily work sheet Form",
							"User Can release the daily work sheet Form Successfully", "pass");
				} else {
						writeTestResults("Verify that User able to release the daily work sheet Form",
								"User Can release the daily work sheet Form",
								"User Can release the daily work sheet Form Fail", "fail");
				}
		    	
		    	WaitClick(btn_ActionBtnA);
		    	click(btn_ActionBtnA);
		    	
		    	WaitElement(btn_RemindersBtnA);
		    	if (isDisplayed(btn_RemindersBtnA) && isDisplayed(btn_ReverseBtnA) 
		    			&& isDisplayed(btn_HistoryBtnA) && isDisplayed(btn_ActivitiesBtnA) && isDisplayed(btn_JournalBtnA)) {
						writeTestResults("Verify that Following Actions are availble in the Action Drop down after release the document Reverse,History,Activities,Reminders", 
								"Following Actions are availble in the Action Drop down after release the document Reverse,History,Activities,Reminders",
							"Following Actions are availble in the Action Drop down after release the document Reverse,History,Activities,Reminders Successfully", "pass");
		    	} else {
						writeTestResults("Verify that Following Actions are availble in the Action Drop down after release the document Reverse,History,Activities,Reminders", 
								"Following Actions are availble in the Action Drop down after release the document Reverse,History,Activities,Reminders",
								"Following Actions are availble in the Action Drop down after release the document Reverse,History,Activities,Reminders Fail", "fail");
		    	}
		    }
		    
		    //Service_DWS_072
		    public void VerifyUserAbleToDeleteTheDailyWorkSheetAfterDraft() throws Exception {
		    	
		    	WaitElement(btn_DailyWorkSheetBtnA);
		    	click(btn_DailyWorkSheetBtnA);
		    	
		    	WaitElement(btn_NewDailyWorksheetBtnA);
		    	click(btn_NewDailyWorksheetBtnA);
		    	
		    	WaitElement(btn_DailyWorkSheetServiceBtnA);
		    	click(btn_DailyWorkSheetServiceBtnA);
		    	
		    	if (getText(txt_WorkedHoursServiceNewPageHeaderA).contentEquals("Worked Hours - New")) {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
							"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitElement(txt_AddNewJobHeaderA);
		    	if (isDisplayed(txt_AddNewJobHeaderA)) {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
							"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Fail", "fail");
		    	}
		    	//=======================================================================================================================
		    	
		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);
		    	
		    	WaitClick(btn_NewEmployeeBtnA);
		    	click(btn_NewEmployeeBtnA);
		    	
		    	switchWindow();
		    	
		    	LocalTime emp1 = LocalTime.now();
		    	
		    	sendKeys(txt_EmployeeCodeA, ""+emp1);
		    	sendKeys(txt_EmployeeNoA, ""+emp1);
		    	sendKeys(txt_FullNameA, "EMP");
		    	sendKeys(txt_NameWithInitialsA, "EMP");
		    	selectText(txt_EmployeeGroupA, "Test Service");
		    	
		    	WaitClick(btn_RateProfileSearchBtnA);
		    	click(btn_RateProfileSearchBtnA);
		    	WaitElement(txt_RateProfileSearchTxtA);
		    	sendKeys(txt_RateProfileSearchTxtA, "Project");
		    	pressEnter(txt_RateProfileSearchTxtA);
		    	WaitClick(sel_RateProfileSearchSelA);
		    	doubleClick(sel_RateProfileSearchSelA);
		    	
		    	WaitClick(btn_WorkingCalendarSearchBtnA);
		    	click(btn_WorkingCalendarSearchBtnA);
		    	WaitElement(txt_WorkingCalendarSearchTxtA);
		    	sendKeys(txt_WorkingCalendarSearchTxtA, "General Calender");
		    	pressEnter(txt_WorkingCalendarSearchTxtA);
		    	WaitClick(sel_WorkingCalendarSearchSelA);
		    	doubleClick(sel_WorkingCalendarSearchSelA);
		    
		    	WaitClick(btn_PayableAccountTickA);
		    	click(btn_PayableAccountTickA);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				String EMP1 = emp1+" [EMP]";
				
				closeWindow();
		    	
		    	//=======================================================================================================================
				
				pageRefersh();
				
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);

		    	WaitClick(btn_ServiceJobOrderSearchBtnA);
		    	click(btn_ServiceJobOrderSearchBtnA);

		    	WaitElement(txt_ServiceJobOrderLookupSearchTxtA);
		    	sendKeys(txt_ServiceJobOrderLookupSearchTxtA, ReleseServiceJobOrderNos1A);
		    	
		    	WaitClick(btn_ServiceJobOrderLookupSearchBtnA);
		    	click(btn_ServiceJobOrderLookupSearchBtnA);
		    	
		    	WaitClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));
		    	doubleClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP1);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP1));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP1));

		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelA);
		    	click(sel_TimeInSelA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelA);
		    	click(sel_TimeOutSelA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj1 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj1);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that User able to draft the daily work sheet Form", 
								"User Can draft the daily work sheet Form",
								"User Can draft the daily work sheet Form Successfully", "pass");
				} else {
						writeTestResults("Verify that User able to draft the daily work sheet Form", 
								"User Can draft the daily work sheet Form",
								"User Can draft the daily work sheet Form Fail", "fail");
				}
		    	
		    	WaitClick(btn_ActionBtnA);
		    	click(btn_ActionBtnA);
		    	
		    	WaitClick(btn_DeleteBtnA);
		    	click(btn_DeleteBtnA);
		    	
		    	WaitClick(btn_YesBtnA);
		    	click(btn_YesBtnA);
		    	
				Thread.sleep(5000);
				if (isDisplayed(txt_pageDeletedNewA)) {
					writeTestResults("Verify user able to delete the daily work sheet after Draft", "user able to delete the daily work sheet after Draft",
							"user able to delete the daily work sheet after Draft Successfully", "pass");
				} else {
					writeTestResults("Verify user able to delete the daily work sheet after Draft", "user able to delete the daily work sheet after Draft",
							"user able to delete the daily work sheet after Draft Fail", "fail");
				}
		    }
		    
		    //Service_DWS_073
		    public void VerifyUserAbleToReverseTheDailyWorkSheet() throws Exception {
		    	
		    	WaitElement(btn_DailyWorkSheetBtnA);
		    	click(btn_DailyWorkSheetBtnA);
		    	
		    	WaitElement(btn_NewDailyWorksheetBtnA);
		    	click(btn_NewDailyWorksheetBtnA);
		    	
		    	WaitElement(btn_DailyWorkSheetServiceBtnA);
		    	click(btn_DailyWorkSheetServiceBtnA);
		    	
		    	if (getText(txt_WorkedHoursServiceNewPageHeaderA).contentEquals("Worked Hours - New")) {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
							"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitElement(txt_AddNewJobHeaderA);
		    	if (isDisplayed(txt_AddNewJobHeaderA)) {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
							"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Fail", "fail");
		    	}
		    	//=======================================================================================================================
		    	
		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);
		    	
		    	WaitClick(btn_NewEmployeeBtnA);
		    	click(btn_NewEmployeeBtnA);
		    	
		    	switchWindow();
		    	
		    	LocalTime emp1 = LocalTime.now();
		    	
		    	sendKeys(txt_EmployeeCodeA, ""+emp1);
		    	sendKeys(txt_EmployeeNoA, ""+emp1);
		    	sendKeys(txt_FullNameA, "EMP");
		    	sendKeys(txt_NameWithInitialsA, "EMP");
		    	selectText(txt_EmployeeGroupA, "Test Service");
		    	
		    	WaitClick(btn_RateProfileSearchBtnA);
		    	click(btn_RateProfileSearchBtnA);
		    	WaitElement(txt_RateProfileSearchTxtA);
		    	sendKeys(txt_RateProfileSearchTxtA, "Project");
		    	pressEnter(txt_RateProfileSearchTxtA);
		    	WaitClick(sel_RateProfileSearchSelA);
		    	doubleClick(sel_RateProfileSearchSelA);
		    	
		    	WaitClick(btn_WorkingCalendarSearchBtnA);
		    	click(btn_WorkingCalendarSearchBtnA);
		    	WaitElement(txt_WorkingCalendarSearchTxtA);
		    	sendKeys(txt_WorkingCalendarSearchTxtA, "General Calender");
		    	pressEnter(txt_WorkingCalendarSearchTxtA);
		    	WaitClick(sel_WorkingCalendarSearchSelA);
		    	doubleClick(sel_WorkingCalendarSearchSelA);
		    
		    	WaitClick(btn_PayableAccountTickA);
		    	click(btn_PayableAccountTickA);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				String EMP1 = emp1+" [EMP]";
				
				closeWindow();
		    	
		    	//=======================================================================================================================
				
				pageRefersh();
				
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);

		    	WaitClick(btn_ServiceJobOrderSearchBtnA);
		    	click(btn_ServiceJobOrderSearchBtnA);

		    	WaitElement(txt_ServiceJobOrderLookupSearchTxtA);
		    	sendKeys(txt_ServiceJobOrderLookupSearchTxtA, ReleseServiceJobOrderNos1A);
		    	
		    	WaitClick(btn_ServiceJobOrderLookupSearchBtnA);
		    	click(btn_ServiceJobOrderLookupSearchBtnA);
		    	
		    	WaitClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));
		    	doubleClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP1);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP1));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP1));

		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelA);
		    	click(sel_TimeInSelA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelA);
		    	click(sel_TimeOutSelA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj1 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj1);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that User able to draft the daily work sheet Form", 
								"User Can draft the daily work sheet Form",
								"User Can draft the daily work sheet Form Successfully", "pass");
				} else {
						writeTestResults("Verify that User able to draft the daily work sheet Form", 
								"User Can draft the daily work sheet Form",
								"User Can draft the daily work sheet Form Fail", "fail");
				}
		    	
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				WaitElement(txt_PageRelease2A);
				if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that User able to release the daily work sheet Form", 
								"User Can release the daily work sheet Form",
							"User Can release the daily work sheet Form Successfully", "pass");
				} else {
						writeTestResults("Verify that User able to release the daily work sheet Form",
								"User Can release the daily work sheet Form",
								"User Can release the daily work sheet Form Fail", "fail");
				}
		    	
		    	WaitClick(btn_ActionBtnA);
		    	click(btn_ActionBtnA);
		    	WaitClick(btn_ReverseBtnA);
		    	click(btn_ReverseBtnA);
		    	
				Thread.sleep(3000);
				JavascriptExecutor j1 = (JavascriptExecutor) driver;
				j1.executeScript("$(\"#txtrdcmnReverseDate\").datepicker(\"setDate\", new Date())");
				j1.executeScript("$(\"#dlgReverseDateCMN\").parent().find('.dialogbox-buttonarea > .button').click()");	
				
		    	WaitClick(btn_YesBtnA);
		    	click(btn_YesBtnA);
		    	
				WaitElement(txt_pageReversedNewA);
				if (isDisplayed(txt_pageReversedNewA)) {
					writeTestResults("Verify user able to reverse the daily work sheet", "user able to reverse the daily work sheet",
							"user able to reverse the daily work sheet Successfully", "pass");
				} else {
					writeTestResults("Verify user able to reverse the daily work sheet", "user able to reverse the daily work sheet",
							"user able to reverse the daily work sheet Fail", "fail");
				}
		    	
		    }
		    
		    //Service_DWS_074to075
		    public void VerifyDuplicateButtonIsNotAvailbleInTheDraftDailyWorkSheets() throws Exception {
		    	
		    	WaitElement(btn_DailyWorkSheetBtnA);
		    	click(btn_DailyWorkSheetBtnA);
		    	
		    	WaitElement(btn_NewDailyWorksheetBtnA);
		    	click(btn_NewDailyWorksheetBtnA);
		    	
		    	WaitElement(btn_DailyWorkSheetServiceBtnA);
		    	click(btn_DailyWorkSheetServiceBtnA);
		    	
		    	if (getText(txt_WorkedHoursServiceNewPageHeaderA).contentEquals("Worked Hours - New")) {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
							"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitElement(txt_AddNewJobHeaderA);
		    	if (isDisplayed(txt_AddNewJobHeaderA)) {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
							"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Fail", "fail");
		    	}
		    	//=======================================================================================================================
		    	
		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);
		    	
		    	WaitClick(btn_NewEmployeeBtnA);
		    	click(btn_NewEmployeeBtnA);
		    	
		    	switchWindow();
		    	
		    	LocalTime emp1 = LocalTime.now();
		    	
		    	sendKeys(txt_EmployeeCodeA, ""+emp1);
		    	sendKeys(txt_EmployeeNoA, ""+emp1);
		    	sendKeys(txt_FullNameA, "EMP");
		    	sendKeys(txt_NameWithInitialsA, "EMP");
		    	selectText(txt_EmployeeGroupA, "Test Service");
		    	
		    	WaitClick(btn_RateProfileSearchBtnA);
		    	click(btn_RateProfileSearchBtnA);
		    	WaitElement(txt_RateProfileSearchTxtA);
		    	sendKeys(txt_RateProfileSearchTxtA, "Project");
		    	pressEnter(txt_RateProfileSearchTxtA);
		    	WaitClick(sel_RateProfileSearchSelA);
		    	doubleClick(sel_RateProfileSearchSelA);
		    	
		    	WaitClick(btn_WorkingCalendarSearchBtnA);
		    	click(btn_WorkingCalendarSearchBtnA);
		    	WaitElement(txt_WorkingCalendarSearchTxtA);
		    	sendKeys(txt_WorkingCalendarSearchTxtA, "General Calender");
		    	pressEnter(txt_WorkingCalendarSearchTxtA);
		    	WaitClick(sel_WorkingCalendarSearchSelA);
		    	doubleClick(sel_WorkingCalendarSearchSelA);
		    
		    	WaitClick(btn_PayableAccountTickA);
		    	click(btn_PayableAccountTickA);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				String EMP1 = emp1+" [EMP]";
				
				closeWindow();
		    	
		    	//=======================================================================================================================
				
				pageRefersh();
				
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);

		    	WaitClick(btn_ServiceJobOrderSearchBtnA);
		    	click(btn_ServiceJobOrderSearchBtnA);

		    	WaitElement(txt_ServiceJobOrderLookupSearchTxtA);
		    	sendKeys(txt_ServiceJobOrderLookupSearchTxtA, ReleseServiceJobOrderNos1A);
		    	
		    	WaitClick(btn_ServiceJobOrderLookupSearchBtnA);
		    	click(btn_ServiceJobOrderLookupSearchBtnA);
		    	
		    	WaitClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));
		    	doubleClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP1);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP1));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP1));

		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelA);
		    	click(sel_TimeInSelA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelA);
		    	click(sel_TimeOutSelA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj1 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj1);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that User able to draft the daily work sheet Form", 
								"User Can draft the daily work sheet Form",
								"User Can draft the daily work sheet Form Successfully", "pass");
				} else {
						writeTestResults("Verify that User able to draft the daily work sheet Form", 
								"User Can draft the daily work sheet Form",
								"User Can draft the daily work sheet Form Fail", "fail");
				}
				
				if (!isDisplayed(btn_DuplicateA)) {
					writeTestResults("Verify \"Duplicate\" button is not availble in the draft daily work sheets", "\"Duplicate\" button is not availble in the draft daily work sheets",
							"\"Duplicate\" button is not availble in the draft daily work sheets Successfully", "pass");
				} else {
					writeTestResults("Verify \"Duplicate\" button is not availble in the draft daily work sheets", "\"Duplicate\" button is not availble in the draft daily work sheets",
							"\"Duplicate\" button is not availble in the draft daily work sheets Fail", "fail");
				}
		    	
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				WaitElement(txt_PageRelease2A);
				if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that User able to release the daily work sheet Form", 
								"User Can release the daily work sheet Form",
							"User Can release the daily work sheet Form Successfully", "pass");
				} else {
						writeTestResults("Verify that User able to release the daily work sheet Form",
								"User Can release the daily work sheet Form",
								"User Can release the daily work sheet Form Fail", "fail");
				}
		    	
				
				if (!isDisplayed(btn_DuplicateA)) {
					writeTestResults("Verify Duplicate Button is not availble in the Released daily work sheets", "Duplicate Button is not availble in the Released daily work sheets",
							"Duplicate Button is not availble in the Released daily work sheets Successfully", "pass");
				} else {
					writeTestResults("Verify Duplicate Button is not availble in the Released daily work sheets", "Duplicate Button is not availble in the Released daily work sheets",
							"Duplicate Button is not availble in the Released daily work sheets Fail", "fail");
				}
		    }
		    
		    //Service_DWS_076
		    public void VerifyCopyFromButtonIsNotAvailableInTheDailyWorkSheetNewForm() throws Exception {
		    	
		    	WaitElement(btn_DailyWorkSheetBtnA);
		    	click(btn_DailyWorkSheetBtnA);
		    	
		    	WaitElement(btn_NewDailyWorksheetBtnA);
		    	click(btn_NewDailyWorksheetBtnA);
		    	
		    	WaitElement(btn_DailyWorkSheetServiceBtnA);
		    	click(btn_DailyWorkSheetServiceBtnA);
		    	
		    	if (getText(txt_WorkedHoursServiceNewPageHeaderA).contentEquals("Worked Hours - New")) {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
							"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Fail", "fail");
		    	}
		    	
				if (!isDisplayed(btn_CopyFromA)) {
					writeTestResults("Verify \"Copy From\" button is not available in the Daily work sheet new form",
							"\"Copy From\" button is not available in the Daily work sheet new form",
							"\"Copy From\" button is not available in the Daily work sheet new form Successfully", "pass");
				} else {
					writeTestResults("Verify \"Copy From\" button is not available in the Daily work sheet new form", 
							"\"Copy From\" button is not available in the Daily work sheet new form",
							"\"Copy From\" button is not available in the Daily work sheet new form Fail", "fail");
				}
		    }
		    
		    //Service_DWS_077to078
		    public void VerifyThatReverseActionIsNotAvailbleForTheReleasedDailyWorkSheetsAfterCompletingTheServiceJobOrder() throws Exception {
		    	
				   navigationCaseForm();
					Thread.sleep(1000);
					LocalTime obj = LocalTime.now();
					sendKeys(txt_TitleTextA, "Case" + obj);
					selectText(txt_OriginTextA, OriginTextFieldA);

					click(btn_CustomerAccountButtonA);
					sendKeys(txt_CustomerAccountTextA, CustomerAccountTextA);
					pressEnter(txt_CustomerAccountTextA);
					WaitClick(sel_CustomerAccountSelA);
					doubleClick(sel_CustomerAccountSelA);

					WaitClick(btn_newWindowProdA);
					click(btn_newWindowProdA);
					WaitClick(sel_newWindowProdSelA);
					click(sel_newWindowProdSelA);
					Thread.sleep(3000);
					JavascriptExecutor j1 = (JavascriptExecutor) driver;
					j1.executeScript("$(\"#divServiceSerial\").parent().find('.dialogbox-buttonarea > .button').click()");

					selectText(txt_TypeTextA, TypeTextFieldA);
					selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);
					selectText(txt_PriorityTextA, PriorityTextFieldA);

					WaitClick(btn_DraftA);
					click(btn_DraftA);
					WaitElement(txt_PageDraft1A);
					if (isDisplayed(txt_PageDraft1A)) {
						writeTestResults("Verify that able to draft the Case via main menu", "able to draft the Case via main menu",
								"able to draft the Case via main menu Successfully", "pass");
					} else {
						writeTestResults("Verify that able to draft the Case via main menu", "able to draft the Case via main menu",
								"able to draft the Case via main menu Fail", "fail");
					}

					WaitClick(btn_ReleaseA);
					click(btn_ReleaseA);
					Thread.sleep(3000);
					trackCode = getText(txt_trackCodeA);
					String CNo = trackCode;
					
					WaitElement(txt_PageRelease1A);
					if (isDisplayed(txt_PageRelease1A)) {
						writeTestResults("Verify that able to Release the Case via main menu",
								"able to Release the Case via main menu", "able to Release the Case via main menu Successfully",
								"pass");
					} else {
						writeTestResults("Verify that able to Release the Case via main menu",
								"able to Release the Case via main menu", "able to Release the Case via main menu Fail", "fail");
					}
					
					pageRefersh();
					WaitClick(btn_ActionBtnA);
					click(btn_ActionBtnA);
					WaitClick(btn_DroptoServiceCentreBtnA);
					click(btn_DroptoServiceCentreBtnA);
					WaitClick(btn_EntutionHeaderA);
					click(btn_EntutionHeaderA);
					WaitClick(btn_RolecenterA);
					click(btn_RolecenterA);
					
					WaitForOpenServiceJobsNoA();
					
					WaitClick(btn_OpenServiceJobsA);
					click(btn_OpenServiceJobsA);
					WaitElement(btn_ServiceJobArrowA.replace("CNo", CNo));
					click(btn_ServiceJobArrowA.replace("CNo", CNo));
					
					
					switchWindow();
					
					WaitClick(btn_OwnerBtnA);
					click(btn_OwnerBtnA);
					sendKeys(txt_OwnerTxtA, OwnerTxtA);
					Thread.sleep(3000);
					pressEnter(txt_OwnerTxtA);
					WaitClick(sel_OwnerSelA);
					doubleClick(sel_OwnerSelA);
					
					WaitClick(btn_PricingProfileBtnA);
					click(btn_PricingProfileBtnA);
					sendKeys(txt_PricingProfileTxtA, PricingProfileTxtA);
					Thread.sleep(3000);
					pressEnter(txt_PricingProfileTxtA);
					WaitClick(sel_PricingProfileSelA);
					doubleClick(sel_PricingProfileSelA);
					
					WaitClick(btn_ServiceLocationBtnA);
					click(btn_ServiceLocationBtnA);
					sendKeys(txt_ServiceLocationTxtA, ServiceLocationTxtA);
					Thread.sleep(3000);
					pressEnter(txt_ServiceLocationTxtA);
					WaitClick(sel_ServiceLocationSelA);
					doubleClick(sel_ServiceLocationSelA);
					
					WaitClick(btn_TaskDetailsA);
					click(btn_TaskDetailsA);
					WaitClick(btn_TaskDetailsProBtnA);
					click(btn_TaskDetailsProBtnA);
					sendKeys(txt_TaskDetailsProTxtA, ServiceProductA);
					Thread.sleep(3000);
					pressEnter(txt_TaskDetailsProTxtA);
					WaitClick(sel_TaskDetailsProSelA);
					doubleClick(sel_TaskDetailsProSelA);
					
					
					WaitClick(btn_DraftA);
					click(btn_DraftA);
					WaitElement(txt_PageDraft1A);
					if (isDisplayed(txt_PageDraft1A)) {
						writeTestResults("Verify that User Can draft the Service Job Order Form",
								"User Can draft the Service Job Order Form",
								"User Can draft the Service Job Order Form Successfully", "pass");
					} else {
						writeTestResults("Verify that User Can draft the Service Job Order Form",
								"User Can draft the Service Job Order Form", "User Can draft the Service Job Order Form Fail",
								"fail");
					}

					WaitClick(btn_ReleaseA);
					click(btn_ReleaseA);
					Thread.sleep(3000);
					trackCode = getText(txt_trackCodeA);
					String SJO = trackCode;
					
					WaitElement(txt_PageRelease1A);
					if (isDisplayed(txt_PageRelease1A)) {
						writeTestResults("Verify that  able to create the service Job Order by convert the Case which created from Menu(Without service quotation)",
								"User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation)",
								"User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation) Successfully", "pass");
					} else {
						writeTestResults("Verify that  able to create the service Job Order by convert the Case which created from Menu(Without service quotation)",
								"User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation)", "User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation) Fail",
								"fail");
					}
					
					//System.out.println(SJO);
					
					//=======================================================================================
					
					
					clickNavigation();
					
			    	WaitElement(btn_DailyWorkSheetBtnA);
			    	click(btn_DailyWorkSheetBtnA);
			    	
			    	WaitElement(btn_NewDailyWorksheetBtnA);
			    	click(btn_NewDailyWorksheetBtnA);
			    	
			    	WaitElement(btn_DailyWorkSheetServiceBtnA);
			    	click(btn_DailyWorkSheetServiceBtnA);
			    	
			    	if (getText(txt_WorkedHoursServiceNewPageHeaderA).contentEquals("Worked Hours - New")) {
							writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
									"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Successfully", "pass");
			    	} else {
							writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
									"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
									"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Fail", "fail");
			    	}
			    	
			    	WaitClick(btn_AddNewRecordPlusA);
			    	click(btn_AddNewRecordPlusA);
			    	
			    	WaitElement(txt_AddNewJobHeaderA);
			    	if (isDisplayed(txt_AddNewJobHeaderA)) {
							writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
									"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Successfully", "pass");
			    	} else {
							writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
									"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
									"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Fail", "fail");
			    	}
			    	//=======================================================================================================================
			    	
			    	WaitClick(btn_EmployeeSearchBtnA);
			    	click(btn_EmployeeSearchBtnA);
			    	
			    	WaitClick(btn_NewEmployeeBtnA);
			    	click(btn_NewEmployeeBtnA);
			    	
			    	switchWindow();
			    	
			    	LocalTime emp1 = LocalTime.now();
			    	
			    	sendKeys(txt_EmployeeCodeA, ""+emp1);
			    	sendKeys(txt_EmployeeNoA, ""+emp1);
			    	sendKeys(txt_FullNameA, "EMP");
			    	sendKeys(txt_NameWithInitialsA, "EMP");
			    	selectText(txt_EmployeeGroupA, "Test Service");
			    	
			    	WaitClick(btn_RateProfileSearchBtnA);
			    	click(btn_RateProfileSearchBtnA);
			    	WaitElement(txt_RateProfileSearchTxtA);
			    	sendKeys(txt_RateProfileSearchTxtA, "Project");
			    	pressEnter(txt_RateProfileSearchTxtA);
			    	WaitClick(sel_RateProfileSearchSelA);
			    	doubleClick(sel_RateProfileSearchSelA);
			    	
			    	WaitClick(btn_WorkingCalendarSearchBtnA);
			    	click(btn_WorkingCalendarSearchBtnA);
			    	WaitElement(txt_WorkingCalendarSearchTxtA);
			    	sendKeys(txt_WorkingCalendarSearchTxtA, "General Calender");
			    	pressEnter(txt_WorkingCalendarSearchTxtA);
			    	WaitClick(sel_WorkingCalendarSearchSelA);
			    	doubleClick(sel_WorkingCalendarSearchSelA);
			    
			    	WaitClick(btn_PayableAccountTickA);
			    	click(btn_PayableAccountTickA);
			    	
			    	WaitClick(btn_DraftA);
			    	click(btn_DraftA);
			    	
			    	WaitClick(btn_ReleaseA);
			    	click(btn_ReleaseA);
					Thread.sleep(3000);
					String EMP1 = emp1+" [EMP]";
					
					closeWindow();
			    	
			    	//=======================================================================================================================
					
					pageRefersh();
					
			    	WaitClick(btn_AddNewRecordPlusA);
			    	click(btn_AddNewRecordPlusA);

			    	WaitClick(btn_ServiceJobOrderSearchBtnA);
			    	click(btn_ServiceJobOrderSearchBtnA);

			    	WaitElement(txt_ServiceJobOrderLookupSearchTxtA);
			    	sendKeys(txt_ServiceJobOrderLookupSearchTxtA, SJO);
			    	
			    	WaitClick(btn_ServiceJobOrderLookupSearchBtnA);
			    	click(btn_ServiceJobOrderLookupSearchBtnA);
			    	
			    	WaitClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", SJO));
			    	doubleClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", SJO));

			    	WaitClick(btn_EmployeeSearchBtnA);
			    	click(btn_EmployeeSearchBtnA);

			    	WaitElement(txt_EmployeeLookupSearchTxtA);
			    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP1);
			    	pressEnter(txt_EmployeeLookupSearchTxtA);
			    	
			    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP1));
			    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP1));

			    	WaitClick(btn_TimeInA);
			    	click(btn_TimeInA);
			    	WaitClick(sel_TimeInSelA);
			    	click(sel_TimeInSelA);
			    	WaitClick(btn_TimeInA);
			    	click(btn_TimeInSelOkA);
			    	
			    	WaitClick(btn_TimeOutA);
			    	click(btn_TimeOutA);
			    	WaitClick(sel_TimeOutSelA);
			    	click(sel_TimeOutSelA);
			    	WaitClick(btn_TimeOutA);
			    	click(btn_TimeOutSelOkA);
			    	
			    	WaitElement(txt_ReferenceNoTextFieldA);
			    	
			    	LocalTime Obj1 = LocalTime.now();
			    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj1);
			    	
			    	WaitClick(btn_AddNewJobUpdateA);
			    	click(btn_AddNewJobUpdateA);
			    	
			    	WaitClick(btn_DraftA);
			    	click(btn_DraftA);
					WaitElement(txt_PageDraft2A);
					if (isDisplayed(txt_PageDraft2A)) {
							writeTestResults("Verify that User able to draft the daily work sheet Form", 
									"User Can draft the daily work sheet Form",
									"User Can draft the daily work sheet Form Successfully", "pass");
					} else {
							writeTestResults("Verify that User able to draft the daily work sheet Form", 
									"User Can draft the daily work sheet Form",
									"User Can draft the daily work sheet Form Fail", "fail");
					}
			    	
			    	WaitClick(btn_ReleaseA);
			    	click(btn_ReleaseA);
					Thread.sleep(3000);
					trackCode = getText(txt_trackCodeA);
					String DWSNo = trackCode;
					
					WaitElement(txt_PageRelease2A);
					if (isDisplayed(txt_PageRelease2A)) {
							writeTestResults("Verify that User able to release the daily work sheet Form", 
									"User Can release the daily work sheet Form",
								"User Can release the daily work sheet Form Successfully", "pass");
					} else {
							writeTestResults("Verify that User able to release the daily work sheet Form",
									"User Can release the daily work sheet Form",
									"User Can release the daily work sheet Form Fail", "fail");
					}
					
					clickNavigation();
					
					WaitClick(btn_ServiceJobOrderBtnA);
					click(btn_ServiceJobOrderBtnA);
					
					WaitElement(txt_ServiceJobOrderSerchByPageTxtA);
					sendKeys(txt_ServiceJobOrderSerchByPageTxtA, SJO);
					pressEnter(txt_ServiceJobOrderSerchByPageTxtA);
					
					WaitElement(sel_ServiceJobOrderSerchByPageSelA.replace("SJONo", SJO));
					WaitClick(sel_ServiceJobOrderSerchByPageSelA.replace("SJONo", SJO));
					doubleClick(sel_ServiceJobOrderSerchByPageSelA.replace("SJONo", SJO));
					
					WaitClick(btn_ActionBtnA);
					click(btn_ActionBtnA);
					WaitClick(btn_StopBtnA);
				    click(btn_StopBtnA);
				    WaitClick(btn_CompleteProcessBtnA);
				    click(btn_CompleteProcessBtnA);
				    
				    pageRefersh();
				    
				    WaitElement(btn_ActionBtnA);
				    WaitClick(btn_ActionBtnA);
					click(btn_ActionBtnA);
					WaitClick(btn_CompleteBtnA);
				    click(btn_CompleteBtnA);
				    WaitClick(btn_CompleteYesBtnA);
				    click(btn_CompleteYesBtnA);
				    
					Thread.sleep(3000);
					JavascriptExecutor j7 = (JavascriptExecutor)driver;
					j7.executeScript("$(\"#txtServiceJOCompPostDate\").datepicker(\"setDate\", new Date())");
					WaitClick(btn_PostDateApplyA);
					click(btn_PostDateApplyA);
					
					WaitElement(txt_pageCompletedA);
					if (isDisplayed(txt_pageCompletedA)) {
						writeTestResults("Verify that User Can Completed the Service Job Order Form",
								"User Can Completed the Service Job Order Form",
								"User Can Completed the Service Job Order Form Successfully", "pass");
					} else {
						writeTestResults("Verify that User Can Completed the Service Job Order Form",
								"User Can Completed the Service Job Order Form",
								"User Can Completed the Service Job Order Form Fail", "fail");
					}
					
					clickNavigation();
					
			    	WaitElement(btn_DailyWorkSheetBtnA);
			    	click(btn_DailyWorkSheetBtnA);
			    	
					WaitElement(txt_DailyWorkSheetByPageTxtA);
					sendKeys(txt_DailyWorkSheetByPageTxtA, DWSNo);
					pressEnter(txt_DailyWorkSheetByPageTxtA);
					
					WaitElement(sel_DailyWorkSheetByPageSelA.replace("DWSNo", DWSNo));
					WaitClick(sel_DailyWorkSheetByPageSelA.replace("DWSNo", DWSNo));
					doubleClick(sel_DailyWorkSheetByPageSelA.replace("DWSNo", DWSNo));
					
					WaitClick(btn_ActionBtnA);
					click(btn_ActionBtnA);
					
					if (!isDisplayed(btn_ReverseBtnA)) {
						writeTestResults("Verify that \"Reverse\" action is not availble for the released daily work sheets after  completing the service job order",
								"\"Reverse\" action is not availble for the released daily work sheets after  completing the service job order",
								"\"Reverse\" action is not availble for the released daily work sheets after  completing the service job order Successfully", "pass");
					} else {
						writeTestResults("Verify that \"Reverse\" action is not availble for the released daily work sheets after  completing the service job order",
								"\"Reverse\" action is not availble for the released daily work sheets after  completing the service job order",
								"\"Reverse\" action is not availble for the released daily work sheets after  completing the service job order Fail", "fail");
					}
					
					clickNavigation();
					
					WaitClick(btn_ServiceJobOrderBtnA);
					click(btn_ServiceJobOrderBtnA);
					
					WaitElement(txt_ServiceJobOrderSerchByPageTxtA);
					sendKeys(txt_ServiceJobOrderSerchByPageTxtA, SJO);
					pressEnter(txt_ServiceJobOrderSerchByPageTxtA);
					
					WaitElement(sel_ServiceJobOrderSerchByPageSelA.replace("SJONo", SJO));
					WaitClick(sel_ServiceJobOrderSerchByPageSelA.replace("SJONo", SJO));
					doubleClick(sel_ServiceJobOrderSerchByPageSelA.replace("SJONo", SJO));
					
					WaitClick(btn_OverviewTabA);
					click(btn_OverviewTabA);
					WaitClick(btn_LabourTabA);
					click(btn_LabourTabA);
					
					WaitElement(txt_LabourTabWorksheetNoA);
//					System.out.println(getText(txt_LabourTabWorksheetNoA));
					
					if (getText(txt_LabourTabWorksheetNoA).contentEquals(DWSNo)) {
						writeTestResults("Verify that released daily work sheet details are displayed in the relevant service job order overview tab", 
								"released daily work sheet details are displayed in the relevant service job order overview tab",
							"released daily work sheet details are displayed in the relevant service job order overview tab Successfully", "pass");
					} else {
						writeTestResults("Verify that released daily work sheet details are displayed in the relevant service job order overview tab",
								"released daily work sheet details are displayed in the relevant service job order overview tab",
								"released daily work sheet details are displayed in the relevant service job order overview tab Fail", "fail");
					}
		    }
		    
		    //Service_DWS_079
		    public void VerifyThatJournelIsCreatedForTheWorkSheet() throws Exception {
		    	
		    	WaitElement(btn_DailyWorkSheetBtnA);
		    	click(btn_DailyWorkSheetBtnA);
		    	
		    	WaitElement(btn_NewDailyWorksheetBtnA);
		    	click(btn_NewDailyWorksheetBtnA);
		    	
		    	WaitElement(btn_DailyWorkSheetServiceBtnA);
		    	click(btn_DailyWorkSheetServiceBtnA);
		    	
		    	if (getText(txt_WorkedHoursServiceNewPageHeaderA).contentEquals("Worked Hours - New")) {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
							"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitElement(txt_AddNewJobHeaderA);
		    	if (isDisplayed(txt_AddNewJobHeaderA)) {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
							"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Fail", "fail");
		    	}
		    	//=======================================================================================================================
		    	
		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);
		    	
		    	WaitClick(btn_NewEmployeeBtnA);
		    	click(btn_NewEmployeeBtnA);
		    	
		    	switchWindow();
		    	
		    	LocalTime emp1 = LocalTime.now();
		    	
		    	sendKeys(txt_EmployeeCodeA, ""+emp1);
		    	sendKeys(txt_EmployeeNoA, ""+emp1);
		    	sendKeys(txt_FullNameA, "EMP");
		    	sendKeys(txt_NameWithInitialsA, "EMP");
		    	selectText(txt_EmployeeGroupA, "Test Service");
		    	
		    	WaitClick(btn_RateProfileSearchBtnA);
		    	click(btn_RateProfileSearchBtnA);
		    	WaitElement(txt_RateProfileSearchTxtA);
		    	sendKeys(txt_RateProfileSearchTxtA, "Project");
		    	pressEnter(txt_RateProfileSearchTxtA);
		    	WaitClick(sel_RateProfileSearchSelA);
		    	doubleClick(sel_RateProfileSearchSelA);
		    	
		    	WaitClick(btn_WorkingCalendarSearchBtnA);
		    	click(btn_WorkingCalendarSearchBtnA);
		    	WaitElement(txt_WorkingCalendarSearchTxtA);
		    	sendKeys(txt_WorkingCalendarSearchTxtA, "General Calender");
		    	pressEnter(txt_WorkingCalendarSearchTxtA);
		    	WaitClick(sel_WorkingCalendarSearchSelA);
		    	doubleClick(sel_WorkingCalendarSearchSelA);
		    
		    	WaitClick(btn_PayableAccountTickA);
		    	click(btn_PayableAccountTickA);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				String EMP1 = emp1+" [EMP]";
				
				closeWindow();
		    	
		    	//=======================================================================================================================
				
				pageRefersh();
				
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);

		    	WaitClick(btn_ServiceJobOrderSearchBtnA);
		    	click(btn_ServiceJobOrderSearchBtnA);

		    	WaitElement(txt_ServiceJobOrderLookupSearchTxtA);
		    	sendKeys(txt_ServiceJobOrderLookupSearchTxtA, ReleseServiceJobOrderNos1A);
		    	
		    	WaitClick(btn_ServiceJobOrderLookupSearchBtnA);
		    	click(btn_ServiceJobOrderLookupSearchBtnA);
		    	
		    	WaitClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));
		    	doubleClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP1);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP1));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP1));

		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelA);
		    	click(sel_TimeInSelA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelA);
		    	click(sel_TimeOutSelA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj1 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj1);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that User able to draft the daily work sheet Form", 
								"User Can draft the daily work sheet Form",
								"User Can draft the daily work sheet Form Successfully", "pass");
				} else {
						writeTestResults("Verify that User able to draft the daily work sheet Form", 
								"User Can draft the daily work sheet Form",
								"User Can draft the daily work sheet Form Fail", "fail");
				}
		    	
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				WaitElement(txt_PageRelease2A);
				if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that User able to release the daily work sheet Form", 
								"User Can release the daily work sheet Form",
							"User Can release the daily work sheet Form Successfully", "pass");
				} else {
						writeTestResults("Verify that User able to release the daily work sheet Form",
								"User Can release the daily work sheet Form",
								"User Can release the daily work sheet Form Fail", "fail");
				}
				
		    	WaitClick(btn_ActionBtnA);
		    	click(btn_ActionBtnA);
		    	
				WaitElement(btn_JournalBtnA);
				if (isDisplayed(btn_JournalBtnA)) {
						writeTestResults("Verify that Journel is created for the work sheet ", 
								"Journel is created for the work sheet",
							"Journel is created for the work sheet Successfully", "pass");
				} else {
						writeTestResults("Verify that Journel is created for the work sheet ",
								"Journel is created for the work sheet",
								"Journel is created for the work sheet Fail", "fail");
				}
		    }
		    
		    //Service_DWS_080to082
		    public void VerifyThatHourlyRateIconIsDisplayedWhenDraftTheDocument() throws Exception {
		    	
		    	WaitElement(btn_DailyWorkSheetBtnA);
		    	click(btn_DailyWorkSheetBtnA);
		    	
		    	WaitElement(btn_NewDailyWorksheetBtnA);
		    	click(btn_NewDailyWorksheetBtnA);
		    	
		    	WaitElement(btn_DailyWorkSheetServiceBtnA);
		    	click(btn_DailyWorkSheetServiceBtnA);
		    	
		    	if (getText(txt_WorkedHoursServiceNewPageHeaderA).contentEquals("Worked Hours - New")) {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
							"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitElement(txt_AddNewJobHeaderA);
		    	if (isDisplayed(txt_AddNewJobHeaderA)) {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
							"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Fail", "fail");
		    	}
		    	//=======================================================================================================================
		    	
		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);
		    	
		    	WaitClick(btn_NewEmployeeBtnA);
		    	click(btn_NewEmployeeBtnA);
		    	
		    	switchWindow();
		    	
		    	LocalTime emp1 = LocalTime.now();
		    	
		    	sendKeys(txt_EmployeeCodeA, ""+emp1);
		    	sendKeys(txt_EmployeeNoA, ""+emp1);
		    	sendKeys(txt_FullNameA, "EMP");
		    	sendKeys(txt_NameWithInitialsA, "EMP");
		    	selectText(txt_EmployeeGroupA, "Test Service");
		    	
		    	WaitClick(btn_RateProfileSearchBtnA);
		    	click(btn_RateProfileSearchBtnA);
		    	WaitElement(txt_RateProfileSearchTxtA);
		    	sendKeys(txt_RateProfileSearchTxtA, "Project");
		    	pressEnter(txt_RateProfileSearchTxtA);
		    	WaitClick(sel_RateProfileSearchSelA);
		    	doubleClick(sel_RateProfileSearchSelA);
		    	
		    	WaitClick(btn_WorkingCalendarSearchBtnA);
		    	click(btn_WorkingCalendarSearchBtnA);
		    	WaitElement(txt_WorkingCalendarSearchTxtA);
		    	sendKeys(txt_WorkingCalendarSearchTxtA, "General Calender");
		    	pressEnter(txt_WorkingCalendarSearchTxtA);
		    	WaitClick(sel_WorkingCalendarSearchSelA);
		    	doubleClick(sel_WorkingCalendarSearchSelA);
		    
		    	WaitClick(btn_PayableAccountTickA);
		    	click(btn_PayableAccountTickA);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				String EMP1 = emp1+" [EMP]";
				
				closeWindow();
		    	
		    	//=======================================================================================================================
				
				pageRefersh();
				
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);

		    	WaitClick(btn_ServiceJobOrderSearchBtnA);
		    	click(btn_ServiceJobOrderSearchBtnA);

		    	WaitElement(txt_ServiceJobOrderLookupSearchTxtA);
		    	sendKeys(txt_ServiceJobOrderLookupSearchTxtA, ReleseServiceJobOrderNos1A);
		    	
		    	WaitClick(btn_ServiceJobOrderLookupSearchBtnA);
		    	click(btn_ServiceJobOrderLookupSearchBtnA);
		    	
		    	WaitClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));
		    	doubleClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP1);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP1));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP1));

		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelA);
		    	click(sel_TimeInSelA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelA);
		    	click(sel_TimeOutSelA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj1 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj1);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that User able to draft the daily work sheet Form", 
								"User Can draft the daily work sheet Form",
								"User Can draft the daily work sheet Form Successfully", "pass");
				} else {
						writeTestResults("Verify that User able to draft the daily work sheet Form", 
								"User Can draft the daily work sheet Form",
								"User Can draft the daily work sheet Form Fail", "fail");
				}
				
				WaitElement(btn_HourlyRateBtnA);
				if (isDisplayed(btn_HourlyRateBtnA)) {
						writeTestResults("Verify that hourly rate icon is displayed when draft the document", 
								"hourly rate icon is displayed when draft the document",
								"hourly rate icon is displayed when draft the document Successfully", "pass");
				} else {
						writeTestResults("Verify that hourly rate icon is displayed when draft the document", 
								"hourly rate icon is displayed when draft the document",
								"hourly rate icon is displayed when draft the document Fail", "fail");
				}
		    	
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				WaitElement(txt_PageRelease2A);
				if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that User able to release the daily work sheet Form", 
								"User Can release the daily work sheet Form",
							"User Can release the daily work sheet Form Successfully", "pass");
				} else {
						writeTestResults("Verify that User able to release the daily work sheet Form",
								"User Can release the daily work sheet Form",
								"User Can release the daily work sheet Form Fail", "fail");
				}
				
				WaitElement(btn_HourlyRateBtnA);
				if (isDisplayed(btn_HourlyRateBtnA)) {
						writeTestResults("Verify that hourly rate icon is displayed when released  the document", 
								"hourly rate icon is displayed when released  the document",
								"hourly rate icon is displayed when released  the document Successfully", "pass");
				} else {
						writeTestResults("Verify that hourly rate icon is displayed when released  the document", 
								"hourly rate icon is displayed when released  the document",
								"hourly rate icon is displayed when released  the document Fail", "fail");
				}
				
				
				String WorkedHoursRecodeTxtA= driver.findElement(By.xpath(txt_WorkedHoursRecodeTxtA)).getAttribute("value");
//				System.out.println(WorkedHoursRecodeTxtA);
				
		    	WaitClick(btn_HourlyRateBtnA);
		    	click(btn_HourlyRateBtnA);
		    	
		    	String HourlyRateWorkedHoursRecodeTxtA= driver.findElement(By.xpath(txt_HourlyRateWorkedHoursRecodeTxtA)).getAttribute("value");
//		    	System.out.println(HourlyRateWorkedHoursRecodeTxtA);
		    	
				if (WorkedHoursRecodeTxtA.contentEquals(HourlyRateWorkedHoursRecodeTxtA)) {
					writeTestResults("Verify that relevant hourly rates are displayed in the look up", 
							"relevant hourly rates are displayed in the look up",
							"relevant hourly rates are displayed in the look up Successfully", "pass");
				} else {
					writeTestResults("Verify that relevant hourly rates are displayed in the look up", 
							"relevant hourly rates are displayed in the look up",
							"relevant hourly rates are displayed in the look up Fail", "fail");
				}
		    }
		    
		    //Service_DWS_083
		    public void VerifyThatUnableToReverseTheDailyWorkSheetWhenAtLeastOneServiceJobOrderIsCompleted() throws Exception {
		    	
				   	navigationCaseForm();
					Thread.sleep(1000);
					LocalTime obj1 = LocalTime.now();
					sendKeys(txt_TitleTextA, "Case" + obj1);
					selectText(txt_OriginTextA, OriginTextFieldA);

					click(btn_CustomerAccountButtonA);
					sendKeys(txt_CustomerAccountTextA, CustomerAccountTextA);
					pressEnter(txt_CustomerAccountTextA);
					WaitClick(sel_CustomerAccountSelA);
					doubleClick(sel_CustomerAccountSelA);

					WaitClick(btn_newWindowProdA);
					click(btn_newWindowProdA);
					WaitClick(sel_newWindowProdSelA);
					click(sel_newWindowProdSelA);
					Thread.sleep(3000);
					JavascriptExecutor j1 = (JavascriptExecutor) driver;
					j1.executeScript("$(\"#divServiceSerial\").parent().find('.dialogbox-buttonarea > .button').click()");

					selectText(txt_TypeTextA, TypeTextFieldA);
					selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);
					selectText(txt_PriorityTextA, PriorityTextFieldA);

					WaitClick(btn_DraftA);
					click(btn_DraftA);
					WaitElement(txt_PageDraft1A);
					if (isDisplayed(txt_PageDraft1A)) {
						writeTestResults("Verify that able to draft the Case via main menu", "able to draft the Case via main menu",
								"able to draft the Case via main menu Successfully", "pass");
					} else {
						writeTestResults("Verify that able to draft the Case via main menu", "able to draft the Case via main menu",
								"able to draft the Case via main menu Fail", "fail");
					}

					WaitClick(btn_ReleaseA);
					click(btn_ReleaseA);
					Thread.sleep(3000);
					trackCode = getText(txt_trackCodeA);
					String CNo1 = trackCode;
					
					WaitElement(txt_PageRelease1A);
					if (isDisplayed(txt_PageRelease1A)) {
						writeTestResults("Verify that able to Release the Case via main menu",
								"able to Release the Case via main menu", "able to Release the Case via main menu Successfully",
								"pass");
					} else {
						writeTestResults("Verify that able to Release the Case via main menu",
								"able to Release the Case via main menu", "able to Release the Case via main menu Fail", "fail");
					}
					
					pageRefersh();
					WaitClick(btn_ActionBtnA);
					click(btn_ActionBtnA);
					WaitClick(btn_DroptoServiceCentreBtnA);
					click(btn_DroptoServiceCentreBtnA);
					WaitClick(btn_EntutionHeaderA);
					click(btn_EntutionHeaderA);
					WaitClick(btn_RolecenterA);
					click(btn_RolecenterA);
					
					WaitForOpenServiceJobsNoA();
					
					WaitClick(btn_OpenServiceJobsA);
					click(btn_OpenServiceJobsA);
					WaitElement(btn_ServiceJobArrowA.replace("CNo", CNo1));
					click(btn_ServiceJobArrowA.replace("CNo", CNo1));
					
					
					switchWindow();
					
					WaitClick(btn_OwnerBtnA);
					click(btn_OwnerBtnA);
					sendKeys(txt_OwnerTxtA, OwnerTxtA);
					Thread.sleep(3000);
					pressEnter(txt_OwnerTxtA);
					WaitClick(sel_OwnerSelA);
					doubleClick(sel_OwnerSelA);
					
					WaitClick(btn_PricingProfileBtnA);
					click(btn_PricingProfileBtnA);
					sendKeys(txt_PricingProfileTxtA, PricingProfileTxtA);
					Thread.sleep(3000);
					pressEnter(txt_PricingProfileTxtA);
					WaitClick(sel_PricingProfileSelA);
					doubleClick(sel_PricingProfileSelA);
					
					WaitClick(btn_ServiceLocationBtnA);
					click(btn_ServiceLocationBtnA);
					sendKeys(txt_ServiceLocationTxtA, ServiceLocationTxtA);
					Thread.sleep(3000);
					pressEnter(txt_ServiceLocationTxtA);
					WaitClick(sel_ServiceLocationSelA);
					doubleClick(sel_ServiceLocationSelA);
					
					WaitClick(btn_TaskDetailsA);
					click(btn_TaskDetailsA);
					WaitClick(btn_TaskDetailsProBtnA);
					click(btn_TaskDetailsProBtnA);
					sendKeys(txt_TaskDetailsProTxtA, ServiceProductA);
					Thread.sleep(3000);
					pressEnter(txt_TaskDetailsProTxtA);
					WaitClick(sel_TaskDetailsProSelA);
					doubleClick(sel_TaskDetailsProSelA);
					
					
					WaitClick(btn_DraftA);
					click(btn_DraftA);
					WaitElement(txt_PageDraft1A);
					if (isDisplayed(txt_PageDraft1A)) {
						writeTestResults("Verify that User Can draft the Service Job Order Form",
								"User Can draft the Service Job Order Form",
								"User Can draft the Service Job Order Form Successfully", "pass");
					} else {
						writeTestResults("Verify that User Can draft the Service Job Order Form",
								"User Can draft the Service Job Order Form", "User Can draft the Service Job Order Form Fail",
								"fail");
					}

					WaitClick(btn_ReleaseA);
					click(btn_ReleaseA);
					Thread.sleep(3000);
					trackCode = getText(txt_trackCodeA);
					String SJO1 = trackCode;
					
					WaitElement(txt_PageRelease1A);
					if (isDisplayed(txt_PageRelease1A)) {
						writeTestResults("Verify that  able to create the service Job Order by convert the Case which created from Menu(Without service quotation)",
								"User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation)",
								"User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation) Successfully", "pass");
					} else {
						writeTestResults("Verify that  able to create the service Job Order by convert the Case which created from Menu(Without service quotation)",
								"User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation)", "User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation) Fail",
								"fail");
					}
					
					System.out.println(SJO1);
					
					closeWindow();
					clickNavigation();
					navigationCaseForm();
					
					Thread.sleep(1000);
					LocalTime obj2 = LocalTime.now();
					sendKeys(txt_TitleTextA, "Case" + obj2);
					selectText(txt_OriginTextA, OriginTextFieldA);

					click(btn_CustomerAccountButtonA);
					sendKeys(txt_CustomerAccountTextA, CustomerAccountTextA);
					pressEnter(txt_CustomerAccountTextA);
					WaitClick(sel_CustomerAccountSelA);
					doubleClick(sel_CustomerAccountSelA);

					WaitClick(btn_newWindowProdA);
					click(btn_newWindowProdA);
					WaitClick(sel_newWindowProdSelA);
					click(sel_newWindowProdSelA);
					Thread.sleep(3000);
					j1.executeScript("$(\"#divServiceSerial\").parent().find('.dialogbox-buttonarea > .button').click()");

					selectText(txt_TypeTextA, TypeTextFieldA);
					selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);
					selectText(txt_PriorityTextA, PriorityTextFieldA);

					WaitClick(btn_DraftA);
					click(btn_DraftA);
					WaitElement(txt_PageDraft1A);
					if (isDisplayed(txt_PageDraft1A)) {
						writeTestResults("Verify that able to draft the Case via main menu", "able to draft the Case via main menu",
								"able to draft the Case via main menu Successfully", "pass");
					} else {
						writeTestResults("Verify that able to draft the Case via main menu", "able to draft the Case via main menu",
								"able to draft the Case via main menu Fail", "fail");
					}

					WaitClick(btn_ReleaseA);
					click(btn_ReleaseA);
					Thread.sleep(3000);
					trackCode = getText(txt_trackCodeA);
					String CNo2 = trackCode;
					
					WaitElement(txt_PageRelease1A);
					if (isDisplayed(txt_PageRelease1A)) {
						writeTestResults("Verify that able to Release the Case via main menu",
								"able to Release the Case via main menu", "able to Release the Case via main menu Successfully",
								"pass");
					} else {
						writeTestResults("Verify that able to Release the Case via main menu",
								"able to Release the Case via main menu", "able to Release the Case via main menu Fail", "fail");
					}
					
					pageRefersh();
					WaitClick(btn_ActionBtnA);
					click(btn_ActionBtnA);
					WaitClick(btn_DroptoServiceCentreBtnA);
					click(btn_DroptoServiceCentreBtnA);
					WaitClick(btn_EntutionHeaderA);
					click(btn_EntutionHeaderA);
					WaitClick(btn_RolecenterA);
					click(btn_RolecenterA);
					
					WaitForOpenServiceJobsNoA();
					
					WaitClick(btn_OpenServiceJobsA);
					click(btn_OpenServiceJobsA);
					WaitElement(btn_ServiceJobArrowA.replace("CNo", CNo2));
					click(btn_ServiceJobArrowA.replace("CNo", CNo2));
					
					
					switchWindow();
					
					WaitClick(btn_OwnerBtnA);
					click(btn_OwnerBtnA);
					sendKeys(txt_OwnerTxtA, OwnerTxtA);
					Thread.sleep(3000);
					pressEnter(txt_OwnerTxtA);
					WaitClick(sel_OwnerSelA);
					doubleClick(sel_OwnerSelA);
					
					WaitClick(btn_PricingProfileBtnA);
					click(btn_PricingProfileBtnA);
					sendKeys(txt_PricingProfileTxtA, PricingProfileTxtA);
					Thread.sleep(3000);
					pressEnter(txt_PricingProfileTxtA);
					WaitClick(sel_PricingProfileSelA);
					doubleClick(sel_PricingProfileSelA);
					
					WaitClick(btn_ServiceLocationBtnA);
					click(btn_ServiceLocationBtnA);
					sendKeys(txt_ServiceLocationTxtA, ServiceLocationTxtA);
					Thread.sleep(3000);
					pressEnter(txt_ServiceLocationTxtA);
					WaitClick(sel_ServiceLocationSelA);
					doubleClick(sel_ServiceLocationSelA);
					
					WaitClick(btn_TaskDetailsA);
					click(btn_TaskDetailsA);
					WaitClick(btn_TaskDetailsProBtnA);
					click(btn_TaskDetailsProBtnA);
					sendKeys(txt_TaskDetailsProTxtA, ServiceProductA);
					Thread.sleep(3000);
					pressEnter(txt_TaskDetailsProTxtA);
					WaitClick(sel_TaskDetailsProSelA);
					doubleClick(sel_TaskDetailsProSelA);
					
					
					WaitClick(btn_DraftA);
					click(btn_DraftA);
					WaitElement(txt_PageDraft1A);
					if (isDisplayed(txt_PageDraft1A)) {
						writeTestResults("Verify that User Can draft the Service Job Order Form",
								"User Can draft the Service Job Order Form",
								"User Can draft the Service Job Order Form Successfully", "pass");
					} else {
						writeTestResults("Verify that User Can draft the Service Job Order Form",
								"User Can draft the Service Job Order Form", "User Can draft the Service Job Order Form Fail",
								"fail");
					}

					WaitClick(btn_ReleaseA);
					click(btn_ReleaseA);
					Thread.sleep(3000);
					trackCode = getText(txt_trackCodeA);
					String SJO2 = trackCode;
					
					WaitElement(txt_PageRelease1A);
					if (isDisplayed(txt_PageRelease1A)) {
						writeTestResults("Verify that  able to create the service Job Order by convert the Case which created from Menu(Without service quotation)",
								"User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation)",
								"User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation) Successfully", "pass");
					} else {
						writeTestResults("Verify that  able to create the service Job Order by convert the Case which created from Menu(Without service quotation)",
								"User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation)", "User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation) Fail",
								"fail");
					}
					
					System.out.println(SJO2);
					
					closeWindow();
					
					//========================================================================================
					
					clickNavigation();
			    	WaitElement(btn_DailyWorkSheetBtnA);
			    	click(btn_DailyWorkSheetBtnA);
			    	
			    	WaitElement(btn_NewDailyWorksheetBtnA);
			    	click(btn_NewDailyWorksheetBtnA);
			    	
			    	WaitElement(btn_DailyWorkSheetServiceBtnA);
			    	click(btn_DailyWorkSheetServiceBtnA);
			    	
			    	if (getText(txt_WorkedHoursServiceNewPageHeaderA).contentEquals("Worked Hours - New")) {
							writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
									"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Successfully", "pass");
			    	} else {
							writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
									"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
									"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Fail", "fail");
			    	}
			    	
			    	WaitClick(btn_AddNewRecordPlusA);
			    	click(btn_AddNewRecordPlusA);
			    	
			    	WaitElement(txt_AddNewJobHeaderA);
			    	if (isDisplayed(txt_AddNewJobHeaderA)) {
							writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
									"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Successfully", "pass");
			    	} else {
							writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
									"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
									"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Fail", "fail");
			    	}
			    	//=======================================================================================================================
			    	
			    	WaitClick(btn_EmployeeSearchBtnA);
			    	click(btn_EmployeeSearchBtnA);
			    	
			    	WaitClick(btn_NewEmployeeBtnA);
			    	click(btn_NewEmployeeBtnA);
			    	
			    	switchWindow();
			    	
			    	LocalTime emp = LocalTime.now();
			    	
			    	sendKeys(txt_EmployeeCodeA, ""+emp);
			    	sendKeys(txt_EmployeeNoA, ""+emp);
			    	sendKeys(txt_FullNameA, "EMP");
			    	sendKeys(txt_NameWithInitialsA, "EMP");
			    	selectText(txt_EmployeeGroupA, "Test Service");
			    	
			    	WaitClick(btn_RateProfileSearchBtnA);
			    	click(btn_RateProfileSearchBtnA);
			    	WaitElement(txt_RateProfileSearchTxtA);
			    	sendKeys(txt_RateProfileSearchTxtA, "Project");
			    	pressEnter(txt_RateProfileSearchTxtA);
			    	WaitClick(sel_RateProfileSearchSelA);
			    	doubleClick(sel_RateProfileSearchSelA);
			    	
			    	WaitClick(btn_WorkingCalendarSearchBtnA);
			    	click(btn_WorkingCalendarSearchBtnA);
			    	WaitElement(txt_WorkingCalendarSearchTxtA);
			    	sendKeys(txt_WorkingCalendarSearchTxtA, "General Calender");
			    	pressEnter(txt_WorkingCalendarSearchTxtA);
			    	WaitClick(sel_WorkingCalendarSearchSelA);
			    	doubleClick(sel_WorkingCalendarSearchSelA);
			    
			    	WaitClick(btn_PayableAccountTickA);
			    	click(btn_PayableAccountTickA);
			    	
			    	WaitClick(btn_DraftA);
			    	click(btn_DraftA);
			    	
			    	WaitClick(btn_ReleaseA);
			    	click(btn_ReleaseA);
					Thread.sleep(3000);
					String EMP = emp+" [EMP]";
					
					closeWindow();
			    	
			    	//=======================================================================================================================
					
					pageRefersh();
					
			    	WaitClick(btn_AddNewRecordPlusA);
			    	click(btn_AddNewRecordPlusA);

			    	WaitClick(btn_ServiceJobOrderSearchBtnA);
			    	click(btn_ServiceJobOrderSearchBtnA);

			    	WaitElement(txt_ServiceJobOrderLookupSearchTxtA);
			    	sendKeys(txt_ServiceJobOrderLookupSearchTxtA, SJO1);
			    	
			    	WaitClick(btn_ServiceJobOrderLookupSearchBtnA);
			    	click(btn_ServiceJobOrderLookupSearchBtnA);
			    	
			    	WaitClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", SJO1));
			    	doubleClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", SJO1));

			    	WaitClick(btn_EmployeeSearchBtnA);
			    	click(btn_EmployeeSearchBtnA);

			    	WaitElement(txt_EmployeeLookupSearchTxtA);
			    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP);
			    	pressEnter(txt_EmployeeLookupSearchTxtA);
			    	
			    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP));
			    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP));

			    	WaitClick(btn_TimeInA);
			    	click(btn_TimeInA);
			    	WaitClick(sel_TimeInSelA);
			    	click(sel_TimeInSelA);
			    	WaitClick(btn_TimeInA);
			    	click(btn_TimeInSelOkA);
			    	
			    	WaitClick(btn_TimeOutA);
			    	click(btn_TimeOutA);
			    	WaitClick(sel_TimeOutSelA);
			    	click(sel_TimeOutSelA);
			    	WaitClick(btn_TimeOutA);
			    	click(btn_TimeOutSelOkA);
			    	
			    	WaitElement(txt_ReferenceNoTextFieldA);
			    	
			    	LocalTime Obj1 = LocalTime.now();
			    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj1);
			    	
			    	WaitClick(btn_AddNewJobUpdateA);
			    	click(btn_AddNewJobUpdateA);
			    	
			    	//===================================================================================
			    	
			    	WaitClick(btn_AddNewRecordPlusA);
			    	click(btn_AddNewRecordPlusA);
			    	
			    	WaitClick(btn_ServiceJobOrderSearchBtnA);
			    	click(btn_ServiceJobOrderSearchBtnA);

			    	WaitElement(txt_ServiceJobOrderLookupSearchTxtA);
			    	sendKeys(txt_ServiceJobOrderLookupSearchTxtA, SJO2);
			    	pressEnter(txt_ServiceJobOrderLookupSearchTxtA);
			    	
			    	WaitClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", SJO2));
			    	doubleClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", SJO2));

			    	WaitClick(btn_EmployeeSearchBtnA);
			    	click(btn_EmployeeSearchBtnA);

			    	WaitElement(txt_EmployeeLookupSearchTxtA);
			    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP);
			    	pressEnter(txt_EmployeeLookupSearchTxtA);
			    	
			    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP));
			    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP));
			    	
			    	WaitClick(btn_TimeInA);
			    	click(btn_TimeInA);
			    	WaitClick(sel_TimeInSelNewA);
			    	click(sel_TimeInSelNewA);
			    	WaitClick(btn_TimeInA);
			    	click(btn_TimeInSelOkA);
			    	
			    	WaitClick(btn_TimeOutA);
			    	click(btn_TimeOutA);
			    	WaitClick(sel_TimeOutSelNewA);
			    	click(sel_TimeOutSelNewA);
			    	WaitClick(btn_TimeOutA);
			    	click(btn_TimeOutSelOkA);
			    	
			    	WaitElement(txt_ReferenceNoTextFieldA);
			    	
			    	LocalTime Obj2 = LocalTime.now();
			    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj2);
			    	
			    	WaitClick(btn_AddNewJobUpdateA);
			    	click(btn_AddNewJobUpdateA);
			    	//===========================================================================
			    	
			    	WaitClick(btn_DraftA);
			    	click(btn_DraftA);
					WaitElement(txt_PageDraft2A);
					if (isDisplayed(txt_PageDraft2A)) {
							writeTestResults("Verify that able to draft the work sheet which having 2 different job orders with same employee but different worked hours", 
									"User Can draft the work sheet which having 2 different job orders with same employee but different worked hours",
									"User Can draft the work sheet which having 2 different job orders with same employee but different worked hours Successfully", "pass");
					} else {
							writeTestResults("Verify that able to draft the work sheet which having 2 different job orders with same employee but different worked hours", 
									"User Can draft the work sheet which having 2 different job orders with same employee but different worked hours",
									"User Can draft the work sheet which having 2 different job orders with same employee but different worked hours Fail", "fail");
					}
					
					//=====================================================================================
					
					WaitClick(btn_ReleaseA);
					click(btn_ReleaseA);
					Thread.sleep(3000);
					trackCode = getText(txt_trackCodeA);
					String DWSNo =trackCode;
					
					WaitElement(txt_PageRelease2A);
					if (isDisplayed(txt_PageRelease2A)) {
							writeTestResults("Verify that able to Release the work sheet which having 2 different job orders with same employee but different worked hours", 
									"User Can Release the work sheet which having 2 different job orders with same employee but different worked hours",
								"User Can Release the work sheet which having 2 different job orders with same employee but different worked hours Successfully", "pass");
					} else {
							writeTestResults("Verify that able to Release the work sheet which having 2 different job orders with same employee but different worked hours",
									"User Can Release the work sheet which having 2 different job orders with same employee but different worked hours",
									"User Can Release the work sheet which having 2 different job orders with same employee but different worked hours Fail", "fail");
					}
					
					
					//=================================================================
					
					clickNavigation();
					
					WaitClick(btn_ServiceJobOrderBtnA);
					click(btn_ServiceJobOrderBtnA);
					
					WaitElement(txt_ServiceJobOrderSerchByPageTxtA);
					sendKeys(txt_ServiceJobOrderSerchByPageTxtA, SJO1);
					pressEnter(txt_ServiceJobOrderSerchByPageTxtA);
					
					WaitElement(sel_ServiceJobOrderSerchByPageSelA.replace("SJONo", SJO1));
					WaitClick(sel_ServiceJobOrderSerchByPageSelA.replace("SJONo", SJO1));
					doubleClick(sel_ServiceJobOrderSerchByPageSelA.replace("SJONo", SJO1));
					
					WaitClick(btn_ActionBtnA);
					click(btn_ActionBtnA);
					WaitClick(btn_StopBtnA);
				    click(btn_StopBtnA);
				    WaitClick(btn_CompleteProcessBtnA);
				    click(btn_CompleteProcessBtnA);
				    
				    pageRefersh();
				    
				    WaitElement(btn_ActionBtnA);
				    WaitClick(btn_ActionBtnA);
					click(btn_ActionBtnA);
					WaitClick(btn_CompleteBtnA);
				    click(btn_CompleteBtnA);
				    WaitClick(btn_CompleteYesBtnA);
				    click(btn_CompleteYesBtnA);
				    
					Thread.sleep(3000);
					JavascriptExecutor j7 = (JavascriptExecutor)driver;
					j7.executeScript("$(\"#txtServiceJOCompPostDate\").datepicker(\"setDate\", new Date())");
					WaitClick(btn_PostDateApplyA);
					click(btn_PostDateApplyA);
					
					WaitElement(txt_pageCompletedA);
					if (isDisplayed(txt_pageCompletedA)) {
						writeTestResults("Verify that User Can Completed the Service Job Order Form",
								"User Can Completed the Service Job Order Form",
								"User Can Completed the Service Job Order Form Successfully", "pass");
					} else {
						writeTestResults("Verify that User Can Completed the Service Job Order Form",
								"User Can Completed the Service Job Order Form",
								"User Can Completed the Service Job Order Form Fail", "fail");
					}
					
					clickNavigation();
					
			    	WaitElement(btn_DailyWorkSheetBtnA);
			    	click(btn_DailyWorkSheetBtnA);
			    	
					WaitElement(txt_DailyWorkSheetByPageTxtA);
					sendKeys(txt_DailyWorkSheetByPageTxtA, DWSNo);
					pressEnter(txt_DailyWorkSheetByPageTxtA);
					
					WaitElement(sel_DailyWorkSheetByPageSelA.replace("DWSNo", DWSNo));
					WaitClick(sel_DailyWorkSheetByPageSelA.replace("DWSNo", DWSNo));
					doubleClick(sel_DailyWorkSheetByPageSelA.replace("DWSNo", DWSNo));
					
					WaitClick(btn_ActionBtnA);
					click(btn_ActionBtnA);
					
					if (!isDisplayed(btn_ReverseBtnA)) {
						writeTestResults("Verify that unable to reverse the daily work sheet when at least one service job order is completed",
								"unable to reverse the daily work sheet when at least one service job order is completed",
								"unable to reverse the daily work sheet when at least one service job order is completed Successfully", "pass");
					} else {
						writeTestResults("Verify that unable to reverse the daily work sheet when at least one service job order is completed",
								"unable to reverse the daily work sheet when at least one service job order is completed",
								"unable to reverse the daily work sheet when at least one service job order is completed Fail", "fail");
					}
		    }
		    
		    //Service_DWS_084
		    public void VerifyThatAbleToCreateaOneDailyWorkSheetForDifferentJobDates() throws Exception {
		    	
			   	navigationCaseForm();
				Thread.sleep(1000);
				LocalTime obj1 = LocalTime.now();
				sendKeys(txt_TitleTextA, "Case" + obj1);
				selectText(txt_OriginTextA, OriginTextFieldA);

				click(btn_CustomerAccountButtonA);
				sendKeys(txt_CustomerAccountTextA, CustomerAccountTextA);
				pressEnter(txt_CustomerAccountTextA);
				WaitClick(sel_CustomerAccountSelA);
				doubleClick(sel_CustomerAccountSelA);

				WaitClick(btn_newWindowProdA);
				click(btn_newWindowProdA);
				WaitClick(sel_newWindowProdSelA);
				click(sel_newWindowProdSelA);
				Thread.sleep(3000);
				JavascriptExecutor j1 = (JavascriptExecutor) driver;
				j1.executeScript("$(\"#divServiceSerial\").parent().find('.dialogbox-buttonarea > .button').click()");

				selectText(txt_TypeTextA, TypeTextFieldA);
				selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);
				selectText(txt_PriorityTextA, PriorityTextFieldA);

				WaitClick(btn_DraftA);
				click(btn_DraftA);
				WaitElement(txt_PageDraft1A);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that able to draft the Case via main menu", "able to draft the Case via main menu",
							"able to draft the Case via main menu Successfully", "pass");
				} else {
					writeTestResults("Verify that able to draft the Case via main menu", "able to draft the Case via main menu",
							"able to draft the Case via main menu Fail", "fail");
				}

				WaitClick(btn_ReleaseA);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String CNo1 = trackCode;
				
				WaitElement(txt_PageRelease1A);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that able to Release the Case via main menu",
							"able to Release the Case via main menu", "able to Release the Case via main menu Successfully",
							"pass");
				} else {
					writeTestResults("Verify that able to Release the Case via main menu",
							"able to Release the Case via main menu", "able to Release the Case via main menu Fail", "fail");
				}
				
				pageRefersh();
				WaitClick(btn_ActionBtnA);
				click(btn_ActionBtnA);
				WaitClick(btn_DroptoServiceCentreBtnA);
				click(btn_DroptoServiceCentreBtnA);
				WaitClick(btn_EntutionHeaderA);
				click(btn_EntutionHeaderA);
				WaitClick(btn_RolecenterA);
				click(btn_RolecenterA);
				
				WaitForOpenServiceJobsNoA();
				
				WaitClick(btn_OpenServiceJobsA);
				click(btn_OpenServiceJobsA);
				WaitElement(btn_ServiceJobArrowA.replace("CNo", CNo1));
				click(btn_ServiceJobArrowA.replace("CNo", CNo1));
				
				
				switchWindow();
				
				WaitClick(btn_OwnerBtnA);
				click(btn_OwnerBtnA);
				sendKeys(txt_OwnerTxtA, OwnerTxtA);
				Thread.sleep(3000);
				pressEnter(txt_OwnerTxtA);
				WaitClick(sel_OwnerSelA);
				doubleClick(sel_OwnerSelA);
				
				WaitClick(btn_PricingProfileBtnA);
				click(btn_PricingProfileBtnA);
				sendKeys(txt_PricingProfileTxtA, PricingProfileTxtA);
				Thread.sleep(3000);
				pressEnter(txt_PricingProfileTxtA);
				WaitClick(sel_PricingProfileSelA);
				doubleClick(sel_PricingProfileSelA);
				
				WaitClick(btn_ServiceLocationBtnA);
				click(btn_ServiceLocationBtnA);
				sendKeys(txt_ServiceLocationTxtA, ServiceLocationTxtA);
				Thread.sleep(3000);
				pressEnter(txt_ServiceLocationTxtA);
				WaitClick(sel_ServiceLocationSelA);
				doubleClick(sel_ServiceLocationSelA);
				
				WaitClick(btn_TaskDetailsA);
				click(btn_TaskDetailsA);
				WaitClick(btn_TaskDetailsProBtnA);
				click(btn_TaskDetailsProBtnA);
				sendKeys(txt_TaskDetailsProTxtA, ServiceProductA);
				Thread.sleep(3000);
				pressEnter(txt_TaskDetailsProTxtA);
				WaitClick(sel_TaskDetailsProSelA);
				doubleClick(sel_TaskDetailsProSelA);
				
				
				WaitClick(btn_DraftA);
				click(btn_DraftA);
				WaitElement(txt_PageDraft1A);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form", "User Can draft the Service Job Order Form Fail",
							"fail");
				}

				WaitClick(btn_ReleaseA);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String SJO1 = trackCode;
				
				WaitElement(txt_PageRelease1A);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that  able to create the service Job Order by convert the Case which created from Menu(Without service quotation)",
							"User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation)",
							"User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation) Successfully", "pass");
				} else {
					writeTestResults("Verify that  able to create the service Job Order by convert the Case which created from Menu(Without service quotation)",
							"User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation)", "User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation) Fail",
							"fail");
				}
				
				System.out.println(SJO1);
				
				closeWindow();
				clickNavigation();
				navigationCaseForm();
				
				Thread.sleep(1000);
				LocalTime obj2 = LocalTime.now();
				sendKeys(txt_TitleTextA, "Case" + obj2);
				selectText(txt_OriginTextA, OriginTextFieldA);

				click(btn_CustomerAccountButtonA);
				sendKeys(txt_CustomerAccountTextA, CustomerAccountTextA);
				pressEnter(txt_CustomerAccountTextA);
				WaitClick(sel_CustomerAccountSelA);
				doubleClick(sel_CustomerAccountSelA);

				WaitClick(btn_newWindowProdA);
				click(btn_newWindowProdA);
				WaitClick(sel_newWindowProdSelA);
				click(sel_newWindowProdSelA);
				Thread.sleep(3000);
				j1.executeScript("$(\"#divServiceSerial\").parent().find('.dialogbox-buttonarea > .button').click()");

				selectText(txt_TypeTextA, TypeTextFieldA);
				selectText(txt_ServiceGroupTextA, ServiceGroupTextFieldA);
				selectText(txt_PriorityTextA, PriorityTextFieldA);

				WaitClick(btn_DraftA);
				click(btn_DraftA);
				WaitElement(txt_PageDraft1A);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that able to draft the Case via main menu", "able to draft the Case via main menu",
							"able to draft the Case via main menu Successfully", "pass");
				} else {
					writeTestResults("Verify that able to draft the Case via main menu", "able to draft the Case via main menu",
							"able to draft the Case via main menu Fail", "fail");
				}

				WaitClick(btn_ReleaseA);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String CNo2 = trackCode;
				
				WaitElement(txt_PageRelease1A);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that able to Release the Case via main menu",
							"able to Release the Case via main menu", "able to Release the Case via main menu Successfully",
							"pass");
				} else {
					writeTestResults("Verify that able to Release the Case via main menu",
							"able to Release the Case via main menu", "able to Release the Case via main menu Fail", "fail");
				}
				
				pageRefersh();
				WaitClick(btn_ActionBtnA);
				click(btn_ActionBtnA);
				WaitClick(btn_DroptoServiceCentreBtnA);
				click(btn_DroptoServiceCentreBtnA);
				WaitClick(btn_EntutionHeaderA);
				click(btn_EntutionHeaderA);
				WaitClick(btn_RolecenterA);
				click(btn_RolecenterA);
				
				WaitForOpenServiceJobsNoA();
				
				WaitClick(btn_OpenServiceJobsA);
				click(btn_OpenServiceJobsA);
				WaitElement(btn_ServiceJobArrowA.replace("CNo", CNo2));
				click(btn_ServiceJobArrowA.replace("CNo", CNo2));
				
				
				switchWindow();
				
				WaitClick(btn_OwnerBtnA);
				click(btn_OwnerBtnA);
				sendKeys(txt_OwnerTxtA, OwnerTxtA);
				Thread.sleep(3000);
				pressEnter(txt_OwnerTxtA);
				WaitClick(sel_OwnerSelA);
				doubleClick(sel_OwnerSelA);
				
				WaitClick(btn_PricingProfileBtnA);
				click(btn_PricingProfileBtnA);
				sendKeys(txt_PricingProfileTxtA, PricingProfileTxtA);
				Thread.sleep(3000);
				pressEnter(txt_PricingProfileTxtA);
				WaitClick(sel_PricingProfileSelA);
				doubleClick(sel_PricingProfileSelA);
				
				WaitClick(btn_ServiceLocationBtnA);
				click(btn_ServiceLocationBtnA);
				sendKeys(txt_ServiceLocationTxtA, ServiceLocationTxtA);
				Thread.sleep(3000);
				pressEnter(txt_ServiceLocationTxtA);
				WaitClick(sel_ServiceLocationSelA);
				doubleClick(sel_ServiceLocationSelA);
				
				WaitClick(btn_TaskDetailsA);
				click(btn_TaskDetailsA);
				WaitClick(btn_TaskDetailsProBtnA);
				click(btn_TaskDetailsProBtnA);
				sendKeys(txt_TaskDetailsProTxtA, ServiceProductA);
				Thread.sleep(3000);
				pressEnter(txt_TaskDetailsProTxtA);
				WaitClick(sel_TaskDetailsProSelA);
				doubleClick(sel_TaskDetailsProSelA);
				
				
				WaitClick(btn_DraftA);
				click(btn_DraftA);
				WaitElement(txt_PageDraft1A);
				if (isDisplayed(txt_PageDraft1A)) {
					writeTestResults("Verify that User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form Successfully", "pass");
				} else {
					writeTestResults("Verify that User Can draft the Service Job Order Form",
							"User Can draft the Service Job Order Form", "User Can draft the Service Job Order Form Fail",
							"fail");
				}

				WaitClick(btn_ReleaseA);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String SJO2 = trackCode;
				
				WaitElement(txt_PageRelease1A);
				if (isDisplayed(txt_PageRelease1A)) {
					writeTestResults("Verify that  able to create the service Job Order by convert the Case which created from Menu(Without service quotation)",
							"User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation)",
							"User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation) Successfully", "pass");
				} else {
					writeTestResults("Verify that  able to create the service Job Order by convert the Case which created from Menu(Without service quotation)",
							"User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation)", "User Can Release created service Job Order by convert the Case which created from Menu(Without service quotation) Fail",
							"fail");
				}
				
				System.out.println(SJO2);
				
				closeWindow();
				
				//========================================================================================
				
				clickNavigation();
		    	WaitElement(btn_DailyWorkSheetBtnA);
		    	click(btn_DailyWorkSheetBtnA);
		    	
		    	WaitElement(btn_NewDailyWorksheetBtnA);
		    	click(btn_NewDailyWorksheetBtnA);
		    	
		    	WaitElement(btn_DailyWorkSheetServiceBtnA);
		    	click(btn_DailyWorkSheetServiceBtnA);
		    	
		    	if (getText(txt_WorkedHoursServiceNewPageHeaderA).contentEquals("Worked Hours - New")) {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
							"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitElement(txt_AddNewJobHeaderA);
		    	if (isDisplayed(txt_AddNewJobHeaderA)) {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
							"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Fail", "fail");
		    	}
		    	//=======================================================================================================================
		    	
		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);
		    	
		    	WaitClick(btn_NewEmployeeBtnA);
		    	click(btn_NewEmployeeBtnA);
		    	
		    	switchWindow();
		    	
		    	LocalTime emp = LocalTime.now();
		    	
		    	sendKeys(txt_EmployeeCodeA, ""+emp);
		    	sendKeys(txt_EmployeeNoA, ""+emp);
		    	sendKeys(txt_FullNameA, "EMP");
		    	sendKeys(txt_NameWithInitialsA, "EMP");
		    	selectText(txt_EmployeeGroupA, "Test Service");
		    	
		    	WaitClick(btn_RateProfileSearchBtnA);
		    	click(btn_RateProfileSearchBtnA);
		    	WaitElement(txt_RateProfileSearchTxtA);
		    	sendKeys(txt_RateProfileSearchTxtA, "Project");
		    	pressEnter(txt_RateProfileSearchTxtA);
		    	WaitClick(sel_RateProfileSearchSelA);
		    	doubleClick(sel_RateProfileSearchSelA);
		    	
		    	WaitClick(btn_WorkingCalendarSearchBtnA);
		    	click(btn_WorkingCalendarSearchBtnA);
		    	WaitElement(txt_WorkingCalendarSearchTxtA);
		    	sendKeys(txt_WorkingCalendarSearchTxtA, "General Calender");
		    	pressEnter(txt_WorkingCalendarSearchTxtA);
		    	WaitClick(sel_WorkingCalendarSearchSelA);
		    	doubleClick(sel_WorkingCalendarSearchSelA);
		    
		    	WaitClick(btn_PayableAccountTickA);
		    	click(btn_PayableAccountTickA);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				String EMP = emp+" [EMP]";
				
				closeWindow();
		    	
		    	//=======================================================================================================================
				
				pageRefersh();
				
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);

		    	WaitClick(btn_ServiceJobOrderSearchBtnA);
		    	click(btn_ServiceJobOrderSearchBtnA);

		    	WaitElement(txt_ServiceJobOrderLookupSearchTxtA);
		    	sendKeys(txt_ServiceJobOrderLookupSearchTxtA, SJO1);
		    	
		    	WaitClick(btn_ServiceJobOrderLookupSearchBtnA);
		    	click(btn_ServiceJobOrderLookupSearchBtnA);
		    	
		    	WaitClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", SJO1));
		    	doubleClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", SJO1));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP));
		    	
				Thread.sleep(3000);
				JavascriptExecutor j7 = (JavascriptExecutor)driver;
				j7.executeScript("$('#txtJobDate').datepicker('setDate', '2020/06/28')");

		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelA);
		    	click(sel_TimeInSelA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelA);
		    	click(sel_TimeOutSelA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj1 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj1);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);
		    	
		    	//===================================================================================
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitClick(btn_ServiceJobOrderSearchBtnA);
		    	click(btn_ServiceJobOrderSearchBtnA);

		    	WaitElement(txt_ServiceJobOrderLookupSearchTxtA);
		    	sendKeys(txt_ServiceJobOrderLookupSearchTxtA, SJO2);
		    	pressEnter(txt_ServiceJobOrderLookupSearchTxtA);
		    	
		    	WaitClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", SJO2));
		    	doubleClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", SJO2));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP));
		    	
		    	j7.executeScript("$('#txtJobDate').datepicker('setDate', '2020/06/29')");
		    	
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelNewA);
		    	click(sel_TimeInSelNewA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelNewA);
		    	click(sel_TimeOutSelNewA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj2 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj2);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);
		    	//===========================================================================
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that able to draft the work sheet which having 2 different job orders with same employee but different worked hours", 
								"User Can draft the work sheet which having 2 different job orders with same employee but different worked hours",
								"User Can draft the work sheet which having 2 different job orders with same employee but different worked hours Successfully", "pass");
				} else {
						writeTestResults("Verify that able to draft the work sheet which having 2 different job orders with same employee but different worked hours", 
								"User Can draft the work sheet which having 2 different job orders with same employee but different worked hours",
								"User Can draft the work sheet which having 2 different job orders with same employee but different worked hours Fail", "fail");
				}
				
				//=====================================================================================
				
				WaitClick(btn_ReleaseA);
				click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String DWSNo =trackCode;
				
				WaitElement(txt_PageRelease2A);
				if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that able to Release the work sheet which having 2 different job orders with same employee but different worked hours", 
								"User Can Release the work sheet which having 2 different job orders with same employee but different worked hours",
							"User Can Release the work sheet which having 2 different job orders with same employee but different worked hours Successfully", "pass");
				} else {
						writeTestResults("Verify that able to Release the work sheet which having 2 different job orders with same employee but different worked hours",
								"User Can Release the work sheet which having 2 different job orders with same employee but different worked hours",
								"User Can Release the work sheet which having 2 different job orders with same employee but different worked hours Fail", "fail");
				}
				
				WaitElement(txt_FirstPostDateA);
				if (isDisplayed(txt_FirstPostDateA) && isDisplayed(txt_SecondPostDateA)) {
						writeTestResults("Verify that able to create a one daily work sheet for different job dates", 
								"User able to create a one daily work sheet for different job dates",
							"User able to create a one daily work sheet for different job dates Successfully", "pass");
				} else {
						writeTestResults("Verify that able to create a one daily work sheet for different job dates",
								"User able to create a one daily work sheet for different job dates",
								"User able to create a one daily work sheet for different job dates Fail", "fail");
				}
		    }
		    
		    //Service_DWS_087
		    public void VerifyThatUserAbleToChangeThePostDateOfTheDailyWorkSheetBeforeDraft() throws Exception {
		
		    	WaitElement(btn_DailyWorkSheetBtnA);
		    	click(btn_DailyWorkSheetBtnA);
		    	
		    	WaitElement(btn_NewDailyWorksheetBtnA);
		    	click(btn_NewDailyWorksheetBtnA);
		    	
		    	WaitElement(btn_DailyWorkSheetServiceBtnA);
		    	click(btn_DailyWorkSheetServiceBtnA);
		    	
		    	if (getText(txt_WorkedHoursServiceNewPageHeaderA).contentEquals("Worked Hours - New")) {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
							"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitElement(txt_AddNewJobHeaderA);
		    	if (isDisplayed(txt_AddNewJobHeaderA)) {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
							"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Fail", "fail");
		    	}
		    	//=======================================================================================================================
		    	
		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);
		    	
		    	WaitClick(btn_NewEmployeeBtnA);
		    	click(btn_NewEmployeeBtnA);
		    	
		    	switchWindow();
		    	
		    	LocalTime emp = LocalTime.now();
		    	
		    	sendKeys(txt_EmployeeCodeA, ""+emp);
		    	sendKeys(txt_EmployeeNoA, ""+emp);
		    	sendKeys(txt_FullNameA, "EMP");
		    	sendKeys(txt_NameWithInitialsA, "EMP");
		    	selectText(txt_EmployeeGroupA, "Test Service");
		    	
		    	WaitClick(btn_RateProfileSearchBtnA);
		    	click(btn_RateProfileSearchBtnA);
		    	WaitElement(txt_RateProfileSearchTxtA);
		    	sendKeys(txt_RateProfileSearchTxtA, "Project");
		    	pressEnter(txt_RateProfileSearchTxtA);
		    	WaitClick(sel_RateProfileSearchSelA);
		    	doubleClick(sel_RateProfileSearchSelA);
		    	
		    	WaitClick(btn_WorkingCalendarSearchBtnA);
		    	click(btn_WorkingCalendarSearchBtnA);
		    	WaitElement(txt_WorkingCalendarSearchTxtA);
		    	sendKeys(txt_WorkingCalendarSearchTxtA, "General Calender");
		    	pressEnter(txt_WorkingCalendarSearchTxtA);
		    	WaitClick(sel_WorkingCalendarSearchSelA);
		    	doubleClick(sel_WorkingCalendarSearchSelA);
		    
		    	WaitClick(btn_PayableAccountTickA);
		    	click(btn_PayableAccountTickA);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				String EMP = emp+" [EMP]";
				
				closeWindow();
		    	
		    	//=======================================================================================================================
				
				pageRefersh();
				
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);

		    	WaitClick(btn_ServiceJobOrderSearchBtnA);
		    	click(btn_ServiceJobOrderSearchBtnA);

		    	WaitElement(txt_ServiceJobOrderLookupSearchTxtA);
		    	sendKeys(txt_ServiceJobOrderLookupSearchTxtA, ReleseServiceJobOrderNos1A);
		    	
		    	WaitClick(btn_ServiceJobOrderLookupSearchBtnA);
		    	click(btn_ServiceJobOrderLookupSearchBtnA);
		    	
		    	WaitClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));
		    	doubleClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP));

		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelA);
		    	click(sel_TimeInSelA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelA);
		    	click(sel_TimeOutSelA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj1 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj1);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);

		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that User able to draft the daily work sheet Form", 
								"User Can draft the daily work sheet Form",
								"User Can draft the daily work sheet Form Successfully", "pass");
				} else {
						writeTestResults("Verify that User able to draft the daily work sheet Form", 
								"User Can draft the daily work sheet Form",
								"User Can draft the daily work sheet Form Fail", "fail");
				}
				
		    	WaitClick(btn_EditA);
		    	click(btn_EditA);
				
				WaitClick(txt_PostDateHeaderA);
				String PostDateHeaderA = getAttribute(txt_PostDateHeaderA, "disabled");
				if (PostDateHeaderA.equals("true")) {
					writeTestResults("Verify that user cannot change the post date after draft - Edit mode", 
							"user cannot change the post date after draft - Edit mode",
							"user cannot change the post date after draft - Edit mode Successfully", "pass");
				} else {
					writeTestResults("Verify that user cannot change the post date after draft - Edit mode", 
							"user cannot change the post date after draft - Edit mode",
							"user cannot change the post date after draft - Edit mode Fail", "fail");
				}
		    }
		    
		    //Service_DWS_088to092
		    public void VerifyThatFollowingTabsAreAvailableInTheWorkedHoursNewFormPostsTC() throws Exception {
		    	
		    	//============================================================Service_DWS_088
		    	WaitElement(btn_DailyWorkSheetBtnA);
		    	click(btn_DailyWorkSheetBtnA);
		    	
		    	WaitElement(btn_NewDailyWorksheetBtnA);
		    	click(btn_NewDailyWorksheetBtnA);
		    	
		    	WaitElement(btn_DailyWorkSheetServiceBtnA);
		    	click(btn_DailyWorkSheetServiceBtnA);
		    	
		    	if (getText(txt_WorkedHoursServiceNewPageHeaderA).contentEquals("Worked Hours - New")) {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
							"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Fail", "fail");
		    	}
		    	
		    	WaitElement(btn_PostsBtnA);
		    	if (isDisplayed(btn_PostsBtnA) && isDisplayed(btn_TandCBtnA)) {
						writeTestResults("Verify that following tabs are available in the \"Worked Hours - New\" form Posts,T&C", 
								"following tabs are available in the \"Worked Hours - New\" form Posts,T&C",
							"following tabs are available in the \"Worked Hours - New\" form Posts,T&C Successfully", "pass");
		    	} else {
						writeTestResults("Verify that following tabs are available in the \"Worked Hours - New\" form Posts,T&C", 
								"following tabs are available in the \"Worked Hours - New\" form Posts,T&C",
								"following tabs are available in the \"Worked Hours - New\" form Posts,T&C Fail", "fail");
		    	}
		    	
		    	//============================================================Service_DWS_089
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitElement(txt_AddNewJobHeaderA);
		    	if (isDisplayed(txt_AddNewJobHeaderA)) {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
							"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);
		    	
		    	WaitClick(btn_NewEmployeeBtnA);
		    	click(btn_NewEmployeeBtnA);
		    	
		    	switchWindow();
		    	
		    	LocalTime emp1 = LocalTime.now();
		    	
		    	sendKeys(txt_EmployeeCodeA, ""+emp1);
		    	sendKeys(txt_EmployeeNoA, ""+emp1);
		    	sendKeys(txt_FullNameA, "EMP");
		    	sendKeys(txt_NameWithInitialsA, "EMP");
		    	selectText(txt_EmployeeGroupA, "Test Service");
		    	
		    	WaitClick(btn_RateProfileSearchBtnA);
		    	click(btn_RateProfileSearchBtnA);
		    	WaitElement(txt_RateProfileSearchTxtA);
		    	sendKeys(txt_RateProfileSearchTxtA, "Project");
		    	pressEnter(txt_RateProfileSearchTxtA);
		    	WaitClick(sel_RateProfileSearchSelA);
		    	doubleClick(sel_RateProfileSearchSelA);
		    	
		    	WaitClick(btn_WorkingCalendarSearchBtnA);
		    	click(btn_WorkingCalendarSearchBtnA);
		    	WaitElement(txt_WorkingCalendarSearchTxtA);
		    	sendKeys(txt_WorkingCalendarSearchTxtA, "General Calender");
		    	pressEnter(txt_WorkingCalendarSearchTxtA);
		    	WaitClick(sel_WorkingCalendarSearchSelA);
		    	doubleClick(sel_WorkingCalendarSearchSelA);
		    
		    	WaitClick(btn_PayableAccountTickA);
		    	click(btn_PayableAccountTickA);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				String EMP1= emp1+" [EMP]";
				
				closeWindow();
		    	
		    	//=======================================================================================================================
				
				pageRefersh();
				
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);

		    	WaitClick(btn_ServiceJobOrderSearchBtnA);
		    	click(btn_ServiceJobOrderSearchBtnA);

		    	WaitElement(txt_ServiceJobOrderLookupSearchTxtA);
		    	sendKeys(txt_ServiceJobOrderLookupSearchTxtA, ReleseServiceJobOrderNos1A);
		    	
		    	WaitClick(btn_ServiceJobOrderLookupSearchBtnA);
		    	click(btn_ServiceJobOrderLookupSearchBtnA);
		    	
		    	WaitClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));
		    	doubleClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP1);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP1));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP1));

		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelA);
		    	click(sel_TimeInSelA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelA);
		    	click(sel_TimeOutSelA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj1 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj1);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);

		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that User able to draft the daily work sheet Form", 
								"User Can draft the daily work sheet Form",
								"User Can draft the daily work sheet Form Successfully", "pass");
				} else {
						writeTestResults("Verify that User able to draft the daily work sheet Form", 
								"User Can draft the daily work sheet Form",
								"User Can draft the daily work sheet Form Fail", "fail");
				}
				
				WaitElement(btn_AttachmentsBtnA);
				if (isDisplayed(btn_AttachmentsBtnA)) {
						writeTestResults("Verify that \"Attachments\" tab is available in the form after draft", 
								"\"Attachments\" tab is available in the form after draft",
								"\"Attachments\" tab is available in the form after draft Successfully", "pass");
				} else {
						writeTestResults("Verify that \"Attachments\" tab is available in the form after draft", 
								"\"Attachments\" tab is available in the form after draft",
								"\"Attachments\" tab is available in the form after draft Fail", "fail");
				}
				
				//============================================================Service_DWS_090
				
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String DWSNo1 = trackCode;
				
				WaitElement(txt_PageRelease2A);
				if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that User able to release the daily work sheet Form", 
								"User Can release the daily work sheet Form",
							"User Can release the daily work sheet Form Successfully", "pass");
				} else {
						writeTestResults("Verify that User able to release the daily work sheet Form",
								"User Can release the daily work sheet Form",
								"User Can release the daily work sheet Form Fail", "fail");
				}
				
		    	WaitClick(btn_DailyWorkSheetHeaderBtnA);
		    	click(btn_DailyWorkSheetHeaderBtnA);
		    	WaitElement(txt_DailyWorkSheetByPageTxtA);
		    	sendKeys(txt_DailyWorkSheetByPageTxtA, DWSNo1);
		    	pressEnter(txt_DailyWorkSheetByPageTxtA);
		    	
		    	WaitElement(sel_DailyWorkSheetByPageSelA.replace("DWSNo", DWSNo1));
				if (isDisplayed(sel_DailyWorkSheetByPageSelA.replace("DWSNo", DWSNo1))) {
					writeTestResults("Verify that released daily work sheets are displayed in the by page", 
							"released daily work sheets are displayed in the by page",
						"released daily work sheets are displayed in the by page Successfully", "pass");
				} else {
					writeTestResults("Verify that released daily work sheets are displayed in the by page",
							"released daily work sheets are displayed in the by page",
							"released daily work sheets are displayed in the by page Fail", "fail");
				}
		    	
				//============================================================Service_DWS_091
		    	
		    	WaitElement(btn_NewDailyWorksheetBtnA);
		    	click(btn_NewDailyWorksheetBtnA);
		    	
		    	WaitElement(btn_DailyWorkSheetServiceBtnA);
		    	click(btn_DailyWorkSheetServiceBtnA);
		    	
		    	if (getText(txt_WorkedHoursServiceNewPageHeaderA).contentEquals("Worked Hours - New")) {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
							"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey",
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Service) \" journey Fail", "fail");
		    	}
		    	
		    	WaitElement(btn_PostsBtnA);
		    	if (isDisplayed(btn_PostsBtnA) && isDisplayed(btn_TandCBtnA)) {
						writeTestResults("Verify that following tabs are available in the \"Worked Hours - New\" form Posts,T&C", 
								"following tabs are available in the \"Worked Hours - New\" form Posts,T&C",
							"following tabs are available in the \"Worked Hours - New\" form Posts,T&C Successfully", "pass");
		    	} else {
						writeTestResults("Verify that following tabs are available in the \"Worked Hours - New\" form Posts,T&C", 
								"following tabs are available in the \"Worked Hours - New\" form Posts,T&C",
								"following tabs are available in the \"Worked Hours - New\" form Posts,T&C Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitElement(txt_AddNewJobHeaderA);
		    	if (isDisplayed(txt_AddNewJobHeaderA)) {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
							"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"", 
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\"",
								"\"ADD NEW JOB\" look up is displayed when click on the \"Add New Record (+) icon\" Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);
		    	
		    	WaitClick(btn_NewEmployeeBtnA);
		    	click(btn_NewEmployeeBtnA);
		    	
		    	switchWindow();
		    	
		    	LocalTime emp2 = LocalTime.now();
		    	
		    	sendKeys(txt_EmployeeCodeA, ""+emp2);
		    	sendKeys(txt_EmployeeNoA, ""+emp2);
		    	sendKeys(txt_FullNameA, "EMP");
		    	sendKeys(txt_NameWithInitialsA, "EMP");
		    	selectText(txt_EmployeeGroupA, "Test Service");
		    	
		    	WaitClick(btn_RateProfileSearchBtnA);
		    	click(btn_RateProfileSearchBtnA);
		    	WaitElement(txt_RateProfileSearchTxtA);
		    	sendKeys(txt_RateProfileSearchTxtA, "Project");
		    	pressEnter(txt_RateProfileSearchTxtA);
		    	WaitClick(sel_RateProfileSearchSelA);
		    	doubleClick(sel_RateProfileSearchSelA);
		    	
		    	WaitClick(btn_WorkingCalendarSearchBtnA);
		    	click(btn_WorkingCalendarSearchBtnA);
		    	WaitElement(txt_WorkingCalendarSearchTxtA);
		    	sendKeys(txt_WorkingCalendarSearchTxtA, "General Calender");
		    	pressEnter(txt_WorkingCalendarSearchTxtA);
		    	WaitClick(sel_WorkingCalendarSearchSelA);
		    	doubleClick(sel_WorkingCalendarSearchSelA);
		    
		    	WaitClick(btn_PayableAccountTickA);
		    	click(btn_PayableAccountTickA);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				String EMP2 = emp2+" [EMP]";
				
				closeWindow();
		    	
		    	//=======================================================================================================================
				
				pageRefersh();
				
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);

		    	WaitClick(btn_ServiceJobOrderSearchBtnA);
		    	click(btn_ServiceJobOrderSearchBtnA);

		    	WaitElement(txt_ServiceJobOrderLookupSearchTxtA);
		    	sendKeys(txt_ServiceJobOrderLookupSearchTxtA, ReleseServiceJobOrderNos1A);
		    	
		    	WaitClick(btn_ServiceJobOrderLookupSearchBtnA);
		    	click(btn_ServiceJobOrderLookupSearchBtnA);
		    	
		    	WaitClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));
		    	doubleClick(txt_ReleseServiceJobOrderNosA.replace("SJONo", ReleseServiceJobOrderNos1A));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP2);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP2));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP2));

		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelA);
		    	click(sel_TimeInSelA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelA);
		    	click(sel_TimeOutSelA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj2 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj2);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);

		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
		    	String DWSNo2 = trackCode;
		    	
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that User able to draft the daily work sheet Form", 
								"User Can draft the daily work sheet Form",
								"User Can draft the daily work sheet Form Successfully", "pass");
				} else {
						writeTestResults("Verify that User able to draft the daily work sheet Form", 
								"User Can draft the daily work sheet Form",
								"User Can draft the daily work sheet Form Fail", "fail");
				}
				
		    	WaitClick(btn_DailyWorkSheetHeaderBtnA);
		    	click(btn_DailyWorkSheetHeaderBtnA);
		    	WaitElement(txt_DailyWorkSheetByPageTxtA);
		    	sendKeys(txt_DailyWorkSheetByPageTxtA, DWSNo2);
		    	pressEnter(txt_DailyWorkSheetByPageTxtA);
		    	
		    	WaitElement(sel_DailyWorkSheetByPageSelA.replace("DWSNo", DWSNo2));
				if (isDisplayed(sel_DailyWorkSheetByPageSelA.replace("DWSNo", DWSNo2))) {
					writeTestResults("Verify that draft daily work sheets are displayed in the by page", 
							"draft daily work sheets are displayed in the by page",
						"draft daily work sheets are displayed in the by page Successfully", "pass");
					doubleClick(sel_DailyWorkSheetByPageSelA.replace("DWSNo", DWSNo2));
				} else {
					writeTestResults("Verify that draft daily work sheets are displayed in the by page",
							"draft daily work sheets are displayed in the by page",
							"draft daily work sheets are displayed in the by page Fail", "fail");
				}
				
				//============================================================Service_DWS_092
				
				WaitElement(btn_ActionBtnA);
				click(btn_ActionBtnA);
				WaitClick(btn_DeleteBtnA);
				click(btn_DeleteBtnA);
				WaitClick(btn_YesBtnA);
				click(btn_YesBtnA);
				
				WaitElement(txt_pageDeletedNewA);
				if (isDisplayed(txt_pageDeletedNewA)) {
					writeTestResults("Verify that able to delete the Draft daily work sheet Form",
							"User Can delete the Draft Case daily work sheet Form",
							"User Can delete the Draft Case daily work sheet Form Successfully", "pass");
				} else {
					writeTestResults("Verify that able to delete the Draft daily work sheet Form",
							"User Can delete the Draft daily work sheet Form",
							"User Can delete the Draft daily work sheet Form Fail", "fail");
				}
				
		    	WaitClick(btn_DailyWorkSheetHeaderBtnA);
		    	click(btn_DailyWorkSheetHeaderBtnA);
		    	WaitElement(txt_DailyWorkSheetByPageTxtA);
		    	sendKeys(txt_DailyWorkSheetByPageTxtA, DWSNo2);
		    	pressEnter(txt_DailyWorkSheetByPageTxtA);
		    	
		    	WaitElement(sel_DailyWorkSheetByPageSelA.replace("DWSNo", DWSNo2));
				if (isDisplayed(sel_DailyWorkSheetByPageSelA.replace("DWSNo", DWSNo2))) {
					writeTestResults("Verify that Deleted daily work sheets are displayed in the by page", 
							"Deleted daily work sheets are displayed in the by page",
						"Deleted daily work sheets are displayed in the by page Successfully", "pass");
				} else {
					writeTestResults("Verify that Deleted daily work sheets are displayed in the by page",
							"Deleted daily work sheets are displayed in the by page",
							"Deleted daily work sheets are displayed in the by page Fail", "fail");
				}
				
		    }
		    
		    //Service_DWS_093to096
		    public void VerifyThatUserAbleToNavigatedToTheWorkedHoursNewFormWhenClickOnTheDailyWorkSheetProjectJourney() throws Exception {
		    	
		    	//============================================================Service_DWS_093
		    	WaitElement(btn_DailyWorkSheetBtnA);
		    	click(btn_DailyWorkSheetBtnA);
		    	
		    	WaitElement(btn_NewDailyWorksheetBtnA);
		    	click(btn_NewDailyWorksheetBtnA);
		    	
		    	WaitElement(btn_DailyWorkSheetProjectBtnA);
		    	click(btn_DailyWorkSheetProjectBtnA);
		    	
		    	if (getText(txt_WorkedHoursProjectNewPageHeaderA).contentEquals("Worked Hours - New")) {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey",
							"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey",
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey Fail", "fail");
		    	}
		    	
		    	//============================================================Service_DWS_094
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitElement(txt_AddNewJobHeaderA);
		    	if (isDisplayed(txt_AddNewJobHeaderA)) {
						writeTestResults("Verify that \"Project task look up\" is displayed when click on the \"Job No\" field", 
								"\"Project task look up\" is displayed when click on the \"Job No\" field",
							"\"Project task look up\" is displayed when click on the \"Job No\" field Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"Project task look up\" is displayed when click on the \"Job No\" field", 
								"\"Project task look up\" is displayed when click on the \"Job No\" field",
								"\"Project task look up\" is displayed when click on the \"Job No\" field Fail", "fail");
		    	}
		    	
		    	//============================================================Service_DWS_095
		    	
		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);
		    	
		    	WaitClick(btn_NewEmployeeBtnA);
		    	click(btn_NewEmployeeBtnA);
		    	
		    	switchWindow();
		    	
		    	LocalTime emp1 = LocalTime.now();
		    	
		    	sendKeys(txt_EmployeeCodeA, ""+emp1);
		    	sendKeys(txt_EmployeeNoA, ""+emp1);
		    	sendKeys(txt_FullNameA, "EMP");
		    	sendKeys(txt_NameWithInitialsA, "EMP");
		    	selectText(txt_EmployeeGroupA, "Test Service");
		    	
		    	WaitClick(btn_RateProfileSearchBtnA);
		    	click(btn_RateProfileSearchBtnA);
		    	WaitElement(txt_RateProfileSearchTxtA);
		    	sendKeys(txt_RateProfileSearchTxtA, "Project");
		    	pressEnter(txt_RateProfileSearchTxtA);
		    	WaitClick(sel_RateProfileSearchSelA);
		    	doubleClick(sel_RateProfileSearchSelA);
		    	
		    	WaitClick(btn_WorkingCalendarSearchBtnA);
		    	click(btn_WorkingCalendarSearchBtnA);
		    	WaitElement(txt_WorkingCalendarSearchTxtA);
		    	sendKeys(txt_WorkingCalendarSearchTxtA, "General Calender");
		    	pressEnter(txt_WorkingCalendarSearchTxtA);
		    	WaitClick(sel_WorkingCalendarSearchSelA);
		    	doubleClick(sel_WorkingCalendarSearchSelA);
		    
		    	WaitClick(btn_PayableAccountTickA);
		    	click(btn_PayableAccountTickA);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				String EMP1= emp1+" [EMP]";
				
				closeWindow();
		    	
		    	//=======================================================================================================================
				
				pageRefersh();
				
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);

		    	WaitClick(btn_ProjectSearchBtnA);
		    	click(btn_ProjectSearchBtnA);

		    	WaitElement(txt_ProjectSearchTxtA);
		    	sendKeys(txt_ProjectSearchTxtA, ReleseProjectTxt1A);
		    	pressEnter(txt_ProjectSearchTxtA);
		    	
		    	WaitClick(sel_ProjectSearchSelA.replace("ProNo", ReleseProjectTxt1A));
		    	doubleClick(sel_ProjectSearchSelA.replace("ProNo", ReleseProjectTxt1A));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP1);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP1));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP1));

		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelA);
		    	click(sel_TimeInSelA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelA);
		    	click(sel_TimeOutSelA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj1 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj1);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);

		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that able to draft the daily work sheet for project", 
								"User able to draft the daily work sheet for project",
								"User able to draft the daily work sheet for project Successfully", "pass");
				} else {
						writeTestResults("Verify that able to draft the daily work sheet for project", 
								"User able to draft the daily work sheet for project",
								"User able to draft the daily work sheet for project Fail", "fail");
				}
				
				//============================================================Service_DWS_095
				
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				
				WaitElement(txt_PageRelease2A);
				if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that able to release the work sheet", 
								"User able to release the work sheet",
							"User able to release the work sheet Successfully", "pass");
				} else {
						writeTestResults("Verify that able to release the work sheet",
								"User able to release the work sheet",
								"User able to release the work sheet Fail", "fail");
				}
		    }
		    
		    //Service_DWS_097
		    public void VerifyThatAbleToDeleteTheWorkSheet() throws Exception {
		    	
		    	WaitElement(btn_DailyWorkSheetBtnA);
		    	click(btn_DailyWorkSheetBtnA);
		    	
		    	WaitElement(btn_NewDailyWorksheetBtnA);
		    	click(btn_NewDailyWorksheetBtnA);
		    	
		    	WaitElement(btn_DailyWorkSheetProjectBtnA);
		    	click(btn_DailyWorkSheetProjectBtnA);
		    	
		    	if (getText(txt_WorkedHoursProjectNewPageHeaderA).contentEquals("Worked Hours - New")) {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey",
							"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey",
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitElement(txt_AddNewJobHeaderA);
		    	if (isDisplayed(txt_AddNewJobHeaderA)) {
						writeTestResults("Verify that \"Project task look up\" is displayed when click on the \"Job No\" field", 
								"\"Project task look up\" is displayed when click on the \"Job No\" field",
							"\"Project task look up\" is displayed when click on the \"Job No\" field Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"Project task look up\" is displayed when click on the \"Job No\" field", 
								"\"Project task look up\" is displayed when click on the \"Job No\" field",
								"\"Project task look up\" is displayed when click on the \"Job No\" field Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);
		    	
		    	WaitClick(btn_NewEmployeeBtnA);
		    	click(btn_NewEmployeeBtnA);
		    	
		    	switchWindow();
		    	
		    	LocalTime emp1 = LocalTime.now();
		    	
		    	sendKeys(txt_EmployeeCodeA, ""+emp1);
		    	sendKeys(txt_EmployeeNoA, ""+emp1);
		    	sendKeys(txt_FullNameA, "EMP");
		    	sendKeys(txt_NameWithInitialsA, "EMP");
		    	selectText(txt_EmployeeGroupA, "Test Service");
		    	
		    	WaitClick(btn_RateProfileSearchBtnA);
		    	click(btn_RateProfileSearchBtnA);
		    	WaitElement(txt_RateProfileSearchTxtA);
		    	sendKeys(txt_RateProfileSearchTxtA, "Project");
		    	pressEnter(txt_RateProfileSearchTxtA);
		    	WaitClick(sel_RateProfileSearchSelA);
		    	doubleClick(sel_RateProfileSearchSelA);
		    	
		    	WaitClick(btn_WorkingCalendarSearchBtnA);
		    	click(btn_WorkingCalendarSearchBtnA);
		    	WaitElement(txt_WorkingCalendarSearchTxtA);
		    	sendKeys(txt_WorkingCalendarSearchTxtA, "General Calender");
		    	pressEnter(txt_WorkingCalendarSearchTxtA);
		    	WaitClick(sel_WorkingCalendarSearchSelA);
		    	doubleClick(sel_WorkingCalendarSearchSelA);
		    
		    	WaitClick(btn_PayableAccountTickA);
		    	click(btn_PayableAccountTickA);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				String EMP1= emp1+" [EMP]";
				
				closeWindow();
				
				pageRefersh();
				
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);

		    	WaitClick(btn_ProjectSearchBtnA);
		    	click(btn_ProjectSearchBtnA);

		    	WaitElement(txt_ProjectSearchTxtA);
		    	sendKeys(txt_ProjectSearchTxtA, ReleseProjectTxt1A);
		    	pressEnter(txt_ProjectSearchTxtA);
		    	
		    	WaitClick(sel_ProjectSearchSelA.replace("ProNo", ReleseProjectTxt1A));
		    	doubleClick(sel_ProjectSearchSelA.replace("ProNo", ReleseProjectTxt1A));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP1);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP1));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP1));

		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelA);
		    	click(sel_TimeInSelA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelA);
		    	click(sel_TimeOutSelA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj1 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj1);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);

		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that able to draft the daily work sheet for project", 
								"User able to draft the daily work sheet for project",
								"User able to draft the daily work sheet for project Successfully", "pass");
				} else {
						writeTestResults("Verify that able to draft the daily work sheet for project", 
								"User able to draft the daily work sheet for project",
								"User able to draft the daily work sheet for project Fail", "fail");
				}
				
				WaitElement(btn_ActionBtnA);
				click(btn_ActionBtnA);
				WaitClick(btn_DeleteBtnA);
				click(btn_DeleteBtnA);
				WaitClick(btn_YesBtnA);
				click(btn_YesBtnA);
				
				WaitElement(txt_pageDeletedNewA);
				if (isDisplayed(txt_pageDeletedNewA)) {
					writeTestResults("Verify that able to delete the work sheet",
							"User able to delete the work sheet",
							"User able to delete the work sheet Successfully", "pass");
				} else {
					writeTestResults("Verify that able to delete the work sheet",
							"User able to delete the work sheet",
							"User able to delete the work sheet Fail", "fail");
				}
		    }
		    
		    //Service_DWS_098
		    public void VerifyThatAbleToReverseTheWorkSheet() throws Exception {
		    	
		    	WaitElement(btn_DailyWorkSheetBtnA);
		    	click(btn_DailyWorkSheetBtnA);
		    	
		    	WaitElement(btn_NewDailyWorksheetBtnA);
		    	click(btn_NewDailyWorksheetBtnA);
		    	
		    	WaitElement(btn_DailyWorkSheetProjectBtnA);
		    	click(btn_DailyWorkSheetProjectBtnA);
		    	
		    	if (getText(txt_WorkedHoursProjectNewPageHeaderA).contentEquals("Worked Hours - New")) {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey",
							"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey",
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitElement(txt_AddNewJobHeaderA);
		    	if (isDisplayed(txt_AddNewJobHeaderA)) {
						writeTestResults("Verify that \"Project task look up\" is displayed when click on the \"Job No\" field", 
								"\"Project task look up\" is displayed when click on the \"Job No\" field",
							"\"Project task look up\" is displayed when click on the \"Job No\" field Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"Project task look up\" is displayed when click on the \"Job No\" field", 
								"\"Project task look up\" is displayed when click on the \"Job No\" field",
								"\"Project task look up\" is displayed when click on the \"Job No\" field Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);
		    	
		    	WaitClick(btn_NewEmployeeBtnA);
		    	click(btn_NewEmployeeBtnA);
		    	
		    	switchWindow();
		    	
		    	LocalTime emp1 = LocalTime.now();
		    	
		    	sendKeys(txt_EmployeeCodeA, ""+emp1);
		    	sendKeys(txt_EmployeeNoA, ""+emp1);
		    	sendKeys(txt_FullNameA, "EMP");
		    	sendKeys(txt_NameWithInitialsA, "EMP");
		    	selectText(txt_EmployeeGroupA, "Test Service");
		    	
		    	WaitClick(btn_RateProfileSearchBtnA);
		    	click(btn_RateProfileSearchBtnA);
		    	WaitElement(txt_RateProfileSearchTxtA);
		    	sendKeys(txt_RateProfileSearchTxtA, "Project");
		    	pressEnter(txt_RateProfileSearchTxtA);
		    	WaitClick(sel_RateProfileSearchSelA);
		    	doubleClick(sel_RateProfileSearchSelA);
		    	
		    	WaitClick(btn_WorkingCalendarSearchBtnA);
		    	click(btn_WorkingCalendarSearchBtnA);
		    	WaitElement(txt_WorkingCalendarSearchTxtA);
		    	sendKeys(txt_WorkingCalendarSearchTxtA, "General Calender");
		    	pressEnter(txt_WorkingCalendarSearchTxtA);
		    	WaitClick(sel_WorkingCalendarSearchSelA);
		    	doubleClick(sel_WorkingCalendarSearchSelA);
		    
		    	WaitClick(btn_PayableAccountTickA);
		    	click(btn_PayableAccountTickA);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				String EMP1= emp1+" [EMP]";
				
				closeWindow();
				
				pageRefersh();
				
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);

		    	WaitClick(btn_ProjectSearchBtnA);
		    	click(btn_ProjectSearchBtnA);

		    	WaitElement(txt_ProjectSearchTxtA);
		    	sendKeys(txt_ProjectSearchTxtA, ReleseProjectTxt1A);
		    	pressEnter(txt_ProjectSearchTxtA);
		    	
		    	WaitClick(sel_ProjectSearchSelA.replace("ProNo", ReleseProjectTxt1A));
		    	doubleClick(sel_ProjectSearchSelA.replace("ProNo", ReleseProjectTxt1A));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP1);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP1));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP1));

		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelA);
		    	click(sel_TimeInSelA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelA);
		    	click(sel_TimeOutSelA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj1 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj1);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);

		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that able to draft the daily work sheet for project", 
								"User able to draft the daily work sheet for project",
								"User able to draft the daily work sheet for project Successfully", "pass");
				} else {
						writeTestResults("Verify that able to draft the daily work sheet for project", 
								"User able to draft the daily work sheet for project",
								"User able to draft the daily work sheet for project Fail", "fail");
				}
				
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				
				WaitElement(txt_PageRelease2A);
				if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that able to release the work sheet", 
								"User able to release the work sheet",
							"User able to release the work sheet Successfully", "pass");
				} else {
						writeTestResults("Verify that able to release the work sheet",
								"User able to release the work sheet",
								"User able to release the work sheet Fail", "fail");
				}
				
				WaitClick(btn_ActionBtnA);
			    click(btn_ActionBtnA);
			    WaitClick(btn_ReverseBtnA);
			    click(btn_ReverseBtnA);
				Thread.sleep(2000);
				JavascriptExecutor j7 = (JavascriptExecutor)driver;
				j7.executeScript("$('#txtrdcmnReverseDate').datepicker('setDate', new Date())");
				WaitElement(btn_reverceApplyA);
			    click(btn_reverceApplyA);
				WaitClick(btn_YesBtnA);
			    click(btn_YesBtnA);
			       
				WaitElement(txt_pageReversedNewA);
				if (isDisplayed(txt_pageReversedNewA)) {
						writeTestResults("Verify that able to reverse the work sheet", 
								"User able to reverse the work sheet",
								"User able to reverse the work sheet Successfully", "pass");
				} else {
						writeTestResults("Verify that able to reverse the work sheet", 
								"User able to reverse the work sheet",
								"User able to reverse the work sheet Fail", "fail");
				}
		    }
		    
		    //Service_DWS_099
		    public void VerifyThatReleasedWorkSheetIsDisplayedInTheOverviewTabOfRelevantProject() throws Exception {
		    	
		    	WaitElement(btn_DailyWorkSheetBtnA);
		    	click(btn_DailyWorkSheetBtnA);
		    	
		    	WaitElement(btn_NewDailyWorksheetBtnA);
		    	click(btn_NewDailyWorksheetBtnA);
		    	
		    	WaitElement(btn_DailyWorkSheetProjectBtnA);
		    	click(btn_DailyWorkSheetProjectBtnA);
		    	
		    	if (getText(txt_WorkedHoursProjectNewPageHeaderA).contentEquals("Worked Hours - New")) {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey",
							"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey",
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitElement(txt_AddNewJobHeaderA);
		    	if (isDisplayed(txt_AddNewJobHeaderA)) {
						writeTestResults("Verify that \"Project task look up\" is displayed when click on the \"Job No\" field", 
								"\"Project task look up\" is displayed when click on the \"Job No\" field",
							"\"Project task look up\" is displayed when click on the \"Job No\" field Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"Project task look up\" is displayed when click on the \"Job No\" field", 
								"\"Project task look up\" is displayed when click on the \"Job No\" field",
								"\"Project task look up\" is displayed when click on the \"Job No\" field Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);
		    	
		    	WaitClick(btn_NewEmployeeBtnA);
		    	click(btn_NewEmployeeBtnA);
		    	
		    	switchWindow();
		    	
		    	LocalTime emp1 = LocalTime.now();
		    	
		    	sendKeys(txt_EmployeeCodeA, ""+emp1);
		    	sendKeys(txt_EmployeeNoA, ""+emp1);
		    	sendKeys(txt_FullNameA, "EMP");
		    	sendKeys(txt_NameWithInitialsA, "EMP");
		    	selectText(txt_EmployeeGroupA, "Test Service");
		    	
		    	WaitClick(btn_RateProfileSearchBtnA);
		    	click(btn_RateProfileSearchBtnA);
		    	WaitElement(txt_RateProfileSearchTxtA);
		    	sendKeys(txt_RateProfileSearchTxtA, "Project");
		    	pressEnter(txt_RateProfileSearchTxtA);
		    	WaitClick(sel_RateProfileSearchSelA);
		    	doubleClick(sel_RateProfileSearchSelA);
		    	
		    	WaitClick(btn_WorkingCalendarSearchBtnA);
		    	click(btn_WorkingCalendarSearchBtnA);
		    	WaitElement(txt_WorkingCalendarSearchTxtA);
		    	sendKeys(txt_WorkingCalendarSearchTxtA, "General Calender");
		    	pressEnter(txt_WorkingCalendarSearchTxtA);
		    	WaitClick(sel_WorkingCalendarSearchSelA);
		    	doubleClick(sel_WorkingCalendarSearchSelA);
		    
		    	WaitClick(btn_PayableAccountTickA);
		    	click(btn_PayableAccountTickA);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				String EMP1= emp1+" [EMP]";
				
				closeWindow();
				
				pageRefersh();
				
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);

		    	WaitClick(btn_ProjectSearchBtnA);
		    	click(btn_ProjectSearchBtnA);

		    	WaitElement(txt_ProjectSearchTxtA);
		    	sendKeys(txt_ProjectSearchTxtA, ReleseProjectTxt1A);
		    	pressEnter(txt_ProjectSearchTxtA);
		    	
		    	WaitClick(sel_ProjectSearchSelA.replace("ProNo", ReleseProjectTxt1A));
		    	doubleClick(sel_ProjectSearchSelA.replace("ProNo", ReleseProjectTxt1A));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP1);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP1));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP1));

		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelA);
		    	click(sel_TimeInSelA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelA);
		    	click(sel_TimeOutSelA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj1 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj1);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);

		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that able to draft the daily work sheet for project", 
								"User able to draft the daily work sheet for project",
								"User able to draft the daily work sheet for project Successfully", "pass");
				} else {
						writeTestResults("Verify that able to draft the daily work sheet for project", 
								"User able to draft the daily work sheet for project",
								"User able to draft the daily work sheet for project Fail", "fail");
				}
				
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String DWSNo = trackCode;
				
				WaitElement(txt_PageRelease2A);
				if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that able to release the work sheet", 
								"User able to release the work sheet",
							"User able to release the work sheet Successfully", "pass");
				} else {
						writeTestResults("Verify that able to release the work sheet",
								"User able to release the work sheet",
								"User able to release the work sheet Fail", "fail");
				}
				
				ProjectModule Pro1 = new ProjectModule();
				
				WaitClick(btn_navigationmenu);
				click(btn_navigationmenu);
				Pro1.clickProjectbutton();
				
				WaitClick(btn_DWSProjectBtnA);
				click(btn_DWSProjectBtnA);
				
				WaitElement(txt_DWSProjectSearchByPageTxtA);
				sendKeys(txt_DWSProjectSearchByPageTxtA, ReleseProjectTxt1A);
				pressEnter(txt_DWSProjectSearchByPageTxtA);
				
				WaitElement(sel_DWSProjectSearchByPageSelA.replace("ProNo", ReleseProjectTxt1A));
				doubleClick(sel_DWSProjectSearchByPageSelA.replace("ProNo", ReleseProjectTxt1A));
				
				WaitClick(btn_DWSProjectOverviewTabA);
				click(btn_DWSProjectOverviewTabA);
				WaitClick(btn_LabourTabA);
				click(btn_LabourTabA);
				
				WaitElement(txt_DWSProjectDWSNoA.replace("DWSNo", DWSNo));
				if (isDisplayed(txt_DWSProjectDWSNoA.replace("DWSNo", DWSNo))) {
						writeTestResults("Verify that released work sheet is displayed in the \"Overview\" tab of relevant project", 
								"released work sheet is displayed in the \"Overview\" tab of relevant project",
							"released work sheet is displayed in the \"Overview\" tab of relevant project Successfully", "pass");
				} else {
						writeTestResults("Verify that released work sheet is displayed in the \"Overview\" tab of relevant project",
								"released work sheet is displayed in the \"Overview\" tab of relevant project",
								"released work sheet is displayed in the \"Overview\" tab of relevant project Fail", "fail");
				}
		    }
		    
		    //Service_DWS_100
		    public void VerifyThatAbleToReleaseOneWorkSheetForTwoDifferentProjectsByAddingSameEmployee() throws Exception {
		    	
		    	WaitElement(btn_DailyWorkSheetBtnA);
		    	click(btn_DailyWorkSheetBtnA);
		    	
		    	WaitElement(btn_NewDailyWorksheetBtnA);
		    	click(btn_NewDailyWorksheetBtnA);
		    	
		    	WaitElement(btn_DailyWorkSheetProjectBtnA);
		    	click(btn_DailyWorkSheetProjectBtnA);
		    	
		    	if (getText(txt_WorkedHoursProjectNewPageHeaderA).contentEquals("Worked Hours - New")) {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey",
							"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey",
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitElement(txt_AddNewJobHeaderA);
		    	if (isDisplayed(txt_AddNewJobHeaderA)) {
						writeTestResults("Verify that \"Project task look up\" is displayed when click on the \"Job No\" field", 
								"\"Project task look up\" is displayed when click on the \"Job No\" field",
							"\"Project task look up\" is displayed when click on the \"Job No\" field Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"Project task look up\" is displayed when click on the \"Job No\" field", 
								"\"Project task look up\" is displayed when click on the \"Job No\" field",
								"\"Project task look up\" is displayed when click on the \"Job No\" field Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);
		    	
		    	WaitClick(btn_NewEmployeeBtnA);
		    	click(btn_NewEmployeeBtnA);
		    	
		    	switchWindow();
		    	
		    	LocalTime emp1 = LocalTime.now();
		    	
		    	sendKeys(txt_EmployeeCodeA, ""+emp1);
		    	sendKeys(txt_EmployeeNoA, ""+emp1);
		    	sendKeys(txt_FullNameA, "EMP");
		    	sendKeys(txt_NameWithInitialsA, "EMP");
		    	selectText(txt_EmployeeGroupA, "Test Service");
		    	
		    	WaitClick(btn_RateProfileSearchBtnA);
		    	click(btn_RateProfileSearchBtnA);
		    	WaitElement(txt_RateProfileSearchTxtA);
		    	sendKeys(txt_RateProfileSearchTxtA, "Project");
		    	pressEnter(txt_RateProfileSearchTxtA);
		    	WaitClick(sel_RateProfileSearchSelA);
		    	doubleClick(sel_RateProfileSearchSelA);
		    	
		    	WaitClick(btn_WorkingCalendarSearchBtnA);
		    	click(btn_WorkingCalendarSearchBtnA);
		    	WaitElement(txt_WorkingCalendarSearchTxtA);
		    	sendKeys(txt_WorkingCalendarSearchTxtA, "General Calender");
		    	pressEnter(txt_WorkingCalendarSearchTxtA);
		    	WaitClick(sel_WorkingCalendarSearchSelA);
		    	doubleClick(sel_WorkingCalendarSearchSelA);
		    
		    	WaitClick(btn_PayableAccountTickA);
		    	click(btn_PayableAccountTickA);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				String EMP1= emp1+" [EMP]";
				
				closeWindow();
				
				pageRefersh();
				
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);

		    	WaitClick(btn_ProjectSearchBtnA);
		    	click(btn_ProjectSearchBtnA);

		    	WaitElement(txt_ProjectSearchTxtA);
		    	sendKeys(txt_ProjectSearchTxtA, ReleseProjectTxt1A);
		    	pressEnter(txt_ProjectSearchTxtA);
		    	
		    	WaitClick(sel_ProjectSearchSelA.replace("ProNo", ReleseProjectTxt1A));
		    	doubleClick(sel_ProjectSearchSelA.replace("ProNo", ReleseProjectTxt1A));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP1);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP1));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP1));

		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelA);
		    	click(sel_TimeInSelA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelA);
		    	click(sel_TimeOutSelA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj1 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj1);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);
		    	
		    	
		    //==================================================================================
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitClick(btn_ServiceJobOrderSearchBtnA);
		    	click(btn_ServiceJobOrderSearchBtnA);

		    	WaitElement(txt_ProjectSearchTxtA);
		    	sendKeys(txt_ProjectSearchTxtA, ReleseProjectTxt2A);
		    	pressEnter(txt_ProjectSearchTxtA);
		    	
		    	WaitClick(sel_ProjectSearchSelA.replace("ProNo", ReleseProjectTxt2A));
		    	doubleClick(sel_ProjectSearchSelA.replace("ProNo", ReleseProjectTxt2A));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP1);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP1));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP1));
		    	
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelNewA);
		    	click(sel_TimeInSelNewA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelNewA);
		    	click(sel_TimeOutSelNewA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj2 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj2);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);

		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that able to draft the daily work sheet for project", 
								"User able to draft the daily work sheet for project",
								"User able to draft the daily work sheet for project Successfully", "pass");
				} else {
						writeTestResults("Verify that able to draft the daily work sheet for project", 
								"User able to draft the daily work sheet for project",
								"User able to draft the daily work sheet for project Fail", "fail");
				}
				
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String DWSNo = trackCode;
				
		    	WaitElement(txt_DWSEmployeeNameLine1A);
		    	String DWSEmployeeNameLine1A= driver.findElement(By.xpath(txt_DWSEmployeeNameLine1A)).getAttribute("value");
		    	String DWSProNoLine1A= driver.findElement(By.xpath(txt_DWSProNoLine1A)).getAttribute("value");
		    	String DWSEmployeeNameLine2A= driver.findElement(By.xpath(txt_DWSEmployeeNameLine2A)).getAttribute("value");
		    	String DWSProNoLine2A= driver.findElement(By.xpath(txt_DWSProNoLine2A)).getAttribute("value");
		    	
				if (DWSEmployeeNameLine1A.contentEquals(EMP1) && DWSEmployeeNameLine2A.contentEquals(EMP1)
						 && DWSProNoLine1A.contentEquals(ReleseProjectTxt1A+"-2") && DWSProNoLine2A.contentEquals(ReleseProjectTxt2A+"-2")) {
						writeTestResults("Verify that able to release one work sheet for 2 different projects by adding same employee", 
								"User able to release one work sheet for 2 different projects by adding same employee",
							"User able to release one work sheet for 2 different projects by adding same employee Successfully", "pass");
				} else {
						writeTestResults("Verify that able to release one work sheet for 2 different projects by adding same employee ",
								"User able to release one work sheet for 2 different projects by adding same employee",
								"User able to release one work sheet for 2 different projects by adding same employee Fail", "fail");
				}
		    }
		    
		    //Service_DWS_101
		    public void VerifyThatAbleToReleaseOneWorkSheetForaProjectWithMultipleEmployees() throws Exception {
		    	
		    	WaitElement(btn_DailyWorkSheetBtnA);
		    	click(btn_DailyWorkSheetBtnA);
		    	
		    	WaitElement(btn_NewDailyWorksheetBtnA);
		    	click(btn_NewDailyWorksheetBtnA);
		    	
		    	WaitElement(btn_DailyWorkSheetProjectBtnA);
		    	click(btn_DailyWorkSheetProjectBtnA);
		    	
		    	if (getText(txt_WorkedHoursProjectNewPageHeaderA).contentEquals("Worked Hours - New")) {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey",
							"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey",
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitElement(txt_AddNewJobHeaderA);
		    	if (isDisplayed(txt_AddNewJobHeaderA)) {
						writeTestResults("Verify that \"Project task look up\" is displayed when click on the \"Job No\" field", 
								"\"Project task look up\" is displayed when click on the \"Job No\" field",
							"\"Project task look up\" is displayed when click on the \"Job No\" field Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"Project task look up\" is displayed when click on the \"Job No\" field", 
								"\"Project task look up\" is displayed when click on the \"Job No\" field",
								"\"Project task look up\" is displayed when click on the \"Job No\" field Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);
		    	
		    	WaitClick(btn_NewEmployeeBtnA);
		    	click(btn_NewEmployeeBtnA);
		    	
		    	switchWindow();
		    	
		    	LocalTime emp1 = LocalTime.now();
		    	
		    	sendKeys(txt_EmployeeCodeA, ""+emp1);
		    	sendKeys(txt_EmployeeNoA, ""+emp1);
		    	sendKeys(txt_FullNameA, "EMP");
		    	sendKeys(txt_NameWithInitialsA, "EMP");
		    	selectText(txt_EmployeeGroupA, "Test Service");
		    	
		    	WaitClick(btn_RateProfileSearchBtnA);
		    	click(btn_RateProfileSearchBtnA);
		    	WaitElement(txt_RateProfileSearchTxtA);
		    	sendKeys(txt_RateProfileSearchTxtA, "Project");
		    	pressEnter(txt_RateProfileSearchTxtA);
		    	WaitClick(sel_RateProfileSearchSelA);
		    	doubleClick(sel_RateProfileSearchSelA);
		    	
		    	WaitClick(btn_WorkingCalendarSearchBtnA);
		    	click(btn_WorkingCalendarSearchBtnA);
		    	WaitElement(txt_WorkingCalendarSearchTxtA);
		    	sendKeys(txt_WorkingCalendarSearchTxtA, "General Calender");
		    	pressEnter(txt_WorkingCalendarSearchTxtA);
		    	WaitClick(sel_WorkingCalendarSearchSelA);
		    	doubleClick(sel_WorkingCalendarSearchSelA);
		    
		    	WaitClick(btn_PayableAccountTickA);
		    	click(btn_PayableAccountTickA);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				String EMP1= emp1+" [EMP]";
				
				WaitClick(btn_DuplicateA);
				click(btn_DuplicateA);
				
				WaitElement(txt_EmployeeCodeA);
		    	LocalTime emp2 = LocalTime.now();
		    	
		    	sendKeys(txt_EmployeeCodeA, ""+emp2);
		    	sendKeys(txt_EmployeeNoA, ""+emp2);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				String EMP2 = emp2+" [EMP]";
				
				closeWindow();
				
				pageRefersh();
				
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);

		    	WaitClick(btn_ProjectSearchBtnA);
		    	click(btn_ProjectSearchBtnA);

		    	WaitElement(txt_ProjectSearchTxtA);
		    	sendKeys(txt_ProjectSearchTxtA, ReleseProjectTxt1A);
		    	pressEnter(txt_ProjectSearchTxtA);
		    	
		    	WaitClick(sel_ProjectSearchSelA.replace("ProNo", ReleseProjectTxt1A));
		    	doubleClick(sel_ProjectSearchSelA.replace("ProNo", ReleseProjectTxt1A));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP1);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP1));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP1));
		    	
		    	WaitClick(btn_EmployeeAddPlusBtnA);
		    	click(btn_EmployeeAddPlusBtnA);
		    	
		    	WaitClick(btn_EmployeeSearchBtn2A);
		    	click(btn_EmployeeSearchBtn2A);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP2);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP2));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP2));

		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelA);
		    	click(sel_TimeInSelA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelA);
		    	click(sel_TimeOutSelA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj1 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj1);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);

		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that able to draft the daily work sheet for project", 
								"User able to draft the daily work sheet for project",
								"User able to draft the daily work sheet for project Successfully", "pass");
				} else {
						writeTestResults("Verify that able to draft the daily work sheet for project", 
								"User able to draft the daily work sheet for project",
								"User able to draft the daily work sheet for project Fail", "fail");
				}
				
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				String DWSNo = trackCode;
				
		    	WaitElement(txt_DWSEmployeeNameLine1A);
		    	String DWSEmployeeNameLine1A= driver.findElement(By.xpath(txt_DWSEmployeeNameLine1A)).getAttribute("value");
		    	String DWSProNoLine1A= driver.findElement(By.xpath(txt_DWSProNoLine1A)).getAttribute("value");
		    	String DWSEmployeeNameLine2A= driver.findElement(By.xpath(txt_DWSEmployeeNameLine2A)).getAttribute("value");
		    	String DWSProNoLine2A= driver.findElement(By.xpath(txt_DWSProNoLine2A)).getAttribute("value");
		    	
				if (DWSEmployeeNameLine1A.contentEquals(EMP1) && DWSEmployeeNameLine2A.contentEquals(EMP2)
						 && DWSProNoLine1A.contentEquals(ReleseProjectTxt1A+"-2") && DWSProNoLine2A.contentEquals(ReleseProjectTxt1A+"-2")) {
						writeTestResults("Verify that able to release one work sheet for a project with multiple employees", 
								"User able to release one work sheet for a project with multiple employees",
							"User able to release one work sheet for a project with multiple employees Successfully", "pass");
				} else {
						writeTestResults("Verify that able to release one work sheet for a project with multiple employees",
								"User able to release one work sheet for a project with multiple employees",
								"User able to release one work sheet for a project with multiple employees Fail", "fail");
				}
		    }
		    
		    
		    //Service_DWS_102
		    public void VerifyThatWorkSheetCannotBeReverseWhenProjectIsCompleted() throws Exception {
		    	
		    	ProjectModule Pro1 = new ProjectModule();
		    	
				WaitClick(btn_navigationmenu);
				click(btn_navigationmenu);

				WaitElement(sidemenu);
				if (isDisplayed(sidemenu)) {
					writeTestResults("verify sidemenu", "view sidemenu", "sidemenu is display", "pass");
				} else {
					writeTestResults("verify sidemenu", "view sidemenu", "sidemenu is not display", "fail");
				}

				Pro1.clickProjectbutton();
				WaitElement(subsidemenu);
				if (isDisplayed(subsidemenu)) {
					writeTestResults("verify sub side menu", "view sub side menu", "sub side menu is display", "pass");
				} else {
					writeTestResults("verify sub side menu", "view sub side menu", "sub side menu is not display", "fail");
				}
				
				WaitClick(btn_DWSProjectBtnA);
				click(btn_DWSProjectBtnA);
				WaitClick(btn_DWSNewProjectBtnA);
				click(btn_DWSNewProjectBtnA);
				
				
				LocalTime Obj1 = LocalTime.now();
				
				WaitElement(txt_DWSProjectCodeA);
				sendKeys(txt_DWSProjectCodeA, "Pro"+Obj1);
				
				sendKeys(txt_DWSProjectTitleA, "Title"+Obj1);
				
				WaitElement(txt_DWSProjectGroupA);
				selectText(txt_DWSProjectGroupA, DWSProjectGroupA);
				
				WaitClick(btn_DWSResponsiblePersonBtnA);
				click(btn_DWSResponsiblePersonBtnA);
				WaitElement(txt_DWSResponsiblePersonTxtA);
				sendKeys(txt_DWSResponsiblePersonTxtA, OwnerTxtA);
				pressEnter(txt_DWSResponsiblePersonTxtA);
				WaitElement(sel_DWSResponsiblePersonSelA.replace("ResponsiblePerson", OwnerTxtA));
				doubleClick(sel_DWSResponsiblePersonSelA.replace("ResponsiblePerson", OwnerTxtA));
				
				WaitElement(txt_DWSAssignedBusinessUnitsA);
				selectText(txt_DWSAssignedBusinessUnitsA, DWSAssignedBusinessUnitsA);
				
				WaitClick(btn_DWSPricingProfileBtnA);
				click(btn_DWSPricingProfileBtnA);
				WaitElement(txt_DWSPricingProfileTxtA);
				sendKeys(txt_DWSPricingProfileTxtA, DWSPricingProfileSelA);
				pressEnter(txt_DWSPricingProfileTxtA);
				WaitElement(sel_DWSPricingProfileSelA.replace("PriPro", DWSPricingProfileSelA));
				doubleClick(sel_DWSPricingProfileSelA.replace("PriPro", DWSPricingProfileSelA));
				
				WaitClick(btn_DWSCustomerAccountBtnA);
				click(btn_DWSCustomerAccountBtnA);
				WaitElement(txt_DWSCustomerAccountTxtA);
				sendKeys(txt_DWSCustomerAccountTxtA, CustomerAccountCodeAndDicriptionA);
				pressEnter(txt_DWSCustomerAccountTxtA);
				WaitElement(sel_DWSCustomerAccountSelA.replace("CusAcc", CustomerAccountCodeAndDicriptionA));
				doubleClick(sel_DWSCustomerAccountSelA.replace("CusAcc", CustomerAccountCodeAndDicriptionA));
				
				WaitElement(txt_DWScboxSalesUnitA);
				selectText(txt_DWScboxSalesUnitA, SalesUnitA);
				
				WaitClick(btn_DWSRequestedStartDateA);
				click(btn_DWSRequestedStartDateA);
				WaitElement(sel_DWSRequestedStartDateselA);
				click(sel_DWSRequestedStartDateselA);
				
				WaitClick(btn_DWSProjectLocationBtnA);
				click(btn_DWSProjectLocationBtnA);
				WaitElement(txt_DWSProjectLocationTxtA);
				sendKeys(txt_DWSProjectLocationTxtA, ServiceLocationTxtA);
				pressEnter(txt_DWSProjectLocationTxtA);
				WaitElement(sel_DWSProjectLocationSelA.replace("ProLoc", ServiceLocationTxtA));
				doubleClick(sel_DWSProjectLocationSelA.replace("ProLoc", ServiceLocationTxtA));
				
				WaitClick(btn_DraftA);
				click(btn_DraftA);
				WaitElement(txt_PageDraft1A);
				if (isDisplayed(txt_PageDraft1A)){

					writeTestResults("verify Project Document Draft","Project Document Draft", "Project Document is Draft", "pass");
				} else {
					writeTestResults("verify Project Document Draft","Project Document Draft", "Project Document is not Draft", "fail");

				}
				
				WaitClick(btn_ReleaseA);
				click(btn_ReleaseA);
				
				Thread.sleep(3000);
				trackCode= getText(txt_trackCodeA);
				ProjectCode=trackCode;
				
				WaitElement(txt_PageRelease1A);
				if (isDisplayed(txt_PageRelease1A)){

					writeTestResults("verify Project Document Released","Project Document Released", "Project Document is Released", "pass");
				} else {
					writeTestResults("verify Project Document Released","Project Document Released", "Project Document is not Released", "fail");

				}
				
				WaitClick(btn_EditA);
				click(btn_EditA);
				
				WaitClick(btn_DWSWorkBreakdownBtnA);
				click(btn_DWSWorkBreakdownBtnA);
				WaitClick(btn_DWSWorkBreakdownAddTaskBtnA);
				click(btn_DWSWorkBreakdownAddTaskBtnA);
				
				WaitElement(txt_DWSWorkBreakdownTaskNameA);
				sendKeys(txt_DWSWorkBreakdownTaskNameA, "Task"+Obj1);
				
				WaitClick(btn_DWSTaskUpdateA);
				click(btn_DWSTaskUpdateA);
				
				pageRefersh();
				WaitClick(btn_DWSWorkBreakdownBtnA);
				click(btn_DWSWorkBreakdownBtnA);
				
				WaitClick(btn_DWSTaskdropdownA);
				click(btn_DWSTaskdropdownA);
				
				WaitClick(btn_DWSReleaseTaskA);
				click(btn_DWSReleaseTaskA);
				
				pageRefersh();
				
				WaitClick(btn_UpdateA);
				click(btn_UpdateA);
				
				clickNavigation();
				
		    	WaitElement(btn_DailyWorkSheetBtnA);
		    	click(btn_DailyWorkSheetBtnA);
		    	
		    	WaitElement(btn_NewDailyWorksheetBtnA);
		    	click(btn_NewDailyWorksheetBtnA);
		    	
		    	WaitElement(btn_DailyWorkSheetProjectBtnA);
		    	click(btn_DailyWorkSheetProjectBtnA);
		    	
		    	if (getText(txt_WorkedHoursProjectNewPageHeaderA).contentEquals("Worked Hours - New")) {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey",
							"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey Successfully", "pass");
		    	} else {
						writeTestResults("Verify that user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey", 
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey",
								"user able to navigated to the \"Worked Hours - New\" form when click on the \"Daily Work sheet (Project) \" journey Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);
		    	
		    	WaitElement(txt_AddNewJobHeaderA);
		    	if (isDisplayed(txt_AddNewJobHeaderA)) {
						writeTestResults("Verify that \"Project task look up\" is displayed when click on the \"Job No\" field", 
								"\"Project task look up\" is displayed when click on the \"Job No\" field",
							"\"Project task look up\" is displayed when click on the \"Job No\" field Successfully", "pass");
		    	} else {
						writeTestResults("Verify that \"Project task look up\" is displayed when click on the \"Job No\" field", 
								"\"Project task look up\" is displayed when click on the \"Job No\" field",
								"\"Project task look up\" is displayed when click on the \"Job No\" field Fail", "fail");
		    	}
		    	
		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);
		    	
		    	WaitClick(btn_NewEmployeeBtnA);
		    	click(btn_NewEmployeeBtnA);
		    	
		    	switchWindow();
		    	
		    	LocalTime emp1 = LocalTime.now();
		    	
		    	sendKeys(txt_EmployeeCodeA, ""+emp1);
		    	sendKeys(txt_EmployeeNoA, ""+emp1);
		    	sendKeys(txt_FullNameA, "EMP");
		    	sendKeys(txt_NameWithInitialsA, "EMP");
		    	selectText(txt_EmployeeGroupA, "Test Service");
		    	
		    	WaitClick(btn_RateProfileSearchBtnA);
		    	click(btn_RateProfileSearchBtnA);
		    	WaitElement(txt_RateProfileSearchTxtA);
		    	sendKeys(txt_RateProfileSearchTxtA, "Project");
		    	pressEnter(txt_RateProfileSearchTxtA);
		    	WaitClick(sel_RateProfileSearchSelA);
		    	doubleClick(sel_RateProfileSearchSelA);
		    	
		    	WaitClick(btn_WorkingCalendarSearchBtnA);
		    	click(btn_WorkingCalendarSearchBtnA);
		    	WaitElement(txt_WorkingCalendarSearchTxtA);
		    	sendKeys(txt_WorkingCalendarSearchTxtA, "General Calender");
		    	pressEnter(txt_WorkingCalendarSearchTxtA);
		    	WaitClick(sel_WorkingCalendarSearchSelA);
		    	doubleClick(sel_WorkingCalendarSearchSelA);
		    
		    	WaitClick(btn_PayableAccountTickA);
		    	click(btn_PayableAccountTickA);
		    	
		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				String EMP1= emp1+" [EMP]";
				
				closeWindow();
				
				pageRefersh();
				
		    	WaitClick(btn_AddNewRecordPlusA);
		    	click(btn_AddNewRecordPlusA);

		    	WaitClick(btn_ProjectSearchBtnA);
		    	click(btn_ProjectSearchBtnA);

		    	WaitElement(txt_ProjectSearchTxtA);
		    	sendKeys(txt_ProjectSearchTxtA, ProjectCode);
		    	pressEnter(txt_ProjectSearchTxtA);
		    	
		    	WaitClick(sel_ProjectSearchSelA.replace("ProNo", ProjectCode));
		    	doubleClick(sel_ProjectSearchSelA.replace("ProNo", ProjectCode));

		    	WaitClick(btn_EmployeeSearchBtnA);
		    	click(btn_EmployeeSearchBtnA);

		    	WaitElement(txt_EmployeeLookupSearchTxtA);
		    	sendKeys(txt_EmployeeLookupSearchTxtA, EMP1);
		    	pressEnter(txt_EmployeeLookupSearchTxtA);
		    	
		    	WaitElement(txt_ReleseEmployeeNamesA.replace("EName", EMP1));
		    	doubleClick(txt_ReleseEmployeeNamesA.replace("EName", EMP1));

		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInA);
		    	WaitClick(sel_TimeInSelA);
		    	click(sel_TimeInSelA);
		    	WaitClick(btn_TimeInA);
		    	click(btn_TimeInSelOkA);
		    	
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutA);
		    	WaitClick(sel_TimeOutSelA);
		    	click(sel_TimeOutSelA);
		    	WaitClick(btn_TimeOutA);
		    	click(btn_TimeOutSelOkA);
		    	
		    	WaitElement(txt_ReferenceNoTextFieldA);
		    	
		    	LocalTime Obj2 = LocalTime.now();
		    	sendKeys(txt_ReferenceNoTextFieldA, "REF"+Obj2);
		    	
		    	WaitClick(btn_AddNewJobUpdateA);
		    	click(btn_AddNewJobUpdateA);

		    	WaitClick(btn_DraftA);
		    	click(btn_DraftA);
		    	
				WaitElement(txt_PageDraft2A);
				if (isDisplayed(txt_PageDraft2A)) {
						writeTestResults("Verify that able to draft the daily work sheet for project", 
								"User able to draft the daily work sheet for project",
								"User able to draft the daily work sheet for project Successfully", "pass");
				} else {
						writeTestResults("Verify that able to draft the daily work sheet for project", 
								"User able to draft the daily work sheet for project",
								"User able to draft the daily work sheet for project Fail", "fail");
				}
				
		    	WaitClick(btn_ReleaseA);
		    	click(btn_ReleaseA);
				Thread.sleep(3000);
				trackCode = getText(txt_trackCodeA);
				
				WaitElement(txt_PageRelease2A);
				if (isDisplayed(txt_PageRelease2A)) {
						writeTestResults("Verify that able to release the work sheet", 
								"User able to release the work sheet",
							"User able to release the work sheet Successfully", "pass");
				} else {
						writeTestResults("Verify that able to release the work sheet",
								"User able to release the work sheet",
								"User able to release the work sheet Fail", "fail");
				}
				
				WaitClick(btn_navigationmenu);
				click(btn_navigationmenu);
				Pro1.clickProjectbutton();
				
				WaitClick(btn_DWSProjectBtnA);
				click(btn_DWSProjectBtnA);
				
				WaitElement(txt_DWSProjectSearchByPageTxtA);
				sendKeys(txt_DWSProjectSearchByPageTxtA, ProjectCode);
				pressEnter(txt_DWSProjectSearchByPageTxtA);
				
				WaitElement(sel_DWSProjectSearchByPageSelA.replace("ProNo", ProjectCode));
				doubleClick(sel_DWSProjectSearchByPageSelA.replace("ProNo", ProjectCode));
				
				WaitElement(btn_ActionBtnA);
				click(btn_ActionBtnA);
				
				WaitClick(btn_DWSUpdateTaskPOCA);
				click(btn_DWSUpdateTaskPOCA);
				
				WaitElement(txt_DWScboxTaskPOCA);
				selectIndex(txt_DWScboxTaskPOCA, 1);
				
				WaitElement(txt_DWStxtTaskPOCA);
				sendKeys(txt_DWStxtTaskPOCA, "100");
				
				Thread.sleep(3000);
				JavascriptExecutor j1 = (JavascriptExecutor)driver;
				j1.executeScript("$('#divProjectActionPOC').parent().find('.dialogbox-buttonarea > .button').click()");
				
				Thread.sleep(3000);
				pageRefersh();
				
				WaitClick(btn_ActionBtnA);
				click(btn_ActionBtnA);
				
				WaitClick(btn_DWSProjectCompleteBtnA);
				click(btn_DWSProjectCompleteBtnA);
				
				WaitClick(btn_YesBtnA);
				click(btn_YesBtnA);
				
		    }
		    
		    //Service_DWS_104
		    public void VerifyThatReleasedJournalEntryIsGenerated() throws Exception {
		    	
		    	VerifyThatUserAbleToNavigatedToTheWorkedHoursNewFormWhenClickOnTheDailyWorkSheetProjectJourney();
		    	
		    	WaitElement(btn_ActionBtnA);
		    	click(btn_ActionBtnA);
		    	WaitClick(btn_JournalBtnA);
		    	click(btn_JournalBtnA);
		    }
		    
			// WaitForEstimatedServiceJobsNoA
		    public void WaitForEstimatedServiceJobsNoA() throws Exception {
		    	
				   Thread.sleep(3000);
				   String EstimatedServiceJobsNoA = "0";
				   
				   while(EstimatedServiceJobsNoA.contentEquals("0")) {
						   EstimatedServiceJobsNoA = getText(txt_EstimatedServiceJobsNoA);
				   }
			}
		    
			// WaitForOpenServiceJobsNoA
		    public void WaitForOpenServiceJobsNoA() throws Exception {
		    	
				   Thread.sleep(3000);
				   String OpenServiceJobsNoA = "0";
				   
				   while(OpenServiceJobsNoA.contentEquals("0")) {
						   OpenServiceJobsNoA = getText(txt_OpenServiceJobsNoA);
				   }
			}
		    
		  //WaitClick
		    public void WaitClick(String Locator)  throws Exception {
		    	
		    	WebDriverWait wait = new WebDriverWait(driver,60);
		    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Locator)));
		    }

		    //WaitElement
		    public void WaitElement(String Locator)  throws Exception {
		    	
		    	WebDriverWait wait = new WebDriverWait(driver,60);
		    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator)));
		    }
}
