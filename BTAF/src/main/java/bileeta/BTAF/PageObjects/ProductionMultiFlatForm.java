package bileeta.BTAF.PageObjects;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Function;

import bileeta.BATF.Pages.ProductionModuleData;
import bileeta.BTAF.Utilities.CommonMethods;

public class ProductionMultiFlatForm extends ProductionModuleData {
	/* Common Variables */
	Map<String, String> products_map = new HashMap<String, String>();
	List<Map<String, String>> products = new ArrayList<Map<String, String>>();
	boolean rowMaterialCreated = false;

	/* write production data */
	private static Workbook wb;
	private static Sheet sh;
	private static FileInputStream fis;
	private static FileOutputStream fos;
	private static Row row;
	private static Cell cell;
	private static Cell cell1;
	private static Cell cell2;

	/* PROD_E2E_002 */
	private String genaratedBatch;

	/* Common Objects */
	CommonMethods common = new CommonMethods();
	InventoryAndWarehouseModule invObj = new InventoryAndWarehouseModule();

	/* Common Methods */
	public void writeProductionData(String name, String variable, int data_row)
			throws InvalidFormatException, IOException {
		String path = "";
		String location = System.getProperty("user.dir");

		String real_project_path = "*src*test*java*productionModule*ProductionData.xlsx".replace("*", "\\");
		path = location + real_project_path;

		fis = new FileInputStream(path);
		wb = WorkbookFactory.create(fis);
		sh = wb.getSheet("Sheet1");
		row = sh.createRow(data_row);
		cell = row.createCell(1);
		cell1 = row.createCell(0);
		cell.setCellValue(variable);
		cell1.setCellValue(name);
		/* System.out.println(cell.getStringCellValue()); */
		fos = new FileOutputStream(path);
		wb.write(fos);
		fos.flush();
		fos.close();
	}

	public String readProductionData(String name) throws IOException, InvalidFormatException {
		String path = "";
		String location = System.getProperty("user.dir");

		String real_project_path = "*src*test*java*productionModule*ProductionData.xlsx".replace("*", "\\");
		path = location + real_project_path;

		fis = new FileInputStream(path);
		wb = WorkbookFactory.create(fis);
		sh = wb.getSheet("Sheet1");

		Map<String, String> iw_creation_map = new HashMap<String, String>();
		List<Map<String, String>> iw_creation = new ArrayList<Map<String, String>>();

		int totRows = sh.getLastRowNum();
		for (i = 0; i <= totRows; i++) {
			row = sh.getRow(i);
			cell = row.getCell(0);
			cell2 = row.getCell(1);

			String test_creation_row1 = cell.getStringCellValue();
			String test_creation_row2 = cell2.getStringCellValue();

			iw_creation_map.put(test_creation_row1, test_creation_row2);
			iw_creation.add(i, iw_creation_map);

		}

		String iw_info = iw_creation.get(0).get(name);

		return iw_info;
	}

	public void loginBiletaAutomation() throws Exception {
		openPage(siteUrlMultiPlatformm);
		Thread.sleep(2000);
		explicitWait(txt_username, 40);
		sendKeys(txt_username, usernameMultiPlatformm);
		Thread.sleep(1000);
		sendKeys(txt_password, passwordMultiPlatformm);
		Thread.sleep(2000);
		click(btn_login);

		explicitWait(navigateMenu, 40);
		if (isDisplayedQuickCheck(div_loginVerification)) {
			openPage(siteUrlMultiPlatformm);
			Thread.sleep(2000);
			explicitWait(txt_username, 40);
			sendKeys(txt_username, usernameMultiPlatformm);
			Thread.sleep(1000);
			sendKeys(txt_password, passwordMultiPlatformm);
			Thread.sleep(2000);
			click(btn_login);
			explicitWait(navigateMenu, 40);
		}

	}

	/* PROD_E2E_001 */
	public void billOfMaterial_PROD_E2E_001() throws Exception {
		System.out.println(getCurrentUrl());
		explicitWaitUntillClickable(img_navigationPane, 60);
		Thread.sleep(2000);
		explicitWaitUntillClickable(img_navigationPane, 20);
		if (click(img_navigationPane)) {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User successfully click navigation pane", "pass");
		} else {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User couldn't click navigation pane", "fail");
		}

		explicitWaitUntillClickable(btn_inventory, 10);
		click(btn_inventory);

		explicitWaitUntillClickable(btn_billOfMaterial2, 20);
		click(btn_billOfMaterial2);

		explicitWaitUntillClickable(btn_newBillOfMaterial, 10);
		click(btn_newBillOfMaterial);

		explicitWaitUntillClickable(txt_descriptionBillOfMaterials, 60);
		sendKeys(txt_descriptionBillOfMaterials, descriptionBOM);

		selectText(drop_productGroupBOM, productionGroupE2E);

		Thread.sleep(2000);
		String selectedProductGroup = getSelectedOptionInDropdown(drop_productGroupBOM);
		if (selectedProductGroup.contains(productionGroupE2E)) {
			writeTestResults("Verify that user able to select Product Group",
					"User should be able to select Product Group", "User successfully select Product Group", "pass");
		} else {
			writeTestResults("Verify that user able to select Product Group",
					"User should be able to select Product Group", "User doesn't select Product Group", "fail");

		}

		explicitWaitUntillClickable(btn_lookupOutputProduct, 20);
		click(btn_lookupOutputProduct);
		explicitWaitUntillClickable(txt_productSearch, 40);
		Thread.sleep(1000);
		sendKeysLookup(txt_productSearch, invObj.readTestCreation("BatchFifoMultiPlatformProductionType"));
		pressEnter(txt_productSearch);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")), 20);

		if (isDisplayedQuickCheck(lnk_inforOnLookupInformationReplace.replace("info",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")))) {
			doubleClick(lnk_inforOnLookupInformationReplace.replace("info",
					invObj.readTestCreation("BatchFifoMultiPlatformProductionType")));

			Thread.sleep(1500);

			String selectedProduct = getAttribute(txt_productFrontPageBOM, "value");
			if (selectedProduct.contains(invObj.readTestCreation("BatchFifoMultiPlatformProductionType"))) {
				writeTestResults("Verify that user able to select Production Type Product",
						"User should be able to select Production Type Product",
						"User successfully select Production Type Product", "pass");
			} else {
				writeTestResults("Verify that user able to select Production Type Product",
						"User should be able to select Production Type Product",
						"User doesn't select Production Type Product", "fail");

			}
		} else {
			Thread.sleep(1500);
			((JavascriptExecutor) driver).executeScript("window.open('about:blank','_blank');");
			Thread.sleep(1000);
			switchWindow();
			openPage(siteUrlMultiPlatformm);
			createProducttionTypeProduct();
			closeWindow();
			sendKeysLookup(txt_productSearch, invObj.readTestCreation("BatchFifoMultiPlatformProductionType"));
			pressEnter(txt_productSearch);
			Thread.sleep(2000);
			explicitWait(lnk_inforOnLookupInformationReplace.replace("info",
					invObj.readTestCreation("BatchFifoMultiPlatformProductionType")), 20);
			doubleClick(lnk_inforOnLookupInformationReplace.replace("info",
					invObj.readTestCreation("BatchFifoMultiPlatformProductionType")));

			Thread.sleep(1500);

			String selectedProduct = getAttribute(txt_productFrontPageBOM, "value");
			if (selectedProduct.contains(invObj.readTestCreation("BatchFifoMultiPlatformProductionType"))) {
				writeTestResults("Verify that user able to select Production Type Product",
						"User should be able to select Production Type Product",
						"User successfully select Production Type Product", "pass");
			} else {
				writeTestResults("Verify that user able to select Production Type Product",
						"User should be able to select Production Type Product",
						"User doesn't select Production Type Product", "fail");

			}
		}
		Thread.sleep(1500);
		sendKeys(txt_quantityBOM, hundred);
		String enterdQty = getAttribute(txt_quantityBOM, "value");
		if (enterdQty.equals(hundred)) {
			writeTestResults("Verify that user able to enter the Quantity", "User should be able to enter the Quantity",
					"User successfully enter the Quantity", "pass");
		} else {
			writeTestResults("Verify that user able to enter the Quantity", "User should be able to enter the Quantity",
					"User doesn't enter the Quantity", "fail");

		}

		if (isDisplayed(header_comlumQtyBoMMaterialInfoTable)
				&& isDisplayed(header_comlumFixedQtyBoMMaterialInfoTable)) {
			writeTestResults("Verify that relevant columns on material information grid is display same as earlier",
					"Relevant columns on material information grid should display same as earlier",
					"Relevant columns on the material information grid are display the same as earlier", "pass");
		} else {
			writeTestResults("Verify that relevant columns on material information grid is display same as earlier",
					"Relevant columns on material information grid should display same as earlier",
					"Relevant columns on the material information grid are not display the same as earlier", "fail");

			pageScrollDown();
		}

		mouseMove(header_scrapFixedQtyBoMMaterialInfoTable);
		if (isDisplayed(header_scrapFixedQtyBoMMaterialInfoTable)) {
			writeTestResults("Verify that Scrap % columns on material information grid is display same as earlier",
					"Scrap % columns on material information grid should display same as earlier",
					"Scrap % columns on the material information grid are display the same as earlier", "pass");
		} else {
			writeTestResults("Verify that Scrap % columns on material information grid is display same as earlier",
					"Scrap % columns on material information grid should display same as earlier",
					"Scrap % columns on the material information grid are not display the same as earlier", "fail");

		}

		mouseMove(btn_lookupOnLineBOMRowReplace.replace("row", "1"));
		click(btn_lookupOnLineBOMRowReplace.replace("row", "1"));
		sendKeysLookup(txt_productSearch, invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialOne"));
		pressEnter(txt_productSearch);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info",
				invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialOne")), 15);
		if (isDisplayedQuickCheck(lnk_inforOnLookupInformationReplace.replace("info",
				invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialOne")))) {
			doubleClick(lnk_inforOnLookupInformationReplace.replace("info",
					invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialOne")));
		} else {
			Thread.sleep(1500);
			((JavascriptExecutor) driver).executeScript("window.open('about:blank','_blank');");
			Thread.sleep(1000);
			switchWindow();
			openPage(siteUrlMultiPlatformm);
			createAllowDecimalRowMaterial1();
			createAllowDecimalRowMaterial2();
			createAllowDecimalRowMaterialHasVarientProduct3();
			closeWindow();
			sendKeysLookup(txt_productSearch, invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialOne"));
			pressEnter(txt_productSearch);
			Thread.sleep(2000);
			explicitWait(lnk_inforOnLookupInformationReplace.replace("info",
					invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialOne")), 15);

			doubleClick(lnk_inforOnLookupInformationReplace.replace("info",
					invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialOne")));
		}
		explicitWait(txt_qtyBOMInputProductLinesRowReplace.replace("row", "1"), 10);
		sendKeys(txt_qtyBOMInputProductLinesRowReplace.replace("row", "1"), rowMaterialQtyOne);
		String enteredRowMaterialOneQty = getAttribute(txt_qtyBOMInputProductLinesRowReplace.replace("row", "1"),
				"value");
		if (enteredRowMaterialOneQty.equals(rowMaterialQtyOne)) {
			writeTestResults("Verify that user able to enter the raw material one Qty",
					"User should be able to enter the raw material one Qty",
					"User successfully enter the raw material one Qty", "pass");
		} else {
			writeTestResults("Verify that user able to enter the raw material one Qty",
					"User should be able to enter the raw material one Qty",
					"User doesn't enter the raw material one Qty", "fail");

		}
		click(btn_addNewRecordBoM);

		click(btn_lookupOnLineBOMRowReplace.replace("row", "2"));
		sendKeysLookup(txt_productSearch, invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialTwo"));
		pressEnter(txt_productSearch);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info",
				invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialTwo")), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info",
				invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialTwo")));

		explicitWait(txt_qtyBOMInputProductLinesRowReplace.replace("row", "2"), 10);
		sendKeys(txt_qtyBOMInputProductLinesRowReplace.replace("row", "2"), rowMaterialQtyTwo);
		String enteredRowMaterialTwoQty = getAttribute(txt_qtyBOMInputProductLinesRowReplace.replace("row", "2"),
				"value");
		if (enteredRowMaterialTwoQty.equals(rowMaterialQtyTwo)) {
			writeTestResults("Verify that user able to enter the raw material two Qty",
					"User should be able to enter the raw material two Qty",
					"User successfully enter the raw material two Qty", "pass");
		} else {
			writeTestResults("Verify that user able to enter the raw material two Qty",
					"User should be able to enter the raw material two Qty",
					"User doesn't enter the raw material two Qty", "fail");

		}
		click(chk_fixedQtyRowReplaceBOM.replace("row", "2"));
		if (isSelected(chk_fixedQtyRowReplaceBOM.replace("row", "2"))) {
			writeTestResults("Verify that user able to enable Fixed Qty", "User should be able to enable Fixed Qty",
					"User successfully enable Fixed Qty", "pass");
		} else {
			writeTestResults("Verify that user able to enable Fixed Qty", "User should be able to enable Fixed Qty",
					"User doesn't enable Fixed Qty", "fail");

		}

		click(btn_addNewRecordBoM);

		click(btn_lookupOnLineBOMRowReplace.replace("row", "3"));
		sendKeysLookup(txt_productSearch,
				invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialHasVarientProductThree"));
		pressEnter(txt_productSearch);
		Thread.sleep(2000);
		explicitWait(
				lnk_inforOnLookupInformationReplace.replace("info",
						invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialHasVarientProductThree")),
				20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info",
				invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialHasVarientProductThree")));

		explicitWait(txt_qtyBOMInputProductLinesRowReplace.replace("row", "3"), 10);
		sendKeys(txt_qtyBOMInputProductLinesRowReplace.replace("row", "3"), rowMaterialQtyThree);
		String enteredRowMaterialThreeQty = getAttribute(txt_qtyBOMInputProductLinesRowReplace.replace("row", "3"),
				"value");
		if (enteredRowMaterialThreeQty.equals(rowMaterialQtyThree)) {
			writeTestResults("Verify that user able to enter the raw material three Qty",
					"User should be able to enter the raw material three Qty",
					"User successfully enter the raw material three Qty", "pass");
		} else {
			writeTestResults("Verify that user able to enter the raw material three Qty",
					"User should be able to enter the raw material three Qty",
					"User doesn't enter the raw material three Qty", "fail");

		}

		sendKeys(txt_scrapRowReplaceBOM.replace("row", "3"), scrapRowMaterilThree);
		String enteredRowMaterialThreeScrap = getAttribute(txt_scrapRowReplaceBOM.replace("row", "3"), "value");
		if (enteredRowMaterialThreeScrap.equals(scrapRowMaterilThree)) {
			writeTestResults("Verify that user able to enter the raw material three Scrap",
					"User should be able to enter the raw material three Scrap",
					"User successfully enter the raw material three Scrap", "pass");
		} else {
			writeTestResults("Verify that user able to enter the raw material three Scrap",
					"User should be able to enter the raw material three Scrap",
					"User doesn't enter the raw material three Scrap", "fail");

		}

		click(btn_draft);
		explicitWait(header_draftedBOM, 50);
		if (isDisplayed(header_draftedBOM)) {
			writeTestResults("Verify user able to draft the Bill Of Material",
					"User should be able to draft the Bill Of Material", "User successfully draft the Bill Of Material",
					"pass");
		} else {
			writeTestResults("Verify user able to draft the Bill Of Material",
					"User should be able to draft the Bill Of Material", "User doesn't draft the Bill Of Material",
					"fail");

		}

		click(btn_release);
		explicitWait(header_releasedBillOfMaterial, 50);
		trackCode = getText(lbl_docNo);
		writeProductionData("BOM_PROD_E2E_001", trackCode, 12);
		if (isDisplayed(header_releasedBillOfMaterial)) {
			writeTestResults("Verify user able to release the Bill Of Material",
					"User should be able to release the Bill Of Material",
					"User successfully release the Bill Of Material", "pass");
		} else {
			writeTestResults("Verify user able to release the Bill Of Material",
					"User should be able to release the Bill Of Material", "User doesn't release the Bill Of Material",
					"fail");

		}
	}

	public void createProducttionTypeProduct() throws Exception {
		explicitWaitUntillClickable(img_navigationPane, 60);
		Thread.sleep(2000);
		explicitWaitUntillClickable(img_navigationPane, 20);
		click(img_navigationPane);

		click(btn_inventory);
		explicitWait(btn_productInformation, 8);
		Thread.sleep(1000);
		click(btn_productInformation);
		explicitWait(btn_newProduct2, 8);

		Thread.sleep(1500);
		click(btn_newProduct2);

		// product code
		String product_code_batch_fifo = common.getCurrentDateTimeAsUniqueCode() + "-BATCH-FIFO-PT";

		explicitWaitUntillClickable(txt_productCodeProductInformation, 20);
		sendKeys(txt_productCodeProductInformation, product_code_batch_fifo);

		// product description
		Thread.sleep(2000);
		sendKeys(txt_descriptionProductInformation, productDesriptionProductInfo);

		// product group
		selectText(drop_productGroupProductInfromation, productionGroupE2E);
		Thread.sleep(1500);
		String selectedProductGroup = getSelectedOptionInDropdown(drop_productGroupProductInfromation);
		if (selectedProductGroup.contains(productionGroupE2E)) {
			writeTestResults("Verify that user able to select Product Group",
					"User should be able to select Product Group", "User successfully select Product Group", "pass");
		} else {
			writeTestResults("Verify that user able to select Product Group",
					"User should be able to select Product Group", "User doesn't select Product Group", "fail");

		}

		click(chk_allowDecimalProductInformation);

		// manufacturer
		click(btn_lookupMenufcturerProductInformation);
		explicitWaitUntillClickable(txt_searchMenfacture, 10);
		pressEnter(txt_searchMenfacture);
		Thread.sleep(1000);
		doubleClick(td_firstResultMenufacturerLookup);

		// default uom group
		selectIndex(drop_defouldUOMGroupProductInfromation, 1);

		// uom group
		selectIndex(drop_defouldUOMProductInfromation, 1);
		Thread.sleep(2000);

		click(tab_detailsProductInformation);
		explicitWait(header_warehouseInfromationDetailsTabProductInfromation, 10);

		Thread.sleep(3000);
		pageScrollDown();
		pageScrollUpToTop();
		selectText(drop_outboundCostingMethodProductInfromation, "FIFO");
		Thread.sleep(1500);
		click(chk_allowInventoryWithoutCostingProductInformation);
		Thread.sleep(1500);

		selectText(drop_menufacTypeProductionProductInformation, menfacTypeProduction);
		Thread.sleep(1500);
		String selectedMenufacType = getSelectedOptionInDropdown(drop_menufacTypeProductionProductInformation);
		if (selectedMenufacType.equals(menfacTypeProduction)) {
			writeTestResults("Verify menufacturing type select as 'Production'",
					"Production type should be 'Production'", "Production type successfully select as 'Production'",
					"pass");
		} else {
			writeTestResults("Verify menufacturing type select as 'Production'",
					"Production type should be 'Production'", "Production type doesn't select as 'Production'", "fail");
		}

		Thread.sleep(2000);
		if (isSelected(chk_isBatchProduct)) {
			writeTestResults("Verify system auto selected flag as Batch Product",
					"System should be auto select flag as Batch Product",
					"System successfully auto selected flag as Batch Product", "pass");
		} else {
			writeTestResults("Verify system auto selected flag as Batch Product",
					"System should be auto select flag as Batch Product",
					"System doesn't auto selected flag as Batch Product", "fail");
		}

		click(btn_draft);
		explicitWait(btn_release, 60);

		click(btn_release);
		explicitWait(header_releasedProduct, 60);

		products_map.put("BatchFifo", product_code_batch_fifo);
		products.add(0, products_map);
		invObj.writeTestCreations("BatchFifoMultiPlatformProductionType", products.get(0).get("BatchFifo"), 24);
	}

	public void createAllowDecimalRowMaterial1() throws Exception {
		rowMaterialCreated = true;
		explicitWaitUntillClickable(img_navigationPane, 60);
		Thread.sleep(2000);
		explicitWaitUntillClickable(img_navigationPane, 20);
		click(img_navigationPane);

		click(btn_inventory);
		explicitWait(btn_productInformation, 8);
		Thread.sleep(1000);
		click(btn_productInformation);
		explicitWait(btn_newProduct2, 8);

		Thread.sleep(1500);
		click(btn_newProduct2);

		// product code
		String product_code_batch_fifo = common.getCurrentDateTimeAsUniqueCode() + "-BATCH-FIFO";

		explicitWaitUntillClickable(txt_productCodeProductInformation, 20);
		sendKeys(txt_productCodeProductInformation, product_code_batch_fifo);

		// product description
		Thread.sleep(2000);
		sendKeys(txt_descriptionProductInformation, productDesriptionProductInfo);

		// product group
		selectText(drop_productGroupProductInfromation, productionGroupE2E);
		Thread.sleep(1500);
		String selectedProductGroup = getSelectedOptionInDropdown(drop_productGroupProductInfromation);
		if (selectedProductGroup.contains(productionGroupE2E)) {
			writeTestResults("Verify that user able to select Product Group",
					"User should be able to select Product Group", "User successfully select Product Group", "pass");
		} else {
			writeTestResults("Verify that user able to select Product Group",
					"User should be able to select Product Group", "User doesn't select Product Group", "fail");

		}

		click(chk_allowDecimalProductInformation);

		// manufacturer
		click(btn_lookupMenufcturerProductInformation);
		explicitWaitUntillClickable(txt_searchMenfacture, 10);
		pressEnter(txt_searchMenfacture);
		Thread.sleep(1000);
		doubleClick(td_firstResultMenufacturerLookup);

		// default uom group
		selectIndex(drop_defouldUOMGroupProductInfromation, 1);

		// uom group
		selectIndex(drop_defouldUOMProductInfromation, 1);
		Thread.sleep(2000);

		click(tab_detailsProductInformation);
		explicitWait(header_warehouseInfromationDetailsTabProductInfromation, 10);

		Thread.sleep(3000);
		pageScrollDown();
		pageScrollUpToTop();
		selectText(drop_outboundCostingMethodProductInfromation, "FIFO");
		Thread.sleep(1500);

		click(chk_allowInventoryWithoutCostingProductInformation);
		Thread.sleep(1500);
		click(chk_isBatchProduct);
		Thread.sleep(2000);
		if (isSelected(chk_isBatchProduct)) {
			writeTestResults("Verify system auto selected flag as Batch Product",
					"System should be auto select flag as Batch Product",
					"System successfully auto selected flag as Batch Product", "pass");
		} else {
			writeTestResults("Verify system auto selected flag as Batch Product",
					"System should be auto select flag as Batch Product",
					"System doesn't auto selected flag as Batch Product", "fail");
		}

		click(btn_draft);
		explicitWait(btn_release, 60);

		click(btn_release);
		explicitWait(header_releasedProduct, 60);

		invObj.writeTestCreations("BatchFifoMultiPlatformAllowDecimalMaterialOne", product_code_batch_fifo, 25);
	}

	public void createAllowDecimalRowMaterial2() throws Exception {
		explicitWaitUntillClickable(img_navigationPane, 60);
		Thread.sleep(2000);
		explicitWaitUntillClickable(img_navigationPane, 20);
		click(img_navigationPane);

		click(btn_inventory);
		explicitWait(btn_productInformation, 8);
		Thread.sleep(1000);
		click(btn_productInformation);
		explicitWait(btn_newProduct2, 8);

		Thread.sleep(1500);
		click(btn_newProduct2);

		// product code
		String product_code_batch_fifo = common.getCurrentDateTimeAsUniqueCode() + "-BATCH-FIFO";

		explicitWaitUntillClickable(txt_productCodeProductInformation, 20);
		sendKeys(txt_productCodeProductInformation, product_code_batch_fifo);

		// product description
		Thread.sleep(2000);
		sendKeys(txt_descriptionProductInformation, productDesriptionProductInfo);

		// product group
		selectText(drop_productGroupProductInfromation, productionGroupE2E);
		Thread.sleep(1500);
		String selectedProductGroup = getSelectedOptionInDropdown(drop_productGroupProductInfromation);
		if (selectedProductGroup.contains(productionGroupE2E)) {
			writeTestResults("Verify that user able to select Product Group",
					"User should be able to select Product Group", "User successfully select Product Group", "pass");
		} else {
			writeTestResults("Verify that user able to select Product Group",
					"User should be able to select Product Group", "User doesn't select Product Group", "fail");

		}

		click(chk_allowDecimalProductInformation);

		// manufacturer
		click(btn_lookupMenufcturerProductInformation);
		explicitWaitUntillClickable(txt_searchMenfacture, 10);
		pressEnter(txt_searchMenfacture);
		Thread.sleep(1000);
		doubleClick(td_firstResultMenufacturerLookup);

		// default uom group
		selectIndex(drop_defouldUOMGroupProductInfromation, 1);

		// uom group
		selectIndex(drop_defouldUOMProductInfromation, 1);
		Thread.sleep(2000);

		click(tab_detailsProductInformation);
		explicitWait(header_warehouseInfromationDetailsTabProductInfromation, 10);

		Thread.sleep(3000);
		pageScrollDown();
		pageScrollUpToTop();
		selectText(drop_outboundCostingMethodProductInfromation, "FIFO");
		Thread.sleep(1500);
		click(chk_allowInventoryWithoutCostingProductInformation);
		Thread.sleep(1500);
		click(chk_isBatchProduct);
		Thread.sleep(2000);
		if (isSelected(chk_isBatchProduct)) {
			writeTestResults("Verify system auto selected flag as Batch Product",
					"System should be auto select flag as Batch Product",
					"System successfully auto selected flag as Batch Product", "pass");
		} else {
			writeTestResults("Verify system auto selected flag as Batch Product",
					"System should be auto select flag as Batch Product",
					"System doesn't auto selected flag as Batch Product", "fail");
		}

		click(btn_draft);
		explicitWait(btn_release, 60);

		click(btn_release);
		explicitWait(header_releasedProduct, 60);

		invObj.writeTestCreations("BatchFifoMultiPlatformAllowDecimalMaterialTwo", product_code_batch_fifo, 26);
	}

	public void createAllowDecimalRowMaterialHasVarientProduct3() throws Exception {
		explicitWaitUntillClickable(img_navigationPane, 60);
		Thread.sleep(2000);
		explicitWaitUntillClickable(img_navigationPane, 20);
		click(img_navigationPane);

		click(btn_inventory);
		explicitWait(btn_productInformation, 8);
		Thread.sleep(1000);
		click(btn_productInformation);
		explicitWait(btn_newProduct2, 8);

		Thread.sleep(1500);
		click(btn_newProduct2);

		// product code
		String product_code_batch_fifo = common.getCurrentDateTimeAsUniqueCode() + "-BATCH-FIFO";

		explicitWaitUntillClickable(txt_productCodeProductInformation, 20);
		sendKeys(txt_productCodeProductInformation, product_code_batch_fifo);

		// product description
		Thread.sleep(2000);
		sendKeys(txt_descriptionProductInformation, productDesriptionProductInfo);

		// product group
		selectText(drop_productGroupProductInfromation, productionGroupE2E);
		Thread.sleep(1500);
		String selectedProductGroup = getSelectedOptionInDropdown(drop_productGroupProductInfromation);
		if (selectedProductGroup.contains(productionGroupE2E)) {
			writeTestResults("Verify that user able to select Product Group",
					"User should be able to select Product Group", "User successfully select Product Group", "pass");
		} else {
			writeTestResults("Verify that user able to select Product Group",
					"User should be able to select Product Group", "User doesn't select Product Group", "fail");

		}

		click(chk_allowDecimalProductInformation);

		// manufacturer
		click(btn_lookupMenufcturerProductInformation);
		explicitWaitUntillClickable(txt_searchMenfacture, 10);
		sendKeysLookup(txt_searchMenfacture, "");
		pressEnter(txt_searchMenfacture);
		Thread.sleep(1000);
		doubleClick(td_firstResultMenufacturerLookup);

		// default uom group
		selectIndex(drop_defouldUOMGroupProductInfromation, 1);

		// uom group
		selectIndex(drop_defouldUOMProductInfromation, 1);
		Thread.sleep(2000);

		click(tab_detailsProductInformation);
		explicitWait(header_warehouseInfromationDetailsTabProductInfromation, 10);

		Thread.sleep(3000);
		pageScrollDown();
		pageScrollUpToTop();
		selectText(drop_outboundCostingMethodProductInfromation, "FIFO");
		Thread.sleep(1500);
		click(chk_allowInventoryWithoutCostingProductInformation);
		Thread.sleep(1500);
		click(chk_isBatchProduct);
		Thread.sleep(2000);
		if (isSelected(chk_isBatchProduct)) {
			writeTestResults("Verify system auto selected flag as Batch Product",
					"System should be auto select flag as Batch Product",
					"System successfully auto selected flag as Batch Product", "pass");
		} else {
			writeTestResults("Verify system auto selected flag as Batch Product",
					"System should be auto select flag as Batch Product",
					"System doesn't auto selected flag as Batch Product", "fail");
		}

		click(btn_draft);
		explicitWait(btn_release, 60);

		click(btn_release);
		explicitWait(header_releasedProduct, 60);

		click(tab_classificationProductInformation);
		explicitWaitUntillClickable(btn_lokkupVarienGroupProductInformation, 20);
		click(btn_lokkupVarienGroupProductInformation);
		explicitWaitUntillClickable(txt_productVarientSearch, 40);
		pressEnter(txt_productVarientSearch);
		explicitWaitUntillClickable(td_firstResultVarirnLookupPI, 20);
		doubleClick(td_firstResultVarirnLookupPI);
		Thread.sleep(2000);
		explicitWaitUntillClickable(drop_varienSelectClassificationTab, 20);
		selectIndex(drop_varienSelectClassificationTab, 1);
		explicitWait(div_varienOnVarientSectionProductReplace.replace("product", product_code_batch_fifo), 10);
		if (isDisplayed(div_varienOnVarientSectionProductReplace.replace("product", product_code_batch_fifo))) {
			writeTestResults("Verify user able to select varient", "User should be able to select varient",
					"User successfully select varient", "pass");
		} else {
			writeTestResults("Verify user able to select varient", "User should be able to select varient",
					"User couldn't select varient", "fail");
		}

		click(btn_varientUpdate);
		if (isElementPresent(p_varientUpdatedValidation)) {
			writeTestResults("Verify that varient is updated", "Varient should be updated",
					"Varient successfully updated", "pass");
		} else {
			writeTestResults("Verify that varient is updated", "Varient should be updated", "Varient doesn't updated",
					"fail");
		}

		invObj.writeTestCreations("BatchFifoMultiPlatformAllowDecimalMaterialHasVarientProductThree",
				product_code_batch_fifo, 27);
	}

	public void createOverheadInformations() throws Exception {
		pageRefersh();
		explicitWaitUntillClickable(img_navigationPane, 60);
		Thread.sleep(2000);
		explicitWaitUntillClickable(img_navigationPane, 20);
		if (click(img_navigationPane)) {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User successfully click navigation pane", "pass");
		} else {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User couldn't click navigation pane", "fail");
		}

		explicitWaitUntillClickable(btn_production, 20);
		click(btn_production);

		explicitWaitUntillClickable(btn_overheadInformation, 30);
		click(btn_overheadInformation);

		explicitWait(div_overheadMainList.replace("code", readProductionData("OverheadOne_PROD_E2E_001")), 30);
		if (!isDisplayedQuickCheck(
				div_overheadMainList.replace("code", readProductionData("OverheadOne_PROD_E2E_001")))) {
			explicitWaitUntillClickable(btn_newOverheadInformation, 60);
			click(btn_newOverheadInformation);

			explicitWait(lookup_newOverheadInformation, 10);
			if (isDisplayed(lookup_newOverheadInformation)) {
				writeTestResults("Verify that overhead information lookup is open",
						"Overhead Information lookup should be open", "Overhead information lookup successfully open",
						"pass");
			} else {
				writeTestResults("Verify that overhead information lookup is open",
						"Overhead Information lookup should be open", "Overhead information lookup doesn't open",
						"fail");
			}

			String overheadCode = common.getCurrentDateTimeAsUniqueCode() + "E2E";
			sendKeys(txt_overheadCode2, overheadCode);
			String enteredOverheadCode = getAttribute(txt_overheadCode2, "value");
			if (enteredOverheadCode.equals(overheadCode)) {
				writeTestResults("Verify user able to enter overhead code",
						"User should be able to enter overhead code", "User successfully enter overhead code", "pass");
			} else {
				writeTestResults("Verify user able to enter overhead code",
						"User should be able to enter overhead code", "User couldn't enter overhead code", "fail");
			}

			sendKeys(txt_overheadDescription, overheadDescription);

			selectIndex(drop_overheadGroup, 1);

			selectText(drop_rateTypeOverhead, rateTypeOverhead);
			String enteredOverheadType = getSelectedOptionInDropdown(drop_rateTypeOverhead);
			if (enteredOverheadType.equals(rateTypeOverhead)) {
				writeTestResults("Verify user able to select rate type", "User should be able to select rate type",
						"User successfully select rate type", "pass");
			} else {
				writeTestResults("Verify user able to select rate type", "User should be able to select rate type",
						"User couldn't select rate type", "fail");
			}

			click(btn_draftOverhead2);
			Thread.sleep(2000);
			if (!isDisplayedQuickCheck(lookup_newOverheadInformation)) {
				writeTestResults("Verify user able to draft a overhead", "User should be able to draft a overhead",
						"User successfully draft a overhead", "pass");
			} else {
				writeTestResults("Verify user able to draft a overhead", "User should be able to draft a overhead",
						"User couldn't draft a overhead", "fail");
			}

			explicitWaitUntillClickable(div_selectOverheadReplaceOverhead.replace("overhead_code", overheadCode), 10);
			click(div_selectOverheadReplaceOverhead.replace("overhead_code", overheadCode));
			explicitWait(div_selectedOverheadReplaceOverhead.replace("overhead_code", overheadCode), 10);
			if (isDisplayedQuickCheck(div_selectedOverheadReplaceOverhead.replace("overhead_code", overheadCode))) {
				writeTestResults("Verify user able to select relevent overhead",
						"User should be able to select relevent overhead", "User successfully select relevent overhead",
						"pass");
			} else {
				writeTestResults("Verify user able to select relevent overhead",
						"User should be able to select relevent overhead", "User couldn't select relevent overhead",
						"fail");
			}

			Thread.sleep(1500);
			explicitWaitUntillClickable(a_acitveOverhead, 6);
			click(a_acitveOverhead);
			explicitWaitUntillClickable(btn_yesConfirmationOverheadActivation, 6);
			click(btn_yesConfirmationOverheadActivation);

			explicitWaitUntillClickable(b_activatedStatusOverhead, 30);
			trackCode = overheadCode;
			if (isDisplayedQuickCheck(b_activatedStatusOverhead)) {
				writeTestResults("Verify user able to activate the overhead",
						"User should be able to activate the overhead", "User successfully activate the overhead",
						"pass");
			} else {
				writeTestResults("Verify user able to activate the overhead",
						"User should be able to activate the overhead", "User couldn't activate the overhead", "fail");
			}

			writeProductionData("OverheadOne_PROD_E2E_001", overheadCode, 13);

			/* Second Overhead */
			explicitWaitUntillClickable(btn_newOverheadInformation, 60);
			click(btn_newOverheadInformation);

			explicitWait(lookup_newOverheadInformation, 10);
			if (isDisplayed(lookup_newOverheadInformation)) {
				writeTestResults("Verify that overhead information lookup is open",
						"Overhead Information lookup should be open", "Overhead information lookup successfully open",
						"pass");
			} else {
				writeTestResults("Verify that overhead information lookup is open",
						"Overhead Information lookup should be open", "Overhead information lookup doesn't open",
						"fail");
			}

			String overheadCode2 = common.getCurrentDateTimeAsUniqueCode() + "E2E";
			sendKeys(txt_overheadCode2, overheadCode2);
			String enteredOverheadCode2 = getAttribute(txt_overheadCode2, "value");
			if (enteredOverheadCode2.equals(overheadCode2)) {
				writeTestResults("Verify user able to enter overhead code",
						"User should be able to enter overhead code", "User successfully enter overhead code", "pass");
			} else {
				writeTestResults("Verify user able to enter overhead code",
						"User should be able to enter overhead code", "User couldn't enter overhead code", "fail");
			}

			sendKeys(txt_overheadDescription, overheadDescription);

			selectIndex(drop_overheadGroup, 1);

			selectText(drop_rateTypeOverhead, rateTypeOverhead);
			String enteredOverheadType2 = getSelectedOptionInDropdown(drop_rateTypeOverhead);
			if (enteredOverheadType2.equals(rateTypeOverhead)) {
				writeTestResults("Verify user able to select rate type", "User should be able to select rate type",
						"User successfully select rate type", "pass");
			} else {
				writeTestResults("Verify user able to select rate type", "User should be able to select rate type",
						"User couldn't select rate type", "fail");
			}

			click(btn_draftOverhead2);
			Thread.sleep(2000);
			if (!isDisplayedQuickCheck(lookup_newOverheadInformation)) {
				writeTestResults("Verify user able to draft a overhead", "User should be able to draft a overhead",
						"User successfully draft a overhead", "pass");
			} else {
				writeTestResults("Verify user able to draft a overhead", "User should be able to draft a overhead",
						"User couldn't draft a overhead", "fail");
			}

			explicitWaitUntillClickable(div_selectOverheadReplaceOverhead.replace("overhead_code", overheadCode2), 10);
			click(div_selectOverheadReplaceOverhead.replace("overhead_code", overheadCode2));

			explicitWait(div_selectedOverheadReplaceOverhead.replace("overhead_code", overheadCode2), 10);
			if (isDisplayedQuickCheck(div_selectedOverheadReplaceOverhead.replace("overhead_code", overheadCode2))) {
				writeTestResults("Verify user able to find relevent overhead",
						"User should be able to find relevent overhead", "User successfully find relevent overhead",
						"pass");
			} else {
				writeTestResults("Verify user able to find relevent overhead",
						"User should be able to find relevent overhead", "User couldn't find relevent overhead",
						"fail");
			}

			Thread.sleep(1500);
			explicitWaitUntillClickable(a_acitveOverhead, 6);
			click(a_acitveOverhead);
			explicitWaitUntillClickable(btn_yesConfirmationOverheadActivation, 6);
			click(btn_yesConfirmationOverheadActivation);

			explicitWaitUntillClickable(b_activatedStatusOverhead, 30);
			trackCode = overheadCode;
			if (isDisplayedQuickCheck(b_activatedStatusOverhead)) {
				writeTestResults("Verify user able to activate the overhead",
						"User should be able to activate the overhead", "User successfully activate the overhead",
						"pass");
			} else {
				writeTestResults("Verify user able to activate the overhead",
						"User should be able to activate the overhead", "User couldn't activate the overhead", "fail");
			}

			writeProductionData("OverheadTwo_PROD_E2E_001", overheadCode2, 14);
		}

	}

	public void createResourses() throws Exception {
		pageRefersh();
		explicitWaitUntillClickable(img_navigationPane, 60);
		Thread.sleep(2000);
		explicitWaitUntillClickable(img_navigationPane, 20);
		click(img_navigationPane);

		explicitWaitUntillClickable(btn_organizationMgt, 10);
		click(btn_organizationMgt);

		explicitWaitUntillClickable(btn_resourseInformation, 20);
		click(btn_resourseInformation);

		explicitWaitUntillClickable(btn_newResourse, 20);

		explicitWaitUntillClickable(txt_searchResources, 40);
		sendKeys(txt_searchResources, readProductionData("ResourceCode01_E2E"));
		pressEnter(txt_searchResources);
		explicitWaitUntillClickable(
				td_resultResourceCodeReplace.replace("code", readProductionData("ResourceCode01_E2E")), 30);

		if (!isDisplayedQuickCheck(
				td_resultResourceCodeReplace.replace("code", readProductionData("ResourceCode01_E2E")))) {
			click(btn_newResourse);

			explicitWaitUntillClickable(txt_resourseCodeRI, 60);
			String resourseCode = common.getCurrentDateTimeAsUniqueCode();
			sendKeys(txt_resourseCodeRI, resourseCode);

			sendKeys(txt_resourseDescriptionRI, resourseDescription);

			selectIndex(drop_resourseGroupRI, 1);

			click(btn_lookupEmployeeRI);
			explicitWaitUntillClickable(txt_searchEmployeeRI, 30);
			sendKeysLookup(txt_searchEmployeeRI, employeeResourseCreation);
			pressEnter(txt_searchEmployeeRI);
			explicitWaitUntillClickable(td_resultEmplyeeLookupEmployeeReplace, 40);
			doubleClick(td_resultEmplyeeLookupEmployeeReplace);
			Thread.sleep(2000);

			sendKeys(txt_hourlyRateResourse, resourseOneHourlyRate);
			String enteredHourlyRate = getAttribute(txt_hourlyRateResourse, "value");
			if (enteredHourlyRate.equals(resourseOneHourlyRate)) {
				writeTestResults("Verify that user able to enter Hourly Rate for the Resourse 01",
						"User should be able to enter Hourly Rate for the Resourse 01",
						"User successfully enter Hourly Rate for the Resourse 01", "pass");
			} else {
				writeTestResults("Verify that user able to enter Hourly Rate for the Resourse 01",
						"User should be able to enter Hourly Rate for the Resourse 01",
						"User couldn't enter Hourly Rate for the Resourse 01", "fail");
			}

			click(chk_productionProjectResourse);
			if (isSelected(chk_productionProjectResourse)) {
				writeTestResults("Verify that user able to enable Production/Project Resourse flag for the Resourse 01",
						"User should be able to enable Production/Project Resourse flag for the Resourse 01",
						"User successfully enable Production/Project Resourse flag for the Resourse 01", "pass");
			} else {
				writeTestResults("Verify that user able to enable Production/Project Resourse flag for the Resourse 01",
						"User should be able to enable Production/Project Resourse flag for the Resourse 01",
						"User couldn't enable Production/Project Resourse flag for the Resourse 01", "fail");
			}

			explicitWaitUntillClickable(btn_overheadLookupResourseInformation, 10);
			click(btn_overheadLookupResourseInformation);
			explicitWaitUntillClickable(txt_searchOverhead, 15);
			Thread.sleep(2000);
			sendKeysLookup(txt_searchOverhead, readProductionData("OverheadOne_PROD_E2E_001"));
			pressEnter(txt_searchOverhead);
			explicitWaitUntillClickable(td_resultOveheadInformationOverheadReplace.replace("overhead",
					readProductionData("OverheadOne_PROD_E2E_001")), 40);
			if (isDisplayed(td_resultOveheadInformationOverheadReplace.replace("overhead",
					readProductionData("OverheadOne_PROD_E2E_001")))) {
				writeTestResults("Verify that user able to find relevent Overhead for the Resourse 01",
						"User should be able to find relevent Overhead for the Resourse 01",
						"User successfully find relevent Overhead for the Resourse 01", "pass");
			} else {
				writeTestResults("Verify that user able to find relevent Overhead for the Resourse 01",
						"User should be able to find relevent Overhead for the Resourse 01",
						"User couldn't find relevent Overhead for the Resourse 01", "fail");
			}
			doubleClick(td_resultOveheadInformationOverheadReplace.replace("overhead",
					readProductionData("OverheadOne_PROD_E2E_001")));
			Thread.sleep(2000);

			sendKeys(txt_overheadeRateAttachedToRsourseOne, hourlyRateOfOverheadeAttachedToResourseOne);
			String enteredHourlyRateOverheadOneResourseOne = getAttribute(txt_overheadeRateAttachedToRsourseOne,
					"value");
			if (enteredHourlyRateOverheadOneResourseOne.equals(hourlyRateOfOverheadeAttachedToResourseOne)) {
				writeTestResults(
						"Verify that user able to enter Hourly Rate for the Overhead that attached to Resourse 01",
						"User should be able to enter Hourly Rate for the Overhead that attached to the Resourse 01",
						"User successfully enter Hourly Rate for the the Overhead that attached to Resourse 01",
						"pass");
			} else {
				writeTestResults(
						"Verify that user able to enter Hourly Rate for the Overhead that attached to Resourse 01",
						"User should be able to enter Hourly Rate for the Overhead that attached to the Resourse 01",
						"User couldn't enter Hourly Rate for the the Overhead that attached to Resourse 01", "fail");
			}

			click(btn_draft);
			explicitWaitUntillClickable(btn_release, 60);
			click(btn_release);
			explicitWait(header_releasedReourse, 60);
			writeProductionData("ResourceCode01_E2E", resourseCode, 15);
			if (isDisplayed(header_releasedReourse)) {
				writeTestResults("Verify that user able to release Resourse 01",
						"User should be able to release Resourse 01", "User successfully release Resourse 01", "pass");
			} else {
				writeTestResults("Verify that user able to release Resourse 01",
						"User should be able to release Resourse 01", "User couldn't release Resourse 01", "fail");
			}

			/******* Resource 02 ******/
			click(btn_plusAddNewResourse);
			Thread.sleep(2000);
			switchWindow();
			explicitWaitUntillClickable(txt_resourseCodeRI, 60);
			String resourseCode2 = common.getCurrentDateTimeAsUniqueCode();
			sendKeys(txt_resourseCodeRI, resourseCode2);

			sendKeys(txt_resourseDescriptionRI, resourseDescription);

			selectIndex(drop_resourseGroupRI, 1);

			click(btn_lookupEmployeeRI);
			explicitWaitUntillClickable(txt_searchEmployeeRI, 30);
			sendKeysLookup(txt_searchEmployeeRI, employeeResourseCreation);
			pressEnter(txt_searchEmployeeRI);
			explicitWaitUntillClickable(td_resultEmplyeeLookupEmployeeReplace, 40);
			doubleClick(td_resultEmplyeeLookupEmployeeReplace);
			Thread.sleep(2000);

			String loadedHourlyRate = getAttribute(txt_hourlyRateResourse, "value");
			if (loadedHourlyRate.equals("0")) {
				writeTestResults("Verify thst Hourly Rate is 0 for the Resourse 02",
						"Hourly Rate should be 0 for the Resourse 02", "Hourly Rate is 0 for the Resourse 02", "pass");
			} else {
				writeTestResults("Verify thst Hourly Rate is 0 for the Resourse 02",
						"Hourly Rate should be 0 for the Resourse 02", "Hourly Rate is not 0 for the Resourse 02",
						"fail");
			}

			click(chk_productionProjectResourse);
			if (isSelected(chk_productionProjectResourse)) {
				writeTestResults("Verify that user able to enable Production/Project Resourse flag for the Resourse 02",
						"User should be able to enable Production/Project Resourse flag for the Resourse 02",
						"User successfully enable Production/Project Resourse flag for the Resourse 02", "pass");
			} else {
				writeTestResults("Verify that user able to enable Production/Project Resourse flag for the Resourse 02",
						"User should be able to enable Production/Project Resourse flag for the Resourse 02",
						"User couldn't enable Production/Project Resourse flag for the Resourse 02", "fail");
			}

			explicitWaitUntillClickable(btn_overheadLookupResourseInformation, 10);
			click(btn_overheadLookupResourseInformation);
			explicitWaitUntillClickable(txt_searchOverhead, 15);
			Thread.sleep(2000);
			sendKeysLookup(txt_searchOverhead, readProductionData("OverheadOne_PROD_E2E_001"));
			pressEnter(txt_searchOverhead);
			explicitWaitUntillClickable(td_resultOveheadInformationOverheadReplace.replace("overhead",
					readProductionData("OverheadOne_PROD_E2E_001")), 40);
			if (isDisplayed(td_resultOveheadInformationOverheadReplace.replace("overhead",
					readProductionData("OverheadOne_PROD_E2E_001")))) {
				writeTestResults("Verify that user able to find relevent Overhead One for the Resourse 02",
						"User should be able to find relevent Overhead One for the Resourse 02",
						"User successfully find relevent Overhead One for the Resourse 02", "pass");
			} else {
				writeTestResults("Verify that user able to find relevent Overhead One for the Resourse 02",
						"User should be able to find relevent Overhead One for the Resourse 02",
						"User couldn't find relevent Overhead One for the Resourse 02", "fail");
			}
			doubleClick(td_resultOveheadInformationOverheadReplace.replace("overhead",
					readProductionData("OverheadOne_PROD_E2E_001")));
			Thread.sleep(2000);

			sendKeys(txt_overheadeRateAttachedToRsourseOne, hourlyRateOfOverheadeAttachedToResourseTwo);
			String enteredHourlyRateOverheadOneResourseTwo = getAttribute(txt_overheadeRateAttachedToRsourseOne,
					"value");
			if (enteredHourlyRateOverheadOneResourseTwo.equals(hourlyRateOfOverheadeAttachedToResourseTwo)) {
				writeTestResults(
						"Verify that user able to enter Hourly Rate for the Overhead One that attached to Resourse 02",
						"User should be able to enter Hourly Rate for the Overhead One that attached to the Resourse 02",
						"User successfully enter Hourly Rate for the Overhead One that attached to Resourse 02",
						"pass");
			} else {
				writeTestResults(
						"Verify that user able to enter Hourly Rate for the Overhead One that attached to Resourse 02",
						"User should be able to enter Hourly Rate for the Overhead One that attached to the Resourse 02",
						"User couldn't enter Hourly Rate for the Overhead One that attached to Resourse 02", "fail");
			}

			click(btn_addNewRowOverheadRI);

			explicitWaitUntillClickable(btn_overheadSecondRowLookupResourseInformation, 10);
			click(btn_overheadSecondRowLookupResourseInformation);
			explicitWaitUntillClickable(txt_searchOverhead, 15);
			Thread.sleep(2000);
			sendKeysLookup(txt_searchOverhead, readProductionData("OverheadTwo_PROD_E2E_001"));
			pressEnter(txt_searchOverhead);
			explicitWaitUntillClickable(td_resultOveheadInformationOverheadReplace.replace("overhead",
					readProductionData("OverheadTwo_PROD_E2E_001")), 40);
			if (isDisplayed(td_resultOveheadInformationOverheadReplace.replace("overhead",
					readProductionData("OverheadTwo_PROD_E2E_001")))) {
				writeTestResults("Verify that user able to find relevent Overhead Two for the Resourse 02",
						"User should be able to find relevent Overhead Two for the Resourse 02",
						"User successfully find relevent Overhead Two for the Resourse 02", "pass");
			} else {
				writeTestResults("Verify that user able to find relevent Overhead Two for the Resourse 02",
						"User should be able to find relevent Overhead Two for the Resourse 02",
						"User couldn't find relevent Overhead Two for the Resourse 02", "fail");
			}
			doubleClick(td_resultOveheadInformationOverheadReplace.replace("overhead",
					readProductionData("OverheadTwo_PROD_E2E_001")));
			Thread.sleep(2000);

			sendKeys(txt_overheadeSecondRowRateAttachedToRsourseOne, hourlyRateOfOverheadeTwoAttachedToResourseTwo);
			String enteredHourlyRateOverheadTwoResourseTwo = getAttribute(
					txt_overheadeSecondRowRateAttachedToRsourseOne, "value");
			if (enteredHourlyRateOverheadTwoResourseTwo.equals(hourlyRateOfOverheadeTwoAttachedToResourseTwo)) {
				writeTestResults(
						"Verify that user able to enter Hourly Rate for the Overhead Two that attached to Resourse 02",
						"User should be able to enter Hourly Rate for the Overhead Two that attached to the Resourse 02",
						"User successfully enter Hourly Rate for the Overhead Two that attached to Resourse 02",
						"pass");
			} else {
				writeTestResults(
						"Verify that user able to enter Hourly Rate for the Overhead Two that attached to Resourse 02",
						"User should be able to enter Hourly Rate for the Overhead Two that attached to the Resourse 02",
						"User couldn't enter Hourly Rate for the Overhead Two that attached to Resourse 02", "fail");
			}

			click(btn_draft);
			explicitWaitUntillClickable(btn_release, 60);
			click(btn_release);
			explicitWait(header_releasedReourse, 60);
			writeProductionData("ResourceCode02_E2E", resourseCode2, 16);
			if (isDisplayed(header_releasedReourse)) {
				writeTestResults("Verify that user able to release Resourse 02",
						"User should be able to release Resourse 02", "User successfully release Resourse 02", "pass");
			} else {
				writeTestResults("Verify that user able to release Resourse 02",
						"User should be able to release Resourse 02", "User couldn't release Resourse 02", "fail");
			}
		}
	}

	public void billOfOperation() throws Exception {
		pageRefersh();
		explicitWait(img_navigationPane, 20);
		Thread.sleep(2000);
		explicitWait(img_navigationPane, 20);
		click(img_navigationPane);
		explicitWait(btn_productionModule, 10);
		click(btn_productionModule);
		explicitWait(btn_billOfOperation, 10);
		click(btn_billOfOperation);
		explicitWait(header_BillOfOperationByPage, 40);
		if (isDisplayed(header_BillOfOperationByPage)) {
			writeTestResults("Verify user can navigate to the Bill Of Operation by-page",
					"User should be able to navigate to Bill Of Operation by-page",
					"User successfully navigate to Bill Of Operation by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to the Bill Of Operation by-page",
					"User should be able to navigate to Bill Of Operation by-page",
					"User doesn't navigate to Bill Of Operation by-page", "fail");

		}
		click(btn_newBillOfOperation);
		explicitWait(header_newBillOfOperation, 50);
		if (isDisplayed(header_newBillOfOperation)) {
			writeTestResults("Verify user can navigate to the new Bill Of Operation",
					"User should be able to navigate to new Bill Of Operation",
					"User successfully navigate to new Bill Of Operation", "pass");
		} else {
			writeTestResults("Verify user can navigate to the new Bill Of Operation",
					"User should be able to navigate to new Bill Of Operation",
					"User doesn't navigate to new Bill Of Operation", "fail");
		}

		String bookCodeBOO = invObj.currentTimeAndDate().replace("/", "").replace(":", "");
		sendKeys(txt_booCodeBillOfOperation, bookCodeBOO);

		click(btn_addBillOfOperationRowReplace.replace("row", "1"));

		explicitWait(drop_elementCategoryBOO, 20);
		selectText(drop_elementCategoryBOO, elementCategorySupply);
		Thread.sleep(1000);
		String initialBOOElementCategory = getSelectedOptionInDropdown(drop_elementCategoryBOO);
		if (initialBOOElementCategory.equals(elementCategorySupply)) {
			writeTestResults("Verify user able to select 'Supply' as Element Category for the initial BOO",
					"User should be able to select 'Supply' as Element Category for the initial BOO",
					"User successfully select 'Supply' as Element Category for the initial BOO", "pass");
		} else {
			writeTestResults("Verify user able to select 'Supply' as Element Category for the initial BOO",
					"User should be able to select 'Supply' as Element Category for the initial BOO",
					"User doesn't select 'Supply' as Element Category for the initial BOO", "fail");
		}

		selectText(drop_activityTypeBOO, activityTypeMoveStock);
		Thread.sleep(1000);
		String initialBOOActivityType = getSelectedOptionInDropdown(drop_activityTypeBOO);
		if (initialBOOActivityType.equals(activityTypeMoveStock)) {
			writeTestResults("Verify user able to select 'Move Stock' as Activity Type for the initial BOO",
					"User should be able to select 'Move Stock' as Activity Type for the initial BOO",
					"User successfully select 'Move Stock' as Activity Type for the initial BOO", "pass");
		} else {
			writeTestResults("Verify user able to select 'Move Stock' as Activity Type for the initial BOO",
					"User should be able to select 'Move Stock' as Activity Type for the initial BOO",
					"User doesn't select 'Move Stock' as Activity Type for the initial BOO", "fail");
		}

		sendKeys(txt_elementDescriptionBOO, moveStockElementDescription);
		Thread.sleep(1000);
		String initialBOOEnteredDesc = getAttribute(txt_elementDescriptionBOO, "value");
		if (initialBOOEnteredDesc.equals(moveStockElementDescription)) {
			writeTestResults("Verify user able to enter Element Description for the initial BOO",
					"User should be able to enter Element Description for the initial BOO",
					"User successfully enter Element Description for the initial BOO", "pass");
		} else {
			writeTestResults("Verify user able to enter Element Description for the initial BOO",
					"User should be able to enter Element Description for the initial BOO",
					"User doesn't enter Element Description for the initial BOO", "fail");
		}

		click(btn_applyOperationOneBOO);

		explicitWait(btn_addBillOfOperationRowReplace.replace("row", "2"), 10);
		click(btn_addBillOfOperationRowReplace.replace("row", "2"));

		explicitWait(drop_elementCategoryBOO, 20);
		selectText(drop_elementCategoryBOO, elementCategoryOperation);
		Thread.sleep(1000);
		String firstBOOElementCategory = getSelectedOptionInDropdown(drop_elementCategoryBOO);
		if (firstBOOElementCategory.equals(elementCategoryOperation)) {
			writeTestResults("Verify user able to select 'Operation' as Element Category for the first BOO",
					"User should be able to select 'Operation' as Element Category for the first BOO",
					"User successfully select 'Operation' as Element Category for the first BOO", "pass");
		} else {
			writeTestResults("Verify user able to select 'Operation' as Element Category for the first BOO",
					"User should be able to select 'Operation' as Element Category for the first BOO",
					"User doesn't select 'Operation' as Element Category for the first BOO", "fail");
		}

		Thread.sleep(1000);
		String firstBOOActivityType = getSelectedOptionInDropdown(drop_activityTypeBOO);
		if (firstBOOActivityType.equals(activityTypeProduce)) {
			writeTestResults("Verify auto select 'Produce' as Activity Type for the first BOO",
					"Should auto select 'Produce' as Activity Type for the first BOO",
					"Auto select 'Produce' as Activity Type for the first BOO", "pass");
		} else {
			writeTestResults("Verify auto select 'Produce' as Activity Type for the first BOO",
					"Should auto select 'Produce' as Activity Type for the first BOO",
					"Not auto select 'Produce' as Activity Type for the first BOO", "fail");
		}

		sendKeys(txt_elementDescriptionBOO, produceElementDescription1);
		Thread.sleep(1000);
		String firstBOOEnteredDesc = getAttribute(txt_elementDescriptionBOO, "value");
		if (firstBOOEnteredDesc.equals(produceElementDescription1)) {
			writeTestResults("Verify user able to enter Element Description for the first BOO",
					"User should be able to enter Element Description for the first BOO",
					"User successfully enter Element Description for the first BOO", "pass");
		} else {
			writeTestResults("Verify user able to enter Element Description for the first BOO",
					"User should be able to enter Element Description for the first BOO",
					"User doesn't enter Element Description for the first BOO", "fail");
		}

		sendKeys(txt_groupIdBOO, groupIdBOOOne);
		Thread.sleep(1000);
		String firstBOOEnteredGroupID = getAttribute(txt_groupIdBOO, "value");
		if (firstBOOEnteredGroupID.equals(groupIdBOOOne)) {
			writeTestResults("Verify user able to enter Group ID for the first BOO",
					"User should be able to enter Group ID for the first BOO",
					"User successfully enter Group ID for the first BOO", "pass");
		} else {
			writeTestResults("Verify user able to enter Group ID for the first BOO",
					"User should be able to enter Group ID for the first BOO",
					"User doesn't enter Group ID for the first BOO", "fail");
		}

		click(btn_mainResourseLookuoOperationOne);
		explicitWaitUntillClickable(txt_searchResource, 30);
		Thread.sleep(2000);
		sendKeysLookup(txt_searchResource, readProductionData("ResourceCode01_E2E"));
		pressEnter(txt_searchResource);
		explicitWaitUntillClickable(
				td_resultResourseOperationOne.replace("resource", readProductionData("ResourceCode01_E2E")), 40);
		doubleClick(td_resultResourseOperationOne.replace("resource", readProductionData("ResourceCode01_E2E")));
		Thread.sleep(2000);
		String selectedMainResourse = getAttribute(txt_mainResourseOnLookupOperationOne, "value");
		if (selectedMainResourse.contains(readProductionData("ResourceCode01_E2E"))) {
			writeTestResults("Verify user able to select Main Resource for the first BOO",
					"User should be able to select Main Resource for the first BOO",
					"User successfully select Main Resource for the first BOO", "pass");
		} else {
			writeTestResults("Verify user able to select Main Resource for the first BOO",
					"User should be able to select Main Resource for the first BOO",
					"User doesn't select Main Resource for the first BOO", "fail");
		}

		click(txt_fixedDurationOpeartionOne);
		explicitWaitUntillClickable(a_oneHourFixedDurationOpeationOne, 20);
		click(a_oneHourFixedDurationOpeationOne);
		Thread.sleep(700);
		click(btn_okFixedDurationOpeationOne);
		Thread.sleep(2000);
		String selecteFixedDurationHours = getAttribute(txt_fixedDurationOpeartionOne, "value");
		String selecteFixedDurationMins = getAttribute(txt_fixedDurationMinOpeartionOne, "value");
		if (selecteFixedDurationHours.equals("01") && selecteFixedDurationMins.equals("00")) {
			writeTestResults("Verify user able to select Fixed Duration as 1 for the first BOO",
					"User should be able to select Fixed Duration as 1 for the first BOO",
					"User successfully select Fixed Duration as 1 for the first BOO", "pass");
		} else {
			writeTestResults("Verify user able to select Fixed Duration as 1 for the first BOO",
					"User should be able to select Fixed Duration as 1 for the first BOO",
					"User doesn't select Fixed Duration as 1 for the first BOO", "fail");
		}

		Thread.sleep(1000);
		String firstBOOBatchMethod = getSelectedOptionInDropdown(drop_batchMethodBOO);
		if (firstBOOBatchMethod.equals(batchMethodTransferBOO)) {
			writeTestResults("Verify auto select 'Transfer' as Batch Method for the first BOO",
					"Should auto select 'Transfer' as Batch Method for the first BOO",
					"Auto select 'Transfer' as Batch Method for the first BOO", "pass");
		} else {
			writeTestResults("Verify auto select 'Transfer' as Batch Method for the first BOO",
					"Should auto select 'Transfer' as Batch Method for the first BOO",
					"Not auto select 'Transfer' as Batch Method for the first BOO", "fail");
		}

		click(btn_applyOperationOneBOO);

		explicitWait(btn_addBillOfOperationRowReplace.replace("row", "2"), 10);
		click(btn_addBillOfOperationRowReplace.replace("row", "2"));

		explicitWait(drop_elementCategoryBOO, 20);
		selectText(drop_elementCategoryBOO, elementCategoryOperation);
		Thread.sleep(1000);
		String secondBOOElementCategory = getSelectedOptionInDropdown(drop_elementCategoryBOO);
		if (secondBOOElementCategory.equals(elementCategoryOperation)) {
			writeTestResults("Verify user able to select 'Operation' as Element Category for the second BOO",
					"User should be able to select 'Operation' as Element Category for the second BOO",
					"User successfully select 'Operation' as Element Category for the second BOO", "pass");
		} else {
			writeTestResults("Verify user able to select 'Operation' as Element Category for the second BOO",
					"User should be able to select 'Operation' as Element Category for the second BOO",
					"User doesn't select 'Operation' as Element Category for the second BOO", "fail");
		}

		Thread.sleep(1000);
		String secondBOOActivityType = getSelectedOptionInDropdown(drop_activityTypeBOO);
		if (secondBOOActivityType.equals(activityTypeProduce)) {
			writeTestResults("Verify auto select 'Produce' as Activity Type for the second BOO",
					"Should auto select 'Produce' as Activity Type for the second BOO",
					"Auto select 'Produce' as Activity Type for the second BOO", "pass");
		} else {
			writeTestResults("Verify auto select 'Produce' as Activity Type for the second BOO",
					"Should auto select 'Produce' as Activity Type for the second BOO",
					"Not auto select 'Produce' as Activity Type for the second BOO", "fail");
		}

		sendKeys(txt_elementDescriptionBOO, produceElementDescription2);
		Thread.sleep(1000);
		String secondBOOEnteredDesc = getAttribute(txt_elementDescriptionBOO, "value");
		if (secondBOOEnteredDesc.equals(produceElementDescription2)) {
			writeTestResults("Verify user able to enter Element Description for the second BOO",
					"User should be able to enter Element Description for the second BOO",
					"User successfully enter Element Description for the second BOO", "pass");
		} else {
			writeTestResults("Verify user able to enter Element Description for the second BOO",
					"User should be able to enter Element Description for the second BOO",
					"User doesn't enter Element Description for the second BOO", "fail");
		}

		sendKeys(txt_groupIdBOO, groupIdBOOTwo);
		Thread.sleep(1000);
		String secondBOOEnteredGroupID = getAttribute(txt_groupIdBOO, "value");
		if (secondBOOEnteredGroupID.equals(groupIdBOOTwo)) {
			writeTestResults("Verify user able to enter Group ID for the second BOO",
					"User should be able to enter Group ID for the second BOO",
					"User successfully enter Group ID for the second BOO", "pass");
		} else {
			writeTestResults("Verify user able to enter Group ID for the second BOO",
					"User should be able to enter Group ID for the second BOO",
					"User doesn't enter Group ID for the second BOO", "fail");
		}

		Thread.sleep(1000);
		String secondBOOBatchMethod = getSelectedOptionInDropdown(drop_batchMethodBOO);
		if (secondBOOBatchMethod.equals(batchMethodTransferBOO)) {
			writeTestResults("Verify auto select 'Transfer' as Batch Method for the second BOO",
					"Should auto select 'Transfer' as Batch Method for the second BOO",
					"Auto select 'Transfer' as Batch Method for the second BOO", "pass");
		} else {
			writeTestResults("Verify auto select 'Transfer' as Batch Method for the second BOO",
					"Should auto select 'Transfer' as Batch Method for the second BOO",
					"Not auto select 'Transfer' as Batch Method for the second BOO", "fail");
		}

		click(btn_applyOperationOneBOO);

		explicitWait(btn_addBillOfOperationRowReplace.replace("row", "2"), 10);
		click(btn_addBillOfOperationRowReplace.replace("row", "2"));

		explicitWait(drop_elementCategoryBOO, 20);
		selectText(drop_elementCategoryBOO, elementCategoryOperation);
		Thread.sleep(1000);
		String thirdBOOElementCategory = getSelectedOptionInDropdown(drop_elementCategoryBOO);
		if (thirdBOOElementCategory.equals(elementCategoryOperation)) {
			writeTestResults("Verify user able to select 'Operation' as Element Category for the third BOO",
					"User should be able to select 'Operation' as Element Category for the third BOO",
					"User successfully select 'Operation' as Element Category for the third BOO", "pass");
		} else {
			writeTestResults("Verify user able to select 'Operation' as Element Category for the third BOO",
					"User should be able to select 'Operation' as Element Category for the third BOO",
					"User doesn't select 'Operation' as Element Category for the third BOO", "fail");
		}

		Thread.sleep(1000);
		selectText(drop_activityTypeBOO, activityTypeQualityCheck);
		String thirdBOOActivityType = getSelectedOptionInDropdown(drop_activityTypeBOO);
		if (thirdBOOActivityType.equals(activityTypeQualityCheck)) {
			writeTestResults("Verify user able to select 'Quality Check' as Activity Type for the third BOO",
					"User should be able to select 'Quality Check' as Activity Type for the third BOO",
					"User successfully select 'Quality Check' as Activity Type for the third BOO", "pass");
		} else {
			writeTestResults("Verify user able to select 'Quality Check' as Activity Type for the third BOO",
					"User should be able to select 'Quality Check' as Activity Type for the third BOO",
					"User doesn't select 'Quality Check' as Activity Type for the third BOO", "fail");
		}

		sendKeys(txt_elementDescriptionBOO, qcElementDescription);
		Thread.sleep(1000);
		String thirdBOOEnteredDesc = getAttribute(txt_elementDescriptionBOO, "value");
		if (thirdBOOEnteredDesc.equals(qcElementDescription)) {
			writeTestResults("Verify user able to enter Element Description for the third BOO",
					"User should be able to enter Element Description for the third BOO",
					"User successfully enter Element Description for the third BOO", "pass");
		} else {
			writeTestResults("Verify user able to enter Element Description for the third BOO",
					"User should be able to enter Element Description for the third BOO",
					"User doesn't enter Element Description for the third BOO", "fail");
		}

		sendKeys(txt_groupIdBOO, groupIdBOOThree);
		Thread.sleep(1000);
		String thirdBOOEnteredGroupID = getAttribute(txt_groupIdBOO, "value");
		if (thirdBOOEnteredGroupID.equals(groupIdBOOThree)) {
			writeTestResults("Verify user able to enter Group ID for the third BOO",
					"User should be able to enter Group ID for the third BOO",
					"User successfully enter Group ID for the third BOO", "pass");
		} else {
			writeTestResults("Verify user able to enter Group ID for the third BOO",
					"User should be able to enter Group ID for the third BOO",
					"User doesn't enter Group ID for the third BOO", "fail");
		}

		click(chk_finalInvestigationBOO);
		if (isSelected(chk_finalInvestigationBOO)) {
			writeTestResults("Verify user able to enable Final Investigation flag for the third BOO",
					"User should be able to enable Final Investigation flag for the third BOO",
					"User successfully enable Final Investigation flag for the third BOO", "pass");
		} else {
			writeTestResults("Verify user able to enable Final Investigation flag for the third BOO",
					"User should be able to enable Final Investigation flag for the third BOO",
					"User doesn't enable Final Investigation flag for the third BOO", "fail");
		}

		Thread.sleep(1000);
		String thirdBOOBatchMethod = getSelectedOptionInDropdown(drop_batchMethodBOO);
		if (thirdBOOBatchMethod.equals(batchMethodTransferBOO)) {
			writeTestResults("Verify auto select 'Transfer' as Batch Method for the third BOO",
					"Should auto select 'Transfer' as Batch Method for the third BOO",
					"Auto select 'Transfer' as Batch Method for the third BOO", "pass");
		} else {
			writeTestResults("Verify auto select 'Transfer' as Batch Method for the third BOO",
					"Should auto select 'Transfer' as Batch Method for the third BOO",
					"Not auto select 'Transfer' as Batch Method for the third BOO", "fail");
		}

		click(btn_applyOperationOneBOO);

		click(btn_draft);
		explicitWait(header_draftedBOO, 50);
		if (isDisplayed(header_draftedBOO)) {
			writeTestResults("Verify user able to draft the Bill Of Operation",
					"User should be able to draft the Bill Of Operation",
					"User successfully draft the Bill Of Operation", "pass");
		} else {
			writeTestResults("Verify user able to draft the Bill Of Operation",
					"User should be able to draft the Bill Of Operation", "User doesn't draft the Bill Of Operation",
					"fail");

		}

		click(btn_release);
		explicitWait(header_releasedBOO, 50);
		trackCode = getText(lbl_docNoBoo);
		writeProductionData("BOO_PROD_E2E_001", trackCode, 17);
		if (isDisplayed(header_releasedBOO)) {
			writeTestResults("Verify user able to release the Bill Of Operation",
					"User should be able to release the Bill Of Operation",
					"User successfully release the Bill Of Operation", "pass");
		} else {
			writeTestResults("Verify user able to release the Bill Of Operation",
					"User should be able to release the Bill Of Operation",
					"User doesn't release the Bill Of Operation", "fail");

		}
	}

	public void productionUnit() throws Exception {
		pageRefersh();
		explicitWaitUntillClickable(img_navigationPane, 60);
		Thread.sleep(2000);
		explicitWaitUntillClickable(img_navigationPane, 20);
		click(img_navigationPane);

		explicitWaitUntillClickable(btn_organizationMgt, 10);
		click(btn_organizationMgt);

		explicitWaitUntillClickable(btn_locationInfo, 20);
		click(btn_locationInfo);

		explicitWaitUntillClickable(btn_newLocation, 20);
		explicitWaitUntillClickable(
				div_productionUnitReplaceCode.replace("code", readProductionData("Location_PROD_E2E")), 20);

		if (!isDisplayedQuickCheck(
				div_productionUnitReplaceCode.replace("code", readProductionData("Location_PROD_E2E")))) {
			click(btn_newLocation);

			explicitWaitUntillClickable(txt_locationCOde, 60);
			String locationCode = common.getCurrentDateTimeAsUniqueCode() + "-E2E";
			sendKeys(txt_locationCOde, locationCode);
			String enteredLocationCode = getAttribute(txt_locationCOde, "value");
			if (enteredLocationCode.equals(locationCode)) {
				writeTestResults("Verify that user able to enter Location Code",
						"User should be able to enter Location Code", "User successfully enter Location Code", "pass");
			} else {
				writeTestResults("Verify that user able to enter Location Code",
						"User should be able to enter Location Code", "User couldn't enter Location Code", "fail");

			}

			sendKeys(txt_descriptionLocationInformation, locationDescription);

			selectIndex(drop_businessUnitLI, 1);

			Thread.sleep(2000);
			selectText(drop_inputWarehouseLI, inputWarehouseLI);
			Thread.sleep(2000);
			String selectedInputWarhouse = getSelectedOptionInDropdown(drop_inputWarehouseLI);
			if (selectedInputWarhouse.equals(inputWarehouseLI)) {
				writeTestResults("Verify that user able to select Input Warehouse",
						"User should be able to select Input Warehouse", "User successfully select Input Warehouse",
						"pass");
			} else {
				writeTestResults("Verify that user able to select Input Warehouse",
						"User should be able to select Input Warehouse", "User couldn't select Input Warehouse",
						"fail");

			}

			selectText(drop_outputWarehouseLI, outputWarehouseLI);
			Thread.sleep(2000);
			String selectedOutputWarhouse = getSelectedOptionInDropdown(drop_outputWarehouseLI);
			if (selectedOutputWarhouse.equals(outputWarehouseLI)) {
				writeTestResults("Verify that user able to select Output Warehouse",
						"User should be able to select Output Warehouse", "User successfully select Output Warehouse",
						"pass");
			} else {
				writeTestResults("Verify that user able to select Output Warehouse",
						"User should be able to select Output Warehouse", "User couldn't select Output Warehouse",
						"fail");

			}

			selectText(drop_wipWarehouseLI, wipWarehouseLI);
			Thread.sleep(2000);
			String selectedWipWarhouse = getSelectedOptionInDropdown(drop_wipWarehouseLI);
			if (selectedWipWarhouse.equals(wipWarehouseLI)) {
				writeTestResults("Verify that user able to select WIP Warehouse",
						"User should be able to select WIP Warehouse", "User successfully select WIP Warehouse",
						"pass");
			} else {
				writeTestResults("Verify that user able to select WIP Warehouse",
						"User should be able to select WIP Warehouse", "User couldn't select WIP Warehouse", "fail");

			}

			sendKeys(drop_addressLI, locationAddress);

			selectIndex(drop_siteLI, 1);

			click(btn_draftLI);
			Thread.sleep(2000);
			if (!isDisplayedQuickCheck(window_newLocation)) {
				writeTestResults("Verify user able to draft a Location", "User should be able to draft a Location",
						"User successfully draft a Location", "pass");
			} else {
				writeTestResults("Verify user able to draft a Location", "User should be able to draft a Location",
						"User couldn't draft a Location", "fail");
			}

			explicitWaitUntillClickable(div_selectLocationReplaceLocation.replace("location_code", locationCode), 10);
			click(div_selectLocationReplaceLocation.replace("location_code", locationCode));

			explicitWait(div_selectedLocationReplaceLocation.replace("location_code", locationCode), 10);
			if (isDisplayedQuickCheck(div_selectedLocationReplaceLocation.replace("location_code", locationCode))) {
				writeTestResults("Verify user able to find relevent Location",
						"User should be able to find relevent Location", "User successfully find relevent Location",
						"pass");
			} else {
				writeTestResults("Verify user able to find relevent Location",
						"User should be able to find relevent Location", "User couldn't find relevent Location",
						"fail");
			}

			Thread.sleep(1500);
			explicitWaitUntillClickable(a_acitveLocation, 6);
			click(a_acitveLocation);
			explicitWaitUntillClickable(btn_yesConfirmationLocationActivation, 6);
			click(btn_yesConfirmationLocationActivation);

			explicitWaitUntillClickable(b_activatedStatusLocation, 30);
			trackCode = locationCode;
			if (isDisplayedQuickCheck(b_activatedStatusLocation)) {
				writeTestResults("Verify user able to activate the Location",
						"User should be able to activate the Location", "User successfully activate the Location",
						"pass");
			} else {
				writeTestResults("Verify user able to activate the Location",
						"User should be able to activate the Location", "User couldn't activate the Location", "fail");
			}

			writeProductionData("Location_PROD_E2E", locationCode, 18);
		}
	}

	public void pricingProfile() throws Exception {
		pageRefersh();
		explicitWait(img_navigationPane, 20);
		Thread.sleep(2000);
		explicitWait(img_navigationPane, 20);
		click(img_navigationPane);
		explicitWait(btn_productionModule, 10);
		click(btn_productionModule);
		explicitWait(btn_pricingProfile2, 10);
		click(btn_pricingProfile2);
		explicitWait(btn_newPricingProfile2, 10);
		explicitWait(div_pricingProfileReplaceCode.replace("code", readProductionData("Pricing_Profile_PROD_E2E")), 20);

		if (!isDisplayedQuickCheck(
				div_pricingProfileReplaceCode.replace("code", readProductionData("Pricing_Profile_PROD_E2E")))) {
			click(btn_newPricingProfile2);

			explicitWait(txt_pricingProfileCode2, 10);
			String pricingProfileCode = common.getCurrentDateTimeAsUniqueCode() + "-E2E";
			sendKeys(txt_pricingProfileCode2, pricingProfileCode);
			String enteredPricingProfileCode = getAttribute(txt_pricingProfileCode2, "value");
			if (enteredPricingProfileCode.equals(pricingProfileCode)) {
				writeTestResults("Verify that user able to enter Pricing Profile Code",
						"User should be able to enter Pricing Profile Code",
						"User successfully enter Pricing Profile Code", "pass");
			} else {
				writeTestResults("Verify that user able to enter Pricing Profile Code",
						"User should be able to enter Pricing Profile Code", "User couldn't enter Pricing Profile Code",
						"fail");

			}

			sendKeys(txt_pricingProfileDescription, descriptionPP);

			selectText(drop_pricingGroupPP, pricingProfileGroup);
			Thread.sleep(1000);
			String selectdPPGroup = getSelectedOptionInDropdown(drop_pricingGroupPP);
			if (selectdPPGroup.equals(pricingProfileGroup)) {
				writeTestResults("Verify that user able to select Pricing Profile Group as Production",
						"User should be able to select Pricing Profile Group as Production",
						"User successfully select Pricing Profile Group as Production", "pass");
			} else {
				writeTestResults("Verify that user able to select Pricing Profile Group as Production",
						"User should be able to select Pricing Profile Group as Production",
						"User couldn't select Pricing Profile Group as Production", "fail");

			}

			click(tab_estimationPricingProfile);

			explicitWaitUntillClickable(drop_materialEstimationTabPP, 15);
			selectText(drop_materialEstimationTabPP, materialEstimationTabPP);
			Thread.sleep(1000);
			String selectdMaterialEstimationTab = getSelectedOptionInDropdown(drop_materialEstimationTabPP);
			if (selectdMaterialEstimationTab.equals(materialEstimationTabPP)) {
				writeTestResults("Verify that user able to select Material on estimation tab as LPP",
						"User should be able to select Material on estimation tab as LPP",
						"User successfully select Material on estimation tab as LPP", "pass");
			} else {
				writeTestResults("Verify that user able to select Material on estimation tab as LPP",
						"User should be able to select Material on estimation tab as LPP",
						"User couldn't select Material on estimation tab as LPP", "fail");

			}

			selectText(drop_labourResorseEstimationTabPP, labourResorceEstimatioTabPP);
			Thread.sleep(1000);
			String selectdLabourResorceEstimationTab = getSelectedOptionInDropdown(drop_labourResorseEstimationTabPP);
			if (selectdLabourResorceEstimationTab.equals(labourResorceEstimatioTabPP)) {
				writeTestResults("Verify that user able to select Labour / Resource on estimation tab as Rate Profile",
						"User should be able to select Labour / Resource on estimation tab as Rate Profile",
						"User successfully select Labour / Resource on estimation tab as Rate Profile", "pass");
			} else {
				writeTestResults("Verify that user able to select Labour / Resource on estimation tab as Rate Profile",
						"User should be able to select Labour / Resource on estimation tab as Rate Profile",
						"User couldn't select Labour / Resource on estimation tab as Rate Profile", "fail");

			}

			selectText(drop_overheadEstimationTabPP, overheadEstimatioTabPP);
			Thread.sleep(1000);
			String selectdOverheadEstimationTab = getSelectedOptionInDropdown(drop_overheadEstimationTabPP);
			if (selectdOverheadEstimationTab.equals(overheadEstimatioTabPP)) {
				writeTestResults("Verify that user able to select Overhead on estimation tab as Standard",
						"User should be able to select Overhead on estimation tab as Standard",
						"User successfully select Overhead on estimation tab as Standard", "pass");
			} else {
				writeTestResults("Verify that user able to select Overhead on estimation tab as Standard",
						"User should be able to select Overhead on estimation tab as Standard",
						"User couldn't select Overhead on estimation tab as Standard", "fail");

			}

			selectText(drop_serviceEstimationTabPP, serviceEstimatioTabPP);
			Thread.sleep(1000);
			String selectdServiceEstimationTab = getSelectedOptionInDropdown(drop_serviceEstimationTabPP);
			if (selectdServiceEstimationTab.equals(serviceEstimatioTabPP)) {
				writeTestResults("Verify that user able to select Service on estimation tab as Custom",
						"User should be able to select Service on estimation tab as Custom",
						"User successfully select Service on estimation tab as Custom", "pass");
			} else {
				writeTestResults("Verify that user able to select Service on estimation tab as Custom",
						"User should be able to select Service on estimation tab as Custom",
						"User couldn't select Service on estimation tab as Custom", "fail");

			}

			selectText(drop_expenceEstimationTabPP, expenceEstimatioTabPP);
			Thread.sleep(1000);
			String selectdExpenceEstimationTab = getSelectedOptionInDropdown(drop_expenceEstimationTabPP);
			if (selectdExpenceEstimationTab.equals(expenceEstimatioTabPP)) {
				writeTestResults("Verify that user able to select Expence on estimation tab as Custom",
						"User should be able to select Expence on estimation tab as Custom",
						"User successfully select Expence on estimation tab as Custom", "pass");
			} else {
				writeTestResults("Verify that user able to select Expence on estimation tab as Custom",
						"User should be able to select Expence on estimation tab as Custom",
						"User couldn't select Expence on estimation tab as Custom", "fail");

			}

			click(btn_actualCalclationTabPP);
			explicitWaitUntillClickable(drop_materialActualTabPP, 10);
			selectText(drop_materialActualTabPP, materialActualTabPP);
			Thread.sleep(1000);
			String selectdMaterialActualTab = getSelectedOptionInDropdown(drop_materialActualTabPP);
			if (selectdMaterialActualTab.equals(materialActualTabPP)) {
				writeTestResults("Verify that user able to select Material on actual tab as Actual",
						"User should be able to select Material on actual tab as Actual",
						"User successfully select Material on actual tab as Actual", "pass");
			} else {
				writeTestResults("Verify that user able to select Material on actual tab as Actual",
						"User should be able to select Material on actual tab as Actual",
						"User couldn't select Material on actual tab as Actual", "fail");

			}

			selectText(drop_labourResorseActualTabPP, labourResourseActualTabPP);
			Thread.sleep(1000);
			String selectdLabourResourseActualTab = getSelectedOptionInDropdown(drop_labourResorseActualTabPP);
			if (selectdLabourResourseActualTab.equals(labourResourseActualTabPP)) {
				writeTestResults("Verify that user able to select Labour / Resourse on actual tab as Actual",
						"User should be able to select Labour / Resourse on actual tab as Actual",
						"User successfully select Labour / Resourse on actual tab as Actual", "pass");
			} else {
				writeTestResults("Verify that user able to select Labour / Resourse on actual tab as Actual",
						"User should be able to select Labour / Resourse on actual tab as Actual",
						"User couldn't select Labour / Resourse on actual tab as Actual", "fail");

			}

			selectText(drop_overheadActualTabPP, overheadActualTabPP);
			Thread.sleep(1000);
			String selectdOverheadActualTab = getSelectedOptionInDropdown(drop_overheadActualTabPP);
			if (selectdOverheadActualTab.equals(overheadActualTabPP)) {
				writeTestResults("Verify that user able to select Overhead on actual tab as Actual",
						"User should be able to select Overhead on actual tab as Actual",
						"User successfully select Overhead on actual tab as Actual", "pass");
			} else {
				writeTestResults("Verify that user able to select Overhead on actual tab as Actual",
						"User should be able to select Overhead on actual tab as Actual",
						"User couldn't select Overhead on actual tab as Actual", "fail");

			}

			selectText(drop_serviceActualTabPP, serviceActualTabPP);
			Thread.sleep(1000);
			String selectdServiceActualTab = getSelectedOptionInDropdown(drop_serviceActualTabPP);
			if (selectdServiceActualTab.equals(serviceActualTabPP)) {
				writeTestResults("Verify that user able to select Service on actual tab as Actual",
						"User should be able to select Service on actual tab as Actual",
						"User successfully select Service on actual tab as Actual", "pass");
			} else {
				writeTestResults("Verify that user able to select Service on actual tab as Actual",
						"User should be able to select Service on actual tab as Actual",
						"User couldn't select Service on actual tab as Actual", "fail");

			}

			selectText(drop_expenceActualTabPP, expenceActualTabPP);
			Thread.sleep(1000);
			String selectdExpenceActualTab = getSelectedOptionInDropdown(drop_expenceActualTabPP);
			if (selectdExpenceActualTab.equals(expenceActualTabPP)) {
				writeTestResults("Verify that user able to select Expence on actual tab as Actual",
						"User should be able to select Expence on actual tab as Actual",
						"User successfully select Expence on actual tab as Actual", "pass");
			} else {
				writeTestResults("Verify that user able to select Expence on actual tab as Actual",
						"User should be able to select Expence on actual tab as Actual",
						"User couldn't select Expence on actual tab as Actual", "fail");

			}

			click(btn_draftPP);
			Thread.sleep(2000);
			if (!isDisplayedQuickCheck(window_newPP)) {
				writeTestResults("Verify user able to draft a Pricing Profile",
						"User should be able to draft a Pricing Profile", "User successfully draft a Pricing Profile",
						"pass");
			} else {
				writeTestResults("Verify user able to draft a Pricing Profile",
						"User should be able to draft a Pricing Profile", "User couldn't draft a Pricing Profile",
						"fail");
			}

			explicitWaitUntillClickable(
					div_selectPricingProfilenReplacePricingProfile.replace("pricing_profile", pricingProfileCode), 15);
			click(div_selectPricingProfilenReplacePricingProfile.replace("pricing_profile", pricingProfileCode));

			explicitWait(div_selectedPricingProfileReplacePricingProfile.replace("pricing_profile", pricingProfileCode),
					10);
			if (isDisplayedQuickCheck(
					div_selectedPricingProfileReplacePricingProfile.replace("pricing_profile", pricingProfileCode))) {
				writeTestResults("Verify user able to find relevent Pricing Profile",
						"User should be able to find relevent Pricing Profile",
						"User successfully find relevent Pricing Profile", "pass");
			} else {
				writeTestResults("Verify user able to find relevent Pricing Profile",
						"User should be able to find relevent Pricing Profile",
						"User couldn't find relevent Pricing Profile", "fail");
			}

			Thread.sleep(1500);
			explicitWaitUntillClickable(a_acitvePP, 6);
			click(a_acitvePP);
			explicitWaitUntillClickable(btn_yesConfirmationPPActivation, 6);
			click(btn_yesConfirmationPPActivation);

			explicitWaitUntillClickable(b_activatedStatusPP, 30);
			trackCode = pricingProfileCode;
			if (isDisplayedQuickCheck(b_activatedStatusPP)) {
				writeTestResults("Verify user able to activate the Pricing Profile",
						"User should be able to activate the Pricing Profile",
						"User successfully activate the Pricing Profile", "pass");
			} else {
				writeTestResults("Verify user able to activate the Pricing Profile",
						"User should be able to activate the Pricing Profile",
						"User couldn't activate the Pricing Profile", "fail");
			}

			writeProductionData("Pricing_Profile_PROD_E2E", pricingProfileCode, 19);
		}
	}

	/* Production Model */
	public void productionModel1_PROD_E2E_001() throws Exception {
		pageRefersh();
		if (click(img_navigationPane)) {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User successfully click navigation pane", "pass");
		} else {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User couldn't click navigation pane", "fail");
		}

		explicitWait(btn_productionModule, 4);
		if (isEnabledQuickCheck(btn_productionModule)) {
			writeTestResults("Verify user able to view the \"Production\" module under main navigation menu",
					"User should be able to view the \"Production\" module under main navigation menu",
					"User successfully view the \"Production\" module under main navigation menu", "pass");
		} else {
			writeTestResults("Verify user able to view the \"Production\" module under main navigation menu",
					"User should be able to view the \"Production\" module under main navigation menu",
					"User couldn't view the \"Production\" module under main navigation menu", "fail");
		}

		click(btn_productionModule);

		explicitWait(btn_productionModel, 4);
		if (isEnabledQuickCheck(btn_productionModel)) {
			writeTestResults("Verify user able to view the \"Production Model\" option under production sub menu",
					"User should be able to view the \"Production Model\" option under production sub menu",
					"User able to view the \"Production Model\" option under production sub menu", "pass");
		} else {
			writeTestResults("Verify user able to view the \"Production Model\" option under production sub menu",
					"User should be able to view the \"Production Model\" option under production sub menu",
					"User couldn't view the \"Production Model\" option under production sub menu", "pass");
		}

		click(btn_productionModel);

		explicitWait(header_productionModelByPage, 40);
		if (isDisplayedQuickCheck(header_productionModelByPage)) {
			writeTestResults("Verify that user on the Production Model by page",
					"The user should be on the Production Model by page",
					"The user successfully on the Production Model by page", "pass");
		} else {
			writeTestResults("Verify that user on the Production Model by page",
					"The user should be on the Production Model by page",
					"The user not on the Production Model by page", "fail");
		}

		click(btn_newButtonProductionModel);
		explicitWait(header_newProductionModel, 40);
		if (isDisplayedQuickCheck(header_newProductionModel)) {
			writeTestResults("Verify that user can navigate to the new production model creation page",
					"The user should be navigate to the new production model creation page",
					"The user successfully navigate to the new production model creation page", "pass");
		} else {
			writeTestResults("Verify that user can navigate to the new production model creation page",
					"The user should be navigate to the new production model creation page",
					"The user doesn't navigate to the new production model creation page", "fail");
		}

		String modelCode = invObj.currentTimeAndDate().replace("/", "").replace(":", "");
		sendKeys(txt_modelCodeProductionModel, modelCode);
		String enteresModelCode = getAttribute(txt_modelCodeProductionModel, "value");
		if (enteresModelCode.equals(modelCode)) {
			writeTestResults(
					"Verify that system is displayed the user entered data in 'Model Code' field of summary sections",
					"The system should be displayed the user entered data in 'Model Code' field of summary sections",
					"The system successfully displayed the user entered data in 'Model Code' field of summary sections",
					"pass");
		} else {
			writeTestResults(
					"Verify that system is displayed the user entered data in 'Model Code' field of summary sections",
					"The system should be displayed the user entered data in 'Model Code' field of summary sections",
					"The system doesn't displayed the user entered data in 'Model Code' field of summary sections",
					"fail");
		}

		sendKeys(txt_descriptionProductionModel, commonDescription);
		String enteresDescription = getAttribute(txt_descriptionProductionModel, "value");
		if (enteresDescription.equals(commonDescription)) {
			writeTestResults(
					"Verify that system is displayed the user entered data in 'Description' field of summary sections",
					"The system should be displayed the user entered data in 'Description' field of summary sections",
					"The system successfully displayed the user entered data in 'Description' field of summary sections",
					"pass");
		} else {
			writeTestResults(
					"Verify that system is displayed the user entered data in 'Description' field of summary sections",
					"The system should be displayed the user entered data in 'Description' field of summary sections",
					"The system doesn't displayed the user entered data in 'Description' field of summary sections",
					"fail");
		}

		selectText(drop_productionGroupProductionModel, productionGroupE2E);
		explicitWaitUntillClickable(btn_yesClearConfirmation, 5);
		click(btn_yesClearConfirmation);
		Thread.sleep(2000);
		String selectedProductGroup = getSelectedOptionInDropdown(drop_productionGroupProductionModel);
		if (selectedProductGroup.equals(productionGroupE2E)) {
			writeTestResults("Verify that user selected product group is displayed",
					"The user selected product group should be displayed",
					"The user selected product group successfully displayed", "pass");
		} else {
			writeTestResults("Verify that user selected product group is displayed",
					"The user selected product group should be displayed",
					"The user selected product group doesn't displayed", "fail");
		}

	}

	public void productionModel2_PROD_E2E_001() throws Exception {
		click(btn_productLookupProductionModel);
		explicitWait(header_productLookup, 20);
		if (isDisplayedQuickCheck(header_productLookup)) {
			writeTestResults("Verify that product look up screen is loaded",
					"The product look up screen should be loaded", "The product look up screen successfully loaded",
					"pass");
		} else {
			writeTestResults("Verify that product look up screen is loaded",
					"The product look up screen should be loaded", "The product look up screen doesn't loaded", "fail");
		}

		sendKeysLookup(txt_searchProductCommon, invObj.readTestCreation("BatchFifoMultiPlatformProductionType"));
		Thread.sleep(500);
		String enteredProduct = getAttribute(txt_searchProductCommon, "value");
		if (enteredProduct.equals(invObj.readTestCreation("BatchFifoMultiPlatformProductionType"))) {
			writeTestResults("Verify that user entred product code/ description on the search area is displayed",
					"The user entred product code/ description on the search area should be displayed",
					"The user entred product code/ description on the search area successfully displayed", "pass");
		} else {
			writeTestResults("Verify that user entred product code/ description on the search area is displayed",
					"The user entred product code/ description on the search area should be displayed",
					"The user entred product code/ description on the search area doesn't displayed", "fail");
		}

		if (click(btn_searchProductProductionModel)) {
			writeTestResults("Verify that search button is clicked successfully",
					"The search button should be clicked successfully", "The search button clicked successfully",
					"pass");
		} else {
			writeTestResults("Verify that search button is clicked successfully",
					"The search button should be clicked successfully", "The search button doesn't clicked", "fail");
		}

		Thread.sleep(2000);
		explicitWait(tr_reultOnLookupInfoReplace.replace("info",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")), 25);
		doubleClick(tr_reultOnLookupInfoReplace.replace("info",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")));
		explicitWaitUntillClickable(btn_yesClearConfirmation, 5);
		click(btn_yesClearConfirmation);
		Thread.sleep(2000);

		String selectedProduct = getAttribute(txt_productOnFrontPagePM, "value");
		if (selectedProduct.contains(invObj.readTestCreation("BatchFifoMultiPlatformProductionType"))) {
			writeTestResults("Verify that user selected product is diplayed",
					"The user selected product should be diplayed", "The user selected product successfully diplayed",
					"pass");
		} else {
			writeTestResults("Verify that user selected product is diplayed",
					"The user selected product should be diplayed", "The user selected product doesn't diplayed",
					"fail");
		}

		click(btn_searchProductionUnit);
		explicitWait(header_productionUnitLookupPM, 15);
		if (isDisplayed(header_productionUnitLookupPM)) {
			writeTestResults("Verify that production unit look up screen was loaded",
					"The production unit look up screen should be loaded",
					"The production unit look up successfully loaded", "pass");
		} else {
			writeTestResults("Verify that production unit look up screen was loaded",
					"The production unit look up screen should be loaded", "The production unit look up doesn't loaded",
					"fail");
		}

		sendKeysLookup(txt_searchProductionUnit, readProductionData("Location_PROD_E2E"));
		String enterdProductionUnitOnSerachBox = getAttribute(txt_searchProductionUnit, "value");
		if (enterdProductionUnitOnSerachBox.equals(readProductionData("Location_PROD_E2E"))) {
			writeTestResults(
					"Verify that the user entred production unit code/ description on the search area is displayed",
					"The user entred production unit code/ description on the search area should be displayed",
					"The user entred production unit code/ description on the search area successfully displayed",
					"pass");
		} else {
			writeTestResults(
					"Verify that the user entred production unit code/ description on the search area is displayed",
					"The user entred production unit code/ description on the search area should be displayed",
					"The user entred production unit code/ description on the search area doesn't displayed", "fail");
		}

		if (click(btn_searchProductionUnitOnLookup)) {
			writeTestResults("Verify that the the search button was clicked",
					"The search button should be clicked successfully", "The search button clicked successfully",
					"pass");
		} else {
			writeTestResults("Verify that the the search button was clicked",
					"The search button should be clicked successfully", "The search button doesn't click", "fail");
		}

		Thread.sleep(2000);
		explicitWaitUntillClickable(lnk_resultProductionUnitOnLookupUnitReplace.replace("production_unit",
				readProductionData("Location_PROD_E2E")), 20);
		if (isDisplayed(lnk_resultProductionUnitOnLookupUnitReplace.replace("production_unit",
				readProductionData("Location_PROD_E2E")))) {
			writeTestResults("Verify that search result relevent to enterd code is dispaly",
					"Search result relevent to enterd code should be dispaly",
					"Search result relevent to enterd code successfully dispaly", "pass");
		} else {
			writeTestResults("Verify that search result relevent to enterd code is dispaly",
					"Search result relevent to enterd code should be dispaly",
					"Search result relevent to enterd code doesn't dispaly", "fail");
		}

		doubleClick(lnk_resultProductionUnitOnLookupUnitReplace.replace("production_unit",
				readProductionData("Location_PROD_E2E")));
		Thread.sleep(2500);

		String selectedProductionUnit = getAttribute(txt_productionUnitOnFrontPage, "value");
		if (selectedProductionUnit.contains(readProductionData("Location_PROD_E2E"))) {
			writeTestResults("Verify that the user selected 'Production Unit' is diplayed",
					"The user selected 'Production Unit' should be diplayed",
					"The user selected 'Production Unit' successfully diplayed", "pass");
		} else {
			writeTestResults("Verify that the user selected 'Production Unit' is diplayed",
					"The user selected 'Production Unit' should be diplayed",
					"The user selected 'Production Unit' doesn't diplayed", "fail");
		}

		String autoSelectedInputWare = getSelectedOptionInDropdown(drop_inputWarehousePM);
		if (autoSelectedInputWare.contains(inputWarehouseLI)) {
			writeTestResults(
					"Verify that the system was auto selected 'Input Warehouse' when user select the 'Production Unit'",
					"The system should be auto selected 'Input Warehouse' when user select the 'Production Unit'",
					"The system successfully auto selected 'Input Warehouse' when user select the 'Production Unit'",
					"pass");
		} else {
			writeTestResults(
					"Verify that the system was auto selected 'Input Warehouse' when user select the 'Production Unit'",
					"The system should be auto selected 'Input Warehouse' when user select the 'Production Unit'",
					"The system doesn't auto selected 'Input Warehouse' when user select the 'Production Unit'",
					"fail");
		}

		String autoSelectedOutputWare = getSelectedOptionInDropdown(drop_outputWarehousePM);
		if (autoSelectedOutputWare.contains(outputWarehouseLI)) {
			writeTestResults(
					"Verify that the system was auto selected 'Output Warehouse' when user select the 'Production Unit'",
					"The system should be auto selected 'Output Warehouse' when user select the 'Production Unit'",
					"The system successfully auto selected 'Output Warehouse' when user select the 'Production Unit'",
					"pass");
		} else {
			writeTestResults(
					"Verify that the system was auto selected 'Output Warehouse' when user select the 'Production Unit'",
					"The system should be auto selected 'Output Warehouse' when user select the 'Production Unit'",
					"The system doesn't auto selected 'Output Warehouse' when user select the 'Production Unit'",
					"fail");
		}

		String autoSelectedWipWare = getSelectedOptionInDropdown(drop_wipWarehousePM);
		if (autoSelectedWipWare.contains(wipWarehouseLI)) {
			writeTestResults(
					"Verify that the system was auto selected 'WIP Warehouse' when user select the 'Production Unit'",
					"The system should be auto selected 'WIP Warehouse' when user select the 'Production Unit'",
					"The system successfully auto selected 'WIP Warehouse' when user select the 'Production Unit'",
					"pass");
		} else {
			writeTestResults(
					"Verify that the system was auto selected 'WIP Warehouse' when user select the 'Production Unit'",
					"The system should be auto selected 'WIP Warehouse' when user select the 'Production Unit'",
					"The system doesn't auto selected 'WIP Warehouse' when user select the 'Production Unit'", "fail");
		}

		sendKeys(txt_batchQuantityOtherInformationPM, batchQuantityE2E);

		selectText(drop_barcodeBookOtherInformationPM, barcodeBookPM);
		String selectedBarcodeBook = getSelectedOptionInDropdown(drop_barcodeBookOtherInformationPM);
		if (selectedBarcodeBook.contains(barcodeBookPM)) {
			writeTestResults("Verify user able to select Barcode Book", "User should be able to select Barcode Book",
					"User successfully select Barcode Book", "pass");
		} else {
			writeTestResults("Verify user able to select Barcode Book", "User should be able to select Barcode Book",
					"User couldn't select Barcode Book", "fail");
		}

		selectText(drop_costingPriorityOtherInformationPM, costicPriorityNormalPM);
		String selectedCostingPriority = getSelectedOptionInDropdown(drop_costingPriorityOtherInformationPM);
		if (selectedCostingPriority.contains(costicPriorityNormalPM)) {
			writeTestResults("Verify user able to select Costing Priority",
					"User should be able to select Costing Priority", "User successfully select Costing Priority",
					"pass");
		} else {
			writeTestResults("Verify user able to select Costing Priority",
					"User should be able to select Costing Priority", "User couldn't select Costing Priority", "fail");
		}

		sendKeys(txt_statndardCostOtherInformationPM, standardCostPM);
		String enteredStandardCost = getAttribute(txt_statndardCostOtherInformationPM, "value");
		if (enteredStandardCost.equals(standardCostPM)) {
			writeTestResults("Verify that user able to enter Standard Cost",
					"User should be able to enter Standard Cost", "User successfully enter Standard Cost", "pass");
		} else {
			writeTestResults("Verify that user able to enter Standard Cost",
					"User should be able to enter Standard Cost", "User couldn't enter Standard Cost", "fail");
		}

		click(btn_lookupPricingProfilePM);
		explicitWait(header_pricingProfileLookupPM, 15);
		if (isDisplayed(header_pricingProfileLookupPM)) {
			writeTestResults("Verify that pricing profile look up screen was loaded",
					"The pricing profile look up screen should be loaded",
					"The pricing profile look up successfully loaded", "pass");
		} else {
			writeTestResults("Verify that pricing profile look up screen was loaded",
					"The pricing profile look up screen should be loaded", "The pricing profile look up doesn't loaded",
					"fail");
		}

		sendKeysLookup(txt_searchPricingProfile, readProductionData("Pricing_Profile_PROD_E2E"));
		String enterdPricingProfileOnSerachBox = getAttribute(txt_searchPricingProfile, "value");
		if (enterdPricingProfileOnSerachBox.equals(readProductionData("Pricing_Profile_PROD_E2E"))) {
			writeTestResults(
					"Verify that the user entred pricing profile code/ description on the search area is displayed",
					"The user entred pricing profile code/ description on the search area should be displayed",
					"The user entred pricing profile code/ description on the search area successfully displayed",
					"pass");
		} else {
			writeTestResults(
					"Verify that the user entred pricing profile code/ description on the search area is displayed",
					"The user entred pricing profile code/ description on the search area should be displayed",
					"The user entred pricing profile code/ description on the search area doesn't displayed", "fail");
		}

		if (click(btn_searchPricingProfilePM)) {
			writeTestResults("Verify that the the search button was clicked",
					"The search button should be clicked successfully", "The search button clicked successfully",
					"pass");
		} else {
			writeTestResults("Verify that the the search button was clicked",
					"The search button should be clicked successfully", "The search button doesn't click", "fail");
		}

		Thread.sleep(2000);
		explicitWaitUntillClickable(td_resultPricingProfileReplacePP.replace("pricing_profile",
				readProductionData("Pricing_Profile_PROD_E2E")), 20);
		if (isDisplayed(td_resultPricingProfileReplacePP.replace("pricing_profile",
				readProductionData("Pricing_Profile_PROD_E2E")))) {
			writeTestResults("Verify that search result relevent to enterd code is dispaly",
					"Search result relevent to enterd code should be dispaly",
					"Search result relevent to enterd code successfully dispaly", "pass");
		} else {
			writeTestResults("Verify that search result relevent to enterd code is dispaly",
					"Search result relevent to enterd code should be dispaly",
					"Search result relevent to enterd code doesn't dispaly", "fail");
		}

		doubleClick(td_resultPricingProfileReplacePP.replace("pricing_profile",
				readProductionData("Pricing_Profile_PROD_E2E")));
		Thread.sleep(2500);

		String selectedPricingProfile = getAttribute(txt_pricingProfileOtherInformationPM, "value");
		if (selectedPricingProfile.contains(readProductionData("Pricing_Profile_PROD_E2E"))) {
			writeTestResults("Verify that the user selected 'Pricing Profile' is diplayed",
					"The user selected 'Pricing Profile' should be diplayed",
					"The user selected 'Pricing Profile' successfully diplayed", "pass");
		} else {
			writeTestResults("Verify that the user selected 'Pricing Profile' is diplayed",
					"The user selected 'Pricing Profile' should be diplayed",
					"The user selected 'Pricing Profile' doesn't diplayed", "fail");
		}

		click(chk_mrpProductionModel);
		if (isSelected(chk_mrpProductionModel)) {
			writeTestResults("Verify that user able to enable MRP", "User should be able to enable MRP",
					"User successfully enable MRP", "pass");
		} else {
			writeTestResults("Verify that user able to enable MRP", "User should be able to enable MRP",
					"User couldn't enable MRP", "fail");
		}

		pageScrollUpToBottom();
		try {
			String bom = readProductionData("BOM_PROD_E2E_001");
			selectTextByContains(drop_bomPM, bom);
			explicitWaitUntillClickable(btn_yesClearConfirmation, 5);
			click(btn_yesClearConfirmation);
			Thread.sleep(2000);
			String selectedBOM = getSelectedOptionInDropdown(drop_bomPM);
			if (selectedBOM.contains(readProductionData("BOM_PROD_E2E_001"))) {
				writeTestResults("Verify user able to select Bill of Material",
						"User should be able to select Bill of Material", "User successfully select Bill of Material",
						"pass");
			} else {
				writeTestResults("Verify user able to select Bill of Material",
						"User should be able to select Bill of Material", "User couldn't select Bill of Material",
						"fail");
			}
		} catch (Exception e) {
			writeTestResults("Verify user able to select Bill of Material",
					"User should be able to select Bill of Material", "User couldn't select Bill of Material", "fail");
		}

	}

	public void productionModel3_PROD_E2E_001() throws Exception {
		click(btn_lookupBOOPM);
		explicitWait(header_billOfOperatioPM, 15);
		if (isDisplayed(header_billOfOperatioPM)) {
			writeTestResults("Verify that Bill of Operation look up screen was loaded",
					"The Bill of Operation look up screen should be loaded",
					"The Bill of Operation look up successfully loaded", "pass");
		} else {
			writeTestResults("Verify that Bill of Operation look up screen was loaded",
					"The Bill of Operation look up screen should be loaded",
					"The Bill of Operation look up doesn't loaded", "fail");
		}

		sendKeysLookup(txt_searchBillOfOperationPM, readProductionData("BOO_PROD_E2E_001"));
		String enterdBOO = getAttribute(txt_searchBillOfOperationPM, "value");
		if (enterdBOO.equals(readProductionData("BOO_PROD_E2E_001"))) {
			writeTestResults(
					"Verify that the user entred bill of operation code/ description on the search area is displayed",
					"The user entred bill of operation code/ description on the search area should be displayed",
					"The user entred bill of operation code/ description on the search area successfully displayed",
					"pass");
		} else {
			writeTestResults(
					"Verify that the user entred bill of operation code/ description on the search area is displayed",
					"The user entred bill of operation code/ description on the search area should be displayed",
					"The user entred bill of operation code/ description on the search area doesn't displayed", "fail");
		}

		if (click(btn_searchBOOInLookup)) {
			writeTestResults("Verify that the the search button was clicked",
					"The search button should be clicked successfully", "The search button clicked successfully",
					"pass");
		} else {
			writeTestResults("Verify that the the search button was clicked",
					"The search button should be clicked successfully", "The search button doesn't click", "fail");
		}

		Thread.sleep(2000);
		explicitWaitUntillClickable(td_resultBOOPMBooReplace.replace("boo", readProductionData("BOO_PROD_E2E_001")),
				20);
		if (isDisplayed(td_resultBOOPMBooReplace.replace("boo", readProductionData("BOO_PROD_E2E_001")))) {

			writeTestResults("Verify that search result relevent to enterd code is dispaly",
					"Search result relevent to enterd code should be dispaly",
					"Search result relevent to enterd code successfully dispaly", "pass");
		} else {
			writeTestResults("Verify that search result relevent to enterd code is dispaly",
					"Search result relevent to enterd code should be dispaly",
					"Search result relevent to enterd code doesn't dispaly", "fail");
		}

		doubleClick(td_resultBOOPMBooReplace.replace("boo", readProductionData("BOO_PROD_E2E_001")));
		Thread.sleep(2500);

		click(btn_okClearConfirmationBOOPM);

		Thread.sleep(2000);
		String selectedBOO = getAttribute(txt_billOfOperationPMFrontPage, "value");
		if (selectedBOO.contains(readProductionData("BOO_PROD_E2E_001"))) {
			writeTestResults("Verify that the user selected 'Bill of Operation' is diplayed",
					"The user selected 'Bill of Operation' should be diplayed",
					"The user selected 'Bill of Operation' successfully diplayed", "pass");
		} else {
			writeTestResults("Verify that the user selected 'Bill of Operation' is diplayed",
					"The user selected 'Bill of Operation' should be diplayed",
					"The user selected 'Bill of Operation' doesn't diplayed", "fail");
		}

		click(tab_billOfOperationProductionModel);
		if (isDisplayed(tab_billOfOperationAfterSelected)) {
			writeTestResults("Verify that the user is on the bill of operations tab",
					"The user should be on the bill of operations tab",
					"The user successfully on the bill of operations tab", "pass");
		} else {
			writeTestResults("Verify that the user is on the bill of operations tab",
					"The user should be on the bill of operations tab", "The user is not on the bill of operations tab",
					"fail");
		}

		click(btn_expandPM);
		Thread.sleep(1500);
		if (isDisplayedQuickCheck(tr_booRow1PM) && isDisplayedQuickCheck(tr_booRow2PM)
				&& isDisplayedQuickCheck(tr_booRow3PM) && isDisplayedQuickCheck(tr_booRow4PM)) {
			writeTestResults("Verify that system is displayed the bill of operations details",
					"The system should be displayed the bill of operations details",
					"The system successfully displayed the bill of operations details", "pass");
		} else {
			writeTestResults("Verify that system is displayed the bill of operations details",
					"The system should be displayed the bill of operations details",
					"The system doesn't displayed the bill of operations details", "fail");

		}

		click(btn_editBillOfOperationPMRowReplace.replace("row", "1"));
		explicitWait(header_operationActivityPopup, 20);
		if (isDisplayedQuickCheck(header_operationActivityPopup)) {
			writeTestResults("Verify that Operations/Activity screen was loaded",
					"Operations/Activity screen should be loaded", "Operations/Activity screen successfully loaded",
					"pass");
		} else {
			writeTestResults("Verify that Operations/Activity screen was loaded",
					"Operations/Activity screen should be loaded", "Operations/Activity screen wasn't loaded", "fail");
		}

		click(btn_inputProductsTabBOOPM);
		explicitWait(li_inputProductSelected, 10);
		if (isDisplayedQuickCheck(li_inputProductSelected)) {
			writeTestResults("Verify that user can moved to the Input Products tab",
					"User should be moved to the Input Products tab",
					"User successfully moved to the Input Products tab", "pass");
		} else {
			writeTestResults("Verify that user can moved to the Input Products tab",
					"User should be moved to the Input Products tab", "User couldn't moved to the Input Products tab",
					"fail");
		}

		click(chk_inputProductsRowReplaceBOOPM.replace("row", "1"));
		click(chk_inputProductsRowReplaceBOOPM.replace("row", "2"));
		click(chk_inputProductsRowReplaceBOOPM.replace("row", "3"));

		if (isSelected(chk_inputProductsRowReplaceBOOPM.replace("row", "1"))
				&& isSelected(chk_inputProductsRowReplaceBOOPM.replace("row", "2"))
				&& isSelected(chk_inputProductsRowReplaceBOOPM.replace("row", "3"))) {
			writeTestResults("Verify that user able to enable both input products for initial operation",
					"User should be able to enable both input products for initial operation",
					"User successfully enable both input products for initial operation", "pass");
		} else {
			writeTestResults("Verify that user able to enable both input products for initial operation",
					"User should be able to enable both input products for initial operation",
					"User doesn't enable both input products for initial operation", "fail");
		}

		click(btn_applyBOOPM);

		explicitWait(btn_editBillOfOperationPMRowReplace.replace("row", "2"), 10);
		click(btn_editBillOfOperationPMRowReplace.replace("row", "2"));

		selectText(drop_processTimeTypeBOOPM, processTimeTypePM);

		click(btn_inputProductsTabBOOPM);
		explicitWait(li_inputProductSelected, 10);
		if (isDisplayedQuickCheck(li_inputProductSelected)) {
			writeTestResults("Verify that user can moved to the Input Products tab",
					"User should be moved to the Input Products tab",
					"User successfully moved to the Input Products tab", "pass");
		} else {
			writeTestResults("Verify that user can moved to the Input Products tab",
					"User should be moved to the Input Products tab", "User couldn't moved to the Input Products tab",
					"fail");
		}

		click(chk_inputProductsRowReplaceBOOPM.replace("row", "1"));
		click(chk_inputProductsRowReplaceBOOPM.replace("row", "2"));
		if (isSelected(chk_inputProductsRowReplaceBOOPM.replace("row", "1"))
				&& isSelected(chk_inputProductsRowReplaceBOOPM.replace("row", "2"))) {
			writeTestResults("Verify that user able to enable a product for the first operation",
					"User should be able to enable a product for the first operation",
					"User successfully enable a product for the first operation", "pass");
		} else {
			writeTestResults("Verify that user able to enable a product for the first operation",
					"User should be able to enable a product for the first operation",
					"User doesn't enable a product for the first operation", "fail");
		}

		click(btn_applyBOOPM);

		explicitWait(btn_editBillOfOperationPMRowReplace.replace("row", "3"), 10);
		click(btn_editBillOfOperationPMRowReplace.replace("row", "3"));

		selectText(drop_processTimeTypeBOOPM, processTimeTypePM);

		click(btn_inputProductsTabBOOPM);
		click(chk_inputProductsRowReplaceBOOPM.replace("row", "1"));
		if (isSelected(chk_inputProductsRowReplaceBOOPM.replace("row", "1"))) {
			writeTestResults("Verify that user able to enable a product for the second operation",
					"User should be able to enable a product for the second operation",
					"User successfully enable a product for the second operation", "pass");
		} else {
			writeTestResults("Verify that user able to enable a product for the second operation",
					"User should be able to enable a product for the second operation",
					"User doesn't enable a product for the second operation", "fail");
		}

		click(btn_applyBOOPM);

		explicitWaitUntillClickable(btn_draft, 15);
		click(btn_draft);
		explicitWait(header_draftedProductionModel, 40);
		if (isDisplayedQuickCheck(header_draftedProductionModel)) {
			writeTestResults("Verify that the system was drafted the newly created Production Model",
					"The system should be drafted the newly created Production Model",
					"The system successfully drafted the newly created Production Model", "pass");
		} else {
			writeTestResults("Verify that the system was drafted the newly created Production Model",
					"The system should be drafted the newly created Production Model",
					"The system wasn't drafted the newly created Production Model", "fail");

		}

		explicitWait(header_draftedProductionModel, 40);
		Thread.sleep(1000);
		click(btn_edit);
		explicitWaitUntillClickable(btn_update, 30);
		if (isDisplayedQuickCheck(btn_update)) {
			writeTestResults("Verify that the system is displayed update button in the summary tab",
					"The system should be displayed update button in the summary tab",
					"The system successfully displayed update button in the summary tab", "pass");
		} else {
			writeTestResults("Verify that the system is displayed update button in the summary tab",
					"The system should be displayed update button in the summary tab",
					"The system doesn't displayed update button in the summary tab", "fail");
		}

		if (isDisplayedQuickCheck(tab_summaryProductionModel)
				&& isDisplayedQuickCheck(tab_billOfOperationProductionModel)) {
			writeTestResults("The system doesn't displayed the tabs mentioned here (Summary, Bill of operations)",
					"The system should be displayed the tabs mentioned here (Summary, Bill of operations)",
					"The system successfully displayed the tabs mentioned here (Summary, Bill of operations)", "pass");
		} else {
			writeTestResults("The system doesn't displayed the tabs mentioned here (Summary, Bill of operations)",
					"The system should be displayed the tabs mentioned here (Summary, Bill of operations)",
					"The system doesn't displayed the tabs mentioned here (Summary, Bill of operations)", "fail");
		}

		click(tab_billOfOperationProductionModel);
		explicitWaitUntillClickable(btn_expandPM, 10);
		click(btn_expandPM);
		explicitWaitUntillClickable(btn_editOperation01PM, 10);
	}

	public void productionModel4_PROD_E2E_001() throws Exception {
		click(btn_editOperation01PM);
		explicitWait(header_operationActivityPopup, 20);
		if (isDisplayedQuickCheck(header_operationActivityPopup)) {
			writeTestResults("Verify that Operations/Activity screen was loaded",
					"Operations/Activity screen should be loaded", "Operations/Activity screen successfully loaded",
					"pass");
		} else {
			writeTestResults("Verify that Operations/Activity screen was loaded",
					"Operations/Activity screen should be loaded", "Operations/Activity screen wasn't loaded", "fail");
		}

		click(btn_additionalResourseTabPM);
		explicitWait(btn_additionalResourseTabSelectedPM, 20);
		if (isDisplayedQuickCheck(btn_additionalResourseTabSelectedPM)) {
			writeTestResults("Verify that user can moved to the additional resourse tab",
					"User should be moved to the additional resourse tab",
					"User successfully moved to the additional resourse tab", "pass");
		} else {
			writeTestResults("Verify that user can moved to the additional resourse tab",
					"User should be moved to the additional resourse tab",
					"User couldn't moved to the additional resourse tab", "fail");
		}

		click(btn_resourseLookupAdditionalResourseTabPM);
		explicitWait(lookup_resource, 20);
		if (isDisplayedQuickCheck(lookup_resource)) {
			writeTestResults("Verify that resource look up was loaded", "Resource look up should be loaded",
					"Resource look up successfully loaded", "pass");
		} else {
			writeTestResults("Verify that resource look up was loaded", "Resource look up should be loaded",
					"Resource look up doesn't loaded", "fail");
		}

		sendKeysLookup(txt_searchResource, readProductionData("ResourceCode02_E2E"));
		click(btn_resourceSearchInLookup);
		Thread.sleep(2000);
		explicitWait(td_resultResourceAdditionalResourceTabPMResourseReplace.replace("resource",
				readProductionData("ResourceCode02_E2E")), 7);
		doubleClick(td_resultResourceAdditionalResourceTabPMResourseReplace.replace("resource",
				readProductionData("ResourceCode02_E2E")));
		explicitWait(div_resourceOnAdditonalResourceGridPMResorceReplace.replace("resource",
				readProductionData("ResourceCode02_E2E")), 30);
		if (isDisplayedQuickCheck(div_resourceOnAdditonalResourceGridPMResorceReplace.replace("resource",
				readProductionData("ResourceCode02_E2E")))) {
			writeTestResults("Verify that selected resource is display in additional resource tab",
					"Selected resource should be display in additional resource tab",
					"Selected resource successfully display in additional resource tab", "pass");
		} else {
			writeTestResults("Verify that selected resource is display in additional resource tab",
					"Selected resource should be display in additional resource tab",
					"Selected resource successfully display in additional resource tab", "pass");
		}

		if (isDisplayedQuickCheck(header_resourceHoursAdditionalResourseTabPM)) {
			writeTestResults(
					"Verify that resource duration column placed without change in position in the additional resource tab",
					"Resource duration column should be placed without change in position in the additional resource tab",
					"Resource duration column placed without change in position in the additional resource tab",
					"pass");
		} else {
			writeTestResults(
					"Verify that resource duration column placed without change in position in the additional resource tab",
					"Resource duration column should be placed without change in position in the additional resource tab",
					"Resource duration column not placed without change in position in the additional resource tab",
					"fail");
		}

		click(btn_selectHoursAddtionalResourceTabPM);
		Thread.sleep(1500);
		click(btn_oneHourAdditionalResourceHoursPM);
		Thread.sleep(1500);
		click(btn_okDurationAdditionalResourceHoursPM);
		Thread.sleep(2000);

		String AdditionalResourseHours = getAttribute(btn_selectHoursAddtionalResourceTabPM, "value");
		String AdditionalResourseMins = getAttribute(btn_selectMinutesAddtionalResourceTabPM, "value");

		if (AdditionalResourseHours.equals("01") && AdditionalResourseMins.equals("00")) {
			writeTestResults("Verify that resource duration entered in the additional resource tab",
					"Resource duration should be entered in the additional resource tab",
					"Resource duration successfully entered in the additional resource tab", "pass");
		} else {
			writeTestResults("Verify that resource duration entered in the additional resource tab",
					"Resource duration should be entered in the additional resource tab",
					"Resource duration doesn't entered in the additional resource tab", "fail");
		}

		click(btn_applyOperationActivity);
		waitUntillNotVisible(header_operationActivityPopup, 15);
		if (!isDisplayedQuickCheck(header_operationActivityPopup)) {
			writeTestResults("Verify that additional resource was added to the operations successfully",
					"The additional resource should be added to the operations successfully",
					"The additional resource successfully added to the operations successfully", "pass");
		} else {
			writeTestResults("Verify that additional resource was added to the operations successfully",
					"The additional resource should be added to the operations successfully",
					"The additional resource was not added to the operations", "fail");
		}

		click(btn_update);
		explicitWaitUntillClickable(btn_edit, 30);
		if (isEnabled(btn_edit)) {
			writeTestResults("Verify user able to update the Production Model",
					"User should be able to update the Production Model",
					"User uccessfully update the Production Model", "pass");
		} else {
			writeTestResults("Verify user able to update the Production Model",
					"User should be able to update the Production Model", "User couldn't update the Production Model",
					"fail");
		}

		click(btn_release);

		explicitWait(header_releasedProductionModel, 60);
		trackCode = getText(div_productionModelCode);
		String productionModelCode = trackCode;
		writeProductionData("ProductionModel_PROD_E2E_001", productionModelCode, 20);
		if (isDisplayed(header_releasedProductionModel)) {
			writeTestResults("Verify user able to release the Production Model",
					"User should be able to release the Production Model",
					"User uccessfully release the Production Model", "pass");
		} else {
			writeTestResults("Verify user able to release the Production Model",
					"User should be able to release the Production Model", "User couldn't release the Production Model",
					"fail");
		}

	}

	/* Production Order */
	public void productionOrder_1() throws Exception {
		explicitWaitUntillClickable(img_navigationPane, 60);
		Thread.sleep(2000);
		explicitWaitUntillClickable(img_navigationPane, 20);
		if (click(img_navigationPane)) {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User successfully click navigation pane", "pass");
		} else {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User couldn't click navigation pane", "fail");
		}

		explicitWait(btn_production, 4);
		if (isEnabledQuickCheck(btn_production)) {
			writeTestResults("Verify user able to view the \"Production\" module under main navigation menu",
					"User should be able to view the \"Production\" module under main navigation menu",
					"User successfully view the \"Production\" module under main navigation menu", "pass");
		} else {
			writeTestResults("Verify user able to view the \"Production\" module under main navigation menu",
					"User should be able to view the \"Production\" module under main navigation menu",
					"User couldn't view the \"Production\" module under main navigation menu", "fail");
		}

		click(btn_production);

		explicitWait(btn_productionOrder, 4);
		if (isEnabledQuickCheck(btn_productionOrder)) {
			writeTestResults("Verify user able to view the \"Production Order\" option under production sub menu",
					"User should be able to view the \"Production Order\" option under production sub menu",
					"User able to view the \"Production Order\" option under production sub menu", "pass");
		} else {
			writeTestResults("Verify user able to view the \"Production Order\" option under production sub menu",
					"User should be able to view the \"Production Order\" option under production sub menu",
					"User couldn't view the \"Production Order\" option under production sub menu", "pass");
		}

		click(btn_productionOrder);

		explicitWait(header_productionOrderByPage, 60);
		if (isDisplayed(header_productionOrderByPage)) {
			writeTestResults("Verify user able to navigate to Production Order by-page",
					"User should be able to navigate to Production Order by-page",
					"User successfully navigate to Production Order by-page", "pass");
		} else {
			writeTestResults("Verify user able to navigate to Production Order by-page",
					"User should be able to navigate to Production Order by-page",
					"User coulsn't navigate to Production Order by-page", "fail");
		}

		if (isDisplayed(header_productionOrderDocNoColumnOnByPage)) {
			writeTestResults("Verify that 'Doc No' column is displayed as earlier",
					"'Doc No' column should be displayed as earlier",
					"'Doc No' column successfully displayed as earlier", "pass");
		} else {
			writeTestResults("Verify that 'Doc No' column is displayed as earlier",
					"'Doc No' column should be displayed as earlier", "'Doc No' column doesn't displayed as earlier",
					"fail");
		}

		click(btn_newProductionOrder);
		explicitWait(header_newProductionOrder, 60);
		if (isDisplayed(header_newProductionOrder)) {
			writeTestResults("Verify user able to navigate to Production Order new-form",
					"User should be able to navigate to Production Order new-form",
					"User successfully navigate to Production Order new-form", "pass");
		} else {
			writeTestResults("Verify user able to navigate to Production Order new-form",
					"User should be able to navigate to Production Order new-form",
					"User coulsn't navigate to Production Order new-form", "fail");
		}

		click(span_productLookupPO);
		explicitWait(lookup_product, 30);
		if (isDisplayed(lookup_product)) {
			writeTestResults("Verify that product lookup is open", "Product lookup should be open",
					"Product lookup successfully open", "pass");
		} else {
			writeTestResults("Verify that product lookup is open", "Product lookup should be open",
					"Product lookup doesn't open", "fail");
		}

		sendKeys(txt_searchProductProductLookup, invObj.readTestCreation("BatchFifoMultiPlatformProductionType"));
		String enteredProduct = getAttribute(txt_searchProductProductLookup, "value");
		if (enteredProduct.equals(invObj.readTestCreation("BatchFifoMultiPlatformProductionType"))) {
			writeTestResults("Verify that user able to enter the relevent product",
					"User should be able to enter the relevent product", "User successfully enter the relevent product",
					"pass");
		} else {
			writeTestResults("Verify that user able to enter the relevent product",
					"User should be able to enter the relevent product", "User doesn't enter the relevent product",
					"fail");
		}

		click(btn_searchProductProductLookup);
		explicitWaitUntillClickable(td_resultProductRegressionPOProductReplace.replace("product_code",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")), 60);
		if (isEnabled(td_resultProductRegressionPOProductReplace.replace("product_code",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")))) {
			writeTestResults("Verify that relevent product is available", "Relevent product should be available",
					"Relevent product successfully available", "pass");
		} else {
			writeTestResults("Verify that relevent product is available", "Relevent product should be available",
					"Relevent product not available", "fail");
		}

		doubleClick(td_resultProductRegressionPOProductReplace.replace("product_code",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")));
		Thread.sleep(2000);

		String selectedProduct = getAttribute(txt_outputProductProductInformationSectionSummaryTabPO, "value");
		if (selectedProduct.contains(invObj.readTestCreation("BatchFifoMultiPlatformProductionType"))) {
			writeTestResults("Verify that user able to select the relevent product",
					"User should be able to select the relevent product",
					"User successfully select the relevent product", "pass");
		} else {
			writeTestResults("Verify that user able to select the relevent product",
					"User should be able to select the relevent product", "User couldn't select the relevent product",
					"fail");
		}
	}

	public void productionOrder_2() throws Exception {
		click(btn_productionModelLookupPO);
		explicitWait(lookup_productionModel, 30);
		if (isDisplayed(lookup_productionModel)) {
			writeTestResults("Verify that Production Model lookup is open", "Production Model lookup should be open",
					"Production Model lookup successfully open", "pass");
		} else {
			writeTestResults("Verify that Production Model lookup is open", "Production Model lookup should be open",
					"Production Model lookup doesn't open", "fail");
		}

		sendKeys(txt_searchProductionModel, readProductionData("ProductionModel_PROD_E2E_001"));
		String enteredProductionModel = getAttribute(txt_searchProductionModel, "value");
		if (enteredProductionModel.equals(readProductionData("ProductionModel_PROD_E2E_001"))) {
			writeTestResults("Verify that user able to enter the relevent Production Model",
					"User should be able to enter the relevent Production Model",
					"User successfully enter the relevent Production Model", "pass");
		} else {
			writeTestResults("Verify that user able to enter the relevent Production Model",
					"User should be able to enter the relevent Production Model",
					"User doesn't enter the relevent Production Model", "fail");
		}

		click(btn_searchProductionModel);
		explicitWaitUntillClickable(td_resultProductionModelDocReplace.replace("doc_no",
				readProductionData("ProductionModel_PROD_E2E_001")), 60);
		if (isEnabled(td_resultProductionModelDocReplace.replace("doc_no",
				readProductionData("ProductionModel_PROD_E2E_001")))) {
			writeTestResults("Verify that relevent Production Model is available",
					"Relevent Production Model should be available", "Relevent Production Model successfully available",
					"pass");
		} else {
			writeTestResults("Verify that relevent Production Model is available",
					"Relevent Production Model should be available", "Relevent Production Model not available", "fail");
		}

		doubleClick(td_resultProductionModelDocReplace.replace("doc_no",
				readProductionData("ProductionModel_PROD_E2E_001")));
		Thread.sleep(2000);

		String selectedProducttionModel = getAttribute(txt_productionModelSummarySectionSummaryTabPO, "value");
		if (selectedProducttionModel.equals(readProductionData("ProductionModel_PROD_E2E_001"))) {
			writeTestResults("Verify that user able to select the relevent Production Model",
					"User should be able to select the relevent Production Model",
					"User successfully select the relevent Production Model", "pass");
		} else {
			writeTestResults("Verify that user able to select the relevent Production Model",
					"User should be able to select the relevent Production Model",
					"User couldn't select the relevent Production Model", "fail");
		}

		String loadedProductionUnit = getAttribute(txt_productionUnitSummarySectionSummaryTabPO, "value");
		if (loadedProductionUnit.contains(readProductionData("Location_PROD_E2E"))) {
			writeTestResults(
					"Verify that Production Unit auto loaded according to the Production Model in summary section",
					"Production Unit should be auto loaded according to the Production Model in summary section",
					"Production Unit successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Production Unit auto loaded according to the Production Model in summary section",
					"Production Unit should be auto loaded according to the Production Model in summary section",
					"Production Unit doesn't auto loaded according to the Production Model in summary section", "fail");
		}

		String loadedInputWare = getSelectedOptionInDropdown(drop_inputWarehouseSummarySectionSummaryTabPO);
		if (loadedInputWare.equals(inputWarehouseLI)) {
			writeTestResults(
					"Verify that Input Warehouse auto loaded according to the Production Model in summary section",
					"Input Warehouse should be auto loaded according to the Production Model in summary section",
					"Input Warehouse successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Input Warehouse auto loaded according to the Production Model in summary section",
					"Input Warehouse should be auto loaded according to the Production Model in summary section",
					"Input Warehouse doesn't auto loaded according to the Production Model in summary section", "fail");
		}

		String loadedOutputWare = getSelectedOptionInDropdown(drop_outputWarehouseSummarySectionSummaryTabPO);
		if (loadedOutputWare.equals(outputWarehouseLI)) {
			writeTestResults(
					"Verify that Output Warehouse auto loaded according to the Production Model in summary section",
					"Output Warehouse should be auto loaded according to the Production Model in summary section",
					"Output Warehouse successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Output Warehouse auto loaded according to the Production Model in summary section",
					"Output Warehouse should be auto loaded according to the Production Model in summary section",
					"Output Warehouse doesn't auto loaded according to the Production Model in summary section",
					"fail");
		}

		String loadedWIPWare = getSelectedOptionInDropdown(drop_WIPWarehouseSummarySectionSummaryTabPO);
		if (loadedWIPWare.equals(wipWarehouseLI)) {
			writeTestResults(
					"Verify that WIP Warehouse auto loaded according to the Production Model in summary section",
					"WIP Warehouse should be auto loaded according to the Production Model in summary section",
					"WIP Warehouse successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that WIP Warehouse auto loaded according to the Production Model in summary section",
					"WIP Warehouse should be auto loaded according to the Production Model in summary section",
					"WIP Warehouse doesn't auto loaded according to the Production Model in summary section", "fail");
		}

		String loadedBatchQty = getAttribute(txt_batchQtyProductInformationSectionSummaryTabPO, "value");
		if (loadedBatchQty.equals("100.00")) {
			writeTestResults(
					"Verify that Batch Qty auto loaded according to the Production Model in product information section (100 Qty)",
					"Batch Qty should be auto loaded according to the Production Model in product information section (100 Qty)",
					"Batch Qty successfully auto loaded according to the Production Model in product information section (100 Qty)",
					"pass");
		} else {
			writeTestResults(
					"Verify that Batch Qty auto loaded according to the Production Model in product information section (100 Qty)",
					"Batch Qty should be auto loaded according to the Production Model in product information section (100 Qty)",
					"Batch Qty doesn't auto loaded according to the Production Model in product information section (100 Qty)",
					"fail");
		}

		String loadedBoM = getText(drop_boMBoMDetailsSectionSummaryTabPO);
		if (loadedBoM.equals(readProductionData("BOM_PROD_E2E_001"))) {
			writeTestResults(
					"Verify that BoM Document Number auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number should be auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number successfully auto loaded according to the Production Model in bill of material details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoM Document Number auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number should be auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number doesn't auto loaded according to the Production Model in bill of material details section",
					"fail");
		}

		String loadedBoO = getText(div_boOBoODetailsSectionSummaryTabPO);
		if (loadedBoO.equals(readProductionData("BOO_PROD_E2E_001"))) {
			writeTestResults(
					"Verify that BoO Number auto loaded according to the Production Model in bill of operation details section",
					"BoO Number should be auto loaded according to the Production Model in bill of operation details section",
					"BoO Number successfully auto loaded according to the Production Model in bill of operation details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoO Number auto loaded according to the Production Model in bill of operation details section",
					"BoO Number should be auto loaded according to the Production Model in bill of operation details section",
					"BoO Number doesn't auto loaded according to the Production Model in bill of operation details section",
					"fail");
		}

		driver.findElement(getLocator(txt_planedQtyProductInformationSectionSummaryTabPO)).click();
		driver.findElement(getLocator(txt_planedQtyProductInformationSectionSummaryTabPO)).sendKeys(planedQtyE2E);
		String enteredPlanedQty = getAttribute(txt_planedQtyProductInformationSectionSummaryTabPO, "value");
		if (enteredPlanedQty.equals(planedQtyE2E)) {
			writeTestResults("Verify that entered Planed Qty is display successfully",
					"Entered Planed Qty should be display successfully", "Entered Planed Qty display successfully",
					"pass");
		} else {
			writeTestResults("Verify that entered Planed Qty is display successfully",
					"Entered Planed Qty should be display successfully", "Entered Planed Qty not display", "fail");
		}

		if (isSelected(chk_mrpProductionOrderRegression)) {
			writeTestResults("Verify that MRP auto enabled by the Production Model",
					"MRP should  auto enabled by the Production Model",
					"MRP successfully auto enabled by the Production Model", "pass");
		} else {
			writeTestResults("Verify that MRP auto enabled by the Production Model",
					"MRP should  auto enabled by the Production Model",
					"MRP wasn't auto enabled by the Production Model", "fail");
		}

		selectText(drop_productOrderGroupSummarySectionSummaryTabPO, productionOrderGroupRegression);
		Thread.sleep(1000);
		String selectedProductionGroup = getSelectedOptionInDropdown(drop_productOrderGroupSummarySectionSummaryTabPO);
		if (selectedProductionGroup.equals(productionOrderGroupRegression)) {
			writeTestResults("Verify user able to select the Production Group",
					"User should be able to select the Production Group",
					"User successfully select the Production Group", "pass");
		} else {
			writeTestResults("Verify user able to select the Production Group",
					"User should be able to select the Production Group", "User couldn't select the Production Group",
					"fail");
		}

		sendKeys(txt_descriptionSummarySectionSummaryTabPO, productionOrderDescriptionRegression);
		String enteredDescription = getAttribute(txt_descriptionSummarySectionSummaryTabPO, "value");
		if (enteredDescription.equals(productionOrderDescriptionRegression)) {
			writeTestResults("Verify user able to enter the Description",
					"User should be able to enter the Description", "User successfully enter the Description", "pass");
		} else {
			writeTestResults("Verify user able to enter the Description",
					"User should be able to enter the Description", "User couldn't enter the Description", "fail");
		}

		click(txt_requestStartDateSheduleSectionSummaryTabPO);
		explicitWaitUntillClickable(a_requestStartDatePOWhenDuplicating, 6);
		click(a_requestStartDatePOWhenDuplicating);
		Thread.sleep(2000);
		String selectedRequestedStartDate = getAttribute(txt_requestStartDateSheduleSectionSummaryTabPO, "value");
		String currentYearMonthWithselectedDate = common.currentDate().substring(0, 8) + "01";
		if (selectedRequestedStartDate.equals(currentYearMonthWithselectedDate)) {
			writeTestResults("Verify user able to select Requested Start Date",
					"User should be able to select Requested Start Date",
					"User successfully select Requested Start Date", "pass");
		} else {
			writeTestResults("Verify user able to select Requested Start Date",
					"User should be able to select Requested Start Date", "User couldn't select Requested Start Date",
					"fail");
		}

		click(txt_requestEndDateSheduleSectionSummaryTabPO);
		explicitWaitUntillClickable(a_requestEndDatePOWhenDuplicating, 6);
		click(a_requestEndDatePOWhenDuplicating);
		Thread.sleep(2000);
		String selectedRequestedEndDate = getAttribute(txt_requestEndDateSheduleSectionSummaryTabPO, "value");
		String currentYearMonthWithselectedEndDate = common.currentDate().substring(0, 8) + "28";
		if (selectedRequestedEndDate.equals(currentYearMonthWithselectedEndDate)) {
			writeTestResults("Verify user able to select Requested End Date",
					"User should be able to select Requested End Date", "User successfully select Requested End Date",
					"pass");
		} else {
			writeTestResults("Verify user able to select Requested End Date",
					"User should be able to select Requested End Date", "User couldn't select Requested End Date",
					"fail");
		}

		click(btn_draft);
		explicitWait(header_draftedProductionOrder, 60);
		if (isDisplayed(header_draftedProductionOrder)) {
			writeTestResults("Verify user able to draft the Production Order",
					"User should be able to draft the Production Order", "User successfully draft the Production Order",
					"pass");
		} else {
			writeTestResults("Verify user able to draft the Production Order",
					"User should be able to draft the Production Order", "User couldn't draft the Production Order",
					"fail");
		}

		click(btn_release);
		explicitWait(header_releasedProductionOrder, 30);
		trackCode = getText(lbl_docNo);
		String productionOrderCode = trackCode;
		writeProductionData("ProductionOrder_PROD_E2E_001", productionOrderCode, 21);
		writeProductionData("ReleaseedPO_PRD_E2E_001", getCurrentUrl(), 27);
		if (isDisplayed(header_releasedProductionOrder)) {
			writeTestResults("Verify user able to release the Production Order",
					"User should be able to release the Production Order",
					"User successfully release the Production Order", "pass");
		} else {
			writeTestResults("Verify user able to release the Production Order",
					"User should be able to release the Production Order", "User couldn't release the Production Order",
					"fail");
		}
	}

	/* MRP Run */
	public void mrpRun1() throws Exception {
		explicitWait(img_navigationPane, 20);
		Thread.sleep(2000);
		explicitWait(img_navigationPane, 20);
		click(img_navigationPane);
		explicitWait(btn_productionModule, 10);
		click(btn_productionModule);
		explicitWait(btn_productionControl, 10);
		click(btn_productionControl);
		explicitWait(header_productionControlByPage, 40);
		if (isDisplayed(header_productionControlByPage)) {
			writeTestResults("Verify user can navigate to the Production Control by-page",
					"User should be able to navigate to Production Control by-page",
					"User successfully navigate to Production Control by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to the Production Control by-page",
					"User should be able to navigate to Production Control by-page",
					"User doesn't navigate to Production Control by-page", "fail");

		}

		sendKeys(txt_searchProductionControlOnByPage, readProductionData("ProductionOrder_PROD_E2E_001"));
		click(btn_searchProductionControlByPage);

		String chkMRPPath = chk_mrpPODoRepalce.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_001"));
		explicitWait(chkMRPPath, 10);
		if (isDisplayed(chkMRPPath)) {
			writeTestResults("Verify relevent Production Order is available",
					"Relevent Production Order should be available", "Relevent Production Order is available", "pass");
		} else {
			writeTestResults("Verify relevent Production Order is available",
					"Relevent Production Order should be available", "Relevent Production Order doesn't available",
					"fail");

		}
		click(chkMRPPath);

		click(btn_actionMRP);
		explicitWait(btn_runMRP, 10);
		if (isDisplayed(btn_runMRP)) {
			writeTestResults("Verify that Run MRP action is availabe under Actions",
					"Run MRP action should be availabe under Actions", "Run MRP action doesn't availabe under Actions",
					"pass");
		} else {
			writeTestResults("Verify that Run MRP action is availabe under Actions",
					"Run MRP action should be availabe under Actions", "Run MRP action doesn't availabe under Actions",
					"fail");

		}
		click(btn_runMRP);

		explicitWait(header_productionControlWindow, 10);
		if (isDisplayed(header_productionControlWindow)) {
			writeTestResults("Verify that Production Control window is open",
					"Production Control window should be open", "Production Control window suceessfully open", "pass");
		} else {
			writeTestResults("Verify that Production Control window is open",
					"Production Control window should be open", "Production Control window doesn't open", "fail");

		}

		click(btn_runMRPWindow);

		explicitWait(btn_releaseMRP, 10);
		if (isEnabled(btn_releaseMRP)) {
			writeTestResults("Verify that Release button is available", "Release button should be available",
					"Release button successfully available", "pass");
		} else {
			writeTestResults("Verify that Release button is available", "Release button should be available",
					"Release button doesn't available", "fail");

		}

		click(btn_releaseMRP);

		explicitWait(para_MRPReleaseValidation, 10);
		if (isEnabled(para_MRPReleaseValidation)) {
			writeTestResults("Verify that \"Successfully Released.\" validation is available",
					"\"Successfully Released.\" validation should be available",
					"\"Successfully Released.\" validation successfully available", "pass");
		} else {
			writeTestResults("Verify that \"Successfully Released.\" validation is available",
					"\"Successfully Released.\" validation should be available",
					"\"Successfully Released.\" validation doesn't available", "fail");

		}

		Thread.sleep(3000);
		explicitWait(btn_logTabProductionControlWindow, 10);
		click(btn_logTabProductionControlWindow);

		explicitWaitUntillClickable(lbl_genaratedInternalOrderMRP, 10);
		String genaratedIO = getText(lbl_genaratedInternalOrderMRP);

		click(btn_cloaseMRPWindow);

		explicitWait(btn_homeLink, 10);
		click(btn_homeLink);
		explicitWait(btn_taskEvent2, 10);
		click(btn_taskEvent2);
		explicitWait(btn_internalDispatchOrderTaskAndEvent, 10);
		Thread.sleep(1500);
		click(btn_internalDispatchOrderTaskAndEvent);
		String extractedIO = genaratedIO.substring(28, (genaratedIO.length() - 1));
		explicitWaitUntillClickable(btn_pendingIDODocRepalce.replace("doc_no", extractedIO), 10);
		Thread.sleep(2000);
		click(btn_pendingIDODocRepalce.replace("doc_no", extractedIO));
		Thread.sleep(2000);
		switchWindow();
		Thread.sleep(1000);
		explicitWait(header_newIDO, 50);
		if (isDisplayed(header_newIDO)) {
			writeTestResults("Verify user can navigate to the new Internal Dispatch Order",
					"User should be able to navigate to new Internal Dispatch Order",
					"User successfully navigate to new Internal Dispatch Order", "pass");
		} else {
			writeTestResults("Verify user can navigate to the new Internal Dispatch Order",
					"User should be able to navigate to new Internal Dispatch Order",
					"User doesn't navigate to new Internal Dispatch Order", "fail");
		}

		sendKeys(txt_shippingAddressIDO, shippingAddress);

		click(btn_draft);
		explicitWait(header_draftedIDO, 50);
		if (isDisplayed(header_draftedIDO)) {
			writeTestResults("Verify user able to draft the Internal Dispatch Order",
					"User should be able to draft the Internal Dispatch Order",
					"User successfully draft the Internal Dispatch Order", "pass");
		} else {
			writeTestResults("Verify user able to draft the Internal Dispatch Order",
					"User should be able to draft the Internal Dispatch Order",
					"User doesn't draft the Internal Dispatch Order", "fail");

		}

		click(btn_release);

		explicitWait(header_releasedIDO, 60);

		if (isDisplayedQuickCheck(header_releasedIDO)) {
			trackCode = getText(lbl_docNumber);
			writeTestResults("Verify user able to release the Internal Dispatch Order",
					"User should be able to release the Internal Dispatch Order",
					"User successfully release the Internal Dispatch Order", "pass");
		} else {
			changeCSS(btn_closeMainValidation, "z-index:-9999");
			click(btn_firstRownErrorOnGrid);
			Thread.sleep(1500);
			click(btn_firstRownErrorOnGrid);
			explicitWait(div_quantityNotAvaialableValidation, 5);
			if (isDisplayed(div_quantityNotAvaialableValidation)) {
				Thread.sleep(1500);
				((JavascriptExecutor) driver).executeScript("window.open('about:blank','_blank');");
				Thread.sleep(1000);
				switchWindow();
				openPage(siteUrlMultiPlatformm);
				purchaseOrderToInboundShipmentToPurchaseInvoice();

				pageRefersh();
				explicitWaitUntillClickable(btn_release, 40);
				click(btn_release);

				explicitWait(header_releasedIDO, 50);
				trackCode = getText(lbl_docNumber);
				if (isDisplayed(header_releasedIDO)) {
					writeTestResults("Verify user able to release the Internal Dispatch Order",
							"User should be able to release the Internal Dispatch Order",
							"User successfully release the Internal Dispatch Order", "pass");
				} else {
					writeTestResults("Verify user able to release the Internal Dispatch Order",
							"User should be able to release the Internal Dispatch Order",
							"User doesn't release the Internal Dispatch Order", "fail");
				}
			} else {
				writeTestResults("Verify user able to release the Internal Dispatch Order",
						"User should be able to release the Internal Dispatch Order",
						"User doesn't release the Internal Dispatch Order", "fail");
			}

		}
	}

	public void mrpRun2() throws Exception {
		openPage(readProductionData("ReleaseedPO_PRD_E2E_001"));
		explicitWaitUntillClickable(tab_costingSummaryPO, 60);
		click(tab_costingSummaryPO);
		explicitWaitUntillClickable(btn_expandCostingSummary, 20);

		click(btn_expandCostingSummary);
		Thread.sleep(500);
		explicitWaitUntillClickable(btn_expandCostingSummary, 20);
		click(btn_expandCostingSummary);
		explicitWaitUntillClickable(btn_expandCostingSummary, 20);
		click(btn_expandCostingSummary);
		explicitWaitUntillClickable(btn_expandCostingSummary, 20);
		click(btn_expandCostingSummary);

		if (isDisplayed(column_headerAllocatedCostingSummary)) {
			writeTestResults("Verify that the Estimated Column is display same as earlier",
					"The Estimated Column should display same as earlier",
					"The Estimated Column successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that the Estimated Column is display same as earlier",
					"The Estimated Column should display same as earlier",
					"The Estimated Column doesn't display same as earlier", "fail");
		}

		if (isDisplayed(td_mainResourseHeaderCostCostingSummary)) {
			writeTestResults("Verify that the Resource Total Row is display same as earlier",
					"The Resource Total Row should display same as earlier",
					"The Resource Total Row successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that the Resource Total Row is display same as earlier",
					"The Resource Total Row should display same as earlier",
					"The Resource Total Row doesn't display same as earlier", "fail");
		}

		if (isDisplayed(td_resourceCostAllocatedCostingSummary)) {
			writeTestResults("Verify that the Resource Total Cost is display correctly",
					"The Resource Total Cost should display correctly",
					"The Resource Total Cost successfully display correctly", "pass");
		} else {
			writeTestResults("Verify that the Resource Total Cost is display correctly",
					"The Resource Total Cost should display correctly",
					"The Resource Total Cost doesn't display correctly", "fail");
		}

		if (isDisplayed(td_resurce1CostingSummary.replace("changing_part", readProductionData("ResourceCode01_E2E")))) {
			writeTestResults("Verify that the Resource 01 Row is display same as earlier",
					"The Resource 01 Row should display same as earlier",
					"The Resource 01 Row successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that the Resource 01 Row is display same as earlier",
					"The Resource 01 Row should display same as earlier",
					"The Resource 01 Row doesn't display same as earlier", "fail");
		}

		if (isDisplayed(td_resourceCost1AllocatedCostingSummary)) {
			writeTestResults("Verify that the Cost of Resource 01 is display correctly",
					"The Cost of Resource 01 should display correctly",
					"The Cost of Resource 01 successfully display correctly", "pass");
		} else {
			writeTestResults("Verify that the Cost of Resource 01 is display correctly",
					"The Cost of Resource 01 should display correctly",
					"The Cost of Resource 01 doesn't display correctly", "fail");
		}

		if (isDisplayed(td_resurce2CostingSummary.replace("changing_part", readProductionData("ResourceCode02_E2E")))) {
			writeTestResults("Verify that the Resource 02 Row is display same as earlier",
					"The Resource 02 Row should display same as earlier",
					"The Resource 02 Row successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that the Resource 02 Row is display same as earlier",
					"The Resource 02 Row should display same as earlier",
					"The Resource 02 Row doesn't display same as earlier", "fail");
		}

		if (isDisplayed(td_resourceCost2AllocatedCostingSummary)) {
			writeTestResults("Verify that the Cost of Resource 02 is display correctly",
					"The Cost of Resource 02 should display correctly",
					"The Cost of Resource 02 successfully display correctly", "pass");
		} else {
			writeTestResults("Verify that the Cost of Resource 02 is display correctly",
					"The Cost of Resource 02 should display correctly",
					"The Cost of Resource 02 doesn't display correctly", "fail");
		}

		if (isDisplayedQuickCheck(td_materialCostAllocatedCostingSummary)) {
			if (isDisplayed(td_mainMaterialsHeaderCostCostingSummary)) {
				writeTestResults("Verify that the Material Total Row is display same as earlier",
						"The Material Total Row should display same as earlier",
						"The Material Total Row successfully display same as earlier", "pass");
			} else {
				writeTestResults("Verify that the Material Total Row is display same as earlier",
						"The Material Total Row should display same as earlier",
						"The Material Total Row doesn't display same as earlier", "fail");
			}

			if (isDisplayed(td_materialCostAllocatedCostingSummary)) {
				writeTestResults("Verify that the Material Total Cost is display correctly",
						"The Material Total Cost should display correctly",
						"The Material Total Cost successfully display correctly", "pass");
			} else {
				writeTestResults("Verify that the Material Total Cost is display correctly",
						"The Material Total Cost should display correctly",
						"The Material Total Cost doesn't display correctly", "fail");
			}

			if (isDisplayed(td_material1CostingSummary.replace("changing_part",
					invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialOne")))) {
				writeTestResults("Verify that the Material 01 Row is display same as earlier",
						"The Material 01 Row should display same as earlier",
						"The Material 01 Row successfully display same as earlier", "pass");
			} else {
				writeTestResults("Verify that the Material 01 Row is display same as earlier",
						"The Material 01 Row should display same as earlier",
						"The Material 01 Row doesn't display same as earlier", "fail");
			}

			if (isDisplayed(td_material1CostAllocatedCostingSummary)) {
				writeTestResults("Verify that the Cost of Material 01 is display correctly",
						"The Cost of Material 01 should display correctly",
						"The Cost of Material 01 successfully display correctly", "pass");
			} else {
				writeTestResults("Verify that the Cost of Material 01 is display correctly",
						"The Cost of Material 01 should display correctly",
						"The Cost of Material 01 doesn't display correctly", "fail");
			}

			if (isDisplayed(td_material2CostingSummary.replace("changing_part",
					invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialTwo")))) {
				writeTestResults("Verify that the Material 02 Row is display same as earlier",
						"The Material 02 Row should display same as earlier",
						"The Material 02 Row successfully display same as earlier", "pass");
			} else {
				writeTestResults("Verify that the Material 02 Row is display same as earlier",
						"The Material 02 Row should display same as earlier",
						"The Material 02 Row doesn't display same as earlier", "fail");
			}

			if (isDisplayed(td_material2CostAllocatedCostingSummary)) {
				writeTestResults("Verify that the Cost of Material 02 is display correctly",
						"The Cost of Material 02 should display correctly",
						"The Cost of Material 02 successfully display correctly", "pass");
			} else {
				writeTestResults("Verify that the Cost of Material 02 is display correctly",
						"The Cost of Material 02 should display correctly",
						"The Cost of Material 02 doesn't display correctly", "fail");
			}

			if (isDisplayed(td_material3CostingSummary.replace("changing_part",
					invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialHasVarientProductThree")))) {
				writeTestResults("Verify that the Material 03 Row is display same as earlier",
						"The Material 03 Row should display same as earlier",
						"The Material 03 Row successfully display same as earlier", "pass");
			} else {
				writeTestResults("Verify that the Material 03 Row is display same as earlier",
						"The Material 03 Row should display same as earlier",
						"The Material 03 Row doesn't display same as earlier", "fail");
			}

			if (isDisplayed(td_material3CostAllocatedCostingSummary)) {
				writeTestResults("Verify that the Cost of Material 03 is display correctly",
						"The Cost of Material 03 should display correctly",
						"The Cost of Material 03 successfully display correctly", "pass");
			} else {
				writeTestResults("Verify that the Cost of Material 03 is display correctly",
						"The Cost of Material 03 should display correctly",
						"The Cost of Material 03 doesn't display correctly", "fail");
			}

		} else if (rowMaterialCreated) {
			if (isDisplayed(td_mainMaterialsHeaderCostCostingSummary)) {
				writeTestResults("Verify that the Material Total Row is display same as earlier",
						"The Material Total Row should display same as earlier",
						"The Material Total Row successfully display same as earlier", "pass");
			} else {
				writeTestResults("Verify that the Material Total Row is display same as earlier",
						"The Material Total Row should display same as earlier",
						"The Material Total Row doesn't display same as earlier", "fail");
			}

			if (isDisplayed(td_materialCostAllocatedCostingSummaryBeforePurchase)) {
				writeTestResults("Verify that the Material Total Cost is display correctly",
						"The Material Total Cost should display correctly",
						"The Material Total Cost successfully display correctly", "pass");
			} else {
				writeTestResults("Verify that the Material Total Cost is display correctly",
						"The Material Total Cost should display correctly",
						"The Material Total Cost doesn't display correctly", "fail");
			}

			if (isDisplayed(td_material1CostingSummary.replace("changing_part",
					invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialOne")))) {
				writeTestResults("Verify that the Material 01 Row is display same as earlier",
						"The Material 01 Row should display same as earlier",
						"The Material 01 Row successfully display same as earlier", "pass");
			} else {
				writeTestResults("Verify that the Material 01 Row is display same as earlier",
						"The Material 01 Row should display same as earlier",
						"The Material 01 Row doesn't display same as earlier", "fail");
			}

			if (isDisplayed(td_material1CostAllocatedCostingSummaryBeforePurchase)) {
				writeTestResults("Verify that the Cost of Material 01 is display correctly",
						"The Cost of Material 01 should display correctly",
						"The Cost of Material 01 successfully display correctly", "pass");
			} else {
				writeTestResults("Verify that the Cost of Material 01 is display correctly",
						"The Cost of Material 01 should display correctly",
						"The Cost of Material 01 doesn't display correctly", "fail");
			}

			if (isDisplayed(td_material2CostingSummary.replace("changing_part",
					invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialTwo")))) {
				writeTestResults("Verify that the Material 02 Row is display same as earlier",
						"The Material 02 Row should display same as earlier",
						"The Material 02 Row successfully display same as earlier", "pass");
			} else {
				writeTestResults("Verify that the Material 02 Row is display same as earlier",
						"The Material 02 Row should display same as earlier",
						"The Material 02 Row doesn't display same as earlier", "fail");
			}

			if (isDisplayed(td_material2CostAllocatedCostingSummaryBeforePurchase)) {
				writeTestResults("Verify that the Cost of Material 02 is display correctly",
						"The Cost of Material 02 should display correctly",
						"The Cost of Material 02 successfully display correctly", "pass");
			} else {
				writeTestResults("Verify that the Cost of Material 02 is display correctly",
						"The Cost of Material 02 should display correctly",
						"The Cost of Material 02 doesn't display correctly", "fail");
			}

			if (isDisplayed(td_material3CostingSummary.replace("changing_part",
					invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialHasVarientProductThree")))) {
				writeTestResults("Verify that the Material 03 Row is display same as earlier",
						"The Material 03 Row should display same as earlier",
						"The Material 03 Row successfully display same as earlier", "pass");
			} else {
				writeTestResults("Verify that the Material 03 Row is display same as earlier",
						"The Material 03 Row should display same as earlier",
						"The Material 03 Row doesn't display same as earlier", "fail");
			}

			if (isDisplayed(td_material3CostAllocatedCostingSummaryBeforePurchase)) {
				writeTestResults("Verify that the Cost of Material 03 is display correctly",
						"The Cost of Material 03 should display correctly",
						"The Cost of Material 03 successfully display correctly", "pass");
			} else {
				writeTestResults("Verify that the Cost of Material 03 is display correctly",
						"The Cost of Material 03 should display correctly",
						"The Cost of Material 03 doesn't display correctly", "fail");
			}
		} else {
			writeTestResults("Verify that the Cost of Materials is display correctly",
					"The Cost of Materials should display correctly", "The Cost of Materials doesn't display correctly",
					"fail");
		}

		if (isDisplayed(td_mainOverheadHeaderCostCostingSummary)) {
			writeTestResults("Verify that the Overhead Total Row is display same as earlier",
					"The Overhead Total Row should display same as earlier",
					"The Overhead Total Row successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that the Overhead Total Row is display same as earlier",
					"The Overhead Total Row should display same as earlier",
					"The Overhead Total Row doesn't display same as earlier", "fail");
		}

		if (isDisplayed(td_overheadCostAllocatedCostingSummary)) {
			writeTestResults("Verify that the Overhead Total Cost is display correctly",
					"The Overhead Total Cost should display correctly",
					"The Overhead Total Cost successfully display correctly", "pass");
		} else {
			writeTestResults("Verify that the Overhead Total Cost is display correctly",
					"The Overhead Total Cost should display correctly",
					"The Overhead Total Cost doesn't display correctly", "fail");
		}

		if (isDisplayed(
				td_overhead1CostingSummary.replace("changing_part1", readProductionData("OverheadOne_PROD_E2E_001"))
						.replace("changing_part2", readProductionData("ResourceCode01_E2E")))) {
			writeTestResults("Verify that the Overhead 01 Row is display same as earlier",
					"The Overhead 01 Row should display same as earlier",
					"The Overhead 01 Row successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that the Overhead 01 Row is display same as earlier",
					"The Overhead 01 Row should display same as earlier",
					"The Overhead 01 Row doesn't display same as earlier", "fail");
		}

		if (isDisplayed(td_overhead1CostAllocatedCostingSummary)) {
			writeTestResults("Verify that the Cost of Overhead 01 is display correctly",
					"The Cost of Overhead 01 should display correctly",
					"The Cost of Overhead 01 successfully display correctly", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead 01 is display correctly",
					"The Cost of Overhead 01 should display correctly",
					"The Cost of Overhead 01 doesn't display correctly", "fail");
		}

		if (isDisplayed(
				td_overhead2CostingSummary.replace("changing_part1", readProductionData("OverheadOne_PROD_E2E_001"))
						.replace("changing_part2", readProductionData("ResourceCode02_E2E")))) {
			writeTestResults("Verify that the Overhead 02 Row is display same as earlier",
					"The Overhead 02 Row should display same as earlier",
					"The Overhead 02 Row successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that the Overhead 02 Row is display same as earlier",
					"The Overhead 02 Row should display same as earlier",
					"The Overhead 02 Row doesn't display same as earlier", "fail");
		}

		if (isDisplayed(td_overhead2CostAllocatedCostingSummary)) {
			writeTestResults("Verify that the Cost of Overhead 02 is display correctly",
					"The Cost of Overhead 02 should display correctly",
					"The Cost of Overhead 02 successfully display correctly", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead 02 is display correctly",
					"The Cost of Overhead 02 should display correctly",
					"The Cost of Overhead 02 doesn't display correctly", "fail");
		}

		if (isDisplayed(
				td_overhead3CostingSummary.replace("changing_part1", readProductionData("OverheadTwo_PROD_E2E_001"))
						.replace("changing_part2", readProductionData("ResourceCode02_E2E")))) {
			writeTestResults("Verify that the Overhead 03 Row is display same as earlier",
					"The Overhead 03 Row should display same as earlier",
					"The Overhead 03 Row successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that the Overhead 03 Row is display same as earlier",
					"The Overhead 03 Row should display same as earlier",
					"The Overhead 03 Row doesn't display same as earlier", "fail");
		}

		if (isDisplayed(td_overhead3CostAllocatedCostingSummary)) {
			writeTestResults("Verify that the Cost of Overhead 03 is display correctly",
					"The Cost of Overhead 03 should display correctly",
					"The Cost of Overhead 03 successfully display correctly", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead 03 is display correctly",
					"The Cost of Overhead 03 should display correctly",
					"The Cost of Overhead 03 doesn't display correctly", "fail");
		}

		if (isDisplayed(td_totalCostingSummary)) {
			writeTestResults("Verify that the Total Row is display same as earlier",
					"The Total Row should display same as earlier",
					"The Total Row successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that the Total Row is display same as earlier",
					"The Total Row should display same as earlier", "The Total Row doesn't display same as earlier",
					"fail");
		}

		if (rowMaterialCreated) {
			if (isDisplayed(td_totalCostAllocatedCostingSummaryBeforePurchase)) {
				writeTestResults("Verify that the Total Cost is display correctly",
						"The Total Cost should display correctly", "The Total Cost successfully display correctly",
						"pass");
			} else {
				writeTestResults("Verify that the Total Cost is display correctly",
						"The Total Cost should display correctly", "The Total Cost doesn't display correctly", "fail");
			}
		} else {
			if (isDisplayed(td_totalCostAllocatedCostingSummary)) {
				writeTestResults("Verify that the Total Cost is display correctly",
						"The Total Cost should display correctly", "The Total Cost successfully display correctly",
						"pass");
			} else {
				writeTestResults("Verify that the Total Cost is display correctly",
						"The Total Cost should display correctly", "The Total Cost doesn't display correctly", "fail");
			}
		}

		if (isDisplayed(td_unitCostHeaderCostingSummary)) {
			writeTestResults("Verify that the Unit Cost Row is display same as earlier",
					"The Unit Cost Row should display same as earlier",
					"The Unit Cost Row successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that the Unit Cost Row is display same as earlier",
					"The Unit Cost Row should display same as earlier",
					"The Unit Cost Row doesn't display same as earlier", "fail");
		}

		if (rowMaterialCreated) {
			if (isDisplayed(td_unitCostAllocatedCostingSummaryBeforePurchase)) {
				writeTestResults("Verify that the Unit Cost is display correctly",
						"The Unit Cost should display correctly", "The Unit Cost successfully display correctly",
						"pass");
			} else {
				writeTestResults("Verify that the Unit Cost is display correctly",
						"The Unit Cost should display correctly", "The Unit Cost doesn't display correctly", "fail");
			}
		} else {
			if (isDisplayed(td_unitCostAllocatedCostingSummary)) {
				writeTestResults("Verify that the Unit Cost is display correctly",
						"The Unit Cost should display correctly", "The Unit Cost successfully display correctly",
						"pass");
			} else {
				writeTestResults("Verify that the Unit Cost is display correctly",
						"The Unit Cost should display correctly", "The Unit Cost doesn't display correctly", "fail");
			}
		}

		if (isDisplayed(td_unitCostHeaderResourseCostingSummary)) {
			writeTestResults("Verify that the Resource (Unit Cost) Row is display same as earlier",
					"The Resource (Unit Cost) Row should display same as earlier",
					"The Resource (Unit Cost) Row successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that the Resource (Unit Cost) Row is display same as earlier",
					"The Resource (Unit Cost) Row should display same as earlier",
					"The Resource (Unit Cost) Row doesn't display same as earlier", "fail");
		}

		if (isDisplayed(td_unitCostResourseAllocatedCostingSummary)) {
			writeTestResults("Verify that the Cost of Resource (Unit Cost) is display correctly",
					"The Cost of Resource (Unit Cost) should display correctly",
					"The Cost of Resource (Unit Cost) successfully display correctly", "pass");
		} else {
			writeTestResults("Verify that the Cost of Resource (Unit Cost) is display correctly",
					"The Cost of Resource (Unit Cost) should display correctly",
					"The Cost of Resource (Unit Cost) doesn't display correctly", "fail");
		}

		if (isDisplayed(td_unitCostHeaderMaterialCostingSummary)) {
			writeTestResults("Verify that the Material (Unit Cost) Row is display same as earlier",
					"The Material (Unit Cost) Row should display same as earlier",
					"The Material (Unit Cost) Row successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that the Material (Unit Cost) Row is display same as earlier",
					"The Material (Unit Cost) Row should display same as earlier",
					"The Material (Unit Cost) Row doesn't display same as earlier", "fail");
		}

		if (rowMaterialCreated) {
			if (isDisplayed(td_unitCostMateraialAllocatedCostingSummaryBeforePurchase)) {
				writeTestResults("Verify that the Cost of Material (Unit Cost) is display correctly",
						"The Cost of Material (Unit Cost) should display correctly",
						"The Cost of Material (Unit Cost) successfully display correctly", "pass");
			} else {
				writeTestResults("Verify that the Cost of Material (Unit Cost) is display correctly",
						"The Cost of Material (Unit Cost) should display correctly",
						"The Cost of Material (Unit Cost) doesn't display correctly", "fail");
			}
		} else {
			if (isDisplayed(td_unitCostMateraialAllocatedCostingSummary)) {
				writeTestResults("Verify that the Cost of Material (Unit Cost) is display correctly",
						"The Cost of Material (Unit Cost) should display correctly",
						"The Cost of Material (Unit Cost) successfully display correctly", "pass");
			} else {
				writeTestResults("Verify that the Cost of Material (Unit Cost) is display correctly",
						"The Cost of Material (Unit Cost) should display correctly",
						"The Cost of Material (Unit Cost) doesn't display correctly", "fail");
			}
		}

		if (isDisplayed(td_unitCostHeaderOverheadCostingSummary)) {
			writeTestResults("Verify that the Overhead (Unit Cost) Row is display same as earlier",
					"The Overhead (Unit Cost) Row should display same as earlier",
					"The Overhead (Unit Cost) Row successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that the Overhead (Unit Cost) Row is display same as earlier",
					"The Overhead (Unit Cost) Row should display same as earlier",
					"The Overhead (Unit Cost) Row doesn't display same as earlier", "fail");
		}

		if (isDisplayed(td_unitCostOverheadAllocatedCostingSummary)) {
			writeTestResults("Verify that the Cost of Overhead (Unit Cost) is display correctly",
					"The Cost of Overhead (Unit Cost) should display correctly",
					"The Cost of Overhead (Unit Cost) successfully display correctly", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead (Unit Cost) is display correctly",
					"The Cost of Overhead (Unit Cost) should display correctly",
					"The Cost of Overhead (Unit Cost) doesn't display correctly", "fail");
		}

	}

	/* Purchase Order */
	public void purchaseOrderToInboundShipmentToPurchaseInvoice() throws Exception {
		try {
			explicitWait(img_navigationPane, 20);
			Thread.sleep(2000);
			explicitWait(img_navigationPane, 20);
			click(img_navigationPane);
			explicitWait(btn_prcumentModule, 10);
			click(btn_prcumentModule);
			explicitWait(btn_purchaseOrder, 10);
			click(btn_purchaseOrder);

			explicitWaitUntillClickable(btn_newPurchaseOrder, 60);
			click(btn_newPurchaseOrder);

			explicitWaitUntillClickable(btn_purchaseOrderInboundShipmentToPurchaseInvoiceJourney, 60);
			click(btn_purchaseOrderInboundShipmentToPurchaseInvoiceJourney);

			explicitWaitUntillClickable(btn_vendorLookup, 60);
			click(btn_vendorLookup);
			Thread.sleep(2000);
			explicitWaitUntillClickable(txt_vendorInformation, 10);
			pressEnter(txt_vendorInformation);
			Thread.sleep(2000);
			explicitWaitUntillClickable(td_resultVendorFirstResult, 40);
			doubleClick(td_resultVendorFirstResult);
			Thread.sleep(2000);

			click(btn_billingAddressLookup);
			explicitWaitUntillClickable(txt_billingAddresePurchaseOrder, 10);
			sendKeys(txt_billingAddresePurchaseOrder, billingAddressPO);
			click(btn_applyAddressPurchaseOrder);
			Thread.sleep(1500);

			/* Product 01 */
			explicitWaitUntillClickable(btn_productionLookupPurchaseOrderGrid.replace("row", "1"), 60);
			click(btn_productionLookupPurchaseOrderGrid.replace("row", "1"));
			Thread.sleep(2000);
			explicitWaitUntillClickable(txt_searchProduct, 10);
			sendKeysLookup(txt_searchProduct, invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialOne"));
			pressEnter(txt_searchProduct);
			Thread.sleep(2000);
			explicitWaitUntillClickable(td_resultProcuctProductionLookupProductReplace.replace("product",
					invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialOne")), 40);
			doubleClick(td_resultProcuctProductionLookupProductReplace.replace("product",
					invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialOne")));
			Thread.sleep(2000);

			sendKeys(txt_qtyPurchaseOrderRowReplace.replace("row", "1"), qtyProductPurchaseOrder);

			sendKeys(txt_unitPricePurchaseOrderRowReplace.replace("row", "1"), unitPriceProduct1PurchaseOrder);

			click(btn_addNewRecordToGrid);

			/* Product 02 */
			explicitWaitUntillClickable(btn_productionLookupPurchaseOrderGrid.replace("row", "2"), 60);
			click(btn_productionLookupPurchaseOrderGrid.replace("row", "2"));
			Thread.sleep(2000);
			explicitWaitUntillClickable(txt_searchProduct, 10);
			sendKeysLookup(txt_searchProduct, invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialTwo"));
			pressEnter(txt_searchProduct);
			Thread.sleep(2000);
			explicitWaitUntillClickable(td_resultProcuctProductionLookupProductReplace.replace("product",
					invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialTwo")), 40);
			doubleClick(td_resultProcuctProductionLookupProductReplace.replace("product",
					invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialTwo")));
			Thread.sleep(2000);

			sendKeys(txt_qtyPurchaseOrderRowReplace.replace("row", "2"), qtyProductPurchaseOrder);

			sendKeys(txt_unitPricePurchaseOrderRowReplace.replace("row", "2"), unitPriceProduct2PurchaseOrder);

			click(btn_addNewRecordToGrid);

			/* Product 03 */
			explicitWaitUntillClickable(btn_productionLookupPurchaseOrderGrid.replace("row", "3"), 7);
			click(btn_productionLookupPurchaseOrderGrid.replace("row", "3"));
			Thread.sleep(2000);
			explicitWaitUntillClickable(txt_searchProduct, 10);
			sendKeysLookup(txt_searchProduct,
					invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialHasVarientProductThree"));
			pressEnter(txt_searchProduct);
			Thread.sleep(2000);
			explicitWaitUntillClickable(td_resultProcuctProductionLookupProductReplace.replace("product",
					invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialHasVarientProductThree")), 40);
			doubleClick(td_resultProcuctProductionLookupProductReplace.replace("product",
					invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialHasVarientProductThree")));
			Thread.sleep(2000);

			sendKeys(txt_qtyPurchaseOrderRowReplace.replace("row", "3"), qtyProductPurchaseOrder);

			sendKeys(txt_unitPricePurchaseOrderRowReplace.replace("row", "3"), unitPriceProduct3PurchaseOrder);

			click(btn_checkOut);
			Thread.sleep(2000);

			explicitWaitUntillClickable(btn_draft, 20);
			click(btn_draft);

			explicitWaitUntillClickable(btn_release, 60);
			click(btn_release);

			explicitWaitUntillClickable(btn_goToPage, 60);
			if (isDisplayed(btn_goToPage)) {
				writeTestResults("Verify user able to release the Purchase Order",
						"User should be able to release the Purchase Order",
						"User successfully release the Purchase Order", "pass");
			} else {
				writeTestResults("Verify user able to release the Purchase Order",
						"User should be able to release the Purchase Order", "User doesn't release the Purchase Order",
						"fail");
			}

			click(btn_goToPage);
			Thread.sleep(2000);
			switchWindow();
			explicitWaitUntillClickable(drop_selectWarehouseInboundShipmentRowReplace.replace("row", "1"), 60);
			selectText(drop_selectWarehouseInboundShipmentRowReplace.replace("row", "1"), inputWarehouseLI);

			explicitWaitUntillClickable(drop_selectWarehouseInboundShipmentRowReplace.replace("row", "2"), 60);
			selectText(drop_selectWarehouseInboundShipmentRowReplace.replace("row", "2"), inputWarehouseLI);

			explicitWaitUntillClickable(drop_selectWarehouseInboundShipmentRowReplace.replace("row", "3"), 60);
			selectText(drop_selectWarehouseInboundShipmentRowReplace.replace("row", "3"), inputWarehouseLI);

			Thread.sleep(1000);
			click(btn_checkOut);
			Thread.sleep(2000);

			click(btn_draft);
			explicitWaitUntillClickable(btn_searilBatch, 60);
			click(btn_searilBatch);

			/* Capture Product 01 */
			explicitWaitUntillClickable(btn_captureProductOne, 20);
			click(btn_captureProductOne);
			Thread.sleep(1500);
			explicitWaitUntillClickable(txt_batchNoSerialBatchCaptureWindow, 20);
			sendKeys(txt_batchNoSerialBatchCaptureWindow, common.getCurrentDateTimeAsUniqueCode());
			sendKeys(txt_lotNoSerialBatchCaptureWindow, common.getCurrentDateTimeAsUniqueCode());

			click(txt_expiryDateSerialBatchCaptureWindow);
			explicitWaitUntillClickable(btn_nextArrowSerialBatchCaptureWindow, 5);
			click(btn_nextArrowSerialBatchCaptureWindow);
			explicitWaitUntillClickable(btn_nextArrowSerialBatchCaptureWindow, 5);
			click(btn_nextArrowSerialBatchCaptureWindow);
			explicitWaitUntillClickable(btn_nextArrowSerialBatchCaptureWindow, 5);
			click(btn_nextArrowSerialBatchCaptureWindow);
			explicitWaitUntillClickable(btn_nextArrowSerialBatchCaptureWindow, 5);
			click(btn_nextArrowSerialBatchCaptureWindow);
			explicitWaitUntillClickable(btn_nextArrowSerialBatchCaptureWindow, 5);
			click(btn_nextArrowSerialBatchCaptureWindow);
			click(btn_defoultLastDateCalender);
			Thread.sleep(1000);

			click(txt_menufactureDateSerialBatchCaptureWindow);
			explicitWaitUntillClickable(btn_defoultFirstDateCalender, 5);
			click(btn_defoultFirstDateCalender);
			Thread.sleep(1000);
			pressEnter(txt_menufactureDateSerialBatchCaptureWindow);
			Thread.sleep(2000);

			click(btn_updateSearielBatchCaptureWindow);
			explicitWait(btn_captureProductOneGreen, 20);

			/* Capture Product 02 */
			explicitWaitUntillClickable(btn_captureProductTwo, 20);
			click(btn_captureProductTwo);
			Thread.sleep(1500);
			explicitWaitUntillClickable(txt_batchNoSerialBatchCaptureWindow, 20);
			sendKeys(txt_batchNoSerialBatchCaptureWindow, common.getCurrentDateTimeAsUniqueCode());
			sendKeys(txt_lotNoSerialBatchCaptureWindow, common.getCurrentDateTimeAsUniqueCode());

			click(txt_expiryDateSerialBatchCaptureWindow);
			explicitWaitUntillClickable(btn_nextArrowSerialBatchCaptureWindow, 5);
			click(btn_nextArrowSerialBatchCaptureWindow);
			explicitWaitUntillClickable(btn_nextArrowSerialBatchCaptureWindow, 5);
			click(btn_nextArrowSerialBatchCaptureWindow);
			explicitWaitUntillClickable(btn_nextArrowSerialBatchCaptureWindow, 5);
			click(btn_nextArrowSerialBatchCaptureWindow);
			explicitWaitUntillClickable(btn_nextArrowSerialBatchCaptureWindow, 5);
			click(btn_nextArrowSerialBatchCaptureWindow);
			explicitWaitUntillClickable(btn_nextArrowSerialBatchCaptureWindow, 5);
			click(btn_nextArrowSerialBatchCaptureWindow);
			click(btn_defoultLastDateCalender);
			Thread.sleep(1000);

			click(txt_menufactureDateSerialBatchCaptureWindow);
			explicitWaitUntillClickable(btn_defoultFirstDateCalender, 5);
			click(btn_defoultFirstDateCalender);
			Thread.sleep(1000);
			pressEnter(txt_menufactureDateSerialBatchCaptureWindow);
			Thread.sleep(2000);

			click(btn_updateSearielBatchCaptureWindow);
			explicitWait(btn_captureProductTwoGreen, 20);

			/* Capture Product 03 */
			explicitWaitUntillClickable(btn_captureProductThree, 20);
			click(btn_captureProductThree);
			Thread.sleep(1500);
			explicitWaitUntillClickable(txt_batchNoSerialBatchCaptureWindow, 20);
			sendKeys(txt_batchNoSerialBatchCaptureWindow, common.getCurrentDateTimeAsUniqueCode());
			sendKeys(txt_lotNoSerialBatchCaptureWindow, common.getCurrentDateTimeAsUniqueCode());

			click(txt_expiryDateSerialBatchCaptureWindow);
			explicitWaitUntillClickable(btn_nextArrowSerialBatchCaptureWindow, 5);
			click(btn_nextArrowSerialBatchCaptureWindow);
			explicitWaitUntillClickable(btn_nextArrowSerialBatchCaptureWindow, 5);
			click(btn_nextArrowSerialBatchCaptureWindow);
			explicitWaitUntillClickable(btn_nextArrowSerialBatchCaptureWindow, 5);
			click(btn_nextArrowSerialBatchCaptureWindow);
			explicitWaitUntillClickable(btn_nextArrowSerialBatchCaptureWindow, 5);
			click(btn_nextArrowSerialBatchCaptureWindow);
			explicitWaitUntillClickable(btn_nextArrowSerialBatchCaptureWindow, 5);
			click(btn_nextArrowSerialBatchCaptureWindow);
			click(btn_defoultLastDateCalender);
			Thread.sleep(1000);

			click(txt_menufactureDateSerialBatchCaptureWindow);
			explicitWaitUntillClickable(btn_defoultFirstDateCalender, 5);
			click(btn_defoultFirstDateCalender);
			Thread.sleep(1000);
			pressEnter(txt_menufactureDateSerialBatchCaptureWindow);
			Thread.sleep(2000);

			click(btn_updateSearielBatchCaptureWindow);
			explicitWait(btn_captureProductThreeGreen, 20);

			explicitWaitUntillClickable(btn_backSearielBatchCaptureWindow, 40);
			click(btn_backSearielBatchCaptureWindow);

			explicitWaitUntillClickable(btn_release, 60);
			click(btn_release);

			explicitWaitUntillClickable(btn_goToPage, 60);
			if (isDisplayed(btn_goToPage)) {
				writeTestResults("Verify user able to release the Inbound Shipment",
						"User should be able to release the Inbound Shipment",
						"User successfully release the Inbound Shipment", "pass");
			} else {
				writeTestResults("Verify user able to release the Inbound Shipment",
						"User should be able to release the Inbound Shipment",
						"User doesn't release the Inbound Shipment", "fail");
			}

			click(btn_goToPage);
			Thread.sleep(2000);
			switchWindow();
			Thread.sleep(2000);
			explicitWaitUntillClickable(btn_checkOut, 60);
			click(btn_checkOut);

			explicitWaitUntillClickable(btn_draft, 60);
			click(btn_draft);

			explicitWaitUntillClickable(btn_release, 60);
			Thread.sleep(2000);
			explicitWaitUntillClickable(btn_release, 60);
			click(btn_release);

			explicitWaitUntillClickable(header_releasedPurchaseInvoice, 60);
			if (isDisplayed(header_releasedPurchaseInvoice)) {
				writeTestResults("Verify user able to release the Purchase Invoice",
						"User should be able to release the Purchase Invoice",
						"User successfully release the Purchase Invoice", "pass");
			} else {
				writeTestResults("Verify user able to release the Purchase Invoice",
						"User should be able to release the Purchase Invoice",
						"User doesn't release the Purchase Invoice", "fail");
			}

			closeWindow();
			switchWindow();
			closeWindow();
			switchWindow();
		} catch (Exception e) {
			System.out.println(e);
			writeTestResults("Purchase Order journey fail", "Purchase Order journey fail",
					"Purchase Order journey fail", "fail");
		}
	}

	/* Shop Floor Update */
	public void shopFloorUpdate() throws InvalidFormatException, IOException, Exception {
		openPage(readProductionData("ReleaseedPO_PRD_E2E_001"));
		explicitWaitUntillClickable(btn_action2, 50);
		click(btn_action2);

		explicitWaitUntillClickable(btn_shopFloorUpdate2, 10);
		if (isDisplayed(btn_shopFloorUpdate2)) {
			writeTestResults("Verify that Shop floor update option is available under Actions",
					"Shop floor update option should be available under Actions",
					"Shop floor update option successfully available under Actions", "pass");
		} else {
			writeTestResults("Verify that Shop floor update option is available under Actions",
					"Shop floor update option should be available under Actions",
					"Shop floor update option doesn't available under Actions", "fail");
		}
		click(btn_shopFloorUpdate2);

		explicitWait(header_shopFloorUpdateWindow, 20);
		if (isDisplayed(header_shopFloorUpdateWindow)) {
			writeTestResults("Verify that shop floor update window is open", "Shop floor update window should be open",
					"Shop floor update window succeessfully open", "pass");
		} else {
			writeTestResults("Verify that shop floor update window is open", "Shop floor update window should be open",
					"Shop floor update window doesn't open", "fail");
		}

		/* First Operation */
		System.out.println("First Operation");
		click(btn_startFirstOperationSFU);
		explicitWaitUntillClickable(btn_startSecondLeveleSFU, 10);
		if (isDisplayed(btn_startSecondLeveleSFU)) {
			writeTestResults("Verify that start button is available", "Start button should be available",
					"Start button successfully available", "pass");
		} else {
			writeTestResults("Verify that start button is available", "Start button should be available",
					"Start button doesn't available", "fail");
		}

		click(btn_startSecondLeveleSFU);

		explicitWaitUntillClickable(btn_actualUpdateSFU, 10);
		if (isDisplayed(btn_actualUpdateSFU)) {
			writeTestResults("Verify that actual update option is available on shop floor update main window",
					"Actual update option should be available on shop floor update main window",
					"Actual update option successfully available on shop floor update main window", "pass");
		} else {
			writeTestResults("Verify that actual update option is available on shop floor update main window",
					"Actual update option should be available on shop floor update main window",
					"Actual update option doesn't available on shop floor update main window", "fail");
		}

		click(btn_actualUpdateSFU);

		Thread.sleep(2500);
		explicitWaitUntillClickable(txt_outoutQuantityACtualUpdateSFU, 20);
		Thread.sleep(2000);
		sendKeys(txt_outoutQuantityACtualUpdateSFU, actualQuantityOutputProductFirstOperation);
		String outputActualUpdateQuantityFirstOperation = getAttribute(txt_outoutQuantityACtualUpdateSFU, "value");
		if (outputActualUpdateQuantityFirstOperation.equals(actualQuantityOutputProductFirstOperation)) {
			writeTestResults("Verify user able to enter Output Product qty for the first operation",
					"User should be able to enter Output Product qty for the first operation",
					"User successfully enter Output Product qty for the first operation", "pass");
		} else {
			writeTestResults("Verify user able to enter Output Product qty for the first operation",
					"User should be able to enter Output Product qty for the first operation",
					"User doesn't enter Output Product qty for the first operation", "fail");
		}
		Thread.sleep(2000);

		click(btn_upateActualSFU);
		explicitWait(para_successfullValidationOutProductActualUpdate, 8);
		if (isDisplayed(para_successfullValidationOutProductActualUpdate)) {
			writeTestResults("Verify that successfully updated validation is appear for the Output Product update",
					"Successfully updated validation should appear for the Output Product update",
					"Successfully updated validation successfully appear for the Output Product update", "pass");
		} else {
			writeTestResults("Verify that successfully updated validation is appear for the Output Product update",
					"Successfully updated validation should appear for the Output Product update",
					"Successfully updated validation doesn't appear for the Output Product update", "fail");
		}
		Thread.sleep(3000);

		click(btn_tapInputProductsShopFloorUpdate);

		sendKeys(txt_actualUpdaInputProductQuantityFirstOperation, actualQuantityInputProductFirstOperation);
		String inputActualUpdateQuantityFirstOperation = getAttribute(txt_actualUpdaInputProductQuantityFirstOperation,
				"value");
		if (inputActualUpdateQuantityFirstOperation.equals(actualQuantityInputProductFirstOperation)) {
			writeTestResults("Verify user able to enter Input Product qty for the first operation - First Material",
					"User should be able to enter Input Product qty for the first operation - First Material",
					"User successfully enter Input Product qty for the first operation - First Material", "pass");
		} else {
			writeTestResults("Verify user able to enter Input Product qty for the first operation - First Material",
					"User should be able to enter Input Product qty for the first operation - First Material",
					"User doesn't enter Input Product qty for the first operation - First Material", "fail");
		}

		sendKeys(txt_actualUpdaInputProductQuantity2FirstOperation, actualQuantity2InputProductFirstOperation);
		String inputActualUpdateQuantity2FirstOperation = getAttribute(
				txt_actualUpdaInputProductQuantity2FirstOperation, "value");
		if (inputActualUpdateQuantity2FirstOperation.equals(actualQuantity2InputProductFirstOperation)) {
			writeTestResults("Verify user able to enter Input Product qty for the first operation - Second Material",
					"User should be able to enter Input Product qty for the first operation - Second Material",
					"User successfully enter Input Product qty for the first operation - Second Material", "pass");
		} else {
			writeTestResults("Verify user able to enter Input Product qty for the first operation - Second Material",
					"User should be able to enter Input Product qty for the first operation - Second Material",
					"User doesn't enter Input Product qty for the first operation - Second Material", "fail");
		}

		click(btn_upateActualSFU);
		explicitWait(para_successfullValidationActualUpdate, 8);
		if (isDisplayed(para_successfullValidationActualUpdate)) {
			writeTestResults("Verify that successfully updated validation is appear for the Input Product update",
					"Successfully updated validation should appear for the Input Product update",
					"Successfully updated validation successfully appear for the Input Product update", "pass");
		} else {
			writeTestResults("Verify that successfully updated validation is appear for the Input Product update",
					"Successfully updated validation should appear for the Input Product update",
					"Successfully updated validation doesn't appear for the Input Product update", "fail");
		}
		Thread.sleep(3000);

		explicitWaitUntillClickable(tab_resourseShopFloorUpdate, 30);
		click(tab_resourseShopFloorUpdate);
		explicitWaitUntillClickable(div_actualHoursResourseTabRow01, 20);
		Thread.sleep(2000);
		String actualHoursRow1 = getAttribute(div_actualHoursResourseTabRow01, "value");
		String actualminsRow1 = getAttribute(div_actualMinsResourseTabRow01, "value");
		String actualHoursRow2 = getAttribute(div_actualHoursResourseTabRow02, "value");
		String actualminsRow2 = getAttribute(div_actualMinsResourseTabRow02, "value");

		if (actualHoursRow1.equals("01") && actualminsRow1.equals("00") && actualHoursRow2.equals("01")
				&& actualminsRow2.equals("00")) {
			writeTestResults("Verify that actual resource hours are auto loaded same as estimated",
					"Actual resource hours should be auto loaded same as estimated",
					"Actual resource hours successfully auto loaded same as estimated", "pass");
		} else {
			writeTestResults("Verify that actual resource hours are auto loaded same as estimated",
					"Actual resource hours should be auto loaded same as estimated",
					"Actual resource hours weren't auto loaded same as estimated", "fail");
		}

		click(btn_upateActualSFU);
		explicitWait(para_successfullValidationActualUpdateResourse, 8);
		if (isDisplayed(para_successfullValidationActualUpdateResourse)) {
			writeTestResults("Verify that successfully updated validation is appear for the Resource update",
					"Successfully updated validation should appear for the Resource update",
					"Successfully updated validation successfully appear for the Resource update", "pass");
		} else {
			writeTestResults("Verify that successfully updated validation is appear for the Resource update",
					"Successfully updated validation should appear for the Resource update",
					"Successfully updated validation doesn't appear for the Resource update", "fail");
		}

		Thread.sleep(3000);

		click(btn_closeFisrstOperationActualUpdateWindow);

		explicitWait(btn_completeFirstOperation, 8);
		if (isDisplayed(btn_completeFirstOperation)) {
			writeTestResults("Verify that complete button is available for the first operation",
					"Complete button should be available for the first operation",
					"Complete button successfully available for the first operation", "pass");
		} else {
			writeTestResults("Verify that complete button is available for the first operation",
					"Complete button should be available for the first operation",
					"Complete button doesn't available for the first operation", "fail");
		}
		click(btn_completeFirstOperation);

		explicitWait(btn_completeSecondLeveleSFU, 8);
		if (isDisplayed(btn_completeSecondLeveleSFU)) {
			writeTestResults("Verify that complete button is available for the complete window of first operation",
					"Complete button should be available for the complete window of first operation",
					"Complete button successfully available for the complete window of first operation", "pass");
		} else {
			writeTestResults("Verify that complete button is available for the complete window of first operation",
					"Complete button should be available for the complete window of first operation",
					"Complete button doesn't available for the complete window of first operation", "fail");
		}
		click(btn_completeSecondLeveleSFU);

		explicitWait(lbl_completedShopFloorUpdateBarFirstOperation, 8);
		if (isDisplayed(lbl_completedShopFloorUpdateBarFirstOperation)) {
			writeTestResults("Verify that shop flow update is completed for the first operation",
					"Shop flow update should be completed for the first operation",
					"Shop flow update successfully completed for the first operation", "pass");
		} else {
			writeTestResults("Verify that shop flow update is completed for the first operation",
					"Shop flow update should be completed for the first operation",
					"Shop flow update doesn't completed for the first operation", "pass");
		}

		/* Second Operation */
		System.out.println("Second Operation");

		explicitWaitUntillClickable(btn_actualUpdateSFU, 10);
		if (isDisplayed(btn_actualUpdateSFU)) {
			writeTestResults(
					"Verify that actual update option is available on shop floor update main window for second operation",
					"Actual update option should be available on shop floor update main window for second operation",
					"Actual update option successfully available on shop floor update main window for second operation",
					"pass");
		} else {
			writeTestResults(
					"Verify that actual update option is available on shop floor update main window for second operation",
					"Actual update option should be available on shop floor update main window for second operation",
					"Actual update option doesn't available on shop floor update main window for second operation",
					"fail");
		}

		click(btn_actualUpdateSFU);

		explicitWaitUntillClickable(txt_outoutQuantityACtualUpdateSFU, 20);
		Thread.sleep(1500);
		sendKeys(txt_outoutQuantityACtualUpdateSFU, actualQuantityOutputProductSecondOperation);
		String outputActualUpdateQuantitySecondOperation = getAttribute(txt_outoutQuantityACtualUpdateSFU, "value");
		if (outputActualUpdateQuantitySecondOperation.equals(actualQuantityOutputProductSecondOperation)) {
			writeTestResults("Verify user able to enter Output Product qty for the second operation",
					"User should be able to enter Output Product qty for the second operation",
					"User successfully enter Output Product qty for the second operation", "pass");
		} else {
			writeTestResults("Verify user able to enter Output Product qty for the second operation",
					"User should be able to enter Output Product qty for the second operation",
					"User doesn't enter Output Product qty for the second operation", "fail");
		}
		Thread.sleep(1000);

		click(btn_upateActualSFU);
		explicitWait(para_successfullValidationOutProductActualUpdate, 8);
		if (isDisplayed(para_successfullValidationOutProductActualUpdate)) {
			writeTestResults("Verify that successfully updated validation is appear for the Output Product update",
					"Successfully updated validation should appear for the Output Product update",
					"Successfully updated validation successfully appear for the Output Product update", "pass");
		} else {
			writeTestResults("Verify that successfully updated validation is appear for the Output Product update",
					"Successfully updated validation should appear for the Output Product update",
					"Successfully updated validation doesn't appear for the Output Product update", "fail");
		}
		Thread.sleep(3000);

		click(btn_tapInputProductsShopFloorUpdate);

		sendKeys(txt_actualUpdaInputProductQuantitySecondOperation, actualQuantityInputProductSecondOperation);
		String inputActualUpdateQuantitySecondOperation = getAttribute(
				txt_actualUpdaInputProductQuantitySecondOperation, "value");
		if (inputActualUpdateQuantitySecondOperation.equals(actualQuantityInputProductSecondOperation)) {
			writeTestResults("Verify user able to enter Input Product qty for the second operation",
					"User should be able to enter Input Product qty for the second operation",
					"User successfully enter Input Product qty for the second operation", "pass");
		} else {
			writeTestResults("Verify user able to enter Input Product qty for the second operation",
					"User should be able to enter Input Product qty for the second operation",
					"User doesn't enter Input Product qty for the second operation", "fail");
		}

		click(btn_upateActualSFU);
		explicitWait(para_successfullValidationActualUpdate, 8);
		if (isDisplayed(para_successfullValidationActualUpdate)) {
			writeTestResults("Verify that successfully updated validation is appear for the Input Product update",
					"Successfully updated validation should appear for the Input Product update",
					"Successfully updated validation successfully appear for the Input Product update", "pass");
		} else {
			writeTestResults("Verify that successfully updated validation is appear for the Input Product update",
					"Successfully updated validation should appear for the Input Product update",
					"Successfully updated validation doesn't appear for the Input Product update", "fail");
		}
		Thread.sleep(3000);

		click(btn_closeSecondOperationActualUpdateWindow);

		explicitWait(btn_completeSecondOperation, 8);
		if (isDisplayed(btn_completeSecondOperation)) {
			writeTestResults("Verify that complete button is available for the second operation",
					"Complete button should be available for the second operation",
					"Complete button successfully available for the second operation", "pass");
		} else {
			writeTestResults("Verify that complete button is available for the second operation",
					"Complete button should be available for the second operation",
					"Complete button doesn't available for the second operation", "fail");
		}
		click(btn_completeSecondOperation);

		explicitWait(btn_completeSecondLeveleSFU, 8);
		if (isDisplayed(btn_completeSecondLeveleSFU)) {
			writeTestResults("Verify that complete button is available for the complete window of second operation",
					"Complete button should be available for the complete window of second operation",
					"Complete button successfully available for the complete window of second operation", "pass");
		} else {
			writeTestResults("Verify that complete button is available for the complete window of second operation",
					"Complete button should be available for the complete window of second operation",
					"Complete button doesn't available for the complete window of second operation", "fail");
		}
		click(btn_completeSecondLeveleSFU);

		explicitWait(lbl_completedShopFloorUpdateBarSecondOperation, 8);
		if (isDisplayed(lbl_completedShopFloorUpdateBarSecondOperation)) {
			writeTestResults("Verify that shop flow update is completed for the second operation",
					"Shop flow update should be completed for the second operation",
					"Shop flow update successfully completed for the second operation", "pass");
		} else {
			writeTestResults("Verify that shop flow update is completed for the second operation",
					"Shop flow update should be completed for the second operation",
					"Shop flow update doesn't completed for the second operation", "pass");
		}
	}

	/* QC */
	public void qc() throws Exception {
		pageRefersh();
		explicitWait(img_navigationPane, 20);
		Thread.sleep(2000);
		explicitWait(img_navigationPane, 20);
		click(img_navigationPane);
		explicitWait(btn_productionModule, 10);
		click(btn_productionModule);
		explicitWait(btn_productionQCAndSorting, 10);
		if (isDisplayed(btn_productionQCAndSorting)) {
			writeTestResults("Verif that Production QC & Sorting option is display under Production Module",
					"Production QC & Sorting option should be display under Production Module",
					"Production QC & Sorting option successfully display under Production Module", "pass");
		} else {
			writeTestResults("Verif that Production QC & Sorting option is display under Production Module",
					"Production QC & Sorting option should be display under Production Module",
					"Production QC & Sorting option doesn't display under Production Module", "fail");
		}

		click(btn_productionQCAndSorting);

		explicitWait(drop_documentTypeProductionQC, 50);
		selectText(drop_documentTypeProductionQC, qcTypeProductionOrder);
		Thread.sleep(1000);
		String selecteDocType = getSelectedOptionInDropdown(drop_documentTypeProductionQC);
		if (selecteDocType.equals(qcTypeProductionOrder)) {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User able to select 'Production Order' as document type", "pass");
		} else {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User not able to select 'Production Order' as document type", "fail");

		}

		sendKeys(txt_docNumberQC, readProductionData("ProductionOrder_PROD_E2E_001"));
		String enteredPO = getAttribute(txt_docNumberQC, "value");
		if (enteredPO.equals(readProductionData("ProductionOrder_PROD_E2E_001"))) {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User successfully enter Production Order document number", "pass");
		} else {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User doesn't enter Production Order document number", "fail");
		}

		Thread.sleep(1000);
		driver.findElement(getLocator(txt_docNumberQC)).sendKeys(Keys.ARROW_DOWN);
		pressEnter(txt_docNumberQC);

		explicitWait(div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_001")),
				30);
		if (isDisplayed(
				div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_001")))) {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User successfully select the relevent Production Order", "pass");
		} else {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User doesn't select the relevent Production Order", "fail");
		}

		Thread.sleep(1500);
		mouseMove(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")));
		Thread.sleep(500);
		click(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")));

		if (isSelected(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")))) {
			writeTestResults("Verify user able to select the relevent Output Product",
					"User should be able to select the relevent Output Product",
					"User successfully select the relevent Output Product", "pass");
		} else {
			writeTestResults("Verify user able to select the relevent Output Product",
					"User should be able to select the relevent Output Product",
					"User doesn't select the relevent Output Product", "fail");
		}

		/* First QC Scenario */
		Thread.sleep(1500);
		sendKeys(txt_failQuantityProductionQCOneRow, firstFailQty);
		String enteredFailQty = getAttribute(txt_failQuantityProductionQCOneRow, "value");
		if (enteredFailQty.equals(firstFailQty)) {
			writeTestResults("Verify user able to enter fail qty (10)", "User should be able to enter fail qty (10)",
					"User successfully enter fail qty (10)", "pass");
		} else {
			writeTestResults("Verify user able to enter fail qty (10)", "User should be able to enter fail qty (10)",
					"User doesn't enter fail qty (10)", "fail");
		}

		click(btn_releseProductionQC);
		explicitWaitUntillPresent(para_successfullValidationProductionQC, 20);

		if (isElementPresent(para_successfullValidationProductionQC)) {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation is appear", "pass");
		} else {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation doesn't appear", "fail");
		}

		Thread.sleep(2000);
		if (isDisplayed(column_headerTotalQtyQC)) {
			writeTestResults("Verify that total qty column header is display as earlier",
					"Total qty column header should be display as earlier",
					"Total qty column header successfully display as earlier", "pass");
		} else {
			writeTestResults("Verify that total qty column header is display as earlier",
					"Total qty column header should be display as earlier",
					"Total qty column header doesn't display as earlier", "fail");
		}

		String totalQuantity = getAttribute(txt_totalQtyQC, "value");
		if (totalQuantity.equals("90.00")) {
			writeTestResults("Verify that total qty is display by deducting fail qty",
					"Total qty should display by deducting fail qty",
					"Total qty successfully display by deducting fail qty", "pass");
		} else {
			writeTestResults("Verify that total qty is display by deducting fail qty",
					"Total qty should display by deducting fail qty", "Total qty doesn't display by deducting fail qty",
					"fail");
		}

		click(tab_historyQC);
		explicitWaitUntillClickable(tab_historySelectedQC, 20);
		if (isDisplayed(tab_historySelectedQC)) {
			writeTestResults("Verify that user able to open history tab", "User should be able to open history tab",
					"User successfully open history tab", "pass");
		} else {
			writeTestResults("Verify that user able to open history tab", "User should be able to open history tab",
					"User couldn't open history tab", "fail");
		}

		explicitWait(drop_documentTypeProductionQC, 50);
		selectText(drop_documentTypeProductionQC, qcTypeProductionOrder);
		Thread.sleep(1000);
		selecteDocType = getSelectedOptionInDropdown(drop_documentTypeProductionQC);
		if (selecteDocType.equals(qcTypeProductionOrder)) {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User able to select 'Production Order' as document type", "pass");
		} else {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User not able to select 'Production Order' as document type", "fail");

		}

		sendKeys(txt_docNumberQC, readProductionData("ProductionOrder_PROD_E2E_001"));
		String enteredPO2 = getAttribute(txt_docNumberQC, "value");
		if (enteredPO2.equals(readProductionData("ProductionOrder_PROD_E2E_001"))) {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User successfully enter Production Order document number", "pass");
		} else {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User doesn't enter Production Order document number", "fail");
		}

		Thread.sleep(1000);
		driver.findElement(getLocator(txt_docNumberQC)).sendKeys(Keys.ARROW_DOWN);
		pressEnter(txt_docNumberQC);

		explicitWait(div_selectedQcDocProductionHistoryTab.replace("doc_no",
				readProductionData("ProductionOrder_PROD_E2E_001")), 30);
		if (isDisplayed(div_selectedQcDocProductionHistoryTab.replace("doc_no",
				readProductionData("ProductionOrder_PROD_E2E_001")))) {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User successfully select the relevent Production Order", "pass");
		} else {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User doesn't select the relevent Production Order", "fail");
		}

		String qcDoc = getText(div_qcDocNo);

		Thread.sleep(2000);
		if (isDisplayed(column_headerFailedQtyHistoryTabQC)) {
			writeTestResults("Verify that fail qty column header is display as earlier",
					"Fail qty column header should be display as earlier",
					"Fail qty column header successfully display as earlier", "pass");
		} else {
			writeTestResults("Verify that fail qty column header is display as earlier",
					"Fail qty column header should be display as earlier",
					"Fail qty column header doesn't display as earlier", "fail");
		}

		String failQuantity = getAttribute(txt_failedQtyQC, "value");
		if (failQuantity.equals("10.00")) {
			writeTestResults("Verify that fail qty is display in history tab", "Fail qty should display in history tab",
					"Fail qty successfully display in history tab", "pass");
		} else {
			writeTestResults("Verify that fail qty is display in history tab", "Fail qty should display in history tab",
					"Fail qty doesn't display in history tab", "fail");
		}

		click(btn_reverseQC);
		explicitWaitUntillClickable(para_successfullValidationProductionQC, 20);

		if (isDisplayed(para_successfullValidationProductionQC)) {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation is appear", "pass");
		} else {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation doesn't appear", "fail");
		}

		waitUntillNotVisible(div_selectedQcDocProduction.replace("doc_no", qcDoc), 15);
		if (!isDisplayedQuickCheck(div_selectedQcDocProduction.replace("doc_no", qcDoc))) {
			writeTestResults("Verify that relevent Production Order is withdraw from history tab",
					"Relevent Production Order should be withdraw from history tab",
					"Relevent Production Order successfully withdraw from history tab", "pass");
		} else {
			writeTestResults("Verify that relevent Production Order is withdraw from history tab",
					"Relevent Production Order should be withdraw from history tab",
					"Relevent Production Order still available on history tab", "fail");
		}

		click(tab_pendingQC);
		explicitWaitUntillClickable(tab_pendingSelectedQC, 20);
		if (isDisplayed(tab_pendingSelectedQC)) {
			writeTestResults("Verify that user able to open pending tab", "User should be able to open pending tab",
					"User successfully open pending tab", "pass");
		} else {
			writeTestResults("Verify that user able to open pending tab", "User should be able to open pending tab",
					"User couldn't open pending tab", "fail");
		}

		explicitWait(drop_documentTypeProductionQC, 50);
		selectText(drop_documentTypeProductionQC, qcTypeProductionOrder);
		Thread.sleep(1000);
		selecteDocType = getSelectedOptionInDropdown(drop_documentTypeProductionQC);
		if (selecteDocType.equals(qcTypeProductionOrder)) {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User able to select 'Production Order' as document type", "pass");
		} else {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User not able to select 'Production Order' as document type", "fail");

		}

		sendKeys(txt_docNumberQC, readProductionData("ProductionOrder_PROD_E2E_001"));
		String enteredPO3 = getAttribute(txt_docNumberQC, "value");
		if (enteredPO3.equals(readProductionData("ProductionOrder_PROD_E2E_001"))) {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User successfully enter Production Order document number", "pass");
		} else {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User doesn't enter Production Order document number", "fail");
		}

		Thread.sleep(1000);
		driver.findElement(getLocator(txt_docNumberQC)).sendKeys(Keys.ARROW_DOWN);
		pressEnter(txt_docNumberQC);

		explicitWait(div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_001")),
				30);
		if (isDisplayed(
				div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_001")))) {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User successfully select the relevent Production Order", "pass");
		} else {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User doesn't select the relevent Production Order", "fail");
		}

		Thread.sleep(2500);
		String totalQuantityAfterReverse = getAttribute(txt_totalQtyQC, "value");
		if (totalQuantityAfterReverse.equals("100.00")) {
			writeTestResults("Verify that total qty is display by adding reverse qty",
					"Total qty should display by adding reverse qty",
					"Total qty successfully display by adding reverse qty", "pass");
		} else {
			writeTestResults("Verify that total qty is display by adding reverse qty",
					"Total qty should display by adding reverse qty", "Total qty doesn't display by adding reverse qty",
					"fail");
		}

		/* Second QC Scenario */
		mouseMove(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")));
		click(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")));

		if (isSelected(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")))) {
			writeTestResults("Verify user able to select the relevent Output Product",
					"User should be able to select the relevent Output Product",
					"User successfully select the relevent Output Product", "pass");
		} else {
			writeTestResults("Verify user able to select the relevent Output Product",
					"User should be able to select the relevent Output Product",
					"User doesn't select the relevent Output Product", "fail");
		}

		Thread.sleep(1500);
		sendKeys(txt_failQuantityProductionQCOneRow, secondFailQty);
		String enteredFailQty2 = getAttribute(txt_failQuantityProductionQCOneRow, "value");
		if (enteredFailQty2.equals(secondFailQty)) {
			writeTestResults("Verify user able to enter fail qty (100)", "User should be able to enter fail qty (100)",
					"User successfully enter fail qty (100)", "pass");
		} else {
			writeTestResults("Verify user able to enter fail qty (100)", "User should be able to enter fail qty (100)",
					"User doesn't enter fail qty (100)", "fail");
		}

		click(btn_releseProductionQC);
		explicitWaitUntillPresent(para_successfullValidationProductionQC, 20);

		if (isElementPresent(para_successfullValidationProductionQC)) {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation is appear", "pass");
		} else {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation doesn't appear", "fail");
		}

		Thread.sleep(2000);
		waitUntillNotVisible(
				div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_001")), 15);
		if (!isDisplayedQuickCheck(
				div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_001")))) {
			writeTestResults("Verify that relevent Production Order is withdraw from pending tab",
					"Relevent Production Order should be withdraw from pending tab",
					"Relevent Production Order successfully withdraw from pending tab", "pass");
		} else {
			writeTestResults("Verify that relevent Production Order is withdraw from pending tab",
					"Relevent Production Order should be withdraw from pending tab",
					"Relevent Production Order still available on pending tab", "fail");
		}

		click(tab_historyQC);
		explicitWaitUntillClickable(tab_historySelectedQC, 20);
		if (isDisplayed(tab_historySelectedQC)) {
			writeTestResults("Verify that user able to open history tab", "User should be able to open history tab",
					"User successfully open history tab", "pass");
		} else {
			writeTestResults("Verify that user able to open history tab", "User should be able to open history tab",
					"User couldn't open history tab", "fail");
		}

		explicitWait(drop_documentTypeProductionQC, 50);
		selectText(drop_documentTypeProductionQC, qcTypeProductionOrder);
		Thread.sleep(1000);
		selecteDocType = getSelectedOptionInDropdown(drop_documentTypeProductionQC);
		if (selecteDocType.equals(qcTypeProductionOrder)) {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User able to select 'Production Order' as document type", "pass");
		} else {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User not able to select 'Production Order' as document type", "fail");

		}

		sendKeys(txt_docNumberQC, readProductionData("ProductionOrder_PROD_E2E_001"));
		String enteredPO4 = getAttribute(txt_docNumberQC, "value");
		if (enteredPO4.equals(readProductionData("ProductionOrder_PROD_E2E_001"))) {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User successfully enter Production Order document number", "pass");
		} else {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User doesn't enter Production Order document number", "fail");
		}

		Thread.sleep(1000);
		driver.findElement(getLocator(txt_docNumberQC)).sendKeys(Keys.ARROW_DOWN);
		pressEnter(txt_docNumberQC);

		explicitWait(div_selectedQcDocProductionHistoryTab.replace("doc_no",
				readProductionData("ProductionOrder_PROD_E2E_001")), 30);
		if (isDisplayed(div_selectedQcDocProductionHistoryTab.replace("doc_no",
				readProductionData("ProductionOrder_PROD_E2E_001")))) {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User successfully select the relevent Production Order", "pass");
		} else {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User doesn't select the relevent Production Order", "fail");
		}

		String qcDoc2 = getText(div_qcDocNo);

		Thread.sleep(2000);
		if (isDisplayed(column_headerFailedQtyHistoryTabQC)) {
			writeTestResults("Verify that fail qty column header is display as earlier",
					"Fail qty column header should be display as earlier",
					"Fail qty column header successfully display as earlier", "pass");
		} else {
			writeTestResults("Verify that fail qty column header is display as earlier",
					"Fail qty column header should be display as earlier",
					"Fail qty column header doesn't display as earlier", "fail");
		}

		String failQuantity2 = getAttribute(txt_failedQtyQC, "value");
		if (failQuantity2.equals("100.00")) {
			writeTestResults("Verify that fail qty is display in history tab", "Fail qty should display in history tab",
					"Fail qty successfully display in history tab", "pass");
		} else {
			writeTestResults("Verify that fail qty is display in history tab", "Fail qty should display in history tab",
					"Fail qty doesn't display in history tab", "fail");
		}

		click(btn_reverseQC);
		explicitWaitUntillClickable(para_successfullValidationProductionQC, 20);

		if (isDisplayed(para_successfullValidationProductionQC)) {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation is appear", "pass");
		} else {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation doesn't appear", "fail");
		}

		waitUntillNotVisible(div_selectedQcDocProduction.replace("doc_no", qcDoc2), 15);
		if (!isDisplayedQuickCheck(div_selectedQcDocProduction.replace("doc_no", qcDoc2))) {
			writeTestResults("Verify that relevent Production Order is withdraw from history tab",
					"Relevent Production Order should be withdraw from history tab",
					"Relevent Production Order successfully withdraw from history tab", "pass");
		} else {
			writeTestResults("Verify that relevent Production Order is withdraw from history tab",
					"Relevent Production Order should be withdraw from history tab",
					"Relevent Production Order still available on history tab", "fail");
		}

		click(tab_pendingQC);
		explicitWaitUntillClickable(tab_pendingSelectedQC, 20);
		if (isDisplayed(tab_pendingSelectedQC)) {
			writeTestResults("Verify that user able to open pending tab", "User should be able to open pending tab",
					"User successfully open pending tab", "pass");
		} else {
			writeTestResults("Verify that user able to open pending tab", "User should be able to open pending tab",
					"User couldn't open pending tab", "fail");
		}

		explicitWait(drop_documentTypeProductionQC, 50);
		selectText(drop_documentTypeProductionQC, qcTypeProductionOrder);
		Thread.sleep(1000);
		selecteDocType = getSelectedOptionInDropdown(drop_documentTypeProductionQC);
		if (selecteDocType.equals(qcTypeProductionOrder)) {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User able to select 'Production Order' as document type", "pass");
		} else {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User not able to select 'Production Order' as document type", "fail");

		}

		sendKeys(txt_docNumberQC, readProductionData("ProductionOrder_PROD_E2E_001"));
		String enteredPO5 = getAttribute(txt_docNumberQC, "value");
		if (enteredPO5.equals(readProductionData("ProductionOrder_PROD_E2E_001"))) {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User successfully enter Production Order document number", "pass");
		} else {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User doesn't enter Production Order document number", "fail");
		}

		Thread.sleep(1000);
		driver.findElement(getLocator(txt_docNumberQC)).sendKeys(Keys.ARROW_DOWN);
		pressEnter(txt_docNumberQC);

		explicitWait(div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_001")),
				30);
		if (isDisplayed(
				div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_001")))) {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User successfully select the relevent Production Order", "pass");
		} else {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User doesn't select the relevent Production Order", "fail");
		}

		Thread.sleep(2500);
		String totalQuantityAfterReverse2 = getAttribute(txt_totalQtyQC, "value");
		if (totalQuantityAfterReverse2.equals("100.00")) {
			writeTestResults("Verify that total qty is display after reverse the toatal qty",
					"Total qty should display after reverse the toatal qty",
					"Total qty successfully display after reverse the toatal qty", "pass");
		} else {
			writeTestResults("Verify that total qty is display after reverse the toatal qty",
					"Total qty should display after reverse the toatal qty",
					"Total qty doesn't display after reverse the toatal qty", "fail");
		}

		/* Third QC Scenario */
		mouseMove(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")));
		click(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")));

		if (isSelected(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")))) {
			writeTestResults("Verify user able to select the relevent Output Product",
					"User should be able to select the relevent Output Product",
					"User successfully select the relevent Output Product", "pass");
		} else {
			writeTestResults("Verify user able to select the relevent Output Product",
					"User should be able to select the relevent Output Product",
					"User doesn't select the relevent Output Product", "fail");
		}

		Thread.sleep(1500);
		sendKeys(txt_passQuantityProductionQCOneRow, thirdPassQty);
		String enteredPassQty = getAttribute(txt_passQuantityProductionQCOneRow, "value");
		if (enteredPassQty.equals(thirdPassQty)) {
			writeTestResults("Verify user able to enter pass qty (70)", "User should be able to enter pass qty (70)",
					"User successfully enter pass qty (70)", "pass");
		} else {
			writeTestResults("Verify user able to enter pass qty (70)", "User should be able to enter pass qty (70)",
					"User doesn't enter pass qty (70)", "fail");
		}
		Thread.sleep(1000);

		sendKeys(txt_failQuantityProductionQCOneRow, thirdFailQty);
		String enteredFailQty3 = getAttribute(txt_failQuantityProductionQCOneRow, "value");
		if (enteredFailQty3.equals(thirdFailQty)) {
			writeTestResults("Verify user able to enter fail qty (30)", "User should be able to enter fail qty (30)",
					"User successfully enter fail qty (30)", "pass");
		} else {
			writeTestResults("Verify user able to enter fail qty (30)", "User should be able to enter fail qty (30)",
					"User doesn't enter fail qty (30)", "fail");
		}

		click(btn_releseProductionQC);
		explicitWaitUntillPresent(para_successfullValidationProductionQC, 20);

		if (isElementPresent(para_successfullValidationProductionQC)) {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation is appear", "pass");
		} else {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation doesn't appear", "fail");
		}

		Thread.sleep(2000);
		waitUntillNotVisible(
				div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_001")), 15);
		if (!isDisplayedQuickCheck(
				div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_001")))) {
			writeTestResults("Verify that relevent Production Order is withdraw from pending tab",
					"Relevent Production Order should be withdraw from pending tab",
					"Relevent Production Order successfully withdraw from pending tab", "pass");
		} else {
			writeTestResults("Verify that relevent Production Order is withdraw from pending tab",
					"Relevent Production Order should be withdraw from pending tab",
					"Relevent Production Order still available on pending tab", "fail");
		}

		click(tab_historyQC);
		explicitWaitUntillClickable(tab_historySelectedQC, 20);
		if (isDisplayed(tab_historySelectedQC)) {
			writeTestResults("Verify that user able to open history tab", "User should be able to open history tab",
					"User successfully open history tab", "pass");
		} else {
			writeTestResults("Verify that user able to open history tab", "User should be able to open history tab",
					"User couldn't open history tab", "fail");
		}

		explicitWait(drop_documentTypeProductionQC, 50);
		selectText(drop_documentTypeProductionQC, qcTypeProductionOrder);
		Thread.sleep(1000);
		selecteDocType = getSelectedOptionInDropdown(drop_documentTypeProductionQC);
		if (selecteDocType.equals(qcTypeProductionOrder)) {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User able to select 'Production Order' as document type", "pass");
		} else {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User not able to select 'Production Order' as document type", "fail");

		}

		sendKeys(txt_docNumberQC, readProductionData("ProductionOrder_PROD_E2E_001"));
		String enteredPO6 = getAttribute(txt_docNumberQC, "value");
		if (enteredPO6.equals(readProductionData("ProductionOrder_PROD_E2E_001"))) {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User successfully enter Production Order document number", "pass");
		} else {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User doesn't enter Production Order document number", "fail");
		}

		Thread.sleep(1000);
		driver.findElement(getLocator(txt_docNumberQC)).sendKeys(Keys.ARROW_DOWN);
		pressEnter(txt_docNumberQC);

		explicitWait(div_selectedQcDocProductionHistoryTab.replace("doc_no",
				readProductionData("ProductionOrder_PROD_E2E_001")), 30);
		if (isDisplayed(div_selectedQcDocProductionHistoryTab.replace("doc_no",
				readProductionData("ProductionOrder_PROD_E2E_001")))) {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User successfully select the relevent Production Order", "pass");
		} else {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User doesn't select the relevent Production Order", "fail");
		}

		Thread.sleep(2000);
		if (isDisplayed(column_headerPassedQtyHistoryTabQC)) {
			writeTestResults("Verify that pass qty column header is display as earlier",
					"Pass qty column header should be display as earlier",
					"Pass qty column header successfully display as earlier", "pass");
		} else {
			writeTestResults("Verify that pass qty column header is display as earlier",
					"Pass qty column header should be display as earlier",
					"Pass qty column header doesn't display as earlier", "fail");
		}

		String passQuantity = getAttribute(txt_passedQtyQC, "value");
		if (passQuantity.equals("70.00")) {
			writeTestResults("Verify that pass qty is display in history tab", "Pass qty should display in history tab",
					"Pass qty successfully display in history tab", "pass");
		} else {
			writeTestResults("Verify that pass qty is display in history tab", "Pass qty should display in history tab",
					"Pass qty doesn't display in history tab", "fail");
		}

		Thread.sleep(1000);
		if (isDisplayed(column_headerFailedQtyHistoryTabQC)) {
			writeTestResults("Verify that fail qty column header is display as earlier",
					"Fail qty column header should be display as earlier",
					"Fail qty column header successfully display as earlier", "pass");
		} else {
			writeTestResults("Verify that fail qty column header is display as earlier",
					"Fail qty column header should be display as earlier",
					"Fail qty column header doesn't display as earlier", "fail");
		}

		String failQuantity3 = getAttribute(txt_failedQtyQC, "value");
		if (failQuantity3.equals("30.00")) {
			writeTestResults("Verify that fail qty is display in history tab", "Fail qty should display in history tab",
					"Fail qty successfully display in history tab", "pass");
		} else {
			writeTestResults("Verify that fail qty is display in history tab", "Fail qty should display in history tab",
					"Fail qty doesn't display in history tab", "fail");
		}
	}

	/* Costing */
	public void productionCosting() throws Exception {
		openPage(readProductionData("ReleaseedPO_PRD_E2E_001"));
		explicitWaitUntillClickable(btn_action2, 50);
		click(btn_action2);

		explicitWaitUntillClickable(btn_productionCostingActions, 10);
		if (isDisplayed(btn_productionCostingActions)) {
			writeTestResults("Verify that Production Costing option is available under Actions",
					"Production Costing option should be available under Actions",
					"Production Costing option successfully available under Actions", "pass");
		} else {
			writeTestResults("Verify that Production Costing option is available under Actions",
					"Production Costing option should be available under Actions",
					"Production Costing option doesn't available under Actions", "fail");
		}
		click(btn_productionCostingActions);

		explicitWait(header_productionCostingWindow, 20);
		if (isDisplayed(header_productionCostingWindow)) {
			writeTestResults("Verify that production costing window is open",
					"Production costing window should be open", "Production costing window succeessfully open", "pass");
		} else {
			writeTestResults("Verify that production costing window is open",
					"Production costing window should be open", "Production costing window doesn't open", "fail");
		}

		explicitWaitUntillClickable(chk_pendingProductionCosting, 20);
		click(chk_pendingProductionCosting);

		if (isDisplayed(chk_pendingProductionCostingSelected)) {
			writeTestResults("Verify user able to select batch costing checkbox on production costing main window",
					"User should be able to select batch costing checkbox on production costing main window",
					"User able to select batch costing checkbox on production costing main window", "pass");
		} else {
			writeTestResults("Verify user able to select batch costing checkbox on production costing main window",
					"User should be able to select batch costing checkbox on production costing main window",
					"User not able to select batch costing checkbox on production costing main window", "fail");
		}

		click(btn_updateProductionCostingMainWindow);

		boolean visibilityFlag = false;
		for (int i = 1; i < 10; i++) {
			try {
				Thread.sleep(20);
				driver.findElement(getLocator(para_batchCostingUpdateValidation)).isDisplayed();
				visibilityFlag = true;
				break;
			} catch (Exception e) {
				continue;
			}
		}

		if (visibilityFlag) {
			writeTestResults("Verify that succesfull validation is appear for Batch Costing",
					"Succesfull validation should appear for Batch Costing",
					"Succesfull validation is appear for Batch Costing", "pass");
		} else {
			writeTestResults("Verify that succesfull validation is appear for Batch Costing",
					"Succesfull validation should appear for Batch Costing",
					"Succesfull validation doesn't appear for Batch Costing", "fail");
		}

//		WebElement element = wait.until(new Function<WebDriver, WebElement>() {
//			public WebElement apply(WebDriver driver) {
//				boolean visibilityOfValidation = false;
//				WebElement linkElement = driver.findElement(By.xpath(para_batchCostingUpdateValidation));
//				if (linkElement.isDisplayed()) {
//					visibilityOfValidation = true;
//				}
//
//				try {
//					if (visibilityOfValidation) {
//						writeTestResults("Verify that succesfull validation is appear for Batch Costing",
//								"Succesfull validation should appear for Batch Costing",
//								"Succesfull validation is appear for Batch Costing", "pass");
//					} else {
//						writeTestResults("Verify that succesfull validation is appear for Batch Costing",
//								"Succesfull validation should appear for Batch Costing",
//								"Succesfull validation doesn't appear for Batch Costing", "fail");
//					}
//
//				} catch (Exception e) {
//
//				}
//
//				return linkElement;
//
//			}
//		});

//		if (isDisplayedQuickCheck(para_batchCostingUpdateValidation)) {
//			writeTestResults("Verify that succesfull validation is appear for Batch Costing",
//					"Succesfull validation should appear for Batch Costing",
//					"Succesfull validation is appear for Batch Costing", "pass");
//		} else {
//			writeTestResults("Verify that succesfull validation is appear for Batch Costing",
//					"Succesfull validation should appear for Batch Costing",
//					"Succesfull validation doesn't appear for Batch Costing", "fail");
//		}

		explicitWait(btn_closeMainCostingUpdateWindowProductionOrder, 5);
		Thread.sleep(2500);
		click(btn_closeMainCostingUpdateWindowProductionOrder);

		explicitWait(header_completedPO, 10);
		if (isDisplayed(header_completedPO)) {
			writeTestResults("Verify that header is changed as Completed", "Header should be change as Completed",
					"Header successfully changed as Completed", "pass");
		} else {
			writeTestResults("Verify that header is changed as Completed", "Header should be change as Completed",
					"Header doesn't changed as Completed", "fail");
		}

		/* Costing Summary - Actual */
		explicitWaitUntillClickable(tab_costingSummaryPO, 60);
		click(tab_costingSummaryPO);
		explicitWaitUntillClickable(btn_expandCostingSummary, 20);

		click(btn_expandCostingSummary);
		Thread.sleep(500);
		explicitWaitUntillClickable(btn_expandCostingSummary, 20);
		click(btn_expandCostingSummary);
		explicitWaitUntillClickable(btn_expandCostingSummary, 20);
		click(btn_expandCostingSummary);
		explicitWaitUntillClickable(btn_expandCostingSummary, 20);
		click(btn_expandCostingSummary);

		if (isDisplayed(column_headerActualCostingSummary)) {
			writeTestResults("Verify that the Actual Column is display same as earlier",
					"The Actual Column should display same as earlier",
					"The Actual Column successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that the Actual Column is display same as earlier",
					"The Actual Column should display same as earlier",
					"The Actual Column doesn't display same as earlier", "fail");
		}

		if (isDisplayed(td_resourceCostActualCostingSummary)) {
			writeTestResults("Verify that the Resource Total Cost is display correctly [Actual Column]",
					"The Resource Total Cost should display correctly [Actual Column]",
					"The Resource Total Cost successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Resource Total Cost is display correctly [Actual Column]",
					"The Resource Total Cost should display correctly [Actual Column]",
					"The Resource Total Cost doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_resourceCost1ActualCostingSummary)) {
			writeTestResults("Verify that the Cost of Resource 01 is display correctly [Actual Column]",
					"The Cost of Resource 01 should display correctly [Actual Column]",
					"The Cost of Resource 01 successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Resource 01 is display correctly [Actual Column]",
					"The Cost of Resource 01 should display correctly [Actual Column]",
					"The Cost of Resource 01 doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_resourceCost2ActualCostingSummary)) {
			writeTestResults("Verify that the Cost of Resource 02 is display correctly [Actual Column]",
					"The Cost of Resource 02 should display correctly [Actual Column]",
					"The Cost of Resource 02 successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Resource 02 is display correctly [Actual Column]",
					"The Cost of Resource 02 should display correctly [Actual Column]",
					"The Cost of Resource 02 doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_materialCostActualCostingSummary)) {
			writeTestResults("Verify that the Material Total Cost is display correctly [Actual Column]",
					"The Material Total Cost should display correctly [Actual Column]",
					"The Material Total Cost successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Material Total Cost is display correctly [Actual Column]",
					"The Material Total Cost should display correctly [Actual Column]",
					"The Material Total Cost doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_material1CostActualCostingSummary)) {
			writeTestResults("Verify that the Cost of Material 01 is display correctly [Actual Column]",
					"The Cost of Material 01 should display correctly [Actual Column]",
					"The Cost of Material 01 successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Material 01 is display correctly [Actual Column]",
					"The Cost of Material 01 should display correctly [Actual Column]",
					"The Cost of Material 01 doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_material2CostActualCostingSummary)) {
			writeTestResults("Verify that the Cost of Material 02 is display correctly [Actual Column]",
					"The Cost of Material 02 should display correctly [Actual Column]",
					"The Cost of Material 02 successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Material 02 is display correctly [Actual Column]",
					"The Cost of Material 02 should display correctly [Actual Column]",
					"The Cost of Material 02 doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_material3CostActualCostingSummary)) {
			writeTestResults("Verify that the Cost of Material 03 is display correctly [Actual Column]",
					"The Cost of Material 03 should display correctly [Actual Column]",
					"The Cost of Material 03 successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Material 03 is display correctly [Actual Column]",
					"The Cost of Material 03 should display correctly [Actual Column]",
					"The Cost of Material 03 doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_overheadCostActualCostingSummary)) {
			writeTestResults("Verify that the Overhead Total Cost is display correctly [Actual Column]",
					"The Overhead Total Cost should display correctly [Actual Column]",
					"The Overhead Total Cost successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Overhead Total Cost is display correctly [Actual Column]",
					"The Overhead Total Cost should display correctly [Actual Column]",
					"The Overhead Total Cost doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_overhead1CostActualCostingSummary)) {
			writeTestResults("Verify that the Cost of Overhead 01 is display correctly [Actual Column]",
					"The Cost of Overhead 01 should display correctly [Actual Column]",
					"The Cost of Overhead 01 successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead 01 is display correctly [Actual Column]",
					"The Cost of Overhead 01 should display correctly [Actual Column]",
					"The Cost of Overhead 01 doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_overhead2CostActualCostingSummary)) {
			writeTestResults("Verify that the Cost of Overhead 02 is display correctly [Actual Column]",
					"The Cost of Overhead 02 should display correctly [Actual Column]",
					"The Cost of Overhead 02 successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead 02 is display correctly [Actual Column]",
					"The Cost of Overhead 02 should display correctly [Actual Column]",
					"The Cost of Overhead 02 doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_overhead3CostActualCostingSummary)) {
			writeTestResults("Verify that the Cost of Overhead 03 is display correctly [Actual Column]",
					"The Cost of Overhead 03 should display correctly [Actual Column]",
					"The Cost of Overhead 03 successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead 03 is display correctly [Actual Column]",
					"The Cost of Overhead 03 should display correctly [Actual Column]",
					"The Cost of Overhead 03 doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_totalCostActualCostingSummary)) {
			writeTestResults("Verify that the Total Cost is display correctly [Actual Column]",
					"The Total Cost should display correctly [Actual Column]",
					"The Total Cost successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Total Cost is display correctly [Actual Column]",
					"The Total Cost should display correctly [Actual Column]",
					"The Total Cost doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_unitCostActualCostingSummary)) {
			writeTestResults("Verify that the Unit Cost is display correctly [Actual Column]",
					"The Unit Cost should display correctly [Actual Column]",
					"The Unit Cost successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Unit Cost is display correctly [Actual Column]",
					"The Unit Cost should display correctly [Actual Column]",
					"The Unit Cost doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_unitCostResourseActualCostingSummary)) {
			writeTestResults("Verify that the Cost of Resource (Unit Cost) is display correctly [Actual Column]",
					"The Cost of Resource (Unit Cost) should display correctly [Actual Column]",
					"The Cost of Resource (Unit Cost) successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Resource (Unit Cost) is display correctly [Actual Column]",
					"The Cost of Resource (Unit Cost) should display correctly [Actual Column]",
					"The Cost of Resource (Unit Cost) doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_unitCostMateraialActualCostingSummary)) {
			writeTestResults("Verify that the Cost of Material (Unit Cost) is display correctly [Actual Column]",
					"The Cost of Material (Unit Cost) should display correctly [Actual Column]",
					"The Cost of Material (Unit Cost) successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Material (Unit Cost) is display correctly [Actual Column]",
					"The Cost of Material (Unit Cost) should display correctly [Actual Column]",
					"The Cost of Material (Unit Cost) doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_unitCostOverheadActualCostingSummary)) {
			writeTestResults("Verify that the Cost of Overhead (Unit Cost) is display correctly [Actual Column]",
					"The Cost of Overhead (Unit Cost) should display correctly [Actual Column]",
					"The Cost of Overhead (Unit Cost) successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead (Unit Cost) is display correctly [Actual Column]",
					"The Cost of Overhead (Unit Cost) should display correctly [Actual Column]",
					"The Cost of Overhead (Unit Cost) doesn't display correctly [Actual Column]", "fail");
		}

		/* Costing Summary - Base Amount */
		if (isDisplayed(column_headerBaseAmountCostingSummary)) {
			writeTestResults("Verify that the Base Amount Column is display same as earlier",
					"The Base Amount Column should display same as earlier",
					"The Base Amount Column successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that the Base Amount Column is display same as earlier",
					"The Base Amount Column should display same as earlier",
					"The Base Amount Column doesn't display same as earlier", "fail");
		}

		if (isDisplayed(td_resourceCostBaseAmountCostingSummary)) {
			writeTestResults("Verify that the Resource Total Cost is display correctly [Base Amount Column]",
					"The Resource Total Cost should display correctly [Base Amount Column]",
					"The Resource Total Cost successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Resource Total Cost is display correctly [Base Amount Column]",
					"The Resource Total Cost should display correctly [Base Amount Column]",
					"The Resource Total Cost doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_resourceCost1BaseAmountCostingSummary)) {
			writeTestResults("Verify that the Cost of Resource 01 is display correctly [Base Amount Column]",
					"The Cost of Resource 01 should display correctly [Base Amount Column]",
					"The Cost of Resource 01 successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Resource 01 is display correctly [Base Amount Column]",
					"The Cost of Resource 01 should display correctly [Base Amount Column]",
					"The Cost of Resource 01 doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_resourceCost2BaseAmountCostingSummary)) {
			writeTestResults("Verify that the Cost of Resource 02 is display correctly [Base Amount Column]",
					"The Cost of Resource 02 should display correctly [Base Amount Column]",
					"The Cost of Resource 02 successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Resource 02 is display correctly [Base Amount Column]",
					"The Cost of Resource 02 should display correctly [Base Amount Column]",
					"The Cost of Resource 02 doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_materialCostBaseAmountCostingSummary)) {
			writeTestResults("Verify that the Material Total Cost is display correctly [Base Amount Column]",
					"The Material Total Cost should display correctly [Base Amount Column]",
					"The Material Total Cost successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Material Total Cost is display correctly [Base Amount Column]",
					"The Material Total Cost should display correctly [Base Amount Column]",
					"The Material Total Cost doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_material1CostBaseAmountCostingSummary)) {
			writeTestResults("Verify that the Cost of Material 01 is display correctly [Base Amount Column]",
					"The Cost of Material 01 should display correctly [Base Amount Column]",
					"The Cost of Material 01 successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Material 01 is display correctly [Base Amount Column]",
					"The Cost of Material 01 should display correctly [Base Amount Column]",
					"The Cost of Material 01 doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_material2CostBaseAmountCostingSummary)) {
			writeTestResults("Verify that the Cost of Material 02 is display correctly [Base Amount Column]",
					"The Cost of Material 02 should display correctly [Base Amount Column]",
					"The Cost of Material 02 successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Material 02 is display correctly [Base Amount Column]",
					"The Cost of Material 02 should display correctly [Base Amount Column]",
					"The Cost of Material 02 doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_material3CostBaseAmountCostingSummary)) {
			writeTestResults("Verify that the Cost of Material 03 is display correctly [Base Amount Column]",
					"The Cost of Material 03 should display correctly [Base Amount Column]",
					"The Cost of Material 03 successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Material 03 is display correctly [Base Amount Column]",
					"The Cost of Material 03 should display correctly [Base Amount Column]",
					"The Cost of Material 03 doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_overheadCostBaseAmountCostingSummary)) {
			writeTestResults("Verify that the Overhead Total Cost is display correctly [Base Amount Column]",
					"The Overhead Total Cost should display correctly [Base Amount Column]",
					"The Overhead Total Cost successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Overhead Total Cost is display correctly [Base Amount Column]",
					"The Overhead Total Cost should display correctly [Base Amount Column]",
					"The Overhead Total Cost doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_overhead1CostBaseAmountCostingSummary)) {
			writeTestResults("Verify that the Cost of Overhead 01 is display correctly [Base Amount Column]",
					"The Cost of Overhead 01 should display correctly [Base Amount Column]",
					"The Cost of Overhead 01 successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead 01 is display correctly [Base Amount Column]",
					"The Cost of Overhead 01 should display correctly [Base Amount Column]",
					"The Cost of Overhead 01 doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_overhead2CostBaseAmountCostingSummary)) {
			writeTestResults("Verify that the Cost of Overhead 02 is display correctly [Base Amount Column]",
					"The Cost of Overhead 02 should display correctly [Base Amount Column]",
					"The Cost of Overhead 02 successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead 02 is display correctly [Base Amount Column]",
					"The Cost of Overhead 02 should display correctly [Base Amount Column]",
					"The Cost of Overhead 02 doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_overhead3CostBaseAmountCostingSummary)) {
			writeTestResults("Verify that the Cost of Overhead 03 is display correctly [Base Amount Column]",
					"The Cost of Overhead 03 should display correctly [Base Amount Column]",
					"The Cost of Overhead 03 successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead 03 is display correctly [Base Amount Column]",
					"The Cost of Overhead 03 should display correctly [Base Amount Column]",
					"The Cost of Overhead 03 doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_totalCostBaseAmountCostingSummary)) {
			writeTestResults("Verify that the Total Cost is display correctly [Base Amount Column]",
					"The Total Cost should display correctly [Base Amount Column]",
					"The Total Cost successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Total Cost is display correctly [Base Amount Column]",
					"The Total Cost should display correctly [Base Amount Column]",
					"The Total Cost doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_unitCostBaseAmountCostingSummary)) {
			writeTestResults("Verify that the Unit Cost is display correctly [Base Amount Column]",
					"The Unit Cost should display correctly [Base Amount Column]",
					"The Unit Cost successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Unit Cost is display correctly [Base Amount Column]",
					"The Unit Cost should display correctly [Base Amount Column]",
					"The Unit Cost doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_unitCostResourseBaseAmountCostingSummary)) {
			writeTestResults("Verify that the Cost of Resource (Unit Cost) is display correctly [Base Amount Column]",
					"The Cost of Resource (Unit Cost) should display correctly [Base Amount Column]",
					"The Cost of Resource (Unit Cost) successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Resource (Unit Cost) is display correctly [Base Amount Column]",
					"The Cost of Resource (Unit Cost) should display correctly [Base Amount Column]",
					"The Cost of Resource (Unit Cost) doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_unitCostMateraialBaseAmountCostingSummary)) {
			writeTestResults("Verify that the Cost of Material (Unit Cost) is display correctly [Base Amount Column]",
					"The Cost of Material (Unit Cost) should display correctly [Base Amount Column]",
					"The Cost of Material (Unit Cost) successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Material (Unit Cost) is display correctly [Base Amount Column]",
					"The Cost of Material (Unit Cost) should display correctly [Base Amount Column]",
					"The Cost of Material (Unit Cost) doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_unitCostOverheadBaseAmountCostingSummary)) {
			writeTestResults("Verify that the Cost of Overhead (Unit Cost) is display correctly [Base Amount Column]",
					"The Cost of Overhead (Unit Cost) should display correctly [Base Amount Column]",
					"The Cost of Overhead (Unit Cost) successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead (Unit Cost) is display correctly [Base Amount Column]",
					"The Cost of Overhead (Unit Cost) should display correctly [Base Amount Column]",
					"The Cost of Overhead (Unit Cost) doesn't display correctly [Base Amount Column]", "fail");
		}
	}

	/* Generate Internal Receipt */
	public void internalReciptAndJournelEntry() throws Exception {
		explicitWaitUntillClickable(btn_action2, 50);
		click(btn_action2);

		explicitWaitUntillClickable(btn_convertToInternalReceipt, 10);
		if (isDisplayed(btn_convertToInternalReceipt)) {
			writeTestResults("Verify that Convert to Internel Recipt option is available under Actions",
					"Convert to Internel Recipt option should be available under Actions",
					"Convert to Internel Recipt option successfully available under Actions", "pass");
		} else {
			writeTestResults("Verify that Convert to Internel Recipt option is available under Actions",
					"Convert to Internel Recipt option should be available under Actions",
					"Convert to Internel Recipt option doesn't available under Actions", "fail");
		}
		click(btn_convertToInternalReceipt);

		explicitWait(header_genarateInternalReciptSmoke, 20);
		if (isDisplayed(header_genarateInternalReciptSmoke)) {
			writeTestResults("Verify that genarate internel receipt window is open",
					"genarate internel receipt window should be open",
					"genarate internel receipt window succeessfully open", "pass");
		} else {
			writeTestResults("Verify that genarate internel receipt window is open",
					"genarate internel receipt window should be open", "genarate internel receipt window doesn't open",
					"fail");
		}

		click(chk_seletcOroductForInternalRecipt);

		if (isSelected(chk_seletcOroductForInternalRecipt)) {
			writeTestResults("Verify user able to select the product line",
					"User should be able to select the product line", "User successfully select the product line",
					"pass");
		} else {
			writeTestResults("Verify user able to select the product line",
					"User should be able to select the product line", "User doesn't select the product line", "fail");
		}

		String enteredReceiptDate = getAttribute(txt_reciptDateGenarateInternalREceiptWindow, "value");
		click(btn_genarateInternalRecipt);
		explicitWait(para_validationInternalREciptGenarateSuccessfull, 10);
		if (isDisplayed(para_validationInternalREciptGenarateSuccessfull)) {
			writeTestResults("Verify that succesfull validation is appear for Internal Receipt Genaration",
					"Succesfull validation should appear for Internal Receipt Genaration",
					"Succesfull validation is appear for Internal Receipt Genaration", "pass");
		} else {
			writeTestResults("Verify that succesfull validation is appear for Internal Receipt Genaration",
					"Succesfull validation should appear for Internal Receipt Genaration",
					"Succesfull validation doesn't appear for Internal Receipt Genaration", "fail");
		}

		Thread.sleep(3500);
		explicitWaitUntillClickable(lnk_genaratedInternalREciptSmoke, 15);
		Thread.sleep(1500);
		click(lnk_genaratedInternalREciptSmoke);
		Thread.sleep(2000);
		switchWindow();
		Thread.sleep(1000);
		explicitWait(header_releasedInternalRecipt, 10);
		trackCode = getText(lbl_docNumber);
		if (isDisplayed(header_releasedInternalRecipt)) {
			writeTestResults("Verify that Internal Receiptis open as released status",
					"Internal Receipt should be open as released status",
					"Internal Receipt successfully open as released status", "pass");
		} else {
			writeTestResults("Verify that Internal Receiptis open as released status",
					"Internal Receipt should be open as released status",
					"Internal Receipt doesn't open or not in released status", "fail");
		}

		String docDate = getText(a_documentDate);
		String postDate = getText(a_postDate);
		String dueDate = getText(a_dueDate);

		if (docDate.equals(enteredReceiptDate) && docDate.equals(postDate) && docDate.equals(dueDate)) {
			writeTestResults(
					"Verify that all three header dates are equal to the date that entered when generating Internal Receipt",
					"All three header dates should be equal to the date that entered when generating Internal Receipt",
					"All three header dates are equal to the date that entered when generating Internal Receipt",
					"pass");
		} else {
			writeTestResults(
					"Verify that all three header dates are equal to the date that entered when generating Internal Receipt",
					"All three header dates should be equal to the date that entered when generating Internal Receipt",
					"All three header dates are not equal to the date that entered when generating Internal Receipt",
					"fail");
		}

		String warehouseOnInternalReceipt = getText(div_warehouseInternalRecipt);
		if (warehouseOnInternalReceipt.equals(outputWarehouseLI)) {
			writeTestResults(
					"Verify that Output Warehouse of Production Order and Warehouse of Internal Receipt is same",
					"Output Warehouse of Production Order and Warehouse of Internal Receipt should be same",
					"Output Warehouse of Production Order and Warehouse of Internal Receipt is same", "pass");
		} else {
			writeTestResults(
					"Verify that Output Warehouse of Production Order and Warehouse of Internal Receipt is same",
					"Output Warehouse of Production Order and Warehouse of Internal Receipt should be same",
					"Output Warehouse of Production Order and Warehouse of Internal Receipt is not same", "fail");
		}

		String qtyInternalReceipt = getText(div_planedQtyInternalRecipt);
		if (qtyInternalReceipt.equals("70.00")) {
			writeTestResults("Verify that quantity is equel to the QC pass quantity",
					"Quantity should be equel to the QC pass quantity",
					"Quantity successfully equel to the QC pass quantity", "pass");
		} else {
			writeTestResults("Verify that quantity is equel to the QC pass quantity",
					"Quantity should be equel to the QC pass quantity",
					"Quantity successfully not equel to the QC pass quantity", "fail");
		}

		click(btn_action2);

		explicitWaitUntillClickable(btn_journal, 10);
		if (isDisplayed(btn_journal)) {
			writeTestResults("Verify that Journal Entry option is available under Actions",
					"Journal Entry option should be available under Actions",
					"Journal Entry option successfully available under Actions", "pass");
		} else {
			writeTestResults("Verify that Journal Entry option is available under Actions",
					"Journal Entry option should be available under Actions",
					"Journal Entry option doesn't available under Actions", "fail");
		}
		click(btn_journal);

		explicitWait(header_journalEntryPopup, 10);
		if (isDisplayed(header_journalEntryPopup)) {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded",
					"Journal Entry details pop up successfully get loaded", "pass");
		} else {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded", "Journal Entry details pop up doesn't get loaded",
					"fail");
		}

		if (isDisplayedQuickCheck(lbl_debitValueJournelEntry)) {
			/* QA */
			String debitAmount = getText(lbl_debitValueJournelEntry);

			if (debitAmount.startsWith("1410.51")) {
				writeTestResults("Verify that amount was debited to the relevent GL Account",
						"Amount should be debited to the relevent GL Account",
						"Amount successfully debited to the relevent GL Account", "pass");
			} else {
				writeTestResults("Verify that amount was debited to the relevent GL Account",
						"Amount should be debited to the relevent GL Account",
						"Amount doesn't debited to the relevent GL Account", "fail");
			}

			String creditAmount = getText(lbl_creditValueJournelEntry);
			if (creditAmount.startsWith("1410.51")) {
				writeTestResults("Verify that amount was credited to the relevent GL Account",
						"Amount should be credited to the relevent GL Account",
						"Amount successfully credited to the relevent GL Account", "pass");
			} else {
				writeTestResults("Verify that amount was credited to the relevent GL Account",
						"Amount should be credited to the relevent GL Account",
						"Amount doesn't credited to the relevent GL Account", "fail");
			}

			String debitTotal = getText(lbl_debitTotalJournelEntrySingleEntry);
			String credTotal = getText(lbl_creditTotalJournelEntrySingleEntry);

			if (debitTotal.startsWith("1410.51") && debitTotal.equals(credTotal)) {
				writeTestResults("Verify total debit and credit amounts were balanced",
						"Total debit and credit amounts should balanced",
						"Total debit and credit amounts successfully balanced", "pass");
			} else {
				writeTestResults("Verify total debit and credit amounts were balanced",
						"Total debit and credit amounts should balanced",
						"Total debit and credit amounts weren't balanced", "fail");
			}

		} else {
			/* Staging */
			String debitAmount = getText(lbl_debitValueJournelEntryStaging).replace(",", "");

			if (debitAmount.startsWith("1410.51")) {
				writeTestResults("Verify that amount was debited to the relevent GL Account",
						"Amount should be debited to the relevent GL Account",
						"Amount successfully debited to the relevent GL Account", "pass");
			} else {
				writeTestResults("Verify that amount was debited to the relevent GL Account",
						"Amount should be debited to the relevent GL Account",
						"Amount doesn't debited to the relevent GL Account", "fail");
			}

			String creditAmount = getText(lbl_creditValueJournelEntryStaging).replace(",", "");
			if (creditAmount.startsWith("1410.51")) {
				writeTestResults("Verify that amount was credited to the relevent GL Account",
						"Amount should be credited to the relevent GL Account",
						"Amount successfully credited to the relevent GL Account", "pass");
			} else {
				writeTestResults("Verify that amount was credited to the relevent GL Account",
						"Amount should be credited to the relevent GL Account",
						"Amount doesn't credited to the relevent GL Account", "fail");
			}

			String debitTotal = getText(lbl_debitTotalJournelEntrySingleEntry);
			String credTotal = getText(lbl_creditTotalJournelEntrySingleEntry);

			if (debitTotal.startsWith("1410.51") && debitTotal.equals(credTotal)) {
				writeTestResults("Verify total debit and credit amounts were balanced",
						"Total debit and credit amounts should balanced",
						"Total debit and credit amounts successfully balanced", "pass");
			} else {
				writeTestResults("Verify total debit and credit amounts were balanced",
						"Total debit and credit amounts should balanced",
						"Total debit and credit amounts weren't balanced", "fail");
			}
		}
	}

	/* PROD_E2E_002 */
	public void billOfMaterial_PROD_E2E_002() throws Exception {
		System.out.println(getCurrentUrl());
		explicitWaitUntillClickable(img_navigationPane, 60);
		Thread.sleep(2000);
		explicitWaitUntillClickable(img_navigationPane, 20);
		if (click(img_navigationPane)) {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User successfully click navigation pane", "pass");
		} else {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User couldn't click navigation pane", "fail");
		}

		explicitWaitUntillClickable(btn_inventory, 10);
		click(btn_inventory);

		explicitWaitUntillClickable(btn_billOfMaterial2, 20);
		click(btn_billOfMaterial2);

		explicitWaitUntillClickable(btn_newBillOfMaterial, 10);
		click(btn_newBillOfMaterial);

		explicitWaitUntillClickable(txt_descriptionBillOfMaterials, 60);
		sendKeys(txt_descriptionBillOfMaterials, descriptionBOM);

		selectText(drop_productGroupBOM, productionGroupE2E);

		Thread.sleep(2000);
		String selectedProductGroup = getSelectedOptionInDropdown(drop_productGroupBOM);
		if (selectedProductGroup.contains(productionGroupE2E)) {
			writeTestResults("Verify that user able to select Product Group",
					"User should be able to select Product Group", "User successfully select Product Group", "pass");
		} else {
			writeTestResults("Verify that user able to select Product Group",
					"User should be able to select Product Group", "User doesn't select Product Group", "fail");

		}

		explicitWaitUntillClickable(btn_lookupOutputProduct, 20);
		click(btn_lookupOutputProduct);
		explicitWaitUntillClickable(txt_productSearch, 40);
		Thread.sleep(1000);
		sendKeysLookup(txt_productSearch, invObj.readTestCreation("BatchFifoMultiPlatformProductionType"));
		pressEnter(txt_productSearch);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")), 20);

		if (isDisplayedQuickCheck(lnk_inforOnLookupInformationReplace.replace("info",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")))) {
			doubleClick(lnk_inforOnLookupInformationReplace.replace("info",
					invObj.readTestCreation("BatchFifoMultiPlatformProductionType")));

			Thread.sleep(1500);

			String selectedProduct = getAttribute(txt_productFrontPageBOM, "value");
			if (selectedProduct.contains(invObj.readTestCreation("BatchFifoMultiPlatformProductionType"))) {
				writeTestResults("Verify that user able to select Production Type Product",
						"User should be able to select Production Type Product",
						"User successfully select Production Type Product", "pass");
			} else {
				writeTestResults("Verify that user able to select Production Type Product",
						"User should be able to select Production Type Product",
						"User doesn't select Production Type Product", "fail");

			}
		} else {
			Thread.sleep(1500);
			((JavascriptExecutor) driver).executeScript("window.open('about:blank','_blank');");
			Thread.sleep(1000);
			switchWindow();
			openPage(siteUrlMultiPlatformm);
			createProducttionTypeProduct();
			closeWindow();
			sendKeysLookup(txt_productSearch, invObj.readTestCreation("BatchFifoMultiPlatformProductionType"));
			pressEnter(txt_productSearch);
			Thread.sleep(2000);
			explicitWait(lnk_inforOnLookupInformationReplace.replace("info",
					invObj.readTestCreation("BatchFifoMultiPlatformProductionType")), 20);
			doubleClick(lnk_inforOnLookupInformationReplace.replace("info",
					invObj.readTestCreation("BatchFifoMultiPlatformProductionType")));

			Thread.sleep(1500);

			String selectedProduct = getAttribute(txt_productFrontPageBOM, "value");
			if (selectedProduct.contains(invObj.readTestCreation("BatchFifoMultiPlatformProductionType"))) {
				writeTestResults("Verify that user able to select Production Type Product",
						"User should be able to select Production Type Product",
						"User successfully select Production Type Product", "pass");
			} else {
				writeTestResults("Verify that user able to select Production Type Product",
						"User should be able to select Production Type Product",
						"User doesn't select Production Type Product", "fail");

			}
		}
		Thread.sleep(1500);
		sendKeys(txt_quantityBOM, hundred);
		String enterdQty = getAttribute(txt_quantityBOM, "value");
		if (enterdQty.equals(hundred)) {
			writeTestResults("Verify that user able to enter the Quantity", "User should be able to enter the Quantity",
					"User successfully enter the Quantity", "pass");
		} else {
			writeTestResults("Verify that user able to enter the Quantity", "User should be able to enter the Quantity",
					"User doesn't enter the Quantity", "fail");

		}

		if (isDisplayed(header_comlumQtyBoMMaterialInfoTable)
				&& isDisplayed(header_comlumFixedQtyBoMMaterialInfoTable)) {
			writeTestResults("Verify that relevant columns on material information grid is display same as earlier",
					"Relevant columns on material information grid should display same as earlier",
					"Relevant columns on the material information grid are display the same as earlier", "pass");
		} else {
			writeTestResults("Verify that relevant columns on material information grid is display same as earlier",
					"Relevant columns on material information grid should display same as earlier",
					"Relevant columns on the material information grid are not display the same as earlier", "fail");

			pageScrollDown();
		}

		mouseMove(header_scrapFixedQtyBoMMaterialInfoTable);
		if (isDisplayed(header_scrapFixedQtyBoMMaterialInfoTable)) {
			writeTestResults("Verify that Scrap % columns on material information grid is display same as earlier",
					"Scrap % columns on material information grid should display same as earlier",
					"Scrap % columns on the material information grid are display the same as earlier", "pass");
		} else {
			writeTestResults("Verify that Scrap % columns on material information grid is display same as earlier",
					"Scrap % columns on material information grid should display same as earlier",
					"Scrap % columns on the material information grid are not display the same as earlier", "fail");

		}

		mouseMove(btn_lookupOnLineBOMRowReplace.replace("row", "1"));
		click(btn_lookupOnLineBOMRowReplace.replace("row", "1"));
		sendKeysLookup(txt_productSearch, invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialOne"));
		pressEnter(txt_productSearch);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info",
				invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialOne")), 15);
		if (isDisplayedQuickCheck(lnk_inforOnLookupInformationReplace.replace("info",
				invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialOne")))) {
			doubleClick(lnk_inforOnLookupInformationReplace.replace("info",
					invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialOne")));
		} else {
			Thread.sleep(1500);
			((JavascriptExecutor) driver).executeScript("window.open('about:blank','_blank');");
			Thread.sleep(1000);
			switchWindow();
			openPage(siteUrlMultiPlatformm);
			createAllowDecimalRowMaterial1();
			createAllowDecimalRowMaterial2();
			createAllowDecimalRowMaterialHasVarientProduct3();
			closeWindow();
			sendKeysLookup(txt_productSearch, invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialOne"));
			pressEnter(txt_productSearch);
			Thread.sleep(2000);
			explicitWait(lnk_inforOnLookupInformationReplace.replace("info",
					invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialOne")), 15);

			doubleClick(lnk_inforOnLookupInformationReplace.replace("info",
					invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialOne")));
		}
		explicitWait(txt_qtyBOMInputProductLinesRowReplace.replace("row", "1"), 10);
		sendKeys(txt_qtyBOMInputProductLinesRowReplace.replace("row", "1"), rowMaterialQtyOne);
		String enteredRowMaterialOneQty = getAttribute(txt_qtyBOMInputProductLinesRowReplace.replace("row", "1"),
				"value");
		if (enteredRowMaterialOneQty.equals(rowMaterialQtyOne)) {
			writeTestResults("Verify that user able to enter the raw material one Qty",
					"User should be able to enter the raw material one Qty",
					"User successfully enter the raw material one Qty", "pass");
		} else {
			writeTestResults("Verify that user able to enter the raw material one Qty",
					"User should be able to enter the raw material one Qty",
					"User doesn't enter the raw material one Qty", "fail");

		}
		click(btn_addNewRecordBoM);

		click(btn_lookupOnLineBOMRowReplace.replace("row", "2"));
		sendKeysLookup(txt_productSearch, invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialTwo"));
		pressEnter(txt_productSearch);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info",
				invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialTwo")), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info",
				invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialTwo")));

		explicitWait(txt_qtyBOMInputProductLinesRowReplace.replace("row", "2"), 10);
		sendKeys(txt_qtyBOMInputProductLinesRowReplace.replace("row", "2"), rowMaterialQtyTwo);
		String enteredRowMaterialTwoQty = getAttribute(txt_qtyBOMInputProductLinesRowReplace.replace("row", "2"),
				"value");
		if (enteredRowMaterialTwoQty.equals(rowMaterialQtyTwo)) {
			writeTestResults("Verify that user able to enter the raw material two Qty",
					"User should be able to enter the raw material two Qty",
					"User successfully enter the raw material two Qty", "pass");
		} else {
			writeTestResults("Verify that user able to enter the raw material two Qty",
					"User should be able to enter the raw material two Qty",
					"User doesn't enter the raw material two Qty", "fail");

		}
		click(chk_fixedQtyRowReplaceBOM.replace("row", "2"));
		if (isSelected(chk_fixedQtyRowReplaceBOM.replace("row", "2"))) {
			writeTestResults("Verify that user able to enable Fixed Qty", "User should be able to enable Fixed Qty",
					"User successfully enable Fixed Qty", "pass");
		} else {
			writeTestResults("Verify that user able to enable Fixed Qty", "User should be able to enable Fixed Qty",
					"User doesn't enable Fixed Qty", "fail");

		}

		click(btn_addNewRecordBoM);

		click(btn_lookupOnLineBOMRowReplace.replace("row", "3"));
		sendKeysLookup(txt_productSearch,
				invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialHasVarientProductThree"));
		pressEnter(txt_productSearch);
		Thread.sleep(2000);
		explicitWait(
				lnk_inforOnLookupInformationReplace.replace("info",
						invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialHasVarientProductThree")),
				20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info",
				invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialHasVarientProductThree")));

		explicitWait(txt_qtyBOMInputProductLinesRowReplace.replace("row", "3"), 10);
		sendKeys(txt_qtyBOMInputProductLinesRowReplace.replace("row", "3"), rowMaterialQtyThree);
		String enteredRowMaterialThreeQty = getAttribute(txt_qtyBOMInputProductLinesRowReplace.replace("row", "3"),
				"value");
		if (enteredRowMaterialThreeQty.equals(rowMaterialQtyThree)) {
			writeTestResults("Verify that user able to enter the raw material three Qty",
					"User should be able to enter the raw material three Qty",
					"User successfully enter the raw material three Qty", "pass");
		} else {
			writeTestResults("Verify that user able to enter the raw material three Qty",
					"User should be able to enter the raw material three Qty",
					"User doesn't enter the raw material three Qty", "fail");

		}

		sendKeys(txt_scrapRowReplaceBOM.replace("row", "3"), scrapRowMaterilThree);
		String enteredRowMaterialThreeScrap = getAttribute(txt_scrapRowReplaceBOM.replace("row", "3"), "value");
		if (enteredRowMaterialThreeScrap.equals(scrapRowMaterilThree)) {
			writeTestResults("Verify that user able to enter the raw material three Scrap",
					"User should be able to enter the raw material three Scrap",
					"User successfully enter the raw material three Scrap", "pass");
		} else {
			writeTestResults("Verify that user able to enter the raw material three Scrap",
					"User should be able to enter the raw material three Scrap",
					"User doesn't enter the raw material three Scrap", "fail");

		}

		click(btn_draft);
		explicitWait(header_draftedBOM, 50);
		if (isDisplayed(header_draftedBOM)) {
			writeTestResults("Verify user able to draft the Bill Of Material",
					"User should be able to draft the Bill Of Material", "User successfully draft the Bill Of Material",
					"pass");
		} else {
			writeTestResults("Verify user able to draft the Bill Of Material",
					"User should be able to draft the Bill Of Material", "User doesn't draft the Bill Of Material",
					"fail");

		}

		click(btn_release);
		explicitWait(header_releasedBillOfMaterial, 50);
		trackCode = getText(lbl_docNo);
		writeProductionData("BOM_PROD_E2E_002", trackCode, 22);
		if (isDisplayed(header_releasedBillOfMaterial)) {
			writeTestResults("Verify user able to release the Bill Of Material",
					"User should be able to release the Bill Of Material",
					"User successfully release the Bill Of Material", "pass");
		} else {
			writeTestResults("Verify user able to release the Bill Of Material",
					"User should be able to release the Bill Of Material", "User doesn't release the Bill Of Material",
					"fail");

		}
	}

	public void billOfOperation_PROD_E2E_002() throws Exception {
		pageRefersh();
		explicitWait(img_navigationPane, 20);
		Thread.sleep(2000);
		explicitWait(img_navigationPane, 20);
		click(img_navigationPane);
		explicitWait(btn_productionModule, 10);
		click(btn_productionModule);
		explicitWait(btn_billOfOperation, 10);
		click(btn_billOfOperation);
		explicitWait(header_BillOfOperationByPage, 40);
		if (isDisplayed(header_BillOfOperationByPage)) {
			writeTestResults("Verify user can navigate to the Bill Of Operation by-page",
					"User should be able to navigate to Bill Of Operation by-page",
					"User successfully navigate to Bill Of Operation by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to the Bill Of Operation by-page",
					"User should be able to navigate to Bill Of Operation by-page",
					"User doesn't navigate to Bill Of Operation by-page", "fail");

		}
		click(btn_newBillOfOperation);
		explicitWait(header_newBillOfOperation, 50);
		if (isDisplayed(header_newBillOfOperation)) {
			writeTestResults("Verify user can navigate to the new Bill Of Operation",
					"User should be able to navigate to new Bill Of Operation",
					"User successfully navigate to new Bill Of Operation", "pass");
		} else {
			writeTestResults("Verify user can navigate to the new Bill Of Operation",
					"User should be able to navigate to new Bill Of Operation",
					"User doesn't navigate to new Bill Of Operation", "fail");
		}

		String bookCodeBOO = invObj.currentTimeAndDate().replace("/", "").replace(":", "");
		sendKeys(txt_booCodeBillOfOperation, bookCodeBOO);

		click(btn_addBillOfOperationRowReplace.replace("row", "1"));

		explicitWait(drop_elementCategoryBOO, 20);
		selectText(drop_elementCategoryBOO, elementCategorySupply);
		Thread.sleep(1000);
		String initialBOOElementCategory = getSelectedOptionInDropdown(drop_elementCategoryBOO);
		if (initialBOOElementCategory.equals(elementCategorySupply)) {
			writeTestResults("Verify user able to select 'Supply' as Element Category for the initial BOO",
					"User should be able to select 'Supply' as Element Category for the initial BOO",
					"User successfully select 'Supply' as Element Category for the initial BOO", "pass");
		} else {
			writeTestResults("Verify user able to select 'Supply' as Element Category for the initial BOO",
					"User should be able to select 'Supply' as Element Category for the initial BOO",
					"User doesn't select 'Supply' as Element Category for the initial BOO", "fail");
		}

		selectText(drop_activityTypeBOO, activityTypeMoveStock);
		Thread.sleep(1000);
		String initialBOOActivityType = getSelectedOptionInDropdown(drop_activityTypeBOO);
		if (initialBOOActivityType.equals(activityTypeMoveStock)) {
			writeTestResults("Verify user able to select 'Move Stock' as Activity Type for the initial BOO",
					"User should be able to select 'Move Stock' as Activity Type for the initial BOO",
					"User successfully select 'Move Stock' as Activity Type for the initial BOO", "pass");
		} else {
			writeTestResults("Verify user able to select 'Move Stock' as Activity Type for the initial BOO",
					"User should be able to select 'Move Stock' as Activity Type for the initial BOO",
					"User doesn't select 'Move Stock' as Activity Type for the initial BOO", "fail");
		}

		sendKeys(txt_elementDescriptionBOO, moveStockElementDescription);
		Thread.sleep(1000);
		String initialBOOEnteredDesc = getAttribute(txt_elementDescriptionBOO, "value");
		if (initialBOOEnteredDesc.equals(moveStockElementDescription)) {
			writeTestResults("Verify user able to enter Element Description for the initial BOO",
					"User should be able to enter Element Description for the initial BOO",
					"User successfully enter Element Description for the initial BOO", "pass");
		} else {
			writeTestResults("Verify user able to enter Element Description for the initial BOO",
					"User should be able to enter Element Description for the initial BOO",
					"User doesn't enter Element Description for the initial BOO", "fail");
		}

		click(btn_applyOperationOneBOO);

		explicitWait(btn_addBillOfOperationRowReplace.replace("row", "2"), 10);
		click(btn_addBillOfOperationRowReplace.replace("row", "2"));

		explicitWait(drop_elementCategoryBOO, 20);
		selectText(drop_elementCategoryBOO, elementCategoryOperation);
		Thread.sleep(1000);
		String firstBOOElementCategory = getSelectedOptionInDropdown(drop_elementCategoryBOO);
		if (firstBOOElementCategory.equals(elementCategoryOperation)) {
			writeTestResults("Verify user able to select 'Operation' as Element Category for the first BOO",
					"User should be able to select 'Operation' as Element Category for the first BOO",
					"User successfully select 'Operation' as Element Category for the first BOO", "pass");
		} else {
			writeTestResults("Verify user able to select 'Operation' as Element Category for the first BOO",
					"User should be able to select 'Operation' as Element Category for the first BOO",
					"User doesn't select 'Operation' as Element Category for the first BOO", "fail");
		}

		Thread.sleep(1000);
		String firstBOOActivityType = getSelectedOptionInDropdown(drop_activityTypeBOO);
		if (firstBOOActivityType.equals(activityTypeProduce)) {
			writeTestResults("Verify auto select 'Produce' as Activity Type for the first BOO",
					"Should auto select 'Produce' as Activity Type for the first BOO",
					"Auto select 'Produce' as Activity Type for the first BOO", "pass");
		} else {
			writeTestResults("Verify auto select 'Produce' as Activity Type for the first BOO",
					"Should auto select 'Produce' as Activity Type for the first BOO",
					"Not auto select 'Produce' as Activity Type for the first BOO", "fail");
		}

		sendKeys(txt_elementDescriptionBOO, produceElementDescription1);
		Thread.sleep(1000);
		String firstBOOEnteredDesc = getAttribute(txt_elementDescriptionBOO, "value");
		if (firstBOOEnteredDesc.equals(produceElementDescription1)) {
			writeTestResults("Verify user able to enter Element Description for the first BOO",
					"User should be able to enter Element Description for the first BOO",
					"User successfully enter Element Description for the first BOO", "pass");
		} else {
			writeTestResults("Verify user able to enter Element Description for the first BOO",
					"User should be able to enter Element Description for the first BOO",
					"User doesn't enter Element Description for the first BOO", "fail");
		}

		sendKeys(txt_groupIdBOO, groupIdBOOOne);
		Thread.sleep(1000);
		String firstBOOEnteredGroupID = getAttribute(txt_groupIdBOO, "value");
		if (firstBOOEnteredGroupID.equals(groupIdBOOOne)) {
			writeTestResults("Verify user able to enter Group ID for the first BOO",
					"User should be able to enter Group ID for the first BOO",
					"User successfully enter Group ID for the first BOO", "pass");
		} else {
			writeTestResults("Verify user able to enter Group ID for the first BOO",
					"User should be able to enter Group ID for the first BOO",
					"User doesn't enter Group ID for the first BOO", "fail");
		}

		click(btn_mainResourseLookuoOperationOne);
		explicitWaitUntillClickable(txt_searchResource, 30);
		Thread.sleep(2000);
		sendKeysLookup(txt_searchResource, readProductionData("ResourceCode01_E2E"));
		pressEnter(txt_searchResource);
		explicitWaitUntillClickable(
				td_resultResourseOperationOne.replace("resource", readProductionData("ResourceCode01_E2E")), 40);
		doubleClick(td_resultResourseOperationOne.replace("resource", readProductionData("ResourceCode01_E2E")));
		Thread.sleep(2000);
		String selectedMainResourse = getAttribute(txt_mainResourseOnLookupOperationOne, "value");
		if (selectedMainResourse.contains(readProductionData("ResourceCode01_E2E"))) {
			writeTestResults("Verify user able to select Main Resource for the first BOO",
					"User should be able to select Main Resource for the first BOO",
					"User successfully select Main Resource for the first BOO", "pass");
		} else {
			writeTestResults("Verify user able to select Main Resource for the first BOO",
					"User should be able to select Main Resource for the first BOO",
					"User doesn't select Main Resource for the first BOO", "fail");
		}

		click(txt_fixedDurationOpeartionOne);
		explicitWaitUntillClickable(a_oneHourFixedDurationOpeationOne, 20);
		click(a_oneHourFixedDurationOpeationOne);
		Thread.sleep(700);
		click(btn_okFixedDurationOpeationOne);
		Thread.sleep(2000);
		String selecteFixedDurationHours = getAttribute(txt_fixedDurationOpeartionOne, "value");
		String selecteFixedDurationMins = getAttribute(txt_fixedDurationMinOpeartionOne, "value");
		if (selecteFixedDurationHours.equals("01") && selecteFixedDurationMins.equals("00")) {
			writeTestResults("Verify user able to select Fixed Duration as 1 for the first BOO",
					"User should be able to select Fixed Duration as 1 for the first BOO",
					"User successfully select Fixed Duration as 1 for the first BOO", "pass");
		} else {
			writeTestResults("Verify user able to select Fixed Duration as 1 for the first BOO",
					"User should be able to select Fixed Duration as 1 for the first BOO",
					"User doesn't select Fixed Duration as 1 for the first BOO", "fail");
		}

		Thread.sleep(1000);
		String firstBOOBatchMethod = getSelectedOptionInDropdown(drop_batchMethodBOO);
		if (firstBOOBatchMethod.equals(batchMethodTransferBOO)) {
			writeTestResults("Verify auto select 'Transfer' as Batch Method for the first BOO",
					"Should auto select 'Transfer' as Batch Method for the first BOO",
					"Auto select 'Transfer' as Batch Method for the first BOO", "pass");
		} else {
			writeTestResults("Verify auto select 'Transfer' as Batch Method for the first BOO",
					"Should auto select 'Transfer' as Batch Method for the first BOO",
					"Not auto select 'Transfer' as Batch Method for the first BOO", "fail");
		}

		click(btn_applyOperationOneBOO);

		explicitWait(btn_addBillOfOperationRowReplace.replace("row", "2"), 10);
		click(btn_addBillOfOperationRowReplace.replace("row", "2"));

		explicitWait(drop_elementCategoryBOO, 20);
		selectText(drop_elementCategoryBOO, elementCategoryOperation);
		Thread.sleep(1000);
		String secondBOOElementCategory = getSelectedOptionInDropdown(drop_elementCategoryBOO);
		if (secondBOOElementCategory.equals(elementCategoryOperation)) {
			writeTestResults("Verify user able to select 'Operation' as Element Category for the second BOO",
					"User should be able to select 'Operation' as Element Category for the second BOO",
					"User successfully select 'Operation' as Element Category for the second BOO", "pass");
		} else {
			writeTestResults("Verify user able to select 'Operation' as Element Category for the second BOO",
					"User should be able to select 'Operation' as Element Category for the second BOO",
					"User doesn't select 'Operation' as Element Category for the second BOO", "fail");
		}

		Thread.sleep(1000);
		String secondBOOActivityType = getSelectedOptionInDropdown(drop_activityTypeBOO);
		if (secondBOOActivityType.equals(activityTypeProduce)) {
			writeTestResults("Verify auto select 'Produce' as Activity Type for the second BOO",
					"Should auto select 'Produce' as Activity Type for the second BOO",
					"Auto select 'Produce' as Activity Type for the second BOO", "pass");
		} else {
			writeTestResults("Verify auto select 'Produce' as Activity Type for the second BOO",
					"Should auto select 'Produce' as Activity Type for the second BOO",
					"Not auto select 'Produce' as Activity Type for the second BOO", "fail");
		}

		sendKeys(txt_elementDescriptionBOO, produceElementDescription2);
		Thread.sleep(1000);
		String secondBOOEnteredDesc = getAttribute(txt_elementDescriptionBOO, "value");
		if (secondBOOEnteredDesc.equals(produceElementDescription2)) {
			writeTestResults("Verify user able to enter Element Description for the second BOO",
					"User should be able to enter Element Description for the second BOO",
					"User successfully enter Element Description for the second BOO", "pass");
		} else {
			writeTestResults("Verify user able to enter Element Description for the second BOO",
					"User should be able to enter Element Description for the second BOO",
					"User doesn't enter Element Description for the second BOO", "fail");
		}

		sendKeys(txt_groupIdBOO, groupIdBOOTwo);
		Thread.sleep(1000);
		String secondBOOEnteredGroupID = getAttribute(txt_groupIdBOO, "value");
		if (secondBOOEnteredGroupID.equals(groupIdBOOTwo)) {
			writeTestResults("Verify user able to enter Group ID for the second BOO",
					"User should be able to enter Group ID for the second BOO",
					"User successfully enter Group ID for the second BOO", "pass");
		} else {
			writeTestResults("Verify user able to enter Group ID for the second BOO",
					"User should be able to enter Group ID for the second BOO",
					"User doesn't enter Group ID for the second BOO", "fail");
		}

		Thread.sleep(1000);
		String secondBOOBatchMethod = getSelectedOptionInDropdown(drop_batchMethodBOO);
		if (secondBOOBatchMethod.equals(batchMethodTransferBOO)) {
			writeTestResults("Verify auto select 'Transfer' as Batch Method for the second BOO",
					"Should auto select 'Transfer' as Batch Method for the second BOO",
					"Auto select 'Transfer' as Batch Method for the second BOO", "pass");
		} else {
			writeTestResults("Verify auto select 'Transfer' as Batch Method for the second BOO",
					"Should auto select 'Transfer' as Batch Method for the second BOO",
					"Not auto select 'Transfer' as Batch Method for the second BOO", "fail");
		}

		click(btn_applyOperationOneBOO);

		explicitWait(btn_addBillOfOperationRowReplace.replace("row", "2"), 10);
		click(btn_addBillOfOperationRowReplace.replace("row", "2"));

		explicitWait(drop_elementCategoryBOO, 20);
		selectText(drop_elementCategoryBOO, elementCategoryOperation);
		Thread.sleep(1000);
		String thirdBOOElementCategory = getSelectedOptionInDropdown(drop_elementCategoryBOO);
		if (thirdBOOElementCategory.equals(elementCategoryOperation)) {
			writeTestResults("Verify user able to select 'Operation' as Element Category for the third BOO",
					"User should be able to select 'Operation' as Element Category for the third BOO",
					"User successfully select 'Operation' as Element Category for the third BOO", "pass");
		} else {
			writeTestResults("Verify user able to select 'Operation' as Element Category for the third BOO",
					"User should be able to select 'Operation' as Element Category for the third BOO",
					"User doesn't select 'Operation' as Element Category for the third BOO", "fail");
		}

		Thread.sleep(1000);
		selectText(drop_activityTypeBOO, activityTypeQualityCheck);
		String thirdBOOActivityType = getSelectedOptionInDropdown(drop_activityTypeBOO);
		if (thirdBOOActivityType.equals(activityTypeQualityCheck)) {
			writeTestResults("Verify user able to select 'Quality Check' as Activity Type for the third BOO",
					"User should be able to select 'Quality Check' as Activity Type for the third BOO",
					"User successfully select 'Quality Check' as Activity Type for the third BOO", "pass");
		} else {
			writeTestResults("Verify user able to select 'Quality Check' as Activity Type for the third BOO",
					"User should be able to select 'Quality Check' as Activity Type for the third BOO",
					"User doesn't select 'Quality Check' as Activity Type for the third BOO", "fail");
		}

		sendKeys(txt_elementDescriptionBOO, qcElementDescription);
		Thread.sleep(1000);
		String thirdBOOEnteredDesc = getAttribute(txt_elementDescriptionBOO, "value");
		if (thirdBOOEnteredDesc.equals(qcElementDescription)) {
			writeTestResults("Verify user able to enter Element Description for the third BOO",
					"User should be able to enter Element Description for the third BOO",
					"User successfully enter Element Description for the third BOO", "pass");
		} else {
			writeTestResults("Verify user able to enter Element Description for the third BOO",
					"User should be able to enter Element Description for the third BOO",
					"User doesn't enter Element Description for the third BOO", "fail");
		}

		sendKeys(txt_groupIdBOO, groupIdBOOThree);
		Thread.sleep(1000);
		String thirdBOOEnteredGroupID = getAttribute(txt_groupIdBOO, "value");
		if (thirdBOOEnteredGroupID.equals(groupIdBOOThree)) {
			writeTestResults("Verify user able to enter Group ID for the third BOO",
					"User should be able to enter Group ID for the third BOO",
					"User successfully enter Group ID for the third BOO", "pass");
		} else {
			writeTestResults("Verify user able to enter Group ID for the third BOO",
					"User should be able to enter Group ID for the third BOO",
					"User doesn't enter Group ID for the third BOO", "fail");
		}

		click(chk_finalInvestigationBOO);
		if (isSelected(chk_finalInvestigationBOO)) {
			writeTestResults("Verify user able to enable Final Investigation flag for the third BOO",
					"User should be able to enable Final Investigation flag for the third BOO",
					"User successfully enable Final Investigation flag for the third BOO", "pass");
		} else {
			writeTestResults("Verify user able to enable Final Investigation flag for the third BOO",
					"User should be able to enable Final Investigation flag for the third BOO",
					"User doesn't enable Final Investigation flag for the third BOO", "fail");
		}

		Thread.sleep(1000);
		String thirdBOOBatchMethod = getSelectedOptionInDropdown(drop_batchMethodBOO);
		if (thirdBOOBatchMethod.equals(batchMethodTransferBOO)) {
			writeTestResults("Verify auto select 'Transfer' as Batch Method for the third BOO",
					"Should auto select 'Transfer' as Batch Method for the third BOO",
					"Auto select 'Transfer' as Batch Method for the third BOO", "pass");
		} else {
			writeTestResults("Verify auto select 'Transfer' as Batch Method for the third BOO",
					"Should auto select 'Transfer' as Batch Method for the third BOO",
					"Not auto select 'Transfer' as Batch Method for the third BOO", "fail");
		}

		click(btn_applyOperationOneBOO);

		click(btn_draft);
		explicitWait(header_draftedBOO, 50);
		if (isDisplayed(header_draftedBOO)) {
			writeTestResults("Verify user able to draft the Bill Of Operation",
					"User should be able to draft the Bill Of Operation",
					"User successfully draft the Bill Of Operation", "pass");
		} else {
			writeTestResults("Verify user able to draft the Bill Of Operation",
					"User should be able to draft the Bill Of Operation", "User doesn't draft the Bill Of Operation",
					"fail");

		}

		click(btn_release);
		explicitWait(header_releasedBOO, 50);
		trackCode = getText(lbl_docNoBoo);
		writeProductionData("BOO_PROD_E2E_002", trackCode, 23);
		if (isDisplayed(header_releasedBOO)) {
			writeTestResults("Verify user able to release the Bill Of Operation",
					"User should be able to release the Bill Of Operation",
					"User successfully release the Bill Of Operation", "pass");
		} else {
			writeTestResults("Verify user able to release the Bill Of Operation",
					"User should be able to release the Bill Of Operation",
					"User doesn't release the Bill Of Operation", "fail");

		}
	}

	/* Production Model PROD_E2E_002 */
	public void productionModel1_PROD_E2E_002() throws Exception {
		pageRefersh();
		if (click(img_navigationPane)) {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User successfully click navigation pane", "pass");
		} else {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User couldn't click navigation pane", "fail");
		}

		explicitWait(btn_productionModule, 4);
		if (isEnabledQuickCheck(btn_productionModule)) {
			writeTestResults("Verify user able to view the \"Production\" module under main navigation menu",
					"User should be able to view the \"Production\" module under main navigation menu",
					"User successfully view the \"Production\" module under main navigation menu", "pass");
		} else {
			writeTestResults("Verify user able to view the \"Production\" module under main navigation menu",
					"User should be able to view the \"Production\" module under main navigation menu",
					"User couldn't view the \"Production\" module under main navigation menu", "fail");
		}

		click(btn_productionModule);

		explicitWait(btn_productionModel, 4);
		if (isEnabledQuickCheck(btn_productionModel)) {
			writeTestResults("Verify user able to view the \"Production Model\" option under production sub menu",
					"User should be able to view the \"Production Model\" option under production sub menu",
					"User able to view the \"Production Model\" option under production sub menu", "pass");
		} else {
			writeTestResults("Verify user able to view the \"Production Model\" option under production sub menu",
					"User should be able to view the \"Production Model\" option under production sub menu",
					"User couldn't view the \"Production Model\" option under production sub menu", "pass");
		}

		click(btn_productionModel);

		explicitWait(header_productionModelByPage, 40);
		if (isDisplayedQuickCheck(header_productionModelByPage)) {
			writeTestResults("Verify that user on the Production Model by page",
					"The user should be on the Production Model by page",
					"The user successfully on the Production Model by page", "pass");
		} else {
			writeTestResults("Verify that user on the Production Model by page",
					"The user should be on the Production Model by page",
					"The user not on the Production Model by page", "fail");
		}

		click(btn_newButtonProductionModel);
		explicitWait(header_newProductionModel, 40);
		if (isDisplayedQuickCheck(header_newProductionModel)) {
			writeTestResults("Verify that user can navigate to the new production model creation page",
					"The user should be navigate to the new production model creation page",
					"The user successfully navigate to the new production model creation page", "pass");
		} else {
			writeTestResults("Verify that user can navigate to the new production model creation page",
					"The user should be navigate to the new production model creation page",
					"The user doesn't navigate to the new production model creation page", "fail");
		}

		String modelCode = invObj.currentTimeAndDate().replace("/", "").replace(":", "");
		sendKeys(txt_modelCodeProductionModel, modelCode);
		String enteresModelCode = getAttribute(txt_modelCodeProductionModel, "value");
		if (enteresModelCode.equals(modelCode)) {
			writeTestResults(
					"Verify that system is displayed the user entered data in 'Model Code' field of summary sections",
					"The system should be displayed the user entered data in 'Model Code' field of summary sections",
					"The system successfully displayed the user entered data in 'Model Code' field of summary sections",
					"pass");
		} else {
			writeTestResults(
					"Verify that system is displayed the user entered data in 'Model Code' field of summary sections",
					"The system should be displayed the user entered data in 'Model Code' field of summary sections",
					"The system doesn't displayed the user entered data in 'Model Code' field of summary sections",
					"fail");
		}

		sendKeys(txt_descriptionProductionModel, commonDescription);
		String enteresDescription = getAttribute(txt_descriptionProductionModel, "value");
		if (enteresDescription.equals(commonDescription)) {
			writeTestResults(
					"Verify that system is displayed the user entered data in 'Description' field of summary sections",
					"The system should be displayed the user entered data in 'Description' field of summary sections",
					"The system successfully displayed the user entered data in 'Description' field of summary sections",
					"pass");
		} else {
			writeTestResults(
					"Verify that system is displayed the user entered data in 'Description' field of summary sections",
					"The system should be displayed the user entered data in 'Description' field of summary sections",
					"The system doesn't displayed the user entered data in 'Description' field of summary sections",
					"fail");
		}

		selectText(drop_productionGroupProductionModel, productionGroupE2E);
		explicitWaitUntillClickable(btn_yesClearConfirmation, 5);
		click(btn_yesClearConfirmation);
		Thread.sleep(2000);
		String selectedProductGroup = getSelectedOptionInDropdown(drop_productionGroupProductionModel);
		if (selectedProductGroup.equals(productionGroupE2E)) {
			writeTestResults("Verify that user selected product group is displayed",
					"The user selected product group should be displayed",
					"The user selected product group successfully displayed", "pass");
		} else {
			writeTestResults("Verify that user selected product group is displayed",
					"The user selected product group should be displayed",
					"The user selected product group doesn't displayed", "fail");
		}

	}

	public void productionModel2_PROD_E2E_002() throws Exception {
		click(btn_productLookupProductionModel);
		explicitWait(header_productLookup, 20);
		if (isDisplayedQuickCheck(header_productLookup)) {
			writeTestResults("Verify that product look up screen is loaded",
					"The product look up screen should be loaded", "The product look up screen successfully loaded",
					"pass");
		} else {
			writeTestResults("Verify that product look up screen is loaded",
					"The product look up screen should be loaded", "The product look up screen doesn't loaded", "fail");
		}

		sendKeysLookup(txt_searchProductCommon, invObj.readTestCreation("BatchFifoMultiPlatformProductionType"));
		Thread.sleep(500);
		String enteredProduct = getAttribute(txt_searchProductCommon, "value");
		if (enteredProduct.equals(invObj.readTestCreation("BatchFifoMultiPlatformProductionType"))) {
			writeTestResults("Verify that user entred product code/ description on the search area is displayed",
					"The user entred product code/ description on the search area should be displayed",
					"The user entred product code/ description on the search area successfully displayed", "pass");
		} else {
			writeTestResults("Verify that user entred product code/ description on the search area is displayed",
					"The user entred product code/ description on the search area should be displayed",
					"The user entred product code/ description on the search area doesn't displayed", "fail");
		}

		if (click(btn_searchProductProductionModel)) {
			writeTestResults("Verify that search button is clicked successfully",
					"The search button should be clicked successfully", "The search button clicked successfully",
					"pass");
		} else {
			writeTestResults("Verify that search button is clicked successfully",
					"The search button should be clicked successfully", "The search button doesn't clicked", "fail");
		}

		Thread.sleep(2000);
		explicitWait(tr_reultOnLookupInfoReplace.replace("info",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")), 25);
		doubleClick(tr_reultOnLookupInfoReplace.replace("info",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")));
		explicitWaitUntillClickable(btn_yesClearConfirmation, 5);
		click(btn_yesClearConfirmation);
		Thread.sleep(2000);

		String selectedProduct = getAttribute(txt_productOnFrontPagePM, "value");
		if (selectedProduct.contains(invObj.readTestCreation("BatchFifoMultiPlatformProductionType"))) {
			writeTestResults("Verify that user selected product is diplayed",
					"The user selected product should be diplayed", "The user selected product successfully diplayed",
					"pass");
		} else {
			writeTestResults("Verify that user selected product is diplayed",
					"The user selected product should be diplayed", "The user selected product doesn't diplayed",
					"fail");
		}

		click(btn_searchProductionUnit);
		explicitWait(header_productionUnitLookupPM, 15);
		if (isDisplayed(header_productionUnitLookupPM)) {
			writeTestResults("Verify that production unit look up screen was loaded",
					"The production unit look up screen should be loaded",
					"The production unit look up successfully loaded", "pass");
		} else {
			writeTestResults("Verify that production unit look up screen was loaded",
					"The production unit look up screen should be loaded", "The production unit look up doesn't loaded",
					"fail");
		}

		sendKeysLookup(txt_searchProductionUnit, readProductionData("Location_PROD_E2E"));
		String enterdProductionUnitOnSerachBox = getAttribute(txt_searchProductionUnit, "value");
		if (enterdProductionUnitOnSerachBox.equals(readProductionData("Location_PROD_E2E"))) {
			writeTestResults(
					"Verify that the user entred production unit code/ description on the search area is displayed",
					"The user entred production unit code/ description on the search area should be displayed",
					"The user entred production unit code/ description on the search area successfully displayed",
					"pass");
		} else {
			writeTestResults(
					"Verify that the user entred production unit code/ description on the search area is displayed",
					"The user entred production unit code/ description on the search area should be displayed",
					"The user entred production unit code/ description on the search area doesn't displayed", "fail");
		}

		if (click(btn_searchProductionUnitOnLookup)) {
			writeTestResults("Verify that the the search button was clicked",
					"The search button should be clicked successfully", "The search button clicked successfully",
					"pass");
		} else {
			writeTestResults("Verify that the the search button was clicked",
					"The search button should be clicked successfully", "The search button doesn't click", "fail");
		}

		Thread.sleep(2000);
		explicitWaitUntillClickable(lnk_resultProductionUnitOnLookupUnitReplace.replace("production_unit",
				readProductionData("Location_PROD_E2E")), 20);
		if (isDisplayed(lnk_resultProductionUnitOnLookupUnitReplace.replace("production_unit",
				readProductionData("Location_PROD_E2E")))) {
			writeTestResults("Verify that search result relevent to enterd code is dispaly",
					"Search result relevent to enterd code should be dispaly",
					"Search result relevent to enterd code successfully dispaly", "pass");
		} else {
			writeTestResults("Verify that search result relevent to enterd code is dispaly",
					"Search result relevent to enterd code should be dispaly",
					"Search result relevent to enterd code doesn't dispaly", "fail");
		}

		doubleClick(lnk_resultProductionUnitOnLookupUnitReplace.replace("production_unit",
				readProductionData("Location_PROD_E2E")));
		Thread.sleep(2500);

		String selectedProductionUnit = getAttribute(txt_productionUnitOnFrontPage, "value");
		if (selectedProductionUnit.contains(readProductionData("Location_PROD_E2E"))) {
			writeTestResults("Verify that the user selected 'Production Unit' is diplayed",
					"The user selected 'Production Unit' should be diplayed",
					"The user selected 'Production Unit' successfully diplayed", "pass");
		} else {
			writeTestResults("Verify that the user selected 'Production Unit' is diplayed",
					"The user selected 'Production Unit' should be diplayed",
					"The user selected 'Production Unit' doesn't diplayed", "fail");
		}

		String autoSelectedInputWare = getSelectedOptionInDropdown(drop_inputWarehousePM);
		if (autoSelectedInputWare.contains(inputWarehouseLI)) {
			writeTestResults(
					"Verify that the system was auto selected 'Input Warehouse' when user select the 'Production Unit'",
					"The system should be auto selected 'Input Warehouse' when user select the 'Production Unit'",
					"The system successfully auto selected 'Input Warehouse' when user select the 'Production Unit'",
					"pass");
		} else {
			writeTestResults(
					"Verify that the system was auto selected 'Input Warehouse' when user select the 'Production Unit'",
					"The system should be auto selected 'Input Warehouse' when user select the 'Production Unit'",
					"The system doesn't auto selected 'Input Warehouse' when user select the 'Production Unit'",
					"fail");
		}

		String autoSelectedOutputWare = getSelectedOptionInDropdown(drop_outputWarehousePM);
		if (autoSelectedOutputWare.contains(outputWarehouseLI)) {
			writeTestResults(
					"Verify that the system was auto selected 'Output Warehouse' when user select the 'Production Unit'",
					"The system should be auto selected 'Output Warehouse' when user select the 'Production Unit'",
					"The system successfully auto selected 'Output Warehouse' when user select the 'Production Unit'",
					"pass");
		} else {
			writeTestResults(
					"Verify that the system was auto selected 'Output Warehouse' when user select the 'Production Unit'",
					"The system should be auto selected 'Output Warehouse' when user select the 'Production Unit'",
					"The system doesn't auto selected 'Output Warehouse' when user select the 'Production Unit'",
					"fail");
		}

		String autoSelectedWipWare = getSelectedOptionInDropdown(drop_wipWarehousePM);
		if (autoSelectedWipWare.contains(wipWarehouseLI)) {
			writeTestResults(
					"Verify that the system was auto selected 'WIP Warehouse' when user select the 'Production Unit'",
					"The system should be auto selected 'WIP Warehouse' when user select the 'Production Unit'",
					"The system successfully auto selected 'WIP Warehouse' when user select the 'Production Unit'",
					"pass");
		} else {
			writeTestResults(
					"Verify that the system was auto selected 'WIP Warehouse' when user select the 'Production Unit'",
					"The system should be auto selected 'WIP Warehouse' when user select the 'Production Unit'",
					"The system doesn't auto selected 'WIP Warehouse' when user select the 'Production Unit'", "fail");
		}

		sendKeys(txt_batchQuantityOtherInformationPM, batchQuantityE2E);

		Thread.sleep(2000);
		selectText(drop_barcodeBookOtherInformationPM, barcodeBookPM);
		String selectedBarcodeBook = getSelectedOptionInDropdown(drop_barcodeBookOtherInformationPM);
		if (selectedBarcodeBook.contains(barcodeBookPM)) {
			writeTestResults("Verify user able to select Barcode Book", "User should be able to select Barcode Book",
					"User successfully select Barcode Book", "pass");
		} else {
			writeTestResults("Verify user able to select Barcode Book", "User should be able to select Barcode Book",
					"User couldn't select Barcode Book", "fail");
		}

		selectText(drop_costingPriorityOtherInformationPM, costicPriorityNormalPM);
		String selectedCostingPriority = getSelectedOptionInDropdown(drop_costingPriorityOtherInformationPM);
		if (selectedCostingPriority.contains(costicPriorityNormalPM)) {
			writeTestResults("Verify user able to select Costing Priority",
					"User should be able to select Costing Priority", "User successfully select Costing Priority",
					"pass");
		} else {
			writeTestResults("Verify user able to select Costing Priority",
					"User should be able to select Costing Priority", "User couldn't select Costing Priority", "fail");
		}

		sendKeys(txt_statndardCostOtherInformationPM, standardCostPM);
		String enteredStandardCost = getAttribute(txt_statndardCostOtherInformationPM, "value");
		if (enteredStandardCost.equals(standardCostPM)) {
			writeTestResults("Verify that user able to enter Standard Cost",
					"User should be able to enter Standard Cost", "User successfully enter Standard Cost", "pass");
		} else {
			writeTestResults("Verify that user able to enter Standard Cost",
					"User should be able to enter Standard Cost", "User couldn't enter Standard Cost", "fail");
		}

		click(btn_lookupPricingProfilePM);
		explicitWait(header_pricingProfileLookupPM, 15);
		if (isDisplayed(header_pricingProfileLookupPM)) {
			writeTestResults("Verify that pricing profile look up screen was loaded",
					"The pricing profile look up screen should be loaded",
					"The pricing profile look up successfully loaded", "pass");
		} else {
			writeTestResults("Verify that pricing profile look up screen was loaded",
					"The pricing profile look up screen should be loaded", "The pricing profile look up doesn't loaded",
					"fail");
		}

		sendKeysLookup(txt_searchPricingProfile, readProductionData("Pricing_Profile_PROD_E2E"));
		String enterdPricingProfileOnSerachBox = getAttribute(txt_searchPricingProfile, "value");
		if (enterdPricingProfileOnSerachBox.equals(readProductionData("Pricing_Profile_PROD_E2E"))) {
			writeTestResults(
					"Verify that the user entred pricing profile code/ description on the search area is displayed",
					"The user entred pricing profile code/ description on the search area should be displayed",
					"The user entred pricing profile code/ description on the search area successfully displayed",
					"pass");
		} else {
			writeTestResults(
					"Verify that the user entred pricing profile code/ description on the search area is displayed",
					"The user entred pricing profile code/ description on the search area should be displayed",
					"The user entred pricing profile code/ description on the search area doesn't displayed", "fail");
		}

		if (click(btn_searchPricingProfilePM)) {
			writeTestResults("Verify that the the search button was clicked",
					"The search button should be clicked successfully", "The search button clicked successfully",
					"pass");
		} else {
			writeTestResults("Verify that the the search button was clicked",
					"The search button should be clicked successfully", "The search button doesn't click", "fail");
		}

		Thread.sleep(2000);
		explicitWaitUntillClickable(td_resultPricingProfileReplacePP.replace("pricing_profile",
				readProductionData("Pricing_Profile_PROD_E2E")), 20);
		if (isDisplayed(td_resultPricingProfileReplacePP.replace("pricing_profile",
				readProductionData("Pricing_Profile_PROD_E2E")))) {
			writeTestResults("Verify that search result relevent to enterd code is dispaly",
					"Search result relevent to enterd code should be dispaly",
					"Search result relevent to enterd code successfully dispaly", "pass");
		} else {
			writeTestResults("Verify that search result relevent to enterd code is dispaly",
					"Search result relevent to enterd code should be dispaly",
					"Search result relevent to enterd code doesn't dispaly", "fail");
		}

		doubleClick(td_resultPricingProfileReplacePP.replace("pricing_profile",
				readProductionData("Pricing_Profile_PROD_E2E")));
		Thread.sleep(2500);

		String selectedPricingProfile = getAttribute(txt_pricingProfileOtherInformationPM, "value");
		if (selectedPricingProfile.contains(readProductionData("Pricing_Profile_PROD_E2E"))) {
			writeTestResults("Verify that the user selected 'Pricing Profile' is diplayed",
					"The user selected 'Pricing Profile' should be diplayed",
					"The user selected 'Pricing Profile' successfully diplayed", "pass");
		} else {
			writeTestResults("Verify that the user selected 'Pricing Profile' is diplayed",
					"The user selected 'Pricing Profile' should be diplayed",
					"The user selected 'Pricing Profile' doesn't diplayed", "fail");
		}

		click(chk_infinitrOtherInformationPM);
		if (isSelected(chk_infinitrOtherInformationPM)) {
			writeTestResults("Verify that user able to enable Infinite", "User should be able to enable Infinite",
					"User successfully enable Infinite", "pass");
		} else {
			writeTestResults("Verify that user able to enable Infinite", "User should be able to enable Infinite",
					"User couldn't enable Infinite", "fail");
		}

		pageScrollUpToBottom();
		try {
			String bom = readProductionData("BOM_PROD_E2E_002");
			selectTextByContains(drop_bomPM, bom);
			explicitWaitUntillClickable(btn_yesClearConfirmation, 5);
			click(btn_yesClearConfirmation);
			Thread.sleep(2000);
			String selectedBOM = getSelectedOptionInDropdown(drop_bomPM);
			if (selectedBOM.contains(readProductionData("BOM_PROD_E2E_002"))) {
				writeTestResults("Verify user able to select Bill of Material",
						"User should be able to select Bill of Material", "User successfully select Bill of Material",
						"pass");
			} else {
				writeTestResults("Verify user able to select Bill of Material",
						"User should be able to select Bill of Material", "User couldn't select Bill of Material",
						"fail");
			}
		} catch (Exception e) {
			writeTestResults("Verify user able to select Bill of Material",
					"User should be able to select Bill of Material", "User couldn't select Bill of Material", "fail");
		}

	}

	public void productionModel3_PROD_E2E_002() throws Exception {
		click(btn_lookupBOOPM);
		explicitWait(header_billOfOperatioPM, 15);
		if (isDisplayed(header_billOfOperatioPM)) {
			writeTestResults("Verify that Bill of Operation look up screen was loaded",
					"The Bill of Operation look up screen should be loaded",
					"The Bill of Operation look up successfully loaded", "pass");
		} else {
			writeTestResults("Verify that Bill of Operation look up screen was loaded",
					"The Bill of Operation look up screen should be loaded",
					"The Bill of Operation look up doesn't loaded", "fail");
		}

		sendKeysLookup(txt_searchBillOfOperationPM, readProductionData("BOO_PROD_E2E_002"));
		String enterdBOO = getAttribute(txt_searchBillOfOperationPM, "value");
		if (enterdBOO.equals(readProductionData("BOO_PROD_E2E_002"))) {
			writeTestResults(
					"Verify that the user entred bill of operation code/ description on the search area is displayed",
					"The user entred bill of operation code/ description on the search area should be displayed",
					"The user entred bill of operation code/ description on the search area successfully displayed",
					"pass");
		} else {
			writeTestResults(
					"Verify that the user entred bill of operation code/ description on the search area is displayed",
					"The user entred bill of operation code/ description on the search area should be displayed",
					"The user entred bill of operation code/ description on the search area doesn't displayed", "fail");
		}

		if (click(btn_searchBOOInLookup)) {
			writeTestResults("Verify that the the search button was clicked",
					"The search button should be clicked successfully", "The search button clicked successfully",
					"pass");
		} else {
			writeTestResults("Verify that the the search button was clicked",
					"The search button should be clicked successfully", "The search button doesn't click", "fail");
		}

		Thread.sleep(2000);
		explicitWaitUntillClickable(td_resultBOOPMBooReplace.replace("boo", readProductionData("BOO_PROD_E2E_002")),
				20);
		if (isDisplayed(td_resultBOOPMBooReplace.replace("boo", readProductionData("BOO_PROD_E2E_002")))) {

			writeTestResults("Verify that search result relevent to enterd code is dispaly",
					"Search result relevent to enterd code should be dispaly",
					"Search result relevent to enterd code successfully dispaly", "pass");
		} else {
			writeTestResults("Verify that search result relevent to enterd code is dispaly",
					"Search result relevent to enterd code should be dispaly",
					"Search result relevent to enterd code doesn't dispaly", "fail");
		}

		doubleClick(td_resultBOOPMBooReplace.replace("boo", readProductionData("BOO_PROD_E2E_002")));
		Thread.sleep(2500);

		click(btn_okClearConfirmationBOOPM);

		Thread.sleep(2000);
		String selectedBOO = getAttribute(txt_billOfOperationPMFrontPage, "value");
		if (selectedBOO.contains(readProductionData("BOO_PROD_E2E_002"))) {
			writeTestResults("Verify that the user selected 'Bill of Operation' is diplayed",
					"The user selected 'Bill of Operation' should be diplayed",
					"The user selected 'Bill of Operation' successfully diplayed", "pass");
		} else {
			writeTestResults("Verify that the user selected 'Bill of Operation' is diplayed",
					"The user selected 'Bill of Operation' should be diplayed",
					"The user selected 'Bill of Operation' doesn't diplayed", "fail");
		}

		click(tab_billOfOperationProductionModel);
		if (isDisplayed(tab_billOfOperationAfterSelected)) {
			writeTestResults("Verify that the user is on the bill of operations tab",
					"The user should be on the bill of operations tab",
					"The user successfully on the bill of operations tab", "pass");
		} else {
			writeTestResults("Verify that the user is on the bill of operations tab",
					"The user should be on the bill of operations tab", "The user is not on the bill of operations tab",
					"fail");
		}

		click(btn_expandPM);
		Thread.sleep(1500);
		if (isDisplayedQuickCheck(tr_booRow1PM) && isDisplayedQuickCheck(tr_booRow2PM)
				&& isDisplayedQuickCheck(tr_booRow3PM) && isDisplayedQuickCheck(tr_booRow4PM)) {
			writeTestResults("Verify that system is displayed the bill of operations details",
					"The system should be displayed the bill of operations details",
					"The system successfully displayed the bill of operations details", "pass");
		} else {
			writeTestResults("Verify that system is displayed the bill of operations details",
					"The system should be displayed the bill of operations details",
					"The system doesn't displayed the bill of operations details", "fail");

		}

		click(btn_editBillOfOperationPMRowReplace.replace("row", "1"));
		explicitWait(header_operationActivityPopup, 20);
		if (isDisplayedQuickCheck(header_operationActivityPopup)) {
			writeTestResults("Verify that Operations/Activity screen was loaded",
					"Operations/Activity screen should be loaded", "Operations/Activity screen successfully loaded",
					"pass");
		} else {
			writeTestResults("Verify that Operations/Activity screen was loaded",
					"Operations/Activity screen should be loaded", "Operations/Activity screen wasn't loaded", "fail");
		}

		click(btn_inputProductsTabBOOPM);
		explicitWait(li_inputProductSelected, 10);
		if (isDisplayedQuickCheck(li_inputProductSelected)) {
			writeTestResults("Verify that user can moved to the Input Products tab",
					"User should be moved to the Input Products tab",
					"User successfully moved to the Input Products tab", "pass");
		} else {
			writeTestResults("Verify that user can moved to the Input Products tab",
					"User should be moved to the Input Products tab", "User couldn't moved to the Input Products tab",
					"fail");
		}

		click(chk_inputProductsRowReplaceBOOPM.replace("row", "1"));
		click(chk_inputProductsRowReplaceBOOPM.replace("row", "2"));
		click(chk_inputProductsRowReplaceBOOPM.replace("row", "3"));

		if (isSelected(chk_inputProductsRowReplaceBOOPM.replace("row", "1"))
				&& isSelected(chk_inputProductsRowReplaceBOOPM.replace("row", "2"))
				&& isSelected(chk_inputProductsRowReplaceBOOPM.replace("row", "3"))) {
			writeTestResults("Verify that user able to enable both input products for initial operation",
					"User should be able to enable both input products for initial operation",
					"User successfully enable both input products for initial operation", "pass");
		} else {
			writeTestResults("Verify that user able to enable both input products for initial operation",
					"User should be able to enable both input products for initial operation",
					"User doesn't enable both input products for initial operation", "fail");
		}

		click(btn_applyBOOPM);

		explicitWait(btn_editBillOfOperationPMRowReplace.replace("row", "2"), 10);
		click(btn_editBillOfOperationPMRowReplace.replace("row", "2"));

		selectText(drop_processTimeTypeBOOPM, processTimeTypePM);

		click(btn_inputProductsTabBOOPM);
		explicitWait(li_inputProductSelected, 10);
		if (isDisplayedQuickCheck(li_inputProductSelected)) {
			writeTestResults("Verify that user can moved to the Input Products tab",
					"User should be moved to the Input Products tab",
					"User successfully moved to the Input Products tab", "pass");
		} else {
			writeTestResults("Verify that user can moved to the Input Products tab",
					"User should be moved to the Input Products tab", "User couldn't moved to the Input Products tab",
					"fail");
		}

		click(chk_inputProductsRowReplaceBOOPM.replace("row", "1"));
		click(chk_inputProductsRowReplaceBOOPM.replace("row", "2"));
		if (isSelected(chk_inputProductsRowReplaceBOOPM.replace("row", "1"))
				&& isSelected(chk_inputProductsRowReplaceBOOPM.replace("row", "2"))) {
			writeTestResults("Verify that user able to enable a product for the first operation",
					"User should be able to enable a product for the first operation",
					"User successfully enable a product for the first operation", "pass");
		} else {
			writeTestResults("Verify that user able to enable a product for the first operation",
					"User should be able to enable a product for the first operation",
					"User doesn't enable a product for the first operation", "fail");
		}

		click(btn_applyBOOPM);

		explicitWait(btn_editBillOfOperationPMRowReplace.replace("row", "3"), 10);
		click(btn_editBillOfOperationPMRowReplace.replace("row", "3"));

		selectText(drop_processTimeTypeBOOPM, processTimeTypePM);

		click(btn_inputProductsTabBOOPM);
		click(chk_inputProductsRowReplaceBOOPM.replace("row", "1"));
		if (isSelected(chk_inputProductsRowReplaceBOOPM.replace("row", "1"))) {
			writeTestResults("Verify that user able to enable a product for the second operation",
					"User should be able to enable a product for the second operation",
					"User successfully enable a product for the second operation", "pass");
		} else {
			writeTestResults("Verify that user able to enable a product for the second operation",
					"User should be able to enable a product for the second operation",
					"User doesn't enable a product for the second operation", "fail");
		}

		click(btn_applyBOOPM);

		explicitWaitUntillClickable(btn_draft, 15);
		click(btn_draft);
		explicitWait(header_draftedProductionModel, 40);
		if (isDisplayedQuickCheck(header_draftedProductionModel)) {
			writeTestResults("Verify that the system was drafted the newly created Production Model",
					"The system should be drafted the newly created Production Model",
					"The system successfully drafted the newly created Production Model", "pass");
		} else {
			writeTestResults("Verify that the system was drafted the newly created Production Model",
					"The system should be drafted the newly created Production Model",
					"The system wasn't drafted the newly created Production Model", "fail");

		}

		click(btn_edit);
		explicitWaitUntillClickable(btn_update, 30);
		if (isDisplayedQuickCheck(btn_update)) {
			writeTestResults("Verify that the system is displayed update button in the summary tab",
					"The system should be displayed update button in the summary tab",
					"The system successfully displayed update button in the summary tab", "pass");
		} else {
			writeTestResults("Verify that the system is displayed update button in the summary tab",
					"The system should be displayed update button in the summary tab",
					"The system doesn't displayed update button in the summary tab", "fail");
		}

		if (isDisplayedQuickCheck(tab_summaryProductionModel)
				&& isDisplayedQuickCheck(tab_billOfOperationProductionModel)) {
			writeTestResults("The system doesn't displayed the tabs mentioned here (Summary, Bill of operations)",
					"The system should be displayed the tabs mentioned here (Summary, Bill of operations)",
					"The system successfully displayed the tabs mentioned here (Summary, Bill of operations)", "pass");
		} else {
			writeTestResults("The system doesn't displayed the tabs mentioned here (Summary, Bill of operations)",
					"The system should be displayed the tabs mentioned here (Summary, Bill of operations)",
					"The system doesn't displayed the tabs mentioned here (Summary, Bill of operations)", "fail");
		}

		click(tab_billOfOperationProductionModel);
		explicitWaitUntillClickable(btn_expandPM, 10);
		click(btn_expandPM);
		explicitWaitUntillClickable(btn_editOperation01PM, 10);
	}

	public void productionModel4_PROD_E2E_002() throws Exception {
		click(btn_editOperation01PM);
		explicitWait(header_operationActivityPopup, 20);
		if (isDisplayedQuickCheck(header_operationActivityPopup)) {
			writeTestResults("Verify that Operations/Activity screen was loaded",
					"Operations/Activity screen should be loaded", "Operations/Activity screen successfully loaded",
					"pass");
		} else {
			writeTestResults("Verify that Operations/Activity screen was loaded",
					"Operations/Activity screen should be loaded", "Operations/Activity screen wasn't loaded", "fail");
		}

		click(btn_additionalResourseTabPM);
		explicitWait(btn_additionalResourseTabSelectedPM, 20);
		if (isDisplayedQuickCheck(btn_additionalResourseTabSelectedPM)) {
			writeTestResults("Verify that user can moved to the additional resourse tab",
					"User should be moved to the additional resourse tab",
					"User successfully moved to the additional resourse tab", "pass");
		} else {
			writeTestResults("Verify that user can moved to the additional resourse tab",
					"User should be moved to the additional resourse tab",
					"User couldn't moved to the additional resourse tab", "fail");
		}

		click(btn_resourseLookupAdditionalResourseTabPM);
		explicitWait(lookup_resource, 20);
		if (isDisplayedQuickCheck(lookup_resource)) {
			writeTestResults("Verify that resource look up was loaded", "Resource look up should be loaded",
					"Resource look up successfully loaded", "pass");
		} else {
			writeTestResults("Verify that resource look up was loaded", "Resource look up should be loaded",
					"Resource look up doesn't loaded", "fail");
		}

		sendKeysLookup(txt_searchResource, readProductionData("ResourceCode02_E2E"));
		click(btn_resourceSearchInLookup);
		Thread.sleep(2000);
		explicitWait(td_resultResourceAdditionalResourceTabPMResourseReplace.replace("resource",
				readProductionData("ResourceCode02_E2E")), 7);
		doubleClick(td_resultResourceAdditionalResourceTabPMResourseReplace.replace("resource",
				readProductionData("ResourceCode02_E2E")));
		explicitWait(div_resourceOnAdditonalResourceGridPMResorceReplace.replace("resource",
				readProductionData("ResourceCode02_E2E")), 30);
		if (isDisplayedQuickCheck(div_resourceOnAdditonalResourceGridPMResorceReplace.replace("resource",
				readProductionData("ResourceCode02_E2E")))) {
			writeTestResults("Verify that selected resource is display in additional resource tab",
					"Selected resource should be display in additional resource tab",
					"Selected resource successfully display in additional resource tab", "pass");
		} else {
			writeTestResults("Verify that selected resource is display in additional resource tab",
					"Selected resource should be display in additional resource tab",
					"Selected resource successfully display in additional resource tab", "pass");
		}

		if (isDisplayedQuickCheck(header_resourceHoursAdditionalResourseTabPM)) {
			writeTestResults(
					"Verify that resource duration column placed without change in position in the additional resource tab",
					"Resource duration column should be placed without change in position in the additional resource tab",
					"Resource duration column placed without change in position in the additional resource tab",
					"pass");
		} else {
			writeTestResults(
					"Verify that resource duration column placed without change in position in the additional resource tab",
					"Resource duration column should be placed without change in position in the additional resource tab",
					"Resource duration column not placed without change in position in the additional resource tab",
					"fail");
		}

		click(btn_selectHoursAddtionalResourceTabPM);
		Thread.sleep(1500);
		click(btn_oneHourAdditionalResourceHoursPM);
		Thread.sleep(1500);
		click(btn_okDurationAdditionalResourceHoursPM);
		Thread.sleep(2000);

		String AdditionalResourseHours = getAttribute(btn_selectHoursAddtionalResourceTabPM, "value");
		String AdditionalResourseMins = getAttribute(btn_selectMinutesAddtionalResourceTabPM, "value");

		if (AdditionalResourseHours.equals("01") && AdditionalResourseMins.equals("00")) {
			writeTestResults("Verify that resource duration entered in the additional resource tab",
					"Resource duration should be entered in the additional resource tab",
					"Resource duration successfully entered in the additional resource tab", "pass");
		} else {
			writeTestResults("Verify that resource duration entered in the additional resource tab",
					"Resource duration should be entered in the additional resource tab",
					"Resource duration doesn't entered in the additional resource tab", "fail");
		}

		click(btn_applyOperationActivity);
		waitUntillNotVisible(header_operationActivityPopup, 15);
		if (!isDisplayedQuickCheck(header_operationActivityPopup)) {
			writeTestResults("Verify that additional resource was added to the operations successfully",
					"The additional resource should be added to the operations successfully",
					"The additional resource successfully added to the operations successfully", "pass");
		} else {
			writeTestResults("Verify that additional resource was added to the operations successfully",
					"The additional resource should be added to the operations successfully",
					"The additional resource was not added to the operations", "fail");
		}

		click(btn_update);
		explicitWaitUntillClickable(btn_edit, 30);
		if (isEnabled(btn_edit)) {
			writeTestResults("Verify user able to update the Production Model",
					"User should be able to update the Production Model",
					"User uccessfully update the Production Model", "pass");
		} else {
			writeTestResults("Verify user able to update the Production Model",
					"User should be able to update the Production Model", "User couldn't update the Production Model",
					"fail");
		}

		click(btn_release);

		explicitWait(header_releasedProductionModel, 60);
		trackCode = getText(div_productionModelCode);
		String productionModelCode = trackCode;
		writeProductionData("ProductionModel_PROD_E2E_002", productionModelCode, 24);
		if (isDisplayed(header_releasedProductionModel)) {
			writeTestResults("Verify user able to release the Production Model",
					"User should be able to release the Production Model",
					"User uccessfully release the Production Model", "pass");
		} else {
			writeTestResults("Verify user able to release the Production Model",
					"User should be able to release the Production Model", "User couldn't release the Production Model",
					"fail");
		}

	}

	/* Production Order PROD_E2E_002 */
	public void productionOrder_1_PROD_E2E_002() throws Exception {
		explicitWaitUntillClickable(img_navigationPane, 60);
		Thread.sleep(2000);
		explicitWaitUntillClickable(img_navigationPane, 20);
		if (click(img_navigationPane)) {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User successfully click navigation pane", "pass");
		} else {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User couldn't click navigation pane", "fail");
		}

		explicitWait(btn_production, 4);
		if (isEnabledQuickCheck(btn_production)) {
			writeTestResults("Verify user able to view the \"Production\" module under main navigation menu",
					"User should be able to view the \"Production\" module under main navigation menu",
					"User successfully view the \"Production\" module under main navigation menu", "pass");
		} else {
			writeTestResults("Verify user able to view the \"Production\" module under main navigation menu",
					"User should be able to view the \"Production\" module under main navigation menu",
					"User couldn't view the \"Production\" module under main navigation menu", "fail");
		}

		click(btn_production);

		explicitWait(btn_productionOrder, 4);
		if (isEnabledQuickCheck(btn_productionOrder)) {
			writeTestResults("Verify user able to view the \"Production Order\" option under production sub menu",
					"User should be able to view the \"Production Order\" option under production sub menu",
					"User able to view the \"Production Order\" option under production sub menu", "pass");
		} else {
			writeTestResults("Verify user able to view the \"Production Order\" option under production sub menu",
					"User should be able to view the \"Production Order\" option under production sub menu",
					"User couldn't view the \"Production Order\" option under production sub menu", "pass");
		}

		click(btn_productionOrder);

		explicitWait(header_productionOrderByPage, 60);
		if (isDisplayed(header_productionOrderByPage)) {
			writeTestResults("Verify user able to navigate to Production Order by-page",
					"User should be able to navigate to Production Order by-page",
					"User successfully navigate to Production Order by-page", "pass");
		} else {
			writeTestResults("Verify user able to navigate to Production Order by-page",
					"User should be able to navigate to Production Order by-page",
					"User coulsn't navigate to Production Order by-page", "fail");
		}

		if (isDisplayed(header_productionOrderDocNoColumnOnByPage)) {
			writeTestResults("Verify that 'Doc No' column is displayed as earlier",
					"'Doc No' column should be displayed as earlier",
					"'Doc No' column successfully displayed as earlier", "pass");
		} else {
			writeTestResults("Verify that 'Doc No' column is displayed as earlier",
					"'Doc No' column should be displayed as earlier", "'Doc No' column doesn't displayed as earlier",
					"fail");
		}

		click(btn_newProductionOrder);
		explicitWait(header_newProductionOrder, 60);
		if (isDisplayed(header_newProductionOrder)) {
			writeTestResults("Verify user able to navigate to Production Order new-form",
					"User should be able to navigate to Production Order new-form",
					"User successfully navigate to Production Order new-form", "pass");
		} else {
			writeTestResults("Verify user able to navigate to Production Order new-form",
					"User should be able to navigate to Production Order new-form",
					"User coulsn't navigate to Production Order new-form", "fail");
		}

		click(span_productLookupPO);
		explicitWait(lookup_product, 30);
		if (isDisplayed(lookup_product)) {
			writeTestResults("Verify that product lookup is open", "Product lookup should be open",
					"Product lookup successfully open", "pass");
		} else {
			writeTestResults("Verify that product lookup is open", "Product lookup should be open",
					"Product lookup doesn't open", "fail");
		}

		sendKeys(txt_searchProductProductLookup, invObj.readTestCreation("BatchFifoMultiPlatformProductionType"));
		String enteredProduct = getAttribute(txt_searchProductProductLookup, "value");
		if (enteredProduct.equals(invObj.readTestCreation("BatchFifoMultiPlatformProductionType"))) {
			writeTestResults("Verify that user able to enter the relevent product",
					"User should be able to enter the relevent product", "User successfully enter the relevent product",
					"pass");
		} else {
			writeTestResults("Verify that user able to enter the relevent product",
					"User should be able to enter the relevent product", "User doesn't enter the relevent product",
					"fail");
		}

		click(btn_searchProductProductLookup);
		explicitWaitUntillClickable(td_resultProductRegressionPOProductReplace.replace("product_code",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")), 60);
		if (isEnabled(td_resultProductRegressionPOProductReplace.replace("product_code",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")))) {
			writeTestResults("Verify that relevent product is available", "Relevent product should be available",
					"Relevent product successfully available", "pass");
		} else {
			writeTestResults("Verify that relevent product is available", "Relevent product should be available",
					"Relevent product not available", "fail");
		}

		doubleClick(td_resultProductRegressionPOProductReplace.replace("product_code",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")));
		Thread.sleep(2000);

		String selectedProduct = getAttribute(txt_outputProductProductInformationSectionSummaryTabPO, "value");
		if (selectedProduct.contains(invObj.readTestCreation("BatchFifoMultiPlatformProductionType"))) {
			writeTestResults("Verify that user able to select the relevent product",
					"User should be able to select the relevent product",
					"User successfully select the relevent product", "pass");
		} else {
			writeTestResults("Verify that user able to select the relevent product",
					"User should be able to select the relevent product", "User couldn't select the relevent product",
					"fail");
		}
	}

	public void productionOrder_2_PROD_E2E_002() throws Exception {
		click(btn_productionModelLookupPO);
		explicitWait(lookup_productionModel, 30);
		if (isDisplayed(lookup_productionModel)) {
			writeTestResults("Verify that Production Model lookup is open", "Production Model lookup should be open",
					"Production Model lookup successfully open", "pass");
		} else {
			writeTestResults("Verify that Production Model lookup is open", "Production Model lookup should be open",
					"Production Model lookup doesn't open", "fail");
		}

		sendKeysLookup(txt_searchProductionModel, readProductionData("ProductionModel_PROD_E2E_002"));
		String enteredProductionModel = getAttribute(txt_searchProductionModel, "value");
		if (enteredProductionModel.equals(readProductionData("ProductionModel_PROD_E2E_002"))) {
			writeTestResults("Verify that user able to enter the relevent Production Model",
					"User should be able to enter the relevent Production Model",
					"User successfully enter the relevent Production Model", "pass");
		} else {
			writeTestResults("Verify that user able to enter the relevent Production Model",
					"User should be able to enter the relevent Production Model",
					"User doesn't enter the relevent Production Model", "fail");
		}

		click(btn_searchProductionModel);
		explicitWaitUntillClickable(td_resultProductionModelDocReplace.replace("doc_no",
				readProductionData("ProductionModel_PROD_E2E_002")), 60);
		if (isEnabled(td_resultProductionModelDocReplace.replace("doc_no",
				readProductionData("ProductionModel_PROD_E2E_002")))) {
			writeTestResults("Verify that relevent Production Model is available",
					"Relevent Production Model should be available", "Relevent Production Model successfully available",
					"pass");
		} else {
			writeTestResults("Verify that relevent Production Model is available",
					"Relevent Production Model should be available", "Relevent Production Model not available", "fail");
		}

		doubleClick(td_resultProductionModelDocReplace.replace("doc_no",
				readProductionData("ProductionModel_PROD_E2E_002")));
		Thread.sleep(2000);

		selectText(drop_barcodeBookSummarySectionSummaryTabPO, barcodeBookPORegression);

		String selectedProducttionModel = getAttribute(txt_productionModelSummarySectionSummaryTabPO, "value");
		if (selectedProducttionModel.equals(readProductionData("ProductionModel_PROD_E2E_002"))) {
			writeTestResults("Verify that user able to select the relevent Production Model",
					"User should be able to select the relevent Production Model",
					"User successfully select the relevent Production Model", "pass");
		} else {
			writeTestResults("Verify that user able to select the relevent Production Model",
					"User should be able to select the relevent Production Model",
					"User couldn't select the relevent Production Model", "fail");
		}

		String loadedProductionUnit = getAttribute(txt_productionUnitSummarySectionSummaryTabPO, "value");
		if (loadedProductionUnit.contains(readProductionData("Location_PROD_E2E"))) {
			writeTestResults(
					"Verify that Production Unit auto loaded according to the Production Model in summary section",
					"Production Unit should be auto loaded according to the Production Model in summary section",
					"Production Unit successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Production Unit auto loaded according to the Production Model in summary section",
					"Production Unit should be auto loaded according to the Production Model in summary section",
					"Production Unit doesn't auto loaded according to the Production Model in summary section", "fail");
		}

		String loadedInputWare = getSelectedOptionInDropdown(drop_inputWarehouseSummarySectionSummaryTabPO);
		if (loadedInputWare.equals(inputWarehouseLI)) {
			writeTestResults(
					"Verify that Input Warehouse auto loaded according to the Production Model in summary section",
					"Input Warehouse should be auto loaded according to the Production Model in summary section",
					"Input Warehouse successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Input Warehouse auto loaded according to the Production Model in summary section",
					"Input Warehouse should be auto loaded according to the Production Model in summary section",
					"Input Warehouse doesn't auto loaded according to the Production Model in summary section", "fail");
		}

		String loadedOutputWare = getSelectedOptionInDropdown(drop_outputWarehouseSummarySectionSummaryTabPO);
		if (loadedOutputWare.equals(outputWarehouseLI)) {
			writeTestResults(
					"Verify that Output Warehouse auto loaded according to the Production Model in summary section",
					"Output Warehouse should be auto loaded according to the Production Model in summary section",
					"Output Warehouse successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Output Warehouse auto loaded according to the Production Model in summary section",
					"Output Warehouse should be auto loaded according to the Production Model in summary section",
					"Output Warehouse doesn't auto loaded according to the Production Model in summary section",
					"fail");
		}

		String loadedWIPWare = getSelectedOptionInDropdown(drop_WIPWarehouseSummarySectionSummaryTabPO);
		if (loadedWIPWare.equals(wipWarehouseLI)) {
			writeTestResults(
					"Verify that WIP Warehouse auto loaded according to the Production Model in summary section",
					"WIP Warehouse should be auto loaded according to the Production Model in summary section",
					"WIP Warehouse successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that WIP Warehouse auto loaded according to the Production Model in summary section",
					"WIP Warehouse should be auto loaded according to the Production Model in summary section",
					"WIP Warehouse doesn't auto loaded according to the Production Model in summary section", "fail");
		}

		String loadedBatchQty = getAttribute(txt_batchQtyProductInformationSectionSummaryTabPO, "value");
		if (loadedBatchQty.equals("0.00")) {
			writeTestResults("Verify that Batch Qty auto loaded as zero in product information section (0 Qty)",
					"Batch Qty should be auto loaded as zero in product information section (0 Qty)",
					"Batch Qty successfully auto loaded as zero in product information section (0 Qty)", "pass");
		} else {
			writeTestResults("Verify that Batch Qty auto loaded as zero in product information section (0 Qty)",
					"Batch Qty should be auto loaded as zero in product information section (0 Qty)",
					"Batch Qty doesn't auto loaded as zero in product information section (0 Qty)", "fail");
		}

		String loadedBoM = getText(drop_boMBoMDetailsSectionSummaryTabPO);
		if (loadedBoM.equals(readProductionData("BOM_PROD_E2E_002"))) {
			writeTestResults(
					"Verify that BoM Document Number auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number should be auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number successfully auto loaded according to the Production Model in bill of material details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoM Document Number auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number should be auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number doesn't auto loaded according to the Production Model in bill of material details section",
					"fail");
		}

		String loadedBoO = getText(div_boOBoODetailsSectionSummaryTabPO);
		if (loadedBoO.equals(readProductionData("BOO_PROD_E2E_002"))) {
			writeTestResults(
					"Verify that BoO Number auto loaded according to the Production Model in bill of operation details section",
					"BoO Number should be auto loaded according to the Production Model in bill of operation details section",
					"BoO Number successfully auto loaded according to the Production Model in bill of operation details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoO Number auto loaded according to the Production Model in bill of operation details section",
					"BoO Number should be auto loaded according to the Production Model in bill of operation details section",
					"BoO Number doesn't auto loaded according to the Production Model in bill of operation details section",
					"fail");
		}

		driver.findElement(getLocator(txt_planedQtyProductInformationSectionSummaryTabPO)).click();
		driver.findElement(getLocator(txt_planedQtyProductInformationSectionSummaryTabPO)).sendKeys(planedQtyE2E);
		String enteredPlanedQty = getAttribute(txt_planedQtyProductInformationSectionSummaryTabPO, "value");
		if (enteredPlanedQty.equals(planedQtyE2E)) {
			writeTestResults("Verify that entered Planed Qty is display successfully",
					"Entered Planed Qty should be display successfully", "Entered Planed Qty display successfully",
					"pass");
		} else {
			writeTestResults("Verify that entered Planed Qty is display successfully",
					"Entered Planed Qty should be display successfully", "Entered Planed Qty not display", "fail");
		}

		if (isSelected(chk_infinitePO)) {
			writeTestResults("Verify that Infinite auto enabled by the Production Model",
					"Infinite should  auto enabled by the Production Model",
					"Infinite successfully auto enabled by the Production Model", "pass");
		} else {
			writeTestResults("Verify that Infinite auto enabled by the Production Model",
					"Infinite should  auto enabled by the Production Model",
					"Infinite wasn't auto enabled by the Production Model", "fail");
		}

		selectText(drop_productOrderGroupSummarySectionSummaryTabPO, productionOrderGroupRegression);
		Thread.sleep(1000);
		String selectedProductionGroup = getSelectedOptionInDropdown(drop_productOrderGroupSummarySectionSummaryTabPO);
		if (selectedProductionGroup.equals(productionOrderGroupRegression)) {
			writeTestResults("Verify user able to select the Production Group",
					"User should be able to select the Production Group",
					"User successfully select the Production Group", "pass");
		} else {
			writeTestResults("Verify user able to select the Production Group",
					"User should be able to select the Production Group", "User couldn't select the Production Group",
					"fail");
		}

		sendKeys(txt_descriptionSummarySectionSummaryTabPO, productionOrderDescriptionRegression);
		String enteredDescription = getAttribute(txt_descriptionSummarySectionSummaryTabPO, "value");
		if (enteredDescription.equals(productionOrderDescriptionRegression)) {
			writeTestResults("Verify user able to enter the Description",
					"User should be able to enter the Description", "User successfully enter the Description", "pass");
		} else {
			writeTestResults("Verify user able to enter the Description",
					"User should be able to enter the Description", "User couldn't enter the Description", "fail");
		}

		click(txt_requestStartDateSheduleSectionSummaryTabPO);
		explicitWaitUntillClickable(a_requestStartDatePOWhenDuplicating, 6);
		click(a_requestStartDatePOWhenDuplicating);
		Thread.sleep(2000);
		String selectedRequestedStartDate = getAttribute(txt_requestStartDateSheduleSectionSummaryTabPO, "value");
		String currentYearMonthWithselectedDate = common.currentDate().substring(0, 8) + "01";
		if (selectedRequestedStartDate.equals(currentYearMonthWithselectedDate)) {
			writeTestResults("Verify user able to select Requested Start Date",
					"User should be able to select Requested Start Date",
					"User successfully select Requested Start Date", "pass");
		} else {
			writeTestResults("Verify user able to select Requested Start Date",
					"User should be able to select Requested Start Date", "User couldn't select Requested Start Date",
					"fail");
		}

		click(txt_requestEndDateSheduleSectionSummaryTabPO);
		explicitWaitUntillClickable(a_requestEndDatePOWhenDuplicating, 6);
		click(a_requestEndDatePOWhenDuplicating);
		Thread.sleep(2000);
		String selectedRequestedEndDate = getAttribute(txt_requestEndDateSheduleSectionSummaryTabPO, "value");
		String currentYearMonthWithselectedEndDate = common.currentDate().substring(0, 8) + "28";
		if (selectedRequestedEndDate.equals(currentYearMonthWithselectedEndDate)) {
			writeTestResults("Verify user able to select Requested End Date",
					"User should be able to select Requested End Date", "User successfully select Requested End Date",
					"pass");
		} else {
			writeTestResults("Verify user able to select Requested End Date",
					"User should be able to select Requested End Date", "User couldn't select Requested End Date",
					"fail");
		}

		click(btn_draft);
		explicitWait(header_draftedProductionOrder, 60);
		if (isDisplayed(header_draftedProductionOrder)) {
			writeTestResults("Verify user able to draft the Production Order",
					"User should be able to draft the Production Order", "User successfully draft the Production Order",
					"pass");
		} else {
			writeTestResults("Verify user able to draft the Production Order",
					"User should be able to draft the Production Order", "User couldn't draft the Production Order",
					"fail");
		}

		click(btn_release);
		explicitWait(header_releasedProductionOrder, 30);
		trackCode = getText(lbl_docNo);
		String productionOrderCode = trackCode;
		writeProductionData("ProductionOrder_PROD_E2E_002", productionOrderCode, 25);
		writeProductionData("ReleaseedPO_PRD_E2E_002", getCurrentUrl(), 26);
		if (isDisplayed(header_releasedProductionOrder)) {
			writeTestResults("Verify user able to release the Production Order",
					"User should be able to release the Production Order",
					"User successfully release the Production Order", "pass");
		} else {
			writeTestResults("Verify user able to release the Production Order",
					"User should be able to release the Production Order", "User couldn't release the Production Order",
					"fail");
		}
	}

	/* Internal Order Journey PROD_E2E_002 */
	public void internalOrderToInternalDispatchOrder_PROD_E2E_002() throws Exception {
		explicitWaitUntillClickable(img_navigationPane, 30);
		Thread.sleep(2000);
		explicitWaitUntillClickable(img_navigationPane, 30);
		click(img_navigationPane);
		explicitWaitUntillClickable(btn_inventoryModule, 30);
		click(btn_inventoryModule);
		explicitWaitUntillClickable(btn_internalOrderInventoryModule, 30);
		click(btn_internalOrderInventoryModule);

		explicitWaitUntillClickable(btn_newInternalOrder, 30);
		click(btn_newInternalOrder);

		explicitWaitUntillClickable(btn_wipRequestJourneyIO, 20);
		click(btn_wipRequestJourneyIO);

		sendKeys(txt_tiltleInternelOrder, titleInternelOrder);

		selectText(drop_toWarehouseIO, wipWarehouseLI);

		explicitWaitUntillClickable(btn_requesterIO, 10);
		click(btn_requesterIO);

		explicitWaitUntillClickable(txt_requestedIO, 30);
		Thread.sleep(2000);
		pressEnter(txt_requestedIO);
		explicitWaitUntillClickable(td_firstResultOfRequestedLookup, 25);
		doubleClick(td_firstResultOfRequestedLookup);
		Thread.sleep(2500);

		/* First Row */
		click(btn_productLookupFirstRowIO);
		explicitWaitUntillClickable(txt_searchProduct, 20);
		Thread.sleep(2000);
		sendKeysLookup(txt_searchProduct, invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialOne"));
		Thread.sleep(1000);
		pressEnter(txt_searchProduct);
		Thread.sleep(1500);
		explicitWaitUntillClickable(td_resultProductReplaceInfo.replace("changing_part",
				invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialOne")), 25);
		Thread.sleep(1000);
		doubleClick(td_resultProductReplaceInfo.replace("changing_part",
				invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialOne")));
		Thread.sleep(2500);

		click(btn_productionOrderLookupIOFirstRow);
		explicitWaitUntillClickable(txt_searchProductionOrderIO, 20);
		Thread.sleep(2000);
		sendKeysLookup(txt_searchProductionOrderIO, readProductionData("ProductionOrder_PROD_E2E_002"));
		Thread.sleep(1000);
		pressEnter(txt_searchProductionOrderIO);
		Thread.sleep(1500);
		explicitWaitUntillClickable(td_resultProductionOrderIOReplace.replace("changing_part",
				readProductionData("ProductionOrder_PROD_E2E_002")), 25);
		Thread.sleep(1000);
		doubleClick(td_resultProductionOrderIOReplace.replace("changing_part",
				readProductionData("ProductionOrder_PROD_E2E_002")));
		Thread.sleep(2500);

		selectText(drop_operationFirstRow, stepOnePO);

		sendKeys(txt_qtyFirstRow, internalOrderQtyWIPRequest);

		click(btn_addNewRowToGridIO);

		/* Second Row */
		click(btn_productLookupSecondRowIO);
		explicitWaitUntillClickable(txt_searchProduct, 20);
		Thread.sleep(2000);
		sendKeysLookup(txt_searchProduct, invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialTwo"));
		Thread.sleep(1000);
		pressEnter(txt_searchProduct);
		Thread.sleep(1500);
		explicitWaitUntillClickable(td_resultProductReplaceInfo.replace("changing_part",
				invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialTwo")), 25);
		Thread.sleep(1000);
		doubleClick(td_resultProductReplaceInfo.replace("changing_part",
				invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialTwo")));
		Thread.sleep(2500);

		click(btn_productionOrderLookupIOSecondRow);
		explicitWaitUntillClickable(txt_searchProductionOrderIO, 20);
		Thread.sleep(2000);
		sendKeysLookup(txt_searchProductionOrderIO, readProductionData("ProductionOrder_PROD_E2E_002"));
		Thread.sleep(1000);
		pressEnter(txt_searchProductionOrderIO);
		Thread.sleep(1500);
		explicitWaitUntillClickable(td_resultProductionOrderIOReplace.replace("changing_part",
				readProductionData("ProductionOrder_PROD_E2E_002")), 25);
		Thread.sleep(1000);
		doubleClick(td_resultProductionOrderIOReplace.replace("changing_part",
				readProductionData("ProductionOrder_PROD_E2E_002")));
		Thread.sleep(2500);

		selectText(drop_operationSecondRow, stepOnePO);

		sendKeys(txt_qtySecondRow, internalOrderQtyWIPRequest);

		click(btn_addNewRowToGridIO);

		/* Third Row */
		click(btn_productLookupThirdRowIO);
		explicitWaitUntillClickable(txt_searchProduct, 20);
		Thread.sleep(2000);
		sendKeysLookup(txt_searchProduct,
				invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialHasVarientProductThree"));
		Thread.sleep(1000);
		pressEnter(txt_searchProduct);
		Thread.sleep(1500);
		explicitWaitUntillClickable(
				td_resultProductReplaceInfo.replace("changing_part",
						invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialHasVarientProductThree")),
				25);
		Thread.sleep(1000);
		doubleClick(td_resultProductReplaceInfo.replace("changing_part",
				invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialHasVarientProductThree")));
		Thread.sleep(2500);

		click(btn_productionOrderLookupIOThirdRow);
		explicitWaitUntillClickable(txt_searchProductionOrderIO, 20);
		Thread.sleep(2000);
		sendKeysLookup(txt_searchProductionOrderIO, readProductionData("ProductionOrder_PROD_E2E_002"));
		Thread.sleep(1000);
		pressEnter(txt_searchProductionOrderIO);
		Thread.sleep(1500);
		explicitWaitUntillClickable(td_resultProductionOrderIOReplace.replace("changing_part",
				readProductionData("ProductionOrder_PROD_E2E_002")), 25);
		Thread.sleep(1000);
		doubleClick(td_resultProductionOrderIOReplace.replace("changing_part",
				readProductionData("ProductionOrder_PROD_E2E_002")));
		Thread.sleep(2500);

		selectText(drop_operationThirdRow, stepTwoPO);

		sendKeys(txt_qtyThirdRow, internalOrderQtyWIPRequest);

		click(btn_draft);
		explicitWait(header_draftedIO, 40);
		click(btn_release);
		explicitWait(btn_goTopage, 40);
		if (isDisplayed(btn_goTopage)) {
			writeTestResults("Verify user able to release Internal Order",
					"User should be able to release Internal Order", "User successfully release Internal Order",
					"pass");
		} else {
			writeTestResults("Verify user able to release Internal Order",
					"User should be able to release Internal Order", "User couldn't release Internal Order", "fail");
		}

		click(btn_goTopage);
		Thread.sleep(2000);
		switchWindow();

		explicitWaitUntillClickable(btn_draft, 40);

		selectText(drop_warehouseIDOFirstRow, inputWarehouseLI);
		selectText(drop_warehouseIDOSecondRow, inputWarehouseLI);
		selectText(drop_warehouseIDOThirdRow, inputWarehouseLI);

		click(btn_draft);
		explicitWait(header_draftedIDO, 40);
		click(btn_release);
		explicitWait(header_releasedIDO, 45);
		if (isDisplayedQuickCheck(header_releasedIDO)) {
			trackCode = getText(lbl_docNumber);
			writeTestResults("Verify user able to release the Internal Dispatch Order",
					"User should be able to release the Internal Dispatch Order",
					"User successfully release the Internal Dispatch Order", "pass");
		} else {
			changeCSS(btn_closeMainValidation, "z-index:-9999");
			click(btn_firstRownErrorOnGrid);
			Thread.sleep(1500);
			click(btn_firstRownErrorOnGrid);
			explicitWait(div_quantityNotAvaialableValidation, 5);
			if (isDisplayed(div_quantityNotAvaialableValidation)) {
				Thread.sleep(1500);
				((JavascriptExecutor) driver).executeScript("window.open('about:blank','_blank');");
				Thread.sleep(1000);
				switchWindow();
				openPage(siteUrlMultiPlatformm);
				purchaseOrderToInboundShipmentToPurchaseInvoice();

				pageRefersh();
				explicitWaitUntillClickable(btn_release, 40);
				click(btn_release);

				explicitWait(header_releasedIDO, 50);
				trackCode = getText(lbl_docNumber);
				if (isDisplayed(header_releasedIDO)) {
					writeTestResults("Verify user able to release the Internal Dispatch Order",
							"User should be able to release the Internal Dispatch Order",
							"User successfully release the Internal Dispatch Order", "pass");
				} else {
					writeTestResults("Verify user able to release the Internal Dispatch Order",
							"User should be able to release the Internal Dispatch Order",
							"User doesn't release the Internal Dispatch Order", "fail");
				}
			} else {
				writeTestResults("Verify user able to release the Internal Dispatch Order",
						"User should be able to release the Internal Dispatch Order",
						"User doesn't release the Internal Dispatch Order", "fail");
			}
		}
	}

	/* Shop Floor Update PROD_E2E_002 */
	public void shopFloorUpdate_PROD_E2E_002() throws InvalidFormatException, IOException, Exception {
		openPage(readProductionData("ReleaseedPO_PRD_E2E_002"));
		explicitWaitUntillClickable(btn_action2, 50);
		click(btn_action2);

		explicitWaitUntillClickable(btn_shopFloorUpdate2, 10);
		if (isDisplayed(btn_shopFloorUpdate2)) {
			writeTestResults("Verify that Shop floor update option is available under Actions",
					"Shop floor update option should be available under Actions",
					"Shop floor update option successfully available under Actions", "pass");
		} else {
			writeTestResults("Verify that Shop floor update option is available under Actions",
					"Shop floor update option should be available under Actions",
					"Shop floor update option doesn't available under Actions", "fail");
		}
		click(btn_shopFloorUpdate2);

		explicitWait(header_shopFloorUpdateWindow, 20);
		if (isDisplayed(header_shopFloorUpdateWindow)) {
			writeTestResults("Verify that shop floor update window is open", "Shop floor update window should be open",
					"Shop floor update window succeessfully open", "pass");
		} else {
			writeTestResults("Verify that shop floor update window is open", "Shop floor update window should be open",
					"Shop floor update window doesn't open", "fail");
		}

		/* First Operation */
		System.out.println("First Operation");
		click(btn_startFirstOperationSFU);
		explicitWaitUntillClickable(btn_startSecondLeveleSFU, 10);
		if (isDisplayed(btn_startSecondLeveleSFU)) {
			writeTestResults("Verify that start button is available", "Start button should be available",
					"Start button successfully available", "pass");
		} else {
			writeTestResults("Verify that start button is available", "Start button should be available",
					"Start button doesn't available", "fail");
		}

		click(btn_startSecondLeveleSFU);

		explicitWaitUntillClickable(btn_actualUpdateSFU, 10);
		if (isDisplayed(btn_actualUpdateSFU)) {
			writeTestResults("Verify that actual update option is available on shop floor update main window",
					"Actual update option should be available on shop floor update main window",
					"Actual update option successfully available on shop floor update main window", "pass");
		} else {
			writeTestResults("Verify that actual update option is available on shop floor update main window",
					"Actual update option should be available on shop floor update main window",
					"Actual update option doesn't available on shop floor update main window", "fail");
		}

		click(btn_actualUpdateSFU);

		Thread.sleep(2500);
		sendKeys(txt_outoutQuantityACtualUpdateSFU, actualQuantityOutputProductFirstOperation);
		String outputActualUpdateQuantityFirstOperation = getAttribute(txt_outoutQuantityACtualUpdateSFU, "value");
		if (outputActualUpdateQuantityFirstOperation.equals(actualQuantityOutputProductFirstOperation)) {
			writeTestResults("Verify user able to enter Output Product qty for the first operation",
					"User should be able to enter Output Product qty for the first operation",
					"User successfully enter Output Product qty for the first operation", "pass");
		} else {
			writeTestResults("Verify user able to enter Output Product qty for the first operation",
					"User should be able to enter Output Product qty for the first operation",
					"User doesn't enter Output Product qty for the first operation", "fail");
		}
		Thread.sleep(1000);

		click(btn_upateActualSFU);
		explicitWait(para_successfullValidationOutProductActualUpdate, 8);
		if (isDisplayed(para_successfullValidationOutProductActualUpdate)) {
			writeTestResults("Verify that successfully updated validation is appear for the Output Product update",
					"Successfully updated validation should appear for the Output Product update",
					"Successfully updated validation successfully appear for the Output Product update", "pass");
		} else {
			writeTestResults("Verify that successfully updated validation is appear for the Output Product update",
					"Successfully updated validation should appear for the Output Product update",
					"Successfully updated validation doesn't appear for the Output Product update", "fail");
		}
		Thread.sleep(3000);

		click(btn_tapInputProductsShopFloorUpdate);

		sendKeys(txt_actualUpdaInputProductQuantityFirstOperation, actualQuantityInputProductFirstOperation);
		String inputActualUpdateQuantityFirstOperation = getAttribute(txt_actualUpdaInputProductQuantityFirstOperation,
				"value");
		if (inputActualUpdateQuantityFirstOperation.equals(actualQuantityInputProductFirstOperation)) {
			writeTestResults("Verify user able to enter Input Product qty for the first operation - First Material",
					"User should be able to enter Input Product qty for the first operation - First Material",
					"User successfully enter Input Product qty for the first operation - First Material", "pass");
		} else {
			writeTestResults("Verify user able to enter Input Product qty for the first operation - First Material",
					"User should be able to enter Input Product qty for the first operation - First Material",
					"User doesn't enter Input Product qty for the first operation - First Material", "fail");
		}

		sendKeys(txt_actualUpdaInputProductQuantity2FirstOperation, actualQuantity2InputProductFirstOperation);
		String inputActualUpdateQuantity2FirstOperation = getAttribute(
				txt_actualUpdaInputProductQuantity2FirstOperation, "value");
		if (inputActualUpdateQuantity2FirstOperation.equals(actualQuantity2InputProductFirstOperation)) {
			writeTestResults("Verify user able to enter Input Product qty for the first operation - Second Material",
					"User should be able to enter Input Product qty for the first operation - Second Material",
					"User successfully enter Input Product qty for the first operation - Second Material", "pass");
		}

		click(btn_upateActualSFU);
		explicitWait(para_successfullValidationActualUpdate, 8);
		if (isDisplayedQuickCheck(para_successfullValidationActualUpdate)) {
			writeTestResults("Verify that successfully updated validation is appear for the Input Product update",
					"Successfully updated validation should appear for the Input Product update",
					"Successfully updated validation successfully appear for the Input Product update", "pass");
		} else {
			click(span_firstErrorOnGridInputProductActualUpdate);
			Thread.sleep(1500);
			click(span_firstErrorOnGridInputProductActualUpdate);
			explicitWait(div_notEnoughQuantityAvaialableValidation, 5);
			if (isDisplayed(div_notEnoughQuantityAvaialableValidation)) {
				Thread.sleep(1500);
				((JavascriptExecutor) driver).executeScript("window.open('about:blank','_blank');");
				Thread.sleep(1000);
				switchWindow();
				openPage(siteUrlMultiPlatformm);
				purchaseOrderToInboundShipmentToPurchaseInvoice();
				Thread.sleep(2000);
				click(btn_upateActualSFU);
				explicitWait(para_successfullValidationActualUpdate, 8);
				if (isDisplayedQuickCheck(para_successfullValidationActualUpdate)) {
					writeTestResults(
							"Verify that successfully updated validation is appear for the Input Product update",
							"Successfully updated validation should appear for the Input Product update",
							"Successfully updated validation successfully appear for the Input Product update", "pass");
				} else {
					writeTestResults(
							"Verify that successfully updated validation is appear for the Input Product update",
							"Successfully updated validation should appear for the Input Product update",
							"Successfully updated validation doesn't appear for the Input Product update", "fail");
				}
			}
		}

		Thread.sleep(3000);

		explicitWaitUntillClickable(tab_resourseShopFloorUpdate, 30);
		click(tab_resourseShopFloorUpdate);
		explicitWaitUntillClickable(div_actualHoursResourseTabRow01, 20);
		Thread.sleep(2000);
		click(txt_actualDuarationResourse01ActualUpdate);
		Thread.sleep(1500);
		click(btn_oneHouserActualDurationResourse01);
		Thread.sleep(1000);
		click(btn_okActualDurationResourse01);
		Thread.sleep(1500);

		click(txt_actualDuarationResourse02ActualUpdate);
		Thread.sleep(1500);
		click(btn_oneHouserActualDurationResourse02);
		Thread.sleep(1000);
		click(btn_okActualDurationResourse02);
		Thread.sleep(1500);

		Thread.sleep(2000);
		String actualHoursRow1 = getAttribute(div_actualHoursResourseTabRow01, "value");
		String actualminsRow1 = getAttribute(div_actualMinsResourseTabRow01, "value");
		String actualHoursRow2 = getAttribute(div_actualHoursResourseTabRow02, "value");
		String actualminsRow2 = getAttribute(div_actualMinsResourseTabRow02, "value");

		if (actualHoursRow1.equals("01") && actualminsRow1.equals("00") && actualHoursRow2.equals("01")
				&& actualminsRow2.equals("00")) {
			writeTestResults("Verify that actual resource hours are loaded correctly",
					"Actual resource hours should be loaded correctly",
					"Actual resource hours successfully loaded correctly", "pass");
		} else {
			writeTestResults("Verify that actual resource hours are loaded correctly",
					"Actual resource hours should be loaded correctly",
					"Actual resource hours doesn't loaded correctly", "fail");
		}

		click(btn_upateActualSFU);
		explicitWait(para_successfullValidationActualUpdateResourse, 8);
		if (isDisplayed(para_successfullValidationActualUpdateResourse)) {
			writeTestResults("Verify that successfully updated validation is appear for the Resource update",
					"Successfully updated validation should appear for the Resource update",
					"Successfully updated validation successfully appear for the Resource update", "pass");
		} else {
			writeTestResults("Verify that successfully updated validation is appear for the Resource update",
					"Successfully updated validation should appear for the Resource update",
					"Successfully updated validation doesn't appear for the Resource update", "fail");
		}

		Thread.sleep(3000);

		click(btn_closeFisrstOperationActualUpdateWindow);

		explicitWait(btn_completeFirstOperation, 8);
		if (isDisplayed(btn_completeFirstOperation)) {
			writeTestResults("Verify that complete button is available for the first operation",
					"Complete button should be available for the first operation",
					"Complete button successfully available for the first operation", "pass");
		} else {
			writeTestResults("Verify that complete button is available for the first operation",
					"Complete button should be available for the first operation",
					"Complete button doesn't available for the first operation", "fail");
		}
		click(btn_completeFirstOperation);

		explicitWait(btn_completeSecondLeveleSFU, 8);
		if (isDisplayed(btn_completeSecondLeveleSFU)) {
			writeTestResults("Verify that complete button is available for the complete window of first operation",
					"Complete button should be available for the complete window of first operation",
					"Complete button successfully available for the complete window of first operation", "pass");
		} else {
			writeTestResults("Verify that complete button is available for the complete window of first operation",
					"Complete button should be available for the complete window of first operation",
					"Complete button doesn't available for the complete window of first operation", "fail");
		}
		click(btn_completeSecondLeveleSFU);

		explicitWait(lbl_completedShopFloorUpdateBarFirstOperation, 8);
		if (isDisplayed(lbl_completedShopFloorUpdateBarFirstOperation)) {
			writeTestResults("Verify that shop flow update is completed for the first operation",
					"Shop flow update should be completed for the first operation",
					"Shop flow update successfully completed for the first operation", "pass");
		} else {
			writeTestResults("Verify that shop flow update is completed for the first operation",
					"Shop flow update should be completed for the first operation",
					"Shop flow update doesn't completed for the first operation", "pass");
		}

		/* Second Operation */
		System.out.println("Second Operation");

		explicitWaitUntillClickable(btn_actualUpdateSFU, 10);
		if (isDisplayed(btn_actualUpdateSFU)) {
			writeTestResults(
					"Verify that actual update option is available on shop floor update main window for second operation",
					"Actual update option should be available on shop floor update main window for second operation",
					"Actual update option successfully available on shop floor update main window for second operation",
					"pass");
		} else {
			writeTestResults(
					"Verify that actual update option is available on shop floor update main window for second operation",
					"Actual update option should be available on shop floor update main window for second operation",
					"Actual update option doesn't available on shop floor update main window for second operation",
					"fail");
		}

		click(btn_actualUpdateSFU);

		sendKeys(txt_outoutQuantityACtualUpdateSFU, actualQuantityOutputProductSecondOperation);
		String outputActualUpdateQuantitySecondOperation = getAttribute(txt_outoutQuantityACtualUpdateSFU, "value");
		if (outputActualUpdateQuantitySecondOperation.equals(actualQuantityOutputProductSecondOperation)) {
			writeTestResults("Verify user able to enter Output Product qty for the second operation",
					"User should be able to enter Output Product qty for the second operation",
					"User successfully enter Output Product qty for the second operation", "pass");
		} else {
			writeTestResults("Verify user able to enter Output Product qty for the second operation",
					"User should be able to enter Output Product qty for the second operation",
					"User doesn't enter Output Product qty for the second operation", "fail");
		}
		Thread.sleep(1000);

		click(btn_upateActualSFU);
		explicitWait(para_successfullValidationOutProductActualUpdate, 8);
		if (isDisplayed(para_successfullValidationOutProductActualUpdate)) {
			writeTestResults("Verify that successfully updated validation is appear for the Output Product update",
					"Successfully updated validation should appear for the Output Product update",
					"Successfully updated validation successfully appear for the Output Product update", "pass");
		} else {
			writeTestResults("Verify that successfully updated validation is appear for the Output Product update",
					"Successfully updated validation should appear for the Output Product update",
					"Successfully updated validation doesn't appear for the Output Product update", "fail");
		}
		Thread.sleep(3000);

		click(btn_tapInputProductsShopFloorUpdate);

		sendKeys(txt_actualUpdaInputProductQuantitySecondOperation, actualQuantityInputProductSecondOperation);
		String inputActualUpdateQuantitySecondOperation = getAttribute(
				txt_actualUpdaInputProductQuantitySecondOperation, "value");
		if (inputActualUpdateQuantitySecondOperation.equals(actualQuantityInputProductSecondOperation)) {
			writeTestResults("Verify user able to enter Input Product qty for the second operation",
					"User should be able to enter Input Product qty for the second operation",
					"User successfully enter Input Product qty for the second operation", "pass");
		} else {
			writeTestResults("Verify user able to enter Input Product qty for the second operation",
					"User should be able to enter Input Product qty for the second operation",
					"User doesn't enter Input Product qty for the second operation", "fail");
		}

		click(btn_upateActualSFU);
		explicitWait(para_successfullValidationActualUpdate, 8);
		if (isDisplayedQuickCheck(para_successfullValidationActualUpdate)) {
			writeTestResults("Verify that successfully updated validation is appear for the Input Product update",
					"Successfully updated validation should appear for the Input Product update",
					"Successfully updated validation successfully appear for the Input Product update", "pass");
		} else {
			writeTestResults("Verify that successfully updated validation is appear for the Input Product update",
					"Successfully updated validation should appear for the Input Product update",
					"Successfully updated validation doesn't appear for the Input Product update", "fail");
		}
		Thread.sleep(3000);

		click(btn_closeSecondOperationActualUpdateWindow);

		explicitWait(btn_completeSecondOperation, 8);
		if (isDisplayed(btn_completeSecondOperation)) {
			writeTestResults("Verify that complete button is available for the second operation",
					"Complete button should be available for the second operation",
					"Complete button successfully available for the second operation", "pass");
		} else {
			writeTestResults("Verify that complete button is available for the second operation",
					"Complete button should be available for the second operation",
					"Complete button doesn't available for the second operation", "fail");
		}
		click(btn_completeSecondOperation);

		explicitWait(btn_completeSecondLeveleSFU, 8);
		if (isDisplayed(btn_completeSecondLeveleSFU)) {
			writeTestResults("Verify that complete button is available for the complete window of second operation",
					"Complete button should be available for the complete window of second operation",
					"Complete button successfully available for the complete window of second operation", "pass");
		} else {
			writeTestResults("Verify that complete button is available for the complete window of second operation",
					"Complete button should be available for the complete window of second operation",
					"Complete button doesn't available for the complete window of second operation", "fail");
		}
		click(btn_completeSecondLeveleSFU);

		explicitWait(lbl_completedShopFloorUpdateBarSecondOperation, 8);
		if (isDisplayed(lbl_completedShopFloorUpdateBarSecondOperation)) {
			writeTestResults("Verify that shop flow update is completed for the second operation",
					"Shop flow update should be completed for the second operation",
					"Shop flow update successfully completed for the second operation", "pass");
		} else {
			writeTestResults("Verify that shop flow update is completed for the second operation",
					"Shop flow update should be completed for the second operation",
					"Shop flow update doesn't completed for the second operation", "pass");
		}
	}

	/* QC PROD_E2E_002 */
	public void qc_PROD_E2E_002() throws Exception {
		pageRefersh();
		explicitWait(img_navigationPane, 20);
		Thread.sleep(2000);
		explicitWait(img_navigationPane, 20);
		click(img_navigationPane);
		explicitWait(btn_productionModule, 10);
		click(btn_productionModule);
		explicitWait(btn_productionQCAndSorting, 10);
		if (isDisplayed(btn_productionQCAndSorting)) {
			writeTestResults("Verif that Production QC & Sorting option is display under Production Module",
					"Production QC & Sorting option should be display under Production Module",
					"Production QC & Sorting option successfully display under Production Module", "pass");
		} else {
			writeTestResults("Verif that Production QC & Sorting option is display under Production Module",
					"Production QC & Sorting option should be display under Production Module",
					"Production QC & Sorting option doesn't display under Production Module", "fail");
		}

		click(btn_productionQCAndSorting);

		explicitWait(drop_documentTypeProductionQC, 50);
		selectText(drop_documentTypeProductionQC, qcTypeProductionOrder);
		Thread.sleep(1000);
		String selecteDocType = getSelectedOptionInDropdown(drop_documentTypeProductionQC);
		if (selecteDocType.equals(qcTypeProductionOrder)) {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User able to select 'Production Order' as document type", "pass");
		} else {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User not able to select 'Production Order' as document type", "fail");

		}

		sendKeys(txt_docNumberQC, readProductionData("ProductionOrder_PROD_E2E_002"));
		String enteredPO = getAttribute(txt_docNumberQC, "value");
		if (enteredPO.equals(readProductionData("ProductionOrder_PROD_E2E_002"))) {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User successfully enter Production Order document number", "pass");
		} else {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User doesn't enter Production Order document number", "fail");
		}

		Thread.sleep(1000);
		driver.findElement(getLocator(txt_docNumberQC)).sendKeys(Keys.ARROW_DOWN);
		pressEnter(txt_docNumberQC);

		explicitWait(div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_002")),
				30);
		if (isDisplayed(
				div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_002")))) {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User successfully select the relevent Production Order", "pass");
		} else {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User doesn't select the relevent Production Order", "fail");
		}

		Thread.sleep(1500);
		mouseMove(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")));
		Thread.sleep(500);
		click(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")));

		if (isSelected(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")))) {
			writeTestResults("Verify user able to select the relevent Output Product",
					"User should be able to select the relevent Output Product",
					"User successfully select the relevent Output Product", "pass");
		} else {
			writeTestResults("Verify user able to select the relevent Output Product",
					"User should be able to select the relevent Output Product",
					"User doesn't select the relevent Output Product", "fail");
		}

		/* First QC Scenario */
		Thread.sleep(1500);
		sendKeys(txt_failQuantityProductionQCOneRow, firstFailQty);
		String enteredFailQty = getAttribute(txt_failQuantityProductionQCOneRow, "value");
		if (enteredFailQty.equals(firstFailQty)) {
			writeTestResults("Verify user able to enter fail qty (10)", "User should be able to enter fail qty (10)",
					"User successfully enter fail qty (10)", "pass");
		} else {
			writeTestResults("Verify user able to enter fail qty (10)", "User should be able to enter fail qty (10)",
					"User doesn't enter fail qty (10)", "fail");
		}

		click(btn_releseProductionQC);
		explicitWaitUntillPresent(para_successfullValidationProductionQC, 20);

		if (isElementPresent(para_successfullValidationProductionQC)) {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation is appear", "pass");
		} else {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation doesn't appear", "fail");
		}

		Thread.sleep(2000);
		if (isDisplayed(column_headerTotalQtyQC)) {
			writeTestResults("Verify that total qty column header is display as earlier",
					"Total qty column header should be display as earlier",
					"Total qty column header successfully display as earlier", "pass");
		} else {
			writeTestResults("Verify that total qty column header is display as earlier",
					"Total qty column header should be display as earlier",
					"Total qty column header doesn't display as earlier", "fail");
		}

		String totalQuantity = getAttribute(txt_totalQtyQC, "value");
		if (totalQuantity.equals("90.00")) {
			writeTestResults("Verify that total qty is display by deducting fail qty",
					"Total qty should display by deducting fail qty",
					"Total qty successfully display by deducting fail qty", "pass");
		} else {
			writeTestResults("Verify that total qty is display by deducting fail qty",
					"Total qty should display by deducting fail qty", "Total qty doesn't display by deducting fail qty",
					"fail");
		}

		click(tab_historyQC);
		explicitWaitUntillClickable(tab_historySelectedQC, 20);
		if (isDisplayed(tab_historySelectedQC)) {
			writeTestResults("Verify that user able to open history tab", "User should be able to open history tab",
					"User successfully open history tab", "pass");
		} else {
			writeTestResults("Verify that user able to open history tab", "User should be able to open history tab",
					"User couldn't open history tab", "fail");
		}

		explicitWait(drop_documentTypeProductionQC, 50);
		selectText(drop_documentTypeProductionQC, qcTypeProductionOrder);
		Thread.sleep(1000);
		selecteDocType = getSelectedOptionInDropdown(drop_documentTypeProductionQC);
		if (selecteDocType.equals(qcTypeProductionOrder)) {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User able to select 'Production Order' as document type", "pass");
		} else {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User not able to select 'Production Order' as document type", "fail");

		}

		sendKeys(txt_docNumberQC, readProductionData("ProductionOrder_PROD_E2E_002"));
		String enteredPO2 = getAttribute(txt_docNumberQC, "value");
		if (enteredPO2.equals(readProductionData("ProductionOrder_PROD_E2E_002"))) {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User successfully enter Production Order document number", "pass");
		} else {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User doesn't enter Production Order document number", "fail");
		}

		Thread.sleep(1000);
		driver.findElement(getLocator(txt_docNumberQC)).sendKeys(Keys.ARROW_DOWN);
		pressEnter(txt_docNumberQC);

		explicitWait(div_selectedQcDocProductionHistoryTab.replace("doc_no",
				readProductionData("ProductionOrder_PROD_E2E_002")), 30);
		if (isDisplayed(div_selectedQcDocProductionHistoryTab.replace("doc_no",
				readProductionData("ProductionOrder_PROD_E2E_002")))) {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User successfully select the relevent Production Order", "pass");
		} else {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User doesn't select the relevent Production Order", "fail");
		}

		String qcDoc = getText(div_qcDocNo);

		Thread.sleep(2000);
		if (isDisplayed(column_headerFailedQtyHistoryTabQC)) {
			writeTestResults("Verify that fail qty column header is display as earlier",
					"Fail qty column header should be display as earlier",
					"Fail qty column header successfully display as earlier", "pass");
		} else {
			writeTestResults("Verify that fail qty column header is display as earlier",
					"Fail qty column header should be display as earlier",
					"Fail qty column header doesn't display as earlier", "fail");
		}

		String failQuantity = getAttribute(txt_failedQtyQC, "value");
		if (failQuantity.equals("10.00")) {
			writeTestResults("Verify that fail qty is display in history tab", "Fail qty should display in history tab",
					"Fail qty successfully display in history tab", "pass");
		} else {
			writeTestResults("Verify that fail qty is display in history tab", "Fail qty should display in history tab",
					"Fail qty doesn't display in history tab", "fail");
		}

		click(btn_reverseQC);
		explicitWaitUntillClickable(para_successfullValidationProductionQC, 20);

		if (isDisplayed(para_successfullValidationProductionQC)) {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation is appear", "pass");
		} else {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation doesn't appear", "fail");
		}

		waitUntillNotVisible(div_selectedQcDocProduction.replace("doc_no", qcDoc), 15);
		if (!isDisplayedQuickCheck(div_selectedQcDocProduction.replace("doc_no", qcDoc))) {
			writeTestResults("Verify that relevent Production Order is withdraw from history tab",
					"Relevent Production Order should be withdraw from history tab",
					"Relevent Production Order successfully withdraw from history tab", "pass");
		} else {
			writeTestResults("Verify that relevent Production Order is withdraw from history tab",
					"Relevent Production Order should be withdraw from history tab",
					"Relevent Production Order still available on history tab", "fail");
		}

		click(tab_pendingQC);
		explicitWaitUntillClickable(tab_pendingSelectedQC, 20);
		if (isDisplayed(tab_pendingSelectedQC)) {
			writeTestResults("Verify that user able to open pending tab", "User should be able to open pending tab",
					"User successfully open pending tab", "pass");
		} else {
			writeTestResults("Verify that user able to open pending tab", "User should be able to open pending tab",
					"User couldn't open pending tab", "fail");
		}

		explicitWait(drop_documentTypeProductionQC, 50);
		selectText(drop_documentTypeProductionQC, qcTypeProductionOrder);
		Thread.sleep(1000);
		selecteDocType = getSelectedOptionInDropdown(drop_documentTypeProductionQC);
		if (selecteDocType.equals(qcTypeProductionOrder)) {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User able to select 'Production Order' as document type", "pass");
		} else {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User not able to select 'Production Order' as document type", "fail");

		}

		sendKeys(txt_docNumberQC, readProductionData("ProductionOrder_PROD_E2E_002"));
		String enteredPO3 = getAttribute(txt_docNumberQC, "value");
		if (enteredPO3.equals(readProductionData("ProductionOrder_PROD_E2E_002"))) {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User successfully enter Production Order document number", "pass");
		} else {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User doesn't enter Production Order document number", "fail");
		}

		Thread.sleep(1000);
		driver.findElement(getLocator(txt_docNumberQC)).sendKeys(Keys.ARROW_DOWN);
		pressEnter(txt_docNumberQC);

		explicitWait(div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_002")),
				30);
		if (isDisplayed(
				div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_002")))) {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User successfully select the relevent Production Order", "pass");
		} else {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User doesn't select the relevent Production Order", "fail");
		}

		String totalQuantityAfterReverse = getAttribute(txt_totalQtyQC, "value");
		if (totalQuantityAfterReverse.equals("100.00")) {
			writeTestResults("Verify that total qty is display by adding reverse qty",
					"Total qty should display by adding reverse qty",
					"Total qty successfully display by adding reverse qty", "pass");
		} else {
			writeTestResults("Verify that total qty is display by adding reverse qty",
					"Total qty should display by adding reverse qty", "Total qty doesn't display by adding reverse qty",
					"fail");
		}

		/* Second QC Scenario */
		mouseMove(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")));
		click(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")));

		if (isSelected(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")))) {
			writeTestResults("Verify user able to select the relevent Output Product",
					"User should be able to select the relevent Output Product",
					"User successfully select the relevent Output Product", "pass");
		} else {
			writeTestResults("Verify user able to select the relevent Output Product",
					"User should be able to select the relevent Output Product",
					"User doesn't select the relevent Output Product", "fail");
		}

		Thread.sleep(1500);
		sendKeys(txt_failQuantityProductionQCOneRow, secondFailQty);
		String enteredFailQty2 = getAttribute(txt_failQuantityProductionQCOneRow, "value");
		if (enteredFailQty2.equals(secondFailQty)) {
			writeTestResults("Verify user able to enter fail qty (100)", "User should be able to enter fail qty (100)",
					"User successfully enter fail qty (100)", "pass");
		} else {
			writeTestResults("Verify user able to enter fail qty (100)", "User should be able to enter fail qty (100)",
					"User doesn't enter fail qty (100)", "fail");
		}

		click(btn_releseProductionQC);
		explicitWaitUntillPresent(para_successfullValidationProductionQC, 20);

		if (isElementPresent(para_successfullValidationProductionQC)) {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation is appear", "pass");
		} else {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation doesn't appear", "fail");
		}

		Thread.sleep(2000);
		waitUntillNotVisible(
				div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_002")), 15);
		if (!isDisplayedQuickCheck(
				div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_002")))) {
			writeTestResults("Verify that relevent Production Order is withdraw from pending tab",
					"Relevent Production Order should be withdraw from pending tab",
					"Relevent Production Order successfully withdraw from pending tab", "pass");
		} else {
			writeTestResults("Verify that relevent Production Order is withdraw from pending tab",
					"Relevent Production Order should be withdraw from pending tab",
					"Relevent Production Order still available on pending tab", "fail");
		}

		click(tab_historyQC);
		explicitWaitUntillClickable(tab_historySelectedQC, 20);
		if (isDisplayed(tab_historySelectedQC)) {
			writeTestResults("Verify that user able to open history tab", "User should be able to open history tab",
					"User successfully open history tab", "pass");
		} else {
			writeTestResults("Verify that user able to open history tab", "User should be able to open history tab",
					"User couldn't open history tab", "fail");
		}

		explicitWait(drop_documentTypeProductionQC, 50);
		selectText(drop_documentTypeProductionQC, qcTypeProductionOrder);
		Thread.sleep(1000);
		selecteDocType = getSelectedOptionInDropdown(drop_documentTypeProductionQC);
		if (selecteDocType.equals(qcTypeProductionOrder)) {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User able to select 'Production Order' as document type", "pass");
		} else {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User not able to select 'Production Order' as document type", "fail");

		}

		sendKeys(txt_docNumberQC, readProductionData("ProductionOrder_PROD_E2E_002"));
		String enteredPO4 = getAttribute(txt_docNumberQC, "value");
		if (enteredPO4.equals(readProductionData("ProductionOrder_PROD_E2E_002"))) {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User successfully enter Production Order document number", "pass");
		} else {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User doesn't enter Production Order document number", "fail");
		}

		Thread.sleep(1000);
		driver.findElement(getLocator(txt_docNumberQC)).sendKeys(Keys.ARROW_DOWN);
		pressEnter(txt_docNumberQC);

		explicitWait(div_selectedQcDocProductionHistoryTab.replace("doc_no",
				readProductionData("ProductionOrder_PROD_E2E_002")), 30);
		if (isDisplayed(div_selectedQcDocProductionHistoryTab.replace("doc_no",
				readProductionData("ProductionOrder_PROD_E2E_002")))) {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User successfully select the relevent Production Order", "pass");
		} else {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User doesn't select the relevent Production Order", "fail");
		}

		String qcDoc2 = getText(div_qcDocNo);

		Thread.sleep(2000);
		if (isDisplayed(column_headerFailedQtyHistoryTabQC)) {
			writeTestResults("Verify that fail qty column header is display as earlier",
					"Fail qty column header should be display as earlier",
					"Fail qty column header successfully display as earlier", "pass");
		} else {
			writeTestResults("Verify that fail qty column header is display as earlier",
					"Fail qty column header should be display as earlier",
					"Fail qty column header doesn't display as earlier", "fail");
		}

		String failQuantity2 = getAttribute(txt_failedQtyQC, "value");
		if (failQuantity2.equals("100.00")) {
			writeTestResults("Verify that fail qty is display in history tab", "Fail qty should display in history tab",
					"Fail qty successfully display in history tab", "pass");
		} else {
			writeTestResults("Verify that fail qty is display in history tab", "Fail qty should display in history tab",
					"Fail qty doesn't display in history tab", "fail");
		}

		click(btn_reverseQC);
		explicitWaitUntillClickable(para_successfullValidationProductionQC, 20);

		if (isDisplayed(para_successfullValidationProductionQC)) {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation is appear", "pass");
		} else {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation doesn't appear", "fail");
		}

		waitUntillNotVisible(div_selectedQcDocProduction.replace("doc_no", qcDoc2), 15);
		if (!isDisplayedQuickCheck(div_selectedQcDocProduction.replace("doc_no", qcDoc2))) {
			writeTestResults("Verify that relevent Production Order is withdraw from history tab",
					"Relevent Production Order should be withdraw from history tab",
					"Relevent Production Order successfully withdraw from history tab", "pass");
		} else {
			writeTestResults("Verify that relevent Production Order is withdraw from history tab",
					"Relevent Production Order should be withdraw from history tab",
					"Relevent Production Order still available on history tab", "fail");
		}

		click(tab_pendingQC);
		explicitWaitUntillClickable(tab_pendingSelectedQC, 20);
		if (isDisplayed(tab_pendingSelectedQC)) {
			writeTestResults("Verify that user able to open pending tab", "User should be able to open pending tab",
					"User successfully open pending tab", "pass");
		} else {
			writeTestResults("Verify that user able to open pending tab", "User should be able to open pending tab",
					"User couldn't open pending tab", "fail");
		}

		explicitWait(drop_documentTypeProductionQC, 50);
		selectText(drop_documentTypeProductionQC, qcTypeProductionOrder);
		Thread.sleep(1000);
		selecteDocType = getSelectedOptionInDropdown(drop_documentTypeProductionQC);
		if (selecteDocType.equals(qcTypeProductionOrder)) {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User able to select 'Production Order' as document type", "pass");
		} else {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User not able to select 'Production Order' as document type", "fail");

		}

		sendKeys(txt_docNumberQC, readProductionData("ProductionOrder_PROD_E2E_002"));
		String enteredPO5 = getAttribute(txt_docNumberQC, "value");
		if (enteredPO5.equals(readProductionData("ProductionOrder_PROD_E2E_002"))) {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User successfully enter Production Order document number", "pass");
		} else {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User doesn't enter Production Order document number", "fail");
		}

		Thread.sleep(1000);
		driver.findElement(getLocator(txt_docNumberQC)).sendKeys(Keys.ARROW_DOWN);
		pressEnter(txt_docNumberQC);

		explicitWait(div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_002")),
				30);
		if (isDisplayed(
				div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_002")))) {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User successfully select the relevent Production Order", "pass");
		} else {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User doesn't select the relevent Production Order", "fail");
		}

		String totalQuantityAfterReverse2 = getAttribute(txt_totalQtyQC, "value");
		if (totalQuantityAfterReverse2.equals("100.00")) {
			writeTestResults("Verify that total qty is display after reverse the toatal qty",
					"Total qty should display after reverse the toatal qty",
					"Total qty successfully display after reverse the toatal qty", "pass");
		} else {
			writeTestResults("Verify that total qty is display after reverse the toatal qty",
					"Total qty should display after reverse the toatal qty",
					"Total qty doesn't display after reverse the toatal qty", "fail");
		}

		/* Third QC Scenario */
		mouseMove(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")));
		click(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")));

		if (isSelected(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")))) {
			writeTestResults("Verify user able to select the relevent Output Product",
					"User should be able to select the relevent Output Product",
					"User successfully select the relevent Output Product", "pass");
		} else {
			writeTestResults("Verify user able to select the relevent Output Product",
					"User should be able to select the relevent Output Product",
					"User doesn't select the relevent Output Product", "fail");
		}

		Thread.sleep(1500);
		sendKeys(txt_passQuantityProductionQCOneRow, thirdPassQty);
		String enteredPassQty = getAttribute(txt_passQuantityProductionQCOneRow, "value");
		if (enteredPassQty.equals(thirdPassQty)) {
			writeTestResults("Verify user able to enter pass qty (70)", "User should be able to enter pass qty (70)",
					"User successfully enter pass qty (70)", "pass");
		} else {
			writeTestResults("Verify user able to enter pass qty (70)", "User should be able to enter pass qty (70)",
					"User doesn't enter pass qty (70)", "fail");
		}
		Thread.sleep(1000);

		sendKeys(txt_failQuantityProductionQCOneRow, thirdFailQty);
		String enteredFailQty3 = getAttribute(txt_failQuantityProductionQCOneRow, "value");
		if (enteredFailQty3.equals(thirdFailQty)) {
			writeTestResults("Verify user able to enter fail qty (30)", "User should be able to enter fail qty (30)",
					"User successfully enter fail qty (30)", "pass");
		} else {
			writeTestResults("Verify user able to enter fail qty (30)", "User should be able to enter fail qty (30)",
					"User doesn't enter fail qty (30)", "fail");
		}

		click(btn_releseProductionQC);
		explicitWaitUntillPresent(para_successfullValidationProductionQC, 20);

		if (isElementPresent(para_successfullValidationProductionQC)) {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation is appear", "pass");
		} else {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation doesn't appear", "fail");
		}

		Thread.sleep(2000);
		waitUntillNotVisible(
				div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_002")), 15);
		if (!isDisplayedQuickCheck(
				div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_002")))) {
			writeTestResults("Verify that relevent Production Order is withdraw from pending tab",
					"Relevent Production Order should be withdraw from pending tab",
					"Relevent Production Order successfully withdraw from pending tab", "pass");
		} else {
			writeTestResults("Verify that relevent Production Order is withdraw from pending tab",
					"Relevent Production Order should be withdraw from pending tab",
					"Relevent Production Order still available on pending tab", "fail");
		}

		click(tab_historyQC);
		explicitWaitUntillClickable(tab_historySelectedQC, 20);
		if (isDisplayed(tab_historySelectedQC)) {
			writeTestResults("Verify that user able to open history tab", "User should be able to open history tab",
					"User successfully open history tab", "pass");
		} else {
			writeTestResults("Verify that user able to open history tab", "User should be able to open history tab",
					"User couldn't open history tab", "fail");
		}

		explicitWait(drop_documentTypeProductionQC, 50);
		selectText(drop_documentTypeProductionQC, qcTypeProductionOrder);
		Thread.sleep(1000);
		selecteDocType = getSelectedOptionInDropdown(drop_documentTypeProductionQC);
		if (selecteDocType.equals(qcTypeProductionOrder)) {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User able to select 'Production Order' as document type", "pass");
		} else {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User not able to select 'Production Order' as document type", "fail");

		}

		sendKeys(txt_docNumberQC, readProductionData("ProductionOrder_PROD_E2E_002"));
		String enteredPO6 = getAttribute(txt_docNumberQC, "value");
		if (enteredPO6.equals(readProductionData("ProductionOrder_PROD_E2E_002"))) {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User successfully enter Production Order document number", "pass");
		} else {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User doesn't enter Production Order document number", "fail");
		}

		Thread.sleep(1000);
		driver.findElement(getLocator(txt_docNumberQC)).sendKeys(Keys.ARROW_DOWN);
		pressEnter(txt_docNumberQC);

		explicitWait(div_selectedQcDocProductionHistoryTab.replace("doc_no",
				readProductionData("ProductionOrder_PROD_E2E_002")), 30);
		if (isDisplayed(div_selectedQcDocProductionHistoryTab.replace("doc_no",
				readProductionData("ProductionOrder_PROD_E2E_002")))) {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User successfully select the relevent Production Order", "pass");
		} else {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User doesn't select the relevent Production Order", "fail");
		}

		Thread.sleep(2000);
		if (isDisplayed(column_headerPassedQtyHistoryTabQC)) {
			writeTestResults("Verify that pass qty column header is display as earlier",
					"Pass qty column header should be display as earlier",
					"Pass qty column header successfully display as earlier", "pass");
		} else {
			writeTestResults("Verify that pass qty column header is display as earlier",
					"Pass qty column header should be display as earlier",
					"Pass qty column header doesn't display as earlier", "fail");
		}

		String passQuantity = getAttribute(txt_passedQtyQC, "value");
		if (passQuantity.equals("70.00")) {
			writeTestResults("Verify that pass qty is display in history tab", "Pass qty should display in history tab",
					"Pass qty successfully display in history tab", "pass");
		} else {
			writeTestResults("Verify that pass qty is display in history tab", "Pass qty should display in history tab",
					"Pass qty doesn't display in history tab", "fail");
		}

		Thread.sleep(1000);
		if (isDisplayed(column_headerFailedQtyHistoryTabQC)) {
			writeTestResults("Verify that fail qty column header is display as earlier",
					"Fail qty column header should be display as earlier",
					"Fail qty column header successfully display as earlier", "pass");
		} else {
			writeTestResults("Verify that fail qty column header is display as earlier",
					"Fail qty column header should be display as earlier",
					"Fail qty column header doesn't display as earlier", "fail");
		}

		String failQuantity3 = getAttribute(txt_failedQtyQC, "value");
		if (failQuantity3.equals("30.00")) {
			writeTestResults("Verify that fail qty is display in history tab", "Fail qty should display in history tab",
					"Fail qty successfully display in history tab", "pass");
		} else {
			writeTestResults("Verify that fail qty is display in history tab", "Fail qty should display in history tab",
					"Fail qty doesn't display in history tab", "fail");
		}
	}

	/* Costing PROD_E2E_002 */
	public void productionCosting_PROD_E2E_002() throws Exception {
		openPage(readProductionData("ReleaseedPO_PRD_E2E_002"));
		explicitWaitUntillClickable(btn_action2, 50);
		click(btn_action2);

		explicitWaitUntillClickable(btn_productionCostingActions, 10);
		if (isDisplayed(btn_productionCostingActions)) {
			writeTestResults("Verify that Production Costing option is available under Actions",
					"Production Costing option should be available under Actions",
					"Production Costing option successfully available under Actions", "pass");
		} else {
			writeTestResults("Verify that Production Costing option is available under Actions",
					"Production Costing option should be available under Actions",
					"Production Costing option doesn't available under Actions", "fail");
		}
		click(btn_productionCostingActions);

		explicitWait(header_productionCostingWindow, 20);
		if (isDisplayed(header_productionCostingWindow)) {
			writeTestResults("Verify that production costing window is open",
					"Production costing window should be open", "Production costing window succeessfully open", "pass");
		} else {
			writeTestResults("Verify that production costing window is open",
					"Production costing window should be open", "Production costing window doesn't open", "fail");
		}

		explicitWaitUntillClickable(chk_pendingProductionCosting, 20);
		click(chk_pendingProductionCosting);

		if (isDisplayed(chk_pendingProductionCostingSelected)) {
			writeTestResults("Verify user able to select batch costing checkbox on production costing main window",
					"User should be able to select batch costing checkbox on production costing main window",
					"User able to select batch costing checkbox on production costing main window", "pass");
		} else {
			writeTestResults("Verify user able to select batch costing checkbox on production costing main window",
					"User should be able to select batch costing checkbox on production costing main window",
					"User not able to select batch costing checkbox on production costing main window", "fail");
		}

		click(btn_updateProductionCostingMainWindow);

		boolean visibilityFlag = false;
		for (int i = 1; i < 10; i++) {
			try {
				Thread.sleep(20);
				driver.findElement(getLocator(para_batchCostingUpdateValidation)).isDisplayed();
				visibilityFlag = true;
				break;
			} catch (Exception e) {
				continue;
			}
		}

		if (visibilityFlag) {
			writeTestResults("Verify that succesfull validation is appear for Batch Costing",
					"Succesfull validation should appear for Batch Costing",
					"Succesfull validation is appear for Batch Costing", "pass");
		} else {
			writeTestResults("Verify that succesfull validation is appear for Batch Costing",
					"Succesfull validation should appear for Batch Costing",
					"Succesfull validation doesn't appear for Batch Costing", "fail");
		}

//		WebElement element = wait.until(new Function<WebDriver, WebElement>() {
//			public WebElement apply(WebDriver driver) {
//				boolean visibilityOfValidation = false;
//				WebElement linkElement = driver.findElement(By.xpath(para_batchCostingUpdateValidation));
//				if (linkElement.isDisplayed()) {
//					visibilityOfValidation = true;
//				}
//
//				try {
//					if (visibilityOfValidation) {
//						writeTestResults("Verify that succesfull validation is appear for Batch Costing",
//								"Succesfull validation should appear for Batch Costing",
//								"Succesfull validation is appear for Batch Costing", "pass");
//					} else {
//						writeTestResults("Verify that succesfull validation is appear for Batch Costing",
//								"Succesfull validation should appear for Batch Costing",
//								"Succesfull validation doesn't appear for Batch Costing", "fail");
//					}
//
//				} catch (Exception e) {
//
//				}
//
//				return linkElement;
//
//			}
//		});

//		if (isDisplayedQuickCheck(para_batchCostingUpdateValidation)) {
//			writeTestResults("Verify that succesfull validation is appear for Batch Costing",
//					"Succesfull validation should appear for Batch Costing",
//					"Succesfull validation is appear for Batch Costing", "pass");
//		} else {
//			writeTestResults("Verify that succesfull validation is appear for Batch Costing",
//					"Succesfull validation should appear for Batch Costing",
//					"Succesfull validation doesn't appear for Batch Costing", "fail");
//		}

		explicitWait(btn_closeMainCostingUpdateWindowProductionOrder, 5);
		Thread.sleep(2500);
		click(btn_closeMainCostingUpdateWindowProductionOrder);

		explicitWait(header_releasedProductionOrder, 10);

		/* Costing Summary - Actual */
		explicitWaitUntillClickable(tab_costingSummaryPO, 60);
		click(tab_costingSummaryPO);
		explicitWaitUntillClickable(btn_expandCostingSummary, 20);

		click(btn_expandCostingSummary);
		Thread.sleep(500);
		explicitWaitUntillClickable(btn_expandCostingSummary, 20);
		click(btn_expandCostingSummary);
		explicitWaitUntillClickable(btn_expandCostingSummary, 20);
		click(btn_expandCostingSummary);
		explicitWaitUntillClickable(btn_expandCostingSummary, 20);
		click(btn_expandCostingSummary);

		if (isDisplayed(column_headerActualCostingSummary)) {
			writeTestResults("Verify that the Actual Column is display same as earlier",
					"The Actual Column should display same as earlier",
					"The Actual Column successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that the Actual Column is display same as earlier",
					"The Actual Column should display same as earlier",
					"The Actual Column doesn't display same as earlier", "fail");
		}

		if (isDisplayed(td_resourceCostActualCostingSummary)) {
			writeTestResults("Verify that the Resource Total Cost is display correctly [Actual Column]",
					"The Resource Total Cost should display correctly [Actual Column]",
					"The Resource Total Cost successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Resource Total Cost is display correctly [Actual Column]",
					"The Resource Total Cost should display correctly [Actual Column]",
					"The Resource Total Cost doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_resourceCost1ActualCostingSummary)) {
			writeTestResults("Verify that the Cost of Resource 01 is display correctly [Actual Column]",
					"The Cost of Resource 01 should display correctly [Actual Column]",
					"The Cost of Resource 01 successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Resource 01 is display correctly [Actual Column]",
					"The Cost of Resource 01 should display correctly [Actual Column]",
					"The Cost of Resource 01 doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_resourceCost2ActualCostingSummary)) {
			writeTestResults("Verify that the Cost of Resource 02 is display correctly [Actual Column]",
					"The Cost of Resource 02 should display correctly [Actual Column]",
					"The Cost of Resource 02 successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Resource 02 is display correctly [Actual Column]",
					"The Cost of Resource 02 should display correctly [Actual Column]",
					"The Cost of Resource 02 doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_materialCostActualCostingSummary)) {
			writeTestResults("Verify that the Material Total Cost is display correctly [Actual Column]",
					"The Material Total Cost should display correctly [Actual Column]",
					"The Material Total Cost successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Material Total Cost is display correctly [Actual Column]",
					"The Material Total Cost should display correctly [Actual Column]",
					"The Material Total Cost doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_material1CostActualCostingSummary)) {
			writeTestResults("Verify that the Cost of Material 01 is display correctly [Actual Column]",
					"The Cost of Material 01 should display correctly [Actual Column]",
					"The Cost of Material 01 successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Material 01 is display correctly [Actual Column]",
					"The Cost of Material 01 should display correctly [Actual Column]",
					"The Cost of Material 01 doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_material2CostActualCostingSummary)) {
			writeTestResults("Verify that the Cost of Material 02 is display correctly [Actual Column]",
					"The Cost of Material 02 should display correctly [Actual Column]",
					"The Cost of Material 02 successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Material 02 is display correctly [Actual Column]",
					"The Cost of Material 02 should display correctly [Actual Column]",
					"The Cost of Material 02 doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_material3CostActualCostingSummary)) {
			writeTestResults("Verify that the Cost of Material 03 is display correctly [Actual Column]",
					"The Cost of Material 03 should display correctly [Actual Column]",
					"The Cost of Material 03 successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Material 03 is display correctly [Actual Column]",
					"The Cost of Material 03 should display correctly [Actual Column]",
					"The Cost of Material 03 doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_overheadCostActualCostingSummary)) {
			writeTestResults("Verify that the Overhead Total Cost is display correctly [Actual Column]",
					"The Overhead Total Cost should display correctly [Actual Column]",
					"The Overhead Total Cost successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Overhead Total Cost is display correctly [Actual Column]",
					"The Overhead Total Cost should display correctly [Actual Column]",
					"The Overhead Total Cost doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_overhead1CostActualCostingSummary)) {
			writeTestResults("Verify that the Cost of Overhead 01 is display correctly [Actual Column]",
					"The Cost of Overhead 01 should display correctly [Actual Column]",
					"The Cost of Overhead 01 successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead 01 is display correctly [Actual Column]",
					"The Cost of Overhead 01 should display correctly [Actual Column]",
					"The Cost of Overhead 01 doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_overhead2CostActualCostingSummary)) {
			writeTestResults("Verify that the Cost of Overhead 02 is display correctly [Actual Column]",
					"The Cost of Overhead 02 should display correctly [Actual Column]",
					"The Cost of Overhead 02 successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead 02 is display correctly [Actual Column]",
					"The Cost of Overhead 02 should display correctly [Actual Column]",
					"The Cost of Overhead 02 doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_overhead3CostActualCostingSummary)) {
			writeTestResults("Verify that the Cost of Overhead 03 is display correctly [Actual Column]",
					"The Cost of Overhead 03 should display correctly [Actual Column]",
					"The Cost of Overhead 03 successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead 03 is display correctly [Actual Column]",
					"The Cost of Overhead 03 should display correctly [Actual Column]",
					"The Cost of Overhead 03 doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_totalCostActualCostingSummary)) {
			writeTestResults("Verify that the Total Cost is display correctly [Actual Column]",
					"The Total Cost should display correctly [Actual Column]",
					"The Total Cost successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Total Cost is display correctly [Actual Column]",
					"The Total Cost should display correctly [Actual Column]",
					"The Total Cost doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_unitCostActualCostingSummary)) {
			writeTestResults("Verify that the Unit Cost is display correctly [Actual Column]",
					"The Unit Cost should display correctly [Actual Column]",
					"The Unit Cost successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Unit Cost is display correctly [Actual Column]",
					"The Unit Cost should display correctly [Actual Column]",
					"The Unit Cost doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_unitCostResourseActualCostingSummary)) {
			writeTestResults("Verify that the Cost of Resource (Unit Cost) is display correctly [Actual Column]",
					"The Cost of Resource (Unit Cost) should display correctly [Actual Column]",
					"The Cost of Resource (Unit Cost) successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Resource (Unit Cost) is display correctly [Actual Column]",
					"The Cost of Resource (Unit Cost) should display correctly [Actual Column]",
					"The Cost of Resource (Unit Cost) doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_unitCostMateraialActualCostingSummary)) {
			writeTestResults("Verify that the Cost of Material (Unit Cost) is display correctly [Actual Column]",
					"The Cost of Material (Unit Cost) should display correctly [Actual Column]",
					"The Cost of Material (Unit Cost) successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Material (Unit Cost) is display correctly [Actual Column]",
					"The Cost of Material (Unit Cost) should display correctly [Actual Column]",
					"The Cost of Material (Unit Cost) doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_unitCostOverheadActualCostingSummary)) {
			writeTestResults("Verify that the Cost of Overhead (Unit Cost) is display correctly [Actual Column]",
					"The Cost of Overhead (Unit Cost) should display correctly [Actual Column]",
					"The Cost of Overhead (Unit Cost) successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead (Unit Cost) is display correctly [Actual Column]",
					"The Cost of Overhead (Unit Cost) should display correctly [Actual Column]",
					"The Cost of Overhead (Unit Cost) doesn't display correctly [Actual Column]", "fail");
		}

		/* Costing Summary - Base Amount */
		if (isDisplayed(column_headerBaseAmountCostingSummary)) {
			writeTestResults("Verify that the Base Amount Column is display same as earlier",
					"The Base Amount Column should display same as earlier",
					"The Base Amount Column successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that the Base Amount Column is display same as earlier",
					"The Base Amount Column should display same as earlier",
					"The Base Amount Column doesn't display same as earlier", "fail");
		}

		if (isDisplayed(td_resourceCostBaseAmountCostingSummary)) {
			writeTestResults("Verify that the Resource Total Cost is display correctly [Base Amount Column]",
					"The Resource Total Cost should display correctly [Base Amount Column]",
					"The Resource Total Cost successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Resource Total Cost is display correctly [Base Amount Column]",
					"The Resource Total Cost should display correctly [Base Amount Column]",
					"The Resource Total Cost doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_resourceCost1BaseAmountCostingSummary)) {
			writeTestResults("Verify that the Cost of Resource 01 is display correctly [Base Amount Column]",
					"The Cost of Resource 01 should display correctly [Base Amount Column]",
					"The Cost of Resource 01 successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Resource 01 is display correctly [Base Amount Column]",
					"The Cost of Resource 01 should display correctly [Base Amount Column]",
					"The Cost of Resource 01 doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_resourceCost2BaseAmountCostingSummary)) {
			writeTestResults("Verify that the Cost of Resource 02 is display correctly [Base Amount Column]",
					"The Cost of Resource 02 should display correctly [Base Amount Column]",
					"The Cost of Resource 02 successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Resource 02 is display correctly [Base Amount Column]",
					"The Cost of Resource 02 should display correctly [Base Amount Column]",
					"The Cost of Resource 02 doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_materialCostBaseAmountCostingSummary)) {
			writeTestResults("Verify that the Material Total Cost is display correctly [Base Amount Column]",
					"The Material Total Cost should display correctly [Base Amount Column]",
					"The Material Total Cost successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Material Total Cost is display correctly [Base Amount Column]",
					"The Material Total Cost should display correctly [Base Amount Column]",
					"The Material Total Cost doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_material1CostBaseAmountCostingSummary)) {
			writeTestResults("Verify that the Cost of Material 01 is display correctly [Base Amount Column]",
					"The Cost of Material 01 should display correctly [Base Amount Column]",
					"The Cost of Material 01 successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Material 01 is display correctly [Base Amount Column]",
					"The Cost of Material 01 should display correctly [Base Amount Column]",
					"The Cost of Material 01 doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_material2CostBaseAmountCostingSummary)) {
			writeTestResults("Verify that the Cost of Material 02 is display correctly [Base Amount Column]",
					"The Cost of Material 02 should display correctly [Base Amount Column]",
					"The Cost of Material 02 successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Material 02 is display correctly [Base Amount Column]",
					"The Cost of Material 02 should display correctly [Base Amount Column]",
					"The Cost of Material 02 doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_material3CostBaseAmountCostingSummary)) {
			writeTestResults("Verify that the Cost of Material 03 is display correctly [Base Amount Column]",
					"The Cost of Material 03 should display correctly [Base Amount Column]",
					"The Cost of Material 03 successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Material 03 is display correctly [Base Amount Column]",
					"The Cost of Material 03 should display correctly [Base Amount Column]",
					"The Cost of Material 03 doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_overheadCostBaseAmountCostingSummary)) {
			writeTestResults("Verify that the Overhead Total Cost is display correctly [Base Amount Column]",
					"The Overhead Total Cost should display correctly [Base Amount Column]",
					"The Overhead Total Cost successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Overhead Total Cost is display correctly [Base Amount Column]",
					"The Overhead Total Cost should display correctly [Base Amount Column]",
					"The Overhead Total Cost doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_overhead1CostBaseAmountCostingSummary)) {
			writeTestResults("Verify that the Cost of Overhead 01 is display correctly [Base Amount Column]",
					"The Cost of Overhead 01 should display correctly [Base Amount Column]",
					"The Cost of Overhead 01 successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead 01 is display correctly [Base Amount Column]",
					"The Cost of Overhead 01 should display correctly [Base Amount Column]",
					"The Cost of Overhead 01 doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_overhead2CostBaseAmountCostingSummary)) {
			writeTestResults("Verify that the Cost of Overhead 02 is display correctly [Base Amount Column]",
					"The Cost of Overhead 02 should display correctly [Base Amount Column]",
					"The Cost of Overhead 02 successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead 02 is display correctly [Base Amount Column]",
					"The Cost of Overhead 02 should display correctly [Base Amount Column]",
					"The Cost of Overhead 02 doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_overhead3CostBaseAmountCostingSummary)) {
			writeTestResults("Verify that the Cost of Overhead 03 is display correctly [Base Amount Column]",
					"The Cost of Overhead 03 should display correctly [Base Amount Column]",
					"The Cost of Overhead 03 successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead 03 is display correctly [Base Amount Column]",
					"The Cost of Overhead 03 should display correctly [Base Amount Column]",
					"The Cost of Overhead 03 doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_totalCostBaseAmountCostingSummary)) {
			writeTestResults("Verify that the Total Cost is display correctly [Base Amount Column]",
					"The Total Cost should display correctly [Base Amount Column]",
					"The Total Cost successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Total Cost is display correctly [Base Amount Column]",
					"The Total Cost should display correctly [Base Amount Column]",
					"The Total Cost doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_unitCostBaseAmountCostingSummary)) {
			writeTestResults("Verify that the Unit Cost is display correctly [Base Amount Column]",
					"The Unit Cost should display correctly [Base Amount Column]",
					"The Unit Cost successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Unit Cost is display correctly [Base Amount Column]",
					"The Unit Cost should display correctly [Base Amount Column]",
					"The Unit Cost doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_unitCostResourseBaseAmountCostingSummary)) {
			writeTestResults("Verify that the Cost of Resource (Unit Cost) is display correctly [Base Amount Column]",
					"The Cost of Resource (Unit Cost) should display correctly [Base Amount Column]",
					"The Cost of Resource (Unit Cost) successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Resource (Unit Cost) is display correctly [Base Amount Column]",
					"The Cost of Resource (Unit Cost) should display correctly [Base Amount Column]",
					"The Cost of Resource (Unit Cost) doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_unitCostMateraialBaseAmountCostingSummary)) {
			writeTestResults("Verify that the Cost of Material (Unit Cost) is display correctly [Base Amount Column]",
					"The Cost of Material (Unit Cost) should display correctly [Base Amount Column]",
					"The Cost of Material (Unit Cost) successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Material (Unit Cost) is display correctly [Base Amount Column]",
					"The Cost of Material (Unit Cost) should display correctly [Base Amount Column]",
					"The Cost of Material (Unit Cost) doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_unitCostOverheadBaseAmountCostingSummary)) {
			writeTestResults("Verify that the Cost of Overhead (Unit Cost) is display correctly [Base Amount Column]",
					"The Cost of Overhead (Unit Cost) should display correctly [Base Amount Column]",
					"The Cost of Overhead (Unit Cost) successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead (Unit Cost) is display correctly [Base Amount Column]",
					"The Cost of Overhead (Unit Cost) should display correctly [Base Amount Column]",
					"The Cost of Overhead (Unit Cost) doesn't display correctly [Base Amount Column]", "fail");
		}
	}

	/* Generate Internal Receipt PROD_E2E_002 */
	public void internalReciptAndJournelEntry_PROD_E2E_002() throws Exception {
		explicitWaitUntillClickable(btn_action2, 50);
		click(btn_action2);

		explicitWaitUntillClickable(btn_convertToInternalReceipt, 10);
		if (isDisplayed(btn_convertToInternalReceipt)) {
			writeTestResults("Verify that Convert to Internel Recipt option is available under Actions",
					"Convert to Internel Recipt option should be available under Actions",
					"Convert to Internel Recipt option successfully available under Actions", "pass");
		} else {
			writeTestResults("Verify that Convert to Internel Recipt option is available under Actions",
					"Convert to Internel Recipt option should be available under Actions",
					"Convert to Internel Recipt option doesn't available under Actions", "fail");
		}
		click(btn_convertToInternalReceipt);

		explicitWait(header_genarateInternalReciptSmoke, 20);
		if (isDisplayed(header_genarateInternalReciptSmoke)) {
			writeTestResults("Verify that genarate internel receipt window is open",
					"genarate internel receipt window should be open",
					"genarate internel receipt window succeessfully open", "pass");
		} else {
			writeTestResults("Verify that genarate internel receipt window is open",
					"genarate internel receipt window should be open", "genarate internel receipt window doesn't open",
					"fail");
		}

		click(chk_seletcOroductForInternalRecipt);

		if (isSelected(chk_seletcOroductForInternalRecipt)) {
			writeTestResults("Verify user able to select the product line",
					"User should be able to select the product line", "User successfully select the product line",
					"pass");
		} else {
			writeTestResults("Verify user able to select the product line",
					"User should be able to select the product line", "User doesn't select the product line", "fail");
		}

		String enteredReceiptDate = getAttribute(txt_reciptDateGenarateInternalREceiptWindow, "value");
		click(btn_genarateInternalRecipt);
		explicitWait(para_validationInternalREciptGenarateSuccessfull, 15);
		if (isDisplayed(para_validationInternalREciptGenarateSuccessfull)) {
			writeTestResults("Verify that succesfull validation is appear for Internal Receipt Genaration",
					"Succesfull validation should appear for Internal Receipt Genaration",
					"Succesfull validation is appear for Internal Receipt Genaration", "pass");
		} else {
			writeTestResults("Verify that succesfull validation is appear for Internal Receipt Genaration",
					"Succesfull validation should appear for Internal Receipt Genaration",
					"Succesfull validation doesn't appear for Internal Receipt Genaration", "fail");
		}

		waitUntillNotVisible(para_validationInternalREciptGenarateSuccessfull, 10);
		Thread.sleep(3500);

		explicitWaitUntillClickable(lnk_genaratedInternalREciptSmoke, 15);
		Thread.sleep(3500);
		mouseMove(lnk_genaratedInternalREciptSmoke);
		click(lnk_genaratedInternalREciptSmoke);
		Thread.sleep(3000);
		switchWindow();
		Thread.sleep(1000);
		explicitWait(header_releasedInternalRecipt, 10);
		trackCode = getText(lbl_docNumber);
		if (isDisplayed(header_releasedInternalRecipt)) {
			writeTestResults("Verify that Internal Receiptis open as released status",
					"Internal Receipt should be open as released status",
					"Internal Receipt successfully open as released status", "pass");
		} else {
			writeTestResults("Verify that Internal Receiptis open as released status",
					"Internal Receipt should be open as released status",
					"Internal Receipt doesn't open or not in released status", "fail");
		}

		String docDate = getText(a_documentDate);
		String postDate = getText(a_postDate);
		String dueDate = getText(a_dueDate);

		if (docDate.equals(enteredReceiptDate) && docDate.equals(postDate) && docDate.equals(dueDate)) {
			writeTestResults(
					"Verify that all three header dates are equal to the date that entered when generating Internal Receipt",
					"All three header dates should be equal to the date that entered when generating Internal Receipt",
					"All three header dates are equal to the date that entered when generating Internal Receipt",
					"pass");
		} else {
			writeTestResults(
					"Verify that all three header dates are equal to the date that entered when generating Internal Receipt",
					"All three header dates should be equal to the date that entered when generating Internal Receipt",
					"All three header dates are not equal to the date that entered when generating Internal Receipt",
					"fail");
		}

		String warehouseOnInternalReceipt = getText(div_warehouseInternalRecipt);
		if (warehouseOnInternalReceipt.equals(outputWarehouseLI)) {
			writeTestResults(
					"Verify that Output Warehouse of Production Order and Warehouse of Internal Receipt is same",
					"Output Warehouse of Production Order and Warehouse of Internal Receipt should be same",
					"Output Warehouse of Production Order and Warehouse of Internal Receipt is same", "pass");
		} else {
			writeTestResults(
					"Verify that Output Warehouse of Production Order and Warehouse of Internal Receipt is same",
					"Output Warehouse of Production Order and Warehouse of Internal Receipt should be same",
					"Output Warehouse of Production Order and Warehouse of Internal Receipt is not same", "fail");
		}

		String qtyInternalReceipt = getText(div_planedQtyInternalRecipt);
		if (qtyInternalReceipt.equals("70.00")) {
			writeTestResults("Verify that quantity is equel to the QC pass quantity",
					"Quantity should be equel to the QC pass quantity",
					"Quantity successfully equel to the QC pass quantity", "pass");
		} else {
			writeTestResults("Verify that quantity is equel to the QC pass quantity",
					"Quantity should be equel to the QC pass quantity",
					"Quantity successfully not equel to the QC pass quantity", "fail");
		}

		click(btn_action2);

		explicitWaitUntillClickable(btn_journal, 10);
		if (isDisplayed(btn_journal)) {
			writeTestResults("Verify that Journal Entry option is available under Actions",
					"Journal Entry option should be available under Actions",
					"Journal Entry option successfully available under Actions", "pass");
		} else {
			writeTestResults("Verify that Journal Entry option is available under Actions",
					"Journal Entry option should be available under Actions",
					"Journal Entry option doesn't available under Actions", "fail");
		}
		click(btn_journal);

		explicitWait(header_journalEntryPopup, 10);
		if (isDisplayed(header_journalEntryPopup)) {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded",
					"Journal Entry details pop up successfully get loaded", "pass");
		} else {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded", "Journal Entry details pop up doesn't get loaded",
					"fail");
		}

		if (isDisplayedQuickCheck(lbl_debitValueJournelEntry)) {
			/* QA */
			String debitAmount = getText(lbl_debitValueJournelEntry);

			if (debitAmount.startsWith("1410.51")) {
				writeTestResults("Verify that amount was debited to the relevent GL Account",
						"Amount should be debited to the relevent GL Account",
						"Amount successfully debited to the relevent GL Account", "pass");
			} else {
				writeTestResults("Verify that amount was debited to the relevent GL Account",
						"Amount should be debited to the relevent GL Account",
						"Amount doesn't debited to the relevent GL Account", "fail");
			}

			String creditAmount = getText(lbl_creditValueJournelEntry);
			if (creditAmount.startsWith("1410.51")) {
				writeTestResults("Verify that amount was credited to the relevent GL Account",
						"Amount should be credited to the relevent GL Account",
						"Amount successfully credited to the relevent GL Account", "pass");
			} else {
				writeTestResults("Verify that amount was credited to the relevent GL Account",
						"Amount should be credited to the relevent GL Account",
						"Amount doesn't credited to the relevent GL Account", "fail");
			}

			String debitTotal = getText(lbl_debitTotalJournelEntrySingleEntry);
			String credTotal = getText(lbl_creditTotalJournelEntrySingleEntry);

			if (debitTotal.startsWith("1410.51") && debitTotal.equals(credTotal)) {
				writeTestResults("Verify total debit and credit amounts were balanced",
						"Total debit and credit amounts should balanced",
						"Total debit and credit amounts successfully balanced", "pass");
			} else {
				writeTestResults("Verify total debit and credit amounts were balanced",
						"Total debit and credit amounts should balanced",
						"Total debit and credit amounts weren't balanced", "fail");
			}

		} else {
			/* Staging */
			String debitAmount = getText(lbl_debitValueJournelEntry);

			if (debitAmount.startsWith("1410.51")) {
				writeTestResults("Verify that amount was debited to the relevent GL Account",
						"Amount should be debited to the relevent GL Account",
						"Amount successfully debited to the relevent GL Account", "pass");
			} else {
				writeTestResults("Verify that amount was debited to the relevent GL Account",
						"Amount should be debited to the relevent GL Account",
						"Amount doesn't debited to the relevent GL Account", "fail");
			}

			String creditAmount = getText(lbl_creditValueJournelEntry);
			if (creditAmount.startsWith("1410.51")) {
				writeTestResults("Verify that amount was credited to the relevent GL Account",
						"Amount should be credited to the relevent GL Account",
						"Amount successfully credited to the relevent GL Account", "pass");
			} else {
				writeTestResults("Verify that amount was credited to the relevent GL Account",
						"Amount should be credited to the relevent GL Account",
						"Amount doesn't credited to the relevent GL Account", "fail");
			}

			String debitTotal = getText(lbl_debitTotalJournelEntrySingleEntry);
			String credTotal = getText(lbl_creditTotalJournelEntrySingleEntry);

			if (debitTotal.startsWith("1410.51") && debitTotal.equals(credTotal)) {
				writeTestResults("Verify total debit and credit amounts were balanced",
						"Total debit and credit amounts should balanced",
						"Total debit and credit amounts successfully balanced", "pass");
			} else {
				writeTestResults("Verify total debit and credit amounts were balanced",
						"Total debit and credit amounts should balanced",
						"Total debit and credit amounts weren't balanced", "fail");
			}
		}
	}

	/*-----Second Batch-----*/
	/* Shop Floor Update PROD_E2E_002 */
	public void shopFloorUpdateSecondBatch_PROD_E2E_002() throws InvalidFormatException, IOException, Exception {
		openPage(readProductionData("ReleaseedPO_PRD_E2E_002"));
		explicitWaitUntillClickable(btn_action2, 50);
		click(btn_action2);

		explicitWaitUntillClickable(btn_shopFloorUpdate2, 10);
		if (isDisplayed(btn_shopFloorUpdate2)) {
			writeTestResults("Verify that Shop floor update option is available under Actions",
					"Shop floor update option should be available under Actions",
					"Shop floor update option successfully available under Actions", "pass");
		} else {
			writeTestResults("Verify that Shop floor update option is available under Actions",
					"Shop floor update option should be available under Actions",
					"Shop floor update option doesn't available under Actions", "fail");
		}
		click(btn_shopFloorUpdate2);

		explicitWait(header_shopFloorUpdateWindow, 20);
		if (isDisplayed(header_shopFloorUpdateWindow)) {
			writeTestResults("Verify that shop floor update window is open", "Shop floor update window should be open",
					"Shop floor update window succeessfully open", "pass");
		} else {
			writeTestResults("Verify that shop floor update window is open", "Shop floor update window should be open",
					"Shop floor update window doesn't open", "fail");
		}

		/* Add new batch */
		selectText(drop_batchNoShopFloor, "--None--");
		click(btn_addNewBatchShopFloor);
		explicitWaitUntillClickable(btn_genarateBatchShopFloor, 10);
		click(btn_genarateBatchShopFloor);
		explicitWait(btn_startFirstOperationSFU, 15);
		genaratedBatch = getSelectedOptionInDropdown(drop_batchNoShopFloor);

		/* First Operation */
		System.out.println("First Operation");
		click(btn_startFirstOperationSFU);
		explicitWaitUntillClickable(btn_startSecondLeveleSFU, 10);
		if (isDisplayed(btn_startSecondLeveleSFU)) {
			writeTestResults("Verify that start button is available", "Start button should be available",
					"Start button successfully available", "pass");
		} else {
			writeTestResults("Verify that start button is available", "Start button should be available",
					"Start button doesn't available", "fail");
		}

		click(btn_startSecondLeveleSFU);

		explicitWaitUntillClickable(btn_actualUpdateSFU, 10);
		if (isDisplayed(btn_actualUpdateSFU)) {
			writeTestResults("Verify that actual update option is available on shop floor update main window",
					"Actual update option should be available on shop floor update main window",
					"Actual update option successfully available on shop floor update main window", "pass");
		} else {
			writeTestResults("Verify that actual update option is available on shop floor update main window",
					"Actual update option should be available on shop floor update main window",
					"Actual update option doesn't available on shop floor update main window", "fail");
		}

		click(btn_actualUpdateSFU);

		Thread.sleep(2500);
		sendKeys(txt_outoutQuantityACtualUpdateSFU, actualQuantityOutputProductFirstOperation);
		String outputActualUpdateQuantityFirstOperation = getAttribute(txt_outoutQuantityACtualUpdateSFU, "value");
		if (outputActualUpdateQuantityFirstOperation.equals(actualQuantityOutputProductFirstOperation)) {
			writeTestResults("Verify user able to enter Output Product qty for the first operation",
					"User should be able to enter Output Product qty for the first operation",
					"User successfully enter Output Product qty for the first operation", "pass");
		} else {
			writeTestResults("Verify user able to enter Output Product qty for the first operation",
					"User should be able to enter Output Product qty for the first operation",
					"User doesn't enter Output Product qty for the first operation", "fail");
		}
		Thread.sleep(1000);

		click(btn_upateActualSFU);
		explicitWait(para_successfullValidationOutProductActualUpdate, 8);
		if (isDisplayed(para_successfullValidationOutProductActualUpdate)) {
			writeTestResults("Verify that successfully updated validation is appear for the Output Product update",
					"Successfully updated validation should appear for the Output Product update",
					"Successfully updated validation successfully appear for the Output Product update", "pass");
		} else {
			writeTestResults("Verify that successfully updated validation is appear for the Output Product update",
					"Successfully updated validation should appear for the Output Product update",
					"Successfully updated validation doesn't appear for the Output Product update", "fail");
		}
		Thread.sleep(3000);

		click(btn_tapInputProductsShopFloorUpdate);

		sendKeys(txt_actualUpdaInputProductQuantityFirstOperation, actualQuantityInputProductFirstOperation);
		String inputActualUpdateQuantityFirstOperation = getAttribute(txt_actualUpdaInputProductQuantityFirstOperation,
				"value");
		if (inputActualUpdateQuantityFirstOperation.equals(actualQuantityInputProductFirstOperation)) {
			writeTestResults("Verify user able to enter Input Product qty for the first operation - First Material",
					"User should be able to enter Input Product qty for the first operation - First Material",
					"User successfully enter Input Product qty for the first operation - First Material", "pass");
		} else {
			writeTestResults("Verify user able to enter Input Product qty for the first operation - First Material",
					"User should be able to enter Input Product qty for the first operation - First Material",
					"User doesn't enter Input Product qty for the first operation - First Material", "fail");
		}

		sendKeys(txt_actualUpdaInputProductQuantity2FirstOperation, actualQuantity2InputProductFirstOperation);
		String inputActualUpdateQuantity2FirstOperation = getAttribute(
				txt_actualUpdaInputProductQuantity2FirstOperation, "value");
		if (inputActualUpdateQuantity2FirstOperation.equals(actualQuantity2InputProductFirstOperation)) {
			writeTestResults("Verify user able to enter Input Product qty for the first operation - Second Material",
					"User should be able to enter Input Product qty for the first operation - Second Material",
					"User successfully enter Input Product qty for the first operation - Second Material", "pass");
		}

		click(btn_upateActualSFU);
		explicitWait(para_successfullValidationActualUpdate, 8);
		if (isDisplayedQuickCheck(para_successfullValidationActualUpdate)) {
			writeTestResults("Verify that successfully updated validation is appear for the Input Product update",
					"Successfully updated validation should appear for the Input Product update",
					"Successfully updated validation successfully appear for the Input Product update", "pass");
		} else {
			click(span_firstErrorOnGridInputProductActualUpdate);
			Thread.sleep(1500);
			click(span_firstErrorOnGridInputProductActualUpdate);
			explicitWait(div_notEnoughQuantityAvaialableValidation, 5);
			if (isDisplayed(div_notEnoughQuantityAvaialableValidation)) {
				Thread.sleep(1500);
				((JavascriptExecutor) driver).executeScript("window.open('about:blank','_blank');");
				Thread.sleep(1000);
				switchWindow();
				openPage(siteUrlMultiPlatformm);
				purchaseOrderToInboundShipmentToPurchaseInvoice();
				Thread.sleep(2000);
				click(btn_upateActualSFU);
				explicitWait(para_successfullValidationActualUpdate, 8);
				if (isDisplayedQuickCheck(para_successfullValidationActualUpdate)) {
					writeTestResults(
							"Verify that successfully updated validation is appear for the Input Product update",
							"Successfully updated validation should appear for the Input Product update",
							"Successfully updated validation successfully appear for the Input Product update", "pass");
				} else {
					writeTestResults(
							"Verify that successfully updated validation is appear for the Input Product update",
							"Successfully updated validation should appear for the Input Product update",
							"Successfully updated validation doesn't appear for the Input Product update", "fail");
				}
			}
		}

		Thread.sleep(3000);

		explicitWaitUntillClickable(tab_resourseShopFloorUpdate, 30);
		click(tab_resourseShopFloorUpdate);
		explicitWaitUntillClickable(div_actualHoursResourseTabRow01, 20);
		Thread.sleep(2000);
		click(txt_actualDuarationResourse01ActualUpdate);
		Thread.sleep(1500);
		click(btn_oneHouserActualDurationResourse01);
		Thread.sleep(1000);
		click(btn_okActualDurationResourse01);
		Thread.sleep(1500);

		click(txt_actualDuarationResourse02ActualUpdate);
		Thread.sleep(1500);
		click(btn_oneHouserActualDurationResourse02);
		Thread.sleep(1000);
		click(btn_okActualDurationResourse02);
		Thread.sleep(1500);

		Thread.sleep(2000);
		String actualHoursRow1 = getAttribute(div_actualHoursResourseTabRow01, "value");
		String actualminsRow1 = getAttribute(div_actualMinsResourseTabRow01, "value");
		String actualHoursRow2 = getAttribute(div_actualHoursResourseTabRow02, "value");
		String actualminsRow2 = getAttribute(div_actualMinsResourseTabRow02, "value");

		if (actualHoursRow1.equals("01") && actualminsRow1.equals("00") && actualHoursRow2.equals("01")
				&& actualminsRow2.equals("00")) {
			writeTestResults("Verify that actual resource hours are loaded correctly",
					"Actual resource hours should be loaded correctly",
					"Actual resource hours successfully loaded correctly", "pass");
		} else {
			writeTestResults("Verify that actual resource hours are loaded correctly",
					"Actual resource hours should be loaded correctly",
					"Actual resource hours doesn't loaded correctly", "fail");
		}

		click(btn_upateActualSFU);
		explicitWait(para_successfullValidationActualUpdateResourse, 8);
		if (isDisplayed(para_successfullValidationActualUpdateResourse)) {
			writeTestResults("Verify that successfully updated validation is appear for the Resource update",
					"Successfully updated validation should appear for the Resource update",
					"Successfully updated validation successfully appear for the Resource update", "pass");
		} else {
			writeTestResults("Verify that successfully updated validation is appear for the Resource update",
					"Successfully updated validation should appear for the Resource update",
					"Successfully updated validation doesn't appear for the Resource update", "fail");
		}

		Thread.sleep(3000);

		click(btn_closeFisrstOperationActualUpdateWindow);

		explicitWait(btn_completeFirstOperation, 8);
		if (isDisplayed(btn_completeFirstOperation)) {
			writeTestResults("Verify that complete button is available for the first operation",
					"Complete button should be available for the first operation",
					"Complete button successfully available for the first operation", "pass");
		} else {
			writeTestResults("Verify that complete button is available for the first operation",
					"Complete button should be available for the first operation",
					"Complete button doesn't available for the first operation", "fail");
		}
		click(btn_completeFirstOperation);

		explicitWait(btn_completeSecondBatchShopFloor, 8);
		if (isDisplayed(btn_completeSecondBatchShopFloor)) {
			writeTestResults("Verify that complete button is available for the complete window of first operation",
					"Complete button should be available for the complete window of first operation",
					"Complete button successfully available for the complete window of first operation", "pass");
		} else {
			writeTestResults("Verify that complete button is available for the complete window of first operation",
					"Complete button should be available for the complete window of first operation",
					"Complete button doesn't available for the complete window of first operation", "fail");
		}
		click(btn_completeSecondBatchShopFloor);

		explicitWait(lbl_completedShopFloorUpdateBarFirstOperation, 8);
		if (isDisplayed(lbl_completedShopFloorUpdateBarFirstOperation)) {
			writeTestResults("Verify that shop flow update is completed for the first operation",
					"Shop flow update should be completed for the first operation",
					"Shop flow update successfully completed for the first operation", "pass");
		} else {
			writeTestResults("Verify that shop flow update is completed for the first operation",
					"Shop flow update should be completed for the first operation",
					"Shop flow update doesn't completed for the first operation", "pass");
		}

		/* Second Operation */
		System.out.println("Second Operation");

		explicitWaitUntillClickable(btn_actualUpdateSFU, 10);
		if (isDisplayed(btn_actualUpdateSFU)) {
			writeTestResults(
					"Verify that actual update option is available on shop floor update main window for second operation",
					"Actual update option should be available on shop floor update main window for second operation",
					"Actual update option successfully available on shop floor update main window for second operation",
					"pass");
		} else {
			writeTestResults(
					"Verify that actual update option is available on shop floor update main window for second operation",
					"Actual update option should be available on shop floor update main window for second operation",
					"Actual update option doesn't available on shop floor update main window for second operation",
					"fail");
		}

		click(btn_actualUpdateSFU);

		sendKeys(txt_outoutQuantityACtualUpdateSFU, actualQuantityOutputProductSecondOperation);
		String outputActualUpdateQuantitySecondOperation = getAttribute(txt_outoutQuantityACtualUpdateSFU, "value");
		if (outputActualUpdateQuantitySecondOperation.equals(actualQuantityOutputProductSecondOperation)) {
			writeTestResults("Verify user able to enter Output Product qty for the second operation",
					"User should be able to enter Output Product qty for the second operation",
					"User successfully enter Output Product qty for the second operation", "pass");
		} else {
			writeTestResults("Verify user able to enter Output Product qty for the second operation",
					"User should be able to enter Output Product qty for the second operation",
					"User doesn't enter Output Product qty for the second operation", "fail");
		}
		Thread.sleep(1000);

		click(btn_upateActualSFU);
		explicitWait(para_successfullValidationOutProductActualUpdate, 8);
		if (isDisplayed(para_successfullValidationOutProductActualUpdate)) {
			writeTestResults("Verify that successfully updated validation is appear for the Output Product update",
					"Successfully updated validation should appear for the Output Product update",
					"Successfully updated validation successfully appear for the Output Product update", "pass");
		} else {
			writeTestResults("Verify that successfully updated validation is appear for the Output Product update",
					"Successfully updated validation should appear for the Output Product update",
					"Successfully updated validation doesn't appear for the Output Product update", "fail");
		}
		Thread.sleep(3000);

		click(btn_tapInputProductsShopFloorUpdate);

		sendKeys(txt_actualUpdaInputProductQuantitySecondOperation, actualQuantityInputProductSecondOperation);
		String inputActualUpdateQuantitySecondOperation = getAttribute(
				txt_actualUpdaInputProductQuantitySecondOperation, "value");
		if (inputActualUpdateQuantitySecondOperation.equals(actualQuantityInputProductSecondOperation)) {
			writeTestResults("Verify user able to enter Input Product qty for the second operation",
					"User should be able to enter Input Product qty for the second operation",
					"User successfully enter Input Product qty for the second operation", "pass");
		} else {
			writeTestResults("Verify user able to enter Input Product qty for the second operation",
					"User should be able to enter Input Product qty for the second operation",
					"User doesn't enter Input Product qty for the second operation", "fail");
		}

		click(btn_upateActualSFU);
		explicitWait(para_successfullValidationActualUpdate, 8);
		if (isDisplayedQuickCheck(para_successfullValidationActualUpdate)) {
			writeTestResults("Verify that successfully updated validation is appear for the Input Product update",
					"Successfully updated validation should appear for the Input Product update",
					"Successfully updated validation successfully appear for the Input Product update", "pass");
		} else {
			writeTestResults("Verify that successfully updated validation is appear for the Input Product update",
					"Successfully updated validation should appear for the Input Product update",
					"Successfully updated validation doesn't appear for the Input Product update", "fail");
		}
		Thread.sleep(3000);

		click(btn_closeSecondOperationActualUpdateWindow);

		explicitWait(btn_completeSecondOperation, 8);
		if (isDisplayed(btn_completeSecondOperation)) {
			writeTestResults("Verify that complete button is available for the second operation",
					"Complete button should be available for the second operation",
					"Complete button successfully available for the second operation", "pass");
		} else {
			writeTestResults("Verify that complete button is available for the second operation",
					"Complete button should be available for the second operation",
					"Complete button doesn't available for the second operation", "fail");
		}
		click(btn_completeSecondOperation);

		explicitWait(btn_completeSecondBatchShopFloor, 8);
		if (isDisplayed(btn_completeSecondBatchShopFloor)) {
			writeTestResults("Verify that complete button is available for the complete window of second operation",
					"Complete button should be available for the complete window of second operation",
					"Complete button successfully available for the complete window of second operation", "pass");
		} else {
			writeTestResults("Verify that complete button is available for the complete window of second operation",
					"Complete button should be available for the complete window of second operation",
					"Complete button doesn't available for the complete window of second operation", "fail");
		}
		click(btn_completeSecondBatchShopFloor);

		explicitWait(lbl_completedShopFloorUpdateBarSecondOperation, 8);
		if (isDisplayed(lbl_completedShopFloorUpdateBarSecondOperation)) {
			writeTestResults("Verify that shop flow update is completed for the second operation",
					"Shop flow update should be completed for the second operation",
					"Shop flow update successfully completed for the second operation", "pass");
		} else {
			writeTestResults("Verify that shop flow update is completed for the second operation",
					"Shop flow update should be completed for the second operation",
					"Shop flow update doesn't completed for the second operation", "pass");
		}
	}

	/* QC PROD_E2E_002 */
	public void qcSecondBatchPROD_E2E_002() throws Exception {
		pageRefersh();
		explicitWait(img_navigationPane, 20);
		Thread.sleep(2000);
		explicitWait(img_navigationPane, 20);
		click(img_navigationPane);
		explicitWait(btn_productionModule, 10);
		click(btn_productionModule);
		explicitWait(btn_productionQCAndSorting, 10);
		if (isDisplayed(btn_productionQCAndSorting)) {
			writeTestResults("Verif that Production QC & Sorting option is display under Production Module",
					"Production QC & Sorting option should be display under Production Module",
					"Production QC & Sorting option successfully display under Production Module", "pass");
		} else {
			writeTestResults("Verif that Production QC & Sorting option is display under Production Module",
					"Production QC & Sorting option should be display under Production Module",
					"Production QC & Sorting option doesn't display under Production Module", "fail");
		}

		click(btn_productionQCAndSorting);

		explicitWait(drop_documentTypeProductionQC, 50);
		selectText(drop_documentTypeProductionQC, qcTypeProductionOrder);
		Thread.sleep(1000);
		String selecteDocType = getSelectedOptionInDropdown(drop_documentTypeProductionQC);
		if (selecteDocType.equals(qcTypeProductionOrder)) {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User able to select 'Production Order' as document type", "pass");
		} else {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User not able to select 'Production Order' as document type", "fail");

		}

		sendKeys(txt_docNumberQC, readProductionData("ProductionOrder_PROD_E2E_002"));
		String enteredPO = getAttribute(txt_docNumberQC, "value");
		if (enteredPO.equals(readProductionData("ProductionOrder_PROD_E2E_002"))) {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User successfully enter Production Order document number", "pass");
		} else {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User doesn't enter Production Order document number", "fail");
		}

		Thread.sleep(1000);
		driver.findElement(getLocator(txt_docNumberQC)).sendKeys(Keys.ARROW_DOWN);
		pressEnter(txt_docNumberQC);

		explicitWait(div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_002")),
				30);
		if (isDisplayed(
				div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_002")))) {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User successfully select the relevent Production Order", "pass");
		} else {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User doesn't select the relevent Production Order", "fail");
		}

		Thread.sleep(1500);

		/* Third QC Scenario */
		mouseMove(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")));
		click(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")));

		if (isSelected(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")))) {
			writeTestResults("Verify user able to select the relevent Output Product",
					"User should be able to select the relevent Output Product",
					"User successfully select the relevent Output Product", "pass");
		} else {
			writeTestResults("Verify user able to select the relevent Output Product",
					"User should be able to select the relevent Output Product",
					"User doesn't select the relevent Output Product", "fail");
		}

		Thread.sleep(1500);
		sendKeys(txt_passQuantityProductionQCOneRow, thirdPassQty);
		String enteredPassQty = getAttribute(txt_passQuantityProductionQCOneRow, "value");
		if (enteredPassQty.equals(thirdPassQty)) {
			writeTestResults("Verify user able to enter pass qty (70)", "User should be able to enter pass qty (70)",
					"User successfully enter pass qty (70)", "pass");
		} else {
			writeTestResults("Verify user able to enter pass qty (70)", "User should be able to enter pass qty (70)",
					"User doesn't enter pass qty (70)", "fail");
		}
		Thread.sleep(1000);

		sendKeys(txt_failQuantityProductionQCOneRow, thirdFailQty);
		String enteredFailQty3 = getAttribute(txt_failQuantityProductionQCOneRow, "value");
		if (enteredFailQty3.equals(thirdFailQty)) {
			writeTestResults("Verify user able to enter fail qty (30)", "User should be able to enter fail qty (30)",
					"User successfully enter fail qty (30)", "pass");
		} else {
			writeTestResults("Verify user able to enter fail qty (30)", "User should be able to enter fail qty (30)",
					"User doesn't enter fail qty (30)", "fail");
		}

		click(btn_releseProductionQC);
		explicitWaitUntillPresent(para_successfullValidationProductionQC, 20);

		if (isElementPresent(para_successfullValidationProductionQC)) {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation is appear", "pass");
		} else {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation doesn't appear", "fail");
		}

		Thread.sleep(2000);
		waitUntillNotVisible(
				div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_002")), 15);
		if (!isDisplayedQuickCheck(
				div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_002")))) {
			writeTestResults("Verify that relevent Production Order is withdraw from pending tab",
					"Relevent Production Order should be withdraw from pending tab",
					"Relevent Production Order successfully withdraw from pending tab", "pass");
		} else {
			writeTestResults("Verify that relevent Production Order is withdraw from pending tab",
					"Relevent Production Order should be withdraw from pending tab",
					"Relevent Production Order still available on pending tab", "fail");
		}

		click(tab_historyQC);
		explicitWaitUntillClickable(tab_historySelectedQC, 20);
		if (isDisplayed(tab_historySelectedQC)) {
			writeTestResults("Verify that user able to open history tab", "User should be able to open history tab",
					"User successfully open history tab", "pass");
		} else {
			writeTestResults("Verify that user able to open history tab", "User should be able to open history tab",
					"User couldn't open history tab", "fail");
		}

		explicitWait(drop_documentTypeProductionQC, 50);
		selectText(drop_documentTypeProductionQC, qcTypeProductionOrder);
		Thread.sleep(1000);
		selecteDocType = getSelectedOptionInDropdown(drop_documentTypeProductionQC);
		if (selecteDocType.equals(qcTypeProductionOrder)) {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User able to select 'Production Order' as document type", "pass");
		} else {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User not able to select 'Production Order' as document type", "fail");

		}

		sendKeys(txt_docNumberQC, readProductionData("ProductionOrder_PROD_E2E_002"));
		String enteredPO6 = getAttribute(txt_docNumberQC, "value");
		if (enteredPO6.equals(readProductionData("ProductionOrder_PROD_E2E_002"))) {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User successfully enter Production Order document number", "pass");
		} else {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User doesn't enter Production Order document number", "fail");
		}

		Thread.sleep(1000);
		driver.findElement(getLocator(txt_docNumberQC)).sendKeys(Keys.ARROW_DOWN);
		pressEnter(txt_docNumberQC);

		explicitWait(div_selectedQcDocProductionHistoryTab.replace("doc_no",
				readProductionData("ProductionOrder_PROD_E2E_002")), 30);
		if (isDisplayed(div_selectedQcDocProductionHistoryTab.replace("doc_no",
				readProductionData("ProductionOrder_PROD_E2E_002")))) {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User successfully select the relevent Production Order", "pass");
		} else {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User doesn't select the relevent Production Order", "fail");
		}

		int docCount3 = getCount(dic_relevantHistoryDocCount);
		int i3 = 1;

		while (i3 <= docCount3) {
			if (isDisplayedQuickCheck(div_batchNoReleventToHistoryDocument.replace("changing_part", genaratedBatch))) {
				break;
			}

			i3++;
			click(dic_relevantHistoryDocument.replace("changing_part", String.valueOf(i3)));

		}

		Thread.sleep(2000);
		if (isDisplayed(column_headerPassedQtyHistoryTabQC)) {
			writeTestResults("Verify that pass qty column header is display as earlier",
					"Pass qty column header should be display as earlier",
					"Pass qty column header successfully display as earlier", "pass");
		} else {
			writeTestResults("Verify that pass qty column header is display as earlier",
					"Pass qty column header should be display as earlier",
					"Pass qty column header doesn't display as earlier", "fail");
		}

		String passQuantity = getAttribute(txt_passedQtyQC, "value");
		if (passQuantity.equals("70.00")) {
			writeTestResults("Verify that pass qty is display in history tab", "Pass qty should display in history tab",
					"Pass qty successfully display in history tab", "pass");
		} else {
			writeTestResults("Verify that pass qty is display in history tab", "Pass qty should display in history tab",
					"Pass qty doesn't display in history tab", "fail");
		}

		Thread.sleep(1000);
		if (isDisplayed(column_headerFailedQtyHistoryTabQC)) {
			writeTestResults("Verify that fail qty column header is display as earlier",
					"Fail qty column header should be display as earlier",
					"Fail qty column header successfully display as earlier", "pass");
		} else {
			writeTestResults("Verify that fail qty column header is display as earlier",
					"Fail qty column header should be display as earlier",
					"Fail qty column header doesn't display as earlier", "fail");
		}

		String failQuantity3 = getAttribute(txt_failedQtyQC, "value");
		if (failQuantity3.equals("30.00")) {
			writeTestResults("Verify that fail qty is display in history tab", "Fail qty should display in history tab",
					"Fail qty successfully display in history tab", "pass");
		} else {
			writeTestResults("Verify that fail qty is display in history tab", "Fail qty should display in history tab",
					"Fail qty doesn't display in history tab", "fail");
		}
	}

	/* Costing PROD_E2E_002 */
	public void productionCostingSecondBatch_PROD_E2E_002() throws Exception {
		openPage(readProductionData("ReleaseedPO_PRD_E2E_002"));
		explicitWaitUntillClickable(btn_action2, 50);
		click(btn_action2);

		explicitWaitUntillClickable(btn_productionCostingActions, 10);
		if (isDisplayed(btn_productionCostingActions)) {
			writeTestResults("Verify that Production Costing option is available under Actions",
					"Production Costing option should be available under Actions",
					"Production Costing option successfully available under Actions", "pass");
		} else {
			writeTestResults("Verify that Production Costing option is available under Actions",
					"Production Costing option should be available under Actions",
					"Production Costing option doesn't available under Actions", "fail");
		}
		click(btn_productionCostingActions);

		explicitWait(header_productionCostingWindow, 20);
		if (isDisplayed(header_productionCostingWindow)) {
			writeTestResults("Verify that production costing window is open",
					"Production costing window should be open", "Production costing window succeessfully open", "pass");
		} else {
			writeTestResults("Verify that production costing window is open",
					"Production costing window should be open", "Production costing window doesn't open", "fail");
		}

		explicitWaitUntillClickable(chk_pendingProductionCosting, 20);
		click(chk_pendingProductionCosting);

		if (isDisplayed(chk_pendingProductionCostingSelected)) {
			writeTestResults("Verify user able to select batch costing checkbox on production costing main window",
					"User should be able to select batch costing checkbox on production costing main window",
					"User able to select batch costing checkbox on production costing main window", "pass");
		} else {
			writeTestResults("Verify user able to select batch costing checkbox on production costing main window",
					"User should be able to select batch costing checkbox on production costing main window",
					"User not able to select batch costing checkbox on production costing main window", "fail");
		}

		click(btn_updateProductionCostingMainWindow);

		boolean visibilityFlag = false;
		for (int i = 1; i < 10; i++) {
			try {
				Thread.sleep(20);
				driver.findElement(getLocator(para_batchCostingUpdateValidation)).isDisplayed();
				visibilityFlag = true;
				break;
			} catch (Exception e) {
				continue;
			}
		}

		if (visibilityFlag) {
			writeTestResults("Verify that succesfull validation is appear for Batch Costing",
					"Succesfull validation should appear for Batch Costing",
					"Succesfull validation is appear for Batch Costing", "pass");
		} else {
			writeTestResults("Verify that succesfull validation is appear for Batch Costing",
					"Succesfull validation should appear for Batch Costing",
					"Succesfull validation doesn't appear for Batch Costing", "fail");
		}

//		WebElement element = wait.until(new Function<WebDriver, WebElement>() {
//			public WebElement apply(WebDriver driver) {
//				boolean visibilityOfValidation = false;
//				WebElement linkElement = driver.findElement(By.xpath(para_batchCostingUpdateValidation));
//				if (linkElement.isDisplayed()) {
//					visibilityOfValidation = true;
//				}
//
//				try {
//					if (visibilityOfValidation) {
//						writeTestResults("Verify that succesfull validation is appear for Batch Costing",
//								"Succesfull validation should appear for Batch Costing",
//								"Succesfull validation is appear for Batch Costing", "pass");
//					} else {
//						writeTestResults("Verify that succesfull validation is appear for Batch Costing",
//								"Succesfull validation should appear for Batch Costing",
//								"Succesfull validation doesn't appear for Batch Costing", "fail");
//					}
//
//				} catch (Exception e) {
//
//				}
//
//				return linkElement;
//
//			}
//		});

//		if (isDisplayedQuickCheck(para_batchCostingUpdateValidation)) {
//			writeTestResults("Verify that succesfull validation is appear for Batch Costing",
//					"Succesfull validation should appear for Batch Costing",
//					"Succesfull validation is appear for Batch Costing", "pass");
//		} else {
//			writeTestResults("Verify that succesfull validation is appear for Batch Costing",
//					"Succesfull validation should appear for Batch Costing",
//					"Succesfull validation doesn't appear for Batch Costing", "fail");
//		}

		explicitWait(btn_closeMainCostingUpdateWindowProductionOrder, 5);
		Thread.sleep(2500);
		click(btn_closeMainCostingUpdateWindowProductionOrder);

		explicitWait(header_releasedProductionOrder, 10);

		/* Costing Summary - Actual */
		explicitWaitUntillClickable(tab_costingSummaryPO, 60);
		click(tab_costingSummaryPO);
		explicitWaitUntillClickable(btn_expandCostingSummary, 20);

		click(btn_expandCostingSummary);
		Thread.sleep(500);
		explicitWaitUntillClickable(btn_expandCostingSummary, 20);
		click(btn_expandCostingSummary);
		explicitWaitUntillClickable(btn_expandCostingSummary, 20);
		click(btn_expandCostingSummary);
		explicitWaitUntillClickable(btn_expandCostingSummary, 20);
		click(btn_expandCostingSummary);

		if (isDisplayed(column_headerActualCostingSummary)) {
			writeTestResults("Verify that the Actual Column is display same as earlier",
					"The Actual Column should display same as earlier",
					"The Actual Column successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that the Actual Column is display same as earlier",
					"The Actual Column should display same as earlier",
					"The Actual Column doesn't display same as earlier", "fail");
		}

		if (isDisplayed(td_resourceCostActualCostingSummaryInfiniteSecondBatch)) {
			writeTestResults("Verify that the Resource Total Cost is display correctly [Actual Column]",
					"The Resource Total Cost should display correctly [Actual Column]",
					"The Resource Total Cost successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Resource Total Cost is display correctly [Actual Column]",
					"The Resource Total Cost should display correctly [Actual Column]",
					"The Resource Total Cost doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_resourceCost1ActualCostingSummaryInfiniteSecondBatch)) {
			writeTestResults("Verify that the Cost of Resource 01 is display correctly [Actual Column]",
					"The Cost of Resource 01 should display correctly [Actual Column]",
					"The Cost of Resource 01 successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Resource 01 is display correctly [Actual Column]",
					"The Cost of Resource 01 should display correctly [Actual Column]",
					"The Cost of Resource 01 doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_resourceCost2ActualCostingSummaryInfiniteSecondBatch)) {
			writeTestResults("Verify that the Cost of Resource 02 is display correctly [Actual Column]",
					"The Cost of Resource 02 should display correctly [Actual Column]",
					"The Cost of Resource 02 successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Resource 02 is display correctly [Actual Column]",
					"The Cost of Resource 02 should display correctly [Actual Column]",
					"The Cost of Resource 02 doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_materialCostActualCostingSummaryInfiniteSecondBatch)) {
			writeTestResults("Verify that the Material Total Cost is display correctly [Actual Column]",
					"The Material Total Cost should display correctly [Actual Column]",
					"The Material Total Cost successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Material Total Cost is display correctly [Actual Column]",
					"The Material Total Cost should display correctly [Actual Column]",
					"The Material Total Cost doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_material1CostActualCostingSummaryInfiniteSecondBatch)) {
			writeTestResults("Verify that the Cost of Material 01 is display correctly [Actual Column]",
					"The Cost of Material 01 should display correctly [Actual Column]",
					"The Cost of Material 01 successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Material 01 is display correctly [Actual Column]",
					"The Cost of Material 01 should display correctly [Actual Column]",
					"The Cost of Material 01 doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_material2CostActualCostingSummaryInfiniteSecondBatch)) {
			writeTestResults("Verify that the Cost of Material 02 is display correctly [Actual Column]",
					"The Cost of Material 02 should display correctly [Actual Column]",
					"The Cost of Material 02 successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Material 02 is display correctly [Actual Column]",
					"The Cost of Material 02 should display correctly [Actual Column]",
					"The Cost of Material 02 doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_material3CostActualCostingSummaryInfiniteSecondBatch)) {
			writeTestResults("Verify that the Cost of Material 03 is display correctly [Actual Column]",
					"The Cost of Material 03 should display correctly [Actual Column]",
					"The Cost of Material 03 successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Material 03 is display correctly [Actual Column]",
					"The Cost of Material 03 should display correctly [Actual Column]",
					"The Cost of Material 03 doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_overheadCostActualCostingSummaryInfiniteSecondBatch)) {
			writeTestResults("Verify that the Overhead Total Cost is display correctly [Actual Column]",
					"The Overhead Total Cost should display correctly [Actual Column]",
					"The Overhead Total Cost successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Overhead Total Cost is display correctly [Actual Column]",
					"The Overhead Total Cost should display correctly [Actual Column]",
					"The Overhead Total Cost doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_overhead1CostActualCostingSummaryInfiniteSecondBatch)) {
			writeTestResults("Verify that the Cost of Overhead 01 is display correctly [Actual Column]",
					"The Cost of Overhead 01 should display correctly [Actual Column]",
					"The Cost of Overhead 01 successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead 01 is display correctly [Actual Column]",
					"The Cost of Overhead 01 should display correctly [Actual Column]",
					"The Cost of Overhead 01 doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_overhead2CostActualCostingSummaryInfiniteSecondBatch)) {
			writeTestResults("Verify that the Cost of Overhead 02 is display correctly [Actual Column]",
					"The Cost of Overhead 02 should display correctly [Actual Column]",
					"The Cost of Overhead 02 successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead 02 is display correctly [Actual Column]",
					"The Cost of Overhead 02 should display correctly [Actual Column]",
					"The Cost of Overhead 02 doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_overhead3CostActualCostingSummaryInfiniteSecondBatch)) {
			writeTestResults("Verify that the Cost of Overhead 03 is display correctly [Actual Column]",
					"The Cost of Overhead 03 should display correctly [Actual Column]",
					"The Cost of Overhead 03 successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead 03 is display correctly [Actual Column]",
					"The Cost of Overhead 03 should display correctly [Actual Column]",
					"The Cost of Overhead 03 doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_totalCostActualCostingSummaryInfiniteSecondBatch)) {
			writeTestResults("Verify that the Total Cost is display correctly [Actual Column]",
					"The Total Cost should display correctly [Actual Column]",
					"The Total Cost successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Total Cost is display correctly [Actual Column]",
					"The Total Cost should display correctly [Actual Column]",
					"The Total Cost doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_unitCostActualCostingSummaryInfiniteSecondBatch)) {
			writeTestResults("Verify that the Unit Cost is display correctly [Actual Column]",
					"The Unit Cost should display correctly [Actual Column]",
					"The Unit Cost successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Unit Cost is display correctly [Actual Column]",
					"The Unit Cost should display correctly [Actual Column]",
					"The Unit Cost doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_unitCostResourseActualCostingSummaryInfiniteSecondBatch)) {
			writeTestResults("Verify that the Cost of Resource (Unit Cost) is display correctly [Actual Column]",
					"The Cost of Resource (Unit Cost) should display correctly [Actual Column]",
					"The Cost of Resource (Unit Cost) successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Resource (Unit Cost) is display correctly [Actual Column]",
					"The Cost of Resource (Unit Cost) should display correctly [Actual Column]",
					"The Cost of Resource (Unit Cost) doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_unitCostMateraialActualCostingSummaryInfiniteSecondBatch)) {
			writeTestResults("Verify that the Cost of Material (Unit Cost) is display correctly [Actual Column]",
					"The Cost of Material (Unit Cost) should display correctly [Actual Column]",
					"The Cost of Material (Unit Cost) successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Material (Unit Cost) is display correctly [Actual Column]",
					"The Cost of Material (Unit Cost) should display correctly [Actual Column]",
					"The Cost of Material (Unit Cost) doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_unitCostOverheadActualCostingSummaryInfiniteSecondBatch)) {
			writeTestResults("Verify that the Cost of Overhead (Unit Cost) is display correctly [Actual Column]",
					"The Cost of Overhead (Unit Cost) should display correctly [Actual Column]",
					"The Cost of Overhead (Unit Cost) successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead (Unit Cost) is display correctly [Actual Column]",
					"The Cost of Overhead (Unit Cost) should display correctly [Actual Column]",
					"The Cost of Overhead (Unit Cost) doesn't display correctly [Actual Column]", "fail");
		}

		/* Costing Summary - Base Amount */
		if (isDisplayed(column_headerBaseAmountCostingSummary)) {
			writeTestResults("Verify that the Base Amount Column is display same as earlier",
					"The Base Amount Column should display same as earlier",
					"The Base Amount Column successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that the Base Amount Column is display same as earlier",
					"The Base Amount Column should display same as earlier",
					"The Base Amount Column doesn't display same as earlier", "fail");
		}

		if (isDisplayed(td_resourceCostBaseAmountCostingSummaryInfiniteSecondBatch)) {
			writeTestResults("Verify that the Resource Total Cost is display correctly [Base Amount Column]",
					"The Resource Total Cost should display correctly [Base Amount Column]",
					"The Resource Total Cost successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Resource Total Cost is display correctly [Base Amount Column]",
					"The Resource Total Cost should display correctly [Base Amount Column]",
					"The Resource Total Cost doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_resourceCost1BaseAmountCostingSummaryInfiniteSecondBatch)) {
			writeTestResults("Verify that the Cost of Resource 01 is display correctly [Base Amount Column]",
					"The Cost of Resource 01 should display correctly [Base Amount Column]",
					"The Cost of Resource 01 successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Resource 01 is display correctly [Base Amount Column]",
					"The Cost of Resource 01 should display correctly [Base Amount Column]",
					"The Cost of Resource 01 doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_resourceCost2BaseAmountCostingSummaryInfiniteSecondBatch)) {
			writeTestResults("Verify that the Cost of Resource 02 is display correctly [Base Amount Column]",
					"The Cost of Resource 02 should display correctly [Base Amount Column]",
					"The Cost of Resource 02 successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Resource 02 is display correctly [Base Amount Column]",
					"The Cost of Resource 02 should display correctly [Base Amount Column]",
					"The Cost of Resource 02 doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_materialCostBaseAmountCostingSummaryInfiniteSecondBatch)) {
			writeTestResults("Verify that the Material Total Cost is display correctly [Base Amount Column]",
					"The Material Total Cost should display correctly [Base Amount Column]",
					"The Material Total Cost successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Material Total Cost is display correctly [Base Amount Column]",
					"The Material Total Cost should display correctly [Base Amount Column]",
					"The Material Total Cost doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_material1CostBaseAmountCostingSummaryInfiniteSecondBatch)) {
			writeTestResults("Verify that the Cost of Material 01 is display correctly [Base Amount Column]",
					"The Cost of Material 01 should display correctly [Base Amount Column]",
					"The Cost of Material 01 successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Material 01 is display correctly [Base Amount Column]",
					"The Cost of Material 01 should display correctly [Base Amount Column]",
					"The Cost of Material 01 doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_material2CostBaseAmountCostingSummaryInfiniteSecondBatch)) {
			writeTestResults("Verify that the Cost of Material 02 is display correctly [Base Amount Column]",
					"The Cost of Material 02 should display correctly [Base Amount Column]",
					"The Cost of Material 02 successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Material 02 is display correctly [Base Amount Column]",
					"The Cost of Material 02 should display correctly [Base Amount Column]",
					"The Cost of Material 02 doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_material3CostBaseAmountCostingSummaryInfiniteSecondBatch)) {
			writeTestResults("Verify that the Cost of Material 03 is display correctly [Base Amount Column]",
					"The Cost of Material 03 should display correctly [Base Amount Column]",
					"The Cost of Material 03 successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Material 03 is display correctly [Base Amount Column]",
					"The Cost of Material 03 should display correctly [Base Amount Column]",
					"The Cost of Material 03 doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_overheadCostBaseAmountCostingSummaryInfiniteSecondBatch)) {
			writeTestResults("Verify that the Overhead Total Cost is display correctly [Base Amount Column]",
					"The Overhead Total Cost should display correctly [Base Amount Column]",
					"The Overhead Total Cost successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Overhead Total Cost is display correctly [Base Amount Column]",
					"The Overhead Total Cost should display correctly [Base Amount Column]",
					"The Overhead Total Cost doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_overhead1CostBaseAmountCostingSummaryInfiniteSecondBatch)) {
			writeTestResults("Verify that the Cost of Overhead 01 is display correctly [Base Amount Column]",
					"The Cost of Overhead 01 should display correctly [Base Amount Column]",
					"The Cost of Overhead 01 successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead 01 is display correctly [Base Amount Column]",
					"The Cost of Overhead 01 should display correctly [Base Amount Column]",
					"The Cost of Overhead 01 doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_overhead2CostBaseAmountCostingSummaryInfiniteSecondBatch)) {
			writeTestResults("Verify that the Cost of Overhead 02 is display correctly [Base Amount Column]",
					"The Cost of Overhead 02 should display correctly [Base Amount Column]",
					"The Cost of Overhead 02 successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead 02 is display correctly [Base Amount Column]",
					"The Cost of Overhead 02 should display correctly [Base Amount Column]",
					"The Cost of Overhead 02 doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_overhead3CostBaseAmountCostingSummaryInfiniteSecondBatch)) {
			writeTestResults("Verify that the Cost of Overhead 03 is display correctly [Base Amount Column]",
					"The Cost of Overhead 03 should display correctly [Base Amount Column]",
					"The Cost of Overhead 03 successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead 03 is display correctly [Base Amount Column]",
					"The Cost of Overhead 03 should display correctly [Base Amount Column]",
					"The Cost of Overhead 03 doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_totalCostBaseAmountCostingSummaryInfiniteSecondBatch)) {
			writeTestResults("Verify that the Total Cost is display correctly [Base Amount Column]",
					"The Total Cost should display correctly [Base Amount Column]",
					"The Total Cost successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Total Cost is display correctly [Base Amount Column]",
					"The Total Cost should display correctly [Base Amount Column]",
					"The Total Cost doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_unitCostBaseAmountCostingSummaryInfiniteSecondBatch)) {
			writeTestResults("Verify that the Unit Cost is display correctly [Base Amount Column]",
					"The Unit Cost should display correctly [Base Amount Column]",
					"The Unit Cost successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Unit Cost is display correctly [Base Amount Column]",
					"The Unit Cost should display correctly [Base Amount Column]",
					"The Unit Cost doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_unitCostResourseBaseAmountCostingSummaryInfiniteSecondBatch)) {
			writeTestResults("Verify that the Cost of Resource (Unit Cost) is display correctly [Base Amount Column]",
					"The Cost of Resource (Unit Cost) should display correctly [Base Amount Column]",
					"The Cost of Resource (Unit Cost) successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Resource (Unit Cost) is display correctly [Base Amount Column]",
					"The Cost of Resource (Unit Cost) should display correctly [Base Amount Column]",
					"The Cost of Resource (Unit Cost) doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_unitCostMateraialBaseAmountCostingSummaryInfiniteSecondBatch)) {
			writeTestResults("Verify that the Cost of Material (Unit Cost) is display correctly [Base Amount Column]",
					"The Cost of Material (Unit Cost) should display correctly [Base Amount Column]",
					"The Cost of Material (Unit Cost) successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Material (Unit Cost) is display correctly [Base Amount Column]",
					"The Cost of Material (Unit Cost) should display correctly [Base Amount Column]",
					"The Cost of Material (Unit Cost) doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_unitCostOverheadBaseAmountCostingSummaryInfiniteSecondBatch)) {
			writeTestResults("Verify that the Cost of Overhead (Unit Cost) is display correctly [Base Amount Column]",
					"The Cost of Overhead (Unit Cost) should display correctly [Base Amount Column]",
					"The Cost of Overhead (Unit Cost) successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead (Unit Cost) is display correctly [Base Amount Column]",
					"The Cost of Overhead (Unit Cost) should display correctly [Base Amount Column]",
					"The Cost of Overhead (Unit Cost) doesn't display correctly [Base Amount Column]", "fail");
		}
	}

	/* Generate Internal Receipt PROD_E2E_002 */
	public void internalReciptAndJournelEntrySecondBatch_PROD_E2E_002() throws Exception {
		explicitWaitUntillClickable(btn_action2, 50);
		click(btn_action2);

		explicitWaitUntillClickable(btn_convertToInternalReceipt, 10);
		if (isDisplayed(btn_convertToInternalReceipt)) {
			writeTestResults("Verify that Convert to Internel Recipt option is available under Actions",
					"Convert to Internel Recipt option should be available under Actions",
					"Convert to Internel Recipt option successfully available under Actions", "pass");
		} else {
			writeTestResults("Verify that Convert to Internel Recipt option is available under Actions",
					"Convert to Internel Recipt option should be available under Actions",
					"Convert to Internel Recipt option doesn't available under Actions", "fail");
		}
		click(btn_convertToInternalReceipt);

		explicitWait(header_genarateInternalReciptSmoke, 20);
		if (isDisplayed(header_genarateInternalReciptSmoke)) {
			writeTestResults("Verify that genarate internel receipt window is open",
					"genarate internel receipt window should be open",
					"genarate internel receipt window succeessfully open", "pass");
		} else {
			writeTestResults("Verify that genarate internel receipt window is open",
					"genarate internel receipt window should be open", "genarate internel receipt window doesn't open",
					"fail");
		}

		click(chk_seletcOroductForInternalReciptSecondBatch);

		if (isSelected(chk_seletcOroductForInternalReciptSecondBatch)) {
			writeTestResults("Verify user able to select the product line",
					"User should be able to select the product line", "User successfully select the product line",
					"pass");
		} else {
			writeTestResults("Verify user able to select the product line",
					"User should be able to select the product line", "User doesn't select the product line", "fail");
		}

		String enteredReceiptDate = getAttribute(txt_reciptDateGenarateInternalREceiptWindow, "value");
		click(btn_genarateInternalRecipt);
		explicitWait(para_validationInternalREciptGenarateSuccessfull, 10);
		if (isDisplayed(para_validationInternalREciptGenarateSuccessfull)) {
			writeTestResults("Verify that succesfull validation is appear for Internal Receipt Genaration",
					"Succesfull validation should appear for Internal Receipt Genaration",
					"Succesfull validation is appear for Internal Receipt Genaration", "pass");
		} else {
			writeTestResults("Verify that succesfull validation is appear for Internal Receipt Genaration",
					"Succesfull validation should appear for Internal Receipt Genaration",
					"Succesfull validation doesn't appear for Internal Receipt Genaration", "fail");
		}

		waitUntillNotVisible(para_validationInternalREciptGenarateSuccessfull, 10);
		Thread.sleep(3500);
		explicitWaitUntillClickable(lnk_genaratedInternalREciptSmokeSecondBatch, 15);
		Thread.sleep(3000);
		click(lnk_genaratedInternalREciptSmokeSecondBatch);
		Thread.sleep(2000);
		switchWindow();
		Thread.sleep(1000);
		explicitWait(header_releasedInternalRecipt, 10);
		trackCode = getText(lbl_docNumber);
		if (isDisplayed(header_releasedInternalRecipt)) {
			writeTestResults("Verify that Internal Receiptis open as released status",
					"Internal Receipt should be open as released status",
					"Internal Receipt successfully open as released status", "pass");
		} else {
			writeTestResults("Verify that Internal Receiptis open as released status",
					"Internal Receipt should be open as released status",
					"Internal Receipt doesn't open or not in released status", "fail");
		}

		String docDate = getText(a_documentDate);
		String postDate = getText(a_postDate);
		String dueDate = getText(a_dueDate);

		if (docDate.equals(enteredReceiptDate) && docDate.equals(postDate) && docDate.equals(dueDate)) {
			writeTestResults(
					"Verify that all three header dates are equal to the date that entered when generating Internal Receipt",
					"All three header dates should be equal to the date that entered when generating Internal Receipt",
					"All three header dates are equal to the date that entered when generating Internal Receipt",
					"pass");
		} else {
			writeTestResults(
					"Verify that all three header dates are equal to the date that entered when generating Internal Receipt",
					"All three header dates should be equal to the date that entered when generating Internal Receipt",
					"All three header dates are not equal to the date that entered when generating Internal Receipt",
					"fail");
		}

		String warehouseOnInternalReceipt = getText(div_warehouseInternalRecipt);
		if (warehouseOnInternalReceipt.equals(outputWarehouseLI)) {
			writeTestResults(
					"Verify that Output Warehouse of Production Order and Warehouse of Internal Receipt is same",
					"Output Warehouse of Production Order and Warehouse of Internal Receipt should be same",
					"Output Warehouse of Production Order and Warehouse of Internal Receipt is same", "pass");
		} else {
			writeTestResults(
					"Verify that Output Warehouse of Production Order and Warehouse of Internal Receipt is same",
					"Output Warehouse of Production Order and Warehouse of Internal Receipt should be same",
					"Output Warehouse of Production Order and Warehouse of Internal Receipt is not same", "fail");
		}

		String qtyInternalReceipt = getText(div_planedQtyInternalRecipt);
		if (qtyInternalReceipt.equals("70.00")) {
			writeTestResults("Verify that quantity is equel to the QC pass quantity",
					"Quantity should be equel to the QC pass quantity",
					"Quantity successfully equel to the QC pass quantity", "pass");
		} else {
			writeTestResults("Verify that quantity is equel to the QC pass quantity",
					"Quantity should be equel to the QC pass quantity",
					"Quantity successfully not equel to the QC pass quantity", "fail");
		}

		click(btn_action2);

		explicitWaitUntillClickable(btn_journal, 10);
		if (isDisplayed(btn_journal)) {
			writeTestResults("Verify that Journal Entry option is available under Actions",
					"Journal Entry option should be available under Actions",
					"Journal Entry option successfully available under Actions", "pass");
		} else {
			writeTestResults("Verify that Journal Entry option is available under Actions",
					"Journal Entry option should be available under Actions",
					"Journal Entry option doesn't available under Actions", "fail");
		}
		click(btn_journal);

		explicitWait(header_journalEntryPopup, 10);
		if (isDisplayed(header_journalEntryPopup)) {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded",
					"Journal Entry details pop up successfully get loaded", "pass");
		} else {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded", "Journal Entry details pop up doesn't get loaded",
					"fail");
		}

		if (isDisplayedQuickCheck(lbl_debitValueJournelEntry)) {
			/* QA */
			String debitAmount = getText(lbl_debitValueJournelEntry);

			if (debitAmount.startsWith("1410.51")) {
				writeTestResults("Verify that amount was debited to the relevent GL Account",
						"Amount should be debited to the relevent GL Account",
						"Amount successfully debited to the relevent GL Account", "pass");
			} else {
				writeTestResults("Verify that amount was debited to the relevent GL Account",
						"Amount should be debited to the relevent GL Account",
						"Amount doesn't debited to the relevent GL Account", "fail");
			}

			String creditAmount = getText(lbl_creditValueJournelEntry);
			if (creditAmount.startsWith("1410.51")) {
				writeTestResults("Verify that amount was credited to the relevent GL Account",
						"Amount should be credited to the relevent GL Account",
						"Amount successfully credited to the relevent GL Account", "pass");
			} else {
				writeTestResults("Verify that amount was credited to the relevent GL Account",
						"Amount should be credited to the relevent GL Account",
						"Amount doesn't credited to the relevent GL Account", "fail");
			}

			String debitTotal = getText(lbl_debitTotalJournelEntrySingleEntry);
			String credTotal = getText(lbl_creditTotalJournelEntrySingleEntry);

			if (debitTotal.startsWith("1410.51") && debitTotal.equals(credTotal)) {
				writeTestResults("Verify total debit and credit amounts were balanced",
						"Total debit and credit amounts should balanced",
						"Total debit and credit amounts successfully balanced", "pass");
			} else {
				writeTestResults("Verify total debit and credit amounts were balanced",
						"Total debit and credit amounts should balanced",
						"Total debit and credit amounts weren't balanced", "fail");
			}

		} else {
			/* Staging */
			String debitAmount = getText(lbl_debitValueJournelEntry);

			if (debitAmount.startsWith("1410.51")) {
				writeTestResults("Verify that amount was debited to the relevent GL Account",
						"Amount should be debited to the relevent GL Account",
						"Amount successfully debited to the relevent GL Account", "pass");
			} else {
				writeTestResults("Verify that amount was debited to the relevent GL Account",
						"Amount should be debited to the relevent GL Account",
						"Amount doesn't debited to the relevent GL Account", "fail");
			}

			String creditAmount = getText(lbl_creditValueJournelEntry);
			if (creditAmount.startsWith("1410.51")) {
				writeTestResults("Verify that amount was credited to the relevent GL Account",
						"Amount should be credited to the relevent GL Account",
						"Amount successfully credited to the relevent GL Account", "pass");
			} else {
				writeTestResults("Verify that amount was credited to the relevent GL Account",
						"Amount should be credited to the relevent GL Account",
						"Amount doesn't credited to the relevent GL Account", "fail");
			}

			String debitTotal = getText(lbl_debitTotalJournelEntrySingleEntry);
			String credTotal = getText(lbl_creditTotalJournelEntrySingleEntry);

			if (debitTotal.startsWith("1410.51") && debitTotal.equals(credTotal)) {
				writeTestResults("Verify total debit and credit amounts were balanced",
						"Total debit and credit amounts should balanced",
						"Total debit and credit amounts successfully balanced", "pass");
			} else {
				writeTestResults("Verify total debit and credit amounts were balanced",
						"Total debit and credit amounts should balanced",
						"Total debit and credit amounts weren't balanced", "fail");
			}
		}
	}

	/*-----Third Batch-----*/
	/* Shop Floor Update PROD_E2E_002 */
	public void shopFloorUpdateThirdBatch_PROD_E2E_002() throws InvalidFormatException, IOException, Exception {
		openPage(readProductionData("ReleaseedPO_PRD_E2E_002"));
		explicitWaitUntillClickable(btn_action2, 50);
		click(btn_action2);

		explicitWaitUntillClickable(btn_shopFloorUpdate2, 10);
		if (isDisplayed(btn_shopFloorUpdate2)) {
			writeTestResults("Verify that Shop floor update option is available under Actions",
					"Shop floor update option should be available under Actions",
					"Shop floor update option successfully available under Actions", "pass");
		} else {
			writeTestResults("Verify that Shop floor update option is available under Actions",
					"Shop floor update option should be available under Actions",
					"Shop floor update option doesn't available under Actions", "fail");
		}
		click(btn_shopFloorUpdate2);

		explicitWait(header_shopFloorUpdateWindow, 20);
		if (isDisplayed(header_shopFloorUpdateWindow)) {
			writeTestResults("Verify that shop floor update window is open", "Shop floor update window should be open",
					"Shop floor update window succeessfully open", "pass");
		} else {
			writeTestResults("Verify that shop floor update window is open", "Shop floor update window should be open",
					"Shop floor update window doesn't open", "fail");
		}

		/* Add new batch */
		selectText(drop_batchNoShopFloor, "--None--");
		click(btn_addNewBatchShopFloor);
		explicitWaitUntillClickable(btn_genarateBatchShopFloor, 10);
		click(btn_genarateBatchShopFloor);
		explicitWait(btn_startFirstOperationSFU, 15);
		genaratedBatch = getSelectedOptionInDropdown(drop_batchNoShopFloor);

		/* First Operation */
		System.out.println("First Operation");
		click(btn_startFirstOperationSFU);
		explicitWaitUntillClickable(btn_startSecondLeveleSFU, 10);
		if (isDisplayed(btn_startSecondLeveleSFU)) {
			writeTestResults("Verify that start button is available", "Start button should be available",
					"Start button successfully available", "pass");
		} else {
			writeTestResults("Verify that start button is available", "Start button should be available",
					"Start button doesn't available", "fail");
		}

		click(btn_startSecondLeveleSFU);

		explicitWaitUntillClickable(btn_actualUpdateSFU, 10);
		if (isDisplayed(btn_actualUpdateSFU)) {
			writeTestResults("Verify that actual update option is available on shop floor update main window",
					"Actual update option should be available on shop floor update main window",
					"Actual update option successfully available on shop floor update main window", "pass");
		} else {
			writeTestResults("Verify that actual update option is available on shop floor update main window",
					"Actual update option should be available on shop floor update main window",
					"Actual update option doesn't available on shop floor update main window", "fail");
		}

		click(btn_actualUpdateSFU);

		Thread.sleep(2500);
		sendKeys(txt_outoutQuantityACtualUpdateSFU, actualQuantityOutputProductFirstOperation);
		String outputActualUpdateQuantityFirstOperation = getAttribute(txt_outoutQuantityACtualUpdateSFU, "value");
		if (outputActualUpdateQuantityFirstOperation.equals(actualQuantityOutputProductFirstOperation)) {
			writeTestResults("Verify user able to enter Output Product qty for the first operation",
					"User should be able to enter Output Product qty for the first operation",
					"User successfully enter Output Product qty for the first operation", "pass");
		} else {
			writeTestResults("Verify user able to enter Output Product qty for the first operation",
					"User should be able to enter Output Product qty for the first operation",
					"User doesn't enter Output Product qty for the first operation", "fail");
		}
		Thread.sleep(1000);

		click(btn_upateActualSFU);
		explicitWait(para_successfullValidationOutProductActualUpdate, 8);
		if (isDisplayed(para_successfullValidationOutProductActualUpdate)) {
			writeTestResults("Verify that successfully updated validation is appear for the Output Product update",
					"Successfully updated validation should appear for the Output Product update",
					"Successfully updated validation successfully appear for the Output Product update", "pass");
		} else {
			writeTestResults("Verify that successfully updated validation is appear for the Output Product update",
					"Successfully updated validation should appear for the Output Product update",
					"Successfully updated validation doesn't appear for the Output Product update", "fail");
		}
		Thread.sleep(3000);

		click(btn_tapInputProductsShopFloorUpdate);

		sendKeys(txt_actualUpdaInputProductQuantityFirstOperation, actualQuantityInputProductFirstOperation);
		String inputActualUpdateQuantityFirstOperation = getAttribute(txt_actualUpdaInputProductQuantityFirstOperation,
				"value");
		if (inputActualUpdateQuantityFirstOperation.equals(actualQuantityInputProductFirstOperation)) {
			writeTestResults("Verify user able to enter Input Product qty for the first operation - First Material",
					"User should be able to enter Input Product qty for the first operation - First Material",
					"User successfully enter Input Product qty for the first operation - First Material", "pass");
		} else {
			writeTestResults("Verify user able to enter Input Product qty for the first operation - First Material",
					"User should be able to enter Input Product qty for the first operation - First Material",
					"User doesn't enter Input Product qty for the first operation - First Material", "fail");
		}

		sendKeys(txt_actualUpdaInputProductQuantity2FirstOperation, actualQuantity2InputProductFirstOperation);
		String inputActualUpdateQuantity2FirstOperation = getAttribute(
				txt_actualUpdaInputProductQuantity2FirstOperation, "value");
		if (inputActualUpdateQuantity2FirstOperation.equals(actualQuantity2InputProductFirstOperation)) {
			writeTestResults("Verify user able to enter Input Product qty for the first operation - Second Material",
					"User should be able to enter Input Product qty for the first operation - Second Material",
					"User successfully enter Input Product qty for the first operation - Second Material", "pass");
		}

		click(btn_upateActualSFU);
		explicitWait(para_successfullValidationActualUpdate, 8);
		if (isDisplayedQuickCheck(para_successfullValidationActualUpdate)) {
			writeTestResults("Verify that successfully updated validation is appear for the Input Product update",
					"Successfully updated validation should appear for the Input Product update",
					"Successfully updated validation successfully appear for the Input Product update", "pass");
		} else {
			click(span_firstErrorOnGridInputProductActualUpdate);
			Thread.sleep(1500);
			click(span_firstErrorOnGridInputProductActualUpdate);
			explicitWait(div_notEnoughQuantityAvaialableValidation, 5);
			if (isDisplayed(div_notEnoughQuantityAvaialableValidation)) {
				Thread.sleep(1500);
				((JavascriptExecutor) driver).executeScript("window.open('about:blank','_blank');");
				Thread.sleep(1000);
				switchWindow();
				openPage(siteUrlMultiPlatformm);
				purchaseOrderToInboundShipmentToPurchaseInvoice();
				Thread.sleep(2000);
				click(btn_upateActualSFU);
				explicitWait(para_successfullValidationActualUpdate, 8);
				if (isDisplayedQuickCheck(para_successfullValidationActualUpdate)) {
					writeTestResults(
							"Verify that successfully updated validation is appear for the Input Product update",
							"Successfully updated validation should appear for the Input Product update",
							"Successfully updated validation successfully appear for the Input Product update", "pass");
				} else {
					writeTestResults(
							"Verify that successfully updated validation is appear for the Input Product update",
							"Successfully updated validation should appear for the Input Product update",
							"Successfully updated validation doesn't appear for the Input Product update", "fail");
				}
			}
		}

		Thread.sleep(3000);

		explicitWaitUntillClickable(tab_resourseShopFloorUpdate, 30);
		click(tab_resourseShopFloorUpdate);
		explicitWaitUntillClickable(div_actualHoursResourseTabRow01, 20);
		Thread.sleep(2000);
		click(txt_actualDuarationResourse01ActualUpdate);
		Thread.sleep(1500);
		click(btn_oneHouserActualDurationResourse01);
		Thread.sleep(1000);
		click(btn_okActualDurationResourse01);
		Thread.sleep(1500);

		click(txt_actualDuarationResourse02ActualUpdate);
		Thread.sleep(1500);
		click(btn_oneHouserActualDurationResourse02);
		Thread.sleep(1000);
		click(btn_okActualDurationResourse02);
		Thread.sleep(1500);

		Thread.sleep(2000);
		String actualHoursRow1 = getAttribute(div_actualHoursResourseTabRow01, "value");
		String actualminsRow1 = getAttribute(div_actualMinsResourseTabRow01, "value");
		String actualHoursRow2 = getAttribute(div_actualHoursResourseTabRow02, "value");
		String actualminsRow2 = getAttribute(div_actualMinsResourseTabRow02, "value");

		if (actualHoursRow1.equals("01") && actualminsRow1.equals("00") && actualHoursRow2.equals("01")
				&& actualminsRow2.equals("00")) {
			writeTestResults("Verify that actual resource hours are loaded correctly",
					"Actual resource hours should be loaded correctly",
					"Actual resource hours successfully loaded correctly", "pass");
		} else {
			writeTestResults("Verify that actual resource hours are loaded correctly",
					"Actual resource hours should be loaded correctly",
					"Actual resource hours doesn't loaded correctly", "fail");
		}

		click(btn_upateActualSFU);
		explicitWait(para_successfullValidationActualUpdateResourse, 8);
		if (isDisplayed(para_successfullValidationActualUpdateResourse)) {
			writeTestResults("Verify that successfully updated validation is appear for the Resource update",
					"Successfully updated validation should appear for the Resource update",
					"Successfully updated validation successfully appear for the Resource update", "pass");
		} else {
			writeTestResults("Verify that successfully updated validation is appear for the Resource update",
					"Successfully updated validation should appear for the Resource update",
					"Successfully updated validation doesn't appear for the Resource update", "fail");
		}

		Thread.sleep(3000);

		click(btn_closeFisrstOperationActualUpdateWindow);

		explicitWait(btn_completeFirstOperation, 8);
		if (isDisplayed(btn_completeFirstOperation)) {
			writeTestResults("Verify that complete button is available for the first operation",
					"Complete button should be available for the first operation",
					"Complete button successfully available for the first operation", "pass");
		} else {
			writeTestResults("Verify that complete button is available for the first operation",
					"Complete button should be available for the first operation",
					"Complete button doesn't available for the first operation", "fail");
		}
		click(btn_completeFirstOperation);

		explicitWait(btn_completeThirdBatchShopFloor, 8);
		if (isDisplayed(btn_completeThirdBatchShopFloor)) {
			writeTestResults("Verify that complete button is available for the complete window of first operation",
					"Complete button should be available for the complete window of first operation",
					"Complete button successfully available for the complete window of first operation", "pass");
		} else {
			writeTestResults("Verify that complete button is available for the complete window of first operation",
					"Complete button should be available for the complete window of first operation",
					"Complete button doesn't available for the complete window of first operation", "fail");
		}
		click(btn_completeThirdBatchShopFloor);

		explicitWait(lbl_completedShopFloorUpdateBarFirstOperation, 8);
		if (isDisplayed(lbl_completedShopFloorUpdateBarFirstOperation)) {
			writeTestResults("Verify that shop flow update is completed for the first operation",
					"Shop flow update should be completed for the first operation",
					"Shop flow update successfully completed for the first operation", "pass");
		} else {
			writeTestResults("Verify that shop flow update is completed for the first operation",
					"Shop flow update should be completed for the first operation",
					"Shop flow update doesn't completed for the first operation", "pass");
		}

		/* Second Operation */
		System.out.println("Second Operation");

		explicitWaitUntillClickable(btn_actualUpdateSFU, 10);
		if (isDisplayed(btn_actualUpdateSFU)) {
			writeTestResults(
					"Verify that actual update option is available on shop floor update main window for second operation",
					"Actual update option should be available on shop floor update main window for second operation",
					"Actual update option successfully available on shop floor update main window for second operation",
					"pass");
		} else {
			writeTestResults(
					"Verify that actual update option is available on shop floor update main window for second operation",
					"Actual update option should be available on shop floor update main window for second operation",
					"Actual update option doesn't available on shop floor update main window for second operation",
					"fail");
		}

		click(btn_actualUpdateSFU);

		sendKeys(txt_outoutQuantityACtualUpdateSFU, actualQuantityOutputProductSecondOperation);
		String outputActualUpdateQuantitySecondOperation = getAttribute(txt_outoutQuantityACtualUpdateSFU, "value");
		if (outputActualUpdateQuantitySecondOperation.equals(actualQuantityOutputProductSecondOperation)) {
			writeTestResults("Verify user able to enter Output Product qty for the second operation",
					"User should be able to enter Output Product qty for the second operation",
					"User successfully enter Output Product qty for the second operation", "pass");
		} else {
			writeTestResults("Verify user able to enter Output Product qty for the second operation",
					"User should be able to enter Output Product qty for the second operation",
					"User doesn't enter Output Product qty for the second operation", "fail");
		}
		Thread.sleep(1000);

		click(btn_upateActualSFU);
		explicitWait(para_successfullValidationOutProductActualUpdate, 8);
		if (isDisplayed(para_successfullValidationOutProductActualUpdate)) {
			writeTestResults("Verify that successfully updated validation is appear for the Output Product update",
					"Successfully updated validation should appear for the Output Product update",
					"Successfully updated validation successfully appear for the Output Product update", "pass");
		} else {
			writeTestResults("Verify that successfully updated validation is appear for the Output Product update",
					"Successfully updated validation should appear for the Output Product update",
					"Successfully updated validation doesn't appear for the Output Product update", "fail");
		}
		Thread.sleep(3000);

		click(btn_tapInputProductsShopFloorUpdate);

		sendKeys(txt_actualUpdaInputProductQuantitySecondOperation, actualQuantityInputProductSecondOperation);
		String inputActualUpdateQuantitySecondOperation = getAttribute(
				txt_actualUpdaInputProductQuantitySecondOperation, "value");
		if (inputActualUpdateQuantitySecondOperation.equals(actualQuantityInputProductSecondOperation)) {
			writeTestResults("Verify user able to enter Input Product qty for the second operation",
					"User should be able to enter Input Product qty for the second operation",
					"User successfully enter Input Product qty for the second operation", "pass");
		} else {
			writeTestResults("Verify user able to enter Input Product qty for the second operation",
					"User should be able to enter Input Product qty for the second operation",
					"User doesn't enter Input Product qty for the second operation", "fail");
		}

		click(btn_upateActualSFU);
		explicitWait(para_successfullValidationActualUpdate, 8);
		if (isDisplayedQuickCheck(para_successfullValidationActualUpdate)) {
			writeTestResults("Verify that successfully updated validation is appear for the Input Product update",
					"Successfully updated validation should appear for the Input Product update",
					"Successfully updated validation successfully appear for the Input Product update", "pass");
		} else {
			writeTestResults("Verify that successfully updated validation is appear for the Input Product update",
					"Successfully updated validation should appear for the Input Product update",
					"Successfully updated validation doesn't appear for the Input Product update", "fail");
		}
		Thread.sleep(3000);

		click(btn_closeSecondOperationActualUpdateWindow);

		explicitWait(btn_completeSecondOperation, 8);
		if (isDisplayed(btn_completeSecondOperation)) {
			writeTestResults("Verify that complete button is available for the second operation",
					"Complete button should be available for the second operation",
					"Complete button successfully available for the second operation", "pass");
		} else {
			writeTestResults("Verify that complete button is available for the second operation",
					"Complete button should be available for the second operation",
					"Complete button doesn't available for the second operation", "fail");
		}
		click(btn_completeSecondOperation);

		explicitWait(btn_completeThirdBatchShopFloor, 8);
		if (isDisplayed(btn_completeThirdBatchShopFloor)) {
			writeTestResults("Verify that complete button is available for the complete window of second operation",
					"Complete button should be available for the complete window of second operation",
					"Complete button successfully available for the complete window of second operation", "pass");
		} else {
			writeTestResults("Verify that complete button is available for the complete window of second operation",
					"Complete button should be available for the complete window of second operation",
					"Complete button doesn't available for the complete window of second operation", "fail");
		}
		click(btn_completeThirdBatchShopFloor);

		explicitWait(lbl_completedShopFloorUpdateBarSecondOperation, 8);
		if (isDisplayed(lbl_completedShopFloorUpdateBarSecondOperation)) {
			writeTestResults("Verify that shop flow update is completed for the second operation",
					"Shop flow update should be completed for the second operation",
					"Shop flow update successfully completed for the second operation", "pass");
		} else {
			writeTestResults("Verify that shop flow update is completed for the second operation",
					"Shop flow update should be completed for the second operation",
					"Shop flow update doesn't completed for the second operation", "pass");
		}
	}

	/* QC PROD_E2E_002 */
	public void qcThirdBatchPROD_E2E_002() throws Exception {
		pageRefersh();
		explicitWait(img_navigationPane, 20);
		Thread.sleep(2000);
		explicitWait(img_navigationPane, 20);
		click(img_navigationPane);
		explicitWait(btn_productionModule, 10);
		click(btn_productionModule);
		explicitWait(btn_productionQCAndSorting, 10);
		if (isDisplayed(btn_productionQCAndSorting)) {
			writeTestResults("Verif that Production QC & Sorting option is display under Production Module",
					"Production QC & Sorting option should be display under Production Module",
					"Production QC & Sorting option successfully display under Production Module", "pass");
		} else {
			writeTestResults("Verif that Production QC & Sorting option is display under Production Module",
					"Production QC & Sorting option should be display under Production Module",
					"Production QC & Sorting option doesn't display under Production Module", "fail");
		}

		click(btn_productionQCAndSorting);

		explicitWait(drop_documentTypeProductionQC, 50);
		selectText(drop_documentTypeProductionQC, qcTypeProductionOrder);
		Thread.sleep(1000);
		String selecteDocType = getSelectedOptionInDropdown(drop_documentTypeProductionQC);
		if (selecteDocType.equals(qcTypeProductionOrder)) {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User able to select 'Production Order' as document type", "pass");
		} else {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User not able to select 'Production Order' as document type", "fail");

		}

		sendKeys(txt_docNumberQC, readProductionData("ProductionOrder_PROD_E2E_002"));
		String enteredPO = getAttribute(txt_docNumberQC, "value");
		if (enteredPO.equals(readProductionData("ProductionOrder_PROD_E2E_002"))) {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User successfully enter Production Order document number", "pass");
		} else {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User doesn't enter Production Order document number", "fail");
		}

		Thread.sleep(1000);
		driver.findElement(getLocator(txt_docNumberQC)).sendKeys(Keys.ARROW_DOWN);
		pressEnter(txt_docNumberQC);

		explicitWait(div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_002")),
				30);
		if (isDisplayed(
				div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_002")))) {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User successfully select the relevent Production Order", "pass");
		} else {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User doesn't select the relevent Production Order", "fail");
		}

		Thread.sleep(1500);

		/* Third QC Scenario */
		mouseMove(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")));
		click(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")));

		if (isSelected(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")))) {
			writeTestResults("Verify user able to select the relevent Output Product",
					"User should be able to select the relevent Output Product",
					"User successfully select the relevent Output Product", "pass");
		} else {
			writeTestResults("Verify user able to select the relevent Output Product",
					"User should be able to select the relevent Output Product",
					"User doesn't select the relevent Output Product", "fail");
		}

		Thread.sleep(1500);
		sendKeys(txt_passQuantityProductionQCOneRow, thirdPassQty);
		String enteredPassQty = getAttribute(txt_passQuantityProductionQCOneRow, "value");
		if (enteredPassQty.equals(thirdPassQty)) {
			writeTestResults("Verify user able to enter pass qty (70)", "User should be able to enter pass qty (70)",
					"User successfully enter pass qty (70)", "pass");
		} else {
			writeTestResults("Verify user able to enter pass qty (70)", "User should be able to enter pass qty (70)",
					"User doesn't enter pass qty (70)", "fail");
		}
		Thread.sleep(1000);

		sendKeys(txt_failQuantityProductionQCOneRow, thirdFailQty);
		String enteredFailQty3 = getAttribute(txt_failQuantityProductionQCOneRow, "value");
		if (enteredFailQty3.equals(thirdFailQty)) {
			writeTestResults("Verify user able to enter fail qty (30)", "User should be able to enter fail qty (30)",
					"User successfully enter fail qty (30)", "pass");
		} else {
			writeTestResults("Verify user able to enter fail qty (30)", "User should be able to enter fail qty (30)",
					"User doesn't enter fail qty (30)", "fail");
		}

		click(btn_releseProductionQC);
		explicitWaitUntillPresent(para_successfullValidationProductionQC, 20);

		if (isElementPresent(para_successfullValidationProductionQC)) {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation is appear", "pass");
		} else {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation doesn't appear", "fail");
		}

		Thread.sleep(2000);
		waitUntillNotVisible(
				div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_002")), 15);
		if (!isDisplayedQuickCheck(
				div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_002")))) {
			writeTestResults("Verify that relevent Production Order is withdraw from pending tab",
					"Relevent Production Order should be withdraw from pending tab",
					"Relevent Production Order successfully withdraw from pending tab", "pass");
		} else {
			writeTestResults("Verify that relevent Production Order is withdraw from pending tab",
					"Relevent Production Order should be withdraw from pending tab",
					"Relevent Production Order still available on pending tab", "fail");
		}

		click(tab_historyQC);
		explicitWaitUntillClickable(tab_historySelectedQC, 20);
		if (isDisplayed(tab_historySelectedQC)) {
			writeTestResults("Verify that user able to open history tab", "User should be able to open history tab",
					"User successfully open history tab", "pass");
		} else {
			writeTestResults("Verify that user able to open history tab", "User should be able to open history tab",
					"User couldn't open history tab", "fail");
		}

		explicitWait(drop_documentTypeProductionQC, 50);
		selectText(drop_documentTypeProductionQC, qcTypeProductionOrder);
		Thread.sleep(1000);
		selecteDocType = getSelectedOptionInDropdown(drop_documentTypeProductionQC);
		if (selecteDocType.equals(qcTypeProductionOrder)) {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User able to select 'Production Order' as document type", "pass");
		} else {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User not able to select 'Production Order' as document type", "fail");

		}

		sendKeys(txt_docNumberQC, readProductionData("ProductionOrder_PROD_E2E_002"));
		String enteredPO6 = getAttribute(txt_docNumberQC, "value");
		if (enteredPO6.equals(readProductionData("ProductionOrder_PROD_E2E_002"))) {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User successfully enter Production Order document number", "pass");
		} else {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User doesn't enter Production Order document number", "fail");
		}

		Thread.sleep(1000);
		driver.findElement(getLocator(txt_docNumberQC)).sendKeys(Keys.ARROW_DOWN);
		pressEnter(txt_docNumberQC);

		explicitWait(div_selectedQcDocProductionHistoryTab.replace("doc_no",
				readProductionData("ProductionOrder_PROD_E2E_002")), 30);
		if (isDisplayed(div_selectedQcDocProductionHistoryTab.replace("doc_no",
				readProductionData("ProductionOrder_PROD_E2E_002")))) {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User successfully select the relevent Production Order", "pass");
		} else {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User doesn't select the relevent Production Order", "fail");
		}

		int docCount3 = getCount(dic_relevantHistoryDocCount);
		int i3 = 1;

		while (i3 <= docCount3) {
			if (isDisplayedQuickCheck(div_batchNoReleventToHistoryDocument.replace("changing_part", genaratedBatch))) {
				break;
			}

			i3++;
			click(dic_relevantHistoryDocument.replace("changing_part", String.valueOf(i3)));

		}

		Thread.sleep(2000);
		if (isDisplayed(column_headerPassedQtyHistoryTabQC)) {
			writeTestResults("Verify that pass qty column header is display as earlier",
					"Pass qty column header should be display as earlier",
					"Pass qty column header successfully display as earlier", "pass");
		} else {
			writeTestResults("Verify that pass qty column header is display as earlier",
					"Pass qty column header should be display as earlier",
					"Pass qty column header doesn't display as earlier", "fail");
		}

		String passQuantity = getAttribute(txt_passedQtyQC, "value");
		if (passQuantity.equals("70.00")) {
			writeTestResults("Verify that pass qty is display in history tab", "Pass qty should display in history tab",
					"Pass qty successfully display in history tab", "pass");
		} else {
			writeTestResults("Verify that pass qty is display in history tab", "Pass qty should display in history tab",
					"Pass qty doesn't display in history tab", "fail");
		}

		Thread.sleep(1000);
		if (isDisplayed(column_headerFailedQtyHistoryTabQC)) {
			writeTestResults("Verify that fail qty column header is display as earlier",
					"Fail qty column header should be display as earlier",
					"Fail qty column header successfully display as earlier", "pass");
		} else {
			writeTestResults("Verify that fail qty column header is display as earlier",
					"Fail qty column header should be display as earlier",
					"Fail qty column header doesn't display as earlier", "fail");
		}

		String failQuantity3 = getAttribute(txt_failedQtyQC, "value");
		if (failQuantity3.equals("30.00")) {
			writeTestResults("Verify that fail qty is display in history tab", "Fail qty should display in history tab",
					"Fail qty successfully display in history tab", "pass");
		} else {
			writeTestResults("Verify that fail qty is display in history tab", "Fail qty should display in history tab",
					"Fail qty doesn't display in history tab", "fail");
		}
	}

	/* Costing PROD_E2E_002 */
	public void productionCostingThirdBatch_PROD_E2E_002() throws Exception {
		openPage(readProductionData("ReleaseedPO_PRD_E2E_002"));
		explicitWaitUntillClickable(btn_action2, 50);
		click(btn_action2);

		explicitWaitUntillClickable(btn_productionCostingActions, 10);
		if (isDisplayed(btn_productionCostingActions)) {
			writeTestResults("Verify that Production Costing option is available under Actions",
					"Production Costing option should be available under Actions",
					"Production Costing option successfully available under Actions", "pass");
		} else {
			writeTestResults("Verify that Production Costing option is available under Actions",
					"Production Costing option should be available under Actions",
					"Production Costing option doesn't available under Actions", "fail");
		}
		click(btn_productionCostingActions);

		explicitWait(header_productionCostingWindow, 20);
		if (isDisplayed(header_productionCostingWindow)) {
			writeTestResults("Verify that production costing window is open",
					"Production costing window should be open", "Production costing window succeessfully open", "pass");
		} else {
			writeTestResults("Verify that production costing window is open",
					"Production costing window should be open", "Production costing window doesn't open", "fail");
		}

		explicitWaitUntillClickable(chk_pendingProductionCosting, 20);
		click(chk_pendingProductionCosting);

		if (isDisplayed(chk_pendingProductionCostingSelected)) {
			writeTestResults("Verify user able to select batch costing checkbox on production costing main window",
					"User should be able to select batch costing checkbox on production costing main window",
					"User able to select batch costing checkbox on production costing main window", "pass");
		} else {
			writeTestResults("Verify user able to select batch costing checkbox on production costing main window",
					"User should be able to select batch costing checkbox on production costing main window",
					"User not able to select batch costing checkbox on production costing main window", "fail");
		}

		click(btn_updateProductionCostingMainWindow);

		boolean visibilityFlag = false;
		for (int i = 1; i < 10; i++) {
			try {
				Thread.sleep(20);
				driver.findElement(getLocator(para_batchCostingUpdateValidation)).isDisplayed();
				visibilityFlag = true;
				break;
			} catch (Exception e) {
				continue;
			}
		}

		if (visibilityFlag) {
			writeTestResults("Verify that succesfull validation is appear for Batch Costing",
					"Succesfull validation should appear for Batch Costing",
					"Succesfull validation is appear for Batch Costing", "pass");
		} else {
			writeTestResults("Verify that succesfull validation is appear for Batch Costing",
					"Succesfull validation should appear for Batch Costing",
					"Succesfull validation doesn't appear for Batch Costing", "fail");
		}

//		WebElement element = wait.until(new Function<WebDriver, WebElement>() {
//			public WebElement apply(WebDriver driver) {
//				boolean visibilityOfValidation = false;
//				WebElement linkElement = driver.findElement(By.xpath(para_batchCostingUpdateValidation));
//				if (linkElement.isDisplayed()) {
//					visibilityOfValidation = true;
//				}
//
//				try {
//					if (visibilityOfValidation) {
//						writeTestResults("Verify that succesfull validation is appear for Batch Costing",
//								"Succesfull validation should appear for Batch Costing",
//								"Succesfull validation is appear for Batch Costing", "pass");
//					} else {
//						writeTestResults("Verify that succesfull validation is appear for Batch Costing",
//								"Succesfull validation should appear for Batch Costing",
//								"Succesfull validation doesn't appear for Batch Costing", "fail");
//					}
//
//				} catch (Exception e) {
//
//				}
//
//				return linkElement;
//
//			}
//		});

//		if (isDisplayedQuickCheck(para_batchCostingUpdateValidation)) {
//			writeTestResults("Verify that succesfull validation is appear for Batch Costing",
//					"Succesfull validation should appear for Batch Costing",
//					"Succesfull validation is appear for Batch Costing", "pass");
//		} else {
//			writeTestResults("Verify that succesfull validation is appear for Batch Costing",
//					"Succesfull validation should appear for Batch Costing",
//					"Succesfull validation doesn't appear for Batch Costing", "fail");
//		}

		explicitWait(btn_closeMainCostingUpdateWindowProductionOrder, 5);
		Thread.sleep(2500);
		click(btn_closeMainCostingUpdateWindowProductionOrder);

		explicitWait(header_releasedProductionOrder, 10);

		/* Costing Summary - Actual */
		explicitWaitUntillClickable(tab_costingSummaryPO, 60);
		click(tab_costingSummaryPO);
		explicitWaitUntillClickable(btn_expandCostingSummary, 20);

		click(btn_expandCostingSummary);
		Thread.sleep(500);
		explicitWaitUntillClickable(btn_expandCostingSummary, 20);
		click(btn_expandCostingSummary);
		explicitWaitUntillClickable(btn_expandCostingSummary, 20);
		click(btn_expandCostingSummary);
		explicitWaitUntillClickable(btn_expandCostingSummary, 20);
		click(btn_expandCostingSummary);

		if (isDisplayed(column_headerActualCostingSummary)) {
			writeTestResults("Verify that the Actual Column is display same as earlier",
					"The Actual Column should display same as earlier",
					"The Actual Column successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that the Actual Column is display same as earlier",
					"The Actual Column should display same as earlier",
					"The Actual Column doesn't display same as earlier", "fail");
		}

		if (isDisplayed(td_resourceCostActualCostingSummaryInfiniteThirdBatch)) {
			writeTestResults("Verify that the Resource Total Cost is display correctly [Actual Column]",
					"The Resource Total Cost should display correctly [Actual Column]",
					"The Resource Total Cost successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Resource Total Cost is display correctly [Actual Column]",
					"The Resource Total Cost should display correctly [Actual Column]",
					"The Resource Total Cost doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_resourceCost1ActualCostingSummaryInfiniteThirdBatch)) {
			writeTestResults("Verify that the Cost of Resource 01 is display correctly [Actual Column]",
					"The Cost of Resource 01 should display correctly [Actual Column]",
					"The Cost of Resource 01 successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Resource 01 is display correctly [Actual Column]",
					"The Cost of Resource 01 should display correctly [Actual Column]",
					"The Cost of Resource 01 doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_resourceCost2ActualCostingSummaryInfiniteThirdBatch)) {
			writeTestResults("Verify that the Cost of Resource 02 is display correctly [Actual Column]",
					"The Cost of Resource 02 should display correctly [Actual Column]",
					"The Cost of Resource 02 successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Resource 02 is display correctly [Actual Column]",
					"The Cost of Resource 02 should display correctly [Actual Column]",
					"The Cost of Resource 02 doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_materialCostActualCostingSummaryInfiniteThirdBatch)) {
			writeTestResults("Verify that the Material Total Cost is display correctly [Actual Column]",
					"The Material Total Cost should display correctly [Actual Column]",
					"The Material Total Cost successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Material Total Cost is display correctly [Actual Column]",
					"The Material Total Cost should display correctly [Actual Column]",
					"The Material Total Cost doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_material1CostActualCostingSummaryInfiniteThirdBatch)) {
			writeTestResults("Verify that the Cost of Material 01 is display correctly [Actual Column]",
					"The Cost of Material 01 should display correctly [Actual Column]",
					"The Cost of Material 01 successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Material 01 is display correctly [Actual Column]",
					"The Cost of Material 01 should display correctly [Actual Column]",
					"The Cost of Material 01 doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_material2CostActualCostingSummaryInfiniteThirdBatch)) {
			writeTestResults("Verify that the Cost of Material 02 is display correctly [Actual Column]",
					"The Cost of Material 02 should display correctly [Actual Column]",
					"The Cost of Material 02 successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Material 02 is display correctly [Actual Column]",
					"The Cost of Material 02 should display correctly [Actual Column]",
					"The Cost of Material 02 doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_material3CostActualCostingSummaryInfiniteThirdBatch)) {
			writeTestResults("Verify that the Cost of Material 03 is display correctly [Actual Column]",
					"The Cost of Material 03 should display correctly [Actual Column]",
					"The Cost of Material 03 successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Material 03 is display correctly [Actual Column]",
					"The Cost of Material 03 should display correctly [Actual Column]",
					"The Cost of Material 03 doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_overheadCostActualCostingSummaryInfiniteThirdBatch)) {
			writeTestResults("Verify that the Overhead Total Cost is display correctly [Actual Column]",
					"The Overhead Total Cost should display correctly [Actual Column]",
					"The Overhead Total Cost successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Overhead Total Cost is display correctly [Actual Column]",
					"The Overhead Total Cost should display correctly [Actual Column]",
					"The Overhead Total Cost doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_overhead1CostActualCostingSummaryInfiniteThirdBatch)) {
			writeTestResults("Verify that the Cost of Overhead 01 is display correctly [Actual Column]",
					"The Cost of Overhead 01 should display correctly [Actual Column]",
					"The Cost of Overhead 01 successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead 01 is display correctly [Actual Column]",
					"The Cost of Overhead 01 should display correctly [Actual Column]",
					"The Cost of Overhead 01 doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_overhead2CostActualCostingSummaryInfiniteThirdBatch)) {
			writeTestResults("Verify that the Cost of Overhead 02 is display correctly [Actual Column]",
					"The Cost of Overhead 02 should display correctly [Actual Column]",
					"The Cost of Overhead 02 successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead 02 is display correctly [Actual Column]",
					"The Cost of Overhead 02 should display correctly [Actual Column]",
					"The Cost of Overhead 02 doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_overhead3CostActualCostingSummaryInfiniteThirdBatch)) {
			writeTestResults("Verify that the Cost of Overhead 03 is display correctly [Actual Column]",
					"The Cost of Overhead 03 should display correctly [Actual Column]",
					"The Cost of Overhead 03 successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead 03 is display correctly [Actual Column]",
					"The Cost of Overhead 03 should display correctly [Actual Column]",
					"The Cost of Overhead 03 doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_totalCostActualCostingSummaryInfiniteThirdBatch)) {
			writeTestResults("Verify that the Total Cost is display correctly [Actual Column]",
					"The Total Cost should display correctly [Actual Column]",
					"The Total Cost successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Total Cost is display correctly [Actual Column]",
					"The Total Cost should display correctly [Actual Column]",
					"The Total Cost doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_unitCostActualCostingSummaryInfiniteThirdBatch)) {
			writeTestResults("Verify that the Unit Cost is display correctly [Actual Column]",
					"The Unit Cost should display correctly [Actual Column]",
					"The Unit Cost successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Unit Cost is display correctly [Actual Column]",
					"The Unit Cost should display correctly [Actual Column]",
					"The Unit Cost doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_unitCostResourseActualCostingSummaryInfiniteThirdBatch)) {
			writeTestResults("Verify that the Cost of Resource (Unit Cost) is display correctly [Actual Column]",
					"The Cost of Resource (Unit Cost) should display correctly [Actual Column]",
					"The Cost of Resource (Unit Cost) successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Resource (Unit Cost) is display correctly [Actual Column]",
					"The Cost of Resource (Unit Cost) should display correctly [Actual Column]",
					"The Cost of Resource (Unit Cost) doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_unitCostMateraialActualCostingSummaryInfiniteThirdBatch)) {
			writeTestResults("Verify that the Cost of Material (Unit Cost) is display correctly [Actual Column]",
					"The Cost of Material (Unit Cost) should display correctly [Actual Column]",
					"The Cost of Material (Unit Cost) successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Material (Unit Cost) is display correctly [Actual Column]",
					"The Cost of Material (Unit Cost) should display correctly [Actual Column]",
					"The Cost of Material (Unit Cost) doesn't display correctly [Actual Column]", "fail");
		}

		if (isDisplayed(td_unitCostOverheadActualCostingSummaryInfiniteThirdBatch)) {
			writeTestResults("Verify that the Cost of Overhead (Unit Cost) is display correctly [Actual Column]",
					"The Cost of Overhead (Unit Cost) should display correctly [Actual Column]",
					"The Cost of Overhead (Unit Cost) successfully display correctly [Actual Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead (Unit Cost) is display correctly [Actual Column]",
					"The Cost of Overhead (Unit Cost) should display correctly [Actual Column]",
					"The Cost of Overhead (Unit Cost) doesn't display correctly [Actual Column]", "fail");
		}

		/* Costing Summary - Base Amount */
		if (isDisplayed(column_headerBaseAmountCostingSummary)) {
			writeTestResults("Verify that the Base Amount Column is display same as earlier",
					"The Base Amount Column should display same as earlier",
					"The Base Amount Column successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that the Base Amount Column is display same as earlier",
					"The Base Amount Column should display same as earlier",
					"The Base Amount Column doesn't display same as earlier", "fail");
		}

		if (isDisplayed(td_resourceCostBaseAmountCostingSummaryInfiniteThirdBatch)) {
			writeTestResults("Verify that the Resource Total Cost is display correctly [Base Amount Column]",
					"The Resource Total Cost should display correctly [Base Amount Column]",
					"The Resource Total Cost successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Resource Total Cost is display correctly [Base Amount Column]",
					"The Resource Total Cost should display correctly [Base Amount Column]",
					"The Resource Total Cost doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_resourceCost1BaseAmountCostingSummaryInfiniteThirdBatch)) {
			writeTestResults("Verify that the Cost of Resource 01 is display correctly [Base Amount Column]",
					"The Cost of Resource 01 should display correctly [Base Amount Column]",
					"The Cost of Resource 01 successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Resource 01 is display correctly [Base Amount Column]",
					"The Cost of Resource 01 should display correctly [Base Amount Column]",
					"The Cost of Resource 01 doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_resourceCost2BaseAmountCostingSummaryInfiniteThirdBatch)) {
			writeTestResults("Verify that the Cost of Resource 02 is display correctly [Base Amount Column]",
					"The Cost of Resource 02 should display correctly [Base Amount Column]",
					"The Cost of Resource 02 successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Resource 02 is display correctly [Base Amount Column]",
					"The Cost of Resource 02 should display correctly [Base Amount Column]",
					"The Cost of Resource 02 doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_materialCostBaseAmountCostingSummaryInfiniteThirdBatch)) {
			writeTestResults("Verify that the Material Total Cost is display correctly [Base Amount Column]",
					"The Material Total Cost should display correctly [Base Amount Column]",
					"The Material Total Cost successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Material Total Cost is display correctly [Base Amount Column]",
					"The Material Total Cost should display correctly [Base Amount Column]",
					"The Material Total Cost doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_material1CostBaseAmountCostingSummaryInfiniteThirdBatch)) {
			writeTestResults("Verify that the Cost of Material 01 is display correctly [Base Amount Column]",
					"The Cost of Material 01 should display correctly [Base Amount Column]",
					"The Cost of Material 01 successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Material 01 is display correctly [Base Amount Column]",
					"The Cost of Material 01 should display correctly [Base Amount Column]",
					"The Cost of Material 01 doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_material2CostBaseAmountCostingSummaryInfiniteThirdBatch)) {
			writeTestResults("Verify that the Cost of Material 02 is display correctly [Base Amount Column]",
					"The Cost of Material 02 should display correctly [Base Amount Column]",
					"The Cost of Material 02 successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Material 02 is display correctly [Base Amount Column]",
					"The Cost of Material 02 should display correctly [Base Amount Column]",
					"The Cost of Material 02 doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_material3CostBaseAmountCostingSummaryInfiniteThirdBatch)) {
			writeTestResults("Verify that the Cost of Material 03 is display correctly [Base Amount Column]",
					"The Cost of Material 03 should display correctly [Base Amount Column]",
					"The Cost of Material 03 successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Material 03 is display correctly [Base Amount Column]",
					"The Cost of Material 03 should display correctly [Base Amount Column]",
					"The Cost of Material 03 doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_overheadCostBaseAmountCostingSummaryInfiniteThirdBatch)) {
			writeTestResults("Verify that the Overhead Total Cost is display correctly [Base Amount Column]",
					"The Overhead Total Cost should display correctly [Base Amount Column]",
					"The Overhead Total Cost successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Overhead Total Cost is display correctly [Base Amount Column]",
					"The Overhead Total Cost should display correctly [Base Amount Column]",
					"The Overhead Total Cost doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_overhead1CostBaseAmountCostingSummaryInfiniteThirdBatch)) {
			writeTestResults("Verify that the Cost of Overhead 01 is display correctly [Base Amount Column]",
					"The Cost of Overhead 01 should display correctly [Base Amount Column]",
					"The Cost of Overhead 01 successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead 01 is display correctly [Base Amount Column]",
					"The Cost of Overhead 01 should display correctly [Base Amount Column]",
					"The Cost of Overhead 01 doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_overhead2CostBaseAmountCostingSummaryInfiniteThirdBatch)) {
			writeTestResults("Verify that the Cost of Overhead 02 is display correctly [Base Amount Column]",
					"The Cost of Overhead 02 should display correctly [Base Amount Column]",
					"The Cost of Overhead 02 successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead 02 is display correctly [Base Amount Column]",
					"The Cost of Overhead 02 should display correctly [Base Amount Column]",
					"The Cost of Overhead 02 doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_overhead3CostBaseAmountCostingSummaryInfiniteThirdBatch)) {
			writeTestResults("Verify that the Cost of Overhead 03 is display correctly [Base Amount Column]",
					"The Cost of Overhead 03 should display correctly [Base Amount Column]",
					"The Cost of Overhead 03 successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead 03 is display correctly [Base Amount Column]",
					"The Cost of Overhead 03 should display correctly [Base Amount Column]",
					"The Cost of Overhead 03 doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_totalCostBaseAmountCostingSummaryInfiniteThirdBatch)) {
			writeTestResults("Verify that the Total Cost is display correctly [Base Amount Column]",
					"The Total Cost should display correctly [Base Amount Column]",
					"The Total Cost successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Total Cost is display correctly [Base Amount Column]",
					"The Total Cost should display correctly [Base Amount Column]",
					"The Total Cost doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_unitCostBaseAmountCostingSummaryInfiniteThirdBatch)) {
			writeTestResults("Verify that the Unit Cost is display correctly [Base Amount Column]",
					"The Unit Cost should display correctly [Base Amount Column]",
					"The Unit Cost successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Unit Cost is display correctly [Base Amount Column]",
					"The Unit Cost should display correctly [Base Amount Column]",
					"The Unit Cost doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_unitCostResourseBaseAmountCostingSummaryInfiniteThirdBatch)) {
			writeTestResults("Verify that the Cost of Resource (Unit Cost) is display correctly [Base Amount Column]",
					"The Cost of Resource (Unit Cost) should display correctly [Base Amount Column]",
					"The Cost of Resource (Unit Cost) successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Resource (Unit Cost) is display correctly [Base Amount Column]",
					"The Cost of Resource (Unit Cost) should display correctly [Base Amount Column]",
					"The Cost of Resource (Unit Cost) doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_unitCostMateraialBaseAmountCostingSummaryInfiniteThirdBatch)) {
			writeTestResults("Verify that the Cost of Material (Unit Cost) is display correctly [Base Amount Column]",
					"The Cost of Material (Unit Cost) should display correctly [Base Amount Column]",
					"The Cost of Material (Unit Cost) successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Material (Unit Cost) is display correctly [Base Amount Column]",
					"The Cost of Material (Unit Cost) should display correctly [Base Amount Column]",
					"The Cost of Material (Unit Cost) doesn't display correctly [Base Amount Column]", "fail");
		}

		if (isDisplayed(td_unitCostOverheadBaseAmountCostingSummaryInfiniteThirdBatch)) {
			writeTestResults("Verify that the Cost of Overhead (Unit Cost) is display correctly [Base Amount Column]",
					"The Cost of Overhead (Unit Cost) should display correctly [Base Amount Column]",
					"The Cost of Overhead (Unit Cost) successfully display correctly [Base Amount Column]", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead (Unit Cost) is display correctly [Base Amount Column]",
					"The Cost of Overhead (Unit Cost) should display correctly [Base Amount Column]",
					"The Cost of Overhead (Unit Cost) doesn't display correctly [Base Amount Column]", "fail");
		}
	}

	/* Generate Internal Receipt PROD_E2E_002 */
	public void internalReciptAndJournelEntryThirdBatch_PROD_E2E_002() throws Exception {
		explicitWaitUntillClickable(btn_action2, 50);
		click(btn_action2);

		explicitWaitUntillClickable(btn_convertToInternalReceipt, 10);
		if (isDisplayed(btn_convertToInternalReceipt)) {
			writeTestResults("Verify that Convert to Internel Recipt option is available under Actions",
					"Convert to Internel Recipt option should be available under Actions",
					"Convert to Internel Recipt option successfully available under Actions", "pass");
		} else {
			writeTestResults("Verify that Convert to Internel Recipt option is available under Actions",
					"Convert to Internel Recipt option should be available under Actions",
					"Convert to Internel Recipt option doesn't available under Actions", "fail");
		}
		click(btn_convertToInternalReceipt);

		explicitWait(header_genarateInternalReciptSmoke, 20);
		if (isDisplayed(header_genarateInternalReciptSmoke)) {
			writeTestResults("Verify that genarate internel receipt window is open",
					"genarate internel receipt window should be open",
					"genarate internel receipt window succeessfully open", "pass");
		} else {
			writeTestResults("Verify that genarate internel receipt window is open",
					"genarate internel receipt window should be open", "genarate internel receipt window doesn't open",
					"fail");
		}

		click(chk_seletcOroductForInternalReciptThirdBatch);

		if (isSelected(chk_seletcOroductForInternalReciptThirdBatch)) {
			writeTestResults("Verify user able to select the product line",
					"User should be able to select the product line", "User successfully select the product line",
					"pass");
		} else {
			writeTestResults("Verify user able to select the product line",
					"User should be able to select the product line", "User doesn't select the product line", "fail");
		}

		String enteredReceiptDate = getAttribute(txt_reciptDateGenarateInternalREceiptWindow, "value");
		click(btn_genarateInternalRecipt);
		explicitWait(para_validationInternalREciptGenarateSuccessfull, 10);
		if (isDisplayed(para_validationInternalREciptGenarateSuccessfull)) {
			writeTestResults("Verify that succesfull validation is appear for Internal Receipt Genaration",
					"Succesfull validation should appear for Internal Receipt Genaration",
					"Succesfull validation is appear for Internal Receipt Genaration", "pass");
		} else {
			writeTestResults("Verify that succesfull validation is appear for Internal Receipt Genaration",
					"Succesfull validation should appear for Internal Receipt Genaration",
					"Succesfull validation doesn't appear for Internal Receipt Genaration", "fail");
		}

		waitUntillNotVisible(para_validationInternalREciptGenarateSuccessfull, 10);
		Thread.sleep(3500);
		explicitWaitUntillClickable(lnk_genaratedInternalREciptSmokeThirdBatch, 15);
		Thread.sleep(3000);
		click(lnk_genaratedInternalREciptSmokeThirdBatch);
		Thread.sleep(2000);
		switchWindow();
		Thread.sleep(1000);
		explicitWait(header_releasedInternalRecipt, 10);
		trackCode = getText(lbl_docNumber);
		if (isDisplayed(header_releasedInternalRecipt)) {
			writeTestResults("Verify that Internal Receiptis open as released status",
					"Internal Receipt should be open as released status",
					"Internal Receipt successfully open as released status", "pass");
		} else {
			writeTestResults("Verify that Internal Receiptis open as released status",
					"Internal Receipt should be open as released status",
					"Internal Receipt doesn't open or not in released status", "fail");
		}

		String docDate = getText(a_documentDate);
		String postDate = getText(a_postDate);
		String dueDate = getText(a_dueDate);

		if (docDate.equals(enteredReceiptDate) && docDate.equals(postDate) && docDate.equals(dueDate)) {
			writeTestResults(
					"Verify that all three header dates are equal to the date that entered when generating Internal Receipt",
					"All three header dates should be equal to the date that entered when generating Internal Receipt",
					"All three header dates are equal to the date that entered when generating Internal Receipt",
					"pass");
		} else {
			writeTestResults(
					"Verify that all three header dates are equal to the date that entered when generating Internal Receipt",
					"All three header dates should be equal to the date that entered when generating Internal Receipt",
					"All three header dates are not equal to the date that entered when generating Internal Receipt",
					"fail");
		}

		String warehouseOnInternalReceipt = getText(div_warehouseInternalRecipt);
		if (warehouseOnInternalReceipt.equals(outputWarehouseLI)) {
			writeTestResults(
					"Verify that Output Warehouse of Production Order and Warehouse of Internal Receipt is same",
					"Output Warehouse of Production Order and Warehouse of Internal Receipt should be same",
					"Output Warehouse of Production Order and Warehouse of Internal Receipt is same", "pass");
		} else {
			writeTestResults(
					"Verify that Output Warehouse of Production Order and Warehouse of Internal Receipt is same",
					"Output Warehouse of Production Order and Warehouse of Internal Receipt should be same",
					"Output Warehouse of Production Order and Warehouse of Internal Receipt is not same", "fail");
		}

		String qtyInternalReceipt = getText(div_planedQtyInternalRecipt);
		if (qtyInternalReceipt.equals("70.00")) {
			writeTestResults("Verify that quantity is equel to the QC pass quantity",
					"Quantity should be equel to the QC pass quantity",
					"Quantity successfully equel to the QC pass quantity", "pass");
		} else {
			writeTestResults("Verify that quantity is equel to the QC pass quantity",
					"Quantity should be equel to the QC pass quantity",
					"Quantity successfully not equel to the QC pass quantity", "fail");
		}

		click(btn_action2);

		explicitWaitUntillClickable(btn_journal, 10);
		if (isDisplayed(btn_journal)) {
			writeTestResults("Verify that Journal Entry option is available under Actions",
					"Journal Entry option should be available under Actions",
					"Journal Entry option successfully available under Actions", "pass");
		} else {
			writeTestResults("Verify that Journal Entry option is available under Actions",
					"Journal Entry option should be available under Actions",
					"Journal Entry option doesn't available under Actions", "fail");
		}
		click(btn_journal);

		explicitWait(header_journalEntryPopup, 10);
		if (isDisplayed(header_journalEntryPopup)) {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded",
					"Journal Entry details pop up successfully get loaded", "pass");
		} else {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded", "Journal Entry details pop up doesn't get loaded",
					"fail");
		}

		if (isDisplayedQuickCheck(lbl_debitValueJournelEntry)) {
			/* QA */
			String debitAmount = getText(lbl_debitValueJournelEntry);

			if (debitAmount.startsWith("1410.51")) {
				writeTestResults("Verify that amount was debited to the relevent GL Account",
						"Amount should be debited to the relevent GL Account",
						"Amount successfully debited to the relevent GL Account", "pass");
			} else {
				writeTestResults("Verify that amount was debited to the relevent GL Account",
						"Amount should be debited to the relevent GL Account",
						"Amount doesn't debited to the relevent GL Account", "fail");
			}

			String creditAmount = getText(lbl_creditValueJournelEntry);
			if (creditAmount.startsWith("1410.51")) {
				writeTestResults("Verify that amount was credited to the relevent GL Account",
						"Amount should be credited to the relevent GL Account",
						"Amount successfully credited to the relevent GL Account", "pass");
			} else {
				writeTestResults("Verify that amount was credited to the relevent GL Account",
						"Amount should be credited to the relevent GL Account",
						"Amount doesn't credited to the relevent GL Account", "fail");
			}

			String debitTotal = getText(lbl_debitTotalJournelEntrySingleEntry);
			String credTotal = getText(lbl_creditTotalJournelEntrySingleEntry);

			if (debitTotal.startsWith("1410.51") && debitTotal.equals(credTotal)) {
				writeTestResults("Verify total debit and credit amounts were balanced",
						"Total debit and credit amounts should balanced",
						"Total debit and credit amounts successfully balanced", "pass");
			} else {
				writeTestResults("Verify total debit and credit amounts were balanced",
						"Total debit and credit amounts should balanced",
						"Total debit and credit amounts weren't balanced", "fail");
			}

		} else {
			/* Staging */
			String debitAmount = getText(lbl_debitValueJournelEntry);

			if (debitAmount.startsWith("1410.51")) {
				writeTestResults("Verify that amount was debited to the relevent GL Account",
						"Amount should be debited to the relevent GL Account",
						"Amount successfully debited to the relevent GL Account", "pass");
			} else {
				writeTestResults("Verify that amount was debited to the relevent GL Account",
						"Amount should be debited to the relevent GL Account",
						"Amount doesn't debited to the relevent GL Account", "fail");
			}

			String creditAmount = getText(lbl_creditValueJournelEntry);
			if (creditAmount.startsWith("1410.51")) {
				writeTestResults("Verify that amount was credited to the relevent GL Account",
						"Amount should be credited to the relevent GL Account",
						"Amount successfully credited to the relevent GL Account", "pass");
			} else {
				writeTestResults("Verify that amount was credited to the relevent GL Account",
						"Amount should be credited to the relevent GL Account",
						"Amount doesn't credited to the relevent GL Account", "fail");
			}

			String debitTotal = getText(lbl_debitTotalJournelEntrySingleEntry);
			String credTotal = getText(lbl_creditTotalJournelEntrySingleEntry);

			if (debitTotal.startsWith("1410.51") && debitTotal.equals(credTotal)) {
				writeTestResults("Verify total debit and credit amounts were balanced",
						"Total debit and credit amounts should balanced",
						"Total debit and credit amounts successfully balanced", "pass");
			} else {
				writeTestResults("Verify total debit and credit amounts were balanced",
						"Total debit and credit amounts should balanced",
						"Total debit and credit amounts weren't balanced", "fail");
			}
		}

		openPage(readProductionData("ReleaseedPO_PRD_E2E_002"));
		explicitWaitUntillClickable(btn_action2, 50);
		click(btn_action2);

		explicitWaitUntillClickable(btn_shopFloorUpdate2, 10);
		if (isDisplayed(btn_shopFloorUpdate2)) {
			writeTestResults("Verify that Shop floor update option is available under Actions",
					"Shop floor update option should be available under Actions",
					"Shop floor update option successfully available under Actions", "pass");
		} else {
			writeTestResults("Verify that Shop floor update option is available under Actions",
					"Shop floor update option should be available under Actions",
					"Shop floor update option doesn't available under Actions", "fail");
		}
		click(btn_shopFloorUpdate2);

		explicitWait(header_shopFloorUpdateWindow, 20);
		if (isDisplayed(header_shopFloorUpdateWindow)) {
			writeTestResults("Verify that shop floor update window is open", "Shop floor update window should be open",
					"Shop floor update window succeessfully open", "pass");
		} else {
			writeTestResults("Verify that shop floor update window is open", "Shop floor update window should be open",
					"Shop floor update window doesn't open", "fail");
		}

		Select se = new Select(driver.findElement(getLocator(drop_batchNoShopFloor)));
		List<WebElement> countOfOptions = se.getOptions();
		int countOfBatched = (countOfOptions.size()) - 1;
		if (countOfBatched == 3) {
			writeTestResults("Verify that all three batches are available",
					"All three batched should be availabe on dropdown",
					"All genarated three batches successfully available", "pass");
		} else {
			writeTestResults("Verify that all three batches are available",
					"All three batched should be availabe on dropdown", "All genarated three batches not available",
					"fail");
		}
	}

	/* PROD_E2E_003 */
	public void billOfMaterial_PROD_E2E_003() throws Exception {
		System.out.println(getCurrentUrl());
		explicitWaitUntillClickable(img_navigationPane, 60);
		Thread.sleep(2000);
		explicitWaitUntillClickable(img_navigationPane, 20);
		if (click(img_navigationPane)) {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User successfully click navigation pane", "pass");
		} else {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User couldn't click navigation pane", "fail");
		}

		explicitWaitUntillClickable(btn_inventory, 10);
		click(btn_inventory);

		explicitWaitUntillClickable(btn_billOfMaterial2, 20);
		click(btn_billOfMaterial2);

		explicitWaitUntillClickable(btn_newBillOfMaterial, 10);
		click(btn_newBillOfMaterial);

		explicitWaitUntillClickable(txt_descriptionBillOfMaterials, 60);
		sendKeys(txt_descriptionBillOfMaterials, descriptionBOM);

		selectText(drop_productGroupBOM, productionGroupE2E);

		Thread.sleep(2000);
		String selectedProductGroup = getSelectedOptionInDropdown(drop_productGroupBOM);
		if (selectedProductGroup.contains(productionGroupE2E)) {
			writeTestResults("Verify that user able to select Product Group",
					"User should be able to select Product Group", "User successfully select Product Group", "pass");
		} else {
			writeTestResults("Verify that user able to select Product Group",
					"User should be able to select Product Group", "User doesn't select Product Group", "fail");

		}

		explicitWaitUntillClickable(btn_lookupOutputProduct, 20);
		click(btn_lookupOutputProduct);
		explicitWaitUntillClickable(txt_productSearch, 40);
		Thread.sleep(1000);
		sendKeysLookup(txt_productSearch, invObj.readTestCreation("BatchFifoMultiPlatformProductionType"));
		pressEnter(txt_productSearch);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")), 20);

		if (isDisplayedQuickCheck(lnk_inforOnLookupInformationReplace.replace("info",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")))) {
			doubleClick(lnk_inforOnLookupInformationReplace.replace("info",
					invObj.readTestCreation("BatchFifoMultiPlatformProductionType")));

			Thread.sleep(1500);

			String selectedProduct = getAttribute(txt_productFrontPageBOM, "value");
			if (selectedProduct.contains(invObj.readTestCreation("BatchFifoMultiPlatformProductionType"))) {
				writeTestResults("Verify that user able to select Production Type Product",
						"User should be able to select Production Type Product",
						"User successfully select Production Type Product", "pass");
			} else {
				writeTestResults("Verify that user able to select Production Type Product",
						"User should be able to select Production Type Product",
						"User doesn't select Production Type Product", "fail");

			}
		} else {
			Thread.sleep(1500);
			((JavascriptExecutor) driver).executeScript("window.open('about:blank','_blank');");
			Thread.sleep(1000);
			switchWindow();
			openPage(siteUrlMultiPlatformm);
			createProducttionTypeProduct();
			closeWindow();
			sendKeysLookup(txt_productSearch, invObj.readTestCreation("BatchFifoMultiPlatformProductionType"));
			pressEnter(txt_productSearch);
			Thread.sleep(2000);
			explicitWait(lnk_inforOnLookupInformationReplace.replace("info",
					invObj.readTestCreation("BatchFifoMultiPlatformProductionType")), 20);
			doubleClick(lnk_inforOnLookupInformationReplace.replace("info",
					invObj.readTestCreation("BatchFifoMultiPlatformProductionType")));

			Thread.sleep(1500);

			String selectedProduct = getAttribute(txt_productFrontPageBOM, "value");
			if (selectedProduct.contains(invObj.readTestCreation("BatchFifoMultiPlatformProductionType"))) {
				writeTestResults("Verify that user able to select Production Type Product",
						"User should be able to select Production Type Product",
						"User successfully select Production Type Product", "pass");
			} else {
				writeTestResults("Verify that user able to select Production Type Product",
						"User should be able to select Production Type Product",
						"User doesn't select Production Type Product", "fail");

			}
		}
		Thread.sleep(1500);
		sendKeys(txt_quantityBOM, hundred);
		String enterdQty = getAttribute(txt_quantityBOM, "value");
		if (enterdQty.equals(hundred)) {
			writeTestResults("Verify that user able to enter the Quantity", "User should be able to enter the Quantity",
					"User successfully enter the Quantity", "pass");
		} else {
			writeTestResults("Verify that user able to enter the Quantity", "User should be able to enter the Quantity",
					"User doesn't enter the Quantity", "fail");

		}

		if (isDisplayed(header_comlumQtyBoMMaterialInfoTable)
				&& isDisplayed(header_comlumFixedQtyBoMMaterialInfoTable)) {
			writeTestResults("Verify that relevant columns on material information grid is display same as earlier",
					"Relevant columns on material information grid should display same as earlier",
					"Relevant columns on the material information grid are display the same as earlier", "pass");
		} else {
			writeTestResults("Verify that relevant columns on material information grid is display same as earlier",
					"Relevant columns on material information grid should display same as earlier",
					"Relevant columns on the material information grid are not display the same as earlier", "fail");

			pageScrollDown();
		}

		mouseMove(header_scrapFixedQtyBoMMaterialInfoTable);
		if (isDisplayed(header_scrapFixedQtyBoMMaterialInfoTable)) {
			writeTestResults("Verify that Scrap % columns on material information grid is display same as earlier",
					"Scrap % columns on material information grid should display same as earlier",
					"Scrap % columns on the material information grid are display the same as earlier", "pass");
		} else {
			writeTestResults("Verify that Scrap % columns on material information grid is display same as earlier",
					"Scrap % columns on material information grid should display same as earlier",
					"Scrap % columns on the material information grid are not display the same as earlier", "fail");

		}

		mouseMove(btn_lookupOnLineBOMRowReplace.replace("row", "1"));
		click(btn_lookupOnLineBOMRowReplace.replace("row", "1"));
		sendKeysLookup(txt_productSearch, invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialOne"));
		pressEnter(txt_productSearch);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info",
				invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialOne")), 15);
		if (isDisplayedQuickCheck(lnk_inforOnLookupInformationReplace.replace("info",
				invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialOne")))) {
			doubleClick(lnk_inforOnLookupInformationReplace.replace("info",
					invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialOne")));
		} else {
			Thread.sleep(1500);
			((JavascriptExecutor) driver).executeScript("window.open('about:blank','_blank');");
			Thread.sleep(1000);
			switchWindow();
			openPage(siteUrlMultiPlatformm);
			createAllowDecimalRowMaterial1();
			createAllowDecimalRowMaterial2();
			createAllowDecimalRowMaterialHasVarientProduct3();
			closeWindow();
			sendKeysLookup(txt_productSearch, invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialOne"));
			pressEnter(txt_productSearch);
			Thread.sleep(2000);
			explicitWait(lnk_inforOnLookupInformationReplace.replace("info",
					invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialOne")), 15);

			doubleClick(lnk_inforOnLookupInformationReplace.replace("info",
					invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialOne")));
		}
		explicitWait(txt_qtyBOMInputProductLinesRowReplace.replace("row", "1"), 10);
		sendKeys(txt_qtyBOMInputProductLinesRowReplace.replace("row", "1"), rowMaterialQtyOne);
		String enteredRowMaterialOneQty = getAttribute(txt_qtyBOMInputProductLinesRowReplace.replace("row", "1"),
				"value");
		if (enteredRowMaterialOneQty.equals(rowMaterialQtyOne)) {
			writeTestResults("Verify that user able to enter the raw material one Qty",
					"User should be able to enter the raw material one Qty",
					"User successfully enter the raw material one Qty", "pass");
		} else {
			writeTestResults("Verify that user able to enter the raw material one Qty",
					"User should be able to enter the raw material one Qty",
					"User doesn't enter the raw material one Qty", "fail");

		}
		click(btn_addNewRecordBoM);

		click(btn_lookupOnLineBOMRowReplace.replace("row", "2"));
		sendKeysLookup(txt_productSearch, invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialTwo"));
		pressEnter(txt_productSearch);
		Thread.sleep(2000);
		explicitWait(lnk_inforOnLookupInformationReplace.replace("info",
				invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialTwo")), 20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info",
				invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialTwo")));

		explicitWait(txt_qtyBOMInputProductLinesRowReplace.replace("row", "2"), 10);
		sendKeys(txt_qtyBOMInputProductLinesRowReplace.replace("row", "2"), rowMaterialQtyTwo);
		String enteredRowMaterialTwoQty = getAttribute(txt_qtyBOMInputProductLinesRowReplace.replace("row", "2"),
				"value");
		if (enteredRowMaterialTwoQty.equals(rowMaterialQtyTwo)) {
			writeTestResults("Verify that user able to enter the raw material two Qty",
					"User should be able to enter the raw material two Qty",
					"User successfully enter the raw material two Qty", "pass");
		} else {
			writeTestResults("Verify that user able to enter the raw material two Qty",
					"User should be able to enter the raw material two Qty",
					"User doesn't enter the raw material two Qty", "fail");

		}
		click(chk_fixedQtyRowReplaceBOM.replace("row", "2"));
		if (isSelected(chk_fixedQtyRowReplaceBOM.replace("row", "2"))) {
			writeTestResults("Verify that user able to enable Fixed Qty", "User should be able to enable Fixed Qty",
					"User successfully enable Fixed Qty", "pass");
		} else {
			writeTestResults("Verify that user able to enable Fixed Qty", "User should be able to enable Fixed Qty",
					"User doesn't enable Fixed Qty", "fail");

		}

		click(btn_addNewRecordBoM);

		click(btn_lookupOnLineBOMRowReplace.replace("row", "3"));
		sendKeysLookup(txt_productSearch,
				invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialHasVarientProductThree"));
		pressEnter(txt_productSearch);
		Thread.sleep(2000);
		explicitWait(
				lnk_inforOnLookupInformationReplace.replace("info",
						invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialHasVarientProductThree")),
				20);
		doubleClick(lnk_inforOnLookupInformationReplace.replace("info",
				invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialHasVarientProductThree")));

		explicitWait(txt_qtyBOMInputProductLinesRowReplace.replace("row", "3"), 10);
		sendKeys(txt_qtyBOMInputProductLinesRowReplace.replace("row", "3"), rowMaterialQtyThree);
		String enteredRowMaterialThreeQty = getAttribute(txt_qtyBOMInputProductLinesRowReplace.replace("row", "3"),
				"value");
		if (enteredRowMaterialThreeQty.equals(rowMaterialQtyThree)) {
			writeTestResults("Verify that user able to enter the raw material three Qty",
					"User should be able to enter the raw material three Qty",
					"User successfully enter the raw material three Qty", "pass");
		} else {
			writeTestResults("Verify that user able to enter the raw material three Qty",
					"User should be able to enter the raw material three Qty",
					"User doesn't enter the raw material three Qty", "fail");

		}

		sendKeys(txt_scrapRowReplaceBOM.replace("row", "3"), scrapRowMaterilThree);
		String enteredRowMaterialThreeScrap = getAttribute(txt_scrapRowReplaceBOM.replace("row", "3"), "value");
		if (enteredRowMaterialThreeScrap.equals(scrapRowMaterilThree)) {
			writeTestResults("Verify that user able to enter the raw material three Scrap",
					"User should be able to enter the raw material three Scrap",
					"User successfully enter the raw material three Scrap", "pass");
		} else {
			writeTestResults("Verify that user able to enter the raw material three Scrap",
					"User should be able to enter the raw material three Scrap",
					"User doesn't enter the raw material three Scrap", "fail");

		}

		click(btn_draft);
		explicitWait(header_draftedBOM, 50);
		if (isDisplayed(header_draftedBOM)) {
			writeTestResults("Verify user able to draft the Bill Of Material",
					"User should be able to draft the Bill Of Material", "User successfully draft the Bill Of Material",
					"pass");
		} else {
			writeTestResults("Verify user able to draft the Bill Of Material",
					"User should be able to draft the Bill Of Material", "User doesn't draft the Bill Of Material",
					"fail");

		}

		click(btn_release);
		explicitWait(header_releasedBillOfMaterial, 50);
		trackCode = getText(lbl_docNo);
		writeProductionData("BOM_PROD_E2E_003", trackCode, 28);
		if (isDisplayed(header_releasedBillOfMaterial)) {
			writeTestResults("Verify user able to release the Bill Of Material",
					"User should be able to release the Bill Of Material",
					"User successfully release the Bill Of Material", "pass");
		} else {
			writeTestResults("Verify user able to release the Bill Of Material",
					"User should be able to release the Bill Of Material", "User doesn't release the Bill Of Material",
					"fail");

		}
	}

	public void billOfOperation_PROD_E2E_003() throws Exception {
		pageRefersh();
		explicitWait(img_navigationPane, 20);
		Thread.sleep(2000);
		explicitWait(img_navigationPane, 20);
		click(img_navigationPane);
		explicitWait(btn_productionModule, 10);
		click(btn_productionModule);
		explicitWait(btn_billOfOperation, 10);
		click(btn_billOfOperation);
		explicitWait(header_BillOfOperationByPage, 40);
		if (isDisplayed(header_BillOfOperationByPage)) {
			writeTestResults("Verify user can navigate to the Bill Of Operation by-page",
					"User should be able to navigate to Bill Of Operation by-page",
					"User successfully navigate to Bill Of Operation by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to the Bill Of Operation by-page",
					"User should be able to navigate to Bill Of Operation by-page",
					"User doesn't navigate to Bill Of Operation by-page", "fail");

		}
		click(btn_newBillOfOperation);
		explicitWait(header_newBillOfOperation, 50);
		if (isDisplayed(header_newBillOfOperation)) {
			writeTestResults("Verify user can navigate to the new Bill Of Operation",
					"User should be able to navigate to new Bill Of Operation",
					"User successfully navigate to new Bill Of Operation", "pass");
		} else {
			writeTestResults("Verify user can navigate to the new Bill Of Operation",
					"User should be able to navigate to new Bill Of Operation",
					"User doesn't navigate to new Bill Of Operation", "fail");
		}

		String bookCodeBOO = invObj.currentTimeAndDate().replace("/", "").replace(":", "");
		sendKeys(txt_booCodeBillOfOperation, bookCodeBOO);

		click(btn_addBillOfOperationRowReplace.replace("row", "1"));

		explicitWait(drop_elementCategoryBOO, 20);
		selectText(drop_elementCategoryBOO, elementCategorySupply);
		Thread.sleep(1000);
		String initialBOOElementCategory = getSelectedOptionInDropdown(drop_elementCategoryBOO);
		if (initialBOOElementCategory.equals(elementCategorySupply)) {
			writeTestResults("Verify user able to select 'Supply' as Element Category for the initial BOO",
					"User should be able to select 'Supply' as Element Category for the initial BOO",
					"User successfully select 'Supply' as Element Category for the initial BOO", "pass");
		} else {
			writeTestResults("Verify user able to select 'Supply' as Element Category for the initial BOO",
					"User should be able to select 'Supply' as Element Category for the initial BOO",
					"User doesn't select 'Supply' as Element Category for the initial BOO", "fail");
		}

		selectText(drop_activityTypeBOO, activityTypeMoveStock);
		Thread.sleep(1000);
		String initialBOOActivityType = getSelectedOptionInDropdown(drop_activityTypeBOO);
		if (initialBOOActivityType.equals(activityTypeMoveStock)) {
			writeTestResults("Verify user able to select 'Move Stock' as Activity Type for the initial BOO",
					"User should be able to select 'Move Stock' as Activity Type for the initial BOO",
					"User successfully select 'Move Stock' as Activity Type for the initial BOO", "pass");
		} else {
			writeTestResults("Verify user able to select 'Move Stock' as Activity Type for the initial BOO",
					"User should be able to select 'Move Stock' as Activity Type for the initial BOO",
					"User doesn't select 'Move Stock' as Activity Type for the initial BOO", "fail");
		}

		sendKeys(txt_elementDescriptionBOO, moveStockElementDescription);
		Thread.sleep(1000);
		String initialBOOEnteredDesc = getAttribute(txt_elementDescriptionBOO, "value");
		if (initialBOOEnteredDesc.equals(moveStockElementDescription)) {
			writeTestResults("Verify user able to enter Element Description for the initial BOO",
					"User should be able to enter Element Description for the initial BOO",
					"User successfully enter Element Description for the initial BOO", "pass");
		} else {
			writeTestResults("Verify user able to enter Element Description for the initial BOO",
					"User should be able to enter Element Description for the initial BOO",
					"User doesn't enter Element Description for the initial BOO", "fail");
		}

		click(btn_applyOperationOneBOO);

		explicitWait(btn_addBillOfOperationRowReplace.replace("row", "2"), 10);
		click(btn_addBillOfOperationRowReplace.replace("row", "2"));

		explicitWait(drop_elementCategoryBOO, 20);
		selectText(drop_elementCategoryBOO, elementCategoryOperation);
		Thread.sleep(1000);
		String firstBOOElementCategory = getSelectedOptionInDropdown(drop_elementCategoryBOO);
		if (firstBOOElementCategory.equals(elementCategoryOperation)) {
			writeTestResults("Verify user able to select 'Operation' as Element Category for the first BOO",
					"User should be able to select 'Operation' as Element Category for the first BOO",
					"User successfully select 'Operation' as Element Category for the first BOO", "pass");
		} else {
			writeTestResults("Verify user able to select 'Operation' as Element Category for the first BOO",
					"User should be able to select 'Operation' as Element Category for the first BOO",
					"User doesn't select 'Operation' as Element Category for the first BOO", "fail");
		}

		Thread.sleep(1000);
		String firstBOOActivityType = getSelectedOptionInDropdown(drop_activityTypeBOO);
		if (firstBOOActivityType.equals(activityTypeProduce)) {
			writeTestResults("Verify auto select 'Produce' as Activity Type for the first BOO",
					"Should auto select 'Produce' as Activity Type for the first BOO",
					"Auto select 'Produce' as Activity Type for the first BOO", "pass");
		} else {
			writeTestResults("Verify auto select 'Produce' as Activity Type for the first BOO",
					"Should auto select 'Produce' as Activity Type for the first BOO",
					"Not auto select 'Produce' as Activity Type for the first BOO", "fail");
		}

		sendKeys(txt_elementDescriptionBOO, produceElementDescription1);
		Thread.sleep(1000);
		String firstBOOEnteredDesc = getAttribute(txt_elementDescriptionBOO, "value");
		if (firstBOOEnteredDesc.equals(produceElementDescription1)) {
			writeTestResults("Verify user able to enter Element Description for the first BOO",
					"User should be able to enter Element Description for the first BOO",
					"User successfully enter Element Description for the first BOO", "pass");
		} else {
			writeTestResults("Verify user able to enter Element Description for the first BOO",
					"User should be able to enter Element Description for the first BOO",
					"User doesn't enter Element Description for the first BOO", "fail");
		}

		sendKeys(txt_groupIdBOO, groupIdBOOOne);
		Thread.sleep(1000);
		String firstBOOEnteredGroupID = getAttribute(txt_groupIdBOO, "value");
		if (firstBOOEnteredGroupID.equals(groupIdBOOOne)) {
			writeTestResults("Verify user able to enter Group ID for the first BOO",
					"User should be able to enter Group ID for the first BOO",
					"User successfully enter Group ID for the first BOO", "pass");
		} else {
			writeTestResults("Verify user able to enter Group ID for the first BOO",
					"User should be able to enter Group ID for the first BOO",
					"User doesn't enter Group ID for the first BOO", "fail");
		}

		click(btn_mainResourseLookuoOperationOne);
		explicitWaitUntillClickable(txt_searchResource, 30);
		Thread.sleep(2000);
		sendKeysLookup(txt_searchResource, readProductionData("ResourceCode01_E2E"));
		pressEnter(txt_searchResource);
		explicitWaitUntillClickable(
				td_resultResourseOperationOne.replace("resource", readProductionData("ResourceCode01_E2E")), 40);
		doubleClick(td_resultResourseOperationOne.replace("resource", readProductionData("ResourceCode01_E2E")));
		Thread.sleep(2000);
		String selectedMainResourse = getAttribute(txt_mainResourseOnLookupOperationOne, "value");
		if (selectedMainResourse.contains(readProductionData("ResourceCode01_E2E"))) {
			writeTestResults("Verify user able to select Main Resource for the first BOO",
					"User should be able to select Main Resource for the first BOO",
					"User successfully select Main Resource for the first BOO", "pass");
		} else {
			writeTestResults("Verify user able to select Main Resource for the first BOO",
					"User should be able to select Main Resource for the first BOO",
					"User doesn't select Main Resource for the first BOO", "fail");
		}

		click(txt_fixedDurationOpeartionOne);
		explicitWaitUntillClickable(a_oneHourFixedDurationOpeationOne, 20);
		click(a_oneHourFixedDurationOpeationOne);
		Thread.sleep(700);
		click(btn_okFixedDurationOpeationOne);
		Thread.sleep(2000);
		String selecteFixedDurationHours = getAttribute(txt_fixedDurationOpeartionOne, "value");
		String selecteFixedDurationMins = getAttribute(txt_fixedDurationMinOpeartionOne, "value");
		if (selecteFixedDurationHours.equals("01") && selecteFixedDurationMins.equals("00")) {
			writeTestResults("Verify user able to select Fixed Duration as 1 for the first BOO",
					"User should be able to select Fixed Duration as 1 for the first BOO",
					"User successfully select Fixed Duration as 1 for the first BOO", "pass");
		} else {
			writeTestResults("Verify user able to select Fixed Duration as 1 for the first BOO",
					"User should be able to select Fixed Duration as 1 for the first BOO",
					"User doesn't select Fixed Duration as 1 for the first BOO", "fail");
		}

		Thread.sleep(1000);
		String firstBOOBatchMethod = getSelectedOptionInDropdown(drop_batchMethodBOO);
		if (firstBOOBatchMethod.equals(batchMethodTransferBOO)) {
			writeTestResults("Verify auto select 'Transfer' as Batch Method for the first BOO",
					"Should auto select 'Transfer' as Batch Method for the first BOO",
					"Auto select 'Transfer' as Batch Method for the first BOO", "pass");
		} else {
			writeTestResults("Verify auto select 'Transfer' as Batch Method for the first BOO",
					"Should auto select 'Transfer' as Batch Method for the first BOO",
					"Not auto select 'Transfer' as Batch Method for the first BOO", "fail");
		}

		click(btn_applyOperationOneBOO);

		explicitWait(btn_addBillOfOperationRowReplace.replace("row", "2"), 10);
		click(btn_addBillOfOperationRowReplace.replace("row", "2"));

		explicitWait(drop_elementCategoryBOO, 20);
		selectText(drop_elementCategoryBOO, elementCategoryOperation);
		Thread.sleep(1000);
		String secondBOOElementCategory = getSelectedOptionInDropdown(drop_elementCategoryBOO);
		if (secondBOOElementCategory.equals(elementCategoryOperation)) {
			writeTestResults("Verify user able to select 'Operation' as Element Category for the second BOO",
					"User should be able to select 'Operation' as Element Category for the second BOO",
					"User successfully select 'Operation' as Element Category for the second BOO", "pass");
		} else {
			writeTestResults("Verify user able to select 'Operation' as Element Category for the second BOO",
					"User should be able to select 'Operation' as Element Category for the second BOO",
					"User doesn't select 'Operation' as Element Category for the second BOO", "fail");
		}

		Thread.sleep(1000);
		String secondBOOActivityType = getSelectedOptionInDropdown(drop_activityTypeBOO);
		if (secondBOOActivityType.equals(activityTypeProduce)) {
			writeTestResults("Verify auto select 'Produce' as Activity Type for the second BOO",
					"Should auto select 'Produce' as Activity Type for the second BOO",
					"Auto select 'Produce' as Activity Type for the second BOO", "pass");
		} else {
			writeTestResults("Verify auto select 'Produce' as Activity Type for the second BOO",
					"Should auto select 'Produce' as Activity Type for the second BOO",
					"Not auto select 'Produce' as Activity Type for the second BOO", "fail");
		}

		sendKeys(txt_elementDescriptionBOO, produceElementDescription2);
		Thread.sleep(1000);
		String secondBOOEnteredDesc = getAttribute(txt_elementDescriptionBOO, "value");
		if (secondBOOEnteredDesc.equals(produceElementDescription2)) {
			writeTestResults("Verify user able to enter Element Description for the second BOO",
					"User should be able to enter Element Description for the second BOO",
					"User successfully enter Element Description for the second BOO", "pass");
		} else {
			writeTestResults("Verify user able to enter Element Description for the second BOO",
					"User should be able to enter Element Description for the second BOO",
					"User doesn't enter Element Description for the second BOO", "fail");
		}

		sendKeys(txt_groupIdBOO, groupIdBOOTwo);
		Thread.sleep(1000);
		String secondBOOEnteredGroupID = getAttribute(txt_groupIdBOO, "value");
		if (secondBOOEnteredGroupID.equals(groupIdBOOTwo)) {
			writeTestResults("Verify user able to enter Group ID for the second BOO",
					"User should be able to enter Group ID for the second BOO",
					"User successfully enter Group ID for the second BOO", "pass");
		} else {
			writeTestResults("Verify user able to enter Group ID for the second BOO",
					"User should be able to enter Group ID for the second BOO",
					"User doesn't enter Group ID for the second BOO", "fail");
		}

		Thread.sleep(1000);
		String secondBOOBatchMethod = getSelectedOptionInDropdown(drop_batchMethodBOO);
		if (secondBOOBatchMethod.equals(batchMethodTransferBOO)) {
			writeTestResults("Verify auto select 'Transfer' as Batch Method for the second BOO",
					"Should auto select 'Transfer' as Batch Method for the second BOO",
					"Auto select 'Transfer' as Batch Method for the second BOO", "pass");
		} else {
			writeTestResults("Verify auto select 'Transfer' as Batch Method for the second BOO",
					"Should auto select 'Transfer' as Batch Method for the second BOO",
					"Not auto select 'Transfer' as Batch Method for the second BOO", "fail");
		}

		click(btn_applyOperationOneBOO);

		explicitWait(btn_addBillOfOperationRowReplace.replace("row", "2"), 10);
		click(btn_addBillOfOperationRowReplace.replace("row", "2"));

		explicitWait(drop_elementCategoryBOO, 20);
		selectText(drop_elementCategoryBOO, elementCategoryOperation);
		Thread.sleep(1000);
		String thirdBOOElementCategory = getSelectedOptionInDropdown(drop_elementCategoryBOO);
		if (thirdBOOElementCategory.equals(elementCategoryOperation)) {
			writeTestResults("Verify user able to select 'Operation' as Element Category for the third BOO",
					"User should be able to select 'Operation' as Element Category for the third BOO",
					"User successfully select 'Operation' as Element Category for the third BOO", "pass");
		} else {
			writeTestResults("Verify user able to select 'Operation' as Element Category for the third BOO",
					"User should be able to select 'Operation' as Element Category for the third BOO",
					"User doesn't select 'Operation' as Element Category for the third BOO", "fail");
		}

		Thread.sleep(1000);
		selectText(drop_activityTypeBOO, activityTypeQualityCheck);
		String thirdBOOActivityType = getSelectedOptionInDropdown(drop_activityTypeBOO);
		if (thirdBOOActivityType.equals(activityTypeQualityCheck)) {
			writeTestResults("Verify user able to select 'Quality Check' as Activity Type for the third BOO",
					"User should be able to select 'Quality Check' as Activity Type for the third BOO",
					"User successfully select 'Quality Check' as Activity Type for the third BOO", "pass");
		} else {
			writeTestResults("Verify user able to select 'Quality Check' as Activity Type for the third BOO",
					"User should be able to select 'Quality Check' as Activity Type for the third BOO",
					"User doesn't select 'Quality Check' as Activity Type for the third BOO", "fail");
		}

		sendKeys(txt_elementDescriptionBOO, qcElementDescription);
		Thread.sleep(1000);
		String thirdBOOEnteredDesc = getAttribute(txt_elementDescriptionBOO, "value");
		if (thirdBOOEnteredDesc.equals(qcElementDescription)) {
			writeTestResults("Verify user able to enter Element Description for the third BOO",
					"User should be able to enter Element Description for the third BOO",
					"User successfully enter Element Description for the third BOO", "pass");
		} else {
			writeTestResults("Verify user able to enter Element Description for the third BOO",
					"User should be able to enter Element Description for the third BOO",
					"User doesn't enter Element Description for the third BOO", "fail");
		}

		sendKeys(txt_groupIdBOO, groupIdBOOThree);
		Thread.sleep(1000);
		String thirdBOOEnteredGroupID = getAttribute(txt_groupIdBOO, "value");
		if (thirdBOOEnteredGroupID.equals(groupIdBOOThree)) {
			writeTestResults("Verify user able to enter Group ID for the third BOO",
					"User should be able to enter Group ID for the third BOO",
					"User successfully enter Group ID for the third BOO", "pass");
		} else {
			writeTestResults("Verify user able to enter Group ID for the third BOO",
					"User should be able to enter Group ID for the third BOO",
					"User doesn't enter Group ID for the third BOO", "fail");
		}

		click(chk_finalInvestigationBOO);
		if (isSelected(chk_finalInvestigationBOO)) {
			writeTestResults("Verify user able to enable Final Investigation flag for the third BOO",
					"User should be able to enable Final Investigation flag for the third BOO",
					"User successfully enable Final Investigation flag for the third BOO", "pass");
		} else {
			writeTestResults("Verify user able to enable Final Investigation flag for the third BOO",
					"User should be able to enable Final Investigation flag for the third BOO",
					"User doesn't enable Final Investigation flag for the third BOO", "fail");
		}

		Thread.sleep(1000);
		String thirdBOOBatchMethod = getSelectedOptionInDropdown(drop_batchMethodBOO);
		if (thirdBOOBatchMethod.equals(batchMethodTransferBOO)) {
			writeTestResults("Verify auto select 'Transfer' as Batch Method for the third BOO",
					"Should auto select 'Transfer' as Batch Method for the third BOO",
					"Auto select 'Transfer' as Batch Method for the third BOO", "pass");
		} else {
			writeTestResults("Verify auto select 'Transfer' as Batch Method for the third BOO",
					"Should auto select 'Transfer' as Batch Method for the third BOO",
					"Not auto select 'Transfer' as Batch Method for the third BOO", "fail");
		}

		click(btn_applyOperationOneBOO);

		click(btn_draft);
		explicitWait(header_draftedBOO, 50);
		if (isDisplayed(header_draftedBOO)) {
			writeTestResults("Verify user able to draft the Bill Of Operation",
					"User should be able to draft the Bill Of Operation",
					"User successfully draft the Bill Of Operation", "pass");
		} else {
			writeTestResults("Verify user able to draft the Bill Of Operation",
					"User should be able to draft the Bill Of Operation", "User doesn't draft the Bill Of Operation",
					"fail");

		}

		click(btn_release);
		explicitWait(header_releasedBOO, 50);
		trackCode = getText(lbl_docNoBoo);
		writeProductionData("BOO_PROD_E2E_003", trackCode, 29);
		if (isDisplayed(header_releasedBOO)) {
			writeTestResults("Verify user able to release the Bill Of Operation",
					"User should be able to release the Bill Of Operation",
					"User successfully release the Bill Of Operation", "pass");
		} else {
			writeTestResults("Verify user able to release the Bill Of Operation",
					"User should be able to release the Bill Of Operation",
					"User doesn't release the Bill Of Operation", "fail");

		}
	}

	/* Production Model PROD_E2E_003 */
	public void productionModel1_PROD_E2E_003() throws Exception {
		pageRefersh();
		if (click(img_navigationPane)) {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User successfully click navigation pane", "pass");
		} else {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User couldn't click navigation pane", "fail");
		}

		explicitWait(btn_productionModule, 4);
		if (isEnabledQuickCheck(btn_productionModule)) {
			writeTestResults("Verify user able to view the \"Production\" module under main navigation menu",
					"User should be able to view the \"Production\" module under main navigation menu",
					"User successfully view the \"Production\" module under main navigation menu", "pass");
		} else {
			writeTestResults("Verify user able to view the \"Production\" module under main navigation menu",
					"User should be able to view the \"Production\" module under main navigation menu",
					"User couldn't view the \"Production\" module under main navigation menu", "fail");
		}

		click(btn_productionModule);

		explicitWait(btn_productionModel, 4);
		if (isEnabledQuickCheck(btn_productionModel)) {
			writeTestResults("Verify user able to view the \"Production Model\" option under production sub menu",
					"User should be able to view the \"Production Model\" option under production sub menu",
					"User able to view the \"Production Model\" option under production sub menu", "pass");
		} else {
			writeTestResults("Verify user able to view the \"Production Model\" option under production sub menu",
					"User should be able to view the \"Production Model\" option under production sub menu",
					"User couldn't view the \"Production Model\" option under production sub menu", "pass");
		}

		click(btn_productionModel);

		explicitWait(header_productionModelByPage, 40);
		if (isDisplayedQuickCheck(header_productionModelByPage)) {
			writeTestResults("Verify that user on the Production Model by page",
					"The user should be on the Production Model by page",
					"The user successfully on the Production Model by page", "pass");
		} else {
			writeTestResults("Verify that user on the Production Model by page",
					"The user should be on the Production Model by page",
					"The user not on the Production Model by page", "fail");
		}

		click(btn_newButtonProductionModel);
		explicitWait(header_newProductionModel, 40);
		if (isDisplayedQuickCheck(header_newProductionModel)) {
			writeTestResults("Verify that user can navigate to the new production model creation page",
					"The user should be navigate to the new production model creation page",
					"The user successfully navigate to the new production model creation page", "pass");
		} else {
			writeTestResults("Verify that user can navigate to the new production model creation page",
					"The user should be navigate to the new production model creation page",
					"The user doesn't navigate to the new production model creation page", "fail");
		}

		String modelCode = invObj.currentTimeAndDate().replace("/", "").replace(":", "");
		sendKeys(txt_modelCodeProductionModel, modelCode);
		String enteresModelCode = getAttribute(txt_modelCodeProductionModel, "value");
		if (enteresModelCode.equals(modelCode)) {
			writeTestResults(
					"Verify that system is displayed the user entered data in 'Model Code' field of summary sections",
					"The system should be displayed the user entered data in 'Model Code' field of summary sections",
					"The system successfully displayed the user entered data in 'Model Code' field of summary sections",
					"pass");
		} else {
			writeTestResults(
					"Verify that system is displayed the user entered data in 'Model Code' field of summary sections",
					"The system should be displayed the user entered data in 'Model Code' field of summary sections",
					"The system doesn't displayed the user entered data in 'Model Code' field of summary sections",
					"fail");
		}

		sendKeys(txt_descriptionProductionModel, commonDescription);
		String enteresDescription = getAttribute(txt_descriptionProductionModel, "value");
		if (enteresDescription.equals(commonDescription)) {
			writeTestResults(
					"Verify that system is displayed the user entered data in 'Description' field of summary sections",
					"The system should be displayed the user entered data in 'Description' field of summary sections",
					"The system successfully displayed the user entered data in 'Description' field of summary sections",
					"pass");
		} else {
			writeTestResults(
					"Verify that system is displayed the user entered data in 'Description' field of summary sections",
					"The system should be displayed the user entered data in 'Description' field of summary sections",
					"The system doesn't displayed the user entered data in 'Description' field of summary sections",
					"fail");
		}

		selectText(drop_productionGroupProductionModel, productionGroupE2E);
		explicitWaitUntillClickable(btn_yesClearConfirmation, 5);
		click(btn_yesClearConfirmation);
		Thread.sleep(2000);
		String selectedProductGroup = getSelectedOptionInDropdown(drop_productionGroupProductionModel);
		if (selectedProductGroup.equals(productionGroupE2E)) {
			writeTestResults("Verify that user selected product group is displayed",
					"The user selected product group should be displayed",
					"The user selected product group successfully displayed", "pass");
		} else {
			writeTestResults("Verify that user selected product group is displayed",
					"The user selected product group should be displayed",
					"The user selected product group doesn't displayed", "fail");
		}

	}

	public void productionModel2_PROD_E2E_003() throws Exception {
		click(btn_productLookupProductionModel);
		explicitWait(header_productLookup, 20);
		if (isDisplayedQuickCheck(header_productLookup)) {
			writeTestResults("Verify that product look up screen is loaded",
					"The product look up screen should be loaded", "The product look up screen successfully loaded",
					"pass");
		} else {
			writeTestResults("Verify that product look up screen is loaded",
					"The product look up screen should be loaded", "The product look up screen doesn't loaded", "fail");
		}

		sendKeysLookup(txt_searchProductCommon, invObj.readTestCreation("BatchFifoMultiPlatformProductionType"));
		Thread.sleep(500);
		String enteredProduct = getAttribute(txt_searchProductCommon, "value");
		if (enteredProduct.equals(invObj.readTestCreation("BatchFifoMultiPlatformProductionType"))) {
			writeTestResults("Verify that user entred product code/ description on the search area is displayed",
					"The user entred product code/ description on the search area should be displayed",
					"The user entred product code/ description on the search area successfully displayed", "pass");
		} else {
			writeTestResults("Verify that user entred product code/ description on the search area is displayed",
					"The user entred product code/ description on the search area should be displayed",
					"The user entred product code/ description on the search area doesn't displayed", "fail");
		}

		if (click(btn_searchProductProductionModel)) {
			writeTestResults("Verify that search button is clicked successfully",
					"The search button should be clicked successfully", "The search button clicked successfully",
					"pass");
		} else {
			writeTestResults("Verify that search button is clicked successfully",
					"The search button should be clicked successfully", "The search button doesn't clicked", "fail");
		}

		Thread.sleep(2000);
		explicitWait(tr_reultOnLookupInfoReplace.replace("info",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")), 25);
		doubleClick(tr_reultOnLookupInfoReplace.replace("info",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")));
		explicitWaitUntillClickable(btn_yesClearConfirmation, 5);
		click(btn_yesClearConfirmation);
		Thread.sleep(2000);

		String selectedProduct = getAttribute(txt_productOnFrontPagePM, "value");
		if (selectedProduct.contains(invObj.readTestCreation("BatchFifoMultiPlatformProductionType"))) {
			writeTestResults("Verify that user selected product is diplayed",
					"The user selected product should be diplayed", "The user selected product successfully diplayed",
					"pass");
		} else {
			writeTestResults("Verify that user selected product is diplayed",
					"The user selected product should be diplayed", "The user selected product doesn't diplayed",
					"fail");
		}

		click(btn_searchProductionUnit);
		explicitWait(header_productionUnitLookupPM, 15);
		if (isDisplayed(header_productionUnitLookupPM)) {
			writeTestResults("Verify that production unit look up screen was loaded",
					"The production unit look up screen should be loaded",
					"The production unit look up successfully loaded", "pass");
		} else {
			writeTestResults("Verify that production unit look up screen was loaded",
					"The production unit look up screen should be loaded", "The production unit look up doesn't loaded",
					"fail");
		}

		sendKeysLookup(txt_searchProductionUnit, readProductionData("Location_PROD_E2E"));
		String enterdProductionUnitOnSerachBox = getAttribute(txt_searchProductionUnit, "value");
		if (enterdProductionUnitOnSerachBox.equals(readProductionData("Location_PROD_E2E"))) {
			writeTestResults(
					"Verify that the user entred production unit code/ description on the search area is displayed",
					"The user entred production unit code/ description on the search area should be displayed",
					"The user entred production unit code/ description on the search area successfully displayed",
					"pass");
		} else {
			writeTestResults(
					"Verify that the user entred production unit code/ description on the search area is displayed",
					"The user entred production unit code/ description on the search area should be displayed",
					"The user entred production unit code/ description on the search area doesn't displayed", "fail");
		}

		if (click(btn_searchProductionUnitOnLookup)) {
			writeTestResults("Verify that the the search button was clicked",
					"The search button should be clicked successfully", "The search button clicked successfully",
					"pass");
		} else {
			writeTestResults("Verify that the the search button was clicked",
					"The search button should be clicked successfully", "The search button doesn't click", "fail");
		}

		Thread.sleep(2000);
		explicitWaitUntillClickable(lnk_resultProductionUnitOnLookupUnitReplace.replace("production_unit",
				readProductionData("Location_PROD_E2E")), 20);
		if (isDisplayed(lnk_resultProductionUnitOnLookupUnitReplace.replace("production_unit",
				readProductionData("Location_PROD_E2E")))) {
			writeTestResults("Verify that search result relevent to enterd code is dispaly",
					"Search result relevent to enterd code should be dispaly",
					"Search result relevent to enterd code successfully dispaly", "pass");
		} else {
			writeTestResults("Verify that search result relevent to enterd code is dispaly",
					"Search result relevent to enterd code should be dispaly",
					"Search result relevent to enterd code doesn't dispaly", "fail");
		}

		doubleClick(lnk_resultProductionUnitOnLookupUnitReplace.replace("production_unit",
				readProductionData("Location_PROD_E2E")));
		Thread.sleep(2500);

		String selectedProductionUnit = getAttribute(txt_productionUnitOnFrontPage, "value");
		if (selectedProductionUnit.contains(readProductionData("Location_PROD_E2E"))) {
			writeTestResults("Verify that the user selected 'Production Unit' is diplayed",
					"The user selected 'Production Unit' should be diplayed",
					"The user selected 'Production Unit' successfully diplayed", "pass");
		} else {
			writeTestResults("Verify that the user selected 'Production Unit' is diplayed",
					"The user selected 'Production Unit' should be diplayed",
					"The user selected 'Production Unit' doesn't diplayed", "fail");
		}

		String autoSelectedInputWare = getSelectedOptionInDropdown(drop_inputWarehousePM);
		if (autoSelectedInputWare.contains(inputWarehouseLI)) {
			writeTestResults(
					"Verify that the system was auto selected 'Input Warehouse' when user select the 'Production Unit'",
					"The system should be auto selected 'Input Warehouse' when user select the 'Production Unit'",
					"The system successfully auto selected 'Input Warehouse' when user select the 'Production Unit'",
					"pass");
		} else {
			writeTestResults(
					"Verify that the system was auto selected 'Input Warehouse' when user select the 'Production Unit'",
					"The system should be auto selected 'Input Warehouse' when user select the 'Production Unit'",
					"The system doesn't auto selected 'Input Warehouse' when user select the 'Production Unit'",
					"fail");
		}

		String autoSelectedOutputWare = getSelectedOptionInDropdown(drop_outputWarehousePM);
		if (autoSelectedOutputWare.contains(outputWarehouseLI)) {
			writeTestResults(
					"Verify that the system was auto selected 'Output Warehouse' when user select the 'Production Unit'",
					"The system should be auto selected 'Output Warehouse' when user select the 'Production Unit'",
					"The system successfully auto selected 'Output Warehouse' when user select the 'Production Unit'",
					"pass");
		} else {
			writeTestResults(
					"Verify that the system was auto selected 'Output Warehouse' when user select the 'Production Unit'",
					"The system should be auto selected 'Output Warehouse' when user select the 'Production Unit'",
					"The system doesn't auto selected 'Output Warehouse' when user select the 'Production Unit'",
					"fail");
		}

		String autoSelectedWipWare = getSelectedOptionInDropdown(drop_wipWarehousePM);
		if (autoSelectedWipWare.contains(wipWarehouseLI)) {
			writeTestResults(
					"Verify that the system was auto selected 'WIP Warehouse' when user select the 'Production Unit'",
					"The system should be auto selected 'WIP Warehouse' when user select the 'Production Unit'",
					"The system successfully auto selected 'WIP Warehouse' when user select the 'Production Unit'",
					"pass");
		} else {
			writeTestResults(
					"Verify that the system was auto selected 'WIP Warehouse' when user select the 'Production Unit'",
					"The system should be auto selected 'WIP Warehouse' when user select the 'Production Unit'",
					"The system doesn't auto selected 'WIP Warehouse' when user select the 'Production Unit'", "fail");
		}

		sendKeys(txt_batchQuantityOtherInformationPM, batchQuantityE2E);

		selectText(drop_barcodeBookOtherInformationPM, barcodeBookPM);
		String selectedBarcodeBook = getSelectedOptionInDropdown(drop_barcodeBookOtherInformationPM);
		if (selectedBarcodeBook.contains(barcodeBookPM)) {
			writeTestResults("Verify user able to select Barcode Book", "User should be able to select Barcode Book",
					"User successfully select Barcode Book", "pass");
		} else {
			writeTestResults("Verify user able to select Barcode Book", "User should be able to select Barcode Book",
					"User couldn't select Barcode Book", "fail");
		}

		selectText(drop_costingPriorityOtherInformationPM, costicPriorityNormalPM);
		String selectedCostingPriority = getSelectedOptionInDropdown(drop_costingPriorityOtherInformationPM);
		if (selectedCostingPriority.contains(costicPriorityNormalPM)) {
			writeTestResults("Verify user able to select Costing Priority",
					"User should be able to select Costing Priority", "User successfully select Costing Priority",
					"pass");
		} else {
			writeTestResults("Verify user able to select Costing Priority",
					"User should be able to select Costing Priority", "User couldn't select Costing Priority", "fail");
		}

		sendKeys(txt_statndardCostOtherInformationPM, standardCostPM);
		String enteredStandardCost = getAttribute(txt_statndardCostOtherInformationPM, "value");
		if (enteredStandardCost.equals(standardCostPM)) {
			writeTestResults("Verify that user able to enter Standard Cost",
					"User should be able to enter Standard Cost", "User successfully enter Standard Cost", "pass");
		} else {
			writeTestResults("Verify that user able to enter Standard Cost",
					"User should be able to enter Standard Cost", "User couldn't enter Standard Cost", "fail");
		}

		click(btn_lookupPricingProfilePM);
		explicitWait(header_pricingProfileLookupPM, 15);
		if (isDisplayed(header_pricingProfileLookupPM)) {
			writeTestResults("Verify that pricing profile look up screen was loaded",
					"The pricing profile look up screen should be loaded",
					"The pricing profile look up successfully loaded", "pass");
		} else {
			writeTestResults("Verify that pricing profile look up screen was loaded",
					"The pricing profile look up screen should be loaded", "The pricing profile look up doesn't loaded",
					"fail");
		}

		sendKeysLookup(txt_searchPricingProfile, readProductionData("Pricing_Profile_PROD_E2E"));
		String enterdPricingProfileOnSerachBox = getAttribute(txt_searchPricingProfile, "value");
		if (enterdPricingProfileOnSerachBox.equals(readProductionData("Pricing_Profile_PROD_E2E"))) {
			writeTestResults(
					"Verify that the user entred pricing profile code/ description on the search area is displayed",
					"The user entred pricing profile code/ description on the search area should be displayed",
					"The user entred pricing profile code/ description on the search area successfully displayed",
					"pass");
		} else {
			writeTestResults(
					"Verify that the user entred pricing profile code/ description on the search area is displayed",
					"The user entred pricing profile code/ description on the search area should be displayed",
					"The user entred pricing profile code/ description on the search area doesn't displayed", "fail");
		}

		if (click(btn_searchPricingProfilePM)) {
			writeTestResults("Verify that the the search button was clicked",
					"The search button should be clicked successfully", "The search button clicked successfully",
					"pass");
		} else {
			writeTestResults("Verify that the the search button was clicked",
					"The search button should be clicked successfully", "The search button doesn't click", "fail");
		}

		Thread.sleep(2000);
		explicitWaitUntillClickable(td_resultPricingProfileReplacePP.replace("pricing_profile",
				readProductionData("Pricing_Profile_PROD_E2E")), 20);
		if (isDisplayed(td_resultPricingProfileReplacePP.replace("pricing_profile",
				readProductionData("Pricing_Profile_PROD_E2E")))) {
			writeTestResults("Verify that search result relevent to enterd code is dispaly",
					"Search result relevent to enterd code should be dispaly",
					"Search result relevent to enterd code successfully dispaly", "pass");
		} else {
			writeTestResults("Verify that search result relevent to enterd code is dispaly",
					"Search result relevent to enterd code should be dispaly",
					"Search result relevent to enterd code doesn't dispaly", "fail");
		}

		doubleClick(td_resultPricingProfileReplacePP.replace("pricing_profile",
				readProductionData("Pricing_Profile_PROD_E2E")));
		Thread.sleep(2500);

		String selectedPricingProfile = getAttribute(txt_pricingProfileOtherInformationPM, "value");
		if (selectedPricingProfile.contains(readProductionData("Pricing_Profile_PROD_E2E"))) {
			writeTestResults("Verify that the user selected 'Pricing Profile' is diplayed",
					"The user selected 'Pricing Profile' should be diplayed",
					"The user selected 'Pricing Profile' successfully diplayed", "pass");
		} else {
			writeTestResults("Verify that the user selected 'Pricing Profile' is diplayed",
					"The user selected 'Pricing Profile' should be diplayed",
					"The user selected 'Pricing Profile' doesn't diplayed", "fail");
		}

		click(chk_mrpProductionModel);
		if (isSelected(chk_mrpProductionModel)) {
			writeTestResults("Verify that user able to enable MRP", "User should be able to enable MRP",
					"User successfully enable MRP", "pass");
		} else {
			writeTestResults("Verify that user able to enable MRP", "User should be able to enable MRP",
					"User couldn't enable MRP", "fail");
		}

		pageScrollUpToBottom();
		try {
			String bom = readProductionData("BOM_PROD_E2E_003");
			selectTextByContains(drop_bomPM, bom);
			explicitWaitUntillClickable(btn_yesClearConfirmation, 5);
			click(btn_yesClearConfirmation);
			Thread.sleep(2000);
			String selectedBOM = getSelectedOptionInDropdown(drop_bomPM);
			if (selectedBOM.contains(readProductionData("BOM_PROD_E2E_003"))) {
				writeTestResults("Verify user able to select Bill of Material",
						"User should be able to select Bill of Material", "User successfully select Bill of Material",
						"pass");
			} else {
				writeTestResults("Verify user able to select Bill of Material",
						"User should be able to select Bill of Material", "User couldn't select Bill of Material",
						"fail");
			}
		} catch (Exception e) {
			writeTestResults("Verify user able to select Bill of Material",
					"User should be able to select Bill of Material", "User couldn't select Bill of Material", "fail");
		}

	}

	public void productionModel3_PROD_E2E_003() throws Exception {
		click(btn_lookupBOOPM);
		explicitWait(header_billOfOperatioPM, 15);
		if (isDisplayed(header_billOfOperatioPM)) {
			writeTestResults("Verify that Bill of Operation look up screen was loaded",
					"The Bill of Operation look up screen should be loaded",
					"The Bill of Operation look up successfully loaded", "pass");
		} else {
			writeTestResults("Verify that Bill of Operation look up screen was loaded",
					"The Bill of Operation look up screen should be loaded",
					"The Bill of Operation look up doesn't loaded", "fail");
		}

		sendKeysLookup(txt_searchBillOfOperationPM, readProductionData("BOO_PROD_E2E_003"));
		String enterdBOO = getAttribute(txt_searchBillOfOperationPM, "value");
		if (enterdBOO.equals(readProductionData("BOO_PROD_E2E_003"))) {
			writeTestResults(
					"Verify that the user entred bill of operation code/ description on the search area is displayed",
					"The user entred bill of operation code/ description on the search area should be displayed",
					"The user entred bill of operation code/ description on the search area successfully displayed",
					"pass");
		} else {
			writeTestResults(
					"Verify that the user entred bill of operation code/ description on the search area is displayed",
					"The user entred bill of operation code/ description on the search area should be displayed",
					"The user entred bill of operation code/ description on the search area doesn't displayed", "fail");
		}

		if (click(btn_searchBOOInLookup)) {
			writeTestResults("Verify that the the search button was clicked",
					"The search button should be clicked successfully", "The search button clicked successfully",
					"pass");
		} else {
			writeTestResults("Verify that the the search button was clicked",
					"The search button should be clicked successfully", "The search button doesn't click", "fail");
		}

		Thread.sleep(2000);
		explicitWaitUntillClickable(td_resultBOOPMBooReplace.replace("boo", readProductionData("BOO_PROD_E2E_003")),
				20);
		if (isDisplayed(td_resultBOOPMBooReplace.replace("boo", readProductionData("BOO_PROD_E2E_003")))) {

			writeTestResults("Verify that search result relevent to enterd code is dispaly",
					"Search result relevent to enterd code should be dispaly",
					"Search result relevent to enterd code successfully dispaly", "pass");
		} else {
			writeTestResults("Verify that search result relevent to enterd code is dispaly",
					"Search result relevent to enterd code should be dispaly",
					"Search result relevent to enterd code doesn't dispaly", "fail");
		}

		doubleClick(td_resultBOOPMBooReplace.replace("boo", readProductionData("BOO_PROD_E2E_003")));
		Thread.sleep(2500);

		click(btn_okClearConfirmationBOOPM);

		Thread.sleep(2000);
		String selectedBOO = getAttribute(txt_billOfOperationPMFrontPage, "value");
		if (selectedBOO.contains(readProductionData("BOO_PROD_E2E_003"))) {
			writeTestResults("Verify that the user selected 'Bill of Operation' is diplayed",
					"The user selected 'Bill of Operation' should be diplayed",
					"The user selected 'Bill of Operation' successfully diplayed", "pass");
		} else {
			writeTestResults("Verify that the user selected 'Bill of Operation' is diplayed",
					"The user selected 'Bill of Operation' should be diplayed",
					"The user selected 'Bill of Operation' doesn't diplayed", "fail");
		}

		click(tab_billOfOperationProductionModel);
		if (isDisplayed(tab_billOfOperationAfterSelected)) {
			writeTestResults("Verify that the user is on the bill of operations tab",
					"The user should be on the bill of operations tab",
					"The user successfully on the bill of operations tab", "pass");
		} else {
			writeTestResults("Verify that the user is on the bill of operations tab",
					"The user should be on the bill of operations tab", "The user is not on the bill of operations tab",
					"fail");
		}

		click(btn_expandPM);
		Thread.sleep(1500);
		if (isDisplayedQuickCheck(tr_booRow1PM) && isDisplayedQuickCheck(tr_booRow2PM)
				&& isDisplayedQuickCheck(tr_booRow3PM) && isDisplayedQuickCheck(tr_booRow4PM)) {
			writeTestResults("Verify that system is displayed the bill of operations details",
					"The system should be displayed the bill of operations details",
					"The system successfully displayed the bill of operations details", "pass");
		} else {
			writeTestResults("Verify that system is displayed the bill of operations details",
					"The system should be displayed the bill of operations details",
					"The system doesn't displayed the bill of operations details", "fail");

		}

		click(btn_editBillOfOperationPMRowReplace.replace("row", "1"));
		explicitWait(header_operationActivityPopup, 20);
		if (isDisplayedQuickCheck(header_operationActivityPopup)) {
			writeTestResults("Verify that Operations/Activity screen was loaded",
					"Operations/Activity screen should be loaded", "Operations/Activity screen successfully loaded",
					"pass");
		} else {
			writeTestResults("Verify that Operations/Activity screen was loaded",
					"Operations/Activity screen should be loaded", "Operations/Activity screen wasn't loaded", "fail");
		}

		click(btn_inputProductsTabBOOPM);
		explicitWait(li_inputProductSelected, 10);
		if (isDisplayedQuickCheck(li_inputProductSelected)) {
			writeTestResults("Verify that user can moved to the Input Products tab",
					"User should be moved to the Input Products tab",
					"User successfully moved to the Input Products tab", "pass");
		} else {
			writeTestResults("Verify that user can moved to the Input Products tab",
					"User should be moved to the Input Products tab", "User couldn't moved to the Input Products tab",
					"fail");
		}

		click(chk_inputProductsRowReplaceBOOPM.replace("row", "1"));
		click(chk_inputProductsRowReplaceBOOPM.replace("row", "2"));
		click(chk_inputProductsRowReplaceBOOPM.replace("row", "3"));

		if (isSelected(chk_inputProductsRowReplaceBOOPM.replace("row", "1"))
				&& isSelected(chk_inputProductsRowReplaceBOOPM.replace("row", "2"))
				&& isSelected(chk_inputProductsRowReplaceBOOPM.replace("row", "3"))) {
			writeTestResults("Verify that user able to enable both input products for initial operation",
					"User should be able to enable both input products for initial operation",
					"User successfully enable both input products for initial operation", "pass");
		} else {
			writeTestResults("Verify that user able to enable both input products for initial operation",
					"User should be able to enable both input products for initial operation",
					"User doesn't enable both input products for initial operation", "fail");
		}

		click(btn_applyBOOPM);

		explicitWait(btn_editBillOfOperationPMRowReplace.replace("row", "2"), 10);
		click(btn_editBillOfOperationPMRowReplace.replace("row", "2"));

		selectText(drop_processTimeTypeBOOPM, processTimeTypePM);

		click(btn_inputProductsTabBOOPM);
		explicitWait(li_inputProductSelected, 10);
		if (isDisplayedQuickCheck(li_inputProductSelected)) {
			writeTestResults("Verify that user can moved to the Input Products tab",
					"User should be moved to the Input Products tab",
					"User successfully moved to the Input Products tab", "pass");
		} else {
			writeTestResults("Verify that user can moved to the Input Products tab",
					"User should be moved to the Input Products tab", "User couldn't moved to the Input Products tab",
					"fail");
		}

		click(chk_inputProductsRowReplaceBOOPM.replace("row", "1"));
		click(chk_inputProductsRowReplaceBOOPM.replace("row", "2"));
		if (isSelected(chk_inputProductsRowReplaceBOOPM.replace("row", "1"))
				&& isSelected(chk_inputProductsRowReplaceBOOPM.replace("row", "2"))) {
			writeTestResults("Verify that user able to enable a product for the first operation",
					"User should be able to enable a product for the first operation",
					"User successfully enable a product for the first operation", "pass");
		} else {
			writeTestResults("Verify that user able to enable a product for the first operation",
					"User should be able to enable a product for the first operation",
					"User doesn't enable a product for the first operation", "fail");
		}

		click(btn_applyBOOPM);

		explicitWait(btn_editBillOfOperationPMRowReplace.replace("row", "3"), 10);
		click(btn_editBillOfOperationPMRowReplace.replace("row", "3"));

		selectText(drop_processTimeTypeBOOPM, processTimeTypePM);

		click(btn_inputProductsTabBOOPM);
		click(chk_inputProductsRowReplaceBOOPM.replace("row", "1"));
		if (isSelected(chk_inputProductsRowReplaceBOOPM.replace("row", "1"))) {
			writeTestResults("Verify that user able to enable a product for the second operation",
					"User should be able to enable a product for the second operation",
					"User successfully enable a product for the second operation", "pass");
		} else {
			writeTestResults("Verify that user able to enable a product for the second operation",
					"User should be able to enable a product for the second operation",
					"User doesn't enable a product for the second operation", "fail");
		}

		click(btn_applyBOOPM);

		explicitWaitUntillClickable(btn_draft, 15);
		click(btn_draft);
		explicitWait(header_draftedProductionModel, 40);
		if (isDisplayedQuickCheck(header_draftedProductionModel)) {
			writeTestResults("Verify that the system was drafted the newly created Production Model",
					"The system should be drafted the newly created Production Model",
					"The system successfully drafted the newly created Production Model", "pass");
		} else {
			writeTestResults("Verify that the system was drafted the newly created Production Model",
					"The system should be drafted the newly created Production Model",
					"The system wasn't drafted the newly created Production Model", "fail");

		}

		click(btn_edit);
		explicitWaitUntillClickable(btn_update, 30);
		if (isDisplayedQuickCheck(btn_update)) {
			writeTestResults("Verify that the system is displayed update button in the summary tab",
					"The system should be displayed update button in the summary tab",
					"The system successfully displayed update button in the summary tab", "pass");
		} else {
			writeTestResults("Verify that the system is displayed update button in the summary tab",
					"The system should be displayed update button in the summary tab",
					"The system doesn't displayed update button in the summary tab", "fail");
		}

		if (isDisplayedQuickCheck(tab_summaryProductionModel)
				&& isDisplayedQuickCheck(tab_billOfOperationProductionModel)) {
			writeTestResults("The system doesn't displayed the tabs mentioned here (Summary, Bill of operations)",
					"The system should be displayed the tabs mentioned here (Summary, Bill of operations)",
					"The system successfully displayed the tabs mentioned here (Summary, Bill of operations)", "pass");
		} else {
			writeTestResults("The system doesn't displayed the tabs mentioned here (Summary, Bill of operations)",
					"The system should be displayed the tabs mentioned here (Summary, Bill of operations)",
					"The system doesn't displayed the tabs mentioned here (Summary, Bill of operations)", "fail");
		}

		click(tab_billOfOperationProductionModel);
		explicitWaitUntillClickable(btn_expandPM, 10);
		click(btn_expandPM);
		explicitWaitUntillClickable(btn_editOperation01PM, 10);
	}

	public void productionModel4_PROD_E2E_003() throws Exception {
		click(btn_editOperation01PM);
		explicitWait(header_operationActivityPopup, 20);
		if (isDisplayedQuickCheck(header_operationActivityPopup)) {
			writeTestResults("Verify that Operations/Activity screen was loaded",
					"Operations/Activity screen should be loaded", "Operations/Activity screen successfully loaded",
					"pass");
		} else {
			writeTestResults("Verify that Operations/Activity screen was loaded",
					"Operations/Activity screen should be loaded", "Operations/Activity screen wasn't loaded", "fail");
		}

		click(btn_additionalResourseTabPM);
		explicitWait(btn_additionalResourseTabSelectedPM, 20);
		if (isDisplayedQuickCheck(btn_additionalResourseTabSelectedPM)) {
			writeTestResults("Verify that user can moved to the additional resourse tab",
					"User should be moved to the additional resourse tab",
					"User successfully moved to the additional resourse tab", "pass");
		} else {
			writeTestResults("Verify that user can moved to the additional resourse tab",
					"User should be moved to the additional resourse tab",
					"User couldn't moved to the additional resourse tab", "fail");
		}

		click(btn_resourseLookupAdditionalResourseTabPM);
		explicitWait(lookup_resource, 20);
		if (isDisplayedQuickCheck(lookup_resource)) {
			writeTestResults("Verify that resource look up was loaded", "Resource look up should be loaded",
					"Resource look up successfully loaded", "pass");
		} else {
			writeTestResults("Verify that resource look up was loaded", "Resource look up should be loaded",
					"Resource look up doesn't loaded", "fail");
		}

		sendKeysLookup(txt_searchResource, readProductionData("ResourceCode02_E2E"));
		click(btn_resourceSearchInLookup);
		Thread.sleep(2000);
		explicitWait(td_resultResourceAdditionalResourceTabPMResourseReplace.replace("resource",
				readProductionData("ResourceCode02_E2E")), 7);
		doubleClick(td_resultResourceAdditionalResourceTabPMResourseReplace.replace("resource",
				readProductionData("ResourceCode02_E2E")));
		explicitWait(div_resourceOnAdditonalResourceGridPMResorceReplace.replace("resource",
				readProductionData("ResourceCode02_E2E")), 30);
		if (isDisplayedQuickCheck(div_resourceOnAdditonalResourceGridPMResorceReplace.replace("resource",
				readProductionData("ResourceCode02_E2E")))) {
			writeTestResults("Verify that selected resource is display in additional resource tab",
					"Selected resource should be display in additional resource tab",
					"Selected resource successfully display in additional resource tab", "pass");
		} else {
			writeTestResults("Verify that selected resource is display in additional resource tab",
					"Selected resource should be display in additional resource tab",
					"Selected resource successfully display in additional resource tab", "pass");
		}

		if (isDisplayedQuickCheck(header_resourceHoursAdditionalResourseTabPM)) {
			writeTestResults(
					"Verify that resource duration column placed without change in position in the additional resource tab",
					"Resource duration column should be placed without change in position in the additional resource tab",
					"Resource duration column placed without change in position in the additional resource tab",
					"pass");
		} else {
			writeTestResults(
					"Verify that resource duration column placed without change in position in the additional resource tab",
					"Resource duration column should be placed without change in position in the additional resource tab",
					"Resource duration column not placed without change in position in the additional resource tab",
					"fail");
		}

		click(btn_selectHoursAddtionalResourceTabPM);
		Thread.sleep(1500);
		click(btn_oneHourAdditionalResourceHoursPM);
		Thread.sleep(1500);
		click(btn_okDurationAdditionalResourceHoursPM);
		Thread.sleep(2000);

		String AdditionalResourseHours = getAttribute(btn_selectHoursAddtionalResourceTabPM, "value");
		String AdditionalResourseMins = getAttribute(btn_selectMinutesAddtionalResourceTabPM, "value");

		if (AdditionalResourseHours.equals("01") && AdditionalResourseMins.equals("00")) {
			writeTestResults("Verify that resource duration entered in the additional resource tab",
					"Resource duration should be entered in the additional resource tab",
					"Resource duration successfully entered in the additional resource tab", "pass");
		} else {
			writeTestResults("Verify that resource duration entered in the additional resource tab",
					"Resource duration should be entered in the additional resource tab",
					"Resource duration doesn't entered in the additional resource tab", "fail");
		}

		click(btn_applyOperationActivity);
		waitUntillNotVisible(header_operationActivityPopup, 15);
		if (!isDisplayedQuickCheck(header_operationActivityPopup)) {
			writeTestResults("Verify that additional resource was added to the operations successfully",
					"The additional resource should be added to the operations successfully",
					"The additional resource successfully added to the operations successfully", "pass");
		} else {
			writeTestResults("Verify that additional resource was added to the operations successfully",
					"The additional resource should be added to the operations successfully",
					"The additional resource was not added to the operations", "fail");
		}

		click(btn_update);
		explicitWaitUntillClickable(btn_edit, 30);
		if (isEnabled(btn_edit)) {
			writeTestResults("Verify user able to update the Production Model",
					"User should be able to update the Production Model",
					"User uccessfully update the Production Model", "pass");
		} else {
			writeTestResults("Verify user able to update the Production Model",
					"User should be able to update the Production Model", "User couldn't update the Production Model",
					"fail");
		}

		click(btn_release);

		explicitWait(header_releasedProductionModel, 60);
		trackCode = getText(div_productionModelCode);
		String productionModelCode = trackCode;
		writeProductionData("ProductionModel_PROD_E2E_003", productionModelCode, 30);
		if (isDisplayed(header_releasedProductionModel)) {
			writeTestResults("Verify user able to release the Production Model",
					"User should be able to release the Production Model",
					"User uccessfully release the Production Model", "pass");
		} else {
			writeTestResults("Verify user able to release the Production Model",
					"User should be able to release the Production Model", "User couldn't release the Production Model",
					"fail");
		}

	}

	/* Production Order PROD_E2E_003 */
	public void productionOrder_1_PROD_E2E_003() throws Exception {
		explicitWaitUntillClickable(img_navigationPane, 60);
		Thread.sleep(2000);
		explicitWaitUntillClickable(img_navigationPane, 20);
		if (click(img_navigationPane)) {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User successfully click navigation pane", "pass");
		} else {
			writeTestResults("Verify user able to click navigation pane",
					"User should be able to click navigation pane", "User couldn't click navigation pane", "fail");
		}

		explicitWait(btn_production, 4);
		if (isEnabledQuickCheck(btn_production)) {
			writeTestResults("Verify user able to view the \"Production\" module under main navigation menu",
					"User should be able to view the \"Production\" module under main navigation menu",
					"User successfully view the \"Production\" module under main navigation menu", "pass");
		} else {
			writeTestResults("Verify user able to view the \"Production\" module under main navigation menu",
					"User should be able to view the \"Production\" module under main navigation menu",
					"User couldn't view the \"Production\" module under main navigation menu", "fail");
		}

		click(btn_production);

		explicitWait(btn_productionOrder, 4);
		if (isEnabledQuickCheck(btn_productionOrder)) {
			writeTestResults("Verify user able to view the \"Production Order\" option under production sub menu",
					"User should be able to view the \"Production Order\" option under production sub menu",
					"User able to view the \"Production Order\" option under production sub menu", "pass");
		} else {
			writeTestResults("Verify user able to view the \"Production Order\" option under production sub menu",
					"User should be able to view the \"Production Order\" option under production sub menu",
					"User couldn't view the \"Production Order\" option under production sub menu", "pass");
		}

		click(btn_productionOrder);

		explicitWait(header_productionOrderByPage, 60);
		if (isDisplayed(header_productionOrderByPage)) {
			writeTestResults("Verify user able to navigate to Production Order by-page",
					"User should be able to navigate to Production Order by-page",
					"User successfully navigate to Production Order by-page", "pass");
		} else {
			writeTestResults("Verify user able to navigate to Production Order by-page",
					"User should be able to navigate to Production Order by-page",
					"User coulsn't navigate to Production Order by-page", "fail");
		}

		if (isDisplayed(header_productionOrderDocNoColumnOnByPage)) {
			writeTestResults("Verify that 'Doc No' column is displayed as earlier",
					"'Doc No' column should be displayed as earlier",
					"'Doc No' column successfully displayed as earlier", "pass");
		} else {
			writeTestResults("Verify that 'Doc No' column is displayed as earlier",
					"'Doc No' column should be displayed as earlier", "'Doc No' column doesn't displayed as earlier",
					"fail");
		}

		click(btn_newProductionOrder);
		explicitWait(header_newProductionOrder, 60);
		if (isDisplayed(header_newProductionOrder)) {
			writeTestResults("Verify user able to navigate to Production Order new-form",
					"User should be able to navigate to Production Order new-form",
					"User successfully navigate to Production Order new-form", "pass");
		} else {
			writeTestResults("Verify user able to navigate to Production Order new-form",
					"User should be able to navigate to Production Order new-form",
					"User coulsn't navigate to Production Order new-form", "fail");
		}

		click(span_productLookupPO);
		explicitWait(lookup_product, 30);
		if (isDisplayed(lookup_product)) {
			writeTestResults("Verify that product lookup is open", "Product lookup should be open",
					"Product lookup successfully open", "pass");
		} else {
			writeTestResults("Verify that product lookup is open", "Product lookup should be open",
					"Product lookup doesn't open", "fail");
		}

		sendKeys(txt_searchProductProductLookup, invObj.readTestCreation("BatchFifoMultiPlatformProductionType"));
		String enteredProduct = getAttribute(txt_searchProductProductLookup, "value");
		if (enteredProduct.equals(invObj.readTestCreation("BatchFifoMultiPlatformProductionType"))) {
			writeTestResults("Verify that user able to enter the relevent product",
					"User should be able to enter the relevent product", "User successfully enter the relevent product",
					"pass");
		} else {
			writeTestResults("Verify that user able to enter the relevent product",
					"User should be able to enter the relevent product", "User doesn't enter the relevent product",
					"fail");
		}

		click(btn_searchProductProductLookup);
		explicitWaitUntillClickable(td_resultProductRegressionPOProductReplace.replace("product_code",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")), 60);
		if (isEnabled(td_resultProductRegressionPOProductReplace.replace("product_code",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")))) {
			writeTestResults("Verify that relevent product is available", "Relevent product should be available",
					"Relevent product successfully available", "pass");
		} else {
			writeTestResults("Verify that relevent product is available", "Relevent product should be available",
					"Relevent product not available", "fail");
		}

		doubleClick(td_resultProductRegressionPOProductReplace.replace("product_code",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")));
		Thread.sleep(2000);

		String selectedProduct = getAttribute(txt_outputProductProductInformationSectionSummaryTabPO, "value");
		if (selectedProduct.contains(invObj.readTestCreation("BatchFifoMultiPlatformProductionType"))) {
			writeTestResults("Verify that user able to select the relevent product",
					"User should be able to select the relevent product",
					"User successfully select the relevent product", "pass");
		} else {
			writeTestResults("Verify that user able to select the relevent product",
					"User should be able to select the relevent product", "User couldn't select the relevent product",
					"fail");
		}
	}

	public void productionOrder_2_PROD_E2E_003() throws Exception {
		click(btn_productionModelLookupPO);
		explicitWait(lookup_productionModel, 30);
		if (isDisplayed(lookup_productionModel)) {
			writeTestResults("Verify that Production Model lookup is open", "Production Model lookup should be open",
					"Production Model lookup successfully open", "pass");
		} else {
			writeTestResults("Verify that Production Model lookup is open", "Production Model lookup should be open",
					"Production Model lookup doesn't open", "fail");
		}

		sendKeys(txt_searchProductionModel, readProductionData("ProductionModel_PROD_E2E_003"));
		String enteredProductionModel = getAttribute(txt_searchProductionModel, "value");
		if (enteredProductionModel.equals(readProductionData("ProductionModel_PROD_E2E_003"))) {
			writeTestResults("Verify that user able to enter the relevent Production Model",
					"User should be able to enter the relevent Production Model",
					"User successfully enter the relevent Production Model", "pass");
		} else {
			writeTestResults("Verify that user able to enter the relevent Production Model",
					"User should be able to enter the relevent Production Model",
					"User doesn't enter the relevent Production Model", "fail");
		}

		click(btn_searchProductionModel);
		explicitWaitUntillClickable(td_resultProductionModelDocReplace.replace("doc_no",
				readProductionData("ProductionModel_PROD_E2E_003")), 60);
		if (isEnabled(td_resultProductionModelDocReplace.replace("doc_no",
				readProductionData("ProductionModel_PROD_E2E_003")))) {
			writeTestResults("Verify that relevent Production Model is available",
					"Relevent Production Model should be available", "Relevent Production Model successfully available",
					"pass");
		} else {
			writeTestResults("Verify that relevent Production Model is available",
					"Relevent Production Model should be available", "Relevent Production Model not available", "fail");
		}

		doubleClick(td_resultProductionModelDocReplace.replace("doc_no",
				readProductionData("ProductionModel_PROD_E2E_003")));
		Thread.sleep(2000);

		String selectedProducttionModel = getAttribute(txt_productionModelSummarySectionSummaryTabPO, "value");
		if (selectedProducttionModel.equals(readProductionData("ProductionModel_PROD_E2E_003"))) {
			writeTestResults("Verify that user able to select the relevent Production Model",
					"User should be able to select the relevent Production Model",
					"User successfully select the relevent Production Model", "pass");
		} else {
			writeTestResults("Verify that user able to select the relevent Production Model",
					"User should be able to select the relevent Production Model",
					"User couldn't select the relevent Production Model", "fail");
		}

		String loadedProductionUnit = getAttribute(txt_productionUnitSummarySectionSummaryTabPO, "value");
		if (loadedProductionUnit.contains(readProductionData("Location_PROD_E2E"))) {
			writeTestResults(
					"Verify that Production Unit auto loaded according to the Production Model in summary section",
					"Production Unit should be auto loaded according to the Production Model in summary section",
					"Production Unit successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Production Unit auto loaded according to the Production Model in summary section",
					"Production Unit should be auto loaded according to the Production Model in summary section",
					"Production Unit doesn't auto loaded according to the Production Model in summary section", "fail");
		}

		String loadedInputWare = getSelectedOptionInDropdown(drop_inputWarehouseSummarySectionSummaryTabPO);
		if (loadedInputWare.equals(inputWarehouseLI)) {
			writeTestResults(
					"Verify that Input Warehouse auto loaded according to the Production Model in summary section",
					"Input Warehouse should be auto loaded according to the Production Model in summary section",
					"Input Warehouse successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Input Warehouse auto loaded according to the Production Model in summary section",
					"Input Warehouse should be auto loaded according to the Production Model in summary section",
					"Input Warehouse doesn't auto loaded according to the Production Model in summary section", "fail");
		}

		String loadedOutputWare = getSelectedOptionInDropdown(drop_outputWarehouseSummarySectionSummaryTabPO);
		if (loadedOutputWare.equals(outputWarehouseLI)) {
			writeTestResults(
					"Verify that Output Warehouse auto loaded according to the Production Model in summary section",
					"Output Warehouse should be auto loaded according to the Production Model in summary section",
					"Output Warehouse successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that Output Warehouse auto loaded according to the Production Model in summary section",
					"Output Warehouse should be auto loaded according to the Production Model in summary section",
					"Output Warehouse doesn't auto loaded according to the Production Model in summary section",
					"fail");
		}

		String loadedWIPWare = getSelectedOptionInDropdown(drop_WIPWarehouseSummarySectionSummaryTabPO);
		if (loadedWIPWare.equals(wipWarehouseLI)) {
			writeTestResults(
					"Verify that WIP Warehouse auto loaded according to the Production Model in summary section",
					"WIP Warehouse should be auto loaded according to the Production Model in summary section",
					"WIP Warehouse successfully auto loaded according to the Production Model in summary section",
					"pass");
		} else {
			writeTestResults(
					"Verify that WIP Warehouse auto loaded according to the Production Model in summary section",
					"WIP Warehouse should be auto loaded according to the Production Model in summary section",
					"WIP Warehouse doesn't auto loaded according to the Production Model in summary section", "fail");
		}

		String loadedBatchQty = getAttribute(txt_batchQtyProductInformationSectionSummaryTabPO, "value");
		if (loadedBatchQty.equals("100.00")) {
			writeTestResults(
					"Verify that Batch Qty auto loaded according to the Production Model in product information section (100 Qty)",
					"Batch Qty should be auto loaded according to the Production Model in product information section (100 Qty)",
					"Batch Qty successfully auto loaded according to the Production Model in product information section (100 Qty)",
					"pass");
		} else {
			writeTestResults(
					"Verify that Batch Qty auto loaded according to the Production Model in product information section (100 Qty)",
					"Batch Qty should be auto loaded according to the Production Model in product information section (100 Qty)",
					"Batch Qty doesn't auto loaded according to the Production Model in product information section (100 Qty)",
					"fail");
		}

		String loadedBoM = getText(drop_boMBoMDetailsSectionSummaryTabPO);
		if (loadedBoM.equals(readProductionData("BOM_PROD_E2E_003"))) {
			writeTestResults(
					"Verify that BoM Document Number auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number should be auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number successfully auto loaded according to the Production Model in bill of material details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoM Document Number auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number should be auto loaded according to the Production Model in bill of material details section",
					"BoM Document Number doesn't auto loaded according to the Production Model in bill of material details section",
					"fail");
		}

		String loadedBoO = getText(div_boOBoODetailsSectionSummaryTabPO);
		if (loadedBoO.equals(readProductionData("BOO_PROD_E2E_003"))) {
			writeTestResults(
					"Verify that BoO Number auto loaded according to the Production Model in bill of operation details section",
					"BoO Number should be auto loaded according to the Production Model in bill of operation details section",
					"BoO Number successfully auto loaded according to the Production Model in bill of operation details section",
					"pass");
		} else {
			writeTestResults(
					"Verify that BoO Number auto loaded according to the Production Model in bill of operation details section",
					"BoO Number should be auto loaded according to the Production Model in bill of operation details section",
					"BoO Number doesn't auto loaded according to the Production Model in bill of operation details section",
					"fail");
		}

		driver.findElement(getLocator(txt_planedQtyProductInformationSectionSummaryTabPO)).click();
		driver.findElement(getLocator(txt_planedQtyProductInformationSectionSummaryTabPO)).sendKeys(planedQtyE2E);
		String enteredPlanedQty = getAttribute(txt_planedQtyProductInformationSectionSummaryTabPO, "value");
		if (enteredPlanedQty.equals(planedQtyE2E)) {
			writeTestResults("Verify that entered Planed Qty is display successfully",
					"Entered Planed Qty should be display successfully", "Entered Planed Qty display successfully",
					"pass");
		} else {
			writeTestResults("Verify that entered Planed Qty is display successfully",
					"Entered Planed Qty should be display successfully", "Entered Planed Qty not display", "fail");
		}

		if (isSelected(chk_mrpProductionOrderRegression)) {
			writeTestResults("Verify that MRP auto enabled by the Production Model",
					"MRP should  auto enabled by the Production Model",
					"MRP successfully auto enabled by the Production Model", "pass");
		} else {
			writeTestResults("Verify that MRP auto enabled by the Production Model",
					"MRP should  auto enabled by the Production Model",
					"MRP wasn't auto enabled by the Production Model", "fail");
		}

		selectText(drop_productOrderGroupSummarySectionSummaryTabPO, productionOrderGroupRegression);
		Thread.sleep(1000);
		String selectedProductionGroup = getSelectedOptionInDropdown(drop_productOrderGroupSummarySectionSummaryTabPO);
		if (selectedProductionGroup.equals(productionOrderGroupRegression)) {
			writeTestResults("Verify user able to select the Production Group",
					"User should be able to select the Production Group",
					"User successfully select the Production Group", "pass");
		} else {
			writeTestResults("Verify user able to select the Production Group",
					"User should be able to select the Production Group", "User couldn't select the Production Group",
					"fail");
		}

		sendKeys(txt_descriptionSummarySectionSummaryTabPO, productionOrderDescriptionRegression);
		String enteredDescription = getAttribute(txt_descriptionSummarySectionSummaryTabPO, "value");
		if (enteredDescription.equals(productionOrderDescriptionRegression)) {
			writeTestResults("Verify user able to enter the Description",
					"User should be able to enter the Description", "User successfully enter the Description", "pass");
		} else {
			writeTestResults("Verify user able to enter the Description",
					"User should be able to enter the Description", "User couldn't enter the Description", "fail");
		}

		click(txt_requestStartDateSheduleSectionSummaryTabPO);
		explicitWaitUntillClickable(a_requestStartDatePOWhenDuplicating, 6);
		click(a_requestStartDatePOWhenDuplicating);
		Thread.sleep(2000);
		String selectedRequestedStartDate = getAttribute(txt_requestStartDateSheduleSectionSummaryTabPO, "value");
		String currentYearMonthWithselectedDate = common.currentDate().substring(0, 8) + "01";
		if (selectedRequestedStartDate.equals(currentYearMonthWithselectedDate)) {
			writeTestResults("Verify user able to select Requested Start Date",
					"User should be able to select Requested Start Date",
					"User successfully select Requested Start Date", "pass");
		} else {
			writeTestResults("Verify user able to select Requested Start Date",
					"User should be able to select Requested Start Date", "User couldn't select Requested Start Date",
					"fail");
		}

		click(txt_requestEndDateSheduleSectionSummaryTabPO);
		explicitWaitUntillClickable(a_requestEndDatePOWhenDuplicating, 6);
		click(a_requestEndDatePOWhenDuplicating);
		Thread.sleep(2000);
		String selectedRequestedEndDate = getAttribute(txt_requestEndDateSheduleSectionSummaryTabPO, "value");
		String currentYearMonthWithselectedEndDate = common.currentDate().substring(0, 8) + "28";
		if (selectedRequestedEndDate.equals(currentYearMonthWithselectedEndDate)) {
			writeTestResults("Verify user able to select Requested End Date",
					"User should be able to select Requested End Date", "User successfully select Requested End Date",
					"pass");
		} else {
			writeTestResults("Verify user able to select Requested End Date",
					"User should be able to select Requested End Date", "User couldn't select Requested End Date",
					"fail");
		}

		click(btn_draft);
		explicitWait(header_draftedProductionOrder, 60);
		if (isDisplayed(header_draftedProductionOrder)) {
			writeTestResults("Verify user able to draft the Production Order",
					"User should be able to draft the Production Order", "User successfully draft the Production Order",
					"pass");
		} else {
			writeTestResults("Verify user able to draft the Production Order",
					"User should be able to draft the Production Order", "User couldn't draft the Production Order",
					"fail");
		}

		click(btn_release);
		explicitWait(header_releasedProductionOrder, 30);
		trackCode = getText(lbl_docNo);
		String productionOrderCode = trackCode;
		writeProductionData("ProductionOrder_PROD_E2E_003", productionOrderCode, 31);
		writeProductionData("ReleaseedPO_PRD_E2E_003", getCurrentUrl(), 32);
		if (isDisplayed(header_releasedProductionOrder)) {
			writeTestResults("Verify user able to release the Production Order",
					"User should be able to release the Production Order",
					"User successfully release the Production Order", "pass");
		} else {
			writeTestResults("Verify user able to release the Production Order",
					"User should be able to release the Production Order", "User couldn't release the Production Order",
					"fail");
		}
	}

	/* MRP Run PROD_E2E_003 */
	public void mrpRun1_PROD_E2E_003() throws Exception {
		explicitWait(img_navigationPane, 20);
		Thread.sleep(2000);
		explicitWait(img_navigationPane, 20);
		click(img_navigationPane);
		explicitWait(btn_productionModule, 10);
		click(btn_productionModule);
		explicitWait(btn_productionControl, 10);
		click(btn_productionControl);
		explicitWait(header_productionControlByPage, 40);
		if (isDisplayed(header_productionControlByPage)) {
			writeTestResults("Verify user can navigate to the Production Control by-page",
					"User should be able to navigate to Production Control by-page",
					"User successfully navigate to Production Control by-page", "pass");
		} else {
			writeTestResults("Verify user can navigate to the Production Control by-page",
					"User should be able to navigate to Production Control by-page",
					"User doesn't navigate to Production Control by-page", "fail");

		}

		sendKeys(txt_searchProductionControlOnByPage, readProductionData("ProductionOrder_PROD_E2E_003"));
		click(btn_searchProductionControlByPage);

		String chkMRPPath = chk_mrpPODoRepalce.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_003"));
		explicitWait(chkMRPPath, 10);
		if (isDisplayed(chkMRPPath)) {
			writeTestResults("Verify relevent Production Order is available",
					"Relevent Production Order should be available", "Relevent Production Order is available", "pass");
		} else {
			writeTestResults("Verify relevent Production Order is available",
					"Relevent Production Order should be available", "Relevent Production Order doesn't available",
					"fail");

		}
		click(chkMRPPath);

		click(btn_actionMRP);
		explicitWait(btn_runMRP, 10);
		if (isDisplayed(btn_runMRP)) {
			writeTestResults("Verify that Run MRP action is availabe under Actions",
					"Run MRP action should be availabe under Actions", "Run MRP action doesn't availabe under Actions",
					"pass");
		} else {
			writeTestResults("Verify that Run MRP action is availabe under Actions",
					"Run MRP action should be availabe under Actions", "Run MRP action doesn't availabe under Actions",
					"fail");

		}
		click(btn_runMRP);

		explicitWait(header_productionControlWindow, 10);
		if (isDisplayed(header_productionControlWindow)) {
			writeTestResults("Verify that Production Control window is open",
					"Production Control window should be open", "Production Control window suceessfully open", "pass");
		} else {
			writeTestResults("Verify that Production Control window is open",
					"Production Control window should be open", "Production Control window doesn't open", "fail");

		}

		click(btn_runMRPWindow);

		explicitWait(btn_releaseMRP, 10);
		if (isEnabled(btn_releaseMRP)) {
			writeTestResults("Verify that Release button is available", "Release button should be available",
					"Release button successfully available", "pass");
		} else {
			writeTestResults("Verify that Release button is available", "Release button should be available",
					"Release button doesn't available", "fail");

		}

		click(btn_releaseMRP);

		explicitWait(para_MRPReleaseValidation, 10);
		if (isEnabled(para_MRPReleaseValidation)) {
			writeTestResults("Verify that \"Successfully Released.\" validation is available",
					"\"Successfully Released.\" validation should be available",
					"\"Successfully Released.\" validation successfully available", "pass");
		} else {
			writeTestResults("Verify that \"Successfully Released.\" validation is available",
					"\"Successfully Released.\" validation should be available",
					"\"Successfully Released.\" validation doesn't available", "fail");

		}

		Thread.sleep(3000);
		explicitWait(btn_logTabProductionControlWindow, 10);
		click(btn_logTabProductionControlWindow);

		explicitWaitUntillClickable(lbl_genaratedInternalOrderMRP, 10);
		String genaratedIO = getText(lbl_genaratedInternalOrderMRP);

		click(btn_cloaseMRPWindow);

		explicitWait(btn_homeLink, 10);
		click(btn_homeLink);
		explicitWait(btn_taskEvent2, 10);
		click(btn_taskEvent2);
		explicitWait(btn_internalDispatchOrderTaskAndEvent, 10);
		Thread.sleep(1500);
		click(btn_internalDispatchOrderTaskAndEvent);
		String extractedIO = genaratedIO.substring(28, (genaratedIO.length() - 1));
		explicitWaitUntillClickable(btn_pendingIDODocRepalce.replace("doc_no", extractedIO), 10);
		Thread.sleep(2000);
		click(btn_pendingIDODocRepalce.replace("doc_no", extractedIO));
		Thread.sleep(2000);
		switchWindow();
		Thread.sleep(1000);
		explicitWait(header_newIDO, 50);
		if (isDisplayed(header_newIDO)) {
			writeTestResults("Verify user can navigate to the new Internal Dispatch Order",
					"User should be able to navigate to new Internal Dispatch Order",
					"User successfully navigate to new Internal Dispatch Order", "pass");
		} else {
			writeTestResults("Verify user can navigate to the new Internal Dispatch Order",
					"User should be able to navigate to new Internal Dispatch Order",
					"User doesn't navigate to new Internal Dispatch Order", "fail");
		}

		sendKeys(txt_shippingAddressIDO, shippingAddress);

		click(btn_draft);
		explicitWait(header_draftedIDO, 50);
		if (isDisplayed(header_draftedIDO)) {
			writeTestResults("Verify user able to draft the Internal Dispatch Order",
					"User should be able to draft the Internal Dispatch Order",
					"User successfully draft the Internal Dispatch Order", "pass");
		} else {
			writeTestResults("Verify user able to draft the Internal Dispatch Order",
					"User should be able to draft the Internal Dispatch Order",
					"User doesn't draft the Internal Dispatch Order", "fail");

		}

		click(btn_release);

		explicitWait(header_releasedIDO, 60);

		if (isDisplayedQuickCheck(header_releasedIDO)) {
			trackCode = getText(lbl_docNumber);
			writeTestResults("Verify user able to release the Internal Dispatch Order",
					"User should be able to release the Internal Dispatch Order",
					"User successfully release the Internal Dispatch Order", "pass");
		} else {
			changeCSS(btn_closeMainValidation, "z-index:-9999");
			click(btn_firstRownErrorOnGrid);
			Thread.sleep(1500);
			click(btn_firstRownErrorOnGrid);
			explicitWait(div_quantityNotAvaialableValidation, 5);
			if (isDisplayed(div_quantityNotAvaialableValidation)) {
				Thread.sleep(1500);
				((JavascriptExecutor) driver).executeScript("window.open('about:blank','_blank');");
				Thread.sleep(1000);
				switchWindow();
				openPage(siteUrlMultiPlatformm);
				purchaseOrderToInboundShipmentToPurchaseInvoice();
				pageRefersh();
				explicitWaitUntillClickable(btn_release, 40);
				click(btn_release);

				explicitWait(header_releasedIDO, 50);
				trackCode = getText(lbl_docNumber);
				if (isDisplayed(header_releasedIDO)) {
					writeTestResults("Verify user able to release the Internal Dispatch Order",
							"User should be able to release the Internal Dispatch Order",
							"User successfully release the Internal Dispatch Order", "pass");
				} else {
					writeTestResults("Verify user able to release the Internal Dispatch Order",
							"User should be able to release the Internal Dispatch Order",
							"User doesn't release the Internal Dispatch Order", "fail");
				}
			} else {
				writeTestResults("Verify user able to release the Internal Dispatch Order",
						"User should be able to release the Internal Dispatch Order",
						"User doesn't release the Internal Dispatch Order", "fail");
			}

		}
	}

	public void mrpRun2_PROD_E2E_003() throws Exception {
		openPage(readProductionData("ReleaseedPO_PRD_E2E_003"));
		explicitWaitUntillClickable(tab_costingSummaryPO, 60);
		click(tab_costingSummaryPO);
		explicitWaitUntillClickable(btn_expandCostingSummary, 20);

		click(btn_expandCostingSummary);
		Thread.sleep(500);
		explicitWaitUntillClickable(btn_expandCostingSummary, 20);
		click(btn_expandCostingSummary);
		explicitWaitUntillClickable(btn_expandCostingSummary, 20);
		click(btn_expandCostingSummary);
		explicitWaitUntillClickable(btn_expandCostingSummary, 20);
		click(btn_expandCostingSummary);

		if (isDisplayed(column_headerAllocatedCostingSummary)) {
			writeTestResults("Verify that the Estimated Column is display same as earlier",
					"The Estimated Column should display same as earlier",
					"The Estimated Column successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that the Estimated Column is display same as earlier",
					"The Estimated Column should display same as earlier",
					"The Estimated Column doesn't display same as earlier", "fail");
		}

		if (isDisplayed(td_mainResourseHeaderCostCostingSummary)) {
			writeTestResults("Verify that the Resource Total Row is display same as earlier",
					"The Resource Total Row should display same as earlier",
					"The Resource Total Row successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that the Resource Total Row is display same as earlier",
					"The Resource Total Row should display same as earlier",
					"The Resource Total Row doesn't display same as earlier", "fail");
		}

		if (isDisplayed(td_resourceCostAllocatedCostingSummary)) {
			writeTestResults("Verify that the Resource Total Cost is display correctly",
					"The Resource Total Cost should display correctly",
					"The Resource Total Cost successfully display correctly", "pass");
		} else {
			writeTestResults("Verify that the Resource Total Cost is display correctly",
					"The Resource Total Cost should display correctly",
					"The Resource Total Cost doesn't display correctly", "fail");
		}

		if (isDisplayed(td_resurce1CostingSummary.replace("changing_part", readProductionData("ResourceCode01_E2E")))) {
			writeTestResults("Verify that the Resource 01 Row is display same as earlier",
					"The Resource 01 Row should display same as earlier",
					"The Resource 01 Row successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that the Resource 01 Row is display same as earlier",
					"The Resource 01 Row should display same as earlier",
					"The Resource 01 Row doesn't display same as earlier", "fail");
		}

		if (isDisplayed(td_resourceCost1AllocatedCostingSummary)) {
			writeTestResults("Verify that the Cost of Resource 01 is display correctly",
					"The Cost of Resource 01 should display correctly",
					"The Cost of Resource 01 successfully display correctly", "pass");
		} else {
			writeTestResults("Verify that the Cost of Resource 01 is display correctly",
					"The Cost of Resource 01 should display correctly",
					"The Cost of Resource 01 doesn't display correctly", "fail");
		}

		if (isDisplayed(td_resurce2CostingSummary.replace("changing_part", readProductionData("ResourceCode02_E2E")))) {
			writeTestResults("Verify that the Resource 02 Row is display same as earlier",
					"The Resource 02 Row should display same as earlier",
					"The Resource 02 Row successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that the Resource 02 Row is display same as earlier",
					"The Resource 02 Row should display same as earlier",
					"The Resource 02 Row doesn't display same as earlier", "fail");
		}

		if (isDisplayed(td_resourceCost2AllocatedCostingSummary)) {
			writeTestResults("Verify that the Cost of Resource 02 is display correctly",
					"The Cost of Resource 02 should display correctly",
					"The Cost of Resource 02 successfully display correctly", "pass");
		} else {
			writeTestResults("Verify that the Cost of Resource 02 is display correctly",
					"The Cost of Resource 02 should display correctly",
					"The Cost of Resource 02 doesn't display correctly", "fail");
		}

		if (isDisplayedQuickCheck(td_materialCostAllocatedCostingSummary)) {
			if (isDisplayed(td_mainMaterialsHeaderCostCostingSummary)) {
				writeTestResults("Verify that the Material Total Row is display same as earlier",
						"The Material Total Row should display same as earlier",
						"The Material Total Row successfully display same as earlier", "pass");
			} else {
				writeTestResults("Verify that the Material Total Row is display same as earlier",
						"The Material Total Row should display same as earlier",
						"The Material Total Row doesn't display same as earlier", "fail");
			}

			if (isDisplayed(td_materialCostAllocatedCostingSummary)) {
				writeTestResults("Verify that the Material Total Cost is display correctly",
						"The Material Total Cost should display correctly",
						"The Material Total Cost successfully display correctly", "pass");
			} else {
				writeTestResults("Verify that the Material Total Cost is display correctly",
						"The Material Total Cost should display correctly",
						"The Material Total Cost doesn't display correctly", "fail");
			}

			if (isDisplayed(td_material1CostingSummary.replace("changing_part",
					invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialOne")))) {
				writeTestResults("Verify that the Material 01 Row is display same as earlier",
						"The Material 01 Row should display same as earlier",
						"The Material 01 Row successfully display same as earlier", "pass");
			} else {
				writeTestResults("Verify that the Material 01 Row is display same as earlier",
						"The Material 01 Row should display same as earlier",
						"The Material 01 Row doesn't display same as earlier", "fail");
			}

			if (isDisplayed(td_material1CostAllocatedCostingSummary)) {
				writeTestResults("Verify that the Cost of Material 01 is display correctly",
						"The Cost of Material 01 should display correctly",
						"The Cost of Material 01 successfully display correctly", "pass");
			} else {
				writeTestResults("Verify that the Cost of Material 01 is display correctly",
						"The Cost of Material 01 should display correctly",
						"The Cost of Material 01 doesn't display correctly", "fail");
			}

			if (isDisplayed(td_material2CostingSummary.replace("changing_part",
					invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialTwo")))) {
				writeTestResults("Verify that the Material 02 Row is display same as earlier",
						"The Material 02 Row should display same as earlier",
						"The Material 02 Row successfully display same as earlier", "pass");
			} else {
				writeTestResults("Verify that the Material 02 Row is display same as earlier",
						"The Material 02 Row should display same as earlier",
						"The Material 02 Row doesn't display same as earlier", "fail");
			}

			if (isDisplayed(td_material2CostAllocatedCostingSummary)) {
				writeTestResults("Verify that the Cost of Material 02 is display correctly",
						"The Cost of Material 02 should display correctly",
						"The Cost of Material 02 successfully display correctly", "pass");
			} else {
				writeTestResults("Verify that the Cost of Material 02 is display correctly",
						"The Cost of Material 02 should display correctly",
						"The Cost of Material 02 doesn't display correctly", "fail");
			}

			if (isDisplayed(td_material3CostingSummary.replace("changing_part",
					invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialHasVarientProductThree")))) {
				writeTestResults("Verify that the Material 03 Row is display same as earlier",
						"The Material 03 Row should display same as earlier",
						"The Material 03 Row successfully display same as earlier", "pass");
			} else {
				writeTestResults("Verify that the Material 03 Row is display same as earlier",
						"The Material 03 Row should display same as earlier",
						"The Material 03 Row doesn't display same as earlier", "fail");
			}

			if (isDisplayed(td_material3CostAllocatedCostingSummary)) {
				writeTestResults("Verify that the Cost of Material 03 is display correctly",
						"The Cost of Material 03 should display correctly",
						"The Cost of Material 03 successfully display correctly", "pass");
			} else {
				writeTestResults("Verify that the Cost of Material 03 is display correctly",
						"The Cost of Material 03 should display correctly",
						"The Cost of Material 03 doesn't display correctly", "fail");
			}

		} else if (rowMaterialCreated) {
			if (isDisplayed(td_mainMaterialsHeaderCostCostingSummary)) {
				writeTestResults("Verify that the Material Total Row is display same as earlier",
						"The Material Total Row should display same as earlier",
						"The Material Total Row successfully display same as earlier", "pass");
			} else {
				writeTestResults("Verify that the Material Total Row is display same as earlier",
						"The Material Total Row should display same as earlier",
						"The Material Total Row doesn't display same as earlier", "fail");
			}

			if (isDisplayed(td_materialCostAllocatedCostingSummaryBeforePurchase)) {
				writeTestResults("Verify that the Material Total Cost is display correctly",
						"The Material Total Cost should display correctly",
						"The Material Total Cost successfully display correctly", "pass");
			} else {
				writeTestResults("Verify that the Material Total Cost is display correctly",
						"The Material Total Cost should display correctly",
						"The Material Total Cost doesn't display correctly", "fail");
			}

			if (isDisplayed(td_material1CostingSummary.replace("changing_part",
					invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialOne")))) {
				writeTestResults("Verify that the Material 01 Row is display same as earlier",
						"The Material 01 Row should display same as earlier",
						"The Material 01 Row successfully display same as earlier", "pass");
			} else {
				writeTestResults("Verify that the Material 01 Row is display same as earlier",
						"The Material 01 Row should display same as earlier",
						"The Material 01 Row doesn't display same as earlier", "fail");
			}

			if (isDisplayed(td_material1CostAllocatedCostingSummaryBeforePurchase)) {
				writeTestResults("Verify that the Cost of Material 01 is display correctly",
						"The Cost of Material 01 should display correctly",
						"The Cost of Material 01 successfully display correctly", "pass");
			} else {
				writeTestResults("Verify that the Cost of Material 01 is display correctly",
						"The Cost of Material 01 should display correctly",
						"The Cost of Material 01 doesn't display correctly", "fail");
			}

			if (isDisplayed(td_material2CostingSummary.replace("changing_part",
					invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialTwo")))) {
				writeTestResults("Verify that the Material 02 Row is display same as earlier",
						"The Material 02 Row should display same as earlier",
						"The Material 02 Row successfully display same as earlier", "pass");
			} else {
				writeTestResults("Verify that the Material 02 Row is display same as earlier",
						"The Material 02 Row should display same as earlier",
						"The Material 02 Row doesn't display same as earlier", "fail");
			}

			if (isDisplayed(td_material2CostAllocatedCostingSummaryBeforePurchase)) {
				writeTestResults("Verify that the Cost of Material 02 is display correctly",
						"The Cost of Material 02 should display correctly",
						"The Cost of Material 02 successfully display correctly", "pass");
			} else {
				writeTestResults("Verify that the Cost of Material 02 is display correctly",
						"The Cost of Material 02 should display correctly",
						"The Cost of Material 02 doesn't display correctly", "fail");
			}

			if (isDisplayed(td_material3CostingSummary.replace("changing_part",
					invObj.readTestCreation("BatchFifoMultiPlatformAllowDecimalMaterialHasVarientProductThree")))) {
				writeTestResults("Verify that the Material 03 Row is display same as earlier",
						"The Material 03 Row should display same as earlier",
						"The Material 03 Row successfully display same as earlier", "pass");
			} else {
				writeTestResults("Verify that the Material 03 Row is display same as earlier",
						"The Material 03 Row should display same as earlier",
						"The Material 03 Row doesn't display same as earlier", "fail");
			}

			if (isDisplayed(td_material3CostAllocatedCostingSummaryBeforePurchase)) {
				writeTestResults("Verify that the Cost of Material 03 is display correctly",
						"The Cost of Material 03 should display correctly",
						"The Cost of Material 03 successfully display correctly", "pass");
			} else {
				writeTestResults("Verify that the Cost of Material 03 is display correctly",
						"The Cost of Material 03 should display correctly",
						"The Cost of Material 03 doesn't display correctly", "fail");
			}
		} else {
			writeTestResults("Verify that the Cost of Materials is display correctly",
					"The Cost of Materials should display correctly", "The Cost of Materials doesn't display correctly",
					"fail");
		}

		if (isDisplayed(td_mainOverheadHeaderCostCostingSummary)) {
			writeTestResults("Verify that the Overhead Total Row is display same as earlier",
					"The Overhead Total Row should display same as earlier",
					"The Overhead Total Row successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that the Overhead Total Row is display same as earlier",
					"The Overhead Total Row should display same as earlier",
					"The Overhead Total Row doesn't display same as earlier", "fail");
		}

		if (isDisplayed(td_overheadCostAllocatedCostingSummary)) {
			writeTestResults("Verify that the Overhead Total Cost is display correctly",
					"The Overhead Total Cost should display correctly",
					"The Overhead Total Cost successfully display correctly", "pass");
		} else {
			writeTestResults("Verify that the Overhead Total Cost is display correctly",
					"The Overhead Total Cost should display correctly",
					"The Overhead Total Cost doesn't display correctly", "fail");
		}

		if (isDisplayed(
				td_overhead1CostingSummary.replace("changing_part1", readProductionData("OverheadOne_PROD_E2E_001"))
						.replace("changing_part2", readProductionData("ResourceCode01_E2E")))) {
			writeTestResults("Verify that the Overhead 01 Row is display same as earlier",
					"The Overhead 01 Row should display same as earlier",
					"The Overhead 01 Row successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that the Overhead 01 Row is display same as earlier",
					"The Overhead 01 Row should display same as earlier",
					"The Overhead 01 Row doesn't display same as earlier", "fail");
		}

		if (isDisplayed(td_overhead1CostAllocatedCostingSummary)) {
			writeTestResults("Verify that the Cost of Overhead 01 is display correctly",
					"The Cost of Overhead 01 should display correctly",
					"The Cost of Overhead 01 successfully display correctly", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead 01 is display correctly",
					"The Cost of Overhead 01 should display correctly",
					"The Cost of Overhead 01 doesn't display correctly", "fail");
		}

		if (isDisplayed(
				td_overhead2CostingSummary.replace("changing_part1", readProductionData("OverheadOne_PROD_E2E_001"))
						.replace("changing_part2", readProductionData("ResourceCode02_E2E")))) {
			writeTestResults("Verify that the Overhead 02 Row is display same as earlier",
					"The Overhead 02 Row should display same as earlier",
					"The Overhead 02 Row successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that the Overhead 02 Row is display same as earlier",
					"The Overhead 02 Row should display same as earlier",
					"The Overhead 02 Row doesn't display same as earlier", "fail");
		}

		if (isDisplayed(td_overhead2CostAllocatedCostingSummary)) {
			writeTestResults("Verify that the Cost of Overhead 02 is display correctly",
					"The Cost of Overhead 02 should display correctly",
					"The Cost of Overhead 02 successfully display correctly", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead 02 is display correctly",
					"The Cost of Overhead 02 should display correctly",
					"The Cost of Overhead 02 doesn't display correctly", "fail");
		}

		if (isDisplayed(
				td_overhead3CostingSummary.replace("changing_part1", readProductionData("OverheadTwo_PROD_E2E_001"))
						.replace("changing_part2", readProductionData("ResourceCode02_E2E")))) {
			writeTestResults("Verify that the Overhead 03 Row is display same as earlier",
					"The Overhead 03 Row should display same as earlier",
					"The Overhead 03 Row successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that the Overhead 03 Row is display same as earlier",
					"The Overhead 03 Row should display same as earlier",
					"The Overhead 03 Row doesn't display same as earlier", "fail");
		}

		if (isDisplayed(td_overhead3CostAllocatedCostingSummary)) {
			writeTestResults("Verify that the Cost of Overhead 03 is display correctly",
					"The Cost of Overhead 03 should display correctly",
					"The Cost of Overhead 03 successfully display correctly", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead 03 is display correctly",
					"The Cost of Overhead 03 should display correctly",
					"The Cost of Overhead 03 doesn't display correctly", "fail");
		}

		if (isDisplayed(td_totalCostingSummary)) {
			writeTestResults("Verify that the Total Row is display same as earlier",
					"The Total Row should display same as earlier",
					"The Total Row successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that the Total Row is display same as earlier",
					"The Total Row should display same as earlier", "The Total Row doesn't display same as earlier",
					"fail");
		}

		if (rowMaterialCreated) {
			if (isDisplayed(td_totalCostAllocatedCostingSummaryBeforePurchase)) {
				writeTestResults("Verify that the Total Cost is display correctly",
						"The Total Cost should display correctly", "The Total Cost successfully display correctly",
						"pass");
			} else {
				writeTestResults("Verify that the Total Cost is display correctly",
						"The Total Cost should display correctly", "The Total Cost doesn't display correctly", "fail");
			}
		} else {
			if (isDisplayed(td_totalCostAllocatedCostingSummary)) {
				writeTestResults("Verify that the Total Cost is display correctly",
						"The Total Cost should display correctly", "The Total Cost successfully display correctly",
						"pass");
			} else {
				writeTestResults("Verify that the Total Cost is display correctly",
						"The Total Cost should display correctly", "The Total Cost doesn't display correctly", "fail");
			}
		}

		if (isDisplayed(td_unitCostHeaderCostingSummary)) {
			writeTestResults("Verify that the Unit Cost Row is display same as earlier",
					"The Unit Cost Row should display same as earlier",
					"The Unit Cost Row successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that the Unit Cost Row is display same as earlier",
					"The Unit Cost Row should display same as earlier",
					"The Unit Cost Row doesn't display same as earlier", "fail");
		}

		if (rowMaterialCreated) {
			if (isDisplayed(td_unitCostAllocatedCostingSummaryBeforePurchase)) {
				writeTestResults("Verify that the Unit Cost is display correctly",
						"The Unit Cost should display correctly", "The Unit Cost successfully display correctly",
						"pass");
			} else {
				writeTestResults("Verify that the Unit Cost is display correctly",
						"The Unit Cost should display correctly", "The Unit Cost doesn't display correctly", "fail");
			}
		} else {
			if (isDisplayed(td_unitCostAllocatedCostingSummary)) {
				writeTestResults("Verify that the Unit Cost is display correctly",
						"The Unit Cost should display correctly", "The Unit Cost successfully display correctly",
						"pass");
			} else {
				writeTestResults("Verify that the Unit Cost is display correctly",
						"The Unit Cost should display correctly", "The Unit Cost doesn't display correctly", "fail");
			}
		}

		if (isDisplayed(td_unitCostHeaderResourseCostingSummary)) {
			writeTestResults("Verify that the Resource (Unit Cost) Row is display same as earlier",
					"The Resource (Unit Cost) Row should display same as earlier",
					"The Resource (Unit Cost) Row successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that the Resource (Unit Cost) Row is display same as earlier",
					"The Resource (Unit Cost) Row should display same as earlier",
					"The Resource (Unit Cost) Row doesn't display same as earlier", "fail");
		}

		if (isDisplayed(td_unitCostResourseAllocatedCostingSummary)) {
			writeTestResults("Verify that the Cost of Resource (Unit Cost) is display correctly",
					"The Cost of Resource (Unit Cost) should display correctly",
					"The Cost of Resource (Unit Cost) successfully display correctly", "pass");
		} else {
			writeTestResults("Verify that the Cost of Resource (Unit Cost) is display correctly",
					"The Cost of Resource (Unit Cost) should display correctly",
					"The Cost of Resource (Unit Cost) doesn't display correctly", "fail");
		}

		if (isDisplayed(td_unitCostHeaderMaterialCostingSummary)) {
			writeTestResults("Verify that the Material (Unit Cost) Row is display same as earlier",
					"The Material (Unit Cost) Row should display same as earlier",
					"The Material (Unit Cost) Row successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that the Material (Unit Cost) Row is display same as earlier",
					"The Material (Unit Cost) Row should display same as earlier",
					"The Material (Unit Cost) Row doesn't display same as earlier", "fail");
		}

		if (rowMaterialCreated) {
			if (isDisplayed(td_unitCostMateraialAllocatedCostingSummaryBeforePurchase)) {
				writeTestResults("Verify that the Cost of Material (Unit Cost) is display correctly",
						"The Cost of Material (Unit Cost) should display correctly",
						"The Cost of Material (Unit Cost) successfully display correctly", "pass");
			} else {
				writeTestResults("Verify that the Cost of Material (Unit Cost) is display correctly",
						"The Cost of Material (Unit Cost) should display correctly",
						"The Cost of Material (Unit Cost) doesn't display correctly", "fail");
			}
		} else {
			if (isDisplayed(td_unitCostMateraialAllocatedCostingSummary)) {
				writeTestResults("Verify that the Cost of Material (Unit Cost) is display correctly",
						"The Cost of Material (Unit Cost) should display correctly",
						"The Cost of Material (Unit Cost) successfully display correctly", "pass");
			} else {
				writeTestResults("Verify that the Cost of Material (Unit Cost) is display correctly",
						"The Cost of Material (Unit Cost) should display correctly",
						"The Cost of Material (Unit Cost) doesn't display correctly", "fail");
			}
		}

		if (isDisplayed(td_unitCostHeaderOverheadCostingSummary)) {
			writeTestResults("Verify that the Overhead (Unit Cost) Row is display same as earlier",
					"The Overhead (Unit Cost) Row should display same as earlier",
					"The Overhead (Unit Cost) Row successfully display same as earlier", "pass");
		} else {
			writeTestResults("Verify that the Overhead (Unit Cost) Row is display same as earlier",
					"The Overhead (Unit Cost) Row should display same as earlier",
					"The Overhead (Unit Cost) Row doesn't display same as earlier", "fail");
		}

		if (isDisplayed(td_unitCostOverheadAllocatedCostingSummary)) {
			writeTestResults("Verify that the Cost of Overhead (Unit Cost) is display correctly",
					"The Cost of Overhead (Unit Cost) should display correctly",
					"The Cost of Overhead (Unit Cost) successfully display correctly", "pass");
		} else {
			writeTestResults("Verify that the Cost of Overhead (Unit Cost) is display correctly",
					"The Cost of Overhead (Unit Cost) should display correctly",
					"The Cost of Overhead (Unit Cost) doesn't display correctly", "fail");
		}

	}

	/* Shop Floor Update PROD_E2E_003 */
	public void shopFloorUpdate_PROD_E2E_003() throws InvalidFormatException, IOException, Exception {
		openPage(readProductionData("ReleaseedPO_PRD_E2E_003"));
		explicitWaitUntillClickable(btn_action2, 50);
		click(btn_action2);

		explicitWaitUntillClickable(btn_shopFloorUpdate2, 10);
		if (isDisplayed(btn_shopFloorUpdate2)) {
			writeTestResults("Verify that Shop floor update option is available under Actions",
					"Shop floor update option should be available under Actions",
					"Shop floor update option successfully available under Actions", "pass");
		} else {
			writeTestResults("Verify that Shop floor update option is available under Actions",
					"Shop floor update option should be available under Actions",
					"Shop floor update option doesn't available under Actions", "fail");
		}
		click(btn_shopFloorUpdate2);

		explicitWait(header_shopFloorUpdateWindow, 20);
		if (isDisplayed(header_shopFloorUpdateWindow)) {
			writeTestResults("Verify that shop floor update window is open", "Shop floor update window should be open",
					"Shop floor update window succeessfully open", "pass");
		} else {
			writeTestResults("Verify that shop floor update window is open", "Shop floor update window should be open",
					"Shop floor update window doesn't open", "fail");
		}

		/* First Operation */
		System.out.println("First Operation");
		click(btn_startFirstOperationSFU);
		explicitWaitUntillClickable(btn_startSecondLeveleSFU, 10);
		if (isDisplayed(btn_startSecondLeveleSFU)) {
			writeTestResults("Verify that start button is available", "Start button should be available",
					"Start button successfully available", "pass");
		} else {
			writeTestResults("Verify that start button is available", "Start button should be available",
					"Start button doesn't available", "fail");
		}

		click(btn_startSecondLeveleSFU);

		explicitWaitUntillClickable(btn_actualUpdateSFU, 10);
		if (isDisplayed(btn_actualUpdateSFU)) {
			writeTestResults("Verify that actual update option is available on shop floor update main window",
					"Actual update option should be available on shop floor update main window",
					"Actual update option successfully available on shop floor update main window", "pass");
		} else {
			writeTestResults("Verify that actual update option is available on shop floor update main window",
					"Actual update option should be available on shop floor update main window",
					"Actual update option doesn't available on shop floor update main window", "fail");
		}

		click(btn_actualUpdateSFU);

		Thread.sleep(2500);
		explicitWaitUntillClickable(txt_outoutQuantityACtualUpdateSFU, 20);
		Thread.sleep(2000);
		sendKeys(txt_outoutQuantityACtualUpdateSFU, actualQuantityOutputProductFirstOperation);
		String outputActualUpdateQuantityFirstOperation = getAttribute(txt_outoutQuantityACtualUpdateSFU, "value");
		if (outputActualUpdateQuantityFirstOperation.equals(actualQuantityOutputProductFirstOperation)) {
			writeTestResults("Verify user able to enter Output Product qty for the first operation",
					"User should be able to enter Output Product qty for the first operation",
					"User successfully enter Output Product qty for the first operation", "pass");
		} else {
			writeTestResults("Verify user able to enter Output Product qty for the first operation",
					"User should be able to enter Output Product qty for the first operation",
					"User doesn't enter Output Product qty for the first operation", "fail");
		}
		Thread.sleep(2000);

		click(btn_upateActualSFU);
		explicitWait(para_successfullValidationOutProductActualUpdate, 8);
		if (isDisplayed(para_successfullValidationOutProductActualUpdate)) {
			writeTestResults("Verify that successfully updated validation is appear for the Output Product update",
					"Successfully updated validation should appear for the Output Product update",
					"Successfully updated validation successfully appear for the Output Product update", "pass");
		} else {
			writeTestResults("Verify that successfully updated validation is appear for the Output Product update",
					"Successfully updated validation should appear for the Output Product update",
					"Successfully updated validation doesn't appear for the Output Product update", "fail");
		}
		Thread.sleep(3000);

		click(btn_tapInputProductsShopFloorUpdate);

		sendKeys(txt_actualUpdaInputProductQuantityFirstOperation, actualQuantityInputProductFirstOperation);
		String inputActualUpdateQuantityFirstOperation = getAttribute(txt_actualUpdaInputProductQuantityFirstOperation,
				"value");
		if (inputActualUpdateQuantityFirstOperation.equals(actualQuantityInputProductFirstOperation)) {
			writeTestResults("Verify user able to enter Input Product qty for the first operation - First Material",
					"User should be able to enter Input Product qty for the first operation - First Material",
					"User successfully enter Input Product qty for the first operation - First Material", "pass");
		} else {
			writeTestResults("Verify user able to enter Input Product qty for the first operation - First Material",
					"User should be able to enter Input Product qty for the first operation - First Material",
					"User doesn't enter Input Product qty for the first operation - First Material", "fail");
		}

		sendKeys(txt_actualUpdaInputProductQuantity2FirstOperation, actualQuantity2InputProductFirstOperation);
		String inputActualUpdateQuantity2FirstOperation = getAttribute(
				txt_actualUpdaInputProductQuantity2FirstOperation, "value");
		if (inputActualUpdateQuantity2FirstOperation.equals(actualQuantity2InputProductFirstOperation)) {
			writeTestResults("Verify user able to enter Input Product qty for the first operation - Second Material",
					"User should be able to enter Input Product qty for the first operation - Second Material",
					"User successfully enter Input Product qty for the first operation - Second Material", "pass");
		} else {
			writeTestResults("Verify user able to enter Input Product qty for the first operation - Second Material",
					"User should be able to enter Input Product qty for the first operation - Second Material",
					"User doesn't enter Input Product qty for the first operation - Second Material", "fail");
		}

		click(btn_upateActualSFU);
		explicitWait(para_successfullValidationActualUpdate, 8);
		if (isDisplayed(para_successfullValidationActualUpdate)) {
			writeTestResults("Verify that successfully updated validation is appear for the Input Product update",
					"Successfully updated validation should appear for the Input Product update",
					"Successfully updated validation successfully appear for the Input Product update", "pass");
		} else {
			writeTestResults("Verify that successfully updated validation is appear for the Input Product update",
					"Successfully updated validation should appear for the Input Product update",
					"Successfully updated validation doesn't appear for the Input Product update", "fail");
		}
		Thread.sleep(3000);

		explicitWaitUntillClickable(tab_resourseShopFloorUpdate, 30);
		click(tab_resourseShopFloorUpdate);
		explicitWaitUntillClickable(div_actualHoursResourseTabRow01, 20);
		Thread.sleep(2000);
		String actualHoursRow1 = getAttribute(div_actualHoursResourseTabRow01, "value");
		String actualminsRow1 = getAttribute(div_actualMinsResourseTabRow01, "value");
		String actualHoursRow2 = getAttribute(div_actualHoursResourseTabRow02, "value");
		String actualminsRow2 = getAttribute(div_actualMinsResourseTabRow02, "value");

		if (actualHoursRow1.equals("01") && actualminsRow1.equals("00") && actualHoursRow2.equals("01")
				&& actualminsRow2.equals("00")) {
			writeTestResults("Verify that actual resource hours are auto loaded same as estimated",
					"Actual resource hours should be auto loaded same as estimated",
					"Actual resource hours successfully auto loaded same as estimated", "pass");
		} else {
			writeTestResults("Verify that actual resource hours are auto loaded same as estimated",
					"Actual resource hours should be auto loaded same as estimated",
					"Actual resource hours weren't auto loaded same as estimated", "fail");
		}

		click(btn_upateActualSFU);
		explicitWait(para_successfullValidationActualUpdateResourse, 8);
		if (isDisplayed(para_successfullValidationActualUpdateResourse)) {
			writeTestResults("Verify that successfully updated validation is appear for the Resource update",
					"Successfully updated validation should appear for the Resource update",
					"Successfully updated validation successfully appear for the Resource update", "pass");
		} else {
			writeTestResults("Verify that successfully updated validation is appear for the Resource update",
					"Successfully updated validation should appear for the Resource update",
					"Successfully updated validation doesn't appear for the Resource update", "fail");
		}

		Thread.sleep(3000);

		click(btn_closeFisrstOperationActualUpdateWindow);

		explicitWait(btn_completeFirstOperation, 8);
		if (isDisplayed(btn_completeFirstOperation)) {
			writeTestResults("Verify that complete button is available for the first operation",
					"Complete button should be available for the first operation",
					"Complete button successfully available for the first operation", "pass");
		} else {
			writeTestResults("Verify that complete button is available for the first operation",
					"Complete button should be available for the first operation",
					"Complete button doesn't available for the first operation", "fail");
		}
		click(btn_completeFirstOperation);

		explicitWait(btn_completeSecondLeveleSFU, 8);
		if (isDisplayed(btn_completeSecondLeveleSFU)) {
			writeTestResults("Verify that complete button is available for the complete window of first operation",
					"Complete button should be available for the complete window of first operation",
					"Complete button successfully available for the complete window of first operation", "pass");
		} else {
			writeTestResults("Verify that complete button is available for the complete window of first operation",
					"Complete button should be available for the complete window of first operation",
					"Complete button doesn't available for the complete window of first operation", "fail");
		}
		click(btn_completeSecondLeveleSFU);

		explicitWait(lbl_completedShopFloorUpdateBarFirstOperation, 8);
		if (isDisplayed(lbl_completedShopFloorUpdateBarFirstOperation)) {
			writeTestResults("Verify that shop flow update is completed for the first operation",
					"Shop flow update should be completed for the first operation",
					"Shop flow update successfully completed for the first operation", "pass");
		} else {
			writeTestResults("Verify that shop flow update is completed for the first operation",
					"Shop flow update should be completed for the first operation",
					"Shop flow update doesn't completed for the first operation", "pass");
		}

		/* Second Operation */
		System.out.println("Second Operation");

		explicitWaitUntillClickable(btn_actualUpdateSFU, 10);
		if (isDisplayed(btn_actualUpdateSFU)) {
			writeTestResults(
					"Verify that actual update option is available on shop floor update main window for second operation",
					"Actual update option should be available on shop floor update main window for second operation",
					"Actual update option successfully available on shop floor update main window for second operation",
					"pass");
		} else {
			writeTestResults(
					"Verify that actual update option is available on shop floor update main window for second operation",
					"Actual update option should be available on shop floor update main window for second operation",
					"Actual update option doesn't available on shop floor update main window for second operation",
					"fail");
		}

		click(btn_actualUpdateSFU);

		explicitWaitUntillClickable(txt_outoutQuantityACtualUpdateSFU, 20);
		Thread.sleep(1500);
		sendKeys(txt_outoutQuantityACtualUpdateSFU, actualQuantityOutputProductSecondOperation);
		String outputActualUpdateQuantitySecondOperation = getAttribute(txt_outoutQuantityACtualUpdateSFU, "value");
		if (outputActualUpdateQuantitySecondOperation.equals(actualQuantityOutputProductSecondOperation)) {
			writeTestResults("Verify user able to enter Output Product qty for the second operation",
					"User should be able to enter Output Product qty for the second operation",
					"User successfully enter Output Product qty for the second operation", "pass");
		} else {
			writeTestResults("Verify user able to enter Output Product qty for the second operation",
					"User should be able to enter Output Product qty for the second operation",
					"User doesn't enter Output Product qty for the second operation", "fail");
		}
		Thread.sleep(1000);

		click(btn_upateActualSFU);
		explicitWait(para_successfullValidationOutProductActualUpdate, 8);
		if (isDisplayed(para_successfullValidationOutProductActualUpdate)) {
			writeTestResults("Verify that successfully updated validation is appear for the Output Product update",
					"Successfully updated validation should appear for the Output Product update",
					"Successfully updated validation successfully appear for the Output Product update", "pass");
		} else {
			writeTestResults("Verify that successfully updated validation is appear for the Output Product update",
					"Successfully updated validation should appear for the Output Product update",
					"Successfully updated validation doesn't appear for the Output Product update", "fail");
		}
		Thread.sleep(3000);

		click(btn_tapInputProductsShopFloorUpdate);

		sendKeys(txt_actualUpdaInputProductQuantitySecondOperation, actualQuantityInputProductSecondOperation);
		String inputActualUpdateQuantitySecondOperation = getAttribute(
				txt_actualUpdaInputProductQuantitySecondOperation, "value");
		if (inputActualUpdateQuantitySecondOperation.equals(actualQuantityInputProductSecondOperation)) {
			writeTestResults("Verify user able to enter Input Product qty for the second operation",
					"User should be able to enter Input Product qty for the second operation",
					"User successfully enter Input Product qty for the second operation", "pass");
		} else {
			writeTestResults("Verify user able to enter Input Product qty for the second operation",
					"User should be able to enter Input Product qty for the second operation",
					"User doesn't enter Input Product qty for the second operation", "fail");
		}

		click(btn_upateActualSFU);
		explicitWait(para_successfullValidationActualUpdate, 8);
		if (isDisplayed(para_successfullValidationActualUpdate)) {
			writeTestResults("Verify that successfully updated validation is appear for the Input Product update",
					"Successfully updated validation should appear for the Input Product update",
					"Successfully updated validation successfully appear for the Input Product update", "pass");
		} else {
			writeTestResults("Verify that successfully updated validation is appear for the Input Product update",
					"Successfully updated validation should appear for the Input Product update",
					"Successfully updated validation doesn't appear for the Input Product update", "fail");
		}
		Thread.sleep(3000);

		click(btn_closeSecondOperationActualUpdateWindow);

		explicitWait(btn_completeSecondOperation, 8);
		if (isDisplayed(btn_completeSecondOperation)) {
			writeTestResults("Verify that complete button is available for the second operation",
					"Complete button should be available for the second operation",
					"Complete button successfully available for the second operation", "pass");
		} else {
			writeTestResults("Verify that complete button is available for the second operation",
					"Complete button should be available for the second operation",
					"Complete button doesn't available for the second operation", "fail");
		}
		click(btn_completeSecondOperation);

		explicitWait(btn_completeSecondLeveleSFU, 8);
		if (isDisplayed(btn_completeSecondLeveleSFU)) {
			writeTestResults("Verify that complete button is available for the complete window of second operation",
					"Complete button should be available for the complete window of second operation",
					"Complete button successfully available for the complete window of second operation", "pass");
		} else {
			writeTestResults("Verify that complete button is available for the complete window of second operation",
					"Complete button should be available for the complete window of second operation",
					"Complete button doesn't available for the complete window of second operation", "fail");
		}
		click(btn_completeSecondLeveleSFU);

		explicitWait(lbl_completedShopFloorUpdateBarSecondOperation, 8);
		if (isDisplayed(lbl_completedShopFloorUpdateBarSecondOperation)) {
			writeTestResults("Verify that shop flow update is completed for the second operation",
					"Shop flow update should be completed for the second operation",
					"Shop flow update successfully completed for the second operation", "pass");
		} else {
			writeTestResults("Verify that shop flow update is completed for the second operation",
					"Shop flow update should be completed for the second operation",
					"Shop flow update doesn't completed for the second operation", "pass");
		}
	}

	/* QC PROD_E2E_003 */
	public void qc_PROD_E2E_003() throws Exception {
		pageRefersh();
		explicitWait(img_navigationPane, 20);
		Thread.sleep(2000);
		explicitWait(img_navigationPane, 20);
		click(img_navigationPane);
		explicitWait(btn_productionModule, 10);
		click(btn_productionModule);
		explicitWait(btn_productionQCAndSorting, 10);
		if (isDisplayed(btn_productionQCAndSorting)) {
			writeTestResults("Verif that Production QC & Sorting option is display under Production Module",
					"Production QC & Sorting option should be display under Production Module",
					"Production QC & Sorting option successfully display under Production Module", "pass");
		} else {
			writeTestResults("Verif that Production QC & Sorting option is display under Production Module",
					"Production QC & Sorting option should be display under Production Module",
					"Production QC & Sorting option doesn't display under Production Module", "fail");
		}

		click(btn_productionQCAndSorting);

		explicitWait(drop_documentTypeProductionQC, 50);
		selectText(drop_documentTypeProductionQC, qcTypeProductionOrder);
		Thread.sleep(1000);
		String selecteDocType = getSelectedOptionInDropdown(drop_documentTypeProductionQC);
		if (selecteDocType.equals(qcTypeProductionOrder)) {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User able to select 'Production Order' as document type", "pass");
		} else {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User not able to select 'Production Order' as document type", "fail");

		}

		sendKeys(txt_docNumberQC, readProductionData("ProductionOrder_PROD_E2E_003"));
		String enteredPO = getAttribute(txt_docNumberQC, "value");
		if (enteredPO.equals(readProductionData("ProductionOrder_PROD_E2E_003"))) {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User successfully enter Production Order document number", "pass");
		} else {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User doesn't enter Production Order document number", "fail");
		}

		Thread.sleep(1000);
		driver.findElement(getLocator(txt_docNumberQC)).sendKeys(Keys.ARROW_DOWN);
		pressEnter(txt_docNumberQC);

		explicitWait(div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_003")),
				30);
		if (isDisplayed(
				div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_003")))) {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User successfully select the relevent Production Order", "pass");
		} else {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User doesn't select the relevent Production Order", "fail");
		}

		Thread.sleep(1500);
		mouseMove(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")));
		Thread.sleep(500);
		click(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")));

		if (isSelected(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")))) {
			writeTestResults("Verify user able to select the relevent Output Product",
					"User should be able to select the relevent Output Product",
					"User successfully select the relevent Output Product", "pass");
		} else {
			writeTestResults("Verify user able to select the relevent Output Product",
					"User should be able to select the relevent Output Product",
					"User doesn't select the relevent Output Product", "fail");
		}

		/* First QC Scenario */
		Thread.sleep(1500);
		sendKeys(txt_failQuantityProductionQCOneRow, firstFailQty);
		String enteredFailQty = getAttribute(txt_failQuantityProductionQCOneRow, "value");
		if (enteredFailQty.equals(firstFailQty)) {
			writeTestResults("Verify user able to enter fail qty (10)", "User should be able to enter fail qty (10)",
					"User successfully enter fail qty (10)", "pass");
		} else {
			writeTestResults("Verify user able to enter fail qty (10)", "User should be able to enter fail qty (10)",
					"User doesn't enter fail qty (10)", "fail");
		}

		click(btn_releseProductionQC);
		explicitWaitUntillPresent(para_successfullValidationProductionQC, 20);

		if (isElementPresent(para_successfullValidationProductionQC)) {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation is appear", "pass");
		} else {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation doesn't appear", "fail");
		}

		Thread.sleep(2000);
		if (isDisplayed(column_headerTotalQtyQC)) {
			writeTestResults("Verify that total qty column header is display as earlier",
					"Total qty column header should be display as earlier",
					"Total qty column header successfully display as earlier", "pass");
		} else {
			writeTestResults("Verify that total qty column header is display as earlier",
					"Total qty column header should be display as earlier",
					"Total qty column header doesn't display as earlier", "fail");
		}

		String totalQuantity = getAttribute(txt_totalQtyQC, "value");
		if (totalQuantity.equals("90.00")) {
			writeTestResults("Verify that total qty is display by deducting fail qty",
					"Total qty should display by deducting fail qty",
					"Total qty successfully display by deducting fail qty", "pass");
		} else {
			writeTestResults("Verify that total qty is display by deducting fail qty",
					"Total qty should display by deducting fail qty", "Total qty doesn't display by deducting fail qty",
					"fail");
		}

		click(tab_historyQC);
		explicitWaitUntillClickable(tab_historySelectedQC, 20);
		if (isDisplayed(tab_historySelectedQC)) {
			writeTestResults("Verify that user able to open history tab", "User should be able to open history tab",
					"User successfully open history tab", "pass");
		} else {
			writeTestResults("Verify that user able to open history tab", "User should be able to open history tab",
					"User couldn't open history tab", "fail");
		}

		explicitWait(drop_documentTypeProductionQC, 50);
		selectText(drop_documentTypeProductionQC, qcTypeProductionOrder);
		Thread.sleep(1000);
		selecteDocType = getSelectedOptionInDropdown(drop_documentTypeProductionQC);
		if (selecteDocType.equals(qcTypeProductionOrder)) {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User able to select 'Production Order' as document type", "pass");
		} else {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User not able to select 'Production Order' as document type", "fail");

		}

		sendKeys(txt_docNumberQC, readProductionData("ProductionOrder_PROD_E2E_003"));
		String enteredPO2 = getAttribute(txt_docNumberQC, "value");
		if (enteredPO2.equals(readProductionData("ProductionOrder_PROD_E2E_003"))) {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User successfully enter Production Order document number", "pass");
		} else {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User doesn't enter Production Order document number", "fail");
		}

		Thread.sleep(1000);
		driver.findElement(getLocator(txt_docNumberQC)).sendKeys(Keys.ARROW_DOWN);
		pressEnter(txt_docNumberQC);

		explicitWait(div_selectedQcDocProductionHistoryTab.replace("doc_no",
				readProductionData("ProductionOrder_PROD_E2E_003")), 30);
		if (isDisplayed(div_selectedQcDocProductionHistoryTab.replace("doc_no",
				readProductionData("ProductionOrder_PROD_E2E_003")))) {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User successfully select the relevent Production Order", "pass");
		} else {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User doesn't select the relevent Production Order", "fail");
		}

		String qcDoc = getText(div_qcDocNo);

		Thread.sleep(2000);
		if (isDisplayed(column_headerFailedQtyHistoryTabQC)) {
			writeTestResults("Verify that fail qty column header is display as earlier",
					"Fail qty column header should be display as earlier",
					"Fail qty column header successfully display as earlier", "pass");
		} else {
			writeTestResults("Verify that fail qty column header is display as earlier",
					"Fail qty column header should be display as earlier",
					"Fail qty column header doesn't display as earlier", "fail");
		}

		String failQuantity = getAttribute(txt_failedQtyQC, "value");
		if (failQuantity.equals("10.00")) {
			writeTestResults("Verify that fail qty is display in history tab", "Fail qty should display in history tab",
					"Fail qty successfully display in history tab", "pass");
		} else {
			writeTestResults("Verify that fail qty is display in history tab", "Fail qty should display in history tab",
					"Fail qty doesn't display in history tab", "fail");
		}

		click(btn_reverseQC);
		explicitWaitUntillClickable(para_successfullValidationProductionQC, 20);

		if (isDisplayed(para_successfullValidationProductionQC)) {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation is appear", "pass");
		} else {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation doesn't appear", "fail");
		}

		waitUntillNotVisible(div_selectedQcDocProduction.replace("doc_no", qcDoc), 15);
		if (!isDisplayedQuickCheck(div_selectedQcDocProduction.replace("doc_no", qcDoc))) {
			writeTestResults("Verify that relevent Production Order is withdraw from history tab",
					"Relevent Production Order should be withdraw from history tab",
					"Relevent Production Order successfully withdraw from history tab", "pass");
		} else {
			writeTestResults("Verify that relevent Production Order is withdraw from history tab",
					"Relevent Production Order should be withdraw from history tab",
					"Relevent Production Order still available on history tab", "fail");
		}

		click(tab_pendingQC);
		explicitWaitUntillClickable(tab_pendingSelectedQC, 20);
		if (isDisplayed(tab_pendingSelectedQC)) {
			writeTestResults("Verify that user able to open pending tab", "User should be able to open pending tab",
					"User successfully open pending tab", "pass");
		} else {
			writeTestResults("Verify that user able to open pending tab", "User should be able to open pending tab",
					"User couldn't open pending tab", "fail");
		}

		explicitWait(drop_documentTypeProductionQC, 50);
		selectText(drop_documentTypeProductionQC, qcTypeProductionOrder);
		Thread.sleep(1000);
		selecteDocType = getSelectedOptionInDropdown(drop_documentTypeProductionQC);
		if (selecteDocType.equals(qcTypeProductionOrder)) {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User able to select 'Production Order' as document type", "pass");
		} else {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User not able to select 'Production Order' as document type", "fail");

		}

		sendKeys(txt_docNumberQC, readProductionData("ProductionOrder_PROD_E2E_003"));
		String enteredPO3 = getAttribute(txt_docNumberQC, "value");
		if (enteredPO3.equals(readProductionData("ProductionOrder_PROD_E2E_003"))) {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User successfully enter Production Order document number", "pass");
		} else {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User doesn't enter Production Order document number", "fail");
		}

		Thread.sleep(1000);
		driver.findElement(getLocator(txt_docNumberQC)).sendKeys(Keys.ARROW_DOWN);
		pressEnter(txt_docNumberQC);

		explicitWait(div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_003")),
				30);
		if (isDisplayed(
				div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_003")))) {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User successfully select the relevent Production Order", "pass");
		} else {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User doesn't select the relevent Production Order", "fail");
		}

		String totalQuantityAfterReverse = getAttribute(txt_totalQtyQC, "value");
		if (totalQuantityAfterReverse.equals("100.00")) {
			writeTestResults("Verify that total qty is display by adding reverse qty",
					"Total qty should display by adding reverse qty",
					"Total qty successfully display by adding reverse qty", "pass");
		} else {
			writeTestResults("Verify that total qty is display by adding reverse qty",
					"Total qty should display by adding reverse qty", "Total qty doesn't display by adding reverse qty",
					"fail");
		}

		/* Second QC Scenario */
		mouseMove(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")));
		click(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")));

		if (isSelected(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")))) {
			writeTestResults("Verify user able to select the relevent Output Product",
					"User should be able to select the relevent Output Product",
					"User successfully select the relevent Output Product", "pass");
		} else {
			writeTestResults("Verify user able to select the relevent Output Product",
					"User should be able to select the relevent Output Product",
					"User doesn't select the relevent Output Product", "fail");
		}

		Thread.sleep(1500);
		sendKeys(txt_failQuantityProductionQCOneRow, secondFailQty);
		String enteredFailQty2 = getAttribute(txt_failQuantityProductionQCOneRow, "value");
		if (enteredFailQty2.equals(secondFailQty)) {
			writeTestResults("Verify user able to enter fail qty (100)", "User should be able to enter fail qty (100)",
					"User successfully enter fail qty (100)", "pass");
		} else {
			writeTestResults("Verify user able to enter fail qty (100)", "User should be able to enter fail qty (100)",
					"User doesn't enter fail qty (100)", "fail");
		}

		click(btn_releseProductionQC);
		explicitWaitUntillPresent(para_successfullValidationProductionQC, 20);

		if (isElementPresent(para_successfullValidationProductionQC)) {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation is appear", "pass");
		} else {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation doesn't appear", "fail");
		}

		Thread.sleep(2000);
		waitUntillNotVisible(
				div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_003")), 15);
		if (!isDisplayedQuickCheck(
				div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_003")))) {
			writeTestResults("Verify that relevent Production Order is withdraw from pending tab",
					"Relevent Production Order should be withdraw from pending tab",
					"Relevent Production Order successfully withdraw from pending tab", "pass");
		} else {
			writeTestResults("Verify that relevent Production Order is withdraw from pending tab",
					"Relevent Production Order should be withdraw from pending tab",
					"Relevent Production Order still available on pending tab", "fail");
		}

		click(tab_historyQC);
		explicitWaitUntillClickable(tab_historySelectedQC, 20);
		if (isDisplayed(tab_historySelectedQC)) {
			writeTestResults("Verify that user able to open history tab", "User should be able to open history tab",
					"User successfully open history tab", "pass");
		} else {
			writeTestResults("Verify that user able to open history tab", "User should be able to open history tab",
					"User couldn't open history tab", "fail");
		}

		explicitWait(drop_documentTypeProductionQC, 50);
		selectText(drop_documentTypeProductionQC, qcTypeProductionOrder);
		Thread.sleep(1000);
		selecteDocType = getSelectedOptionInDropdown(drop_documentTypeProductionQC);
		if (selecteDocType.equals(qcTypeProductionOrder)) {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User able to select 'Production Order' as document type", "pass");
		} else {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User not able to select 'Production Order' as document type", "fail");

		}

		sendKeys(txt_docNumberQC, readProductionData("ProductionOrder_PROD_E2E_003"));
		String enteredPO4 = getAttribute(txt_docNumberQC, "value");
		if (enteredPO4.equals(readProductionData("ProductionOrder_PROD_E2E_003"))) {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User successfully enter Production Order document number", "pass");
		} else {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User doesn't enter Production Order document number", "fail");
		}

		Thread.sleep(1000);
		driver.findElement(getLocator(txt_docNumberQC)).sendKeys(Keys.ARROW_DOWN);
		pressEnter(txt_docNumberQC);

		explicitWait(div_selectedQcDocProductionHistoryTab.replace("doc_no",
				readProductionData("ProductionOrder_PROD_E2E_003")), 30);
		if (isDisplayed(div_selectedQcDocProductionHistoryTab.replace("doc_no",
				readProductionData("ProductionOrder_PROD_E2E_003")))) {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User successfully select the relevent Production Order", "pass");
		} else {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User doesn't select the relevent Production Order", "fail");
		}

		String qcDoc2 = getText(div_qcDocNo);

		Thread.sleep(2000);
		if (isDisplayed(column_headerFailedQtyHistoryTabQC)) {
			writeTestResults("Verify that fail qty column header is display as earlier",
					"Fail qty column header should be display as earlier",
					"Fail qty column header successfully display as earlier", "pass");
		} else {
			writeTestResults("Verify that fail qty column header is display as earlier",
					"Fail qty column header should be display as earlier",
					"Fail qty column header doesn't display as earlier", "fail");
		}

		String failQuantity2 = getAttribute(txt_failedQtyQC, "value");
		if (failQuantity2.equals("100.00")) {
			writeTestResults("Verify that fail qty is display in history tab", "Fail qty should display in history tab",
					"Fail qty successfully display in history tab", "pass");
		} else {
			writeTestResults("Verify that fail qty is display in history tab", "Fail qty should display in history tab",
					"Fail qty doesn't display in history tab", "fail");
		}

		click(btn_reverseQC);
		explicitWaitUntillClickable(para_successfullValidationProductionQC, 20);

		if (isDisplayed(para_successfullValidationProductionQC)) {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation is appear", "pass");
		} else {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation doesn't appear", "fail");
		}

		waitUntillNotVisible(div_selectedQcDocProduction.replace("doc_no", qcDoc2), 15);
		if (!isDisplayedQuickCheck(div_selectedQcDocProduction.replace("doc_no", qcDoc2))) {
			writeTestResults("Verify that relevent Production Order is withdraw from history tab",
					"Relevent Production Order should be withdraw from history tab",
					"Relevent Production Order successfully withdraw from history tab", "pass");
		} else {
			writeTestResults("Verify that relevent Production Order is withdraw from history tab",
					"Relevent Production Order should be withdraw from history tab",
					"Relevent Production Order still available on history tab", "fail");
		}

		click(tab_pendingQC);
		explicitWaitUntillClickable(tab_pendingSelectedQC, 20);
		if (isDisplayed(tab_pendingSelectedQC)) {
			writeTestResults("Verify that user able to open pending tab", "User should be able to open pending tab",
					"User successfully open pending tab", "pass");
		} else {
			writeTestResults("Verify that user able to open pending tab", "User should be able to open pending tab",
					"User couldn't open pending tab", "fail");
		}

		explicitWait(drop_documentTypeProductionQC, 50);
		selectText(drop_documentTypeProductionQC, qcTypeProductionOrder);
		Thread.sleep(1000);
		selecteDocType = getSelectedOptionInDropdown(drop_documentTypeProductionQC);
		if (selecteDocType.equals(qcTypeProductionOrder)) {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User able to select 'Production Order' as document type", "pass");
		} else {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User not able to select 'Production Order' as document type", "fail");

		}

		sendKeys(txt_docNumberQC, readProductionData("ProductionOrder_PROD_E2E_003"));
		String enteredPO5 = getAttribute(txt_docNumberQC, "value");
		if (enteredPO5.equals(readProductionData("ProductionOrder_PROD_E2E_003"))) {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User successfully enter Production Order document number", "pass");
		} else {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User doesn't enter Production Order document number", "fail");
		}

		Thread.sleep(1000);
		driver.findElement(getLocator(txt_docNumberQC)).sendKeys(Keys.ARROW_DOWN);
		pressEnter(txt_docNumberQC);

		explicitWait(div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_003")),
				30);
		if (isDisplayed(
				div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_003")))) {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User successfully select the relevent Production Order", "pass");
		} else {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User doesn't select the relevent Production Order", "fail");
		}

		String totalQuantityAfterReverse2 = getAttribute(txt_totalQtyQC, "value");
		if (totalQuantityAfterReverse2.equals("100.00")) {
			writeTestResults("Verify that total qty is display after reverse the toatal qty",
					"Total qty should display after reverse the toatal qty",
					"Total qty successfully display after reverse the toatal qty", "pass");
		} else {
			writeTestResults("Verify that total qty is display after reverse the toatal qty",
					"Total qty should display after reverse the toatal qty",
					"Total qty doesn't display after reverse the toatal qty", "fail");
		}

		/* Third QC Scenario */
		mouseMove(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")));
		click(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")));

		if (isSelected(chk_qcProductionAccordingToProductReplaceProduct.replace("product",
				invObj.readTestCreation("BatchFifoMultiPlatformProductionType")))) {
			writeTestResults("Verify user able to select the relevent Output Product",
					"User should be able to select the relevent Output Product",
					"User successfully select the relevent Output Product", "pass");
		} else {
			writeTestResults("Verify user able to select the relevent Output Product",
					"User should be able to select the relevent Output Product",
					"User doesn't select the relevent Output Product", "fail");
		}

		Thread.sleep(1500);
		sendKeys(txt_passQuantityProductionQCOneRow, thirdPassQty);
		String enteredPassQty = getAttribute(txt_passQuantityProductionQCOneRow, "value");
		if (enteredPassQty.equals(thirdPassQty)) {
			writeTestResults("Verify user able to enter pass qty (70)", "User should be able to enter pass qty (70)",
					"User successfully enter pass qty (70)", "pass");
		} else {
			writeTestResults("Verify user able to enter pass qty (70)", "User should be able to enter pass qty (70)",
					"User doesn't enter pass qty (70)", "fail");
		}
		Thread.sleep(1000);

		sendKeys(txt_failQuantityProductionQCOneRow, thirdFailQty);
		String enteredFailQty3 = getAttribute(txt_failQuantityProductionQCOneRow, "value");
		if (enteredFailQty3.equals(thirdFailQty)) {
			writeTestResults("Verify user able to enter fail qty (30)", "User should be able to enter fail qty (30)",
					"User successfully enter fail qty (30)", "pass");
		} else {
			writeTestResults("Verify user able to enter fail qty (30)", "User should be able to enter fail qty (30)",
					"User doesn't enter fail qty (30)", "fail");
		}

		click(btn_releseProductionQC);
		explicitWaitUntillPresent(para_successfullValidationProductionQC, 20);

		if (isElementPresent(para_successfullValidationProductionQC)) {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation is appear", "pass");
		} else {
			writeTestResults("Verify that successfull validation is appear", "Successfull validation should appear",
					"Successfull validation doesn't appear", "fail");
		}

		Thread.sleep(2000);
		waitUntillNotVisible(
				div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_003")), 15);
		if (!isDisplayedQuickCheck(
				div_selectedQcDocProduction.replace("doc_no", readProductionData("ProductionOrder_PROD_E2E_003")))) {
			writeTestResults("Verify that relevent Production Order is withdraw from pending tab",
					"Relevent Production Order should be withdraw from pending tab",
					"Relevent Production Order successfully withdraw from pending tab", "pass");
		} else {
			writeTestResults("Verify that relevent Production Order is withdraw from pending tab",
					"Relevent Production Order should be withdraw from pending tab",
					"Relevent Production Order still available on pending tab", "fail");
		}

		click(tab_historyQC);
		explicitWaitUntillClickable(tab_historySelectedQC, 20);
		if (isDisplayed(tab_historySelectedQC)) {
			writeTestResults("Verify that user able to open history tab", "User should be able to open history tab",
					"User successfully open history tab", "pass");
		} else {
			writeTestResults("Verify that user able to open history tab", "User should be able to open history tab",
					"User couldn't open history tab", "fail");
		}

		explicitWait(drop_documentTypeProductionQC, 50);
		selectText(drop_documentTypeProductionQC, qcTypeProductionOrder);
		Thread.sleep(1000);
		selecteDocType = getSelectedOptionInDropdown(drop_documentTypeProductionQC);
		if (selecteDocType.equals(qcTypeProductionOrder)) {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User able to select 'Production Order' as document type", "pass");
		} else {
			writeTestResults("Verify user able to select 'Production Order' as document type",
					"User should be able to select 'Production Order' as document type",
					"User not able to select 'Production Order' as document type", "fail");

		}

		sendKeys(txt_docNumberQC, readProductionData("ProductionOrder_PROD_E2E_003"));
		String enteredPO6 = getAttribute(txt_docNumberQC, "value");
		if (enteredPO6.equals(readProductionData("ProductionOrder_PROD_E2E_003"))) {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User successfully enter Production Order document number", "pass");
		} else {
			writeTestResults("Verify user able to enter Production Order document number",
					"User should be able to enter Production Order document number",
					"User doesn't enter Production Order document number", "fail");
		}

		Thread.sleep(1000);
		driver.findElement(getLocator(txt_docNumberQC)).sendKeys(Keys.ARROW_DOWN);
		pressEnter(txt_docNumberQC);

		explicitWait(div_selectedQcDocProductionHistoryTab.replace("doc_no",
				readProductionData("ProductionOrder_PROD_E2E_003")), 30);
		if (isDisplayed(div_selectedQcDocProductionHistoryTab.replace("doc_no",
				readProductionData("ProductionOrder_PROD_E2E_003")))) {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User successfully select the relevent Production Order", "pass");
		} else {
			writeTestResults("Verify user able to select the relevent Production Order",
					"User should be able to select the relevent Production Order",
					"User doesn't select the relevent Production Order", "fail");
		}

		Thread.sleep(2000);
		if (isDisplayed(column_headerPassedQtyHistoryTabQC)) {
			writeTestResults("Verify that pass qty column header is display as earlier",
					"Pass qty column header should be display as earlier",
					"Pass qty column header successfully display as earlier", "pass");
		} else {
			writeTestResults("Verify that pass qty column header is display as earlier",
					"Pass qty column header should be display as earlier",
					"Pass qty column header doesn't display as earlier", "fail");
		}

		String passQuantity = getAttribute(txt_passedQtyQC, "value");
		if (passQuantity.equals("70.00")) {
			writeTestResults("Verify that pass qty is display in history tab", "Pass qty should display in history tab",
					"Pass qty successfully display in history tab", "pass");
		} else {
			writeTestResults("Verify that pass qty is display in history tab", "Pass qty should display in history tab",
					"Pass qty doesn't display in history tab", "fail");
		}

		Thread.sleep(1000);
		if (isDisplayed(column_headerFailedQtyHistoryTabQC)) {
			writeTestResults("Verify that fail qty column header is display as earlier",
					"Fail qty column header should be display as earlier",
					"Fail qty column header successfully display as earlier", "pass");
		} else {
			writeTestResults("Verify that fail qty column header is display as earlier",
					"Fail qty column header should be display as earlier",
					"Fail qty column header doesn't display as earlier", "fail");
		}

		String failQuantity3 = getAttribute(txt_failedQtyQC, "value");
		if (failQuantity3.equals("30.00")) {
			writeTestResults("Verify that fail qty is display in history tab", "Fail qty should display in history tab",
					"Fail qty successfully display in history tab", "pass");
		} else {
			writeTestResults("Verify that fail qty is display in history tab", "Fail qty should display in history tab",
					"Fail qty doesn't display in history tab", "fail");
		}
	}

	/* Generate Internal Receipt PROD_E2E_003 */
	public void internalReciptAndJournelEntry_PROD_E2E_003() throws Exception {
		openPage(readProductionData("ReleaseedPO_PRD_E2E_003"));
		explicitWaitUntillClickable(btn_action2, 50);
		Thread.sleep(2000);
		click(btn_action2);

		explicitWaitUntillClickable(btn_convertToInternalReceipt, 10);
		if (isDisplayed(btn_convertToInternalReceipt)) {
			writeTestResults("Verify that Convert to Internel Recipt option is available under Actions",
					"Convert to Internel Recipt option should be available under Actions",
					"Convert to Internel Recipt option successfully available under Actions", "pass");
		} else {
			writeTestResults("Verify that Convert to Internel Recipt option is available under Actions",
					"Convert to Internel Recipt option should be available under Actions",
					"Convert to Internel Recipt option doesn't available under Actions", "fail");
		}
		click(btn_convertToInternalReceipt);

		explicitWait(header_genarateInternalReciptSmoke, 20);
		if (isDisplayed(header_genarateInternalReciptSmoke)) {
			writeTestResults("Verify that genarate internel receipt window is open",
					"genarate internel receipt window should be open",
					"genarate internel receipt window succeessfully open", "pass");
		} else {
			writeTestResults("Verify that genarate internel receipt window is open",
					"genarate internel receipt window should be open", "genarate internel receipt window doesn't open",
					"fail");
		}

		click(chk_seletcOroductForInternalRecipt);

		if (isSelected(chk_seletcOroductForInternalRecipt)) {
			writeTestResults("Verify user able to select the product line",
					"User should be able to select the product line", "User successfully select the product line",
					"pass");
		} else {
			writeTestResults("Verify user able to select the product line",
					"User should be able to select the product line", "User doesn't select the product line", "fail");
		}

		String enteredReceiptDate = getAttribute(txt_reciptDateGenarateInternalREceiptWindow, "value");
		click(btn_genarateInternalRecipt);
		explicitWait(para_validationInternalREciptGenarateSuccessfull, 10);
		if (isDisplayed(para_validationInternalREciptGenarateSuccessfull)) {
			writeTestResults("Verify that succesfull validation is appear for Internal Receipt Genaration",
					"Succesfull validation should appear for Internal Receipt Genaration",
					"Succesfull validation is appear for Internal Receipt Genaration", "pass");
		} else {
			writeTestResults("Verify that succesfull validation is appear for Internal Receipt Genaration",
					"Succesfull validation should appear for Internal Receipt Genaration",
					"Succesfull validation doesn't appear for Internal Receipt Genaration", "fail");
		}

		Thread.sleep(3500);
		explicitWaitUntillClickable(lnk_genaratedInternalREciptSmoke, 15);
		Thread.sleep(1500);
		click(lnk_genaratedInternalREciptSmoke);
		Thread.sleep(2000);
		switchWindow();
		Thread.sleep(1000);
		explicitWait(header_releasedInternalRecipt, 10);
		trackCode = getText(lbl_docNumber);
		if (isDisplayed(header_releasedInternalRecipt)) {
			writeTestResults("Verify that Internal Receiptis open as released status",
					"Internal Receipt should be open as released status",
					"Internal Receipt successfully open as released status", "pass");
		} else {
			writeTestResults("Verify that Internal Receiptis open as released status",
					"Internal Receipt should be open as released status",
					"Internal Receipt doesn't open or not in released status", "fail");
		}

		String docDate = getText(a_documentDate);
		String postDate = getText(a_postDate);
		String dueDate = getText(a_dueDate);

		if (docDate.equals(enteredReceiptDate) && docDate.equals(postDate) && docDate.equals(dueDate)) {
			writeTestResults(
					"Verify that all three header dates are equal to the date that entered when generating Internal Receipt",
					"All three header dates should be equal to the date that entered when generating Internal Receipt",
					"All three header dates are equal to the date that entered when generating Internal Receipt",
					"pass");
		} else {
			writeTestResults(
					"Verify that all three header dates are equal to the date that entered when generating Internal Receipt",
					"All three header dates should be equal to the date that entered when generating Internal Receipt",
					"All three header dates are not equal to the date that entered when generating Internal Receipt",
					"fail");
		}

		String warehouseOnInternalReceipt = getText(div_warehouseInternalRecipt);
		if (warehouseOnInternalReceipt.equals(outputWarehouseLI)) {
			writeTestResults(
					"Verify that Output Warehouse of Production Order and Warehouse of Internal Receipt is same",
					"Output Warehouse of Production Order and Warehouse of Internal Receipt should be same",
					"Output Warehouse of Production Order and Warehouse of Internal Receipt is same", "pass");
		} else {
			writeTestResults(
					"Verify that Output Warehouse of Production Order and Warehouse of Internal Receipt is same",
					"Output Warehouse of Production Order and Warehouse of Internal Receipt should be same",
					"Output Warehouse of Production Order and Warehouse of Internal Receipt is not same", "fail");
		}

		String qtyInternalReceipt = getText(div_planedQtyInternalRecipt);
		if (qtyInternalReceipt.equals("70.00")) {
			writeTestResults("Verify that quantity is equel to the QC pass quantity",
					"Quantity should be equel to the QC pass quantity",
					"Quantity successfully equel to the QC pass quantity", "pass");
		} else {
			writeTestResults("Verify that quantity is equel to the QC pass quantity",
					"Quantity should be equel to the QC pass quantity",
					"Quantity successfully not equel to the QC pass quantity", "fail");
		}

		click(btn_action2);

		explicitWaitUntillClickable(btn_journal, 10);
		if (isDisplayed(btn_journal)) {
			writeTestResults("Verify that Journal Entry option is available under Actions",
					"Journal Entry option should be available under Actions",
					"Journal Entry option successfully available under Actions", "pass");
		} else {
			writeTestResults("Verify that Journal Entry option is available under Actions",
					"Journal Entry option should be available under Actions",
					"Journal Entry option doesn't available under Actions", "fail");
		}
		click(btn_journal);

		explicitWait(header_journalEntryPopup, 10);
		if (isDisplayed(header_journalEntryPopup)) {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded",
					"Journal Entry details pop up successfully get loaded", "pass");
		} else {
			writeTestResults("Verify Journal Entry details pop up is loaded",
					"Journal Entry details pop up should get loaded", "Journal Entry details pop up doesn't get loaded",
					"fail");
		}

		if (isDisplayedQuickCheck(lbl_debitValueJournelEntry)) {
			/* QA */
			String debitAmount = getText(lbl_debitValueJournelEntry).replace(",", "");

			if (debitAmount.startsWith("14000.00")) {
				writeTestResults("Verify that amount was debited to the relevent GL Account",
						"Amount should be debited to the relevent GL Account",
						"Amount successfully debited to the relevent GL Account", "pass");
			} else {
				writeTestResults("Verify that amount was debited to the relevent GL Account",
						"Amount should be debited to the relevent GL Account",
						"Amount doesn't debited to the relevent GL Account", "fail");
			}

			String creditAmount = getText(lbl_creditValueJournelEntryQAStandardCost).replace(",", "");
			if (creditAmount.startsWith("14000.00")) {
				writeTestResults("Verify that amount was credited to the relevent GL Account",
						"Amount should be credited to the relevent GL Account",
						"Amount successfully credited to the relevent GL Account", "pass");
			} else {
				writeTestResults("Verify that amount was credited to the relevent GL Account",
						"Amount should be credited to the relevent GL Account",
						"Amount doesn't credited to the relevent GL Account", "fail");
			}

			String debitTotal = getText(lbl_debitTotalJournelEntrySingleEntry).replace(",", "");
			String credTotal = getText(lbl_creditTotalJournelEntrySingleEntry.replace(",", ""));

			if (debitTotal.startsWith("14000.00") && debitTotal.equals(credTotal)) {
				writeTestResults("Verify total debit and credit amounts were balanced",
						"Total debit and credit amounts should balanced",
						"Total debit and credit amounts successfully balanced", "pass");
			} else {
				writeTestResults("Verify total debit and credit amounts were balanced",
						"Total debit and credit amounts should balanced",
						"Total debit and credit amounts weren't balanced", "fail");
			}

		} else {
			/* Staging */
			String debitAmount = getText(lbl_debitValueJournelEntryStaging).replace(",", "");

			if (debitAmount.startsWith("14000.00")) {
				writeTestResults("Verify that amount was debited to the relevent GL Account",
						"Amount should be debited to the relevent GL Account",
						"Amount successfully debited to the relevent GL Account", "pass");
			} else {
				writeTestResults("Verify that amount was debited to the relevent GL Account",
						"Amount should be debited to the relevent GL Account",
						"Amount doesn't debited to the relevent GL Account", "fail");
			}

			String creditAmount = getText(lbl_creditValueJournelEntryStagingStandardCost).replace(",", "");
			if (creditAmount.startsWith("14000.00")) {
				writeTestResults("Verify that amount was credited to the relevent GL Account",
						"Amount should be credited to the relevent GL Account",
						"Amount successfully credited to the relevent GL Account", "pass");
			} else {
				writeTestResults("Verify that amount was credited to the relevent GL Account",
						"Amount should be credited to the relevent GL Account",
						"Amount doesn't credited to the relevent GL Account", "fail");
			}

			String debitTotal = getText(lbl_debitTotalJournelEntrySingleEntry).replace(",", "");
			String credTotal = getText(lbl_creditTotalJournelEntrySingleEntry).replace(",", "");

			if (debitTotal.startsWith("14000.00") && debitTotal.equals(credTotal)) {
				writeTestResults("Verify total debit and credit amounts were balanced",
						"Total debit and credit amounts should balanced",
						"Total debit and credit amounts successfully balanced", "pass");
			} else {
				writeTestResults("Verify total debit and credit amounts were balanced",
						"Total debit and credit amounts should balanced",
						"Total debit and credit amounts weren't balanced", "fail");
			}

		}
	}

}
