package bileeta.BATF.Pages;

import bileeta.BTAF.Utilities.TestBase;

public class InventoryAndWarehouseModuleData extends TestBase {

	/* Reading the element locators to variables */
	// Common
	protected static String header_documentReleasedStatus01;
	protected static String header_documentDraftStatus01;
	protected static String lbl_releasedStatusCommonWithoutSpace;
	protected static String btn_apply;
	protected static String header_holdStatusTransferOrder;
	protected static String header_journelEntryDialogBox;
	protected static String lbl_relesedConfirmation;
	protected static String btn_newPrimaryForm;
	protected static String btn_releese2;
	protected static String brn_serielBatchCapture;
	protected static String txt_batchNumberCapture;
	protected static String btn_updateCapture;
	protected static String btn_productCaptureBackButton;
	protected static String txt_serialNumberCaptureILOOutboundShipment;
	protected static String header_searielBatchCapturePage;
	protected static String header_draft;
	protected static String btn_draft2;
	protected static String btn_checkout2;
	protected static String chk_productCaptureRange;
	protected static String drop_warehouseMultipleProductGrid;
	protected static String btn_commoApplyLast;
	protected static String span_afterCaptureSerialGreen;
	protected static String chk_isQurantineWarehouse;

	// Login
	protected static String siteLogo;
	protected static String txt_username;
	protected static String txt_password;
	protected static String btn_login;
	protected static String lnk_home;
	protected static String div_loginVerification;

	// Com_TC_002
	protected static String navigation_pane;
	protected static String inventory_module;
	protected static String icon_logged_info;

	// IW_TC_001
	protected static String lnk_product_group_configuration; // IW_TC_002
	protected static String btn_new_group_configuration;
	protected static String btn_product_group_plus_mark_table;
	protected static String btn_product_group_plus_mark_second;
	protected static String btn_update_product_group;
	protected static String txt_product_group;
	protected static String tab_pg_details;
	protected static String btn_inventory_active; // IW_TC_003
	protected static String btn_purchase_active;
	protected static String btn_sales_active;
	protected static String btn_product_function_confirmation;
	protected static String btn_draft;
	protected static String btn_relese;
	protected static String btn_status_relese;
	protected static String header_productGroupConfigPage;
	protected static String header_newProductGroupForm;
	protected static String tbl_productGroup;
	protected static String btn_productGroupUpdate;
	protected static String dropdown_productGroupConfig;
	protected static String btn_purchaseActiveConfirmation;
	protected static String btn_salesActiveConfirmation;
	protected static String header_warehouseInforProductGroupConfig;
	protected static String header_producInfoProductGroupConfig;
	protected static String header_sellingDeatailsProductGroupConfig;
	protected static String dropdown_outboundCostingMethodProductGroupConfig;
	protected static String chk_allowInventoryWithoutCostingProductGroupConfig;
	protected static String chk_batchProductProductGroupConfig;
	protected static String txt_tollerenceProductGroupConfig;
	protected static String txt_minPriceProductGroupConfig;
	protected static String txt_maxPriceProductGroupConfig;

	// IW_TC_002
	protected static String btn_productInformation;
	protected static String btn_newProduct;
	protected static String header_productInformation;
	protected static String dropdown_productGroup;

	// IW_TC_003
	protected static String btn_product_info;
	protected static String btn_new_product;
	protected static String txt_product_code;
	protected static String txt_product_desc;
	protected static String dropdown_product_group;
	protected static String txt_menufacturer;
	protected static String dropdown_defoult_uom_group;
	protected static String dropdown_defoult_uom;
	protected static String txt_length;
	protected static String txt_width;
	protected static String txt_height;
	protected static String tab_detail;
	protected static String checkbox_batch_product;
	protected static String dropdown_outbound_costing_method;
	protected static String checkbox_without_costing;
	protected static String checkbox_allow_allocation;
	protected static String checkbox_validate_expiery;
	protected static String txt_tollerence_date;
	protected static String checkbox_qc_acceptance;
	protected static String checkbox_validate_expiery_date;
	protected static String header_product_information;
	protected static String header_new_product;
	protected static String btn_menufactururSearchBatchProduct;
	protected static String lnk_resultMenufacBatchProduct;
	protected static String drop_lengthUnitBtachProduct;
	protected static String drop_widthUnitBtachProduct;
	protected static String drop_heightUnitBtachProduct;
	protected static String header_inventoryBatchProduct;
	protected static String chk_allocationBatchProduct;
	protected static String btn_roqBatchProduct;
	protected static String lbl_creationsDocNo;
	protected static String btn_chkIsSeriel;

	// IW_TC_004
	protected static String btn_warehouseInfo;
	protected static String btn_newWarehouse;
	protected static String txt_warehouseCode;
	protected static String txt_warehouseName;
	protected static String dropdown_businessUnit;
	protected static String checkbox_isAllocation;
	protected static String dropdown_qurantineWarehouse;
	protected static String checkbox_autoLot;
	protected static String dropdown_lotBook;
	protected static String header_warehouseCretionForm;
	protected static String btn_draftConfirmation;
	protected static String btn_releseConfirmation;
	protected static String header_warehouseInformationPage;

	// IW_TC_005
	protected static String btn_stockAdjustment;
	protected static String btn_newStockAdjustment;
	protected static String txt_descStockAdj;
	protected static String dropdown_warehouseStockAdj;
	protected static String btn_productLookupStockAdj;
	protected static String btn_searchProductStockAdj;
	protected static String btn_resultProduct;
	protected static String header_newStockAdj;
	protected static String txt_productSearch;
	protected static String txt_qtyProductGrid1;
	protected static String txt_qtyProductGrid2;
	protected static String txt_costProductGrid1;
	protected static String txt_costProductGrid2;
	protected static String btn_addNewRow;
	protected static String btn_searchProductStockAdj2;
	protected static String btn_newTransferOrder;
	protected static String btn_productLookupStockAdjForLoop;
	protected static String txt_qtyProductGridStockAdjustment;
	protected static String txt_costProductGridStockAdjustment;
	protected static String btn_deleteEmotyRowStockAdjustment;
	protected static String btn_itemProductCapturePage;
	protected static String txt_lotNumberStockAdjustment;
	protected static String txt_expiryDateStockAdjustmentProductCapture;
	protected static String txt_searielLotNo;
	protected static String txt_dateSerielProductProductCapturePage;
	protected static String td_availableStockWarehouseGST_Warehouse;
	protected static String btn_avilabilityCheckWidget;
	protected static String btn_avilabilytCheckSecondMenu;
	protected static String btn_closeAvailabilityCheckPopup;
	protected static String btn_availabilityCheckWidgetAfterrelesed;
	protected static String td_availableStockGstmWarehouseAfterRelesed;
	protected static String btn_deleteEmotyRowCommonStockAdjustment;

	// IW_TC_006
	protected static String btn_transferOrder;
	protected static String header_newTransferOrder;
	protected static String txt_fromWareTO;
	protected static String txt_toWareTO;
	protected static String btn_checkout;
	protected static String header_outboundShipment;
	protected static String txt_serielBatchProductCaptureStockAdjustment;
	protected static String txt_qtyProductGridStockTransferOrder;
	protected static String txt_costProductGridStockTransferOrder;
	protected static String chk_availableProductsTransferOrderOutboundShipment;
	protected static String lbl_qtyBatchSpecifificCapturePage;
	protected static String lbl_captureQtyBatchSpecifificCapturePage;
	protected static String txt_seriealProductCapture;
	protected static String btn_productAvilabilityWidgetShipmentDetailsTable;
	protected static String btn_availabilityMoveFirstButtonTransferOrder;
	protected static String btn_productLookupTOForLoop;
	protected static String header_draftedTrsansferOrder;

	// IW_TC_007
	protected static String btn_productGridSearchInterDepartmentTransferOrder;
	protected static String txt_prductSearchInterDepartmentTransferOrder;
	protected static String btn_InterDepartmentTransferOrder;
	protected static String drop_salesPriceModel;
	protected static String btn_salesPriceModelConfirmation;
	protected static String txt_qtyProductGridInterDepartmentTransferOrder;
	protected static String txt_closePopupProductAvailabilityInterDepartmentTransferOrder;
	protected static String lbl_availQuantityProductCapturuPage;
	protected static String btn_deleteRowProductCaptururePage;
	protected static String chk_deleteItemCapturePage;
	protected static String header_releasedInboundShipment;

	// IW_TC_008
	protected static String btn_productConversion;
	protected static String bnt_newProductConversion;
	protected static String drop_wareProductCon;
	protected static String lbl_bokNoProductConversion;
	protected static String btn_productLookupOroductConversion;
	protected static String heder_popupProductLookupProductConversion;
	protected static String div_productNameProductGrigPrductConversion;
	protected static String txt_quantityProductConversion;
	protected static String btn_serialNosFromProductConversion;
	protected static String header_searialBatchNosProductConversion;
	protected static String lnk_resutlSeraialBatchCaptureProductConversion;
	protected static String btn_seraialBatchCaptureDocIconProductConversion;
	protected static String chk_batchSelectProductConversion;
	protected static String header_existinfSerialBatchProductConversion;
	protected static String txt_qtyToProductProductConversion;
	protected static String btn_applyProductBatchCaptureProductConversion;
	protected static String btn_productLoocupToProductProductConversion;
	protected static String header_productConversionPage;
	protected static String div_productNameProductGrigPrductConversionToProduc1;
	protected static String txt_costToProductOroductConversion;
	protected static String txt_quntityToProductProductConversion;
	protected static String btn_serialNosFromProductConversionToProduc;
	protected static String txt_batchNoCaptureProductConversionNosPopup;
	protected static String txt_LotNoProductConversion;
	protected static String btn_exoiryDateProductConversionToProduct;
	protected static String btn_date31ProductConversionToProduct;
	protected static String lbl_resultCaptureToProductProductConversion;
	protected static String btn_draftBayJavaScrptFirstButton;
	protected static String lbl_releasedStatusProductConversion;

	// IW_TC_010
	protected static String btn_internelOrder;
	protected static String btn_newInternelOrder;
	protected static String btn_employeeRequestJourneyInternelOrder;
	protected static String txt_tiltleInternelOrder;
	protected static String btn_verifyInternelOrderPage;
	protected static String btn_searchRequester;
	protected static String btn_refreshRequsterWindow;
	protected static String lnk_searchResultRequester;
	protected static String txt_RequesterSearch;
	protected static String btn_serchLockupProduct1InternelOrder;
	protected static String txt_productsearch1IneternelOrder;
	protected static String lnk_resultProduct1InternelOrder;
	protected static String dropdown_selectWarehouseInternelOrder;
	protected static String txt_qtyProduct1InternelOrder;
	protected static String btn_addSecondRowProductGridInternelOrder;
	protected static String txt_productsearch2IneternelOrder;
	protected static String dropdown_selectWarehouseInternelOrderRaw2;
	protected static String txt_qtyProduct2InternelOrder;
	protected static String btn_searchLokupInternelOrderRow2;
	protected static String headerVerifyDraftInternelOrder;
	protected static String btn_draftInternelOrder;
	protected static String btn_goToPageLink;
	protected static String btn_draftInternelDispatchOrder;
	protected static String btn_popoupOkLastStep;
	protected static String btn_productLookupInterDeprtmentTransferOrderForLoop;
	protected static String btn_detailTab;
	protected static String drop_warehouseInternelOrderEmployeeRequest;

	// IW_TC_011
	protected static String btn_internelReturnOrder;
	protected static String btn_newInternelReturnOrder;
	protected static String btn_employeeReturnJourneyIRO;
	protected static String btn_searchIconEmployeeIRO;
	protected static String txt_employeeSearchRequesterWindowIRO;
	protected static String lnk_resultRequesterIRO;
	protected static String dropdown_toWarehouseIRO;
	protected static String btn_docIconSelectInternelOrderIRO;
	protected static String txt_internelOrderSearchIRO;
	protected static String lnk_resultInternelOrderIRO1;
	protected static String lnk_resultInternelOrderIRO2;
	protected static String btn_applyInternelOrderIRO;
	protected static String btn_captureItem1IRO;
	protected static String btn_captureItem2IRO;
	protected static String btn_selectAllCaptureItemsIRO;
	protected static String heder_internelReturnOrder;
	protected static String header_newJourneyPopupIRO;
	protected static String header_newIRO;
	protected static String header_documentListIRO;
	protected static String result_documentListIRO;
	protected static String code_productAvailableIRO;
	protected static String header_internelReciptIRO;
	protected static String txt_RequesterIRO;
	protected static String btn_serchDocInternelReturnOrder;
	protected static String td_serchedDocNoIRO;
	protected static String lbl_product1DocList;
	protected static String lbl_product2DocList;
	protected static String lbl_product1IRO;
	protected static String lbl_product2IRO;
	protected static String header_pageLabel;
	public static String btn_print;
	protected static String chk_selectAllCaptureItem;
	protected static String btn_backButtonUpdateCapture;

	// IW_TC_012
	protected static String btn_inboundLoanOrder;
	protected static String btn_newInboundLoanOrder;
	protected static String btn_vendorSearchILO;
	protected static String txt_vendorSearchILO;
	protected static String lnk_resultVendorILO;
	protected static String btn_addressILO;
	protected static String txt_billingAddressILO1;
	protected static String txt_deliveryAddressILO;
	protected static String header_ILOPage;
	protected static String txt_costILOProduct1;
	protected static String txt_costILOProduct2;
	protected static String dropdown_warehouseILOInboundShipment;
	protected static String btn_addNewRowProductGridILOOutboundShipment;
	protected static String dropdown_warehouseProduct2ILOOutboundShipment;
	protected static String btn_checkoutILOOutboundShipment;
	protected static String btn_serielBatchCaptureProduct1ILOOutboundShipment;
	protected static String txt_batchNumberCaptureILOOutboundShipment;
	protected static String btn_serielBatchCaptureProduct2ILOOutboundShipment;
	protected static String btn_editOutboundShipmentILO;
	protected static String txt_qtyProduct2InternelReturnOrder;
	protected static String txt_qtyProductGridInboundLoanOrder;
	protected static String btn_tabInboundLoanOrder;

	// IW_TC_013
	protected static String btn_outboundLoanOrder;
	protected static String btn_newOutboundLoanOrder;
	protected static String btn_customerAccountSearchLookupILO;
	protected static String txt_accountSearchILO;
	protected static String lnk_resultCustomerAccount;
	protected static String btn_billingAddressILO;
	protected static String txt_billingAddressILO;
	protected static String txt_shippingAddressILO;
	protected static String btn_applyAddressILO;
	protected static String dropdown_salesUnitILO;
	protected static String btn_productLookupSearchILO;
	protected static String dropdown_warehouse1ILO;
	protected static String txt_quantity1ILO;
	protected static String btn_addNewProductRowILO;
	protected static String dropdown_warehouse2ILO;
	protected static String txt_quantity2ILO;
	protected static String btn_detailsTabILO;
	protected static String chkbox_underDeliveryILO;
	protected static String chkbox_partielDeliveryILO;
	protected static String btn_reqSlipDateILO;
	protected static String btn_reqSlipDate17ILO;
	protected static String btn_conSlipDateILO;
	protected static String btn_conSlipDate18ILO;
	protected static String btn_reqReciptDateILO;
	protected static String btn_reqReciptDate19ILO;
	protected static String btn_conReciptDateILO;
	protected static String btn_conReciptDate20ILO;
	protected static String btn_releseILO;
	protected static String btn_goToNextTaskILO;
	protected static String product2ILO;
	protected static String btn_productLookup13OLO;

	// IW_TC_014
	protected static String lnl_resultOLO;
	protected static String cell_beforeAvailableQuantity;
	protected static String btn_actionOLO014;
	protected static String btn_convertToInboundShipment;
	protected static String txt_inboundQuantity;
	protected static String cell_afterAvailableQuantity;
	protected static String txt_inboundQuantity1;
	protected static String lbl_checkoutUnitsBalance014;
	protected static String lbl_checkoutUnitsBalanceAfter014;
	protected static String txt_outboundLoanOrderSearch;
	protected static String chk_cpatureSelection1_014;
	protected static String btn_delRowsCapturePage;
	protected static String btn_availabilityWidgetOutboundLoanOrder;

	// IW_TC_015
	protected static String txt_searchOLO;
	protected static String btn_conToSalesInvoiceOLO;
	protected static String txt_customerAccOLOSlaesInvoic;
	protected static String drop_downCurencyOLOSlaesInvoic;
	protected static String txt_billingAddressILOSalesInvoice;
	protected static String txt_shippingAddressILOSalesInvoice;
	protected static String dropdownSalesUnitOLOSalesInvoice;
	protected static String btn_draftOLOSalesInvoice;
	protected static String btn_checkoutOLOSalesInvoice;

	// IW_TC_016
	protected static String btn_assemblyCostEstimation;
	protected static String btn_newAssemblyCostAstimation;
	protected static String txt_assemblyCostEstiamtionDescription;
	protected static String btn_searchCustomeACE;
	protected static String txt_cusSearchACE;
	protected static String lnk_resultCusAccountACE;
	protected static String btn_searchAssemblyProACE;
	protected static String txt_AssemblyProductSearchACE;
	protected static String lnk_resltProductAssemblyACE;
	protected static String btn_searchPricingProfileACE;
	protected static String txt_pricingProfileACE;
	protected static String lnk_pricingProfileACE;
	protected static String tab_estimationDeatailsACE;
	protected static String drop_typeACE1;
	protected static String btn_searchProductEstmationAce1;
	protected static String txt_estimatedQuantityACE;
	protected static String txt_costEstmatedACE1Input;
	protected static String btn_adnewRow1ACE;
	protected static String drop_typeACE2;
	protected static String btn_searchProduct2ACE;
	protected static String txt_estimatedQuantityACE2;
	protected static String txt_costEstmatedACEOutput2;
	protected static String btn_adaNewRow3;
	protected static String drop_typeExpenceACE;
	protected static String drop_estiamationRow3ReferenceCodeExpenceACE;
	protected static String btn_searchServiceACE3ProductLookup;
	protected static String txt_costServiceProductEstmationACE;
	protected static String btn_addNewRow4EstmationDetailsACE;
	protected static String chk_billableEstmationRow1ACE;
	protected static String chk_billableEstmationRow2ACE;
	protected static String chk_billableEstmationRow3ACE;
	protected static String drop_type4ServiceACE;
	protected static String btn_searchProductLookuop4ACE;
	protected static String txt_cost4ServiceProductEstimationACE;
	protected static String chk_billable4ServiceACeEstimation;
	protected static String tab_summaryACE;
	protected static String btn_checkoutACE;
	protected static String lbl_totalProfitACE;
	protected static String lbl_relesedACEDocNumber;

	/* Common PO For Fin_TC_016 */
	protected static String txt_lotNoCapturePage1;

//	IW_TC_017
	protected static String btn_assemblyOredr;
	protected static String btn_newAssemblyOrder;
	protected static String btn_assemblyProductSearch;
	protected static String drop_assemblyGroup;
	protected static String btn_emloyeeSerchAO;
	protected static String txt_employee;
	protected static String lnk_resultEmployee;
	protected static String drop_priorityAO;
	protected static String btn_searchSCostEstimtioAo;
	protected static String txt_costEstimation;
	protected static String lnk_resultCostEstimation;
	protected static String drop_barcodeAO;
	protected static String btn_searchProductionUnit;
	protected static String drop_warehouseWIPAO;
	protected static String drop_inputWareAO;
	protected static String drop_wareOutAO;
	protected static String txt_serialNoAO;
	protected static String date_secondSeclectioAO;
	protected static String date_firstClickAO;
	protected static String date_secondClickAO;
	protected static String tab_productAO;
	protected static String drop_warehouseProductAO1;
	protected static String tab_estimationDeatailsAO;
	protected static String btn_availabilityCheckForVErifyEstmationProductAO;
	protected static String txt_productionUnit;
	protected static String lnk_resultProductUnit;
	protected static String btn_clanderBack;
	protected static String btn_clanderFw;

	/* IW_TC_018 */
	protected static String btn_salesModule;
	protected static String btn_salesOrder;
	protected static String btn_newSalesOrder;
	protected static String btn_salesOrderToSalesInvoiceJourney;
	protected static String btn_lookupCustomerAccount;
	protected static String txt_searchCustomerSalesOrder;
	protected static String drop_curencySalesOrder;
	protected static String drop_salesUnitSalesOrder;
	protected static String btn_productLoockup;
	protected static String resul_assemblyProductProductSearchSalesOrder;
	protected static String txt_quantitySalesOrder;
	protected static String txt_unitPriceSalesOrder;
	protected static String btn_actionSalesOrder;
	protected static String btn_costEstimation;
	protected static String btn_essambyEstimationSalesOrder;
	protected static String btn_addNewCostEstimationSalesOrder;
	protected static String header_salesOrder;
	protected static String txt_customerAccountOnInterface;
	protected static String txt_biilingAdressFrontPage;
	protected static String txt_shippingAddressFrontPage;
	protected static String drop_accountOrner;
	protected static String drop_warehouseProductGridSalesOrder;
	protected static String txt_productAssemblyCostEstimationAssemblyProcess;
	protected static String header_assemblyCostEstimation;
	protected static String txt_customerAssemblyCostEstimationAssemblyProcess;
	protected static String txt_descriptonAssemblyCostEstimationAssemblyProcess;
	protected static String btn_searchPricingProfileAssemblyCostEstimationAssemblyProcess;
	protected static String btn_tabEstimationDeatailsAseemblyCostEstmationAssemblyProcess;
	protected static String tbl_costEstimationProductAssemblyProcess;
	protected static String lnk_resultInputProductAssemblyProcessCostEstimation;
	protected static String btn_summaryCostEstimationAssemblyProcess;
	protected static String btn_checkoutCostEstimationAssemblyProcess;
	protected static String lbl_ttalProfitCostEstimationAssemblyProcess;
	protected static String lbl_esitimationTdAssemblyProcess;
	protected static String lbl_unitCostAfterApplyCostEstimationAssmblyProcess;
	protected static String btn_genrateEstimationAssemblyProcess;
	protected static String btn_applyToMAinGridAssemblyProcess;
	protected static String lbl_bannerTotalAssemblyProcess;
	protected static String btn_okAssemblyProcess;
	protected static String btn_genarateAssemblyOrder;
	protected static String header_popupAssemblyOrder;
	protected static String btn_taskAssignUserLookup;
	protected static String txt_userSearch;
	protected static String btn_resultUserNialPeeris;
	protected static String txt_userPopupPageAssemblyOrerAssembluProcess;
	protected static String chk_assemblyOrderAssemblyPopup;
	protected static String btn_applyAssemblyPopupAssemblyOrder;
	protected static String btn_entutionLogo;
	protected static String btn_taskTile;
	protected static String btn_assemblyOrderTile;
	protected static String btn_arrowGoToTaskReleventAssemblyOrder;
	protected static String headrer_assemblyOrder;
	protected static String btn_jobStart;
	protected static String txt_timeStartAssemblyProcess;
	protected static String txt_timeEndAssemblyProcess;
	protected static String btn_startAssemblyProcess;
	protected static String lbl_statusAssemblyOrder;
	protected static String btn_cretaeInternelOrderAssemblyProcess;
	protected static String header_internelOrderPopup;
	protected static String chk_InternelOrerProductAssemblyProcess;
	protected static String btn_applyInternelOrderAssemblyProcess;
	protected static String txt_shipAddressAssemblyProcess;
	protected static String btn_outputProductUpdateAssemblyProcess;
	protected static String header_updateOutputProdictAssemblyProcess;
	protected static String chk_productThatUpdateAssemblyProcess;
	protected static String btn_applyAssemblyProcess;
	protected static String btn_jobComplteAssemblyProcess;
	protected static String btn_complteJobComplteAssemblyProcess;
	protected static String btn_convertToInternelReciptAssemblyProcess;
	protected static String header_genarateInternelReciptAssemblyProcess;
	protected static String chk_selectAllInternelReciptAssemblyProcess;
	protected static String btn_checkoutInternelReciptsAssemblyProcess;
	protected static String div_verifySerielAutoLoadAssemblyProductAssemblyProcess;
	protected static String btn_itemNumber2ProductCaptureWindowAssemblyProduct;
	protected static String btn_itemNumber1ProductCaptureWindowAssmblyProcess;
	protected static String btn_itemNumber3ProductCaptureWindowAssemblyProcess;

	// IW_TC_019
	protected static String btn_barcodeGenerator;
	protected static String icon_inboundShipmentBarcodeGeneratorPage;
	protected static String dropdown_warehouseBarcodeGeneratorPage;
	protected static String span_dateRangeBarcodeGeneratorPage;
	protected static String txt_searchBarcodeGeneratorPage;
	protected static String btn_docSaecrhBarcodeGeneratorPage;
	protected static String lbl_relesedInboundShipmentNoBarcodeGeneratorPage;
	protected static String btn_todayDateSetBarcodeGeneratorPage;
	protected static String btn_barcodeGenerateBarcodeGeneratorPage;
	protected static String dropdown_batchBookGenerateBarcodeGeneratorPage;
	protected static String txt_lotGenerateBarcodeGeneratorPage;
	protected static String btn_generateGenerateWindowBarcodeGeneratorPage;
	protected static String btn_updateGenerateWindowBarcodeGeneratorPage;
	protected static String btn_viewBarcodeGenarate;
	protected static String btn_printBarcodeGenarate;
	protected static String lbl_productAvailability019;

	// IW_TC_020
	protected static String btn_serialNumberViewer;
	protected static String btn_productSearchSerialNumberViewer; // IW_TC_021
	protected static String checkbox_inStockProductSerielBatchViewer;
	protected static String btn_serachProductSerielBatchViewer;
	protected static String header_resultCountSerielBatchViewer;
	protected static String drop_warehouseSerialBacthViewer;

	/* IW_TC_021 */
	protected static String btn_editSearial;
	protected static String txt_searielNumberEditTo;
	protected static String btn_changeEditedSeriel;

	// IW_TC_022
	// SetUp QC Acceptance
	protected static String btn_adminModule;
	protected static String btn_balancingLevelAdminModule;
	protected static String dropdown_QCAcceptanaceJourneyAdminModule;
	protected static String btn_updateBalancingLevelQCAcceptance;
	protected static String tbl_QCAcceptance;
	protected static String row_ifMoreRowAvailableQCAcceptance;
	protected static String txt_batchNoProductCapture022;
	protected static String chk_qcAcceptGeneral;
	protected static String btn_updateGenaralSectionBalancingLevel;
	protected static String lnk_infoOnLoolkupInfoReplace;

	// End SetUp QC Acceptance
	protected static String btn_qcAcceptance;
	protected static String drpdown_selectionQCAcceptance;
	protected static String lnk_outboundShipmentNoQCAcceptance;
	protected static String icon_serielNosQCAcceptance;
	protected static String txt_passQtyQCAcceptance;
	protected static String txt_failQtyQCAcceptance;
	protected static String txt_reasonQCAcceptance;
	protected static String btn_inboundShipment;
	protected static String txt_searchInboundShipment;
	protected static String lbl_batchNoGetFromQcAcceptancePage;
	protected static String dropdown_templateQcAcceptancePage;
	protected static String btn_batchSearialWiseProductAvailabilityQCAcceptance;
	protected static String cell_firstProductBatchSerialWiseAvailableTableQCAcceptnnce;
	protected static String lbl_outboundShipmentDocNumberQCAcceptance;
	protected static String header_QCAcceptanceForm;
	protected static String txt_docSearchQCAcceptance;
	protected static String chk_QCAccept;
	protected static String btn_docInboundSelectionQCAcceptance;
	protected static String btn_lastDocInboundSelectionQCAcceptance;
	protected static String tbl_qcAcceptanceProduct;
	protected static String btn_serielNosQCAcceptance;
	protected static String header_QCAcceptancePage;
	protected static String btn_closeGoToLinkPopup;
	protected static String header_inboundShipmemtQC;
	protected static String widget_checkAvailabilityProductQC;
	protected static String btn_productAvailabilityQC;
	protected static String td_productAvailableQuantityQC;
	protected static String header_productAvailabilityPopup;

	// Common Dispatch only batch product
	protected static String dropdown_productGroupDispatchOnlyBatchProduct;
	protected static String txt_menufacturerDispatchOnlyBatchProduct;
	protected static String dropdownDefoultUOMGroupDispatchOnlyBatchProduct;
	protected static String dropdownDefoultUOMDispatchOnlyBatchProduct;
	protected static String tab_deatilsDispatchOnlyBatchProduct;
	protected static String btn_inventoryActiveDispatchOnlyBatchProduct;
	protected static String chk_batchProductDispatchOnlyBatchProduct;
	protected static String dropdown_dispatchSelectionDispatchOnlyBatchProduct;
	protected static String btn_menufacturerSearchDispatchOnlyBatchProduct;
	protected static String lnk_resultMenufacturerDispatchOnlyBatchProduct;
	protected static String tab_summaryQCAcceptanec;

	// Common manufacturer creation
	protected static String btn_menufacturerInformtion;
	protected static String btn_newMenufacturer;
	protected static String txt_menufacturerCode;
	protected static String txt_nameMenufacturer;
	protected static String btn_upadateMenufacturer;
	protected static String btn_deleestShipmentAceptance;
	protected static String txt_lotNoCapturePage;

	// IW_TC_023
	protected static String btn_procumentModule;
	protected static String btn_purchaseOrder;
	protected static String btn_newPrurchaseOrder;
	protected static String btn_purchaseOrderToInboundShipmentToPurchaseInvoiceJourney;
	protected static String txt_vendorSearch23;
	protected static String lnk_resultVendorSeacrh23;
	protected static String btn_serachProductLookup23;
	protected static String txt_qtyProductPurchaseOrder;
	protected static String txt_unitPricePurchaseOrder;
	protected static String drop_warehousePurchaseOrder;
	protected static String btn_checkoutPurchaaseOrdeer;
	protected static String drop_inboundShipmentAsseptance;
	protected static String btn_shipmentAsseptance;
	protected static String drop_documentTypeShipmentAcceptance;
	protected static String txt_docNoInboundShipmentShipmentAcceptance;
	protected static String btn_serielNosShipmentAcceptance;
	protected static String chk_rangeCaptureAShipmentAsseptance;
	protected static String txt_rangecountShipmentAcceptance;
	protected static String txt_serielNumberStartShipmentAcceptance;
	protected static String txt_lotNumberStartShipmentAcceptance;
	protected static String plicker_exoiryDateShipmentAcceptance;
	protected static String btn_arrowToNExtMotnthShipmentAcceptanse;
	protected static String btn_date28ShipmentAcceptanse;
	protected static String btn_applyShipmentAcceptanceCaptureProdut;
	protected static String chk_releseSelectionShipmentAcceptance;
	protected static String btn_releseShipmentAcceptance;
	protected static String lnk_wiewInboundShipmenthipmentAcceptance;
	protected static String btn_draftPurchaseInvoiseShipmentAcceptance;
	protected static String btn_checkoutPurchaseInvoiceShipmentAcceptance;
	protected static String btn_releesePurchseInvoiceshipmentAcceptance;
	protected static String btn_vendorSearchPurchaseOredr;
	protected static String lnk_resultProductPurchaseOrder;
	protected static String lbl_releseForPickingInboundShipment;
	protected static String lbl_inboundShipmentDocNoShipmentAcceptance;
	protected static String chk_verifyReleeseIsSelected;
	protected static String lblDocumentStatusAfterShipmentAccewptanceOutboundShipment;
	protected static String btn_goToPurchaseInvoiveInboundShipmet;
	protected static String lbl_docNo;
	protected static String btn_releseShipmentAcceptanceJS;
	protected static String chk_shipemtAceptanceGeneral;
	protected static String div_productShipmentAcceptanceOnShipmentAcceptanceGrid;

	/* Fin_TC_009 */
	protected static String btn_stockTake;
	protected static String btn_newStockTake;
	protected static String drop_warehouseStockTske;
	protected static String btn_refreshStockTake;
	protected static String txt_filterProductStockTake;
	protected static String txt_physicalQuantityStockTake;
	protected static String txt_systemQuantityStockTake;
	protected static String btn_convertToStockAdjustment;
	protected static String btn_captureItem1;
	protected static String btn_captureItem2;
	protected static String txt_batchNoCapturePage;
	protected static String btn_capturePageDocIcon;
	protected static String chk_captureFirstRowSelect;
	protected static String header_page;
	protected static String lbl_docStatus1;
	protected static String row_stockTakeTable;
	protected static String td_varianvcePro1;
	protected static String td_varianvcePro2;
	protected static String btn_action;
	protected static String heade_page;
	protected static String lbl_docStatus;
	protected static String productionGroupBatchProductCreation;
	protected static String lbl_bannerTotal;
	protected static String txt_lotNoCapturePageStockTake;

	/* Action Verification Common */
	protected static String btn_convertToCommonActionVerification;
	protected static String header_commonActionVerification;

	/* AV_IW_ProductGroup */
	protected static String btn_editActionVerification;
	protected static String btn_updateActionVerification;
	protected static String btn_updateAndNewActionVerification;
	protected static String btn_duplicateActionVerification;
	protected static String btn_draftAndNewActionVerification;
	protected static String btn_copyFromActionVerification;
	protected static String header_productGroupLookupActionVerification;
	protected static String btn_closeLookupActionVerification;
	protected static String div_historyTableActionVerification;
	protected static String btn_historyActionVerification;
	protected static String btn_activitiesActionVerification;
	protected static String btn_activitiesHeaderActionVrification;
	protected static String btn_configureCategoriesActionVerification;
	protected static String btn_configureCategoriesHeaderActionVrification;
	protected static String btn_deleteActionVrification;
	protected static String btn_deleteConfirationActionVerification;

	/* AV_IW_WarehouseInformation */
	protected static String header_warehouseLookupActionVerification;

	/* AV_IW_AllocationManager */
	protected static String btn_allocationManger;
	protected static String btn_genarateAllocationMangerActionVerification;
	protected static String chk_manageDocumentAllocationManagerActionVerification;
	protected static String header_allocationManagerActionVerification;

	/* AV_IW_ProductInformation */
	protected static String header_productLookupActionVerification;
	protected static String btn_sellingPriceActionVerification;
	protected static String btn_PLUCodeAllocationMangerActionVerification;
	protected static String btn_remindersAllocationMangerActionVerification;
	protected static String header_reminderActionVerification;
	protected static String header_sellingPriceActionVerification;
	protected static String header_updatePluCodesActionVerification;

	/* AV_IW_InventoryParameters */
	protected static String btn_inventoryParameters;
	protected static String btn_plusMarkInventoryParameters;
	protected static String txt_parameterActionVerification;
	protected static String para_validator;

	/* AV_IW_AssemblyCostEstimation */
	protected static String header_costEstimationActionVerification;
	protected static String btn_reverseActionVrification;
	protected static String btn_reverseConfirationActionVerification;

	/* AV_IW_StockTake */
	protected static String table_stockTakeActionVerification;
	protected static String btn_dtaftedStockTakeActionVerification;

	/* AV_IW_InterdepartmentalTransferOrder */
	protected static String btn_dockflowActionVrification;
	protected static String header_documentFlowActionVerification;

	/* Common product create */
	protected static String txt_minPriceTextProductCreation;
	protected static String txt_maxPriceTextProductCreation;

	/* AV_IW_OutboundLoanOrder */
	protected static String btn_convertToInboundLoanOrderActionVerification;
	protected static String btn_convertToInboundShimentActionVerification;
	protected static String btn_convertToSalesInvoiceActionVerification;

	/* AV_IW_AssemblyProposal */
	protected static String btn_assemblyProposal;
	protected static String btn_productLoockupActionVerfication;
	protected static String btn_newAssemblyProcess;
	protected static String drop_priorityAssemblyProposal;
	protected static String datepliker_requestDateActionVerification;
	protected static String datepliker_requestEndDateActionVerification;
	protected static String txt_productSearchActionVerifcation;
	protected static String btn_resultAssemblyProductActionVerification;
	protected static String btn_productionUnitLookupActionVerification;
	protected static String txt_productionUnitSearchActionVerification;
	protected static String result_productionUnitActionVerification;
	protected static String drop_warehouseAssemblyProcessActionVerification;
	protected static String btn_draftActionVerification;
	protected static String btn_convertToAssemblyOrderActionVerification;
	protected static String header_assemblyOrderTaskActionVerification;
	protected static String header_assemblyProposalActionVerification;

	/* AV_IW_RackTransfer */
	protected static String btn_rackTransfer;
	protected static String btn_newRackTransfer;
	protected static String drop_warehouseRackTransfer;
	protected static String drop_filterBy;
	protected static String btn_productDocIconRackTransfer;
	protected static String chk_productSelectRackTransfer;
	protected static String btn_applyProductDocIconRackTransfer;
	protected static String btn_rackTransferNosIcon;
	protected static String txt_quantityRackTransfer;

	/* AV_IW_AssemblyOrder */
	protected static String header_JobStartHoltActionVerification;
	protected static String btn_stratJobActionVerificationAssemblyOrder;
	protected static String btn_completeJobActionVerificationAssemblyOrder;
	protected static String txt_changeReson;
	protected static String btn_okAssemblyOrderHoldUnholdActionVerification;
	protected static String btn_holdActionSectionVerification;
	protected static String btn_unholdActionSectionVerification;
	protected static String btn_closeActionSectionVerificatio;

	/* AV_IW_InboundShipment */
	protected static String drop_printInboundShipmentActionVerification;
	protected static String btn_reverseConfirmInboundShipmentActionVerification;

	/* AV_IW_InboundLoanOrder */
	protected static String btn_convertToOutboundLoanOrderActionVerification;
	protected static String btn_convertToOutboundShimentActionVerification;
	protected static String btn_convertToPurchaseInvoiceActionVerification;

	/* AV_IW_InternelRecipt */
	protected static String btn_journelActionVerification;
	protected static String btn_journelHeaderActionVrification;

	/* AV_IW_InternelRecipt */
	protected static String btn_availabilityCheckWidgetActionVerification;
	protected static String btn_productAvailabilityRackWiseActionVerification;
	protected static String btn_rackWiseProductAvailabilityHeaderActionVrification;

	/* Inventory And Warehouse Module Creations */

	/* Lot Book */
	protected static String btn_trnsactionBookConfiguration;
	protected static String drop_filterByTransactionBookConfiguration;
	protected static String btn_warehouseLotBookConfiguration;
	protected static String btn_transactionBookConfigRowAdding;
	protected static String txt_bookNoTBC;
	protected static String txt_prefixTBCWarehouse;
	protected static String chk_defoultWarehouseTBC;
	protected static String btn_updateWarehouseTransactionBookConfiguration;

	/******************************
	 * Regression 01
	 *************************************/
	/* IN_IO_004 */
	protected static String btn_genarelBalancingLevelSetting;
	protected static String drop_employeeSerchConfig;
	protected static String btn_updateBalncilgLevelEmployeeSerchConfig;

	/* IN_IO_005 */
	protected static String btn_campaigsSalesModule;
	protected static String txt_searchCampaign;
	protected static String btn_resultCampaignSalesModule;
	protected static String btn_statusDocument;
	protected static String btn_edit;
	protected static String btn_update;
	protected static String drop_docStatus;
	protected static String btn_applyStatusMenu;
	protected static String btn_campaignLookup;
	protected static String btn_resultCampaign;
	protected static String txt_campaignFrontPage;

	/* IN_IO_006 */
	protected static String txt_productAutoSearch;
	protected static String btn_productLoockupCommon;
	protected static String result_varientMainProduct;
	protected static String txt_prductGridSelectedProductOnlyOneRowIsThere;
	protected static String header_productLookup;
	protected static String result_cpmmonProduct;

	/* IN_IO_007 */
	protected static String td_uomWhenThereIsOneRow;

	/* IN_IO_008 */
	protected static String td_OHWhenThereIsOneRow;
	protected static String widget_checkAvailabilityWhenOneRowIsThere;
	protected static String btn_productAvailabilitySecondMenuWhenOneRowIsThere;
	protected static String drop_wareWhenOnlyOneRowInGrid;

	/* IN_IO_009 */
	protected static String btn_inentoryBalancingLevelSetting;
	protected static String drop_productSerchConfig;
	protected static String btn_updateBalncilgLevelProductSerchConfig;

	/* IN_IO_010 */
	protected static String btn_draftCommon;

	/* IN_IO_011 */
	protected static String btn_draftAndNewCommon;

	/* IN_IO_012 */
	protected static String btn_releaseCommon;

	/* IN_IO_014 */
	protected static String btn_copyFromCommon;
	protected static String header_IOLookup;
	protected static String txt_internelOrderLookupSearch;
	protected static String btn_resultIO;
	protected static String tab_summaryCommon;

	/* IN_IO_016 */
	protected static String link_resultDocInList;

	/* IN_IO_019 */
	protected static String btn_taskTileCommon;
	protected static String btn_internelDispatchOrderTile;
	protected static String txt_taskSearch;
	protected static String btn_releventTaskOpenLink;
	protected static String header_InternelDispatchOrder;

	/* IN_IO_022 */
	protected static String lbl_titleIOFrontPage;
	protected static String lbl_requesterIOFrontPage;
	protected static String lbl_productIOFrontPage;
	protected static String lbl_warehouseIOFrontPage;
	protected static String btn_duplicate;
	protected static String txt_nameInternelOrder;
	protected static String txt_requstrerInternelOrderFrontPage;
	protected static String txt_productInternelOrderFrontPage;
	protected static String txt_warehouseInternelOrderFrontPage;

	/* IN_IO_024 */
	protected static String btn_homeIcon;
	protected static String btn_deleteRowRoReplaceInternelDispatchOrder;

	/* IN_IO_025 */
	protected static String lbl_binCardBalanceLastRow;
	protected static String btn_reports;
	protected static String drop_reportTemplate;
	protected static String btn_editTemplateInReports;
	protected static String btn_quickPreviewRepots;
	protected static String txt_filterProductBinCardReports;

	/* IN_IO_025_I_026_I_027 */
	protected static String lbl_docValueJournelEntryInternelDispatchOrder;

	/* IN_IO_036 */
	protected static String btn_journeyConfiguration;
	protected static String chk_stockValidate;
	protected static String drop_stockReservationMethod;
	protected static String btnn_updateEmployeeRequestJourneyConfiguration;
	protected static String btn_advanceWidgetInternelOrder;
	protected static String txt_reservedQuantityAdvanceMenu;
	protected static String btn_aplyAdvanceMenuInternelOrder;
	protected static String btn_journeyConfigInternelOrderEmployeeRequest;
	protected static String lbl_reserverdQuantityAllocationManager;
	protected static String txt_docSearchAllocationManager;
	protected static String btn_documentSearchAllocationManager;

	/* IN_IO_040 */
	protected static String div_lineValidationProductCapture;
	protected static String btn_errorViewGrid;
	protected static String div_errorGridQuantityNotAvailable;
	protected static String btn_lookupFromProductProductConversion;
	protected static String txt_fromQuantityProductConversion;
	protected static String btn_serleNosProductConversionFromProduct;
	protected static String btn_lookupToProductProductConversion;
	protected static String btn_serleNosProductConversionToProduct;
	protected static String div_quantityDraftedStockAdjustment;
	protected static String btn_deleteRowCommonRowReplace;
	protected static String para_validatorErrorMessageSerialNos;

	/* IN_IO_041 */
	protected static String div_reservedQuantity;

	/* IN_IO_042_043_044 */
	protected static String btn_productAlternativeWidget;
	protected static String chk_productAlternative;
	protected static String lbl_ohAlternative;

	/* IN_IO_049 */
	protected static String btn_commonResultProductContains;
	protected static String lbl_draftedInternelOrderQuantityWhenOnlyOneRowIsThere;

	/* IN_IO_050 */
	protected static String btn_resultProductInSecrhByPage;
	protected static String chk_allowDecimal;

	/* IN_IO_051 */
	protected static String btn_removeUnavilableProductFromGrid;
	protected static String btn_unavailbleProductRemoveConfirmation;
	protected static String div_productVerifyInGridIDO;

	/* IN_IO_054 */
	protected static String btn_workflowConfiguration;
	protected static String txt_workflowSerch;
	protected static String btn_actieWorkflow;
	protected static String btn_inctieWorkflow;
	protected static String btn_firstButtonSignOut;
	protected static String btn_signOutLink;
	protected static String btn_sendReview;
	protected static String btn_approvalTab;
	protected static String btn_approvalOk;
	protected static String header_workflow_IN_IO_054;
	protected static String btn_workflowActiveConfirmation;
	protected static String header_draftApproved;
	protected static String header_draftReviewing;
	protected static String header_newInternelOrder;

	/* IN_IO_055_058 */
	protected static String btn_stockAvailabilityWidgetAfterReleasedInternelOrder;

	/* IN_IO_056 */
	protected static String header_releasedInternalOrder;

	/* IN_IO_063 */
	protected static String td_allocationMangerRecervedProductCount;
	protected static String btn_okHoldUnhold;

	/* IN_IO_065 */
	protected static String btn_availabilityWidget;

	/* IN_IO_066 */
	protected static String btn_postDate;
	protected static String btn_postDateInsitePostDatePopup;
	protected static String btn_calenderBack;
	protected static String para_validatorErrorMessage;
	protected static String header_newInternelOrderCommon;
	protected static String header_internelDispatchOredrCommon;
	protected static String link_commonDoubleCrickProductResult;
	protected static String headre_internalDispatchOrderReleasedStatus;

	/* IN_IO_067 */
	protected static String drop_warehouseInGrid;
	protected static String btn_productLookupInGrid;
	protected static String txt_quantityInGrid;
	protected static String txt_unitPriceInGrid;
	protected static String btn_salesReturnOrder;
	protected static String btn_newSalesReturnOrder;
	protected static String btn_journeySalesReturnOrderInboundShipmentSalesReturnInvoice;
	protected static String btn_soSelectorDocIcomIRO;
	protected static String drop_searchByIRO;
	protected static String txt_docNomIROSerach;
	protected static String btn_commonreturnSearch;
	protected static String chk_commonReturnDoc;
	protected static String btn_commoApply;
	protected static String drop_returnReason;
	protected static String btn_customerAccountLookupCommon;

	/* IN_IO_068 */
	protected static String btn_summaryTab;
	protected static String chk_allInternelOrderForReturn;

	/* IN_IO_069 */
	protected static String txt_serielNo;
	protected static String txt_lotNo;
	protected static String calender_serelSpecificCapturuePage;
	protected static String chk_batchSelectProductConversion_IN_IO_069;

	/* IN_IO_070 */
	protected static String td_availableStockNewlyCreatedWarehouse;

	/* IN_IO_072 */
	protected static String drop_dispatchOnlySelectionSeriel;
	protected static String btn_lookupProductsMultipleProductGrids;
	protected static String btn_genarateProductBarcode;
	protected static String btn_viewProductBarcode;
	protected static String btn_expiryDateBArcodeGenarate;
	protected static String btn_day28barcodeGenarator;
	protected static String btn_dateNextBArcodeGenarator;
	protected static String select_serelBookBarcodeGenarate;
	protected static String btn_removeSuccessfulMessage;
	protected static String btn_backBarcodeGenSerielBatchCaptureWindow;

	/* IN_IO_074 */
	protected static String btn_addRackPlusMark;
	protected static String txt_rackName;
	protected static String btn_updateRacks;
	protected static String div_updateSuccessfullValidation;
	protected static String drop_rackRackTransfer;
	protected static String drop_destinationRackRackTransfer;
	protected static String input_rackTarnsferQunatity;
	protected static String btn_destityRackSelectionMenuLastRow;
	protected static String chk_allProductsRackTransfer;
	protected static String drop_rackSelectStockAdjustment;
	protected static String btn_applyDestinationRack;

	/* IN_IO_075 */
	protected static String btn_plusMarkProductAddStockTake;
	protected static String btn_productLookupStockTake;
	protected static String txt_physicalBalanceStockTakeSearchedProduct;
	protected static String btn_productApplyStockTake;
	protected static String div_forVerifyPlusStockAdjustmentLoaded;
	protected static String txt_systemBalanceStockTakeSearchedProduct;

	/* IN_IO_076_077_078 */
	protected static String row_relesedDetailsHistory;
	protected static String row_draftDetailsHistory;
	protected static String row_updateDetailsHistory;
	protected static String header_docflowDocumentInternelOrder;

	/* IN_IRO_Emp_001_002_003_004_005 */
	protected static String heder_internelReturnOrderByPage;
	protected static String heder_journeyPopup;
	protected static String heder_internelReturnOrderNewForm;
	protected static String btn_employeeReturnsJourney;
	protected static String lnk_resultEmployeeCommon;
	protected static String btn_productLoockupsForVerifyAvailabiliyt;

	/* IN_IRO_Emp_006 */
	protected static String lnk_commonResultEmployee;
	protected static String td_countProductReleventToIDO;

	/* IN_IRO_Emp_007 */
	protected static String txt_searchByProductIRO;
	protected static String txt_CommonSearchIRO;

	/* IN_IRO_Emp_008 */
	protected static String chk_productsReleventToIDO;
	protected static String lbl_productIROGrid;
	protected static String div_selectedRowProductColumIRO;

	/* IN_IRO_Emp_009 */
	protected static String div_allrowsByRowProductColumnProductSelectIRO;

	/* IN_IRO_Emp_010 */
	protected static String btn_gridClearConfirmationIRO;
	protected static String header_clearConfirmationIRO;
	protected static String row_countSelectedProductsIRO;

	/* IN_IRO_Emp_011 */
	protected static String btn_deleteFirstRecordIRO;

	/* IN_IRO_Emp_012 */
	protected static String btn_advanceIDO;
	protected static String btn_serielBatchTabAdvanceWindow;
	protected static String lbl_lotAdvanceMenu;
	protected static String lbl_batchAdvanceMenu;
	protected static String btn_closeWindowPopup;
	protected static String txt_quantityIROGrid;
	protected static String lbl_lotNoIROGrid;
	protected static String lbl_batchNoIROGrid;
	protected static String lbl_expiryDateIROGrid;

	/* IN_IRO_Emp_013 */
	protected static String header_newInternalReturnOrder;

	/* IN_IRO_Emp_014 */
	protected static String header_andHeaderStatusDarftedIntrnalReturOrder;

	/* IN_IRO_Emp_017 */
	protected static String btn_deleteRowInternelReturnOrder;
	protected static String btn_availableProdctCountIRO;
	protected static String lbl_productRowsDraftedIRO;
	protected static String chk_differentProductIRO;
	protected static String lbl_getProductIROGrid;
	protected static String btn_appplyDocumentListEditIRO;

	/* IN_IRO_Emp_019 */
	protected static String header_newInternalRecipt;

	/* IN_IRO_Emp_020 */
	protected static String btn_internelReceiptTile;

	/* IN_IRO_Emp_023 */
	protected static String btn_advanceMenuRowDynamic;
	protected static String btn_advanceMenuLogisticSection;
	protected static String chk_underDeliveryAdvanceMenu;
	protected static String chk_partialDeliveryAdvanceMenu;
	protected static String div_changeCssAdvanceMenu;
	protected static String txt_quantityInternalReceiptGrid;
	protected static String txt_planedQuantityInternalReceiptGridAfterUpdate;

	/* IN_IRO_Emp_044 */
	protected static String header_docflowSecondDocument;
	protected static String header_docflowThirdDocument;

	/* IN_TO_001 */
	protected static String header_transferOrderByPage;

	/* IN_TO_003 */
	protected static String txt_fromWarehouseAddressFrontPage;

	/************** Regression-Transfer Order *********/
	/* common */

	/* IN_TO_005 */
	protected static String txt_addressWarehouseCreation;
	protected static String txt_destinationAddressTransferOrderFrontPage;

	/* IN_TO_006 */
	protected static String btn_addressLookupFromWarehouse;
	protected static String btn_addressLookupToWarehouse;
	protected static String btn_applyAddress;
	protected static String txt_addressOnLookup;
	protected static String header_addressLookup;

	/* IN_TO_008 */
	protected static String txt_autoSearchQuantity;
	protected static String txt_addedProductInGridSingleProduct;
	protected static String txt_qtyTransferOrder;

	/* IN_TO_010 */
	protected static String row_multipleRowSelectionOnProductLookup;
	protected static String txt_addedProductInGridMultipleProduct;

	/* IN_TO_011 */
	protected static String div_defoultUOMNainGridTransferOrder;

	/* IN_TO_012 */
	protected static String div_ohQuantityToTransferOrder;
	protected static String div_ohQuantityFromTransferOrder;

	/* IN_TO_013 */
	protected static String txt_transferCostOnTransferOrderGrid;

	/* IN_TO_014 */
	protected static String btn_dismissError;
	protected static String lbl_validationFromWarehouseMissing;
	protected static String lbl_validationDestinationWarehouseMissing;
	protected static String lbl_validationProductMissing;
	protected static String lbl_validationProductQtyMissing;

	/* IN_TO_015 */
	protected static String header_andHeaderStatusDarftedTransferOrder;

	/* IN_IO_016 */
	protected static String header_newTransferOrderVerify;
	protected static String dropdown_templateFilter;
	protected static String btn_searchTransferOrder;
	protected static String lbl_newlyDraftedTransferOrderDocNo;
	protected static String txt_searchTransferOrder;
	protected static String lnk_documentOnByPageTransferOrder;

	/* IN_TO_017 */
	protected static String btn_clearConfimationEditModeTransferOrder;
	protected static String lbl_fromWareDraftedTransferOrder;
	protected static String lbl_destinationWareDraftedTransferOrder;
	protected static String lbl_productDraftedTransferOrder;
	protected static String lbl_qtyWareDraftedTransferOrder;

	/* IN_TO_020 */
	protected static String header_newOutboundShiment;
	protected static String header_newInboundShiment;
	protected static String lbl_statusConfirmation;

	/* IN_TO_021 */
	protected static String btn_outboundShipmentTile;

	/* IN_TO_022 */
	protected static String btn_inboundShipmentTile;

	/* IN_TO_025 */
	protected static String header_releaseTransferOrder;
	protected static String div_actionButtopnArea;

	/* IN_TO_028 */
	protected static String header_releaseOutboundShipment;

	/* IN_TO_031 */
	protected static String header_releasedStatusInternalDispatchOrder;

	/* IN_TO_032 */
	protected static String btn_availabilityWidgetRowReplace;

	/* IN_TO_034 */
	protected static String btn_journeyConfigTransferOrder;

	/* IN_TO_036_038 */
	protected static String btn_advanceWidget;
	protected static String btn_inventoryReports;
	protected static String lbl_unitCostBincardReport;
	protected static String dropdown_templateBinCardReports;
	protected static String btn_viewBinCardReport;
	protected static String lbl_filteredProduct;
	protected static String txt_lotLotProducts;
	protected static String lbl_lot02AdvanceMenu;
	protected static String input_transactionNoBincardReports;
	protected static String btn_quickPreviewBincardReports;
	protected static String lbl_genarelInventoryCreditedValue;
	protected static String lbl_genarelInventoryDebitedValue;

	/* IN_TO_040 */
	protected static String chk_selectCaptureSeriels;
	protected static String txt_plabnedQuantityInboundShipment;
	protected static String btn_closeReleasedReferenceDocument;

	/* IN_TO_041 */
	protected static String txt_plabnedQuantityOutboundShipment;
	protected static String btn_closeGoToNextPageWindow;

	/* IN_TO_042 */
	protected static String btn_deleteRecordAccordingOutbondShipment_IN_IO_042;

	/* IN_TO_046 */
	protected static String td_productAvailabilytyAccordingToTheBatchAndWare;
	protected static String btn_batchWiseCostSecondMenu;
	protected static String lbl_secondBAtchNumberInSpecificCaptureList;

	/* IN_TO_049 */
	protected static String drop_reportCategory;
	protected static String txt_productSeearchBinReports;
	protected static String lbl_remainingLotWarehouseBincardReports;
	protected static String lbl_corrrectRemainingQuntityAccordingToLot;
	protected static String lbl_corrrectRemainingQuntityAccordingToFirstLot;
	protected static String lbl_issuedQtyAccordingToLotTen;
	protected static String lbl_issuedQtyAccordingToLotFive;

	/* IN_TO_050 */
	protected static String div_serleBatchLotExpiryCaptured;
	protected static String btn_calenderBinCardReports;
	protected static String btn_todayCalenderBincardReport;
	protected static String txt_transactionNoWhenDoableConditions;

	/* IN_TO_052 */
	protected static String txt_postDateInside;
	protected static String btn_dateFifteen;
	protected static String lbl_validateBeforePostDateOutboundShipmentTransferOrder;
	protected static String record_batchLotExpiry;

	/* IN_TO_055 */
	protected static String lbl_gridErrorQuantityNotAvailableSelectedPostDate;

	/* IN_TO_059_060 */
	protected static String txt_reservedQtyAdvanceMenuTO;

	/* IN_TO_061 */
	protected static String headre_transferOrderClosedStatus;

	/* IN_TO_062 */
	protected static String header_docflowDocumentTransferOrderJoueney;

	/* IN_TO_067 */
	protected static String header_releasedTransferOrder;

	/* IN_TO_068 */
	protected static String header_releasedPurchaseInvoice;

	/* IN_TO_070 */
	protected static String btn_outboundLoanOrderToOutboundShipmentJourney;

	/* IN_TO_073 */
	protected static String header_releasedSalesReturnOrder;
	protected static String lbl_debitTotalJournelEntry;
	protected static String lbl_creditTotalJournelEntry;

	/* IN_TO_074 */
	protected static String div_capturedQuntityTransferOrder_IN_TO_074;

	/* IN_TO_075 */
	protected static String div_pruductOnlyOnGrid;

	/* IN_TO_076 */
	protected static String div_descriptionOnlyOnGrid;

	/* IN_TO_077 */
	protected static String div_codeAndDescOnGrid;

	/* IN_TO_078 */
	protected static String drop_warehouseConfigrationBalancingLavel;
	protected static String lbl_fromWarehouseTransferOrderfterReleased;
	protected static String lbl_toWarehouseOutboundShipmentReleased;
	protected static String lbl_toWarehouseTransferOrderfterReleased;
	protected static String lbl_toWarehouseInboundShipmentReleased;
	protected static String btn_updateGenaralTabBalancingLavelSetting;

	/* IN_TO_081_082 */
	protected static String header_journeyConfiguration;
	protected static String btn_activeJourney;
	protected static String btn_inactiveJourney;
	protected static String btn_yesConfirmationJourney;
	protected static String validator_noPermissionForTheJourney;
	protected static String header_newStatusTransferOrder;

	/* IN_TO_083 */
	protected static String chk_autoGenarateOutboundShipment;
	protected static String btn_gridErrosFirstRow;
	protected static String lbl_gridErroeBallonSerielBatchCapture;
	protected static String lbl_autoGenaratedOutboundShipment;

	/* IN_TO_084 */
	protected static String chk_autoGenarateInboundShipment;
	protected static String lbl_autoGenaratedInboundShipment;

	/* IN_TO_086 */
	protected static String btn_rowReplaceAndClickGridErrorOutboundShipment;
	protected static String div_validation_IN_TO_086;

	/* IN_TO_088 */
	protected static String div_errorQuantityNotAvailableInGridErros;
	protected static String btn_firstClickGridErrorOutboundShipment;

	/* IN_TO_090 */
	protected static String btn_updateJourneyConfigurationTransferOrder;
	protected static String header_releasedStatusTransferOrder;
	protected static String lbl_zeroReservedQuantity;

	/* IN_TO_092 */
	protected static String lbl_reservertionInAllocationManagerAccordingToTheProduct;

	/* IN_TO_094 */
	protected static String header_selectedProductInOutboundShipmentGrid;

	/* IN_TO_095 */
	protected static String chk_allocationProductAllocationManager;
	protected static String btn_productDivAllocationManager;
	protected static String txt_reservedQuantityAllocationManagerWindow;
	protected static String btn_checkoutAllocationMangerWindow;
	protected static String btn_updateAllocationManagerWindow;
	protected static String lbl_releasedStatusOutboundShipment;

	/* IN_TO_096 */
	protected static String txt_reservedQtyOnGridTransferOrder;

	/* IN_TO_097 */
	protected static String para_validationReserveQuntityAdvanceMenuTransferOrder;
	protected static String btn_closeAdvanceMenuLast;

	/* IN_TO_099 */
	protected static String txt_reserveQuantityAdvanceMenuTransferOrder;
	protected static String btn_applyLast;

	/* IN_TO_101 */
	protected static String txt_failQuantityWhnMultiplePrductQC;
	protected static String div_qcStatusFailCount_IN_TO_101;

	/* IN_TO_102 */
	protected static String btn_deleteQCAcceptanceJourneys;
	protected static String span_countOfJourneyInQCAcceptance;
	protected static String btn_thisWeekBincardReports;
	protected static String div_qcStatusPendingCount_IN_TO_102;
	protected static String div_qcStatusPassCount_IN_TO_102;
	protected static String div_quantityBincardQCRowReplace_IN_TO_102;
	protected static String btn_serielNosQCAcceptanceRowReplace;
	protected static String txt_remainingQuantityQC;
	protected static String txt_passQuantityQcAceptanceWhenMultipleProducts;
	protected static String drop_qcStatusQCAcceptance;
	protected static String btn_applyQCStatusSerielBatch;
	protected static String txt_remainingQCRowsVerify;
	protected static String div_headerQCStatusBinCard;
	protected static String div_headerQCBalanceQuantityBinCard;

	/* IN_TO_103 */
	protected static String drop_passFailSerleIncludedProducts;
	protected static String div_headerRecievedQuantity;
	protected static String div_checkMultipleStatusOfQcDocument;

	/* IN_TO_105 */
	protected static String div_qcPendingDocument;

	/* IN_TO_106 */
	protected static String span_historyQCAcceptance;
	protected static String span_closeButtonLast;
	protected static String span_historyQCAcceptanceAfterSeletc;
	protected static String txt_failQuantityWhnMultiplePrductQCHistoryTab;
	protected static String txt_passQuantityQcAceptanceWhenMultipleProductsHistoryTab;

	/* IN_TO_113 */
	protected static String div_selectedInboundShipmentShipmentAcceptance;
	protected static String tr_countShipmentAcceptance;
	protected static String div_productShipmentAcceptance;

	/* IN_TO_114 */
	protected static String span_countOfJourneyInShipmentAcceptance;
	protected static String header_releaseForPickingInboundShipment;

	/* IN_TO_115 */
	protected static String lbl_quantityCaptureAvailableBatchSpecific;
	protected static String div_validationMessageBackBd;

	/* IN_TO_116 */
	protected static String para_inboundShipmentSuccessfullyReleased;
	protected static String chk_releseAllShipmentAcceptance;
	protected static String btn_releseAllShipmentAcceptance;
	protected static String para_validationSuccessfullySaved;
	protected static String btn_dismissSucessValidation;

	/* IN_TO_118 */
	protected static String chk_releseShipmentAcceptanceRowReplace;

	/* IN_TO_122 */
	protected static String dropdown_selectOutboundShipmentAcceptanceJourney;
	protected static String span_countOfJourneyShipmentAcceptanceOutboundShipment;
	protected static String btn_deleetOutboundShipmentAceptance;
	protected static String header_releaseForPickingOutboundShipment;
	protected static String tab_outbounsShipmentAcceptance;
	protected static String txt_docNoOutboundShipmentShipmentAcceptance;
	protected static String div_selectedOutboundShipmentShipmentAcceptance;

	/* IN_TO_124_125_128 */
	protected static String para_outboundShipmentSuccessfullyReleased;

	/* Stock Take */
	/* IN_ST_001 */
	protected static String header_stockTakeByPage;

	/* IN_ST_002 */
	protected static String heade_newStockTakeForm;

	/* IN_ST_003 */
	protected static String drop_warehouseStockTakeWithErrorBorder;
	protected static String lbl_selectWarehouseValidationStockTake;
	protected static String span_refreshErrorBorder;
	protected static String lbl_productRefreshErrorMessage;

	/* IN_ST_004 */
	protected static String div_productVerfiStockTakePage;
	protected static String row_countStockTakePage;

	/* IN_ST_008_009 */
	protected static String btn_addProductStockTake;
	protected static String header_addProductLookupStockTake;
	protected static String drop_rackSelectStockTake;
	protected static String td_productOnGridStockTake;
	protected static String td_rackOnGridStockTake;
	protected static String td_defoultUOMOnGridStockTake;
	protected static String td_systemBalanceOnGridStockTake;
	protected static String lbl_selectedProductOnPopupStockTake;
	protected static String headers_gridStockTake;

	/* IN_ST_011 */
	protected static String td_physicalBalanceOnGridStockTake;
	protected static String header_physicalBalanceStockTake;
	protected static String txt_physicalBalanceOnGridStockTake;

	/* IN_ST_013 */
	protected static String headers_stockTakDraftedStatusVerification;
	protected static String td_varienceQtyOnGridStockTake;

	/* IN_ST_014 */
	protected static String td_adjustedQtyOnGridStockTake;

	/* IN_ST_015 */
	protected static String txt_physycalBalanceEditModeAfterSearch;
	protected static String txt_serachProductEditModeStockTake;

	/* IN_ST_019 */
	protected static String header_newStockTake;
	protected static String btn_searchForms;
	protected static String lbl_recentlyDraftedStockTakeDocNo;
	protected static String lbl_documentDate;
	protected static String txt_searchStockTake;
	protected static String lnk_releventStockTakeOnByPage;
	protected static String header_draftedStockTakeReplaceDocNo;

	/* IN_ST_032 */
	protected static String btn_deleteEmotyRowStockAdjustmentRowTwo;
	protected static String div_validationStokTakeAlreadyDrafted;

	/* IN_ST_034 */
	protected static String btn_convertToStockAdjustmentStockTake;
	protected static String header_newStockAdjustment;
	protected static String div_productOnGridStockAdjustmentReplaceProduct;
	protected static String txt_quntityOnGridStockAdjustmentReplaceProduct;
	protected static String header_draftStatusStockAdjustment;
	protected static String headers_releasedStatusStockAdjustment;
	protected static String headers_stockAdjustmentGenarateFromStockTake;
	protected static String txt_refDocStockAdjustmentGenarateFromStockTake;

	/* IN_ST_039 */
	protected static String hedere_relesedStockTakeDocReplace;

	/* IN_ST_040_041 */
	protected static String btn_deleteRecordOnGrid;
	protected static String row_countInGridStockAdjustment;
	protected static String txt_quantityAccordingToTheProduct;

	/* IN_ST_046 */
	protected static String chk_serielSelectionBatchProducts;
	protected static String btn_dockIconBatchProduct;

	/* IN_ST_047 */
	protected static String txt_searchStockAdjustmentOnByPage;
	protected static String lnk_stockAdjustmentOnByPageDocReplace;
	protected static String para_validatorStockAdjustmentAlreadyUsed;

	/* IN_ST_048 */
	protected static String lbl_docNoRecentlyDraftedStockAdjustment;

	/* IN_ST_049 */
	protected static String div_descriptionOfDraftedStockAdjustment;

	/* IN_ST_051 */
	protected static String btn_deleteLastRowOfTheGrid;

	/* IN_ST_057 */
	protected static String header_draftedInterdepartmentalTransferOrder;
	protected static String header_draftedOutboundShipment;
	
	/* IN_ST_058 */
	protected static String drop_rackSelectStockAdjRowReplace;
	
	/* IN_ST_059 */
	protected static String div_productOnRelesedStockAdjustment;
	protected static String div_productOnRelesedStockTake;

	/* IN_ST_062 */
	protected static String lbl_warehouseOnReleasedDoc;
	
	/* Product Conversion */
	/* IN_PC_001 */
	protected static String header_productConversionByPage;

	/* IN_PC_002 */
	protected static String header_newProductConversion;

	/* IN_PC_004 */
	protected static String label_requiredFildAsteriskProductConversionWarehouse;
	protected static String drop_warehouseWithErrorBorder;
	protected static String lbl_errorMessageWarehouseSelectionProductConversion;
	protected static String header_productLookupProductConversion;

	/* IN_PC_005 */
	protected static String div_productOnGridProductConversation;

	/* IN_PC_006 */
	protected static String div_uomOnGridProductConversion;
	protected static String header_verifyProductConversionForm;

	/* IN_PC_007 */
	protected static String btn_productMasterWidget;
	protected static String td_ohQuantityProductConversionFirstRowWhenSelectSingleProduct;

	/* IN_PC_008 */
	protected static String btn_productRelatedPriceOnWidgetMaster;
	protected static String lbl_costPrice;
	protected static String headers_productConversionGrid;
	protected static String td_lastCostPriceProductConversionGrid;
	
	/* IN_PC_009 */
	protected static String btn_serielBatchCaptureNosProductConversionRowReplace;
	protected static String chk_selectBatchForBatchSpecificProduct;
	protected static String txt_capturedQuantityProductConversion;
	protected static String btn_appalySerielBatchesFromProductProductConversion;
	protected static String header_batchCaptureWindowProductConversion;
	protected static String header_serileCaptureWindowProductConversion;
	protected static String btn_productLookupRowReplaceProductConversion;
	protected static String txt_quantityRowReplaceProductConversionFromProduct;
	protected static String btn_addNewRowProductConversion;
	protected static String chk_serielRangeProductConversion;
	protected static String txt_serielCountProductConversion;
	
	/* IN_PC_010 */
	protected static String btn_productLookupRowReplaceProductConversionToProduct;
	
	/* IN_PC_011 */
	protected static String btn_statusMenu;
	protected static String div_toProductOnGridRowReplace;
	
	/* IN_PC_012 */
	protected static String div_defoultUomToProductProductConversion;
	
	/* IN_PC_013 */
	protected static String txt_costPriceProductConversion;
	
	/* IN_PC_014 */
	protected static String txt_conRateProductConversion;
	protected static String txt_quantityToProductProductConversion;
	
	/* IN_PC_019 */
	protected static String txt_menufactureDate;
	
	/* IN_PC_020 */
	protected static String div_transparent;
	protected static String error_invalidSerielNo;
	
	/* IN_PC_021 */
	protected static String error_commonSerielNos;
	protected static String icon_rowErrorsSerielCaptureWindow;
	protected static String div_rowErrorBaloonSerielCaptureWindow;
	
	/* IN_PC_022 */
	protected static String error_fromProductRequired;
	protected static String error_toProductRequired;
	protected static String error_warehouseValidation;
	
	/* IN_PC_023_025_029_030 */
	protected static String header_drftedProductConversion;
	protected static String header_releasedProductConversion;
	protected static String headers_serialBatchNosUptoExpiryDateProductConversion;
	protected static String txt_disabledExpiryDateReleasedProductConversion;
	
	/* IN_PC_024 */
	protected static String div_gridErrorMessageDuplicateProductProductConversion;
	protected static String div_visibleGridBaloon;
	protected static String div_toProductWithErrorBorderProductConversion;
	
	/* IN_PC_026 */
	protected static String txt_fromQuantityWithErrorBorder;
	protected static String error_cantExceedOH;
	
	/* IN_PC_028 */
	protected static String txt_costPriceProductConversionRowReplace;
	protected static String div_productAccordingToRowProductConversionFromProduct;
	protected static String div_productAccordingToRowProductConversionToProduct;
	protected static String tr_countProductConversion;
	protected static String lbl_warehouseProductConversion;
	
	/* IN_PC_034 */
	protected static String count_rowProductConversionGrid;
	protected static String txt_capturedQuantityAfterDraftProductConversion;
	
	/* IN_PC_037 */
	protected static String headers_existingSerialBatchNos;
	protected static String div_batchInExistingSerialBatchesBatchReplace;
	protected static String btn_closeBatchNos;
	
	/* IN_PC_039 */
	protected static String txt_batchNoProductConversionToProduct;
	protected static String td_capturedLotOnSerielNos;
	protected static String headers_upTpToLotCapturedSerielBatchToProductProductConversion;
	
	/* IN_PC_040 */
	protected static String txt_qtyToProductProductConcersionRowReplace;
	protected static String txt_batchNoToProductProductConversion;
	protected static String txt_expiryDateToProductProductConversion;
	protected static String txt_lotNoToProductProductConversion;
	protected static String txt_menufacDateToProductProductConversion;
	protected static String chk_batchSelectionProductConversion;
	protected static String header_reversedProductConversion;
	
	/* IN_PC_041_051 */
	protected static String txt_lotNumber;
	
	/* IN_PC_042 */
	protected static String lbl_recentlyDraftedProductConversion;
	protected static String lnk_releventDocumentOnByPage;
	protected static String header_draftedProductConversionReplaceDocNo;
	protected static String txt_searchProductConversion;
	
	/* IN_PC_043 */
	protected static String lnk_resultDocumentOnLookup;
	
	/* IN_PC_054 */
	protected static String div_firstSerialBarcodeGeanarateView;
	
	/* IN_PC_057 */
	protected static String div_productAccordingToRowRelesedProductConversionFromProduct;
	protected static String div_productAccordingToRowRelesedProductConversionToProduct;
	
	/* IN_PC_063 */
	protected static String btn_journeyConfigProductConversion;

	// ============================================RegressionAruna======================================================================
	// IN_SA_001
	protected static String txt_pageHeaderLableA;

	// IN_SA_002
	protected static String btn_AdministrationA;
	protected static String btn_JourneyConfigurationA;
	protected static String btn_ProcessStatusA;
	protected static String btn_YesA;
	protected static String btn_JourneyConfigurationUpdateA;

	// IN_SA_003
	protected static String txt_PreparerA;
	protected static String txt_DescriptionA;
	protected static String txt_WarehouseA;
	protected static String txt_AdjustmentGroupA;
	protected static String txt_ProductA;

	// IN_SA_005
	protected static String btn_ProductAddA;
	protected static String txt_ProductAddTxtA;
	protected static String sel_ProductAddSelA;

	// IN_SA_007
	protected static String btn_BalancingLevelSettingsA;
	protected static String btn_GeneralA;
	protected static String sel_selectWarehouseSerchTypeA;

	// IN_SA_011
	protected static String btn_InactiveAdjustmentGroupA;
	protected static String btn_AddAdjustmentGroupA;
	protected static String btn_AddAdjustmentGroupUpdateA;

	// IN_SA_012
	protected static String btn_AdjustmentGroupAddA;
	protected static String txt_AdjustmentGroupTxtA;

	// IN_SA_013
	protected static String txt_ProductLoadTxtA;
	protected static String txt_ProductQtyA;

	// IN_SA_015
	protected static String sel_ConfigProA;
	protected static String btn_ConfigProUpdateA;

	// IN_SA_017
	protected static String btn_ProductAddSelDeleteA;

	// IN_SA_018
	protected static String txt_ProductAddQtyA;
	protected static String txt_ProductAddAfterQtyA;

	// IN_SA_019
	protected static String txt_ProductSerchA;
	protected static String sel_ProductSerchSelA;
	protected static String btn_AllowDecimalA;

	// IN_SA_022
	protected static String btn_Product2A;
	protected static String btn_ProductAdd2A;

	// IN_SA_023
	protected static String sel_pro1A;
	protected static String sel_pro2A;
	protected static String sel_pro3A;
	protected static String txt_tval1A;
	protected static String txt_tval2A;
	protected static String txt_tval3A;

	// IN_SA_024
	protected static String btn_ExDescriptionA;
	protected static String txt_ExDescriptiontxtA;

	// IN_SA_025
	protected static String txt_DefaultUOMA;
	protected static String txt_DefaultUOMtxtA;

	// IN_SA_026
	protected static String txt_OnHandQtyA;

	// IN_SA_027
	protected static String txt_ReservedQtyA;

	// IN_SA_028
	protected static String btn_RackA;

	// IN_SA_029
	protected static String txt_pageDraftA;
	protected static String txt_pageReleaseA;

	// IN_SA_030
	protected static String txt_selectWarehouseWarningA;

	// IN_SA_034
	protected static String txt_HeaderA;
	protected static String btn_StockAdjustmentByPageA;
	protected static String txt_StockAdjustmentSerchA;
	protected static String sel_StockAdjustmentSerchselA;

	// IN_SA_036
	protected static String txt_CostPerUnitA;

	// IN_SA_038
	protected static String sel_StockAdjustmentSerchCopyselA;

	// IN_SA_039
	protected static String txt_filterListIdA;

	// IN_SA_042
	protected static String btn_ProductAvailabilityA;
	protected static String txt_ProductAvailabilityValueA;
	protected static String btn_ReportsA;
	protected static String btn_ViewA;
	protected static String txt_ProductAvailabilityAfterValueA;

	// IN_SA_043
	protected static String btn_reversebtnA;

	// IN_SA_046
	protected static String txt_ProductAddQty2A;
	protected static String btn_MasterInfo2A;
	protected static String btn_AfterMasterInfo2A;

	// IN_SA_047
	protected static String btn_SerialBatchA;
	protected static String btn_ItemNumber1A;
	protected static String txt_SerialNoA;
	protected static String txt_LotNoA;
	protected static String txt_ExpiryDateA;
	protected static String txt_ExpiryDateNextYearA;
	protected static String sel_ExpiryDateSelA;
	protected static String btn_updateSerialA;
	protected static String btn_closeProductList;
	protected static String btn_ProcurementA;
	protected static String btn_PurchaseOrderA;
	protected static String btn_NewPurchaseOrderA;
	protected static String btn_PurchaseOrdertoPurchaseInvoiceA;
	protected static String btn_VendorbtnA;
	protected static String txt_VendortxtA;
	protected static String sel_VendorselA;
	protected static String btn_SerialRangeA;
	protected static String txt_SerialCountA;
	protected static String txt_ValidatorReverceA;

	// IN_SA_048
	protected static String txt_historyForDraftA;
	protected static String txt_historyForReleasedA;

	// IN_SA_049
	protected static String txt_CostPerUnit2A;

	// IN_SA_050
	protected static String txt_CostPerUnitAfterValueA;
	protected static String btn_ProductRelatedPriceA;
	protected static String txt_ProductRelatedPriceValueA;

	// IN_SA_051
	protected static String btn_RackwiseAvailableQtyA;
	protected static String txt_RackwiseAvailableQtyValueA;

	// IN_SA_052
	protected static String btn_ClassificationA;
	protected static String btn_VariantGroupbtnA;
	protected static String txt_VariantGrouptxtA;
	protected static String sel_VariantGroupselA;
	protected static String txt_VariantGroupGradeA;
	protected static String btn_VariantUpdateA;

	// IN_SA_053
	protected static String txt_ValidatorReleasedA;

	// IN_SA_054
	protected static String btn_ItemNumber2A;

	// IN_SA_057
	protected static String txt_Racktxt1A;
	protected static String txt_Racktxt2A;

	// IN_SA_059
	protected static String btn_SalesAndMarketingA;
	protected static String btn_SalesOrderA;
	protected static String btn_NewSalesOrderA;
	protected static String btn_SalesOrderToSalesInvoiceA;
	protected static String btn_CustomerAccountA;
	protected static String sel_CustomerAccountselA;
	protected static String txt_WarehouseSalesA;
	protected static String txt_pageSalesDraftA;
	protected static String txt_pageSalesReleasedA;
	protected static String txt_BatchNoSalesA;
	protected static String btn_SalesReturnOrderA;
	protected static String btn_NewSalesReturnOrderA;
	protected static String btn_SalesReturnOrderToSalesReturnInvoiceA;
	protected static String btn_CustomerAccountSalesReturnA;
	protected static String txt_SalesReturnSearchByA;
	protected static String txt_SalesReturnSearchBytxtA;
	protected static String btn_SalesReturnSearchA;
	protected static String sel_SalesReturnSearchSelA;
	protected static String btn_SalesReturnSearchApplyA;
	protected static String txt_changeDispositionA;
	protected static String txt_ReturnReasonA;
	protected static String txt_UnitPriceSalesReturnInvoiceA;

	// IN_SA_060
	protected static String btn_NewInternalOrderA;
	protected static String btn_EmployeeRequestsA;
	protected static String btn_RequesterBtnA;
	protected static String sel_RequesterselA;
	protected static String txt_WarehouseIOA;
	protected static String btn_NewInternelReturnOrderA;
	protected static String btn_EmployeeReturnsA;
	protected static String txt_CommonSearchIROA;
	protected static String sel_IROSearchSelA;
	protected static String btn_ViewLinkA;

	// IN_SA_063
	protected static String btn_NewInterdepartmentalTransferOrderA;

	// IN_SA_064
	protected static String sel_dateRangeBarcodeGeneratorPageSelA;
	protected static String sel_inboundShipmentBarcodeGeneratorSelA;
	protected static String btn_GenerateViewA;
	protected static String txt_GeneraterBatchNoA;
	protected static String txt_GeneraterLotNoA;

	// IN_SA_066
	protected static String txt_DocProductA;
	protected static String txt_DocProductRackA;
	protected static String sel_DocselA;
	protected static String btn_DocApplyA;
	protected static String txt_NewTransferRackA;
	protected static String btn_NewTransferRackApplyA;

	// IN_SA_067
	protected static String btn_ProductStockTakebtnA;
	protected static String btn_AddProductStockTakebtnA;
	protected static String txt_ProductStockTakeRackA;
	protected static String txt_SysBalA;
	protected static String txt_PhyBalA;
	protected static String txt_StockAdjQuan;

	// IN_SA_068
	protected static String txt_BVal1A;
	protected static String txt_BVal2A;

	// IN_SA_069
	protected static String btn_PostDateSAJA;
	protected static String btn_PostDateApplySAJA;

	// IN_SA_070
	protected static String txt_ValidatorCapA;

	// IN_SA_073
	protected static String sel_ExpiryDateSel2A;
	protected static String txt_ValidatorExpireDateA;

	// IN_SA_076
	protected static String txt_ManufactureDateA;
	protected static String txt_ValidatorManufactureDateA;

	// IN_IRO_Emp_047
	protected static String btn_AutoGenerateInternalReceiptsCheckboxA;
	protected static String btn_JourneyConfUpdateA;

	// IN_IRO_Emp_048
	protected static String txt_IROprotextA;
	protected static String txt_IRprotextA;

	// IN_IRO_Emp_051
	protected static String txt_WarehouseIRA;

	// IN_IRO_Emp_054
	protected static String sel_selectEmployeeSerchTypeA;
	protected static String txt_EmployeeIRO;
	protected static String txt_EmployeeIR;

	// IN_IRO_Emp_029
	protected static String sel_IROSearchSel2A;
	protected static String txt_IROqty1A;
	protected static String txt_IROqty2A;
	protected static String txt_serchIROA;
	protected static String sel_serchSelIROA;

	// IN_IRO_Emp_033
	protected static String btn_InternalReceiptsA;
	protected static String txt_InternalReceiptsSerchA;
	protected static String sel_InternalReceiptsSerchSelA;

	// IN_IRO_Emp_036
	protected static String txt_WarehouseIO1A;
	protected static String txt_WarehouseIO2A;
	protected static String txt_WarehouseIO3A;
	protected static String txt_WarehouseIO4A;
	protected static String txt_WarehouseIO5A;
	protected static String txt_WarehouseIO6A;
	protected static String txt_WarehouseIO7A;

	protected static String txt_BatchNuA;
	protected static String txt_BatchLotNuA;
	protected static String txt_BatchExpiryDateA;
	protected static String txt_SerialExpiryDateA;
	protected static String sel_BatchExpiryDateSelA;
	protected static String txt_SerialFromNuA;
	protected static String txt_SerialLotNuA;
	protected static String txt_SerialBatchNuA;

	protected static String btn_ItemNumber3A;
	protected static String btn_ItemNumber4A;
	protected static String btn_ItemNumber5A;
	protected static String btn_ItemNumber6A;
	protected static String btn_ItemNumber7A;

	protected static String sel_IROSearchSel3A;
	protected static String sel_IROSearchSel4A;
	protected static String sel_IROSearchSel5A;
	protected static String sel_IROSearchSel6A;
	protected static String sel_IROSearchSel7A;

	protected static String txt_LotNamePro1A;
	protected static String txt_BatchNamePro1A;
	protected static String txt_LotNamePro2A;
	protected static String txt_BatchNamePro2A;
	protected static String txt_LotNamePro3A;
	protected static String txt_LotNamePro4A;
	protected static String txt_LotNamePro5A;
	protected static String txt_BatchNamePro5A;
	protected static String txt_LotNamePro6A;
	protected static String txt_BatchNamePro6A;
	protected static String txt_LotNamePro7A;

	protected static String btn_AdvanceWidget1A;
	protected static String btn_AdvanceWidget2A;
	protected static String btn_AdvanceWidget3A;
	protected static String btn_AdvanceWidget4A;
	protected static String btn_AdvanceWidget5A;
	protected static String btn_AdvanceWidget6A;
	protected static String btn_AdvanceWidget7A;
	protected static String btn_SerialBatchTabA;
	protected static String txt_LotNoFieldA;
	protected static String txt_BatchNoFieldA;

	// IN_IRO_Emp_037
	protected static String btn_GridMasterInfo1A;
	protected static String btn_GridMasterInfo2A;
	protected static String btn_DeleteRaw2A;
	protected static String btn_GridMasterInfo1IRA;
	protected static String btn_EntutionLogoA;
	protected static String btn_TaskEventA;
	protected static String btn_InternalReceiptsTileA;
	protected static String txt_InternalReceiptsSerchInTaskEventA;
	protected static String btn_InternalReceiptsBlueArrowA;
	protected static String btn_headerCloseA;

	// IN_IRO_Emp_042
	protected static String btn_PostDateA;
	protected static String btn_PostDatetxtA;
	protected static String btn_PostDateMonthA;
	protected static String txt_PostDateValidatorIROA;
	protected static String btn_PostDateApplyA;
	
	/* Smoke Production */
	protected static String txt_externelDescriptionProductInformation;
	protected static String txt_basePriceProductInformation;
	protected static String txt_genaricNameProductInformation;
	protected static String drop_mnufacturingTypeProductInformation;

	/* Reading the Data to variables */
	// Login
	protected static String siteURL;
	protected static String userNameData;
	protected static String passwordData;
	protected static String gbvBileetaUsername;
	protected static String gbvBileetaPassword;

	/* Common automation tenant */
	protected static String commonCustomer;
	protected static String quantityOne;
	protected static String hundred;

	/* Common */
	protected static String warehouseGstWarehouseNegombo;
	protected static String commonQuantity;
	protected static String commonUnitCost;
	protected static String productGroup;
	protected static String manufacturer;
	protected static String commonWarehouseCodeOnly;
	protected static String commonWarehouseCodeAndDescription;
	
	// IW_TC_001
	protected static String outboundCostingMethodProductGroupConfig;
	protected static String tollerenceProductGroupConfig;
	protected static String minPriceProductGroupConfig;

	// IW_TC_003
	protected static String productCode;
	protected static String productDesription;
	protected static String menufacturerName;
	protected static String uomGroup;
	protected static String uom;
	protected static String productWidth;
	protected static String productHeight;
	protected static String productLength;
	protected static String outboundCostingMethod;
	protected static String tollerenceDate;

	// IW_TC_004
	protected static String warehouseName;
	protected static String qurantineWarehouse;
	protected static String businessUnit;
	protected static String lotBookWarehouse;

	// IW_TC_005
	protected static String descStockAdj;
	protected static String serialProduct;
	protected static String batchProduct;
	protected static String warehouseStockAdj;
	protected static String productBatchIW;
	protected static String productSerialIW;
	protected static String productQuantity1;
	protected static String productCost1;
	protected static String productCost2;
	protected static String qtyCommonStockAdjustment;

	// IW_TC_006
	protected static String fromWareTO;
	protected static String toWareTO;
	protected static String productTO;
	protected static String productTOCost;
	protected static String fromWareTransferOrder;
	protected static String toWareTransferOrder;
	protected static String qtyTransferOrder;

	// IW_TC_007
	protected static String customerAccountILO;
	protected static String address1ILO;
	protected static String address2ILO;
	protected static String product1ILO;
	protected static String quantitya1ILO;
	protected static String quantity2ILO;
	protected static String fromWareInterDepartmentlTransferOrder;
	protected static String toWareInterDepartmentlTransferOrder;
	protected static String salesPriceModelInterDepartmentalTransferOrder;
	protected static String qtyInterDepartmentTransferOrder;

	// IW_TC_008
	protected static String qtyProductConvertionFromProduct;
	protected static String toProductProductConversion;
	protected static String toProductCoseProductConversion;
	protected static String lotNoProductConversionToProduct;
	protected static String productTo08;
	protected static String productStockConversion;

	// IW_TC_010
	protected static String titleInternelOrder;
	protected static String shipingModeInternelOrder;
	protected static String shipingTermInternelOrder;
	protected static String batch_product1;
	protected static String serial_product1;
	protected static String warehouse1;
	protected static String qtyInternelOrderProduct1;
	protected static String qtyInternelOrderProduct2;
	protected static String warehouseInternelOrderEmployeeRequest;

	// IW_TC_013
	protected static String qtyProduct1OutboundShipmentILO;
	protected static String qtyProduct2OutboundShipmentILO;
	protected static String billingAddressILO;
	protected static String deliveryAddressILO;
	protected static String costProduct01ILO;
	protected static String costProduct02ILO;
	protected static String customerAccountILO2;

	// IW_TC_014
	protected static String qtyPlaned;

	// IW_TC_016
	protected static String descriptionAssemblyCostEstimation;
	protected static String customerAccountACE;
	protected static String assemblyProductACE;
	protected static String pricingProfileACE;
	protected static String typeEstmationAce1;
	protected static String product1EstimationDeatailsACE;
	protected static String qtyInputProduct1ACE;
	protected static String costInputProduct1ACE;
	protected static String typeEstmationAce2;
	protected static String product2EstimationDeatailsACE;
	protected static String qtyInputProduct2ACE;
	protected static String costInputProduct2ACE;
	protected static String typeEstmationAce3Expence;
	protected static String costInputService3ACE;
	protected static String typeEstmationAce4;
	protected static String costInputService4ACE;
	protected static String product3EstimationDeatailsACESerivice;
	protected static String product4EstimationDeatailsACEService;

	// IW_TC_017
	protected static String employee1;
	protected static String productionUnit1;
	protected static String warehouseAO;
	protected static String warehouseWIPAO;
	protected static String assemblyProductAO;

	/* IW_TC_018 */
	protected static String customerAccountSAlesOrder;
	protected static String currencySalesOrder;
	protected static String salesUnitSalesOrder;
	protected static String assemblyProductSalesOrder;
	protected static String quantitySalesOrder;
	protected static String unitPriceSalesOrder;
	protected static String warehouseSalesOrder;
	protected static String expenceService;
	protected static String serviceAssemblyProcessCostEstimation;
	protected static String taskAssignUser;

	// IW_TC_020
	protected static String productSerialNumberViewer;

	/* IW_TC_021 */
	protected static String outboundQuantityChangedSeriel;
	protected static String serielChangingProduct;
	protected static String warehouseChangedSerielOutbound;

	// IW_TC_022
	protected static String selectionInboundShipment;
	protected static String selectionQCQcceptancePurchaseOrderInboundShipment;
	protected static String passQtyQCAcceptance;
	protected static String failQtyQCAcceptance;
	protected static String reasonFailQuantityQcAcceptance;
	protected static String vendor;

	// IW_TC_023
	protected static String qtyPurchaseORder;
	protected static String unitPricePurchaseOrder;
	protected static String warehousePurchaseOrder;
	protected static String documentTypeShipmentAceptance;
	protected static String purchsaeOrderProduct;
	protected static String serialNumberStartShipmentAcceptance;
	protected static String lotNoShipmentAcceptance;
	protected static String shipmentAcceptanceJourney;

	/* Common PO For Fin_TC_016 */
	protected static String vendorCommonPurchasOrder;

	/* Fin_TC_009 */
	protected static String warehouseStockTake;
	protected static String batchProduct1StockTake;
	protected static String batchProduct2StockTake;
	protected static String vrndorBarcodeGenarate;

	/* Common product creation */
	protected static String minPriceCoommnonCreateProduct;
	protected static String maxPriceCoommnonCreateProduct;
	protected static String productGroupServiceCommonProductCreate;

	/* AV_IW_AssemblyProposal */
	protected static String assemblyProductAssemblyProposal;
	protected static String assemblyProcessProductionUnitActionVerification;

	/* AV_IW_RackTransfer */
	protected static String warehouseRackTransfer;
	protected static String filterByRackTransfer;
	protected static String rackTransferQuantity;

	/* AV_IW_AssemblyOrder */
	protected static String changeReasonHoldUnholdActionVerification;

	/******************* Inventory And Warehouse Creations ********************/

	/* Lot Book */
	protected static String lotBookModule;

	/*********************** Regression01 **********************/
	/* IN_IO_002 */
	protected static String header_journeyList;
	protected static String header_newsStatus;

	/* IN_IO_004 */
	protected static String employeeNameRegressionNew1;

	protected static String mmployeeCodeRegressionNew1;

	/* IN_IO_005 */
	protected static String campaign_iwIO005;

	/* IN_IO_006 */
	protected static String varientProduct;

	/* IN_IO_009 */
	protected static String productCode_IW_IO_009;
	protected static String productDescription_IW_IO_009;

	/* IN_IO_025_I_026_027 */
	protected static String batchSpecificProductWithDifferentLots;
	protected static String costForBatchProductsWithDifferentLots;

	/* IN_IO_028_I_029_030 */
	protected static String batchSpecificProductWithDifferentBatchesSameLot;
	protected static String costForBbatchSpecificProductWithDifferentBatchesSameLot;

	/* IN_IO_031_032_033 */
	protected static String costOfProductForIN_IO_031_032_033;
	protected static String stockAdjustmentQuantiyt_IN_IO_031_032_033;

	/* IN_IO_034 */
	protected static String ohVerifyProduct;

	/* IN_IO_036 */
	protected static String stockReservationMethod;
	protected static String reservationVerifyProduct;
	protected static String productReserveQuantity;

	/* IN_IO_037 */
	protected static String stockReservationMethodAutomatic;

	/* IN_IO_038 */
	protected static String productAutoOutboundReservationVerify;
	protected static String stockReservationMethodAutomaticAndInbound;
	protected static String purchaseOrderQuantity_IN_IO_038;

	/* IN_IO_040 */
	protected static String qtyInternelOrderReserved_IN_IO_040;
	protected static String qtyOutbound_IN_IO_040;

	/* IN_IO_042_043_044 */
	protected static String productTaggedWithAlternatives;
	protected static String alternativeProduct;
	protected static String orderQuantity_IN_IO_042;
	protected static String costForAlternativeProduct;

	/* IN_IO_045_046_047 */
	protected static String costForAlternativeProductAndWithoutTagAlternativeProduct;

	/* IN_IO_048 */
	protected static String decimalAllowProduct;
	protected static String quantity_IN_IO_048;

	/* IN_IO_050 */
	protected static String quantity_IN_IO_050;

	/* IN_IO_054 */
	protected static String workflowJourney_IN_IO_054;
	protected static String usernamePradeep;
	protected static String passwordPradeep;

	/* IN_IO_055_058 */
	protected static String internelOrderQuantity;

	/* IN_IO_065 */
	protected static String firstLotStockAdjustmentQuantity_IN_IO_065;
	protected static String internalOrderQuantity01_IN_IO_065;
	protected static String internalOrderQuantity02_IN_IO_065;
	protected static String internalOrderQuantity03_IN_IO_065;

	/* IN_IO_066 */
	protected static String warhouesePO_IN_IO_066;
	protected static String quantityPO_IN_IO_066;
	protected static String pricePO_IN_IO_066;

	/* IN_IO_067 */
	protected static String customerAccountSalesOrder_IN_IO_067;
	protected static String salesOrderUnitsQuantity_IN_IO_067;
	protected static String salesOrderUnitPrise_IN_IO_067;
	protected static String salesOrderWarehouse_IN_IO_067;
	protected static String salesOrderSearchByMethodSalesReturs;
	protected static String returnReasonSalesReturnOrder;

	/* IN_IO_069 */
	protected static String warehouse_IN_IO_069;

	/* IN_IO_072 */
	protected static String warehouse_IN_IO_072;
	protected static String quantity_IN_IO_072;
	protected static String unitPrice_IN_IO_072;
	protected static String quantityInternelOrder_IN_IO_072;

	/* IN_IO_074 */
	protected static String productQuantity_IN_IO_074;

	/* IN_IO_075 */
	protected static String physicalQuantity_IN_IO_075;
	protected static String internelOrderQuntity_IN_IO_075;

	/* IN_IRO_Emp_001_002_003_004_005 */
	protected static String employeeCommonVerify;
	protected static String warehouse_IN_IRO_Emp_004;

	/* IN_IRO_Emp_006 */
	protected static String employee01;
	protected static String employee02;
	protected static String quantity_IN_IRO_Emp_006;

	/* IN_IRO_Emp_017 */
	protected static String quntity_edit_IN_IRO_Emp_017;

	/************** Regression-Transfer Order *********/
	/* common */
	protected static String commonUseWarehouseTransferOrder;

	/* IN_TO_003 */
	protected static String addressGSTWarehouse;

	/* IN_TO_006 */
	protected static String changedAddressFromWarehouse;
	protected static String changedAddressDestinationWarehouse;

	/* IN_TO_008 */
	protected static String autoSearchProductQuantityTransferOrder;

	/* IN_TO_010 */
	protected static String defoultUOMCommonProduct;

	/* IN_TO_013 */
	protected static String transferCostTransferOrder;

	/* IN_TO_015 */
	protected static String qtyTransferOrderRegression;

	/* IN_TO_016 */
	protected static String myTemplate;

	/* IN_TO_017 */
	protected static String updatedTransferOrderQuantity;

	/* IN_TO_031 */
	protected static String stockUsedQuantity_IN_TO_031;
	protected static String employee01GBV;

	/* IN_TO_036_037_038 */
	protected static String warehouse_IN_TO_036_038;
	protected static String quantity_IN_TO_036_038;
	protected static String unitCostStockAdjustment01;
	protected static String unitCostStockAdjustment02;
	protected static String lot01_IN_TO_036_038;
	protected static String lot02_IN_TO_036_038;
	protected static String transferCost_IN_TO_036_038;
	protected static String unitCostBinCardTemplate;
	protected static String stockValueWhenOutbound_IN_TO_37;
	protected static String stockValueWhenInbound_IN_TO_39;

	/* IN_TO_040 */
	protected static String partialQuntity01_IN_TO_040;

	/* IN_TO_041 */
	protected static String partialQuntity01_IN_TO_041;
	protected static String restOfQuntity01_IN_TO_041;

	/* IN_TO_044 */
	protected static String quantityFive_IN_TO_044;
	protected static String quantityFour_IN_TO_044;
	protected static String quantityTwo_IN_TO_044;

	/* IN_TO_046 */
	protected static String quantity_IN_TO_046;
	protected static String unitCost_IN_TO_046;
	protected static String quantityFifteen_IN_TO_046;
	protected static String quantityFive_IN_TO_046;

	/* IN_TO_047 */
	protected static String quantity_IN_TO_047;
	protected static String unitCost_IN_TO_047;
	protected static String quantityFifteen_IN_TO_047;
	protected static String quantityFive_IN_TO_047;

	/* IN_TO_049 */
	protected static String quantity_IN_TO_049;
	protected static String unitCost_IN_TO_049;
	protected static String lotWiseStockTemplate_IN_TO_049;
	protected static String serielBatchLotNumberReport;

	/* IN_TO_050_051 */
	protected static String templateOutboundShipment_IN_TO_050_051;
	protected static String templateInboundShipment_IN_TO_050_051;

	/* IN_TO_056 */
	protected static String quantityCommonTransferOrder;
	protected static String unitCostCommonTransferOrder;
	protected static String decimalQuantityTransferOrder;

	/* IN_TO_057 */
	protected static String roundedDecimalValueTransferOrder;

	/* IN_TO_054 */
	protected static String quantity_IN_TO_054;
	protected static String unitCost_IN_TO_054;

	/* IN_TO_061 */
	protected static String partialQuantityCommonOutboundShipmentTransferOrderJourney;

	/* IN_TO_065 */
	protected static String quantiry_IN_TO_065;

	/* IN_TO_067 */
	protected static String quantiry_IN_TO_067;

	/* IN_TO_068 */
	protected static String vendorCommonAutomationTenant;

	/* IN_TO_070 */
	protected static String customerAccount_IN_TO_070;

	/* IN_TO_086 */
	protected static String quantity_IN_TO_086;
	protected static String unitCost_IN_TO_086;
	protected static String quantityOverhead_IN_TO_086;

	/* IN_TO_087 */
	protected static String stockAdjustmentQuantiyt_IN_TO_087;
	protected static String transferOrderQuantiyt01_IN_TO_087;
	protected static String transferOrderQuantiyt02_IN_TO_087;

	/* IN_TO_089 */
	protected static String stockAdjustmentQuantiyt_IN_TO_089;
	protected static String transferOrderQuantiyt01_IN_TO_089;
	protected static String transferOrderQuantiyt02_IN_TO_089;

	/* IN_TO_090 */
	protected static String quantityStockAnjustment_IN_TO_090;
	protected static String unitPriceStockAdjustment_IN_IO_090;

	/* IN_TO_094 */
	protected static String customerAccount_IN_TO_094;

	/* IN_TO_097 */
	protected static String qtyGreaterThanOH_IN_TO_097;
	protected static String qtyGreaterThanOrderQty_IN_TO_097;

	/* IN_TO_101 */
	protected static String failStatusQCAcceptance;

	/* IN_TO_102 */
	protected static String template_IN_TO_102;
	protected static String qcAcceptanceJourneyTransferOrderInboundShipment;
	protected static String passQtyQCAcceptanceCommon;
	protected static String passStatusQcAcceptance;

	/* IN_TO_103 */
	protected static String passFailPArtialQuantityQCAcceptance;

	/* IN_TO_104 */
	protected static String deductionQuantityQCReverseTransferOrderToWare;
	protected static String deductionQuantityQCReverseTransferOrderQuarantineWare;

	/* IN_TO_114 */
	protected static String shipmentAcceptanceJourneyTransferOrder;
	protected static String seleilNoInvalidValidation;

	/* IN_TO_122 */
	protected static String documentTypeOutboundShipmentAceptance;

	/* Stock Take */
	/* IN_ST_008_009 */
	protected static String rackStockTakeProductAddingPopup;
	protected static String poruductAddingPhysicalQuantity;

	/* IN_ST_011 */
	protected static String stockTakeWarehouse;
	protected static String decimalQuantityStockTake;
	protected static String afterResetAsdecimalQuantityToWithoutDecimalStockTake;

	/* IN_ST_013 */
	protected static String varienceQuantityStockTake;

	/* IN_ST_014 */
	protected static String adjustedQuantityBeforeStockAdjustment;

	/* IN_ST_015 */
	protected static String changeQuantityStockTakeWhenUpdate;
	protected static String newQuntityStockTakeWhenUpdate;

	/* IN_ST_016 */
	protected static String varienceQuantityFifteen;

	/* IN_ST_021 */
	protected static String zeroBalanceStockTake;

	/* IN_ST_022 */
	protected static String qtyReducePhysicalBalanceComparedToSystemBalance;

	/* IN_ST_023 */
	protected static String qtyIncreacePhysicalBalanceComparedToSystemBalance;

	/* IN_ST_032 */
	protected static String mainValidationOfForms;

	/* IN_ST_033 */
	protected static String stockAlreadyDraftedValidationWhenTryToDoStockTake;

	/* IN_ST_034 */
	protected static String quantityStockTakeRegression;
	protected static String adjustedQuantityStockAdjustment;

	/* IN_ST_040_041 */
	protected static String reduceQuantityStockAdjustmentComesFromStockTake;
	protected static String remainQuantityPartialStockAdjustmentComesFromStockTake;

	/* IN_ST_046 */
	protected static String varienceQuantityMinusFive;
	protected static String varienceQuantityPlusFive;
	protected static String adjustedQuantityMinusFive;
	protected static String adjustedQuantityPlusFive;

	/* IN_ST_049 */
	protected static String newDescriptionForEditedStockAdjustment;

	/* IN_ST_051 */
	protected static String physicalBalanceFive;
	protected static String varienceQuantityPlusFour;
	protected static String adjustedQuantityPlusFour;

	/* Product Conversion */
	/* IN_PC_003 */
	protected static String productConversionWarehouseRegression;

	/* IN_PC_006 */
	protected static String defoultUOMProductConversion;
	
	/* IN_PC_009 */
	protected static String capturedQuantityProductConversion;
	protected static String qtyFromProductProductConversion;
	
	/* IN_PC_011 */
	protected static String inactiveStatus;
	
	/* IN_PC_012 */
	protected static String productDefoultUom;
	
	/* IN_PC_013 */
	protected static String costPriceProductConversion;
	
	/* IN_PC_014 */
	protected static String conRateProductConversion;
	protected static String quantityAccordingToConRate;
	
	/* IN_PC_015 */
	protected static String toQuantityForCheckConRate;
	protected static String conRateAccordingToToQuantity;
	
	/* IN_PC_016 */
	protected static String decimalQtyFromProductProductConversion;
	protected static String decimalQtyToProductProductConversion;
	
	/* IN_PC_017_018 */
	protected static String decimalRemovedQuantityFromProductProductConversion;
	protected static String decimalRemovedQuantityToProductProductConversion;
	
	/* IN_PC_019 */
	protected static String quantityToProductProductConversion;
	protected static String quantityFiveProductConversion;
	
	/* IN_PC_026 */
	protected static String qtyFiveHundred;
	
	/* IN_PC_027 */
	protected static String qtyFifteen;
	protected static String costNinetyFive;
	
	/* IN_PC_040 */
	protected static String quantityTenProductConversion;
	
	// ============================================RegressionAruna======================================================================
	// IN_SA_005
	protected static String ProductAddTxtA;

	// IN_SA_012
	protected static String AdjustmentGroupTxtA;

	// IN_SA_016
	protected static String ProductAddTxtDicA;

	// IN_SA_020
	protected static String ProductAddTxt2A;

	// IN_SA_028
	protected static String RackTxtA;

	// IN_SA_042
	protected static String productQuantityA;

	// IN_SA_047
	protected static String SerialNoA;
	protected static String LotNoA;

	// IN_SA_049
	protected static String Pro1A;
	protected static String Pro2A;

	// IN_SA_052
	protected static String SampleProA;
	protected static String VariantGrouptxtA;

	// IN_SA_056
	protected static String BatchNoA;

	// IN_SA_059
	protected static String searchCustomerSalesOrderA;

	// IN_SA_060
	protected static String RequestertxtA;

	// IN_SA_065
	protected static String MinusProA;

	// IN_SA_067
	protected static String WarehouseA;

	// IN_SA_068
	protected static String MinusPro2A;

	// IN_SA_070
	protected static String SerialProA;

	// IN_IRO_Emp_051
	protected static String WarehouseCodeA;

	// IN_IRO_Emp_052
	protected static String WarehouseDicriptionA;

	// IN_IRO_Emp_053
	protected static String WarehouseCodeAndDicriptionA;

	// IN_IRO_Emp_054
	protected static String EmployeeCodeA;

	// IN_IRO_Emp_055
	protected static String EmployeeDicriptionA;

	// IN_IRO_Emp_056
	protected static String EmployeeCodeAndDicriptionA;

	// IN_IRO_Emp_029
	protected static String ArunaIROA;
	protected static String DilshanIROA;
	
	/* Smoke Production */
	protected static String productGroupSmokeProduction;
	protected static String quantityTen;
	protected static String menufacturer;
	protected static String menufacturerTypeProduction;
	protected static String inputWarehouseSmokeProduction;

	// Calling the constructor
	public static void readElementlocators() throws Exception {
		// Common
		header_documentReleasedStatus01 = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_documentReleasedStatus01");
		header_documentDraftStatus01 = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_documentDraftStatus01");
		lbl_releasedStatusCommonWithoutSpace = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_releasedStatusCommonWithoutSpace");
		btn_apply = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_apply");
		header_holdStatusTransferOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_holdStatusTransferOrder");
		header_journelEntryDialogBox = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_journelEntryDialogBox");
		lbl_relesedConfirmation = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lbl_relesedConfirmation");
		btn_newPrimaryForm = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_newPrimaryForm");
		btn_releese2 = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_releese2");
		brn_serielBatchCapture = findElementInXLSheet(getParameterInvertoryAndWarehouse, "brn_serielBatchCapture");
		txt_batchNumberCapture = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_batchNumberCapture");
		btn_updateCapture = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_updateCapture");
		btn_productCaptureBackButton = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_productCaptureBackButton");
		header_searielBatchCapturePage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_searielBatchCapturePage");

		header_draft = findElementInXLSheet(getParameterInvertoryAndWarehouse, "header_draft");
		btn_draft2 = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_draft2");
		btn_checkout2 = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_checkout2");
		drop_warehouseMultipleProductGrid = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"drop_warehouseMultipleProductGrid");
		btn_commoApplyLast = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_commoApplyLast");
		span_afterCaptureSerialGreen = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"span_afterCaptureSerialGreen");
		chk_isQurantineWarehouse = findElementInXLSheet(getParameterInvertoryAndWarehouse, "chk_isQurantineWarehouse");

		// Login
		siteLogo = findElementInXLSheet(getParameterInvertoryAndWarehouse, "site logo");
		txt_username = findElementInXLSheet(getParameterInvertoryAndWarehouse, "user name");
		txt_password = findElementInXLSheet(getParameterInvertoryAndWarehouse, "password");
		btn_login = findElementInXLSheet(getParameterInvertoryAndWarehouse, "login button");
		lnk_home = findElementInXLSheet(getParameterInvertoryAndWarehouse, "home link");
		div_loginVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse, "div_loginVerification");

		// Com_TC_002
		navigation_pane = findElementInXLSheet(getParameterInvertoryAndWarehouse, "navigation_pane");
		inventory_module = findElementInXLSheet(getParameterInvertoryAndWarehouse, "inventory_module");
		icon_logged_info = findElementInXLSheet(getParameterInvertoryAndWarehouse, "icon_logged_info");

		// IW_TC_001
		lnk_product_group_configuration = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lnk_product_group_configuration");
		btn_new_group_configuration = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_new_group_configuration");
		btn_product_group_plus_mark_table = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_product_group_plus_mark_table");
		btn_product_group_plus_mark_second = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_product_group_plus_mark_second");
		btn_update_product_group = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_update_product_group");
		txt_product_group = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_product_group");
		tab_pg_details = findElementInXLSheet(getParameterInvertoryAndWarehouse, "tab_pg_details");
		btn_purchase_active = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_purchase_active");
		btn_sales_active = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_sales_active");
		btn_inventory_active = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_inventory_active");
		btn_product_function_confirmation = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_product_function_confirmation");
		btn_draft = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_draft");
		btn_relese = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_relese");
		btn_status_relese = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_status_relese");
		header_productGroupConfigPage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_productGroupConfigPage");
		header_newProductGroupForm = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_newProductGroupForm");
		tbl_productGroup = findElementInXLSheet(getParameterInvertoryAndWarehouse, "tbl_productGroup");
		btn_productGroupUpdate = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_productGroupUpdate");
		dropdown_productGroupConfig = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"dropdown_productGroupConfig");
		btn_purchaseActiveConfirmation = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_purchaseActiveConfirmation");
		btn_salesActiveConfirmation = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_salesActiveConfirmation");
		header_warehouseInforProductGroupConfig = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_warehouseInforProductGroupConfig");
		header_producInfoProductGroupConfig = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_producInfoProductGroupConfig");
		header_sellingDeatailsProductGroupConfig = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_sellingDeatailsProductGroupConfig");
		dropdown_outboundCostingMethodProductGroupConfig = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"dropdown_outboundCostingMethodProductGroupConfig");
		chk_allowInventoryWithoutCostingProductGroupConfig = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_allowInventoryWithoutCostingProductGroupConfig");
		chk_batchProductProductGroupConfig = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_batchProductProductGroupConfig");
		txt_tollerenceProductGroupConfig = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_tollerenceProductGroupConfig");
		txt_minPriceProductGroupConfig = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_minPriceProductGroupConfig");
		txt_maxPriceProductGroupConfig = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_maxPriceProductGroupConfig");

		// IW_TC_002
		btn_productInformation = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_productInformation");
		btn_newProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_newProduct");
		header_productInformation = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_productInformation");
		dropdown_productGroup = findElementInXLSheet(getParameterInvertoryAndWarehouse, "dropdown_productGroup");

		// IW_TC_003
		btn_product_info = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_product_info");
		btn_new_product = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_new_product");
		txt_product_code = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_product_code");
		txt_product_desc = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_product_desc");
		dropdown_product_group = findElementInXLSheet(getParameterInvertoryAndWarehouse, "dropdown_product_group");
		txt_menufacturer = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_menufacturer");
		dropdown_defoult_uom_group = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"dropdown_defoult_uom_group");
		dropdown_defoult_uom = findElementInXLSheet(getParameterInvertoryAndWarehouse, "dropdown_defoult_uom");
		txt_length = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_length");
		txt_width = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_width");
		txt_height = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_height");
		tab_detail = findElementInXLSheet(getParameterInvertoryAndWarehouse, "tab_detail");
		checkbox_batch_product = findElementInXLSheet(getParameterInvertoryAndWarehouse, "checkbox_batch_product");
		dropdown_outbound_costing_method = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"dropdown_outbound_costing_method");
		checkbox_without_costing = findElementInXLSheet(getParameterInvertoryAndWarehouse, "checkbox_without_costing");
		checkbox_validate_expiery = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"checkbox_validate_expiery");
		txt_tollerence_date = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_tollerence_date");
		checkbox_qc_acceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse, "checkbox_qc_acceptance");
		checkbox_validate_expiery_date = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"checkbox_validate_expiery_date");
		header_product_information = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_product_information");
		header_new_product = findElementInXLSheet(getParameterInvertoryAndWarehouse, "header_new_product");
		checkbox_allow_allocation = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"checkbox_allow_allocation");
		btn_menufactururSearchBatchProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_menufactururSearchBatchProduct");
		lnk_resultMenufacBatchProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lnk_resultMenufacBatchProduct");
		drop_lengthUnitBtachProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"drop_lengthUnitBtachProduct");
		drop_widthUnitBtachProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"drop_widthUnitBtachProduct");
		drop_heightUnitBtachProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"drop_heightUnitBtachProduct");
		header_inventoryBatchProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_inventoryBatchProduct");
		chk_allocationBatchProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_allocationBatchProduct");
		btn_roqBatchProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_roqBatchProduct");
		lbl_creationsDocNo = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lbl_creationsDocNo");
		btn_chkIsSeriel = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_chkIsSeriel");

		// IW_TC_004
		btn_warehouseInfo = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_warehouseInfo");
		btn_newWarehouse = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_newWarehouse");
		txt_warehouseCode = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_warehouseCode");
		txt_warehouseName = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_warehouseName");
		dropdown_businessUnit = findElementInXLSheet(getParameterInvertoryAndWarehouse, "dropdown_businessUnit");
		checkbox_isAllocation = findElementInXLSheet(getParameterInvertoryAndWarehouse, "checkbox_isAllocation");
		dropdown_qurantineWarehouse = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"dropdown_qurantineWarehouse");
		checkbox_autoLot = findElementInXLSheet(getParameterInvertoryAndWarehouse, "checkbox_autoLot");
		dropdown_lotBook = findElementInXLSheet(getParameterInvertoryAndWarehouse, "dropdown_lotBook");
		header_warehouseCretionForm = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_warehouseCretionForm");
		btn_draftConfirmation = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_draftConfirmation");
		btn_releseConfirmation = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_releseConfirmation");
		header_warehouseInformationPage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_warehouseInformationPage");

		// IW_TC_005
		btn_stockAdjustment = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_stockAdjustment");
		btn_newStockAdjustment = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_newStockAdjustment");
		txt_descStockAdj = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_descStockAdj");
		dropdown_warehouseStockAdj = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"dropdown_warehouseStockAdj");
		btn_productLookupStockAdj = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_productLookupStockAdj");
		btn_searchProductStockAdj = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_searchProductStockAdj");
		btn_resultProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_resultProduct");
		header_newStockAdj = findElementInXLSheet(getParameterInvertoryAndWarehouse, "header_newStockAdj");
		txt_productSearch = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_productSearch");
		txt_qtyProductGrid1 = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_qtyProductGrid1");
		txt_qtyProductGrid2 = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_qtyProductGrid2");
		txt_costProductGrid1 = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_costProductGrid1");
		txt_costProductGrid2 = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_costProductGrid2");
		btn_addNewRow = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_addNewRow");
		txt_costProductGrid1 = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_costProductGrid1");
		btn_searchProductStockAdj2 = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_searchProductStockAdj2");
		chk_productCaptureRange = findElementInXLSheet(getParameterInvertoryAndWarehouse, "chk_productCaptureRange");
		btn_productLookupStockAdjForLoop = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_productLookupStockAdjForLoop");
		txt_qtyProductGridStockAdjustment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_qtyProductGridStockAdjustment");
		txt_costProductGridStockAdjustment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_costProductGridStockAdjustment");
		btn_deleteEmotyRowStockAdjustment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_deleteEmotyRowStockAdjustment");
		btn_itemProductCapturePage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_itemProductCapturePage");
		txt_lotNumberStockAdjustment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_lotNumberStockAdjustment");
		txt_expiryDateStockAdjustmentProductCapture = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_expiryDateStockAdjustmentProductCapture");
		txt_searielLotNo = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_searielLotNo");
		txt_dateSerielProductProductCapturePage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_dateSerielProductProductCapturePage");
		txt_serielBatchProductCaptureStockAdjustment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_serielBatchProductCaptureStockAdjustment");
		td_availableStockWarehouseGST_Warehouse = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"td_availableStockWarehouseGST_Warehouse");
		btn_avilabilityCheckWidget = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_avilabilityCheckWidget");
		btn_avilabilytCheckSecondMenu = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_avilabilytCheckSecondMenu");
		btn_closeAvailabilityCheckPopup = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_closeAvailabilityCheckPopup");
		btn_availabilityCheckWidgetAfterrelesed = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_availabilityCheckWidgetAfterrelesed");
		td_availableStockGstmWarehouseAfterRelesed = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"td_availableStockGstmWarehouseAfterRelesed");
		btn_deleteEmotyRowCommonStockAdjustment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_deleteEmotyRowCommonStockAdjustment");

		// IW_TC_006
		btn_newTransferOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_newTransferOrder");
		btn_transferOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_transferOrder");
		txt_fromWareTO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_fromWareTO");
		txt_toWareTO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_toWareTO");
		btn_checkout = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_checkout");
		header_outboundShipment = findElementInXLSheet(getParameterInvertoryAndWarehouse, "header_outboundShipment");
		header_newTransferOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse, "header_newTransferOrder");
		txt_qtyProductGridStockTransferOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_qtyProductGridStockTransferOrder");
		txt_costProductGridStockTransferOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_costProductGridStockTransferOrder");
		chk_availableProductsTransferOrderOutboundShipment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_availableProductsTransferOrderOutboundShipment");
		lbl_qtyBatchSpecifificCapturePage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_qtyBatchSpecifificCapturePage");
		lbl_captureQtyBatchSpecifificCapturePage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_captureQtyBatchSpecifificCapturePage");
		txt_seriealProductCapture = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_seriealProductCapture");
		btn_productAvilabilityWidgetShipmentDetailsTable = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_productAvilabilityWidgetShipmentDetailsTable");
		btn_availabilityMoveFirstButtonTransferOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_availabilityMoveFirstButtonTransferOrder");
		btn_productLookupTOForLoop = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_productLookupTOForLoop");
		header_draftedTrsansferOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_draftedTrsansferOrder");
		
		// IW_TC_007
		btn_productGridSearchInterDepartmentTransferOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_productGridSearchInterDepartmentTransferOrder");
		txt_prductSearchInterDepartmentTransferOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_prductSearchInterDepartmentTransferOrder");
		btn_InterDepartmentTransferOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_InterDepartmentTransferOrder");
		btn_salesPriceModelConfirmation = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_salesPriceModelConfirmation");
		txt_qtyProductGridInterDepartmentTransferOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_qtyProductGridInterDepartmentTransferOrder");
		txt_closePopupProductAvailabilityInterDepartmentTransferOrder = findElementInXLSheet(
				getParameterInvertoryAndWarehouse, "txt_closePopupProductAvailabilityInterDepartmentTransferOrder");
		lbl_availQuantityProductCapturuPage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_availQuantityProductCapturuPage");
		btn_deleteRowProductCaptururePage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_deleteRowProductCaptururePage");
		chk_deleteItemCapturePage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_deleteItemCapturePage");
		header_releasedInboundShipment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_releasedInboundShipment");

		// IW_TC_008
		btn_productConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_productConversion");
		bnt_newProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse, "bnt_newProductConversion");
		drop_wareProductCon = findElementInXLSheet(getParameterInvertoryAndWarehouse, "drop_wareProductCon");
		lbl_bokNoProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_bokNoProductConversion");
		btn_productLookupOroductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_productLookupOroductConversion");
		heder_popupProductLookupProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"heder_popupProductLookupProductConversion");
		div_productNameProductGrigPrductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_productNameProductGrigPrductConversion");
		txt_quantityProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_quantityProductConversion");
		btn_serialNosFromProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_serialNosFromProductConversion");
		header_searialBatchNosProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_searialBatchNosProductConversion");
		lnk_resutlSeraialBatchCaptureProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lnk_resutlSeraialBatchCaptureProductConversion");
		btn_seraialBatchCaptureDocIconProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_seraialBatchCaptureDocIconProductConversion");
		chk_batchSelectProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_batchSelectProductConversion");
		header_existinfSerialBatchProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_existinfSerialBatchProductConversion");
		txt_qtyToProductProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_qtyToProductProductConversion");
		btn_applyProductBatchCaptureProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_applyProductBatchCaptureProductConversion");
		btn_productLoocupToProductProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_productLoocupToProductProductConversion");
		header_productConversionPage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_productConversionPage");
		div_productNameProductGrigPrductConversionToProduc1 = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_productNameProductGrigPrductConversionToProduc1");
		txt_costToProductOroductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_costToProductOroductConversion");
		txt_quntityToProductProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_quntityToProductProductConversion");
		btn_serialNosFromProductConversionToProduc = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_serialNosFromProductConversionToProduc");
		txt_batchNoCaptureProductConversionNosPopup = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_batchNoCaptureProductConversionNosPopup");
		txt_LotNoProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_LotNoProductConversion");
		btn_exoiryDateProductConversionToProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_exoiryDateProductConversionToProduct");
		btn_date31ProductConversionToProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_date31ProductConversionToProduct");
		lbl_resultCaptureToProductProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_resultCaptureToProductProductConversion");
		drop_salesPriceModel = findElementInXLSheet(getParameterInvertoryAndWarehouse, "drop_salesPriceModel");
		btn_draftBayJavaScrptFirstButton = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_draftBayJavaScrptFirstButton");
		lbl_releasedStatusProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_releasedStatusProductConversion");

		// IW_TC_010
		btn_internelOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_internelOrder");
		btn_newInternelOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_newInternelOrder");
		btn_employeeRequestJourneyInternelOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_employeeRequestJourneyInternelOrder");
		txt_tiltleInternelOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_tiltleInternelOrder");
		btn_verifyInternelOrderPage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_verifyInternelOrderPage");
		btn_searchRequester = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_searchRequester");
		lnk_searchResultRequester = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lnk_searchResultRequester");
		btn_refreshRequsterWindow = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_refreshRequsterWindow");
		txt_RequesterSearch = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_RequesterSearch");
		btn_serchLockupProduct1InternelOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_serchLockupProduct1InternelOrder");
		txt_productsearch1IneternelOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_productsearch1IneternelOrder");
		lnk_resultProduct1InternelOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lnk_resultProduct1InternelOrder");
		dropdown_selectWarehouseInternelOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"dropdown_selectWarehouseInternelOrder");
		txt_qtyProduct1InternelOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_qtyProduct1InternelOrder");
		btn_addSecondRowProductGridInternelOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_addSecondRowProductGridInternelOrder");
		txt_productsearch2IneternelOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_productsearch2IneternelOrder");
		dropdown_selectWarehouseInternelOrderRaw2 = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"dropdown_selectWarehouseInternelOrderRaw2");
		txt_qtyProduct2InternelOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_qtyProduct2InternelOrder");
		btn_searchLokupInternelOrderRow2 = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_searchLokupInternelOrderRow2");
		headerVerifyDraftInternelOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"headerVerifyDraftInternelOrder");
		btn_draftInternelOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_draftInternelOrder");
		btn_goToPageLink = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_goToPageLink");
		btn_draftInternelDispatchOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_draftInternelDispatchOrder");
		btn_productLookupInterDeprtmentTransferOrderForLoop = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_productLookupInterDeprtmentTransferOrderForLoop");
		btn_detailTab = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_detailTab");
		drop_warehouseInternelOrderEmployeeRequest = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"drop_warehouseInternelOrderEmployeeRequest");

		// IW_TC_011
		btn_internelReturnOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_internelReturnOrder");
		btn_internelReturnOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_internelReturnOrder");
		btn_newInternelReturnOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_newInternelReturnOrder");
		btn_employeeReturnJourneyIRO = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_employeeReturnJourneyIRO");
		btn_searchIconEmployeeIRO = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_searchIconEmployeeIRO");
		txt_employeeSearchRequesterWindowIRO = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_employeeSearchRequesterWindowIRO");
		lnk_resultRequesterIRO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lnk_resultRequesterIRO");
		dropdown_toWarehouseIRO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "dropdown_toWarehouseIRO");
		btn_docIconSelectInternelOrderIRO = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_docIconSelectInternelOrderIRO");
		txt_internelOrderSearchIRO = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_internelOrderSearchIRO");
		lnk_resultInternelOrderIRO1 = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lnk_resultInternelOrderIRO1");
		lnk_resultInternelOrderIRO2 = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lnk_resultInternelOrderIRO2");
		btn_applyInternelOrderIRO = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_applyInternelOrderIRO");
		btn_captureItem1IRO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_captureItem1IRO");
		btn_captureItem2IRO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_captureItem2IRO");
		btn_selectAllCaptureItemsIRO = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_selectAllCaptureItemsIRO");
		heder_internelReturnOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"heder_internelReturnOrder");
		header_newJourneyPopupIRO = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_newJourneyPopupIRO");
		header_newIRO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "header_newIRO");
		header_documentListIRO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "header_documentListIRO");
		result_documentListIRO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "result_documentListIRO");
		code_productAvailableIRO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "code_productAvailableIRO");
		header_internelReciptIRO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "header_internelReciptIRO");
		txt_RequesterIRO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_RequesterIRO");
		btn_serchDocInternelReturnOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_serchDocInternelReturnOrder");
		td_serchedDocNoIRO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "td_serchedDocNoIRO");
		lbl_product1DocList = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lbl_product1DocList");
		lbl_product2DocList = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lbl_product2DocList");
		lbl_product1IRO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lbl_product1IRO");
		lbl_product2IRO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lbl_product2IRO");
		header_pageLabel = findElementInXLSheet(getParameterInvertoryAndWarehouse, "header_pageLabel");
		btn_print = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_print");
		chk_selectAllCaptureItem = findElementInXLSheet(getParameterInvertoryAndWarehouse, "chk_selectAllCaptureItem");
		btn_backButtonUpdateCapture = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_backButtonUpdateCapture");

		// IW_TC_012
		btn_inboundLoanOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_inboundLoanOrder");
		btn_newInboundLoanOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_newInboundLoanOrder");
		btn_vendorSearchILO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_vendorSearchILO");
		txt_vendorSearchILO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_vendorSearchILO");
		lnk_resultVendorILO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lnk_resultVendorILO");
		btn_addressILO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_addressILO");
		txt_billingAddressILO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_billingAddressILO");
		txt_deliveryAddressILO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_deliveryAddressILO");
		btn_applyAddressILO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_applyAddressILO");
		header_ILOPage = findElementInXLSheet(getParameterInvertoryAndWarehouse, "header_ILOPage");
		txt_costILOProduct1 = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_costILOProduct1");
		txt_costILOProduct2 = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_costILOProduct2");
		dropdown_warehouseILOInboundShipment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"dropdown_warehouseILOInboundShipment");
		btn_addNewRowProductGridILOOutboundShipment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_addNewRowProductGridILOOutboundShipment");
		dropdown_warehouseProduct2ILOOutboundShipment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"dropdown_warehouseProduct2ILOOutboundShipment");
		dropdown_warehouseProduct2ILOOutboundShipment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"dropdown_warehouseProduct2ILOOutboundShipment");
		btn_checkoutILOOutboundShipment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_checkoutILOOutboundShipment");
		btn_serielBatchCaptureProduct1ILOOutboundShipment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_serielBatchCaptureProduct1ILOOutboundShipment");
		txt_batchNumberCaptureILOOutboundShipment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_batchNumberCaptureILOOutboundShipment");
		btn_serielBatchCaptureProduct2ILOOutboundShipment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_serielBatchCaptureProduct2ILOOutboundShipment");
		btn_editOutboundShipmentILO = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_editOutboundShipmentILO");
		txt_serialNumberCaptureILOOutboundShipment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_serialNumberCaptureILOOutboundShipment");
		txt_qtyProduct2InternelReturnOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_qtyProduct2InternelReturnOrder");
		txt_qtyProductGridInboundLoanOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_qtyProductGridInboundLoanOrder");
		btn_tabInboundLoanOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_tabInboundLoanOrder");

		// IW_TC_013
		txt_quantity2ILO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_quantity2ILO");
		btn_detailsTabILO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_detailsTabILO");
		chkbox_underDeliveryILO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "chkbox_underDeliveryILO");
		chkbox_partielDeliveryILO = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chkbox_partielDeliveryILO");
		btn_reqSlipDateILO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_reqSlipDateILO");
		btn_reqSlipDate17ILO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_reqSlipDate17ILO");
		btn_conSlipDateILO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_conSlipDateILO");
		btn_conSlipDate18ILO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_conSlipDate18ILO");
		btn_reqReciptDateILO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_reqReciptDateILO");
		btn_reqReciptDate19ILO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_reqReciptDate19ILO");
		btn_conReciptDateILO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_conReciptDateILO");
		btn_conReciptDate20ILO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_conReciptDate20ILO");
		btn_conReciptDate20ILO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_conReciptDate20ILO");
		btn_releseILO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_releseILO");
		btn_goToNextTaskILO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_goToNextTaskILO");
		btn_outboundLoanOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_outboundLoanOrder");
		btn_newOutboundLoanOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_newOutboundLoanOrder");
		btn_customerAccountSearchLookupILO = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_customerAccountSearchLookupILO");
		txt_accountSearchILO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_accountSearchILO");
		lnk_resultCustomerAccount = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lnk_resultCustomerAccount");
		btn_billingAddressILO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_billingAddressILO");
		txt_billingAddressILO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_billingAddressILO");
		txt_shippingAddressILO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_shippingAddressILO");
		btn_applyAddressILO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_applyAddressILO");
		dropdown_salesUnitILO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "dropdown_salesUnitILO");
		btn_productLookupSearchILO = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_productLookupSearchILO");
		dropdown_warehouse1ILO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "dropdown_warehouse1ILO");
		txt_quantity1ILO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_quantity1ILO");
		btn_addNewProductRowILO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_addNewProductRowILO");
		dropdown_warehouse2ILO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "dropdown_warehouse2ILO");
		btn_popoupOkLastStep = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_popoupOkLastStep");
		btn_productLookup13OLO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_productLookup13OLO");

		// IW_TC_014
		lnl_resultOLO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lnl_resultOLO");
		cell_beforeAvailableQuantity = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"cell_beforeAvailableQuantity");
		btn_actionOLO014 = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_actionOLO014");
		btn_convertToInboundShipment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_convertToInboundShipment");
		txt_inboundQuantity = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_inboundQuantity");
		cell_afterAvailableQuantity = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"cell_afterAvailableQuantity");
		txt_inboundQuantity1 = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_inboundQuantity1");
		lbl_checkoutUnitsBalance014 = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_checkoutUnitsBalance014");
		lbl_checkoutUnitsBalanceAfter014 = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_checkoutUnitsBalanceAfter014");
		txt_outboundLoanOrderSearch = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_outboundLoanOrderSearch");
		chk_cpatureSelection1_014 = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_cpatureSelection1_014");
		btn_delRowsCapturePage = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_delRowsCapturePage");
		btn_availabilityWidgetOutboundLoanOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_availabilityWidgetOutboundLoanOrder");

		// IW_TC_015
		txt_searchOLO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_searchOLO");
		btn_conToSalesInvoiceOLO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_conToSalesInvoiceOLO");
		txt_customerAccOLOSlaesInvoic = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_customerAccOLOSlaesInvoic");
		drop_downCurencyOLOSlaesInvoic = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"drop_downCurencyOLOSlaesInvoic");
		txt_billingAddressILOSalesInvoice = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_billingAddressILOSalesInvoice");
		txt_shippingAddressILOSalesInvoice = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_shippingAddressILOSalesInvoice");
		dropdownSalesUnitOLOSalesInvoice = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"dropdownSalesUnitOLOSalesInvoice");
		btn_draftOLOSalesInvoice = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_draftOLOSalesInvoice");
		btn_checkoutOLOSalesInvoice = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_checkoutOLOSalesInvoice");

		// IW_TC_016
		btn_assemblyCostEstimation = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_assemblyCostEstimation");
		btn_newAssemblyCostAstimation = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_newAssemblyCostAstimation");
		txt_assemblyCostEstiamtionDescription = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_assemblyCostEstiamtionDescription");
		btn_searchCustomeACE = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_searchCustomeACE");
		txt_cusSearchACE = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_cusSearchACE");
		lnk_resultCusAccountACE = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lnk_resultCusAccountACE");
		btn_searchAssemblyProACE = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_searchAssemblyProACE");
		txt_AssemblyProductSearchACE = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_AssemblyProductSearchACE");
		lnk_resltProductAssemblyACE = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lnk_resltProductAssemblyACE");
		btn_searchPricingProfileACE = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_searchPricingProfileACE");
		txt_pricingProfileACE = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_pricingProfileACE");
		lnk_pricingProfileACE = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lnk_pricingProfileACE");
		tab_estimationDeatailsACE = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"tab_estimationDeatailsACE");
		drop_typeACE1 = findElementInXLSheet(getParameterInvertoryAndWarehouse, "drop_typeACE1");
		btn_searchProductEstmationAce1 = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_searchProductEstmationAce1");
		txt_estimatedQuantityACE = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_estimatedQuantityACE");
		txt_costEstmatedACE1Input = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_costEstmatedACE1Input");
		btn_adnewRow1ACE = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_adnewRow1ACE");
		drop_typeACE2 = findElementInXLSheet(getParameterInvertoryAndWarehouse, "drop_typeACE2");
		btn_searchProduct2ACE = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_searchProduct2ACE");
		txt_estimatedQuantityACE2 = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_estimatedQuantityACE2");
		txt_costEstmatedACEOutput2 = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_costEstmatedACEOutput2");
		btn_adaNewRow3 = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_adaNewRow3");
		drop_typeExpenceACE = findElementInXLSheet(getParameterInvertoryAndWarehouse, "drop_typeExpenceACE");
		drop_estiamationRow3ReferenceCodeExpenceACE = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"drop_estiamationRow3ReferenceCodeExpenceACE");
		btn_searchServiceACE3ProductLookup = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_searchServiceACE3ProductLookup");
		txt_costServiceProductEstmationACE = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_costServiceProductEstmationACE");
		btn_addNewRow4EstmationDetailsACE = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_addNewRow4EstmationDetailsACE");
		chk_billableEstmationRow1ACE = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_billableEstmationRow1ACE");
		chk_billableEstmationRow2ACE = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_billableEstmationRow2ACE");
		chk_billableEstmationRow3ACE = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_billableEstmationRow3ACE");
		drop_type4ServiceACE = findElementInXLSheet(getParameterInvertoryAndWarehouse, "drop_type4ServiceACE");
		btn_searchProductLookuop4ACE = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_searchProductLookuop4ACE");
		txt_cost4ServiceProductEstimationACE = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_cost4ServiceProductEstimationACE");
		chk_billable4ServiceACeEstimation = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_billable4ServiceACeEstimation");
		tab_summaryACE = findElementInXLSheet(getParameterInvertoryAndWarehouse, "tab_summaryACE");
		btn_checkoutACE = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_checkoutACE");
		lbl_totalProfitACE = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lbl_totalProfitACE");
		lbl_relesedACEDocNumber = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lbl_relesedACEDocNumber");
		txt_productionUnit = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_productionUnit");
		lnk_resultProductUnit = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lnk_resultProductUnit");

		/* Common PO For Fin_TC_016 */
		txt_lotNoCapturePage1 = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_lotNoCapturePage1");

		// IW_TC_017
		btn_assemblyOredr = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_assemblyOredr");
		btn_newAssemblyOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_newAssemblyOrder");
		btn_assemblyProductSearch = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_assemblyProductSearch");
		drop_assemblyGroup = findElementInXLSheet(getParameterInvertoryAndWarehouse, "drop_assemblyGroup");
		btn_emloyeeSerchAO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_emloyeeSerchAO");
		txt_employee = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_employee");
		lnk_resultEmployee = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lnk_resultEmployee");
		drop_priorityAO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "drop_priorityAO");
		btn_searchSCostEstimtioAo = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_searchSCostEstimtioAo");
		txt_costEstimation = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_costEstimation");
		lnk_resultCostEstimation = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lnk_resultCostEstimation");
		drop_barcodeAO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "drop_barcodeAO");
		btn_searchProductionUnit = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_searchProductionUnit");
		drop_warehouseWIPAO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "drop_warehouseWIPAO");
		drop_inputWareAO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "drop_inputWareAO");
		drop_wareOutAO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "drop_wareOutAO");
		txt_serialNoAO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_serialNoAO");
		date_secondSeclectioAO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "date_secondSeclectioAO");
		date_firstClickAO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "date_firstClickAO");
		date_secondClickAO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "date_secondClickAO");
		tab_productAO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "tab_productAO");
		drop_warehouseProductAO1 = findElementInXLSheet(getParameterInvertoryAndWarehouse, "drop_warehouseProductAO1");
		tab_estimationDeatailsAO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "tab_estimationDeatailsAO");
		btn_availabilityCheckForVErifyEstmationProductAO = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_availabilityCheckForVErifyEstmationProductAO");
		btn_clanderBack = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_clanderBack");
		btn_clanderFw = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_clanderFw");

		/* IW_TC_018 */
		btn_salesModule = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_salesModule");
		btn_salesOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_salesOrder");
		btn_newSalesOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_newSalesOrder");
		btn_salesOrderToSalesInvoiceJourney = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_salesOrderToSalesInvoiceJourney");
		btn_lookupCustomerAccount = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_lookupCustomerAccount");
		txt_searchCustomerSalesOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_searchCustomerSalesOrder");
		drop_curencySalesOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse, "drop_curencySalesOrder");
		drop_salesUnitSalesOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse, "drop_salesUnitSalesOrder");
		btn_productLoockupActionVerfication = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_productLoockupActionVerfication");
		resul_assemblyProductProductSearchSalesOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"resul_assemblyProductProductSearchSalesOrder");
		txt_quantitySalesOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_quantitySalesOrder");
		txt_unitPriceSalesOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_unitPriceSalesOrder");
		btn_actionSalesOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_actionSalesOrder");
		btn_costEstimation = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_costEstimation");
		btn_essambyEstimationSalesOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_essambyEstimationSalesOrder");
		btn_addNewCostEstimationSalesOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_addNewCostEstimationSalesOrder");
		header_salesOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse, "header_salesOrder");
		txt_customerAccountOnInterface = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_customerAccountOnInterface");
		txt_biilingAdressFrontPage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_biilingAdressFrontPage");
		txt_shippingAddressFrontPage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_shippingAddressFrontPage");
		drop_accountOrner = findElementInXLSheet(getParameterInvertoryAndWarehouse, "drop_accountOrner");
		drop_warehouseProductGridSalesOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"drop_warehouseProductGridSalesOrder");
		txt_productAssemblyCostEstimationAssemblyProcess = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_productAssemblyCostEstimationAssemblyProcess");
		header_assemblyCostEstimation = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_assemblyCostEstimation");
		txt_customerAssemblyCostEstimationAssemblyProcess = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_customerAssemblyCostEstimationAssemblyProcess");
		txt_descriptonAssemblyCostEstimationAssemblyProcess = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_descriptonAssemblyCostEstimationAssemblyProcess");
		btn_searchPricingProfileAssemblyCostEstimationAssemblyProcess = findElementInXLSheet(
				getParameterInvertoryAndWarehouse, "btn_searchPricingProfileAssemblyCostEstimationAssemblyProcess");
		btn_tabEstimationDeatailsAseemblyCostEstmationAssemblyProcess = findElementInXLSheet(
				getParameterInvertoryAndWarehouse, "btn_tabEstimationDeatailsAseemblyCostEstmationAssemblyProcess");
		tbl_costEstimationProductAssemblyProcess = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"tbl_costEstimationProductAssemblyProcess");
		lnk_resultInputProductAssemblyProcessCostEstimation = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lnk_resultInputProductAssemblyProcessCostEstimation");
		btn_summaryCostEstimationAssemblyProcess = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_summaryCostEstimationAssemblyProcess");
		btn_checkoutCostEstimationAssemblyProcess = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_checkoutCostEstimationAssemblyProcess");
		lbl_ttalProfitCostEstimationAssemblyProcess = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_ttalProfitCostEstimationAssemblyProcess");
		lbl_esitimationTdAssemblyProcess = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_esitimationTdAssemblyProcess");
		lbl_unitCostAfterApplyCostEstimationAssmblyProcess = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_unitCostAfterApplyCostEstimationAssmblyProcess");
		btn_genrateEstimationAssemblyProcess = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_genrateEstimationAssemblyProcess");
		btn_applyToMAinGridAssemblyProcess = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_applyToMAinGridAssemblyProcess");
		lbl_bannerTotalAssemblyProcess = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_bannerTotalAssemblyProcess");
		btn_okAssemblyProcess = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_okAssemblyProcess");
		btn_genarateAssemblyOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_genarateAssemblyOrder");
		header_popupAssemblyOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_popupAssemblyOrder");
		btn_taskAssignUserLookup = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_taskAssignUserLookup");
		txt_userSearch = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_userSearch");
		btn_resultUserNialPeeris = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_resultUserNialPeeris");
		txt_userPopupPageAssemblyOrerAssembluProcess = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_userPopupPageAssemblyOrerAssembluProcess");
		chk_assemblyOrderAssemblyPopup = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_assemblyOrderAssemblyPopup");
		btn_applyAssemblyPopupAssemblyOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_applyAssemblyPopupAssemblyOrder");
		btn_entutionLogo = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_entutionLogo");
		btn_taskTile = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_taskTile");
		btn_assemblyOrderTile = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_assemblyOrderTile");
		btn_arrowGoToTaskReleventAssemblyOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_arrowGoToTaskReleventAssemblyOrder");
		headrer_assemblyOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse, "headrer_assemblyOrder");
		btn_jobStart = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_jobStart");
		txt_timeStartAssemblyProcess = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_timeStartAssemblyProcess");
		txt_timeEndAssemblyProcess = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_timeEndAssemblyProcess");
		btn_startAssemblyProcess = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_startAssemblyProcess");
		lbl_statusAssemblyOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lbl_statusAssemblyOrder");
		btn_cretaeInternelOrderAssemblyProcess = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_cretaeInternelOrderAssemblyProcess");
		header_internelOrderPopup = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_internelOrderPopup");
		chk_InternelOrerProductAssemblyProcess = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_InternelOrerProductAssemblyProcess");
		btn_applyInternelOrderAssemblyProcess = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_applyInternelOrderAssemblyProcess");
		txt_shipAddressAssemblyProcess = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_shipAddressAssemblyProcess");
		btn_outputProductUpdateAssemblyProcess = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_outputProductUpdateAssemblyProcess");
		header_updateOutputProdictAssemblyProcess = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_updateOutputProdictAssemblyProcess");
		chk_productThatUpdateAssemblyProcess = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_productThatUpdateAssemblyProcess");
		btn_applyAssemblyProcess = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_applyAssemblyProcess");
		btn_jobComplteAssemblyProcess = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_jobComplteAssemblyProcess");
		btn_complteJobComplteAssemblyProcess = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_complteJobComplteAssemblyProcess");
		btn_convertToInternelReciptAssemblyProcess = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_convertToInternelReciptAssemblyProcess");
		header_genarateInternelReciptAssemblyProcess = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_genarateInternelReciptAssemblyProcess");
		chk_selectAllInternelReciptAssemblyProcess = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_selectAllInternelReciptAssemblyProcess");
		btn_checkoutInternelReciptsAssemblyProcess = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_checkoutInternelReciptsAssemblyProcess");
		div_verifySerielAutoLoadAssemblyProductAssemblyProcess = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_verifySerielAutoLoadAssemblyProductAssemblyProcess");
		btn_itemNumber2ProductCaptureWindowAssemblyProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_itemNumber2ProductCaptureWindowAssemblyProduct");
		btn_itemNumber1ProductCaptureWindowAssmblyProcess = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_itemNumber1ProductCaptureWindowAssmblyProcess");
		btn_itemNumber3ProductCaptureWindowAssemblyProcess = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_itemNumber3ProductCaptureWindowAssemblyProcess");
		btn_productLoockup = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_productLoockup");

		// IW_TC_019
		btn_barcodeGenerator = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_barcodeGenerator");
		icon_inboundShipmentBarcodeGeneratorPage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"icon_inboundShipmentBarcodeGeneratorPage");
		span_dateRangeBarcodeGeneratorPage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"span_dateRangeBarcodeGeneratorPage");
		btn_productLookupSearchILO = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_productLookupSearchILO");
		txt_searchBarcodeGeneratorPage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_searchBarcodeGeneratorPage");
		btn_docSaecrhBarcodeGeneratorPage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_docSaecrhBarcodeGeneratorPage");
		dropdown_warehouseBarcodeGeneratorPage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"dropdown_warehouseBarcodeGeneratorPage");
		btn_docSaecrhBarcodeGeneratorPage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_docSaecrhBarcodeGeneratorPage");
		lbl_relesedInboundShipmentNoBarcodeGeneratorPage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_relesedInboundShipmentNoBarcodeGeneratorPage");
		btn_todayDateSetBarcodeGeneratorPage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_todayDateSetBarcodeGeneratorPage");
		btn_barcodeGenerateBarcodeGeneratorPage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_barcodeGenerateBarcodeGeneratorPage");
		dropdown_batchBookGenerateBarcodeGeneratorPage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"dropdown_batchBookGenerateBarcodeGeneratorPage");
		txt_lotGenerateBarcodeGeneratorPage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_lotGenerateBarcodeGeneratorPage");
		btn_generateGenerateWindowBarcodeGeneratorPage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_generateGenerateWindowBarcodeGeneratorPage");
		btn_updateGenerateWindowBarcodeGeneratorPage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_updateGenerateWindowBarcodeGeneratorPage");
		td_varianvcePro1 = findElementInXLSheet(getParameterInvertoryAndWarehouse, "td_varianvcePro1");
		td_varianvcePro2 = findElementInXLSheet(getParameterInvertoryAndWarehouse, "td_varianvcePro2");
		btn_viewBarcodeGenarate = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_viewBarcodeGenarate");
		btn_printBarcodeGenarate = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_printBarcodeGenarate");
		lbl_productAvailability019 = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_productAvailability019");

		// IW_TC_020
		btn_serialNumberViewer = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_serialNumberViewer"); // IW_TC_007
		btn_productSearchSerialNumberViewer = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_productSearchSerialNumberViewer");// IW_TC_007
		checkbox_inStockProductSerielBatchViewer = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"checkbox_inStockProductSerielBatchViewer");
		btn_serachProductSerielBatchViewer = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_serachProductSerielBatchViewer");
		header_resultCountSerielBatchViewer = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_resultCountSerielBatchViewer");
		drop_warehouseSerialBacthViewer = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"drop_warehouseSerialBacthViewer");

		// IW_TC_022
		// QC Acceptance
		btn_adminModule = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_adminModule");
		btn_balancingLevelAdminModule = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_balancingLevelAdminModule");
		dropdown_QCAcceptanaceJourneyAdminModule = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"dropdown_QCAcceptanaceJourneyAdminModule");
		btn_updateBalancingLevelQCAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_updateBalancingLevelQCAcceptance");
		btn_qcAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_qcAcceptance");
		drpdown_selectionQCAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"drpdown_selectionQCAcceptance");
		row_ifMoreRowAvailableQCAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"row_ifMoreRowAvailableQCAcceptance");
		chk_qcAcceptGeneral = findElementInXLSheet(getParameterInvertoryAndWarehouse, "chk_qcAcceptGeneral");
		btn_updateGenaralSectionBalancingLevel = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_updateGenaralSectionBalancingLevel");
		lnk_infoOnLoolkupInfoReplace = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lnk_infoOnLoolkupInfoReplace");
		// End QC Acceptance
		lnk_outboundShipmentNoQCAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lnk_outboundShipmentNoQCAcceptance");
		icon_serielNosQCAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"icon_serielNosQCAcceptance");
		txt_passQtyQCAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_passQtyQCAcceptance");
		txt_failQtyQCAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_failQtyQCAcceptance");
		txt_reasonQCAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_reasonQCAcceptance");
		btn_inboundShipment = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_inboundShipment");
		txt_searchInboundShipment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_searchInboundShipment");
		dropdown_templateQcAcceptancePage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"dropdown_templateQcAcceptancePage");
		btn_batchSearialWiseProductAvailabilityQCAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_batchSearialWiseProductAvailabilityQCAcceptance");
		cell_firstProductBatchSerialWiseAvailableTableQCAcceptnnce = findElementInXLSheet(
				getParameterInvertoryAndWarehouse, "cell_firstProductBatchSerialWiseAvailableTableQCAcceptnnce");
		tbl_QCAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse, "tbl_QCAcceptance");
		lbl_outboundShipmentDocNumberQCAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_outboundShipmentDocNumberQCAcceptance");
		header_QCAcceptanceForm = findElementInXLSheet(getParameterInvertoryAndWarehouse, "header_QCAcceptanceForm");
		txt_docSearchQCAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_docSearchQCAcceptance");
		chk_QCAccept = findElementInXLSheet(getParameterInvertoryAndWarehouse, "chk_QCAccept");
		btn_docInboundSelectionQCAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_docInboundSelectionQCAcceptance");
		btn_lastDocInboundSelectionQCAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_lastDocInboundSelectionQCAcceptance");
		tbl_qcAcceptanceProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse, "tbl_qcAcceptanceProduct");
		header_QCAcceptancePage = findElementInXLSheet(getParameterInvertoryAndWarehouse, "header_QCAcceptancePage");
		txt_batchNoProductCapture022 = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_batchNoProductCapture022");

		// Common Dispatch only batch product
		dropdown_productGroupDispatchOnlyBatchProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"dropdown_productGroupDispatchOnlyBatchProduct");
		txt_menufacturerDispatchOnlyBatchProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_menufacturerDispatchOnlyBatchProduct");
		dropdownDefoultUOMGroupDispatchOnlyBatchProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"dropdownDefoultUOMGroupDispatchOnlyBatchProduct");
		dropdownDefoultUOMDispatchOnlyBatchProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"dropdownDefoultUOMDispatchOnlyBatchProduct");
		tab_deatilsDispatchOnlyBatchProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"tab_deatilsDispatchOnlyBatchProduct");
		btn_inventoryActiveDispatchOnlyBatchProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_inventoryActiveDispatchOnlyBatchProduct");
		chk_batchProductDispatchOnlyBatchProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_batchProductDispatchOnlyBatchProduct");
		dropdown_dispatchSelectionDispatchOnlyBatchProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"dropdown_dispatchSelectionDispatchOnlyBatchProduct");

		// Common create manufacturer
		btn_menufacturerInformtion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_menufacturerInformtion");
		btn_newMenufacturer = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_newMenufacturer");
		txt_menufacturerCode = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_menufacturerCode");
		txt_nameMenufacturer = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_nameMenufacturer");
		btn_upadateMenufacturer = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_upadateMenufacturer");
		btn_menufacturerSearchDispatchOnlyBatchProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_menufacturerSearchDispatchOnlyBatchProduct");
		lnk_resultMenufacturerDispatchOnlyBatchProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lnk_resultMenufacturerDispatchOnlyBatchProduct");
		btn_serielNosQCAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_serielNosQCAcceptance");
		tab_summaryQCAcceptanec = findElementInXLSheet(getParameterInvertoryAndWarehouse, "tab_summaryQCAcceptanec");
		txt_lotNoCapturePage = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_lotNoCapturePage");
		btn_closeGoToLinkPopup = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_closeGoToLinkPopup");

		/* IW_TC_021 */
		btn_editSearial = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_editSearial");
		txt_searielNumberEditTo = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_searielNumberEditTo");
		btn_changeEditedSeriel = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_changeEditedSeriel");

		// IW_TC_023
		btn_procumentModule = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_procumentModule");
		btn_purchaseOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_purchaseOrder");
		btn_newPrurchaseOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_newPrurchaseOrder");
		btn_purchaseOrderToInboundShipmentToPurchaseInvoiceJourney = findElementInXLSheet(
				getParameterInvertoryAndWarehouse, "btn_purchaseOrderToInboundShipmentToPurchaseInvoiceJourney");
		txt_vendorSearch23 = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_vendorSearch23");
		lnk_resultVendorSeacrh23 = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lnk_resultVendorSeacrh23");
		btn_serachProductLookup23 = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_serachProductLookup23");
		txt_qtyProductPurchaseOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_qtyProductPurchaseOrder");
		txt_unitPricePurchaseOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_unitPricePurchaseOrder");
		drop_warehousePurchaseOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"drop_warehousePurchaseOrder");
		btn_checkoutPurchaaseOrdeer = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_checkoutPurchaaseOrdeer");
		drop_inboundShipmentAsseptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"drop_inboundShipmentAsseptance");
		btn_shipmentAsseptance = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_shipmentAsseptance");
		btn_serielNosQCAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_serielNosQCAcceptance");
		drop_documentTypeShipmentAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"drop_documentTypeShipmentAcceptance");
		txt_docNoInboundShipmentShipmentAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_docNoInboundShipmentShipmentAcceptance");
		btn_serielNosShipmentAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_serielNosShipmentAcceptance");
		chk_rangeCaptureAShipmentAsseptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_rangeCaptureAShipmentAsseptance");
		txt_rangecountShipmentAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_rangecountShipmentAcceptance");
		txt_serielNumberStartShipmentAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_serielNumberStartShipmentAcceptance");
		txt_lotNumberStartShipmentAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_lotNumberStartShipmentAcceptance");
		plicker_exoiryDateShipmentAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"plicker_exoiryDateShipmentAcceptance");
		btn_arrowToNExtMotnthShipmentAcceptanse = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_arrowToNExtMotnthShipmentAcceptanse");
		btn_date28ShipmentAcceptanse = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_date28ShipmentAcceptanse");
		btn_applyShipmentAcceptanceCaptureProdut = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_applyShipmentAcceptanceCaptureProdut");
		chk_releseSelectionShipmentAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_releseSelectionShipmentAcceptance");
		btn_releseShipmentAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_releseShipmentAcceptance");
		lnk_wiewInboundShipmenthipmentAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lnk_wiewInboundShipmenthipmentAcceptance");
		btn_draftPurchaseInvoiseShipmentAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_draftPurchaseInvoiseShipmentAcceptance");
		btn_checkoutPurchaseInvoiceShipmentAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_checkoutPurchaseInvoiceShipmentAcceptance");
		btn_releesePurchseInvoiceshipmentAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_releesePurchseInvoiceshipmentAcceptance");
		btn_vendorSearchPurchaseOredr = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_vendorSearchPurchaseOredr");
		lnk_resultProductPurchaseOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lnk_resultProductPurchaseOrder");
		lbl_releseForPickingInboundShipment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_releseForPickingInboundShipment");
		lbl_inboundShipmentDocNoShipmentAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_inboundShipmentDocNoShipmentAcceptance");
		chk_verifyReleeseIsSelected = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_verifyReleeseIsSelected");
		lblDocumentStatusAfterShipmentAccewptanceOutboundShipment = findElementInXLSheet(
				getParameterInvertoryAndWarehouse, "lblDocumentStatusAfterShipmentAccewptanceOutboundShipment");
		btn_goToPurchaseInvoiveInboundShipmet = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_goToPurchaseInvoiveInboundShipmet");
		btn_deleestShipmentAceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_deleestShipmentAceptance");
		header_inboundShipmemtQC = findElementInXLSheet(getParameterInvertoryAndWarehouse, "header_inboundShipmemtQC");
		widget_checkAvailabilityProductQC = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"widget_checkAvailabilityProductQC");
		btn_productAvailabilityQC = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_productAvailabilityQC");
		td_productAvailableQuantityQC = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"td_productAvailableQuantityQC");
		header_productAvailabilityPopup = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_productAvailabilityPopup");
		lbl_docNo = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lbl_docNo");
		btn_releseShipmentAcceptanceJS = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_releseShipmentAcceptanceJS");
		chk_shipemtAceptanceGeneral = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_shipemtAceptanceGeneral");
		div_productShipmentAcceptanceOnShipmentAcceptanceGrid = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_productShipmentAcceptanceOnShipmentAcceptanceGrid");

		/* Fin_TC_009 */
		btn_stockTake = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_stockTake");
		btn_newStockTake = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_newStockTake");
		drop_warehouseStockTske = findElementInXLSheet(getParameterInvertoryAndWarehouse, "drop_warehouseStockTske");
		btn_refreshStockTake = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_refreshStockTake");
		txt_filterProductStockTake = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_filterProductStockTake");
		txt_physicalQuantityStockTake = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_physicalQuantityStockTake");
		txt_systemQuantityStockTake = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_systemQuantityStockTake");
		btn_convertToStockAdjustment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_convertToStockAdjustment");
		btn_captureItem1 = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_captureItem1");
		btn_captureItem2 = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_captureItem2");
		txt_batchNoCapturePage = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_batchNoCapturePage");
		btn_capturePageDocIcon = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_capturePageDocIcon");
		chk_captureFirstRowSelect = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_captureFirstRowSelect");
		header_page = findElementInXLSheet(getParameterInvertoryAndWarehouse, "header_page");
		lbl_docStatus1 = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lbl_docStatus1");
		row_stockTakeTable = findElementInXLSheet(getParameterInvertoryAndWarehouse, "row_stockTakeTable");
		btn_action = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_action");
		heade_page = findElementInXLSheet(getParameterInvertoryAndWarehouse, "heade_page");
		lbl_docStatus = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lbl_docStatus");
		lbl_bannerTotal = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lbl_bannerTotal");
		txt_lotNoCapturePageStockTake = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_lotNoCapturePageStockTake");

		/* Action Verification Common */
		btn_convertToCommonActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_convertToCommonActionVerification");
		header_commonActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_commonActionVerification");

		/* AV_IW_ProductGroup */
		btn_editActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_editActionVerification");
		btn_updateActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_updateActionVerification");
		btn_updateAndNewActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_updateAndNewActionVerification");
		btn_duplicateActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_duplicateActionVerification");
		btn_draftAndNewActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_draftAndNewActionVerification");
		btn_copyFromActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_copyFromActionVerification");
		header_productGroupLookupActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_productGroupLookupActionVerification");
		btn_closeLookupActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_closeLookupActionVerification");
		div_historyTableActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_historyTableActionVerification");
		btn_historyActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_historyActionVerification");
		btn_activitiesActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_activitiesActionVerification");
		btn_activitiesHeaderActionVrification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_activitiesHeaderActionVrification");
		btn_configureCategoriesActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_configureCategoriesActionVerification");
		btn_configureCategoriesHeaderActionVrification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_configureCategoriesHeaderActionVrification");
		btn_deleteActionVrification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_deleteActionVrification");
		btn_deleteConfirationActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_deleteConfirationActionVerification");

		/* AV_IW_WarehouseInformation */
		header_warehouseLookupActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_warehouseLookupActionVerification");

		/* AV_IW_AllocationManager */
		btn_allocationManger = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_allocationManger");
		btn_genarateAllocationMangerActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_genarateAllocationMangerActionVerification");
		chk_manageDocumentAllocationManagerActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_manageDocumentAllocationManagerActionVerification");
		header_allocationManagerActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_allocationManagerActionVerification");

		/* AV_IW_ProductInformation */
		header_productLookupActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_productLookupActionVerification");
		btn_sellingPriceActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_sellingPriceActionVerification");
		btn_PLUCodeAllocationMangerActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_PLUCodeAllocationMangerActionVerification");
		btn_remindersAllocationMangerActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_remindersAllocationMangerActionVerification");
		header_reminderActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_reminderActionVerification");
		header_sellingPriceActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_sellingPriceActionVerification");
		header_updatePluCodesActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_updatePluCodesActionVerification");

		/* AV_IW_InventoryParameters */
		btn_inventoryParameters = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_inventoryParameters");
		btn_plusMarkInventoryParameters = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_plusMarkInventoryParameters");
		txt_parameterActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_parameterActionVerification");
		para_validator = findElementInXLSheet(getParameterInvertoryAndWarehouse, "para_validator");

		/* AV_IW_AssemblyCostEstimation */
		header_costEstimationActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_costEstimationActionVerification");
		btn_reverseActionVrification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_reverseActionVrification");
		btn_reverseConfirationActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_reverseConfirationActionVerification");

		/* AV_IW_StockTake */
		table_stockTakeActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"table_stockTakeActionVerification");
		btn_dtaftedStockTakeActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_dtaftedStockTakeActionVerification");

		/* AV_IW_InterDepartmentTarnsferOrder */
		btn_dockflowActionVrification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_dockflowActionVrification");
		header_documentFlowActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_documentFlowActionVerification");

		/* Create common products */
		txt_minPriceTextProductCreation = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_minPriceTextProductCreation");
		txt_maxPriceTextProductCreation = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_maxPriceTextProductCreation");

		/* AV_IW_OutboundLOanOrder */
		btn_convertToInboundLoanOrderActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_convertToInboundLoanOrderActionVerification");
		btn_convertToInboundShimentActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_convertToInboundShimentActionVerification");
		btn_convertToSalesInvoiceActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_convertToSalesInvoiceActionVerification");

		/* AV_IW_AssemblyProposal */
		btn_assemblyProposal = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_assemblyProposal");
		btn_newAssemblyProcess = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_newAssemblyProcess");
		drop_priorityAssemblyProposal = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"drop_priorityAssemblyProposal");
		datepliker_requestDateActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"datepliker_requestDateActionVerification");
		datepliker_requestEndDateActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"datepliker_requestEndDateActionVerification");
		txt_productSearchActionVerifcation = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_productSearchActionVerifcation");
		btn_resultAssemblyProductActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_resultAssemblyProductActionVerification");
		btn_productionUnitLookupActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_productionUnitLookupActionVerification");
		txt_productionUnitSearchActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_productionUnitSearchActionVerification");
		result_productionUnitActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"result_productionUnitActionVerification");
		drop_warehouseAssemblyProcessActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"drop_warehouseAssemblyProcessActionVerification");
		btn_draftActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_draftActionVerification");
		btn_convertToAssemblyOrderActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_convertToAssemblyOrderActionVerification");
		header_assemblyOrderTaskActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_assemblyOrderTaskActionVerification");
		header_assemblyProposalActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_assemblyProposalActionVerification");

		/* AV_IW_RackTransfer */
		btn_rackTransfer = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_rackTransfer");
		btn_newRackTransfer = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_newRackTransfer");
		drop_warehouseRackTransfer = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"drop_warehouseRackTransfer");
		drop_filterBy = findElementInXLSheet(getParameterInvertoryAndWarehouse, "drop_filterBy");
		btn_productDocIconRackTransfer = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_productDocIconRackTransfer");
		chk_productSelectRackTransfer = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_productSelectRackTransfer");
		btn_applyProductDocIconRackTransfer = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_applyProductDocIconRackTransfer");
		btn_rackTransferNosIcon = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_rackTransferNosIcon");
		txt_quantityRackTransfer = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_quantityRackTransfer");

		/* AV_IW_AssemblyOrder */
		header_JobStartHoltActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_JobStartHoltActionVerification");
		btn_stratJobActionVerificationAssemblyOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_stratJobActionVerificationAssemblyOrder");
		txt_changeReson = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_changeReson");
		btn_okAssemblyOrderHoldUnholdActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_okAssemblyOrderHoldUnholdActionVerification");
		btn_holdActionSectionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_holdActionSectionVerification");
		btn_unholdActionSectionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_unholdActionSectionVerification");
		btn_closeActionSectionVerificatio = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_closeActionSectionVerificatio");
		btn_completeJobActionVerificationAssemblyOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_completeJobActionVerificationAssemblyOrder");

		/* AV_IW_OutboundShipment */
		drop_printInboundShipmentActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"drop_printInboundShipmentActionVerification");
		btn_reverseConfirmInboundShipmentActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_reverseConfirmInboundShipmentActionVerification");

		/* AV_IW_InboundLoanOrder */
		btn_convertToOutboundLoanOrderActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_convertToOutboundLoanOrderActionVerification");
		btn_convertToOutboundShimentActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_convertToOutboundShimentActionVerification");
		btn_convertToPurchaseInvoiceActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_convertToPurchaseInvoiceActionVerification");

		/* AV_IW_InternelRecipt */
		btn_journelActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_journelActionVerification");
		btn_journelHeaderActionVrification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_journelHeaderActionVrification");

		/* AV_IW_InternelDispathOrder */
		btn_availabilityCheckWidgetActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_availabilityCheckWidgetActionVerification");
		btn_productAvailabilityRackWiseActionVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_productAvailabilityRackWiseActionVerification");
		btn_rackWiseProductAvailabilityHeaderActionVrification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_rackWiseProductAvailabilityHeaderActionVrification");

		/******************* Inventory And Warehouse Creations ********************/

		/* Lot Book */
		btn_trnsactionBookConfiguration = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_trnsactionBookConfiguration");
		drop_filterByTransactionBookConfiguration = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"drop_filterByTransactionBookConfiguration");
		btn_warehouseLotBookConfiguration = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_warehouseLotBookConfiguration");
		btn_transactionBookConfigRowAdding = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_transactionBookConfigRowAdding");
		txt_bookNoTBC = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_bookNoTBC");
		txt_prefixTBCWarehouse = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_prefixTBCWarehouse");
		chk_defoultWarehouseTBC = findElementInXLSheet(getParameterInvertoryAndWarehouse, "chk_defoultWarehouseTBC");
		btn_updateWarehouseTransactionBookConfiguration = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_updateWarehouseTransactionBookConfiguration");

		/*********************** Regression01 **********************/
		/* IN_IO_002 */
		header_journeyList = findElementInXLSheet(getParameterInvertoryAndWarehouse, "header_journeyList");
		header_newsStatus = findElementInXLSheet(getParameterInvertoryAndWarehouse, "header_newsStatus");

		/* IN_IO_004 */
		btn_genarelBalancingLevelSetting = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_genarelBalancingLevelSetting");
		drop_employeeSerchConfig = findElementInXLSheet(getParameterInvertoryAndWarehouse, "drop_employeeSerchConfig");
		btn_updateBalncilgLevelEmployeeSerchConfig = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_updateBalncilgLevelEmployeeSerchConfig");

		/* IN_IO_005 */
		btn_campaigsSalesModule = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_campaigsSalesModule");
		btn_resultCampaignSalesModule = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_resultCampaignSalesModule");
		btn_statusDocument = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_statusDocument");
		btn_edit = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_edit");
		btn_update = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_update");
		drop_docStatus = findElementInXLSheet(getParameterInvertoryAndWarehouse, "drop_docStatus");
		btn_applyStatusMenu = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_applyStatusMenu");
		btn_campaignLookup = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_campaignLookup");
		btn_resultCampaign = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_resultCampaign");
		txt_campaignFrontPage = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_campaignFrontPage");
		txt_searchCampaign = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_searchCampaign");

		/* IN_IO_006 */
		txt_productAutoSearch = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_productAutoSearch");
		btn_productLoockupCommon = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_productLoockupCommon");
		result_varientMainProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"result_varientMainProduct");
		txt_prductGridSelectedProductOnlyOneRowIsThere = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_prductGridSelectedProductOnlyOneRowIsThere");
		header_productLookup = findElementInXLSheet(getParameterInvertoryAndWarehouse, "header_productLookup");
		result_cpmmonProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse, "result_cpmmonProduct");

		/* IN_IO_007 */
		td_uomWhenThereIsOneRow = findElementInXLSheet(getParameterInvertoryAndWarehouse, "td_uomWhenThereIsOneRow");

		/* IN_IO_008 */
		td_OHWhenThereIsOneRow = findElementInXLSheet(getParameterInvertoryAndWarehouse, "td_OHWhenThereIsOneRow");
		widget_checkAvailabilityWhenOneRowIsThere = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"widget_checkAvailabilityWhenOneRowIsThere");
		btn_productAvailabilitySecondMenuWhenOneRowIsThere = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_productAvailabilitySecondMenuWhenOneRowIsThere");
		drop_wareWhenOnlyOneRowInGrid = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"drop_wareWhenOnlyOneRowInGrid");

		/* IN_IO_009 */
		btn_inentoryBalancingLevelSetting = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_inentoryBalancingLevelSetting");
		drop_productSerchConfig = findElementInXLSheet(getParameterInvertoryAndWarehouse, "drop_productSerchConfig");
		btn_updateBalncilgLevelProductSerchConfig = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_updateBalncilgLevelProductSerchConfig");

		/* IN_IO_010 */
		btn_draftCommon = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_draftCommon");

		/* IN_IO_011 */
		btn_draftAndNewCommon = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_draftAndNewCommon");

		/* IN_IO_012 */
		btn_releaseCommon = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_releaseCommon");

		/* IN_IO_014 */
		btn_copyFromCommon = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_copyFromCommon");
		header_IOLookup = findElementInXLSheet(getParameterInvertoryAndWarehouse, "header_IOLookup");
		txt_internelOrderLookupSearch = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_internelOrderLookupSearch");
		btn_resultIO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_resultIO");
		tab_summaryCommon = findElementInXLSheet(getParameterInvertoryAndWarehouse, "tab_summaryCommon");

		/* IN_IO_016 */
		link_resultDocInList = findElementInXLSheet(getParameterInvertoryAndWarehouse, "link_resultDocInList");

		/* IN_IO_019 */
		btn_taskTileCommon = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_taskTileCommon");
		btn_internelDispatchOrderTile = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_internelDispatchOrderTile");
		txt_taskSearch = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_taskSearch");
		btn_releventTaskOpenLink = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_releventTaskOpenLink");
		header_InternelDispatchOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_InternelDispatchOrder");

		/* IN_IO_022 */
		lbl_titleIOFrontPage = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lbl_titleIOFrontPage");
		lbl_requesterIOFrontPage = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lbl_requesterIOFrontPage");
		lbl_productIOFrontPage = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lbl_productIOFrontPage");
		lbl_warehouseIOFrontPage = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lbl_warehouseIOFrontPage");
		btn_duplicate = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_duplicate");
		txt_nameInternelOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_nameInternelOrder");
		txt_requstrerInternelOrderFrontPage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_requstrerInternelOrderFrontPage");
		txt_productInternelOrderFrontPage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_productInternelOrderFrontPage");
		txt_warehouseInternelOrderFrontPage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_warehouseInternelOrderFrontPage");

		/* IN_IO_024 */
		btn_homeIcon = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_homeIcon");
		btn_deleteRowRoReplaceInternelDispatchOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_deleteRowRoReplaceInternelDispatchOrder");

		/* IN_IO_025 */
		lbl_binCardBalanceLastRow = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_binCardBalanceLastRow");
		btn_reports = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_reports");
		drop_reportTemplate = findElementInXLSheet(getParameterInvertoryAndWarehouse, "drop_reportTemplate");
		btn_editTemplateInReports = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_editTemplateInReports");
		btn_quickPreviewRepots = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_quickPreviewRepots");
		txt_filterProductBinCardReports = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_filterProductBinCardReports");

		/* IN_IO_025_I_026_I_027 */
		lbl_docValueJournelEntryInternelDispatchOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_docValueJournelEntryInternelDispatchOrder");

		/* IN_IO_036 */
		btn_journeyConfiguration = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_journeyConfiguration");
		chk_stockValidate = findElementInXLSheet(getParameterInvertoryAndWarehouse, "chk_stockValidate");
		drop_stockReservationMethod = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"drop_stockReservationMethod");
		btnn_updateEmployeeRequestJourneyConfiguration = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btnn_updateEmployeeRequestJourneyConfiguration");
		btn_advanceWidgetInternelOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_advanceWidgetInternelOrder");
		txt_reservedQuantityAdvanceMenu = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_reservedQuantityAdvanceMenu");
		btn_aplyAdvanceMenuInternelOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_aplyAdvanceMenuInternelOrder");
		btn_journeyConfigInternelOrderEmployeeRequest = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_journeyConfigInternelOrderEmployeeRequest");
		lbl_reserverdQuantityAllocationManager = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_reserverdQuantityAllocationManager");
		txt_docSearchAllocationManager = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_docSearchAllocationManager");
		btn_documentSearchAllocationManager = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_documentSearchAllocationManager");

		/* IN_IO_040 */
		div_lineValidationProductCapture = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_lineValidationProductCapture");
		btn_errorViewGrid = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_errorViewGrid");
		div_errorGridQuantityNotAvailable = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_errorGridQuantityNotAvailable");
		btn_lookupFromProductProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_lookupFromProductProductConversion");
		txt_fromQuantityProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_fromQuantityProductConversion");
		btn_serleNosProductConversionFromProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_serleNosProductConversionFromProduct");
		btn_lookupToProductProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_lookupToProductProductConversion");
		btn_serleNosProductConversionToProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_serleNosProductConversionToProduct");
		div_quantityDraftedStockAdjustment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_quantityDraftedStockAdjustment");
		btn_deleteRowCommonRowReplace = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_deleteRowCommonRowReplace");
		para_validatorErrorMessageSerialNos = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"para_validatorErrorMessageSerialNos");

		/* IN_IO_041 */
		div_reservedQuantity = findElementInXLSheet(getParameterInvertoryAndWarehouse, "div_reservedQuantity");

		/* IN_IO_042_043_044 */
		btn_productAlternativeWidget = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_productAlternativeWidget");
		chk_productAlternative = findElementInXLSheet(getParameterInvertoryAndWarehouse, "chk_productAlternative");
		lbl_ohAlternative = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lbl_ohAlternative");

		/* IN_IO_049 */
		btn_commonResultProductContains = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_commonResultProductContains");
		lbl_draftedInternelOrderQuantityWhenOnlyOneRowIsThere = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_draftedInternelOrderQuantityWhenOnlyOneRowIsThere");

		/* IN_IO_050 */
		btn_resultProductInSecrhByPage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_resultProductInSecrhByPage");
		chk_allowDecimal = findElementInXLSheet(getParameterInvertoryAndWarehouse, "chk_allowDecimal");

		/* IN_IO_051 */
		btn_removeUnavilableProductFromGrid = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_removeUnavilableProductFromGrid");
		btn_unavailbleProductRemoveConfirmation = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_unavailbleProductRemoveConfirmation");
		div_productVerifyInGridIDO = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_productVerifyInGridIDO");

		/* IN_IO_054 */
		btn_workflowConfiguration = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_workflowConfiguration");
		txt_workflowSerch = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_workflowSerch");
		btn_actieWorkflow = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_actieWorkflow");
		btn_inctieWorkflow = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_inctieWorkflow");
		btn_firstButtonSignOut = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_firstButtonSignOut");
		btn_signOutLink = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_signOutLink");
		btn_sendReview = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_sendReview");
		btn_approvalTab = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_approvalTab");
		btn_approvalOk = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_approvalOk");
		header_workflow_IN_IO_054 = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_workflow_IN_IO_054");
		btn_workflowActiveConfirmation = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_workflowActiveConfirmation");
		header_draftApproved = findElementInXLSheet(getParameterInvertoryAndWarehouse, "header_draftApproved");
		header_draftReviewing = findElementInXLSheet(getParameterInvertoryAndWarehouse, "header_draftReviewing");
		header_newInternelOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse, "header_newInternelOrder");

		/* IN_IO_055_058 */
		btn_stockAvailabilityWidgetAfterReleasedInternelOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_stockAvailabilityWidgetAfterReleasedInternelOrder");

		/* IN_IO_056 */
		header_releasedInternalOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_releasedInternalOrder");

		/* IN_IO_063 */
		td_allocationMangerRecervedProductCount = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"td_allocationMangerRecervedProductCount");
		btn_okHoldUnhold = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_okHoldUnhold");

		/* IN_IO_065 */
		btn_availabilityWidget = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_availabilityWidget");

		/* IN_IO_066 */
		btn_postDate = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_postDate");
		btn_postDateInsitePostDatePopup = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_postDateInsitePostDatePopup");
		btn_calenderBack = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_calenderBack");
		para_validatorErrorMessage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"para_validatorErrorMessage");
		header_newInternelOrderCommon = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_newInternelOrderCommon");
		header_internelDispatchOredrCommon = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_internelDispatchOredrCommon");
		link_commonDoubleCrickProductResult = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"link_commonDoubleCrickProductResult");
		headre_internalDispatchOrderReleasedStatus = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"headre_internalDispatchOrderReleasedStatus");

		/* IN_IO_067 */
		drop_warehouseInGrid = findElementInXLSheet(getParameterInvertoryAndWarehouse, "drop_warehouseInGrid");
		btn_productLookupInGrid = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_productLookupInGrid");
		txt_quantityInGrid = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_quantityInGrid");
		txt_unitPriceInGrid = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_unitPriceInGrid");
		btn_salesReturnOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_salesReturnOrder");
		btn_newSalesReturnOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_newSalesReturnOrder");
		btn_journeySalesReturnOrderInboundShipmentSalesReturnInvoice = findElementInXLSheet(
				getParameterInvertoryAndWarehouse, "btn_journeySalesReturnOrderInboundShipmentSalesReturnInvoice");
		btn_soSelectorDocIcomIRO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_soSelectorDocIcomIRO");
		drop_searchByIRO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "drop_searchByIRO");
		txt_docNomIROSerach = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_docNomIROSerach");
		btn_commonreturnSearch = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_commonreturnSearch");
		chk_commonReturnDoc = findElementInXLSheet(getParameterInvertoryAndWarehouse, "chk_commonReturnDoc");
		btn_commoApply = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_commoApply");
		drop_returnReason = findElementInXLSheet(getParameterInvertoryAndWarehouse, "drop_returnReason");
		btn_customerAccountLookupCommon = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_customerAccountLookupCommon");

		/* IN_IO_068 */
		btn_summaryTab = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_summaryTab");
		chk_allInternelOrderForReturn = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_allInternelOrderForReturn");

		/* IN_IO_069 */
		txt_serielNo = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_serielNo");
		txt_lotNo = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_lotNo");
		calender_serelSpecificCapturuePage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"calender_serelSpecificCapturuePage");
		chk_batchSelectProductConversion_IN_IO_069 = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_batchSelectProductConversion_IN_IO_069");

		/* IN_IO_070 */
		td_availableStockNewlyCreatedWarehouse = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"td_availableStockNewlyCreatedWarehouse");

		/* IN_IO_072 */
		drop_dispatchOnlySelectionSeriel = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"drop_dispatchOnlySelectionSeriel");
		btn_lookupProductsMultipleProductGrids = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_lookupProductsMultipleProductGrids");
		btn_genarateProductBarcode = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_genarateProductBarcode");
		btn_viewProductBarcode = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_viewProductBarcode");
		btn_expiryDateBArcodeGenarate = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_expiryDateBArcodeGenarate");
		btn_day28barcodeGenarator = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_day28barcodeGenarator");
		btn_dateNextBArcodeGenarator = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_dateNextBArcodeGenarator");
		select_serelBookBarcodeGenarate = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"select_serelBookBarcodeGenarate");
		btn_removeSuccessfulMessage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_removeSuccessfulMessage");
		btn_backBarcodeGenSerielBatchCaptureWindow = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_backBarcodeGenSerielBatchCaptureWindow");

		/* IN_IO_074 */
		btn_addRackPlusMark = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_addRackPlusMark");
		txt_rackName = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_rackName");
		btn_updateRacks = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_updateRacks");
		div_updateSuccessfullValidation = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_updateSuccessfullValidation");
		drop_rackRackTransfer = findElementInXLSheet(getParameterInvertoryAndWarehouse, "drop_rackRackTransfer");
		drop_destinationRackRackTransfer = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"drop_destinationRackRackTransfer");
		input_rackTarnsferQunatity = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"input_rackTarnsferQunatity");
		btn_destityRackSelectionMenuLastRow = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_destityRackSelectionMenuLastRow");
		chk_allProductsRackTransfer = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_allProductsRackTransfer");
		drop_rackSelectStockAdjustment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"drop_rackSelectStockAdjustment");
		btn_applyDestinationRack = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_applyDestinationRack");

		/* IN_IO_075 */
		btn_plusMarkProductAddStockTake = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_plusMarkProductAddStockTake");
		btn_productLookupStockTake = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_productLookupStockTake");
		txt_physicalBalanceStockTakeSearchedProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_physicalBalanceStockTakeSearchedProduct");
		btn_productApplyStockTake = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_productApplyStockTake");
		div_forVerifyPlusStockAdjustmentLoaded = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_forVerifyPlusStockAdjustmentLoaded");
		txt_systemBalanceStockTakeSearchedProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_systemBalanceStockTakeSearchedProduct");

		/* IN_IO_076_077_078 */
		row_relesedDetailsHistory = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"row_relesedDetailsHistory");
		row_draftDetailsHistory = findElementInXLSheet(getParameterInvertoryAndWarehouse, "row_draftDetailsHistory");
		row_updateDetailsHistory = findElementInXLSheet(getParameterInvertoryAndWarehouse, "row_updateDetailsHistory");
		header_docflowDocumentInternelOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_docflowDocumentInternelOrder");

		/* IN_IRO_Emp_001_002_003_004 */
		heder_internelReturnOrderByPage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"heder_internelReturnOrderByPage");
		heder_journeyPopup = findElementInXLSheet(getParameterInvertoryAndWarehouse, "heder_journeyPopup");
		heder_internelReturnOrderNewForm = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"heder_internelReturnOrderNewForm");
		btn_employeeReturnsJourney = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_employeeReturnsJourney");
		lnk_resultEmployeeCommon = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lnk_resultEmployeeCommon");
		btn_productLoockupsForVerifyAvailabiliyt = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_productLoockupsForVerifyAvailabiliyt");

		/* IN_IRO_Emp_006 */
		lnk_commonResultEmployee = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lnk_commonResultEmployee");
		td_countProductReleventToIDO = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"td_countProductReleventToIDO");

		/* IN_IRO_Emp_007 */
		txt_searchByProductIRO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_searchByProductIRO");
		txt_CommonSearchIRO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_CommonSearchIRO");

		/* IN_IRO_Emp_008 */
		chk_productsReleventToIDO = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_productsReleventToIDO");
		lbl_productIROGrid = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lbl_productIROGrid");
		div_selectedRowProductColumIRO = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_selectedRowProductColumIRO");

		/* IN_IRO_Emp_009 */
		div_allrowsByRowProductColumnProductSelectIRO = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_allrowsByRowProductColumnProductSelectIRO");

		/* IN_IRO_Emp_010 */
		btn_gridClearConfirmationIRO = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_gridClearConfirmationIRO");
		header_clearConfirmationIRO = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_clearConfirmationIRO");
		row_countSelectedProductsIRO = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"row_countSelectedProductsIRO");

		/* IN_IRO_Emp_011 */
		btn_deleteFirstRecordIRO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_deleteFirstRecordIRO");

		/* IN_IRO_Emp_012 */
		btn_advanceIDO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_advanceIDO");
		btn_serielBatchTabAdvanceWindow = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_serielBatchTabAdvanceWindow");
		lbl_lotAdvanceMenu = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lbl_lotAdvanceMenu");
		lbl_batchAdvanceMenu = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lbl_batchAdvanceMenu");
		btn_closeWindowPopup = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_closeWindowPopup");
		txt_quantityIROGrid = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_quantityIROGrid");
		lbl_lotNoIROGrid = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lbl_lotNoIROGrid");
		lbl_batchNoIROGrid = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lbl_batchNoIROGrid");
		lbl_expiryDateIROGrid = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lbl_expiryDateIROGrid");

		/* IN_IRO_Emp_013 */
		header_newInternalReturnOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_newInternalReturnOrder");

		/* IN_IRO_Emp_014 */
		header_andHeaderStatusDarftedIntrnalReturOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_andHeaderStatusDarftedIntrnalReturOrder");

		/* IN_IRO_Emp_017 */
		btn_deleteRowInternelReturnOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_deleteRowInternelReturnOrder");
		btn_availableProdctCountIRO = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_availableProdctCountIRO");
		lbl_productRowsDraftedIRO = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_productRowsDraftedIRO");
		chk_differentProductIRO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "chk_differentProductIRO");
		lbl_getProductIROGrid = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lbl_getProductIROGrid");
		btn_appplyDocumentListEditIRO = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_appplyDocumentListEditIRO");

		/* IN_IRO_Emp_019 */
		header_newInternalRecipt = findElementInXLSheet(getParameterInvertoryAndWarehouse, "header_newInternalRecipt");

		/* IN_IRO_Emp_020 */
		btn_internelReceiptTile = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_internelReceiptTile");

		/* IN_IRO_Emp_023 */
		btn_advanceMenuRowDynamic = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_advanceMenuRowDynamic");
		btn_advanceMenuLogisticSection = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_advanceMenuLogisticSection");
		chk_underDeliveryAdvanceMenu = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_underDeliveryAdvanceMenu");
		chk_partialDeliveryAdvanceMenu = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_partialDeliveryAdvanceMenu");
		div_changeCssAdvanceMenu = findElementInXLSheet(getParameterInvertoryAndWarehouse, "div_changeCssAdvanceMenu");
		txt_quantityInternalReceiptGrid = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_quantityInternalReceiptGrid");
		txt_planedQuantityInternalReceiptGridAfterUpdate = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_planedQuantityInternalReceiptGridAfterUpdate");

		/* IN_IRO_Emp_044 */
		header_docflowSecondDocument = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_docflowSecondDocument");
		header_docflowThirdDocument = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_docflowThirdDocument");

		/* IN_TO_001 */
		header_transferOrderByPage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_transferOrderByPage");

		/* IN_TO_003 */
		txt_fromWarehouseAddressFrontPage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_fromWarehouseAddressFrontPage");

		/******** Transfer Order **********/
		/* IN_TO_005 */
		txt_addressWarehouseCreation = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_addressWarehouseCreation");
		txt_destinationAddressTransferOrderFrontPage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_destinationAddressTransferOrderFrontPage");

		/* IN_TO_006 */
		btn_addressLookupFromWarehouse = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_addressLookupFromWarehouse");
		btn_addressLookupToWarehouse = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_addressLookupToWarehouse");
		btn_applyAddress = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_applyAddress");
		txt_addressOnLookup = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_addressOnLookup");
		header_addressLookup = findElementInXLSheet(getParameterInvertoryAndWarehouse, "header_addressLookup");

		/* IN_TO_008 */
		txt_autoSearchQuantity = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_autoSearchQuantity");
		txt_addedProductInGridSingleProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_addedProductInGridSingleProduct");
		txt_qtyTransferOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_qtyTransferOrder");

		/* IN_TO_010 */
		row_multipleRowSelectionOnProductLookup = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"row_multipleRowSelectionOnProductLookup");
		txt_addedProductInGridMultipleProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_addedProductInGridMultipleProduct");

		/* IN_TO_011 */
		div_defoultUOMNainGridTransferOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_defoultUOMNainGridTransferOrder");

		/* IN_TO_012 */
		div_ohQuantityToTransferOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_ohQuantityToTransferOrder");
		div_ohQuantityFromTransferOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_ohQuantityFromTransferOrder");

		/* IN_TO_013 */
		txt_transferCostOnTransferOrderGrid = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_transferCostOnTransferOrderGrid");

		/* IN_TO_014 */
		btn_dismissError = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_dismissError");
		lbl_validationFromWarehouseMissing = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_validationFromWarehouseMissing");
		lbl_validationDestinationWarehouseMissing = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_validationDestinationWarehouseMissing");
		lbl_validationProductMissing = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_validationProductMissing");
		lbl_validationProductQtyMissing = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_validationProductQtyMissing");

		/* IN_TO_015 */
		header_andHeaderStatusDarftedTransferOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_andHeaderStatusDarftedTransferOrder");

		/* IN_IO_016 */
		header_newTransferOrderVerify = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_newTransferOrderVerify");
		dropdown_templateFilter = findElementInXLSheet(getParameterInvertoryAndWarehouse, "dropdown_templateFilter");
		btn_searchTransferOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_searchTransferOrder");
		lbl_newlyDraftedTransferOrderDocNo = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_newlyDraftedTransferOrderDocNo");
		txt_searchTransferOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_searchTransferOrder");
		lnk_documentOnByPageTransferOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lnk_documentOnByPageTransferOrder");

		/* IN_IO_017 */
		btn_clearConfimationEditModeTransferOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_clearConfimationEditModeTransferOrder");
		lbl_fromWareDraftedTransferOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_fromWareDraftedTransferOrder");
		lbl_destinationWareDraftedTransferOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_destinationWareDraftedTransferOrder");
		lbl_productDraftedTransferOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_productDraftedTransferOrder");
		lbl_qtyWareDraftedTransferOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_qtyWareDraftedTransferOrder");

		/* IN_TO_020 */
		header_newOutboundShiment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_newOutboundShiment");
		header_newInboundShiment = findElementInXLSheet(getParameterInvertoryAndWarehouse, "header_newInboundShiment");
		lbl_statusConfirmation = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lbl_statusConfirmation");

		/* IN_TO_021 */
		btn_outboundShipmentTile = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_outboundShipmentTile");

		/* IN_TO_022 */
		btn_inboundShipmentTile = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_inboundShipmentTile");

		/* IN_TO_025 */
		header_releaseTransferOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_releaseTransferOrder");
		div_actionButtopnArea = findElementInXLSheet(getParameterInvertoryAndWarehouse, "div_actionButtopnArea");

		/* IN_TO_028 */
		header_releaseOutboundShipment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_releaseOutboundShipment");

		/* IN_TO_031 */
		header_releasedStatusInternalDispatchOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_releasedStatusInternalDispatchOrder");

		/* IN_TO_032 */
		btn_availabilityWidgetRowReplace = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_availabilityWidgetRowReplace");

		/* IN_TO_034 */
		btn_journeyConfigTransferOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_journeyConfigTransferOrder");

		/* IN_TO_036_038 */
		btn_advanceWidget = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_advanceWidget");
		btn_inventoryReports = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_inventoryReports");
		lbl_unitCostBincardReport = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_unitCostBincardReport");
		dropdown_templateBinCardReports = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"dropdown_templateBinCardReports");
		btn_viewBinCardReport = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_viewBinCardReport");
		lbl_filteredProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lbl_filteredProduct");
		txt_lotLotProducts = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_lotLotProducts");
		lbl_lot02AdvanceMenu = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lbl_lot02AdvanceMenu");
		input_transactionNoBincardReports = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"input_transactionNoBincardReports");
		btn_quickPreviewBincardReports = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_quickPreviewBincardReports");
		lbl_genarelInventoryCreditedValue = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_genarelInventoryCreditedValue");
		lbl_genarelInventoryDebitedValue = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_genarelInventoryDebitedValue");

		/* IN_TO_040 */
		chk_selectCaptureSeriels = findElementInXLSheet(getParameterInvertoryAndWarehouse, "chk_selectCaptureSeriels");
		txt_plabnedQuantityInboundShipment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_plabnedQuantityInboundShipment");
		btn_closeReleasedReferenceDocument = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_closeReleasedReferenceDocument");

		/* IN_TO_041 */
		txt_plabnedQuantityOutboundShipment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_plabnedQuantityOutboundShipment");
		btn_closeGoToNextPageWindow = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_closeGoToNextPageWindow");

		/* IN_TO_042 */
		btn_deleteRecordAccordingOutbondShipment_IN_IO_042 = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_deleteRecordAccordingOutbondShipment_IN_IO_042");

		/* IN_TO_046 */
		td_productAvailabilytyAccordingToTheBatchAndWare = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"td_productAvailabilytyAccordingToTheBatchAndWare");
		btn_batchWiseCostSecondMenu = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_batchWiseCostSecondMenu");
		lbl_secondBAtchNumberInSpecificCaptureList = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_secondBAtchNumberInSpecificCaptureList");

		/* IN_TO_049 */
		drop_reportCategory = findElementInXLSheet(getParameterInvertoryAndWarehouse, "drop_reportCategory");
		txt_productSeearchBinReports = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_productSeearchBinReports");
		lbl_remainingLotWarehouseBincardReports = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_remainingLotWarehouseBincardReports");
		lbl_corrrectRemainingQuntityAccordingToLot = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_corrrectRemainingQuntityAccordingToLot");
		lbl_corrrectRemainingQuntityAccordingToFirstLot = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_corrrectRemainingQuntityAccordingToFirstLot");
		lbl_issuedQtyAccordingToLotTen = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_issuedQtyAccordingToLotTen");
		lbl_issuedQtyAccordingToLotFive = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_issuedQtyAccordingToLotFive");

		/* IN_TO_050 */
		div_serleBatchLotExpiryCaptured = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_serleBatchLotExpiryCaptured");
		btn_calenderBinCardReports = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_calenderBinCardReports");
		btn_todayCalenderBincardReport = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_todayCalenderBincardReport");
		txt_transactionNoWhenDoableConditions = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_transactionNoWhenDoableConditions");

		/* IN_TO_052 */
		txt_postDateInside = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_postDateInside");
		btn_dateFifteen = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_dateFifteen");
		lbl_validateBeforePostDateOutboundShipmentTransferOrder = findElementInXLSheet(
				getParameterInvertoryAndWarehouse, "lbl_validateBeforePostDateOutboundShipmentTransferOrder");
		record_batchLotExpiry = findElementInXLSheet(getParameterInvertoryAndWarehouse, "record_batchLotExpiry");

		/* IN_TO_055 */
		lbl_gridErrorQuantityNotAvailableSelectedPostDate = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_gridErrorQuantityNotAvailableSelectedPostDate");

		/* IN_TO_059_060 */
		txt_reservedQtyAdvanceMenuTO = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_reservedQtyAdvanceMenuTO");

		/* IN_TO_061 */
		headre_transferOrderClosedStatus = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"headre_transferOrderClosedStatus");

		/* IN_TO_062 */
		header_docflowDocumentTransferOrderJoueney = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_docflowDocumentTransferOrderJoueney");

		/* IN_TO_067 */
		header_releasedTransferOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_releasedTransferOrder");

		/* IN_TO_068 */
		header_releasedPurchaseInvoice = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_releasedPurchaseInvoice");

		/* IN_TO_070 */
		btn_outboundLoanOrderToOutboundShipmentJourney = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_outboundLoanOrderToOutboundShipmentJourney");
		lbl_debitTotalJournelEntry = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_debitTotalJournelEntry");
		lbl_creditTotalJournelEntry = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_creditTotalJournelEntry");

		/* IN_TO_073 */
		header_releasedSalesReturnOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_releasedSalesReturnOrder");

		/* IN_TO_074 */
		div_capturedQuntityTransferOrder_IN_TO_074 = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_capturedQuntityTransferOrder_IN_TO_074");

		/* IN_TO_075 */
		div_pruductOnlyOnGrid = findElementInXLSheet(getParameterInvertoryAndWarehouse, "div_pruductOnlyOnGrid");

		/* IN_TO_076 */
		div_descriptionOnlyOnGrid = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_descriptionOnlyOnGrid");

		/* IN_TO_077 */
		div_codeAndDescOnGrid = findElementInXLSheet(getParameterInvertoryAndWarehouse, "div_codeAndDescOnGrid");

		/* IN_TO_078 */
		drop_warehouseConfigrationBalancingLavel = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"drop_warehouseConfigrationBalancingLavel");
		lbl_fromWarehouseTransferOrderfterReleased = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_fromWarehouseTransferOrderfterReleased");
		lbl_toWarehouseOutboundShipmentReleased = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_toWarehouseOutboundShipmentReleased");
		lbl_toWarehouseTransferOrderfterReleased = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_toWarehouseTransferOrderfterReleased");
		lbl_toWarehouseInboundShipmentReleased = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_toWarehouseInboundShipmentReleased");
		btn_updateGenaralTabBalancingLavelSetting = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_updateGenaralTabBalancingLavelSetting");

		/* IN_TO_081 */
		header_journeyConfiguration = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_journeyConfiguration");
		btn_activeJourney = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_activeJourney");
		btn_inactiveJourney = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_inactiveJourney");
		btn_yesConfirmationJourney = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_yesConfirmationJourney");
		validator_noPermissionForTheJourney = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"validator_noPermissionForTheJourney");
		header_newStatusTransferOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_newStatusTransferOrder");

		/* IN_TO_083 */
		chk_autoGenarateOutboundShipment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_autoGenarateOutboundShipment");
		btn_gridErrosFirstRow = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_gridErrosFirstRow");
		lbl_gridErroeBallonSerielBatchCapture = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_gridErroeBallonSerielBatchCapture");

		lbl_autoGenaratedOutboundShipment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_autoGenaratedOutboundShipment");

		/* IN_TO_084 */
		chk_autoGenarateInboundShipment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_autoGenarateInboundShipment");
		lbl_autoGenaratedInboundShipment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_autoGenaratedInboundShipment");

		/* IN_TO_086 */
		btn_rowReplaceAndClickGridErrorOutboundShipment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_rowReplaceAndClickGridErrorOutboundShipment");
		div_validation_IN_TO_086 = findElementInXLSheet(getParameterInvertoryAndWarehouse, "div_validation_IN_TO_086");

		/* IN_TO_088 */
		div_errorQuantityNotAvailableInGridErros = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_errorQuantityNotAvailableInGridErros");
		btn_firstClickGridErrorOutboundShipment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_firstClickGridErrorOutboundShipment");

		/* IN_TO_090 */
		btn_updateJourneyConfigurationTransferOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_updateJourneyConfigurationTransferOrder");
		header_releasedStatusTransferOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_releasedStatusTransferOrder");
		lbl_zeroReservedQuantity = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lbl_zeroReservedQuantity");

		/* IN_TO_092 */
		lbl_reservertionInAllocationManagerAccordingToTheProduct = findElementInXLSheet(
				getParameterInvertoryAndWarehouse, "lbl_reservertionInAllocationManagerAccordingToTheProduct");

		/* IN_TO_094 */
		header_selectedProductInOutboundShipmentGrid = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_selectedProductInOutboundShipmentGrid");

		/* IN_TO_095 */
		chk_allocationProductAllocationManager = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_allocationProductAllocationManager");
		btn_productDivAllocationManager = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_productDivAllocationManager");
		txt_reservedQuantityAllocationManagerWindow = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_reservedQuantityAllocationManagerWindow");
		btn_checkoutAllocationMangerWindow = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_checkoutAllocationMangerWindow");
		btn_updateAllocationManagerWindow = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_updateAllocationManagerWindow");
		lbl_releasedStatusOutboundShipment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_releasedStatusOutboundShipment");

		/* IN_TO_096 */
		txt_reservedQtyOnGridTransferOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_reservedQtyOnGridTransferOrder");

		/* IN_TO_097 */
		para_validationReserveQuntityAdvanceMenuTransferOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"para_validationReserveQuntityAdvanceMenuTransferOrder");
		btn_closeAdvanceMenuLast = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_closeAdvanceMenuLast");

		/* IN_TO_099 */
		txt_reserveQuantityAdvanceMenuTransferOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_reserveQuantityAdvanceMenuTransferOrder");
		btn_applyLast = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_applyLast");

		/* IN_TO_101 */
		txt_failQuantityWhnMultiplePrductQC = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_failQuantityWhnMultiplePrductQC");
		div_qcStatusFailCount_IN_TO_101 = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_qcStatusFailCount_IN_TO_101");

		/* IN_TO_102 */
		btn_deleteQCAcceptanceJourneys = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_deleteQCAcceptanceJourneys");
		span_countOfJourneyInQCAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"span_countOfJourneyInQCAcceptance");
		btn_thisWeekBincardReports = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_thisWeekBincardReports");
		div_qcStatusPendingCount_IN_TO_102 = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_qcStatusPendingCount_IN_TO_102");
		div_qcStatusPassCount_IN_TO_102 = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_qcStatusPassCount_IN_TO_102");
		div_quantityBincardQCRowReplace_IN_TO_102 = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_quantityBincardQCRowReplace_IN_TO_102");
		btn_serielNosQCAcceptanceRowReplace = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_serielNosQCAcceptanceRowReplace");
		txt_remainingQuantityQC = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_remainingQuantityQC");
		txt_passQuantityQcAceptanceWhenMultipleProducts = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_passQuantityQcAceptanceWhenMultipleProducts");
		drop_qcStatusQCAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"drop_qcStatusQCAcceptance");
		btn_applyQCStatusSerielBatch = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_applyQCStatusSerielBatch");
		txt_remainingQCRowsVerify = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_remainingQCRowsVerify");
		div_headerQCStatusBinCard = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_headerQCStatusBinCard");
		div_headerQCBalanceQuantityBinCard = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_headerQCBalanceQuantityBinCard");

		/* IN_TO_103 */
		drop_passFailSerleIncludedProducts = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"drop_passFailSerleIncludedProducts");
		div_headerRecievedQuantity = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_headerRecievedQuantity");
		div_checkMultipleStatusOfQcDocument = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_checkMultipleStatusOfQcDocument");

		/* IN_TO_105 */
		div_qcPendingDocument = findElementInXLSheet(getParameterInvertoryAndWarehouse, "div_qcPendingDocument");

		/* IN_TO_106 */
		span_historyQCAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse, "span_historyQCAcceptance");
		span_closeButtonLast = findElementInXLSheet(getParameterInvertoryAndWarehouse, "span_closeButtonLast");
		span_historyQCAcceptanceAfterSeletc = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"span_historyQCAcceptanceAfterSeletc");
		txt_failQuantityWhnMultiplePrductQCHistoryTab = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_failQuantityWhnMultiplePrductQCHistoryTab");
		txt_passQuantityQcAceptanceWhenMultipleProductsHistoryTab = findElementInXLSheet(
				getParameterInvertoryAndWarehouse, "txt_passQuantityQcAceptanceWhenMultipleProductsHistoryTab");

		/* IN_TO_113 */
		div_selectedInboundShipmentShipmentAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_selectedInboundShipmentShipmentAcceptance");
		tr_countShipmentAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"tr_countShipmentAcceptance");
		div_productShipmentAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_productShipmentAcceptance");

		/* IN_TO_114 */
		span_countOfJourneyInShipmentAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"span_countOfJourneyInShipmentAcceptance");
		header_releaseForPickingInboundShipment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_releaseForPickingInboundShipment");

		/* IN_TO_115 */
		lbl_quantityCaptureAvailableBatchSpecific = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_quantityCaptureAvailableBatchSpecific");
		div_validationMessageBackBd = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_validationMessageBackBd");

		/* IN_TO_116 */
		para_inboundShipmentSuccessfullyReleased = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"para_inboundShipmentSuccessfullyReleased");
		chk_releseAllShipmentAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_releseAllShipmentAcceptance");
		btn_releseAllShipmentAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_releseAllShipmentAcceptance");
		para_validationSuccessfullySaved = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"para_validationSuccessfullySaved");
		btn_dismissSucessValidation = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_dismissSucessValidation");

		/* IN_TO_118 */
		chk_releseShipmentAcceptanceRowReplace = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_releseShipmentAcceptanceRowReplace");

		/* IN_TO_122 */
		dropdown_selectOutboundShipmentAcceptanceJourney = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"dropdown_selectOutboundShipmentAcceptanceJourney");
		span_countOfJourneyShipmentAcceptanceOutboundShipment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"span_countOfJourneyShipmentAcceptanceOutboundShipment");
		btn_deleetOutboundShipmentAceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_deleetOutboundShipmentAceptance");
		header_releaseForPickingOutboundShipment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_releaseForPickingOutboundShipment");
		tab_outbounsShipmentAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"tab_outbounsShipmentAcceptance");
		txt_docNoOutboundShipmentShipmentAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_docNoOutboundShipmentShipmentAcceptance");
		div_selectedOutboundShipmentShipmentAcceptance = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_selectedOutboundShipmentShipmentAcceptance");
		para_outboundShipmentSuccessfullyReleased = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"para_outboundShipmentSuccessfullyReleased");

		/* Stock Take */
		/* IN_ST_001 */
		header_stockTakeByPage = findElementInXLSheet(getParameterInvertoryAndWarehouse, "header_stockTakeByPage");

		/* IN_ST_002 */
		heade_newStockTakeForm = findElementInXLSheet(getParameterInvertoryAndWarehouse, "heade_newStockTakeForm");

		/* IN_ST_003 */
		drop_warehouseStockTakeWithErrorBorder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"drop_warehouseStockTakeWithErrorBorder");
		lbl_selectWarehouseValidationStockTake = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_selectWarehouseValidationStockTake");
		span_refreshErrorBorder = findElementInXLSheet(getParameterInvertoryAndWarehouse, "span_refreshErrorBorder");
		lbl_productRefreshErrorMessage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_productRefreshErrorMessage");

		/* IN_ST_004 */
		div_productVerfiStockTakePage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_productVerfiStockTakePage");
		row_countStockTakePage = findElementInXLSheet(getParameterInvertoryAndWarehouse, "row_countStockTakePage");

		/* IN_ST_008 */
		btn_addProductStockTake = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_addProductStockTake");
		header_addProductLookupStockTake = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_addProductLookupStockTake");
		drop_rackSelectStockTake = findElementInXLSheet(getParameterInvertoryAndWarehouse, "drop_rackSelectStockTake");
		td_productOnGridStockTake = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"td_productOnGridStockTake");
		td_rackOnGridStockTake = findElementInXLSheet(getParameterInvertoryAndWarehouse, "td_rackOnGridStockTake");
		td_defoultUOMOnGridStockTake = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"td_defoultUOMOnGridStockTake");
		td_systemBalanceOnGridStockTake = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"td_systemBalanceOnGridStockTake");
		lbl_selectedProductOnPopupStockTake = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_selectedProductOnPopupStockTake");
		headers_gridStockTake = findElementInXLSheet(getParameterInvertoryAndWarehouse, "headers_gridStockTake");

		/* IN_ST_011 */
		td_physicalBalanceOnGridStockTake = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"td_physicalBalanceOnGridStockTake");
		header_physicalBalanceStockTake = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_physicalBalanceStockTake");
		txt_physicalBalanceOnGridStockTake = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_physicalBalanceOnGridStockTake");

		/* IN_ST_013 */
		headers_stockTakDraftedStatusVerification = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"headers_stockTakDraftedStatusVerification");
		td_varienceQtyOnGridStockTake = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"td_varienceQtyOnGridStockTake");

		/* IN_ST_014 */
		td_adjustedQtyOnGridStockTake = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"td_adjustedQtyOnGridStockTake");

		/* IN_ST_015 */
		txt_physycalBalanceEditModeAfterSearch = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_physycalBalanceEditModeAfterSearch");
		txt_serachProductEditModeStockTake = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_serachProductEditModeStockTake");

		/* IN_ST_019 */
		header_newStockTake = findElementInXLSheet(getParameterInvertoryAndWarehouse, "header_newStockTake");
		btn_searchForms = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_searchForms");
		lbl_recentlyDraftedStockTakeDocNo = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_recentlyDraftedStockTakeDocNo");
		lbl_documentDate = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lbl_documentDate");
		txt_searchStockTake = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_searchStockTake");
		lnk_releventStockTakeOnByPage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lnk_releventStockTakeOnByPage");
		header_draftedStockTakeReplaceDocNo = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_draftedStockTakeReplaceDocNo");

		/* IN_ST_032 */
		btn_deleteEmotyRowStockAdjustmentRowTwo = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_deleteEmotyRowStockAdjustmentRowTwo");
		div_validationStokTakeAlreadyDrafted = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_validationStokTakeAlreadyDrafted");

		/* IN_ST_034 */
		btn_convertToStockAdjustmentStockTake = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_convertToStockAdjustmentStockTake");
		header_newStockAdjustment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_newStockAdjustment");
		div_productOnGridStockAdjustmentReplaceProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_productOnGridStockAdjustmentReplaceProduct");
		txt_quntityOnGridStockAdjustmentReplaceProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_quntityOnGridStockAdjustmentReplaceProduct");
		header_draftStatusStockAdjustment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_draftStatusStockAdjustment");
		headers_releasedStatusStockAdjustment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"headers_releasedStatusStockAdjustment");
		headers_stockAdjustmentGenarateFromStockTake = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"headers_stockAdjustmentGenarateFromStockTake");
		txt_refDocStockAdjustmentGenarateFromStockTake = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_refDocStockAdjustmentGenarateFromStockTake");

		/* IN_ST_039 */
		hedere_relesedStockTakeDocReplace = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"hedere_relesedStockTakeDocReplace");

		/* IN_ST_040_041 */
		btn_deleteRecordOnGrid = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_deleteRecordOnGrid");
		row_countInGridStockAdjustment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"row_countInGridStockAdjustment");
		txt_quantityAccordingToTheProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_quantityAccordingToTheProduct");

		/* IN_ST_046 */
		chk_serielSelectionBatchProducts = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_serielSelectionBatchProducts");
		btn_dockIconBatchProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_dockIconBatchProduct");

		/* IN_ST_047 */
		txt_searchStockAdjustmentOnByPage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_searchStockAdjustmentOnByPage");
		lnk_stockAdjustmentOnByPageDocReplace = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lnk_stockAdjustmentOnByPageDocReplace");
		para_validatorStockAdjustmentAlreadyUsed = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"para_validatorStockAdjustmentAlreadyUsed");

		/* IN_ST_048 */
		lbl_docNoRecentlyDraftedStockAdjustment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_docNoRecentlyDraftedStockAdjustment");

		/* IN_ST_049 */
		div_descriptionOfDraftedStockAdjustment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_descriptionOfDraftedStockAdjustment");

		/* IN_ST_051 */
		btn_deleteLastRowOfTheGrid = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_deleteLastRowOfTheGrid");

		/* IN_ST_057 */
		header_draftedInterdepartmentalTransferOrder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_draftedInterdepartmentalTransferOrder");
		header_draftedOutboundShipment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_draftedOutboundShipment");
		
		/* IN_ST_058 */
		drop_rackSelectStockAdjRowReplace = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"drop_rackSelectStockAdjRowReplace");
		
		/* IN_ST_059 */
		div_productOnRelesedStockAdjustment = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_productOnRelesedStockAdjustment");
		div_productOnRelesedStockTake = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_productOnRelesedStockTake");
		
		/* IN_ST_062 */
		lbl_warehouseOnReleasedDoc = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_warehouseOnReleasedDoc");
		
		/* Product Conversion */
		/* IN_PC_001 */
		header_productConversionByPage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_productConversionByPage");

		/* IN_PC_002 */
		header_newProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_newProductConversion");

		/* IN_PC_004 */
		label_requiredFildAsteriskProductConversionWarehouse = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"label_requiredFildAsteriskProductConversionWarehouse");
		drop_warehouseWithErrorBorder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"drop_warehouseWithErrorBorder");
		lbl_errorMessageWarehouseSelectionProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_errorMessageWarehouseSelectionProductConversion");
		header_productLookupProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_productLookupProductConversion");

		/* IN_ST_005 */
		div_productOnGridProductConversation = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_productOnGridProductConversation");

		/* IN_PC_006 */
		header_verifyProductConversionForm = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_verifyProductConversionForm");
		div_uomOnGridProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_uomOnGridProductConversion");

		/* IN_PC_007 */
		btn_productMasterWidget = findElementInXLSheet(getParameterInvertoryAndWarehouse, "btn_productMasterWidget");
		td_ohQuantityProductConversionFirstRowWhenSelectSingleProduct = findElementInXLSheet(
				getParameterInvertoryAndWarehouse, "td_ohQuantityProductConversionFirstRowWhenSelectSingleProduct");

		/* IN_PC_008 */
		btn_productRelatedPriceOnWidgetMaster = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_productRelatedPriceOnWidgetMaster");
		lbl_costPrice = findElementInXLSheet(getParameterInvertoryAndWarehouse, "lbl_costPrice");
		headers_productConversionGrid = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"headers_productConversionGrid");
		td_lastCostPriceProductConversionGrid = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"td_lastCostPriceProductConversionGrid");
		
		/* IN_PC_009 */
		btn_serielBatchCaptureNosProductConversionRowReplace = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_serielBatchCaptureNosProductConversionRowReplace");
		chk_selectBatchForBatchSpecificProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse, "chk_selectBatchForBatchSpecificProduct");
		txt_capturedQuantityProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_capturedQuantityProductConversion");
		btn_appalySerielBatchesFromProductProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_appalySerielBatchesFromProductProductConversion");
		header_batchCaptureWindowProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_batchCaptureWindowProductConversion");
		header_serileCaptureWindowProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_serileCaptureWindowProductConversion");
		btn_productLookupRowReplaceProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_productLookupRowReplaceProductConversion");
		txt_quantityRowReplaceProductConversionFromProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_quantityRowReplaceProductConversionFromProduct");
		btn_addNewRowProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_addNewRowProductConversion");
		chk_serielRangeProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_serielRangeProductConversion");
		txt_serielCountProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_serielCountProductConversion");
		
		/* IN_PC_010 */
		btn_productLookupRowReplaceProductConversionToProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_productLookupRowReplaceProductConversionToProduct");
		
		/* IN_PC_011 */
		btn_statusMenu = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_statusMenu");
		div_toProductOnGridRowReplace = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_toProductOnGridRowReplace");
		
		/* IN_PC_012 */
		div_defoultUomToProductProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_defoultUomToProductProductConversion");
		
		/* IN_PC_013 */
		txt_costPriceProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_costPriceProductConversion");
		
		/* IN_PC_014 */
		txt_conRateProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_conRateProductConversion");
		txt_quantityToProductProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_quantityToProductProductConversion");
		
		/* IN_PC_019 */
		txt_menufactureDate = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_menufactureDate");
		
		/* IN_PC_020 */
		div_transparent = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_transparent");
		error_invalidSerielNo = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"error_invalidSerielNo");
		
		/* IN_PC_021 */
		error_commonSerielNos = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"error_commonSerielNos");
		icon_rowErrorsSerielCaptureWindow = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"icon_rowErrorsSerielCaptureWindow");
		div_rowErrorBaloonSerielCaptureWindow = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_rowErrorBaloonSerielCaptureWindow");
		
		/* IN_PC_022 */
		error_fromProductRequired = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"error_fromProductRequired");
		error_toProductRequired = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"error_toProductRequired");
		error_warehouseValidation = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"error_warehouseValidation");
		
		/* IN_PC_023_025_029_030 */
		header_drftedProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_drftedProductConversion");
		header_releasedProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_releasedProductConversion");
		headers_serialBatchNosUptoExpiryDateProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"headers_serialBatchNosUptoExpiryDateProductConversion");
		txt_disabledExpiryDateReleasedProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_disabledExpiryDateReleasedProductConversion");
		
		/* IN_PC_024 */
		div_gridErrorMessageDuplicateProductProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_gridErrorMessageDuplicateProductProductConversion");
		div_visibleGridBaloon = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_visibleGridBaloon");
		div_toProductWithErrorBorderProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_toProductWithErrorBorderProductConversion");
		
		/* IN_PC_026 */
		txt_fromQuantityWithErrorBorder = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_fromQuantityWithErrorBorder");
		error_cantExceedOH = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"error_cantExceedOH");
		
		/* IN_PC_028 */
		txt_costPriceProductConversionRowReplace = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_costPriceProductConversionRowReplace");
		div_productAccordingToRowProductConversionFromProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_productAccordingToRowProductConversionFromProduct");
		div_productAccordingToRowProductConversionToProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_productAccordingToRowProductConversionToProduct");
		tr_countProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"tr_countProductConversion");
		lbl_warehouseProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_warehouseProductConversion");
		
		/* IN_PC_034 */
		count_rowProductConversionGrid = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"count_rowProductConversionGrid");
		txt_capturedQuantityAfterDraftProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_capturedQuantityAfterDraftProductConversion");
		
		/* IN_PC_037 */
		headers_existingSerialBatchNos = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"headers_existingSerialBatchNos");
		div_batchInExistingSerialBatchesBatchReplace = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_batchInExistingSerialBatchesBatchReplace");
		btn_closeBatchNos = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_closeBatchNos");
		
		/* IN_PC_039 */
		txt_batchNoProductConversionToProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_batchNoProductConversionToProduct");
		td_capturedLotOnSerielNos = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"td_capturedLotOnSerielNos");
		headers_upTpToLotCapturedSerielBatchToProductProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"headers_upTpToLotCapturedSerielBatchToProductProductConversion");
		
		/* IN_PC_040 */
		txt_qtyToProductProductConcersionRowReplace = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_qtyToProductProductConcersionRowReplace");
		txt_batchNoToProductProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_batchNoToProductProductConversion");
		txt_expiryDateToProductProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_expiryDateToProductProductConversion");
		txt_lotNoToProductProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_lotNoToProductProductConversion");
		txt_menufacDateToProductProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_menufacDateToProductProductConversion");
		chk_batchSelectionProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"chk_batchSelectionProductConversion");
		header_reversedProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_reversedProductConversion");
		
		/* IN_PC_041_051 */
		txt_lotNumber = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_lotNumber");
		
		/* IN_PC_042 */
		lbl_recentlyDraftedProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lbl_recentlyDraftedProductConversion");
		lnk_releventDocumentOnByPage = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lnk_releventDocumentOnByPage");
		header_draftedProductConversionReplaceDocNo = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"header_draftedProductConversionReplaceDocNo");
		txt_searchProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"txt_searchProductConversion");
		
		/* IN_PC_043 */
		lnk_resultDocumentOnLookup = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"lnk_resultDocumentOnLookup");
		
		/* IN_PC_054 */
		div_firstSerialBarcodeGeanarateView = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_firstSerialBarcodeGeanarateView");
		
		/* IN_PC_057 */
		div_productAccordingToRowRelesedProductConversionFromProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_productAccordingToRowRelesedProductConversionFromProduct");
		div_productAccordingToRowRelesedProductConversionToProduct = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"div_productAccordingToRowRelesedProductConversionToProduct");
		
		/* IN_PC_063 */
		btn_journeyConfigProductConversion = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"btn_journeyConfigProductConversion");
		
		// ============================================RegressionAruna======================================================================
		// IN_SA_001
		txt_pageHeaderLableA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "pageHeaderLableA");

		// IN_SA_002
		btn_AdministrationA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "AdministrationA");
		btn_JourneyConfigurationA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "JourneyConfigurationA");
		btn_ProcessStatusA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ProcessStatusA");
		btn_YesA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "YesA");
		btn_JourneyConfigurationUpdateA = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"JourneyConfigurationUpdateA");

		// IN_SA_003
		txt_PreparerA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "PreparerA");
		txt_DescriptionA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "DescriptionA");
		txt_WarehouseA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "WarehouseA");
		txt_AdjustmentGroupA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "AdjustmentGroupA");
		txt_ProductA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ProductA");

		// IN_SA_005
		btn_ProductAddA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ProductAddA");
		txt_ProductAddTxtA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ProductAddTxtA");
		sel_ProductAddSelA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ProductAddSelA");

		// IN_SA_007
		btn_BalancingLevelSettingsA = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"BalancingLevelSettingsA");
		btn_GeneralA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "GeneralA");
		sel_selectWarehouseSerchTypeA = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"selectWarehouseSerchTypeA");

		// IN_SA_011
		btn_InactiveAdjustmentGroupA = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"InactiveAdjustmentGroupA");
		btn_AddAdjustmentGroupA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "AddAdjustmentGroupA");
		btn_AddAdjustmentGroupUpdateA = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"AddAdjustmentGroupUpdateA");

		// IN_SA_012
		btn_AdjustmentGroupAddA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "AdjustmentGroupAddA");
		txt_AdjustmentGroupTxtA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "AdjustmentGroupTxtA");

		// IN_SA_013
		txt_ProductLoadTxtA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ProductLoadTxtA");
		txt_ProductQtyA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ProductQtyA");

		// IN_SA_015
		sel_ConfigProA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ConfigProA");
		btn_ConfigProUpdateA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ConfigProUpdateA");

		// IN_SA_017
		btn_ProductAddSelDeleteA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ProductAddSelDeleteA");

		// IN_SA_018
		txt_ProductAddQtyA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ProductAddQtyA");
		txt_ProductAddAfterQtyA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ProductAddAfterQtyA");

		// IN_SA_019
		txt_ProductSerchA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ProductSerchA");
		sel_ProductSerchSelA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ProductSerchSelA");
		btn_AllowDecimalA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "AllowDecimalA");

		// IN_SA_022
		btn_Product2A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "Product2A");
		btn_ProductAdd2A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ProductAdd2A");

		// IN_SA_023
		sel_pro1A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "pro1A");
		sel_pro2A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "pro2A");
		sel_pro3A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "pro3A");
		txt_tval1A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "tval1A");
		txt_tval2A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "tval2A");
		txt_tval3A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "tval3A");

		// IN_SA_024
		btn_ExDescriptionA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ExDescriptionA");
		txt_ExDescriptiontxtA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ExDescriptiontxtA");

		// IN_SA_025
		txt_DefaultUOMA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "DefaultUOMA");
		txt_DefaultUOMtxtA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "DefaultUOMtxtA");

		// IN_SA_026
		txt_OnHandQtyA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "OnHandQtyA");

		// IN_SA_027
		txt_ReservedQtyA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ReservedQtyA");

		// IN_SA_028
		btn_RackA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "RackA");

		// IN_SA_029
		txt_pageDraftA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "pageDraftA");
		txt_pageReleaseA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "pageReleaseA");

		// IN_SA_030
		txt_selectWarehouseWarningA = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"selectWarehouseWarningA");

		// IN_SA_034
		txt_HeaderA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "HeaderA");
		btn_StockAdjustmentByPageA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "StockAdjustmentByPageA");
		txt_StockAdjustmentSerchA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "StockAdjustmentSerchA");
		sel_StockAdjustmentSerchselA = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"StockAdjustmentSerchselA");

		// IN_SA_036
		txt_CostPerUnitA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "CostPerUnitA");

		// IN_SA_038
		sel_StockAdjustmentSerchCopyselA = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"StockAdjustmentSerchCopyselA");

		// IN_SA_039
		txt_filterListIdA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "filterListIdA");

		// IN_SA_042
		btn_ProductAvailabilityA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ProductAvailabilityA");
		txt_ProductAvailabilityValueA = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"ProductAvailabilityValueA");
		btn_ReportsA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ReportsA");
		btn_ViewA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ViewA");
		txt_ProductAvailabilityAfterValueA = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"ProductAvailabilityAfterValueA");

		// IN_SA_043
		btn_reversebtnA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "reversebtnA");

		// IN_SA_046
		txt_ProductAddQty2A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ProductAddQty2A");
		btn_MasterInfo2A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "MasterInfo2A");
		btn_AfterMasterInfo2A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "AfterMasterInfo2A");

		// IN_SA_047
		btn_SerialBatchA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "SerialBatchA");
		btn_ItemNumber1A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ItemNumber1A");
		txt_SerialNoA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "SerialNoA");
		txt_LotNoA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "LotNoA");
		txt_ExpiryDateA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ExpiryDateA");
		txt_ExpiryDateNextYearA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ExpiryDateNextYearA");
		sel_ExpiryDateSelA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ExpiryDateSelA");
		btn_updateSerialA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "updateSerialA");
		btn_closeProductList = findElementInXLSheet(getParameterInvertoryAndWarehouse, "closeProductList");
		btn_ProcurementA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ProcurementA");
		btn_PurchaseOrderA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "PurchaseOrderA");
		btn_NewPurchaseOrderA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "NewPurchaseOrderA");
		btn_PurchaseOrdertoPurchaseInvoiceA = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"PurchaseOrdertoPurchaseInvoiceA");
		btn_VendorbtnA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "VendorbtnA");
		txt_VendortxtA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "VendortxtA");
		sel_VendorselA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "VendorselA");
		btn_SerialRangeA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "SerialRangeA");
		txt_SerialCountA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "SerialCountA");
		txt_ValidatorReverceA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ValidatorReverceA");

		// IN_SA_048
		txt_historyForDraftA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "historyForDraftA");
		txt_historyForReleasedA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "historyForReleasedA");

		// IN_SA_049
		txt_CostPerUnit2A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "CostPerUnit2A");

		// IN_SA_050
		txt_CostPerUnitAfterValueA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "CostPerUnitAfterValueA");
		btn_ProductRelatedPriceA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ProductRelatedPriceA");
		txt_ProductRelatedPriceValueA = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"ProductRelatedPriceValueA");

		// IN_SA_051
		btn_RackwiseAvailableQtyA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "RackwiseAvailableQtyA");
		txt_RackwiseAvailableQtyValueA = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"RackwiseAvailableQtyValueA");

		// IN_SA_052
		btn_ClassificationA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ClassificationA");
		btn_VariantGroupbtnA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "VariantGroupbtnA");
		txt_VariantGrouptxtA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "VariantGrouptxtA");
		sel_VariantGroupselA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "VariantGroupselA");
		txt_VariantGroupGradeA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "VariantGroupGradeA");
		btn_VariantUpdateA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "VariantUpdateA");

		// IN_SA_053
		txt_ValidatorReleasedA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ValidatorReleasedA");

		// IN_SA_054
		btn_ItemNumber2A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ItemNumber2A");

		// IN_SA_057
		txt_Racktxt1A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "Racktxt1A");
		txt_Racktxt2A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "Racktxt2A");

		// IN_SA_059
		btn_SalesAndMarketingA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "SalesAndMarketingA");
		btn_SalesOrderA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "SalesOrderA");
		btn_NewSalesOrderA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "NewSalesOrderA");
		btn_SalesOrderToSalesInvoiceA = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"SalesOrderToSalesInvoiceA");
		btn_CustomerAccountA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "CustomerAccountA");
		sel_CustomerAccountselA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "CustomerAccountselA");
		txt_WarehouseSalesA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "WarehouseSalesA");
		txt_pageSalesDraftA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "pageSalesDraftA");
		txt_pageSalesReleasedA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "pageSalesReleasedA");
		txt_BatchNoSalesA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "BatchNoSalesA");
		btn_SalesReturnOrderA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "SalesReturnOrderA");
		btn_NewSalesReturnOrderA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "NewSalesReturnOrderA");
		btn_SalesReturnOrderToSalesReturnInvoiceA = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"SalesReturnOrderToSalesReturnInvoiceA");
		btn_CustomerAccountSalesReturnA = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"CustomerAccountSalesReturnA");
		txt_SalesReturnSearchByA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "SalesReturnSearchByA");
		txt_SalesReturnSearchBytxtA = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"SalesReturnSearchBytxtA");
		btn_SalesReturnSearchA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "SalesReturnSearchA");
		sel_SalesReturnSearchSelA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "SalesReturnSearchSelA");
		btn_SalesReturnSearchApplyA = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"SalesReturnSearchApplyA");
		txt_changeDispositionA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "changeDispositionA");
		txt_ReturnReasonA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ReturnReasonA");
		txt_UnitPriceSalesReturnInvoiceA = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"UnitPriceSalesReturnInvoiceA");

		// IN_SA_060
		btn_NewInternalOrderA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "NewInternalOrderA");
		btn_EmployeeRequestsA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "EmployeeRequestsA");
		btn_RequesterBtnA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "RequesterBtnA");
		sel_RequesterselA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "RequesterselA");
		txt_WarehouseIOA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "WarehouseIOA");
		btn_NewInternelReturnOrderA = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"NewInternelReturnOrderA");
		btn_EmployeeReturnsA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "EmployeeReturnsA");
		txt_CommonSearchIROA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "CommonSearchIROA");
		sel_IROSearchSelA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "IROSearchSelA");
		btn_ViewLinkA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ViewLinkA");

		// IN_SA_063
		btn_NewInterdepartmentalTransferOrderA = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"NewInterdepartmentalTransferOrderA");

		// IN_SA_064
		sel_dateRangeBarcodeGeneratorPageSelA = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"dateRangeBarcodeGeneratorPageSelA");
		sel_inboundShipmentBarcodeGeneratorSelA = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"inboundShipmentBarcodeGeneratorSelA");
		btn_GenerateViewA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "GenerateViewA");
		txt_GeneraterBatchNoA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "GeneraterBatchNoA");
		txt_GeneraterLotNoA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "GeneraterLotNoA");

		// IN_SA_066
		txt_DocProductA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "DocProductA");
		txt_DocProductRackA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "DocProductRackA");
		sel_DocselA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "DocselA");
		btn_DocApplyA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "DocApplyA");
		txt_NewTransferRackA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "NewTransferRackA");
		btn_NewTransferRackApplyA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "NewTransferRackApplyA");

		// IN_SA_067
		btn_ProductStockTakebtnA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ProductStockTakebtnA");
		btn_AddProductStockTakebtnA = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"AddProductStockTakebtnA");
		txt_ProductStockTakeRackA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ProductStockTakeRackA");
		txt_SysBalA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "SysBalA");
		txt_PhyBalA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "PhyBalA");
		txt_StockAdjQuan = findElementInXLSheet(getParameterInvertoryAndWarehouse, "StockAdjQuan");

		// IN_SA_068
		txt_BVal1A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "BVal1A");
		txt_BVal2A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "BVal2A");

		// IN_SA_069
		btn_PostDateSAJA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "PostDateSAJA");
		btn_PostDateApplySAJA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "PostDateApplySAJA");

		// IN_SA_070
		txt_ValidatorCapA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ValidatorCapA");

		// IN_SA_073
		sel_ExpiryDateSel2A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ExpiryDateSel2A");
		txt_ValidatorExpireDateA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ValidatorExpireDateA");

		// IN_SA_076
		txt_ManufactureDateA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ManufactureDateA");
		txt_ValidatorManufactureDateA = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"ValidatorManufactureDateA");

		// IN_IRO_Emp_047
		btn_AutoGenerateInternalReceiptsCheckboxA = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"AutoGenerateInternalReceiptsCheckboxA");
		btn_JourneyConfUpdateA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "JourneyConfUpdateA");

		// IN_IRO_Emp_048
		txt_IROprotextA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "IROprotextA");
		txt_IRprotextA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "IRprotextA");

		// IN_IRO_Emp_051
		txt_WarehouseIRA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "WarehouseIRA");

		// IN_IRO_Emp_054
		sel_selectEmployeeSerchTypeA = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"selectEmployeeSerchTypeA");
		txt_EmployeeIRO = findElementInXLSheet(getParameterInvertoryAndWarehouse, "EmployeeIRO");
		txt_EmployeeIR = findElementInXLSheet(getParameterInvertoryAndWarehouse, "EmployeeIR");

		// IN_IRO_Emp_029
		sel_IROSearchSel2A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "IROSearchSel2A");
		txt_IROqty1A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "IROqty1A");
		txt_IROqty2A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "IROqty2A");
		txt_serchIROA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "serchIROA");
		sel_serchSelIROA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "serchSelIROA");

		// IN_IRO_Emp_033
		btn_InternalReceiptsA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "InternalReceiptsA");
		txt_InternalReceiptsSerchA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "InternalReceiptsSerchA");
		sel_InternalReceiptsSerchSelA = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"InternalReceiptsSerchSelA");

		// IN_IRO_Emp_036
		txt_WarehouseIO1A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "WarehouseIO1A");
		txt_WarehouseIO2A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "WarehouseIO2A");
		txt_WarehouseIO3A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "WarehouseIO3A");
		txt_WarehouseIO4A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "WarehouseIO4A");
		txt_WarehouseIO5A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "WarehouseIO5A");
		txt_WarehouseIO6A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "WarehouseIO6A");
		txt_WarehouseIO7A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "WarehouseIO7A");

		txt_BatchNuA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "BatchNuA");
		txt_BatchLotNuA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "BatchLotNuA");
		txt_BatchExpiryDateA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "BatchExpiryDateA");
		txt_SerialExpiryDateA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "SerialExpiryDateA");
		sel_BatchExpiryDateSelA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "BatchExpiryDateSelA");
		txt_SerialFromNuA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "SerialFromNuA");
		txt_SerialLotNuA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "SerialLotNuA");
		txt_SerialBatchNuA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "SerialBatchNuA");

		btn_ItemNumber3A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ItemNumber3A");
		btn_ItemNumber4A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ItemNumber4A");
		btn_ItemNumber5A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ItemNumber5A");
		btn_ItemNumber6A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ItemNumber6A");
		btn_ItemNumber7A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "ItemNumber7A");

		sel_IROSearchSel3A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "IROSearchSel3A");
		sel_IROSearchSel4A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "IROSearchSel4A");
		sel_IROSearchSel5A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "IROSearchSel5A");
		sel_IROSearchSel6A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "IROSearchSel6A");
		sel_IROSearchSel7A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "IROSearchSel7A");

		txt_LotNamePro1A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "LotNamePro1A");
		txt_BatchNamePro1A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "BatchNamePro1A");
		txt_LotNamePro2A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "LotNamePro2A");
		txt_BatchNamePro2A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "BatchNamePro2A");
		txt_LotNamePro3A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "LotNamePro3A");
		txt_LotNamePro4A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "LotNamePro4A");
		txt_LotNamePro5A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "LotNamePro5A");
		txt_BatchNamePro5A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "BatchNamePro5A");
		txt_LotNamePro6A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "LotNamePro6A");
		txt_BatchNamePro6A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "BatchNamePro6A");
		txt_LotNamePro7A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "LotNamePro7A");

		btn_AdvanceWidget1A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "AdvanceWidget1A");
		btn_AdvanceWidget2A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "AdvanceWidget2A");
		btn_AdvanceWidget3A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "AdvanceWidget3A");
		btn_AdvanceWidget4A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "AdvanceWidget4A");
		btn_AdvanceWidget5A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "AdvanceWidget5A");
		btn_AdvanceWidget6A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "AdvanceWidget6A");
		btn_AdvanceWidget7A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "AdvanceWidget7A");
		btn_SerialBatchTabA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "SerialBatchTabA");
		txt_LotNoFieldA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "LotNoFieldA");
		txt_BatchNoFieldA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "BatchNoFieldA");

		// IN_IRO_Emp_037
		btn_GridMasterInfo1A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "GridMasterInfo1A");
		btn_GridMasterInfo2A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "GridMasterInfo2A");
		btn_DeleteRaw2A = findElementInXLSheet(getParameterInvertoryAndWarehouse, "DeleteRaw2A");
		btn_GridMasterInfo1IRA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "GridMasterInfo1IRA");
		btn_EntutionLogoA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "EntutionLogoA");
		btn_TaskEventA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "TaskEventA");
		btn_InternalReceiptsTileA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "InternalReceiptsTileA");
		txt_InternalReceiptsSerchInTaskEventA = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"InternalReceiptsSerchInTaskEventA");
		btn_InternalReceiptsBlueArrowA = findElementInXLSheet(getParameterInvertoryAndWarehouse,
				"InternalReceiptsBlueArrowA");
		btn_headerCloseA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "headerCloseA");

		// IN_IRO_Emp_042
		btn_PostDateA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "PostDateA");
		btn_PostDatetxtA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "PostDatetxtA");
		btn_PostDateMonthA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "PostDateMonthA");
		txt_PostDateValidatorIROA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "PostDateValidatorIROA");
		btn_PostDateApplyA = findElementInXLSheet(getParameterInvertoryAndWarehouse, "PostDateApplyA");
		
		/* Smoke Production */
		txt_externelDescriptionProductInformation = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_externelDescriptionProductInformation");
		txt_basePriceProductInformation = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_basePriceProductInformation");
		txt_genaricNameProductInformation = findElementInXLSheet(getParameterInvertoryAndWarehouse, "txt_genaricNameProductInformation");
		drop_mnufacturingTypeProductInformation = findElementInXLSheet(getParameterInvertoryAndWarehouse, "drop_mnufacturingTypeProductInformation");

	}

	public static void readData() throws Exception {
		// Login
		String computerName = System.getenv().get("COMPUTERNAME");
		if (computerName.equals("DESKTOP-C6E8CJU") || computerName.equals("MADHUSHAN-LAPada")) {
			siteURL = findElementInXLSheet(getDataInvertoryAndWarehouse, "site url external");
		} else {
			siteURL = findElementInXLSheet(getDataInvertoryAndWarehouse, "site url");
		}
		userNameData = findElementInXLSheet(getDataInvertoryAndWarehouse, "user name data");
		passwordData = findElementInXLSheet(getDataInvertoryAndWarehouse, "password data");
		gbvBileetaUsername = findElementInXLSheet(getDataInvertoryAndWarehouse, "gbvBileetaUsername");
		gbvBileetaPassword = findElementInXLSheet(getDataInvertoryAndWarehouse, "gbvBileetaPassword");

		/* Common automation tenant */
		commonCustomer = findElementInXLSheet(getDataInvertoryAndWarehouse, "commonCustomer");
		quantityOne = findElementInXLSheet(getDataInvertoryAndWarehouse, "quantityOne");
		hundred = findElementInXLSheet(getDataInvertoryAndWarehouse, "hundred");
		
		/* Common */
		warehouseGstWarehouseNegombo = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"warehouseGstWarehouseNegombo");
		commonQuantity = findElementInXLSheet(getDataInvertoryAndWarehouse, "commonQuantity");
		commonUnitCost = findElementInXLSheet(getDataInvertoryAndWarehouse, "commonUnitCost");
		productGroup = findElementInXLSheet(getDataInvertoryAndWarehouse, "productGroup");
		manufacturer = findElementInXLSheet(getDataInvertoryAndWarehouse, "manufacturer");
		commonWarehouseCodeOnly = findElementInXLSheet(getDataInvertoryAndWarehouse, "commonWarehouseCodeOnly");
		commonWarehouseCodeAndDescription = findElementInXLSheet(getDataInvertoryAndWarehouse, "commonWarehouseCodeAndDescription");

		// IW_TC_001
		outboundCostingMethodProductGroupConfig = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"outboundCostingMethodProductGroupConfig");
		tollerenceProductGroupConfig = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"tollerenceProductGroupConfig");
		minPriceProductGroupConfig = findElementInXLSheet(getDataInvertoryAndWarehouse, "minPriceProductGroupConfig");

		// IW_TC_003
		productCode = findElementInXLSheet(getDataInvertoryAndWarehouse, "productCode");
		productDesription = findElementInXLSheet(getDataInvertoryAndWarehouse, "productDesription");
		menufacturerName = findElementInXLSheet(getDataInvertoryAndWarehouse, "menufacturerName");
		uomGroup = findElementInXLSheet(getDataInvertoryAndWarehouse, "uomGroup");
		uom = findElementInXLSheet(getDataInvertoryAndWarehouse, "uom");
		productWidth = findElementInXLSheet(getDataInvertoryAndWarehouse, "productWidth");
		productHeight = findElementInXLSheet(getDataInvertoryAndWarehouse, "productHeight");
		productLength = findElementInXLSheet(getDataInvertoryAndWarehouse, "productLength");
		outboundCostingMethod = findElementInXLSheet(getDataInvertoryAndWarehouse, "outboundCostingMethod");
		tollerenceDate = findElementInXLSheet(getDataInvertoryAndWarehouse, "tollerenceDate");
		productionGroupBatchProductCreation = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"productionGroupBatchProductCreation");

		// IW_TC_004
		warehouseName = findElementInXLSheet(getDataInvertoryAndWarehouse, "warehouseName");
		qurantineWarehouse = findElementInXLSheet(getDataInvertoryAndWarehouse, "qurantineWarehouse");
		businessUnit = findElementInXLSheet(getDataInvertoryAndWarehouse, "businessUnit");
		lotBookWarehouse = findElementInXLSheet(getDataInvertoryAndWarehouse, "lotBookWarehouse");

		// IW_TC_005
		descStockAdj = findElementInXLSheet(getDataInvertoryAndWarehouse, "warehouseName");
		serialProduct = findElementInXLSheet(getDataInvertoryAndWarehouse, "warehouseCode");
		batchProduct = findElementInXLSheet(getDataInvertoryAndWarehouse, "businessUnit");
		warehouseStockAdj = findElementInXLSheet(getDataInvertoryAndWarehouse, "warehouseStockAdj");
		productBatchIW = findElementInXLSheet(getDataInvertoryAndWarehouse, "productBatchIW");
		productQuantity1 = findElementInXLSheet(getDataInvertoryAndWarehouse, "productQuantity1");
		productCost1 = findElementInXLSheet(getDataInvertoryAndWarehouse, "productCost1");
		productCost2 = findElementInXLSheet(getDataInvertoryAndWarehouse, "productCost2");
		fromWareTO = findElementInXLSheet(getDataInvertoryAndWarehouse, "fromWareTO");
		productSerialIW = findElementInXLSheet(getDataInvertoryAndWarehouse, "productSerialIW");
		qtyCommonStockAdjustment = findElementInXLSheet(getDataInvertoryAndWarehouse, "qtyCommonStockAdjustment");

		// IW_TC_006
		fromWareTO = findElementInXLSheet(getDataInvertoryAndWarehouse, "fromWareTO"); // IW_TC_007
		toWareTO = findElementInXLSheet(getDataInvertoryAndWarehouse, "toWareTO");// IW_TC_007
		productTO = findElementInXLSheet(getDataInvertoryAndWarehouse, "productTO");
		productTOCost = findElementInXLSheet(getDataInvertoryAndWarehouse, "productTOCost");
		fromWareTransferOrder = findElementInXLSheet(getDataInvertoryAndWarehouse, "fromWareTransferOrder");
		toWareTransferOrder = findElementInXLSheet(getDataInvertoryAndWarehouse, "toWareTransferOrder");
		qtyTransferOrder = findElementInXLSheet(getDataInvertoryAndWarehouse, "qtyTransferOrder");

		/* IW_TC_007 */
		fromWareInterDepartmentlTransferOrder = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"fromWareInterDepartmentlTransferOrder");
		toWareInterDepartmentlTransferOrder = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"toWareInterDepartmentlTransferOrder");
		salesPriceModelInterDepartmentalTransferOrder = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"salesPriceModelInterDepartmentalTransferOrder");
		qtyInterDepartmentTransferOrder = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"qtyInterDepartmentTransferOrder");

		// IW_TC_008
		qtyProductConvertionFromProduct = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"qtyProductConvertionFromProduct");
		toProductProductConversion = findElementInXLSheet(getDataInvertoryAndWarehouse, "toProductProductConversion");
		toProductCoseProductConversion = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"toProductCoseProductConversion");
		lotNoProductConversionToProduct = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"lotNoProductConversionToProduct");
		productTo08 = findElementInXLSheet(getDataInvertoryAndWarehouse, "productTo08");
		productStockConversion = findElementInXLSheet(getDataInvertoryAndWarehouse, "productStockConversion");

		// IW_TC_010
		titleInternelOrder = findElementInXLSheet(getDataInvertoryAndWarehouse, "titleInternelOrder");
		titleInternelOrder = findElementInXLSheet(getDataInvertoryAndWarehouse, "titleInternelOrder");
		shipingModeInternelOrder = findElementInXLSheet(getDataInvertoryAndWarehouse, "shipingModeInternelOrder");
		batch_product1 = findElementInXLSheet(getDataInvertoryAndWarehouse, "batch_product1");
		serial_product1 = findElementInXLSheet(getDataInvertoryAndWarehouse, "serial_product1");
		warehouse1 = findElementInXLSheet(getDataInvertoryAndWarehouse, "warehouse1");
		qtyInternelOrderProduct1 = findElementInXLSheet(getDataInvertoryAndWarehouse, "qtyInternelOrderProduct1");
		qtyInternelOrderProduct2 = findElementInXLSheet(getDataInvertoryAndWarehouse, "qtyInternelOrderProduct2");
		warehouseInternelOrderEmployeeRequest = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"warehouseInternelOrderEmployeeRequest");
		// IW_TC_012
		qtyProduct1OutboundShipmentILO = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"qtyProduct1OutboundShipmentILO");
		qtyProduct2OutboundShipmentILO = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"qtyProduct2OutboundShipmentILO");
		billingAddressILO = findElementInXLSheet(getDataInvertoryAndWarehouse, "billingAddressILO");
		deliveryAddressILO = findElementInXLSheet(getDataInvertoryAndWarehouse, "deliveryAddressILO");
		costProduct01ILO = findElementInXLSheet(getDataInvertoryAndWarehouse, "costProduct01ILO");
		costProduct02ILO = findElementInXLSheet(getDataInvertoryAndWarehouse, "costProduct02ILO");

		// IW_TC_013
		customerAccountILO = findElementInXLSheet(getDataInvertoryAndWarehouse, "customerAccountILO");
		address1ILO = findElementInXLSheet(getDataInvertoryAndWarehouse, "address1ILO");
		address2ILO = findElementInXLSheet(getDataInvertoryAndWarehouse, "address2ILO");
		product1ILO = findElementInXLSheet(getDataInvertoryAndWarehouse, "product1ILO");
		quantitya1ILO = findElementInXLSheet(getDataInvertoryAndWarehouse, "quantitya1ILO");
		quantity2ILO = findElementInXLSheet(getDataInvertoryAndWarehouse, "quantity2ILO");
		product2ILO = findElementInXLSheet(getDataInvertoryAndWarehouse, "product2ILO");
		customerAccountILO2 = findElementInXLSheet(getDataInvertoryAndWarehouse, "customerAccountILO2");

		// IW_TC_014
		qtyPlaned = findElementInXLSheet(getDataInvertoryAndWarehouse, "qtyPlaned");

		// IW_TC_016
		descriptionAssemblyCostEstimation = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"descriptionAssemblyCostEstimation");
		customerAccountACE = findElementInXLSheet(getDataInvertoryAndWarehouse, "customerAccountACE");
		assemblyProductACE = findElementInXLSheet(getDataInvertoryAndWarehouse, "assemblyProductACE");
		pricingProfileACE = findElementInXLSheet(getDataInvertoryAndWarehouse, "pricingProfileACE");
		typeEstmationAce1 = findElementInXLSheet(getDataInvertoryAndWarehouse, "typeEstmationAce1");
		product1EstimationDeatailsACE = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"product1EstimationDeatailsACE");
		qtyInputProduct1ACE = findElementInXLSheet(getDataInvertoryAndWarehouse, "qtyInputProduct1ACE");
		costInputProduct1ACE = findElementInXLSheet(getDataInvertoryAndWarehouse, "costInputProduct1ACE");
		typeEstmationAce2 = findElementInXLSheet(getDataInvertoryAndWarehouse, "typeEstmationAce2");
		product2EstimationDeatailsACE = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"product2EstimationDeatailsACE");
		qtyInputProduct2ACE = findElementInXLSheet(getDataInvertoryAndWarehouse, "qtyInputProduct2ACE");
		costInputProduct2ACE = findElementInXLSheet(getDataInvertoryAndWarehouse, "costInputProduct2ACE");
		typeEstmationAce3Expence = findElementInXLSheet(getDataInvertoryAndWarehouse, "typeEstmationAce3Expence");
		costInputService3ACE = findElementInXLSheet(getDataInvertoryAndWarehouse, "costInputService3ACE");
		typeEstmationAce4 = findElementInXLSheet(getDataInvertoryAndWarehouse, "typeEstmationAce4");
		costInputService4ACE = findElementInXLSheet(getDataInvertoryAndWarehouse, "costInputService4ACE");
		product3EstimationDeatailsACESerivice = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"product3EstimationDeatailsACESerivice");
		product4EstimationDeatailsACEService = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"product4EstimationDeatailsACEService");

		// IW_TC_017
		employee1 = findElementInXLSheet(getDataInvertoryAndWarehouse, "employee1");
		productionUnit1 = findElementInXLSheet(getDataInvertoryAndWarehouse, "productionUnit1");
		warehouseAO = findElementInXLSheet(getDataInvertoryAndWarehouse, "warehouseAO");
		warehouseWIPAO = findElementInXLSheet(getDataInvertoryAndWarehouse, "warehouseWIPAO");
		assemblyProductAO = findElementInXLSheet(getDataInvertoryAndWarehouse, "assemblyProductAO");

		/* IW_TC_018 */
		customerAccountSAlesOrder = findElementInXLSheet(getDataInvertoryAndWarehouse, "customerAccountSAlesOrder");
		currencySalesOrder = findElementInXLSheet(getDataInvertoryAndWarehouse, "currencySalesOrder");
		salesUnitSalesOrder = findElementInXLSheet(getDataInvertoryAndWarehouse, "salesUnitSalesOrder");
		assemblyProductSalesOrder = findElementInXLSheet(getDataInvertoryAndWarehouse, "assemblyProductSalesOrder");
		quantitySalesOrder = findElementInXLSheet(getDataInvertoryAndWarehouse, "quantitySalesOrder");
		unitPriceSalesOrder = findElementInXLSheet(getDataInvertoryAndWarehouse, "unitPriceSalesOrder");
		warehouseSalesOrder = findElementInXLSheet(getDataInvertoryAndWarehouse, "warehouseSalesOrder");
		expenceService = findElementInXLSheet(getDataInvertoryAndWarehouse, "expenceService");
		serviceAssemblyProcessCostEstimation = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"serviceAssemblyProcessCostEstimation");
		taskAssignUser = findElementInXLSheet(getDataInvertoryAndWarehouse, "taskAssignUser");

		// IW_TC_020
		productSerialNumberViewer = findElementInXLSheet(getDataInvertoryAndWarehouse, "productSerialNumberViewer");

		/* IW_TC_021 */
		outboundQuantityChangedSeriel = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"outboundQuantityChangedSeriel");
		serielChangingProduct = findElementInXLSheet(getDataInvertoryAndWarehouse, "serielChangingProduct");
		warehouseChangedSerielOutbound = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"warehouseChangedSerielOutbound");

		// IW_TC_022
		selectionInboundShipment = findElementInXLSheet(getDataInvertoryAndWarehouse, "selectionInboundShipment");
		selectionQCQcceptancePurchaseOrderInboundShipment = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"selectionQCQcceptancePurchaseOrderInboundShipment");
		passQtyQCAcceptance = findElementInXLSheet(getDataInvertoryAndWarehouse, "passQtyQCAcceptance");
		failQtyQCAcceptance = findElementInXLSheet(getDataInvertoryAndWarehouse, "failQtyQCAcceptance");
		reasonFailQuantityQcAcceptance = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"reasonFailQuantityQcAcceptance");
		vendor = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"vendor");

		// IW_TC_023
		qtyPurchaseORder = findElementInXLSheet(getDataInvertoryAndWarehouse, "qtyPurchaseORder");
		unitPricePurchaseOrder = findElementInXLSheet(getDataInvertoryAndWarehouse, "unitPricePurchaseOrder");
		warehousePurchaseOrder = findElementInXLSheet(getDataInvertoryAndWarehouse, "warehousePurchaseOrder");
		documentTypeShipmentAceptance = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"documentTypeShipmentAceptance");
		purchsaeOrderProduct = findElementInXLSheet(getDataInvertoryAndWarehouse, "purchsaeOrderProduct");
		serialNumberStartShipmentAcceptance = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"serialNumberStartShipmentAcceptance");
		lotNoShipmentAcceptance = findElementInXLSheet(getDataInvertoryAndWarehouse, "lotNoShipmentAcceptance");
		shipmentAcceptanceJourney = findElementInXLSheet(getDataInvertoryAndWarehouse, "shipmentAcceptanceJourney");

		// Common PO For Fin_TC_016
		vendorCommonPurchasOrder = findElementInXLSheet(getDataInvertoryAndWarehouse, "vendorCommonPurchasOrder");

		// Fin_TC_009
		warehouseStockTake = findElementInXLSheet(getDataInvertoryAndWarehouse, "warehouseStockTake");
		batchProduct1StockTake = findElementInXLSheet(getDataInvertoryAndWarehouse, "batchProduct1StockTake");
		batchProduct2StockTake = findElementInXLSheet(getDataInvertoryAndWarehouse, "batchProduct2StockTake");
		
		// Fin_TC_019
		vrndorBarcodeGenarate = findElementInXLSheet(getDataInvertoryAndWarehouse, "vrndorBarcodeGenarate");

		/* Create common products */
		minPriceCoommnonCreateProduct = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"minPriceCoommnonCreateProduct");
		maxPriceCoommnonCreateProduct = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"maxPriceCoommnonCreateProduct");
		productGroupServiceCommonProductCreate = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"productGroupServiceCommonProductCreate");

		/* AV_IW_AssemblyProposal */
		assemblyProductAssemblyProposal = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"assemblyProductAssemblyProposal");
		assemblyProcessProductionUnitActionVerification = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"assemblyProcessProductionUnitActionVerification");

		/* AV_IW_RackTransfer */
		warehouseRackTransfer = findElementInXLSheet(getDataInvertoryAndWarehouse, "warehouseRackTransfer");
		filterByRackTransfer = findElementInXLSheet(getDataInvertoryAndWarehouse, "filterByRackTransfer");
		rackTransferQuantity = findElementInXLSheet(getDataInvertoryAndWarehouse, "rackTransferQuantity");

		/* AV_IW_AssemblyOrder */
		changeReasonHoldUnholdActionVerification = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"changeReasonHoldUnholdActionVerification");

		/******************* Inventory And Warehouse Creations ********************/

		/* Lot Book */
		lotBookModule = findElementInXLSheet(getDataInvertoryAndWarehouse, "lotBookModule");

		/*********************** Regression01 **********************/
		/* IN_IO_004 */
		employeeNameRegressionNew1 = findElementInXLSheet(getDataInvertoryAndWarehouse, "employeeNameRegressionNew1");
		mmployeeCodeRegressionNew1 = findElementInXLSheet(getDataInvertoryAndWarehouse, "mmployeeCodeRegressionNew1");

		/* IN_IO_005 */
		campaign_iwIO005 = findElementInXLSheet(getDataInvertoryAndWarehouse, "campaign_iwIO005");

		/* IN_IO_006 */
		varientProduct = findElementInXLSheet(getDataInvertoryAndWarehouse, "varientProduct");

		/* IN_IO_009 */
		productCode_IW_IO_009 = findElementInXLSheet(getDataInvertoryAndWarehouse, "productCode_IW_IO_009");
		productDescription_IW_IO_009 = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"productDescription_IW_IO_009");

		/* IN_IO_025_I_026_027 */
		batchSpecificProductWithDifferentLots = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"batchSpecificProductWithDifferentLots");
		costForBatchProductsWithDifferentLots = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"costForBatchProductsWithDifferentLots");

		/* IN_IO_028_I_029_030 */
		batchSpecificProductWithDifferentBatchesSameLot = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"batchSpecificProductWithDifferentBatchesSameLot");
		costForBbatchSpecificProductWithDifferentBatchesSameLot = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"costForBbatchSpecificProductWithDifferentBatchesSameLot");

		/* IN_IO_031_032_033 */
		costOfProductForIN_IO_031_032_033 = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"costOfProductForIN_IO_031_032_033");
		stockAdjustmentQuantiyt_IN_IO_031_032_033 = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"stockAdjustmentQuantiyt_IN_IO_031_032_033");

		/* IN_IO_034 */
		ohVerifyProduct = findElementInXLSheet(getDataInvertoryAndWarehouse, "ohVerifyProduct");

		/* IN_IO_036 */
		stockReservationMethod = findElementInXLSheet(getDataInvertoryAndWarehouse, "stockReservationMethod");
		reservationVerifyProduct = findElementInXLSheet(getDataInvertoryAndWarehouse, "reservationVerifyProduct");
		productReserveQuantity = findElementInXLSheet(getDataInvertoryAndWarehouse, "productReserveQuantity");

		/* IN_IO_037 */
		stockReservationMethodAutomatic = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"stockReservationMethodAutomatic");

		/* IN_IO_038 */
		productAutoOutboundReservationVerify = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"productAutoOutboundReservationVerify");
		stockReservationMethodAutomaticAndInbound = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"stockReservationMethodAutomaticAndInbound");
		purchaseOrderQuantity_IN_IO_038 = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"purchaseOrderQuantity_IN_IO_038");

		/* IN_IO_040 */
		qtyInternelOrderReserved_IN_IO_040 = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"qtyInternelOrderReserved_IN_IO_040");
		qtyOutbound_IN_IO_040 = findElementInXLSheet(getDataInvertoryAndWarehouse, "qtyOutbound_IN_IO_040");

		/* IN_IO_042_043_044 */
		productTaggedWithAlternatives = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"productTaggedWithAlternatives");
		alternativeProduct = findElementInXLSheet(getDataInvertoryAndWarehouse, "alternativeProduct");
		orderQuantity_IN_IO_042 = findElementInXLSheet(getDataInvertoryAndWarehouse, "orderQuantity_IN_IO_042");
		costForAlternativeProduct = findElementInXLSheet(getDataInvertoryAndWarehouse, "costForAlternativeProduct");

		/* IN_IO_045_046_047 */
		costForAlternativeProductAndWithoutTagAlternativeProduct = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"costForAlternativeProductAndWithoutTagAlternativeProduct");

		/* IN_IO_048 */
		decimalAllowProduct = findElementInXLSheet(getDataInvertoryAndWarehouse, "decimalAllowProduct");
		quantity_IN_IO_048 = findElementInXLSheet(getDataInvertoryAndWarehouse, "quantity_IN_IO_048");

		/* IN_IO_050 */
		quantity_IN_IO_050 = findElementInXLSheet(getDataInvertoryAndWarehouse, "quantity_IN_IO_050");

		/* IN_IO_054 */
		workflowJourney_IN_IO_054 = findElementInXLSheet(getDataInvertoryAndWarehouse, "workflowJourney_IN_IO_054");
		usernamePradeep = findElementInXLSheet(getDataInvertoryAndWarehouse, "usernamePradeep");
		passwordPradeep = findElementInXLSheet(getDataInvertoryAndWarehouse, "passwordPradeep");

		/* IN_IO_055_058 */
		internelOrderQuantity = findElementInXLSheet(getDataInvertoryAndWarehouse, "internelOrderQuantity");

		/* IN_IO_065 */
		firstLotStockAdjustmentQuantity_IN_IO_065 = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"firstLotStockAdjustmentQuantity_IN_IO_065");
		internalOrderQuantity01_IN_IO_065 = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"internalOrderQuantity01_IN_IO_065");
		internalOrderQuantity02_IN_IO_065 = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"internalOrderQuantity02_IN_IO_065");
		internalOrderQuantity03_IN_IO_065 = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"internalOrderQuantity03_IN_IO_065");

		/* IN_IO_066 */
		warhouesePO_IN_IO_066 = findElementInXLSheet(getDataInvertoryAndWarehouse, "warhouesePO_IN_IO_066");
		quantityPO_IN_IO_066 = findElementInXLSheet(getDataInvertoryAndWarehouse, "quantityPO_IN_IO_066");
		pricePO_IN_IO_066 = findElementInXLSheet(getDataInvertoryAndWarehouse, "pricePO_IN_IO_066");

		/* IN_IO_067 */
		customerAccountSalesOrder_IN_IO_067 = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"customerAccountSalesOrder_IN_IO_067");
		salesOrderUnitsQuantity_IN_IO_067 = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"salesOrderUnitsQuantity_IN_IO_067");
		salesOrderUnitPrise_IN_IO_067 = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"salesOrderUnitPrise_IN_IO_067");
		salesOrderWarehouse_IN_IO_067 = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"salesOrderWarehouse_IN_IO_067");
		salesOrderSearchByMethodSalesReturs = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"salesOrderSearchByMethodSalesReturs");
		returnReasonSalesReturnOrder = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"returnReasonSalesReturnOrder");

		/* IN_IO_069 */
		warehouse_IN_IO_069 = findElementInXLSheet(getDataInvertoryAndWarehouse, "warehouse_IN_IO_069");

		/* IN_IO_072 */
		warehouse_IN_IO_072 = findElementInXLSheet(getDataInvertoryAndWarehouse, "warehouse_IN_IO_072");
		quantity_IN_IO_072 = findElementInXLSheet(getDataInvertoryAndWarehouse, "quantity_IN_IO_072");
		unitPrice_IN_IO_072 = findElementInXLSheet(getDataInvertoryAndWarehouse, "unitPrice_IN_IO_072");
		quantityInternelOrder_IN_IO_072 = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"quantityInternelOrder_IN_IO_072");

		/* IN_IO_074 */
		productQuantity_IN_IO_074 = findElementInXLSheet(getDataInvertoryAndWarehouse, "productQuantity_IN_IO_074");

		/* IN_IO_075 */
		physicalQuantity_IN_IO_075 = findElementInXLSheet(getDataInvertoryAndWarehouse, "physicalQuantity_IN_IO_075");
		internelOrderQuntity_IN_IO_075 = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"internelOrderQuntity_IN_IO_075");

		/* IN_IRO_Emp_001_002_003_004_005 */
		employeeCommonVerify = findElementInXLSheet(getDataInvertoryAndWarehouse, "employeeCommonVerify");
		warehouse_IN_IRO_Emp_004 = findElementInXLSheet(getDataInvertoryAndWarehouse, "warehouse_IN_IRO_Emp_004");

		/* IN_IRO_Emp_006 */
		employee01 = findElementInXLSheet(getDataInvertoryAndWarehouse, "employee01");
		employee02 = findElementInXLSheet(getDataInvertoryAndWarehouse, "employee02");
		quantity_IN_IRO_Emp_006 = findElementInXLSheet(getDataInvertoryAndWarehouse, "quantity_IN_IRO_Emp_006");

		/* IN_IRO_Emp_017 */
		quntity_edit_IN_IRO_Emp_017 = findElementInXLSheet(getDataInvertoryAndWarehouse, "quntity_edit_IN_IRO_Emp_017");

		/************** Regression-Transfer Order *********/
		/* common */
		commonUseWarehouseTransferOrder = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"commonUseWarehouseTransferOrder");

		/* IN_TO_003 */
		addressGSTWarehouse = findElementInXLSheet(getDataInvertoryAndWarehouse, "addressGSTWarehouse");

		/* IN_TO_006 */
		changedAddressFromWarehouse = findElementInXLSheet(getDataInvertoryAndWarehouse, "changedAddressFromWarehouse");
		changedAddressDestinationWarehouse = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"changedAddressDestinationWarehouse");

		/* IN_TO_008 */
		autoSearchProductQuantityTransferOrder = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"autoSearchProductQuantityTransferOrder");

		/* IN_TO_010 */
		defoultUOMCommonProduct = findElementInXLSheet(getDataInvertoryAndWarehouse, "defoultUOMCommonProduct");

		/* IN_TO_013 */
		transferCostTransferOrder = findElementInXLSheet(getDataInvertoryAndWarehouse, "transferCostTransferOrder");

		/* IN_TO_015 */
		qtyTransferOrderRegression = findElementInXLSheet(getDataInvertoryAndWarehouse, "qtyTransferOrderRegression");

		/* IN_TO_016 */
		myTemplate = findElementInXLSheet(getDataInvertoryAndWarehouse, "myTemplate");

		/* IN_TO_017 */
		updatedTransferOrderQuantity = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"updatedTransferOrderQuantity");

		/* IN_TO_031 */
		stockUsedQuantity_IN_TO_031 = findElementInXLSheet(getDataInvertoryAndWarehouse, "stockUsedQuantity_IN_TO_031");
		employee01GBV = findElementInXLSheet(getDataInvertoryAndWarehouse, "employee01GBV");

		/* IN_TO_036_038 */
		warehouse_IN_TO_036_038 = findElementInXLSheet(getDataInvertoryAndWarehouse, "warehouse_IN_TO_036_038");
		quantity_IN_TO_036_038 = findElementInXLSheet(getDataInvertoryAndWarehouse, "quantity_IN_TO_036_038");
		unitCostStockAdjustment01 = findElementInXLSheet(getDataInvertoryAndWarehouse, "unitCostStockAdjustment01");
		unitCostStockAdjustment02 = findElementInXLSheet(getDataInvertoryAndWarehouse, "unitCostStockAdjustment02");
		lot01_IN_TO_036_038 = findElementInXLSheet(getDataInvertoryAndWarehouse, "lot01_IN_TO_036_038");
		lot02_IN_TO_036_038 = findElementInXLSheet(getDataInvertoryAndWarehouse, "lot02_IN_TO_036_038");
		transferCost_IN_TO_036_038 = findElementInXLSheet(getDataInvertoryAndWarehouse, "transferCost_IN_TO_036_038");
		unitCostBinCardTemplate = findElementInXLSheet(getDataInvertoryAndWarehouse, "unitCostBinCardTemplate");
		stockValueWhenOutbound_IN_TO_37 = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"stockValueWhenOutbound_IN_TO_37");
		stockValueWhenInbound_IN_TO_39 = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"stockValueWhenInbound_IN_TO_39");

		/* IN_TO_040 */
		partialQuntity01_IN_TO_040 = findElementInXLSheet(getDataInvertoryAndWarehouse, "partialQuntity01_IN_TO_040");

		/* IN_TO_041 */
		partialQuntity01_IN_TO_041 = findElementInXLSheet(getDataInvertoryAndWarehouse, "partialQuntity01_IN_TO_041");
		restOfQuntity01_IN_TO_041 = findElementInXLSheet(getDataInvertoryAndWarehouse, "restOfQuntity01_IN_TO_041");

		/* IN_TO_044 */
		quantityFive_IN_TO_044 = findElementInXLSheet(getDataInvertoryAndWarehouse, "quantityFive_IN_TO_044");
		quantityFour_IN_TO_044 = findElementInXLSheet(getDataInvertoryAndWarehouse, "quantityFour_IN_TO_044");
		quantityTwo_IN_TO_044 = findElementInXLSheet(getDataInvertoryAndWarehouse, "quantityTwo_IN_TO_044");

		/* IN_TO_046 */
		quantity_IN_TO_046 = findElementInXLSheet(getDataInvertoryAndWarehouse, "quantity_IN_TO_046");
		unitCost_IN_TO_046 = findElementInXLSheet(getDataInvertoryAndWarehouse, "unitCost_IN_TO_046");
		quantityFifteen_IN_TO_046 = findElementInXLSheet(getDataInvertoryAndWarehouse, "quantityFifteen_IN_TO_046");
		quantityFive_IN_TO_046 = findElementInXLSheet(getDataInvertoryAndWarehouse, "quantityFive_IN_TO_046");

		/* IN_TO_047 */
		quantity_IN_TO_047 = findElementInXLSheet(getDataInvertoryAndWarehouse, "quantity_IN_TO_047");
		unitCost_IN_TO_047 = findElementInXLSheet(getDataInvertoryAndWarehouse, "unitCost_IN_TO_047");
		quantityFifteen_IN_TO_047 = findElementInXLSheet(getDataInvertoryAndWarehouse, "quantityFifteen_IN_TO_047");
		quantityFive_IN_TO_047 = findElementInXLSheet(getDataInvertoryAndWarehouse, "quantityFive_IN_TO_047");

		/* IN_TO_049 */
		quantity_IN_TO_049 = findElementInXLSheet(getDataInvertoryAndWarehouse, "quantity_IN_TO_049");
		unitCost_IN_TO_049 = findElementInXLSheet(getDataInvertoryAndWarehouse, "unitCost_IN_TO_049");
		lotWiseStockTemplate_IN_TO_049 = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"lotWiseStockTemplate_IN_TO_049");
		serielBatchLotNumberReport = findElementInXLSheet(getDataInvertoryAndWarehouse, "serielBatchLotNumberReport");

		/* IN_TO_050_051 */
		templateOutboundShipment_IN_TO_050_051 = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"templateOutboundShipment_IN_TO_050_051");
		templateInboundShipment_IN_TO_050_051 = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"templateInboundShipment_IN_TO_050_051");

		/* IN_TO_054 */
		quantity_IN_TO_054 = findElementInXLSheet(getDataInvertoryAndWarehouse, "quantity_IN_TO_054");
		unitCost_IN_TO_054 = findElementInXLSheet(getDataInvertoryAndWarehouse, "unitCost_IN_TO_054");

		/* IN_TO_070 */
		customerAccount_IN_TO_070 = findElementInXLSheet(getDataInvertoryAndWarehouse, "customerAccount_IN_TO_070");

		/* IN_TO_056 */
		quantityCommonTransferOrder = findElementInXLSheet(getDataInvertoryAndWarehouse, "quantityCommonTransferOrder");
		unitCostCommonTransferOrder = findElementInXLSheet(getDataInvertoryAndWarehouse, "unitCostCommonTransferOrder");
		decimalQuantityTransferOrder = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"decimalQuantityTransferOrder");

		/* IN_TO_057 */
		roundedDecimalValueTransferOrder = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"roundedDecimalValueTransferOrder");

		/* IN_TO_061 */
		partialQuantityCommonOutboundShipmentTransferOrderJourney = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"partialQuantityCommonOutboundShipmentTransferOrderJourney");

		/* IN_TO_065 */
		quantiry_IN_TO_065 = findElementInXLSheet(getDataInvertoryAndWarehouse, "quantiry_IN_TO_065");

		/* IN_TO_067 */
		quantiry_IN_TO_067 = findElementInXLSheet(getDataInvertoryAndWarehouse, "quantiry_IN_TO_067");

		/* IN_TO_068 */
		vendorCommonAutomationTenant = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"vendorCommonAutomationTenant");

		/* IN_TO_086 */
		quantity_IN_TO_086 = findElementInXLSheet(getDataInvertoryAndWarehouse, "quantity_IN_TO_086");
		unitCost_IN_TO_086 = findElementInXLSheet(getDataInvertoryAndWarehouse, "unitCost_IN_TO_086");
		quantityOverhead_IN_TO_086 = findElementInXLSheet(getDataInvertoryAndWarehouse, "quantityOverhead_IN_TO_086");

		/* IN_TO_087 */
		stockAdjustmentQuantiyt_IN_TO_087 = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"stockAdjustmentQuantiyt_IN_TO_087");
		transferOrderQuantiyt01_IN_TO_087 = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"transferOrderQuantiyt01_IN_TO_087");
		transferOrderQuantiyt02_IN_TO_087 = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"transferOrderQuantiyt02_IN_TO_087");

		/* IN_TO_089 */
		stockAdjustmentQuantiyt_IN_TO_089 = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"stockAdjustmentQuantiyt_IN_TO_089");
		transferOrderQuantiyt01_IN_TO_089 = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"transferOrderQuantiyt01_IN_TO_089");
		transferOrderQuantiyt02_IN_TO_089 = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"transferOrderQuantiyt02_IN_TO_089");

		/* IN_TO_090 */
		quantityStockAnjustment_IN_TO_090 = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"quantityStockAnjustment_IN_TO_090");
		unitPriceStockAdjustment_IN_IO_090 = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"unitPriceStockAdjustment_IN_IO_090");

		/* IN_TO_094 */
		customerAccount_IN_TO_094 = findElementInXLSheet(getDataInvertoryAndWarehouse, "customerAccount_IN_TO_094");

		/* IN_TO_097 */
		qtyGreaterThanOH_IN_TO_097 = findElementInXLSheet(getDataInvertoryAndWarehouse, "qtyGreaterThanOH_IN_TO_097");
		qtyGreaterThanOrderQty_IN_TO_097 = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"qtyGreaterThanOrderQty_IN_TO_097");

		/* IN_TO_101 */
		failStatusQCAcceptance = findElementInXLSheet(getDataInvertoryAndWarehouse, "failStatusQCAcceptance");

		/* IN_TO_102 */
		template_IN_TO_102 = findElementInXLSheet(getDataInvertoryAndWarehouse, "template_IN_TO_102");
		qcAcceptanceJourneyTransferOrderInboundShipment = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"qcAcceptanceJourneyTransferOrderInboundShipment");
		passQtyQCAcceptanceCommon = findElementInXLSheet(getDataInvertoryAndWarehouse, "passQtyQCAcceptanceCommon");
		passStatusQcAcceptance = findElementInXLSheet(getDataInvertoryAndWarehouse, "passStatusQcAcceptance");

		/* IN_TO_103 */
		passFailPArtialQuantityQCAcceptance = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"passFailPArtialQuantityQCAcceptance");

		/* IN_TO_104 */
		deductionQuantityQCReverseTransferOrderToWare = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"deductionQuantityQCReverseTransferOrderToWare");
		deductionQuantityQCReverseTransferOrderQuarantineWare = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"deductionQuantityQCReverseTransferOrderQuarantineWare");

		/* IN_TO_114 */
		shipmentAcceptanceJourneyTransferOrder = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"shipmentAcceptanceJourneyTransferOrder");
		seleilNoInvalidValidation = findElementInXLSheet(getDataInvertoryAndWarehouse, "seleilNoInvalidValidation");

		/* IN_TO_122 */
		documentTypeOutboundShipmentAceptance = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"documentTypeOutboundShipmentAceptance");

		/* Stock Take */
		/* IN_ST_008_009 */
		rackStockTakeProductAddingPopup = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"rackStockTakeProductAddingPopup");
		poruductAddingPhysicalQuantity = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"poruductAddingPhysicalQuantity");

		/* IN_ST_011 */
		stockTakeWarehouse = findElementInXLSheet(getDataInvertoryAndWarehouse, "stockTakeWarehouse");
		decimalQuantityStockTake = findElementInXLSheet(getDataInvertoryAndWarehouse, "decimalQuantityStockTake");
		afterResetAsdecimalQuantityToWithoutDecimalStockTake = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"afterResetAsdecimalQuantityToWithoutDecimalStockTake");

		/* IN_ST_013 */
		varienceQuantityStockTake = findElementInXLSheet(getDataInvertoryAndWarehouse, "varienceQuantityStockTake");

		/* IN_ST_014 */
		adjustedQuantityBeforeStockAdjustment = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"adjustedQuantityBeforeStockAdjustment");

		/* IN_ST_015 */
		changeQuantityStockTakeWhenUpdate = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"changeQuantityStockTakeWhenUpdate");
		newQuntityStockTakeWhenUpdate = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"newQuntityStockTakeWhenUpdate");

		/* IN_ST_016 */
		varienceQuantityFifteen = findElementInXLSheet(getDataInvertoryAndWarehouse, "varienceQuantityFifteen");

		/* IN_ST_021 */
		zeroBalanceStockTake = findElementInXLSheet(getDataInvertoryAndWarehouse, "zeroBalanceStockTake");

		/* IN_ST_022 */
		qtyReducePhysicalBalanceComparedToSystemBalance = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"qtyReducePhysicalBalanceComparedToSystemBalance");

		/* IN_ST_023 */
		qtyIncreacePhysicalBalanceComparedToSystemBalance = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"qtyIncreacePhysicalBalanceComparedToSystemBalance");

		/* IN_ST_032 */
		mainValidationOfForms = findElementInXLSheet(getDataInvertoryAndWarehouse, "mainValidationOfForms");

		/* IN_ST_033 */
		stockAlreadyDraftedValidationWhenTryToDoStockTake = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"stockAlreadyDraftedValidationWhenTryToDoStockTake");

		/* IN_ST_034 */
		quantityStockTakeRegression = findElementInXLSheet(getDataInvertoryAndWarehouse, "quantityStockTakeRegression");
		adjustedQuantityStockAdjustment = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"adjustedQuantityStockAdjustment");

		/* IN_ST_040_041 */
		reduceQuantityStockAdjustmentComesFromStockTake = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"reduceQuantityStockAdjustmentComesFromStockTake");
		remainQuantityPartialStockAdjustmentComesFromStockTake = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"remainQuantityPartialStockAdjustmentComesFromStockTake");

		/* IN_ST_046 */
		varienceQuantityMinusFive = findElementInXLSheet(getDataInvertoryAndWarehouse, "varienceQuantityMinusFive");
		varienceQuantityPlusFive = findElementInXLSheet(getDataInvertoryAndWarehouse, "varienceQuantityPlusFive");
		adjustedQuantityMinusFive = findElementInXLSheet(getDataInvertoryAndWarehouse, "adjustedQuantityMinusFive");
		adjustedQuantityPlusFive = findElementInXLSheet(getDataInvertoryAndWarehouse, "adjustedQuantityPlusFive");

		/* IN_ST_049 */
		newDescriptionForEditedStockAdjustment = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"newDescriptionForEditedStockAdjustment");

		/* IN_ST_051 */
		physicalBalanceFive = findElementInXLSheet(getDataInvertoryAndWarehouse, "physicalBalanceFive");
		varienceQuantityPlusFour = findElementInXLSheet(getDataInvertoryAndWarehouse, "varienceQuantityPlusFour");
		adjustedQuantityPlusFour = findElementInXLSheet(getDataInvertoryAndWarehouse, "adjustedQuantityPlusFour");

		/* Product Conversion */
		/* IN_PC_003 */
		productConversionWarehouseRegression = findElementInXLSheet(getDataInvertoryAndWarehouse,
				"productConversionWarehouseRegression");

		/* IN_PC_006 */
		defoultUOMProductConversion = findElementInXLSheet(getDataInvertoryAndWarehouse, "defoultUOMProductConversion");
		
		/* IN_PC_009 */
		capturedQuantityProductConversion = findElementInXLSheet(getDataInvertoryAndWarehouse, "capturedQuantityProductConversion");
		qtyFromProductProductConversion = findElementInXLSheet(getDataInvertoryAndWarehouse, "qtyFromProductProductConversion");

		/* IN_PC_011 */
		inactiveStatus = findElementInXLSheet(getDataInvertoryAndWarehouse, "inactiveStatus");
		
		/* IN_PC_012 */
		productDefoultUom = findElementInXLSheet(getDataInvertoryAndWarehouse, "productDefoultUom");
		
		/* IN_PC_013 */
		costPriceProductConversion = findElementInXLSheet(getDataInvertoryAndWarehouse, "costPriceProductConversion");
		
		/* IN_PC_014 */
		conRateProductConversion = findElementInXLSheet(getDataInvertoryAndWarehouse, "conRateProductConversion");
		quantityAccordingToConRate = findElementInXLSheet(getDataInvertoryAndWarehouse, "quantityAccordingToConRate");
		
		/* IN_PC_015 */
		toQuantityForCheckConRate = findElementInXLSheet(getDataInvertoryAndWarehouse, "toQuantityForCheckConRate");
		conRateAccordingToToQuantity = findElementInXLSheet(getDataInvertoryAndWarehouse, "conRateAccordingToToQuantity");

		/* IN_PC_016 */
		decimalQtyFromProductProductConversion = findElementInXLSheet(getDataInvertoryAndWarehouse, "decimalQtyFromProductProductConversion");
		decimalQtyToProductProductConversion = findElementInXLSheet(getDataInvertoryAndWarehouse, "decimalQtyToProductProductConversion");
		
		/* IN_PC_017 */
		decimalRemovedQuantityFromProductProductConversion = findElementInXLSheet(getDataInvertoryAndWarehouse, "decimalRemovedQuantityFromProductProductConversion");
		decimalRemovedQuantityToProductProductConversion = findElementInXLSheet(getDataInvertoryAndWarehouse, "decimalRemovedQuantityToProductProductConversion");
		
		/* IN_PC_019 */
		quantityToProductProductConversion = findElementInXLSheet(getDataInvertoryAndWarehouse, "quantityToProductProductConversion");
		quantityFiveProductConversion = findElementInXLSheet(getDataInvertoryAndWarehouse, "quantityFiveProductConversion");
		
		/* IN_PC_026 */
		qtyFiveHundred = findElementInXLSheet(getDataInvertoryAndWarehouse, "qtyFiveHundred");

		/* IN_PC_027 */
		qtyFifteen = findElementInXLSheet(getDataInvertoryAndWarehouse, "qtyFifteen");
		costNinetyFive = findElementInXLSheet(getDataInvertoryAndWarehouse, "costNinetyFive");
		
		/* IN_PC_040 */
		quantityTenProductConversion = findElementInXLSheet(getDataInvertoryAndWarehouse, "quantityTenProductConversion");

		// ============================================RegressionAruna======================================================================
		// IN_SA_005
		ProductAddTxtA = findElementInXLSheet(getDataInvertoryAndWarehouse, "ProductAddTxtA");

		// IN_SA_012
		AdjustmentGroupTxtA = findElementInXLSheet(getDataInvertoryAndWarehouse, "AdjustmentGroupTxtA");

		// IN_SA_016
		ProductAddTxtDicA = findElementInXLSheet(getDataInvertoryAndWarehouse, "ProductAddTxtDicA");

		// IN_SA_020
		ProductAddTxt2A = findElementInXLSheet(getDataInvertoryAndWarehouse, "ProductAddTxt2A");

		// IN_SA_028
		RackTxtA = findElementInXLSheet(getDataInvertoryAndWarehouse, "RackTxtA");

		// IN_SA_042
		productQuantityA = findElementInXLSheet(getDataInvertoryAndWarehouse, "productQuantityA");

		// IN_SA_047
		SerialNoA = findElementInXLSheet(getDataInvertoryAndWarehouse, "SerialNoA");
		LotNoA = findElementInXLSheet(getDataInvertoryAndWarehouse, "LotNoA");

		// IN_SA_049
		Pro1A = findElementInXLSheet(getDataInvertoryAndWarehouse, "Pro1A");
		Pro2A = findElementInXLSheet(getDataInvertoryAndWarehouse, "Pro2A");

		// IN_SA_052
		SampleProA = findElementInXLSheet(getDataInvertoryAndWarehouse, "SampleProA");
		VariantGrouptxtA = findElementInXLSheet(getDataInvertoryAndWarehouse, "VariantGrouptxtA");

		// IN_SA_056
		BatchNoA = findElementInXLSheet(getDataInvertoryAndWarehouse, "BatchNoA");

		// IN_SA_059
		searchCustomerSalesOrderA = findElementInXLSheet(getDataInvertoryAndWarehouse, "searchCustomerSalesOrderA");

		// IN_SA_060
		RequestertxtA = findElementInXLSheet(getDataInvertoryAndWarehouse, "RequestertxtA");

		// IN_SA_065
		MinusProA = findElementInXLSheet(getDataInvertoryAndWarehouse, "MinusProA");

		// IN_SA_067
		WarehouseA = findElementInXLSheet(getDataInvertoryAndWarehouse, "WarehouseA");

		// IN_SA_068
		MinusPro2A = findElementInXLSheet(getDataInvertoryAndWarehouse, "MinusPro2A");

		// IN_SA_070
		SerialProA = findElementInXLSheet(getDataInvertoryAndWarehouse, "SerialProA");

		// IN_IRO_Emp_051
		WarehouseCodeA = findElementInXLSheet(getDataInvertoryAndWarehouse, "WarehouseCodeA");

		// IN_IRO_Emp_052
		WarehouseDicriptionA = findElementInXLSheet(getDataInvertoryAndWarehouse, "WarehouseDicriptionA");

		// IN_IRO_Emp_053
		WarehouseCodeAndDicriptionA = findElementInXLSheet(getDataInvertoryAndWarehouse, "WarehouseCodeAndDicriptionA");

		// IN_IRO_Emp_054
		EmployeeCodeA = findElementInXLSheet(getDataInvertoryAndWarehouse, "EmployeeCodeA");

		// IN_IRO_Emp_055
		EmployeeDicriptionA = findElementInXLSheet(getDataInvertoryAndWarehouse, "EmployeeDicriptionA");

		// IN_IRO_Emp_056
		EmployeeCodeAndDicriptionA = findElementInXLSheet(getDataInvertoryAndWarehouse, "EmployeeCodeAndDicriptionA");

		// IN_IRO_Emp_029
		ArunaIROA = findElementInXLSheet(getDataInvertoryAndWarehouse, "ArunaIROA");
		DilshanIROA = findElementInXLSheet(getDataInvertoryAndWarehouse, "DilshanIROA");
		
		/* Smoke Production */
		productGroupSmokeProduction = findElementInXLSheet(getDataInvertoryAndWarehouse, "productGroupSmokeProduction");
		quantityTen = findElementInXLSheet(getDataInvertoryAndWarehouse, "quantityTen");
		menufacturer = findElementInXLSheet(getDataInvertoryAndWarehouse, "menufacturer");
		menufacturerTypeProduction = findElementInXLSheet(getDataInvertoryAndWarehouse, "menufacturerTypeProduction");
		inputWarehouseSmokeProduction = findElementInXLSheet(getDataInvertoryAndWarehouse, "inputWarehouseSmokeProduction");
		
	}

}
