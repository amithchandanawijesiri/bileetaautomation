package bileeta.BATF.Pages;

import bileeta.BTAF.Utilities.TestBase;

public class FleetManagementModuleSmokeData extends TestBase {

	// Reading Element Locators to the variables
	// Login
		protected static String siteLogo;
		protected static String txt_username;
		protected static String txt_password;
		protected static String btn_login;
		protected static String lnk_home;
		
		protected static String btn_navigationmenu;
	    protected static String sidemenu;
	    protected static String btn_fleetmodule;
	    protected static String subsidemenu;
        protected static String fleetParameterMenu;
        protected static String fleetParameterHeader;
        
        protected static String vehicleMakeTab;
        protected static String vehicleMakeHeader;
        protected static String vehicleMakeAddBtn;
        protected static String vehicleMakeTxtBox;
		protected static String updateVehicleMakeParameter;
		
		protected static String vehicleModelMenu;
        protected static String vehicleModelConfigurationHeader;
        protected static String newVehicleModelBtn;
        protected static String vehicleModelHeader;
        protected static String vehicleModelDropDown;
        protected static String vehicleModelDescription;
        protected static String vehicleModel1;
        protected static String vehicleType1;
        protected static String damageUpload;
        protected static String addVehicleModelBtn;
        protected static String vehicleModel2;
        protected static String vehicleType2;
        protected static String updateVehicleModel;
        protected static String searchVehicleModel;
        protected static String activateVehicleModel;
        protected static String confirmationHeader;
        protected static String confirmationYes;
        
        protected static String packageInformationMenu;
        protected static String packageInformationHeader;
        protected static String newPackageBtn;
        protected static String newpackageHeader;
        protected static String packageCode;
        protected static String packageDes;
        protected static String accountGroup;
        protected static String rateType;
        protected static String rate;
        protected static String invoicingProductLookup;
        protected static String invoicingProductHeader;
        protected static String invoicingProductTxtBox;
        protected static String invoicingProductSearchBtn;
        protected static String selectInvoicingProduct;
        protected static String allottedMileage;
        protected static String extraMileageRate;
        protected static String extraMileageProductLookup;
        protected static String extraMileageProductHeader;
        protected static String extraMileageProductTxtBox;
        protected static String extraMileageProductSearchBtn;
        protected static String selectextraMileageProduct;
        protected static String allottedTime;
        protected static String extraTimeRate;
        protected static String extraTimeProductLookup;
        protected static String extraTimeProductHeader;
        protected static String extraTimeProductTxtBox;
        protected static String extraTimeProductSearchBtn;
        protected static String selectextraTimeProduct;
        protected static String withDriverFlag;
        protected static String labourProductLookup;
        protected static String labourProductHeader;
        protected static String labourProductTxtBox;
        protected static String labourProductSearchBtn;
        protected static String selectlabourProduct;
        protected static String packageDraft;
        protected static String packageDraftHeader;
        protected static String packageId;
        protected static String packageRelease;
        protected static String packageReleaseHeader;
        
        protected static String OrgMgtModule;
        protected static String VehicleInformationMenu;
        protected static String VIBypageHeader;
        protected static String NewVehicleBtn;
        protected static String newVehicleHeader;
        protected static String vehicleNo;
        protected static String vehicleDescription;
        protected static String vehicleGroup;
        protected static String vehicleLocation;
        protected static String fixedAssetLookup;
        protected static String fixedAssetHeader;
        protected static String fixedAssetSearch;
        protected static String selectFixedAsset;
        protected static String selectVehicleType;
        protected static String selectVehicleMake;
        protected static String selectVehicleModel;
        protected static String selectVehicleTransmission;
        protected static String selectFuelType;
        protected static String selectColor;
        protected static String draftVehicle;
        protected static String draftVehicleHeader;
        protected static String releaseVehicle;
        protected static String releaseVehicleHeader;
        
        protected static String gatePassMenu;
        protected static String gatePassByPageHeader;
        protected static String newGatePassBtn;
        protected static String newGatePassHeader;
        protected static String vgpPostDate;
        protected static String vgpPostDateHeader;
        protected static String vgpPostDateTxt;
        protected static String vgpPostDateSelection;
        protected static String postDateApply;
        protected static String purposeTxt;
        protected static String DesTxt;
        protected static String vehicleLookup;
        protected static String vehicleHeader;
        protected static String vehicleSearch;
        protected static String selectVehicle;
        protected static String driverLookup;
        protected static String driverHeader;
        protected static String driverSearch;
        protected static String selectDriver;
        protected static String vgpEstimatedCheckoutDate;
        protected static String vgpEstimatedCheckoutDateSelection;
        protected static String vgpEstimatedCheckoutTimeTxt;
        protected static String vgpEstimatedCheckoutTime;
        protected static String vgpEstimatedCheckoutTimeOk;
        protected static String vgpEstimatedCheckinDate;
        protected static String vgpEstimatedCheckinDateSelection;
        protected static String vgpEstimatedCheckinTimeTxt;
        protected static String vgpEstimatedCheckinTime;
        protected static String vgpEstimatedCheckinTimeOk;
        protected static String draftGatePass;
        protected static String draftGatepassStatus;
        protected static String releaseGatePass;
        protected static String releaseGatePassStatus;
        protected static String gatePassAction;
        protected static String actionCheckout;
        protected static String gatePassChekoutHeader;
        protected static String gatePassCheckoutDate;
        protected static String gatePassCheckoutTime;
        protected static String gatePassCheckoutMileage;
        protected static String gatePassCheckoutFuelLevel;
        protected static String gatePassCheckoutLocation;
        protected static String updateGatePassCheckout;
        protected static String actionCheckin;
        protected static String gatePassChekInHeader;
        protected static String gatePassCheckInDate;
        protected static String gatePassCheckInTime;
        protected static String gatePassCheckInMileage;
        protected static String gatePassCheckInFuelLevel;
        protected static String gatePassCheckInLocation;
        protected static String updateGatePassCheckIn;
        protected static String dateOK;
        
        
        
        protected static String rentAgreementMenu;
        protected static String rentAgreementByPageHeader;
        protected static String rentAgreementBtn;
        protected static String newRentAgreementHeader;
        protected static String accountLookup;
        protected static String accountHeader;
        protected static String accountSearch;
        protected static String selectAccount;
        protected static String selectContactPerson;
        protected static String raVehicleLookup;
        protected static String raVehicleHeader;
        protected static String raVehicleSearch;
        protected static String raSelectVehicle;
        protected static String raDes;
        protected static String raCheckoutDate;
        protected static String raCheckoutTime;
        protected static String raCheckOutTimeOk;
        protected static String raCheckInDate;
        protected static String raCheckInTime;
        protected static String raCheckInTimeOk;
        protected static String raSelectBillingType;
        protected static String raSelectRateType;
        protected static String raWithDriver;
        protected static String raPackageLookup;
        protected static String raPackageHeader;
        protected static String raPackageSearch;
        protected static String raSelectPackage;
        protected static String raEstimationTab;
        protected static String raMainDiscountPercentage;
        protected static String raMainTaxGroup;
        protected static String raChargesAnalzeCode1;
        protected static String raChargesProductLookup1;
        protected static String raChargesProductLookupHeader1;
        protected static String raChargesProductSearch1;
        protected static String raChargesSelectProduct1;
        protected static String raChargesFromDate1;
        protected static String raChargesToDate1;
        protected static String raChargesRate1;
        protected static String raChargesDiscountPercentage1;
        protected static String raChargesTaxGroup1;
        protected static String raAddChargeLine;
        protected static String raChargesAnalzeCode2;
        protected static String raChargesProductLookup2;
        protected static String raChargesProductLookupHeader2;
        protected static String raChargesProductSearch2;
        protected static String raChargesSelectProduct2;
        protected static String raChargesFromDate2;
        protected static String raChargesToDate2;
        protected static String raChargesRate2;
        protected static String raChargesDiscountPercentage2;
        protected static String raChargesTaxGroup2;
        protected static String raAccessoriesProductLookup1;
        protected static String raAccessoriesProductHeader1;
        protected static String raAccessoriesProductSearch1;
        protected static String raAccessoriesSelectProduct1;
        protected static String raAccessoriesFromDate1;
        protected static String raAccessoriesToDate1;
        protected static String raAccessoriesRate1;
        protected static String raAccessoriesDiscountPercentage1;
        protected static String raAccessoriesTaxGroup1;
        protected static String raAddAccessoriesLine;
        protected static String raAccessoriesProductLookup2;
        protected static String raAccessoriesProductHeader2;
        protected static String raAccessoriesProductSearch2;
        protected static String raAccessoriesSelectProduct2;
        protected static String raAccessoriesFromDate2;
        protected static String raAccessoriesToDate2;
        protected static String raAccessoriesRate2;
        protected static String raAccessoriesDiscountPercentage2;
        protected static String raAccessoriesTaxGroup2;
        protected static String raCheckoutBtn;
        protected static String raDraft;
        protected static String raDraftHeader;
        protected static String raRelease;
        protected static String raReleaseHeader;
        protected static String raConfirmVersion;
        protected static String raVerionBtn;
        protected static String raCofirmationHeader;
        protected static String raConfirmBtn;
        protected static String raConfirmedStatus;







	
     
	/* Reading the Data to variables */
	// Login
		protected static String siteURL;
		protected static String userNameData;
		protected static String passwordData;
		protected static String vehicleMakeTxtVal;
		protected static String vehicleModelDescriptionVal;
		protected static String vehicleModelVal1;
		protected static String vehicleTypeVal1;
		protected static String vehicleModelVal2;
		protected static String vehicleTypeVal2;

		
		protected static String packageCodeVal;
		protected static String packageDesVal;
		protected static String accountGroupVal;
		protected static String rateTypeVal;
		protected static String rateVal;
		protected static String invoicingProductVal;
		protected static String allottedMileageVal;
		protected static String extraMileageRateVal;
		protected static String extraMileageProductVal;
		protected static String allottedTimeVal;
		protected static String extraTimeRateval;
		protected static String extraTimeProductVal;
		protected static String labourProductVal;
		
		protected static String vehicleNoVal;
		protected static String vehicleDescriptionVal;
		protected static String vehicleGroupVal;
		protected static String vehicleLocationVal;
		protected static String fixedAssetSearchVal;
		protected static String vehicleTypeVal;
		protected static String selectVehicleTransmissionVal;
		protected static String selectFuelTypeVal;
		protected static String selectColorVal;
		
		protected static String purposeTxtVal;
		protected static String desTxtVal;
		protected static String driverSearchVal;
		protected static String gatePassCheckoutMileageVal;
		protected static String gatePassCheckInMileageVal;
		
		protected static String accountSearchVal;
		protected static String raDesval;
		protected static String raTaxGroupVal;
		protected static String raChargesProductSearchVal1;
		protected static String raChargesProductSearchVal2;
		protected static String raAccessoriesProductSearchVal1;
		protected static String raAccessoriesProductSearchVal2;





		
	public static void readElementlocators() throws Exception
	{
		// Login
		siteLogo = findElementInXLSheet(getParameterFleetSmoke, "site logo");
		txt_username = findElementInXLSheet(getParameterFleetSmoke, "user name");
		txt_password = findElementInXLSheet(getParameterFleetSmoke, "password");
		btn_login = findElementInXLSheet(getParameterFleetSmoke, "login button");
		lnk_home = findElementInXLSheet(getParameterFleetSmoke, "home link");	
		
		btn_navigationmenu= findElementInXLSheet(getParameterFleetSmoke, "btn_navigation");
		sidemenu= findElementInXLSheet(getParameterFleetSmoke,"sidemenu");
		btn_fleetmodule= findElementInXLSheet(getParameterFleetSmoke,"btn_fleet_module");
		subsidemenu= findElementInXLSheet(getParameterFleetSmoke,"subsidemenu");
		fleetParameterMenu= findElementInXLSheet(getParameterFleetSmoke,"fleet parameter menu");
		fleetParameterHeader= findElementInXLSheet(getParameterFleetSmoke,"fleet parameter header");
		
		vehicleMakeTab= findElementInXLSheet(getParameterFleetSmoke,"vehicleMakeTab");
		vehicleMakeHeader= findElementInXLSheet(getParameterFleetSmoke,"vehicleMakeHeader");
		vehicleMakeAddBtn= findElementInXLSheet(getParameterFleetSmoke,"vehicleMakeAddBtn");
		vehicleMakeTxtBox= findElementInXLSheet(getParameterFleetSmoke,"vehicleMakeAddTextBox");
		updateVehicleMakeParameter=findElementInXLSheet(getParameterFleetSmoke,"updateVehicleMakeParameter");
		
		vehicleModelMenu=findElementInXLSheet(getParameterFleetSmoke,"vehicle Model menu");
		vehicleModelConfigurationHeader=findElementInXLSheet(getParameterFleetSmoke,"vehicle Model Configuration header");
		newVehicleModelBtn=findElementInXLSheet(getParameterFleetSmoke,"new Vehicle Model Btn");
		vehicleModelHeader=findElementInXLSheet(getParameterFleetSmoke,"new Vehicle Model Header");
		vehicleModelDropDown=findElementInXLSheet(getParameterFleetSmoke,"vehicle model drop down");
		vehicleModelDescription=findElementInXLSheet(getParameterFleetSmoke,"VM Description");
		vehicleModel1=findElementInXLSheet(getParameterFleetSmoke,"vehicleModel1");
		vehicleType1=findElementInXLSheet(getParameterFleetSmoke,"vehicleType1");
		damageUpload=findElementInXLSheet(getParameterFleetSmoke,"damageUpload");
		addVehicleModelBtn=findElementInXLSheet(getParameterFleetSmoke,"addVehicleModelBtn");
		vehicleModel2=findElementInXLSheet(getParameterFleetSmoke,"vehicleModel2");
		vehicleType2=findElementInXLSheet(getParameterFleetSmoke,"vehicleType2");
		updateVehicleModel=findElementInXLSheet(getParameterFleetSmoke,"updateVehicleModel");
		searchVehicleModel=findElementInXLSheet(getParameterFleetSmoke,"searchVehicleModel");
		activateVehicleModel=findElementInXLSheet(getParameterFleetSmoke,"activateVehicleModel");
		confirmationHeader=findElementInXLSheet(getParameterFleetSmoke,"confirmationHeader");
		confirmationYes=findElementInXLSheet(getParameterFleetSmoke,"confirmationYes");
		
		packageInformationMenu=findElementInXLSheet(getParameterFleetSmoke,"Package Information menu");
		packageInformationHeader=findElementInXLSheet(getParameterFleetSmoke,"package information header");
		newPackageBtn=findElementInXLSheet(getParameterFleetSmoke,"new package btn");
		newpackageHeader=findElementInXLSheet(getParameterFleetSmoke,"new package information header");
		packageCode=findElementInXLSheet(getParameterFleetSmoke,"package code");
		packageDes=findElementInXLSheet(getParameterFleetSmoke,"package description");
		accountGroup=findElementInXLSheet(getParameterFleetSmoke,"account group");
		rateType=findElementInXLSheet(getParameterFleetSmoke,"rate type");
		rate=findElementInXLSheet(getParameterFleetSmoke,"rate");
		invoicingProductLookup=findElementInXLSheet(getParameterFleetSmoke,"invoice product lookup");
		invoicingProductHeader=findElementInXLSheet(getParameterFleetSmoke,"invoice product lookup header");
		invoicingProductTxtBox=findElementInXLSheet(getParameterFleetSmoke,"invoice product txt");
		invoicingProductSearchBtn=findElementInXLSheet(getParameterFleetSmoke,"invoice product search btn");
		selectInvoicingProduct=findElementInXLSheet(getParameterFleetSmoke,"select invoice product");
		allottedMileage=findElementInXLSheet(getParameterFleetSmoke,"allotted mileage");
		extraMileageRate=findElementInXLSheet(getParameterFleetSmoke,"extra mileage rate");
		extraMileageProductLookup=findElementInXLSheet(getParameterFleetSmoke,"extra mileage product lookup");
		extraMileageProductHeader=findElementInXLSheet(getParameterFleetSmoke,"extra mileage product lookup header");
		extraMileageProductTxtBox=findElementInXLSheet(getParameterFleetSmoke,"extra mileage product txt");
		extraMileageProductSearchBtn=findElementInXLSheet(getParameterFleetSmoke,"extra mileage product search btn");
		selectextraMileageProduct=findElementInXLSheet(getParameterFleetSmoke,"select extra mileage product");
		
		allottedTime=findElementInXLSheet(getParameterFleetSmoke,"allotted time");
	    extraTimeRate=findElementInXLSheet(getParameterFleetSmoke,"extra time rate");
	    extraTimeProductLookup=findElementInXLSheet(getParameterFleetSmoke,"extra time product lookup");
	    extraTimeProductHeader=findElementInXLSheet(getParameterFleetSmoke,"extra time product lookup header");
	    extraTimeProductTxtBox=findElementInXLSheet(getParameterFleetSmoke,"extra time product txt");
	    extraTimeProductSearchBtn=findElementInXLSheet(getParameterFleetSmoke,"extra time product search btn");
	    selectextraTimeProduct=findElementInXLSheet(getParameterFleetSmoke,"select extra time product");
	    withDriverFlag=findElementInXLSheet(getParameterFleetSmoke,"with labour flag");
	    labourProductLookup=findElementInXLSheet(getParameterFleetSmoke,"labour product lookup");
	    labourProductHeader=findElementInXLSheet(getParameterFleetSmoke,"labour product lookup header");
	    labourProductTxtBox=findElementInXLSheet(getParameterFleetSmoke,"labour product txt");
	    labourProductSearchBtn=findElementInXLSheet(getParameterFleetSmoke,"labour product search btn");
	    selectlabourProduct=findElementInXLSheet(getParameterFleetSmoke,"select labour product");
	    
	    packageDraft=findElementInXLSheet(getParameterFleetSmoke,"draft package");
	    packageId=findElementInXLSheet(getParameterFleetSmoke,"package Id");
	    packageDraftHeader=findElementInXLSheet(getParameterFleetSmoke,"draft header");
	    packageRelease=findElementInXLSheet(getParameterFleetSmoke,"release package");
	    packageReleaseHeader=findElementInXLSheet(getParameterFleetSmoke,"release header");
	    
	    OrgMgtModule=findElementInXLSheet(getParameterFleetSmoke,"OrgMgtModule");
	    VehicleInformationMenu=findElementInXLSheet(getParameterFleetSmoke,"VehicleInformationMenu");
	    VIBypageHeader=findElementInXLSheet(getParameterFleetSmoke,"VIBypageHeader");
	    NewVehicleBtn=findElementInXLSheet(getParameterFleetSmoke,"NewVehicleBtn");
	    newVehicleHeader=findElementInXLSheet(getParameterFleetSmoke,"newVehicleHeader");
	    vehicleNo=findElementInXLSheet(getParameterFleetSmoke,"vehicleNo");
	    vehicleDescription=findElementInXLSheet(getParameterFleetSmoke,"vehicleDescription");
	    vehicleGroup = findElementInXLSheet(getParameterFleetSmoke,"vehicleGroup");
	    vehicleLocation=findElementInXLSheet(getParameterFleetSmoke,"vehicleLocation");
	    fixedAssetLookup=findElementInXLSheet(getParameterFleetSmoke,"fixedAssetLookup");
	    fixedAssetHeader=findElementInXLSheet(getParameterFleetSmoke,"fixedAssetHeader");
	    fixedAssetSearch=findElementInXLSheet(getParameterFleetSmoke,"fixedAssetSearch");
	    selectFixedAsset=findElementInXLSheet(getParameterFleetSmoke,"selectFixedAsset");
	    selectVehicleType=findElementInXLSheet(getParameterFleetSmoke,"selectVehicleType");
	    selectVehicleMake=findElementInXLSheet(getParameterFleetSmoke,"selectVehicleMake");
	    selectVehicleModel=findElementInXLSheet(getParameterFleetSmoke,"selectVehicleModel");
	    selectVehicleTransmission=findElementInXLSheet(getParameterFleetSmoke,"selectVehicleTransmission");
	    selectFuelType=findElementInXLSheet(getParameterFleetSmoke,"selectFuelType");
	    selectColor=findElementInXLSheet(getParameterFleetSmoke,"selectColor");
	    draftVehicle=findElementInXLSheet(getParameterFleetSmoke,"draftVehicle");
	    draftVehicleHeader=findElementInXLSheet(getParameterFleetSmoke,"draftVehicleHeader");
	    releaseVehicle=findElementInXLSheet(getParameterFleetSmoke,"releaseVehicle");
	    releaseVehicleHeader=findElementInXLSheet(getParameterFleetSmoke,"releaseVehicleHeader");
	    
	    
	    gatePassMenu=findElementInXLSheet(getParameterFleetSmoke,"gatePassMenu");
	    gatePassByPageHeader=findElementInXLSheet(getParameterFleetSmoke,"gatePassByPageHeader");
	    newGatePassBtn=findElementInXLSheet(getParameterFleetSmoke,"newGatePassBtn");
	    newGatePassHeader=findElementInXLSheet(getParameterFleetSmoke,"newGatePassHeader");
	    vgpPostDate=findElementInXLSheet(getParameterFleetSmoke,"vgpPostDate");
	    vgpPostDateHeader=findElementInXLSheet(getParameterFleetSmoke,"vgpPostDateHeader");
	    vgpPostDateTxt=findElementInXLSheet(getParameterFleetSmoke,"vgpPostDateTxt");
	    vgpPostDateSelection=findElementInXLSheet(getParameterFleetSmoke,"vgpPostDateSelection");
	    postDateApply = findElementInXLSheet(getParameterFleetSmoke,"postDateApply");
	    purposeTxt=findElementInXLSheet(getParameterFleetSmoke,"purposeTxt");
	    DesTxt=findElementInXLSheet(getParameterFleetSmoke,"DesTxt");
	    vehicleLookup=findElementInXLSheet(getParameterFleetSmoke,"vehicleLookup");
	    vehicleHeader=findElementInXLSheet(getParameterFleetSmoke,"vehicleHeader");
	    vehicleSearch=findElementInXLSheet(getParameterFleetSmoke,"vehicleSearch");
	    selectVehicle=findElementInXLSheet(getParameterFleetSmoke,"selectVehicle");
	    driverLookup=findElementInXLSheet(getParameterFleetSmoke,"driverLookup");
	    driverHeader=findElementInXLSheet(getParameterFleetSmoke,"driverHeader");
	    driverSearch=findElementInXLSheet(getParameterFleetSmoke,"driverSearch");
	    selectDriver=findElementInXLSheet(getParameterFleetSmoke,"selectDriver");
	    vgpEstimatedCheckoutDate=findElementInXLSheet(getParameterFleetSmoke,"vgpEstimatedCheckoutDate");
	    vgpEstimatedCheckoutDateSelection=findElementInXLSheet(getParameterFleetSmoke,"vgpEstimatedCheckoutDateSelection");
	    vgpEstimatedCheckoutTimeTxt=findElementInXLSheet(getParameterFleetSmoke,"vgpEstimatedCheckoutTimeTxt");
	    vgpEstimatedCheckoutTime=findElementInXLSheet(getParameterFleetSmoke,"vgpEstimatedCheckoutTime");
	    vgpEstimatedCheckoutTimeOk=findElementInXLSheet(getParameterFleetSmoke,"vgpEstimatedCheckoutTimeOk");
	    vgpEstimatedCheckinDate=findElementInXLSheet(getParameterFleetSmoke,"vgpEstimatedCheckinDate");
	    vgpEstimatedCheckinDateSelection=findElementInXLSheet(getParameterFleetSmoke,"vgpEstimatedCheckinDateSelection");
	    vgpEstimatedCheckinTimeTxt=findElementInXLSheet(getParameterFleetSmoke,"vgpEstimatedCheckinTimeTxt");
	    vgpEstimatedCheckinTime=findElementInXLSheet(getParameterFleetSmoke,"vgpEstimatedCheckinTime");
	    vgpEstimatedCheckinTimeOk=findElementInXLSheet(getParameterFleetSmoke,"vgpEstimatedCheckinTimeOk");
	    draftGatePass=findElementInXLSheet(getParameterFleetSmoke,"draftGatePass");
	    draftGatepassStatus=findElementInXLSheet(getParameterFleetSmoke,"draftGatepassStatus");
	    releaseGatePass=findElementInXLSheet(getParameterFleetSmoke,"releaseGatePass");
	    releaseGatePassStatus=findElementInXLSheet(getParameterFleetSmoke,"releaseGatePassStatus");
	    gatePassAction=findElementInXLSheet(getParameterFleetSmoke,"gatePassAction");
	    actionCheckout=findElementInXLSheet(getParameterFleetSmoke,"actionCheckout");
	    gatePassChekoutHeader=findElementInXLSheet(getParameterFleetSmoke,"gatePassChekoutHeader");
	    gatePassCheckoutDate=findElementInXLSheet(getParameterFleetSmoke,"gatePassCheckoutDate");
	    gatePassCheckoutTime=findElementInXLSheet(getParameterFleetSmoke,"gatePassCheckoutTime");
	    gatePassCheckoutMileage=findElementInXLSheet(getParameterFleetSmoke,"gatePassCheckoutMileage");
	    gatePassCheckoutFuelLevel=findElementInXLSheet(getParameterFleetSmoke,"gatePassCheckoutFuelLevel");
	    gatePassCheckoutLocation=findElementInXLSheet(getParameterFleetSmoke,"gatePassCheckoutLocation");
	    updateGatePassCheckout=findElementInXLSheet(getParameterFleetSmoke,"updateGatePassCheckout");
	    actionCheckin=findElementInXLSheet(getParameterFleetSmoke,"actionCheckin");
	    gatePassChekInHeader=findElementInXLSheet(getParameterFleetSmoke,"gatePassChekInHeader");
	    gatePassCheckInDate=findElementInXLSheet(getParameterFleetSmoke,"gatePassCheckInDate");
	    gatePassCheckInTime=findElementInXLSheet(getParameterFleetSmoke,"gatePassCheckInTime");
	    gatePassCheckInMileage=findElementInXLSheet(getParameterFleetSmoke,"gatePassCheckInMileage");
	    gatePassCheckInFuelLevel=findElementInXLSheet(getParameterFleetSmoke,"gatePassCheckInFuelLevel");
	    gatePassCheckInLocation=findElementInXLSheet(getParameterFleetSmoke,"gatePassCheckInLocation");
	    updateGatePassCheckIn=findElementInXLSheet(getParameterFleetSmoke,"updateGatePassCheckIn");
	    dateOK=findElementInXLSheet(getParameterFleetSmoke,"dateOK");    
	    
	    rentAgreementMenu=findElementInXLSheet(getParameterFleetSmoke,"rentAgreementMenu");
	    rentAgreementByPageHeader=findElementInXLSheet(getParameterFleetSmoke,"rentAgreementByPageHeader");
	    rentAgreementBtn=findElementInXLSheet(getParameterFleetSmoke,"rentAgreementBtn");
	    newRentAgreementHeader=findElementInXLSheet(getParameterFleetSmoke,"newRentAgreementHeader");
	    
	    accountLookup=findElementInXLSheet(getParameterFleetSmoke,"accountLookup");
	    accountHeader=findElementInXLSheet(getParameterFleetSmoke,"accountHeader");
	    accountSearch=findElementInXLSheet(getParameterFleetSmoke,"accountSearch");
	    selectAccount=findElementInXLSheet(getParameterFleetSmoke,"selectAccount");
	    selectContactPerson=findElementInXLSheet(getParameterFleetSmoke,"selectContactPerson");
	    
	    raVehicleLookup=findElementInXLSheet(getParameterFleetSmoke,"raVehicleLookup");
	    raVehicleHeader=findElementInXLSheet(getParameterFleetSmoke,"raVehicleHeader");
	    raVehicleSearch=findElementInXLSheet(getParameterFleetSmoke,"raVehicleSearch");
	    raSelectVehicle=findElementInXLSheet(getParameterFleetSmoke,"raSelectVehicle");
	    
	    raDes=findElementInXLSheet(getParameterFleetSmoke,"raDes");
	    raCheckoutDate=findElementInXLSheet(getParameterFleetSmoke,"raCheckoutDate");
	    raCheckoutTime=findElementInXLSheet(getParameterFleetSmoke,"raCheckoutTime");
	    raCheckOutTimeOk=findElementInXLSheet(getParameterFleetSmoke,"raCheckOutTimeOk");
	    raCheckInDate=findElementInXLSheet(getParameterFleetSmoke,"raCheckInDate");
	    raCheckInTime=findElementInXLSheet(getParameterFleetSmoke,"raCheckInTime");
	    raCheckInTimeOk=findElementInXLSheet(getParameterFleetSmoke,"raCheckInTimeOk");
	    raSelectBillingType=findElementInXLSheet(getParameterFleetSmoke,"raSelectBillingType");
	    raSelectRateType=findElementInXLSheet(getParameterFleetSmoke,"raSelectRateType");
	    
	    raWithDriver=findElementInXLSheet(getParameterFleetSmoke,"raWithDriver");
	    raPackageLookup=findElementInXLSheet(getParameterFleetSmoke,"raPackageLookup");
	    raPackageHeader=findElementInXLSheet(getParameterFleetSmoke,"raPackageHeader");
	    raPackageSearch=findElementInXLSheet(getParameterFleetSmoke,"raPackageSearch");
	    raSelectPackage=findElementInXLSheet(getParameterFleetSmoke,"raSelectPackage");
	    
	    raEstimationTab=findElementInXLSheet(getParameterFleetSmoke,"raEstimationTab");
	    raMainDiscountPercentage=findElementInXLSheet(getParameterFleetSmoke,"raMainDiscountPercentage");
	    raMainTaxGroup=findElementInXLSheet(getParameterFleetSmoke,"raMainTaxGroup");
	    
	    raChargesAnalzeCode1=findElementInXLSheet(getParameterFleetSmoke,"raChargesAnalzeCode1");
	    raChargesProductLookup1=findElementInXLSheet(getParameterFleetSmoke,"raChargesProductLookup1");
	    raChargesProductLookupHeader1=findElementInXLSheet(getParameterFleetSmoke,"raChargesProductLookupHeader1");
	    raChargesProductSearch1=findElementInXLSheet(getParameterFleetSmoke,"raChargesProductSearch1");
	    raChargesSelectProduct1=findElementInXLSheet(getParameterFleetSmoke,"raChargesSelectProduct1");
	    raChargesFromDate1=findElementInXLSheet(getParameterFleetSmoke,"raChargesFromDate1");
	    raChargesToDate1=findElementInXLSheet(getParameterFleetSmoke,"raChargesToDate1");
	    raChargesRate1=findElementInXLSheet(getParameterFleetSmoke,"raChargesRate1");
	    raChargesDiscountPercentage1=findElementInXLSheet(getParameterFleetSmoke,"raChargesDiscountPercentage1");
	    raChargesTaxGroup1=findElementInXLSheet(getParameterFleetSmoke,"raChargesTaxGroup1");
	    raAddChargeLine=findElementInXLSheet(getParameterFleetSmoke,"raAddChargeLine");
	    
	    raChargesAnalzeCode2=findElementInXLSheet(getParameterFleetSmoke,"raChargesAnalzeCode2");
	    raChargesProductLookup2=findElementInXLSheet(getParameterFleetSmoke,"raChargesProductLookup2");
	    raChargesProductLookupHeader2=findElementInXLSheet(getParameterFleetSmoke,"raChargesProductLookupHeader2");
	    raChargesProductSearch2=findElementInXLSheet(getParameterFleetSmoke,"raChargesProductSearch2");
	    raChargesSelectProduct2=findElementInXLSheet(getParameterFleetSmoke,"raChargesSelectProduct2");
	    raChargesFromDate2=findElementInXLSheet(getParameterFleetSmoke,"raChargesFromDate2");
	    raChargesToDate2=findElementInXLSheet(getParameterFleetSmoke,"raChargesToDate2");
	    raChargesRate2=findElementInXLSheet(getParameterFleetSmoke,"raChargesRate2");
	    raChargesDiscountPercentage2=findElementInXLSheet(getParameterFleetSmoke,"raChargesDiscountPercentage2");
	    raChargesTaxGroup2=findElementInXLSheet(getParameterFleetSmoke,"raChargesTaxGroup2");
	    
	    raAccessoriesProductLookup1=findElementInXLSheet(getParameterFleetSmoke,"raAccessoriesProductLookup1");
	    raAccessoriesProductHeader1=findElementInXLSheet(getParameterFleetSmoke,"raAccessoriesProductHeader1");
	    raAccessoriesProductSearch1=findElementInXLSheet(getParameterFleetSmoke,"raAccessoriesProductSearch1");
	    raAccessoriesSelectProduct1=findElementInXLSheet(getParameterFleetSmoke,"raAccessoriesSelectProduct1");
	    raAccessoriesFromDate1=findElementInXLSheet(getParameterFleetSmoke,"raAccessoriesFromDate1");
	    raAccessoriesToDate1=findElementInXLSheet(getParameterFleetSmoke,"raAccessoriesToDate1");
	    raAccessoriesRate1=findElementInXLSheet(getParameterFleetSmoke,"raAccessoriesRate1");
	    raAccessoriesDiscountPercentage1=findElementInXLSheet(getParameterFleetSmoke,"raAccessoriesDiscountPercentage1");
	    raAccessoriesTaxGroup1=findElementInXLSheet(getParameterFleetSmoke,"raAccessoriesTaxGroup1");
	    raAddAccessoriesLine=findElementInXLSheet(getParameterFleetSmoke,"raAddAccessoriesLine");
	    
	    raAccessoriesProductLookup2=findElementInXLSheet(getParameterFleetSmoke,"raAccessoriesProductLookup2");
	    raAccessoriesProductHeader2=findElementInXLSheet(getParameterFleetSmoke,"raAccessoriesProductHeader2");
	    raAccessoriesProductSearch2=findElementInXLSheet(getParameterFleetSmoke,"raAccessoriesProductSearch2");
	    raAccessoriesSelectProduct2=findElementInXLSheet(getParameterFleetSmoke,"raAccessoriesSelectProduct2");
	    raAccessoriesFromDate2=findElementInXLSheet(getParameterFleetSmoke,"raAccessoriesFromDate2");
	    raAccessoriesToDate2=findElementInXLSheet(getParameterFleetSmoke,"raAccessoriesToDate2");
	    raAccessoriesRate2=findElementInXLSheet(getParameterFleetSmoke,"raAccessoriesRate2");
	    raAccessoriesDiscountPercentage2=findElementInXLSheet(getParameterFleetSmoke,"raAccessoriesDiscountPercentage2");
	    raAccessoriesTaxGroup2=findElementInXLSheet(getParameterFleetSmoke,"raAccessoriesTaxGroup2");
	    
	    raCheckoutBtn=findElementInXLSheet(getParameterFleetSmoke,"raCheckoutBtn");
	    raDraft=findElementInXLSheet(getParameterFleetSmoke,"raDraft");
	    raDraftHeader=findElementInXLSheet(getParameterFleetSmoke,"raDraftHeader");
	    raRelease=findElementInXLSheet(getParameterFleetSmoke,"raRelease");
	    raReleaseHeader=findElementInXLSheet(getParameterFleetSmoke,"raReleaseHeader");
	    
	    raConfirmVersion=findElementInXLSheet(getParameterFleetSmoke,"raConfirmVersion");
	    raVerionBtn=findElementInXLSheet(getParameterFleetSmoke,"raVerionBtn");
	    raCofirmationHeader=findElementInXLSheet(getParameterFleetSmoke,"raCofirmationHeader");
	    raConfirmBtn=findElementInXLSheet(getParameterFleetSmoke,"raConfirmBtn");
	    raConfirmedStatus=findElementInXLSheet(getParameterFleetSmoke,"raConfirmedStatus");


	    
	}
	

	
	public static void readData() throws Exception
	{
		// Login
		siteURL = findElementInXLSheet(getDataFleetSmoke, "site url");
		userNameData = findElementInXLSheet(getDataFleetSmoke, "user name data");
		passwordData = findElementInXLSheet(getDataFleetSmoke, "password data");
		
		vehicleMakeTxtVal=findElementInXLSheet(getDataFleetSmoke, "vehicle Make data");
		vehicleModelDescriptionVal=findElementInXLSheet(getDataFleetSmoke, "VM Description data");
		vehicleModelVal1=findElementInXLSheet(getDataFleetSmoke, "vehicleModelVal1");
		vehicleTypeVal1=findElementInXLSheet(getDataFleetSmoke, "vehicleTypeVal1");
		vehicleModelVal2=findElementInXLSheet(getDataFleetSmoke, "vehicleModelVal2");
		vehicleTypeVal2=findElementInXLSheet(getDataFleetSmoke, "vehicleTypeVal2");

		
		packageCodeVal=findElementInXLSheet(getDataFleetSmoke, "package code val");
		packageDesVal=findElementInXLSheet(getDataFleetSmoke, "package description val");
		accountGroupVal=findElementInXLSheet(getDataFleetSmoke, "account group val");
		rateTypeVal=findElementInXLSheet(getDataFleetSmoke, "rate type val");
		rateVal=findElementInXLSheet(getDataFleetSmoke, "rate val");
		invoicingProductVal=findElementInXLSheet(getDataFleetSmoke, "invoicing product val");
		allottedMileageVal=findElementInXLSheet(getDataFleetSmoke, "allotted mileage val");
		extraMileageRateVal=findElementInXLSheet(getDataFleetSmoke, "extra mileage rate val");
		extraMileageProductVal=findElementInXLSheet(getDataFleetSmoke, "extra mileage product val");
		allottedTimeVal=findElementInXLSheet(getDataFleetSmoke, "allotted time val");
		extraTimeRateval=findElementInXLSheet(getDataFleetSmoke, "extra time rate val");
		extraTimeProductVal=findElementInXLSheet(getDataFleetSmoke, "extra time product val");
		labourProductVal=findElementInXLSheet(getDataFleetSmoke, "labour product val");
		
		vehicleNoVal=findElementInXLSheet(getDataFleetSmoke, "vehicleNoVal");
		vehicleDescriptionVal=findElementInXLSheet(getDataFleetSmoke, "vehicleDescriptionVal");
		vehicleGroupVal=findElementInXLSheet(getDataFleetSmoke, "vehicleGroupVal");
		vehicleLocationVal=findElementInXLSheet(getDataFleetSmoke, "vehicleLocationVal");
		fixedAssetSearchVal=findElementInXLSheet(getDataFleetSmoke, "fixedAssetSearchVal");
		selectVehicleTransmissionVal=findElementInXLSheet(getDataFleetSmoke, "selectVehicleTransmissionVal");
		selectFuelTypeVal=findElementInXLSheet(getDataFleetSmoke, "selectFuelTypeVal");
		selectColorVal=findElementInXLSheet(getDataFleetSmoke, "selectColorVal");
		vehicleTypeVal=findElementInXLSheet(getDataFleetSmoke, "vehicleTypeVal");
		
		
		purposeTxtVal=findElementInXLSheet(getDataFleetSmoke, "purposeTxtVal");
		desTxtVal=findElementInXLSheet(getDataFleetSmoke, "desTxtVal");
		driverSearchVal=findElementInXLSheet(getDataFleetSmoke, "driverSearchVal");
		gatePassCheckoutMileageVal=findElementInXLSheet(getDataFleetSmoke, "gatePassCheckoutMileageVal");
		gatePassCheckInMileageVal=findElementInXLSheet(getDataFleetSmoke, "gatePassCheckInMileageVal");
		
		accountSearchVal=findElementInXLSheet(getDataFleetSmoke, "accountSearchVal");
		raDesval=findElementInXLSheet(getDataFleetSmoke, "raDesval");
		raTaxGroupVal=findElementInXLSheet(getDataFleetSmoke, "raTaxGroupVal");
		raChargesProductSearchVal1=findElementInXLSheet(getDataFleetSmoke, "raChargesProductSearchVal1");
		raChargesProductSearchVal2=findElementInXLSheet(getDataFleetSmoke, "raChargesProductSearchVal2");
		raAccessoriesProductSearchVal1=findElementInXLSheet(getDataFleetSmoke, "raAccessoriesProductSearchVal1");
		raAccessoriesProductSearchVal2=findElementInXLSheet(getDataFleetSmoke, "raAccessoriesProductSearchVal2");




	}
	


}
