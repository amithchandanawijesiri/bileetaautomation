package bileeta.BATF.Pages;

import bileeta.BTAF.Utilities.TestBase;

public class FinanceModuleData extends TestBase {

	/* Reading the element locators to variables */
	protected static String site_logo;
	protected static String txt_username;
	protected static String txt_password;
	protected static String btn_login;
	protected static String link_home;
	protected static String div_loginVerification;

	/* Com_TC_002 */
	protected static String navigation_pane;
	protected static String btn_financeModule;
	protected static String btn_bulkChecqu;

	/* Fin_TC_001 */
	protected static String btn_inboundPaymentAdvice;
	protected static String btn_newInboundPaymentAdvice;
	protected static String btn_CusAdvanceJourneyIPA;
	protected static String lnl_postBusinessUnitIPA;
	protected static String drop_postBusinessUnit;
	protected static String btn_documentDataIPA;
	protected static String btn_docDateIPA;
	protected static String btn_postDateIPA;
	protected static String btn_dueDateIPA;
	protected static String btn_searchCustomerIPA;
	protected static String txt_cusAccountSearchIPA;
	protected static String lnk_resultCustomerAccountIPA;
	protected static String txt_refNoIPA;
	protected static String txt_descIPA;
	protected static String drop_cusrencyIPA;
	protected static String drop_taxIPA;
	protected static String txt_amountIPA;
	protected static String btn_calenderBackIAP;
	protected static String btn_calenderForwardIAP;
	protected static String tab_subbaryIPA;
	protected static String btn_checkout;
	public static String btn_draft;
	public static String btn_reelese;
	protected static String btn_print;
	protected static String lbl_docNoIPA;

	/* Fin_TC_002 */
	protected static String btn_searchGlCRV;
	protected static String txt_amountCustomerDebirMemoCRV;
	protected static String txt_secrhGlCRV;
	protected static String lnk_resultGL;
	protected static String btn_actionCRV;
	protected static String btn_converToInboundPaymentCRV;
	protected static String drop_paidCurrencyCRV;
	protected static String drop_paymentMethodCRV;
	protected static String drop_bankNAmeCRV;
	protected static String txt_checkNoCRV;
	protected static String plicker_dateCheckDateCRV;
	protected static String btn_detailTabCRV;
	protected static String drop_filterCurrencyCRV;
	protected static String txt_payingAmountCRV;
	protected static String btn_viewAllPendings;
	protected static String chk_adviceCRV;
	protected static String txt_paidAmountCRV;
	protected static String btn_journel;
	protected static String btn_cusDebitMemo;
	protected static String btn_summaryTab;
	protected static String btn_documentDataCRV;
	protected static String header_journelEntry;
	protected static String btn_viewPaymentFinTC002;
	protected static String lnk_paymentNoInboundPayemnt;

	/* Fin_TC_003 */
	protected static String btn_sallesMarketingModule;
	protected static String btn_salesInvoice;
	protected static String btn_newSalesInvoice;
	protected static String btn_slaesInvoiceServiceJourney;
	protected static String btn_customerSearchSalesInvoice;
	protected static String drop_currencySlaesInvoise;
	protected static String drop_salesUnitSalesIvoice;
	protected static String btn_productLockupSalesInvoice;
	protected static String txt_productSearch;
	protected static String lnk_resultProduct;
	protected static String txt_unitPriceSalesInvoice;
	protected static String btn_docFlowInboundPaymentAdvice;
	protected static String header_inboundPaymentAdviceSalesInvoice;

	/* Fin_TC_004 */
	protected static String btn_bankDeposit;
	protected static String lbl_InboundPaymentDocNo;
	protected static String btn_newBankDeposit;
	protected static String drop_paymentMethod;
	protected static String drop_bankNameBD;
	protected static String drop_accountNo;
	protected static String btn_viewPaymentBD;
	protected static String btn_rowPaymentBD;
	protected static String chk_paymentBD;

	/* Fin_TC_005 */
	protected static String txt_searchSalesInvoice;
	protected static String lnk_resultDocument;
	protected static String btn_inboundPaymentAdviceDocFlow;
	protected static String btn_setOffActionmenu;
	protected static String chk_setOffInvoiceDocNumber;
	protected static String btn_setOffWindowSetOffButton;
	protected static String txt_adviceDueAmountAfterSetOffSAlesInvoiceInboundPaymentAdvice;
	protected static String chk_inboundPaymentAdviceInboundPayment;

	/* Fin_TC_006 */
	protected static String btn_customerDebitMemoJourney;
	protected static String btn_nosCostAllocationDebitMemo;
	protected static String drop_costCenterCustomerDebitMemo;
	protected static String btn_addRowCostCenterCustomerDebitMemo;
	protected static String btn_updateCostCenterCustomerDebitMemo;
	protected static String txt_amountCustomerDebirMemo6;
	protected static String txt_costAllocationPresentageCustomerDebitMemo;

	/* Fin_TC_007 */
	protected static String btn_customerReciptVoucherJourneyboundPAyment;
	protected static String btn_customerAccountSearchCustomerReciptVoucher;
	protected static String btn_setOffNosCRV;
	protected static String chk_InboundPaymentCRV7;
	protected static String chk_InboundPaymentCRV78;
	protected static String lbl_baseAmountCRVSetOff;
	protected static String lbl_dueAmountCRVSetOff;
	protected static String lbl_dueAccordingAdvice7;
	protected static String txt_painAmountSetOffCRV;
	protected static String btn_closeSetOffPopUp;
	protected static String txt_paidAmountCRV7;
	protected static String txt_paidAmountCRVAfterHundredRecords;
	protected static String btn_inboundPayment;
	protected static String btn_newInboundPayment;
	protected static String header_inboundPaymentPage;
	protected static String tab_summaryCRV7;
	protected static String txt_billingAddressCRV;
	protected static String drop_contactPersonCRV7;
	protected static String btn_setOffUodateInsideButtonCRV;
	protected static String btn_scrollRight007;
	protected static String tbl_gridSectionRowCount_Fin_TC_007;
	protected static String btn_setOffAfter100Records;

	/* Fin_TC_008 */
	protected static String btn_action08;
	protected static String btn_converToInboundPayment08;
	protected static String btn_docDate08;
	protected static String btn_postDate08;
	protected static String btn_dueDate08;
	protected static String drop_paidCurrency08;
	protected static String drop_paymentMethod08;
	protected static String btn_detailsPage08;
	protected static String txt_payingAmount08;
	protected static String chk_IPA08;
	protected static String txt_paidAmount08;
	protected static String txt_searchInboundPayment;
	protected static String lnk_searchResultInboundPayment;
	protected static String tab_summary8;
	protected static String drom_customer8;
	protected static String drop_bankCheque8;
	protected static String txt_chequNo8;

	/* Fin_TC_009 */
	protected static String drop_payMethodBankDeposi;
	protected static String drop_bank;
	protected static String drop_bankAccountNo;
	protected static String btn_viewBankDeposit;
	protected static String chk_voucherBankDeposit;

	/* Fin_TC_010 */
	protected static String btn_outboundPaymentAdvice;
	protected static String btn_newOutboundPaymentAdvice;
	protected static String btn_vendorAdvanceJourney;
	protected static String btn_postBusinessUnit;
	protected static String drop_posBusinessUnit;
	protected static String btn_docDate10;
	protected static String btn_postDate10;
	protected static String btn_dueDate10;
	protected static String btn_searchVendorOPA;
	protected static String txt_serchVendorOPA;
	protected static String lnk_resultVendorOPA;
	protected static String txt_referenceCodeOPA;
	protected static String txt_descriptionOPA;
	protected static String drop_currencyOPA;
	protected static String drop_taxOPA;
	protected static String txt_amountOPA;
	protected static String drop_contactPerson;
	protected static String txt_billingAddress;

	/* Fin_TC_011 */
	protected static String txt_searchOutboundPaymentAdvice;
	protected static String lnk_outboundPaymentAdvice;
	protected static String btn_action;
	protected static String btn_convertToOutboundPaymentAdvice;
	protected static String btn_docDate;
	protected static String btn_calenderBack;
	protected static String btn_calenderForward;
	protected static String btn_postDate;
	protected static String btn_dueDate;
	protected static String drop_currency;
	protected static String drop_payBook;
	protected static String tab_detailOutboundPayment;
	protected static String drop_filterCurrencyOutboundPaymnet;
	protected static String chk_choseOPA;
	protected static String lbl_docNo;
	protected static String btn_viewButonOutboundPayment;

	/* Fin_TC_012 */
	protected static String btn_procument;
	protected static String btn_purchaseInvoice;
	protected static String btn_newPurchaseInvoice;
	protected static String btn_purchaseInvoiceJourney;
	protected static String btn_serchVendor;
	protected static String txt_vendorSearch;
	protected static String lnk_resultVendorSearch;
	protected static String btn_pdPostDate;
	protected static String btn_productLoockup;
	protected static String txt_unitPricePurchaseInvoice;
	protected static String btn_docFlow;
	protected static String header_page;
	public static String lbl_docStatus;
	protected static String btn_dateApply;
	protected static String btn_documentValueTotal;

	/* Fin_TC_014 */
	protected static String btn_vendorCreditMemoJourney;
	protected static String btn_vendorLookupCreditMemo;
	protected static String txt_description;
	protected static String txt_referenceCode;
	protected static String drop_tax;
	protected static String btn_glLookupCreditMemory;
	protected static String txt_GL;
	protected static String txt_amountCreditMemo;
	protected static String ico_noCostAlloacation;
	protected static String drop_costCenter;
	protected static String txt_apportionPresentage;
	protected static String btn_addRecordApportion;
	protected static String btn_update;
	protected static String header_releasedOutboundPaymentAdvice;

	/* Fin_TC_015 */
	protected static String btn_vendorPaymentVoucherJourney;
	protected static String btn_vendorSearchLookupVPV;
	protected static String tab_payment;
	protected static String chk_outboundPaymentAdvice;
	protected static String btn_setOffNos;
	protected static String btn_outboundPayment;
	protected static String btn_addNew;
	protected static String drop_filterCurrency;
	protected static String header_outboundPaymentSetOff;
	protected static String txt_paidAMountOutboundPaymentVendor;
	protected static String lbl_dueAmountOutboundPaymentVendor;
	protected static String btn_viewPendingAdvices;

	/* Fin_TC_016 */
	protected static String btn_letterOfGurentee;
	protected static String btn_new;
	protected static String txt_originalDocNo;
	protected static String drop_requesterType;
	protected static String btn_vendorLookupLOG;
	protected static String txt_vendor;
	protected static String lnk_resultVendor;
	protected static String btn_documentSearchNosLOG;
	protected static String txt_serchPurchaseOrder;
	protected static String lnk_serchResultPurchaseOrder;
	protected static String btn_applyPurchaseOrder;
	protected static String drop_gurenteeGroup;
	protected static String calende_openDate;
	protected static String calende_endDate;
	protected static String txt_amount;
	protected static String lbl_docStatus1;
	protected static String btn_documentSearchLookup16;
	protected static String btn_creditExtentionLetterOFGurentee;

	/* Fin_TC_017 */
	protected static String txt_letterPfGurenteeSearch;
	protected static String lnk_letterOfGurenteeResult;
	protected static String btn_convertToBankFacilityAgreement;
	protected static String txt_originalDocNoLetterOfGurentee;
	protected static String drop_bankBFA;
	protected static String txt_amountBFA;
	protected static String txt_startDate;
	protected static String txt_endDate;
	protected static String btn_date01;
	public static String lbl_journelEntryFirstRowDebit;
	public static String lbl_journelEntrySecondRowCredit;
	protected static String txt_referenceDocumenrLOG;
	protected static String header_page2;
	protected static String drop_bankFacilityGroup;

	/* Fin_TC_018 */
	protected static String lbl_OutboundPaymentNoLOG;
	protected static String btn_releeseLOGOutboundPayment;
	protected static String lnk_resultOutboundPayment;
	protected static String txt_searchOutboundPayment;

	/* Fin_TC_019 */
	protected static String btn_bankAdjustment;
	protected static String chk_facilitySettlement;
	protected static String btn_bankFacilityLookup;
	protected static String txt_bankFacilityAgreementSearch;
	protected static String lnl_bankFacilityAgreementSearchResult;
	protected static String drop_settlementTypeBankAdjustment;
	protected static String btn_costAllocationBankAdjustment;
	protected static String btn_glLookupBAnkAdjustment;
	protected static String txt_description1;
	protected static String lbl_minusValueCheck;
	protected static String header_taxBreackdown;
	protected static String close_headerPopup;
	protected static String btn_btnTaxBrackDownBankAdjustment;
	protected static String td_taxNameCell;

	/* Fin_TC_020 */
	protected static String btn_bankReconcillation;
	protected static String btn_newBankReconcilation;
	protected static String btn_statementDateBankReconcilation;
	protected static String lbl_lastBalaceBankReconcilation;
	protected static String txt_statementBalance;
	protected static String lbl_unReconciledAmount;
	protected static String btn_adjustentBankReconcilation;
	protected static String btn_descriptionLookupBAnkReconcilationAdjustment;
	protected static String txt_descriptionAdjustmenrBankReconcilation;
	protected static String btn_updateAdjustmentBankReconcilation;
	protected static String gl_nosAdjustmentBankReconcilation;
	protected static String txt_amountUluBankReconsillAdjustment;
	protected static String chk_advicePathBankReconcilation;
	protected static String lbl_amountAdviceBankReconcilation;
	protected static String lbl_reconcilAmount;
	protected static String btn_applyDescriptionBankeconcilation;
	protected static String btn_costAllocationAdjustmentBankReconcilation;
	protected static String btn_updateCostCenter;
	protected static String btn_scrollDownArrowBankReconsilGrid;
	protected static String div_reconcilAdvice;

	/* Fin_TC_021 */
	protected static String btn_pettyCash;
	protected static String btn_newPettyCash;
	protected static String drop_pettyCashAccount;
	protected static String btn_lookupPaidTo;
	protected static String txt_searchEmployee;
	protected static String lnk_resultEmployye;
	protected static String txt_descPettyCash;
	protected static String drop_pettyCashType;
	protected static String txt_amountPettyCash;
	protected static String nos_costApportionPettyCash;
	protected static String drop_costCenterPettyCash;
	protected static String txt_costPresentagePettyCash;
	protected static String btn_journelEntryPettyCashHeaderTExt;

	/* Fin_TC_023 */
	protected static String btn_glLookupPettyCash;
	protected static String nos_narrationPettyCash;
	protected static String txt_descriptionNarationPettyCash;
	protected static String btn_applyDescriptionPettyCash;
	protected static String btn_iouLookup;
	protected static String lnk_iouResult;

	/* Smoke_Finance_001 */
	protected static String btn_bankaAdjustment;
	protected static String btn_newBankAdjustment;
	protected static String drop_bankBankAdjustmentSmoke;
	protected static String drop_acoountNoBankAdjustmentSmoke;
	protected static String btn_glAccountLookupBankAdjustmentSmoke;
	protected static String txt_glInFrontPageBankAdjustmentSmoke;
	protected static String txt_amountBankAdjustmentSmoke;
	protected static String btn_summaryBankAdjustmentSmoke;

	/* Smoke_Finance_011 */
	protected static String btn_journelEntryPage;
	protected static String btn_newJournelEntryPage;
	protected static String btn_description;
	protected static String txt_descriptionJournelEntry;
	protected static String btn_applyDescriptionJournelEntry;
	protected static String btn_addRowJournelEntry;
	protected static String btn_addRow2JournelEntry;
	protected static String btn_serchLoockupGLJournelEntry;
	protected static String btn_serchLoockupGLJournelEntry2;
	protected static String btn_serchLoockupGLJournelEntry3;
	protected static String txt_value1JjornelEntry;
	protected static String txt_value2JjornelEntry;
	protected static String txt_value3JjornelEntry;
	protected static String txt_setDescriptionJournelEntry;
	protected static String lbl_value1JournelAccount;
	protected static String lbl_value2JournelAccount;
	protected static String lbl_value3JournelAccount;

	/* Smoke_Finance_02 */
	protected static String btn_bankFacilityAgreement;
	protected static String btn_newBankFacilityAgreement;
	protected static String txt_originalDocNoBFA;
	protected static String drop_bankFacilityGroupBFA;
	protected static String txt_startDateBFA;
	protected static String txt_endDateBFA;
	protected static String lbl_loanCreationConfirmation1;
	protected static String lbl_loanCreationConfirmation2;

	/* Common customer creation */
	protected static String btn_accountInformation;
	protected static String btn_newAccountCustomerAccount;
	protected static String txt_accountNameCustomerAccount;
	protected static String drop_accountGtopuCustomerAccount;

	/* Regression */
	/* FIN_BD_1_2 */
	protected static String header_bankDeposit;
	protected static String header_newBankDeposit;
	protected static String btn_closeMainValidation;
	protected static String lbl_mainValidation;
	protected static String lbl_errorPayMethodDropDown;
	protected static String drop_payMethodWithErrorBorder;
	protected static String lbl_errorBankDropDown;
	protected static String drop_BankWithErrorBorder;
	protected static String lbl_errorBankAccountDropDown;
	protected static String btn_bankDepositGridErrorOpen;
	protected static String lbl_gridErroeBallonBankDeposit;

	/* FIN_BD_3_4_5_6_7_10 */
	protected static String txt_descriptinArea;
	protected static String btn_cashBankBook;
	protected static String txt_searchCashBankBook;
	protected static String count_bankAccount;
	protected static String td_bankAccountRowReplace;
	protected static String drop_cashBookFilter;

	/* FIN_BD_9 */
	protected static String txt_chequeFilterBy;
	protected static String count_paymentBankDeposit;

	/* FIN_BD_12_13_14_15_17 */
	protected static String header_releasedInboundPaymentAdvice;
	protected static String header_releasedInboundPayment;
	protected static String chk_firstAdviceOnGridInboundPayment;
	protected static String chk_firstAdviceOnGridInBankDeposit;
	protected static String header_draftedBankDeposit;
	protected static String txt_abountSelectedInboundPaymentBankDeposit;
	protected static String btn_edit;
	protected static String btn_updateMainDoc;
	protected static String div_updatedAmountBankDeposit;
	protected static String btn_updateAndNew;
	protected static String btn_duplicate;
	protected static String lbl_draftDescriptionOnHistoryTab;
	protected static String lbl_updateDescriptionOnHistoryTab;
	protected static String lbl_draftOnHistoryTab;
	protected static String lbl_updateOnHistoryTab;
	protected static String btn_history;

	/* FIN_BD_16 */
	protected static String btn_delete;
	protected static String btn_yes;
	protected static String header_deletedBankDeposit;

	/* FIN_BD_18 */
	protected static String btn_draftAndNew;
	protected static String label_recentlyDraftedBankDeposit;
	protected static String lnl_releventDraftedBandDepositDocReplace;
	protected static String txt_descriptionDraftedBankDeposit;

	/* FIN_BD_19_20 */
	protected static String btn_reverse;
	protected static String btn_reverseSecond;
	protected static String header_reversedBankDeposit;

	/* FIN_BD_21 */
	protected static String btn_curencyExchangeRate;
	protected static String txt_usdBuyingRate;
	protected static String txt_usdSellingRate;
	protected static String btn_update2;
	protected static String txt_amountWithGL;
	protected static String txt_customerInboundPaymentFrontPage;
	protected static String drop_paidCurrency;
	protected static String header_inboundPaymentNewCustomerRecipt;
	protected static String drop_filterCurrencyBankDeposit;

	/* FIN_BD_22 */
	protected static String drop_cashAccount;
	protected static String div_paymentDocumentListBankDepositDocReplace;
	protected static String chk_paymentDocumentListBankDepositDocReplace;
	protected static String txt_depositAmount;

	/* FIN_BD_23 */
	protected static String btn_vendorLoockup;
	protected static String drop_payMethodVendorRefund;
	protected static String btn_glLookupVendorRefund;
	protected static String txt_amountVendorRefund;
	protected static String btn_journeyVendorRefund;
	protected static String lnk_inforOnLookupInformationReplace;

	/* FIN_BD_24 */
	protected static String btn_journeyReciptVoucher;
	protected static String txt_amountReciptVoucher;
	protected static String btn_glLookup;

	/* FIN_BD_25 */
	protected static String btn_employeeLookup;
	protected static String txt_amountEmployeeRecipt;
	protected static String btn_journeyEmployeeReceiptAdvice;
	protected static String btn_journeiesDows;

	/* FIN_BD_26_27 */
	protected static String header_releasedBankDeposit;
	protected static String lbl_creditJournelEntryBankDepositCash;
	protected static String lbl_debitJournelEntryBankDepositCash;
	protected static String lbl_debitTotalJournelEntry;
	protected static String lbl_creditTotalJournelEntry;

	/* FIN_BD_28 */
	protected static String drop_bankNameInboundPayment;
	protected static String txt_checkNoInboundPayment;

	/* FIN_BD_32_33_34 */
	protected static String tbl_coundJournelEntries;
	protected static String lbl_creditJournelEntryBankDepositGLReplace;
	protected static String lbl_debitJournelEntryBankDepositGLReplace;
	protected static String lbl_debitTotalJournelEntryRecordReplace;
	protected static String lbl_creditTotalJournelEntryRecordReplace;
	protected static String lbl_journelEntryDocNoJEReplace;
	protected static String txt_searchInboundPayment2;
	protected static String lnl_releventInboundPaymentDocReplace;
	protected static String lbl_journelEntryDocNoSingleEntry;
	protected static String txt_depositAmountBankDeposit;

	/* FIN_BD_35_36_37 */
	protected static String btn_closeWindow;
	protected static String div_summaryTab;
	protected static String properties_docFlow;

	/* FIN_BD_38 */
	protected static String para_validatorInboundPaymentAlreadyUsed;

	/* FIN_BD_39 */
	protected static String btn_chequeReturn;
	protected static String txt_returnDate;
	protected static String btn_day;
	protected static String btn_return;
	protected static String header_checkReturnWindow;
	protected static String lbl_docStatusChequeReturn;

	/* Bank Adjustment */
	/* FIN_BA_1_5_6 */
	protected static String header_bankAdjustmentByPage;
	protected static String header_newBankAdjustment;
	protected static String error_msgFieldDropdownBankBankAdjustment;
	protected static String error_borderBankAccountNoBankAdjustment;
	protected static String error_msgFieldDropdownBankAccountBankAdjustment;
	protected static String error_borderCurencyBankAdjustment;
	protected static String error_msgFieldCurrencyBankAdjustment;
	protected static String error_msgFieldGLBankAdjustment;
	protected static String error_borderAmountBankAdjustment;
	protected static String error_msgFieldAmountBankAdjustment;

	/* FIN_BA_2 */
	protected static String btn_adminModule;
	protected static String btn_userPermission;
	protected static String btn_userLookupUserPermission;
	protected static String txt_userSearch;
	protected static String drop_moduleNameUserPermission;
	protected static String chk_reverseBankAdjustmentUserPermission;
	protected static String chk_releaseBankAdjustmentUserPermission;
	protected static String chk_viewBankAdjustmentUserPermission;
	protected static String chk_allowTempBankAdjustmentUserPermission;
	protected static String lbl_noPermissionValidation;

	/* FIN_BA_3 */
	protected static String drop_templateOnByPage;

	/* FIN_BA_4_7_8_9 */
	protected static String btn_lookupGLBankAdjustment;
	protected static String header_draftedBankAdjustment;
	protected static String header_releasedBankAdjustment;
	protected static String lnk_firstRecordBankAdjustmentByPage;
	protected static String header_releasedBankAdjustmentDocReplace;
	protected static String txt_searchBankAdjustment;
	protected static String btn_searchOnByPage;
	protected static String lnk_resultDocOnByPage;

	/* FIN_BA_10 */
	protected static String txt_bankFacilityFrontPageBankAdjustment;
	protected static String btn_lookupBankFacilityOnBankAdjustment;

	/* FIN_BA_12 */
	protected static String lbl_recentlyDraftedBankAdjustment;
	protected static String lnl_releventDraftedBankAdjustmentDocReplace;
	protected static String txt_descriptionDraftedBankAdjustment;

	/* FIN_BA_13 */
	protected static String btn_copyFrom;
	protected static String header_loockupBankAdjustment;

	/* FIN_BA_14 */
	protected static String btn_expandPropertyPlantAndEquipment;
	protected static String btn_expandInventory;
	protected static String btn_editLandGLAccount;
	protected static String btn_accountStatusGL;
	protected static String drop_glAccountStatus;
	protected static String btn_applyGlAccountStatus;
	protected static String btn_updateLast;
	protected static String btn_editChartOfAccount;
	protected static String btn_chartOfAccount;

	/* FIN_BA_16 */
	protected static String header_reverseDatePopup;
	protected static String header_reversedBankAdjustment;

	/* FIN_BA_18_19 */
	protected static String txt_amountWithErrorBorderBankAdjustment;
	protected static String lbl_errorPlusAmountCapitalSettlementBankAdjustment;
	protected static String div_balanceAmountBankFacilityAgreement;
	protected static String para_validationHigherAdjustmentThanLOanBalance;

	/* FIN_BA_20_23 */
	protected static String lnk_bankFacilityAgreementDocReplace;
	protected static String drop_costAssignmentTypeBankAdjustment;
	protected static String btn_closeCostAllocationWindow;

	/* FIN_BA_30 */
	protected static String para_validationCheckoutBeforeDraft;
	protected static String btn_checkoutWithErrorBorder;

	/* FIN_BA_31_32 */
	protected static String txt_taxTotal;

	/* FIN_BA_33 */
	protected static String txt_currencyBankAdjustment;

	/* Cash Bank Book */
	/* FIN_CBB_1_4_5_6 */
	protected static String header_cashBankBookByPage;
	protected static String btn_newCashBankBook;
	protected static String header_newCashBankBook;
	protected static String drop_typeCashBankBook;
	protected static String txt_bankAccountNoCashBankBook;
	protected static String txt_floatCashBankBook;
	protected static String btn_permissionGroupLookupCashBankBook;
	protected static String txt_searcUserGroup;
	protected static String txt_permissionGroupFrontPageCBB;
	protected static String drop_bankCodeCBB;
	protected static String txt_companyStatementCBB;
	protected static String txt_bankBranchCBB;
	protected static String drop_accountTypeCBB;
	protected static String txt_telephoneNumberCBB;
	protected static String txt_faxCBB;
	protected static String txt_emailCBB;
	protected static String txt_webURLCBB;
	protected static String txt_addressCBB;
	protected static String chk_moreCurrenciesCBB;
	protected static String txt_discountingMaximumCBB;
	protected static String txt_activeFromCBB;
	protected static String txt_activeToCBB;
	protected static String txt_deawerCBB;
	protected static String txt_swiftCodeCBB;
	protected static String chk_iouCBB;
	protected static String chk_leaseFacilityCBB;
	protected static String header_draftedCashBankBook;
	protected static String header_releasedCashBankBook;
	protected static String header_releasedCashBankBookDocReplace;
	protected static String count_cashBannkBookByPage;
	protected static String lbl_docNoCashBankBook;

	/* FIN_CBB_2 */
	protected static String chk_reverseCashBankBookUserPermission;
	protected static String chk_releaseCashBankBookUserPermission;
	protected static String chk_viewCashBankBookUserPermission;
	protected static String chk_allowTempCashBankBookUserPermission;

	/* FIN_CBB_7 */
	protected static String txt_odLimit;

	/* FIN_CBB_12 */
	protected static String para_validationDuplicateAccountNumber;

	/* FIN_CBB_013 */
	protected static String lbl_accountNoDrfatedCashBankBook;
	protected static String lbl_recentlyDraftedCashBAnkBook;

	/* FIN_CBB_14 */
	protected static String header_lookupCashBankBook;

	/* FIN_CBB_15 */
	protected static String btn_removePermissionGroupCBB;

	/* FIN_CBB_18 */
	protected static String btn_statusAction;
	protected static String drop_status;
	protected static String btn_applyStatus;
	protected static String lbl_status;
	protected static String btn_payBookGenaration;
	protected static String btn_newPayBookGenaration;
	protected static String drop_payMethod;
	protected static String header_payBookGenerationByPage;
	protected static String header_newPayBookGenaration;

	/* FIN_CBB_20 */
	protected static String header_newPettyCash;
	protected static String txt_amountPettyCash2;
	protected static String txt_amountWithErrorBorderFrontPagePettyCash;
	protected static String error_msgFrontPageFloatAmountPettyCash;
	protected static String txt_amountWithErrorBorderPettyCash;
	protected static String btn_rowErrorPettyCash;
	protected static String error_gridFloatExceedPettyCash;
	protected static String error_gridTotalAmountFloatExceedPettyCash;

	/* FIN_CBB_21 */
	protected static String txt_payCodePayBookGenaration;
	protected static String header_draftedPayBook;
	protected static String header_releasedPayBook;

	/* FIN_CBB_22 */
	protected static String btn_curencyWidgetCashBankBook;
	protected static String lbl_exchageRateCashBankBook;

	/* FIN_CBB_25_26_27 */
	protected static String header_newBankReconcilation;

	/* FIN_CBB_29 */
	protected static String error_floatFieldMandatoryValidationCashBankBook;
	protected static String error_borderFloatCashBankBook;

	/* Bank Facility Agreement */
	/* FIN_BFA_1_5_7_8_9_10_11_14_22 */
	protected static String header_newBFA;
	protected static String header_BFAByPage;
	protected static String btn_dayCalenderBankFacilityDayReplace;
	protected static String drop_currencyBFA;
	protected static String header_draftedBankFacilityAgreement;
	protected static String header_releasedBankFacilityAgreement;
	protected static String drop_bankAccountNoBankFacilityAgreement;
	protected static String drop_yeaeDatePlickerBFA;

	/* FIN_BFA_2 */
	protected static String chk_reverseBFAUserPermission;
	protected static String chk_releaseBFAUserPermission;
	protected static String chk_viewBFAUserPermission;
	protected static String chk_allowTempBFAUserPermission;

	/* FIN_BFA_4 */
	protected static String lnk_firstRecordBankFacilityAgreementByPage;
	protected static String header_releasedBankFacilityAgreementDocReplace;

	/* FIN_BFA_13 */
	protected static String error_amountFieldMinusValidationBFA;

	/* FIN_BFA_15 */
	protected static String lab_bankAccountDrfatedBFA;

	/* FIN_BFA_16 */
	protected static String lnk_documentOnBankFacilityAgreementByPageDocReplace;
	protected static String lnk_bankFacilityAgrrementByPageOnNewForm;

	/* FIN_BFA_18 */
	protected static String header_deletedBankFacilityAgreement;

	/* FIN_BFA_19 */
	protected static String td_updatedTimeHistoryDetailsOnlyOneAction;
	protected static String td_actionDraftDoneByUseHistoryDetailsOnlyOneAction;
	protected static String td_userHistoryDetailsOnlyOneAction;
	protected static String td_descriptionHistoryDetailsOnlyOneAction;

	/* FIN_BFA_20 */
	protected static String lnk_documentOnBankFacilityAgreementByPageOriginDocReplace;
	protected static String lnk_bankFacilityAgrrement;

	/* FIN_BFA_21 */
	protected static String header_lookupBFA;

	/* FIN_BFA_23 */
	protected static String header_reversedBFA;

	/* FIN_BFA_24_25 */
	protected static String btn_hold;
	protected static String btn_unhold;
	protected static String txt_changeReson;
	protected static String btn_okHoldUnhold;
	protected static String header_holdBFA;

	/* FIN_BFA_27 */
	protected static String txt_amountLetterOfGurantee;
	protected static String txt_originalDocNumberLOG;
	protected static String drop_requestTypeLOG;
	protected static String btn_lookupRequesterLOG;
	protected static String drop_guranteeGroupLOG;
	protected static String txt_expirationDateLOG;
	protected static String btn_nextArrowCalender;
	protected static String btn_28Calender;
	protected static String btn_lookupReferenceDocLOG;
	protected static String btn_widgetReferenceDocLOG;
	protected static String btn_refreshRefrenceDocLookupLOG;
	protected static String lnk_firstResultRefrenceDocLOG;
	protected static String btn_applyRefernceDocLOG;
	protected static String header_drfatedLOG;
	protected static String header_releasedLOG;
	protected static String btn_newLetterOfGurantee;
	protected static String txt_captalAmountBFA;
	protected static String txt_interestAmountBFA;

	/* FIN_BFA_34_35_36 */
	protected static String btn_dayCommonDateFlicker;
	protected static String btn_applyDatesBFA;
	protected static String tab_settlementDetailsBFA;
	protected static String btn_downloadTemplateBFA;
	protected static String btn_uploadTemplateBFA;
	protected static String btn_okGridClearConfirmationBFA;
	protected static String btn_chooceFileBFA;
	protected static String btn_tagFixedAssert;
	protected static String btn_lookupFixedAssertTagFixedAssert;
	protected static String headr_fixedAssertInformationLookup;
	protected static String txt_searchFixedAssert;
	protected static String btn_updateTaggedFixedAssert;
	protected static String btn_fixedAssetModule;
	protected static String btn_fixedAssetInformation;
	protected static String lnk_resultOnByPageFixedAssertReplace;
	protected static String lbl_attachedLeasedFacilityFixedAssert;
	protected static String div_uploadedLastRowInstallments;
	protected static String btn_closeJournelEntryBankFacilityAgreement;
	protected static String btn_deatlilsTabFixedAssertInformation;

	/* Petty Cash */
	/* FIN_PC_01 */
	protected static String header_pettyCashByPage;

	/* FIN_PC_04 */
	protected static String txt_floatBalancePettyCash;

	/* FIN_PC_05 */
	protected static String txt_balancePettyCash;

	/* FIN_PC_06 */
	protected static String span_paidToLookupPettyCash;
	protected static String txt_paitToPettyCashFrontPage;
	protected static String header_paidToLookupPC;
	protected static String td_resultEmployeePC;

	/* FIN_PC_08 */
	protected static String txt_amountPettyCashPC;
	protected static String header_amountGridPettyCash;

	/* FIN_PC_12 */
	protected static String btn_glAccountLookupPC;
	protected static String header_glAccountLookupPC;
	protected static String txt_searchGLAccount;
	protected static String td_resultGLAccountPC;
	protected static String div_selectedGLAccountPC;

	/* FIN_PC_13 */
	protected static String btn_narrationPC;
	protected static String txt_extendedNarrationPC;
	protected static String btn_applyExtendedNaration;
	protected static String header_draftedPettyCash;

	/* FIN_PC_15 */
	protected static String lnk_pettyCashHeaderOnNewPage;
	protected static String txt_searchPettyCash;
	protected static String lnk_resultPettyCashByPage;

	/* FIN_PC_17 */
	protected static String header_deletedPettyCash;

	/* FIN_PC_18 */
	protected static String td_descriptionHistoryDetailsUpdatedPC;

	/* FIN_PC_19 */
	protected static String lbl_recentlyDraftedPettyCash;
	protected static String lnk_releventDraftedPettyCashDocReplace;
	protected static String div_descriptionAfterDraftThePC;

	/* FIN_PC_20 */
	protected static String header_releasedPettyCash;

	/* FIN_PC_21 */
	protected static String header_reversedPC;

	/* FIN_PC_23 */
	protected static String btn_iouLookupPC;
	protected static String header_pettyCashPopupPC;
	protected static String td_resultIOUPC;
	protected static String txt_selectedIOUFrondPagePC;
	protected static String lbl_creditJournelEntryPettyCashGLReplace;
	protected static String lbl_debitJournelEntryPettyCashGLReplace;

	/* FIN_PC_25 */
	protected static String txt_amountWithErrorBorderPC;
	protected static String txt_lineAmountWithErrorBorderPC;
	protected static String lbl_amountFieldErrorMsgFloatAndBalanceExceedPC;
	protected static String btn_lineRowErrorsPC;
	protected static String div_lineErrorExceedAmountPC;
	protected static String div_lineErrorExceedTotalAmountPC;
	protected static String div_lineErrorExceedFloatAmountPC;
	protected static String p_mainValidation;

	/* FIN_PC_26 */
	protected static String btn_dayHeaderMainDates;
	protected static String btn_applyPostDate;
	protected static String btn_backDatePlicker;
	protected static String btn_lastDateOfTheMonthHeaderMainDates;
	protected static String p_validationCannotReversePettyCash;

	/* FIN_PC_28 */
	protected static String txt_narrationOnGridPettyCash;

	/* FIN_PC_29 */
	protected static String header_newDuplicatedPettyCash;

	/* FIN_PC_30 */
	protected static String btn_fundTransferJourney;
	protected static String btn_lookupReceiverCashBook;
	protected static String td_resultCashBookFT;
	protected static String txt_selectedCashBankBookFT;
	protected static String txt_amountFundTransferAdvice;
	protected static String btn_convertToOutboundPayment;
	protected static String btn_detailsTabFT;
	protected static String chk_adviceSelectOutboundPaymentFT;
	protected static String p_totalExceedFloatBalancealidation;

	/* FIN_PC_31 */
	protected static String header_outboundPaymentAdviceByPage;
	protected static String header_newOutboundPaymentAdviceFT;
	protected static String header_draftedOutboundPayementAdviceFT;
	protected static String header_releasedOutboundPayementAdviceFT;
	protected static String widget_balanceAmountOutboundPaymentReceivingAccount;
	protected static String btn_cashBankBalanceOnWidgetOP;
	protected static String label_accountBalanceOP;
	protected static String drop_paybookOP;
	protected static String header_draftedOutboundPayementFT;
	protected static String header_releasedOutboundPayementFT;
	protected static String widget_balanceAmountOutboundPaymentReceivingAccountAfterReleadsed;
	protected static String header_newOutboundPaymentFT;
	protected static String drop_paymentMethodOutboundPayment;
	protected static String td_resultPettyCashAccountFT;
	protected static String btn_closeBalaceWidgetOP;

	/********* Outbound Payment Advice ********/
	/* FIN_OPA_02 */
	protected static String btn_vendorAdvanceJourneyOPA;
	protected static String btn_vendorCreditMemoJourneyOPA;
	protected static String btn_accruelVoucherJourneyOPA;
	protected static String btn_fundTransferAdviceJourneyOPA;
	protected static String btn_customerRefundJourneyOPA;
	protected static String btn_paymentVoucherJourneyOPA;
	protected static String btn_employeePaymentAdviceJourneyOPA;
	protected static String btn_outboundPaymentAdviceJourneyOPA;

	/* FIN_OPA_03 */
	protected static String header_newVendorAdvanceOPA;

	/* FIN_OPA_04 */
	protected static String btn_vendorLookupOPA;
	protected static String txt_searchVendor;
	protected static String popup_vendorLookup;
	protected static String td_resultVendorVendorAdvanceOPA;
	protected static String txt_vendorFrontPageOPA;
	protected static String drop_autoSelectedContactPersonOPA;
	protected static String drop_autoSelectedAddressOPA;

	/* FIN_OPA_05 */
	protected static String txt_referenceNoOPA;
	protected static String txt_descriptionVendorAdvanceOPA;

	/* FIN_OPA_06 */
	protected static String drop_currencyOutboundPaymentAdviceVendorAdvance;
	protected static String lbl_currencyTotakLableVA_OPA;

	/* FIN_OPA_07 */
	protected static String txt_amountVA_OPA;

	/* FIN_OPA_08 */
	protected static String drop_taxVA_OPA;

	/* FIN_OPA_10 */
	protected static String header_releaseVA_OPA;
	protected static String header_drfatedVA_OPA;
	protected static String lbl_bannerTotalVAOPA;

	/* FIN_OPA_11_A */
	protected static String header_newOP_VAP;
	protected static String drop_payCurrencyOP_VAP;
	protected static String drop_payMethod_VAP;
	protected static String tab_detailsOP_VAP;
	protected static String chk_selectPaymenAdviceOP_VAP;
	protected static String lbl_bannerTotalOP_VAP;
	protected static String header_draftedOP_VAP;
	protected static String header_releasedOP_VAP;

	/* FIN_OPA_11_B */
	protected static String header_reversedOP_VAP;

	/* FIN_OPA_12 */
	protected static String header_newVendorCreditMemo;
	protected static String btn_vendorLookup_OPA_VCM;
	protected static String txt_selectedVendor_OPA_VCM;
	protected static String drop_currency_OPA_VCM;
	protected static String span_glLookup_OPA_VCM;
	protected static String td_resultGlAccount_OPA_VCM;
	protected static String txt_amount_OPA_VCM;
	protected static String header_drafted_OPA_VCM;
	protected static String header_released_OPA_VCM;
	protected static String btn_setOff_OPA_VCM;
	protected static String popup_outboundPaymentSetoff;
	protected static String chk_setOffAdvice_OPA_VCM;
	protected static String btn_setOffOnOPsetOffPopup;
	protected static String p_successfullySetOff_OPA_VCM;
	protected static String p_successfullySetOffFullAmount_OPA_VCM;
	protected static String btn_setOffDetailsOnAvtionMenu;
	protected static String chk_reverseSetOffAdvice_OPA_VCM;
	protected static String btn_reverseOotboundPaymentSetOff;
	protected static String p_successfullyReverseSetOff_OPA_VCM;
	protected static String td_resultVendorVendorCreditMemoOPA;
	protected static String div_glAccountAfterSeleted_OPA_VCM;
	protected static String txt_paiAmountSetOffWindow_OPA_VCM;
	protected static String td_resultVendorVendorAdvance_OPA_VCM;
	protected static String lbl_bannerTotal;

	/* FIN_OPA_14 */
	protected static String drop_autoSelectedContactPerson_OPA_VCM;
	protected static String drop_autoSelectedAddress_OPA_VCM;

	/* FIN_OPA_15 */
	protected static String txt_referenceNo_OPA_VCM;
	protected static String txt_descriptionVendorAdvance_OPA_VCM;

	/* FIN_OPA_19_B */
	protected static String header_new_OP_VCM;
	protected static String drop_payCurrency_OP_VCM;
	protected static String drop_payMethod_OP_VCM;
	protected static String tab_details_OP_VCM;
	protected static String chk_selectPaymenAdvice_OP_VCM;
	protected static String lbl_bannerTotal_OP_VCM;
	protected static String header_drafted_OP_VCM;
	protected static String header_released_OP_VCM;
	protected static String header_reversedOP_VCM;

	/* FIN_OPA_21 */
	protected static String p_setOffAmountExceedValidatioVsAdviceDue;

	/* FIN_OPA_23 */
	protected static String txt_paidAmountPartialAmount_VCM_OPA;

	/* FIN_OPA_24 */
	protected static String td_inactiveGL_OPA;
	protected static String btn_expandChartOfAccount;
	protected static String td_resultInactiveGLAccountOPA;

	/* FIN_OPA_25 */
	protected static String header_newAccruelVoucher;
	protected static String btn_vendorLookup_OPA_AV;
	protected static String txt_selectedVendor_OPA_AV;
	protected static String drop_currency_OPA_AV;
	protected static String span_glLookup_OPA_AV;
	protected static String td_resultGlAccount_OPA_AV;
	protected static String txt_amount_OPA_AV;
	protected static String header_drafted_OPA_AV;
	protected static String header_released_OPA_AV;
	protected static String btn_setOff_OPA_AV;
	protected static String chk_setOffAdvice_OPA_AV;
	protected static String p_successfullySetOff_OPA_AV;
	protected static String p_successfullySetOffFullAmount_OPA_AV;
	protected static String chk_reverseSetOffAdvice_OPA_AV;
	protected static String p_successfullyReverseSetOff_OPA_AV;
	protected static String td_resultVendor_AV_OPA;
	protected static String div_glAccountAfterSeleted_OPA_AV;
	protected static String txt_paiAmountSetOffWindow_OPA_AV;
	protected static String td_resultVendorVendorAdvance_OPA_AV;
	protected static String drop_autoSelectedContactPerson_OPA_AV;
	protected static String drop_autoSelectedAddress_OPA_AV;

	/* FIN_OPA_26 */
	protected static String txt_referenceNo_OPA_AV;
	protected static String txt_descriptionVendorAdvance_OPA_AV;

	/* FIN_OPA_30_B */
	protected static String drop_payCurrencyOP_AV;
	protected static String drop_payMethod_AV;
	protected static String tab_detailsOP_AV;
	protected static String chk_selectPaymenAdviceOP_AV;
	protected static String header_draftedOP_AV;
	protected static String lbl_bannerTotalOP_AV;
	protected static String header_releasedOP_AV;
	protected static String header_newVendorPaymentAV;

	/* FIN_OPA_30_C */
	protected static String header_reversed_OPA_AV;

	/* FIN_OPA_34 */
	protected static String p_successfullValidationSetOffHalfAmountAV;

	/* FIN_OPA_36 */
	protected static String header_newFundTransferAdvice;
	protected static String td_resultCashReceiveBook_OPA_FT;
	protected static String txt_selectedCashBankBook_OPA_FT;
	protected static String lookup_cashBook;
	protected static String btn_searchOnLookupCashBook;

	/* FIN_OPA_37 */
	protected static String txt_description_OPA_FT;

	/* FIN_OPA_38_39 */
	protected static String txt_amount_OPA_FT;

	/* FIN_OPA_40_A */
	protected static String header_drafted_OPA_FT;
	protected static String header_released_OPA_FT;

	/* FIN_OPA_40_B */
	protected static String header_reversed_OPA_FT;

	/* FIN_OPA_41 */
	protected static String header_newVendorPaymentFT;
	protected static String drop_payCurrencyOP_FT;
	protected static String drop_payMethod_FT;
	protected static String tab_detailsOP_FT;
	protected static String chk_selectPaymenAdviceOP_FT;
	protected static String header_draftedOP_FT;
	protected static String header_releasedOP_FT;

	/* FIN_OPA_42 */
	protected static String btn_journeyDown;
	protected static String header_newCustomerRefund;
	protected static String btn_customerLookup_OPA_CR;
	protected static String lookup_customer;
	protected static String btn_searchCustomer;
	protected static String td_resultCustomer_OPA_CR;
	protected static String txt_selectedCustomer_OPA_CR;
	protected static String drop_contactPerson_OPA_CR;
	protected static String txt_billingAddress_OPA_CR;
	protected static String txt_searchCustomer;

	/* FIN_OPA_43 */
	protected static String txt_customerReference_OPA_CR;
	protected static String txt_description_OPA_CR;

	/* FIN_OPA_44 */
	protected static String drop_currency_OPA_CR;
	protected static String lbl_currencyTotakLable_OPA_CR;

	/* FIN_OPA_45 */
	protected static String drop_taxGroup_OPA_CR;

	/* FIN_OPA_46 */
	protected static String span_glLookup_OPA_CR;
	protected static String header_glAccountLookup;
	protected static String td_resultGlAccount_OPA_CR;
	protected static String div_glAccountAfterSeleted_OPA_CR;

	/* FIN_OPA_47_48 */
	protected static String txt_amount_OPA_CR;

	/* FIN_OPA_49_A */
	protected static String header_drafted_OPA_CR;
	protected static String header_released_OPA_CR;

	/* FIN_OPA_49_B */
	protected static String header_reversed_OPA_CR;

	/* FIN_OPA_50 */
	protected static String header_newVendorPaymentCR;
	protected static String drop_payCurrencyOP_CR;
	protected static String drop_payMethod_CR;
	protected static String tab_detailsOP_CR;
	protected static String chk_selectPaymenAdviceOP_CR;
	protected static String header_draftedOP_CR;
	protected static String header_releasedOP_CR;

	/* FIN_OPA_51 */
	protected static String header_released_IPA_CDM;
	protected static String btn_setOff_OPA_CR;
	protected static String chk_setOffAdvice_OPA_CR;
	protected static String txt_paiAmountSetOffWindow_OPA_CR;
	protected static String p_successfullySetOff_OPA_CR;

	/* FIN_OPA_53 */
	protected static String chk_reverseSetOffAdvice_OPA_CR;
	protected static String p_successfullyReverseSetOff_OPA_CR;

	/* FIN_OPA_54 */
	protected static String p_successfullValidationSetOffHalfAmountCR;

	/* FIN_OPA_55 */
	protected static String header_new_OPA_PV;
	protected static String txt_description_OPA_PV;

	/* FIN_OPA_56 */
	protected static String drop_currency_OPA_PV;

	/* FIN_OPA_57 */
	protected static String drop_taxGroup_OPA_PV;

	/* FIN_OPA_58 */
	protected static String span_glLookup_OPA_PV;
	protected static String td_resultGlAccount_OPA_PV;
	protected static String div_glAccountAfterSeleted_OPA_PV;

	/* FIN_OPA_59_60 */
	protected static String txt_amount_OPA_PV;

	/* FIN_OPA_61_A */
	protected static String header_drafted_OPA_PV;
	protected static String header_released_OPA_PV;

	/* FIN_OPA_61_B */
	protected static String header_reversed_OPA_PV;

	/* FIN_OPA_62 */
	protected static String header_newVendorPaymentPV;
	protected static String drop_payCurrencyOP_PV;
	protected static String drop_payMethod_PV;
	protected static String tab_detailsOP_PV;
	protected static String chk_selectPaymenAdviceOP_PV;
	protected static String header_draftedOP_PV;
	protected static String header_releasedOP_PV;
	protected static String txt_payee_OP_PV;

	/* FIN_OPA_63 */
	protected static String header_paidAmount_OP_PV;
	protected static String txt_paidAmount_OP_PV;

	/* FIN_OPA_64_A */
	protected static String header_new_OPA_EPA;
	protected static String btn_employeeLookup_OPA_EPA;
	protected static String lookup_employee;
	protected static String td_resultEmployee_OPA_EPA;
	protected static String txt_selectedEmployee_OPA_EPA;
	protected static String btn_searchEmployeeLookup;

	/* FIN_OPA_65_A */
	protected static String txt_employeeReference_OPA_EPA;
	protected static String txt_description_OPA_EPA;

	/* FIN_OPA_66_A */
	protected static String span_glLookup_OPA_EPA;
	protected static String td_resultGlAccount_OPA_EPA;
	protected static String div_glAccountAfterSeleted_OPA_EPA;

	/* FIN_OPA_67_A_68_A */
	protected static String txt_amount_OPA_EPA;

	/* FIN_OPA_69_A */
	protected static String header_columnDescriptionGid_OPA_EPA;
	protected static String txt_descriptionOnGrid_OPA_EPA;

	/* FIN_OPA_70_A */
	protected static String header_drafted_OPA_EPA;
	protected static String header_released_OPA_EPA;

	/* FIN_OPA_70_A_B */
	protected static String header_reversed_OPA_EPA;

	/* FIN_OPA_71_A_72 */
	protected static String header_newEmployeePaymentEP;
	protected static String drop_payCurrencyOP_EP;
	protected static String drop_payMethod_EP;
	protected static String tab_detailsOP_EP;
	protected static String chk_selectPaymenAdviceOP_EP;
	protected static String txt_paidAmount_OP_EP;
	protected static String header_paidAmount_OP_EP;
	protected static String header_draftedOP_EP;
	protected static String header_releasedOP_EP;

	/* FIN_OPA_75_A */
	protected static String txt_amountImEditingStatge_OPA_EPA;
	protected static String header_amountGridEditingStage_OPA_EPA;
	protected static String txt_amountDraftedStatge_OPA_EPA;

	/* FIN_OPA_76_A */
	protected static String lnk_outboundPaymentAdviceHeaderOnNewPage;
	protected static String lnk_resultOPAByPage;

	/* FIN_OPA_77_A */
	protected static String drop_currency_OPA_EPA;

	/* FIN_OPA_78_A */
	protected static String header_deleeted_OPA_EPA;

	/* FIN_OPA_79_A */
	protected static String td_descriptionHistoryDetailsUpdatedOPA;

	/* FIN_OPA_80_A */
	protected static String lbl_recentlyDraftedOutboundPaymentAdvice;
	protected static String div_descriptionAfterDraftThe_OPA_EPA;
	protected static String lnk_releventDraftedOutboundPaymentAdviceDocReplace;

	/* FIN_OPA_81_A */
	protected static String header_lookup_OPA;

	/* FIN_OPA_84_A */
	protected static String header_hold_OPA_EPA;

	/* Employee Advance Advice */
	/* FIN_OPA_64_B */
	protected static String btn_employeeAdvanceAdviceJourneyOPA;
	protected static String header_new_OPA_EAA;
	protected static String btn_employeeLookup_OPA_EAA;
	protected static String td_resultEmployee_OPA_EAA;
	protected static String txt_selectedEmployee_OPA_EAA;

	/* FIN_OPA_65_B */
	protected static String txt_employeeReference_OPA_EAA;
	protected static String txt_description_OPA_EAA;

	/* FIN_OPA_67_B_68_B */
	protected static String txt_amount_OPA_EAA;

	/* FIN_OPA_70_B_70_B_B */
	protected static String header_drafted_OPA_EAA;
	protected static String header_released_OPA_EAA;
	protected static String header_reversed_OPA_EAA;

	/* FIN_OPA_71_B */
	protected static String header_newEmployeePaymentEA;
	protected static String drop_payCurrencyOP_EA;
	protected static String drop_payMethod_EA;
	protected static String tab_detailsOP_EA;
	protected static String chk_selectPaymenAdviceOP_EA;
	protected static String header_draftedOP_EA;
	protected static String header_releasedOP_EA;

	/* FIN_OPA_73 */
	protected static String txt_paidAmount_OP_EA;
	protected static String header_paidAmount_OP_EA;
	protected static String p_mainErrorValidation;
	protected static String txt_paidAmountWithErrorBorder_OP_EA;
	protected static String btn_rowError_OP_EA;
	protected static String div_errorPaidAmountShoulsEquelToDueAmount_OP_EA;

	/* FIN_OPA_74_B */
	protected static String txt_amountAfterDraft_OPA_EAA;

	/* FIN_OPA_77_B */
	protected static String drop_currency_OPA_EAA;

	/* FIN_OPA_78_B */
	protected static String header_deleeted_OPA_EAA;

	/* FIN_OPA_80_B */
	protected static String div_descriptionAfterDraftThe_OPA_EAA;

	/* FIN_OPA_82_B_84_B_85_B */
	protected static String header_hold_OPA_EAA;

	/* Outbound Payment */
	/* FIN_OP_01 */
	protected static String btn_customerPaymentVoucherJourneyOP;
	protected static String btn_vendorPaymentVoucherJourneyOP;
	protected static String btn_paymentVoucherJourneyOP;
	protected static String btn_fundTransferVoucherJourneyOP;
	protected static String btn_employeePaymentVoucherJourneyOP;
	protected static String header_outboundPaymentByPage;
	protected static String btn_newOutboundPayment;

	/* FIN_OP_02 */
	protected static String header_newCustomerPaymentVoucherOP;

	/* FIN_OP_03 */
	protected static String btn_customerLookup_OP_CPV;
	protected static String td_resultCustomer_OP_CPV;
	protected static String txt_selectedCustomer_OP_CPV;
	protected static String drop_contactPerson_OP_CPV;
	protected static String txt_billingAddress_OP_CPV;

	/* FIN_OP_04 */
	protected static String drop_currency_OP_CPV;

	/* FIN_OP_05 */
	protected static String tab_details_OP_CPV;
	protected static String drop_filterCurrency_OP_CPV;

	/* FIN_OP_06 */
	protected static String drop_paybook_OP_CPV;
	protected static String drop_paymentMethod_OP_CPV;
	protected static String btn_tabdDetails_OP_CPV;
	protected static String chk_firstAdvice_OP_CPV;
	protected static String header_drafted_OP_CPV;
	protected static String header_released_OP_CPV;
	protected static String lbl_debitJournelEntry_OP_CPV;
	protected static String lbl_creditJournelEntry_OP_CPV;

	/* FIN_OP_07 */
	protected static String txt_payee_OP_CPV;
	protected static String lbl_creditJournelEntryChecquPayment_OP_CPV;

	/* FIN_OP_08 */
	protected static String txt_chequeDate_OP_CPV;
	protected static String txt_bankingDate_OP_CPV;
	protected static String btn_nextArrowCalender_OP_CPV;
	protected static String btn_fifteenCalenderDate;

	/* FIN_OP_10 */
	protected static String lbl_creditJournelEntryEFTPayment_OP_CPV;

	/* FIN_OP_13 */
	protected static String lbl_creditJournelEntryLKRvsUSD_OP_CPV;
	protected static String lbl_debitJournelEntryExchangeRelizedGain_OP_CPV;
	protected static String lbl_creditJournelEntryExchangeRelizedGain_OP_CPV;

	/* FIN_OP_14 */
	protected static String td_resultCustomer_2_OPA_CR;
	protected static String txt_paidAmountSetOffDocReplace_OP_CPV;
	protected static String header_paidAmountSetOff_OP_CPV;
	protected static String btn_setOff_OP_CPV;
	protected static String p_validationSetOffSuccessfull_OP_CPV;
	protected static String txt_dueAmount_OP_CPV;
	protected static String btn_setOffWidget_OP_CPV;
	protected static String window_outboundPaymentSetOff;
	protected static String btn_closeOutboundPaymentSetOffWindow;

	/* FIN_OP_15 */
	protected static String txt_paidAmount_OP_CPV;

	/* Vendor Payment Voucher */
	/* FIN_OP_16 */
	protected static String btn_vendorLookup_OP_VPV;
	protected static String td_resultVendor_OP_VPV;
	protected static String txt_selectedVendor_OP_VPV;
	protected static String drop_autoSelectedContactPerson_OP_VPV;
	protected static String drop_autoSelectedAddress_OP_VPV;
	
	/* FIN_OP_17 */
	protected static String txt_cashAccount_OP_VPV;
	protected static String drop_payBook_OP_VPV;
	protected static String drop_oayMethod_OP_VPV;
	
	/* FIN_OP_18_19 */
	protected static String drop_filterCurrency_OP_VPV;
	protected static String tab_details_OP_VPV;
	protected static String chk_advice_OP_VPV;
	protected static String header_drafted_OP_VPV;
	protected static String header_released_OP_VPV;
	protected static String lbl_debitJournelEntry_OP_VPV;
	protected static String lbl_creditJournelEntry_OP_VPV;
	
	/* FIN_OP_20 */
	protected static String txt_payee_OP_VPV;
	protected static String lbl_creditJournelEntryChecquPayment_OP_VPV;
	
	/* FIN_OP_21 */
	protected static String txt_chequeDate_OP_VPV;
	protected static String btn_nextArrowCalender_OP_VPV;
	protected static String btn_fifteenCalenderDate_OP_VPV;
	protected static String txt_bankingDate_OP_VPV;
	protected static String header_new_OP_VPV;
	
	/* FIN_OP_23 */
	protected static String lbl_creditJournelEntryEFTPayment_OP_VPV;
	
	/* FIN_OP_25 */
	protected static String drop_currency_OP_VPV;
	
	/* FIN_OP_26 */
	protected static String lbl_creditJournelEntryLKRvsUSD_OP_VPV;
	protected static String lbl_debitJournelEntryExchangeRelizedGain_OP_VPV;
	protected static String lbl_creditJournelEntryExchangeRelizedGain_OP_VPV;
	
	/* FIN_OP_27 */
	protected static String div_dueAmountSetOff_OP_VPV;
	protected static String header_dueAmountSetOff_OP_VPV;
	protected static String btn_setOffWidget_OP_VPV;
	protected static String header_paidAmountSetOff_OP_VPV;
	protected static String txt_paidAmountSetOff_OP_VPV;
	protected static String btn_setOff_OP_VPV;
	protected static String p_validationSetOffSuccessfull_OP_VPV;
	protected static String txt_dueAmount_OP_VPV;
	
	/*FIN_OP_28*/
	protected static String txt_paidAmount_OP_VPV;
	
	/* Payment Voucher */
	/* FIN_OP_29 */
	protected static String header_new_OP_PV;
	protected static String drop_currency_OP_PV;
	protected static String drop_payBook_OP_PV;
	protected static String drop_payMethod_OP_PV;
	protected static String txt_cashAccount_OP_PV;
	protected static String tab_details_OP_PV;
	protected static String chk_advice_OP_PV;
	protected static String lbl_bannerTotal_OP_PV;
	protected static String header_drafted_OP_PV;
	protected static String header_released_OP_PV;
	protected static String lbl_debitJournelEntry_OP_PV;
	protected static String lbl_creditJournelEntry_OP_PV;
	protected static String btn_view_OP_PV;
	
	/* FIN_OP_30 */
	protected static String lbl_creditJournelEntryChequePayment_OP_PV;
	
	/* FIN_OP_32 */
	protected static String lbl_creditJournelEntryEFTPayment_OP_PV;
	
	/* FIN_OP_35 */
	protected static String drop_filterCurrency_OP_PV;

	/* Fund Transfer Voucher */
	/* FIN_OP_36 */
	protected static String header_newFundTransferVoucherOP;
	protected static String lookup_cashBook_OP_FTV;
	protected static String btn_reciverCashBookLookup_OP_FTV;
	protected static String txt_searchCashBook;
	protected static String btn_searchCashBookLookup;
	protected static String td_resultCashBook_OP_FTV;
	protected static String txt_receiverCashBookFrontPage_OP_FTV;

	/* FIN_OP_37 */
	protected static String txt_releventCashBookAccordingTpPayBook_OP_FTV;
	protected static String drop_payBook_OP_FTV;
	protected static String chk_adviceFirstOne_OP_FTV;
	protected static String p_validateSameCashBookForSendAndReceive;
	protected static String td_resultCashBook_2_OP_FTV;
	protected static String tab_pricingAndCharges_2_OP_FTV;

	/* FIN_OP_38 */
	protected static String td_resultPettyCashBook_OPA_FTA;
	protected static String p_validationTotalExceedPettyCashBookFloat;

	/* FIN_OP_40 */
	protected static String td_resultPettyCashBookAccountReplaceble_OPA_FTA;
	protected static String p_validationPaymentExceedReimbursement_OPA_FTV;

	/* Employee Payment Voucher */
	/* FIN_OP_41_42 */
	protected static String btn_employeeLookup_OP_EPV;
	protected static String td_resultEmployee_OP_EPV;
	protected static String txt_selectedEmployee_OP_EPV;
	protected static String header_newEmployeePaymentVoucherOP;
	protected static String drop_currency_OP_EPV;
	protected static String drop_paybook_OP_EPV;
	protected static String drop_paymentMethod_OP_EPV;
	protected static String btn_tabdDetails_OP_EPV;
	protected static String chk_firstAdvice_OP_EPV;
	protected static String header_drafted_OP_EPV;
	protected static String header_released_OP_EPV;
	protected static String lbl_creditJournelEntry_OP_EPV;

	/* FIN_OP_43 */
	protected static String lbl_debitJournelEntry_OP_EPV;
	protected static String lbl_creditJournelEntryBank_OP_EPV;
	protected static String txt_payee_OP_EPV;
	protected static String header_checkReturnWindowOutboundPayment;

	/* FIN_OP_46_47 */
	protected static String div_payBookInDraftedStage;

	/* FIN_OP_48 */
	protected static String lnk_outboundPaymentHeaderOnNewPage;
	protected static String lnk_resultOPByPage;

	/* FIN_OP_50 */
	protected static String header_deleeted_OP_EPV;

	/* FIN_OP_51 */
	protected static String td_descriptionHistoryDetailsUpdatedOP;

	/* FIN_OP_52 */
	protected static String lbl_recentlyDraftedOutboundPayment;
	protected static String txt_descriptio_OP_EPV;
	protected static String div_description_OP_EPV;
	protected static String lnk_releventDraftedOutboundPaymentDocReplace;

	/* FIN_OP_54 */
	protected static String header_reversed_OP_EPV;

	/* FIN_OP_55_56_57 */
	protected static String lnk_releventAdvice_OP_EPV;
	protected static String header_releasedDocNoReplace_OP_EPV;
	protected static String txt_subTotal_OP_EPV;
	protected static String txt_discount_OP_EPV;

	// Aruna_JournalEntry
	protected static String txt_userNameA;
	protected static String txt_passwordA;
	protected static String btn_loginA;
	protected static String siteLogoA;
	protected static String headerLinkA;
	protected static String btn_navbuttonA;
	protected static String sidemenuA;
	protected static String subsidemenuA;

	// FIN_18_1to18_12
	protected static String btn_FinanceBtnA;
	protected static String btn_JournalEntryBtnA;
	protected static String btn_NewJournalEntryBtnA;
	protected static String txt_PageHeaderA;
	protected static String btn_DraftBtnA;
	protected static String btn_ReleaseBtnA;
	protected static String btn_TableErrorBtnA;
	protected static String txt_TableErrorTxt1A;
	protected static String txt_TableErrorTxt2A;
	protected static String txt_TableErrorTxt3A;
	protected static String btn_GLAccBtnA;
	protected static String txt_GLAccTxtA;
	protected static String sel_GLAccSelA;
	protected static String txt_GLAccSelTableValueA;
	protected static String txt_GLAccSelTableValue2A;
	protected static String txt_NarrationTxtA;
	protected static String txt_DocValueTxtA;
	protected static String btn_GLAccBtn2A;
	protected static String txt_NarrationTxt2A;
	protected static String txt_DocValueTxt2A;
	protected static String txt_DebitValueA;
	protected static String txt_CreditValueA;
	protected static String btn_SummaryTabA;
	protected static String btn_TablePlusBtn1A;
	protected static String txt_pageDraft1A;
	protected static String txt_pageRelease1A;
	protected static String txt_pageDraft2A;
	protected static String txt_pageRelease2A;
	protected static String btn_MainTableErrorBtnA;
	protected static String txt_TableErrorTxt4A;
	protected static String btn_EditBtnA;
	protected static String btn_UpdateBtnA;
	protected static String btn_UpdateAndNewBtnA;
	protected static String btn_DuplicateBtnA;
	protected static String txt_HeaderDocNoA;
	protected static String btn_JournalEntryByPageBtnA;
	protected static String txt_JournalEntryByPageSearchTxtA;
	protected static String sel_JournalEntryByPageSearchSelA;
	protected static String txt_DocValueDebitTxtA;
	protected static String txt_DocValueCreditTxtA;

	// FIN_18_13to18_14
	protected static String btn_ActionBtnA;
	protected static String btn_DeleteBtnA;
	protected static String txt_pageDeleteA;
	protected static String btn_YesBtnA;
	protected static String btn_NoBtnA;

	// FIN_18_15
	protected static String btn_HistoryBtnA;
	protected static String txt_DraftHistoryTxtA;

	// FIN_18_16
	protected static String btn_DraftAndNewBtnA;

	// FIN_18_17
	protected static String btn_CopyFromBtnA;
	protected static String txt_CopyFromSearchTxtA;
	protected static String sel_CopyFromSearchSelA;

	// FIN_18_18to18_19
	protected static String btn_ReverseBtnA;
	protected static String btn_ReverseLookupBtnA;
	protected static String txt_ReverseHistoryTxtA;
	protected static String btn_ProcumentBtnA;

	// FIN_18_27
	protected static String btn_PurchaseInvoiceBtnA;
	protected static String btn_NewPurchaseInvoiceBtnA;
	protected static String btn_PurchaseInvoiceServiceJourneyA;

	// Aruna-InboundPaymentAdvice
	// FIN_10_1
	protected static String btn_InboundPaymentAdviceBtnA;
	protected static String btn_InboundPaymentAdviceHeaderA;

	// FIN_10_5to10_10
	protected static String btn_NewInboundPaymentAdviceBtnA;
	protected static String btn_JourneyUpArrowBtnA;
	protected static String btn_JourneyDownArrowBtnA;
	protected static String btn_CustomerAdvanceJourneyBtnA;
	protected static String btn_CustomerDebitMemoJourneyBtnA;
	protected static String btn_VendorRefundJourneyBtnA;
	protected static String btn_ReceiptVoucherJourneyIPABtnA;
	protected static String btn_EmployeeReceiptAdviceJourneyBtnA;
	protected static String btn_CustomerRefundableDepositJourneyBtnA;
	protected static String btn_EmployeeBalanceReceiptJourneyBtnA;
	protected static String btn_CustomerLookUpIPABtnA;
	protected static String txt_AmountFieldIPATxtA;
	protected static String btn_SummaryTabIPAA;
	protected static String btn_CheckoutBtnA;

	// FIN_10_11to10_12B
	protected static String btn_ConvertToInboundPaymentIPABtnA;
	protected static String btn_IPAFlagBtnA;
	protected static String btn_SetOffActionBtnA;
	protected static String sel_SetOffDocSelA;
	protected static String txt_SetOffPaidAmountTxtA;
	protected static String btn_SetOffBtnA;
	protected static String btn_SetOffDetailActionBtnA;
	protected static String txt_PaidAmountTxtA;
	protected static String sel_SetOffDetailReverseDocSelA;
	protected static String btn_SetOffReverseBtnA;

	// FIN_10_13
	protected static String btn_OutboundPaymentBtnA;
	protected static String btn_NewOutboundPaymentBtnA;
	protected static String btn_CustomerPaymentVoucherJourneyBtnA;
	protected static String sel_OutboundPaymentIPASelA;

	// FIN_10_15to10_21
	protected static String btn_GLAccIPABtnA;
	protected static String txt_GLAccSelIPATableValueA;
	protected static String txt_AmountFieldCDMTxtA;
	protected static String btn_JournalActionBtnA;
	protected static String txt_JournalCreditTxtA;
	protected static String txt_JournalDeditTxtA;

	// FIN_10_22to10_24
	protected static String txt_SetOffValidatorA;

	// FIN_10_25to10_30
	protected static String btn_VendorLookUpIPABtnA;
	protected static String txt_VendorLookUpIPATxtA;
	protected static String sel_VendorLookUpIPASelA;

	// FIN_10_34to10_39
	protected static String txt_DescriptionIPATxtA;

	// FIN_10_40to10_46
	protected static String btn_EmployeeLookupIPABtnA;
	protected static String txt_EmployeeLookupIPATxtA;
	protected static String sel_EmployeeLookupIPASelA;

	// FIN_10_47to10_53
	protected static String txt_AmountIPATxtA;

	// FIN_10_56to10_58
	protected static String txt_AmountAfterDraftIPATxtA;
	protected static String txt_DescriptionAfterDraftIPATxtA;

	// FIN_10_61
	protected static String txt_DraftedIPAHistoryTextA;

	// FIN_10_63to10_65
	protected static String btn_InboundPaymentAdvicePlusBtnA;
	protected static String txt_CopyFromIPATxtA;
	protected static String sel_CopyFromIPASelA;
	protected static String txt_PageReverseA;

	// FIN_10_66to10_67
	protected static String btn_HoldBtnA;
	protected static String txt_ReasonTxtA;
	protected static String btn_OkBtnA;
	protected static String txt_PageHoldA;
	protected static String btn_UnHoldBtnA;

	// FIN_10_68
	protected static String btn_CostAllocationIPABtnA;
	protected static String sel_CostCenterSelA;
	protected static String txt_CostAllocationPercentageTxtA;
	protected static String btn_CostAllocationAddRecordBtnA;
	protected static String btn_CostAllocationUpdateBtnA;
	protected static String txt_CostAllocationAmountTxtA;
	protected static String txt_CostAllocationValidatorTxtA;
	protected static String btn_CostAllocationSelDeleteBtnA;

	// FIN_10_69
	protected static String btn_SalesAndMarcketingBtnA;
	protected static String btn_SalesInvoiceBtnA;
	protected static String btn_NewSalesInvoiceBtnA;
	protected static String btn_SalesInvoiceToOutboundShipmentJourneyBtnA;
	protected static String btn_CustomerLookUpSIBtnA;
	protected static String txt_AutoCompleteProductTxtA;
	protected static String txt_AutoCompleteProductQtyTxtA;
	protected static String sel_WarehouseSISelA;
	protected static String btn_DocFlowBtnA;
	protected static String btn_InboundPaymentDocFlowBtnA;

	// Aruna-InboundPayment
	// FIN_11_1
	protected static String btn_InboundPaymentBtnA;
	protected static String txt_InboundPaymentHeaderA;

	// FIN_11_5to11_9
	protected static String btn_NewInboundPaymentBtnA;
	protected static String btn_CustomerReceiptVoucherJourneyBtnA;
	protected static String btn_VendorReceiptVoucherJourneyBtnA;
	protected static String btn_ReceiptVoucherJourneyBtnA;
	protected static String btn_EmployeeReceiptVoucherJourneyBtnA;
	protected static String btn_CustomerLookUpBtnA;
	protected static String txt_CustomerLookUpTxtA;
	protected static String sel_CustomerLookUpSelA;
	protected static String txt_CustomerACCTxtA;
	protected static String sel_PaymentMethodSelA;
	protected static String sel_PaidCurrencySelA;

	// FIN_11_10
	protected static String btn_DetailsTabIPBtnA;
	protected static String txt_JournalCreditIPTxtA;
	protected static String txt_JournalDebitIPTxtA;

	// FIN_11_11
	protected static String sel_BankNameIPSelA;
	protected static String txt_ChequeNoIPTxtA;

	// FIN_11_12and11_20
	protected static String btn_BankDepositBtnA;
	protected static String btn_NewBankDepositBtnA;
	protected static String sel_PayMethodBDSelA;
	protected static String sel_BankBDSelA;
	protected static String sel_BankAccountNoBDSelA;
	protected static String txt_CheqNoBDTxtA;
	protected static String btn_ViewBDBtnA;
	protected static String btn_SortByBDBtnA;
	protected static String btn_DolSelBDBtnA;
	protected static String btn_ChequeReturnBtnIPA;
	protected static String sel_ReturnResonSelIPA;
	protected static String txt_ReturnRemarkTxtIPA;
	protected static String btn_ReturnBtnIPABtnIPA;

	// FIN_11_15
	protected static String txt_PayingAmountIPTxtA;

	// FIN_11_17
	protected static String txt_PayingCurrencyIPTxtA;
	protected static String txt_FilterCurrencyIPTxtA;

	// FIN_11_21to11_23
	protected static String btn_VendorLookUpIPBtnA;

	// FIN_11_33to11_35
	protected static String btn_EmployeeLookupIPBtnA;

	// FIN_11_50
	protected static String btn_ViewAllPendingBtnIPA;

	// FIN_11_51to11_53
	protected static String txt_PaymentMethodAfterDraftIPA;
	protected static String txt_DescriptionIPA;
	protected static String txt_DescriptionAfterDraftIPA;

	// FIN_11_55
	protected static String txt_DraftedHistoryTxtIPA;

	// FIN_11_59
	protected static String txt_SameChequeNoValidatorIPA;

	// FIN_16_1to16_10
	protected static String sel_cboxEFTBankIP;
	protected static String sel_cboxEFTBankAccountIP;
	protected static String sel_cboxDDBankIP;
	protected static String sel_cboxDDBankAccountIP;
	protected static String btn_OutboundPaymentAdviceBtnA;
	protected static String btn_NewOutboundPaymentAdviceBtnA;
	protected static String btn_VendorAdvanceJourneyBtnOPA;
	protected static String btn_VendorLookUpBtnOPA;
	protected static String txt_AmountTextOPA;
	protected static String btn_ConvertToOutboundPaymentOPABtnA;
	protected static String sel_cboxPaybook;
	protected static String sel_OPAFlagBtnA;
	protected static String btn_BankAdjustmentBtnA;
	protected static String btn_NewBankAdjustmentBtnA;
	protected static String sel_cboxBankBA;
	protected static String sel_cboxBankAccountNoBA;
	protected static String btn_GLAccountLookupBABtnA;
	protected static String txt_txtAmountBATxtA;
	protected static String btn_BankReconciliationBtnA;
	protected static String btn_NewBankReconciliationBtnA;
	protected static String sel_cboxBankBR;
	protected static String sel_cboxBankAccountNoBR;
	protected static String sel_DocSelBR;
	protected static String txt_StatementBalanceBR;
	protected static String txt_ReconciliedAmountBR;
	protected static String txt_BankAdjustmentBRValidatorA;
	protected static String btn_BankReconciliationByPageBtnA;
	protected static String txt_BankReconciliationByPageSearchTxtA;
	protected static String sel_BankReconciliationByPageSearchSelA;
	protected static String sel_DocSel1BR;
	protected static String sel_DocSel2BR;

	// FIN_16_18to16_20
	protected static String txt_DocNoSearchBRA;

	// FIN_16_33
	protected static String txt_BankAdjustmentGrideValueA;

	// FIN_16_35
	protected static String txt_UnReconciliedAmountTxtA;

	// FIN_16_36
	protected static String txt_BankReconciliationDraftValidatorA;

	// FIN_16_37
	protected static String txt_BankReconciliationReverseValidatorA;

	/* Reading the Data to variables */
	/* Login */
	protected static String site_Url;
	protected static String userNameData;
	protected static String passwordData;

	/* Fin_TC_001 */
	protected static String customerAccountIPA;
	protected static String testAllCharcterWordIPA;
	protected static String curenceIPA;
	protected static String amountIPA;
	protected static String postBusinessUnitIPA;

	/* Fin_TC_002 */
	protected static String amountCRV;
	protected static String glAccount;
	protected static String paidCurrencyCRV;
	protected static String paymentMethodCRV;
	protected static String filterCurrencyCRV;
	protected static String payingAmountCRV;
	protected static String customerAccountCRV;
	protected static String paidAmountCRV;

	/* Fin_TC_003 */
	protected static String currencySalesInvoise;
	protected static String slaesUnitSlaesInvoice;
	protected static String serviceProductSalesInvooice;
	protected static String unitPriceSalesInvice;

	/* Fin_TC_004 */
	protected static String paymentMethodBD;
	protected static String bankBD;

	/* Fin_TC_006 */
	protected static String customerCustomerDebitMemo;
	protected static String departmentCostAllocationCustomerDebitMemo;
	protected static String oresentageCostAllocationCustomerDebitMEmo;

	/* Fin_TC_007 */
	protected static String postBusinessUnitCustomerReciptVoucher;
	protected static String curencyCustomerReciptVoucher;
	protected static String paymentMethodCustomerReciptVoucher;
	protected static String filterCurrencyCustomerReciptVoucher;
	protected static String paidAmountCRV7;
	protected static String paymentMethodCRV7;
	protected static String payingAmountCRV7;

	/* Fin_TC_008 */
	protected static String paidCurrency08;
	protected static String paymentMethod08;
	protected static String payingAmount08;
	protected static String paidAmpunt8;
	protected static String bankCheque8;

	/* Fin_TC_009 */
	protected static String payMethodBankDeposit09;
	protected static String bank09;
	protected static String bankAccount09;

	/* Fin_TC_010 */
	protected static String vendorOutboundPaymentAdvice;
	protected static String referenceCodeOPA;
	protected static String descriptionOPA;
	protected static String currencyOPA;
	protected static String amountOPA;
	protected static String postBusinessUnitOPA;

	/* Fin_TC_011 */
	protected static String curencyTypeOutboundPayment;
	protected static String filterCurencyTypeOutboundPayment;
	protected static String payBookOutboundPayment;

	/* Fin_TC_011 */
	protected static String vendorPurchaseInvoice;
	protected static String serviceProductPurchaseInvoice;
	protected static String unitPricePurchaseInvice;
	protected static String currencyPurchaseInvoice;

	/* Fin_TC_014 */
	protected static String vendorVendorCreditMemo;
	protected static String description;
	protected static String referenceCode;
	protected static String curencyCreditMemo;
	protected static String glAccountCreditMemo;
	protected static String amountCreditMemo;
	protected static String postBusinessUnit;
	protected static String costUnit;
	protected static String costCenterAlloacatePresentage;
	protected static String postBusinessUnitOutboundPaymentVendorPaymentVoucher;

	/* Fin_TC_015 */
	protected static String vendorVRP;
	protected static String curencyVRP;
	protected static String filterCurrency;
	protected static String payBookVendorPaymentVoucher;

	/* Fin_TC_016 */
	protected static String payBookLetterOFGurentee;
	protected static String amountLOG;
	protected static String currencyLOG;
	public static String vebdorLetterOfGurentee;

	/* Fin_TC_017 */
	protected static String originalDocNoLetterOfGurentee;

	/* Fin_TC_019 */
	protected static String bankAccountBankAdjustment;
	protected static String glAccountBankAdjustment;
	protected static String bankBankAdjustment;
	protected static String costAllocatioPresentageNankAdjustment;
	protected static String postBusinessUnitBankAdjustment;
	protected static String amountBankAdjustment;
	protected static String costCenterBankAdjustment;

	/* Fin_TC_020 */
	protected static String bankStatementREconcilationStatementBalance;
	protected static String glAccountAdjustmetnBankReconcillation;
	protected static String bankBankReconcillation;
	protected static String bankAccountBankReconcilation;
	protected static String adjustmentDescriptionBankReconcilation;
	protected static String departmentCostAllocationBankReconcilationAdjustment;
	protected static String presentageCostAllocationBankReconcilationAdjustment;

	/* Fin_TC_021 */
	protected static String employeePettyCash;
	protected static String pettyCshType021;
	protected static String amountPettyCash;
	protected static String costPresentagePettyCash;
	protected static String costCenterPettyCsh;
	protected static String descriptionPettyCashIOU;

	/* Fin_TC_023 */
	protected static String narrationDescription;

	/* Smoke_Finance_001 */
	protected static String bankBankAdjustmentSmoke;
	protected static String accountBankAdjustmentSmoke;
	protected static String glAccountBankAdjustmentSmoke;
	protected static String amountBankAdjustmentSmoke;

	/* Smoke_Finance_011 */
	protected static String account1JournelEntry;
	protected static String account2JournelEntry;
	protected static String account3JournelEntry;
	protected static String value1JournelEntry;
	protected static String value2JournelEntry;
	protected static String value3JournelEntry;
	protected static String descriptionJournelEntry;

	/* Smoke_Finance_02 */
	protected static String originalDocNoBFA;
	protected static String bankNameBFA;
	protected static String accountNoBFA;
	protected static String currencyBFA;
	protected static String amountBFA;
	protected static String bankFacilityGroup;
	protected static String acoounLoanConfiramationBFA;

	/* Common customer account creation */
	protected static String accountGroup;

	/* Regression */
	/* FIN_BD_3_4_5_6_7_10 */
	protected static String payMethodCash;
	protected static String sampathBank;
	protected static String sampathCurrentAccount;
	protected static String payMethodCheque;

	/* FIN_BD_12_13_14 */
	protected static String postBusinessUnitSch;
	protected static String customerAccount;
	protected static String currencyLKR;
	protected static String amountThousand;
	protected static String amountThousandFiveHundred;

	/* FIN_BD_21 */
	protected static String oneHundred;
	protected static String oneHundredTwentyFive;
	protected static String oneHundredFifty;
	protected static String currencyUSD;
	protected static String billingAddressCustomerAccount;
	protected static String glTradeDebtors;
	protected static String glCurrencyGain;

	/* FIN_BD_22 */
	protected static String amountTwoThousand;
	protected static String cashAccount;

	/* FIN_BD_23 */
	protected static String glAccountVendorRefund;
	protected static String vendorAccount;
	protected static String fiftyThousand;

	/* FIN_BD_24 */
	protected static String glAccountMotorVehicles;
	protected static String eightThousand;

	/* FIN_BD_25 */
	protected static String employeeMadhushan;
	protected static String twoThousandFiveHundred;
	protected static String glInvenstInShares;

	/* FIN_BD_28 */
	protected static String fiveThousandEightHundred;
	protected static String bank;

	/* FIN_BD_29 */
	protected static String twoThousandTwoHundred;

	/* FIN_BD_30 */
	protected static String twentyFiveThousand;

	/* FIN_BD_31 */
	protected static String tenThousand;

	/* FIN_BD_32_33_34 */
	protected static String glBank;
	protected static String glCustomerAdvance;
	protected static String glTradeCreditors;

	/* FIN_BD_35_36_37 */
	protected static String commercialBank;
	protected static String commercialCurrentAccount;
	protected static String glBank2;

	/* Bank Adjustment */
	/* FIN_BA_2 */
	protected static String moduleFinance;
	protected static String logUserEmail;

	/* FIN_BD_3 */
	protected static String myTemplateOption;
	protected static String allOption;

	/* FIN_BA_10 */
	protected static String bankFacilityOriginalDocNumber;
	protected static String setlementTypeBankFacilityOnBankAdjustment;

	/* FIN_BA_11 */
	protected static String glAccountMotorVehiclesDuplicatedBankAdjustment;

	/* FIN_BA_14 */
	protected static String statusActive;
	protected static String statusInactive;
	protected static String glLand;

	/* FIN_BA_18_19 */
	protected static String setlementTypeCapital;
	protected static String minusTenThousand;

	/* FIN_BA_21 */
	protected static String lnk_resultBankFailityDocReplace;

	/* FIN_BA_24 */
	protected static String setlementTypeInterest;

	/* FIN_BA_24 */
	protected static String minusTwoThousand;

	/* FIN_BA_26 */
	protected static String setlementTypePanaltyInterest;

	/* FIN_BA_28 */
	protected static String setlementTypeLateFee;

	/* FIN_BA_31_32 */
	protected static String taxGroup;
	protected static String glVATRecievable;

	/* FIN_BA_33 */
	protected static String sampathCurrentAccountUSD;

	/* FIN_BA_36 */
	protected static String sampathBankAccountWithLeaseFacilityAllow;

	/* FIN_CBB_1_4_5_6 */
	/* FIN_BA_36 */
	protected static String cashBankBookTypeCash;
	protected static String permissionGroupCBB;

	/* FIN_CBB_7 */
	protected static String cashBankBookTypeBank;
	protected static String bankBranchNegombo;
	protected static String accountTypeCBB;
	protected static String fax;
	protected static String email;
	protected static String webUrl;
	protected static String address;
	protected static String companyStatement;
	protected static String telephone;
	protected static String amountFiftThousand;
	protected static String drawer;
	protected static String swiftCode;

	/* FIN_CBB_18 */
	protected static String statusInactiveFrontPage;

	/* FIN_CBB_20 */
	protected static String bookTypePettyCash;
	protected static String employee;
	protected static String pettyCashTypeIOU;

	/* FIN_BFA_1_5_7_8_9_10_11_14_22_26 */
	protected static String bankFacilityGroupImportLoan;
	protected static String amountRegressionBFA;
	protected static String glSampathBank;
	protected static String glCashAdvance;

	/* FIN_BFA_12 */
	protected static String alpabaticSampleLetters;
	protected static String specialSampleCharacters;

	/* FIN_BFA_13 */
	protected static String minusAmountBFA;

	/* FIN_BFA_15 */
	protected static String changedBankAccountBFA;

	/* FIN_BFA_24_25 */
	protected static String changeReasonHoldUnholdActionVerification;

	/* FIN_BFA_27 */
	protected static String payBookLOG;
	protected static String letterOfGuranteeAmount_FIN_BFA_27;
	protected static String requestTypeVendorLOG;
	protected static String guranteeGroupLC;

	/* FIN_BFA_34_35_36 */
	protected static String facilityAgreementGroupLease;
	protected static String leaseFacilityAgreementAmount;
	protected static String leaseFacilityAgreementInterest;
	protected static String glInterestInSuspence;
	protected static String gl_cashAtBank;
	protected static String fixedAssertLFA;
	protected static String equelentInstallmentLeaseFacility;
	protected static String equelentInterestLeaseFacility;

	/* Petty Cash */
	/* FIN_PC_03 */
	protected static String pettyCashAccountRegression;

	/* FIN_PC_06 */
	protected static String employeePaidToAfterSelectedPC;
	protected static String employeePaidToPC;

	/* FIN_PC_07 */
	protected static String pettyCashTypeOneOff;
	protected static String pettyCashTypeSettlement;

	/* FIN_PC_08 */
	protected static String testAmountWithLettersAndCharcters;

	/* FIN_PC_09 */
	protected static String pettyCashAccountRegression2;
	protected static String pettyCashAmountMinus;
	protected static String pettyCashAmountPlus;

	/* FIN_PC_12 */
	protected static String glAccountPC;

	/* FIN_PC_13 */
	protected static String narrationPC;

	/* FIN_PC_14 */
	protected static String updatedAmountPC;

	/* FIN_PC_15 */
	protected static String updatedAmountPC2;

	/* FIN_PC_18 */
	protected static String descPettyCash;

	/* FIN_PC_22 */
	protected static String glCashInHandPC;

	/* FIN_PC_23 */
	protected static String glLandPC;
	protected static String glCashAdvancePC;

	/* FIN_PC_25 */
	protected static String pettyCashAccountFloatVerify;

	/* FIN_PC_26 */
	protected static String pettyCashAccountReverseValidation;

	/* FIN_PC_30 */
	protected static String cashAccountFloatValidation;
	protected static String amountFundTransferFloatValidate;

	/* FIN_PC_31 */
	protected static String amountRecieveAccountFundTransfer;
	protected static String fundTransferAmount;
	protected static String payBookCashFT;
	protected static String autoSelectdPaymentMethodOP;

	/* FIN_PC_32 */
	protected static String payBookBankFT;
	protected static String autoSelectdPaymentMethodChequeOP;

	/* FIN_PC_33 */
	protected static String payBookEFTFundTransfer;
	protected static String autoSelectdPaymentMethodEFTOP;

	/************ Outbound Payment Advice *************/
	/* FIN_OPA_04 */
	protected static String selectedVendorOPA;
	protected static String autoSelectedContactPersonOPA;
	protected static String autoSelectedAddressOPA;

	/* FIN_OPA_05 */
	protected static String testPhraseVendorReferenceNo;
	protected static String testPhraseOPADescription;

	/* FIN_OPA_06 */
	protected static String currencyRegressionOPA;
	protected static String totalLableCurrencyVA_OPA;

	/* FIN_OPA_07 */
	protected static String testAmountWithLettersAndCharctersVA_OPA;

	/* FIN_OPA_08 */
	protected static String negativeAmountVA_OPA;
	protected static String positiveAmountVA_OPA;

	/* FIN_OPA_09 */
	protected static String taxGroupVA_OPA;

	/* FIN_OPA_11 */
	protected static String paidCurrencyOP_VPA;
	protected static String payBookOP_VPA;

	/* FIN_OPA_12_13 */
	protected static String amountAdvanceAndCreditMemo;
	protected static String glAccount_OPA_VCM;
	protected static String setOffAmount_OPA_VCM;
	protected static String selectedVendor_OPA_VCM;
	protected static String curency_OPA_VCM;

	/* FIN_OPA_14 */
	protected static String autoSelectedContactPerson_OPA_VCM;
	protected static String autoSelectedAddress_OPA_VCM;

	/* FIN_OPA_17 */
	protected static String testAmountWithLettersAndCharcters_OPA_VCM;

	/* FIN_OPA_18 */
	protected static String negativeAmount_OPA_VCM;
	protected static String positiveAmount_OPA_VCM;

	/* FIN_OPA_19_B */
	protected static String paidCurrency_OP_VCM;
	protected static String payBook_OP_VCM;

	/* FIN_OPA_21 */
	protected static String vendorCrediMemoAmount;
	protected static String setOffAmount;

	/* FIN_OPA_23 */
	protected static String partialPayment_VCM_OP;

	/* FIN_OPA_24 */
	protected static String inactiveGLAccountOPA;

	/* FIN_OPA_25 */
	protected static String amountAV;
	protected static String glAccount_OPA_AV;
	protected static String curency_OPA_AV;
	protected static String setOffAmount_OPA_AV;
	protected static String selectedVendor_OPA_AV;
	protected static String autoSelectedContactPerson_OPA_AV;
	protected static String autoSelectedAddress_OPA_AV;

	/* FIN_OPA_25 */
	protected static String testAmountWithLettersAndCharcters_OPA_AV;

	/* FIN_OPA_30_B */
	protected static String paidCurrencyOP_AVP;
	protected static String payBookOP_AVP;

	/* FIN_OPA_32 */
	protected static String amountTwoHundred;
	protected static String amountOneHundredFifty;

	/* FIN_OPA_34 */
	protected static String setOffPartialAmountAV;

	/* FIN_OPA_36 */
	protected static String receiverCashBankBook_OPA_FT;

	/* FIN_OPA_37 */
	protected static String description_OPA_FT;

	/* FIN_OPA_38 */
	protected static String testAmountWithLettersAndCharcters_OPA_FT;

	/* FIN_OPA_39 */
	protected static String amountFT;

	/* FIN_OPA_41 */
	protected static String paidCurrencyOP_FT;
	protected static String payBookOP_FT;

	/* FIN_OPA_42 */
	protected static String contactPersona_OPA_CR;
	protected static String billingAddress_OPA_CR;

	/* FIN_OPA_43 */
	protected static String testPhraseCustomerReferenceNo;

	/* FIN_OPA_44 */
	protected static String currency_OPA_CR;
	protected static String totalLableCurrency_OPA_CR;

	/* FIN_OPA_45 */
	protected static String taxGroup_OPA_CR;

	/* FIN_OPA_46 */
	protected static String glAccount_OPA_CR;

	/* FIN_OPA_47_48 */
	protected static String testAmountWithLettersAndCharcters_OPA_CR;
	protected static String amount_OPA_CR;

	/* FIN_OPA_49 */
	protected static String customerFrontPage_OPA_CR;

	/* FIN_OPA_50 */
	protected static String paidCurrencyOP_CR;
	protected static String payBookOP_CR;

	/* FIN_OPA_51 */
	protected static String setOffAmount_OPA_CR;

	/* FIN_OPA_52 */
	protected static String setOffAmountThreeHundred;
	protected static String adviceAmoutnThreeHundred;

	/* FIN_OPA_54 */
	protected static String partialSetOffAmountHundred;

	/* FIN_OPA_55 */
	protected static String description_OPA_PV;

	/* FIN_OPA_56 */
	protected static String currency_OPA_PV;

	/* FIN_OPA_57 */
	protected static String taxGroup_OPA_PV;

	/* FIN_OPA_58 */
	protected static String glAccount_OPA_PV;

	/* FIN_OPA_59_60 */
	protected static String testAmountWithLettersAndCharcters_OPA_PV;
	protected static String negativeAmount_OPA_PV;
	protected static String amount_OPA_PV;

	/* FIN_OPA_62 */
	protected static String paidCurrencyOP_PV;
	protected static String payBookOP_PV;
	protected static String payee_OP_PV;

	/* FIN_OPA_64_A */
	protected static String employee_OPA_EPA;
	protected static String employeeFrontPage_OPA_EPA;

	/* FIN_OPA_65_A */
	protected static String testPhraseEmployeeReferenceNo;
	protected static String testPhraseDescription_OPA_EPA;

	/* FIN_OPA_66_A */
	protected static String glAccount_OPA_EPA;

	/* FIN_OPA_67_A_68_A */
	protected static String testAmountWithLettersAndCharcters_OPA_EPA;
	protected static String negativeAmount_OPA_EPA;
	protected static String amount_OPA_EPA;

	/* FIN_OPA_69_A */
	protected static String descriptionGrid_OPA_EPA;

	/* FIN_OPA_71_A_72 */
	protected static String paidCurrencyOP_EP;
	protected static String payBookOP_EP;
	protected static String payee_OP_EP;

	/* FIN_OPA_75_A */
	protected static String updatedAmount_OPA_EPA;

	/* FIN_OPA_76_A */
	protected static String currency_OPA_EPA;

	/* Employee Advance Advice */
	/* FIN_OPA_64_B */
	protected static String employee_OPA_EAA;
	protected static String employeeFrontPage_OPA_EAA;

	/* FIN_OPA_64_B */
	protected static String testPhraseDescription_OPA_EAA;

	/* FIN_OPA_67_B_68_B */
	protected static String testAmountWithLettersAndCharcters_OPA_EAA;
	protected static String negativeAmount_OPA_EAA;
	protected static String amount_OPA_EAA;

	/* FIN_OPA_71_B */
	protected static String paidCurrencyOP_EA;
	protected static String payBookOP_EA;

	/* FIN_OPA_74_B */
	protected static String updatedAmount_OPA_EAA;

	/* FIN_OPA_77_B */
	protected static String currency_OPA_EAA;

	/* Customer Payment Voucher */
	/* FIN_OP_03 */
	protected static String customerFrontPage_OP_CPV;
	protected static String contactPersona_OP_CPV;
	protected static String billingAddress_OP_CPV;

	/* FIN_OP_04 */
	protected static String currency_OP_CPV;

	/* FIN_OP_05 */
	protected static String filterCurrency_OP_CPV;

	/* FIN_OP_06 */
	protected static String paidCurrency_OP_CPV;
	protected static String payBook_OP_CPV;
	protected static String loadedPaymentMethod_OP_CPV;
	protected static String currencyLKR_OPA_CR;
	protected static String customerFrontPage_OP_CR;

	/* FIN_OP_07 */
	protected static String payBookCheque_OP_CPV;
	protected static String loadedPaymentMethodCheque_OP_CPV;
	protected static String payee_OP_CPV;

	/* FIN_OP_10 */
	protected static String payBookEFT_OP_CPV;
	protected static String loadedPaymentMethodEFT_OP_CPV;

	/* FIN_OP_12 */
	protected static String paidCurrencyUSD_OP_CPV;
	protected static String filterCurrencyUSD_OP_CPV;
	protected static String amountUSD_OPA_CR;
	protected static String currencyUSD_OPA_CR;
	protected static String payBookUSDCheque_OP_CPV;

	/* FIN_OP_14 */
	protected static String customerFrontPage_2_OP_CR;
	protected static String contactPersona_2_OPA_CR;
	protected static String billingAddress_2_OPA_CR;
	protected static String paidAmountSetOff_OPA_CPV;

	/* FIN_OP_15 */
	protected static String paidAmount_OPA_CPV;

	/* Vendor Payment Voucher */
	/* FIN_OP_16 */
	protected static String curencyLKR_OPA_VCM;
	protected static String selectedVendor_OP_VPV;
	protected static String autoSelectedContactPerson_OP_VCV;
	protected static String autoSelectedAddress_OP_VPV;
	
	/* FIN_OP_17 */
	protected static String payBook_OP_VPV;
	protected static String payMethodCash_OP_VPV;
	protected static String cashAccount_OP_VPV;
	
	/* FIN_OP_18 */
	protected static String baseCurrency_OP_VPV;
	protected static String currencyUSD_OP_VPV;
	
	/* FIN_OP_20 */
	protected static String bankBook_OP_VPV;
	protected static String payMethodCheque_OP_VPV;
	protected static String payee_OP_VPV;
	
	/* FIN_OP_23 */
	protected static String payBookEFT_OP_VPV;
	protected static String loadedPaymentMethodEFT_OP_VPV;
	
	/* FIN_OP_25 */
	protected static String curencyUSD_OPA_VCM;
	protected static String paidCurrencyUSD_OP_VPV;
	protected static String filterCurrencyUSD_OP_VPV;
	protected static String amount_OPA_VCM;
	protected static String payBookUSD_OP_VPV;
	
	/* FIN_OP_25 */
	protected static String paidCurrencyLKR_OP_VPV;
	
	/* FIN_OP_28 */
	protected static String paidAmount_OP_VPV;
	
	/* Payment Voucher */
	/* FIN_OP_29 */
	protected static String currencyLKR_OPA_PV;
	protected static String paidCurrency_OP_PV;
	protected static String payBook_OP_PV;
	protected static String payMethodCash_OP_PV;
	protected static String cashAccount_OP_PV;
	protected static String amount2_OP_PV;
	protected static String filterCurrency_OP_PV;
	
	/* FIN_OP_30 */
	protected static String bankBook_OP_PV;
	protected static String payMethodCheque_OP_PV;
	
	/* FIN_OP_32 */
	protected static String payBookEFT_OP_PV;
	protected static String loadedPaymentMethodEFT_OP_PV;
	
	/* FIN_OP_34 */
	protected static String paidCurrencyUSD_OP_PV;
	protected static String payBookUSD_OP_PV;
	protected static String amount3_OPA_PV;
	
	/* FIN_OP_35 */
	protected static String filterCurrencyUSD_OP_PV;

	/* Fund Transfer Voucher */
	/* FIN_OP_36 */
	protected static String reciverCashBook_OP_FTV;

	/* FIN_OP_37 */
	protected static String reciverCashBook_2_OP_FTV;
	protected static String payBook_OP_FTV;

	/* FIN_OP_38 */
	protected static String pettyCashBook_OP_FTV;
	protected static String amount_OP_FTV;

	/* FIN_OP_40 */
	protected static String floatAmount_OP_FTV;
	protected static String baseFundTransferAmount_OP_FTV;
	protected static String pettyCashAmount_OP_FTV;
	protected static String reinbursementAmount_OP_FTV;

	/* Employee Payment Voucher */
	/* FIN_OP_41_42 */
	protected static String employee_OP_EPV;
	protected static String employeeFrontPage_OP_EPV;
	protected static String paidCurrency_OP_EPV;
	protected static String payBook_OP_EPV;
	protected static String loadedPaymentMethod_OP_EPV;

	/* FIN_OP_43 */
	protected static String payBookCheque_OP_EPV;
	protected static String loadedPaymentMethodCheque_OP_EPV;
	protected static String payee_OP_EPV;

	/* FIN_OP_45 */
	protected static String payBookEFT_OP_EPV;
	protected static String loadedPaymentMethodEFT_OP_EPV;

	/* FIN_OP_46_47 */
	protected static String updatedPayBook_OP_EPV;

	// Aruna-JournalEntry
	protected static String siteUrlA;
	protected static String userNameDataReg;
	protected static String passwordDataReg;
	protected static String NewUserNameA;
	protected static String NewPasswordA;

	// FIN_18_1to18_11
	protected static String GLAccTxtA;

	// FIN_18_20
	protected static String GLAccTxtInactiveA;

	// FIN_11_5to11_9
	protected static String CustomerLookUpTxtA;
	protected static String PaymentMethodSelA;
	protected static String PaidCurrencySelA;

	// FIN_10_15to10_21
	protected static String CustomerLookUpTxt2A;

	// FIN_10_25to10_30
	protected static String VendorLookUpIPATxtA;

	// FIN_10_40to10_46
	protected static String EmployeeLookupIPATxtA;

	// FIN_10_47to10_53
	protected static String CustomerLookUpTxt3A;

	// FIN_10_59to10_60
	protected static String CustomerLookUpTxt3NewA;

	// FIN_10_68
	protected static String CostCenterSelA;

	// FIN_10_69
	protected static String AutoCompleteProductTxtA;
	protected static String WarehouseSISelA;
	protected static String CustomerLookUpTxt4NewA;

	// FIN_11_11
	protected static String PaymentMethodSel2A;
	protected static String BankNameIPSelA;

	// FIN_11_12and11_20
	protected static String BankBDSelA;
	protected static String BankAccountNoBDSelA;

	// FIN_11_13
	protected static String PaymentMethodSel3A;

	// FIN_11_14
	protected static String PaymentMethodSel4A;

	// FIN_11_18
	protected static String PaidCurrencyForignA;

	// FIN_16_1to16_2
	protected static String BankAccountNoBDNewSelA;
	protected static String cboxPaybook1A;
	protected static String cboxPaybook2A;

	// FIN_16_10
	protected static String BankAccountNoLeaseFacilitySelA;

	// Calling the constructor
	public static void readElementlocators() throws Exception {
		/* Login */
		site_logo = findElementInXLSheet(getParameterFinance, "site_logo");
		txt_username = findElementInXLSheet(getParameterFinance, "txt_username");
		txt_password = findElementInXLSheet(getParameterFinance, "txt_password");
		btn_login = findElementInXLSheet(getParameterFinance, "btn_login");
		link_home = findElementInXLSheet(getParameterFinance, "link_home");
		div_loginVerification = findElementInXLSheet(getParameterFinance, "div_loginVerification");

		/* Com_TC_002 */
		navigation_pane = findElementInXLSheet(getParameterFinance, "navigation_pane");
		btn_financeModule = findElementInXLSheet(getParameterFinance, "btn_financeModule");
		btn_bulkChecqu = findElementInXLSheet(getParameterFinance, "btn_bulkChecqu");

		/* Fin_TC_001 */
		btn_inboundPaymentAdvice = findElementInXLSheet(getParameterFinance, "btn_inboundPaymentAdvice");
		btn_newInboundPaymentAdvice = findElementInXLSheet(getParameterFinance, "btn_newInboundPaymentAdvice");
		btn_CusAdvanceJourneyIPA = findElementInXLSheet(getParameterFinance, "btn_CusAdvanceJourneyIPA");
		lnl_postBusinessUnitIPA = findElementInXLSheet(getParameterFinance, "lnl_postBusinessUnitIPA");
		drop_postBusinessUnit = findElementInXLSheet(getParameterFinance, "drop_postBusinessUnit");
		btn_documentDataIPA = findElementInXLSheet(getParameterFinance, "btn_documentDataIPA");
		btn_docDateIPA = findElementInXLSheet(getParameterFinance, "btn_docDateIPA");
		btn_postDateIPA = findElementInXLSheet(getParameterFinance, "btn_postDateIPA");
		btn_dueDateIPA = findElementInXLSheet(getParameterFinance, "btn_dueDateIPA");
		btn_searchCustomerIPA = findElementInXLSheet(getParameterFinance, "btn_searchCustomerIPA");
		txt_cusAccountSearchIPA = findElementInXLSheet(getParameterFinance, "txt_cusAccountSearchIPA");
		lnk_resultCustomerAccountIPA = findElementInXLSheet(getParameterFinance, "lnk_resultCustomerAccountIPA");
		txt_refNoIPA = findElementInXLSheet(getParameterFinance, "txt_refNoIPA");
		txt_descIPA = findElementInXLSheet(getParameterFinance, "txt_descIPA");
		drop_cusrencyIPA = findElementInXLSheet(getParameterFinance, "drop_cusrencyIPA");
		drop_taxIPA = findElementInXLSheet(getParameterFinance, "drop_taxIPA");
		txt_amountIPA = findElementInXLSheet(getParameterFinance, "txt_amountIPA");
		btn_calenderBackIAP = findElementInXLSheet(getParameterFinance, "btn_calenderBackIAP");
		btn_calenderForwardIAP = findElementInXLSheet(getParameterFinance, "btn_calenderForwardIAP");
		tab_subbaryIPA = findElementInXLSheet(getParameterFinance, "tab_subbaryIPA");
		btn_checkout = findElementInXLSheet(getParameterFinance, "btn_checkout");
		btn_draft = findElementInXLSheet(getParameterFinance, "btn_draft");
		btn_reelese = findElementInXLSheet(getParameterFinance, "btn_reelese");
		btn_print = findElementInXLSheet(getParameterFinance, "btn_print");
		lbl_docNoIPA = findElementInXLSheet(getParameterFinance, "lbl_docNoIPA");

		/* Fin_TC_002 */
		btn_searchGlCRV = findElementInXLSheet(getParameterFinance, "btn_searchGlCRV");
		txt_amountCustomerDebirMemoCRV = findElementInXLSheet(getParameterFinance, "txt_amountCustomerDebirMemoCRV");
		txt_secrhGlCRV = findElementInXLSheet(getParameterFinance, "txt_secrhGlCRV");
		lnk_resultGL = findElementInXLSheet(getParameterFinance, "lnk_resultGL");
		btn_actionCRV = findElementInXLSheet(getParameterFinance, "btn_actionCRV");
		btn_converToInboundPaymentCRV = findElementInXLSheet(getParameterFinance, "btn_converToInboundPaymentCRV");
		drop_paidCurrencyCRV = findElementInXLSheet(getParameterFinance, "drop_paidCurrencyCRV");
		drop_paymentMethodCRV = findElementInXLSheet(getParameterFinance, "drop_paymentMethodCRV");
		drop_bankNAmeCRV = findElementInXLSheet(getParameterFinance, "drop_bankNAmeCRV");
		txt_checkNoCRV = findElementInXLSheet(getParameterFinance, "txt_checkNoCRV");
		plicker_dateCheckDateCRV = findElementInXLSheet(getParameterFinance, "plicker_dateCheckDateCRV");
		btn_detailTabCRV = findElementInXLSheet(getParameterFinance, "btn_detailTabCRV");
		drop_filterCurrencyCRV = findElementInXLSheet(getParameterFinance, "drop_filterCurrencyCRV");
		txt_payingAmountCRV = findElementInXLSheet(getParameterFinance, "txt_payingAmountCRV");
		btn_viewAllPendings = findElementInXLSheet(getParameterFinance, "btn_viewAllPendings");
		chk_adviceCRV = findElementInXLSheet(getParameterFinance, "chk_adviceCRV");
		txt_paidAmountCRV = findElementInXLSheet(getParameterFinance, "txt_paidAmountCRV");
		btn_journel = findElementInXLSheet(getParameterFinance, "btn_journel");
		btn_cusDebitMemo = findElementInXLSheet(getParameterFinance, "btn_cusDebitMemo");
		btn_summaryTab = findElementInXLSheet(getParameterFinance, "btn_summaryTab");
		btn_documentDataCRV = findElementInXLSheet(getParameterFinance, "btn_documentDataCRV");
		header_journelEntry = findElementInXLSheet(getParameterFinance, "header_journelEntry");
		btn_viewPaymentFinTC002 = findElementInXLSheet(getParameterFinance, "btn_viewPaymentFinTC002");
		lnk_paymentNoInboundPayemnt = findElementInXLSheet(getParameterFinance, "lnk_paymentNoInboundPayemnt");

		/* Fin_TC_003 */
		btn_sallesMarketingModule = findElementInXLSheet(getParameterFinance, "btn_sallesMarketingModule");
		btn_salesInvoice = findElementInXLSheet(getParameterFinance, "btn_salesInvoice");
		btn_newSalesInvoice = findElementInXLSheet(getParameterFinance, "btn_newSalesInvoice");
		btn_slaesInvoiceServiceJourney = findElementInXLSheet(getParameterFinance, "btn_slaesInvoiceServiceJourney");
		btn_customerSearchSalesInvoice = findElementInXLSheet(getParameterFinance, "btn_customerSearchSalesInvoice");
		drop_currencySlaesInvoise = findElementInXLSheet(getParameterFinance, "drop_currencySlaesInvoise");
		drop_salesUnitSalesIvoice = findElementInXLSheet(getParameterFinance, "drop_salesUnitSalesIvoice");
		btn_productLockupSalesInvoice = findElementInXLSheet(getParameterFinance, "btn_productLockupSalesInvoice");
		txt_productSearch = findElementInXLSheet(getParameterFinance, "txt_productSearch");
		lnk_resultProduct = findElementInXLSheet(getParameterFinance, "lnk_resultProduct");
		txt_unitPriceSalesInvoice = findElementInXLSheet(getParameterFinance, "txt_unitPriceSalesInvoice");
		btn_docFlowInboundPaymentAdvice = findElementInXLSheet(getParameterFinance, "btn_docFlowInboundPaymentAdvice");
		header_inboundPaymentAdviceSalesInvoice = findElementInXLSheet(getParameterFinance,
				"header_inboundPaymentAdviceSalesInvoice");

		/* Fin_TC_004 */
		btn_bankDeposit = findElementInXLSheet(getParameterFinance, "btn_bankDeposit");
		lbl_InboundPaymentDocNo = findElementInXLSheet(getParameterFinance, "lbl_InboundPaymentDocNo");
		btn_newBankDeposit = findElementInXLSheet(getParameterFinance, "btn_newBankDeposit");
		drop_paymentMethod = findElementInXLSheet(getParameterFinance, "drop_paymentMethod");
		drop_bankNameBD = findElementInXLSheet(getParameterFinance, "drop_bankNameBD");
		drop_accountNo = findElementInXLSheet(getParameterFinance, "drop_accountNo");
		btn_viewPaymentBD = findElementInXLSheet(getParameterFinance, "btn_viewPaymentBD");
		btn_rowPaymentBD = findElementInXLSheet(getParameterFinance, "btn_rowPaymentBD");
		chk_paymentBD = findElementInXLSheet(getParameterFinance, "chk_paymentBD");

		/* Fin_TC_005 */
		txt_searchSalesInvoice = findElementInXLSheet(getParameterFinance, "txt_searchSalesInvoice");
		lnk_resultDocument = findElementInXLSheet(getParameterFinance, "lnk_resultDocument");
		btn_inboundPaymentAdviceDocFlow = findElementInXLSheet(getParameterFinance, "btn_inboundPaymentAdviceDocFlow");
		btn_setOffActionmenu = findElementInXLSheet(getParameterFinance, "btn_setOffActionmenu");
		chk_setOffInvoiceDocNumber = findElementInXLSheet(getParameterFinance, "chk_setOffInvoiceDocNumber");
		btn_setOffWindowSetOffButton = findElementInXLSheet(getParameterFinance, "btn_setOffWindowSetOffButton");
		txt_adviceDueAmountAfterSetOffSAlesInvoiceInboundPaymentAdvice = findElementInXLSheet(getParameterFinance,
				"txt_adviceDueAmountAfterSetOffSAlesInvoiceInboundPaymentAdvice");
		chk_inboundPaymentAdviceInboundPayment = findElementInXLSheet(getParameterFinance,
				"chk_inboundPaymentAdviceInboundPayment");

		/* Fin_TC_006 */
		btn_customerDebitMemoJourney = findElementInXLSheet(getParameterFinance, "btn_customerDebitMemoJourney");
		btn_nosCostAllocationDebitMemo = findElementInXLSheet(getParameterFinance, "btn_nosCostAllocationDebitMemo");
		drop_costCenterCustomerDebitMemo = findElementInXLSheet(getParameterFinance,
				"drop_costCenterCustomerDebitMemo");
		btn_addRowCostCenterCustomerDebitMemo = findElementInXLSheet(getParameterFinance,
				"btn_addRowCostCenterCustomerDebitMemo");
		btn_updateCostCenterCustomerDebitMemo = findElementInXLSheet(getParameterFinance,
				"btn_updateCostCenterCustomerDebitMemo");
		txt_amountCustomerDebirMemo6 = findElementInXLSheet(getParameterFinance, "txt_amountCustomerDebirMemo6");
		txt_costAllocationPresentageCustomerDebitMemo = findElementInXLSheet(getParameterFinance,
				"txt_costAllocationPresentageCustomerDebitMemo");

		/* Fin_TC_007 */
		btn_customerReciptVoucherJourneyboundPAyment = findElementInXLSheet(getParameterFinance,
				"btn_customerReciptVoucherJourneyboundPAyment");
		btn_customerAccountSearchCustomerReciptVoucher = findElementInXLSheet(getParameterFinance,
				"btn_customerAccountSearchCustomerReciptVoucher");
		btn_setOffNosCRV = findElementInXLSheet(getParameterFinance, "btn_setOffNosCRV");
		chk_InboundPaymentCRV7 = findElementInXLSheet(getParameterFinance, "chk_InboundPaymentCRV7");
		chk_InboundPaymentCRV78 = findElementInXLSheet(getParameterFinance, "chk_InboundPaymentCRV78");
		lbl_baseAmountCRVSetOff = findElementInXLSheet(getParameterFinance, "lbl_baseAmountCRVSetOff");
		lbl_dueAmountCRVSetOff = findElementInXLSheet(getParameterFinance, "lbl_dueAmountCRVSetOff");
		lbl_dueAccordingAdvice7 = findElementInXLSheet(getParameterFinance, "lbl_dueAccordingAdvice7");
		txt_painAmountSetOffCRV = findElementInXLSheet(getParameterFinance, "txt_painAmountSetOffCRV");
		btn_closeSetOffPopUp = findElementInXLSheet(getParameterFinance, "btn_closeSetOffPopUp");
		txt_paidAmountCRV7 = findElementInXLSheet(getParameterFinance, "txt_paidAmountCRV7");
		txt_paidAmountCRVAfterHundredRecords = findElementInXLSheet(getParameterFinance,
				"txt_paidAmountCRVAfterHundredRecords");
		btn_inboundPayment = findElementInXLSheet(getParameterFinance, "btn_inboundPayment");
		btn_newInboundPayment = findElementInXLSheet(getParameterFinance, "btn_newInboundPayment");
		header_inboundPaymentPage = findElementInXLSheet(getParameterFinance, "header_inboundPaymentPage");
		tab_summaryCRV7 = findElementInXLSheet(getParameterFinance, "tab_summaryCRV7");
		txt_billingAddressCRV = findElementInXLSheet(getParameterFinance, "txt_billingAddressCRV");
		drop_contactPersonCRV7 = findElementInXLSheet(getParameterFinance, "drop_contactPersonCRV7");
		btn_setOffUodateInsideButtonCRV = findElementInXLSheet(getParameterFinance, "btn_setOffUodateInsideButtonCRV");
		btn_scrollRight007 = findElementInXLSheet(getParameterFinance, "btn_scrollRight007");
		tbl_gridSectionRowCount_Fin_TC_007 = findElementInXLSheet(getParameterFinance,
				"tbl_gridSectionRowCount_Fin_TC_007");
		btn_setOffAfter100Records = findElementInXLSheet(getParameterFinance, "btn_setOffAfter100Records");

		/* Fin_TC_008 */
		btn_action08 = findElementInXLSheet(getParameterFinance, "btn_action08");
		btn_converToInboundPayment08 = findElementInXLSheet(getParameterFinance, "btn_converToInboundPayment08");
		btn_docDate08 = findElementInXLSheet(getParameterFinance, "btn_docDate08");
		btn_postDate08 = findElementInXLSheet(getParameterFinance, "btn_postDate08");
		btn_dueDate08 = findElementInXLSheet(getParameterFinance, "btn_dueDate08");
		drop_paidCurrency08 = findElementInXLSheet(getParameterFinance, "drop_paidCurrency08");
		drop_paymentMethod08 = findElementInXLSheet(getParameterFinance, "drop_paymentMethod08");
		btn_detailsPage08 = findElementInXLSheet(getParameterFinance, "btn_detailsPage08");
		txt_payingAmount08 = findElementInXLSheet(getParameterFinance, "txt_payingAmount08");
		chk_IPA08 = findElementInXLSheet(getParameterFinance, "chk_IPA08");
		txt_paidAmount08 = findElementInXLSheet(getParameterFinance, "txt_paidAmount08");
		txt_searchInboundPayment = findElementInXLSheet(getParameterFinance, "txt_searchInboundPayment");
		lnk_searchResultInboundPayment = findElementInXLSheet(getParameterFinance, "lnk_searchResultInboundPayment");
		tab_summary8 = findElementInXLSheet(getParameterFinance, "tab_summary8");
		drom_customer8 = findElementInXLSheet(getParameterFinance, "drom_customer8");
		drop_bankCheque8 = findElementInXLSheet(getParameterFinance, "drop_bankCheque8");
		txt_chequNo8 = findElementInXLSheet(getParameterFinance, "txt_chequNo8");
		drop_payMethodBankDeposi = findElementInXLSheet(getParameterFinance, "drop_payMethodBankDeposi");
		drop_bank = findElementInXLSheet(getParameterFinance, "drop_bank");
		drop_bankAccountNo = findElementInXLSheet(getParameterFinance, "drop_bankAccountNo");
		btn_viewBankDeposit = findElementInXLSheet(getParameterFinance, "btn_viewBankDeposit");
		chk_voucherBankDeposit = findElementInXLSheet(getParameterFinance, "chk_voucherBankDeposit");

		/* Fin_TC_010 */
		btn_outboundPaymentAdvice = findElementInXLSheet(getParameterFinance, "btn_outboundPaymentAdvice");
		btn_newOutboundPaymentAdvice = findElementInXLSheet(getParameterFinance, "btn_newOutboundPaymentAdvice");
		btn_vendorAdvanceJourney = findElementInXLSheet(getParameterFinance, "btn_vendorAdvanceJourney");
		btn_postBusinessUnit = findElementInXLSheet(getParameterFinance, "btn_postBusinessUnit");
		drop_posBusinessUnit = findElementInXLSheet(getParameterFinance, "drop_posBusinessUnit");
		btn_docDate10 = findElementInXLSheet(getParameterFinance, "btn_docDate10");
		btn_postDate10 = findElementInXLSheet(getParameterFinance, "btn_postDate10");
		btn_dueDate10 = findElementInXLSheet(getParameterFinance, "btn_dueDate10");
		btn_searchVendorOPA = findElementInXLSheet(getParameterFinance, "btn_searchVendorOPA");
		txt_serchVendorOPA = findElementInXLSheet(getParameterFinance, "txt_serchVendorOPA");
		lnk_resultVendorOPA = findElementInXLSheet(getParameterFinance, "lnk_resultVendorOPA");
		txt_referenceCodeOPA = findElementInXLSheet(getParameterFinance, "txt_referenceCodeOPA");
		txt_descriptionOPA = findElementInXLSheet(getParameterFinance, "txt_descriptionOPA");
		drop_currencyOPA = findElementInXLSheet(getParameterFinance, "drop_currencyOPA");
		drop_taxOPA = findElementInXLSheet(getParameterFinance, "drop_taxOPA");
		txt_amountOPA = findElementInXLSheet(getParameterFinance, "txt_amountOPA");
		drop_contactPerson = findElementInXLSheet(getParameterFinance, "drop_contactPerson");
		txt_billingAddress = findElementInXLSheet(getParameterFinance, "txt_billingAddress");

		/* Fin_TC_011 */
		txt_searchOutboundPaymentAdvice = findElementInXLSheet(getParameterFinance, "txt_searchOutboundPaymentAdvice");
		lnk_outboundPaymentAdvice = findElementInXLSheet(getParameterFinance, "lnk_outboundPaymentAdvice");
		btn_action = findElementInXLSheet(getParameterFinance, "btn_action");
		btn_convertToOutboundPaymentAdvice = findElementInXLSheet(getParameterFinance,
				"btn_convertToOutboundPaymentAdvice");
		btn_docDate = findElementInXLSheet(getParameterFinance, "btn_docDate");
		btn_calenderBack = findElementInXLSheet(getParameterFinance, "btn_calenderBack");
		btn_calenderForward = findElementInXLSheet(getParameterFinance, "btn_calenderForward");
		btn_postDate = findElementInXLSheet(getParameterFinance, "btn_postDate");
		btn_dueDate = findElementInXLSheet(getParameterFinance, "btn_dueDate");
		drop_currency = findElementInXLSheet(getParameterFinance, "drop_currency");
		drop_payBook = findElementInXLSheet(getParameterFinance, "drop_payBook");
		tab_detailOutboundPayment = findElementInXLSheet(getParameterFinance, "tab_detailOutboundPayment");
		drop_filterCurrencyOutboundPaymnet = findElementInXLSheet(getParameterFinance,
				"drop_filterCurrencyOutboundPaymnet");
		chk_choseOPA = findElementInXLSheet(getParameterFinance, "chk_choseOPA");
		lbl_docNo = findElementInXLSheet(getParameterFinance, "lbl_docNo");
		btn_viewButonOutboundPayment = findElementInXLSheet(getParameterFinance, "btn_viewButonOutboundPayment");
		txt_setDescriptionJournelEntry = findElementInXLSheet(getParameterFinance, "txt_setDescriptionJournelEntry");

		/* Fin_TC_012 */
		btn_procument = findElementInXLSheet(getParameterFinance, "btn_procument");
		btn_purchaseInvoice = findElementInXLSheet(getParameterFinance, "btn_purchaseInvoice");
		btn_newPurchaseInvoice = findElementInXLSheet(getParameterFinance, "btn_newPurchaseInvoice");
		btn_purchaseInvoiceJourney = findElementInXLSheet(getParameterFinance, "btn_purchaseInvoiceJourney");
		btn_serchVendor = findElementInXLSheet(getParameterFinance, "btn_serchVendor");
		txt_vendorSearch = findElementInXLSheet(getParameterFinance, "txt_vendorSearch");
		lnk_resultVendorSearch = findElementInXLSheet(getParameterFinance, "lnk_resultVendorSearch");
		btn_pdPostDate = findElementInXLSheet(getParameterFinance, "btn_pdPostDate");
		btn_productLoockup = findElementInXLSheet(getParameterFinance, "btn_productLoockup");
		txt_unitPricePurchaseInvoice = findElementInXLSheet(getParameterFinance, "txt_unitPricePurchaseInvoice");
		btn_docFlow = findElementInXLSheet(getParameterFinance, "btn_docFlow");
		header_page = findElementInXLSheet(getParameterFinance, "header_page");
		lbl_docStatus = findElementInXLSheet(getParameterFinance, "lbl_docStatus");
		btn_dateApply = findElementInXLSheet(getParameterFinance, "btn_dateApply");
		btn_documentValueTotal = findElementInXLSheet(getParameterFinance, "btn_documentValueTotal");

		/* Fin_TC_014 */
		btn_vendorCreditMemoJourney = findElementInXLSheet(getParameterFinance, "btn_vendorCreditMemoJourney");
		btn_vendorLookupCreditMemo = findElementInXLSheet(getParameterFinance, "btn_vendorLookupCreditMemo");
		txt_referenceCode = findElementInXLSheet(getParameterFinance, "txt_referenceCode");
		drop_tax = findElementInXLSheet(getParameterFinance, "drop_tax");
		btn_glLookupCreditMemory = findElementInXLSheet(getParameterFinance, "btn_glLookupCreditMemory");
		txt_GL = findElementInXLSheet(getParameterFinance, "txt_GL");
		txt_amountCreditMemo = findElementInXLSheet(getParameterFinance, "txt_amountCreditMemo");
		ico_noCostAlloacation = findElementInXLSheet(getParameterFinance, "ico_noCostAlloacation");
		drop_costCenter = findElementInXLSheet(getParameterFinance, "drop_costCenter");
		txt_apportionPresentage = findElementInXLSheet(getParameterFinance, "txt_apportionPresentage");
		btn_addRecordApportion = findElementInXLSheet(getParameterFinance, "btn_addRecordApportion");
		btn_update = findElementInXLSheet(getParameterFinance, "btn_update");
		txt_description = findElementInXLSheet(getParameterFinance, "txt_description");
		header_releasedOutboundPaymentAdvice = findElementInXLSheet(getParameterFinance,
				"header_releasedOutboundPaymentAdvice");

		/* Fin_TC_015 */
		btn_vendorPaymentVoucherJourney = findElementInXLSheet(getParameterFinance, "btn_vendorPaymentVoucherJourney");
		btn_vendorSearchLookupVPV = findElementInXLSheet(getParameterFinance, "btn_vendorSearchLookupVPV");
		tab_payment = findElementInXLSheet(getParameterFinance, "tab_payment");
		chk_outboundPaymentAdvice = findElementInXLSheet(getParameterFinance, "chk_outboundPaymentAdvice");
		btn_setOffNos = findElementInXLSheet(getParameterFinance, "btn_setOffNos");
		btn_outboundPayment = findElementInXLSheet(getParameterFinance, "btn_outboundPayment");
		btn_addNew = findElementInXLSheet(getParameterFinance, "btn_addNew");
		drop_filterCurrency = findElementInXLSheet(getParameterFinance, "drop_filterCurrency");
		header_outboundPaymentSetOff = findElementInXLSheet(getParameterFinance, "header_outboundPaymentSetOff");
		txt_paidAMountOutboundPaymentVendor = findElementInXLSheet(getParameterFinance,
				"txt_paidAMountOutboundPaymentVendor");
		lbl_dueAmountOutboundPaymentVendor = findElementInXLSheet(getParameterFinance,
				"lbl_dueAmountOutboundPaymentVendor");
		btn_viewPendingAdvices = findElementInXLSheet(getParameterFinance, "btn_viewPendingAdvices");

		/* Fin_TC_016 */
		btn_letterOfGurentee = findElementInXLSheet(getParameterFinance, "btn_letterOfGurentee");
		btn_new = findElementInXLSheet(getParameterFinance, "btn_new");
		txt_originalDocNo = findElementInXLSheet(getParameterFinance, "txt_originalDocNo");
		drop_requesterType = findElementInXLSheet(getParameterFinance, "drop_requesterType");
		btn_vendorLookupLOG = findElementInXLSheet(getParameterFinance, "btn_vendorLookupLOG");
		txt_vendor = findElementInXLSheet(getParameterFinance, "txt_vendor");
		lnk_resultVendor = findElementInXLSheet(getParameterFinance, "lnk_resultVendor");
		btn_documentSearchNosLOG = findElementInXLSheet(getParameterFinance, "btn_documentSearchNosLOG");
		txt_serchPurchaseOrder = findElementInXLSheet(getParameterFinance, "txt_serchPurchaseOrder");
		lnk_serchResultPurchaseOrder = findElementInXLSheet(getParameterFinance, "lnk_serchResultPurchaseOrder");
		btn_applyPurchaseOrder = findElementInXLSheet(getParameterFinance, "btn_applyPurchaseOrder");
		drop_gurenteeGroup = findElementInXLSheet(getParameterFinance, "drop_gurenteeGroup");
		calende_openDate = findElementInXLSheet(getParameterFinance, "calende_openDate");
		calende_endDate = findElementInXLSheet(getParameterFinance, "calende_endDate");
		txt_amount = findElementInXLSheet(getParameterFinance, "txt_amount");
		lbl_docStatus1 = findElementInXLSheet(getParameterFinance, "lbl_docStatus1");
		btn_documentSearchLookup16 = findElementInXLSheet(getParameterFinance, "btn_documentSearchLookup16");
		btn_creditExtentionLetterOFGurentee = findElementInXLSheet(getParameterFinance,
				"btn_creditExtentionLetterOFGurentee");

		/* Fin_TC_017 */
		txt_letterPfGurenteeSearch = findElementInXLSheet(getParameterFinance, "txt_letterPfGurenteeSearch");
		lnk_letterOfGurenteeResult = findElementInXLSheet(getParameterFinance, "lnk_letterOfGurenteeResult");
		btn_convertToBankFacilityAgreement = findElementInXLSheet(getParameterFinance,
				"btn_convertToBankFacilityAgreement");
		txt_originalDocNoLetterOfGurentee = findElementInXLSheet(getParameterFinance,
				"txt_originalDocNoLetterOfGurentee");
		drop_bankBFA = findElementInXLSheet(getParameterFinance, "drop_bankBFA");
		txt_amountBFA = findElementInXLSheet(getParameterFinance, "txt_amountBFA");
		txt_startDate = findElementInXLSheet(getParameterFinance, "txt_startDate");
		txt_endDate = findElementInXLSheet(getParameterFinance, "txt_endDate");
		btn_date01 = findElementInXLSheet(getParameterFinance, "btn_date01");
		lbl_journelEntryFirstRowDebit = findElementInXLSheet(getParameterFinance, "lbl_journelEntryFirstRowDebit");
		lbl_journelEntrySecondRowCredit = findElementInXLSheet(getParameterFinance, "lbl_journelEntrySecondRowCredit");
		lbl_journelEntrySecondRowCredit = findElementInXLSheet(getParameterFinance, "lbl_journelEntrySecondRowCredit");
		txt_referenceDocumenrLOG = findElementInXLSheet(getParameterFinance, "txt_referenceDocumenrLOG");
		header_page2 = findElementInXLSheet(getParameterFinance, "header_page2");
		drop_bankFacilityGroup = findElementInXLSheet(getParameterFinance, "drop_bankFacilityGroup");

		/* Fin_TC_018 */
		lbl_OutboundPaymentNoLOG = findElementInXLSheet(getParameterFinance, "lbl_OutboundPaymentNoLOG");
		btn_releeseLOGOutboundPayment = findElementInXLSheet(getParameterFinance, "btn_releeseLOGOutboundPayment");
		lnk_resultOutboundPayment = findElementInXLSheet(getParameterFinance, "lnk_resultOutboundPayment");
		txt_searchOutboundPayment = findElementInXLSheet(getParameterFinance, "txt_searchOutboundPayment");

		/* Fin_TC_019 */
		btn_bankAdjustment = findElementInXLSheet(getParameterFinance, "btn_bankAdjustment");
		chk_facilitySettlement = findElementInXLSheet(getParameterFinance, "chk_facilitySettlement");
		btn_bankFacilityLookup = findElementInXLSheet(getParameterFinance, "btn_bankFacilityLookup");
		txt_bankFacilityAgreementSearch = findElementInXLSheet(getParameterFinance, "txt_bankFacilityAgreementSearch");
		lnl_bankFacilityAgreementSearchResult = findElementInXLSheet(getParameterFinance,
				"lnl_bankFacilityAgreementSearchResult");
		drop_settlementTypeBankAdjustment = findElementInXLSheet(getParameterFinance,
				"drop_settlementTypeBankAdjustment");
		btn_costAllocationBankAdjustment = findElementInXLSheet(getParameterFinance,
				"btn_costAllocationBankAdjustment");
		btn_glLookupBAnkAdjustment = findElementInXLSheet(getParameterFinance, "btn_glLookupBAnkAdjustment");
		txt_description1 = findElementInXLSheet(getParameterFinance, "txt_description1");
		lbl_minusValueCheck = findElementInXLSheet(getParameterFinance, "lbl_minusValueCheck");
		header_taxBreackdown = findElementInXLSheet(getParameterFinance, "header_taxBreackdown");
		close_headerPopup = findElementInXLSheet(getParameterFinance, "close_headerPopup");
		btn_btnTaxBrackDownBankAdjustment = findElementInXLSheet(getParameterFinance,
				"btn_btnTaxBrackDownBankAdjustment");
		td_taxNameCell = findElementInXLSheet(getParameterFinance, "td_taxNameCell");

		/* Fin_TC_020 */
		btn_bankReconcillation = findElementInXLSheet(getParameterFinance, "btn_bankReconcillation");
		btn_newBankReconcilation = findElementInXLSheet(getParameterFinance, "btn_newBankReconcilation");
		btn_statementDateBankReconcilation = findElementInXLSheet(getParameterFinance,
				"btn_statementDateBankReconcilation");
		lbl_lastBalaceBankReconcilation = findElementInXLSheet(getParameterFinance, "lbl_lastBalaceBankReconcilation");
		txt_statementBalance = findElementInXLSheet(getParameterFinance, "txt_statementBalance");
		lbl_unReconciledAmount = findElementInXLSheet(getParameterFinance, "lbl_unReconciledAmount");
		btn_adjustentBankReconcilation = findElementInXLSheet(getParameterFinance, "btn_adjustentBankReconcilation");
		btn_descriptionLookupBAnkReconcilationAdjustment = findElementInXLSheet(getParameterFinance,
				"btn_descriptionLookupBAnkReconcilationAdjustment");
		txt_descriptionAdjustmenrBankReconcilation = findElementInXLSheet(getParameterFinance,
				"txt_descriptionAdjustmenrBankReconcilation");
		btn_updateAdjustmentBankReconcilation = findElementInXLSheet(getParameterFinance,
				"btn_updateAdjustmentBankReconcilation");
		gl_nosAdjustmentBankReconcilation = findElementInXLSheet(getParameterFinance,
				"gl_nosAdjustmentBankReconcilation");
		txt_amountUluBankReconsillAdjustment = findElementInXLSheet(getParameterFinance,
				"txt_amountUluBankReconsillAdjustment");
		chk_advicePathBankReconcilation = findElementInXLSheet(getParameterFinance, "chk_advicePathBankReconcilation");
		lbl_amountAdviceBankReconcilation = findElementInXLSheet(getParameterFinance,
				"lbl_amountAdviceBankReconcilation");
		lbl_reconcilAmount = findElementInXLSheet(getParameterFinance, "lbl_reconcilAmount");
		btn_applyDescriptionBankeconcilation = findElementInXLSheet(getParameterFinance,
				"btn_applyDescriptionBankeconcilation");
		btn_costAllocationAdjustmentBankReconcilation = findElementInXLSheet(getParameterFinance,
				"btn_costAllocationAdjustmentBankReconcilation");
		btn_updateCostCenter = findElementInXLSheet(getParameterFinance, "btn_updateCostCenter");
		btn_scrollDownArrowBankReconsilGrid = findElementInXLSheet(getParameterFinance,
				"btn_scrollDownArrowBankReconsilGrid");
		div_reconcilAdvice = findElementInXLSheet(getParameterFinance, "div_reconcilAdvice");

		/* Fin_TC_021 */
		btn_pettyCash = findElementInXLSheet(getParameterFinance, "btn_pettyCash");
		btn_newPettyCash = findElementInXLSheet(getParameterFinance, "btn_newPettyCash");
		drop_pettyCashAccount = findElementInXLSheet(getParameterFinance, "drop_pettyCashAccount");
		btn_lookupPaidTo = findElementInXLSheet(getParameterFinance, "btn_lookupPaidTo");
		txt_searchEmployee = findElementInXLSheet(getParameterFinance, "txt_searchEmployee");
		lnk_resultEmployye = findElementInXLSheet(getParameterFinance, "lnk_resultEmployye");
		txt_descPettyCash = findElementInXLSheet(getParameterFinance, "txt_descPettyCash");
		drop_pettyCashType = findElementInXLSheet(getParameterFinance, "drop_pettyCashType");
		txt_amountPettyCash = findElementInXLSheet(getParameterFinance, "txt_amountPettyCash");
		nos_costApportionPettyCash = findElementInXLSheet(getParameterFinance, "nos_costApportionPettyCash");
		drop_costCenterPettyCash = findElementInXLSheet(getParameterFinance, "drop_costCenterPettyCash");
		txt_costPresentagePettyCash = findElementInXLSheet(getParameterFinance, "txt_costPresentagePettyCash");
		btn_journelEntryPettyCashHeaderTExt = findElementInXLSheet(getParameterFinance,
				"btn_journelEntryPettyCashHeaderTExt");

		/* Fin_TC_023 */
		btn_glLookupPettyCash = findElementInXLSheet(getParameterFinance, "btn_glLookupPettyCash");
		nos_narrationPettyCash = findElementInXLSheet(getParameterFinance, "nos_narrationPettyCash");
		txt_descriptionNarationPettyCash = findElementInXLSheet(getParameterFinance,
				"txt_descriptionNarationPettyCash");
		btn_applyDescriptionPettyCash = findElementInXLSheet(getParameterFinance, "btn_applyDescriptionPettyCash");
		btn_iouLookup = findElementInXLSheet(getParameterFinance, "btn_iouLookup");
		lnk_iouResult = findElementInXLSheet(getParameterFinance, "lnk_iouResult");

		/* Smoke_Finance_001 */
		btn_bankaAdjustment = findElementInXLSheet(getParameterFinance, "btn_bankaAdjustment");
		btn_newBankAdjustment = findElementInXLSheet(getParameterFinance, "btn_newBankAdjustment");
		drop_bankBankAdjustmentSmoke = findElementInXLSheet(getParameterFinance, "drop_bankBankAdjustmentSmoke");
		drop_acoountNoBankAdjustmentSmoke = findElementInXLSheet(getParameterFinance,
				"drop_acoountNoBankAdjustmentSmoke");
		btn_glAccountLookupBankAdjustmentSmoke = findElementInXLSheet(getParameterFinance,
				"btn_glAccountLookupBankAdjustmentSmoke");
		txt_glInFrontPageBankAdjustmentSmoke = findElementInXLSheet(getParameterFinance,
				"txt_glInFrontPageBankAdjustmentSmoke");
		txt_amountBankAdjustmentSmoke = findElementInXLSheet(getParameterFinance, "txt_amountBankAdjustmentSmoke");
		btn_summaryBankAdjustmentSmoke = findElementInXLSheet(getParameterFinance, "btn_summaryBankAdjustmentSmoke");

		/* Smoke_Finance_011 */
		btn_journelEntryPage = findElementInXLSheet(getParameterFinance, "btn_journelEntryPage");
		btn_newJournelEntryPage = findElementInXLSheet(getParameterFinance, "btn_newJournelEntryPage");
		btn_description = findElementInXLSheet(getParameterFinance, "btn_description");
		txt_descriptionJournelEntry = findElementInXLSheet(getParameterFinance, "txt_descriptionJournelEntry");
		btn_applyDescriptionJournelEntry = findElementInXLSheet(getParameterFinance,
				"btn_applyDescriptionJournelEntry");
		btn_addRowJournelEntry = findElementInXLSheet(getParameterFinance, "btn_addRowJournelEntry");
		btn_addRow2JournelEntry = findElementInXLSheet(getParameterFinance, "btn_addRow2JournelEntry");
		btn_serchLoockupGLJournelEntry = findElementInXLSheet(getParameterFinance, "btn_serchLoockupGLJournelEntry");
		btn_serchLoockupGLJournelEntry2 = findElementInXLSheet(getParameterFinance, "btn_serchLoockupGLJournelEntry2");
		btn_serchLoockupGLJournelEntry3 = findElementInXLSheet(getParameterFinance, "btn_serchLoockupGLJournelEntry3");
		txt_value1JjornelEntry = findElementInXLSheet(getParameterFinance, "txt_value1JjornelEntry");
		txt_value2JjornelEntry = findElementInXLSheet(getParameterFinance, "txt_value2JjornelEntry");
		txt_value3JjornelEntry = findElementInXLSheet(getParameterFinance, "txt_value3JjornelEntry");
		lbl_value1JournelAccount = findElementInXLSheet(getParameterFinance, "lbl_value1JournelAccount");
		lbl_value2JournelAccount = findElementInXLSheet(getParameterFinance, "lbl_value2JournelAccount");
		lbl_value3JournelAccount = findElementInXLSheet(getParameterFinance, "lbl_value3JournelAccount");

		/* Smoke_Finance_02 */
		btn_bankFacilityAgreement = findElementInXLSheet(getParameterFinance, "btn_bankFacilityAgreement");
		btn_newBankFacilityAgreement = findElementInXLSheet(getParameterFinance, "btn_newBankFacilityAgreement");
		txt_originalDocNoBFA = findElementInXLSheet(getParameterFinance, "txt_originalDocNoBFA");
		drop_bankFacilityGroupBFA = findElementInXLSheet(getParameterFinance, "drop_bankFacilityGroupBFA");
		txt_startDateBFA = findElementInXLSheet(getParameterFinance, "txt_startDateBFA");
		txt_endDateBFA = findElementInXLSheet(getParameterFinance, "txt_endDateBFA");
		lbl_loanCreationConfirmation1 = findElementInXLSheet(getParameterFinance, "lbl_loanCreationConfirmation1");
		lbl_loanCreationConfirmation2 = findElementInXLSheet(getParameterFinance, "lbl_loanCreationConfirmation2");

		/* Common customer creation */
		btn_accountInformation = findElementInXLSheet(getParameterFinance, "btn_accountInformation");
		btn_newAccountCustomerAccount = findElementInXLSheet(getParameterFinance, "btn_newAccountCustomerAccount");
		txt_accountNameCustomerAccount = findElementInXLSheet(getParameterFinance, "txt_accountNameCustomerAccount");
		drop_accountGtopuCustomerAccount = findElementInXLSheet(getParameterFinance,
				"drop_accountGtopuCustomerAccount");

		/* Regression */
		/* FIN_BD_001 */
		header_bankDeposit = findElementInXLSheet(getParameterFinance, "header_bankDeposit");
		header_newBankDeposit = findElementInXLSheet(getParameterFinance, "header_newBankDeposit");
		btn_closeMainValidation = findElementInXLSheet(getParameterFinance, "btn_closeMainValidation");
		lbl_mainValidation = findElementInXLSheet(getParameterFinance, "lbl_mainValidation");
		lbl_errorPayMethodDropDown = findElementInXLSheet(getParameterFinance, "lbl_errorPayMethodDropDown");
		drop_payMethodWithErrorBorder = findElementInXLSheet(getParameterFinance, "drop_payMethodWithErrorBorder");
		lbl_errorBankDropDown = findElementInXLSheet(getParameterFinance, "lbl_errorBankDropDown");
		drop_BankWithErrorBorder = findElementInXLSheet(getParameterFinance, "drop_BankWithErrorBorder");
		lbl_errorBankAccountDropDown = findElementInXLSheet(getParameterFinance, "lbl_errorBankAccountDropDown");
		btn_bankDepositGridErrorOpen = findElementInXLSheet(getParameterFinance, "btn_bankDepositGridErrorOpen");
		lbl_gridErroeBallonBankDeposit = findElementInXLSheet(getParameterFinance, "lbl_gridErroeBallonBankDeposit");

		/* FIN_BD_3_4_5_6_7 */
		txt_descriptinArea = findElementInXLSheet(getParameterFinance, "txt_descriptinArea");
		btn_cashBankBook = findElementInXLSheet(getParameterFinance, "btn_cashBankBook");
		txt_searchCashBankBook = findElementInXLSheet(getParameterFinance, "txt_searchCashBankBook");
		count_bankAccount = findElementInXLSheet(getParameterFinance, "count_bankAccount");
		td_bankAccountRowReplace = findElementInXLSheet(getParameterFinance, "td_bankAccountRowReplace");
		drop_cashBookFilter = findElementInXLSheet(getParameterFinance, "drop_cashBookFilter");

		/* FIN_BD_9 */
		txt_chequeFilterBy = findElementInXLSheet(getParameterFinance, "txt_chequeFilterBy");
		count_paymentBankDeposit = findElementInXLSheet(getParameterFinance, "count_paymentBankDeposit");

		/* FIN_BD_12_13_14_15_17 */
		header_releasedInboundPaymentAdvice = findElementInXLSheet(getParameterFinance,
				"header_releasedInboundPaymentAdvice");
		header_releasedInboundPayment = findElementInXLSheet(getParameterFinance, "header_releasedInboundPayment");
		chk_firstAdviceOnGridInboundPayment = findElementInXLSheet(getParameterFinance,
				"chk_firstAdviceOnGridInboundPayment");
		chk_firstAdviceOnGridInBankDeposit = findElementInXLSheet(getParameterFinance,
				"chk_firstAdviceOnGridInBankDeposit");
		header_draftedBankDeposit = findElementInXLSheet(getParameterFinance, "header_draftedBankDeposit");
		txt_abountSelectedInboundPaymentBankDeposit = findElementInXLSheet(getParameterFinance,
				"txt_abountSelectedInboundPaymentBankDeposit");
		btn_edit = findElementInXLSheet(getParameterFinance, "btn_edit");
		btn_updateMainDoc = findElementInXLSheet(getParameterFinance, "btn_updateMainDoc");
		div_updatedAmountBankDeposit = findElementInXLSheet(getParameterFinance, "div_updatedAmountBankDeposit");
		btn_updateAndNew = findElementInXLSheet(getParameterFinance, "btn_updateAndNew");
		btn_duplicate = findElementInXLSheet(getParameterFinance, "btn_duplicate");
		lbl_draftDescriptionOnHistoryTab = findElementInXLSheet(getParameterFinance,
				"lbl_draftDescriptionOnHistoryTab");
		lbl_updateDescriptionOnHistoryTab = findElementInXLSheet(getParameterFinance,
				"lbl_updateDescriptionOnHistoryTab");
		lbl_draftOnHistoryTab = findElementInXLSheet(getParameterFinance, "lbl_draftOnHistoryTab");
		lbl_updateOnHistoryTab = findElementInXLSheet(getParameterFinance, "lbl_updateOnHistoryTab");
		btn_history = findElementInXLSheet(getParameterFinance, "btn_history");

		/* FIN_BD_16 */
		btn_delete = findElementInXLSheet(getParameterFinance, "btn_delete");
		btn_yes = findElementInXLSheet(getParameterFinance, "btn_yes");
		header_deletedBankDeposit = findElementInXLSheet(getParameterFinance, "header_deletedBankDeposit");

		/* FIN_BD_18 */
		btn_draftAndNew = findElementInXLSheet(getParameterFinance, "btn_draftAndNew");
		label_recentlyDraftedBankDeposit = findElementInXLSheet(getParameterFinance,
				"label_recentlyDraftedBankDeposit");
		lnl_releventDraftedBandDepositDocReplace = findElementInXLSheet(getParameterFinance,
				"lnl_releventDraftedBandDepositDocReplace");
		txt_descriptionDraftedBankDeposit = findElementInXLSheet(getParameterFinance,
				"txt_descriptionDraftedBankDeposit");

		/* FIN_BD_19_20 */
		btn_reverse = findElementInXLSheet(getParameterFinance, "btn_reverse");
		btn_reverseSecond = findElementInXLSheet(getParameterFinance, "btn_reverseSecond");
		header_reversedBankDeposit = findElementInXLSheet(getParameterFinance, "header_reversedBankDeposit");

		/* FIN_BD_21 */
		btn_curencyExchangeRate = findElementInXLSheet(getParameterFinance, "btn_curencyExchangeRate");
		txt_usdBuyingRate = findElementInXLSheet(getParameterFinance, "txt_usdBuyingRate");
		txt_usdSellingRate = findElementInXLSheet(getParameterFinance, "txt_usdSellingRate");
		btn_update2 = findElementInXLSheet(getParameterFinance, "btn_update2");
		txt_amountWithGL = findElementInXLSheet(getParameterFinance, "txt_amountWithGL");
		txt_customerInboundPaymentFrontPage = findElementInXLSheet(getParameterFinance,
				"txt_customerInboundPaymentFrontPage");
		drop_paidCurrency = findElementInXLSheet(getParameterFinance, "drop_paidCurrency");
		header_inboundPaymentNewCustomerRecipt = findElementInXLSheet(getParameterFinance,
				"header_inboundPaymentNewCustomerRecipt");
		drop_filterCurrencyBankDeposit = findElementInXLSheet(getParameterFinance, "drop_filterCurrencyBankDeposit");

		/* FIN_BD_22 */
		drop_cashAccount = findElementInXLSheet(getParameterFinance, "drop_cashAccount");
		div_paymentDocumentListBankDepositDocReplace = findElementInXLSheet(getParameterFinance,
				"div_paymentDocumentListBankDepositDocReplace");
		chk_paymentDocumentListBankDepositDocReplace = findElementInXLSheet(getParameterFinance,
				"chk_paymentDocumentListBankDepositDocReplace");
		txt_depositAmount = findElementInXLSheet(getParameterFinance, "txt_depositAmount");

		/* FIN_BD_23 */
		btn_vendorLoockup = findElementInXLSheet(getParameterFinance, "btn_vendorLoockup");
		drop_payMethodVendorRefund = findElementInXLSheet(getParameterFinance, "drop_payMethodVendorRefund");
		btn_glLookupVendorRefund = findElementInXLSheet(getParameterFinance, "btn_glLookupVendorRefund");
		txt_amountVendorRefund = findElementInXLSheet(getParameterFinance, "txt_amountVendorRefund");
		btn_journeyVendorRefund = findElementInXLSheet(getParameterFinance, "btn_journeyVendorRefund");
		lnk_inforOnLookupInformationReplace = findElementInXLSheet(getParameterFinance,
				"lnk_inforOnLookupInformationReplace");

		/* FIN_BD_24 */
		btn_journeyReciptVoucher = findElementInXLSheet(getParameterFinance, "btn_journeyReciptVoucher");
		txt_amountReciptVoucher = findElementInXLSheet(getParameterFinance, "txt_amountReciptVoucher");
		btn_glLookup = findElementInXLSheet(getParameterFinance, "btn_glLookup");

		/* FIN_BD_25 */
		btn_employeeLookup = findElementInXLSheet(getParameterFinance, "btn_employeeLookup");
		txt_amountEmployeeRecipt = findElementInXLSheet(getParameterFinance, "txt_amountEmployeeRecipt");
		btn_journeyEmployeeReceiptAdvice = findElementInXLSheet(getParameterFinance,
				"btn_journeyEmployeeReceiptAdvice");
		btn_journeiesDows = findElementInXLSheet(getParameterFinance, "btn_journeiesDows");

		/* FIN_BD_26_27 */
		header_releasedBankDeposit = findElementInXLSheet(getParameterFinance, "header_releasedBankDeposit");
		lbl_creditJournelEntryBankDepositCash = findElementInXLSheet(getParameterFinance,
				"lbl_creditJournelEntryBankDepositCash");
		lbl_debitJournelEntryBankDepositCash = findElementInXLSheet(getParameterFinance,
				"lbl_debitJournelEntryBankDepositCash");
		lbl_debitTotalJournelEntry = findElementInXLSheet(getParameterFinance, "lbl_debitTotalJournelEntry");
		lbl_creditTotalJournelEntry = findElementInXLSheet(getParameterFinance, "lbl_creditTotalJournelEntry");

		/* FIN_BD_28 */
		drop_bankNameInboundPayment = findElementInXLSheet(getParameterFinance, "drop_bankNameInboundPayment");
		txt_checkNoInboundPayment = findElementInXLSheet(getParameterFinance, "txt_checkNoInboundPayment");

		/* FIN_BD_32_33_34 */
		tbl_coundJournelEntries = findElementInXLSheet(getParameterFinance, "tbl_coundJournelEntries");
		lbl_creditJournelEntryBankDepositGLReplace = findElementInXLSheet(getParameterFinance,
				"lbl_creditJournelEntryBankDepositGLReplace");
		lbl_debitJournelEntryBankDepositGLReplace = findElementInXLSheet(getParameterFinance,
				"lbl_debitJournelEntryBankDepositGLReplace");
		lbl_debitTotalJournelEntryRecordReplace = findElementInXLSheet(getParameterFinance,
				"lbl_debitTotalJournelEntryRecordReplace");
		lbl_creditTotalJournelEntryRecordReplace = findElementInXLSheet(getParameterFinance,
				"lbl_creditTotalJournelEntryRecordReplace");
		lbl_journelEntryDocNoJEReplace = findElementInXLSheet(getParameterFinance, "lbl_journelEntryDocNoJEReplace");
		txt_searchInboundPayment2 = findElementInXLSheet(getParameterFinance, "txt_searchInboundPayment2");
		lnl_releventInboundPaymentDocReplace = findElementInXLSheet(getParameterFinance,
				"lnl_releventInboundPaymentDocReplace");
		lbl_journelEntryDocNoSingleEntry = findElementInXLSheet(getParameterFinance,
				"lbl_journelEntryDocNoSingleEntry");
		txt_depositAmountBankDeposit = findElementInXLSheet(getParameterFinance, "txt_depositAmountBankDeposit");

		/* FIN_BD_35_36_37 */
		div_summaryTab = findElementInXLSheet(getParameterFinance, "div_summaryTab");
		btn_closeWindow = findElementInXLSheet(getParameterFinance, "btn_closeWindow");
		properties_docFlow = findElementInXLSheet(getParameterFinance, "properties_docFlow");

		/* FIN_BD_38 */
		para_validatorInboundPaymentAlreadyUsed = findElementInXLSheet(getParameterFinance,
				"para_validatorInboundPaymentAlreadyUsed");

		/* FIN_BD_39 */
		btn_chequeReturn = findElementInXLSheet(getParameterFinance, "btn_chequeReturn");
		txt_returnDate = findElementInXLSheet(getParameterFinance, "txt_returnDate");
		btn_day = findElementInXLSheet(getParameterFinance, "btn_day");
		btn_return = findElementInXLSheet(getParameterFinance, "btn_return");
		header_checkReturnWindow = findElementInXLSheet(getParameterFinance, "header_checkReturnWindow");
		lbl_docStatusChequeReturn = findElementInXLSheet(getParameterFinance, "lbl_docStatusChequeReturn");

		/* FIN_BA_1_5_6 */
		header_bankAdjustmentByPage = findElementInXLSheet(getParameterFinance, "header_bankAdjustmentByPage");
		header_newBankAdjustment = findElementInXLSheet(getParameterFinance, "header_newBankAdjustment");
		error_msgFieldDropdownBankBankAdjustment = findElementInXLSheet(getParameterFinance,
				"error_msgFieldDropdownBankBankAdjustment");
		error_borderBankAccountNoBankAdjustment = findElementInXLSheet(getParameterFinance,
				"error_borderBankAccountNoBankAdjustment");
		error_msgFieldDropdownBankAccountBankAdjustment = findElementInXLSheet(getParameterFinance,
				"error_msgFieldDropdownBankAccountBankAdjustment");
		error_borderCurencyBankAdjustment = findElementInXLSheet(getParameterFinance,
				"error_borderCurencyBankAdjustment");
		error_msgFieldCurrencyBankAdjustment = findElementInXLSheet(getParameterFinance,
				"error_msgFieldCurrencyBankAdjustment");
		error_msgFieldGLBankAdjustment = findElementInXLSheet(getParameterFinance, "error_msgFieldGLBankAdjustment");
		error_borderAmountBankAdjustment = findElementInXLSheet(getParameterFinance,
				"error_borderAmountBankAdjustment");
		error_msgFieldAmountBankAdjustment = findElementInXLSheet(getParameterFinance,
				"error_msgFieldAmountBankAdjustment");

		/* FIN_BA_2 */
		btn_adminModule = findElementInXLSheet(getParameterFinance, "btn_adminModule");
		btn_userPermission = findElementInXLSheet(getParameterFinance, "btn_userPermission");
		btn_userLookupUserPermission = findElementInXLSheet(getParameterFinance, "btn_userLookupUserPermission");
		txt_userSearch = findElementInXLSheet(getParameterFinance, "txt_userSearch");
		drop_moduleNameUserPermission = findElementInXLSheet(getParameterFinance, "drop_moduleNameUserPermission");
		chk_reverseBankAdjustmentUserPermission = findElementInXLSheet(getParameterFinance,
				"chk_reverseBankAdjustmentUserPermission");
		chk_releaseBankAdjustmentUserPermission = findElementInXLSheet(getParameterFinance,
				"chk_releaseBankAdjustmentUserPermission");
		chk_viewBankAdjustmentUserPermission = findElementInXLSheet(getParameterFinance,
				"chk_viewBankAdjustmentUserPermission");
		chk_allowTempBankAdjustmentUserPermission = findElementInXLSheet(getParameterFinance,
				"chk_allowTempBankAdjustmentUserPermission");
		lbl_noPermissionValidation = findElementInXLSheet(getParameterFinance, "lbl_noPermissionValidation");

		/* FIN_BA_3 */
		drop_templateOnByPage = findElementInXLSheet(getParameterFinance, "drop_templateOnByPage");

		/* FIN_BA_7_8_9 */
		btn_lookupGLBankAdjustment = findElementInXLSheet(getParameterFinance, "btn_lookupGLBankAdjustment");
		header_draftedBankAdjustment = findElementInXLSheet(getParameterFinance, "header_draftedBankAdjustment");
		header_releasedBankAdjustment = findElementInXLSheet(getParameterFinance, "header_releasedBankAdjustment");
		lnk_firstRecordBankAdjustmentByPage = findElementInXLSheet(getParameterFinance,
				"lnk_firstRecordBankAdjustmentByPage");
		header_releasedBankAdjustmentDocReplace = findElementInXLSheet(getParameterFinance,
				"header_releasedBankAdjustmentDocReplace");
		txt_searchBankAdjustment = findElementInXLSheet(getParameterFinance, "txt_searchBankAdjustment");
		btn_searchOnByPage = findElementInXLSheet(getParameterFinance, "btn_searchOnByPage");
		lnk_resultDocOnByPage = findElementInXLSheet(getParameterFinance, "lnk_resultDocOnByPage");

		/* FIN_BA_10 */
		txt_bankFacilityFrontPageBankAdjustment = findElementInXLSheet(getParameterFinance,
				"txt_bankFacilityFrontPageBankAdjustment");
		btn_lookupBankFacilityOnBankAdjustment = findElementInXLSheet(getParameterFinance,
				"btn_lookupBankFacilityOnBankAdjustment");

		/* FIN_BA_12 */
		lbl_recentlyDraftedBankAdjustment = findElementInXLSheet(getParameterFinance,
				"lbl_recentlyDraftedBankAdjustment");
		lnl_releventDraftedBankAdjustmentDocReplace = findElementInXLSheet(getParameterFinance,
				"lnl_releventDraftedBankAdjustmentDocReplace");
		txt_descriptionDraftedBankAdjustment = findElementInXLSheet(getParameterFinance,
				"txt_descriptionDraftedBankAdjustment");

		/* FIN_BA_13 */
		btn_copyFrom = findElementInXLSheet(getParameterFinance, "btn_copyFrom");
		header_loockupBankAdjustment = findElementInXLSheet(getParameterFinance, "header_loockupBankAdjustment");

		/* FIN_BA_14 */
		btn_expandPropertyPlantAndEquipment = findElementInXLSheet(getParameterFinance,
				"btn_expandPropertyPlantAndEquipment");
		btn_expandInventory = findElementInXLSheet(getParameterFinance, "btn_expandInventory");
		btn_editLandGLAccount = findElementInXLSheet(getParameterFinance, "btn_editLandGLAccount");
		btn_accountStatusGL = findElementInXLSheet(getParameterFinance, "btn_accountStatusGL");
		drop_glAccountStatus = findElementInXLSheet(getParameterFinance, "drop_glAccountStatus");
		btn_applyGlAccountStatus = findElementInXLSheet(getParameterFinance, "btn_applyGlAccountStatus");
		btn_updateLast = findElementInXLSheet(getParameterFinance, "btn_updateLast");
		btn_editChartOfAccount = findElementInXLSheet(getParameterFinance, "btn_editChartOfAccount");
		btn_chartOfAccount = findElementInXLSheet(getParameterFinance, "btn_chartOfAccount");

		/* FIN_BA_16 */
		header_reverseDatePopup = findElementInXLSheet(getParameterFinance, "header_reverseDatePopup");
		header_reversedBankAdjustment = findElementInXLSheet(getParameterFinance, "header_reversedBankAdjustment");

		/* FIN_BA_18_19 */
		txt_amountWithErrorBorderBankAdjustment = findElementInXLSheet(getParameterFinance,
				"txt_amountWithErrorBorderBankAdjustment");
		lbl_errorPlusAmountCapitalSettlementBankAdjustment = findElementInXLSheet(getParameterFinance,
				"lbl_errorPlusAmountCapitalSettlementBankAdjustment");
		div_balanceAmountBankFacilityAgreement = findElementInXLSheet(getParameterFinance,
				"div_balanceAmountBankFacilityAgreement");
		para_validationHigherAdjustmentThanLOanBalance = findElementInXLSheet(getParameterFinance,
				"para_validationHigherAdjustmentThanLOanBalance");

		/* FIN_BA_20_32 */
		lnk_bankFacilityAgreementDocReplace = findElementInXLSheet(getParameterFinance,
				"lnk_bankFacilityAgreementDocReplace");
		drop_costAssignmentTypeBankAdjustment = findElementInXLSheet(getParameterFinance,
				"drop_costAssignmentTypeBankAdjustment");
		btn_closeCostAllocationWindow = findElementInXLSheet(getParameterFinance, "btn_closeCostAllocationWindow");

		/* FIN_BA_21 */
		lnk_resultBankFailityDocReplace = findElementInXLSheet(getParameterFinance, "lnk_resultBankFailityDocReplace");

		/* FIN_BA_30 */
		para_validationCheckoutBeforeDraft = findElementInXLSheet(getParameterFinance,
				"para_validationCheckoutBeforeDraft");
		btn_checkoutWithErrorBorder = findElementInXLSheet(getParameterFinance, "btn_checkoutWithErrorBorder");

		/* FIN_BA_31_32 */
		txt_taxTotal = findElementInXLSheet(getParameterFinance, "txt_taxTotal");

		/* FIN_BA_33 */
		txt_currencyBankAdjustment = findElementInXLSheet(getParameterFinance, "txt_currencyBankAdjustment");

		/* FIN_CBB_1_4_5_6 */
		header_cashBankBookByPage = findElementInXLSheet(getParameterFinance, "header_cashBankBookByPage");
		btn_newCashBankBook = findElementInXLSheet(getParameterFinance, "btn_newCashBankBook");
		header_newCashBankBook = findElementInXLSheet(getParameterFinance, "header_newCashBankBook");
		drop_typeCashBankBook = findElementInXLSheet(getParameterFinance, "drop_typeCashBankBook");
		txt_bankAccountNoCashBankBook = findElementInXLSheet(getParameterFinance, "txt_bankAccountNoCashBankBook");
		txt_floatCashBankBook = findElementInXLSheet(getParameterFinance, "txt_floatCashBankBook");
		btn_permissionGroupLookupCashBankBook = findElementInXLSheet(getParameterFinance,
				"btn_permissionGroupLookupCashBankBook");
		txt_searcUserGroup = findElementInXLSheet(getParameterFinance, "txt_searcUserGroup");
		txt_permissionGroupFrontPageCBB = findElementInXLSheet(getParameterFinance, "txt_permissionGroupFrontPageCBB");
		drop_bankCodeCBB = findElementInXLSheet(getParameterFinance, "drop_bankCodeCBB");
		txt_companyStatementCBB = findElementInXLSheet(getParameterFinance, "txt_companyStatementCBB");
		txt_bankBranchCBB = findElementInXLSheet(getParameterFinance, "txt_bankBranchCBB");
		drop_accountTypeCBB = findElementInXLSheet(getParameterFinance, "drop_accountTypeCBB");
		txt_telephoneNumberCBB = findElementInXLSheet(getParameterFinance, "txt_telephoneNumberCBB");
		txt_faxCBB = findElementInXLSheet(getParameterFinance, "txt_faxCBB");
		txt_emailCBB = findElementInXLSheet(getParameterFinance, "txt_emailCBB");
		txt_webURLCBB = findElementInXLSheet(getParameterFinance, "txt_webURLCBB");
		txt_addressCBB = findElementInXLSheet(getParameterFinance, "txt_addressCBB");
		chk_moreCurrenciesCBB = findElementInXLSheet(getParameterFinance, "chk_moreCurrenciesCBB");
		txt_discountingMaximumCBB = findElementInXLSheet(getParameterFinance, "txt_discountingMaximumCBB");
		txt_activeFromCBB = findElementInXLSheet(getParameterFinance, "txt_activeFromCBB");
		txt_activeToCBB = findElementInXLSheet(getParameterFinance, "txt_activeToCBB");
		txt_deawerCBB = findElementInXLSheet(getParameterFinance, "txt_deawerCBB");
		txt_swiftCodeCBB = findElementInXLSheet(getParameterFinance, "txt_swiftCodeCBB");
		chk_iouCBB = findElementInXLSheet(getParameterFinance, "chk_iouCBB");
		chk_leaseFacilityCBB = findElementInXLSheet(getParameterFinance, "chk_leaseFacilityCBB");
		header_draftedCashBankBook = findElementInXLSheet(getParameterFinance, "header_draftedCashBankBook");
		header_releasedCashBankBook = findElementInXLSheet(getParameterFinance, "header_releasedCashBankBook");
		header_releasedCashBankBookDocReplace = findElementInXLSheet(getParameterFinance,
				"header_releasedCashBankBookDocReplace");
		count_cashBannkBookByPage = findElementInXLSheet(getParameterFinance, "count_cashBannkBookByPage");
		lbl_docNoCashBankBook = findElementInXLSheet(getParameterFinance, "lbl_docNoCashBankBook");

		/* FIN_CBB_2 */
		chk_reverseCashBankBookUserPermission = findElementInXLSheet(getParameterFinance,
				"chk_reverseCashBankBookUserPermission");
		chk_releaseCashBankBookUserPermission = findElementInXLSheet(getParameterFinance,
				"chk_releaseCashBankBookUserPermission");
		chk_viewCashBankBookUserPermission = findElementInXLSheet(getParameterFinance,
				"chk_viewCashBankBookUserPermission");
		chk_allowTempCashBankBookUserPermission = findElementInXLSheet(getParameterFinance,
				"chk_allowTempCashBankBookUserPermission");

		/* FIN_CBB_7 */
		txt_odLimit = findElementInXLSheet(getParameterFinance, "txt_odLimit");

		/* FIN_CBB_12 */
		para_validationDuplicateAccountNumber = findElementInXLSheet(getParameterFinance,
				"para_validationDuplicateAccountNumber");

		/* FIN_CBB_013 */
		lbl_accountNoDrfatedCashBankBook = findElementInXLSheet(getParameterFinance,
				"lbl_accountNoDrfatedCashBankBook");
		lbl_recentlyDraftedCashBAnkBook = findElementInXLSheet(getParameterFinance, "lbl_recentlyDraftedCashBAnkBook");

		/* FIN_CBB_14 */
		header_lookupCashBankBook = findElementInXLSheet(getParameterFinance, "header_lookupCashBankBook");

		/* FIN_CBB_15 */
		btn_removePermissionGroupCBB = findElementInXLSheet(getParameterFinance, "btn_removePermissionGroupCBB");

		/* FIN_CBB_18 */
		btn_statusAction = findElementInXLSheet(getParameterFinance, "btn_statusAction");
		drop_status = findElementInXLSheet(getParameterFinance, "drop_status");
		btn_applyStatus = findElementInXLSheet(getParameterFinance, "btn_applyStatus");
		lbl_status = findElementInXLSheet(getParameterFinance, "lbl_status");
		btn_payBookGenaration = findElementInXLSheet(getParameterFinance, "btn_payBookGenaration");
		btn_newPayBookGenaration = findElementInXLSheet(getParameterFinance, "btn_newPayBookGenaration");
		drop_payMethod = findElementInXLSheet(getParameterFinance, "drop_payMethod");
		header_payBookGenerationByPage = findElementInXLSheet(getParameterFinance, "header_payBookGenerationByPage");
		header_newPayBookGenaration = findElementInXLSheet(getParameterFinance, "header_newPayBookGenaration");

		/* FIN_CBB_20 */
		header_newPettyCash = findElementInXLSheet(getParameterFinance, "header_newPettyCash");
		txt_amountPettyCash2 = findElementInXLSheet(getParameterFinance, "txt_amountPettyCash2");
		txt_amountWithErrorBorderFrontPagePettyCash = findElementInXLSheet(getParameterFinance,
				"txt_amountWithErrorBorderFrontPagePettyCash");
		error_msgFrontPageFloatAmountPettyCash = findElementInXLSheet(getParameterFinance,
				"error_msgFrontPageFloatAmountPettyCash");
		txt_amountWithErrorBorderPettyCash = findElementInXLSheet(getParameterFinance,
				"txt_amountWithErrorBorderPettyCash");
		btn_rowErrorPettyCash = findElementInXLSheet(getParameterFinance, "btn_rowErrorPettyCash");
		error_gridFloatExceedPettyCash = findElementInXLSheet(getParameterFinance, "error_gridFloatExceedPettyCash");
		error_gridTotalAmountFloatExceedPettyCash = findElementInXLSheet(getParameterFinance,
				"error_gridTotalAmountFloatExceedPettyCash");

		/* FIN_CBB_21 */
		txt_payCodePayBookGenaration = findElementInXLSheet(getParameterFinance, "txt_payCodePayBookGenaration");
		header_draftedPayBook = findElementInXLSheet(getParameterFinance, "header_draftedPayBook");
		header_releasedPayBook = findElementInXLSheet(getParameterFinance, "header_releasedPayBook");

		/* FIN_CBB_22 */
		btn_curencyWidgetCashBankBook = findElementInXLSheet(getParameterFinance, "btn_curencyWidgetCashBankBook");
		lbl_exchageRateCashBankBook = findElementInXLSheet(getParameterFinance, "lbl_exchageRateCashBankBook");

		/* FIN_CBB_25_26_27 */
		header_newBankReconcilation = findElementInXLSheet(getParameterFinance, "header_newBankReconcilation");

		/* FIN_CBB_29 */
		error_floatFieldMandatoryValidationCashBankBook = findElementInXLSheet(getParameterFinance,
				"error_floatFieldMandatoryValidationCashBankBook");
		error_borderFloatCashBankBook = findElementInXLSheet(getParameterFinance, "error_borderFloatCashBankBook");

		/* Bank Facility Agreement */
		/* FIN_BFA_1_5_7_8_9_10_11_14_22 */
		header_newBFA = findElementInXLSheet(getParameterFinance, "header_newBFA");
		header_BFAByPage = findElementInXLSheet(getParameterFinance, "header_BFAByPage");
		btn_dayCalenderBankFacilityDayReplace = findElementInXLSheet(getParameterFinance,
				"btn_dayCalenderBankFacilityDayReplace");
		drop_currencyBFA = findElementInXLSheet(getParameterFinance, "drop_currencyBFA");
		header_draftedBankFacilityAgreement = findElementInXLSheet(getParameterFinance,
				"header_draftedBankFacilityAgreement");
		header_releasedBankFacilityAgreement = findElementInXLSheet(getParameterFinance,
				"header_releasedBankFacilityAgreement");
		drop_bankAccountNoBankFacilityAgreement = findElementInXLSheet(getParameterFinance,
				"drop_bankAccountNoBankFacilityAgreement");
		drop_yeaeDatePlickerBFA = findElementInXLSheet(getParameterFinance, "drop_yeaeDatePlickerBFA");

		/* FIN_BFA_2 */
		chk_reverseBFAUserPermission = findElementInXLSheet(getParameterFinance, "chk_reverseBFAUserPermission");
		chk_releaseBFAUserPermission = findElementInXLSheet(getParameterFinance, "chk_releaseBFAUserPermission");
		chk_viewBFAUserPermission = findElementInXLSheet(getParameterFinance, "chk_viewBFAUserPermission");
		chk_allowTempBFAUserPermission = findElementInXLSheet(getParameterFinance, "chk_allowTempBFAUserPermission");

		/* FIN_BFA_4 */
		lnk_firstRecordBankFacilityAgreementByPage = findElementInXLSheet(getParameterFinance,
				"lnk_firstRecordBankFacilityAgreementByPage");
		header_releasedBankFacilityAgreementDocReplace = findElementInXLSheet(getParameterFinance,
				"header_releasedBankFacilityAgreementDocReplace");

		/* FIN_BFA_13 */
		error_amountFieldMinusValidationBFA = findElementInXLSheet(getParameterFinance,
				"error_amountFieldMinusValidationBFA");

		/* FIN_BFA_15 */
		lab_bankAccountDrfatedBFA = findElementInXLSheet(getParameterFinance, "lab_bankAccountDrfatedBFA");

		/* FIN_BFA_16 */
		lnk_documentOnBankFacilityAgreementByPageDocReplace = findElementInXLSheet(getParameterFinance,
				"lnk_documentOnBankFacilityAgreementByPageDocReplace");
		lnk_bankFacilityAgrrementByPageOnNewForm = findElementInXLSheet(getParameterFinance,
				"lnk_bankFacilityAgrrementByPageOnNewForm");

		/* FIN_BFA_18 */
		header_deletedBankFacilityAgreement = findElementInXLSheet(getParameterFinance,
				"header_deletedBankFacilityAgreement");

		/* FIN_BFA_19 */
		td_updatedTimeHistoryDetailsOnlyOneAction = findElementInXLSheet(getParameterFinance,
				"td_updatedTimeHistoryDetailsOnlyOneAction");
		td_actionDraftDoneByUseHistoryDetailsOnlyOneAction = findElementInXLSheet(getParameterFinance,
				"td_actionDraftDoneByUseHistoryDetailsOnlyOneAction");
		td_userHistoryDetailsOnlyOneAction = findElementInXLSheet(getParameterFinance,
				"td_userHistoryDetailsOnlyOneAction");
		td_descriptionHistoryDetailsOnlyOneAction = findElementInXLSheet(getParameterFinance,
				"td_descriptionHistoryDetailsOnlyOneAction");

		/* FIN_BFA_20 */
		lnk_documentOnBankFacilityAgreementByPageOriginDocReplace = findElementInXLSheet(getParameterFinance,
				"lnk_documentOnBankFacilityAgreementByPageOriginDocReplace");
		lnk_bankFacilityAgrrement = findElementInXLSheet(getParameterFinance, "lnk_bankFacilityAgrrement");

		/* FIN_BFA_21 */
		header_lookupBFA = findElementInXLSheet(getParameterFinance, "header_lookupBFA");

		/* FIN_BFA_23 */
		header_reversedBFA = findElementInXLSheet(getParameterFinance, "header_reversedBFA");

		/* FIN_BFA_24_25 */
		btn_hold = findElementInXLSheet(getParameterFinance, "btn_hold");
		btn_unhold = findElementInXLSheet(getParameterFinance, "btn_unhold");
		txt_changeReson = findElementInXLSheet(getParameterFinance, "txt_changeReson");
		btn_okHoldUnhold = findElementInXLSheet(getParameterFinance, "btn_okHoldUnhold");
		header_holdBFA = findElementInXLSheet(getParameterFinance, "header_holdBFA");

		/* FIN_BFA_27 */
		txt_amountLetterOfGurantee = findElementInXLSheet(getParameterFinance, "txt_amountLetterOfGurantee");
		txt_originalDocNumberLOG = findElementInXLSheet(getParameterFinance, "txt_originalDocNumberLOG");
		drop_requestTypeLOG = findElementInXLSheet(getParameterFinance, "drop_requestTypeLOG");
		btn_lookupRequesterLOG = findElementInXLSheet(getParameterFinance, "btn_lookupRequesterLOG");
		drop_guranteeGroupLOG = findElementInXLSheet(getParameterFinance, "drop_guranteeGroupLOG");
		txt_expirationDateLOG = findElementInXLSheet(getParameterFinance, "txt_expirationDateLOG");
		btn_nextArrowCalender = findElementInXLSheet(getParameterFinance, "btn_nextArrowCalender");
		btn_28Calender = findElementInXLSheet(getParameterFinance, "btn_28Calender");
		btn_lookupReferenceDocLOG = findElementInXLSheet(getParameterFinance, "btn_lookupReferenceDocLOG");
		btn_widgetReferenceDocLOG = findElementInXLSheet(getParameterFinance, "btn_widgetReferenceDocLOG");
		btn_refreshRefrenceDocLookupLOG = findElementInXLSheet(getParameterFinance, "btn_refreshRefrenceDocLookupLOG");
		lnk_firstResultRefrenceDocLOG = findElementInXLSheet(getParameterFinance, "lnk_firstResultRefrenceDocLOG");
		btn_applyRefernceDocLOG = findElementInXLSheet(getParameterFinance, "btn_applyRefernceDocLOG");
		header_drfatedLOG = findElementInXLSheet(getParameterFinance, "header_drfatedLOG");
		header_releasedLOG = findElementInXLSheet(getParameterFinance, "header_releasedLOG");
		btn_newLetterOfGurantee = findElementInXLSheet(getParameterFinance, "btn_newLetterOfGurantee");
		txt_captalAmountBFA = findElementInXLSheet(getParameterFinance, "txt_captalAmountBFA");
		txt_interestAmountBFA = findElementInXLSheet(getParameterFinance, "txt_interestAmountBFA");

		/* FIN_BFA_34_35_36 */
		btn_dayCommonDateFlicker = findElementInXLSheet(getParameterFinance, "btn_dayCommonDateFlicker");
		btn_applyDatesBFA = findElementInXLSheet(getParameterFinance, "btn_applyDatesBFA");
		tab_settlementDetailsBFA = findElementInXLSheet(getParameterFinance, "tab_settlementDetailsBFA");
		btn_downloadTemplateBFA = findElementInXLSheet(getParameterFinance, "btn_downloadTemplateBFA");
		btn_uploadTemplateBFA = findElementInXLSheet(getParameterFinance, "btn_uploadTemplateBFA");
		btn_okGridClearConfirmationBFA = findElementInXLSheet(getParameterFinance, "btn_okGridClearConfirmationBFA");
		btn_chooceFileBFA = findElementInXLSheet(getParameterFinance, "btn_chooceFileBFA");
		btn_tagFixedAssert = findElementInXLSheet(getParameterFinance, "btn_tagFixedAssert");
		btn_lookupFixedAssertTagFixedAssert = findElementInXLSheet(getParameterFinance,
				"btn_lookupFixedAssertTagFixedAssert");
		headr_fixedAssertInformationLookup = findElementInXLSheet(getParameterFinance,
				"headr_fixedAssertInformationLookup");
		txt_searchFixedAssert = findElementInXLSheet(getParameterFinance, "txt_searchFixedAssert");
		btn_updateTaggedFixedAssert = findElementInXLSheet(getParameterFinance, "btn_updateTaggedFixedAssert");
		btn_fixedAssetModule = findElementInXLSheet(getParameterFinance, "btn_fixedAssetModule");
		btn_fixedAssetInformation = findElementInXLSheet(getParameterFinance, "btn_fixedAssetInformation");
		lnk_resultOnByPageFixedAssertReplace = findElementInXLSheet(getParameterFinance,
				"lnk_resultOnByPageFixedAssertReplace");
		lbl_attachedLeasedFacilityFixedAssert = findElementInXLSheet(getParameterFinance,
				"lbl_attachedLeasedFacilityFixedAssert");
		div_uploadedLastRowInstallments = findElementInXLSheet(getParameterFinance, "div_uploadedLastRowInstallments");
		btn_closeJournelEntryBankFacilityAgreement = findElementInXLSheet(getParameterFinance,
				"btn_closeJournelEntryBankFacilityAgreement");
		btn_deatlilsTabFixedAssertInformation = findElementInXLSheet(getParameterFinance,
				"btn_deatlilsTabFixedAssertInformation");

		/* Petty Cash */
		/* FIN_PC_01 */
		header_pettyCashByPage = findElementInXLSheet(getParameterFinance, "header_pettyCashByPage");

		/* FIN_PC_04 */
		txt_floatBalancePettyCash = findElementInXLSheet(getParameterFinance, "txt_floatBalancePettyCash");

		/* FIN_PC_05 */
		txt_balancePettyCash = findElementInXLSheet(getParameterFinance, "txt_balancePettyCash");

		/* FIN_PC_06 */
		span_paidToLookupPettyCash = findElementInXLSheet(getParameterFinance, "span_paidToLookupPettyCash");
		txt_paitToPettyCashFrontPage = findElementInXLSheet(getParameterFinance, "txt_paitToPettyCashFrontPage");
		header_paidToLookupPC = findElementInXLSheet(getParameterFinance, "header_paidToLookupPC");
		td_resultEmployeePC = findElementInXLSheet(getParameterFinance, "td_resultEmployeePC");

		/* FIN_PC_08 */
		txt_amountPettyCashPC = findElementInXLSheet(getParameterFinance, "txt_amountPettyCashPC");
		header_amountGridPettyCash = findElementInXLSheet(getParameterFinance, "header_amountGridPettyCash");

		/* FIN_PC_12 */
		btn_glAccountLookupPC = findElementInXLSheet(getParameterFinance, "btn_glAccountLookupPC");
		header_glAccountLookupPC = findElementInXLSheet(getParameterFinance, "header_glAccountLookupPC");
		txt_searchGLAccount = findElementInXLSheet(getParameterFinance, "txt_searchGLAccount");
		td_resultGLAccountPC = findElementInXLSheet(getParameterFinance, "td_resultGLAccountPC");
		div_selectedGLAccountPC = findElementInXLSheet(getParameterFinance, "div_selectedGLAccountPC");

		/* FIN_PC_13 */
		btn_narrationPC = findElementInXLSheet(getParameterFinance, "btn_narrationPC");
		txt_extendedNarrationPC = findElementInXLSheet(getParameterFinance, "txt_extendedNarrationPC");
		btn_applyExtendedNaration = findElementInXLSheet(getParameterFinance, "btn_applyExtendedNaration");
		header_draftedPettyCash = findElementInXLSheet(getParameterFinance, "header_draftedPettyCash");

		/* FIN_PC_15 */
		lnk_pettyCashHeaderOnNewPage = findElementInXLSheet(getParameterFinance, "lnk_pettyCashHeaderOnNewPage");
		txt_searchPettyCash = findElementInXLSheet(getParameterFinance, "txt_searchPettyCash");
		lnk_resultPettyCashByPage = findElementInXLSheet(getParameterFinance, "lnk_resultPettyCashByPage");

		/* FIN_PC_17 */
		header_deletedPettyCash = findElementInXLSheet(getParameterFinance, "header_deletedPettyCash");

		/* FIN_PC_18 */
		td_descriptionHistoryDetailsUpdatedPC = findElementInXLSheet(getParameterFinance,
				"td_descriptionHistoryDetailsUpdatedPC");

		/* FIN_PC_19 */
		lbl_recentlyDraftedPettyCash = findElementInXLSheet(getParameterFinance, "lbl_recentlyDraftedPettyCash");
		lnk_releventDraftedPettyCashDocReplace = findElementInXLSheet(getParameterFinance,
				"lnk_releventDraftedPettyCashDocReplace");
		div_descriptionAfterDraftThePC = findElementInXLSheet(getParameterFinance, "div_descriptionAfterDraftThePC");

		/* FIN_PC_20 */
		header_releasedPettyCash = findElementInXLSheet(getParameterFinance, "header_releasedPettyCash");

		/* FIN_PC_21 */
		header_reversedPC = findElementInXLSheet(getParameterFinance, "header_reversedPC");

		/* FIN_PC_23 */
		btn_iouLookupPC = findElementInXLSheet(getParameterFinance, "btn_iouLookupPC");
		header_pettyCashPopupPC = findElementInXLSheet(getParameterFinance, "header_pettyCashPopupPC");
		td_resultIOUPC = findElementInXLSheet(getParameterFinance, "td_resultIOUPC");
		txt_selectedIOUFrondPagePC = findElementInXLSheet(getParameterFinance, "txt_selectedIOUFrondPagePC");
		lbl_creditJournelEntryPettyCashGLReplace = findElementInXLSheet(getParameterFinance,
				"lbl_creditJournelEntryPettyCashGLReplace");
		lbl_debitJournelEntryPettyCashGLReplace = findElementInXLSheet(getParameterFinance,
				"lbl_debitJournelEntryPettyCashGLReplace");

		/* FIN_PC_25 */
		txt_amountWithErrorBorderPC = findElementInXLSheet(getParameterFinance, "txt_amountWithErrorBorderPC");
		txt_lineAmountWithErrorBorderPC = findElementInXLSheet(getParameterFinance, "txt_lineAmountWithErrorBorderPC");
		lbl_amountFieldErrorMsgFloatAndBalanceExceedPC = findElementInXLSheet(getParameterFinance,
				"lbl_amountFieldErrorMsgFloatAndBalanceExceedPC");
		btn_lineRowErrorsPC = findElementInXLSheet(getParameterFinance, "btn_lineRowErrorsPC");
		div_lineErrorExceedAmountPC = findElementInXLSheet(getParameterFinance, "div_lineErrorExceedAmountPC");
		div_lineErrorExceedTotalAmountPC = findElementInXLSheet(getParameterFinance,
				"div_lineErrorExceedTotalAmountPC");
		div_lineErrorExceedFloatAmountPC = findElementInXLSheet(getParameterFinance,
				"div_lineErrorExceedFloatAmountPC");
		p_mainValidation = findElementInXLSheet(getParameterFinance, "p_mainValidation");

		/* FIN_PC_26 */
		btn_dayHeaderMainDates = findElementInXLSheet(getParameterFinance, "btn_dayHeaderMainDates");
		btn_applyPostDate = findElementInXLSheet(getParameterFinance, "btn_applyPostDate");
		btn_backDatePlicker = findElementInXLSheet(getParameterFinance, "btn_backDatePlicker");
		btn_lastDateOfTheMonthHeaderMainDates = findElementInXLSheet(getParameterFinance,
				"btn_lastDateOfTheMonthHeaderMainDates");
		p_validationCannotReversePettyCash = findElementInXLSheet(getParameterFinance,
				"p_validationCannotReversePettyCash");

		/* FIN_PC_28 */
		txt_narrationOnGridPettyCash = findElementInXLSheet(getParameterFinance, "txt_narrationOnGridPettyCash");

		/* FIN_PC_29 */
		header_newDuplicatedPettyCash = findElementInXLSheet(getParameterFinance, "header_newDuplicatedPettyCash");

		/* FIN_PC_30 */
		btn_fundTransferJourney = findElementInXLSheet(getParameterFinance, "btn_fundTransferJourney");
		btn_lookupReceiverCashBook = findElementInXLSheet(getParameterFinance, "btn_lookupReceiverCashBook");
		td_resultCashBookFT = findElementInXLSheet(getParameterFinance, "td_resultCashBookFT");
		txt_selectedCashBankBookFT = findElementInXLSheet(getParameterFinance, "txt_selectedCashBankBookFT");
		txt_amountFundTransferAdvice = findElementInXLSheet(getParameterFinance, "txt_amountFundTransferAdvice");
		btn_convertToOutboundPayment = findElementInXLSheet(getParameterFinance, "btn_convertToOutboundPayment");
		btn_detailsTabFT = findElementInXLSheet(getParameterFinance, "btn_detailsTabFT");
		chk_adviceSelectOutboundPaymentFT = findElementInXLSheet(getParameterFinance,
				"chk_adviceSelectOutboundPaymentFT");
		p_totalExceedFloatBalancealidation = findElementInXLSheet(getParameterFinance,
				"p_totalExceedFloatBalancealidation");

		/* FIN_PC_31 */
		header_outboundPaymentAdviceByPage = findElementInXLSheet(getParameterFinance,
				"header_outboundPaymentAdviceByPage");
		header_newOutboundPaymentAdviceFT = findElementInXLSheet(getParameterFinance,
				"header_newOutboundPaymentAdviceFT");
		header_draftedOutboundPayementAdviceFT = findElementInXLSheet(getParameterFinance,
				"header_draftedOutboundPayementAdviceFT");
		header_releasedOutboundPayementAdviceFT = findElementInXLSheet(getParameterFinance,
				"header_releasedOutboundPayementAdviceFT");
		widget_balanceAmountOutboundPaymentReceivingAccount = findElementInXLSheet(getParameterFinance,
				"widget_balanceAmountOutboundPaymentReceivingAccount");
		btn_cashBankBalanceOnWidgetOP = findElementInXLSheet(getParameterFinance, "btn_cashBankBalanceOnWidgetOP");
		label_accountBalanceOP = findElementInXLSheet(getParameterFinance, "label_accountBalanceOP");
		drop_paybookOP = findElementInXLSheet(getParameterFinance, "drop_paybookOP");
		header_draftedOutboundPayementFT = findElementInXLSheet(getParameterFinance,
				"header_draftedOutboundPayementFT");
		header_releasedOutboundPayementFT = findElementInXLSheet(getParameterFinance,
				"header_releasedOutboundPayementFT");
		widget_balanceAmountOutboundPaymentReceivingAccountAfterReleadsed = findElementInXLSheet(getParameterFinance,
				"widget_balanceAmountOutboundPaymentReceivingAccountAfterReleadsed");
		header_newOutboundPaymentFT = findElementInXLSheet(getParameterFinance, "header_newOutboundPaymentFT");
		drop_paymentMethodOutboundPayment = findElementInXLSheet(getParameterFinance,
				"drop_paymentMethodOutboundPayment");
		td_resultPettyCashAccountFT = findElementInXLSheet(getParameterFinance, "td_resultPettyCashAccountFT");
		btn_closeBalaceWidgetOP = findElementInXLSheet(getParameterFinance, "btn_closeBalaceWidgetOP");

		/********* Outbound Payment Advice ***********/
		/* FIN_OPA_02 */
		btn_vendorAdvanceJourneyOPA = findElementInXLSheet(getParameterFinance, "btn_vendorAdvanceJourneyOPA");
		btn_vendorCreditMemoJourneyOPA = findElementInXLSheet(getParameterFinance, "btn_vendorCreditMemoJourneyOPA");
		btn_accruelVoucherJourneyOPA = findElementInXLSheet(getParameterFinance, "btn_accruelVoucherJourneyOPA");
		btn_fundTransferAdviceJourneyOPA = findElementInXLSheet(getParameterFinance,
				"btn_fundTransferAdviceJourneyOPA");
		btn_customerRefundJourneyOPA = findElementInXLSheet(getParameterFinance, "btn_customerRefundJourneyOPA");
		btn_paymentVoucherJourneyOPA = findElementInXLSheet(getParameterFinance, "btn_paymentVoucherJourneyOPA");
		btn_employeePaymentAdviceJourneyOPA = findElementInXLSheet(getParameterFinance,
				"btn_employeePaymentAdviceJourneyOPA");
		btn_outboundPaymentAdviceJourneyOPA = findElementInXLSheet(getParameterFinance,
				"btn_outboundPaymentAdviceJourneyOPA");

		/* FIN_OPA_03 */
		header_newVendorAdvanceOPA = findElementInXLSheet(getParameterFinance, "header_newVendorAdvanceOPA");

		/* FIN_OPA_04 */
		btn_vendorLookupOPA = findElementInXLSheet(getParameterFinance, "btn_vendorLookupOPA");
		txt_searchVendor = findElementInXLSheet(getParameterFinance, "txt_searchVendor");
		popup_vendorLookup = findElementInXLSheet(getParameterFinance, "popup_vendorLookup");
		td_resultVendorVendorAdvanceOPA = findElementInXLSheet(getParameterFinance, "td_resultVendorVendorAdvanceOPA");
		txt_vendorFrontPageOPA = findElementInXLSheet(getParameterFinance, "txt_vendorFrontPageOPA");
		drop_autoSelectedContactPersonOPA = findElementInXLSheet(getParameterFinance,
				"drop_autoSelectedContactPersonOPA");
		drop_autoSelectedAddressOPA = findElementInXLSheet(getParameterFinance, "drop_autoSelectedAddressOPA");

		/* FIN_OPA_05 */
		txt_referenceNoOPA = findElementInXLSheet(getParameterFinance, "txt_referenceNoOPA");
		txt_descriptionVendorAdvanceOPA = findElementInXLSheet(getParameterFinance, "txt_descriptionVendorAdvanceOPA");

		/* FIN_OPA_06 */
		drop_currencyOutboundPaymentAdviceVendorAdvance = findElementInXLSheet(getParameterFinance,
				"drop_currencyOutboundPaymentAdviceVendorAdvance");
		lbl_currencyTotakLableVA_OPA = findElementInXLSheet(getParameterFinance, "lbl_currencyTotakLableVA_OPA");

		/* FIN_OPA_07 */
		txt_amountVA_OPA = findElementInXLSheet(getParameterFinance, "txt_amountVA_OPA");

		/* FIN_OPA_09 */
		drop_taxVA_OPA = findElementInXLSheet(getParameterFinance, "drop_taxVA_OPA");

		/* FIN_OPA_10 */
		header_releaseVA_OPA = findElementInXLSheet(getParameterFinance, "header_releaseVA_OPA");
		header_drfatedVA_OPA = findElementInXLSheet(getParameterFinance, "header_drfatedVA_OPA");
		lbl_bannerTotalVAOPA = findElementInXLSheet(getParameterFinance, "lbl_bannerTotalVAOPA");

		/* FIN_OPA_11 */
		header_newOP_VAP = findElementInXLSheet(getParameterFinance, "header_newOP_VAP");
		drop_payCurrencyOP_VAP = findElementInXLSheet(getParameterFinance, "drop_payCurrencyOP_VAP");
		drop_payMethod_VAP = findElementInXLSheet(getParameterFinance, "drop_payMethod_VAP");
		tab_detailsOP_VAP = findElementInXLSheet(getParameterFinance, "tab_detailsOP_VAP");
		chk_selectPaymenAdviceOP_VAP = findElementInXLSheet(getParameterFinance, "chk_selectPaymenAdviceOP_VAP");
		lbl_bannerTotalOP_VAP = findElementInXLSheet(getParameterFinance, "lbl_bannerTotalOP_VAP");
		header_draftedOP_VAP = findElementInXLSheet(getParameterFinance, "header_draftedOP_VAP");
		header_releasedOP_VAP = findElementInXLSheet(getParameterFinance, "header_releasedOP_VAP");

		/* FIN_OPA_11_B */
		header_reversedOP_VAP = findElementInXLSheet(getParameterFinance, "header_reversedOP_VAP");

		/* FIN_OPA_12_13 */
		header_newVendorCreditMemo = findElementInXLSheet(getParameterFinance, "header_newVendorCreditMemo");
		btn_vendorLookup_OPA_VCM = findElementInXLSheet(getParameterFinance, "btn_vendorLookup_OPA_VCM");
		txt_selectedVendor_OPA_VCM = findElementInXLSheet(getParameterFinance, "txt_selectedVendor_OPA_VCM");
		drop_currency_OPA_VCM = findElementInXLSheet(getParameterFinance, "drop_currency_OPA_VCM");
		span_glLookup_OPA_VCM = findElementInXLSheet(getParameterFinance, "span_glLookup_OPA_VCM");
		td_resultGlAccount_OPA_VCM = findElementInXLSheet(getParameterFinance, "td_resultGlAccount_OPA_VCM");
		txt_amount_OPA_VCM = findElementInXLSheet(getParameterFinance, "txt_amount_OPA_VCM");
		header_drafted_OPA_VCM = findElementInXLSheet(getParameterFinance, "header_drafted_OPA_VCM");
		header_released_OPA_VCM = findElementInXLSheet(getParameterFinance, "header_released_OPA_VCM");
		btn_setOff_OPA_VCM = findElementInXLSheet(getParameterFinance, "btn_setOff_OPA_VCM");
		popup_outboundPaymentSetoff = findElementInXLSheet(getParameterFinance, "popup_outboundPaymentSetoff");
		chk_setOffAdvice_OPA_VCM = findElementInXLSheet(getParameterFinance, "chk_setOffAdvice_OPA_VCM");
		btn_setOffOnOPsetOffPopup = findElementInXLSheet(getParameterFinance, "btn_setOffOnOPsetOffPopup");
		p_successfullySetOff_OPA_VCM = findElementInXLSheet(getParameterFinance, "p_successfullySetOff_OPA_VCM");
		p_successfullySetOffFullAmount_OPA_VCM = findElementInXLSheet(getParameterFinance,
				"p_successfullySetOffFullAmount_OPA_VCM");
		btn_setOffDetailsOnAvtionMenu = findElementInXLSheet(getParameterFinance, "btn_setOffDetailsOnAvtionMenu");
		chk_reverseSetOffAdvice_OPA_VCM = findElementInXLSheet(getParameterFinance, "chk_reverseSetOffAdvice_OPA_VCM");
		btn_reverseOotboundPaymentSetOff = findElementInXLSheet(getParameterFinance,
				"btn_reverseOotboundPaymentSetOff");
		p_successfullyReverseSetOff_OPA_VCM = findElementInXLSheet(getParameterFinance,
				"p_successfullyReverseSetOff_OPA_VCM");
		td_resultVendorVendorCreditMemoOPA = findElementInXLSheet(getParameterFinance,
				"td_resultVendorVendorCreditMemoOPA");
		div_glAccountAfterSeleted_OPA_VCM = findElementInXLSheet(getParameterFinance,
				"div_glAccountAfterSeleted_OPA_VCM");
		txt_paiAmountSetOffWindow_OPA_VCM = findElementInXLSheet(getParameterFinance,
				"txt_paiAmountSetOffWindow_OPA_VCM");
		td_resultVendorVendorAdvance_OPA_VCM = findElementInXLSheet(getParameterFinance,
				"td_resultVendorVendorAdvance_OPA_VCM");
		lbl_bannerTotal = findElementInXLSheet(getParameterFinance, "lbl_bannerTotal");

		/* FIN_OPA_14 */
		drop_autoSelectedContactPerson_OPA_VCM = findElementInXLSheet(getParameterFinance,
				"drop_autoSelectedContactPerson_OPA_VCM");
		drop_autoSelectedAddress_OPA_VCM = findElementInXLSheet(getParameterFinance,
				"drop_autoSelectedAddress_OPA_VCM");

		/* FIN_OPA_15 */
		txt_referenceNo_OPA_VCM = findElementInXLSheet(getParameterFinance, "txt_referenceNo_OPA_VCM");
		txt_descriptionVendorAdvance_OPA_VCM = findElementInXLSheet(getParameterFinance,
				"txt_descriptionVendorAdvance_OPA_VCM");

		/* FIN_OPA_19_B */
		header_new_OP_VCM = findElementInXLSheet(getParameterFinance, "header_new_OP_VCM");
		drop_payCurrency_OP_VCM = findElementInXLSheet(getParameterFinance, "drop_payCurrency_OP_VCM");
		drop_payMethod_OP_VCM = findElementInXLSheet(getParameterFinance, "drop_payMethod_OP_VCM");
		tab_details_OP_VCM = findElementInXLSheet(getParameterFinance, "tab_details_OP_VCM");
		chk_selectPaymenAdvice_OP_VCM = findElementInXLSheet(getParameterFinance, "chk_selectPaymenAdvice_OP_VCM");
		lbl_bannerTotal_OP_VCM = findElementInXLSheet(getParameterFinance, "lbl_bannerTotal_OP_VCM");
		header_drafted_OP_VCM = findElementInXLSheet(getParameterFinance, "header_drafted_OP_VCM");
		header_released_OP_VCM = findElementInXLSheet(getParameterFinance, "header_released_OP_VCM");
		header_reversedOP_VCM = findElementInXLSheet(getParameterFinance, "header_reversedOP_VCM");

		/* FIN_OPA_21 */
		p_setOffAmountExceedValidatioVsAdviceDue = findElementInXLSheet(getParameterFinance,
				"p_setOffAmountExceedValidatioVsAdviceDue");

		/* FIN_OPA_23 */
		txt_paidAmountPartialAmount_VCM_OPA = findElementInXLSheet(getParameterFinance,
				"txt_paidAmountPartialAmount_VCM_OPA");

		/* FIN_OPA_24 */
		td_inactiveGL_OPA = findElementInXLSheet(getParameterFinance, "td_inactiveGL_OPA");
		btn_expandChartOfAccount = findElementInXLSheet(getParameterFinance, "btn_expandChartOfAccount");
		td_resultInactiveGLAccountOPA = findElementInXLSheet(getParameterFinance, "td_resultInactiveGLAccountOPA");

		/* FIN_OPA_25 */
		header_newAccruelVoucher = findElementInXLSheet(getParameterFinance, "header_newAccruelVoucher");
		btn_vendorLookup_OPA_AV = findElementInXLSheet(getParameterFinance, "btn_vendorLookup_OPA_AV");
		txt_selectedVendor_OPA_AV = findElementInXLSheet(getParameterFinance, "txt_selectedVendor_OPA_AV");
		drop_currency_OPA_AV = findElementInXLSheet(getParameterFinance, "drop_currency_OPA_AV");
		span_glLookup_OPA_AV = findElementInXLSheet(getParameterFinance, "span_glLookup_OPA_AV");
		td_resultGlAccount_OPA_AV = findElementInXLSheet(getParameterFinance, "td_resultGlAccount_OPA_AV");
		txt_amount_OPA_AV = findElementInXLSheet(getParameterFinance, "txt_amount_OPA_AV");
		header_drafted_OPA_AV = findElementInXLSheet(getParameterFinance, "header_drafted_OPA_AV");
		header_released_OPA_AV = findElementInXLSheet(getParameterFinance, "header_released_OPA_AV");
		btn_setOff_OPA_AV = findElementInXLSheet(getParameterFinance, "btn_setOff_OPA_AV");
		chk_setOffAdvice_OPA_AV = findElementInXLSheet(getParameterFinance, "chk_setOffAdvice_OPA_AV");
		p_successfullySetOff_OPA_AV = findElementInXLSheet(getParameterFinance, "p_successfullySetOff_OPA_AV");
		p_successfullySetOffFullAmount_OPA_AV = findElementInXLSheet(getParameterFinance,
				"p_successfullySetOffFullAmount_OPA_AV");
		chk_reverseSetOffAdvice_OPA_AV = findElementInXLSheet(getParameterFinance, "chk_reverseSetOffAdvice_OPA_AV");
		p_successfullyReverseSetOff_OPA_AV = findElementInXLSheet(getParameterFinance,
				"p_successfullyReverseSetOff_OPA_AV");
		td_resultVendor_AV_OPA = findElementInXLSheet(getParameterFinance, "td_resultVendor_AV_OPA");
		div_glAccountAfterSeleted_OPA_AV = findElementInXLSheet(getParameterFinance,
				"div_glAccountAfterSeleted_OPA_AV");
		txt_paiAmountSetOffWindow_OPA_AV = findElementInXLSheet(getParameterFinance,
				"txt_paiAmountSetOffWindow_OPA_AV");
		td_resultVendorVendorAdvance_OPA_AV = findElementInXLSheet(getParameterFinance,
				"td_resultVendorVendorAdvance_OPA_AV");
		drop_autoSelectedContactPerson_OPA_AV = findElementInXLSheet(getParameterFinance,
				"drop_autoSelectedContactPerson_OPA_AV");
		drop_autoSelectedAddress_OPA_AV = findElementInXLSheet(getParameterFinance, "drop_autoSelectedAddress_OPA_AV");

		/* FIN_OPA_26 */
		txt_referenceNo_OPA_AV = findElementInXLSheet(getParameterFinance, "txt_referenceNo_OPA_AV");
		txt_descriptionVendorAdvance_OPA_AV = findElementInXLSheet(getParameterFinance,
				"txt_descriptionVendorAdvance_OPA_AV");

		/* FIN_OPA_30_B */
		drop_payCurrencyOP_AV = findElementInXLSheet(getParameterFinance, "drop_payCurrencyOP_AV");
		drop_payMethod_AV = findElementInXLSheet(getParameterFinance, "drop_payMethod_AV");
		tab_detailsOP_AV = findElementInXLSheet(getParameterFinance, "tab_detailsOP_AV");
		chk_selectPaymenAdviceOP_AV = findElementInXLSheet(getParameterFinance, "chk_selectPaymenAdviceOP_AV");
		header_draftedOP_AV = findElementInXLSheet(getParameterFinance, "header_draftedOP_AV");
		lbl_bannerTotalOP_AV = findElementInXLSheet(getParameterFinance, "lbl_bannerTotalOP_AV");
		header_releasedOP_AV = findElementInXLSheet(getParameterFinance, "header_releasedOP_AV");
		header_newVendorPaymentAV = findElementInXLSheet(getParameterFinance, "header_newVendorPaymentAV");

		/* FIN_OPA_30_C */
		header_reversed_OPA_AV = findElementInXLSheet(getParameterFinance, "header_reversed_OPA_AV");

		/* FIN_OPA_34 */
		p_successfullValidationSetOffHalfAmountAV = findElementInXLSheet(getParameterFinance,
				"p_successfullValidationSetOffHalfAmountAV");

		/* FIN_OPA_36 */
		header_newFundTransferAdvice = findElementInXLSheet(getParameterFinance, "header_newFundTransferAdvice");
		td_resultCashReceiveBook_OPA_FT = findElementInXLSheet(getParameterFinance, "td_resultCashReceiveBook_OPA_FT");
		txt_selectedCashBankBook_OPA_FT = findElementInXLSheet(getParameterFinance, "txt_selectedCashBankBook_OPA_FT");
		lookup_cashBook = findElementInXLSheet(getParameterFinance, "lookup_cashBook");
		btn_searchOnLookupCashBook = findElementInXLSheet(getParameterFinance, "btn_searchOnLookupCashBook");

		/* FIN_OPA_37 */
		txt_description_OPA_FT = findElementInXLSheet(getParameterFinance, "txt_description_OPA_FT");

		/* FIN_OPA_38_39 */
		txt_amount_OPA_FT = findElementInXLSheet(getParameterFinance, "txt_amount_OPA_FT");

		/* FIN_OPA_40_A */
		header_drafted_OPA_FT = findElementInXLSheet(getParameterFinance, "header_drafted_OPA_FT");
		header_released_OPA_FT = findElementInXLSheet(getParameterFinance, "header_released_OPA_FT");

		/* FIN_OPA_40_B */
		header_reversed_OPA_FT = findElementInXLSheet(getParameterFinance, "header_reversed_OPA_FT");

		/* FIN_OPA_41 */
		header_newVendorPaymentFT = findElementInXLSheet(getParameterFinance, "header_newVendorPaymentFT");
		drop_payCurrencyOP_FT = findElementInXLSheet(getParameterFinance, "drop_payCurrencyOP_FT");
		drop_payMethod_FT = findElementInXLSheet(getParameterFinance, "drop_payMethod_FT");
		tab_detailsOP_FT = findElementInXLSheet(getParameterFinance, "tab_detailsOP_FT");
		chk_selectPaymenAdviceOP_FT = findElementInXLSheet(getParameterFinance, "chk_selectPaymenAdviceOP_FT");
		header_draftedOP_FT = findElementInXLSheet(getParameterFinance, "header_draftedOP_FT");
		header_releasedOP_FT = findElementInXLSheet(getParameterFinance, "header_releasedOP_FT");

		/* FIN_OPA_42 */
		btn_journeyDown = findElementInXLSheet(getParameterFinance, "btn_journeyDown");
		header_newCustomerRefund = findElementInXLSheet(getParameterFinance, "header_newCustomerRefund");
		btn_customerLookup_OPA_CR = findElementInXLSheet(getParameterFinance, "btn_customerLookup_OPA_CR");
		lookup_customer = findElementInXLSheet(getParameterFinance, "lookup_customer");
		btn_searchCustomer = findElementInXLSheet(getParameterFinance, "btn_searchCustomer");
		td_resultCustomer_OPA_CR = findElementInXLSheet(getParameterFinance, "td_resultCustomer_OPA_CR");
		txt_selectedCustomer_OPA_CR = findElementInXLSheet(getParameterFinance, "txt_selectedCustomer_OPA_CR");
		drop_contactPerson_OPA_CR = findElementInXLSheet(getParameterFinance, "drop_contactPerson_OPA_CR");
		txt_billingAddress_OPA_CR = findElementInXLSheet(getParameterFinance, "txt_billingAddress_OPA_CR");
		txt_searchCustomer = findElementInXLSheet(getParameterFinance, "txt_searchCustomer");

		/* FIN_OPA_43 */
		txt_customerReference_OPA_CR = findElementInXLSheet(getParameterFinance, "txt_customerReference_OPA_CR");
		txt_description_OPA_CR = findElementInXLSheet(getParameterFinance, "txt_description_OPA_CR");

		/* FIN_OPA_44 */
		drop_currency_OPA_CR = findElementInXLSheet(getParameterFinance, "drop_currency_OPA_CR");
		lbl_currencyTotakLable_OPA_CR = findElementInXLSheet(getParameterFinance, "lbl_currencyTotakLable_OPA_CR");

		/* FIN_OPA_45 */
		drop_taxGroup_OPA_CR = findElementInXLSheet(getParameterFinance, "drop_taxGroup_OPA_CR");

		/* FIN_OPA_46 */
		span_glLookup_OPA_CR = findElementInXLSheet(getParameterFinance, "span_glLookup_OPA_CR");
		header_glAccountLookup = findElementInXLSheet(getParameterFinance, "header_glAccountLookup");
		td_resultGlAccount_OPA_CR = findElementInXLSheet(getParameterFinance, "td_resultGlAccount_OPA_CR");
		div_glAccountAfterSeleted_OPA_CR = findElementInXLSheet(getParameterFinance,
				"div_glAccountAfterSeleted_OPA_CR");

		/* FIN_OPA_47_48 */
		txt_amount_OPA_CR = findElementInXLSheet(getParameterFinance, "txt_amount_OPA_CR");

		/* FIN_OPA_49_A */
		header_drafted_OPA_CR = findElementInXLSheet(getParameterFinance, "header_drafted_OPA_CR");
		header_released_OPA_CR = findElementInXLSheet(getParameterFinance, "header_released_OPA_CR");

		/* FIN_OPA_49_B */
		header_reversed_OPA_CR = findElementInXLSheet(getParameterFinance, "header_reversed_OPA_CR");

		/* FIN_OPA_50 */
		header_newVendorPaymentCR = findElementInXLSheet(getParameterFinance, "header_newVendorPaymentCR");
		drop_payCurrencyOP_CR = findElementInXLSheet(getParameterFinance, "drop_payCurrencyOP_CR");
		drop_payMethod_CR = findElementInXLSheet(getParameterFinance, "drop_payMethod_CR");
		tab_detailsOP_CR = findElementInXLSheet(getParameterFinance, "tab_detailsOP_CR");
		chk_selectPaymenAdviceOP_CR = findElementInXLSheet(getParameterFinance, "chk_selectPaymenAdviceOP_CR");
		header_draftedOP_CR = findElementInXLSheet(getParameterFinance, "header_draftedOP_CR");
		header_releasedOP_CR = findElementInXLSheet(getParameterFinance, "header_releasedOP_CR");

		/* FIN_OPA_51 */
		header_released_IPA_CDM = findElementInXLSheet(getParameterFinance, "header_released_IPA_CDM");
		btn_setOff_OPA_CR = findElementInXLSheet(getParameterFinance, "btn_setOff_OPA_CR");
		chk_setOffAdvice_OPA_CR = findElementInXLSheet(getParameterFinance, "chk_setOffAdvice_OPA_CR");
		txt_paiAmountSetOffWindow_OPA_CR = findElementInXLSheet(getParameterFinance,
				"txt_paiAmountSetOffWindow_OPA_CR");
		p_successfullySetOff_OPA_CR = findElementInXLSheet(getParameterFinance, "p_successfullySetOff_OPA_CR");

		/* FIN_OPA_53 */
		chk_reverseSetOffAdvice_OPA_CR = findElementInXLSheet(getParameterFinance, "chk_reverseSetOffAdvice_OPA_CR");
		p_successfullyReverseSetOff_OPA_CR = findElementInXLSheet(getParameterFinance,
				"p_successfullyReverseSetOff_OPA_CR");

		/* FIN_OPA_54 */
		p_successfullValidationSetOffHalfAmountCR = findElementInXLSheet(getParameterFinance,
				"p_successfullValidationSetOffHalfAmountCR");

		/* FIN_OPA_55 */
		header_new_OPA_PV = findElementInXLSheet(getParameterFinance, "header_new_OPA_PV");
		txt_description_OPA_PV = findElementInXLSheet(getParameterFinance, "txt_description_OPA_PV");

		/* FIN_OPA_56 */
		drop_currency_OPA_PV = findElementInXLSheet(getParameterFinance, "drop_currency_OPA_PV");

		/* FIN_OPA_57 */
		drop_taxGroup_OPA_PV = findElementInXLSheet(getParameterFinance, "drop_taxGroup_OPA_PV");

		/* FIN_OPA_58 */
		span_glLookup_OPA_PV = findElementInXLSheet(getParameterFinance, "span_glLookup_OPA_PV");
		td_resultGlAccount_OPA_PV = findElementInXLSheet(getParameterFinance, "td_resultGlAccount_OPA_PV");
		div_glAccountAfterSeleted_OPA_PV = findElementInXLSheet(getParameterFinance,
				"div_glAccountAfterSeleted_OPA_PV");

		/* FIN_OPA_59_60 */
		txt_amount_OPA_PV = findElementInXLSheet(getParameterFinance, "txt_amount_OPA_PV");

		/* FIN_OPA_61_A */
		header_drafted_OPA_PV = findElementInXLSheet(getParameterFinance, "header_drafted_OPA_PV");
		header_released_OPA_PV = findElementInXLSheet(getParameterFinance, "header_released_OPA_PV");

		/* FIN_OPA_61_B */
		header_reversed_OPA_PV = findElementInXLSheet(getParameterFinance, "header_reversed_OPA_PV");

		/* FIN_OPA_62 */
		header_newVendorPaymentPV = findElementInXLSheet(getParameterFinance, "header_newVendorPaymentPV");
		drop_payCurrencyOP_PV = findElementInXLSheet(getParameterFinance, "drop_payCurrencyOP_PV");
		drop_payMethod_PV = findElementInXLSheet(getParameterFinance, "drop_payMethod_PV");
		tab_detailsOP_PV = findElementInXLSheet(getParameterFinance, "tab_detailsOP_PV");
		chk_selectPaymenAdviceOP_PV = findElementInXLSheet(getParameterFinance, "chk_selectPaymenAdviceOP_PV");
		header_draftedOP_PV = findElementInXLSheet(getParameterFinance, "header_draftedOP_PV");
		header_releasedOP_PV = findElementInXLSheet(getParameterFinance, "header_releasedOP_PV");
		txt_payee_OP_PV = findElementInXLSheet(getParameterFinance, "txt_payee_OP_PV");

		/* FIN_OPA_63 */
		header_paidAmount_OP_PV = findElementInXLSheet(getParameterFinance, "header_paidAmount_OP_PV");
		txt_paidAmount_OP_PV = findElementInXLSheet(getParameterFinance, "txt_paidAmount_OP_PV");

		/* FIN_OPA_64_A */
		header_new_OPA_EPA = findElementInXLSheet(getParameterFinance, "header_new_OPA_EPA");
		btn_employeeLookup_OPA_EPA = findElementInXLSheet(getParameterFinance, "btn_employeeLookup_OPA_EPA");
		lookup_employee = findElementInXLSheet(getParameterFinance, "lookup_employee");
		td_resultEmployee_OPA_EPA = findElementInXLSheet(getParameterFinance, "td_resultEmployee_OPA_EPA");
		txt_selectedEmployee_OPA_EPA = findElementInXLSheet(getParameterFinance, "txt_selectedEmployee_OPA_EPA");
		btn_searchEmployeeLookup = findElementInXLSheet(getParameterFinance, "btn_searchEmployeeLookup");

		/* FIN_OPA_65_A */
		txt_employeeReference_OPA_EPA = findElementInXLSheet(getParameterFinance, "txt_employeeReference_OPA_EPA");
		txt_description_OPA_EPA = findElementInXLSheet(getParameterFinance, "txt_description_OPA_EPA");

		/* FIN_OPA_66_A */
		span_glLookup_OPA_EPA = findElementInXLSheet(getParameterFinance, "span_glLookup_OPA_EPA");
		td_resultGlAccount_OPA_EPA = findElementInXLSheet(getParameterFinance, "td_resultGlAccount_OPA_EPA");
		div_glAccountAfterSeleted_OPA_EPA = findElementInXLSheet(getParameterFinance,
				"div_glAccountAfterSeleted_OPA_EPA");

		/* FIN_OPA_67_A_68_A */
		txt_amount_OPA_EPA = findElementInXLSheet(getParameterFinance, "txt_amount_OPA_EPA");

		/* FIN_OPA_69_A */
		header_columnDescriptionGid_OPA_EPA = findElementInXLSheet(getParameterFinance,
				"header_columnDescriptionGid_OPA_EPA");
		txt_descriptionOnGrid_OPA_EPA = findElementInXLSheet(getParameterFinance, "txt_descriptionOnGrid_OPA_EPA");

		/* FIN_OPA_70_A */
		header_drafted_OPA_EPA = findElementInXLSheet(getParameterFinance, "header_drafted_OPA_EPA");
		header_released_OPA_EPA = findElementInXLSheet(getParameterFinance, "header_released_OPA_EPA");

		/* FIN_OPA_70_A_B */
		header_reversed_OPA_EPA = findElementInXLSheet(getParameterFinance, "header_reversed_OPA_EPA");

		/* FIN_OPA_71_A_72 */
		header_newEmployeePaymentEP = findElementInXLSheet(getParameterFinance, "header_newEmployeePaymentEP");
		drop_payCurrencyOP_EP = findElementInXLSheet(getParameterFinance, "drop_payCurrencyOP_EP");
		drop_payMethod_EP = findElementInXLSheet(getParameterFinance, "drop_payMethod_EP");
		tab_detailsOP_EP = findElementInXLSheet(getParameterFinance, "tab_detailsOP_EP");
		chk_selectPaymenAdviceOP_EP = findElementInXLSheet(getParameterFinance, "chk_selectPaymenAdviceOP_EP");
		txt_paidAmount_OP_EP = findElementInXLSheet(getParameterFinance, "txt_paidAmount_OP_EP");
		header_paidAmount_OP_EP = findElementInXLSheet(getParameterFinance, "header_paidAmount_OP_EP");
		header_draftedOP_EP = findElementInXLSheet(getParameterFinance, "header_draftedOP_EP");
		header_releasedOP_EP = findElementInXLSheet(getParameterFinance, "header_releasedOP_EP");

		/* FIN_OPA_75_A */
		txt_amountImEditingStatge_OPA_EPA = findElementInXLSheet(getParameterFinance,
				"txt_amountImEditingStatge_OPA_EPA");
		header_amountGridEditingStage_OPA_EPA = findElementInXLSheet(getParameterFinance,
				"header_amountGridEditingStage_OPA_EPA");
		txt_amountDraftedStatge_OPA_EPA = findElementInXLSheet(getParameterFinance, "txt_amountDraftedStatge_OPA_EPA");

		/* FIN_OPA_76_A */
		lnk_outboundPaymentAdviceHeaderOnNewPage = findElementInXLSheet(getParameterFinance,
				"lnk_outboundPaymentAdviceHeaderOnNewPage");
		lnk_resultOPAByPage = findElementInXLSheet(getParameterFinance, "lnk_resultOPAByPage");

		/* FIN_OPA_76_A */
		drop_currency_OPA_EPA = findElementInXLSheet(getParameterFinance, "drop_currency_OPA_EPA");

		/* FIN_OPA_78_A */
		header_deleeted_OPA_EPA = findElementInXLSheet(getParameterFinance, "header_deleeted_OPA_EPA");

		/* FIN_OPA_79_A */
		td_descriptionHistoryDetailsUpdatedOPA = findElementInXLSheet(getParameterFinance,
				"td_descriptionHistoryDetailsUpdatedOPA");

		/* FIN_OPA_80_A */
		lbl_recentlyDraftedOutboundPaymentAdvice = findElementInXLSheet(getParameterFinance,
				"lbl_recentlyDraftedOutboundPaymentAdvice");
		div_descriptionAfterDraftThe_OPA_EPA = findElementInXLSheet(getParameterFinance,
				"div_descriptionAfterDraftThe_OPA_EPA");
		lnk_releventDraftedOutboundPaymentAdviceDocReplace = findElementInXLSheet(getParameterFinance,
				"lnk_releventDraftedOutboundPaymentAdviceDocReplace");

		/* FIN_OPA_81_A */
		header_lookup_OPA = findElementInXLSheet(getParameterFinance, "header_lookup_OPA");

		/* FIN_OPA_84_A */
		header_hold_OPA_EPA = findElementInXLSheet(getParameterFinance, "header_hold_OPA_EPA");

		/* Employee Advance Advice */
		/* FIN_OPA_64_B */
		btn_employeeAdvanceAdviceJourneyOPA = findElementInXLSheet(getParameterFinance,
				"btn_employeeAdvanceAdviceJourneyOPA");
		header_new_OPA_EAA = findElementInXLSheet(getParameterFinance, "header_new_OPA_EAA");
		btn_employeeLookup_OPA_EAA = findElementInXLSheet(getParameterFinance, "btn_employeeLookup_OPA_EAA");
		td_resultEmployee_OPA_EAA = findElementInXLSheet(getParameterFinance, "td_resultEmployee_OPA_EAA");
		txt_selectedEmployee_OPA_EAA = findElementInXLSheet(getParameterFinance, "txt_selectedEmployee_OPA_EAA");

		/* FIN_OPA_65_B */
		txt_employeeReference_OPA_EAA = findElementInXLSheet(getParameterFinance, "txt_employeeReference_OPA_EAA");
		txt_description_OPA_EAA = findElementInXLSheet(getParameterFinance, "txt_description_OPA_EAA");

		/* FIN_OPA_67_B_68_B */
		txt_amount_OPA_EAA = findElementInXLSheet(getParameterFinance, "txt_amount_OPA_EAA");

		/* FIN_OPA_70_B_70_B_B */
		header_drafted_OPA_EAA = findElementInXLSheet(getParameterFinance, "header_drafted_OPA_EAA");
		header_released_OPA_EAA = findElementInXLSheet(getParameterFinance, "header_released_OPA_EAA");
		header_reversed_OPA_EAA = findElementInXLSheet(getParameterFinance, "header_reversed_OPA_EAA");

		/* FIN_OPA_71_B */
		header_newEmployeePaymentEA = findElementInXLSheet(getParameterFinance, "header_newEmployeePaymentEA");
		drop_payCurrencyOP_EA = findElementInXLSheet(getParameterFinance, "drop_payCurrencyOP_EA");
		drop_payMethod_EA = findElementInXLSheet(getParameterFinance, "drop_payMethod_EA");
		tab_detailsOP_EA = findElementInXLSheet(getParameterFinance, "tab_detailsOP_EA");
		chk_selectPaymenAdviceOP_EA = findElementInXLSheet(getParameterFinance, "chk_selectPaymenAdviceOP_EA");
		header_draftedOP_EA = findElementInXLSheet(getParameterFinance, "header_draftedOP_EA");
		header_releasedOP_EA = findElementInXLSheet(getParameterFinance, "header_releasedOP_EA");

		/* FIN_OPA_73 */
		txt_paidAmount_OP_EA = findElementInXLSheet(getParameterFinance, "txt_paidAmount_OP_EA");
		header_paidAmount_OP_EA = findElementInXLSheet(getParameterFinance, "header_paidAmount_OP_EA");
		p_mainErrorValidation = findElementInXLSheet(getParameterFinance, "p_mainErrorValidation");
		txt_paidAmountWithErrorBorder_OP_EA = findElementInXLSheet(getParameterFinance,
				"txt_paidAmountWithErrorBorder_OP_EA");
		btn_rowError_OP_EA = findElementInXLSheet(getParameterFinance, "btn_rowError_OP_EA");
		div_errorPaidAmountShoulsEquelToDueAmount_OP_EA = findElementInXLSheet(getParameterFinance,
				"div_errorPaidAmountShoulsEquelToDueAmount_OP_EA");

		/* FIN_OPA_74_B */
		txt_amountAfterDraft_OPA_EAA = findElementInXLSheet(getParameterFinance, "txt_amountAfterDraft_OPA_EAA");

		/* FIN_OPA_77_B */
		drop_currency_OPA_EAA = findElementInXLSheet(getParameterFinance, "drop_currency_OPA_EAA");

		/* FIN_OPA_78_B */
		header_deleeted_OPA_EAA = findElementInXLSheet(getParameterFinance, "header_deleeted_OPA_EAA");

		/* FIN_OPA_80_B */
		div_descriptionAfterDraftThe_OPA_EAA = findElementInXLSheet(getParameterFinance,
				"div_descriptionAfterDraftThe_OPA_EAA");

		/* FIN_OPA_82_B_84_B_85_B */
		header_hold_OPA_EAA = findElementInXLSheet(getParameterFinance, "header_hold_OPA_EAA");

		/* Outbound Payment */
		/* FIN_OP_01 */
		btn_customerPaymentVoucherJourneyOP = findElementInXLSheet(getParameterFinance,
				"btn_customerPaymentVoucherJourneyOP");
		btn_vendorPaymentVoucherJourneyOP = findElementInXLSheet(getParameterFinance,
				"btn_vendorPaymentVoucherJourneyOP");
		btn_paymentVoucherJourneyOP = findElementInXLSheet(getParameterFinance, "btn_paymentVoucherJourneyOP");
		btn_fundTransferVoucherJourneyOP = findElementInXLSheet(getParameterFinance,
				"btn_fundTransferVoucherJourneyOP");
		btn_employeePaymentVoucherJourneyOP = findElementInXLSheet(getParameterFinance,
				"btn_employeePaymentVoucherJourneyOP");
		header_outboundPaymentByPage = findElementInXLSheet(getParameterFinance, "header_outboundPaymentByPage");
		btn_newOutboundPayment = findElementInXLSheet(getParameterFinance, "btn_newOutboundPayment");

		/* FIN_OP_02 */
		header_newCustomerPaymentVoucherOP = findElementInXLSheet(getParameterFinance,
				"header_newCustomerPaymentVoucherOP");

		/* FIN_OP_03 */
		btn_customerLookup_OP_CPV = findElementInXLSheet(getParameterFinance, "btn_customerLookup_OP_CPV");
		td_resultCustomer_OP_CPV = findElementInXLSheet(getParameterFinance, "td_resultCustomer_OP_CPV");
		txt_selectedCustomer_OP_CPV = findElementInXLSheet(getParameterFinance, "txt_selectedCustomer_OP_CPV");
		drop_contactPerson_OP_CPV = findElementInXLSheet(getParameterFinance, "drop_contactPerson_OP_CPV");
		txt_billingAddress_OP_CPV = findElementInXLSheet(getParameterFinance, "txt_billingAddress_OP_CPV");

		/* FIN_OP_04 */
		drop_currency_OP_CPV = findElementInXLSheet(getParameterFinance, "drop_currency_OP_CPV");

		/* FIN_OP_05 */
		tab_details_OP_CPV = findElementInXLSheet(getParameterFinance, "tab_details_OP_CPV");
		drop_filterCurrency_OP_CPV = findElementInXLSheet(getParameterFinance, "drop_filterCurrency_OP_CPV");

		/* FIN_OP_06 */
		drop_paybook_OP_CPV = findElementInXLSheet(getParameterFinance, "drop_paybook_OP_CPV");
		drop_paymentMethod_OP_CPV = findElementInXLSheet(getParameterFinance, "drop_paymentMethod_OP_CPV");
		btn_tabdDetails_OP_CPV = findElementInXLSheet(getParameterFinance, "btn_tabdDetails_OP_CPV");
		chk_firstAdvice_OP_CPV = findElementInXLSheet(getParameterFinance, "chk_firstAdvice_OP_CPV");
		header_drafted_OP_CPV = findElementInXLSheet(getParameterFinance, "header_drafted_OP_CPV");
		header_released_OP_CPV = findElementInXLSheet(getParameterFinance, "header_released_OP_CPV");
		lbl_debitJournelEntry_OP_CPV = findElementInXLSheet(getParameterFinance, "lbl_debitJournelEntry_OP_CPV");
		lbl_creditJournelEntry_OP_CPV = findElementInXLSheet(getParameterFinance, "lbl_creditJournelEntry_OP_CPV");

		/* FIN_OP_07 */
		txt_payee_OP_CPV = findElementInXLSheet(getParameterFinance, "txt_payee_OP_CPV");
		lbl_creditJournelEntryChecquPayment_OP_CPV = findElementInXLSheet(getParameterFinance,
				"lbl_creditJournelEntryChecquPayment_OP_CPV");

		/* FIN_OP_08 */
		txt_chequeDate_OP_CPV = findElementInXLSheet(getParameterFinance, "txt_chequeDate_OP_CPV");
		txt_bankingDate_OP_CPV = findElementInXLSheet(getParameterFinance, "txt_bankingDate_OP_CPV");
		btn_nextArrowCalender_OP_CPV = findElementInXLSheet(getParameterFinance, "btn_nextArrowCalender_OP_CPV");
		btn_fifteenCalenderDate = findElementInXLSheet(getParameterFinance, "btn_fifteenCalenderDate");

		/* FIN_OP_10 */
		lbl_creditJournelEntryEFTPayment_OP_CPV = findElementInXLSheet(getParameterFinance,
				"lbl_creditJournelEntryEFTPayment_OP_CPV");

		/* FIN_OP_13 */
		lbl_creditJournelEntryLKRvsUSD_OP_CPV = findElementInXLSheet(getParameterFinance,
				"lbl_creditJournelEntryLKRvsUSD_OP_CPV");
		lbl_debitJournelEntryExchangeRelizedGain_OP_CPV = findElementInXLSheet(getParameterFinance,
				"lbl_debitJournelEntryExchangeRelizedGain_OP_CPV");
		lbl_creditJournelEntryExchangeRelizedGain_OP_CPV = findElementInXLSheet(getParameterFinance,
				"lbl_creditJournelEntryExchangeRelizedGain_OP_CPV");

		/* FIN_OP_14 */
		td_resultCustomer_2_OPA_CR = findElementInXLSheet(getParameterFinance, "td_resultCustomer_2_OPA_CR");
		txt_paidAmountSetOffDocReplace_OP_CPV = findElementInXLSheet(getParameterFinance,
				"txt_paidAmountSetOffDocReplace_OP_CPV");
		header_paidAmountSetOff_OP_CPV = findElementInXLSheet(getParameterFinance, "header_paidAmountSetOff_OP_CPV");
		btn_setOff_OP_CPV = findElementInXLSheet(getParameterFinance, "btn_setOff_OP_CPV");
		p_validationSetOffSuccessfull_OP_CPV = findElementInXLSheet(getParameterFinance,
				"p_validationSetOffSuccessfull_OP_CPV");
		txt_dueAmount_OP_CPV = findElementInXLSheet(getParameterFinance, "txt_dueAmount_OP_CPV");
		btn_setOffWidget_OP_CPV = findElementInXLSheet(getParameterFinance, "btn_setOffWidget_OP_CPV");
		window_outboundPaymentSetOff = findElementInXLSheet(getParameterFinance, "window_outboundPaymentSetOff");
		btn_closeOutboundPaymentSetOffWindow = findElementInXLSheet(getParameterFinance,
				"btn_closeOutboundPaymentSetOffWindow");

		/* FIN_OP_15 */
		txt_paidAmount_OP_CPV = findElementInXLSheet(getParameterFinance, "txt_paidAmount_OP_CPV");

		/* Vendor Payment Voucher */
		/* FIN_OP_16 */
		btn_vendorLookup_OP_VPV = findElementInXLSheet(getParameterFinance, "btn_vendorLookup_OP_VPV");
		td_resultVendor_OP_VPV = findElementInXLSheet(getParameterFinance, "td_resultVendor_OP_VPV");
		txt_selectedVendor_OP_VPV = findElementInXLSheet(getParameterFinance, "txt_selectedVendor_OP_VPV");
		drop_autoSelectedContactPerson_OP_VPV = findElementInXLSheet(getParameterFinance,
				"drop_autoSelectedContactPerson_OP_VPV");
		drop_autoSelectedAddress_OP_VPV = findElementInXLSheet(getParameterFinance, "drop_autoSelectedAddress_OP_VPV");

		/* FIN_OP_17 */
		txt_cashAccount_OP_VPV = findElementInXLSheet(getParameterFinance, "txt_cashAccount_OP_VPV");
		drop_payBook_OP_VPV = findElementInXLSheet(getParameterFinance, "drop_payBook_OP_VPV");
		drop_oayMethod_OP_VPV = findElementInXLSheet(getParameterFinance, "drop_oayMethod_OP_VPV");
		
		/* FIN_OP_18_19 */
		drop_filterCurrency_OP_VPV = findElementInXLSheet(getParameterFinance, "drop_filterCurrency_OP_VPV");
		tab_details_OP_VPV = findElementInXLSheet(getParameterFinance, "tab_details_OP_VPV");
		chk_advice_OP_VPV = findElementInXLSheet(getParameterFinance, "chk_advice_OP_VPV");
		header_drafted_OP_VPV = findElementInXLSheet(getParameterFinance, "header_drafted_OP_VPV");
		header_released_OP_VPV = findElementInXLSheet(getParameterFinance, "header_released_OP_VPV");
		lbl_debitJournelEntry_OP_VPV = findElementInXLSheet(getParameterFinance, "lbl_debitJournelEntry_OP_VPV");
		lbl_creditJournelEntry_OP_VPV = findElementInXLSheet(getParameterFinance, "lbl_creditJournelEntry_OP_VPV");
		
		/* FIN_OP_20 */
		txt_payee_OP_VPV = findElementInXLSheet(getParameterFinance, "txt_payee_OP_VPV");
		lbl_creditJournelEntryChecquPayment_OP_VPV = findElementInXLSheet(getParameterFinance, "lbl_creditJournelEntryChecquPayment_OP_VPV");

		/* FIN_OP_21 */
		txt_chequeDate_OP_VPV = findElementInXLSheet(getParameterFinance, "txt_chequeDate_OP_VPV");
		btn_nextArrowCalender_OP_VPV = findElementInXLSheet(getParameterFinance, "btn_nextArrowCalender_OP_VPV");
		btn_fifteenCalenderDate_OP_VPV = findElementInXLSheet(getParameterFinance, "btn_fifteenCalenderDate_OP_VPV");
		txt_bankingDate_OP_VPV = findElementInXLSheet(getParameterFinance, "txt_bankingDate_OP_VPV");
		
		/* FIN_OP_22 */
		header_new_OP_VPV = findElementInXLSheet(getParameterFinance, "header_new_OP_VPV");
	
		/* FIN_OP_23 */
		lbl_creditJournelEntryEFTPayment_OP_VPV = findElementInXLSheet(getParameterFinance, "lbl_creditJournelEntryEFTPayment_OP_VPV");

		/* FIN_OP_25 */
		drop_currency_OP_VPV = findElementInXLSheet(getParameterFinance, "drop_currency_OP_VPV");
		
		/* FIN_OP_26 */
		lbl_creditJournelEntryLKRvsUSD_OP_VPV = findElementInXLSheet(getParameterFinance, "lbl_creditJournelEntryLKRvsUSD_OP_VPV");
		lbl_debitJournelEntryExchangeRelizedGain_OP_VPV = findElementInXLSheet(getParameterFinance, "lbl_debitJournelEntryExchangeRelizedGain_OP_VPV");
		lbl_creditJournelEntryExchangeRelizedGain_OP_VPV = findElementInXLSheet(getParameterFinance, "lbl_creditJournelEntryExchangeRelizedGain_OP_VPV");
		
		/* FIN_OP_27 */
		div_dueAmountSetOff_OP_VPV = findElementInXLSheet(getParameterFinance, "div_dueAmountSetOff_OP_VPV");
		header_dueAmountSetOff_OP_VPV = findElementInXLSheet(getParameterFinance, "header_dueAmountSetOff_OP_VPV");
		btn_setOffWidget_OP_VPV = findElementInXLSheet(getParameterFinance, "btn_setOffWidget_OP_VPV");
		header_paidAmountSetOff_OP_VPV = findElementInXLSheet(getParameterFinance, "header_paidAmountSetOff_OP_VPV");
		txt_paidAmountSetOff_OP_VPV = findElementInXLSheet(getParameterFinance, "txt_paidAmountSetOff_OP_VPV");
		btn_setOff_OP_VPV = findElementInXLSheet(getParameterFinance, "btn_setOff_OP_VPV");
		p_validationSetOffSuccessfull_OP_VPV = findElementInXLSheet(getParameterFinance, "p_validationSetOffSuccessfull_OP_VPV");
		txt_dueAmount_OP_VPV = findElementInXLSheet(getParameterFinance, "txt_dueAmount_OP_VPV");
		
		/* FIN_OP_28 */
		txt_paidAmount_OP_VPV = findElementInXLSheet(getParameterFinance, "txt_paidAmount_OP_VPV");
		
		/* Payment Voucher */
		/* FIN_OP_29 */
		header_new_OP_PV = findElementInXLSheet(getParameterFinance, "header_new_OP_PV");
		drop_currency_OP_PV = findElementInXLSheet(getParameterFinance, "drop_currency_OP_PV");
		drop_payBook_OP_PV = findElementInXLSheet(getParameterFinance, "drop_payBook_OP_PV");
		drop_payMethod_OP_PV = findElementInXLSheet(getParameterFinance, "drop_payMethod_OP_PV");
		txt_cashAccount_OP_PV = findElementInXLSheet(getParameterFinance, "txt_cashAccount_OP_PV");
		tab_details_OP_PV = findElementInXLSheet(getParameterFinance, "tab_details_OP_PV");
		chk_advice_OP_PV = findElementInXLSheet(getParameterFinance, "chk_advice_OP_PV");
		lbl_bannerTotal_OP_PV = findElementInXLSheet(getParameterFinance, "lbl_bannerTotal_OP_PV");
		header_drafted_OP_PV = findElementInXLSheet(getParameterFinance, "header_drafted_OP_PV");
		header_released_OP_PV = findElementInXLSheet(getParameterFinance, "header_released_OP_PV");
		lbl_debitJournelEntry_OP_PV = findElementInXLSheet(getParameterFinance, "lbl_debitJournelEntry_OP_PV");
		lbl_creditJournelEntry_OP_PV = findElementInXLSheet(getParameterFinance, "lbl_creditJournelEntry_OP_PV");
		btn_view_OP_PV = findElementInXLSheet(getParameterFinance, "btn_view_OP_PV");
		
		/* FIN_OP_30 */
		lbl_creditJournelEntryChequePayment_OP_PV = findElementInXLSheet(getParameterFinance, "lbl_creditJournelEntryChequePayment_OP_PV");
		
		/* FIN_OP_32 */
		lbl_creditJournelEntryEFTPayment_OP_PV = findElementInXLSheet(getParameterFinance, "lbl_creditJournelEntryEFTPayment_OP_PV");

		/* FIN_OP_35 */
		drop_filterCurrency_OP_PV = findElementInXLSheet(getParameterFinance, "drop_filterCurrency_OP_PV");
		
		/* Fund Transfer Voucher */
		/* FIN_OP_36 */
		header_newFundTransferVoucherOP = findElementInXLSheet(getParameterFinance, "header_newFundTransferVoucherOP");
		lookup_cashBook_OP_FTV = findElementInXLSheet(getParameterFinance, "lookup_cashBook_OP_FTV");
		btn_reciverCashBookLookup_OP_FTV = findElementInXLSheet(getParameterFinance,
				"btn_reciverCashBookLookup_OP_FTV");
		txt_searchCashBook = findElementInXLSheet(getParameterFinance, "txt_searchCashBook");
		btn_searchCashBookLookup = findElementInXLSheet(getParameterFinance, "btn_searchCashBookLookup");
		td_resultCashBook_OP_FTV = findElementInXLSheet(getParameterFinance, "td_resultCashBook_OP_FTV");
		txt_receiverCashBookFrontPage_OP_FTV = findElementInXLSheet(getParameterFinance,
				"txt_receiverCashBookFrontPage_OP_FTV");

		/* FIN_OP_37 */
		txt_releventCashBookAccordingTpPayBook_OP_FTV = findElementInXLSheet(getParameterFinance,
				"txt_releventCashBookAccordingTpPayBook_OP_FTV");
		drop_payBook_OP_FTV = findElementInXLSheet(getParameterFinance, "drop_payBook_OP_FTV");
		chk_adviceFirstOne_OP_FTV = findElementInXLSheet(getParameterFinance, "chk_adviceFirstOne_OP_FTV");
		p_validateSameCashBookForSendAndReceive = findElementInXLSheet(getParameterFinance,
				"p_validateSameCashBookForSendAndReceive");
		td_resultCashBook_2_OP_FTV = findElementInXLSheet(getParameterFinance, "td_resultCashBook_2_OP_FTV");
		tab_pricingAndCharges_2_OP_FTV = findElementInXLSheet(getParameterFinance, "tab_pricingAndCharges_2_OP_FTV");

		/* FIN_OP_38 */
		td_resultPettyCashBook_OPA_FTA = findElementInXLSheet(getParameterFinance, "td_resultPettyCashBook_OPA_FTA");
		p_validationTotalExceedPettyCashBookFloat = findElementInXLSheet(getParameterFinance,
				"p_validationTotalExceedPettyCashBookFloat");

		/* FIN_OP_40 */
		td_resultPettyCashBookAccountReplaceble_OPA_FTA = findElementInXLSheet(getParameterFinance,
				"td_resultPettyCashBookAccountReplaceble_OPA_FTA");
		p_validationPaymentExceedReimbursement_OPA_FTV = findElementInXLSheet(getParameterFinance,
				"p_validationPaymentExceedReimbursement_OPA_FTV");

		/* Employee Payment Voucher */
		/* FIN_OP_41_42 */
		btn_employeeLookup_OP_EPV = findElementInXLSheet(getParameterFinance, "btn_employeeLookup_OP_EPV");
		td_resultEmployee_OP_EPV = findElementInXLSheet(getParameterFinance, "td_resultEmployee_OP_EPV");
		txt_selectedEmployee_OP_EPV = findElementInXLSheet(getParameterFinance, "txt_selectedEmployee_OP_EPV");
		header_newEmployeePaymentVoucherOP = findElementInXLSheet(getParameterFinance,
				"header_newEmployeePaymentVoucherOP");
		drop_currency_OP_EPV = findElementInXLSheet(getParameterFinance, "drop_currency_OP_EPV");
		drop_paybook_OP_EPV = findElementInXLSheet(getParameterFinance, "drop_paybook_OP_EPV");
		drop_paymentMethod_OP_EPV = findElementInXLSheet(getParameterFinance, "drop_paymentMethod_OP_EPV");
		btn_tabdDetails_OP_EPV = findElementInXLSheet(getParameterFinance, "btn_tabdDetails_OP_EPV");
		chk_firstAdvice_OP_EPV = findElementInXLSheet(getParameterFinance, "chk_firstAdvice_OP_EPV");
		header_drafted_OP_EPV = findElementInXLSheet(getParameterFinance, "header_drafted_OP_EPV");
		header_released_OP_EPV = findElementInXLSheet(getParameterFinance, "header_released_OP_EPV");
		lbl_creditJournelEntry_OP_EPV = findElementInXLSheet(getParameterFinance, "lbl_creditJournelEntry_OP_EPV");

		/* FIN_OP_43 */
		lbl_debitJournelEntry_OP_EPV = findElementInXLSheet(getParameterFinance, "lbl_debitJournelEntry_OP_EPV");
		lbl_creditJournelEntryBank_OP_EPV = findElementInXLSheet(getParameterFinance,
				"lbl_creditJournelEntryBank_OP_EPV");
		txt_payee_OP_EPV = findElementInXLSheet(getParameterFinance, "txt_payee_OP_EPV");
		header_checkReturnWindowOutboundPayment = findElementInXLSheet(getParameterFinance,
				"header_checkReturnWindowOutboundPayment");

		/* FIN_OP_46_47 */
		div_payBookInDraftedStage = findElementInXLSheet(getParameterFinance, "div_payBookInDraftedStage");

		/* FIN_OP_48 */
		lnk_outboundPaymentHeaderOnNewPage = findElementInXLSheet(getParameterFinance,
				"lnk_outboundPaymentHeaderOnNewPage");
		lnk_resultOPByPage = findElementInXLSheet(getParameterFinance, "lnk_resultOPByPage");

		/* FIN_OP_50 */
		header_deleeted_OP_EPV = findElementInXLSheet(getParameterFinance, "header_deleeted_OP_EPV");

		/* FIN_OP_51 */
		td_descriptionHistoryDetailsUpdatedOP = findElementInXLSheet(getParameterFinance,
				"td_descriptionHistoryDetailsUpdatedOP");

		/* FIN_OP_52 */
		lbl_recentlyDraftedOutboundPayment = findElementInXLSheet(getParameterFinance,
				"lbl_recentlyDraftedOutboundPayment");
		txt_descriptio_OP_EPV = findElementInXLSheet(getParameterFinance, "txt_descriptio_OP_EPV");
		div_description_OP_EPV = findElementInXLSheet(getParameterFinance, "div_description_OP_EPV");
		lnk_releventDraftedOutboundPaymentDocReplace = findElementInXLSheet(getParameterFinance,
				"lnk_releventDraftedOutboundPaymentDocReplace");

		/* FIN_OP_54 */
		header_reversed_OP_EPV = findElementInXLSheet(getParameterFinance, "header_reversed_OP_EPV");

		/* FIN_OP_55_56_57 */
		lnk_releventAdvice_OP_EPV = findElementInXLSheet(getParameterFinance, "lnk_releventAdvice_OP_EPV");
		header_releasedDocNoReplace_OP_EPV = findElementInXLSheet(getParameterFinance,
				"header_releasedDocNoReplace_OP_EPV");
		txt_subTotal_OP_EPV = findElementInXLSheet(getParameterFinance, "txt_subTotal_OP_EPV");
		txt_discount_OP_EPV = findElementInXLSheet(getParameterFinance, "txt_discount_OP_EPV");

		// Aruna_JournalEntry
		txt_userNameA = findElementInXLSheet(getParameterFinance, "userNameA");
		txt_passwordA = findElementInXLSheet(getParameterFinance, "passwordA");
		btn_loginA = findElementInXLSheet(getParameterFinance, "loginA");
		siteLogoA = findElementInXLSheet(getParameterFinance, "siteLogoA");
		headerLinkA = findElementInXLSheet(getParameterFinance, "headerLinkA");
		btn_navbuttonA = findElementInXLSheet(getParameterFinance, "navbuttonA");
		sidemenuA = findElementInXLSheet(getParameterFinance, "sidemenuA");
		subsidemenuA = findElementInXLSheet(getParameterFinance, "subsidemenuA");

		// FIN_18_1to18_12
		btn_FinanceBtnA = findElementInXLSheet(getParameterFinance, "FinanceBtnA");
		btn_JournalEntryBtnA = findElementInXLSheet(getParameterFinance, "JournalEntryBtnA");
		btn_NewJournalEntryBtnA = findElementInXLSheet(getParameterFinance, "NewJournalEntryBtnA");
		txt_PageHeaderA = findElementInXLSheet(getParameterFinance, "PageHeaderA");
		btn_DraftBtnA = findElementInXLSheet(getParameterFinance, "DraftBtnA");
		btn_ReleaseBtnA = findElementInXLSheet(getParameterFinance, "ReleaseBtnA");
		btn_TableErrorBtnA = findElementInXLSheet(getParameterFinance, "TableErrorBtnA");
		txt_TableErrorTxt1A = findElementInXLSheet(getParameterFinance, "TableErrorTxt1A");
		txt_TableErrorTxt2A = findElementInXLSheet(getParameterFinance, "TableErrorTxt2A");
		txt_TableErrorTxt3A = findElementInXLSheet(getParameterFinance, "TableErrorTxt3A");
		btn_GLAccBtnA = findElementInXLSheet(getParameterFinance, "GLAccBtnA");
		txt_GLAccTxtA = findElementInXLSheet(getParameterFinance, "GLAccTxtA");
		sel_GLAccSelA = findElementInXLSheet(getParameterFinance, "GLAccSelA");
		txt_GLAccSelTableValueA = findElementInXLSheet(getParameterFinance, "GLAccSelTableValueA");
		txt_GLAccSelTableValue2A = findElementInXLSheet(getParameterFinance, "GLAccSelTableValue2A");
		txt_NarrationTxtA = findElementInXLSheet(getParameterFinance, "NarrationTxtA");
		txt_DocValueTxtA = findElementInXLSheet(getParameterFinance, "DocValueTxtA");
		btn_GLAccBtn2A = findElementInXLSheet(getParameterFinance, "GLAccBtn2A");
		txt_NarrationTxt2A = findElementInXLSheet(getParameterFinance, "NarrationTxt2A");
		txt_DocValueTxt2A = findElementInXLSheet(getParameterFinance, "DocValueTxt2A");
		txt_DebitValueA = findElementInXLSheet(getParameterFinance, "DebitValueA");
		txt_CreditValueA = findElementInXLSheet(getParameterFinance, "CreditValueA");
		btn_SummaryTabA = findElementInXLSheet(getParameterFinance, "SummaryTabA");
		btn_TablePlusBtn1A = findElementInXLSheet(getParameterFinance, "TablePlusBtn1A");
		txt_pageDraft1A = findElementInXLSheet(getParameterFinance, "pageDraft1A");
		txt_pageRelease1A = findElementInXLSheet(getParameterFinance, "pageRelease1A");
		txt_pageDraft2A = findElementInXLSheet(getParameterFinance, "pageDraft2A");
		txt_pageRelease2A = findElementInXLSheet(getParameterFinance, "pageRelease2A");
		btn_MainTableErrorBtnA = findElementInXLSheet(getParameterFinance, "MainTableErrorBtnA");
		txt_TableErrorTxt4A = findElementInXLSheet(getParameterFinance, "TableErrorTxt4A");
		btn_EditBtnA = findElementInXLSheet(getParameterFinance, "EditBtnA");
		btn_UpdateBtnA = findElementInXLSheet(getParameterFinance, "UpdateBtnA");
		btn_UpdateAndNewBtnA = findElementInXLSheet(getParameterFinance, "UpdateAndNewBtnA");
		btn_DuplicateBtnA = findElementInXLSheet(getParameterFinance, "DuplicateBtnA");
		txt_HeaderDocNoA = findElementInXLSheet(getParameterFinance, "HeaderDocNoA");
		btn_JournalEntryByPageBtnA = findElementInXLSheet(getParameterFinance, "JournalEntryByPageBtnA");
		txt_JournalEntryByPageSearchTxtA = findElementInXLSheet(getParameterFinance, "JournalEntryByPageSearchTxtA");
		sel_JournalEntryByPageSearchSelA = findElementInXLSheet(getParameterFinance, "JournalEntryByPageSearchSelA");
		txt_DocValueDebitTxtA = findElementInXLSheet(getParameterFinance, "DocValueDebitTxtA");
		txt_DocValueCreditTxtA = findElementInXLSheet(getParameterFinance, "DocValueCreditTxtA");

		// FIN_18_13to18_14
		btn_ActionBtnA = findElementInXLSheet(getParameterFinance, "ActionBtnA");
		btn_DeleteBtnA = findElementInXLSheet(getParameterFinance, "DeleteBtnA");
		txt_pageDeleteA = findElementInXLSheet(getParameterFinance, "pageDeleteA");
		btn_YesBtnA = findElementInXLSheet(getParameterFinance, "YesBtnA");
		btn_NoBtnA = findElementInXLSheet(getParameterFinance, "NoBtnA");

		// FIN_18_15
		btn_HistoryBtnA = findElementInXLSheet(getParameterFinance, "HistoryBtnA");
		txt_DraftHistoryTxtA = findElementInXLSheet(getParameterFinance, "DraftHistoryTxtA");

		// FIN_18_16
		btn_DraftAndNewBtnA = findElementInXLSheet(getParameterFinance, "DraftAndNewBtnA");

		// FIN_18_17
		btn_CopyFromBtnA = findElementInXLSheet(getParameterFinance, "CopyFromBtnA");
		txt_CopyFromSearchTxtA = findElementInXLSheet(getParameterFinance, "CopyFromSearchTxtA");
		sel_CopyFromSearchSelA = findElementInXLSheet(getParameterFinance, "CopyFromSearchSelA");

		// FIN_18_18to18_19
		btn_ReverseBtnA = findElementInXLSheet(getParameterFinance, "ReverseBtnA");
		btn_ReverseLookupBtnA = findElementInXLSheet(getParameterFinance, "ReverseLookupBtnA");
		txt_ReverseHistoryTxtA = findElementInXLSheet(getParameterFinance, "ReverseHistoryTxtA");

		// FIN_18_27
		btn_ProcumentBtnA = findElementInXLSheet(getParameterFinance, "ProcumentBtnA");
		btn_PurchaseInvoiceBtnA = findElementInXLSheet(getParameterFinance, "PurchaseInvoiceBtnA");
		btn_NewPurchaseInvoiceBtnA = findElementInXLSheet(getParameterFinance, "NewPurchaseInvoiceBtnA");
		btn_PurchaseInvoiceServiceJourneyA = findElementInXLSheet(getParameterFinance,
				"PurchaseInvoiceServiceJourneyA");

		// Aruna-InboundPaymentAdvice
		// FIN_10_1
		btn_InboundPaymentAdviceBtnA = findElementInXLSheet(getParameterFinance, "InboundPaymentAdviceBtnA");
		btn_InboundPaymentAdviceHeaderA = findElementInXLSheet(getParameterFinance, "InboundPaymentAdviceHeaderA");

		// FIN_10_5to10_10
		btn_NewInboundPaymentAdviceBtnA = findElementInXLSheet(getParameterFinance, "NewInboundPaymentAdviceBtnA");
		btn_JourneyUpArrowBtnA = findElementInXLSheet(getParameterFinance, "JourneyUpArrowBtnA");
		btn_JourneyDownArrowBtnA = findElementInXLSheet(getParameterFinance, "JourneyDownArrowBtnA");
		btn_CustomerAdvanceJourneyBtnA = findElementInXLSheet(getParameterFinance, "CustomerAdvanceJourneyBtnA");
		btn_CustomerDebitMemoJourneyBtnA = findElementInXLSheet(getParameterFinance, "CustomerDebitMemoJourneyBtnA");
		btn_VendorRefundJourneyBtnA = findElementInXLSheet(getParameterFinance, "VendorRefundJourneyBtnA");
		btn_ReceiptVoucherJourneyIPABtnA = findElementInXLSheet(getParameterFinance, "ReceiptVoucherJourneyIPABtnA");
		btn_EmployeeReceiptAdviceJourneyBtnA = findElementInXLSheet(getParameterFinance,
				"EmployeeReceiptAdviceJourneyBtnA");
		btn_CustomerRefundableDepositJourneyBtnA = findElementInXLSheet(getParameterFinance,
				"CustomerRefundableDepositJourneyBtnA");
		btn_EmployeeBalanceReceiptJourneyBtnA = findElementInXLSheet(getParameterFinance,
				"EmployeeBalanceReceiptJourneyBtnA");
		btn_CustomerLookUpIPABtnA = findElementInXLSheet(getParameterFinance, "CustomerLookUpIPABtnA");
		txt_AmountFieldIPATxtA = findElementInXLSheet(getParameterFinance, "AmountFieldIPATxtA");
		btn_SummaryTabIPAA = findElementInXLSheet(getParameterFinance, "SummaryTabIPAA");
		btn_CheckoutBtnA = findElementInXLSheet(getParameterFinance, "CheckoutBtnA");

		// FIN_10_11to10_12B
		btn_ConvertToInboundPaymentIPABtnA = findElementInXLSheet(getParameterFinance,
				"ConvertToInboundPaymentIPABtnA");
		btn_IPAFlagBtnA = findElementInXLSheet(getParameterFinance, "IPAFlagBtnA");
		btn_SetOffActionBtnA = findElementInXLSheet(getParameterFinance, "SetOffActionBtnA");
		sel_SetOffDocSelA = findElementInXLSheet(getParameterFinance, "SetOffDocSelA");
		txt_SetOffPaidAmountTxtA = findElementInXLSheet(getParameterFinance, "SetOffPaidAmountTxtA");
		btn_SetOffBtnA = findElementInXLSheet(getParameterFinance, "SetOffBtnA");
		btn_SetOffDetailActionBtnA = findElementInXLSheet(getParameterFinance, "SetOffDetailActionBtnA");
		txt_PaidAmountTxtA = findElementInXLSheet(getParameterFinance, "PaidAmountTxtA");
		sel_SetOffDetailReverseDocSelA = findElementInXLSheet(getParameterFinance, "SetOffDetailReverseDocSelA");
		btn_SetOffReverseBtnA = findElementInXLSheet(getParameterFinance, "SetOffReverseBtnA");

		// FIN_10_13
		btn_OutboundPaymentBtnA = findElementInXLSheet(getParameterFinance, "OutboundPaymentBtnA");
		btn_NewOutboundPaymentBtnA = findElementInXLSheet(getParameterFinance, "NewOutboundPaymentBtnA");
		btn_CustomerPaymentVoucherJourneyBtnA = findElementInXLSheet(getParameterFinance,
				"CustomerPaymentVoucherJourneyBtnA");
		sel_OutboundPaymentIPASelA = findElementInXLSheet(getParameterFinance, "OutboundPaymentIPASelA");

		// FIN_10_15to10_21
		btn_GLAccIPABtnA = findElementInXLSheet(getParameterFinance, "GLAccIPABtnA");
		txt_GLAccSelIPATableValueA = findElementInXLSheet(getParameterFinance, "GLAccSelIPATableValueA");
		txt_AmountFieldCDMTxtA = findElementInXLSheet(getParameterFinance, "AmountFieldCDMTxtA");
		btn_JournalActionBtnA = findElementInXLSheet(getParameterFinance, "JournalActionBtnA");
		txt_JournalCreditTxtA = findElementInXLSheet(getParameterFinance, "JournalCreditTxtA");
		txt_JournalDeditTxtA = findElementInXLSheet(getParameterFinance, "JournalDeditTxtA");

		// FIN_10_22to10_24
		txt_SetOffValidatorA = findElementInXLSheet(getParameterFinance, "SetOffValidatorA");

		// FIN_10_25to10_30
		btn_VendorLookUpIPABtnA = findElementInXLSheet(getParameterFinance, "VendorLookUpIPABtnA");
		txt_VendorLookUpIPATxtA = findElementInXLSheet(getParameterFinance, "VendorLookUpIPATxtA");
		sel_VendorLookUpIPASelA = findElementInXLSheet(getParameterFinance, "VendorLookUpIPASelA");

		// FIN_10_34to10_39
		txt_DescriptionIPATxtA = findElementInXLSheet(getParameterFinance, "DescriptionIPATxtA");

		// FIN_10_40to10_46
		btn_EmployeeLookupIPABtnA = findElementInXLSheet(getParameterFinance, "EmployeeLookupIPABtnA");
		txt_EmployeeLookupIPATxtA = findElementInXLSheet(getParameterFinance, "EmployeeLookupIPATxtA");
		sel_EmployeeLookupIPASelA = findElementInXLSheet(getParameterFinance, "EmployeeLookupIPASelA");

		// FIN_10_47to10_53
		txt_AmountIPATxtA = findElementInXLSheet(getParameterFinance, "AmountIPATxtA");

		// FIN_10_56to10_58
		txt_AmountAfterDraftIPATxtA = findElementInXLSheet(getParameterFinance, "AmountAfterDraftIPATxtA");
		txt_DescriptionAfterDraftIPATxtA = findElementInXLSheet(getParameterFinance, "DescriptionAfterDraftIPATxtA");

		// FIN_10_61
		txt_DraftedIPAHistoryTextA = findElementInXLSheet(getParameterFinance, "DraftedIPAHistoryTextA");

		// FIN_10_63to10_65
		btn_InboundPaymentAdvicePlusBtnA = findElementInXLSheet(getParameterFinance, "InboundPaymentAdvicePlusBtnA");
		txt_CopyFromIPATxtA = findElementInXLSheet(getParameterFinance, "CopyFromIPATxtA");
		sel_CopyFromIPASelA = findElementInXLSheet(getParameterFinance, "CopyFromIPASelA");
		txt_PageReverseA = findElementInXLSheet(getParameterFinance, "PageReverseA");

		// FIN_10_66to10_67
		btn_HoldBtnA = findElementInXLSheet(getParameterFinance, "HoldBtnA");
		txt_ReasonTxtA = findElementInXLSheet(getParameterFinance, "ReasonTxtA");
		btn_OkBtnA = findElementInXLSheet(getParameterFinance, "OkBtnA");
		txt_PageHoldA = findElementInXLSheet(getParameterFinance, "PageHoldA");
		btn_UnHoldBtnA = findElementInXLSheet(getParameterFinance, "UnHoldBtnA");

		// FIN_10_68
		btn_CostAllocationIPABtnA = findElementInXLSheet(getParameterFinance, "CostAllocationIPABtnA");
		sel_CostCenterSelA = findElementInXLSheet(getParameterFinance, "CostCenterSelA");
		txt_CostAllocationPercentageTxtA = findElementInXLSheet(getParameterFinance, "CostAllocationPercentageTxtA");
		btn_CostAllocationAddRecordBtnA = findElementInXLSheet(getParameterFinance, "CostAllocationAddRecordBtnA");
		txt_CostAllocationAmountTxtA = findElementInXLSheet(getParameterFinance, "CostAllocationAmountTxtA");
		btn_CostAllocationUpdateBtnA = findElementInXLSheet(getParameterFinance, "CostAllocationUpdateBtnA");
		txt_CostAllocationValidatorTxtA = findElementInXLSheet(getParameterFinance, "CostAllocationValidatorTxtA");
		btn_CostAllocationSelDeleteBtnA = findElementInXLSheet(getParameterFinance, "CostAllocationSelDeleteBtnA");

		// FIN_10_69
		btn_SalesAndMarcketingBtnA = findElementInXLSheet(getParameterFinance, "SalesAndMarcketingBtnA");
		btn_SalesInvoiceBtnA = findElementInXLSheet(getParameterFinance, "SalesInvoiceBtnA");
		btn_NewSalesInvoiceBtnA = findElementInXLSheet(getParameterFinance, "NewSalesInvoiceBtnA");
		btn_SalesInvoiceToOutboundShipmentJourneyBtnA = findElementInXLSheet(getParameterFinance,
				"SalesInvoiceToOutboundShipmentJourneyBtnA");
		btn_CustomerLookUpSIBtnA = findElementInXLSheet(getParameterFinance, "CustomerLookUpSIBtnA");
		txt_AutoCompleteProductTxtA = findElementInXLSheet(getParameterFinance, "AutoCompleteProductTxtA");
		txt_AutoCompleteProductQtyTxtA = findElementInXLSheet(getParameterFinance, "AutoCompleteProductQtyTxtA");
		sel_WarehouseSISelA = findElementInXLSheet(getParameterFinance, "WarehouseSISelA");
		btn_DocFlowBtnA = findElementInXLSheet(getParameterFinance, "DocFlowBtnA");
		btn_InboundPaymentDocFlowBtnA = findElementInXLSheet(getParameterFinance, "InboundPaymentDocFlowBtnA");

		// Aruna-InboundPayment
		// FIN_11_1
		btn_InboundPaymentBtnA = findElementInXLSheet(getParameterFinance, "InboundPaymentBtnA");
		txt_InboundPaymentHeaderA = findElementInXLSheet(getParameterFinance, "InboundPaymentHeaderA");

		// FIN_11_5to11_9
		btn_NewInboundPaymentBtnA = findElementInXLSheet(getParameterFinance, "NewInboundPaymentBtnA");
		btn_CustomerReceiptVoucherJourneyBtnA = findElementInXLSheet(getParameterFinance,
				"CustomerReceiptVoucherJourneyBtnA");
		btn_VendorReceiptVoucherJourneyBtnA = findElementInXLSheet(getParameterFinance,
				"VendorReceiptVoucherJourneyBtnA");
		btn_ReceiptVoucherJourneyBtnA = findElementInXLSheet(getParameterFinance, "ReceiptVoucherJourneyBtnA");
		btn_EmployeeReceiptVoucherJourneyBtnA = findElementInXLSheet(getParameterFinance,
				"EmployeeReceiptVoucherJourneyBtnA");
		btn_CustomerLookUpBtnA = findElementInXLSheet(getParameterFinance, "CustomerLookUpBtnA");
		txt_CustomerLookUpTxtA = findElementInXLSheet(getParameterFinance, "CustomerLookUpTxtA");
		sel_CustomerLookUpSelA = findElementInXLSheet(getParameterFinance, "CustomerLookUpSelA");
		txt_CustomerACCTxtA = findElementInXLSheet(getParameterFinance, "CustomerACCTxtA");
		sel_PaymentMethodSelA = findElementInXLSheet(getParameterFinance, "PaymentMethodSelA");
		sel_PaidCurrencySelA = findElementInXLSheet(getParameterFinance, "PaidCurrencySelA");

		// FIN_11_10
		btn_DetailsTabIPBtnA = findElementInXLSheet(getParameterFinance, "DetailsTabIPBtnA");
		txt_JournalCreditIPTxtA = findElementInXLSheet(getParameterFinance, "JournalCreditIPTxtA");
		txt_JournalDebitIPTxtA = findElementInXLSheet(getParameterFinance, "JournalDebitIPTxtA");

		// FIN_11_11
		sel_BankNameIPSelA = findElementInXLSheet(getParameterFinance, "BankNameIPSelA");
		txt_ChequeNoIPTxtA = findElementInXLSheet(getParameterFinance, "ChequeNoIPTxtA");

		// FIN_11_12and11_20
		btn_BankDepositBtnA = findElementInXLSheet(getParameterFinance, "BankDepositBtnA");
		btn_NewBankDepositBtnA = findElementInXLSheet(getParameterFinance, "NewBankDepositBtnA");
		sel_PayMethodBDSelA = findElementInXLSheet(getParameterFinance, "PayMethodBDSelA");
		sel_BankBDSelA = findElementInXLSheet(getParameterFinance, "BankBDSelA");
		sel_BankAccountNoBDSelA = findElementInXLSheet(getParameterFinance, "BankAccountNoBDSelA");
		txt_CheqNoBDTxtA = findElementInXLSheet(getParameterFinance, "CheqNoBDTxtA");
		btn_ViewBDBtnA = findElementInXLSheet(getParameterFinance, "ViewBDBtnA");
		btn_SortByBDBtnA = findElementInXLSheet(getParameterFinance, "SortByBDBtnA");
		btn_DolSelBDBtnA = findElementInXLSheet(getParameterFinance, "DolSelBDBtnA");
		btn_ChequeReturnBtnIPA = findElementInXLSheet(getParameterFinance, "ChequeReturnBtnIPA");
		sel_ReturnResonSelIPA = findElementInXLSheet(getParameterFinance, "ReturnResonSelIPA");
		txt_ReturnRemarkTxtIPA = findElementInXLSheet(getParameterFinance, "ReturnRemarkTxtIPA");
		btn_ReturnBtnIPABtnIPA = findElementInXLSheet(getParameterFinance, "ReturnBtnIPABtnIPA");

		// FIN_11_15
		txt_PayingAmountIPTxtA = findElementInXLSheet(getParameterFinance, "PayingAmountIPTxtA");

		// FIN_11_17
		txt_PayingCurrencyIPTxtA = findElementInXLSheet(getParameterFinance, "PayingCurrencyIPTxtA");
		txt_FilterCurrencyIPTxtA = findElementInXLSheet(getParameterFinance, "FilterCurrencyIPTxtA");

		// FIN_11_21to11_23
		btn_VendorLookUpIPBtnA = findElementInXLSheet(getParameterFinance, "VendorLookUpIPBtnA");

		// FIN_11_33to11_35
		btn_EmployeeLookupIPBtnA = findElementInXLSheet(getParameterFinance, "EmployeeLookupIPBtnA");

		// FIN_11_50
		btn_ViewAllPendingBtnIPA = findElementInXLSheet(getParameterFinance, "ViewAllPendingBtnIPA");

		// FIN_11_51to11_53
		txt_PaymentMethodAfterDraftIPA = findElementInXLSheet(getParameterFinance, "PaymentMethodAfterDraftIPA");
		txt_DescriptionIPA = findElementInXLSheet(getParameterFinance, "DescriptionIPA");
		txt_DescriptionAfterDraftIPA = findElementInXLSheet(getParameterFinance, "DescriptionAfterDraftIPA");

		// FIN_11_55
		txt_DraftedHistoryTxtIPA = findElementInXLSheet(getParameterFinance, "DraftedHistoryTxtIPA");

		// FIN_11_59
		txt_SameChequeNoValidatorIPA = findElementInXLSheet(getParameterFinance, "SameChequeNoValidatorIPA");

		// FIN_16_1to16_10
		sel_cboxEFTBankIP = findElementInXLSheet(getParameterFinance, "cboxEFTBankIP");
		sel_cboxEFTBankAccountIP = findElementInXLSheet(getParameterFinance, "cboxEFTBankAccountIP");
		sel_cboxDDBankIP = findElementInXLSheet(getParameterFinance, "cboxDDBankIP");
		sel_cboxDDBankAccountIP = findElementInXLSheet(getParameterFinance, "cboxDDBankAccountIP");
		btn_OutboundPaymentAdviceBtnA = findElementInXLSheet(getParameterFinance, "OutboundPaymentAdviceBtnA");
		btn_NewOutboundPaymentAdviceBtnA = findElementInXLSheet(getParameterFinance, "NewOutboundPaymentAdviceBtnA");
		btn_VendorAdvanceJourneyBtnOPA = findElementInXLSheet(getParameterFinance, "VendorAdvanceJourneyBtnOPA");
		btn_VendorLookUpBtnOPA = findElementInXLSheet(getParameterFinance, "VendorLookUpBtnOPA");
		txt_AmountTextOPA = findElementInXLSheet(getParameterFinance, "AmountTextOPA");
		btn_ConvertToOutboundPaymentOPABtnA = findElementInXLSheet(getParameterFinance,
				"ConvertToOutboundPaymentOPABtnA");
		sel_cboxPaybook = findElementInXLSheet(getParameterFinance, "cboxPaybook");
		sel_OPAFlagBtnA = findElementInXLSheet(getParameterFinance, "OPAFlagBtnA");
		btn_BankAdjustmentBtnA = findElementInXLSheet(getParameterFinance, "BankAdjustmentBtnA");
		btn_NewBankAdjustmentBtnA = findElementInXLSheet(getParameterFinance, "NewBankAdjustmentBtnA");
		sel_cboxBankBA = findElementInXLSheet(getParameterFinance, "cboxBankBA");
		sel_cboxBankAccountNoBA = findElementInXLSheet(getParameterFinance, "cboxBankAccountNoBA");
		btn_GLAccountLookupBABtnA = findElementInXLSheet(getParameterFinance, "GLAccountLookupBABtnA");
		txt_txtAmountBATxtA = findElementInXLSheet(getParameterFinance, "txtAmountBATxtA");
		btn_BankReconciliationBtnA = findElementInXLSheet(getParameterFinance, "BankReconciliationBtnA");
		btn_NewBankReconciliationBtnA = findElementInXLSheet(getParameterFinance, "NewBankReconciliationBtnA");
		sel_cboxBankBR = findElementInXLSheet(getParameterFinance, "cboxBankBR");
		sel_cboxBankAccountNoBR = findElementInXLSheet(getParameterFinance, "cboxBankAccountNoBR");
		sel_DocSelBR = findElementInXLSheet(getParameterFinance, "DocSelBR");
		txt_StatementBalanceBR = findElementInXLSheet(getParameterFinance, "StatementBalanceBR");
		txt_ReconciliedAmountBR = findElementInXLSheet(getParameterFinance, "ReconciliedAmountBR");
		txt_BankAdjustmentBRValidatorA = findElementInXLSheet(getParameterFinance, "BankAdjustmentBRValidatorA");
		btn_BankReconciliationByPageBtnA = findElementInXLSheet(getParameterFinance, "BankReconciliationByPageBtnA");
		txt_BankReconciliationByPageSearchTxtA = findElementInXLSheet(getParameterFinance,
				"BankReconciliationByPageSearchTxtA");
		sel_BankReconciliationByPageSearchSelA = findElementInXLSheet(getParameterFinance,
				"BankReconciliationByPageSearchSelA");
		sel_DocSel1BR = findElementInXLSheet(getParameterFinance, "DocSel1BR");
		sel_DocSel2BR = findElementInXLSheet(getParameterFinance, "DocSel2BR");

		// FIN_16_18to16_20
		txt_DocNoSearchBRA = findElementInXLSheet(getParameterFinance, "DocNoSearchBRA");

		// FIN_16_33
		txt_BankAdjustmentGrideValueA = findElementInXLSheet(getParameterFinance, "BankAdjustmentGrideValueA");

		// FIN_16_35
		txt_UnReconciliedAmountTxtA = findElementInXLSheet(getParameterFinance, "UnReconciliedAmountTxtA");

		// FIN_16_36
		txt_BankReconciliationDraftValidatorA = findElementInXLSheet(getParameterFinance,
				"BankReconciliationDraftValidatorA");

		// FIN_16_37
		txt_BankReconciliationReverseValidatorA = findElementInXLSheet(getParameterFinance,
				"BankReconciliationReverseValidatorA");
	}

	public static void readData() throws Exception {
		/* Fin_TC_001 */
		String computerName = System.getenv().get("COMPUTERNAME");
		if (computerName.equals("DESKTOP-C6E8CJU") || computerName.equals("MADHUSHAN-LAPfg")) {
			site_Url = findElementInXLSheet(getDataFinance, "siteUrlExternal");
		} else {
			site_Url = findElementInXLSheet(getDataFinance, "site_Url");
		}

		userNameData = findElementInXLSheet(getDataFinance, "userNameData");
		passwordData = findElementInXLSheet(getDataFinance, "passwordData");
		customerAccountIPA = findElementInXLSheet(getDataFinance, "customerAccountIPA");
		testAllCharcterWordIPA = findElementInXLSheet(getDataFinance, "testAllCharcterWordIPA");
		curenceIPA = findElementInXLSheet(getDataFinance, "curenceIPA");
		amountIPA = findElementInXLSheet(getDataFinance, "amountIPA");
		postBusinessUnitIPA = findElementInXLSheet(getDataFinance, "postBusinessUnitIPA");

		/* Fin_TC_002 */
		amountCRV = findElementInXLSheet(getDataFinance, "amountCRV");
		glAccount = findElementInXLSheet(getDataFinance, "glAccount");
		paidCurrencyCRV = findElementInXLSheet(getDataFinance, "paidCurrencyCRV");
		paymentMethodCRV = findElementInXLSheet(getDataFinance, "paymentMethodCRV");
		filterCurrencyCRV = findElementInXLSheet(getDataFinance, "filterCurrencyCRV");
		payingAmountCRV = findElementInXLSheet(getDataFinance, "payingAmountCRV");
		customerAccountCRV = findElementInXLSheet(getDataFinance, "customerAccountCRV");
		paidAmountCRV = findElementInXLSheet(getDataFinance, "paidAmountCRV");

		/* Fin_TC_003 */
		currencySalesInvoise = findElementInXLSheet(getDataFinance, "currencySalesInvoise");
		slaesUnitSlaesInvoice = findElementInXLSheet(getDataFinance, "slaesUnitSlaesInvoice");
		serviceProductSalesInvooice = findElementInXLSheet(getDataFinance, "serviceProductSalesInvooice");
		unitPriceSalesInvice = findElementInXLSheet(getDataFinance, "unitPriceSalesInvice");

		/* Fin_TC_004 */
		paymentMethodBD = findElementInXLSheet(getDataFinance, "paymentMethodBD");
		bankBD = findElementInXLSheet(getDataFinance, "bankBD");

		/* Fin_TC_006 */
		customerCustomerDebitMemo = findElementInXLSheet(getDataFinance, "customerCustomerDebitMemo");
		departmentCostAllocationCustomerDebitMemo = findElementInXLSheet(getDataFinance,
				"departmentCostAllocationCustomerDebitMemo");
		oresentageCostAllocationCustomerDebitMEmo = findElementInXLSheet(getDataFinance,
				"oresentageCostAllocationCustomerDebitMEmo");
		paymentMethodCRV7 = findElementInXLSheet(getDataFinance, "paymentMethodCRV7");
		postBusinessUnitCustomerReciptVoucher = findElementInXLSheet(getDataFinance,
				"postBusinessUnitCustomerReciptVoucher");
		paymentMethodCustomerReciptVoucher = findElementInXLSheet(getDataFinance, "paymentMethodCustomerReciptVoucher");
		payingAmountCRV7 = findElementInXLSheet(getDataFinance, "payingAmountCRV7");
		paidAmountCRV7 = findElementInXLSheet(getDataFinance, "paidAmountCRV7");
		filterCurrencyCustomerReciptVoucher = findElementInXLSheet(getDataFinance,
				"filterCurrencyCustomerReciptVoucher");

		/* Fin_TC_007 */
		paidCurrency08 = findElementInXLSheet(getDataFinance, "paidCurrency08");
		paymentMethod08 = findElementInXLSheet(getDataFinance, "paymentMethod08");
		payingAmount08 = findElementInXLSheet(getDataFinance, "payingAmount08");
		paidAmpunt8 = findElementInXLSheet(getDataFinance, "paidAmpunt8");
		bankCheque8 = findElementInXLSheet(getDataFinance, "bankCheque8");

		/* Fin_TC_009 */
		payMethodBankDeposit09 = findElementInXLSheet(getDataFinance, "payMethodBankDeposit09");
		bank09 = findElementInXLSheet(getDataFinance, "bank09");
		bankAccount09 = findElementInXLSheet(getDataFinance, "bankAccount09");

		/* Fin_TC_010 */
		vendorOutboundPaymentAdvice = findElementInXLSheet(getDataFinance, "vendorOutboundPaymentAdvice");
		referenceCodeOPA = findElementInXLSheet(getDataFinance, "referenceCodeOPA");
		descriptionOPA = findElementInXLSheet(getDataFinance, "descriptionOPA");
		currencyOPA = findElementInXLSheet(getDataFinance, "currencyOPA");
		amountOPA = findElementInXLSheet(getDataFinance, "amountOPA");
		postBusinessUnitOPA = findElementInXLSheet(getDataFinance, "postBusinessUnitOPA");

		/* Fin_TC_011 */
		curencyTypeOutboundPayment = findElementInXLSheet(getDataFinance, "curencyTypeOutboundPayment");
		filterCurencyTypeOutboundPayment = findElementInXLSheet(getDataFinance, "filterCurencyTypeOutboundPayment");
		payBookOutboundPayment = findElementInXLSheet(getDataFinance, "payBookOutboundPayment");

		/* Fin_TC_012 */
		vendorPurchaseInvoice = findElementInXLSheet(getDataFinance, "vendorPurchaseInvoice");
		serviceProductPurchaseInvoice = findElementInXLSheet(getDataFinance, "serviceProductPurchaseInvoice");
		unitPricePurchaseInvice = findElementInXLSheet(getDataFinance, "unitPricePurchaseInvice");
		currencyPurchaseInvoice = findElementInXLSheet(getDataFinance, "currencyPurchaseInvoice");

		/* Fin_TC_014 */
		vendorVendorCreditMemo = findElementInXLSheet(getDataFinance, "vendorVendorCreditMemo");
		description = findElementInXLSheet(getDataFinance, "description");
		referenceCode = findElementInXLSheet(getDataFinance, "referenceCode");
		curencyCreditMemo = findElementInXLSheet(getDataFinance, "curencyCreditMemo");
		glAccountCreditMemo = findElementInXLSheet(getDataFinance, "glAccountCreditMemo");
		amountCreditMemo = findElementInXLSheet(getDataFinance, "amountCreditMemo");
		postBusinessUnit = findElementInXLSheet(getDataFinance, "postBusinessUnit");
		costUnit = findElementInXLSheet(getDataFinance, "costUnit");
		costCenterAlloacatePresentage = findElementInXLSheet(getDataFinance, "costCenterAlloacatePresentage");

		/* Fin_TC_015 */
		vendorVRP = findElementInXLSheet(getDataFinance, "vendorVRP");
		curencyVRP = findElementInXLSheet(getDataFinance, "curencyVRP");
		filterCurrency = findElementInXLSheet(getDataFinance, "filterCurrency");
		postBusinessUnitOutboundPaymentVendorPaymentVoucher = findElementInXLSheet(getDataFinance,
				"postBusinessUnitOutboundPaymentVendorPaymentVoucher");
		payBookVendorPaymentVoucher = findElementInXLSheet(getDataFinance, "payBookVendorPaymentVoucher");

		/* Fin_TC_016 */
		payBookLetterOFGurentee = findElementInXLSheet(getDataFinance, "payBookLetterOFGurentee");
		amountLOG = findElementInXLSheet(getDataFinance, "amountLOG");
		currencyLOG = findElementInXLSheet(getDataFinance, "currencyLOG");
		vebdorLetterOfGurentee = findElementInXLSheet(getDataFinance, "vebdorLetterOfGurentee");

		/* Fin_TC_017 */
		originalDocNoLetterOfGurentee = findElementInXLSheet(getDataFinance, "originalDocNoLetterOfGurentee");

		/* Fin_TC_019 */
		bankAccountBankAdjustment = findElementInXLSheet(getDataFinance, "bankAccountBankAdjustment");
		glAccountBankAdjustment = findElementInXLSheet(getDataFinance, "glAccountBankAdjustment");
		bankBankAdjustment = findElementInXLSheet(getDataFinance, "bankBankAdjustment");
		costAllocatioPresentageNankAdjustment = findElementInXLSheet(getDataFinance,
				"costAllocatioPresentageNankAdjustment");
		postBusinessUnitBankAdjustment = findElementInXLSheet(getDataFinance, "postBusinessUnitBankAdjustment");
		amountBankAdjustment = findElementInXLSheet(getDataFinance, "amountBankAdjustment");
		costCenterBankAdjustment = findElementInXLSheet(getDataFinance, "costCenterBankAdjustment");

		/* Fin_TC_020 */
		bankStatementREconcilationStatementBalance = findElementInXLSheet(getDataFinance,
				"bankStatementREconcilationStatementBalance");
		glAccountAdjustmetnBankReconcillation = findElementInXLSheet(getDataFinance,
				"glAccountAdjustmetnBankReconcillation");
		bankBankReconcillation = findElementInXLSheet(getDataFinance, "bankBankReconcillation");
		bankAccountBankReconcilation = findElementInXLSheet(getDataFinance, "bankAccountBankReconcilation");
		adjustmentDescriptionBankReconcilation = findElementInXLSheet(getDataFinance,
				"adjustmentDescriptionBankReconcilation");
		departmentCostAllocationBankReconcilationAdjustment = findElementInXLSheet(getDataFinance,
				"departmentCostAllocationBankReconcilationAdjustment");
		presentageCostAllocationBankReconcilationAdjustment = findElementInXLSheet(getDataFinance,
				"presentageCostAllocationBankReconcilationAdjustment");

		/* Fn_TC_021 */
		employeePettyCash = findElementInXLSheet(getDataFinance, "employeePettyCash");
		pettyCshType021 = findElementInXLSheet(getDataFinance, "pettyCshType021");
		amountPettyCash = findElementInXLSheet(getDataFinance, "amountPettyCash");
		costPresentagePettyCash = findElementInXLSheet(getDataFinance, "costPresentagePettyCash");
		costCenterPettyCsh = findElementInXLSheet(getDataFinance, "costCenterPettyCsh");
		descriptionPettyCashIOU = findElementInXLSheet(getDataFinance, "descriptionPettyCashIOU");

		/* Fin_TC_023 */
		narrationDescription = findElementInXLSheet(getDataFinance, "narrationDescription");

		/* Smoke_Finance_001 */
		bankBankAdjustmentSmoke = findElementInXLSheet(getDataFinance, "bankBankAdjustmentSmoke");
		accountBankAdjustmentSmoke = findElementInXLSheet(getDataFinance, "accountBankAdjustmentSmoke");
		glAccountBankAdjustmentSmoke = findElementInXLSheet(getDataFinance, "glAccountBankAdjustmentSmoke");
		amountBankAdjustmentSmoke = findElementInXLSheet(getDataFinance, "amountBankAdjustmentSmoke");

		/* Smoke_Finance_011 */
		account1JournelEntry = findElementInXLSheet(getDataFinance, "account1JournelEntry");
		account2JournelEntry = findElementInXLSheet(getDataFinance, "account2JournelEntry");
		account3JournelEntry = findElementInXLSheet(getDataFinance, "account3JournelEntry");
		value1JournelEntry = findElementInXLSheet(getDataFinance, "value1JournelEntry");
		value2JournelEntry = findElementInXLSheet(getDataFinance, "value2JournelEntry");
		value3JournelEntry = findElementInXLSheet(getDataFinance, "value3JournelEntry");
		descriptionJournelEntry = findElementInXLSheet(getDataFinance, "descriptionJournelEntry");

		/* Smoke_Finance_02 */
		originalDocNoBFA = findElementInXLSheet(getDataFinance, "originalDocNoBFA");
		bankNameBFA = findElementInXLSheet(getDataFinance, "bankNameBFA");
		accountNoBFA = findElementInXLSheet(getDataFinance, "accountNoBFA");
		currencyBFA = findElementInXLSheet(getDataFinance, "currencyBFA");
		amountBFA = findElementInXLSheet(getDataFinance, "amountBFA");
		bankFacilityGroup = findElementInXLSheet(getDataFinance, "bankFacilityGroup");
		acoounLoanConfiramationBFA = findElementInXLSheet(getDataFinance, "acoounLoanConfiramationBFA");

		/* Common customer account creation */
		accountGroup = findElementInXLSheet(getDataFinance, "accountGroup");

		/* Regression */
		/* FIN_BD_3_4_5_6 */
		payMethodCash = findElementInXLSheet(getDataFinance, "payMethodCash");
		sampathBank = findElementInXLSheet(getDataFinance, "sampathBank");
		sampathCurrentAccount = findElementInXLSheet(getDataFinance, "sampathCurrentAccount");
		payMethodCheque = findElementInXLSheet(getDataFinance, "payMethodCheque");

		/* FIN_BD_12_13_14 */
		postBusinessUnitSch = findElementInXLSheet(getDataFinance, "postBusinessUnitSch");
		customerAccount = findElementInXLSheet(getDataFinance, "customerAccount");
		currencyLKR = findElementInXLSheet(getDataFinance, "currencyLKR");
		amountThousand = findElementInXLSheet(getDataFinance, "amountThousand");
		amountThousandFiveHundred = findElementInXLSheet(getDataFinance, "amountThousandFiveHundred");

		/* FIN_BD_21 */
		oneHundred = findElementInXLSheet(getDataFinance, "oneHundred");
		oneHundredTwentyFive = findElementInXLSheet(getDataFinance, "oneHundredTwentyFive");
		oneHundredFifty = findElementInXLSheet(getDataFinance, "oneHundredFifty");
		currencyUSD = findElementInXLSheet(getDataFinance, "currencyUSD");
		billingAddressCustomerAccount = findElementInXLSheet(getDataFinance, "billingAddressCustomerAccount");
		glTradeDebtors = findElementInXLSheet(getDataFinance, "glTradeDebtors");
		glCurrencyGain = findElementInXLSheet(getDataFinance, "glCurrencyGain");

		/* FIN_BD_22 */
		amountTwoThousand = findElementInXLSheet(getDataFinance, "amountTwoThousand");
		cashAccount = findElementInXLSheet(getDataFinance, "cashAccount");

		/* FIN_BD_23 */
		glAccountVendorRefund = findElementInXLSheet(getDataFinance, "glAccountVendorRefund");
		vendorAccount = findElementInXLSheet(getDataFinance, "vendorAccount");
		fiftyThousand = findElementInXLSheet(getDataFinance, "fiftyThousand");

		/* FIN_BD_24 */
		glAccountMotorVehicles = findElementInXLSheet(getDataFinance, "glAccountMotorVehicles");
		eightThousand = findElementInXLSheet(getDataFinance, "eightThousand");

		/* FIN_BD_25 */
		employeeMadhushan = findElementInXLSheet(getDataFinance, "employeeMadhushan");
		twoThousandFiveHundred = findElementInXLSheet(getDataFinance, "twoThousandFiveHundred");
		glInvenstInShares = findElementInXLSheet(getDataFinance, "glInvenstInShares");

		/* FIN_BD_28 */
		fiveThousandEightHundred = findElementInXLSheet(getDataFinance, "fiveThousandEightHundred");
		bank = findElementInXLSheet(getDataFinance, "bank");

		/* FIN_BD_29 */
		twoThousandTwoHundred = findElementInXLSheet(getDataFinance, "twoThousandTwoHundred");

		/* FIN_BD_30 */
		twentyFiveThousand = findElementInXLSheet(getDataFinance, "twentyFiveThousand");

		/* FIN_BD_31 */
		tenThousand = findElementInXLSheet(getDataFinance, "tenThousand");

		/* FIN_BD_32_33_34 */
		glBank = findElementInXLSheet(getDataFinance, "glBank");
		glCustomerAdvance = findElementInXLSheet(getDataFinance, "glCustomerAdvance");
		glTradeCreditors = findElementInXLSheet(getDataFinance, "glTradeCreditors");

		/* FIN_BD_35_36_37 */
		commercialBank = findElementInXLSheet(getDataFinance, "commercialBank");
		commercialCurrentAccount = findElementInXLSheet(getDataFinance, "commercialCurrentAccount");
		glBank2 = findElementInXLSheet(getDataFinance, "glBank2");

		/* Bank Adjustment */
		/* FIN_BA_2 */
		moduleFinance = findElementInXLSheet(getDataFinance, "moduleFinance");
		logUserEmail = findElementInXLSheet(getDataFinance, "logUserEmail");

		/* FIN_BA_2 */
		myTemplateOption = findElementInXLSheet(getDataFinance, "myTemplateOption");
		allOption = findElementInXLSheet(getDataFinance, "allOption");

		/* FIN_BA_10 */
		bankFacilityOriginalDocNumber = findElementInXLSheet(getDataFinance, "bankFacilityOriginalDocNumber");
		setlementTypeBankFacilityOnBankAdjustment = findElementInXLSheet(getDataFinance,
				"setlementTypeBankFacilityOnBankAdjustment");

		/* FIN_BA_11 */
		glAccountMotorVehiclesDuplicatedBankAdjustment = findElementInXLSheet(getDataFinance,
				"glAccountMotorVehiclesDuplicatedBankAdjustment");

		/* FIN_BA_14 */
		statusActive = findElementInXLSheet(getDataFinance, "statusActive");
		statusInactive = findElementInXLSheet(getDataFinance, "statusInactive");
		glLand = findElementInXLSheet(getDataFinance, "glLand");

		/* FIN_BA_18_19 */
		setlementTypeCapital = findElementInXLSheet(getDataFinance, "setlementTypeCapital");
		minusTenThousand = findElementInXLSheet(getDataFinance, "minusTenThousand");

		/* FIN_BA_24 */
		setlementTypeInterest = findElementInXLSheet(getDataFinance, "setlementTypeInterest");
		minusTwoThousand = findElementInXLSheet(getDataFinance, "minusTwoThousand");

		/* FIN_BA_26 */
		setlementTypePanaltyInterest = findElementInXLSheet(getDataFinance, "setlementTypePanaltyInterest");

		/* FIN_BA_28 */
		setlementTypeLateFee = findElementInXLSheet(getDataFinance, "setlementTypeLateFee");

		/* FIN_BA_31_32 */
		taxGroup = findElementInXLSheet(getDataFinance, "taxGroup");
		glVATRecievable = findElementInXLSheet(getDataFinance, "glVATRecievable");

		/* FIN_BA_33 */
		sampathCurrentAccountUSD = findElementInXLSheet(getDataFinance, "sampathCurrentAccountUSD");

		/* FIN_BA_36 */
		sampathBankAccountWithLeaseFacilityAllow = findElementInXLSheet(getDataFinance,
				"sampathBankAccountWithLeaseFacilityAllow");

		/* Cash Bank Book */
		/* FIN_CBB_1_4_5_6 */
		cashBankBookTypeCash = findElementInXLSheet(getDataFinance, "cashBankBookTypeCash");
		permissionGroupCBB = findElementInXLSheet(getDataFinance, "permissionGroupCBB");

		/* FIN_CBB_7 */
		cashBankBookTypeBank = findElementInXLSheet(getDataFinance, "cashBankBookTypeBank");
		bankBranchNegombo = findElementInXLSheet(getDataFinance, "bankBranchNegombo");
		accountTypeCBB = findElementInXLSheet(getDataFinance, "accountTypeCBB");
		fax = findElementInXLSheet(getDataFinance, "fax");
		email = findElementInXLSheet(getDataFinance, "email");
		webUrl = findElementInXLSheet(getDataFinance, "webUrl");
		address = findElementInXLSheet(getDataFinance, "address");
		companyStatement = findElementInXLSheet(getDataFinance, "companyStatement");
		telephone = findElementInXLSheet(getDataFinance, "telephone");
		amountFiftThousand = findElementInXLSheet(getDataFinance, "amountFiftThousand");
		drawer = findElementInXLSheet(getDataFinance, "drawer");
		swiftCode = findElementInXLSheet(getDataFinance, "swiftCode");

		/* FIN_CBB_18 */
		statusInactiveFrontPage = findElementInXLSheet(getDataFinance, "statusInactiveFrontPage");

		/* FIN_CBB_20 */
		bookTypePettyCash = findElementInXLSheet(getDataFinance, "bookTypePettyCash");
		employee = findElementInXLSheet(getDataFinance, "employee");
		pettyCashTypeIOU = findElementInXLSheet(getDataFinance, "pettyCashTypeIOU");

		/* Bank Facility Agreement */
		/* FIN_BFA_1_5_7_8_9_10_11_14_22_26 */
		bankFacilityGroupImportLoan = findElementInXLSheet(getDataFinance, "bankFacilityGroupImportLoan");
		amountRegressionBFA = findElementInXLSheet(getDataFinance, "amountRegressionBFA");
		glSampathBank = findElementInXLSheet(getDataFinance, "glSampathBank");
		glCashAdvance = findElementInXLSheet(getDataFinance, "glCashAdvance");

		/* FIN_BFA_12 */
		alpabaticSampleLetters = findElementInXLSheet(getDataFinance, "alpabaticSampleLetters");
		specialSampleCharacters = findElementInXLSheet(getDataFinance, "specialSampleCharacters");

		/* FIN_BFA_13 */
		minusAmountBFA = findElementInXLSheet(getDataFinance, "minusAmountBFA");

		/* FIN_BFA_15 */
		changedBankAccountBFA = findElementInXLSheet(getDataFinance, "changedBankAccountBFA");

		/* FIN_BFA_24_25 */
		changeReasonHoldUnholdActionVerification = findElementInXLSheet(getDataFinance,
				"changeReasonHoldUnholdActionVerification");

		/* FIN_BFA_27 */
		payBookLOG = findElementInXLSheet(getDataFinance, "payBookLOG");
		letterOfGuranteeAmount_FIN_BFA_27 = findElementInXLSheet(getDataFinance, "letterOfGuranteeAmount_FIN_BFA_27");
		requestTypeVendorLOG = findElementInXLSheet(getDataFinance, "requestTypeVendorLOG");
		guranteeGroupLC = findElementInXLSheet(getDataFinance, "guranteeGroupLC");

		/* FIN_BFA_34_35_36 */
		facilityAgreementGroupLease = findElementInXLSheet(getDataFinance, "facilityAgreementGroupLease");
		leaseFacilityAgreementAmount = findElementInXLSheet(getDataFinance, "leaseFacilityAgreementAmount");
		leaseFacilityAgreementInterest = findElementInXLSheet(getDataFinance, "leaseFacilityAgreementInterest");
		glInterestInSuspence = findElementInXLSheet(getDataFinance, "glInterestInSuspence");
		gl_cashAtBank = findElementInXLSheet(getDataFinance, "gl_cashAtBank");
		fixedAssertLFA = findElementInXLSheet(getDataFinance, "fixedAssertLFA");
		equelentInstallmentLeaseFacility = findElementInXLSheet(getDataFinance, "equelentInstallmentLeaseFacility");
		equelentInterestLeaseFacility = findElementInXLSheet(getDataFinance, "equelentInterestLeaseFacility");

		/* Petty Cash */
		/* FIN_PC_03 */
		pettyCashAccountRegression = findElementInXLSheet(getDataFinance, "pettyCashAccountRegression");

		/* FIN_PC_06 */
		employeePaidToAfterSelectedPC = findElementInXLSheet(getDataFinance, "employeePaidToAfterSelectedPC");
		employeePaidToPC = findElementInXLSheet(getDataFinance, "employeePaidToPC");

		/* FIN_PC_07 */
		pettyCashTypeOneOff = findElementInXLSheet(getDataFinance, "pettyCashTypeOneOff");
		pettyCashTypeSettlement = findElementInXLSheet(getDataFinance, "pettyCashTypeSettlement");

		/* FIN_PC_08 */
		testAmountWithLettersAndCharcters = findElementInXLSheet(getDataFinance, "testAmountWithLettersAndCharcters");

		/* FIN_PC_09 */
		pettyCashAccountRegression2 = findElementInXLSheet(getDataFinance, "pettyCashAccountRegression2");
		pettyCashAmountMinus = findElementInXLSheet(getDataFinance, "pettyCashAmountMinus");
		pettyCashAmountPlus = findElementInXLSheet(getDataFinance, "pettyCashAmountPlus");

		/* FIN_PC_12 */
		glAccountPC = findElementInXLSheet(getDataFinance, "glAccountPC");

		/* FIN_PC_13 */
		narrationPC = findElementInXLSheet(getDataFinance, "narrationPC");

		/* FIN_PC_14 */
		updatedAmountPC = findElementInXLSheet(getDataFinance, "updatedAmountPC");

		/* FIN_PC_15 */
		updatedAmountPC2 = findElementInXLSheet(getDataFinance, "updatedAmountPC2");

		/* FIN_PC_18 */
		descPettyCash = findElementInXLSheet(getDataFinance, "descPettyCash");

		/* FIN_PC_22 */
		glCashInHandPC = findElementInXLSheet(getDataFinance, "glCashInHandPC");

		/* FIN_PC_23 */
		glLandPC = findElementInXLSheet(getDataFinance, "glLandPC");
		glCashAdvancePC = findElementInXLSheet(getDataFinance, "glCashAdvancePC");

		/* FIN_PC_25 */
		pettyCashAccountFloatVerify = findElementInXLSheet(getDataFinance, "pettyCashAccountFloatVerify");

		/* FIN_PC_26 */
		pettyCashAccountReverseValidation = findElementInXLSheet(getDataFinance, "pettyCashAccountReverseValidation");

		/* FIN_PC_30 */
		cashAccountFloatValidation = findElementInXLSheet(getDataFinance, "cashAccountFloatValidation");
		amountFundTransferFloatValidate = findElementInXLSheet(getDataFinance, "amountFundTransferFloatValidate");

		/* FIN_PC_31 */
		amountRecieveAccountFundTransfer = findElementInXLSheet(getDataFinance, "amountRecieveAccountFundTransfer");
		fundTransferAmount = findElementInXLSheet(getDataFinance, "fundTransferAmount");
		payBookCashFT = findElementInXLSheet(getDataFinance, "payBookCashFT");
		autoSelectdPaymentMethodOP = findElementInXLSheet(getDataFinance, "autoSelectdPaymentMethodOP");

		/* FIN_PC_32 */
		payBookBankFT = findElementInXLSheet(getDataFinance, "payBookBankFT");
		autoSelectdPaymentMethodChequeOP = findElementInXLSheet(getDataFinance, "autoSelectdPaymentMethodChequeOP");

		/* FIN_PC_33 */
		payBookEFTFundTransfer = findElementInXLSheet(getDataFinance, "payBookEFTFundTransfer");
		autoSelectdPaymentMethodEFTOP = findElementInXLSheet(getDataFinance, "autoSelectdPaymentMethodEFTOP");

		/************ Outbound Payment Advice *************/
		/* FIN_OPA_04 */
		selectedVendorOPA = findElementInXLSheet(getDataFinance, "selectedVendorOPA");
		autoSelectedContactPersonOPA = findElementInXLSheet(getDataFinance, "autoSelectedContactPersonOPA");
		autoSelectedAddressOPA = findElementInXLSheet(getDataFinance, "autoSelectedAddressOPA");

		/* FIN_OPA_05 */
		testPhraseVendorReferenceNo = findElementInXLSheet(getDataFinance, "testPhraseVendorReferenceNo");
		testPhraseOPADescription = findElementInXLSheet(getDataFinance, "testPhraseOPADescription");

		/* FIN_OPA_06 */
		currencyRegressionOPA = findElementInXLSheet(getDataFinance, "currencyRegressionOPA");
		totalLableCurrencyVA_OPA = findElementInXLSheet(getDataFinance, "totalLableCurrencyVA_OPA");

		/* FIN_OPA_07 */
		testAmountWithLettersAndCharctersVA_OPA = findElementInXLSheet(getDataFinance,
				"testAmountWithLettersAndCharctersVA_OPA");

		/* FIN_OPA_08 */
		negativeAmountVA_OPA = findElementInXLSheet(getDataFinance, "negativeAmountVA_OPA");
		positiveAmountVA_OPA = findElementInXLSheet(getDataFinance, "positiveAmountVA_OPA");

		/* FIN_OPA_09 */
		taxGroupVA_OPA = findElementInXLSheet(getDataFinance, "taxGroupVA_OPA");

		/* FIN_OPA_11 */
		paidCurrencyOP_VPA = findElementInXLSheet(getDataFinance, "paidCurrencyOP_VPA");
		payBookOP_VPA = findElementInXLSheet(getDataFinance, "payBookOP_VPA");

		/* FIN_OPA_12_13 */
		amountAdvanceAndCreditMemo = findElementInXLSheet(getDataFinance, "amountAdvanceAndCreditMemo");
		glAccount_OPA_VCM = findElementInXLSheet(getDataFinance, "glAccount_OPA_VCM");
		setOffAmount_OPA_VCM = findElementInXLSheet(getDataFinance, "setOffAmount_OPA_VCM");
		selectedVendor_OPA_VCM = findElementInXLSheet(getDataFinance, "selectedVendor_OPA_VCM");
		curency_OPA_VCM = findElementInXLSheet(getDataFinance, "curency_OPA_VCM");

		/* FIN_OPA_14 */
		autoSelectedContactPerson_OPA_VCM = findElementInXLSheet(getDataFinance, "autoSelectedContactPerson_OPA_VCM");
		autoSelectedAddress_OPA_VCM = findElementInXLSheet(getDataFinance, "autoSelectedAddress_OPA_VCM");

		/* FIN_OPA_17 */
		testAmountWithLettersAndCharcters_OPA_VCM = findElementInXLSheet(getDataFinance,
				"testAmountWithLettersAndCharcters_OPA_VCM");

		/* FIN_OPA_18 */
		negativeAmount_OPA_VCM = findElementInXLSheet(getDataFinance, "negativeAmount_OPA_VCM");
		positiveAmount_OPA_VCM = findElementInXLSheet(getDataFinance, "positiveAmount_OPA_VCM");

		/* FIN_OPA_19_B */
		paidCurrency_OP_VCM = findElementInXLSheet(getDataFinance, "paidCurrency_OP_VCM");
		payBook_OP_VCM = findElementInXLSheet(getDataFinance, "payBook_OP_VCM");

		/* FIN_OPA_21 */
		vendorCrediMemoAmount = findElementInXLSheet(getDataFinance, "vendorCrediMemoAmount");
		setOffAmount = findElementInXLSheet(getDataFinance, "setOffAmount");

		/* FIN_OPA_23 */
		partialPayment_VCM_OP = findElementInXLSheet(getDataFinance, "partialPayment_VCM_OP");

		/* FIN_OPA_24 */
		inactiveGLAccountOPA = findElementInXLSheet(getDataFinance, "inactiveGLAccountOPA");

		/* FIN_OPA_25 */
		amountAV = findElementInXLSheet(getDataFinance, "amountAV");
		glAccount_OPA_AV = findElementInXLSheet(getDataFinance, "glAccount_OPA_AV");
		curency_OPA_AV = findElementInXLSheet(getDataFinance, "curency_OPA_AV");
		setOffAmount_OPA_AV = findElementInXLSheet(getDataFinance, "setOffAmount_OPA_AV");
		selectedVendor_OPA_AV = findElementInXLSheet(getDataFinance, "selectedVendor_OPA_AV");
		autoSelectedContactPerson_OPA_AV = findElementInXLSheet(getDataFinance, "autoSelectedContactPerson_OPA_AV");
		autoSelectedAddress_OPA_AV = findElementInXLSheet(getDataFinance, "autoSelectedAddress_OPA_AV");

		/* FIN_OPA_28 */
		testAmountWithLettersAndCharcters_OPA_AV = findElementInXLSheet(getDataFinance,
				"testAmountWithLettersAndCharcters_OPA_AV");

		/* FIN_OPA_30_B */
		paidCurrencyOP_AVP = findElementInXLSheet(getDataFinance, "paidCurrencyOP_AVP");
		payBookOP_AVP = findElementInXLSheet(getDataFinance, "payBookOP_AVP");

		/* FIN_OPA_32 */
		amountTwoHundred = findElementInXLSheet(getDataFinance, "amountTwoHundred");
		amountOneHundredFifty = findElementInXLSheet(getDataFinance, "amountOneHundredFifty");

		/* FIN_OPA_34 */
		setOffPartialAmountAV = findElementInXLSheet(getDataFinance, "setOffPartialAmountAV");

		/* FIN_OPA_36 */
		receiverCashBankBook_OPA_FT = findElementInXLSheet(getDataFinance, "receiverCashBankBook_OPA_FT");

		/* FIN_OPA_37 */
		description_OPA_FT = findElementInXLSheet(getDataFinance, "description_OPA_FT");

		/* FIN_OPA_38 */
		testAmountWithLettersAndCharcters_OPA_FT = findElementInXLSheet(getDataFinance,
				"testAmountWithLettersAndCharcters_OPA_FT");

		/* FIN_OPA_39 */
		amountFT = findElementInXLSheet(getDataFinance, "amountFT");

		/* FIN_OPA_41 */
		paidCurrencyOP_FT = findElementInXLSheet(getDataFinance, "paidCurrencyOP_FT");
		payBookOP_FT = findElementInXLSheet(getDataFinance, "payBookOP_FT");

		/* FIN_OPA_42 */
		contactPersona_OPA_CR = findElementInXLSheet(getDataFinance, "contactPersona_OPA_CR");
		billingAddress_OPA_CR = findElementInXLSheet(getDataFinance, "billingAddress_OPA_CR");

		/* FIN_OPA_43 */
		testPhraseCustomerReferenceNo = findElementInXLSheet(getDataFinance, "testPhraseCustomerReferenceNo");

		/* FIN_OPA_44 */
		currency_OPA_CR = findElementInXLSheet(getDataFinance, "currency_OPA_CR");
		totalLableCurrency_OPA_CR = findElementInXLSheet(getDataFinance, "totalLableCurrency_OPA_CR");

		/* FIN_OPA_45 */
		taxGroup_OPA_CR = findElementInXLSheet(getDataFinance, "taxGroup_OPA_CR");

		/* FIN_OPA_46 */
		glAccount_OPA_CR = findElementInXLSheet(getDataFinance, "glAccount_OPA_CR");

		/* FIN_OPA_47_48 */
		testAmountWithLettersAndCharcters_OPA_CR = findElementInXLSheet(getDataFinance,
				"testAmountWithLettersAndCharcters_OPA_CR");
		amount_OPA_CR = findElementInXLSheet(getDataFinance, "amount_OPA_CR");

		/* FIN_OPA_49 */
		customerFrontPage_OPA_CR = findElementInXLSheet(getDataFinance, "customerFrontPage_OPA_CR");

		/* FIN_OPA_50 */
		paidCurrencyOP_CR = findElementInXLSheet(getDataFinance, "paidCurrencyOP_CR");
		payBookOP_CR = findElementInXLSheet(getDataFinance, "payBookOP_CR");

		/* FIN_OPA_51 */
		setOffAmount_OPA_CR = findElementInXLSheet(getDataFinance, "setOffAmount_OPA_CR");

		/* FIN_OPA_52 */
		setOffAmountThreeHundred = findElementInXLSheet(getDataFinance, "setOffAmountThreeHundred");
		adviceAmoutnThreeHundred = findElementInXLSheet(getDataFinance, "adviceAmoutnThreeHundred");

		/* FIN_OPA_54 */
		partialSetOffAmountHundred = findElementInXLSheet(getDataFinance, "partialSetOffAmountHundred");

		/* FIN_OPA_55 */
		description_OPA_PV = findElementInXLSheet(getDataFinance, "description_OPA_PV");

		/* FIN_OPA_56 */
		currency_OPA_PV = findElementInXLSheet(getDataFinance, "currency_OPA_PV");

		/* FIN_OPA_57 */
		taxGroup_OPA_PV = findElementInXLSheet(getDataFinance, "taxGroup_OPA_PV");

		/* FIN_OPA_58 */
		glAccount_OPA_PV = findElementInXLSheet(getDataFinance, "glAccount_OPA_PV");

		/* FIN_OPA_59_60 */
		testAmountWithLettersAndCharcters_OPA_PV = findElementInXLSheet(getDataFinance,
				"testAmountWithLettersAndCharcters_OPA_PV");
		negativeAmount_OPA_PV = findElementInXLSheet(getDataFinance, "negativeAmount_OPA_PV");
		amount_OPA_PV = findElementInXLSheet(getDataFinance, "amount_OPA_PV");

		/* FIN_OPA_62 */
		paidCurrencyOP_PV = findElementInXLSheet(getDataFinance, "paidCurrencyOP_PV");
		payBookOP_PV = findElementInXLSheet(getDataFinance, "payBookOP_PV");
		payee_OP_PV = findElementInXLSheet(getDataFinance, "payee_OP_PV");

		/* FIN_OPA_64_A */
		employee_OPA_EPA = findElementInXLSheet(getDataFinance, "employee_OPA_EPA");
		employeeFrontPage_OPA_EPA = findElementInXLSheet(getDataFinance, "employeeFrontPage_OPA_EPA");

		/* FIN_OPA_65_A */
		testPhraseEmployeeReferenceNo = findElementInXLSheet(getDataFinance, "testPhraseEmployeeReferenceNo");
		testPhraseDescription_OPA_EPA = findElementInXLSheet(getDataFinance, "testPhraseDescription_OPA_EPA");

		/* FIN_OPA_66_A */
		glAccount_OPA_EPA = findElementInXLSheet(getDataFinance, "glAccount_OPA_EPA");

		/* FIN_OPA_67_A_68_A */
		testAmountWithLettersAndCharcters_OPA_EPA = findElementInXLSheet(getDataFinance,
				"testAmountWithLettersAndCharcters_OPA_EPA");
		negativeAmount_OPA_EPA = findElementInXLSheet(getDataFinance, "negativeAmount_OPA_EPA");
		amount_OPA_EPA = findElementInXLSheet(getDataFinance, "amount_OPA_EPA");

		/* FIN_OPA_69_A */
		descriptionGrid_OPA_EPA = findElementInXLSheet(getDataFinance, "descriptionGrid_OPA_EPA");

		/* FIN_OPA_71_A_72 */
		paidCurrencyOP_EP = findElementInXLSheet(getDataFinance, "paidCurrencyOP_EP");
		payBookOP_EP = findElementInXLSheet(getDataFinance, "payBookOP_EP");

		/* FIN_OPA_75_A */
		updatedAmount_OPA_EPA = findElementInXLSheet(getDataFinance, "updatedAmount_OPA_EPA");

		/* FIN_OPA_76_A */
		currency_OPA_EPA = findElementInXLSheet(getDataFinance, "currency_OPA_EPA");

		/* Employee Advance Advice */
		/* FIN_OPA_64_B */
		employee_OPA_EAA = findElementInXLSheet(getDataFinance, "employee_OPA_EAA");
		employeeFrontPage_OPA_EAA = findElementInXLSheet(getDataFinance, "employeeFrontPage_OPA_EAA");

		/* FIN_OPA_65_B */
		testPhraseDescription_OPA_EAA = findElementInXLSheet(getDataFinance, "testPhraseDescription_OPA_EAA");

		/* FIN_OPA_67_B_68_B */
		testAmountWithLettersAndCharcters_OPA_EAA = findElementInXLSheet(getDataFinance,
				"testAmountWithLettersAndCharcters_OPA_EAA");
		negativeAmount_OPA_EAA = findElementInXLSheet(getDataFinance, "negativeAmount_OPA_EAA");
		amount_OPA_EAA = findElementInXLSheet(getDataFinance, "amount_OPA_EAA");

		/* FIN_OPA_71_B */
		paidCurrencyOP_EA = findElementInXLSheet(getDataFinance, "paidCurrencyOP_EA");
		payBookOP_EA = findElementInXLSheet(getDataFinance, "payBookOP_EA");

		/* FIN_OPA_74_B */
		updatedAmount_OPA_EAA = findElementInXLSheet(getDataFinance, "updatedAmount_OPA_EAA");

		/* FIN_OPA_77_B */
		currency_OPA_EAA = findElementInXLSheet(getDataFinance, "currency_OPA_EAA");

		/* Customer Payment Voucher */
		/* FIN_OP_03 */
		customerFrontPage_OP_CPV = findElementInXLSheet(getDataFinance, "customerFrontPage_OP_CPV");
		contactPersona_OP_CPV = findElementInXLSheet(getDataFinance, "contactPersona_OP_CPV");
		billingAddress_OP_CPV = findElementInXLSheet(getDataFinance, "billingAddress_OP_CPV");

		/* FIN_OP_04 */
		currency_OP_CPV = findElementInXLSheet(getDataFinance, "currency_OP_CPV");

		/* FIN_OP_05 */
		filterCurrency_OP_CPV = findElementInXLSheet(getDataFinance, "filterCurrency_OP_CPV");

		/* FIN_OP_06 */
		paidCurrency_OP_CPV = findElementInXLSheet(getDataFinance, "paidCurrency_OP_CPV");
		payBook_OP_CPV = findElementInXLSheet(getDataFinance, "payBook_OP_CPV");
		loadedPaymentMethod_OP_CPV = findElementInXLSheet(getDataFinance, "loadedPaymentMethod_OP_CPV");
		currencyLKR_OPA_CR = findElementInXLSheet(getDataFinance, "currencyLKR_OPA_CR");
		customerFrontPage_OP_CR = findElementInXLSheet(getDataFinance, "customerFrontPage_OP_CR");

		/* FIN_OP_07 */
		payBookCheque_OP_CPV = findElementInXLSheet(getDataFinance, "payBookCheque_OP_CPV");
		loadedPaymentMethodCheque_OP_CPV = findElementInXLSheet(getDataFinance, "loadedPaymentMethodCheque_OP_CPV");
		payee_OP_CPV = findElementInXLSheet(getDataFinance, "payee_OP_CPV");

		/* FIN_OP_10 */
		payBookEFT_OP_CPV = findElementInXLSheet(getDataFinance, "payBookEFT_OP_CPV");
		loadedPaymentMethodEFT_OP_CPV = findElementInXLSheet(getDataFinance, "loadedPaymentMethodEFT_OP_CPV");

		/* FIN_OP_12 */
		paidCurrencyUSD_OP_CPV = findElementInXLSheet(getDataFinance, "paidCurrencyUSD_OP_CPV");
		filterCurrencyUSD_OP_CPV = findElementInXLSheet(getDataFinance, "filterCurrencyUSD_OP_CPV");
		amountUSD_OPA_CR = findElementInXLSheet(getDataFinance, "amountUSD_OPA_CR");
		currencyUSD_OPA_CR = findElementInXLSheet(getDataFinance, "currencyUSD_OPA_CR");
		payBookUSDCheque_OP_CPV = findElementInXLSheet(getDataFinance, "payBookUSDCheque_OP_CPV");

		/* FIN_OP_14 */
		customerFrontPage_2_OP_CR = findElementInXLSheet(getDataFinance, "customerFrontPage_2_OP_CR");
		contactPersona_2_OPA_CR = findElementInXLSheet(getDataFinance, "contactPersona_2_OPA_CR");
		billingAddress_2_OPA_CR = findElementInXLSheet(getDataFinance, "billingAddress_2_OPA_CR");
		paidAmountSetOff_OPA_CPV = findElementInXLSheet(getDataFinance, "paidAmountSetOff_OPA_CPV");

		/* FIN_OP_15 */
		paidAmount_OPA_CPV = findElementInXLSheet(getDataFinance, "paidAmount_OPA_CPV");

		/* Vendor Payment Voucher */
		/* FIN_OP_16 */
		curencyLKR_OPA_VCM = findElementInXLSheet(getDataFinance, "curencyLKR_OPA_VCM");
		selectedVendor_OP_VPV = findElementInXLSheet(getDataFinance, "selectedVendor_OP_VPV");
		autoSelectedContactPerson_OP_VCV = findElementInXLSheet(getDataFinance, "autoSelectedContactPerson_OP_VCV");
		autoSelectedAddress_OP_VPV = findElementInXLSheet(getDataFinance, "autoSelectedAddress_OP_VPV");

		/* FIN_OP_17 */
		payBook_OP_VPV = findElementInXLSheet(getDataFinance, "payBook_OP_VPV");
		payMethodCash_OP_VPV = findElementInXLSheet(getDataFinance, "payMethodCash_OP_VPV");
		cashAccount_OP_VPV = findElementInXLSheet(getDataFinance, "cashAccount_OP_VPV");
		
		/* FIN_OP_18 */
		baseCurrency_OP_VPV = findElementInXLSheet(getDataFinance, "baseCurrency_OP_VPV");
		currencyUSD_OP_VPV = findElementInXLSheet(getDataFinance, "currencyUSD_OP_VPV");
		
		/* FIN_OP_20 */
		bankBook_OP_VPV = findElementInXLSheet(getDataFinance, "bankBook_OP_VPV");
		payMethodCheque_OP_VPV = findElementInXLSheet(getDataFinance, "payMethodCheque_OP_VPV");
		payee_OP_VPV = findElementInXLSheet(getDataFinance, "payee_OP_VPV");
		
		/* FIN_OP_23 */
		payBookEFT_OP_VPV = findElementInXLSheet(getDataFinance, "payBookEFT_OP_VPV");
		loadedPaymentMethodEFT_OP_VPV = findElementInXLSheet(getDataFinance, "loadedPaymentMethodEFT_OP_VPV");
		
		/* FIN_OP_25 */
		curencyUSD_OPA_VCM = findElementInXLSheet(getDataFinance, "curencyUSD_OPA_VCM");
		paidCurrencyUSD_OP_VPV = findElementInXLSheet(getDataFinance, "paidCurrencyUSD_OP_VPV");
		filterCurrencyUSD_OP_VPV = findElementInXLSheet(getDataFinance, "filterCurrencyUSD_OP_VPV");
		amount_OPA_VCM = findElementInXLSheet(getDataFinance, "amount_OPA_VCM");
		payBookUSD_OP_VPV = findElementInXLSheet(getDataFinance, "payBookUSD_OP_VPV");
		
		/* FIN_OP_26 */
		paidCurrencyLKR_OP_VPV = findElementInXLSheet(getDataFinance, "paidCurrencyLKR_OP_VPV");
		
		/* FIN_OP_28 */
		paidAmount_OP_VPV = findElementInXLSheet(getDataFinance, "paidAmount_OP_VPV");
		
		/* Payment Voucher */
		/* FIN_OP_29 */
		currencyLKR_OPA_PV = findElementInXLSheet(getDataFinance, "currencyLKR_OPA_PV");
		paidCurrency_OP_PV = findElementInXLSheet(getDataFinance, "paidCurrency_OP_PV");
		payBook_OP_PV = findElementInXLSheet(getDataFinance, "payBook_OP_PV");
		payMethodCash_OP_PV = findElementInXLSheet(getDataFinance, "payMethodCash_OP_PV");
		cashAccount_OP_PV = findElementInXLSheet(getDataFinance, "cashAccount_OP_PV");
		amount2_OP_PV = findElementInXLSheet(getDataFinance, "amount2_OP_PV");
		filterCurrency_OP_PV = findElementInXLSheet(getDataFinance, "filterCurrency_OP_PV");
		
		/* FIN_OP_30 */
		bankBook_OP_PV = findElementInXLSheet(getDataFinance, "bankBook_OP_PV");
		payMethodCheque_OP_PV = findElementInXLSheet(getDataFinance, "payMethodCheque_OP_PV");
		
		/* FIN_OP_32 */
		payBookEFT_OP_PV = findElementInXLSheet(getDataFinance, "payBookEFT_OP_PV");
		loadedPaymentMethodEFT_OP_PV = findElementInXLSheet(getDataFinance, "loadedPaymentMethodEFT_OP_PV");

		/* FIN_OP_34 */
		paidCurrencyUSD_OP_PV = findElementInXLSheet(getDataFinance, "paidCurrencyUSD_OP_PV");
		payBookUSD_OP_PV = findElementInXLSheet(getDataFinance, "payBookUSD_OP_PV");
		amount3_OPA_PV = findElementInXLSheet(getDataFinance, "amount3_OPA_PV");
		
		/* FIN_OP_35 */
		filterCurrencyUSD_OP_PV = findElementInXLSheet(getDataFinance, "filterCurrencyUSD_OP_PV");
	
		/* Fund Transfer Voucher */
		/* FIN_OP_36 */
		reciverCashBook_OP_FTV = findElementInXLSheet(getDataFinance, "reciverCashBook_OP_FTV");

		/* FIN_OP_37 */
		reciverCashBook_2_OP_FTV = findElementInXLSheet(getDataFinance, "reciverCashBook_2_OP_FTV");
		payBook_OP_FTV = findElementInXLSheet(getDataFinance, "payBook_OP_FTV");

		/* FIN_OP_38 */
		pettyCashBook_OP_FTV = findElementInXLSheet(getDataFinance, "pettyCashBook_OP_FTV");
		amount_OP_FTV = findElementInXLSheet(getDataFinance, "amount_OP_FTV");

		/* FIN_OP_40 */
		floatAmount_OP_FTV = findElementInXLSheet(getDataFinance, "floatAmount_OP_FTV");
		baseFundTransferAmount_OP_FTV = findElementInXLSheet(getDataFinance, "baseFundTransferAmount_OP_FTV");
		pettyCashAmount_OP_FTV = findElementInXLSheet(getDataFinance, "pettyCashAmount_OP_FTV");
		reinbursementAmount_OP_FTV = findElementInXLSheet(getDataFinance, "reinbursementAmount_OP_FTV");

		/* Employee Payment Voucher */
		/* FIN_OP_41_42 */
		employee_OP_EPV = findElementInXLSheet(getDataFinance, "employee_OP_EPV");
		employeeFrontPage_OP_EPV = findElementInXLSheet(getDataFinance, "employeeFrontPage_OP_EPV");
		paidCurrency_OP_EPV = findElementInXLSheet(getDataFinance, "paidCurrency_OP_EPV");
		payBook_OP_EPV = findElementInXLSheet(getDataFinance, "payBook_OP_EPV");
		loadedPaymentMethod_OP_EPV = findElementInXLSheet(getDataFinance, "loadedPaymentMethod_OP_EPV");

		/* FIN_OP_43 */
		payBookCheque_OP_EPV = findElementInXLSheet(getDataFinance, "payBookCheque_OP_EPV");
		loadedPaymentMethodCheque_OP_EPV = findElementInXLSheet(getDataFinance, "loadedPaymentMethodCheque_OP_EPV");
		payee_OP_EPV = findElementInXLSheet(getDataFinance, "payee_OP_EPV");

		/* FIN_OP_45 */
		payBookEFT_OP_EPV = findElementInXLSheet(getDataFinance, "payBookEFT_OP_EPV");
		loadedPaymentMethodEFT_OP_EPV = findElementInXLSheet(getDataFinance, "loadedPaymentMethodEFT_OP_EPV");

		/* FIN_OP_46_47 */
		updatedPayBook_OP_EPV = findElementInXLSheet(getDataFinance, "updatedPayBook_OP_EPV");

		// Aruna-JournalEntry
		siteUrlA = findElementInXLSheet(getDataFinance, "siteUrlA");
		userNameDataReg = findElementInXLSheet(getDataFinance, "userNameDataReg");
		passwordDataReg = findElementInXLSheet(getDataFinance, "passwordDataReg");
		NewUserNameA = findElementInXLSheet(getDataFinance, "NewUserNameA");
		NewPasswordA = findElementInXLSheet(getDataFinance, "NewPasswordA");

		// FIN_18_1to18_11
		GLAccTxtA = findElementInXLSheet(getDataFinance, "GLAccTxtA");

		// FIN_18_20
		GLAccTxtInactiveA = findElementInXLSheet(getDataFinance, "GLAccTxtInactiveA");

		// FIN_11_5to11_9
		CustomerLookUpTxtA = findElementInXLSheet(getDataFinance, "CustomerLookUpTxtA");
		PaymentMethodSelA = findElementInXLSheet(getDataFinance, "PaymentMethodSelA");
		PaidCurrencySelA = findElementInXLSheet(getDataFinance, "PaidCurrencySelA");

		// FIN_10_15to10_21
		CustomerLookUpTxt2A = findElementInXLSheet(getDataFinance, "CustomerLookUpTxt2A");

		// FIN_10_25to10_30
		VendorLookUpIPATxtA = findElementInXLSheet(getDataFinance, "VendorLookUpIPATxtA");

		// FIN_10_40to10_46
		EmployeeLookupIPATxtA = findElementInXLSheet(getDataFinance, "EmployeeLookupIPATxtA");

		// FIN_10_47to10_53
		CustomerLookUpTxt3A = findElementInXLSheet(getDataFinance, "CustomerLookUpTxt3A");

		// FIN_10_59to10_60
		CustomerLookUpTxt3NewA = findElementInXLSheet(getDataFinance, "CustomerLookUpTxt3NewA");

		// FIN_10_68
		CostCenterSelA = findElementInXLSheet(getDataFinance, "CostCenterSelA");

		// FIN_10_69
		AutoCompleteProductTxtA = findElementInXLSheet(getDataFinance, "AutoCompleteProductTxtA");
		WarehouseSISelA = findElementInXLSheet(getDataFinance, "WarehouseSISelA");
		CustomerLookUpTxt4NewA = findElementInXLSheet(getDataFinance, "CustomerLookUpTxt4NewA");

		// FIN_11_11
		PaymentMethodSel2A = findElementInXLSheet(getDataFinance, "PaymentMethodSel2A");
		BankNameIPSelA = findElementInXLSheet(getDataFinance, "BankNameIPSelA");

		// FIN_11_12and11_20
		BankBDSelA = findElementInXLSheet(getDataFinance, "BankBDSelA");
		BankAccountNoBDSelA = findElementInXLSheet(getDataFinance, "BankAccountNoBDSelA");

		// FIN_11_13
		PaymentMethodSel3A = findElementInXLSheet(getDataFinance, "PaymentMethodSel3A");

		// FIN_11_14
		PaymentMethodSel4A = findElementInXLSheet(getDataFinance, "PaymentMethodSel4A");

		// FIN_11_18
		PaidCurrencyForignA = findElementInXLSheet(getDataFinance, "PaidCurrencyForignA");

		// FIN_16_1to16_9
		BankAccountNoBDNewSelA = findElementInXLSheet(getDataFinance, "BankAccountNoBDNewSelA");
		cboxPaybook1A = findElementInXLSheet(getDataFinance, "cboxPaybook1A");
		cboxPaybook2A = findElementInXLSheet(getDataFinance, "cboxPaybook2A");

		// FIN_16_10
		BankAccountNoLeaseFacilitySelA = findElementInXLSheet(getDataFinance, "BankAccountNoLeaseFacilitySelA");

	}
}
