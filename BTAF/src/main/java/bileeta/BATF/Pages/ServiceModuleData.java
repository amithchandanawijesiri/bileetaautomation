package bileeta.BATF.Pages;

import bileeta.BTAF.Utilities.TestBase;

public class ServiceModuleData extends TestBase{
	
	
	/* Reading the element locators to variables */
	protected static String siteURL;
	protected static String siteLogo;
	protected static String username;
	protected static String password;
	protected static String btn_login;
	protected static String lnk_home;
	protected static String btn_navigationmenu;
    protected static String sidemenu;
    protected static String btn_servicemodule;
    protected static String subsidemenu;

    //TC001service
    protected static String btn_ServiceJobOrder;
    protected static String  ServiceJobOrderPage;
    protected static String btn_NewServiceJobOrder;
    protected static String StartNewJourneyPage;
    protected static String btn_ServiceOrdertoInternalDispatchOrderExternal;
    protected static String NewServiceJobOrderPage;
    protected static String  ServiceJobOrderTitle;
    protected static String ServiceJobOrderAccount;
    protected static String ServiceGroup;
    protected static String ServiceJobOrderDescription;
    protected static String Priority;
    protected static String template;
    protected static String ServiceLocation;
    protected static String btn_location;
    protected static String btn_PricingProfileSearch;
    protected static String PricingProfileLookUpPage;
    protected static String btn_PricingProfile;
    protected static String btn_accountsearch;
    protected static String  AccountLookUPPage;
    protected static String  Accountname;
    protected static String btn_accountuser;
    protected static String btn_searchservicelocation;
    protected static String btn_searchowner;
    protected static String OwnerLookUpPage;
    protected static String btn_owner;
    protected static String  btn_taskdetails;
    protected static String TaskDetailsTab;
    protected static String btn_searchserialproduct;
    protected static String ProductLookUpPage;
    protected static String Product;
    protected static String btn_product;
    protected static String btn_expand;
    protected static String  tasktype;
    protected static String div;
    protected static String EstimatedQty;
    protected static String btn_searchbatchproduct;
    protected static String btn_expand2;
    protected static String btn_expand3;
    protected static String btn_searchFIFOproduct;
    protected static String btn_expand4;
    protected static String btn_searchserviceproduct;
    protected static String  btn_expand5;
    protected static String btn_searchResources;
    protected static String  tasktyperesource;
    protected static String btn_searchlabour;
    protected static String tasktypelabour;
    protected static String btn_expand6;
    protected static String btn_searchsubcontract;
    protected static String tasktypesubcontract;
    protected static String btn_expand7;
    protected static String tasktypeexpense;
    protected static String   btn_searchexpense;
    protected static String btn_expand8;
    protected static String btn_DraftServiceJobOrder;
    protected static String btn_ReleaseServiceJobOrder;
    protected static String EstimatedQtyLabour;
    protected static String EstimatedQtyResourse;
    protected static String btn_searchlabouremployee;
    protected static String LabourOwnerLookUpPage;
    protected static String  LabourEmployee;
    protected static String txt_LabourEmployee;
    protected static String btn_searchResourcesReferenceCode;
    protected static String ResourceLookUpPage;
    protected static String Resource;
    protected static String btn_Resource;
    protected static String btn_LabourEmployee;
    protected static String DraftServiceJobOrderPage;
    protected static String ReleaseServiceJobOrderPage;
    protected static String EstimatedQtyserial;
    protected static String EstimatedBatch;
    protected static String EstimatedQtyFIFO;
    protected static String EstimatedQtyservice;
    protected static String EstimatedQtysubcontract;
    protected static String EstimatedQtyExpense;
    protected static String  btn_action;
    protected static String  btn_CreateInternalOrder;
    protected static String GernerateInternalOrderPage;
    protected static String btn_selectall;
    protected static String  btn_apply;
    protected static String  btn_applyCommon;
    protected static String  btn_joborder;
    protected static String  NewInternalOrderPage;
    protected static String btn_draftInternalNewOrder;
    protected static String btn_releaseInternalNewOrder;
    protected static String InternalOrderInformationPopUp;
    protected static String btn_gotopage;
    protected static String InternalDispatchOrderPage;
    protected static String InternalDispatchOrderShippingAddress;
    protected static String btn_DraftInternalDispatchOrder;
    protected static String btn_SerialBatchInternalDispatchOrder;
    protected static String btn_ok;
    protected static String btn_DraftIO;
    protected static String btn_ReleaseInternalDispatchOrder;
    protected static String SerialorBatchnoPage;
    protected static String btn_product1;
    protected static String  btn_range;
    protected static String CountofSerial;
    protected static String SerialNo;
    protected static String btn_refresh;
    protected static String btn_product2;
    protected static String btn_alterslide;
    protected static String ExistingSerial_BatchNosPage;
    protected static String btn_addbatchproduct;
    protected static String btn_arrowtointernaldistbatchorder;
    protected static String  btn_releaseinternaldispatchorder;
    protected static String  btn_overview;
    protected static String  btn_actualUpdateConfiamation;
    protected static String btn_ActualUpdate;
    protected static String  actualupdatepage;
    protected static String btn_OpenSerialNoSelection;
    protected static String  InputProductDetailsPopUp;
    protected static String  btn_serialproduct;
    protected static String btn_update;
    protected static String btn_OpenBatchNoSelection;
    protected static String  btn_NoOfSerialProduct;
    protected static String  availableqty;
    protected static String  btn_close;
    protected static String FifoActualQty;
    protected static String btn_updateinputproducts;
    protected static String btn_closeInputProductDetails;
    protected static String btn_CostingSummaary;
    protected static String CostingSummaryPage;
    protected static String btn_material;
    protected static String  ActualUpdatePage;
    protected static String btn_resource;
    protected static String btn_actualduration;
    protected static String btn_okActualDuration;
    protected static String btn_date;
    protected static String  btn_ResourceOverview;
    protected static String btn_updateActualDuration;
    protected static String ActualWorkpage;
    protected static String btn_ResourseCost;
    protected static String btn_DailyWorkSheet;
    protected static String DailyWorkSheetPage;
    protected static String btn_NewDailyWorkSheet;
    protected static String StartNewJourneyForm;
    protected static String btn_DailyWorkSheetServiceJourney;
    protected static String btn_addnewrecord;
    protected static String btn_SearchJobNo;
    protected static String templateJOBNO;
    protected static String searchjobno;
    protected static String btn_jobno;
    protected static String btn_searchemployee;
    protected static String searchemployee;
    protected static String  btn_employee;
    protected static String btn_timein;
    protected static String timein;
    protected static String btn_oktime;
    protected static String btn_timeout;
    protected static String timeout;
    protected static String  btn_oktimeout;
    protected static String btn_updateNewJob;
    protected static String  btn_WorkedHrsDraft;
    protected static String btn_WorkedHrsRelease;
    protected static String ReleasedPage;
    protected static String btn_jobnoServiceJobOrder;
    protected static String OverviewTabPage;
    protected static String btn_labour;
    protected static String LabourTabPage;
//    protected static String btn_CostingSummary;
    protected static String btn_hour;
    protected static String  EstimatedValuesubcontract;
    protected static String btn_SubContractOrder;
    protected static String SubContractOrderPage;
    protected static String btn_searchvendor;
    protected static String searchvendor;
    protected static String btn_vendor;
    protected static String btn_checkboxsubcontractorder;
    protected static String NewPurchaseOrderPage;
    protected static String btn_CheckOutPurchaseOrder;
    protected static String VendorRefrenceNo;
    protected static String btn_SearchBillingAddress;
    protected static String BillingAddress;
    protected static String btn_applyBillingAddress;
    protected static String  btn_DraftPurchaseOrder;
    protected static String DraftPurchaseOrderPage;
    protected static String btn_ReleasePurchaseOrder;
    protected static String btn_gotopagePurchaseOrder;
    protected static String NewPurchaseInvoice;
    protected static String btn_DraftPurchaseInvoice;
    protected static String btn_checkoutpurchaseinvoice;
    protected static String btn_ReleasePurchaseInvoice;
    protected static String btn_SubContractServices;
    protected static String  btn_PurchaseOrderLink;
    protected static String PurchaseOrderPage;
    protected static String btn_PurchaseInvoiceLink;
    protected static String PurchaseInvoicePage;
    protected static String btn_CreateInvoiceProposal;
    protected static String InvoiceProposalPopup;
    protected static String  btn_InputproductCreateInvoiceProposal;
    protected static String btn_ResourceCreateInvoiceProposal;
    protected static String  btn_SubcontractCreateInvoiceProposal;
    protected static String btn_OtherServiceCreateInvoiceProposal;
    protected static String btn_checkoutCreateInvoiceProposal;
    protected static String btn_applyCreateInvoiceProposal;
    protected static String NewInvoiceProposalPage;
    protected static String btn_checkoutInvoiceProposal;
    protected static String  btn_DraftInvoiceProposal;
    protected static String  btn_LabourCreateInvoiceProposal;
    protected static String BillableQtyInputProduct;
    protected static String SalesUnit;
    protected static String  DraftInvoiceProposalPage;
    protected static String btn_ReleaseInvoiceProposal;
    protected static String BillableQtyInvoiceResource;
    protected static String btn_GoToPageInvoiceProposal;
    protected static String  btn_ServiceDetails;
    protected static String  btn_checkoutServiceInvoice;
    protected static String btn_DraftServiceInvoice;
    protected static String btn_ReleaseServiceInvoice;
    protected static String DraftServiceInvoice;
    protected static String btn_CreateServiceQuatation;
    protected static String NewServiceQuatationPage;
    protected static String btn_ServiceDetailsServiceQuatation;
    protected static String btn_checkoutServiceQuatation;
    protected static String btn_DraftServiceQuatation;
    protected static String UnitPriceInputSerial;
    protected static String UnitPriceSubContract;
    protected static String  UnitPriceService;
    protected static String  UnitPriceBatch;
    protected static String  UnitPriceFifo;
    protected static String UnitPriceLabour;
    protected static String btn_ReleaseServiceQuatation;
    protected static String DraftServiceQuatationPage;
    protected static String btn_version;
    protected static String btn_tick;
    protected static String ConfirmQuatationPopUp;
    protected static String btn_confirm;
    protected static String btn_gotopageServiceQuatation;
    protected static String btn_draftServiceOrder;
    protected static String btn_ReleaseServiceOrder;
    protected static String btn_checkoutServiceOrder;
    protected static String  btn_ServiceDetailsServiceOrder;
    protected static String  btn_QuotationDetails;
    protected static String  btn_InternalReturnOrder;
    protected static String btn_SearchInternalReturnOrder;
    protected static String btn_productreturn;
    protected static String btn_applytoreturn;
    protected static String ToWareHouse;
    protected static String  btn_DraftInternalReturnOrder;
    protected static String DraftInternalReturnOrderPage;
    protected static String btn_SerialbatchInternalReturnOrder;
    protected static String  btn_serialproducttocapture;
    protected static String  btn_updateInternalReturnOrder;
    protected static String btn_backInternalReturnOrder;
    protected static String  btn_ReleaseInternalReturnOrder;
    protected static String  btn_GoToPageInternalReturnOrder;
    protected static String  NewInternalReceiptPage;
    protected static String  btn_Draftinternalreceipt;
    protected static String DraftedInternalReceiptPage;
    protected static String btn_Releaseinternalreceipt;
    protected static String btn_Finance;
    protected static String  FinanceSubSideMenu;
     protected static String btn_OutboundPaymentAdvice;
     protected static String NewOutboundPaymentAdvicePage;
     protected static String btn_NewOutboundPaymentAdvice;
     protected static String  btn_PaymentVoucherJourney;
     protected static String   AnalysisCode;
     protected static String  Amount;
     protected static String btn_downarrow;
     protected static String btn_CostAllocation;
     protected static String CostAllocationPopUp;
     protected static String  Percentage;
     protected static String CostAssignmentType;
     protected static String btn_SearchCostObject;
     protected static String btn_AddRecordCostAllocation;
     protected static String btn_updateCostAllocation_Serice_TC_012;
     protected static String btn_UpdateCostAllocation;
     protected static String btn_jobnoServiceOrderLookup;
     protected static String btn_checkoutCostAllocation;
     protected static String btn_DraftOutboundPaymentAdvice;
     protected static String DraftOutboundPaymentAdvicePage;
     protected static String btn_ReleaseOutboundPaymentAdvicePage;
     protected static String btn_ActionOutboundPaymentAdvice;
     protected static String btn_ConverttoOutboundPayment;
     protected static String Payee;
     protected static String  btn_OutboundPaymentDetails;
     protected static String btn_CheckoutOutboundPaymentDetails;
     protected static String btn_DraftPaymentDetails;
     protected static String  DraftOutboundPaymentPage;
     protected static String btnReleasePaymentDetails;
     protected static String btn_expenses;
     protected static String  btn_Expense;
     protected static String  btn_PettyCash;
     protected static String  PettyCashPage;
     protected static String btn_NewPettyCash;
     protected static String PettyCashAccount;
     protected static String  PettyCashDescription;
     protected static String btn_paidtoemployee;
     protected static String Employee;
     protected static String  TypePaymentInformation;
     protected static String btn_CostAllocation1;
     protected static String btn_updateCostAllocation_Service_TC_011;
     protected static String btn_UpdateCostAllocation1;
     protected static String btn_DraftPettyCash;
     protected static String btn_narration;
     protected static String ExtendNarration;
     protected static String btn_narrationapply;
     protected static String btn_ReleasePettyCash;
     protected static String OutboundPaymentAdvicePage;
     protected static String  btn_AccuralVoucher;
     protected static String btn_searchvendorAcrualVoucher;
     protected static String btn_CheckoutOutboundPaymentAdvice;
      protected static String VendorReferenceNo;
      protected static String btn_navigationmenu1;
      protected static String  btn_BankAdjustment;
      protected static String  BankAdjustmentPage;
      protected static String btn_NewBankAdjustment;
      protected static String bank;
      protected static String  bankaccountno;
      protected static String  analysiscode;
      protected static String  AmountBankAdjustment;
      protected static String btn_costallocationbankadjustment;
      protected static String  btn_UpdateCostAllocation2;
      protected static String btn_CheckoutBankAdjustment;
      protected static String btn_InboundPaymentAdvice;
      protected static String btn_NewInboundPaymentAdvice;
      protected static String btn_VendorRefund;
      protected static String btn_searchvendorInboundPaymentAdvice;
      protected static String AmountInboundPaymentAdvice;
      protected static String btn_CostAllocation2;
      protected static String btn_CheckoutInboundPaymentAdvice;
      protected static String btn_DraftInboundPaymentAdvice;
      protected static String btn_CustomerDebitMemo;
      protected static String btn_SearchCustomerDebitMemo;
      protected static String customer;
      protected static String btn_customer;
      protected static String btn_RevenueAdjustment;
      protected static String btn_Adjustment;
      protected static String btn_CustomerRefund;
      protected static String btn_SearchCustomerRefund;
      protected static String AmountCustomerRefund;
      protected static String  btn_CostAllocationCustomerRefund;
      protected static String btn_UpdateCustomerRefund;
      protected static String btn_procurement;
      protected static String  ProcuremnentSubSideMenu;
      protected static String  btn_PurchaseInvoice;
      protected static String btn_NewPurchaseInvoice;
      protected static String btn_NewPurchaseInvoiceService;
      protected static String  btn_searchvendorPurchaseinvoicejourney;
      protected static String btn_productPurchaseinvoicejourney;
      protected static String btn_BillingAddress;
      protected static String BillingAddressPurchaseInvoice;
      protected static String  btn_ApplyBillingAddress;
      protected static String Discount;
      protected static String btn_CheckoutPurchaseInvoice;
      protected static String btn_ReleasePurchaseInvoice1;
      protected static String btn_CostAllocationPurchaseInvoice;
      protected static String btn_UpdatePurchaseInvoice;
      protected static String Costcenter;
      protected static String btn_deletecostallocation;
      protected static String  btn_navigationmenuPurchaseInvoice;
      protected static String btn_Product;
      protected static String btn_SerialNos;
      protected static String btn_Productso;
      protected static String btn_OutBoundloanOrder;
      protected static String btn_draftOutboundLoanOrder;
      protected static String   btn_ReleaseOutboundLoanOrder;
      protected static String  btn_GoToPageOutboundLoanOrder;
      protected static String  NewOutboundShipmentPage;
      protected static String  btn_DraftOutboundLoanOrder2;
      protected static String  DraftOutboundShipmentPage;
      protected static String btn_SerialBatchOutboundShipmentPage;
      protected static String btn_ProductOutboundShipmentPage;
      protected static String  SerialProductOutboundShipment;
      protected static String  btn_DraftOutboundShipmentPage;
      protected static String  btn_InventoryandWareHousing;
      protected static String  btn_OutBoundLoanOrder;
      protected static String OutBoundLoanOrderOrderPage;
      protected static String btn_OutboundloanOrder;
      protected static String btn_WarrantyReplacement;
      protected static String WarrantyReplacementPopUp;
      protected static String  btn_ReplacementSerialNo;
      protected static String btn_SerialProductSelect;
      protected static String btn_OutboundloanOrderDoc;
      protected static String  btn_stopServiceJobOrder;
      protected static String  btn_CompleteServiceJobOrder;
      protected static String  CompleteProcessPopup;
      protected static String btn_ServiceContract;
      protected static String btn_NewServiceContractForm;
      protected static String NewServiceContractFormPage;
      protected static String  ServiceContractTitle;
      protected static String  btn_SearchCustomerServiceContract;
      protected static String  btn_EffectiveFrom;
      protected static String btn_EffectiveFromDate;
      protected static String btn_EffectiveTo;
      protected static String  btn_EffectiveToDate;
      protected static String  btn_nextMonthDatePlicker;
      protected static String ContractGroup;
      protected static String  btn_OrderNo1;
      protected static String  InternalOrderPage;
      protected static String  DraftedInternalOrderPage;
      protected static String btn_NewEmployee;
      protected static String employeecode;
      protected static String  employeename;
      protected static String namewithintials;
      protected static String  employeegroup;
      protected static String  employeelookup;
      protected static String   btn_OrganizationManagement;
      protected static String  btn_EmployeeInformation;
      protected static String  btn_ReleaseEmployee;
      protected static String btn_searchrateprofile;
      protected static String btn_rate;
      protected static String payableaccount;
      protected static String btn_ratesearch;
      protected static String ReleaseServiceInvoicePage;
      protected static String  btn_action1;
      protected static String  btn_ServiceLevelAgreement;
      protected static String  btn_agreementname;
      protected static String  Description;
      protected static String  searchowner;
      protected static String  btn_owner1;
      protected static String  menu;
      protected static String  serialWise;
      protected static String serialTextVal;
      protected static String   serialMenuClose;
      protected static String serialTextVal2;
      protected static String serialTextVal2_Service_TC_001;
      protected static String menu1;
      protected static String btn_ServiceLevelAgreement1;
      protected static String btn_searchserviceproduct1;
      protected static String btn_SearchSceduleCode;
      protected static String SceduleCode;
      protected static String btn_SceduleCode;
      protected static String btn_responsibleuser;
      protected static String  responsibleuser;
      protected static String btn_responsibleusername;
      protected static String btn_ContractLine;
      protected static String  tagno;
      protected static String btn_Summary;
      protected static String btn_checkoutServiceContract;
      protected static String  btn_draftServiceContract;
      protected static String  EstimatedCost;
      protected static String pricingprofile;
      protected static String btn_pricingprofile12;
      protected static String   btn_searchserviceproductfirst;
      protected static String btn_billable;
      protected static String Referencecode;
      protected static String btn_searchlotproduct;
      protected static String  btn_expand9;
      protected static String btn_searchBatchSerialproduct;
      protected static String  btn_expand10;
      protected static String  EstimatedValueService;
      protected static String ServiceJobOrderReleaseNoTC;
      protected static String   EstimatedQtyLOT;
      protected static String  EstimatedSerialBatchQty;
      protected static String InternalOrderReleaseNoTC;
      protected static String  internaldispatchorderpage;
      protected static String internaldispatchorderReleaseTC;
      protected static String InternalReturnOrderReleaseNoTC;
      protected static String  ReleasedInternalReturnOrderPage;
      protected static String ReleasedInternalReceiptTC;
      protected static String ReleasedInternalReceipt;
      protected static String  ReleaseInvoiceProposalPage;
      protected static String ReleasePurchaseInvoicePage;
      protected static String ReleasedInvoiceProposalPage;
      protected static String ReleasedServiceOrderPage;
      protected static String ReleasedServiceQuatationPage;
      protected static String UnitPriceLabour1;
      protected static String  UnitPriceExpense;
      protected static String   ReleasedOutboundPaymentAdvicePage;
      protected static String  ReleaseOutboundPaymentPage;
      protected static String ReleasedpettycashPage;
      protected static String rateprofile;
      protected static String rateprofilevalue;
      protected static String btn_searchworkingcalander;
      protected static String workingcalender;
      protected static String workingcalendernew;
      protected static String btn_nextdate;
      protected static String  btn_editServiceContract;
      protected static String  btn_PreventiveMaintenance;
      protected static String  btn_plus;
      protected static String Title;
      protected static String PMCategory;
      protected static String PMcount;
      protected static String PMinterval;
      protected static String owner;
      protected static String btn_ChargeableProd;
      protected static String btn_searchproducts;
      protected static String btn_UpdatePreventiveMaintenance;
      protected static String   ReleaseServiceContractPage;
      protected static String  btn_ConverttoServiceInvoice;
      protected static String  btn_checkoutServiceInvoice1;
      protected static String  btn_draftServiceInvoice;
      protected static String  btn_updateServiceContract;
      protected static String btn_draftServiceInvoice1;
      protected static String btn_releaseServiceInvoice1;
      protected static String ReleasedServiceInvoice1page;
      protected static String btn_entution;
      protected static String btn_Rolecenter;
      protected static String  Dashboardpage;
      protected static String btn_ScheduledJobs;
      protected static String btn_OldServiceContract;
      protected static String btn_ServiceContractLine;
      protected static String btn_ServiceInvoiceLink;
      protected static String  btn_reverce;
      protected static String btn_Revercedate;
      protected static String btn_selectRevercedate;
      protected static String btn_reverceapply;
      protected static String RevercePopupPage;
      protected static String btn_yesServiceInvoice;
      protected static String btn_reverceservicecontract;
      protected static String btn_yesServiceContract;
      protected static String  btn_ServiceQuotation;
      protected static String  ServiceQuotationPage;
      protected static String btn_newServiceQuotation;
      protected static String btn_newServiceQuotationtoServiceJobOrder;
      protected static String btn_SearchCustomerServiceQuoatation;
      protected static String currency;
      protected static String btn_searchBillingAddress;
      protected static String BillingAddress1;
      protected static String ShipingAddress1;
      protected static String salesunit;
      protected static String btn_searchServiceProduct;
      protected static String serviceproduct;
      protected static String btn_ServiceDetails1;
      protected static String unitprice;
      protected static String btn_checkoutServiceQuatation1 ;
      protected static String btn_searchSerialProduct;
      protected static String  btn_plusSQ1;
      protected static String btn_draftSericeQuotation;
      protected static String btn_draftSericeQuotationNew;
      protected static String  btn_ReleaseSericeQuotationNew;
      protected static String ReleaseSericeQuotationNewPage;
      protected static String btn_contractdate;
      protected static String btn_beforedate;
      protected static String btn_selectcontractdate;
      protected static String btn_ChargeableProd1;
      protected static String qty;
      protected static String btn_plus2;
      protected static String typelabour;
      protected static String  btn_searchlabourSQ;
      protected static String LabourEmployeeSQ;
      protected static String  unitpricelabour;
      protected static String  QTY1;
      protected static String  QTY2;
      protected static String  QTY3;
      protected static String  btn_searchlabouremployeeSQ;
      protected static String  btn_checkoutServiceOrder1;
      protected static String  btn_DraftServiceOrder1;
      protected static String btn_ReleaseServiceOrder1;
      protected static String btn_CreateSJO;
      protected static String btn_ServiceDetails2;
      protected static String DraftedSericeQuotationNewPage;
      protected static String DraftedServiceOrderNewPage;
      protected static String btn_actionSO;
      protected static String filledcoloums;
      protected static String Productlist1;
      protected static String btn_range1;
      protected static String btn_HireAgreement;
      protected static String btn_NewHireAgreement;
      protected static String NewHireAgreementpage;
      protected static String AgreementNo;
      protected static String  TitleAgreement;
      protected static String btn_agreementdate;
      protected static String  DescriptionHireAgreement;
      protected static String  salesunitHireAgreement;
      protected static String  HireBasis;
      protected static String btn_accountsearchHireAgreement;
      protected static String btn_SearchSceduleCodeHireAgreement;
      protected static String  btn_responsibleuserHireAgreement;
      protected static String Contractgroup;
      protected static String btn_ResourceInformation;
      protected static String RecourceInformationPage;
      protected static String btn_searchbtnReferncecode;
      protected static String ReferncecodePage;
      protected static String Referncecode;
      protected static String btn_Referncecode;
      protected static String  openingunit;
      protected static String  Minimumunit;
      protected static String btn_RateRatio;
      protected static String  RateRangeSetup;
      protected static String  Start1;
      protected static String End1;
      protected static String  Rate1;
      protected static String btn_plusRR;
      protected static String Start2;
      protected static String End2;
      protected static String  Rate2;
      protected static String btn_draftHireAgreement;
      protected static String DraftedHireAgreement;
      protected static String btn_ReleaseHireAgreement;
      protected static String ReleasedHireAgreement;
      protected static String btn_case;
      protected static String CasePage;
      protected static String  btn_NewCase;
      protected static String NewCasePage;
      protected static String TitleNewCase;
      protected static String Orgin;
      protected static String btn_accountsearchCase;
      protected static String btn_ProductsoCase;
      protected static String  btn_SerialNosCase;
      protected static String TypeCase;
      protected static String btn_ApplyCase_Smoke_Service_004;
      protected static String btn_draftcase;
      protected static String DraftedCasePage;
      protected static String btn_releasecase;
      protected static String btn_ServiceProductRegistration;
      protected static String btn_NewServiceProductRegistration;
      protected static String RegistrationType;
      protected static String btn_accountsearchServiceProductRegis;
      protected static String btn_Tagno;
      protected static String btn_SerialNo;
      protected static String btn_Tagnos;
      protected static String btn_SearchWarrantyProfile;
      protected static String Warranty;
      protected static String  btn_Warranty;
      protected static String  btn_WarrantyDate;
      protected static String btn_selectWarrantyDate;
      protected static String btn_warrantyProfileSales;
      protected static String btn_WarrantyDateSales;
      protected static String btn_DraftServiceProductReg;
      protected static String btn_ReleaseServiceProductReg;
      protected static String tagnoDirectSR;
      protected static String btn_searchReferenceCode;
      protected static String btn_searchShippingAddress;
      protected static String ShippingAddress;
      protected static String btn_ApplyAdress;
      protected static String btn_searchtagno;
      protected static String  btn_serialno;
      protected static String  btn_PricingProfileSearchSE;
      protected static String pricingprofileSE;
      protected static String btn_EstimationDetails;
      protected static String btn_ServiceCostEstimation;
      protected static String btn_NewServiceCostEstimation;
      protected static String DescriptionService;
      protected static String btn_SearchCustomerServiceCE;
      protected static String AccountLookUPPageSE;
      protected static String btn_EmployeeAdvanceRequest;
      protected static String btn_NewEmployeeAdvanceRequest;
      protected static String  btn_searchemployeeEmployeeAdvanceRequest;
      protected static String ExpenseType;
      protected static String Purpose;
      protected static String  AdvanceAmount;
      protected static String btn_DraftEmployeeAdvanceRequest;
      protected static String btn_PayEmployeeAdvance;
      protected static String OutboundPaymentAdvancePage;
      protected static String btn_NewConvertToOutboundPayment;
      protected static String OutboundPaymentPage;
      protected static String btn_putTick;
      protected static String  btn_checkoutOutboundPaymentAdvice;
      protected static String DraftOutboundpaymentAdvice;
      protected static String btn_details;
      protected static String btn_DraftOutBoundPaymentAdvice;
      protected static String btn_checkoutOutboundPayment;
      protected static String btn_NewcheckoutOutboundPaymentAdvice;
      protected static String  btn_actionNewConvertToOutboundPayment;
      protected static String  selectagreementdate;
      protected static String  warehouse1;
      protected static String btn_billable1;
      protected static String  btn_searchbatchproduct2;
      protected static String btn_searchResourcesReferenceCode3;
      protected static String btn_searchResources3;
      protected static String btn_checkoutServiceCostEstimation;
      protected static String btn_LastdayofMonthPlus;
      protected static String btn_ServiceContractLastdayofMonth;
      protected static String btn_selectallServiceContractLastdayofMonth;
      protected static String  btn_runServiceContractLastdayofMonth;
      protected static String BillingBasis;
      protected static String actualunit;
      protected static String btn_checkoutActualUpdate;
      protected static String btn_applyActualUpdate;
      protected static String  MessagePopup;
      protected static String btn_serviceInvoicePath;
      protected static String ServiceInvoicePage;
      protected static String btn_ServiceHireAgreement;
      protected static String  ActualUpdate;
      protected static String btn_ProductRegistration;
      protected static String btn_SearchWarrantyProfileProductRegis;
      protected static String  btn_ApplyProductRegis;
      protected static String serialBatchProductRegis;
      protected static String productListBtn;
      protected static String serialTextValuePR;
      protected static String btn_okProductRegistration;
      protected static String serialNoProductRegis;
      protected static String btn_SalesReferDoc;
      protected static String SalesInvoicePage;
      protected static String btn_AssosiateProduct;
      protected static String btn_searchAssosiateProduct;
      protected static String  TagnoAssosiateProduct;
      protected static String  btn_searchWarrantyProfileAssosiateProduct;
      protected static String  btn_AssosiateProductTab;
      protected static String  btn_ChangeButtonOwnerShipInformation;
      protected static String  btn_SerarchNewOwner;
      protected static String btn_applyChangeOwnershipInformation;
      protected static String btn_ChangeProductRegisInformation;
      protected static String  btn_SearchEndUser;
      protected static String   btn_ApplyProductRegisInformation;
      protected static String btn_ExtendSalesWarranty;
      protected static String btn_searchSalesWarrantyExtend;
      protected static String  btn_plusSalesWarrantyExtend;
      protected static String btn_UpdateSalesWarranty;
      protected static String btn_Renewal;
      protected static String  btn_yesRenewal;
      protected static String  btn_Renewal2;
      protected static String  btn_ReplaceProducts;
      protected static String btn_SearchReplaceProduct;
      protected static String Replacetagno;
      protected static String  btn_SearchSerialProduct;
      protected static String  TagNoSO;
      protected static String  SearchTagNo;
      protected static String btn_SpecificTagNo;
      protected static String  HireAgreeNumber;
      protected static String SpecificHireagreementNo;
      protected static String run;
      protected static String btn_axpandFirstLevelSedulesdJob;
      protected static String btn_axpandSecondLevelSedulesdJob;
      protected static String ReleasedServiceInvoice;
      protected static String SerialSpecific;
      protected static String SerialspecificServiceJobOrderName;
      protected static String SerialBatchNAME;
      protected static String  SerialBatchServiceJobOrderName;
      protected static String btn_OrderNo5;
      protected static String LotAutoNAME;
      protected static String LotAutoServiceJobOrderName;
      protected static String btn_OrderNo4;
      protected static String  FifoBatchNAME;
      protected static String  FifoBatchServiceJobOrderName;
      protected static String BatchspecificNAME;
      protected static String BatchspecificServiceJobOrderName;
      protected static String btn_OrderNo2;
      protected static String serialspecificAQValue;
      protected static String BatchspecificAQValue;
      protected static String  FIFOBATCHAQValue;
      protected static String autotestingAQValue;
      protected static String  btn_reversecase;
      protected static String btn_yesReverseCase;
      protected static String  btn_OutboundPaymentAdviceLink;
      protected static String btn_newservicejoborder;
      protected static String  ServiceJobOrderValue;
      protected static String TotalValueNo;
      protected static String TotalExpenceValue;
      protected static String OutboundPaymentValueNo;
      protected static String TotalValueNoBankAdjustment;
      protected static String  btn_BankAdjustmentLink;
      protected static String TotalDutyValue;
      protected static String  btn_CostAdjustment;
      protected static String  btn_InboundPaymetAdviceLink;
      protected static String  btn_CostAdjustmentArrow;
      protected static String  VendorAdjustmentValue;
      protected static String TotalValueVendorRefund;
      protected static String  btn_InboundPaymetAdviceLinkRevenueAdjustment;
      protected static String btn_RevenueAdjustmentArrow;
      protected static String CustomerAdjustmentValue;
      protected static String btn_deletePI;
      protected static String  btn_updatePI;
      protected static String btn_EditServiceJobOrder;
      protected static String  EditServiceJobOrderPage;
      protected static String  UpadatedServiceJobOrderPage;
      protected static String btn_UpdateandNewServiceJobOrder;
      protected static String btn_DraftandNewServiceJobOrder;
      protected static String btn_closepopup;
      protected static String btn_AddProductTagNo;
      protected static String AddProductTagNoPage;
      protected static String btn_GenerateCustomerAdvance;
      protected static String  CustomerAdvancePage;
      protected static String btn_GenerateResponceDetails;
      protected static String ResponceDetailsPage;
      protected static String InvoiceProposalPage;
      protected static String  btn_RMA;
      protected static String RMADetailsPage;
      protected static String ServiceQuotationPagePopup;
      protected static String SerialBatchAQValue;
      protected static String ReleaseOutboundLoanOrderPage;
      protected static String ProductRegistrationPage;
      protected static String  customeraccountvalue;
      protected static String warrantyprofilevalue;
      protected static String btn_backdate;
      protected static String  btn_link;
      protected static String btn_runHireAgreement;
      protected static String stopserviceOrderPage;
      protected static String  btn_closeRMA;
      protected static String  SubContractOrderPage1;
      protected static String  btn_ServiceOrdertoInternalDispatchOrderInternal;
      protected static String btn_ServiceInvoice;
      protected static String  ServiceInvoice;
      protected static String btn_selectServiceInvoice;
      protected static String btn_History;
      protected static String ServiceInvoiceReversedPage;
      protected static String  btn_docflow;
      protected static String ServiceDocFlowPage;
      protected static String btn_activities;
      protected static String ServiceInvoiceActivitiesPage;
      protected static String btn_journal;
      protected static String ServiceInvoiceJournalPage;
      protected static String  btn_CloseActivitiesPage;
      protected static String btn_ServiceOrder;
      protected static String ServiceOrderNo;
      protected static String btn_selectServiceOrder;
      protected static String btn_CopyFrom;
      protected static String caseno;
      protected static String btn_selectcaseno;
      protected static String btn_DraftandNewEmployeeAdvanceRequest;
      protected static String btn_copyfromCostEstimation;
      protected static String ServiceCostEstimationForm;
      protected static String SelectServiceCoseEstimation;
      protected static String btn_checkoutCostEstimation;
      protected static String  btn_duplicateServiceCostEstimation;
      protected static String HireAgreementNo;
      protected static String btn_selectHireagreement;
      protected static String btn_draftnewHireAgreement;
      protected static String  btn_CopyFromHireAgreement;
      protected static String  btn_closeactualupdate;
      protected static String  btn_ServiceGroupconfig;
      protected static String ServiceGroupconfigpage;
      protected static String  btn_newServiceConfiguration;
      protected static String  NewServiceGroupconfigpage;
      protected static String  SelectServiceGroup;
      protected static String  Doctitle;
      protected static String btn_copyfromServiceGroupConfiguaration;
      protected static String serviceConfig;
      protected static String btn_specificServiceConfig;
      protected static String btn_searchServiceGroupCongig;
      protected static String btn_DraftandNewServiceCongig;
      protected static String  btn_searchEmployeeHiearchy;
      protected static String searchhiearchy;
      protected static String btn_specificsearchhiearchy;
      protected static String  btn_ServiceControl;
      protected static String  ServiceControlpage;
      protected static String btn_casedetails;
      protected static String  btn_gernerateServiceOrder;
      protected static String ServiceJobOrderpageSC;
      protected static String btn_CopyfromBankAdjustment;
      protected static String BankAdjustment;
      protected static String btn_selectBankAdjustment;
      protected static String btn_draftandnewBankAdjustment;
      protected static String  NewBankAdjustmentPage;
      protected static String btn_draftBankAdjustment;
      protected static String DraftedBankAdjustmentPage;
      protected static String  btn_ReleaseBankAdjustmentPage;
      protected static String  ReleasedBankAdjustmentPage;
      protected static String  btn_CopyfromPettyCash;
      protected static String  pettycashName;
      protected static String btn_draftpettycash;
      protected static String DraftedPettyCashPage;
      protected static String btn_copyfromInboundPaymentAdvice;
      protected static String InboundAdviseNo;
      protected static String selectInboundAdvice;
      protected static String btn_SalesMarkting;
      protected static String btn_WarrantyProfile;
      protected static String Newbtn_WarrantyProfile;
      protected static String profilecode;
      protected static String profilename;
      protected static String btn_DraftWarrantyProfile;
      protected static String btn_expandFirstPlusMarkHireAgreementDaily;
      protected static String btn_expandSecondPlusHireAgreement;
      protected static String btn_runDailyMethodHireAgreement;
      
    //Service_Case_001
    protected static String btn_FormCaseA;
    protected static String btn_FormNewCaseA;
    protected static String txt_pageHeaderA;
    
    //Service_Case_002
    protected static String txt_TitleA;
    protected static String txt_OriginA;
    protected static String txt_CustomerAccountA;
    protected static String txt_ProductA;
    protected static String txt_TagNoA;
    protected static String txt_TypeA;
    protected static String txt_ServiceGroupA;
    protected static String txt_PriorityA;
    protected static String txt_CurrencyA;
    protected static String txt_BillingAddressA;
    protected static String txt_ShippingAddressA;
    
    //Service_Case_003
    protected static String btn_DraftA;
    protected static String btn_ReleaseA;
    protected static String txt_DraftValidatorA;
    protected static String txt_TitleValidatorA;
    protected static String txt_OriginValidatorA;
    protected static String txt_CustomerAccountValidatorA;
    protected static String txt_ProductValidatorA;
    protected static String txt_TagNoValidatorA;
    protected static String txt_TypeValidatorA;
    protected static String txt_ServiceGroupValidatorA;

    //Service_Case_004
    protected static String btn_CustomerAccountButtonA;
    protected static String txt_CustomerAccountTextA;
    protected static String sel_CustomerAccountSelA;
    protected static String btn_newWindowProdA;
    protected static String sel_newWindowProdSelA;
    protected static String txt_TitleTextA;
    protected static String txt_OriginTextA;
    protected static String txt_TypeTextA;
    protected static String txt_ServiceGroupTextA;
    protected static String txt_PriorityTextA;
    protected static String txt_PageDraft1A;
    protected static String txt_PageRelease1A;
    protected static String txt_PageDraft2A;
    protected static String txt_PageRelease2A;
    
    //Service_Case_005
    protected static String txt_trackCodeA;
    
    //Service_Case_006
    protected static String btn_RolecenterA;
    protected static String btn_searchTagA;
    protected static String txt_searchTagtxtA;
    protected static String btn_searchTagarrowA;

    
    //Service_Case_007
    protected static String btn_CreateServiceQuotationBtnA;
    protected static String btn_DroptoServiceCentreBtnA;
    protected static String btn_ResponseDetailBtnA;
    protected static String btn_ActionBtnA;
    
    //Service_Case_008
    protected static String btn_DuplicateA;

    //Service_Case_009
    protected static String btn_DeleteBtnA;
    protected static String btn_YesBtnA;
    protected static String txt_pageDeletedA;
    
    //Service_Case_010
    protected static String btn_EditA;
    protected static String btn_UpdateA;
    protected static String txt_OriginTextFieldA;
    protected static String txt_TypeTextFieldA;
    
    //Service_Case_011
    protected static String btn_UpdateAndNewA;
      
    //Service_Case_012
    protected static String btn_DraftAndNewA;
    protected static String btn_CaseFormByPageA;
    protected static String sel_DraftedCaseDocA;
    
    //Service_Case_013
    protected static String btn_CopyFromA;
    protected static String txt_CopyFromSerchtxtA;
    protected static String sel_CopyFromSerchselA;
    protected static String txt_TitleTextFieldA;
    protected static String txt_CustomerAccountTextFieldA;
    protected static String txt_ProductTextFieldA;
    protected static String txt_TagNoTextFieldA;
    protected static String txt_ServiceGroupTextFieldA;
    protected static String txt_PriorityTextFieldA;

    //Service_Case_014
    protected static String btn_ReverseBtnA;
    protected static String txt_pageReversedA;
    
    //Service_Case_015
    protected static String btn_BillingAddressDeleteA;
    protected static String btn_ShippingAddressDeleteA;
    protected static String txt_BillingAddressValidatorA;
    protected static String txt_ShippingAddressValidatorA;

    
    
    //Service_Case_016
    protected static String txt_StageFieldA;
    protected static String btn_StatusFieldArrowA;
    protected static String txt_StatusFieldTxtA;
    
    //Service_Case_017
    protected static String txt_SalesUnitA;
    protected static String btn_ServiceDetailsA;
    protected static String btn_Plus1A;
    protected static String btn_Plus2A;
    protected static String btn_Plus3A;
    protected static String btn_Plus4A;
    protected static String btn_Plus5A;
    protected static String txt_Type1A;
    protected static String txt_Type2A;
    protected static String txt_Type3A;
    protected static String txt_Type4A;
    protected static String txt_Type5A;
    protected static String txt_Type6A;
    protected static String btn_ProSearch1A;
    protected static String btn_ProSearch2A;
    protected static String btn_ProSearch3A;
    protected static String btn_ProSearch4A;
    protected static String btn_ProSearch5A;
    protected static String btn_ProSearch6A;
    protected static String txt_ProtxtA;
    protected static String sel_ProselA;
    protected static String txt_UnitPrice1A;
    protected static String txt_UnitPrice2A;
    protected static String txt_UnitPrice3A;
    protected static String txt_UnitPrice4A;
    protected static String txt_UnitPrice5A;
    protected static String txt_UnitPrice6A;
    protected static String btn_ReferenceCodeLabourA;
    protected static String btn_ReferenceCodeResourceA;
    protected static String txt_ReferenceCodeExpenseA;
    protected static String txt_ReferenceCodeLabourtxtA;
    protected static String sel_ReferenceCodeLabourselA;
    protected static String txt_ReferenceCodeResourcetxtA;
    protected static String sel_ReferenceCodeResourceselA;
    protected static String btn_CheckoutA;
    protected static String btn_VersionA;
    protected static String btn_colorSelectedA;
    protected static String btn_ConfirmTikA;
    protected static String btn_ConfirmbtnA;
    protected static String btn_GoToPageLinkA;
    protected static String btn_ServiceDetailsSOA;
    protected static String txt_pageConfirmedA;
    
    //Service_Case_019
    protected static String sel_CaseSerchSelA;
    protected static String btn_MiddleNavigationBarA;
    protected static String sel_CaseSerchSelNewA;

    //Service_Case_020
    protected static String txt_CaseSerchSelStatusA;
    
    //Service_Case_021
    protected static String txt_ConfirmNotificationTxt1A;
    protected static String txt_ConfirmNotificationTxt2A;
    
    //Service_Case_024
    protected static String btn_DroptoServiceCentreBtnAfterSQA;
    protected static String btn_EntutionHeaderA;
    protected static String btn_EstimatedServiceJobsA;
    protected static String txt_EstimatedServiceJobsNoA;
    protected static String btn_ServiceJobArrowA;
    protected static String btn_OwnerBtnA;
    protected static String txt_OwnerTxtA;
    protected static String sel_OwnerSelA;
    protected static String btn_PricingProfileBtnA;
    protected static String txt_PricingProfileTxtA;
    protected static String sel_PricingProfileSelA;
    protected static String btn_ServiceLocationBtnA;
    protected static String txt_ServiceLocationTxtA;
    protected static String sel_ServiceLocationSelA;

    //Service_Case_030
    protected static String btn_OpenServiceJobsA;
    protected static String txt_OpenServiceJobsNoA;
    protected static String btn_TaskDetailsA;
    protected static String btn_TaskDetailsProBtnA;
    protected static String txt_TaskDetailsProTxtA;
    protected static String sel_TaskDetailsProSelA;
    //Service_Case_032
    protected static String txt_DropNotificationTxt3A;
    
    //Service_Case_033
    protected static String btn_CloseBtnA;
    protected static String txt_cboxPOPSatisfactionA;
    protected static String txt_PageClosedA;
    
    //Service_Case_034
    protected static String btn_ViewHistoryBtnA;
    protected static String txt_PendingCasesTileHeaderA;
    protected static String txt_CompletedCasesTileHeaderA;
    protected static String txt_RegisteredProductsTileHeaderA;
    
    //Service_Case_035
    protected static String btn_HoldBtnA;
    protected static String txt_HoldReasonA;
    protected static String txt_PageHoldA;
    
    //Service_Case_036
    protected static String icn_HoldIconA;
    
    //Service_Case_037
    protected static String btn_UnHoldBtnA;
    
    //Service_Case_038
    protected static String icn_UnHoldIconA;
    
    //Service_Case_039
    protected static String txt_BookNoTxtA;
    protected static String txt_DocumentNoTxtA;
    protected static String txt_DocumentDateTxtA;

    //Service_Case_040
    protected static String txt_ResolutionDescriptionA;
    protected static String txt_ResponseTypeA;
    protected static String btn_ResponseDateA;
    protected static String txt_NextYearA;
    protected static String txt_SelectDateA;
    protected static String txt_SatisfactionA;
    protected static String btn_ResponseDetailsUpdateA;
    
    //Service_Case_041
    protected static String btn_ResolutionTabA;
    protected static String txt_ResolutionTextA;
    protected static String btn_ResolutionTabSJOA;
    protected static String txt_ResolutionTextSJOA;
    
    //Service_Case_042
    protected static String btn_ResponseDetailBtnSJOA;
    protected static String txt_ResponseTypeSJOA;
    
    //Service_Case_044
    protected static String txt_WarrantyStartA;
    protected static String txt_WarrantyEndA;
    
    //Service_Case_045
    protected static String btn_ServiceContractBtnA;
    protected static String txt_ServiceContractTxtA;
    protected static String sel_ServiceContractselA;
    protected static String txt_ServiceContractTextFieldA;

    //Service_Case_046
    protected static String txt_ServiceLevelAgreementTextFieldA;
    
    //Service_Case_047
    protected static String btn_knowledgeBaseBtnA;
    protected static String txt_ProblemTxtA;
    protected static String txt_QuestionTxtA;
    protected static String txt_RequestTxtA;
    protected static String txt_InquiryTypeA;
    
    //Service_Case_048
    protected static String icn_DraftIconA;
    
    //Service_Case_049
    protected static String icn_ReleasedIconA;

    //Service_Case_050
    protected static String icn_ReverseIconA;
    
    //Service_Case_051
    protected static String icn_DeleteIconA;
    
    //Service_Case_052
    protected static String btn_OriginAddBtnA;
    protected static String btn_OriginAddPlusBtnA;
    protected static String txt_OriginAddTextA;
    
    //Service_Case_053
    protected static String btn_OriginInactiveA;
    
    //Service_Case_054
    protected static String sel_SerialNosSelA;

    //Service_Case_055
    protected static String btn_AddNewCasePlusA;
    protected static String btn_ParentCaseSerchBtnA;
    protected static String txt_ParentCaseSerchTxtA;
    protected static String sel_ParentCaseSerchSelA;
    protected static String txt_ParentCaseTxtA;
    
    //Service_Case_056
    protected static String btn_TypeAddBtnA;
    protected static String btn_TypeAddPlusA;
    protected static String txt_TypeAddTextA;
    
    //Service_Case_057
    protected static String btn_TypeInactiveA;
    
    //Service_Case_058
    protected static String btn_ServiceGroupAddBtnA;
    protected static String btn_ServiceGroupAddPlusA;
    protected static String txt_ServiceGroupAddTextA ;
    
    //Service_Case_059
    protected static String btn_ServiceGroupInactiveA;
    
    //Service_Case_060
    protected static String btn_AdministrationBtnA;
    protected static String btn_BalancingLevelSettingsBtnA;
    protected static String btn_SalesAndMarketingBtnA;
    protected static String txt_cboxAccountCodeNameA;
    protected static String btn_SalesAndMarketingUpdateBtnA;

    //Service_Case_061
    protected static String sel_CustomerAccountSelCodeA;
    
    //Service_Case_062
    protected static String sel_CustomerAccountSelNameA;
    
    //Service_Case_063
    protected static String btn_InventoryBtnA;
    protected static String txt_cboxProductCodeNameA;
    protected static String btn_InventoryUpdateBtnA;
    
    //Service_Case_066
    protected static String btn_StopBtnA;
    protected static String btn_CompleteProcessBtnA;
    protected static String btn_CompleteBtnA;
    protected static String btn_CompleteYesBtnA;
    protected static String txt_PostDateA;
    protected static String btn_PostDateApplyA;
    protected static String txt_pageCompletedA;
    
    //Service_HA_001
    protected static String btn_HireAgreementBtnA;
    protected static String btn_NewHireAgreementBtnA;
    protected static String txt_NewHireAgreementPageA;
    
    //Service_HA_002
    protected static String txt_AgreementNoStarA;
    protected static String txt_TitleStarA;
    protected static String txt_AgreementDateStarA;
    protected static String txt_ContractGroupStarA;
    protected static String txt_ValidityPeriodForStarA;
    protected static String txt_EffectiveFromStarA;
    protected static String txt_EffectiveToStarA;
    protected static String txt_DescriptionStarA;
    protected static String txt_SalesUnitStarA;
    protected static String txt_AccountOwnerStarA;
    protected static String txt_CustomerAccountStarA;
    protected static String txt_CurrencyStarA;
    protected static String txt_ScheduleCodeStarA;
    
    //Service_HA_003
    protected static String txt_AgreementNoValidationA;
    protected static String txt_TitleValidationA;
    protected static String txt_AgreementDateValidationA;
    protected static String txt_ContractGroupValidationA;
    protected static String txt_EffectiveFromValidationA;
    protected static String txt_EffectiveToValidationA;
    protected static String txt_DescriptionValidationA;
    protected static String txt_SalesUnitValidationA;
    protected static String txt_AccountOwnerValidationA;
    protected static String txt_CustomerAccountValidationA;
    protected static String txt_ScheduleCodeValidationA;
    
    //Service_HA_004
    protected static String txt_AgreementNoTxtA;
    protected static String txt_TitleTxtA;
    protected static String txt_ContractGroupTxtA;
    protected static String txt_DescriptionTxtA;
    protected static String btn_CustomerAccountHABtnA;
    protected static String txt_CustomerAccountHATxtA;
    protected static String Sel_CustomerAccountHASelA;
    protected static String txt_SalesUnitHAA;
    protected static String btn_ScheduleCodeHABtnA;
    protected static String txt_ScheduleCodeHATxtA;
    protected static String Sel_ScheduleCodeHASelA;
    protected static String btn_ResourceInformationTabA;
    protected static String txt_TypeHAA;
    protected static String btn_ReferenceCodeHABtnA;
    protected static String txt_ReferenceCodeHATxtA;
    protected static String Sel_ReferenceCodeHASelA;
    protected static String btn_ResponsibleUserHABtnA;
    protected static String txt_ResponsibleUserHATxtA;
    protected static String Sel_ResponsibleUserHASelA;
    protected static String txt_HireBasisA;
    protected static String txt_OpeningUnitA;
    protected static String btn_RateRangeA;
    protected static String txt_MinimumUnitA;
    protected static String txt_Rate1A;
    protected static String txt_Rate2A;
    protected static String btn_RateRangeSetupApplyA;
    protected static String btn_RateRangeSetupPlusA;
    protected static String txt_RateRangeSetupStartA;
    protected static String txt_RateRangeSetupEndA;
    
    //Service_HA_005
    protected static String txt_ReferenceCodeErrorA;
    protected static String txt_ServiceProductErrorA;
    protected static String btn_ErrorBtnA;
    protected static String txt_SelectaServiceProductErrorA;
    protected static String txt_SelectaResourceProductErrorA;
    protected static String txt_EnterOpeningUnitsErrorA;
    
    //Service_HA_006
    protected static String txt_TitleFieldA;
    
    //Service_HA_007
    protected static String btn_DeleteHABtnA;
    
    //Service_HA_009
    protected static String btn_ReverseHABtnA;
    protected static String btn_ActivitiesHABtnA;
    protected static String btn_ActualUpdateHABtnA;
    protected static String btn_CloseAgreementHABtnA;
    protected static String btn_ConvertToAcquireDepositHABtnA;
    
    //Service_HA_011
    protected static String btn_HireAgreementByPageBtnA;
    protected static String txt_HireAgreementSerchTxtA;
    protected static String sel_HireAgreementSerchSelA;
    protected static String btn_DuplicateHABtnA;
    
    //Service_HA_012
    protected static String btn_DuplicateHABtnAfterReleaseA;

    //Service_HA_016
    protected static String btn_ResourcePlusBtnA;
    protected static String btn_ReferenceCodeHABtn2A;
    protected static String btn_RateRange2A;
    protected static String sel_ReferenceCodeHASel2A;
    protected static String txt_OpeningUnit2A;
    protected static String txt_Resource1A;
    protected static String txt_Resource2A;

    
    //Service_HA_017
    protected static String txt_CopyFromSerchHATxtA;
    protected static String sel_CopyFromSerchHASelA;

    //Service_HA_018
    protected static String txt_cboxActualResourcesA;

    //Service_HA_019
    protected static String txt_ActualUnitA;
    protected static String btn_CheckoutHAA;
    
    //Service_HA_020
    protected static String txt_ActualUpdateStatusA;

    //Service_HA_022
    protected static String txt_AgreementDateA;
    protected static String txt_EffectiveFromDateA;
    protected static String txt_EffectiveToDateA;
    protected static String txt_BillingBasisA;
    protected static String Sel_ReferenceCodeHA2SelA;
    protected static String txt_NextBillingDateA;
    
    //Service_HA_023
    protected static String Sel_ReferenceCodeHA3SelA;
    
    //Service_HA_024
    protected static String Sel_ReferenceCodeHA4SelA;
    protected static String txt_EffectiveFromDateAfterReleaseA;
    
    //Service_HA_025_026
    protected static String Sel_ResponsibleUserHA2SelA;
    protected static String btn_LogOutBtnA;
    protected static String btn_SignOutBtnA;
    protected static String btn_ScheduledJobsA;
    protected static String btn_scheduleCodePlusA;
    protected static String btn_HireAgreementPlusA;
    protected static String txt_HANoA;
    protected static String btn_checkboxRelevantHireAgreementA;
    protected static String btn_RunButtonRelevantHireAgreementA;
    protected static String btn_LinkReleasedServiceInvoiceA;
    
    //Service_HA_028
    protected static String txt_ActualUpdatedAmountA;
    protected static String txt_ServiceInvoiceTotalHeaderA;
    
    //Service_HA_029
    protected static String txt_ActualUnit2A;
    protected static String txt_ActualUpdatedAmount2A;
    protected static String txt_ServiceInvoiceSubTotalA;
    protected static String btn_ServiceDetailsTabA;
    
    //Service_HA_030_031
    protected static String txt_RateRangeSetupEnd1A;
    protected static String txt_ResourceUnitPrice1A;
    protected static String txt_ResourceUnitPrice2A;
    protected static String txt_ReverceValidatorA;
    
    //Service_HA_032
    protected static String btn_HireAgreementPlusNewA;
    protected static String btn_RunButtonRelevantHireAgreementNewA;
    protected static String txt_LastBillDateA;

    //Service_HA_035_036_037
    protected static String btn_OkBtnA;
    protected static String txt_CloseValidatorA;
    protected static String btn_CloseAgreementBtnAfterServiceInvoiceA;
    protected static String txt_HireAgreementStatusByPageA;
    
    //Service_HA_038_039
    protected static String txt_BaseAmountA;

    //Service_HA_040
    protected static String txt_AgreementDateValidationEffectiveFromDateA;
    
    //Service_HA_041
    protected static String txt_EffectiveToA;
    protected static String txt_EffectiveToMonthA;
    protected static String txt_EffectiveToYearA;
    protected static String txt_EffectiveToDatePreA;
    
    //Service_HA_042
    protected static String txt_OverDueTxtA;
    
    //Service_HA_044
    protected static String btn_DuplicateHireAgreementA;
    protected static String txt_HireAgreementDuplicateValidatorA;
    
    //Service_HA_046
    protected static String txt_ResponsibleUserValidatorA;
    
    //Service_HA_049
    protected static String txt_ScheduleCodeA;
    protected static String txt_ScheduleCodeNoA;
    
    //Service_HA_051
    protected static String Sel_ReferenceCodeHA5SelA;
    
    //Service_HA_052
    protected static String txt_PreparerA;
    
    //Service_HA_054
    protected static String txt_CurrencyCboxA;
    protected static String txt_AgreedRateA;
    
    //Service_HA_058
    protected static String txt_PostBusinessUnitTxtA;
    protected static String sel_PostBusinessUnitSelA;
    protected static String txt_PostBusinessUnitTxtAfterReleaseA;
    
    //Service_HA_059
    protected static String btn_GeneralBtnA;
    protected static String btn_cboxConfigResourceA;
    protected static String btn_GeneralUpdateA;
    protected static String txt_ResourceCodeAndDescriptionA;
    
    //Service_HA_060
    protected static String sel_ResourceCodeHASel2A;
    protected static String txt_ResourceCodeA;

    //Service_HA_061
    protected static String sel_ResourceCodeHASel3A;
    protected static String txt_ResourceDescriptionA;
    
    //Service_EAR_001
    protected static String btn_EmployeeAdvanceRequestBtnA;
    
    //Service_EAR_002
    protected static String txt_EmployeeAdvanceRequestByPageA;
    
    //Service_EAR_003
    protected static String txt_DocNoLableA;
    protected static String txt_DocDateLableA;
    protected static String txt_EmployeeCodeLableA;
    protected static String txt_ExpenseTypeLableA;
    protected static String txt_PurposeLableA;
    protected static String txt_PostBusinessUnitCodeLableA;
    protected static String txt_PostBusinessUnitNameLableA;
    
    //Service_EAR_004
    protected static String btn_NewEmployeeAdvanceRequestBtnA;
    protected static String txt_NewEmployeeAdvanceRequestPageHeaderA;
    
    //Service_EAR_005
    protected static String txt_EmployeeTextFieldA;
    protected static String txt_ExpenseTypeTextFieldA;
    protected static String txt_PurposeTextFieldA;
    protected static String txt_AdditionalRemarkTextFieldA;
    protected static String txt_CurrencyTextFieldA;
    protected static String txt_AmountTextFieldA;
    
    //Service_EAR_006
    protected static String txt_ExpenseTypeTextFieldValidatorA;
    protected static String txt_PurposeTextFieldValidatorA;
    protected static String txt_AmountTextFieldValidatorA;
    
    //Service_EAR_007
    protected static String txt_PurposeTextFieldDraftValidatorA;
    protected static String txt_AmountTextFieldDraftValidatorA;
    
    //Service_EAR_008
    protected static String btn_EmployeeBtnEARA;
    protected static String txt_EmployeeSearchTextEARA;
    protected static String sel_EmployeeSearchSelEARA;
    
    //Service_EAR_009_010
    protected static String txt_EmployeeTextFieldAfterValueA;
    protected static String txt_ExpenseTypeTextFieldAfterValueA;
    protected static String txt_PurposeTextFieldAfterValueA;
    protected static String txt_AdditionalRemarkTextFieldAfterValueA;
    protected static String txt_CurrencyTextFieldAfterValueA;
    protected static String txt_AmountTextFieldAfterValueA;

    //Service_EAR_012
    protected static String txt_pageDeletedNewA;
    
    //Service_EAR_013
    protected static String txt_pageReversedNewA;
    protected static String btn_reverceApplyA;
    
    //Service_EAR_014_015_016_017
    protected static String btn_PayEmployeeAdvanceA;
    protected static String txt_NewOutboundPaymentAdvicePageHeaderA;
    protected static String txt_CustomerAccountOPAA;
    protected static String txt_ReferenceTypeOPAA;
    protected static String txt_RefDocNoOPAA;
    protected static String txt_DescriptionOPAA;
    protected static String txt_cboxCurrencyOPAA;
    protected static String txt_AmountOPAA;
    
    //Service_EAR_018_019_020
    protected static String txt_OutboundPaymentAdviceRecord1A;
    protected static String btn_ReferenceDetailsTabA;
    
    //Service_EAR_021_022_023_024_025
    protected static String btn_ConvertToOutboundPaymentBtnA;
    protected static String txt_NewOutboundPaymentPageHeaderA;
    protected static String txt_AmountOPA;
    protected static String btn_DetailsTabA;
    protected static String txt_PaidAmountA;
    protected static String txt_OutboundPaymentAdviceRecord2A;
    protected static String txt_BalanceAmountA;
    protected static String txt_DueAmountA;
    protected static String txt_PaidAmountEARA;
    protected static String btn_SummaryTabA;
    
    //Service_EAR_027
    protected static String btn_OutboundPaymentCheckBoxA;
    protected static String btn_OutboundPaymentRowErrorA;
    protected static String txt_PartialPaymentErrorA;
    
    //Service_EAR_028_029_030
    protected static String btn_OutboundPaymentAdviceRecordCheckboxA;
    protected static String btn_ViewAllPendingBtnA;
    protected static String txt_EmployeeAdvanceRequestSearchTxtA;
    protected static String btn_EmployeeAdvanceRequestSearchBtnA;
    protected static String sel_EmployeeAdvanceRequestSearchSelA;
    
    //Service_EAR_031to037
    protected static String btn_OutboundPaymentAdviceByPageHeaderA;
    protected static String txt_OutboundPaymentAdviceSearchTxtA;
    protected static String btn_OutboundPaymentAdviceSearchBtnA;
    protected static String sel_OutboundPaymentAdviceSearchSelA;
    protected static String txt_OutboundPaymentRecord1A;
    protected static String txt_OutboundPaymentRecord2A;
    protected static String txt_OutboundPaymentHeaderTotalA;

    //Service_DWS_001
    protected static String btn_DailyWorkSheetBtnA;
    
    //Service_DWS_002
    protected static String txt_DailyWorksheetByPageA;
    
    //Service_DWS_003
    protected static String txt_DailyWorkSheetNoColumnA;
    protected static String txt_ReferenceNoColumnA;;
    protected static String txt_DocDateColumnA;
    protected static String txt_CreatedUserColumnA;
    protected static String txt_CreatedDateColumnA;
    protected static String txt_PostedDateColumnA;
    protected static String txt_PostBusinessUnitColumnA;
    
    //Service_DWS_004_005_006
    protected static String btn_NewDailyWorksheetBtnA;
    protected static String txt_StartNewJourneyHeaderA;
    protected static String btn_DailyWorkSheetServiceBtnA;
    protected static String btn_DailyWorkSheetProjectBtnA;
    
    //Service_DWS_007to012
    protected static String txt_WorkedHoursServiceNewPageHeaderA;
    protected static String txt_JobNoFieldA;
    protected static String btn_AddNewRecordPlusA;
    protected static String txt_AddNewJobHeaderA;
    protected static String txt_JobNoStarA;
    protected static String txt_EmployeeStarA;
    protected static String btn_AddNewJobUpdateA;
    protected static String txt_JobNoValidatorA;
    protected static String btn_EmployeeBtnErrorA;
    protected static String txt_EmployeeIsRequiredErrorA;
    
    //Service_DWS_013to019
    protected static String txt_AddNewJobDateA;
    protected static String btn_ServiceJobOrderSearchBtnA;
    protected static String txt_ServiceJobOrderLookupHeaderA;
    protected static String txt_ReleseServiceJobOrderNosA;
    protected static String btn_ServiceJobOrderLookupSearchBtnA;
    protected static String txt_ServiceJobOrderLookupSearchTxtA;
    protected static String txt_JobNoTextFieldA;
    protected static String btn_TimeInA;
    protected static String sel_TimeInSelA;
    protected static String btn_TimeInSelOkA;
    protected static String btn_TimeOutA;
    protected static String sel_TimeOutSelA;
    protected static String btn_TimeOutSelOkA;
    
    //Service_DWS_020to025
    protected static String btn_EmployeeSearchBtnA;
    protected static String txt_EmployeeLookupHeaderA;
    protected static String btn_EmployeeLookupSearchBtnA;
    protected static String txt_ReleseEmployeeNamesA;
    protected static String txt_EmployeeLookupSearchTxtA;
    protected static String txt_EmployeeTextFieldDWA;
    protected static String txt_EmployeeTextFieldDWValuesA;
    
    //Service_DWS_026to028
    protected static String txt_DefineaValidRateProfileErrorA;
    protected static String btn_EmployeeDeleteBtnA;
    
    //Service_DWS_029to041
    protected static String btn_EmployeeAddPlusBtnA;
    protected static String txt_EmployeeAddedNewLineA;
    protected static String btn_JobDateA;
    protected static String txt_JobDatePickerA;
    protected static String sel_JobDateSelA;
    protected static String txt_TimeInTimePickerA;
    protected static String sel_TimeInSelectedA;
    protected static String txt_TimeInTextFieldA;
    protected static String txt_TimeOutTimePickerA;
    protected static String txt_WorkedHoursA;
    
    //Service_DWS_042to045
    protected static String txt_workedHoursValidatorA;
    protected static String txt_ReferenceNoTextFieldA;
    protected static String btn_UpdateAndNewDWSBtnA;
    protected static String btn_UpdateDWSBtnA;
    protected static String btn_CloseDWSBtnA;
    protected static String txt_ReferenceNoDWSTextA;
    protected static String txt_EmployeeNameDWSTextA;
    protected static String txt_JobNoDWSTextA;

    //Service_DWS_048to050
    protected static String btn_EmployeeSearchBtn2A;
    protected static String txt_EmployeeDuplicateErrorA;
    protected static String txt_EmployeeAllocatedErrorA;
    
    //Service_DWS_051to053
    protected static String btn_NewEmployeeBtnA;
    protected static String txt_EmployeeCodeA;
    protected static String txt_EmployeeNoA;
    protected static String txt_FullNameA;
    protected static String txt_NameWithInitialsA;
    protected static String txt_EmployeeGroupA;
    protected static String btn_WorkingCalendarSearchBtnA;
    protected static String txt_WorkingCalendarSearchTxtA;
    protected static String sel_WorkingCalendarSearchSelA;
    protected static String btn_RateProfileSearchBtnA;
    protected static String txt_RateProfileSearchTxtA;
    protected static String sel_RateProfileSearchSelA;
    protected static String btn_PayableAccountTickA;
    protected static String sel_TimeInSelNewA;
    protected static String sel_TimeOutSelNewA;
    protected static String txt_JobNoDWSTextNewA;
    protected static String txt_EmployeeNameDWSTextNewA;
    
    //Service_DWS_063to65
    protected static String btn_DeleteRecordRaw1A;
    protected static String btn_DeleteRecordRaw2A;
    protected static String btn_RecordErrorBtnA;
    protected static String txt_EmployeeNameIsRequiredErrorA;
    protected static String txt_JobDateIsRequiredErrorA;
    protected static String txt_JobNoIsRequiredErrorA;
    
    //Service_DWS_070to71
    protected static String btn_RemindersBtnA;
    protected static String btn_HistoryBtnA;;
    protected static String btn_ActivitiesBtnA;
    protected static String btn_JournalBtnA;
    
    //Service_DWS_077to078
    protected static String btn_ServiceJobOrderBtnA;
    protected static String txt_ServiceJobOrderSerchByPageTxtA;
    protected static String sel_ServiceJobOrderSerchByPageSelA;
    protected static String txt_DailyWorkSheetByPageTxtA;
    protected static String sel_DailyWorkSheetByPageSelA;
    protected static String btn_OverviewTabA;
    protected static String btn_LabourTabA;
    protected static String txt_LabourTabWorksheetNoA;
    

    //Service_DWS_080to082
    protected static String btn_HourlyRateBtnA;
    protected static String txt_WorkedHoursRecodeTxtA;
    protected static String txt_HourlyRateWorkedHoursRecodeTxtA;
    
    //Service_DWS_084
    protected static String txt_FirstPostDateA;
    protected static String txt_SecondPostDateA;
    
    //Service_DWS_087
    protected static String txt_PostDateHeaderA;
    protected static String txt_PostDateTextA;
    protected static String txt_PostDateYearTextA;
    protected static String sel_PostDateSelA;
    
    //Service_DWS_088to092
    protected static String btn_PostsBtnA;
    protected static String btn_TandCBtnA;
    protected static String btn_AttachmentsBtnA;
    protected static String btn_DailyWorkSheetHeaderBtnA;
    
    //Service_DWS_093to096
    protected static String txt_WorkedHoursProjectNewPageHeaderA;
    protected static String btn_ProjectSearchBtnA;
    protected static String txt_ProjectSearchTxtA;
    protected static String sel_ProjectSearchSelA;
    
    //Service_DWS_099
    protected static String btn_DWSProjectBtnA;
    protected static String txt_DWSProjectSearchByPageTxtA;
    protected static String sel_DWSProjectSearchByPageSelA;
    protected static String btn_DWSProjectOverviewTabA;
    protected static String txt_DWSProjectDWSNoA;
    
    //Service_DWS_100
    protected static String txt_DWSEmployeeNameLine1A;
    protected static String txt_DWSEmployeeNameLine2A;
    protected static String txt_DWSProNoLine1A;
    protected static String txt_DWSProNoLine2A;
    
    //Service_DWS_102
    protected static String btn_DWSNewProjectBtnA;
    protected static String txt_DWSProjectCodeA;
    protected static String txt_DWSProjectTitleA;
    protected static String txt_DWSProjectGroupA;
    protected static String btn_DWSResponsiblePersonBtnA;
    protected static String txt_DWSResponsiblePersonTxtA;
    protected static String sel_DWSResponsiblePersonSelA;
    protected static String txt_DWSAssignedBusinessUnitsA;
    protected static String btn_DWSPricingProfileBtnA;
    protected static String txt_DWSPricingProfileTxtA;
    protected static String sel_DWSPricingProfileSelA;
    protected static String btn_DWSCustomerAccountBtnA;
    protected static String txt_DWSCustomerAccountTxtA;
    protected static String sel_DWSCustomerAccountSelA;
    protected static String txt_DWScboxSalesUnitA;
    protected static String txt_DWScboxAccOwnerA;
    protected static String btn_DWSRequestedStartDateA;
    protected static String sel_DWSRequestedStartDateselA;
    protected static String btn_DWSProjectLocationBtnA;
    protected static String txt_DWSProjectLocationTxtA;
    protected static String sel_DWSProjectLocationSelA;
    protected static String btn_DWSWorkBreakdownBtnA;
    protected static String btn_DWSWorkBreakdownAddTaskBtnA;
    protected static String txt_DWSWorkBreakdownTaskNameA;
    protected static String btn_DWSTaskUpdateA;
    protected static String btn_DWSTaskdropdownA;
    protected static String btn_DWSReleaseTaskA;
    protected static String btn_DWSUpdateTaskPOCA;
    protected static String txt_DWScboxTaskPOCA;
    protected static String txt_DWStxtTaskPOCA;
    protected static String btn_DWSProjectCompleteBtnA;
    
	/* Reading the Data to variables */ 
       
    protected static String txt_UserNameDataService;
	protected static String txt_PasswordDataService;
	protected static String txt_ServiceJobOrderTitle;
	protected static String txt_ServiceJobOrderAccount;
	protected static String txt_ServiceGroup;
	protected static String txt_ServiceJobOrderDescription;
	protected static String  txt_Priority;
	protected static String txt_Template;
	protected static String txt_ServiceLocation;
	protected static String txt_Accountname;
	protected static String  txt_Labour;
	protected static String txt_taskktype;
	protected static String txt_SerialSpecificProduct;
	protected static String txt_EstimatedQty;
	protected static String txt_BatchSpecificProduct;
	protected static String  txt_FIFOProduct;
	protected static String txt_serviceProduct;
	protected static String txt_Resources;
	protected static String txt_tasktyperesource;
	protected static String txt_EstimatedQtyLabour;
	protected static String txt_EstimatedQtyResourse;
	protected static String txt_Resource;
	protected static String txt_EstimatedQtyserial;
	protected static String txt_EstimatedQtyBatch;
	protected static String txt_EstimatedQtyFIFO;
	protected static String txt_EstimatedQtyservice;
	protected static String txt_EstimatedQtysubcontract;
	protected static String txt_EstimatedQtyExpense;
	protected static String txt_InternalDispatchOrderShippingAddress;
	protected static String txt_CountofSerial;
	protected static String txt_SerialNo;
	protected static String txt_quty;
	protected static String txt_FifoActualQty;
	protected static String  txt_templateJOBNO;
	protected static String  txt_searchjobno;
	protected static String txt_searchemployee;
	protected static String txt_searchvendor;
	protected static String  txt_VendorRefrenceNo;
	protected static String txt_BillingAddress;
	protected static String txt_BillableQtyInputProduct;
	protected static String txt_SalesUnit;
	protected static String txt_BillableQtyInvoiceResource;
	protected static String txt_UnitPriceInputSerial;
	protected static String txt_UnitPriceSubContract;
	protected static String txt_UnitPriceService;
	protected static String txt_UnitPriceBatch;
	protected static String txt_UnitPriceFifo;
	protected static String  txt_UnitPriceLabour;
	protected static String version;
	protected static String txt_ToWareHouse;
	protected static String  txt_AnalysisCode;
	protected static String  txt_Amount;
	protected static String txt_Percentage;
	protected static String txt_CostAssignmentType;
	protected static String txt_Payee;
	protected static String  txt_PettyCashAccount;
	protected static String txt_PettyCashDescription;
	protected static String txt_Employee;
	protected static String txt_TypePaymentInformation;
	protected static String  txt_ExtendNarration;
	protected static String  txt_VendorReferenceNo;
	protected static String txt_bank;
	protected static String txt_bankaccountno;
	protected static String  txt_analysiscode;
	protected static String txt_AmountBankAdjustment;
	protected static String  txt_customer;
	protected static String txt_BillingAddressPurchaseInvoice;
	protected static String txt_Discount;
	protected static String txt_PercentagePurchaseInvoice;
	protected static String  txt_Costcenter;
	protected static String txt_searchjobnoOutboundLoanOrder;
	protected static String txt_NewSerialSpecificProduct;
	protected static String txt_NewEstimatedQtyserial;
	protected static String txt_SerialProductOutboundShipment;
	protected static String  txt_ServiceContractTitle;
	protected static String  txt_ContractGroup;
	protected static String  txt_employeecode;
	protected static String  txt_employeename;
	protected static String txt_namewithintials;
	protected static String txt_employeegroup;
	protected static String txt_Description;
	protected static String txt_owner;
	protected static String txt_SerialSpecificProduct1;
	protected static String txt_product1;
	protected static String  txt_SceduleCode;
	protected static String  txt_responsibleuser;
	protected static String txt_tagno;
	protected static String txt_EstimatedCost;
	protected static String  txt_pricingprofile;
	protected static String  txt_Referencecode;
	protected static String txt_lotProduct;
	protected static String  txt_BatchSerialProduct;
	protected static String txt_title;
	protected static String txt_PMCategory;
	protected static String  txt_PMcount;
	protected static String  txt_PMinterval;
	protected static String  txt_SceduleCodeFirstDayFM;
	protected static String   txt_currency;
	protected static String  txt_BillingAddress1;
	protected static String  txt_ShipingAddress1;
	protected static String  txt_salesunit;
	protected static String txt_unitprice;
	protected static String txt_AgreementNo;
	protected static String  txt_TitleAgreement;
	protected static String  txt_DescriptionHireAgreement;
	protected static String  txt_TitleNewCase;
	protected static String  txt_tagnoDirectSR;
	protected static String  serialTextValOLO;
	protected static String txt_Doctile;
	
    //Service_Case_001
	protected static String usernamedataA;
	protected static String passworddataA;
	  
	//Service_Case_004
	protected static String CustomerAccountTextA;

	//Service_Case_013
	protected static String CopyFromSerchtxtA;
	protected static String CopyFromSerchtitleA;
	protected static String OriginTextFieldA;
	protected static String ProductTextFieldA;
	protected static String TagNoTextFieldA;
	protected static String TypeTextFieldA;
	protected static String ServiceGroupTextFieldA;
	protected static String PriorityTextFieldA;

	
	//Service_Case_017
	protected static String InputProductA;
	protected static String LabourProductA;
	protected static String ResourceProductA;
	protected static String ExpenseProductA;
	protected static String SubContractProductA;
	protected static String ServiceProductA;
	protected static String SalesUnitA;
	protected static String ReferenceCodeLabourtxtA;
	protected static String ReferenceCodeResourcetxtA;
	protected static String ReferenceCodeExpenseA;

	
	//Service_Case_024
	protected static String OwnerTxtA;
	protected static String PricingProfileTxtA;
	protected static String ServiceLocationTxtA;
	
	//Service_Case_045_046
	protected static String ServiceContractTxtA;
	protected static String ServiceAgreementTxtA;
	
	//Service_Case_060
	protected static String CustomerAccountCodeAndDicriptionA;
	
	//Service_Case_061
	protected static String CustomerAccountCodeA;

	//Service_Case_062
	protected static String CustomerAccountDicriptionA;
	
	//Service_Case_063
	protected static String ProductCodeAndDicriptionA;
	
	//Service_Case_064
	protected static String ProductCodeA;
	
	//Service_Case_065
	protected static String ProductDicriptionA;
	
	//Service_HA_004
	protected static String ScheduleCodeHATxtA;
	protected static String ResponsibleUserHATxtA;
	protected static String ReferenceCodeHA1TxtA;
	
	//Service_HA_016
	protected static String ReferenceCodeHA2TxtA;
	
	//Service_HA_017
	protected static String HireAgreementNoA;
	
	//Service_HA_022
	protected static String ScheduleCodeHA2TxtA;
	
	//Service_HA_023
	protected static String ScheduleCodeHA3TxtA;
	
	//Service_HA_024
	protected static String ScheduleCodeHA4TxtA;
	
	//Service_HA_025
	protected static String ResponsibleUserHA2TxtA;
	protected static String NewUserNameA;
	protected static String NewPasswordA;
	
    //Service_HA_051
	protected static String ScheduleCodeHA5TxtA;
	
    //Service_HA_058
	protected static String PostBusinessUnitTxtA;
	protected static String PostBusinessUnitAfterReleaseTxtA;
	
    //Service_HA_059
	protected static String ResourceCodeAndDescriptionA;
    
    //Service_HA_060
	protected static String ResourceCodeA;
    
    //Service_HA_061
	protected static String ResourceDescriptionA;
	
	//Service_EAR_008
	protected static String EmployeeSearchTextEAR1A;
	
	//Service_EAR_009
	protected static String EmployeeSearchTextEAR2A;
	
	//Service_DWS_013to019
	protected static String ReleseServiceJobOrderNos1A;
	protected static String ReleseServiceJobOrderNos2A;
	protected static String ReleseServiceJobOrderNos3A;
	protected static String RevercedServiceJobOrderNos1A;
	protected static String RevercedServiceJobOrderNos2A;
	protected static String RevercedServiceJobOrderNos3A;
	protected static String DeletedServiceJobOrderNos1A;
	protected static String DeletedServiceJobOrderNos2A;
	protected static String DeletedServiceJobOrderNos3A;
	
	//Service_DWS_020to025
	protected static String ReleseEmployeeNames1A;
	protected static String ReleseEmployeeNames2A;
	protected static String InactiveEmployeeNames1A;
	protected static String InactiveEmployeeNames2A;

	//Service_DWS_093to096
	protected static String ReleseProjectTxt1A;
	
	//Service_DWS_100
	protected static String ReleseProjectTxt2A;
	
	//Service_DWS_102
	protected static String DWSPricingProfileSelA;
	protected static String DWSProjectGroupA;
	protected static String DWSAssignedBusinessUnitsA;
	
	//Calling the constructor

	
	public static void readElementlocators() throws Exception
	{
		
		
		siteLogo= findElementInXLSheet(getParameterService,"siteLogo"); 
		username= findElementInXLSheet(getParameterService,"username"); 
		password= findElementInXLSheet(getParameterService,"password"); 
		btn_login= findElementInXLSheet(getParameterService,"login button"); 
		lnk_home=findElementInXLSheet(getParameterService,"lnk_home");
		btn_navigationmenu= findElementInXLSheet(getParameterService, "btn_navigation");
		sidemenu= findElementInXLSheet(getParameterService,"sidemenu");
		btn_servicemodule= findElementInXLSheet(getParameterService,"btn_servicemodule");
		subsidemenu= findElementInXLSheet(getParameterService,"subsidemenu");
		btn_ServiceJobOrder= findElementInXLSheet(getParameterService,"btn_ServiceJobOrder");
		ServiceJobOrderPage= findElementInXLSheet(getParameterService,"ServiceJobOrderPage");
		btn_NewServiceJobOrder= findElementInXLSheet(getParameterService,"btn_NewServiceJobOrder");
		StartNewJourneyPage= findElementInXLSheet(getParameterService,"StartNewJourneyPage");
		btn_ServiceOrdertoInternalDispatchOrderExternal= findElementInXLSheet(getParameterService,"btn_Service(External)");
		NewServiceJobOrderPage= findElementInXLSheet(getParameterService,"NewServiceJobOrderPage");
		ServiceJobOrderTitle= findElementInXLSheet(getParameterService,"ServiceJobOrderTitle");
		ServiceJobOrderAccount= findElementInXLSheet(getParameterService,"ServiceJobOrderAccount");
	    ServiceGroup= findElementInXLSheet(getParameterService,"ServiceGroup");
	    ServiceJobOrderDescription= findElementInXLSheet(getParameterService,"ServiceJobOrderDescription");
	    Priority= findElementInXLSheet(getParameterService,"Priority");
	    btn_searchservicelocation= findElementInXLSheet(getParameterService,"btn_searchservicelocation");
	    template= findElementInXLSheet(getParameterService,"template");
	    ServiceLocation= findElementInXLSheet(getParameterService,"ServiceLocation");
	    btn_location= findElementInXLSheet(getParameterService,"btn_location");
	    btn_PricingProfileSearch= findElementInXLSheet(getParameterService,"btn_PricingProfileSearch");
	    PricingProfileLookUpPage= findElementInXLSheet(getParameterService,"PricingProfileLookUpPage");
	    btn_PricingProfile= findElementInXLSheet(getParameterService,"btn_PricingProfile");
	    btn_accountsearch= findElementInXLSheet(getParameterService,"btn_accountsearch");
	    AccountLookUPPage= findElementInXLSheet(getParameterService,"AccountLookUPPage");
	    Accountname= findElementInXLSheet(getParameterService,"Accountname");
	    btn_accountuser= findElementInXLSheet(getParameterService,"btn_accountuser");
	    btn_searchowner= findElementInXLSheet(getParameterService,"btn_searchowner");
	    OwnerLookUpPage= findElementInXLSheet(getParameterService,"OwnerLookUpPage");
	    btn_owner= findElementInXLSheet(getParameterService,"btn_owner");
	    btn_taskdetails= findElementInXLSheet(getParameterService,"btn_taskdetails");
	    TaskDetailsTab= findElementInXLSheet(getParameterService,"TaskDetailsTab");
	    btn_searchserialproduct= findElementInXLSheet(getParameterService,"btn_searchserialproduct");
	    ProductLookUpPage= findElementInXLSheet(getParameterService,"ProductLookUpPage");
	    Product= findElementInXLSheet(getParameterService,"Product");
	    btn_product= findElementInXLSheet(getParameterService,"btn_product");
	    btn_expand= findElementInXLSheet(getParameterService,"btn_expand");
	    tasktype= findElementInXLSheet(getParameterService,"tasktype");
	   div= findElementInXLSheet(getParameterService,"div");
	   EstimatedQty= findElementInXLSheet(getParameterService,"EstimatedQty");
	   btn_searchbatchproduct= findElementInXLSheet(getParameterService,"btn_searchbatchproduct");
	   btn_expand2= findElementInXLSheet(getParameterService,"btn_expand2");
	   btn_expand3= findElementInXLSheet(getParameterService,"btn_expand3");
	   btn_searchFIFOproduct= findElementInXLSheet(getParameterService,"btn_searchFIFOproduct");
	   btn_expand4= findElementInXLSheet(getParameterService,"btn_expand4");
	   btn_searchserviceproduct= findElementInXLSheet(getParameterService,"btn_searchserviceproduct");
	   btn_expand5= findElementInXLSheet(getParameterService,"btn_expand5");
	   btn_searchResources= findElementInXLSheet(getParameterService,"btn_searchResources");
	   tasktyperesource= findElementInXLSheet(getParameterService,"tasktyperesource");
	   btn_searchlabour= findElementInXLSheet(getParameterService,"btn_searchlabour");
	   tasktypelabour= findElementInXLSheet(getParameterService,"tasktypelabour");
	   btn_expand6= findElementInXLSheet(getParameterService,"btn_expand6");
	   btn_searchsubcontract= findElementInXLSheet(getParameterService,"btn_searchsubcontract");
	   tasktypesubcontract= findElementInXLSheet(getParameterService,"tasktypesubcontract");
	   btn_expand7= findElementInXLSheet(getParameterService,"btn_expand7");
	   tasktypeexpense= findElementInXLSheet(getParameterService,"tasktypeexpense");
	   btn_searchexpense= findElementInXLSheet(getParameterService,"btn_searchexpense");
	   btn_expand8= findElementInXLSheet(getParameterService,"btn_expand8");
	   btn_DraftServiceJobOrder= findElementInXLSheet(getParameterService,"btn_DraftServiceJobOrder");
	   btn_ReleaseServiceJobOrder= findElementInXLSheet(getParameterService,"btn_ReleaseServiceJobOrder");
	   EstimatedQtyLabour= findElementInXLSheet(getParameterService,"EstimatedQtyLabour");
	   EstimatedQtyResourse= findElementInXLSheet(getParameterService,"EstimatedQtyResourse");
	   btn_searchlabouremployee= findElementInXLSheet(getParameterService,"btn_searchlabouremployee");
	   LabourOwnerLookUpPage= findElementInXLSheet(getParameterService,"LabourOwnerLookUpPage");
	   LabourEmployee= findElementInXLSheet(getParameterService,"LabourEmployee");
	   btn_searchResourcesReferenceCode= findElementInXLSheet(getParameterService,"btn_searchResourcesReferenceCode");
	   ResourceLookUpPage= findElementInXLSheet(getParameterService,"ResourceLookUpPage");
	   Resource= findElementInXLSheet(getParameterService,"Resource");
	   btn_Resource= findElementInXLSheet(getParameterService,"btn_Resource");
	   btn_LabourEmployee= findElementInXLSheet(getParameterService,"btn_LabourEmployee");
	   DraftServiceJobOrderPage= findElementInXLSheet(getParameterService,"DraftServiceJobOrderPage");
	   ReleaseServiceJobOrderPage= findElementInXLSheet(getParameterService,"ReleaseServiceJobOrderPage");
	   EstimatedQtyserial= findElementInXLSheet(getParameterService,"EstimatedQtyserial");
	   EstimatedBatch= findElementInXLSheet(getParameterService,"EstimatedBatch");
	   EstimatedQtyFIFO= findElementInXLSheet(getParameterService,"EstimatedQtyFIFO");
	   EstimatedQtyservice= findElementInXLSheet(getParameterService,"EstimatedQtyservice");
	   EstimatedQtysubcontract= findElementInXLSheet(getParameterService,"EstimatedQtysubcontract");
	   EstimatedQtyExpense= findElementInXLSheet(getParameterService,"EstimatedQtyExpense");
	   btn_action= findElementInXLSheet(getParameterService,"btn_action");
	   btn_CreateInternalOrder= findElementInXLSheet(getParameterService,"btn_CreateInternalOrder");
	   GernerateInternalOrderPage= findElementInXLSheet(getParameterService,"GernerateInternalOrderPage");
	   btn_selectall= findElementInXLSheet(getParameterService,"btn_selectall");
	   btn_apply= findElementInXLSheet(getParameterService,"btn_apply");
	   btn_applyCommon= findElementInXLSheet(getParameterService,"btn_applyCommon");
	   btn_joborder= findElementInXLSheet(getParameterService,"btn_joborder");
	   NewInternalOrderPage= findElementInXLSheet(getParameterService,"NewInternalOrderPage");
	   btn_draftInternalNewOrder= findElementInXLSheet(getParameterService,"btn_draftInternalNewOrder");
	   btn_releaseInternalNewOrder= findElementInXLSheet(getParameterService,"btn_releaseInternalNewOrder");
	   InternalOrderInformationPopUp= findElementInXLSheet(getParameterService,"InternalOrderInformationPopUp");
	   btn_gotopage= findElementInXLSheet(getParameterService,"btn_gotopage");
	   InternalDispatchOrderPage= findElementInXLSheet(getParameterService,"InternalDispatchOrderPage");
	   InternalDispatchOrderShippingAddress= findElementInXLSheet(getParameterService,"InternalDispatchOrderShippingAddress");
	   btn_DraftInternalDispatchOrder= findElementInXLSheet(getParameterService,"btn_DraftInternalDispatchOrder");
	   btn_SerialBatchInternalDispatchOrder= findElementInXLSheet(getParameterService,"btn_SerialBatchInternalDispatchOrder");
	   btn_ReleaseInternalDispatchOrder= findElementInXLSheet(getParameterService,"btn_ReleaseInternalDispatchOrder");
	     btn_ok= findElementInXLSheet(getParameterService,"btn_ok");
	     btn_DraftIO= findElementInXLSheet(getParameterService,"btn_DraftIO");
	     SerialorBatchnoPage= findElementInXLSheet(getParameterService,"SerialorBatchnoPage");
	     btn_product1= findElementInXLSheet(getParameterService,"btn_product1");
	     btn_range= findElementInXLSheet(getParameterService,"btn_range");
	     CountofSerial= findElementInXLSheet(getParameterService,"CountofSerial");
	     SerialNo= findElementInXLSheet(getParameterService,"SerialNo");
	     btn_refresh= findElementInXLSheet(getParameterService,"btn_refresh");
	     btn_product2= findElementInXLSheet(getParameterService,"btn_product2");
	     btn_alterslide= findElementInXLSheet(getParameterService,"btn_alterslide");
	     ExistingSerial_BatchNosPage= findElementInXLSheet(getParameterService,"ExistingSerial_BatchNosPage");
	     btn_addbatchproduct= findElementInXLSheet(getParameterService,"btn_addbatchproduct");
	     btn_arrowtointernaldistbatchorder= findElementInXLSheet(getParameterService,"btn_arrowtointernaldistbatchorder");
	     btn_releaseinternaldispatchorder= findElementInXLSheet(getParameterService,"btn_releaseinternaldispatchorder");
	     btn_overview= findElementInXLSheet(getParameterService,"btn_overview");
	     btn_actualUpdateConfiamation= findElementInXLSheet(getParameterService,"btn_actualUpdateConfiamation");
	     btn_ActualUpdate= findElementInXLSheet(getParameterService,"btn_ActualUpdate");
	     actualupdatepage= findElementInXLSheet(getParameterService,"actualupdatepage");
	     btn_OpenSerialNoSelection= findElementInXLSheet(getParameterService,"btn_OpenSerialNoSelection");
	     InputProductDetailsPopUp= findElementInXLSheet(getParameterService,"InputProductDetailsPopUp");
	     btn_serialproduct= findElementInXLSheet(getParameterService,"btn_serialproduct");
	     btn_update= findElementInXLSheet(getParameterService,"btn_update");
	     btn_OpenBatchNoSelection= findElementInXLSheet(getParameterService,"btn_OpenBatchNoSelection");
	     btn_NoOfSerialProduct= findElementInXLSheet(getParameterService,"btn_NoOfSerialProduct");
	     availableqty= findElementInXLSheet(getParameterService,"availableqty");
	     btn_close= findElementInXLSheet(getParameterService,"btn_close");
	     FifoActualQty= findElementInXLSheet(getParameterService,"FifoActualQty");
	     btn_updateinputproducts= findElementInXLSheet(getParameterService,"btn_updateinputproducts");
	     btn_closeInputProductDetails= findElementInXLSheet(getParameterService,"btn_closeInputProductDetails");
	     btn_CostingSummaary= findElementInXLSheet(getParameterService,"btn_CostingSummaary");
	     CostingSummaryPage= findElementInXLSheet(getParameterService,"btn_CostingSummaryPage");
	     btn_material= findElementInXLSheet(getParameterService,"btn_material");
	     ActualUpdatePage= findElementInXLSheet(getParameterService,"ActualUpdatePage");
	     btn_resource= findElementInXLSheet(getParameterService,"btn_resource");
	     btn_actualduration= findElementInXLSheet(getParameterService,"btn_actualduration");
	     btn_okActualDuration= findElementInXLSheet(getParameterService,"btn_okActualDuration");
	     btn_date= findElementInXLSheet(getParameterService,"btn_date");
	     btn_ResourceOverview= findElementInXLSheet(getParameterService,"btn_ResourceOverview");
	     btn_updateActualDuration= findElementInXLSheet(getParameterService,"btn_updateActualDuration");
	     ActualWorkpage= findElementInXLSheet(getParameterService,"ActualWorkpage");
	     btn_ResourseCost= findElementInXLSheet(getParameterService,"btn_ResourseCost");
	     btn_DailyWorkSheet= findElementInXLSheet(getParameterService,"btn_DailyWorkSheet");
	     DailyWorkSheetPage= findElementInXLSheet(getParameterService,"DailyWorkSheetPage");
	     btn_NewDailyWorkSheet= findElementInXLSheet(getParameterService,"btn_NewDailyWorkSheet");
	     StartNewJourneyForm= findElementInXLSheet(getParameterService,"StartNewJourneyForm");
	     btn_DailyWorkSheetServiceJourney= findElementInXLSheet(getParameterService,"btn_DailyWorkSheetServiceJourney");
	     btn_addnewrecord= findElementInXLSheet(getParameterService,"btn_addnewrecord");
	     btn_SearchJobNo= findElementInXLSheet(getParameterService,"btn_SearchJobNo");
	     templateJOBNO= findElementInXLSheet(getParameterService,"templateJOBNO");
	     searchjobno= findElementInXLSheet(getParameterService,"searchjobno");
	     btn_jobno= findElementInXLSheet(getParameterService,"btn_jobno");
	     btn_searchemployee= findElementInXLSheet(getParameterService,"btn_searchemployee");
	     searchemployee= findElementInXLSheet(getParameterService,"searchemployee");
	     btn_employee= findElementInXLSheet(getParameterService,"btn_employee");
	     btn_timein= findElementInXLSheet(getParameterService,"btn_timein");
	     timein= findElementInXLSheet(getParameterService,"timein");
	     btn_oktime= findElementInXLSheet(getParameterService,"btn_oktime");
	     btn_timeout= findElementInXLSheet(getParameterService,"btn_timeout");
	     timeout= findElementInXLSheet(getParameterService,"timeout");
	     btn_oktimeout= findElementInXLSheet(getParameterService,"btn_oktimeout");
	     btn_updateNewJob= findElementInXLSheet(getParameterService,"btn_updateNewJob");
	     btn_WorkedHrsDraft= findElementInXLSheet(getParameterService,"btn_WorkedHrsDraft");
	     btn_WorkedHrsRelease= findElementInXLSheet(getParameterService,"btn_WorkedHrsRelease");
	     ReleasedPage= findElementInXLSheet(getParameterService,"ReleasedPage");
	     btn_jobnoServiceJobOrder= findElementInXLSheet(getParameterService,"btn_jobnoServiceJobOrder");
	     OverviewTabPage= findElementInXLSheet(getParameterService,"OverviewTabPage");
	     btn_labour= findElementInXLSheet(getParameterService,"btn_labour");
	     LabourTabPage= findElementInXLSheet(getParameterService,"LabourTabPage");
//	     btn_CostingSummary2= findElementInXLSheet(getParameterService,"btn_CostingSummary2");
	     btn_hour= findElementInXLSheet(getParameterService,"btn_hour");
	     EstimatedValuesubcontract= findElementInXLSheet(getParameterService,"EstimatedValuesubcontract");
	     btn_SubContractOrder= findElementInXLSheet(getParameterService,"btn_SubContractOrder");
	     SubContractOrderPage= findElementInXLSheet(getParameterService,"SubContractOrderPage");
	     btn_searchvendor= findElementInXLSheet(getParameterService,"btn_searchvendor");
	     searchvendor= findElementInXLSheet(getParameterService,"searchvendor");
	     btn_vendor= findElementInXLSheet(getParameterService,"btn_vendor");
	     btn_checkboxsubcontractorder= findElementInXLSheet(getParameterService,"btn_checkboxsubcontractorder");
	     NewPurchaseOrderPage= findElementInXLSheet(getParameterService,"NewPurchaseOrderPage");
	     btn_CheckOutPurchaseOrder= findElementInXLSheet(getParameterService,"btn_CheckOutPurchaseOrder");
	     VendorRefrenceNo= findElementInXLSheet(getParameterService,"VendorRefrenceNo");
	     btn_SearchBillingAddress= findElementInXLSheet(getParameterService,"btn_SearchBillingAddress");
	     BillingAddress= findElementInXLSheet(getParameterService,"BillingAddress");
	     btn_applyBillingAddress= findElementInXLSheet(getParameterService,"btn_applyBillingAddress");
	     btn_DraftPurchaseOrder= findElementInXLSheet(getParameterService,"btn_DraftPurchaseOrder");
	     DraftPurchaseOrderPage= findElementInXLSheet(getParameterService,"DraftPurchaseOrderPage");
	     btn_ReleasePurchaseOrder= findElementInXLSheet(getParameterService,"btn_ReleasePurchaseOrder");
	     btn_gotopagePurchaseOrder= findElementInXLSheet(getParameterService,"btn_gotopagePurchaseOrder");
	     NewPurchaseInvoice= findElementInXLSheet(getParameterService,"NewPurchaseInvoice");
	     btn_DraftPurchaseInvoice= findElementInXLSheet(getParameterService,"btn_DraftPurchaseInvoice");
	     btn_checkoutpurchaseinvoice= findElementInXLSheet(getParameterService,"btn_checkoutpurchaseinvoice");
	     btn_ReleasePurchaseInvoice1= findElementInXLSheet(getParameterService,"btn_ReleasePurchaseInvoice");
	     btn_SubContractServices= findElementInXLSheet(getParameterService,"btn_SubContractServices");
	     btn_PurchaseOrderLink= findElementInXLSheet(getParameterService,"btn_PurchaseOrderLink");
	     PurchaseOrderPage= findElementInXLSheet(getParameterService,"PurchaseOrderPage");
	     btn_PurchaseInvoiceLink= findElementInXLSheet(getParameterService,"btn_PurchaseInvoiceLink");
	     PurchaseInvoicePage= findElementInXLSheet(getParameterService,"PurchaseInvoicePage");
	     btn_CreateInvoiceProposal= findElementInXLSheet(getParameterService,"btn_CreateInvoiceProposal");
	     InvoiceProposalPopup= findElementInXLSheet(getParameterService,"InvoiceProposalPopup");
	     btn_InputproductCreateInvoiceProposal= findElementInXLSheet(getParameterService,"btn_InputproductCreateInvoiceProposal");
	     btn_ResourceCreateInvoiceProposal= findElementInXLSheet(getParameterService,"btn_ResourceCreateInvoiceProposal");
	     btn_SubcontractCreateInvoiceProposal= findElementInXLSheet(getParameterService,"btn_SubcontractCreateInvoiceProposal");
	     btn_OtherServiceCreateInvoiceProposal= findElementInXLSheet(getParameterService,"btn_OtherServiceCreateInvoiceProposal");
	     btn_checkoutCreateInvoiceProposal= findElementInXLSheet(getParameterService,"btn_checkoutCreateInvoiceProposal");
	     btn_applyCreateInvoiceProposal= findElementInXLSheet(getParameterService,"btn_applyCreateInvoiceProposal");
	     NewInvoiceProposalPage= findElementInXLSheet(getParameterService,"NewInvoiceProposalPage");
	     btn_checkoutInvoiceProposal= findElementInXLSheet(getParameterService,"btn_checkoutInvoiceProposal");
	     btn_DraftInvoiceProposal= findElementInXLSheet(getParameterService,"btn_DraftInvoiceProposal");
	     btn_LabourCreateInvoiceProposal= findElementInXLSheet(getParameterService,"btn_LabourCreateInvoiceProposal");
	     BillableQtyInputProduct= findElementInXLSheet(getParameterService,"BillableQtyInputProduct");
	     SalesUnit= findElementInXLSheet(getParameterService,"SalesUnit");
	     DraftInvoiceProposalPage= findElementInXLSheet(getParameterService,"DraftInvoiceProposalPage");
	     btn_ReleaseInvoiceProposal= findElementInXLSheet(getParameterService,"btn_ReleaseInvoiceProposal");
	     BillableQtyInvoiceResource= findElementInXLSheet(getParameterService,"BillableQtyInvoiceResource");
	     btn_GoToPageInvoiceProposal= findElementInXLSheet(getParameterService,"btn_GoToPageInvoiceProposal");
	     btn_ServiceDetails= findElementInXLSheet(getParameterService,"btn_ServiceDetails");
	     btn_checkoutServiceInvoice= findElementInXLSheet(getParameterService,"btn_checkoutServiceInvoice");
	     btn_DraftServiceInvoice= findElementInXLSheet(getParameterService,"btn_DraftServiceInvoice");
	     btn_ReleaseServiceInvoice= findElementInXLSheet(getParameterService,"btn_ReleaseServiceInvoice");
	     DraftServiceInvoice= findElementInXLSheet(getParameterService,"DraftServiceInvoice");
	     btn_CreateServiceQuatation= findElementInXLSheet(getParameterService,"btn_CreateServiceQuatation");
	     NewServiceQuatationPage= findElementInXLSheet(getParameterService,"NewServiceQuatationPage");
	     btn_ServiceDetailsServiceQuatation= findElementInXLSheet(getParameterService,"btn_ServiceDetailsServiceQuatation");
	     btn_checkoutServiceQuatation= findElementInXLSheet(getParameterService,"btn_checkoutServiceQuatation");
	     btn_DraftServiceQuatation= findElementInXLSheet(getParameterService,"btn_DraftServiceQuatation");
	     UnitPriceInputSerial= findElementInXLSheet(getParameterService,"UnitPriceInputSerial");
	     UnitPriceSubContract= findElementInXLSheet(getParameterService,"UnitPriceSubContract");
	     UnitPriceService= findElementInXLSheet(getParameterService,"UnitPriceService");
	     UnitPriceBatch= findElementInXLSheet(getParameterService,"UnitPriceBatch");
	     UnitPriceFifo= findElementInXLSheet(getParameterService,"UnitPriceFifo");
	     UnitPriceLabour= findElementInXLSheet(getParameterService,"UnitPriceLabour");
	     btn_ReleaseServiceQuatation= findElementInXLSheet(getParameterService,"btn_ReleaseServiceQuatation");
	     DraftServiceQuatationPage= findElementInXLSheet(getParameterService,"DraftServiceQuatationPage");
	     btn_version= findElementInXLSheet(getParameterService,"btn_version");
	     btn_tick= findElementInXLSheet(getParameterService,"btn_tick");
	     ConfirmQuatationPopUp= findElementInXLSheet(getParameterService,"ConfirmQuatationPopUp");
	     btn_confirm= findElementInXLSheet(getParameterService,"btn_confirm");
	     version= findElementInXLSheet(getParameterService,"version");
	     btn_gotopageServiceQuatation= findElementInXLSheet(getParameterService,"btn_gotopageServiceQuatation");
	     btn_draftServiceOrder= findElementInXLSheet(getParameterService,"btn_draftServiceOrder");
	     btn_ReleaseServiceOrder= findElementInXLSheet(getParameterService,"btn_ReleaseServiceOrder");
	     btn_checkoutServiceOrder= findElementInXLSheet(getParameterService,"btn_checkoutServiceOrder");
	     btn_ServiceDetailsServiceOrder= findElementInXLSheet(getParameterService,"btn_ServiceDetailsServiceOrder");
	     btn_QuotationDetails= findElementInXLSheet(getParameterService,"btn_QuotationDetails");
	     btn_InternalReturnOrder= findElementInXLSheet(getParameterService,"btn_InternalReturnOrder");
	     btn_SearchInternalReturnOrder= findElementInXLSheet(getParameterService,"btn_SearchInternalReturnOrder");
	     btn_productreturn= findElementInXLSheet(getParameterService,"btn_productreturn");
	     btn_applytoreturn= findElementInXLSheet(getParameterService,"btn_applytoreturn");
	     ToWareHouse= findElementInXLSheet(getParameterService,"ToWareHouse");
	     btn_DraftInternalReturnOrder= findElementInXLSheet(getParameterService,"btn_DraftInternalReturnOrder");
	     DraftInternalReturnOrderPage= findElementInXLSheet(getParameterService,"DraftInternalReturnOrderPage");
	     btn_SerialbatchInternalReturnOrder= findElementInXLSheet(getParameterService,"btn_Serialbatch1");
	     btn_serialproducttocapture= findElementInXLSheet(getParameterService,"btn_serialproducttocapture");
	     btn_updateInternalReturnOrder= findElementInXLSheet(getParameterService,"btn_updateInternalReturnOrder");
	     btn_backInternalReturnOrder= findElementInXLSheet(getParameterService,"btn_backInternalReturnOrder");
	     btn_ReleaseInternalReturnOrder= findElementInXLSheet(getParameterService,"btn_ReleaseInternalReturnOrder");
	     btn_GoToPageInternalReturnOrder= findElementInXLSheet(getParameterService,"btn_GoToPageInternalReturnOrder");
	     NewInternalReceiptPage= findElementInXLSheet(getParameterService,"NewInternalReceiptPage");
	     btn_Draftinternalreceipt= findElementInXLSheet(getParameterService,"btn_Draftinternalreceipt1");
	     btn_Releaseinternalreceipt= findElementInXLSheet(getParameterService,"btn_Releaseinternalreceipt");
	     btn_Finance= findElementInXLSheet(getParameterService,"btn_Finance");
	     FinanceSubSideMenu= findElementInXLSheet(getParameterService,"FinanceSubSideMenu");
	     btn_OutboundPaymentAdvice= findElementInXLSheet(getParameterService,"btn_OutboundPaymentAdvice");
	     NewOutboundPaymentAdvicePage= findElementInXLSheet(getParameterService,"NewOutboundPaymentAdvicePage");
	     btn_NewOutboundPaymentAdvice= findElementInXLSheet(getParameterService,"btn_NewOutboundPaymentAdvice");
	     btn_PaymentVoucherJourney= findElementInXLSheet(getParameterService,"btn_PaymentVoucherJourney");
	     AnalysisCode= findElementInXLSheet(getParameterService,"AnalysisCode1");
	     Amount= findElementInXLSheet(getParameterService,"Amount");
	     btn_downarrow= findElementInXLSheet(getParameterService,"btn_downarrow");
	     btn_CostAllocation= findElementInXLSheet(getParameterService,"btn_CostAllocation");
	     CostAllocationPopUp= findElementInXLSheet(getParameterService,"CostAllocationPopUp");
	     Percentage= findElementInXLSheet(getParameterService,"Percentage");
	     CostAssignmentType= findElementInXLSheet(getParameterService,"CostAssignmentType");
	     btn_SearchCostObject= findElementInXLSheet(getParameterService,"btn_SearchCostObject");
	     btn_AddRecordCostAllocation= findElementInXLSheet(getParameterService,"btn_AddRecordCostAllocation");
	     btn_updateCostAllocation_Serice_TC_012= findElementInXLSheet(getParameterService,"btn_updateCostAllocation_Serice_TC_012");
	     btn_UpdateCostAllocation= findElementInXLSheet(getParameterService,"btn_UpdateCostAllocation");
	     btn_jobnoServiceOrderLookup= findElementInXLSheet(getParameterService,"btn_jobnoServiceOrderLookup");
	     btn_checkoutCostAllocation= findElementInXLSheet(getParameterService,"btn_checkoutCostAllocation");
	     btn_DraftOutboundPaymentAdvice= findElementInXLSheet(getParameterService,"btn_DraftOutboundPaymentAdvice");
	     DraftOutboundPaymentAdvicePage= findElementInXLSheet(getParameterService,"DraftOutboundPaymentAdvicePage");
	     btn_ReleaseOutboundPaymentAdvicePage= findElementInXLSheet(getParameterService,"btn_ReleaseOutboundPaymentAdvicePage");
	     btn_ActionOutboundPaymentAdvice= findElementInXLSheet(getParameterService,"btn_ActionOutboundPaymentAdvice1");
	     btn_ConverttoOutboundPayment= findElementInXLSheet(getParameterService,"btn_ConverttoOutboundPayment");
	     Payee= findElementInXLSheet(getParameterService,"Payee");
	     btn_OutboundPaymentDetails= findElementInXLSheet(getParameterService,"btn_OutboundPaymentDetails");
	     btn_CheckoutOutboundPaymentDetails= findElementInXLSheet(getParameterService,"btn_CheckoutOutboundPaymentDetails");
	     btn_DraftPaymentDetails= findElementInXLSheet(getParameterService,"btn_DraftPaymentDetails");
	     DraftOutboundPaymentPage= findElementInXLSheet(getParameterService,"DraftOutboundPaymentPage");
	     btnReleasePaymentDetails= findElementInXLSheet(getParameterService,"btnReleasePaymentDetails");
	     btn_expenses= findElementInXLSheet(getParameterService,"btn_expenses");
	     btn_Expense= findElementInXLSheet(getParameterService,"btn_Expense");
	     btn_PettyCash= findElementInXLSheet(getParameterService,"btn_PettyCash");
	     PettyCashPage= findElementInXLSheet(getParameterService,"PettyCashPage");
	     btn_NewPettyCash= findElementInXLSheet(getParameterService,"btn_NewPettyCash");
	     PettyCashAccount= findElementInXLSheet(getParameterService,"PettyCashAccount");
	     PettyCashDescription= findElementInXLSheet(getParameterService,"PettyCashDescription");
	     btn_paidtoemployee= findElementInXLSheet(getParameterService,"btn_paidtoemployee");
	     Employee= findElementInXLSheet(getParameterService,"Employee");
	     TypePaymentInformation= findElementInXLSheet(getParameterService,"TypePaymentInformation");
	     btn_CostAllocation1= findElementInXLSheet(getParameterService,"btn_CostAllocation1");
	     btn_updateCostAllocation_Service_TC_011= findElementInXLSheet(getParameterService,"btn_updateCostAllocation_Service_TC_011");
	     btn_UpdateCostAllocation1= findElementInXLSheet(getParameterService,"btn_UpdateCostAllocation1");
	     btn_DraftPettyCash= findElementInXLSheet(getParameterService,"btn_DraftPettyCash");
	     btn_narration= findElementInXLSheet(getParameterService,"btn_narration");
	     ExtendNarration= findElementInXLSheet(getParameterService,"ExtendNarration");
	     btn_narrationapply= findElementInXLSheet(getParameterService,"btn_narrationapply");
	     btn_ReleasePettyCash= findElementInXLSheet(getParameterService,"btn_ReleasePettyCash");
	     OutboundPaymentAdvicePage= findElementInXLSheet(getParameterService,"OutboundPaymentAdvicePage");
	     btn_AccuralVoucher= findElementInXLSheet(getParameterService,"btn_AccuralVoucher");
	     btn_searchvendorAcrualVoucher= findElementInXLSheet(getParameterService,"btn_searchvendorAcrualVoucher");
	     btn_CheckoutOutboundPaymentAdvice= findElementInXLSheet(getParameterService,"btn_CheckoutOutboundPaymentAdvice");
	     VendorReferenceNo= findElementInXLSheet(getParameterService,"VendorReferenceNo");
	     btn_navigationmenu1= findElementInXLSheet(getParameterService,"btn_navigationmenu1");
	     btn_BankAdjustment= findElementInXLSheet(getParameterService,"btn_BankAdjustment");
	     BankAdjustmentPage= findElementInXLSheet(getParameterService,"BankAdjustmentPage");
	     btn_NewBankAdjustment= findElementInXLSheet(getParameterService,"btn_NewBankAdjustment");
	     bank= findElementInXLSheet(getParameterService,"bank");
	     bankaccountno= findElementInXLSheet(getParameterService,"bankaccountno");
	     analysiscode= findElementInXLSheet(getParameterService,"analysiscode");
	     AmountBankAdjustment= findElementInXLSheet(getParameterService,"AmountBankAdjustment");
	     btn_costallocationbankadjustment= findElementInXLSheet(getParameterService,"btn_costallocationbankadjustment");
	     btn_UpdateCostAllocation2= findElementInXLSheet(getParameterService,"btn_UpdateCostAllocation2");
	     btn_CheckoutBankAdjustment= findElementInXLSheet(getParameterService,"btn_CheckoutBankAdjustment");
	     btn_InboundPaymentAdvice= findElementInXLSheet(getParameterService,"btn_InboundPaymentAdvice");
	     btn_NewInboundPaymentAdvice= findElementInXLSheet(getParameterService,"btn_NewInboundPaymentAdvice");
	     btn_VendorRefund= findElementInXLSheet(getParameterService,"btn_VendorRefund");
	     btn_searchvendorInboundPaymentAdvice= findElementInXLSheet(getParameterService,"btn_searchvendorInboundPaymentAdvice");
	     AmountInboundPaymentAdvice= findElementInXLSheet(getParameterService,"AmountInboundPaymentAdvice");
	     btn_CostAllocation2= findElementInXLSheet(getParameterService,"btn_CostAllocation2");
	     btn_CheckoutInboundPaymentAdvice= findElementInXLSheet(getParameterService,"btn_CheckoutInboundPaymentAdvice");
	     btn_DraftInboundPaymentAdvice= findElementInXLSheet(getParameterService,"btn_DraftInboundPaymentAdvice");
	     btn_CustomerDebitMemo= findElementInXLSheet(getParameterService,"btn_CustomerDebitMemo");
	     btn_SearchCustomerDebitMemo= findElementInXLSheet(getParameterService,"btn_SearchCustomerDebitMemo");
	     customer= findElementInXLSheet(getParameterService,"customer");
	     btn_customer= findElementInXLSheet(getParameterService,"btn_customer");
	     btn_RevenueAdjustment= findElementInXLSheet(getParameterService,"btn_RevenueAdjustment");
	     btn_Adjustment= findElementInXLSheet(getParameterService,"btn_Adjustment");
	     btn_CustomerRefund= findElementInXLSheet(getParameterService,"btn_CustomerRefund");
	     btn_SearchCustomerRefund= findElementInXLSheet(getParameterService,"btn_SearchCustomerRefund");
	     AmountCustomerRefund= findElementInXLSheet(getParameterService,"AmountCustomerRefund");
	     btn_CostAllocationCustomerRefund= findElementInXLSheet(getParameterService,"btn_CostAllocationCustomerRefund");
	     btn_UpdateCustomerRefund= findElementInXLSheet(getParameterService,"btn_UpdateCustomerRefund");
	     btn_procurement= findElementInXLSheet(getParameterService,"btn_procurement");
	     ProcuremnentSubSideMenu= findElementInXLSheet(getParameterService,"ProcuremnentSubSideMenu");
	     btn_PurchaseInvoice= findElementInXLSheet(getParameterService,"btn_PurchaseInvoice");
	     btn_NewPurchaseInvoice= findElementInXLSheet(getParameterService,"btn_NewPurchaseInvoice");
	     btn_NewPurchaseInvoiceService= findElementInXLSheet(getParameterService,"btn_NewPurchaseInvoiceService");
	    btn_searchvendorPurchaseinvoicejourney= findElementInXLSheet(getParameterService,"btn_searchvendorPurchaseinvoicejourney");
	    btn_productPurchaseinvoicejourney= findElementInXLSheet(getParameterService,"btn_productPurchaseinvoicejourney");
	    btn_BillingAddress= findElementInXLSheet(getParameterService,"btn_BillingAddress");
	    BillingAddressPurchaseInvoice= findElementInXLSheet(getParameterService,"BillingAddressPurchaseInvoice");
	    btn_ApplyBillingAddress= findElementInXLSheet(getParameterService,"btn_ApplyBillingAddress");
	    Discount= findElementInXLSheet(getParameterService,"Discount");
	    btn_CheckoutPurchaseInvoice= findElementInXLSheet(getParameterService,"btn_CheckoutPurchaseInvoice");
	    btn_ReleasePurchaseInvoice1= findElementInXLSheet(getParameterService,"btn_ReleasePurchaseInvoice1");
	    btn_CostAllocationPurchaseInvoice= findElementInXLSheet(getParameterService,"btn_CostAllocationPurchaseInvoice");
	    btn_UpdatePurchaseInvoice= findElementInXLSheet(getParameterService,"btn_UpdatePurchaseInvoice");
	    Costcenter= findElementInXLSheet(getParameterService,"Costcenter");
	    btn_deletecostallocation= findElementInXLSheet(getParameterService,"btn_deletecostallocation");
	    btn_navigationmenuPurchaseInvoice= findElementInXLSheet(getParameterService,"btn_navigationmenuPurchaseInvoice");
	    btn_Product= findElementInXLSheet(getParameterService,"btn_Product");
	    btn_SerialNos = findElementInXLSheet(getParameterService,"btn_SerialNos");
	    btn_Productso= findElementInXLSheet(getParameterService,"btn_Productso");
	    btn_OutBoundloanOrder= findElementInXLSheet(getParameterService,"btn_OutBoundloanOrder");
	    btn_draftOutboundLoanOrder= findElementInXLSheet(getParameterService,"btn_draftOutboundLoanOrder");
	    btn_ReleaseOutboundLoanOrder= findElementInXLSheet(getParameterService,"btn_ReleaseOutboundLoanOrder");
	    btn_GoToPageOutboundLoanOrder= findElementInXLSheet(getParameterService,"btn_GoToPageOutboundLoanOrder");
	    NewOutboundShipmentPage= findElementInXLSheet(getParameterService,"NewOutboundShipmentPage");
	    btn_DraftOutboundLoanOrder2= findElementInXLSheet(getParameterService,"btn_DraftOutboundLoanOrder2");
	    DraftOutboundShipmentPage= findElementInXLSheet(getParameterService,"DraftOutboundShipmentPage");
	    btn_SerialBatchOutboundShipmentPage= findElementInXLSheet(getParameterService,"btn_SerialBatchOutboundShipmentPage");
	    btn_ProductOutboundShipmentPage= findElementInXLSheet(getParameterService,"btn_ProductOutboundShipmentPage");
	    SerialProductOutboundShipment= findElementInXLSheet(getParameterService,"SerialProductOutboundShipment");
	    btn_DraftOutboundShipmentPage= findElementInXLSheet(getParameterService,"btn_DraftOutboundShipmentPage");
	    btn_InventoryandWareHousing= findElementInXLSheet(getParameterService,"btn_InventoryandWareHousing");
	    btn_OutBoundLoanOrder= findElementInXLSheet(getParameterService,"btn_OutBoundLoanOrder");
	    OutBoundLoanOrderOrderPage= findElementInXLSheet(getParameterService,"OutBoundLoanOrderOrderPage");
	    btn_OutboundloanOrder= findElementInXLSheet(getParameterService,"btn_OutboundloanOrder");
	    btn_WarrantyReplacement= findElementInXLSheet(getParameterService,"btn_WarrantyReplacement");
	    WarrantyReplacementPopUp= findElementInXLSheet(getParameterService,"WarrantyReplacementPopUp");
	    btn_ReplacementSerialNo= findElementInXLSheet(getParameterService,"btn_ReplacementSerialNo");
	    btn_OutboundloanOrderDoc= findElementInXLSheet(getParameterService,"btn_OutboundloanOrderDoc");
	    btn_SerialProductSelect= findElementInXLSheet(getParameterService,"btn_SerialProductSelect");
	    btn_CompleteServiceJobOrder= findElementInXLSheet(getParameterService,"btn_CompleteServiceJobOrder");
	    btn_stopServiceJobOrder= findElementInXLSheet(getParameterService,"btn_stopServiceJobOrder");
	    CompleteProcessPopup= findElementInXLSheet(getParameterService,"CompleteProcessPopup");
	    btn_ServiceContract= findElementInXLSheet(getParameterService,"btn_ServiceContract");
	    btn_NewServiceContractForm= findElementInXLSheet(getParameterService,"btn_NewServiceContractForm");
	    NewServiceContractFormPage= findElementInXLSheet(getParameterService,"NewServiceContractFormPage");
	    ServiceContractTitle= findElementInXLSheet(getParameterService,"ServiceContractTitle");
	    btn_SearchCustomerServiceContract= findElementInXLSheet(getParameterService,"btn_SearchCustomerServiceContract");
	    btn_EffectiveFrom= findElementInXLSheet(getParameterService,"btn_EffectiveFrom");
	    btn_EffectiveFromDate= findElementInXLSheet(getParameterService,"btn_EffectiveFromDate");
	    btn_EffectiveTo= findElementInXLSheet(getParameterService,"btn_EffectiveTo");
	    btn_EffectiveToDate= findElementInXLSheet(getParameterService,"btn_EffectiveToDate");
	    btn_nextMonthDatePlicker= findElementInXLSheet(getParameterService,"btn_nextMonthDatePlicker");
	    ContractGroup= findElementInXLSheet(getParameterService,"ContractGroup");
	    btn_OrderNo1= findElementInXLSheet(getParameterService,"btn_OrderNo1");
	    InternalOrderPage= findElementInXLSheet(getParameterService, "InternalOrderPage");
	    DraftedInternalOrderPage= findElementInXLSheet(getParameterService, "DraftedInternalOrderPage");
	    btn_NewEmployee= findElementInXLSheet(getParameterService, "btn_NewEmployee1");
	    employeecode= findElementInXLSheet(getParameterService, "employeecode");
	    employeename= findElementInXLSheet(getParameterService, "employeename");
	    namewithintials= findElementInXLSheet(getParameterService, "namewithintials");
	    employeegroup= findElementInXLSheet(getParameterService, "employeegroup");
	    employeelookup= findElementInXLSheet(getParameterService, "employeelookup");
	    btn_OrganizationManagement= findElementInXLSheet(getParameterService, "btn_OrganizationManagement");
	    btn_EmployeeInformation= findElementInXLSheet(getParameterService, "btn_EmployeeInformation");
	    btn_ReleaseEmployee= findElementInXLSheet(getParameterService, "btn_ReleaseEmployee1");
	    btn_searchrateprofile= findElementInXLSheet(getParameterService, "btn_searchrateprofile");
	    btn_rate= findElementInXLSheet(getParameterService, "btn_rate");
	    payableaccount= findElementInXLSheet(getParameterService, "payableaccount");
	    btn_ratesearch= findElementInXLSheet(getParameterService, "btn_ratesearch");
	    ReleaseServiceInvoicePage= findElementInXLSheet(getParameterService, "ReleaseServiceInvoicePage");
	    btn_action1= findElementInXLSheet(getParameterService, "btn_action1");
	    DraftedInternalReceiptPage= findElementInXLSheet(getParameterService, "DraftedInternalReceiptPage");
	    btn_ServiceLevelAgreement= findElementInXLSheet(getParameterService, "btn_ServiceLevelAgreement");
	    btn_agreementname= findElementInXLSheet(getParameterService, "btn_agreementname");
	    Description= findElementInXLSheet(getParameterService, "Description");
	    searchowner= findElementInXLSheet(getParameterService, "searchowner");
	    btn_owner1= findElementInXLSheet(getParameterService, "btn_owner1");
	    menu= findElementInXLSheet(getParameterService, "menu");
	    serialWise= findElementInXLSheet(getParameterService, "serialWise");
	    serialTextVal= findElementInXLSheet(getParameterService, "serialTextVal");
	    serialMenuClose= findElementInXLSheet(getParameterService, "serialMenuClose");
	    serialTextVal2= findElementInXLSheet(getParameterService, "serialTextVal2");
	    serialTextVal2_Service_TC_001= findElementInXLSheet(getParameterService, "serialTextVal2_Service_TC_001");
	    menu1= findElementInXLSheet(getParameterService, "menu1");
	    btn_ServiceLevelAgreement1= findElementInXLSheet(getParameterService,"btn_ServiceLevelAgreement1");
	    btn_searchserviceproduct1= findElementInXLSheet(getParameterService,"btn_searchserviceproduct1");
	    btn_SearchSceduleCode= findElementInXLSheet(getParameterService,"btn_SearchSceduleCode");
	    SceduleCode= findElementInXLSheet(getParameterService,"SceduleCode");
	    btn_SceduleCode= findElementInXLSheet(getParameterService,"btn_SceduleCode");
	    btn_responsibleuser= findElementInXLSheet(getParameterService,"btn_responsibleuser");
	    responsibleuser= findElementInXLSheet(getParameterService,"responsibleuser");
	    btn_responsibleusername= findElementInXLSheet(getParameterService,"btn_responsibleusername");
	    btn_ContractLine= findElementInXLSheet(getParameterService,"btn_ContractLine");
	    tagno= findElementInXLSheet(getParameterService,"tagno");
	    btn_Summary= findElementInXLSheet(getParameterService,"btn_Summary");
	    btn_checkoutServiceContract= findElementInXLSheet(getParameterService,"btn_checkoutServiceContract");
	    btn_draftServiceContract= findElementInXLSheet(getParameterService,"btn_draftServiceContract");
	    EstimatedCost= findElementInXLSheet(getParameterService,"EstimatedCost");
	    pricingprofile= findElementInXLSheet(getParameterService,"pricingprofile");
	    btn_pricingprofile12= findElementInXLSheet(getParameterService,"btn_pricingprofile12");
	    btn_searchserviceproductfirst= findElementInXLSheet(getParameterService,"btn_searchserviceproductfirst");
	    btn_billable= findElementInXLSheet(getParameterService,"btn_billable");
	    Referencecode= findElementInXLSheet(getParameterService,"Referencecode");
	    btn_searchlotproduct= findElementInXLSheet(getParameterService,"btn_searchlotproduct");
	    btn_expand9= findElementInXLSheet(getParameterService,"btn_expand9");
	    btn_searchBatchSerialproduct= findElementInXLSheet(getParameterService,"btn_searchBatchSerialproduct");
	    btn_expand10= findElementInXLSheet(getParameterService,"btn_expand10");
	    EstimatedValueService= findElementInXLSheet(getParameterService,"EstimatedValueService");
	    ServiceJobOrderReleaseNoTC= findElementInXLSheet(getParameterService,"ServiceJobOrderReleaseNoTC");
	    EstimatedQtyLOT= findElementInXLSheet(getParameterService,"EstimatedQtyLOT");
	    EstimatedSerialBatchQty= findElementInXLSheet(getParameterService,"EstimatedSerialBatchQty");
	    InternalOrderReleaseNoTC= findElementInXLSheet(getParameterService,"InternalOrderReleaseNoTC");
	    internaldispatchorderpage= findElementInXLSheet(getParameterService,"internaldispatchorderpage");
	    internaldispatchorderReleaseTC= findElementInXLSheet(getParameterService,"internaldispatchorderReleaseTC");
	    InternalReturnOrderReleaseNoTC= findElementInXLSheet(getParameterService,"InternalReturnOrderReleaseNoTC");
	    ReleasedInternalReturnOrderPage= findElementInXLSheet(getParameterService,"ReleasedInternalReturnOrderPage");
	    ReleasedInternalReceiptTC= findElementInXLSheet(getParameterService,"ReleasedInternalReceiptTC");
	    ReleasedInternalReceipt= findElementInXLSheet(getParameterService,"ReleasedInternalReceipt");
	    ReleaseInvoiceProposalPage= findElementInXLSheet(getParameterService,"ReleaseInvoiceProposalPage");
	    ReleasePurchaseInvoicePage= findElementInXLSheet(getParameterService,"ReleasePurchaseInvoicePage");
	    ReleasedInvoiceProposalPage= findElementInXLSheet(getParameterService,"ReleasedInvoiceProposalPage");
	    ReleasedServiceOrderPage= findElementInXLSheet(getParameterService,"ReleasedServiceOrderPage");
	    ReleasedServiceQuatationPage= findElementInXLSheet(getParameterService,"ReleasedServiceQuatationPage");
	    UnitPriceLabour1= findElementInXLSheet(getParameterService,"UnitPriceLabour1");
	    UnitPriceExpense= findElementInXLSheet(getParameterService,"UnitPriceExpense");
	    ReleasedOutboundPaymentAdvicePage= findElementInXLSheet(getParameterService,"ReleasedOutboundPaymentAdvicePage");
	    ReleaseOutboundPaymentPage= findElementInXLSheet(getParameterService,"ReleaseOutboundPaymentPage");
	    ReleasedpettycashPage= findElementInXLSheet(getParameterService,"ReleasedpettycashPage");
	    rateprofile= findElementInXLSheet(getParameterService,"rateprofile");
	    rateprofilevalue= findElementInXLSheet(getParameterService,"rateprofilevalue");
	    btn_searchworkingcalander= findElementInXLSheet(getParameterService,"btn_searchworkingcalander");
	    workingcalender= findElementInXLSheet(getParameterService,"workingcalender");
	    workingcalendernew= findElementInXLSheet(getParameterService,"workingcalendernew");
	    btn_nextdate= findElementInXLSheet(getParameterService,"btn_nextdate");
	    btn_editServiceContract= findElementInXLSheet(getParameterService,"btn_editServiceContract");
	    btn_PreventiveMaintenance= findElementInXLSheet(getParameterService,"btn_PreventiveMaintenance");
	    btn_plus= findElementInXLSheet(getParameterService,"btn_plus");
	    Title= findElementInXLSheet(getParameterService,"Title");
	    PMCategory= findElementInXLSheet(getParameterService,"PMCategory");
	    PMcount= findElementInXLSheet(getParameterService,"PMcount");
	    PMinterval= findElementInXLSheet(getParameterService,"PMinterval");
	    owner= findElementInXLSheet(getParameterService,"owner");
	    btn_ChargeableProd= findElementInXLSheet(getParameterService,"btn_ChargeableProd");
	    btn_searchproducts= findElementInXLSheet(getParameterService,"btn_searchproducts");
	    btn_UpdatePreventiveMaintenance= findElementInXLSheet(getParameterService,"btn_UpdatePreventiveMaintenance");
	    ReleaseServiceContractPage= findElementInXLSheet(getParameterService,"ReleaseServiceContractPage");
	    btn_ConverttoServiceInvoice= findElementInXLSheet(getParameterService,"btn_ConverttoServiceInvoice");
	    btn_checkoutServiceInvoice1= findElementInXLSheet(getParameterService,"btn_checkoutServiceInvoice1");
	    btn_draftServiceInvoice= findElementInXLSheet(getParameterService,"btn_draftServiceInvoice");
	    btn_updateServiceContract= findElementInXLSheet(getParameterService,"btn_updateServiceContract");
	    btn_draftServiceInvoice1= findElementInXLSheet(getParameterService,"btn_draftServiceInvoice1");
	    btn_releaseServiceInvoice1= findElementInXLSheet(getParameterService,"btn_releaseServiceInvoice1");
	    ReleasedServiceInvoice1page= findElementInXLSheet(getParameterService,"ReleasedServiceInvoice1page");
	    btn_entution= findElementInXLSheet(getParameterService,"btn_entution");
	    btn_Rolecenter= findElementInXLSheet(getParameterService,"btn_Rolecenter");
	    Dashboardpage= findElementInXLSheet(getParameterService,"Dashboardpage");
	    btn_ScheduledJobs= findElementInXLSheet(getParameterService,"btn_ScheduledJobs");
	    btn_OldServiceContract= findElementInXLSheet(getParameterService,"btn_OldServiceContract");
	    btn_ServiceContractLine= findElementInXLSheet(getParameterService,"btn_ServiceContractLine");
	    btn_ServiceInvoiceLink= findElementInXLSheet(getParameterService,"btn_ServiceInvoiceLink");
	    btn_reverce= findElementInXLSheet(getParameterService,"btn_reverce");
	    btn_Revercedate= findElementInXLSheet(getParameterService,"btn_Revercedate");
	    btn_selectRevercedate= findElementInXLSheet(getParameterService,"btn_selectRevercedate");
	    btn_reverceapply= findElementInXLSheet(getParameterService,"btn_reverceapply");
	    RevercePopupPage= findElementInXLSheet(getParameterService,"RevercePopupPage");
	    btn_yesServiceInvoice= findElementInXLSheet(getParameterService,"btn_yesServiceInvoice");
	    btn_reverceservicecontract= findElementInXLSheet(getParameterService,"btn_reverceservicecontract");
	    btn_yesServiceContract= findElementInXLSheet(getParameterService,"btn_yesServiceContract");
	    btn_ServiceQuotation= findElementInXLSheet(getParameterService,"btn_ServiceQuotation");
	    ServiceQuotationPage= findElementInXLSheet(getParameterService,"ServiceQuotationPage");
	    btn_newServiceQuotation= findElementInXLSheet(getParameterService,"btn_newServiceQuotation");
	    btn_newServiceQuotationtoServiceJobOrder= findElementInXLSheet(getParameterService,"btn_newServiceQuotationtoServiceJobOrder");
	    btn_SearchCustomerServiceQuoatation= findElementInXLSheet(getParameterService,"btn_SearchCustomerServiceQuoatation");
	    currency= findElementInXLSheet(getParameterService,"currency");
	    btn_searchBillingAddress= findElementInXLSheet(getParameterService,"btn_searchBillingAddress");
	    BillingAddress1= findElementInXLSheet(getParameterService,"BillingAddress1");
	    ShipingAddress1= findElementInXLSheet(getParameterService,"ShipingAddress1");
	    salesunit= findElementInXLSheet(getParameterService,"salesunit");
	    btn_searchServiceProduct= findElementInXLSheet(getParameterService,"btn_searchServiceProduct");
	    serviceproduct= findElementInXLSheet(getParameterService,"serviceproduct");
	    btn_ServiceDetails1= findElementInXLSheet(getParameterService,"btn_ServiceDetails1");
	    unitprice= findElementInXLSheet(getParameterService,"unitprice");
	    btn_checkoutServiceQuatation1= findElementInXLSheet(getParameterService,"btn_checkoutServiceQuatation1");
	    btn_searchSerialProduct= findElementInXLSheet(getParameterService,"btn_searchSerialProduct");
	    btn_plusSQ1= findElementInXLSheet(getParameterService,"btn_plusSQ1");
	    btn_draftSericeQuotation= findElementInXLSheet(getParameterService,"btn_draftSericeQuotation");
	    btn_draftSericeQuotationNew= findElementInXLSheet(getParameterService,"btn_draftSericeQuotationNew");
	    btn_ReleaseSericeQuotationNew= findElementInXLSheet(getParameterService,"btn_ReleaseSericeQuotationNew");
	    ReleaseSericeQuotationNewPage= findElementInXLSheet(getParameterService,"ReleaseSericeQuotationNewPage");
	    btn_contractdate= findElementInXLSheet(getParameterService,"btn_contractdate");
	    btn_beforedate= findElementInXLSheet(getParameterService,"btn_beforedate");
	    btn_selectcontractdate= findElementInXLSheet(getParameterService,"btn_selectcontractdate");
	    btn_ChargeableProd1= findElementInXLSheet(getParameterService,"btn_ChargeableProd1");
	    qty= findElementInXLSheet(getParameterService,"qty");
	    btn_plus2= findElementInXLSheet(getParameterService,"btn_plus2");
	    typelabour= findElementInXLSheet(getParameterService,"typelabour");
	    btn_searchlabourSQ= findElementInXLSheet(getParameterService,"btn_searchlabourSQ");
	    unitpricelabour= findElementInXLSheet(getParameterService,"unitpricelabour");
	    QTY1= findElementInXLSheet(getParameterService,"QTY1");
	    QTY2= findElementInXLSheet(getParameterService,"QTY2");
	    QTY3= findElementInXLSheet(getParameterService,"QTY3");
	    btn_searchlabouremployeeSQ= findElementInXLSheet(getParameterService,"btn_searchlabouremployeeSQ");
	    btn_checkoutServiceOrder1= findElementInXLSheet(getParameterService,"btn_checkoutServiceOrder1");
	    btn_DraftServiceOrder1= findElementInXLSheet(getParameterService,"btn_DraftServiceOrder1");
	    btn_ReleaseServiceOrder1= findElementInXLSheet(getParameterService,"btn_ReleaseServiceOrder1");
	    btn_CreateSJO= findElementInXLSheet(getParameterService,"btn_CreateSJO");
	    btn_ServiceDetails2= findElementInXLSheet(getParameterService,"btn_ServiceDetails2");
	    DraftedSericeQuotationNewPage= findElementInXLSheet(getParameterService,"DraftedSericeQuotationNewPage");
	    DraftedServiceOrderNewPage= findElementInXLSheet(getParameterService,"DraftedServiceOrderNewPage");
	    btn_actionSO= findElementInXLSheet(getParameterService,"btn_actionSO");
	    filledcoloums= findElementInXLSheet(getParameterService,"filledcoloums");
	    Productlist1= findElementInXLSheet(getParameterService,"Productlist1");
	    btn_range1= findElementInXLSheet(getParameterService,"btn_range1");
	    btn_HireAgreement= findElementInXLSheet(getParameterService,"btn_HireAgreement");
	    btn_NewHireAgreement= findElementInXLSheet(getParameterService,"btn_NewHireAgreement");
	    NewHireAgreementpage= findElementInXLSheet(getParameterService,"NewHireAgreementpage");
	    AgreementNo= findElementInXLSheet(getParameterService,"AgreementNo");
	    TitleAgreement= findElementInXLSheet(getParameterService,"TitleAgreement");
	    btn_agreementdate= findElementInXLSheet(getParameterService,"btn_agreementdate");
	    DescriptionHireAgreement= findElementInXLSheet(getParameterService,"DescriptionHireAgreement");
	    salesunitHireAgreement= findElementInXLSheet(getParameterService,"salesunitHireAgreement");
	    HireBasis= findElementInXLSheet(getParameterService,"HireBasis");
	    btn_accountsearchHireAgreement= findElementInXLSheet(getParameterService,"btn_accountsearchHireAgreement");
	    btn_SearchSceduleCodeHireAgreement= findElementInXLSheet(getParameterService,"btn_SearchSceduleCodeHireAgreement");
	    btn_responsibleuserHireAgreement= findElementInXLSheet(getParameterService,"btn_responsibleuserHireAgreement");
	    Contractgroup= findElementInXLSheet(getParameterService,"Contractgroup");
	    btn_ResourceInformation= findElementInXLSheet(getParameterService,"btn_ResourceInformation");
	    RecourceInformationPage= findElementInXLSheet(getParameterService,"RecourceInformationPage");
	    btn_searchbtnReferncecode= findElementInXLSheet(getParameterService,"btn_searchbtnReferncecode");
	    ReferncecodePage= findElementInXLSheet(getParameterService,"ReferncecodePage");
	    Referncecode= findElementInXLSheet(getParameterService,"Referncecode");
	    btn_Referncecode= findElementInXLSheet(getParameterService,"btn_Referncecode");
	    openingunit= findElementInXLSheet(getParameterService,"openingunit");
	    Minimumunit= findElementInXLSheet(getParameterService,"Minimumunit");
	    btn_RateRatio= findElementInXLSheet(getParameterService,"btn_RateRatio");
	    RateRangeSetup= findElementInXLSheet(getParameterService,"RateRangeSetup");
	    Start1= findElementInXLSheet(getParameterService,"Start1");
	    End1= findElementInXLSheet(getParameterService,"End1");
	    Rate1= findElementInXLSheet(getParameterService,"Rate1");
	    btn_plusRR= findElementInXLSheet(getParameterService,"btn_plusRR");
	    Start2= findElementInXLSheet(getParameterService,"Start2");
	    End2= findElementInXLSheet(getParameterService,"End2");
	    Rate2= findElementInXLSheet(getParameterService,"Rate2");
	    btn_draftHireAgreement= findElementInXLSheet(getParameterService,"btn_draftHireAgreement");
	    DraftedHireAgreement= findElementInXLSheet(getParameterService,"DraftedHireAgreement");
	    btn_ReleaseHireAgreement= findElementInXLSheet(getParameterService,"btn_ReleaseHireAgreement");
	    ReleasedHireAgreement= findElementInXLSheet(getParameterService,"ReleasedHireAgreement");
	    btn_case= findElementInXLSheet(getParameterService,"btn_case");
	    CasePage= findElementInXLSheet(getParameterService,"CasePage");
	    btn_NewCase= findElementInXLSheet(getParameterService,"btn_NewCase");
	    NewCasePage= findElementInXLSheet(getParameterService,"NewCasePage");
	    TitleNewCase= findElementInXLSheet(getParameterService,"TitleNewCase");
	    Orgin= findElementInXLSheet(getParameterService,"Orgin");
	    btn_accountsearchCase= findElementInXLSheet(getParameterService,"btn_accountsearchCase");
	    btn_ProductsoCase= findElementInXLSheet(getParameterService,"btn_ProductsoCase");
	    btn_SerialNosCase= findElementInXLSheet(getParameterService,"btn_SerialNosCase");
	    TypeCase= findElementInXLSheet(getParameterService,"TypeCase");
	    btn_ApplyCase_Smoke_Service_004= findElementInXLSheet(getParameterService,"btn_ApplyCase_Smoke_Service_004");
	    btn_draftcase= findElementInXLSheet(getParameterService,"btn_draftcase");
	    DraftedCasePage= findElementInXLSheet(getParameterService,"DraftedCasePage");
	    btn_releasecase= findElementInXLSheet(getParameterService,"btn_releasecase");
	    btn_ServiceProductRegistration= findElementInXLSheet(getParameterService,"btn_ServiceProductRegistration");
	    btn_NewServiceProductRegistration= findElementInXLSheet(getParameterService,"btn_NewServiceProductRegistration");
	    RegistrationType= findElementInXLSheet(getParameterService,"RegistrationType");
	    btn_accountsearchServiceProductRegis= findElementInXLSheet(getParameterService,"btn_accountsearchServiceProductRegis");
	    btn_Tagno= findElementInXLSheet(getParameterService,"btn_Tagno");
	    btn_SerialNo= findElementInXLSheet(getParameterService,"btn_SerialNo");
	    btn_Tagnos= findElementInXLSheet(getParameterService,"btn_Tagnos");
	    btn_SearchWarrantyProfile= findElementInXLSheet(getParameterService,"btn_SearchWarrantyProfile");
	    Warranty= findElementInXLSheet(getParameterService,"Warranty");
	    btn_Warranty= findElementInXLSheet(getParameterService,"btn_Warranty");
	    btn_WarrantyDate= findElementInXLSheet(getParameterService,"btn_WarrantyDate");
	    btn_selectWarrantyDate= findElementInXLSheet(getParameterService,"btn_selectWarrantyDate");
	    btn_warrantyProfileSales= findElementInXLSheet(getParameterService,"btn_warrantyProfileSales");
	    btn_WarrantyDateSales= findElementInXLSheet(getParameterService,"btn_WarrantyDateSales");
	    btn_DraftServiceProductReg= findElementInXLSheet(getParameterService,"btn_DraftServiceProductReg");
	    btn_ReleaseServiceProductReg= findElementInXLSheet(getParameterService,"btn_ReleaseServiceProductReg");
	    tagnoDirectSR= findElementInXLSheet(getParameterService,"tagnoDirectSR");
	    btn_searchReferenceCode= findElementInXLSheet(getParameterService,"btn_searchReferenceCode");
	    btn_searchShippingAddress= findElementInXLSheet(getParameterService,"btn_searchShippingAddress");
	    ShippingAddress= findElementInXLSheet(getParameterService,"ShippingAddress");
	    btn_ApplyAdress= findElementInXLSheet(getParameterService,"btn_ApplyAdress");
	    btn_searchtagno= findElementInXLSheet(getParameterService,"btn_searchtagno");
	    btn_serialno= findElementInXLSheet(getParameterService,"btn_serialno");
	    btn_PricingProfileSearchSE= findElementInXLSheet(getParameterService,"btn_PricingProfileSearchSE");
	    pricingprofileSE= findElementInXLSheet(getParameterService,"pricingprofileSE");
	    btn_EstimationDetails= findElementInXLSheet(getParameterService,"btn_EstimationDetails");
	    btn_ServiceCostEstimation= findElementInXLSheet(getParameterService,"btn_ServiceCostEstimation");
	    btn_NewServiceCostEstimation= findElementInXLSheet(getParameterService,"btn_NewServiceCostEstimation");
	    DescriptionService= findElementInXLSheet(getParameterService,"DescriptionService");
	    btn_SearchCustomerServiceCE= findElementInXLSheet(getParameterService,"btn_SearchCustomerServiceCE");
	    AccountLookUPPageSE= findElementInXLSheet(getParameterService,"AccountLookUPPageSE");
	    btn_EmployeeAdvanceRequest= findElementInXLSheet(getParameterService,"btn_EmployeeAdvanceRequest");
	    btn_NewEmployeeAdvanceRequest= findElementInXLSheet(getParameterService,"btn_NewEmployeeAdvanceRequest");
	    btn_searchemployeeEmployeeAdvanceRequest= findElementInXLSheet(getParameterService,"btn_searchemployeeEmployeeAdvanceRequest");
	    ExpenseType= findElementInXLSheet(getParameterService,"ExpenseType");
	    Purpose= findElementInXLSheet(getParameterService,"Purpose");
	    AdvanceAmount= findElementInXLSheet(getParameterService,"AdvanceAmount");
	    btn_DraftEmployeeAdvanceRequest= findElementInXLSheet(getParameterService,"btn_DraftEmployeeAdvanceRequest");
	    btn_PayEmployeeAdvance= findElementInXLSheet(getParameterService,"btn_PayEmployeeAdvance");
	    OutboundPaymentAdvancePage= findElementInXLSheet(getParameterService,"OutboundPaymentAdvancePage");
	    btn_NewConvertToOutboundPayment= findElementInXLSheet(getParameterService,"btn_NewConvertToOutboundPayment");
	    OutboundPaymentPage= findElementInXLSheet(getParameterService,"OutboundPaymentPage");
	    btn_putTick= findElementInXLSheet(getParameterService,"btn_putTick");
	    btn_checkoutOutboundPaymentAdvice= findElementInXLSheet(getParameterService,"btn_checkoutOutboundPaymentAdvice");
	    DraftOutboundpaymentAdvice= findElementInXLSheet(getParameterService,"DraftOutboundpaymentAdvice");
	    btn_details= findElementInXLSheet(getParameterService,"btn_details");
	    btn_DraftOutBoundPaymentAdvice= findElementInXLSheet(getParameterService,"btn_DraftOutBoundPaymentAdvice");
	    btn_checkoutOutboundPayment= findElementInXLSheet(getParameterService,"btn_checkoutOutboundPayment");
	    btn_NewcheckoutOutboundPaymentAdvice= findElementInXLSheet(getParameterService,"btn_NewcheckoutOutboundPaymentAdvice");
	    btn_actionNewConvertToOutboundPayment= findElementInXLSheet(getParameterService,"btn_actionNewConvertToOutboundPayment");
	    selectagreementdate= findElementInXLSheet(getParameterService,"selectagreementdate");
	    warehouse1= findElementInXLSheet(getParameterService,"warehouse1");
	    btn_searchbatchproduct2= findElementInXLSheet(getParameterService,"btn_searchbatchproduct2");
	    btn_searchResourcesReferenceCode3= findElementInXLSheet(getParameterService,"btn_searchResourcesReferenceCode3");
	    btn_searchResources3= findElementInXLSheet(getParameterService,"btn_searchResources3");
	    btn_checkoutServiceCostEstimation= findElementInXLSheet(getParameterService,"btn_checkoutServiceCostEstimation");
	    btn_LastdayofMonthPlus= findElementInXLSheet(getParameterService,"btn_LastdayofMonthPlus");
	    btn_ServiceContractLastdayofMonth= findElementInXLSheet(getParameterService,"btn_ServiceContractLastdayofMonth");
	    btn_selectallServiceContractLastdayofMonth= findElementInXLSheet(getParameterService,"btn_selectallServiceContractLastdayofMonth");
	    BillingBasis= findElementInXLSheet(getParameterService,"BillingBasis");
	    actualunit= findElementInXLSheet(getParameterService,"actualunit");
	    btn_checkoutActualUpdate= findElementInXLSheet(getParameterService,"btn_checkoutActualUpdate");
	    btn_applyActualUpdate= findElementInXLSheet(getParameterService,"btn_applyActualUpdate");
	    MessagePopup= findElementInXLSheet(getParameterService,"MessagePopup");
	    btn_serviceInvoicePath= findElementInXLSheet(getParameterService,"btn_serviceInvoicePath");
	    ServiceInvoicePage= findElementInXLSheet(getParameterService,"ServiceInvoicePage");
	    btn_ServiceHireAgreement= findElementInXLSheet(getParameterService,"btn_ServiceHireAgreement");
	    ActualUpdate= findElementInXLSheet(getParameterService,"ActualUpdate");
	    btn_ProductRegistration= findElementInXLSheet(getParameterService,"btn_ProductRegistration");
	    btn_SearchWarrantyProfileProductRegis= findElementInXLSheet(getParameterService,"btn_SearchWarrantyProfileProductRegis");
	    btn_ApplyProductRegis= findElementInXLSheet(getParameterService,"btn_ApplyProductRegis");
	    serialBatchProductRegis= findElementInXLSheet(getParameterService,"serialBatchProductRegis");
	    productListBtn= findElementInXLSheet(getParameterService,"productListBtn");
	    serialTextValuePR= findElementInXLSheet(getParameterService,"serialTextValuePR");
	    btn_okProductRegistration= findElementInXLSheet(getParameterService,"btn_okProductRegistration");
	    serialNoProductRegis= findElementInXLSheet(getParameterService,"serialNoProductRegis");
	    btn_SalesReferDoc= findElementInXLSheet(getParameterService,"btn_SalesReferDoc");
	    SalesInvoicePage= findElementInXLSheet(getParameterService,"SalesInvoicePage");
	    btn_AssosiateProduct= findElementInXLSheet(getParameterService,"btn_AssosiateProduct");
	    btn_searchAssosiateProduct= findElementInXLSheet(getParameterService,"btn_searchAssosiateProduct");
	    TagnoAssosiateProduct= findElementInXLSheet(getParameterService,"TagnoAssosiateProduct");
	    btn_searchWarrantyProfileAssosiateProduct= findElementInXLSheet(getParameterService,"btn_searchWarrantyProfileAssosiateProduct");
	    btn_AssosiateProductTab= findElementInXLSheet(getParameterService,"btn_AssosiateProductTab");
	    btn_ChangeButtonOwnerShipInformation= findElementInXLSheet(getParameterService,"btn_ChangeButtonOwnerShipInformation");
	    btn_SerarchNewOwner= findElementInXLSheet(getParameterService,"btn_SerarchNewOwner");
	    btn_applyChangeOwnershipInformation= findElementInXLSheet(getParameterService,"btn_applyChangeOwnershipInformation");
	    btn_ChangeProductRegisInformation= findElementInXLSheet(getParameterService,"btn_ChangeProductRegisInformation");
	    btn_SearchEndUser= findElementInXLSheet(getParameterService,"btn_SearchEndUser");
	    btn_ApplyProductRegisInformation= findElementInXLSheet(getParameterService,"btn_ApplyProductRegisInformation");
	    btn_ExtendSalesWarranty= findElementInXLSheet(getParameterService,"btn_ExtendSalesWarranty");
	    btn_searchSalesWarrantyExtend= findElementInXLSheet(getParameterService,"btn_searchSalesWarrantyExtend");
	    btn_plusSalesWarrantyExtend= findElementInXLSheet(getParameterService,"btn_plusSalesWarrantyExtend");
	    btn_UpdateSalesWarranty= findElementInXLSheet(getParameterService,"btn_UpdateSalesWarranty");
	    btn_Renewal= findElementInXLSheet(getParameterService,"btn_Renewal");
	    btn_yesRenewal= findElementInXLSheet(getParameterService,"btn_yesRenewal");
	    btn_Renewal2= findElementInXLSheet(getParameterService,"btn_Renewal2");
	    btn_ReplaceProducts= findElementInXLSheet(getParameterService,"btn_ReplaceProducts");
	    btn_SearchReplaceProduct= findElementInXLSheet(getParameterService,"btn_SearchReplaceProduct");
	    Replacetagno= findElementInXLSheet(getParameterService,"Replacetagno");
	    serialTextValOLO= findElementInXLSheet(getParameterService,"serialTextValOLO");
	    btn_SearchSerialProduct= findElementInXLSheet(getParameterService,"btn_SearchSerialProduct");
	    TagNoSO= findElementInXLSheet(getParameterService,"TagNoSO");
	    SearchTagNo= findElementInXLSheet(getParameterService,"SearchTagNo");
	    btn_SpecificTagNo= findElementInXLSheet(getParameterService,"btn_SpecificTagNo");
	    HireAgreeNumber= findElementInXLSheet(getParameterService,"HireAgreeNumber");
	    SpecificHireagreementNo= findElementInXLSheet(getParameterService,"SpecificHireagreementNo");
	    run= findElementInXLSheet(getParameterService,"run");
	    btn_axpandFirstLevelSedulesdJob= findElementInXLSheet(getParameterService,"btn_axpandFirstLevelSedulesdJob");
	    btn_axpandSecondLevelSedulesdJob= findElementInXLSheet(getParameterService,"btn_axpandSecondLevelSedulesdJob");
	    ReleasedServiceInvoice= findElementInXLSheet(getParameterService,"ReleasedServiceInvoice");
	    SerialSpecific= findElementInXLSheet(getParameterService,"SerialSpecific");
	    SerialspecificServiceJobOrderName= findElementInXLSheet(getParameterService,"SerialspecificServiceJobOrderName");
	    SerialBatchNAME= findElementInXLSheet(getParameterService,"SerialBatchNAME");
	    SerialBatchServiceJobOrderName= findElementInXLSheet(getParameterService,"SerialBatchServiceJobOrderName");
	    btn_OrderNo5= findElementInXLSheet(getParameterService,"btn_OrderNo5");
	    LotAutoNAME= findElementInXLSheet(getParameterService,"LotAutoNAME");
	    LotAutoServiceJobOrderName= findElementInXLSheet(getParameterService,"LotAutoServiceJobOrderName");
	    btn_OrderNo4= findElementInXLSheet(getParameterService,"btn_OrderNo4");
	    FifoBatchNAME= findElementInXLSheet(getParameterService,"FifoBatchNAME");
	    FifoBatchServiceJobOrderName= findElementInXLSheet(getParameterService,"FifoBatchServiceJobOrderName");
	    BatchspecificNAME= findElementInXLSheet(getParameterService,"BatchspecificNAME");
	    BatchspecificServiceJobOrderName= findElementInXLSheet(getParameterService,"BatchspecificServiceJobOrderName");
	    btn_OrderNo2= findElementInXLSheet(getParameterService,"btn_OrderNo2");
	    serialspecificAQValue= findElementInXLSheet(getParameterService,"serialspecificAQValue");
	    BatchspecificAQValue= findElementInXLSheet(getParameterService,"BatchspecificAQValue");
	    FIFOBATCHAQValue= findElementInXLSheet(getParameterService,"FIFOBATCHAQValue");
	    autotestingAQValue= findElementInXLSheet(getParameterService,"autotestingAQValue");
	    btn_reversecase= findElementInXLSheet(getParameterService,"btn_reversecase");
	    btn_yesReverseCase= findElementInXLSheet(getParameterService,"btn_yesReverseCase");
	    btn_OutboundPaymentAdviceLink= findElementInXLSheet(getParameterService,"btn_OutboundPaymentAdviceLink");
	    btn_newservicejoborder= findElementInXLSheet(getParameterService,"btn_newservicejoborder");
	    ServiceJobOrderValue= findElementInXLSheet(getParameterService,"ServiceJobOrderValue");
	    TotalValueNo= findElementInXLSheet(getParameterService,"TotalValueNo");
	    TotalExpenceValue= findElementInXLSheet(getParameterService,"TotalExpenceValue");
	    OutboundPaymentValueNo= findElementInXLSheet(getParameterService,"OutboundPaymentValueNo");
	    TotalValueNoBankAdjustment= findElementInXLSheet(getParameterService,"TotalValueNoBankAdjustment");
	    btn_BankAdjustmentLink= findElementInXLSheet(getParameterService,"btn_BankAdjustmentLink");
	    TotalDutyValue= findElementInXLSheet(getParameterService,"TotalDutyValue");
	    btn_CostAdjustment= findElementInXLSheet(getParameterService,"btn_CostAdjustment");
	    btn_InboundPaymetAdviceLink= findElementInXLSheet(getParameterService,"btn_InboundPaymetAdviceLink");
	    btn_CostAdjustmentArrow= findElementInXLSheet(getParameterService,"btn_CostAdjustmentArrow");
	    VendorAdjustmentValue= findElementInXLSheet(getParameterService,"VendorAdjustmentValue");
	    TotalValueVendorRefund= findElementInXLSheet(getParameterService,"TotalValueVendorRefund");
	    btn_InboundPaymetAdviceLinkRevenueAdjustment= findElementInXLSheet(getParameterService,"btn_InboundPaymetAdviceLinkRevenueAdjustment");
	    btn_RevenueAdjustmentArrow= findElementInXLSheet(getParameterService,"btn_RevenueAdjustmentArrow");
	    CustomerAdjustmentValue= findElementInXLSheet(getParameterService,"CustomerAdjustmentValue");
	    btn_deletePI= findElementInXLSheet(getParameterService,"btn_deletePI");
	    btn_updatePI= findElementInXLSheet(getParameterService,"btn_updatePI");
	    btn_EditServiceJobOrder= findElementInXLSheet(getParameterService,"btn_EditServiceJobOrder");
	    EditServiceJobOrderPage= findElementInXLSheet(getParameterService,"EditServiceJobOrderPage");
	    UpadatedServiceJobOrderPage= findElementInXLSheet(getParameterService,"UpadatedServiceJobOrderPage");
	    btn_UpdateandNewServiceJobOrder= findElementInXLSheet(getParameterService,"btn_UpdateandNewServiceJobOrder");
	    btn_DraftandNewServiceJobOrder= findElementInXLSheet(getParameterService,"btn_DraftandNewServiceJobOrder");
	    btn_closepopup= findElementInXLSheet(getParameterService,"btn_closepopup");
	    btn_AddProductTagNo= findElementInXLSheet(getParameterService,"btn_AddProductTagNo");
	    AddProductTagNoPage= findElementInXLSheet(getParameterService,"AddProductTagNoPage");
	    btn_GenerateCustomerAdvance= findElementInXLSheet(getParameterService,"btn_GenerateCustomerAdvance");
	    CustomerAdvancePage= findElementInXLSheet(getParameterService,"CustomerAdvancePage");
	    btn_GenerateResponceDetails= findElementInXLSheet(getParameterService,"btn_GenerateResponceDetails");
	    ResponceDetailsPage= findElementInXLSheet(getParameterService,"ResponceDetailsPage");
	    InvoiceProposalPage= findElementInXLSheet(getParameterService,"InvoiceProposalPage");
	    btn_RMA= findElementInXLSheet(getParameterService,"btn_RMA");
	    RMADetailsPage= findElementInXLSheet(getParameterService,"RMADetailsPage");
	    ServiceQuotationPagePopup= findElementInXLSheet(getParameterService,"ServiceQuotationPagePopup");
	    SerialBatchAQValue= findElementInXLSheet(getParameterService,"SerialBatchAQValue");
	    ReleaseOutboundLoanOrderPage= findElementInXLSheet(getParameterService,"ReleaseOutboundLoanOrderPage");
	    ProductRegistrationPage= findElementInXLSheet(getParameterService,"ProductRegistrationPage");
	    customeraccountvalue= findElementInXLSheet(getParameterService,"customeraccountvalue");
	    warrantyprofilevalue= findElementInXLSheet(getParameterService,"warrantyprofilevalue");
	    btn_backdate= findElementInXLSheet(getParameterService,"btn_backdate");
	    btn_link= findElementInXLSheet(getParameterService,"btn_link");
	    btn_runHireAgreement= findElementInXLSheet(getParameterService,"btn_runHireAgreement");
	    stopserviceOrderPage= findElementInXLSheet(getParameterService,"stopserviceOrderPage");
	    btn_closeRMA= findElementInXLSheet(getParameterService,"btn_closeRMA");
	    SubContractOrderPage1= findElementInXLSheet(getParameterService,"SubContractOrderPage1");
	    btn_ServiceOrdertoInternalDispatchOrderInternal= findElementInXLSheet(getParameterService,"btn_ServiceOrdertoInternalDispatchOrderInternal");
	    btn_ServiceInvoice= findElementInXLSheet(getParameterService,"btn_ServiceInvoice");
	    ServiceInvoice= findElementInXLSheet(getParameterService,"ServiceInvoice");
	    btn_selectServiceInvoice= findElementInXLSheet(getParameterService,"btn_selectServiceInvoice");
	    btn_History= findElementInXLSheet(getParameterService,"btn_History");
	    ServiceInvoiceReversedPage= findElementInXLSheet(getParameterService,"ServiceInvoiceReversedPage");
	    btn_docflow= findElementInXLSheet(getParameterService,"btn_docflow");
	    ServiceDocFlowPage= findElementInXLSheet(getParameterService,"ServiceDocFlowPage");
	    ServiceInvoiceActivitiesPage= findElementInXLSheet(getParameterService,"ServiceInvoiceActivitiesPage");
	    btn_activities= findElementInXLSheet(getParameterService,"btn_activities");
	    btn_journal= findElementInXLSheet(getParameterService,"btn_journal");
	    ServiceInvoiceJournalPage= findElementInXLSheet(getParameterService,"ServiceInvoiceJournalPage");
	    btn_CloseActivitiesPage= findElementInXLSheet(getParameterService,"btn_CloseActivitiesPage");
	    btn_ServiceOrder= findElementInXLSheet(getParameterService,"btn_ServiceOrder");
	    ServiceOrderNo= findElementInXLSheet(getParameterService,"ServiceOrderNo");
	    btn_selectServiceOrder= findElementInXLSheet(getParameterService,"btn_selectServiceOrder");
	    btn_CopyFrom= findElementInXLSheet(getParameterService,"btn_CopyFrom");
	    caseno= findElementInXLSheet(getParameterService,"caseno");
	    btn_selectcaseno= findElementInXLSheet(getParameterService,"btn_selectcaseno");
	    btn_DraftandNewEmployeeAdvanceRequest= findElementInXLSheet(getParameterService,"btn_selectcaseno");
	    btn_copyfromCostEstimation= findElementInXLSheet(getParameterService,"btn_copyfromCostEstimation");
	    ServiceCostEstimationForm= findElementInXLSheet(getParameterService,"ServiceCostEstimationForm");
	    SelectServiceCoseEstimation= findElementInXLSheet(getParameterService,"SelectServiceCoseEstimation");
	    btn_checkoutCostEstimation= findElementInXLSheet(getParameterService,"btn_checkoutCostEstimation");
	    btn_duplicateServiceCostEstimation= findElementInXLSheet(getParameterService,"btn_duplicateServiceCostEstimation");
	    HireAgreementNo= findElementInXLSheet(getParameterService,"HireAgreementNo");
	    btn_selectHireagreement= findElementInXLSheet(getParameterService,"btn_selectHireagreement");
	    btn_draftnewHireAgreement= findElementInXLSheet(getParameterService,"btn_draftnewHireAgreement");
	    btn_CopyFromHireAgreement= findElementInXLSheet(getParameterService,"btn_CopyFromHireAgreement");
	    btn_closeactualupdate= findElementInXLSheet(getParameterService,"btn_closeactualupdate");
	    btn_ServiceGroupconfig= findElementInXLSheet(getParameterService,"btn_ServiceGroupconfig");
	    ServiceGroupconfigpage= findElementInXLSheet(getParameterService,"ServiceGroupconfigpage");
	    btn_newServiceConfiguration= findElementInXLSheet(getParameterService,"btn_newServiceConfiguration");
	    NewServiceGroupconfigpage= findElementInXLSheet(getParameterService,"NewServiceGroupconfigpage");
	    SelectServiceGroup= findElementInXLSheet(getParameterService,"SelectServiceGroup");
	    Doctitle= findElementInXLSheet(getParameterService,"Doctitle");
	    btn_copyfromServiceGroupConfiguaration= findElementInXLSheet(getParameterService,"btn_copyfromServiceGroupConfiguaration");
	    serviceConfig= findElementInXLSheet(getParameterService,"serviceConfig");
	    btn_specificServiceConfig= findElementInXLSheet(getParameterService,"btn_specificServiceConfig");
	    btn_searchServiceGroupCongig= findElementInXLSheet(getParameterService,"btn_searchServiceGroupCongig");
	    btn_DraftandNewServiceCongig= findElementInXLSheet(getParameterService,"btn_DraftandNewServiceCongig");
	    btn_searchEmployeeHiearchy= findElementInXLSheet(getParameterService,"btn_searchEmployeeHiearchy");
	    searchhiearchy= findElementInXLSheet(getParameterService,"searchhiearchy");
	    btn_specificsearchhiearchy= findElementInXLSheet(getParameterService,"btn_specificsearchhiearchy");
	    btn_ServiceControl= findElementInXLSheet(getParameterService,"btn_ServiceControl");
	    ServiceControlpage= findElementInXLSheet(getParameterService,"ServiceControlpage");
	    btn_casedetails= findElementInXLSheet(getParameterService,"btn_casedetails");
	    btn_gernerateServiceOrder= findElementInXLSheet(getParameterService,"btn_gernerateServiceOrder");
	    ServiceJobOrderpageSC= findElementInXLSheet(getParameterService,"ServiceJobOrderpageSC");
	    btn_CopyfromBankAdjustment= findElementInXLSheet(getParameterService,"btn_CopyfromBankAdjustment");
	    BankAdjustment= findElementInXLSheet(getParameterService,"BankAdjustment");
	    btn_selectBankAdjustment= findElementInXLSheet(getParameterService,"btn_selectBankAdjustment");
	    btn_draftandnewBankAdjustment= findElementInXLSheet(getParameterService,"btn_draftandnewBankAdjustment");
	    NewBankAdjustmentPage= findElementInXLSheet(getParameterService,"NewBankAdjustmentPage");
	    btn_draftBankAdjustment= findElementInXLSheet(getParameterService,"btn_draftBankAdjustment");
	    DraftedBankAdjustmentPage= findElementInXLSheet(getParameterService,"DraftedBankAdjustmentPage");
	    btn_ReleaseBankAdjustmentPage= findElementInXLSheet(getParameterService,"btn_ReleaseBankAdjustmentPage");
	    ReleasedBankAdjustmentPage= findElementInXLSheet(getParameterService,"ReleasedBankAdjustmentPage");
	    btn_CopyfromPettyCash= findElementInXLSheet(getParameterService,"btn_CopyfromPettyCash");
	    pettycashName= findElementInXLSheet(getParameterService,"pettycashName");
	    btn_draftpettycash= findElementInXLSheet(getParameterService,"btn_draftpettycash");
	    DraftedPettyCashPage= findElementInXLSheet(getParameterService,"DraftedPettyCashPage");
	    btn_copyfromInboundPaymentAdvice= findElementInXLSheet(getParameterService,"btn_copyfromInboundPaymentAdvice");
	    InboundAdviseNo= findElementInXLSheet(getParameterService,"InboundAdviseNo");
	    selectInboundAdvice= findElementInXLSheet(getParameterService,"selectInboundAdvice");
	    btn_SalesMarkting= findElementInXLSheet(getParameterService,"btn_SalesMarkting");
	    btn_WarrantyProfile= findElementInXLSheet(getParameterService,"btn_WarrantyProfile");
	    Newbtn_WarrantyProfile= findElementInXLSheet(getParameterService,"Newbtn_WarrantyProfile");
	    profilecode= findElementInXLSheet(getParameterService,"profilecode");
	    profilename= findElementInXLSheet(getParameterService,"profilename");
	    btn_DraftWarrantyProfile= findElementInXLSheet(getParameterService,"btn_DraftWarrantyProfile");
	    btn_expandFirstPlusMarkHireAgreementDaily= findElementInXLSheet(getParameterService,"btn_expandFirstPlusMarkHireAgreementDaily");
	    btn_expandSecondPlusHireAgreement= findElementInXLSheet(getParameterService,"btn_expandSecondPlusHireAgreement");
	    btn_runDailyMethodHireAgreement= findElementInXLSheet(getParameterService,"btn_runDailyMethodHireAgreement");
	    
	    
	    //Service_Case_001
	    btn_FormCaseA= findElementInXLSheet(getParameterService,"FormCaseA");
	    btn_FormNewCaseA= findElementInXLSheet(getParameterService,"FormNewCaseA");
	    txt_pageHeaderA= findElementInXLSheet(getParameterService,"pageHeaderA");
	    
	    //Service_Case_002
	    txt_TitleA= findElementInXLSheet(getParameterService,"TitleA");
	    txt_OriginA= findElementInXLSheet(getParameterService,"OriginA");
	    txt_CustomerAccountA= findElementInXLSheet(getParameterService,"CustomerAccountA");
	    txt_ProductA= findElementInXLSheet(getParameterService,"ProductA");
	    txt_TagNoA= findElementInXLSheet(getParameterService,"TagNoA");
	    txt_TypeA= findElementInXLSheet(getParameterService,"TypeA");
	    txt_ServiceGroupA= findElementInXLSheet(getParameterService,"ServiceGroupA");
	    txt_PriorityA= findElementInXLSheet(getParameterService,"PriorityA");
	    txt_CurrencyA= findElementInXLSheet(getParameterService,"CurrencyA");
	    txt_BillingAddressA= findElementInXLSheet(getParameterService,"BillingAddressA");
	    txt_ShippingAddressA= findElementInXLSheet(getParameterService,"ShippingAddressA");
	    
	    //Service_Case_003
	    btn_DraftA= findElementInXLSheet(getParameterService,"DraftA");
	    btn_ReleaseA= findElementInXLSheet(getParameterService,"ReleaseA");
	    txt_DraftValidatorA= findElementInXLSheet(getParameterService,"DraftValidatorA");
	    txt_TitleValidatorA= findElementInXLSheet(getParameterService,"TitleValidatorA");
	    txt_OriginValidatorA= findElementInXLSheet(getParameterService,"OriginValidatorA");
	    txt_CustomerAccountValidatorA= findElementInXLSheet(getParameterService,"CustomerAccountValidatorA");
	    txt_ProductValidatorA= findElementInXLSheet(getParameterService,"ProductValidatorA");
	    txt_TagNoValidatorA= findElementInXLSheet(getParameterService,"TagNoValidatorA");
	    txt_TypeValidatorA= findElementInXLSheet(getParameterService,"TypeValidatorA");
	    txt_ServiceGroupValidatorA= findElementInXLSheet(getParameterService,"ServiceGroupValidatorA");
	    
	    //Service_Case_004
	    btn_CustomerAccountButtonA= findElementInXLSheet(getParameterService,"CustomerAccountButtonA");
	    txt_CustomerAccountTextA= findElementInXLSheet(getParameterService,"CustomerAccountTextA");
	    sel_CustomerAccountSelA= findElementInXLSheet(getParameterService,"CustomerAccountSelA");
	    btn_newWindowProdA= findElementInXLSheet(getParameterService,"newWindowProdA");
	    sel_newWindowProdSelA= findElementInXLSheet(getParameterService,"newWindowProdSelA");
	    txt_TitleTextA= findElementInXLSheet(getParameterService,"TitleTextA");
	    txt_OriginTextA= findElementInXLSheet(getParameterService,"OriginTextA");
	    txt_TypeTextA= findElementInXLSheet(getParameterService,"TypeTextA");
	    txt_ServiceGroupTextA= findElementInXLSheet(getParameterService,"ServiceGroupTextA");
	    txt_PriorityTextA= findElementInXLSheet(getParameterService,"PriorityTextA");
	    txt_PageDraft1A= findElementInXLSheet(getParameterService,"PageDraft1A");
	    txt_PageRelease1A= findElementInXLSheet(getParameterService,"PageRelease1A");
	    txt_PageDraft2A= findElementInXLSheet(getParameterService,"PageDraft2A");
	    txt_PageRelease2A= findElementInXLSheet(getParameterService,"PageRelease2A");

	    //Service_Case_005
	    txt_trackCodeA= findElementInXLSheet(getParameterService,"trackCodeA");
	    
	    //Service_Case_006
	    btn_RolecenterA= findElementInXLSheet(getParameterService,"RolecenterA");
	    btn_searchTagA= findElementInXLSheet(getParameterService,"searchTagA");
	    txt_searchTagtxtA= findElementInXLSheet(getParameterService,"searchTagtxtA");
	    btn_searchTagarrowA= findElementInXLSheet(getParameterService,"searchTagarrowA");
	    
	    //Service_Case_007
	    btn_CreateServiceQuotationBtnA= findElementInXLSheet(getParameterService,"CreateServiceQuotationBtnA");
	    btn_DroptoServiceCentreBtnA= findElementInXLSheet(getParameterService,"DroptoServiceCentreBtnA");
	    btn_ResponseDetailBtnA= findElementInXLSheet(getParameterService,"ResponseDetailBtnA");
	    btn_ActionBtnA= findElementInXLSheet(getParameterService,"ActionBtnA");
	    
	    //Service_Case_008
	    btn_DuplicateA= findElementInXLSheet(getParameterService,"DuplicateA");

	    //Service_Case_009
	    btn_DeleteBtnA= findElementInXLSheet(getParameterService,"DeleteBtnA");
	    btn_YesBtnA= findElementInXLSheet(getParameterService,"YesBtnA");
	    txt_pageDeletedA= findElementInXLSheet(getParameterService,"pageDeletedA");

	    //Service_Case_010
	    btn_EditA= findElementInXLSheet(getParameterService,"EditA");
	    btn_UpdateA= findElementInXLSheet(getParameterService,"UpdateA");
	    txt_OriginTextFieldA= findElementInXLSheet(getParameterService,"OriginTextFieldA");
	    txt_TypeTextFieldA= findElementInXLSheet(getParameterService,"TypeTextFieldA");
	    
	    //Service_Case_011
	    btn_UpdateAndNewA= findElementInXLSheet(getParameterService,"UpdateAndNewA");

	    //Service_Case_012
	    btn_DraftAndNewA= findElementInXLSheet(getParameterService,"DraftAndNewA");
	    btn_CaseFormByPageA= findElementInXLSheet(getParameterService,"CaseFormByPageA");
	    sel_DraftedCaseDocA= findElementInXLSheet(getParameterService,"DraftedCaseDocA");
	    
	    //Service_Case_013
	    btn_CopyFromA= findElementInXLSheet(getParameterService,"CopyFromA");
	    txt_CopyFromSerchtxtA= findElementInXLSheet(getParameterService,"CopyFromSerchtxtA");
	    sel_CopyFromSerchselA= findElementInXLSheet(getParameterService,"CopyFromSerchselA");
	    txt_TitleTextFieldA= findElementInXLSheet(getParameterService,"TitleTextFieldA");
	    txt_CustomerAccountTextFieldA= findElementInXLSheet(getParameterService,"CustomerAccountTextFieldA");
	    txt_ProductTextFieldA= findElementInXLSheet(getParameterService,"ProductTextFieldA");
	    txt_TagNoTextFieldA= findElementInXLSheet(getParameterService,"TagNoTextFieldA");
	    txt_ServiceGroupTextFieldA= findElementInXLSheet(getParameterService,"ServiceGroupTextFieldA");
	    txt_PriorityTextFieldA= findElementInXLSheet(getParameterService,"PriorityTextFieldA");

	    //Service_Case_014
	    btn_ReverseBtnA= findElementInXLSheet(getParameterService,"ReverseBtnA");
	    txt_pageReversedA= findElementInXLSheet(getParameterService,"pageReversedA");
	    
	    //Service_Case_015
	    btn_BillingAddressDeleteA= findElementInXLSheet(getParameterService,"BillingAddressDeleteA");
	    btn_ShippingAddressDeleteA= findElementInXLSheet(getParameterService,"ShippingAddressDeleteA");
	    txt_BillingAddressValidatorA= findElementInXLSheet(getParameterService,"BillingAddressValidatorA");
	    txt_ShippingAddressValidatorA= findElementInXLSheet(getParameterService,"ShippingAddressValidatorA");
	    
	    //Service_Case_016
	    txt_StageFieldA= findElementInXLSheet(getParameterService,"StageFieldA");
	    btn_StatusFieldArrowA= findElementInXLSheet(getParameterService,"StatusFieldArrowA");
	    txt_StatusFieldTxtA= findElementInXLSheet(getParameterService,"StatusFieldTxtA");

	    //Service_Case_017
	    txt_SalesUnitA= findElementInXLSheet(getParameterService,"SalesUnitA");
	    btn_ServiceDetailsA= findElementInXLSheet(getParameterService,"ServiceDetailsA");
	    btn_Plus1A= findElementInXLSheet(getParameterService,"Plus1A");
	    btn_Plus2A= findElementInXLSheet(getParameterService,"Plus2A");
	    btn_Plus3A= findElementInXLSheet(getParameterService,"Plus3A");
	    btn_Plus4A= findElementInXLSheet(getParameterService,"Plus4A");
	    btn_Plus5A= findElementInXLSheet(getParameterService,"Plus5A");
	    txt_Type1A= findElementInXLSheet(getParameterService,"Type1A");
	    txt_Type2A= findElementInXLSheet(getParameterService,"Type2A");
	    txt_Type3A= findElementInXLSheet(getParameterService,"Type3A");
	    txt_Type4A= findElementInXLSheet(getParameterService,"Type4A");
	    txt_Type5A= findElementInXLSheet(getParameterService,"Type5A");
	    txt_Type6A= findElementInXLSheet(getParameterService,"Type6A");
	    btn_ProSearch1A= findElementInXLSheet(getParameterService,"ProSearch1A");
	    btn_ProSearch2A= findElementInXLSheet(getParameterService,"ProSearch2A");
	    btn_ProSearch3A= findElementInXLSheet(getParameterService,"ProSearch3A");
	    btn_ProSearch4A= findElementInXLSheet(getParameterService,"ProSearch4A");
	    btn_ProSearch5A= findElementInXLSheet(getParameterService,"ProSearch5A");
	    btn_ProSearch6A= findElementInXLSheet(getParameterService,"ProSearch6A");
	    txt_ProtxtA= findElementInXLSheet(getParameterService,"ProtxtA");
	    sel_ProselA= findElementInXLSheet(getParameterService,"ProselA");
	    txt_UnitPrice1A= findElementInXLSheet(getParameterService,"UnitPrice1A");
	    txt_UnitPrice2A= findElementInXLSheet(getParameterService,"UnitPrice2A");
	    txt_UnitPrice3A= findElementInXLSheet(getParameterService,"UnitPrice3A");
	    txt_UnitPrice4A= findElementInXLSheet(getParameterService,"UnitPrice4A");
	    txt_UnitPrice5A= findElementInXLSheet(getParameterService,"UnitPrice5A");
	    txt_UnitPrice6A= findElementInXLSheet(getParameterService,"UnitPrice6A");
	    btn_ReferenceCodeLabourA= findElementInXLSheet(getParameterService,"ReferenceCodeLabourA");
	    btn_ReferenceCodeResourceA= findElementInXLSheet(getParameterService,"ReferenceCodeResourceA");
	    txt_ReferenceCodeExpenseA= findElementInXLSheet(getParameterService,"ReferenceCodeExpenseA");
	    txt_ReferenceCodeLabourtxtA= findElementInXLSheet(getParameterService,"ReferenceCodeLabourtxtA");
	    sel_ReferenceCodeLabourselA= findElementInXLSheet(getParameterService,"ReferenceCodeLabourselA");
	    txt_ReferenceCodeResourcetxtA= findElementInXLSheet(getParameterService,"ReferenceCodeResourcetxtA");
	    sel_ReferenceCodeResourceselA= findElementInXLSheet(getParameterService,"ReferenceCodeResourceselA");
	    btn_CheckoutA= findElementInXLSheet(getParameterService,"CheckoutA");
	    btn_VersionA= findElementInXLSheet(getParameterService,"VersionA");
	    btn_colorSelectedA= findElementInXLSheet(getParameterService,"colorSelectedA");
	    btn_ConfirmTikA= findElementInXLSheet(getParameterService,"ConfirmTikA");
	    btn_ConfirmbtnA= findElementInXLSheet(getParameterService,"ConfirmbtnA");
	    btn_GoToPageLinkA= findElementInXLSheet(getParameterService,"GoToPageLinkA");
	    btn_ServiceDetailsSOA= findElementInXLSheet(getParameterService,"ServiceDetailsSOA");
	    txt_pageConfirmedA= findElementInXLSheet(getParameterService,"pageConfirmedA");

	    //Service_Case_019
	    sel_CaseSerchSelA= findElementInXLSheet(getParameterService,"CaseSerchSelA");
	    btn_MiddleNavigationBarA= findElementInXLSheet(getParameterService,"MiddleNavigationBarA");
	    sel_CaseSerchSelNewA= findElementInXLSheet(getParameterService,"CaseSerchSelNewA");
	    
	    //Service_Case_020
	    txt_CaseSerchSelStatusA= findElementInXLSheet(getParameterService,"CaseSerchSelStatusA");

	    //Service_Case_021
	    txt_ConfirmNotificationTxt1A= findElementInXLSheet(getParameterService,"ConfirmNotificationTxt1A");
	    txt_ConfirmNotificationTxt2A= findElementInXLSheet(getParameterService,"ConfirmNotificationTxt2A");
	    
	    //Service_Case_024
	    btn_DroptoServiceCentreBtnAfterSQA= findElementInXLSheet(getParameterService,"DroptoServiceCentreBtnAfterSQA");
	    btn_EntutionHeaderA= findElementInXLSheet(getParameterService,"EntutionHeaderA");
	    btn_EstimatedServiceJobsA= findElementInXLSheet(getParameterService,"EstimatedServiceJobsA");
	    txt_EstimatedServiceJobsNoA= findElementInXLSheet(getParameterService,"EstimatedServiceJobsNoA");
	    btn_ServiceJobArrowA= findElementInXLSheet(getParameterService,"ServiceJobArrowA");
	    btn_OwnerBtnA= findElementInXLSheet(getParameterService,"OwnerBtnA");
	    txt_OwnerTxtA= findElementInXLSheet(getParameterService,"OwnerTxtA");
	    sel_OwnerSelA= findElementInXLSheet(getParameterService,"OwnerSelA");
	    btn_PricingProfileBtnA= findElementInXLSheet(getParameterService,"PricingProfileBtnA");
	    txt_PricingProfileTxtA= findElementInXLSheet(getParameterService,"PricingProfileTxtA");
	    sel_PricingProfileSelA= findElementInXLSheet(getParameterService,"PricingProfileSelA");
	    btn_ServiceLocationBtnA= findElementInXLSheet(getParameterService,"ServiceLocationBtnA");
	    txt_ServiceLocationTxtA= findElementInXLSheet(getParameterService,"ServiceLocationTxtA");
	    sel_ServiceLocationSelA= findElementInXLSheet(getParameterService,"ServiceLocationSelA");
	    
	    //Service_Case_030
	    btn_OpenServiceJobsA= findElementInXLSheet(getParameterService,"OpenServiceJobsA");
	    txt_OpenServiceJobsNoA= findElementInXLSheet(getParameterService,"OpenServiceJobsNoA");
	    btn_TaskDetailsA= findElementInXLSheet(getParameterService,"TaskDetailsA");
	    btn_TaskDetailsProBtnA= findElementInXLSheet(getParameterService,"TaskDetailsProBtnA");
	    txt_TaskDetailsProTxtA= findElementInXLSheet(getParameterService,"TaskDetailsProTxtA");
	    sel_TaskDetailsProSelA= findElementInXLSheet(getParameterService,"TaskDetailsProSelA");

	    
	    //Service_Case_032
	    btn_HireAgreementPlusNewA= findElementInXLSheet(getParameterService,"HireAgreementPlusNewA");
	    btn_RunButtonRelevantHireAgreementNewA= findElementInXLSheet(getParameterService,"RunButtonRelevantHireAgreementNewA");
	    txt_DropNotificationTxt3A= findElementInXLSheet(getParameterService,"DropNotificationTxt3A");

	    //Service_Case_033
	    btn_CloseBtnA= findElementInXLSheet(getParameterService,"CloseBtnA");
	    txt_cboxPOPSatisfactionA= findElementInXLSheet(getParameterService,"cboxPOPSatisfactionA");
	    txt_PageClosedA= findElementInXLSheet(getParameterService,"PageClosedA");
	    
	    //Service_Case_034
	    btn_ViewHistoryBtnA= findElementInXLSheet(getParameterService,"ViewHistoryBtnA");
	    txt_PendingCasesTileHeaderA= findElementInXLSheet(getParameterService,"PendingCasesTileHeaderA");
	    txt_CompletedCasesTileHeaderA= findElementInXLSheet(getParameterService,"CompletedCasesTileHeaderA");
	    txt_RegisteredProductsTileHeaderA= findElementInXLSheet(getParameterService,"RegisteredProductsTileHeaderA");

	    //Service_Case_035
	    btn_HoldBtnA= findElementInXLSheet(getParameterService,"HoldBtnA");
	    txt_HoldReasonA= findElementInXLSheet(getParameterService,"HoldReasonA");
	    txt_PageHoldA= findElementInXLSheet(getParameterService,"PageHoldA");
	    
	    //Service_Case_036
	    icn_HoldIconA= findElementInXLSheet(getParameterService,"HoldIconA");

	    //Service_Case_037
	    btn_UnHoldBtnA= findElementInXLSheet(getParameterService,"UnHoldBtnA");
	    
	    //Service_Case_038
	    icn_UnHoldIconA= findElementInXLSheet(getParameterService,"UnHoldIconA");
	    
	    //Service_Case_039
	    txt_BookNoTxtA= findElementInXLSheet(getParameterService,"BookNoTxtA");
	    txt_DocumentNoTxtA= findElementInXLSheet(getParameterService,"DocumentNoTxtA");
	    txt_DocumentDateTxtA= findElementInXLSheet(getParameterService,"DocumentDateTxtA");

	    //Service_Case_040
	    txt_ResolutionDescriptionA= findElementInXLSheet(getParameterService,"ResolutionDescriptionA");
	    txt_ResponseTypeA= findElementInXLSheet(getParameterService,"ResponseTypeA");
	    btn_ResponseDateA= findElementInXLSheet(getParameterService,"ResponseDateA");
	    txt_NextYearA= findElementInXLSheet(getParameterService,"NextYearA");
	    txt_SelectDateA= findElementInXLSheet(getParameterService,"SelectDateA");
	    txt_SatisfactionA= findElementInXLSheet(getParameterService,"SatisfactionA");
	    btn_ResponseDetailsUpdateA= findElementInXLSheet(getParameterService,"ResponseDetailsUpdateA");
	    
	    //Service_Case_041
	    btn_ResolutionTabA= findElementInXLSheet(getParameterService,"ResolutionTabA");
	    txt_ResolutionTextA= findElementInXLSheet(getParameterService,"ResolutionTextA");
	    btn_ResolutionTabSJOA= findElementInXLSheet(getParameterService,"ResolutionTabSJOA");
	    txt_ResolutionTextSJOA= findElementInXLSheet(getParameterService,"ResolutionTextSJOA");
	    
	    //Service_Case_042
	    btn_ResponseDetailBtnSJOA= findElementInXLSheet(getParameterService,"ResponseDetailBtnSJOA");
	    txt_ResponseTypeSJOA= findElementInXLSheet(getParameterService,"ResponseTypeSJOA");
	    
	    //Service_Case_044
	    txt_WarrantyStartA= findElementInXLSheet(getParameterService,"WarrantyStartA");
	    txt_WarrantyEndA= findElementInXLSheet(getParameterService,"WarrantyEndA");
	    
	    //Service_Case_045
	    btn_ServiceContractBtnA= findElementInXLSheet(getParameterService,"ServiceContractBtnA");
	    txt_ServiceContractTxtA= findElementInXLSheet(getParameterService,"ServiceContractTxtA");
	    sel_ServiceContractselA= findElementInXLSheet(getParameterService,"ServiceContractselA");
	    txt_ServiceContractTextFieldA= findElementInXLSheet(getParameterService,"ServiceContractTextFieldA");
	    
	    //Service_Case_046
	    txt_ServiceLevelAgreementTextFieldA= findElementInXLSheet(getParameterService,"ServiceLevelAgreementTextFieldA");

	    
	    //Service_Case_047
	    btn_knowledgeBaseBtnA= findElementInXLSheet(getParameterService,"knowledgeBaseBtnA");
	    txt_ProblemTxtA= findElementInXLSheet(getParameterService,"ProblemTxtA");
	    txt_QuestionTxtA= findElementInXLSheet(getParameterService,"QuestionTxtA");
	    txt_RequestTxtA= findElementInXLSheet(getParameterService,"RequestTxtA");
	    txt_InquiryTypeA= findElementInXLSheet(getParameterService,"InquiryTypeA");
	    
	    //Service_Case_048
	    icn_DraftIconA= findElementInXLSheet(getParameterService,"DraftIconA");

	    //Service_Case_049
	    icn_ReleasedIconA= findElementInXLSheet(getParameterService,"ReleasedIconA");
	    
	    //Service_Case_050
	    icn_ReverseIconA= findElementInXLSheet(getParameterService,"ReverseIconA");
	    
	    //Service_Case_051
	    icn_DeleteIconA= findElementInXLSheet(getParameterService,"DeleteIconA");
	    
	    //Service_Case_052
	    btn_OriginAddBtnA= findElementInXLSheet(getParameterService,"OriginAddBtnA");
	    btn_OriginAddPlusBtnA= findElementInXLSheet(getParameterService,"OriginAddPlusBtnA");
	    txt_OriginAddTextA= findElementInXLSheet(getParameterService,"OriginAddTextA");
	    
	    //Service_Case_053
	    btn_OriginInactiveA= findElementInXLSheet(getParameterService,"OriginInactiveA");

	    //Service_Case_054
	    sel_SerialNosSelA= findElementInXLSheet(getParameterService,"SerialNosSelA");
	    
	    //Service_Case_055
	    btn_AddNewCasePlusA= findElementInXLSheet(getParameterService,"AddNewCasePlusA");
	    btn_ParentCaseSerchBtnA= findElementInXLSheet(getParameterService,"ParentCaseSerchBtnA");
	    txt_ParentCaseSerchTxtA= findElementInXLSheet(getParameterService,"ParentCaseSerchTxtA");
	    sel_ParentCaseSerchSelA= findElementInXLSheet(getParameterService,"ParentCaseSerchSelA");
	    txt_ParentCaseTxtA= findElementInXLSheet(getParameterService,"ParentCaseTxtA");
	    
	    //Service_Case_056
	    btn_TypeAddBtnA= findElementInXLSheet(getParameterService,"TypeAddBtnA");
	    btn_TypeAddPlusA= findElementInXLSheet(getParameterService,"TypeAddPlusA");
	    txt_TypeAddTextA= findElementInXLSheet(getParameterService,"TypeAddTextA");
	    
	    //Service_Case_057
	    btn_TypeInactiveA= findElementInXLSheet(getParameterService,"TypeInactiveA");

	    //Service_Case_058
	    btn_ServiceGroupAddBtnA= findElementInXLSheet(getParameterService,"ServiceGroupAddBtnA");
	    btn_ServiceGroupAddPlusA= findElementInXLSheet(getParameterService,"ServiceGroupAddPlusA");
	    txt_ServiceGroupAddTextA = findElementInXLSheet(getParameterService,"ServiceGroupAddTextA");
	    
	    //Service_Case_059
	    btn_ServiceGroupInactiveA= findElementInXLSheet(getParameterService,"ServiceGroupInactiveA");
	    
	    //Service_Case_060
	    btn_AdministrationBtnA= findElementInXLSheet(getParameterService,"AdministrationBtnA");
	    btn_BalancingLevelSettingsBtnA= findElementInXLSheet(getParameterService,"BalancingLevelSettingsBtnA");
	    btn_SalesAndMarketingBtnA= findElementInXLSheet(getParameterService,"SalesAndMarketingBtnA");
	    txt_cboxAccountCodeNameA= findElementInXLSheet(getParameterService,"cboxAccountCodeNameA");
	    btn_SalesAndMarketingUpdateBtnA= findElementInXLSheet(getParameterService,"SalesAndMarketingUpdateBtnA");
	    
	    //Service_Case_061
	    sel_CustomerAccountSelCodeA= findElementInXLSheet(getParameterService,"CustomerAccountSelCodeA");

	    //Service_Case_062
	    sel_CustomerAccountSelNameA= findElementInXLSheet(getParameterService,"CustomerAccountSelNameA");
	    
	    //Service_Case_063
	    btn_InventoryBtnA= findElementInXLSheet(getParameterService,"InventoryBtnA");
	    txt_cboxProductCodeNameA= findElementInXLSheet(getParameterService,"cboxProductCodeNameA");
	    btn_InventoryUpdateBtnA= findElementInXLSheet(getParameterService,"InventoryUpdateBtnA");
	    
	    //Service_Case_066
	    btn_StopBtnA= findElementInXLSheet(getParameterService,"StopBtnA");
	    btn_CompleteProcessBtnA= findElementInXLSheet(getParameterService,"CompleteProcessBtnA");
	    btn_CompleteBtnA= findElementInXLSheet(getParameterService,"CompleteBtnA");
	    btn_CompleteYesBtnA= findElementInXLSheet(getParameterService,"CompleteYesBtnA");
	    txt_PostDateA= findElementInXLSheet(getParameterService,"PostDateA");
	    btn_PostDateApplyA= findElementInXLSheet(getParameterService,"PostDateApplyA");
	    txt_pageCompletedA= findElementInXLSheet(getParameterService,"pageCompletedA");
	    
	    //Service_HA_001
	    btn_HireAgreementBtnA= findElementInXLSheet(getParameterService,"HireAgreementBtnA");
	    btn_NewHireAgreementBtnA= findElementInXLSheet(getParameterService,"NewHireAgreementBtnA");
	    txt_NewHireAgreementPageA= findElementInXLSheet(getParameterService,"NewHireAgreementPageA");
	    
	    
	    //Service_HA_002
	    txt_AgreementNoStarA= findElementInXLSheet(getParameterService,"AgreementNoStarA");
	    txt_TitleStarA= findElementInXLSheet(getParameterService,"TitleStarA");
	    txt_AgreementDateStarA= findElementInXLSheet(getParameterService,"AgreementDateStarA");
	    txt_ContractGroupStarA= findElementInXLSheet(getParameterService,"ContractGroupStarA");
	    txt_ValidityPeriodForStarA= findElementInXLSheet(getParameterService,"ValidityPeriodForStarA");
	    txt_EffectiveFromStarA= findElementInXLSheet(getParameterService,"EffectiveFromStarA");
	    txt_EffectiveToStarA= findElementInXLSheet(getParameterService,"EffectiveToStarA");
	    txt_DescriptionStarA= findElementInXLSheet(getParameterService,"DescriptionStarA");
	    txt_SalesUnitStarA= findElementInXLSheet(getParameterService,"SalesUnitStarA");
	    txt_AccountOwnerStarA= findElementInXLSheet(getParameterService,"AccountOwnerStarA");
	    txt_CustomerAccountStarA= findElementInXLSheet(getParameterService,"CustomerAccountStarA");
	    txt_CurrencyStarA= findElementInXLSheet(getParameterService,"CurrencyStarA");
	    txt_ScheduleCodeStarA= findElementInXLSheet(getParameterService,"ScheduleCodeStarA");

	    //Service_HA_003
	    txt_AgreementNoValidationA= findElementInXLSheet(getParameterService,"AgreementNoValidationA");
	    txt_TitleValidationA= findElementInXLSheet(getParameterService,"TitleValidationA");
	    txt_AgreementDateValidationA= findElementInXLSheet(getParameterService,"AgreementDateValidationA");
	    txt_ContractGroupValidationA= findElementInXLSheet(getParameterService,"ContractGroupValidationA");
	    txt_EffectiveFromValidationA= findElementInXLSheet(getParameterService,"EffectiveFromValidationA");
	    txt_EffectiveToValidationA= findElementInXLSheet(getParameterService,"EffectiveToValidationA");
	    txt_DescriptionValidationA= findElementInXLSheet(getParameterService,"DescriptionValidationA");
	    txt_SalesUnitValidationA= findElementInXLSheet(getParameterService,"SalesUnitValidationA");
	    txt_AccountOwnerValidationA= findElementInXLSheet(getParameterService,"AccountOwnerValidationA");
	    txt_CustomerAccountValidationA= findElementInXLSheet(getParameterService,"CustomerAccountValidationA");
	    txt_ScheduleCodeValidationA= findElementInXLSheet(getParameterService,"ScheduleCodeValidationA");

	    //Service_HA_004
	    txt_AgreementNoTxtA= findElementInXLSheet(getParameterService,"AgreementNoTxtA");
	    txt_TitleTxtA= findElementInXLSheet(getParameterService,"TitleTxtA");
	    txt_ContractGroupTxtA= findElementInXLSheet(getParameterService,"ContractGroupTxtA");
	    txt_DescriptionTxtA= findElementInXLSheet(getParameterService,"DescriptionTxtA");
	    btn_CustomerAccountHABtnA= findElementInXLSheet(getParameterService,"CustomerAccountHABtnA");
	    txt_CustomerAccountHATxtA= findElementInXLSheet(getParameterService,"CustomerAccountHATxtA");
	    Sel_CustomerAccountHASelA= findElementInXLSheet(getParameterService,"CustomerAccountHASelA");
	    txt_SalesUnitHAA= findElementInXLSheet(getParameterService,"SalesUnitHAA");
	    btn_ScheduleCodeHABtnA= findElementInXLSheet(getParameterService,"ScheduleCodeHABtnA");
	    txt_ScheduleCodeHATxtA= findElementInXLSheet(getParameterService,"ScheduleCodeHATxtA");
	    Sel_ScheduleCodeHASelA= findElementInXLSheet(getParameterService,"ScheduleCodeHASelA");
	    btn_ResourceInformationTabA= findElementInXLSheet(getParameterService,"ResourceInformationTabA");
	    txt_TypeHAA= findElementInXLSheet(getParameterService,"TypeHAA");
	    btn_ReferenceCodeHABtnA= findElementInXLSheet(getParameterService,"ReferenceCodeHABtnA");
	    txt_ReferenceCodeHATxtA= findElementInXLSheet(getParameterService,"ReferenceCodeHATxtA");
	    Sel_ReferenceCodeHASelA= findElementInXLSheet(getParameterService,"ReferenceCodeHASelA");
	    btn_ResponsibleUserHABtnA= findElementInXLSheet(getParameterService,"ResponsibleUserHABtnA");
	    txt_ResponsibleUserHATxtA= findElementInXLSheet(getParameterService,"ResponsibleUserHATxtA");
	    Sel_ResponsibleUserHASelA= findElementInXLSheet(getParameterService,"ResponsibleUserHASelA");
	    txt_HireBasisA= findElementInXLSheet(getParameterService,"HireBasisA");
	    txt_OpeningUnitA= findElementInXLSheet(getParameterService,"OpeningUnitA");
	    btn_RateRangeA= findElementInXLSheet(getParameterService,"RateRangeA");
	    txt_MinimumUnitA= findElementInXLSheet(getParameterService,"MinimumUnitA");
	    txt_Rate1A= findElementInXLSheet(getParameterService,"Rate1A");
	    txt_Rate2A= findElementInXLSheet(getParameterService,"Rate2A");
	    btn_RateRangeSetupApplyA= findElementInXLSheet(getParameterService,"RateRangeSetupApplyA");
	    btn_RateRangeSetupPlusA= findElementInXLSheet(getParameterService,"RateRangeSetupPlusA");
	    txt_RateRangeSetupStartA= findElementInXLSheet(getParameterService,"RateRangeSetupStartA");
	    txt_RateRangeSetupEndA= findElementInXLSheet(getParameterService,"RateRangeSetupEndA");

	    //Service_HA_005
	    txt_ReferenceCodeErrorA= findElementInXLSheet(getParameterService,"ReferenceCodeErrorA");
	    txt_ServiceProductErrorA= findElementInXLSheet(getParameterService,"ServiceProductErrorA");
	    btn_ErrorBtnA= findElementInXLSheet(getParameterService,"ErrorBtnA");
	    txt_SelectaServiceProductErrorA= findElementInXLSheet(getParameterService,"SelectaServiceProductErrorA");
	    txt_SelectaResourceProductErrorA= findElementInXLSheet(getParameterService,"SelectaResourceProductErrorA");
	    txt_EnterOpeningUnitsErrorA= findElementInXLSheet(getParameterService,"EnterOpeningUnitsErrorA");

	    
	    //Service_HA_006
	    txt_TitleFieldA= findElementInXLSheet(getParameterService,"TitleFieldA");

	    //Service_HA_007
	    btn_DeleteHABtnA= findElementInXLSheet(getParameterService,"DeleteHABtnA");

	    //Service_HA_009
	    btn_ReverseHABtnA= findElementInXLSheet(getParameterService,"ReverseHABtnA");
	    btn_ActivitiesHABtnA= findElementInXLSheet(getParameterService,"ActivitiesHABtnA");
	    btn_ActualUpdateHABtnA= findElementInXLSheet(getParameterService,"ActualUpdateHABtnA");
	    btn_CloseAgreementHABtnA= findElementInXLSheet(getParameterService,"CloseAgreementHABtnA");
	    btn_ConvertToAcquireDepositHABtnA= findElementInXLSheet(getParameterService,"ConvertToAcquireDepositHABtnA");
	    
	    //Service_HA_011
	    btn_HireAgreementByPageBtnA= findElementInXLSheet(getParameterService,"HireAgreementByPageBtnA");
	    txt_HireAgreementSerchTxtA= findElementInXLSheet(getParameterService,"HireAgreementSerchTxtA");
	    sel_HireAgreementSerchSelA= findElementInXLSheet(getParameterService,"HireAgreementSerchSelA");
	    btn_DuplicateHABtnA= findElementInXLSheet(getParameterService,"DuplicateHABtnA");

	    //Service_HA_012
	    btn_DuplicateHABtnAfterReleaseA= findElementInXLSheet(getParameterService,"DuplicateHABtnAfterReleaseA");
	    
	    //Service_HA_016
	    btn_ResourcePlusBtnA= findElementInXLSheet(getParameterService,"ResourcePlusBtnA");
	    btn_ReferenceCodeHABtn2A= findElementInXLSheet(getParameterService,"ReferenceCodeHABtn2A");
	    btn_RateRange2A= findElementInXLSheet(getParameterService,"RateRange2A");
	    sel_ReferenceCodeHASel2A= findElementInXLSheet(getParameterService,"ReferenceCodeHASel2A");
	    txt_OpeningUnit2A= findElementInXLSheet(getParameterService,"OpeningUnit2A");
	    txt_Resource1A= findElementInXLSheet(getParameterService,"Resource1A");
	    txt_Resource2A= findElementInXLSheet(getParameterService,"Resource2A");
	    
	    //Service_HA_017
	    txt_CopyFromSerchHATxtA= findElementInXLSheet(getParameterService,"CopyFromSerchHATxtA");
	    sel_CopyFromSerchHASelA= findElementInXLSheet(getParameterService,"CopyFromSerchHASelA");
	    
	    //Service_HA_018
	    txt_cboxActualResourcesA= findElementInXLSheet(getParameterService,"cboxActualResourcesA");
	    
	    //Service_HA_019
	    txt_ActualUnitA= findElementInXLSheet(getParameterService,"ActualUnitA");
	    btn_CheckoutHAA= findElementInXLSheet(getParameterService,"CheckoutHAA");

	    //Service_HA_020
	    txt_ActualUpdateStatusA= findElementInXLSheet(getParameterService,"ActualUpdateStatusA");
	    
	    //Service_HA_022
	    txt_AgreementDateA= findElementInXLSheet(getParameterService,"AgreementDateA");
	    txt_EffectiveFromDateA= findElementInXLSheet(getParameterService,"EffectiveFromDateA");
	    txt_EffectiveToDateA= findElementInXLSheet(getParameterService,"EffectiveToDateA");
	    txt_BillingBasisA= findElementInXLSheet(getParameterService,"BillingBasisA");
	    Sel_ReferenceCodeHA2SelA= findElementInXLSheet(getParameterService,"ReferenceCodeHA2SelA");
	    txt_NextBillingDateA= findElementInXLSheet(getParameterService,"NextBillingDateA");
	    
	    //Service_HA_023
	    Sel_ReferenceCodeHA3SelA= findElementInXLSheet(getParameterService,"ReferenceCodeHA3SelA");
	    
	    //Service_HA_024
	    Sel_ReferenceCodeHA4SelA= findElementInXLSheet(getParameterService,"ReferenceCodeHA4SelA");
	    txt_EffectiveFromDateAfterReleaseA= findElementInXLSheet(getParameterService,"EffectiveFromDateAfterReleaseA");
	    
	    //Service_HA_025_026
	    Sel_ResponsibleUserHA2SelA= findElementInXLSheet(getParameterService,"ResponsibleUserHA2SelA");
	    btn_LogOutBtnA= findElementInXLSheet(getParameterService,"LogOutBtnA");
	    btn_SignOutBtnA= findElementInXLSheet(getParameterService,"SignOutBtnA");
	    btn_ScheduledJobsA= findElementInXLSheet(getParameterService,"ScheduledJobsA");
	    btn_scheduleCodePlusA= findElementInXLSheet(getParameterService,"scheduleCodePlusA");
	    btn_HireAgreementPlusA= findElementInXLSheet(getParameterService,"HireAgreementPlusA");
	    txt_HANoA= findElementInXLSheet(getParameterService,"HANoA");
	    btn_checkboxRelevantHireAgreementA= findElementInXLSheet(getParameterService,"checkboxRelevantHireAgreementA");
	    btn_RunButtonRelevantHireAgreementA= findElementInXLSheet(getParameterService,"RunButtonRelevantHireAgreementA");
	    btn_LinkReleasedServiceInvoiceA= findElementInXLSheet(getParameterService,"LinkReleasedServiceInvoiceA");

	    //Service_HA_028
	    txt_ActualUpdatedAmountA= findElementInXLSheet(getParameterService,"ActualUpdatedAmountA");
	    txt_ServiceInvoiceTotalHeaderA= findElementInXLSheet(getParameterService,"ServiceInvoiceTotalHeaderA");
	    
	    //Service_HA_029
	    txt_ActualUnit2A= findElementInXLSheet(getParameterService,"ActualUnit2A");
	    txt_ActualUpdatedAmount2A= findElementInXLSheet(getParameterService,"ActualUpdatedAmount2A");
	    txt_ServiceInvoiceSubTotalA= findElementInXLSheet(getParameterService,"ServiceInvoiceSubTotalA");
	    btn_ServiceDetailsTabA= findElementInXLSheet(getParameterService,"ServiceDetailsTabA");
	    
	    //Service_HA_030_031
	    txt_RateRangeSetupEnd1A= findElementInXLSheet(getParameterService,"RateRangeSetupEnd1A");
	    txt_ResourceUnitPrice1A= findElementInXLSheet(getParameterService,"ResourceUnitPrice1A");
	    txt_ResourceUnitPrice2A= findElementInXLSheet(getParameterService,"ResourceUnitPrice2A");
	    txt_ReverceValidatorA= findElementInXLSheet(getParameterService,"ReverceValidatorA");

	    //Service_HA_032
	    txt_LastBillDateA= findElementInXLSheet(getParameterService,"LastBillDateA");
	    
	    //Service_HA_035_036_037
	    btn_OkBtnA= findElementInXLSheet(getParameterService,"OkBtnA");
	    txt_CloseValidatorA= findElementInXLSheet(getParameterService,"CloseValidatorA");
	    btn_CloseAgreementBtnAfterServiceInvoiceA= findElementInXLSheet(getParameterService,"CloseAgreementBtnAfterServiceInvoiceA");
	    txt_HireAgreementStatusByPageA= findElementInXLSheet(getParameterService,"HireAgreementStatusByPageA");
	    
	    //Service_HA_038_039
	    txt_BaseAmountA= findElementInXLSheet(getParameterService,"BaseAmountA");

	    //Service_HA_040
	    txt_AgreementDateValidationEffectiveFromDateA= findElementInXLSheet(getParameterService,"AgreementDateValidationEffectiveFromDateA");
	    
	    //Service_HA_041
	    txt_EffectiveToA= findElementInXLSheet(getParameterService,"EffectiveToA");
	    txt_EffectiveToMonthA= findElementInXLSheet(getParameterService,"EffectiveToMonthA");
	    txt_EffectiveToYearA= findElementInXLSheet(getParameterService,"EffectiveToYearA");
	    txt_EffectiveToDatePreA= findElementInXLSheet(getParameterService,"EffectiveToDatePreA");

	    
	    //Service_HA_042
	    txt_OverDueTxtA= findElementInXLSheet(getParameterService,"OverDueTxtA");
	    
	    //Service_HA_044
	    btn_DuplicateHireAgreementA= findElementInXLSheet(getParameterService,"DuplicateHireAgreementA");
	    txt_HireAgreementDuplicateValidatorA= findElementInXLSheet(getParameterService,"HireAgreementDuplicateValidatorA");
	    
	    //Service_HA_046
	    txt_ResponsibleUserValidatorA= findElementInXLSheet(getParameterService,"ResponsibleUserValidatorA");

	    //Service_HA_049
	    txt_ScheduleCodeA= findElementInXLSheet(getParameterService,"ScheduleCodeA");
	    txt_ScheduleCodeNoA= findElementInXLSheet(getParameterService,"ScheduleCodeNoA");
	    
	    //Service_HA_051
	    Sel_ReferenceCodeHA5SelA= findElementInXLSheet(getParameterService,"ReferenceCodeHA5SelA");
	    
	    //Service_HA_052
	    txt_PreparerA= findElementInXLSheet(getParameterService,"PreparerA");

	    //Service_HA_054
	    txt_CurrencyCboxA= findElementInXLSheet(getParameterService,"CurrencyCboxA");
	    txt_AgreedRateA= findElementInXLSheet(getParameterService,"AgreedRateA");

	    //Service_HA_058
	    txt_PostBusinessUnitTxtA= findElementInXLSheet(getParameterService,"PostBusinessUnitTxtA");
	    sel_PostBusinessUnitSelA= findElementInXLSheet(getParameterService,"PostBusinessUnitSelA");
	    txt_PostBusinessUnitTxtAfterReleaseA= findElementInXLSheet(getParameterService,"PostBusinessUnitTxtAfterReleaseA");
	    
	    //Service_HA_059
	    btn_GeneralBtnA= findElementInXLSheet(getParameterService,"GeneralBtnA");
	    btn_cboxConfigResourceA= findElementInXLSheet(getParameterService,"cboxConfigResourceA");
	    btn_GeneralUpdateA= findElementInXLSheet(getParameterService,"GeneralUpdateA");
	    txt_ResourceCodeAndDescriptionA= findElementInXLSheet(getParameterService,"ResourceCodeAndDescriptionA");
	    
	    //Service_HA_060
	    sel_ResourceCodeHASel2A= findElementInXLSheet(getParameterService,"ResourceCodeHASel2A");
	    txt_ResourceCodeA= findElementInXLSheet(getParameterService,"ResourceCodeA");
	    
	    //Service_HA_061
	    sel_ResourceCodeHASel3A= findElementInXLSheet(getParameterService,"ResourceCodeHASel3A");
	    txt_ResourceDescriptionA= findElementInXLSheet(getParameterService,"ResourceDescriptionA");
	    
	    //Service_EAR_001
	    btn_EmployeeAdvanceRequestBtnA= findElementInXLSheet(getParameterService,"EmployeeAdvanceRequestBtnA");

	    //Service_EAR_002
	    txt_EmployeeAdvanceRequestByPageA= findElementInXLSheet(getParameterService,"EmployeeAdvanceRequestByPageA");
	    
	    //Service_EAR_003
	    txt_DocNoLableA= findElementInXLSheet(getParameterService,"DocNoLableA");
	    txt_DocDateLableA= findElementInXLSheet(getParameterService,"DocDateLableA");
	    txt_EmployeeCodeLableA= findElementInXLSheet(getParameterService,"EmployeeCodeLableA");
	    txt_ExpenseTypeLableA= findElementInXLSheet(getParameterService,"ExpenseTypeLableA");
	    txt_PurposeLableA= findElementInXLSheet(getParameterService,"PurposeLableA");
	    txt_PostBusinessUnitCodeLableA= findElementInXLSheet(getParameterService,"PostBusinessUnitCodeLableA");
	    txt_PostBusinessUnitNameLableA= findElementInXLSheet(getParameterService,"PostBusinessUnitNameLableA");

	    //Service_EAR_004
	    btn_NewEmployeeAdvanceRequestBtnA= findElementInXLSheet(getParameterService,"NewEmployeeAdvanceRequestBtnA");
	    txt_NewEmployeeAdvanceRequestPageHeaderA= findElementInXLSheet(getParameterService,"NewEmployeeAdvanceRequestPageHeaderA");
	    
	    //Service_EAR_005
	    txt_EmployeeTextFieldA= findElementInXLSheet(getParameterService,"EmployeeTextFieldA");
	    txt_ExpenseTypeTextFieldA= findElementInXLSheet(getParameterService,"ExpenseTypeTextFieldA");
	    txt_PurposeTextFieldA= findElementInXLSheet(getParameterService,"PurposeTextFieldA");
	    txt_AdditionalRemarkTextFieldA= findElementInXLSheet(getParameterService,"AdditionalRemarkTextFieldA");
	    txt_CurrencyTextFieldA= findElementInXLSheet(getParameterService,"CurrencyTextFieldA");
	    txt_AmountTextFieldA= findElementInXLSheet(getParameterService,"AmountTextFieldA");
	    
	    //Service_EAR_006
	    txt_ExpenseTypeTextFieldValidatorA= findElementInXLSheet(getParameterService,"ExpenseTypeTextFieldValidatorA");
	    txt_PurposeTextFieldValidatorA= findElementInXLSheet(getParameterService,"PurposeTextFieldValidatorA");
	    txt_AmountTextFieldValidatorA= findElementInXLSheet(getParameterService,"AmountTextFieldValidatorA");

	    //Service_EAR_007
	    txt_PurposeTextFieldDraftValidatorA= findElementInXLSheet(getParameterService,"PurposeTextFieldDraftValidatorA");
	    txt_AmountTextFieldDraftValidatorA= findElementInXLSheet(getParameterService,"AmountTextFieldDraftValidatorA");
	    
	    //Service_EAR_008
	    btn_EmployeeBtnEARA= findElementInXLSheet(getParameterService,"EmployeeBtnEARA");
	    txt_EmployeeSearchTextEARA= findElementInXLSheet(getParameterService,"EmployeeSearchTextEARA");
	    sel_EmployeeSearchSelEARA= findElementInXLSheet(getParameterService,"EmployeeSearchSelEARA");
	    
	    //Service_EAR_009_010
	    txt_EmployeeTextFieldAfterValueA= findElementInXLSheet(getParameterService,"EmployeeTextFieldAfterValueA");
	    txt_ExpenseTypeTextFieldAfterValueA= findElementInXLSheet(getParameterService,"ExpenseTypeTextFieldAfterValueA");
	    txt_PurposeTextFieldAfterValueA= findElementInXLSheet(getParameterService,"PurposeTextFieldAfterValueA");
	    txt_AdditionalRemarkTextFieldAfterValueA= findElementInXLSheet(getParameterService,"AdditionalRemarkTextFieldAfterValueA");
	    txt_CurrencyTextFieldAfterValueA= findElementInXLSheet(getParameterService,"CurrencyTextFieldAfterValueA");
	    txt_AmountTextFieldAfterValueA= findElementInXLSheet(getParameterService,"AmountTextFieldAfterValueA");

	    //Service_EAR_012
	    txt_pageDeletedNewA= findElementInXLSheet(getParameterService,"pageDeletedNewA");
	    
	    //Service_EAR_013
	    txt_pageReversedNewA= findElementInXLSheet(getParameterService,"pageReversedNewA");
	    btn_reverceApplyA= findElementInXLSheet(getParameterService,"reverceApplyA");
	    
	    //Service_EAR_014_015_016_017
	    btn_PayEmployeeAdvanceA= findElementInXLSheet(getParameterService,"PayEmployeeAdvanceA");
	    txt_NewOutboundPaymentAdvicePageHeaderA= findElementInXLSheet(getParameterService,"NewOutboundPaymentAdvicePageHeaderA");
	    txt_CustomerAccountOPAA= findElementInXLSheet(getParameterService,"CustomerAccountOPAA");
	    txt_ReferenceTypeOPAA= findElementInXLSheet(getParameterService,"ReferenceTypeOPAA");
	    txt_RefDocNoOPAA= findElementInXLSheet(getParameterService,"RefDocNoOPAA");
	    txt_DescriptionOPAA= findElementInXLSheet(getParameterService,"DescriptionOPAA");
	    txt_cboxCurrencyOPAA= findElementInXLSheet(getParameterService,"cboxCurrencyOPAA");
	    txt_AmountOPAA= findElementInXLSheet(getParameterService,"AmountOPAA");
	    
	    //Service_EAR_018_019_020
	    txt_OutboundPaymentAdviceRecord1A= findElementInXLSheet(getParameterService,"OutboundPaymentAdviceRecord1A");
	    btn_ReferenceDetailsTabA= findElementInXLSheet(getParameterService,"ReferenceDetailsTabA");
	    
	    //Service_EAR_021_022_023_024_025
	    btn_ConvertToOutboundPaymentBtnA= findElementInXLSheet(getParameterService,"ConvertToOutboundPaymentBtnA");
	    txt_NewOutboundPaymentPageHeaderA= findElementInXLSheet(getParameterService,"NewOutboundPaymentPageHeaderA");
	    txt_AmountOPA= findElementInXLSheet(getParameterService,"AmountOPA");
	    btn_DetailsTabA= findElementInXLSheet(getParameterService,"DetailsTabA");
	    txt_PaidAmountA= findElementInXLSheet(getParameterService,"PaidAmountA");
	    txt_OutboundPaymentAdviceRecord2A= findElementInXLSheet(getParameterService,"OutboundPaymentAdviceRecord2A");
	    txt_BalanceAmountA= findElementInXLSheet(getParameterService,"BalanceAmountA");
	    txt_DueAmountA= findElementInXLSheet(getParameterService,"DueAmountA");
	    txt_PaidAmountEARA= findElementInXLSheet(getParameterService,"PaidAmountEARA");
	    btn_SummaryTabA= findElementInXLSheet(getParameterService,"SummaryTabA");
	    
	    //Service_EAR_027
	    btn_OutboundPaymentCheckBoxA= findElementInXLSheet(getParameterService,"OutboundPaymentCheckBoxA");
	    btn_OutboundPaymentRowErrorA =findElementInXLSheet(getParameterService,"OutboundPaymentRowErrorA");
	    txt_PartialPaymentErrorA= findElementInXLSheet(getParameterService,"PartialPaymentErrorA");
	    
	    //Service_EAR_028_029_030
	    btn_OutboundPaymentAdviceRecordCheckboxA= findElementInXLSheet(getParameterService,"OutboundPaymentAdviceRecordCheckboxA");
	    btn_ViewAllPendingBtnA= findElementInXLSheet(getParameterService,"ViewAllPendingBtnA");
	    txt_EmployeeAdvanceRequestSearchTxtA= findElementInXLSheet(getParameterService,"EmployeeAdvanceRequestSearchTxtA");
	    btn_EmployeeAdvanceRequestSearchBtnA= findElementInXLSheet(getParameterService,"EmployeeAdvanceRequestSearchBtnA");
	    sel_EmployeeAdvanceRequestSearchSelA= findElementInXLSheet(getParameterService,"EmployeeAdvanceRequestSearchSelA");
	    
	    //Service_EAR_031to037
	    btn_OutboundPaymentAdviceByPageHeaderA= findElementInXLSheet(getParameterService,"OutboundPaymentAdviceByPageHeaderA");
	    txt_OutboundPaymentAdviceSearchTxtA= findElementInXLSheet(getParameterService,"OutboundPaymentAdviceSearchTxtA");
	    btn_OutboundPaymentAdviceSearchBtnA= findElementInXLSheet(getParameterService,"OutboundPaymentAdviceSearchBtnA");
	    sel_OutboundPaymentAdviceSearchSelA= findElementInXLSheet(getParameterService,"OutboundPaymentAdviceSearchSelA");
	    txt_OutboundPaymentRecord1A= findElementInXLSheet(getParameterService,"OutboundPaymentRecord1A");
	    txt_OutboundPaymentRecord2A= findElementInXLSheet(getParameterService,"OutboundPaymentRecord2A");
	    txt_OutboundPaymentHeaderTotalA= findElementInXLSheet(getParameterService,"OutboundPaymentHeaderTotalA");
	    
	    //Service_DWS_001
	    btn_DailyWorkSheetBtnA= findElementInXLSheet(getParameterService,"DailyWorkSheetBtnA");
	    
	    //Service_DWS_002
	    txt_DailyWorksheetByPageA= findElementInXLSheet(getParameterService,"DailyWorksheetByPageA");

	    //Service_DWS_003
	    txt_DailyWorkSheetNoColumnA= findElementInXLSheet(getParameterService,"DailyWorkSheetNoColumnA");
	    txt_ReferenceNoColumnA= findElementInXLSheet(getParameterService,"ReferenceNoColumnA");
	    txt_DocDateColumnA= findElementInXLSheet(getParameterService,"DocDateColumnA");
	    txt_CreatedUserColumnA= findElementInXLSheet(getParameterService,"CreatedUserColumnA");
	    txt_CreatedDateColumnA= findElementInXLSheet(getParameterService,"CreatedDateColumnA");
	    txt_PostedDateColumnA= findElementInXLSheet(getParameterService,"PostedDateColumnA");
	    txt_PostBusinessUnitColumnA= findElementInXLSheet(getParameterService,"PostBusinessUnitColumnA");
	    
	    //Service_DWS_004_005_006
	    btn_NewDailyWorksheetBtnA= findElementInXLSheet(getParameterService,"NewDailyWorksheetBtnA");
	    txt_StartNewJourneyHeaderA= findElementInXLSheet(getParameterService,"StartNewJourneyHeaderA");
	    btn_DailyWorkSheetServiceBtnA= findElementInXLSheet(getParameterService,"DailyWorkSheetServiceBtnA");
	    btn_DailyWorkSheetProjectBtnA= findElementInXLSheet(getParameterService,"DailyWorkSheetProjectBtnA");
	    
	    //Service_DWS_007to012
	    txt_WorkedHoursServiceNewPageHeaderA= findElementInXLSheet(getParameterService,"WorkedHoursServiceNewPageHeaderA");
	    txt_JobNoFieldA= findElementInXLSheet(getParameterService,"JobNoFieldA");
	    btn_AddNewRecordPlusA= findElementInXLSheet(getParameterService,"AddNewRecordPlusA");
	    txt_AddNewJobHeaderA= findElementInXLSheet(getParameterService,"AddNewJobHeaderA");
	    txt_JobNoStarA= findElementInXLSheet(getParameterService,"JobNoStarA");
	    txt_EmployeeStarA= findElementInXLSheet(getParameterService,"EmployeeStarA");
	    btn_AddNewJobUpdateA= findElementInXLSheet(getParameterService,"AddNewJobUpdateA");
	    txt_JobNoValidatorA= findElementInXLSheet(getParameterService,"JobNoValidatorA");
	    btn_EmployeeBtnErrorA= findElementInXLSheet(getParameterService,"EmployeeBtnErrorA");
	    txt_EmployeeIsRequiredErrorA= findElementInXLSheet(getParameterService,"EmployeeIsRequiredErrorA");
	    
	    //Service_DWS_013to019
	    txt_AddNewJobDateA= findElementInXLSheet(getParameterService,"AddNewJobDateA");
	    btn_ServiceJobOrderSearchBtnA= findElementInXLSheet(getParameterService,"ServiceJobOrderSearchBtnA");
	    txt_ServiceJobOrderLookupHeaderA= findElementInXLSheet(getParameterService,"ServiceJobOrderLookupHeaderA");
	    txt_ReleseServiceJobOrderNosA= findElementInXLSheet(getParameterService,"ReleseServiceJobOrderNosA");
	    btn_ServiceJobOrderLookupSearchBtnA= findElementInXLSheet(getParameterService,"ServiceJobOrderLookupSearchBtnA");
	    txt_ServiceJobOrderLookupSearchTxtA= findElementInXLSheet(getParameterService,"ServiceJobOrderLookupSearchTxtA");
	    txt_JobNoTextFieldA= findElementInXLSheet(getParameterService,"JobNoTextFieldA");
	    btn_TimeInA= findElementInXLSheet(getParameterService,"TimeInA");
	    sel_TimeInSelA= findElementInXLSheet(getParameterService,"TimeInSelA");
	    btn_TimeInSelOkA= findElementInXLSheet(getParameterService,"TimeInSelOkA");
	    btn_TimeOutA= findElementInXLSheet(getParameterService,"TimeOutA");
	    sel_TimeOutSelA= findElementInXLSheet(getParameterService,"TimeOutSelA");
	    btn_TimeOutSelOkA= findElementInXLSheet(getParameterService,"TimeOutSelOkA");
	    
	    //Service_DWS_020to025
	    btn_EmployeeSearchBtnA= findElementInXLSheet(getParameterService,"EmployeeSearchBtnA");
	    txt_EmployeeLookupHeaderA= findElementInXLSheet(getParameterService,"EmployeeLookupHeaderA");
	    btn_EmployeeLookupSearchBtnA= findElementInXLSheet(getParameterService,"EmployeeLookupSearchBtnA");
	    txt_ReleseEmployeeNamesA= findElementInXLSheet(getParameterService,"ReleseEmployeeNamesA");
	    txt_EmployeeLookupSearchTxtA= findElementInXLSheet(getParameterService,"EmployeeLookupSearchTxtA");
	    txt_EmployeeTextFieldDWA= findElementInXLSheet(getParameterService,"EmployeeTextFieldDWA");
	    txt_EmployeeTextFieldDWValuesA= findElementInXLSheet(getParameterService,"EmployeeTextFieldDWValuesA");
	    
	    //Service_DWS_026to028
	    txt_DefineaValidRateProfileErrorA= findElementInXLSheet(getParameterService,"DefineaValidRateProfileErrorA");
	    btn_EmployeeDeleteBtnA= findElementInXLSheet(getParameterService,"EmployeeDeleteBtnA");
	    
	    //Service_DWS_029to041
	    btn_EmployeeAddPlusBtnA= findElementInXLSheet(getParameterService,"EmployeeAddPlusBtnA");
	    txt_EmployeeAddedNewLineA= findElementInXLSheet(getParameterService,"EmployeeAddedNewLineA");
	    btn_JobDateA= findElementInXLSheet(getParameterService,"JobDateA");
	    txt_JobDatePickerA= findElementInXLSheet(getParameterService,"JobDatePickerA");
	    sel_JobDateSelA= findElementInXLSheet(getParameterService,"JobDateSelA");
	    txt_TimeInTimePickerA= findElementInXLSheet(getParameterService,"TimeInTimePickerA");
	    sel_TimeInSelectedA= findElementInXLSheet(getParameterService,"TimeInSelectedA");
	    txt_TimeInTextFieldA= findElementInXLSheet(getParameterService,"TimeInTextFieldA");
	    txt_TimeOutTimePickerA= findElementInXLSheet(getParameterService,"TimeOutTimePickerA");
	    txt_WorkedHoursA= findElementInXLSheet(getParameterService,"WorkedHoursA");
	    
	    //Service_DWS_042to045
	    txt_workedHoursValidatorA= findElementInXLSheet(getParameterService,"workedHoursValidatorA");
	    txt_ReferenceNoTextFieldA= findElementInXLSheet(getParameterService,"ReferenceNoTextFieldA");
	    btn_UpdateAndNewDWSBtnA= findElementInXLSheet(getParameterService,"UpdateAndNewDWSBtnA");
	    btn_UpdateDWSBtnA= findElementInXLSheet(getParameterService,"UpdateDWSBtnA");
	    btn_CloseDWSBtnA= findElementInXLSheet(getParameterService,"CloseDWSBtnA");
	    txt_ReferenceNoDWSTextA= findElementInXLSheet(getParameterService,"ReferenceNoDWSTextA");
	    txt_EmployeeNameDWSTextA= findElementInXLSheet(getParameterService,"EmployeeNameDWSTextA");
	    txt_JobNoDWSTextA= findElementInXLSheet(getParameterService,"JobNoDWSTextA");
	    
	    //Service_DWS_048to050
	    btn_EmployeeSearchBtn2A= findElementInXLSheet(getParameterService,"EmployeeSearchBtn2A");
	    txt_EmployeeDuplicateErrorA= findElementInXLSheet(getParameterService,"EmployeeDuplicateErrorA");
	    txt_EmployeeAllocatedErrorA= findElementInXLSheet(getParameterService,"EmployeeAllocatedErrorA");
	    
	    //Service_DWS_051to053
	    btn_NewEmployeeBtnA= findElementInXLSheet(getParameterService,"NewEmployeeBtnA");
	    txt_EmployeeCodeA= findElementInXLSheet(getParameterService,"EmployeeCodeA");
	    txt_EmployeeNoA= findElementInXLSheet(getParameterService,"EmployeeNoA");
	    txt_FullNameA= findElementInXLSheet(getParameterService,"FullNameA");
	    txt_NameWithInitialsA= findElementInXLSheet(getParameterService,"NameWithInitialsA");
	    txt_EmployeeGroupA= findElementInXLSheet(getParameterService,"EmployeeGroupA");
	    btn_WorkingCalendarSearchBtnA= findElementInXLSheet(getParameterService,"WorkingCalendarSearchBtnA");
	    txt_WorkingCalendarSearchTxtA= findElementInXLSheet(getParameterService,"WorkingCalendarSearchTxtA");
	    sel_WorkingCalendarSearchSelA= findElementInXLSheet(getParameterService,"WorkingCalendarSearchSelA");
	    btn_RateProfileSearchBtnA= findElementInXLSheet(getParameterService,"RateProfileSearchBtnA");
	    txt_RateProfileSearchTxtA= findElementInXLSheet(getParameterService,"RateProfileSearchTxtA");
	    sel_RateProfileSearchSelA= findElementInXLSheet(getParameterService,"RateProfileSearchSelA");
	    btn_PayableAccountTickA= findElementInXLSheet(getParameterService,"PayableAccountTickA");
	    sel_TimeInSelNewA= findElementInXLSheet(getParameterService,"TimeInSelNewA");
	    sel_TimeOutSelNewA= findElementInXLSheet(getParameterService,"TimeOutSelNewA");
	    txt_JobNoDWSTextNewA= findElementInXLSheet(getParameterService,"JobNoDWSTextNewA");
	    txt_EmployeeNameDWSTextNewA= findElementInXLSheet(getParameterService,"EmployeeNameDWSTextNewA");
	    
	    //Service_DWS_063to65
	    btn_DeleteRecordRaw1A= findElementInXLSheet(getParameterService,"DeleteRecordRaw1A");
	    btn_DeleteRecordRaw2A= findElementInXLSheet(getParameterService,"DeleteRecordRaw2A");
	    btn_RecordErrorBtnA= findElementInXLSheet(getParameterService,"RecordErrorBtnA");
	    txt_EmployeeNameIsRequiredErrorA= findElementInXLSheet(getParameterService,"EmployeeNameIsRequiredErrorA");
	    txt_JobDateIsRequiredErrorA= findElementInXLSheet(getParameterService,"JobDateIsRequiredErrorA");
	    txt_JobNoIsRequiredErrorA= findElementInXLSheet(getParameterService,"JobNoIsRequiredErrorA");

	    //Service_DWS_070to71
	    btn_RemindersBtnA= findElementInXLSheet(getParameterService,"RemindersBtnA");
	    btn_HistoryBtnA= findElementInXLSheet(getParameterService,"HistoryBtnA");
	    btn_ActivitiesBtnA= findElementInXLSheet(getParameterService,"ActivitiesBtnA");
	    btn_JournalBtnA= findElementInXLSheet(getParameterService,"JournalBtnA");
	    
	    //Service_DWS_077to078
	    btn_ServiceJobOrderBtnA= findElementInXLSheet(getParameterService,"ServiceJobOrderBtnA");
	    txt_ServiceJobOrderSerchByPageTxtA= findElementInXLSheet(getParameterService,"ServiceJobOrderSerchByPageTxtA");
	    sel_ServiceJobOrderSerchByPageSelA= findElementInXLSheet(getParameterService,"ServiceJobOrderSerchByPageSelA");
	    txt_DailyWorkSheetByPageTxtA= findElementInXLSheet(getParameterService,"DailyWorkSheetByPageTxtA");
	    sel_DailyWorkSheetByPageSelA= findElementInXLSheet(getParameterService,"DailyWorkSheetByPageSelA");
	    btn_OverviewTabA= findElementInXLSheet(getParameterService,"OverviewTabA");
	    btn_LabourTabA= findElementInXLSheet(getParameterService,"LabourTabA");
	    txt_LabourTabWorksheetNoA= findElementInXLSheet(getParameterService,"LabourTabWorksheetNoA");

	    //Service_DWS_080to082
	    btn_HourlyRateBtnA= findElementInXLSheet(getParameterService,"HourlyRateBtnA");
	    txt_WorkedHoursRecodeTxtA= findElementInXLSheet(getParameterService,"WorkedHoursRecodeTxtA");
	    txt_HourlyRateWorkedHoursRecodeTxtA= findElementInXLSheet(getParameterService,"HourlyRateWorkedHoursRecodeTxtA");
	    
	    //Service_DWS_084
	    txt_FirstPostDateA= findElementInXLSheet(getParameterService,"FirstPostDateA");
	    txt_SecondPostDateA= findElementInXLSheet(getParameterService,"SecondPostDateA");

	    //Service_DWS_087
	    txt_PostDateHeaderA= findElementInXLSheet(getParameterService,"PostDateHeaderA");
	    txt_PostDateTextA= findElementInXLSheet(getParameterService,"PostDateTextA");
	    txt_PostDateYearTextA= findElementInXLSheet(getParameterService,"PostDateYearTextA");
	    sel_PostDateSelA= findElementInXLSheet(getParameterService,"PostDateSelA");
	    
	    //Service_DWS_088to092
	    btn_PostsBtnA= findElementInXLSheet(getParameterService,"PostsBtnA");
	    btn_TandCBtnA= findElementInXLSheet(getParameterService,"TandCBtnA");
	    btn_AttachmentsBtnA= findElementInXLSheet(getParameterService,"AttachmentsBtnA");
	    btn_DailyWorkSheetHeaderBtnA= findElementInXLSheet(getParameterService,"DailyWorkSheetHeaderBtnA");

	    //Service_DWS_093to096
	    txt_WorkedHoursProjectNewPageHeaderA= findElementInXLSheet(getParameterService,"WorkedHoursProjectNewPageHeaderA");
	    btn_ProjectSearchBtnA= findElementInXLSheet(getParameterService,"ProjectSearchBtnA");
	    txt_ProjectSearchTxtA= findElementInXLSheet(getParameterService,"ProjectSearchTxtA");
	    sel_ProjectSearchSelA= findElementInXLSheet(getParameterService,"ProjectSearchSelA");
	    
	    //Service_DWS_099
	    btn_DWSProjectBtnA= findElementInXLSheet(getParameterService,"DWSProjectBtnA");
	    txt_DWSProjectSearchByPageTxtA= findElementInXLSheet(getParameterService,"DWSProjectSearchByPageTxtA");
	    sel_DWSProjectSearchByPageSelA= findElementInXLSheet(getParameterService,"DWSProjectSearchByPageSelA");
	    btn_DWSProjectOverviewTabA= findElementInXLSheet(getParameterService,"DWSProjectOverviewTabA");
	    txt_DWSProjectDWSNoA= findElementInXLSheet(getParameterService,"DWSProjectDWSNoA");

	    //Service_DWS_100
	    txt_DWSEmployeeNameLine1A= findElementInXLSheet(getParameterService,"DWSEmployeeNameLine1A");
	    txt_DWSEmployeeNameLine2A= findElementInXLSheet(getParameterService,"DWSEmployeeNameLine2A");
	    txt_DWSProNoLine1A= findElementInXLSheet(getParameterService,"DWSProNoLine1A");
	    txt_DWSProNoLine2A= findElementInXLSheet(getParameterService,"DWSProNoLine2A");
	    
	    //Service_DWS_102
	    btn_DWSNewProjectBtnA= findElementInXLSheet(getParameterService,"DWSNewProjectBtnA");
	    txt_DWSProjectCodeA= findElementInXLSheet(getParameterService,"DWSProjectCodeA");
	    txt_DWSProjectTitleA= findElementInXLSheet(getParameterService,"DWSProjectTitleA");
	    txt_DWSProjectGroupA= findElementInXLSheet(getParameterService,"DWSProjectGroupA");
	    btn_DWSResponsiblePersonBtnA= findElementInXLSheet(getParameterService,"DWSResponsiblePersonBtnA");
	    txt_DWSResponsiblePersonTxtA= findElementInXLSheet(getParameterService,"DWSResponsiblePersonTxtA");
	    sel_DWSResponsiblePersonSelA= findElementInXLSheet(getParameterService,"DWSResponsiblePersonSelA");
	    txt_DWSAssignedBusinessUnitsA= findElementInXLSheet(getParameterService,"DWSAssignedBusinessUnitsA");
	    btn_DWSPricingProfileBtnA= findElementInXLSheet(getParameterService,"DWSPricingProfileBtnA");
	    txt_DWSPricingProfileTxtA= findElementInXLSheet(getParameterService,"DWSPricingProfileTxtA");
	    sel_DWSPricingProfileSelA= findElementInXLSheet(getParameterService,"DWSPricingProfileSelA");
	    btn_DWSCustomerAccountBtnA= findElementInXLSheet(getParameterService,"DWSCustomerAccountBtnA");
	    txt_DWSCustomerAccountTxtA= findElementInXLSheet(getParameterService,"DWSCustomerAccountTxtA");
	    sel_DWSCustomerAccountSelA= findElementInXLSheet(getParameterService,"DWSCustomerAccountSelA");
	    txt_DWScboxSalesUnitA= findElementInXLSheet(getParameterService,"DWScboxSalesUnitA");
	    txt_DWScboxAccOwnerA= findElementInXLSheet(getParameterService,"DWScboxAccOwnerA");
	    btn_DWSRequestedStartDateA= findElementInXLSheet(getParameterService,"DWSRequestedStartDateA");
	    sel_DWSRequestedStartDateselA= findElementInXLSheet(getParameterService,"DWSRequestedStartDateselA");
	    btn_DWSProjectLocationBtnA= findElementInXLSheet(getParameterService,"DWSProjectLocationBtnA");
	    txt_DWSProjectLocationTxtA= findElementInXLSheet(getParameterService,"DWSProjectLocationTxtA");
	    sel_DWSProjectLocationSelA= findElementInXLSheet(getParameterService,"DWSProjectLocationSelA");
	    btn_DWSWorkBreakdownBtnA= findElementInXLSheet(getParameterService,"DWSWorkBreakdownBtnA");
	    btn_DWSWorkBreakdownAddTaskBtnA= findElementInXLSheet(getParameterService,"DWSWorkBreakdownAddTaskBtnA");
	    txt_DWSWorkBreakdownTaskNameA= findElementInXLSheet(getParameterService,"DWSWorkBreakdownTaskNameA");
	    btn_DWSTaskUpdateA= findElementInXLSheet(getParameterService,"DWSTaskUpdateA");
	    btn_DWSTaskdropdownA= findElementInXLSheet(getParameterService,"DWSTaskdropdownA");
	    btn_DWSReleaseTaskA= findElementInXLSheet(getParameterService,"DWSReleaseTaskA");
	    btn_DWSUpdateTaskPOCA= findElementInXLSheet(getParameterService,"DWSUpdateTaskPOCA");
	    txt_DWScboxTaskPOCA= findElementInXLSheet(getParameterService,"DWScboxTaskPOCA");
	    txt_DWStxtTaskPOCA= findElementInXLSheet(getParameterService,"DWStxtTaskPOCA");
	    btn_DWSProjectCompleteBtnA= findElementInXLSheet(getParameterService,"DWSProjectCompleteBtnA");
	}
	
	public static void readData() throws Exception
	{

		String computerName = System.getenv().get("COMPUTERNAME");
		if (computerName.equals("DESKTOP-0C81D38") || computerName.equals("MADHUSHAN-LAPdvdv")) {
			siteURL = findElementInXLSheet(getDataService, "site url external");
		} else {
			siteURL = findElementInXLSheet(getDataService, "site url");
		}
		txt_UserNameDataService=findElementInXLSheet(getDataService,"txt_UserNameDataServicemodule");
		txt_PasswordDataService=findElementInXLSheet(getDataService,"txt_PasswordDataServicemodule");
		txt_ServiceJobOrderTitle=findElementInXLSheet(getDataService,"txt_ServiceJobOrderTitle");
		txt_ServiceJobOrderAccount=findElementInXLSheet(getDataService,"txt_ServiceJobOrderAccount");
	    txt_ServiceGroup=findElementInXLSheet(getDataService,"txt_ServiceGroup");
	    txt_ServiceJobOrderDescription=findElementInXLSheet(getDataService,"txt_ServiceJobOrderDescription");
	    txt_Template=findElementInXLSheet(getDataService,"txt_Template");
	    txt_ServiceLocation=findElementInXLSheet(getDataService,"txt_ServiceLocation");
	    txt_Accountname=findElementInXLSheet(getDataService,"txt_Accountname");
	    txt_Labour=findElementInXLSheet(getDataService,"txt_Labour");
	    txt_SerialSpecificProduct= findElementInXLSheet(getDataService,"txt_SerialSpecificProduct");
	    txt_BatchSpecificProduct= findElementInXLSheet(getDataService,"txt_BatchSpecificProduct");
	    txt_FIFOProduct= findElementInXLSheet(getDataService,"txt_FIFOProduct");
	    txt_serviceProduct= findElementInXLSheet(getDataService,"txt_serviceProduct");
	    txt_Resources= findElementInXLSheet(getDataService,"txt_Resources");
	    txt_tasktyperesource= findElementInXLSheet(getDataService,"txt_tasktyperesource");
	    txt_EstimatedQtyLabour= findElementInXLSheet(getDataService,"txt_EstimatedQtyLabour");
	    txt_EstimatedQtyResourse= findElementInXLSheet(getDataService,"txt_EstimatedQtyResourse");
	    txt_LabourEmployee= findElementInXLSheet(getDataService,"txt_LabourEmployee");
	    txt_Resource= findElementInXLSheet(getDataService,"txt_Resource");
	    txt_EstimatedQtyserial= findElementInXLSheet(getDataService,"txt_EstimatedQtyserial");
	    txt_EstimatedQtyBatch= findElementInXLSheet(getDataService,"txt_EstimatedQtyBatch");
	    txt_EstimatedQtyFIFO= findElementInXLSheet(getDataService,"txt_EstimatedQtyFIFO");
	    txt_EstimatedQtyservice= findElementInXLSheet(getDataService,"txt_EstimatedQtyservice");
	    txt_EstimatedQtysubcontract= findElementInXLSheet(getDataService,"txt_EstimatedQtysubcontract");
	    txt_EstimatedQtyExpense= findElementInXLSheet(getDataService,"txt_EstimatedQtyExpense");
	    txt_InternalDispatchOrderShippingAddress= findElementInXLSheet(getDataService,"txt_InternalDispatchOrderShippingAddress");
	    txt_quty= findElementInXLSheet(getDataService,"txt_quty");
	    txt_FifoActualQty= findElementInXLSheet(getDataService,"txt_FifoActualQty");
	    txt_templateJOBNO= findElementInXLSheet(getDataService,"txt_templateJOBNO");
	    txt_searchjobno= findElementInXLSheet(getDataService,"txt_searchjobno");
	    txt_searchemployee= findElementInXLSheet(getDataService,"txt_searchemployee");
	    txt_searchvendor= findElementInXLSheet(getDataService,"txt_searchvendor");
	    txt_VendorRefrenceNo= findElementInXLSheet(getDataService,"txt_VendorRefrenceNo");
	    txt_BillingAddress= findElementInXLSheet(getDataService,"txt_BillingAddress");
	    txt_BillableQtyInputProduct= findElementInXLSheet(getDataService,"txt_BillableQtyInputProduct");
	    txt_SalesUnit= findElementInXLSheet(getDataService,"txt_SalesUnit");
	    txt_BillableQtyInvoiceResource= findElementInXLSheet(getDataService,"txt_BillableQtyInvoiceResource");
	    txt_UnitPriceInputSerial= findElementInXLSheet(getDataService,"txt_UnitPriceInputSerial");
	    txt_UnitPriceSubContract= findElementInXLSheet(getDataService,"txt_UnitPriceSubContract");
	    txt_UnitPriceService= findElementInXLSheet(getDataService,"txt_UnitPriceService");
	    txt_UnitPriceBatch= findElementInXLSheet(getDataService,"txt_UnitPriceBatch");
	    txt_UnitPriceFifo= findElementInXLSheet(getDataService,"txt_UnitPriceFifo");
	    txt_UnitPriceLabour= findElementInXLSheet(getDataService,"txt_UnitPriceLabour");
	    txt_ToWareHouse= findElementInXLSheet(getDataService,"txt_ToWareHouse");
	    txt_AnalysisCode= findElementInXLSheet(getDataService,"txt_AnalysisCode");
	    txt_Amount= findElementInXLSheet(getDataService,"txt_AnalysisCode");
	    txt_Percentage= findElementInXLSheet(getDataService,"txt_Percentage");
	    txt_CostAssignmentType= findElementInXLSheet(getDataService,"txt_CostAssignmentType");
	    txt_Payee= findElementInXLSheet(getDataService,"txt_Payee");
	    txt_PettyCashAccount= findElementInXLSheet(getDataService,"txt_PettyCashAccount");
	    txt_PettyCashDescription= findElementInXLSheet(getDataService,"txt_PettyCashDescription");
	    txt_Employee= findElementInXLSheet(getDataService,"txt_Employee");
	    txt_TypePaymentInformation= findElementInXLSheet(getDataService,"txt_TypePaymentInformation");
	    txt_ExtendNarration= findElementInXLSheet(getDataService,"txt_ExtendNarration");
	    txt_VendorReferenceNo= findElementInXLSheet(getDataService,"txt_VendorReferenceNo");
	    txt_bank= findElementInXLSheet(getDataService,"txt_bank");
	    txt_bankaccountno= findElementInXLSheet(getDataService,"txt_bankaccountno");
	    txt_analysiscode= findElementInXLSheet(getDataService,"txt_analysiscode");
	    txt_AmountBankAdjustment= findElementInXLSheet(getDataService,"txt_AmountBankAdjustment");
	    txt_customer= findElementInXLSheet(getDataService,"txt_customer");
	    txt_BillingAddressPurchaseInvoice= findElementInXLSheet(getDataService,"txt_BillingAddressPurchaseInvoice");
	    txt_PercentagePurchaseInvoice= findElementInXLSheet(getDataService,"txt_PercentagePurchaseInvoice");
	    txt_Discount= findElementInXLSheet(getDataService,"txt_Discount");
	    txt_Costcenter= findElementInXLSheet(getDataService,"txt_Costcenter");
	    txt_searchjobnoOutboundLoanOrder= findElementInXLSheet(getDataService,"txt_searchjobnoOutboundLoanOrder");
	    txt_NewSerialSpecificProduct= findElementInXLSheet(getDataService,"txt_NewSerialSpecificProduct");
	    txt_NewEstimatedQtyserial= findElementInXLSheet(getDataService,"txt_NewEstimatedQtyserial");
	    txt_SerialProductOutboundShipment= findElementInXLSheet(getDataService,"txt_SerialProductOutboundShipment");
	    txt_ServiceContractTitle= findElementInXLSheet(getDataService,"txt_ServiceContractTitle");
	    txt_ContractGroup= findElementInXLSheet(getDataService,"txt_ContractGroup");
	    txt_employeecode= findElementInXLSheet(getDataService,"txt_employeecode");
	    txt_employeename= findElementInXLSheet(getDataService,"txt_employeename");
	    txt_namewithintials= findElementInXLSheet(getDataService,"txt_namewithintials");
	    txt_employeegroup= findElementInXLSheet(getDataService,"txt_employeegroup");
	    txt_Description= findElementInXLSheet(getDataService,"txt_Description");
	    txt_SerialSpecificProduct1= findElementInXLSheet(getDataService,"txt_SerialSpecificProduct1");
	    txt_product1= findElementInXLSheet(getDataService,"txt_product1");
	    txt_SceduleCode= findElementInXLSheet(getDataService,"txt_SceduleCode");
	    txt_responsibleuser= findElementInXLSheet(getDataService,"txt_responsibleuser");
	    txt_tagno= findElementInXLSheet(getDataService,"txt_tagno");
	    txt_EstimatedCost= findElementInXLSheet(getDataService,"txt_EstimatedCost");
	    txt_pricingprofile= findElementInXLSheet(getDataService,"txt_pricingprofile");
	    txt_Referencecode= findElementInXLSheet(getDataService,"txt_Referencecode");
	    txt_lotProduct= findElementInXLSheet(getDataService,"txt_lotProduct");
	    txt_BatchSerialProduct= findElementInXLSheet(getDataService,"txt_BatchSerialProduct");
	    txt_title= findElementInXLSheet(getDataService,"txt_title");
	    txt_PMCategory= findElementInXLSheet(getDataService,"txt_PMCategory");
	    txt_PMcount= findElementInXLSheet(getDataService,"txt_PMcount");
	    txt_PMinterval= findElementInXLSheet(getDataService,"txt_PMinterval");
	    txt_SceduleCodeFirstDayFM= findElementInXLSheet(getDataService,"txt_SceduleCodeFirstDayFM");
	    txt_currency= findElementInXLSheet(getDataService,"txt_currency");
	    txt_BillingAddress1= findElementInXLSheet(getDataService,"txt_BillingAddress1");
	    txt_ShipingAddress1= findElementInXLSheet(getDataService,"txt_ShipingAddress1");
	    txt_unitprice= findElementInXLSheet(getDataService,"txt_unitprice");
	    txt_AgreementNo= findElementInXLSheet(getDataService,"txt_AgreementNo");
	    txt_DescriptionHireAgreement= findElementInXLSheet(getDataService,"txt_DescriptionHireAgreement");
	    txt_TitleNewCase= findElementInXLSheet(getDataService,"txt_TitleNewCase");
	    txt_tagnoDirectSR= findElementInXLSheet(getDataService,"txt_tagnoDirectSR");
	    txt_Doctile= findElementInXLSheet(getDataService,"txt_Doctile");
	   
	    //Service_Case_001
		usernamedataA= findElementInXLSheet(getDataService,"usernamedataA");
		passworddataA= findElementInXLSheet(getDataService,"passworddataA");
		
		//Service_Case_004
		CustomerAccountTextA= findElementInXLSheet(getDataService,"CustomerAccountTextA");

		//Service_Case_013
		CopyFromSerchtxtA= findElementInXLSheet(getDataService,"CopyFromSerchtxtA");
		CopyFromSerchtitleA= findElementInXLSheet(getDataService,"CopyFromSerchtitleA");
		OriginTextFieldA= findElementInXLSheet(getDataService,"OriginTextFieldA");
		ProductTextFieldA= findElementInXLSheet(getDataService,"ProductTextFieldA");
		TagNoTextFieldA= findElementInXLSheet(getDataService,"TagNoTextFieldA");
		TypeTextFieldA= findElementInXLSheet(getDataService,"TypeTextFieldA");
		ServiceGroupTextFieldA= findElementInXLSheet(getDataService,"ServiceGroupTextFieldA");
		PriorityTextFieldA= findElementInXLSheet(getDataService,"PriorityTextFieldA");

		
		//Service_Case_017
		InputProductA= findElementInXLSheet(getDataService,"InputProductA");
		LabourProductA= findElementInXLSheet(getDataService,"LabourProductA");
		ResourceProductA= findElementInXLSheet(getDataService,"ResourceProductA");
		ExpenseProductA= findElementInXLSheet(getDataService,"ExpenseProductA");
		SubContractProductA= findElementInXLSheet(getDataService,"SubContractProductA");
		ServiceProductA= findElementInXLSheet(getDataService,"ServiceProductA");
		SalesUnitA= findElementInXLSheet(getDataService,"SalesUnitA");
		ReferenceCodeLabourtxtA= findElementInXLSheet(getDataService,"ReferenceCodeLabourtxtA");
		ReferenceCodeResourcetxtA= findElementInXLSheet(getDataService,"ReferenceCodeResourcetxtA");
		ReferenceCodeExpenseA= findElementInXLSheet(getDataService,"ReferenceCodeExpenseA");

		//Service_Case_024
		OwnerTxtA= findElementInXLSheet(getDataService,"OwnerTxtA");
		PricingProfileTxtA= findElementInXLSheet(getDataService,"PricingProfileTxtA");
		ServiceLocationTxtA= findElementInXLSheet(getDataService,"ServiceLocationTxtA");
		
		//Service_Case_045_046
		ServiceContractTxtA= findElementInXLSheet(getDataService,"ServiceContractTxtA");
		ServiceAgreementTxtA= findElementInXLSheet(getDataService,"ServiceAgreementTxtA");

		//Service_Case_060
		CustomerAccountCodeAndDicriptionA= findElementInXLSheet(getDataService,"CustomerAccountCodeAndDicriptionA");
		
		//Service_Case_061
		CustomerAccountCodeA= findElementInXLSheet(getDataService,"CustomerAccountCodeA");
		
		//Service_Case_062
		CustomerAccountDicriptionA= findElementInXLSheet(getDataService,"CustomerAccountDicriptionA");

		//Service_Case_063
		ProductCodeAndDicriptionA= findElementInXLSheet(getDataService,"ProductCodeAndDicriptionA");
		
		//Service_Case_064
		ProductCodeA= findElementInXLSheet(getDataService,"ProductCodeA");
		
		//Service_Case_065
		ProductDicriptionA= findElementInXLSheet(getDataService,"ProductDicriptionA");

		//Service_HA_004
		ScheduleCodeHATxtA= findElementInXLSheet(getDataService,"ScheduleCodeHATxtA");
		ResponsibleUserHATxtA= findElementInXLSheet(getDataService,"ResponsibleUserHATxtA");
		ReferenceCodeHA1TxtA= findElementInXLSheet(getDataService,"ReferenceCodeHA1TxtA");
		
		//Service_HA_016
		ReferenceCodeHA2TxtA= findElementInXLSheet(getDataService,"ReferenceCodeHA2TxtA");

		//Service_HA_017
		HireAgreementNoA= findElementInXLSheet(getDataService,"HireAgreementNoA");

		//Service_HA_022
		ScheduleCodeHA2TxtA= findElementInXLSheet(getDataService,"ScheduleCodeHA2TxtA");

		//Service_HA_023
		ScheduleCodeHA3TxtA= findElementInXLSheet(getDataService,"ScheduleCodeHA3TxtA");

		//Service_HA_024
		ScheduleCodeHA4TxtA= findElementInXLSheet(getDataService,"ScheduleCodeHA4TxtA");
		
		//Service_HA_025
		ResponsibleUserHA2TxtA= findElementInXLSheet(getDataService,"ResponsibleUserHA2TxtA");
		NewUserNameA= findElementInXLSheet(getDataService,"NewUserNameA");
		NewPasswordA= findElementInXLSheet(getDataService,"NewPasswordA");

	    //Service_HA_051
	    ScheduleCodeHA5TxtA= findElementInXLSheet(getDataService,"ScheduleCodeHA5TxtA");
	    
	    //Service_HA_058
	    PostBusinessUnitTxtA= findElementInXLSheet(getDataService,"PostBusinessUnitTxtA");
	    PostBusinessUnitAfterReleaseTxtA= findElementInXLSheet(getDataService,"PostBusinessUnitAfterReleaseTxtA");
	    
	    //Service_HA_059
	    ResourceCodeAndDescriptionA= findElementInXLSheet(getDataService,"ResourceCodeAndDescriptionA");
	    
	    //Service_HA_060
	    ResourceCodeA= findElementInXLSheet(getDataService,"ResourceCodeA");
	    
	    //Service_HA_061
	    ResourceDescriptionA= findElementInXLSheet(getDataService,"ResourceDescriptionA");

		//Service_EAR_008
		EmployeeSearchTextEAR1A= findElementInXLSheet(getDataService,"EmployeeSearchTextEAR1A");
		
		//Service_EAR_009
		EmployeeSearchTextEAR2A= findElementInXLSheet(getDataService,"EmployeeSearchTextEAR2A");
		
		//Service_DWS_013to019
		ReleseServiceJobOrderNos1A= findElementInXLSheet(getDataService,"ReleseServiceJobOrderNos1A");
		ReleseServiceJobOrderNos2A= findElementInXLSheet(getDataService,"ReleseServiceJobOrderNos2A");
		ReleseServiceJobOrderNos3A= findElementInXLSheet(getDataService,"ReleseServiceJobOrderNos3A");
		RevercedServiceJobOrderNos1A= findElementInXLSheet(getDataService,"RevercedServiceJobOrderNos1A");
		RevercedServiceJobOrderNos2A= findElementInXLSheet(getDataService,"RevercedServiceJobOrderNos2A");
		RevercedServiceJobOrderNos3A= findElementInXLSheet(getDataService,"RevercedServiceJobOrderNos3A");
		DeletedServiceJobOrderNos1A= findElementInXLSheet(getDataService,"DeletedServiceJobOrderNos1A");
		DeletedServiceJobOrderNos2A= findElementInXLSheet(getDataService,"DeletedServiceJobOrderNos2A");
		DeletedServiceJobOrderNos3A= findElementInXLSheet(getDataService,"DeletedServiceJobOrderNos3A");

		//Service_DWS_020to025
		ReleseEmployeeNames1A= findElementInXLSheet(getDataService,"ReleseEmployeeNames1A");
		ReleseEmployeeNames2A= findElementInXLSheet(getDataService,"ReleseEmployeeNames2A");
		InactiveEmployeeNames1A= findElementInXLSheet(getDataService,"InactiveEmployeeNames1A");
		InactiveEmployeeNames2A= findElementInXLSheet(getDataService,"InactiveEmployeeNames2A");

		//Service_DWS_093to096
		ReleseProjectTxt1A= findElementInXLSheet(getDataService,"ReleseProjectTxt1A");

		//Service_DWS_100
		ReleseProjectTxt2A= findElementInXLSheet(getDataService,"ReleseProjectTxt2A");

		//Service_DWS_102
		DWSProjectGroupA= findElementInXLSheet(getDataService,"DWSProjectGroupA");
		DWSPricingProfileSelA= findElementInXLSheet(getDataService,"DWSPricingProfileSelA");
		DWSAssignedBusinessUnitsA= findElementInXLSheet(getDataService,"DWSAssignedBusinessUnitsA");
	}


}
