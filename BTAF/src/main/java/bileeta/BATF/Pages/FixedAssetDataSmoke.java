package bileeta.BATF.Pages;

import bileeta.BTAF.Utilities.TestBase;

public class FixedAssetDataSmoke extends TestBase {


	protected static String siteURL;
	protected static String siteLogo;
	protected static String txt_username;
	protected static String txt_password;
	protected static String btn_login;
	protected static String lnk_home;
	
	/*Navigate to side menu*/
	protected static String navigateMenu;
	protected static String sideNavBar;
	
	/*Navigate to fixed asset module*/
	protected static String fixedAsset_btn; 
	protected static String fixedAssetLogo;
	
	/*Navigate to procurement and PO*/
	protected static String btn_procurement;
	protected static String label_procurement;
	protected static String btn_purchaseOrder;
	protected static String label_PurchaseOrder;
	protected static String btn_newPurchaseOrder;
	protected static String tab_journey;
	protected static String btn_newServiceJourney;
	protected static String label_PurchaseOrder1;
	
	/*Filling mandatory fields*/
	protected static String btn_vendorSearch;
	protected static String txt_vendorSearch;
	protected static String doubleClickVendor;
	protected static String txt_venderRefNo;
	protected static String btn_productSearch;
	protected static String txt_productSearch;
	protected static String doubleClickProduct;
	protected static String txt_qty;
	protected static String txt_unitPrice;
	protected static String btn_advance;
	protected static String btn_fixedAss;
	protected static String label_advance;
	protected static String label_assetGrp;
	protected static String btn_assetGrp;
	protected static String btn_apply;
	protected static String btn_checkout;
	protected static String btn_sendReview;
	protected static String btn_entution;
	protected static String btn_taskEvent;
	protected static String btn_approvals;
	protected static String btn_PO;
	protected static String btn_draft;
	protected static String btn_release;
	protected static String txt_PONumber;
	protected static String txt_approveNo;
	protected static String btn_approval;
	protected static String btn_releaseProc;
	protected static String btn_checkoutPro;
	protected static String btn_draftPro;
	protected static String btn_releaseButton;
	protected static String btn_actionPro;
	protected static String btn_journal;
	protected static String label_purchaseOrderPage;
	protected static String label_purchaseInvoicePage;
	protected static String label_falabel;
	protected static String label_fixedAssetInformation;
	protected static String label_purchaseInvoicePageRelease;
	protected static String label_purchaseOrderPageNew;
	protected static String label_purchaseOrderPageRelease;
	protected static String label_gotopage;
	protected static String label_journalEntryDetails;
	protected static String label_fixedAssetInformationRelease;
	protected static String label_fixedAssetInformationlabel;

	
	protected static String btn_inventory;
	protected static String label_inventory;
	protected static String btn_internalOrder;
	protected static String label_InernalOrder;
	protected static String btn_newInternalOrder;
	protected static String tab_internalOrderjourney;
	protected static String btn_newInternalServiceJourney;
	protected static String label_InternalOrder1;
	protected static String txt_title;
	protected static String btn_requester;
	protected static String txt_requesterSearch;
	protected static String doubleClickRequester;
	protected static String btn_date;
	protected static String btn_internalProductSearch;
	protected static String txt_internalProductSearch;
	protected static String doubleClickinternalProduct;
	protected static String txt_internalqty;
	protected static String btn_advanceInventory;
	protected static String label_advanceInventory;
	protected static String btn_fixedAssInternal;
	protected static String label_assetGrpInternal;
	protected static String btn_assetGrpInternal;
	protected static String btn_applyInternal;
	protected static String btn_goToPageInternal;
	protected static String txt_shipping;
	protected static String btn_draftdispatch;
	protected static String btn_releasedispatch;
	protected static String btn_action;
	protected static String btn_journalEntry;
	protected static String txt_FA;
	protected static String btn_fixedAssSegment;
	protected static String txt_segmentNo;
	protected static String btn_fixedAssetInfo;
	protected static String txt_fixedAssetInfo;
	protected static String btn_ok;
	protected static String btn_close;
	protected static String doubleClickSegmentNo;
	protected static String btn_endRelease;
	protected static String label_InternalOrder;
	protected static String label_InternalDispatchOrder;
	protected static String label_journalEntry;
	protected static String label_InternalOrderRelease;
	

	protected static String btn_project;
	protected static String label_project;
	protected static String btn_subProject;
	protected static String label_subProject;
	protected static String btn_newProject;
	protected static String txt_project;
	protected static String txt_projectCode;
	protected static String txt_projectTitle;
	protected static String txt_projectGroup;
	protected static String txt_postingMethod;
	protected static String txt_projectType;
	protected static String txt_assetGroup;
	protected static String btn_responsiblePersonSearch;
	protected static String txt_responsiblePersonSearch;
	protected static String doubleClickresponsiblePerson;
	protected static String txt_bussinessUnit;
	protected static String btn_pricingProfile;
	protected static String txt_pricingProfile;
	protected static String txt_startDate;
	protected static String txt_endDate;
	protected static String doubleClick_startDate;
	protected static String doubleClick_endDate;
	protected static String btn_projectLocation;
	protected static String txt_projectLocation;
	protected static String doubleClickProjectLocation;
	protected static String txt_inputWarehouse;
	protected static String txt_WIPWarehouse;
	protected static String btn_edit;
	protected static String btn_workBreakdown;
	protected static String btn_plus;
	protected static String txt_projectCodeNo;
	protected static String txt_taskName;
	protected static String btn_update;
	protected static String btn_expand;
	protected static String btn_releaseTask;
	protected static String btn_updateMain;
	protected static String btn_finance;
	protected static String txt_financeInfo;
	protected static String btn_outboundPaymentAdvice;
	protected static String txt_OutBoundPaymentAdvice;
	protected static String btn_newOutboundPaymentAdvice;
	protected static String btn_accrualVoucher;
	protected static String btn_vendorDetails;
	protected static String txt_vendorDetails;
	protected static String doubleClickvendorDetails;
	protected static String btn_GLAcc;
	protected static String txt_GLAcc;
	protected static String doubleClickGLAcc;
	protected static String txt_amount;
	protected static String btn_costAllocation;
	protected static String txt_vendorRefNo;
	protected static String txt_costCenter;
	protected static String txt_percentage;
	protected static String txt_costAssignmentType;
	protected static String btn_costObject;
	protected static String txt_costObject;
	protected static String doubleClickCostObject;
	protected static String btn_addRecord;
	protected static String btn_updateCostAllocations;
	protected static String btn_checkoutCost;
	protected static String btn_draftCost;
	protected static String btn_releaseCost;
	protected static String txt_projecttxt;
	protected static String btn_submenuProject;
	protected static String clickProjectCode;
	protected static String btn_actionProject;
	protected static String btn_updateTaskPOC;
	protected static String txt_task;
	protected static String txt_POC;
	protected static String btn_applyTask;
	protected static String btn_complete;
	protected static String btn_yes;
	protected static String doubleClick_postDate;
	protected static String btn_SelectAll;
	protected static String btn_postDate;
	protected static String btn_applytaskCompletion;
	protected static String btn_actionCompletedProject;
	protected static String btn_journalCompletedProject;
	protected static String txt_FABookProject;
	protected static String btn_SelectCheck;
	protected static String btn_fixedAssSegmentNo;
	protected static String label_responsiblePerson;
	protected static String label_pricingProfile;
	protected static String label_projectLocation;
	protected static String label_projectAgain;
	protected static String label_projecttask;
	protected static String label_outboundPaymentAdvice;
	protected static String label_newJourney;
	protected static String label_outboundPayment;
	protected static String label_vendorDetails;
	protected static String label_GLAcc;
	protected static String label_costAllocation;
	protected static String label_taskCompletion;
	protected static String label_projectJournal;
	protected static String label_Project;
	

	protected static String btn_fixedAssetBook;
	protected static String txt_fixedAssetBook;
	protected static String btn_template;
	protected static String txt_fixedAssetBookNo;
	protected static String doubleClickFixedAssetBookNo;
	protected static String btn_depreciation;
	protected static String btn_releaseBook;
	protected static String btn_selectAll;
	protected static String txt_fixedAssetInfoNo;
	protected static String doubleClickFixedAssetInfoNo;
	protected static String btn_Revaluation;
	protected static String label_fixedAssetInformationMain;
	protected static String label_fixedAssetRevaluation;
	protected static String click_close;
	
	protected static String btn_fixedAssetInformation;
	protected static String txt_fixedAssetInformation;
	protected static String btn_AssetBook;
	protected static String txt_revalueAmount;
	protected static String txt_usefulLife;
	protected static String btn_applyAssetInfo;
	protected static String txt_assetDescription;
	protected static String btn_assetInfoSearch;
	protected static String txt_assetInfoLookup;
	protected static String doubleClickAssetInfoLookup;
	protected static String txt_disposalDate;
	protected static String txt_disposalMode;
	protected static String btn_applyDisposal;
	
	protected static String btn_split;
	protected static String doubleClickFixedAssetInfoNo1;
	protected static String btn_disposal;
	protected static String doubleClick_disposalDate;
	protected static String btn_createNewwAsset;
	protected static String btn_AssetBookSplit1;
	protected static String txt_usefulLifeSplit1;
	protected static String label_fixedAssetSplit;
	protected static String label_fixedAssetInformationSplit;
	
	protected static String btn_existingBook;
	protected static String txt_fixedAssetInfoNo3;
	protected static String doubleClickFixedAssetInfoNo3;
	protected static String label_fixedAssetDisposal;
	
	protected static String doubleClickFixedAssetInfoNoDisposal;
	protected static String btn_applyAssetInfoDisposal;
	
	protected static String txt_disposalDateSelling;
	protected static String doubleClick_disposalDateSelling;
	protected static String txt_disposalModeSelling;
	protected static String btn_applyAssetInfoSelling;
	
	protected static String txt_freeTextInvoice;
	protected static String btn_customer;
	protected static String txt_customer;
	protected static String doubleClickCustomer;
	protected static String txt_salesUnit;
	protected static String label_freeTextInvoice;
	protected static String txt_amountText;
	
	protected static String txt_dispatchOrderNo;
	protected static String btn_reports;
	protected static String btn_templatebtn;
	protected static String btn_reportTmpWiz;
	protected static String txt_productCode;
	protected static String btn_quickPreview;
	
	protected static String btn_AssetBooks;
	protected static String btn_FABook;
	protected static String txt_netBookNo1;
	protected static String txt_netBookNo2;
	
	protected static String txt_credit;
	protected static String txt_debit;
	protected static String txt_docValue;
	protected static String txt_stockValue;
	protected static String goToPageInternal;
	
	
	//SMOKE Test case properties
	protected static String btn_fixedAssetBookCreation;
	protected static String btn_newfixedAssetBook;
	protected static String txt_assetBookCode;
	protected static String btn_desc;
	protected static String txt_desc;
	protected static String txt_usefulLife1;
	protected static String btn_dateA;
	protected static String btn_aquisitionDate;
	protected static String btn_dateB;
	protected static String btn_depreciationClick;
	protected static String btn_depreciationStartDate;
	protected static String btn_scheduleCode;
	protected static String txt_scheduleCode;
	protected static String doubleClickscheduleCode;
	protected static String btn_responsiblePerson;
	protected static String txt_responsiblePerson;
	protected static String doubleClickresponsiblePerson1;
	protected static String btn_applyBook;
	protected static String label_fixedAssetBook;
	protected static String label_outboundPaymentnew;
	protected static String label_purchaseInvoicePageReleasebtn;
	
	
	protected static String btn_plus1;
	protected static String btn_plus2;
	protected static String txt_group;
	protected static String txt_GroupName;
	protected static String txt_Book;
	protected static String btn_fixedAssetBooks;
	protected static String txt_fixedAssetBooks;
	protected static String doubleClickfixedAssetBook;
	protected static String btn_post;
	protected static String btn_bookView;
	protected static String txt_aquisitionValue;
	protected static String label_GroupConfig;
	protected static String btn_fixedAssetGroupConfig;
	protected static String btn_newFixedAssetGroupConfig;
	protected static String btn_applyGroup;
	protected static String assetGroup_tbl;
	protected static String label_outboundPaymentlabel;
	
	protected static String btn_newFixedAssetInformation;
	protected static String txt_InfoDescription;
	protected static String txt_quantity;
	protected static String txt_perUnitCost;
	protected static String txt_barcodeBook;
	protected static String btn_assetBookTab;
	protected static String btn_fixedAssetBooksearch;
	protected static String btn_disposalAndDepreciation;
	protected static String btn_scheduleCodeSearch;
	protected static String btn_fixedAssetInformationCreate;
	protected static String txt_AssetGroupName;
	protected static String doubleClickscheduleCodeInfo;
	protected static String btn_applyInfo;
	protected static String label_fixedAssetInfoRelease;
	

	
	/* Reading the Data to variables */
	
	protected static String UserNameData;
	protected static String PasswordData;
	
	protected static String vendorData;
	protected static String vendorRefData;
	protected static String productsData;
	protected static String qtyData;
	protected static String unitPriceData;
	protected static String assetGrpData;
	
	
	protected static String titleData;
	protected static String requesterData;
	protected static String dateData;
	protected static String productData1;
	protected static String internalqtyData;
	protected static String assetGrpInternalData;
	protected static String shippingData;

	
	
	protected static String projectCodeData;
	protected static String projectTitleData;
	protected static String projectGroupData;
	protected static String postingMethodData;
	protected static String projectTypeData;
	protected static String assetGroupData;
	protected static String responsiblePerson;
	protected static String bussinessUnitData;
	protected static String pricingProfileData;
	protected static String doubleClickPricingProfile;
	protected static String projectLocationData;
	protected static String inputwareData;
	protected static String WIPWareData;
	protected static String taskNameData;
	protected static String vendorDetailsData;
	protected static String GLAccData;
	protected static String amountData;
	protected static String vendorRefNoData;
	protected static String costCenterData;
	protected static String percentageData;
	protected static String costAssignmentData;
	protected static String costObjectData;
	protected static String taskData;
	protected static String POCData;
	
	protected static String fixedAssetBookNoData;
	protected static String templateData;
	
	protected static String fixedAssetInfo;
	protected static String AssetBookData;
	protected static String RevalueAmountData;
	protected static String usefulLifeData;
	protected static String fixedAssetInfo1;
	
	protected static String disposalModeDataWriteOff;
	protected static String disposalModeDataWrite;
	protected static String fixedAssetInfo2;
	
	protected static String AssetBookDataSplit1;
	protected static String assetDescriptionData;
	protected static String usefulLifeDataSplit1;
	
	protected static String fixedAssetInfo3;
	
	protected static String assetInfoLookupData;
	
	protected static String customerData;
	protected static String salesUnitData;
	protected static String amountDataText;
	//Calling the constructor

	//SMOKE Test case data
	protected static String assetBookCodeData;
	protected static String assetBookCodeDataCreate;
	protected static String descData;
	protected static String usefulLifeData1;
	protected static String scheduleCodeData;
	protected static String responsiblePersonData;
	
	//test case 02
	protected static String groupData;
	protected static String fixedAssetBooksData;
	protected static String quisitionData;
	
	//test case 04
	protected static String InfoDescriptionData;
	protected static String quantityData;
	protected static String perUnitCostData;

	
	
	public static void readElementlocators() throws Exception
	{
	
		siteLogo= findElementInXLSheet(getParameterFixedAssetSmoke,"site logo"); 
		txt_username= findElementInXLSheet(getParameterFixedAssetSmoke,"user name"); 
		txt_password= findElementInXLSheet(getParameterFixedAssetSmoke,"password"); 
		btn_login= findElementInXLSheet(getParameterFixedAssetSmoke,"login button"); 
		lnk_home=findElementInXLSheet(getParameterFixedAssetSmoke,"home_lnk");
		
		/*Navigate to side menu*/
		navigateMenu = findElementInXLSheet(getParameterFixedAssetSmoke, "nav_btn");
		sideNavBar = findElementInXLSheet(getParameterFixedAssetSmoke, "nav_view");
		fixedAsset_btn = findElementInXLSheet(getParameterFixedAssetSmoke, "fixedAssetbtn");
		fixedAssetLogo = findElementInXLSheet(getParameterFixedAssetSmoke, "fixedAsset_logo");
		
		/*Navigate to fixed asset module*/
		btn_procurement =findElementInXLSheet(getParameterFixedAssetSmoke, "procurement_btn");
		label_procurement=findElementInXLSheet(getParameterFixedAssetSmoke, "procurement_label");
		btn_purchaseOrder = findElementInXLSheet(getParameterFixedAssetSmoke, "purchaseOrder_btn");
		label_PurchaseOrder = findElementInXLSheet(getParameterFixedAssetSmoke, "purchaseOrderPage_logo");
		btn_newPurchaseOrder = findElementInXLSheet(getParameterFixedAssetSmoke, "newPurchase_btn");
		tab_journey = findElementInXLSheet(getParameterFixedAssetSmoke, "journey_label");
		btn_newServiceJourney = findElementInXLSheet(getParameterFixedAssetSmoke, "serviceJourney");
		label_PurchaseOrder1 = findElementInXLSheet(getParameterFixedAssetSmoke, "purchaseOrderPage_logo1");
		
		/*Filling mandatory fields Form*/
		btn_vendorSearch = findElementInXLSheet(getParameterFixedAssetSmoke, "vendorSearch_btn");
		txt_vendorSearch = findElementInXLSheet(getParameterFixedAssetSmoke, "vendorSearch_txt");
		doubleClickVendor = findElementInXLSheet(getParameterFixedAssetSmoke, "doubleClick");
		txt_venderRefNo = findElementInXLSheet(getParameterFixedAssetSmoke, "venderRefNo_txt");
		btn_productSearch = findElementInXLSheet(getParameterFixedAssetSmoke, "product_Search");
		txt_productSearch = findElementInXLSheet(getParameterFixedAssetSmoke, "productsearch_txt");
		doubleClickProduct = findElementInXLSheet(getParameterFixedAssetSmoke, "product double click");
		txt_qty = findElementInXLSheet(getParameterFixedAssetSmoke, "qty_txt");
		txt_unitPrice = findElementInXLSheet(getParameterFixedAssetSmoke, "unitPrice_txt");
		btn_advance = findElementInXLSheet(getParameterFixedAssetSmoke, "advance_btn");
		label_advance = findElementInXLSheet(getParameterFixedAssetSmoke, "advance_label");
		btn_fixedAss = findElementInXLSheet(getParameterFixedAssetSmoke, "asset_btn");
		label_assetGrp = findElementInXLSheet(getParameterFixedAssetSmoke, "asset Group_label");
		btn_assetGrp = findElementInXLSheet(getParameterFixedAssetSmoke, "assetgrp_btn");
		btn_apply = findElementInXLSheet(getParameterFixedAssetSmoke, "app_btn");
		btn_checkout = findElementInXLSheet(getParameterFixedAssetSmoke, "checkout_btn");
		btn_sendReview = findElementInXLSheet(getParameterFixedAssetSmoke, "sendReview_btn");
		btn_entution = findElementInXLSheet(getParameterFixedAssetSmoke, "entution_btn");
		btn_taskEvent = findElementInXLSheet(getParameterFixedAssetSmoke, "taskEvent_btn"); 
		btn_approvals = findElementInXLSheet(getParameterFixedAssetSmoke, "approval_btn");
		btn_PO = findElementInXLSheet(getParameterFixedAssetSmoke, "PO_btn");
		btn_draft = findElementInXLSheet(getParameterFixedAssetSmoke, "draft_btn");
		txt_PONumber = findElementInXLSheet(getParameterFixedAssetSmoke, "PONumber_txt");
		txt_approveNo = findElementInXLSheet(getParameterFixedAssetSmoke, "approveNo_txt");
		btn_approval = findElementInXLSheet(getParameterFixedAssetSmoke, "approval_btn");
		btn_releaseProc = findElementInXLSheet(getParameterFixedAssetSmoke, "releaseProc_btn");
		btn_checkoutPro = findElementInXLSheet(getParameterFixedAssetSmoke, "checkoutPro_btn");
		label_purchaseOrderPageRelease = findElementInXLSheet(getParameterFixedAssetSmoke, "label_purchaseOrderPageRelease");
		btn_draftPro = findElementInXLSheet(getParameterFixedAssetSmoke, "draftPro_btn");
		btn_releaseButton = findElementInXLSheet(getParameterFixedAssetSmoke, "releaseButton_btn");
		btn_actionPro = findElementInXLSheet(getParameterFixedAssetSmoke, "actionPro_btn");
		btn_journal = findElementInXLSheet(getParameterFixedAssetSmoke, "journal_btn");
		label_purchaseOrderPageNew = findElementInXLSheet(getParameterFixedAssetSmoke, "label_purchaseOrderPageNew");
		goToPageInternal = findElementInXLSheet(getParameterFixedAssetSmoke, "goToPageInternal");
		
		label_purchaseOrderPage = findElementInXLSheet(getParameterFixedAssetSmoke, "purchaseOrderPage_label");
		label_purchaseInvoicePage = findElementInXLSheet(getParameterFixedAssetSmoke, "purchaseInvoicePage_label");
		label_purchaseInvoicePageRelease = findElementInXLSheet(getParameterFixedAssetSmoke, "label_purchaseInvoicePageRelease");
		label_journalEntryDetails = findElementInXLSheet(getParameterFixedAssetSmoke, "label_journalEntryDetails");
		label_falabel = findElementInXLSheet(getParameterFixedAssetSmoke, "falabel_label");
		label_fixedAssetInformation = findElementInXLSheet(getParameterFixedAssetSmoke, "fixedAssetInformation_label");
		label_journalEntry = findElementInXLSheet(getParameterFixedAssetSmoke, "journalEntry_label");
		label_fixedAssetInformationRelease = findElementInXLSheet(getParameterFixedAssetSmoke, "label_fixedAssetInformationRelease");
		label_purchaseInvoicePageReleasebtn = findElementInXLSheet(getParameterFixedAssetSmoke, "label_purchaseInvoicePageReleasebtn");
		
		/*Navigate to inventory and warehousing*/
		btn_inventory = findElementInXLSheet(getParameterFixedAssetSmoke, "inventory_btn");
		label_inventory = findElementInXLSheet(getParameterFixedAssetSmoke, "inventory_label");
		btn_internalOrder = findElementInXLSheet(getParameterFixedAssetSmoke, "internalOrder_btn");
		label_InernalOrder = findElementInXLSheet(getParameterFixedAssetSmoke, "internalOrder_label");
		btn_newInternalOrder = findElementInXLSheet(getParameterFixedAssetSmoke, "newInternalOrder_btn");
		tab_internalOrderjourney = findElementInXLSheet(getParameterFixedAssetSmoke, "internalOrderjourney_tab");
		btn_newInternalServiceJourney = findElementInXLSheet(getParameterFixedAssetSmoke, "newInternalServiceJourney_btn");
		label_InternalOrder1 = findElementInXLSheet(getParameterFixedAssetSmoke, "InternalOrder1_label");
		txt_title = findElementInXLSheet(getParameterFixedAssetSmoke, "title_txt");
		btn_requester = findElementInXLSheet(getParameterFixedAssetSmoke, "requester_btn");
		txt_requesterSearch = findElementInXLSheet(getParameterFixedAssetSmoke, "requester search");
		doubleClickRequester = findElementInXLSheet(getParameterFixedAssetSmoke, "doubleclick requester");
		btn_date = findElementInXLSheet(getParameterFixedAssetSmoke, "date button");
		btn_internalProductSearch = findElementInXLSheet(getParameterFixedAssetSmoke, "internal_product_Search");
		txt_internalProductSearch = findElementInXLSheet(getParameterFixedAssetSmoke, "internal_productsearch_txt");
		doubleClickinternalProduct = findElementInXLSheet(getParameterFixedAssetSmoke, "internal_product double click");
		txt_internalqty = findElementInXLSheet(getParameterFixedAssetSmoke, "internal qty");
		btn_advanceInventory = findElementInXLSheet(getParameterFixedAssetSmoke, "advanceInventory_btn");
		label_advanceInventory = findElementInXLSheet(getParameterFixedAssetSmoke, "advanceInventory_label");
		btn_fixedAssInternal = findElementInXLSheet(getParameterFixedAssetSmoke, "fixedAssetInternal_btn");
		label_assetGrpInternal = findElementInXLSheet(getParameterFixedAssetSmoke, "assetGrpInternal_label");
		btn_assetGrpInternal = findElementInXLSheet(getParameterFixedAssetSmoke, "assetGrpInternal_btn");
		btn_applyInternal = findElementInXLSheet(getParameterFixedAssetSmoke, "applyInternal_btn");
		btn_release = findElementInXLSheet(getParameterFixedAssetSmoke, "release_btn");
		btn_goToPageInternal = findElementInXLSheet(getParameterFixedAssetSmoke, "goToPageInternal_btn");
		label_gotopage = findElementInXLSheet(getParameterFixedAssetSmoke, "label_gotopage");
		txt_shipping = findElementInXLSheet(getParameterFixedAssetSmoke, "shipping_txt");
		btn_draftdispatch = findElementInXLSheet(getParameterFixedAssetSmoke, "draftDispatch_btn");
		btn_releasedispatch = findElementInXLSheet(getParameterFixedAssetSmoke, "releasedispatch_btn");
		btn_action = findElementInXLSheet(getParameterFixedAssetSmoke, "action_btn");
		btn_journalEntry = findElementInXLSheet(getParameterFixedAssetSmoke, "journalEntry_btn");
		txt_FA = findElementInXLSheet(getParameterFixedAssetSmoke, "journalEntryNo_btn");
		btn_fixedAssSegment = findElementInXLSheet(getParameterFixedAssetSmoke, "fixedAssSegment_btn");
		txt_segmentNo = findElementInXLSheet(getParameterFixedAssetSmoke, "segmentNo_txt");
		btn_ok = findElementInXLSheet(getParameterFixedAssetSmoke, "ok_btn");
		btn_fixedAssetInfo = findElementInXLSheet(getParameterFixedAssetSmoke, "fixedAssetInfo_btn");
		txt_fixedAssetInfo = findElementInXLSheet(getParameterFixedAssetSmoke, "fixedAssetInfo_txt");
		btn_close = findElementInXLSheet(getParameterFixedAssetSmoke, "close_btn");
		doubleClickSegmentNo = findElementInXLSheet(getParameterFixedAssetSmoke, "doubleClick segmentNo");
		btn_endRelease = findElementInXLSheet(getParameterFixedAssetSmoke, "endRelease_btn");
		
		label_InternalOrder = findElementInXLSheet(getParameterFixedAssetSmoke, "InternalOrder_label");
		label_InternalDispatchOrder = findElementInXLSheet(getParameterFixedAssetSmoke, "internalDispatchOrder_label");
		label_InternalOrderRelease = findElementInXLSheet(getParameterFixedAssetSmoke, "label_InternalOrderRelease");
		
		/*Navigate to Project*/
		btn_project = findElementInXLSheet(getParameterFixedAssetSmoke, "project_btn");
		label_project = findElementInXLSheet(getParameterFixedAssetSmoke, "project_label");
		btn_subProject = findElementInXLSheet(getParameterFixedAssetSmoke, "subProject_btn");
		label_subProject = findElementInXLSheet(getParameterFixedAssetSmoke, "subProject_label");
		btn_newProject = findElementInXLSheet(getParameterFixedAssetSmoke, "newProject_btn");
		txt_project = findElementInXLSheet(getParameterFixedAssetSmoke, "project_txt");
		txt_projectCode =findElementInXLSheet(getParameterFixedAssetSmoke, "projectCode_txt");
		txt_projectTitle = findElementInXLSheet(getParameterFixedAssetSmoke, "projectTitle_txt");
		txt_projectGroup = findElementInXLSheet(getParameterFixedAssetSmoke, "projectGroup_txt");
		txt_postingMethod = findElementInXLSheet(getParameterFixedAssetSmoke, "postingMethod_txt");
		txt_projectType = findElementInXLSheet(getParameterFixedAssetSmoke, "projectType_txt");
		txt_assetGroup = findElementInXLSheet(getParameterFixedAssetSmoke, "asset group");
		btn_responsiblePersonSearch = findElementInXLSheet(getParameterFixedAssetSmoke, "responsible person_btn");
		txt_responsiblePersonSearch = findElementInXLSheet(getParameterFixedAssetSmoke, "responsible person_txt");
		doubleClickresponsiblePerson = findElementInXLSheet(getParameterFixedAssetSmoke, "doubleClick_responsiblePerson");
		txt_bussinessUnit = findElementInXLSheet(getParameterFixedAssetSmoke, "bussinessUnit_txt");
		btn_pricingProfile = findElementInXLSheet(getParameterFixedAssetSmoke, "pricing profile_btn");
		txt_pricingProfile = findElementInXLSheet(getParameterFixedAssetSmoke, "pricing profile_txt");
		doubleClickPricingProfile = findElementInXLSheet(getParameterFixedAssetSmoke, "doubleClick pricingProfile");
		txt_startDate = findElementInXLSheet(getParameterFixedAssetSmoke, "startdate_txt");
		txt_endDate = findElementInXLSheet(getParameterFixedAssetSmoke, "enddate_txt");
		doubleClick_startDate =findElementInXLSheet(getParameterFixedAssetSmoke, "startdate doublelick");
		doubleClick_endDate = findElementInXLSheet(getParameterFixedAssetSmoke, "enddate doubleclick");
		btn_projectLocation = findElementInXLSheet(getParameterFixedAssetSmoke, "projectLocation_btn");
		txt_projectLocation = findElementInXLSheet(getParameterFixedAssetSmoke, "projectLocation_txt");
		doubleClickProjectLocation = findElementInXLSheet(getParameterFixedAssetSmoke, "doubleClick projectLocation");
		txt_inputWarehouse = findElementInXLSheet(getParameterFixedAssetSmoke, "inputWare_txt");
		txt_WIPWarehouse = findElementInXLSheet(getParameterFixedAssetSmoke, "WIPWare_txt");
		btn_edit = findElementInXLSheet(getParameterFixedAssetSmoke, "edit_btn");
		btn_workBreakdown = findElementInXLSheet(getParameterFixedAssetSmoke, "workBreakdown_btn");
		btn_plus = findElementInXLSheet(getParameterFixedAssetSmoke, "plus_btn");
		txt_projectCodeNo = findElementInXLSheet(getParameterFixedAssetSmoke, "projectCodeNo_txt");
		txt_taskName = findElementInXLSheet(getParameterFixedAssetSmoke, "taskName_txt");
		btn_update = findElementInXLSheet(getParameterFixedAssetSmoke, "update_btn");
		btn_expand = findElementInXLSheet(getParameterFixedAssetSmoke, "expand_btn");
		btn_releaseTask = findElementInXLSheet(getParameterFixedAssetSmoke, "releaseTask_btn");
		btn_updateMain = findElementInXLSheet(getParameterFixedAssetSmoke, "updateMain_btn");
		btn_finance = findElementInXLSheet(getParameterFixedAssetSmoke, "finance_btn");
		txt_financeInfo = findElementInXLSheet(getParameterFixedAssetSmoke, "financeInfo_txt");
		btn_outboundPaymentAdvice = findElementInXLSheet(getParameterFixedAssetSmoke, "outboundPaymentAdvice_btn");
		txt_OutBoundPaymentAdvice = findElementInXLSheet(getParameterFixedAssetSmoke, "outboundPaymentAdvice_txt");
		btn_newOutboundPaymentAdvice = findElementInXLSheet(getParameterFixedAssetSmoke, "newOutboundPaymentAdvice_btn");
		btn_accrualVoucher = findElementInXLSheet(getParameterFixedAssetSmoke, "accrualVoucher_btn");
		btn_vendorDetails = findElementInXLSheet(getParameterFixedAssetSmoke, "vendorDetails_btn");
		txt_vendorDetails = findElementInXLSheet(getParameterFixedAssetSmoke, "vendorDetails_txt");
		doubleClickvendorDetails = findElementInXLSheet(getParameterFixedAssetSmoke, "doubleClick vendorDetails");
		btn_GLAcc = findElementInXLSheet(getParameterFixedAssetSmoke, "GLAcc_btn");
		txt_GLAcc = findElementInXLSheet(getParameterFixedAssetSmoke, "GLAcc_txt");
		doubleClickGLAcc = findElementInXLSheet(getParameterFixedAssetSmoke, "doubleClick GLAcc");
		txt_amount = findElementInXLSheet(getParameterFixedAssetSmoke, "amount_txt");
		btn_costAllocation = findElementInXLSheet(getParameterFixedAssetSmoke, "costAllocation_btn");
		txt_vendorRefNo = findElementInXLSheet(getParameterFixedAssetSmoke, "vendorRefNo_txt");
		txt_costCenter = findElementInXLSheet(getParameterFixedAssetSmoke, "costCenter_txt");
		txt_percentage = findElementInXLSheet(getParameterFixedAssetSmoke, "percentage_txt");
		txt_costAssignmentType = findElementInXLSheet(getParameterFixedAssetSmoke, "costAssignmentType_txt");
		btn_costObject = findElementInXLSheet(getParameterFixedAssetSmoke, "costObject_btn");
		txt_costObject = findElementInXLSheet(getParameterFixedAssetSmoke, "costObject_txt");
		doubleClickCostObject = findElementInXLSheet(getParameterFixedAssetSmoke, "doubleClick costObject");
		btn_addRecord = findElementInXLSheet(getParameterFixedAssetSmoke, "addRecord_btn");
		btn_updateCostAllocations = findElementInXLSheet(getParameterFixedAssetSmoke, "updateCostAllocation_btn");
		btn_checkoutCost = findElementInXLSheet(getParameterFixedAssetSmoke, "checkoutCost_btn");
		btn_draftCost = findElementInXLSheet(getParameterFixedAssetSmoke, "draftCost_btn");
		btn_releaseCost = findElementInXLSheet(getParameterFixedAssetSmoke, "releaseCost_btn");
		txt_projecttxt = findElementInXLSheet(getParameterFixedAssetSmoke, "projecttxt_txt");
		btn_submenuProject = findElementInXLSheet(getParameterFixedAssetSmoke, "submenuProject_btn");
		clickProjectCode = findElementInXLSheet(getParameterFixedAssetSmoke, "click ProjectCode");
		doubleClick_postDate = findElementInXLSheet(getParameterFixedAssetSmoke, "postDate_doubleClick");
		btn_actionProject = findElementInXLSheet(getParameterFixedAssetSmoke, "actionProject_btn");
		btn_updateTaskPOC = findElementInXLSheet(getParameterFixedAssetSmoke, "updateTaskPOC_btn");
		txt_task = findElementInXLSheet(getParameterFixedAssetSmoke, "task_txt");
		txt_POC = findElementInXLSheet(getParameterFixedAssetSmoke, "POC_txt");
		btn_applyTask = findElementInXLSheet(getParameterFixedAssetSmoke, "applyTask_btn");
		btn_complete = findElementInXLSheet(getParameterFixedAssetSmoke, "btn_complete");
		btn_yes = findElementInXLSheet(getParameterFixedAssetSmoke, "yes_btn");
		btn_SelectAll = findElementInXLSheet(getParameterFixedAssetSmoke, "selectAll_btn");
		btn_SelectCheck = findElementInXLSheet(getParameterFixedAssetSmoke, "SelectCheck_btn");
		btn_postDate = findElementInXLSheet(getParameterFixedAssetSmoke, "postDate_btn");
		btn_applytaskCompletion = findElementInXLSheet(getParameterFixedAssetSmoke, "applytaskCompletion_btn");
		btn_actionCompletedProject = findElementInXLSheet(getParameterFixedAssetSmoke, "actionCompletedProject_btn");
		btn_journalCompletedProject = findElementInXLSheet(getParameterFixedAssetSmoke, "journalCompletedProject_btn");
		txt_FABookProject = findElementInXLSheet(getParameterFixedAssetSmoke, "FABookProject_txt");
		btn_fixedAssSegmentNo = findElementInXLSheet(getParameterFixedAssetSmoke, "fixedAssSegmentNo_btn");
		label_responsiblePerson = findElementInXLSheet(getParameterFixedAssetSmoke, "responsiblePerson_label");
		label_pricingProfile = findElementInXLSheet(getParameterFixedAssetSmoke, "pricingProfile_label");
		label_projectLocation = findElementInXLSheet(getParameterFixedAssetSmoke, "projectLocation_label");
		label_projectAgain = findElementInXLSheet(getParameterFixedAssetSmoke, "projectAgain_label");
		label_projecttask = findElementInXLSheet(getParameterFixedAssetSmoke, "projecttask_label");
		label_outboundPaymentAdvice = findElementInXLSheet(getParameterFixedAssetSmoke, "outboundPaymentAdvice_label");
		label_newJourney = findElementInXLSheet(getParameterFixedAssetSmoke, "newJourney_label");
		label_outboundPayment = findElementInXLSheet(getParameterFixedAssetSmoke, "outboundPayment_label");
		label_vendorDetails = findElementInXLSheet(getParameterFixedAssetSmoke, "vendorDetails_label");
		label_GLAcc = findElementInXLSheet(getParameterFixedAssetSmoke, "GLAcc_label");
		label_costAllocation = findElementInXLSheet(getParameterFixedAssetSmoke, "costAllocation_label");
		label_taskCompletion = findElementInXLSheet(getParameterFixedAssetSmoke, "taskCompletion_label");
		label_projectJournal = findElementInXLSheet(getParameterFixedAssetSmoke, "projectJournal_label");
		label_Project = findElementInXLSheet(getParameterFixedAssetSmoke, "Project_label");
		label_outboundPaymentnew = findElementInXLSheet(getParameterFixedAssetSmoke, "label_outboundPaymentnew");
		label_outboundPaymentlabel = findElementInXLSheet(getParameterFixedAssetSmoke, "label_outboundPaymentlabel");
		label_fixedAssetInformationlabel = findElementInXLSheet(getParameterFixedAssetSmoke, "label_fixedAssetInformationlabel");
		
		
		btn_fixedAssetBook = findElementInXLSheet(getParameterFixedAssetSmoke, "fixedAssetBook_btn");
		txt_fixedAssetBook = findElementInXLSheet(getParameterFixedAssetSmoke, "fixedAssetBook_txt");
		btn_template = findElementInXLSheet(getParameterFixedAssetSmoke, "template_btn");
		txt_fixedAssetBookNo = findElementInXLSheet(getParameterFixedAssetSmoke, "fixedAssetBookNo_txt");
		doubleClickFixedAssetBookNo = findElementInXLSheet(getParameterFixedAssetSmoke, "doubleClick fixedAssetBook No");
		btn_depreciation = findElementInXLSheet(getParameterFixedAssetSmoke, "depreciation_btn");
		btn_releaseBook = findElementInXLSheet(getParameterFixedAssetSmoke, "releaseBook_btn");
		btn_selectAll = findElementInXLSheet(getParameterFixedAssetSmoke, "selectAll_btn");
		btn_Revaluation = findElementInXLSheet(getParameterFixedAssetSmoke, "Revaluation_btn");
		label_fixedAssetInformationMain = findElementInXLSheet(getParameterFixedAssetSmoke, "fixedAssetInformationMain_label");
		
		btn_fixedAssetInformation = findElementInXLSheet(getParameterFixedAssetSmoke, "fixedAssetInformation_btn");
		txt_fixedAssetInformation = findElementInXLSheet(getParameterFixedAssetSmoke, "fixedAssetInformation_txt");
		txt_fixedAssetInfoNo = findElementInXLSheet(getParameterFixedAssetSmoke, "fixedAssetInfoNo_txt");
		doubleClickFixedAssetInfoNo = findElementInXLSheet(getParameterFixedAssetSmoke, "fixedAssetInfoNo double");
		btn_AssetBook = findElementInXLSheet(getParameterFixedAssetSmoke, "AssetBook_btn");
		txt_revalueAmount = findElementInXLSheet(getParameterFixedAssetSmoke, "revalueAmount_txt");
		txt_usefulLife = findElementInXLSheet(getParameterFixedAssetSmoke, "usefulLife_txt");
		btn_applyAssetInfo = findElementInXLSheet(getParameterFixedAssetSmoke, "applyAssetInfo_btn");
		txt_assetDescription = findElementInXLSheet(getParameterFixedAssetSmoke, "assetDescription_txt");
		btn_assetInfoSearch = findElementInXLSheet(getParameterFixedAssetSmoke, "assetInfoSearch_btn");
		txt_assetInfoLookup = findElementInXLSheet(getParameterFixedAssetSmoke, "assetInfoLookup_txt");
		doubleClickAssetInfoLookup = findElementInXLSheet(getParameterFixedAssetSmoke, "AssetInfoLookupDoubleClick");
		txt_disposalDate = findElementInXLSheet(getParameterFixedAssetSmoke, "disposaldate_txt");
		txt_disposalMode = findElementInXLSheet(getParameterFixedAssetSmoke, "disposalMode_txt");
		btn_applyDisposal = findElementInXLSheet(getParameterFixedAssetSmoke, "applyDisposal_btn");
		label_fixedAssetRevaluation = findElementInXLSheet(getParameterFixedAssetSmoke, "fixedAssetRevaluation_label");
		click_close = findElementInXLSheet(getParameterFixedAssetSmoke, "close_click");
		
		btn_split = findElementInXLSheet(getParameterFixedAssetSmoke, "split_btn");
		label_fixedAssetSplit = findElementInXLSheet(getParameterFixedAssetSmoke, "fixedAssetSplit_label");
		label_fixedAssetInformationSplit = findElementInXLSheet(getParameterFixedAssetSmoke, "fixedAssetInformationSplit_label");
		
		btn_disposal = findElementInXLSheet(getParameterFixedAssetSmoke, "disposal_btn");
		doubleClick_disposalDate = findElementInXLSheet(getParameterFixedAssetSmoke, "disposaldate doubleClick");
		doubleClickFixedAssetInfoNo1 = findElementInXLSheet(getParameterFixedAssetSmoke, "FixedAssetInfoNo1 doubleClick");
		btn_createNewwAsset = findElementInXLSheet(getParameterFixedAssetSmoke, "createNewAsset btn");
		btn_AssetBookSplit1 = findElementInXLSheet(getParameterFixedAssetSmoke, "AssetBookSplit1_btn");
		txt_usefulLifeSplit1 = findElementInXLSheet(getParameterFixedAssetSmoke, "usefullifeSplit1_txt");
		label_fixedAssetDisposal = findElementInXLSheet(getParameterFixedAssetSmoke, "fixedAssetDisposal_label");
		
		btn_existingBook = findElementInXLSheet(getParameterFixedAssetSmoke, "existingBook_btn");
		txt_fixedAssetInfoNo3 = findElementInXLSheet(getParameterFixedAssetSmoke, "fixedAssetInfoNo3_txt");
		doubleClickFixedAssetInfoNo3 = findElementInXLSheet(getParameterFixedAssetSmoke, "doubleClick FixedAsset3");
		
		doubleClickFixedAssetInfoNoDisposal = findElementInXLSheet(getParameterFixedAssetSmoke, "doubleClick noDisposal");
		
		txt_disposalDateSelling = findElementInXLSheet(getParameterFixedAssetSmoke, "disposalDateSelling_txt");
		doubleClick_disposalDateSelling = findElementInXLSheet(getParameterFixedAssetSmoke, "disposalDateSelling_doubleclick");
		txt_disposalModeSelling = findElementInXLSheet(getParameterFixedAssetSmoke, "disposalModeSelling_txt");
		btn_applyAssetInfoSelling = findElementInXLSheet(getParameterFixedAssetSmoke, "applyAssetInfoSelling_btn");
		
		btn_applyAssetInfoDisposal = findElementInXLSheet(getParameterFixedAssetSmoke, "applyAssetInfoDisposal_btn");
		
		
		txt_freeTextInvoice = findElementInXLSheet(getParameterFixedAssetSmoke, "freeTextInvoice_txt");
		btn_customer = findElementInXLSheet(getParameterFixedAssetSmoke, "customer_btn");
		txt_customer = findElementInXLSheet(getParameterFixedAssetSmoke, "customer_txt");
		doubleClickCustomer = findElementInXLSheet(getParameterFixedAssetSmoke, "doubleclick customer");
		txt_salesUnit = findElementInXLSheet(getParameterFixedAssetSmoke, "salesunit_txt");
		label_freeTextInvoice = findElementInXLSheet(getParameterFixedAssetSmoke, "freeTextInvoice_label");
		txt_amountText = findElementInXLSheet(getParameterFixedAssetSmoke, "amountText_txt");
		
		txt_dispatchOrderNo = findElementInXLSheet(getParameterFixedAssetSmoke, "dispatchOrderNo_txt");
		btn_reports = findElementInXLSheet(getParameterFixedAssetSmoke, "reports_btn");
		btn_templatebtn = findElementInXLSheet(getParameterFixedAssetSmoke, "templatebtn_btn");
		btn_reportTmpWiz = findElementInXLSheet(getParameterFixedAssetSmoke, "reportTmpWiz_btn");
		txt_productCode = findElementInXLSheet(getParameterFixedAssetSmoke, "productCode_txt");
		btn_quickPreview = findElementInXLSheet(getParameterFixedAssetSmoke, "quickPreview_btn");
		
		btn_AssetBooks = findElementInXLSheet(getParameterFixedAssetSmoke, "AssetBooks_btn");
		btn_FABook = findElementInXLSheet(getParameterFixedAssetSmoke, "FABook_btn");
		txt_netBookNo1 = findElementInXLSheet(getParameterFixedAssetSmoke, "netBookNo1_txt");
		txt_netBookNo2 = findElementInXLSheet(getParameterFixedAssetSmoke, "netBookNo2_txt");
		
		txt_credit = findElementInXLSheet(getParameterFixedAssetSmoke, "credit_txt");
		txt_debit = findElementInXLSheet(getParameterFixedAssetSmoke, "debit_txt");
		
		txt_docValue = findElementInXLSheet(getParameterFixedAssetSmoke, "docValue_txt");
		txt_stockValue = findElementInXLSheet(getParameterFixedAssetSmoke, "stockValue_txt");
		
		
		//SMOKE Test case properties
		btn_fixedAssetBookCreation = findElementInXLSheet(getParameterFixedAssetSmoke, "fixedAssetBookCreation_btn");
		btn_newfixedAssetBook = findElementInXLSheet(getParameterFixedAssetSmoke, "newfixedAssetBook_btn");
		txt_assetBookCode = findElementInXLSheet(getParameterFixedAssetSmoke, "assetBookCode_txt");
		btn_desc = findElementInXLSheet(getParameterFixedAssetSmoke, "desc_btn");
		txt_desc = findElementInXLSheet(getParameterFixedAssetSmoke, "desc_txt");
		txt_usefulLife1 = findElementInXLSheet(getParameterFixedAssetSmoke, "usefulLife1_txt");
		btn_dateA = findElementInXLSheet(getParameterFixedAssetSmoke, "dateA_btn");
		btn_aquisitionDate = findElementInXLSheet(getParameterFixedAssetSmoke, "aquisitionDate_btn");
		btn_dateB = findElementInXLSheet(getParameterFixedAssetSmoke, "dateB_btn");
		btn_depreciationClick = findElementInXLSheet(getParameterFixedAssetSmoke, "depreciationClick_btn");
		btn_depreciationStartDate = findElementInXLSheet(getParameterFixedAssetSmoke, "depreciationStartDate_btn");
		btn_scheduleCode = findElementInXLSheet(getParameterFixedAssetSmoke, "scheduleCode_btn");
		txt_scheduleCode = findElementInXLSheet(getParameterFixedAssetSmoke, "scheduleCode_txt");
		doubleClickscheduleCode = findElementInXLSheet(getParameterFixedAssetSmoke, "doubleClickschedule Code");
		btn_responsiblePerson = findElementInXLSheet(getParameterFixedAssetSmoke, "responsiblePerson_btn");
		txt_responsiblePerson = findElementInXLSheet(getParameterFixedAssetSmoke, "responsiblePerson_txt");
		doubleClickresponsiblePerson1 = findElementInXLSheet(getParameterFixedAssetSmoke, "doubleClickresponsible Person1");
		btn_applyBook = findElementInXLSheet(getParameterFixedAssetSmoke, "applyBook_btn");
		label_fixedAssetBook = findElementInXLSheet(getParameterFixedAssetSmoke, "fixedAssetBook_label");
		
		
		//smoke test case 02
		btn_plus1 = findElementInXLSheet(getParameterFixedAssetSmoke, "plus1_btn");
		btn_plus2 = findElementInXLSheet(getParameterFixedAssetSmoke, "plus2_btn");
		txt_group = findElementInXLSheet(getParameterFixedAssetSmoke, "group_txt");
		txt_GroupName = findElementInXLSheet(getParameterFixedAssetSmoke, "GroupName_txt");
		txt_Book = findElementInXLSheet(getParameterFixedAssetSmoke, "Book_txt");
		btn_fixedAssetBooks = findElementInXLSheet(getParameterFixedAssetSmoke, "fixedAssetBooks_btn");
		txt_fixedAssetBooks = findElementInXLSheet(getParameterFixedAssetSmoke, "fixedAssetBooks_txt");
		doubleClickfixedAssetBook = findElementInXLSheet(getParameterFixedAssetSmoke, "doubleClick fixedAssetBook");
		btn_post = findElementInXLSheet(getParameterFixedAssetSmoke, "post_btn");
		btn_bookView = findElementInXLSheet(getParameterFixedAssetSmoke, "bookView_btn");
		txt_aquisitionValue = findElementInXLSheet(getParameterFixedAssetSmoke, "aquisitionValue_txt");
		label_GroupConfig = findElementInXLSheet(getParameterFixedAssetSmoke, "GroupConfig_label");
		btn_fixedAssetGroupConfig = findElementInXLSheet(getParameterFixedAssetSmoke, "fixedAssetGroupConfig_btn");
		btn_newFixedAssetGroupConfig = findElementInXLSheet(getParameterFixedAssetSmoke, "newFixedAssetGroupConfig_btn");
		btn_applyGroup = findElementInXLSheet(getParameterFixedAssetSmoke, "applyGroup_btn");
		assetGroup_tbl = findElementInXLSheet(getParameterFixedAssetSmoke, "tbl_assetGroup");
		
		//smoke test case 04
		btn_newFixedAssetInformation = findElementInXLSheet(getParameterFixedAssetSmoke, "newFixedAssetInformation_btn");
		txt_InfoDescription = findElementInXLSheet(getParameterFixedAssetSmoke, "InfoDescription_txt");
		btn_fixedAssetBooksearch = findElementInXLSheet(getParameterFixedAssetSmoke, "fixedAssetBooksearch_btn");
		txt_quantity = findElementInXLSheet(getParameterFixedAssetSmoke, "quantity_txt");
		txt_perUnitCost = findElementInXLSheet(getParameterFixedAssetSmoke, "perUnitCost_txt");
		txt_barcodeBook = findElementInXLSheet(getParameterFixedAssetSmoke, "barcodeBook_txt");
		btn_assetBookTab = findElementInXLSheet(getParameterFixedAssetSmoke, "assetBookTab_btn");
		btn_disposalAndDepreciation = findElementInXLSheet(getParameterFixedAssetSmoke, "disposalAndDepreciation_btn");
		btn_scheduleCodeSearch = findElementInXLSheet(getParameterFixedAssetSmoke, "scheduleCodeSearch_btn");
		btn_fixedAssetInformationCreate = findElementInXLSheet(getParameterFixedAssetSmoke, "fixedAssetInformationCreate_btn");
		txt_AssetGroupName = findElementInXLSheet(getParameterFixedAssetSmoke, "AssetGroupName_txt");
		doubleClickscheduleCodeInfo = findElementInXLSheet(getParameterFixedAssetSmoke, "doubleClickschedule CodeInfo");
		btn_applyInfo = findElementInXLSheet(getParameterFixedAssetSmoke, "applyInfo_btn");
		label_fixedAssetInfoRelease = findElementInXLSheet(getParameterFixedAssetSmoke, "fixedAssetInfoRelease_label");
		
		
		//action verification
		txt_draft = findElementInXLSheet(getParameterFixedAssetSmoke, "txt_draft");
		btn_Delete = findElementInXLSheet(getParameterFixedAssetSmoke, "btn_Delete");
		txt_delete = findElementInXLSheet(getParameterFixedAssetSmoke, "txt_delete");
		btn_duplicate = findElementInXLSheet(getParameterFixedAssetSmoke, "btn_duplicate");
		txt_duplicate = findElementInXLSheet(getParameterFixedAssetSmoke, "txt_duplicate");
		txt_edit = findElementInXLSheet(getParameterFixedAssetSmoke, "txt_edit");
		btn_updateNNew = findElementInXLSheet(getParameterFixedAssetSmoke, "btn_updateNNew");
		txt_updateNNewBOM = findElementInXLSheet(getParameterFixedAssetSmoke, "txt_updateNNewBOM");
		txt_groupName = findElementInXLSheet(getParameterFixedAssetSmoke, "txt_groupName");
		txt_update = findElementInXLSheet(getParameterFixedAssetSmoke, "txt_update");
		btn_Update = findElementInXLSheet(getParameterFixedAssetSmoke, "btn_Update");
		btn_draftNNew = findElementInXLSheet(getParameterFixedAssetSmoke, "btn_draftNNew");
		txt_draftNNew = findElementInXLSheet(getParameterFixedAssetSmoke, "txt_draftNNew");
		btn_copyFrom = findElementInXLSheet(getParameterFixedAssetSmoke, "btn_copyFrom");
		txt_copyFrom = findElementInXLSheet(getParameterFixedAssetSmoke, "txt_copyFrom");
		txt_fxGroup = findElementInXLSheet(getParameterFixedAssetSmoke, "txt_fxGroup");
		doublfxGroup = findElementInXLSheet(getParameterFixedAssetSmoke, "doublfxGroup");
		txt_fxBook = findElementInXLSheet(getParameterFixedAssetSmoke, "txt_fxBook");
		btn_fxGroup = findElementInXLSheet(getParameterFixedAssetSmoke, "btn_fxGroup");
		label_fixedAssetBookdraft = findElementInXLSheet(getParameterFixedAssetSmoke, "label_fixedAssetBookdraft");
		doublfxcopy = findElementInXLSheet(getParameterFixedAssetSmoke, "doublfxcopy");
		txt_fxcopy = findElementInXLSheet(getParameterFixedAssetSmoke, "txt_fxcopy");
		txt_copyFromBook = findElementInXLSheet(getParameterFixedAssetSmoke, "txt_copyFromBook");
		btn_newfixedAssetInfo = findElementInXLSheet(getParameterFixedAssetSmoke, "btn_newfixedAssetInfo");
		txt_fixedAssetInfoDesc = findElementInXLSheet(getParameterFixedAssetSmoke, "txt_fixedAssetInfoDesc");
		btn_plusMark = findElementInXLSheet(getParameterFixedAssetSmoke, "btn_plusMark");
		doubleClickschedulecode = findElementInXLSheet(getParameterFixedAssetSmoke, "doubleClickschedulecode");
		txt_resourceCode = findElementInXLSheet(getParameterFixedAssetSmoke, "txt_resourceCode");
		txt_resourceDesc = findElementInXLSheet(getParameterFixedAssetSmoke, "txt_resourceDesc");
		txt_resourceGroup = findElementInXLSheet(getParameterFixedAssetSmoke, "txt_resourceGroup");
		btn_resource = findElementInXLSheet(getParameterFixedAssetSmoke, "btn_resource");
		txt_fxInfoNo = findElementInXLSheet(getParameterFixedAssetSmoke, "txt_fxInfoNo");
		btn_barCode = findElementInXLSheet(getParameterFixedAssetSmoke, "btn_barCode");
		txt_barcode = findElementInXLSheet(getParameterFixedAssetSmoke, "txt_barcode");
		btn_transfer = findElementInXLSheet(getParameterFixedAssetSmoke, "btn_transfer");
		txt_transfer = findElementInXLSheet(getParameterFixedAssetSmoke, "txt_transfer");
		txt_book = findElementInXLSheet(getParameterFixedAssetSmoke, "txt_book");
		
		//master test cases properties
		btn_vendorInfo = findElementInXLSheet(getParameterFixedAssetSmoke, "btn_vendorInfo");
		newVendor = findElementInXLSheet(getParameterFixedAssetSmoke, "newVendor");
		txt_vendorType = findElementInXLSheet(getParameterFixedAssetSmoke, "txt_vendorType");
		txt_vendorName = findElementInXLSheet(getParameterFixedAssetSmoke, "txt_vendorName");
		txt_vendorName1 = findElementInXLSheet(getParameterFixedAssetSmoke, "txt_vendorName1");
		txt_vendorGroup  = findElementInXLSheet(getParameterFixedAssetSmoke, "txt_vendorGroup");
		btn_warehouseInfo = findElementInXLSheet(getParameterFixedAssetSmoke, "btn_warehouseInfo");
		btn_newWarehouse = findElementInXLSheet(getParameterFixedAssetSmoke, "btn_newWarehouse");
		txt_warehouseCode = findElementInXLSheet(getParameterFixedAssetSmoke, "txt_warehouseCode");
		txt_wraehousename = findElementInXLSheet(getParameterFixedAssetSmoke, "txt_wraehousename");
	}
	
	
	public static void readData() throws Exception
	{
		siteURL = findElementInXLSheet(getDataFixedAssetSmoke,"site url");
		UserNameData=findElementInXLSheet(getDataFixedAssetSmoke,"user name data");
		PasswordData=findElementInXLSheet(getDataFixedAssetSmoke,"password data");
		
		vendorData = findElementInXLSheet(getDataFixedAssetSmoke, "vendor data");
		vendorRefData = findElementInXLSheet(getDataFixedAssetSmoke, "vendor ref no");
		productsData = findElementInXLSheet(getDataFixedAssetSmoke, "product data");
		qtyData = findElementInXLSheet(getDataFixedAssetSmoke, "qty data");
		unitPriceData = findElementInXLSheet(getDataFixedAssetSmoke, "unit price data");
		assetGrpData = findElementInXLSheet(getDataFixedAssetSmoke, "asset Grp Data");
		titleData = findElementInXLSheet(getDataFixedAssetSmoke, "title data");
		requesterData = findElementInXLSheet(getDataFixedAssetSmoke, "requester data");
		dateData = findElementInXLSheet(getDataFixedAssetSmoke, "date data");
		productData1 = findElementInXLSheet(getDataFixedAssetSmoke, "product data1");
		internalqtyData = findElementInXLSheet(getDataFixedAssetSmoke, "internal_qty_data");
		shippingData = findElementInXLSheet(getDataFixedAssetSmoke, "shippingdata");
		projectCodeData = findElementInXLSheet(getDataFixedAssetSmoke, "project code");
		projectTitleData = findElementInXLSheet(getDataFixedAssetSmoke, "project title");
		projectGroupData = findElementInXLSheet(getDataFixedAssetSmoke, "project group");
		postingMethodData = findElementInXLSheet(getDataFixedAssetSmoke, "posting method");
		projectTypeData = findElementInXLSheet(getDataFixedAssetSmoke, "project type");
		assetGroupData = findElementInXLSheet(getDataFixedAssetSmoke, "assetGroup data");
		responsiblePerson = findElementInXLSheet(getDataFixedAssetSmoke, "responsiblePerson data");
		bussinessUnitData = findElementInXLSheet(getDataFixedAssetSmoke, "businessUnit data");
		pricingProfileData = findElementInXLSheet(getDataFixedAssetSmoke, "pricingProfile data");
		projectLocationData = findElementInXLSheet(getDataFixedAssetSmoke, "projectLocation data");
		inputwareData = findElementInXLSheet(getDataFixedAssetSmoke, "inputWare data");
		WIPWareData = findElementInXLSheet(getDataFixedAssetSmoke, "WIPWare data");
		taskNameData = findElementInXLSheet(getDataFixedAssetSmoke, "taskName data");
		vendorDetailsData = findElementInXLSheet(getDataFixedAssetSmoke, "vendorDetails data");
		GLAccData = findElementInXLSheet(getDataFixedAssetSmoke, "GLAcc data");
		amountData = findElementInXLSheet(getDataFixedAssetSmoke, "amount data");
		vendorRefNoData = findElementInXLSheet(getDataFixedAssetSmoke, "vendorRefNo data");
		costCenterData = findElementInXLSheet(getDataFixedAssetSmoke, "costCenter data");
		percentageData = findElementInXLSheet(getDataFixedAssetSmoke, "percentage data");
		costAssignmentData = findElementInXLSheet(getDataFixedAssetSmoke, "costAssignment data");
		costObjectData = findElementInXLSheet(getDataFixedAssetSmoke, "costObject data");
		taskData = findElementInXLSheet(getDataFixedAssetSmoke, "task data");
		POCData = findElementInXLSheet(getDataFixedAssetSmoke, "POC data");
		
		
		fixedAssetBookNoData = findElementInXLSheet(getDataFixedAssetSmoke, "fixedAssetBookNo data");
		templateData = findElementInXLSheet(getDataFixedAssetSmoke, "template data");
		fixedAssetInfo = findElementInXLSheet(getDataFixedAssetSmoke, "fixedAsset Info data");
		fixedAssetInfo1 = findElementInXLSheet(getDataFixedAssetSmoke, "fixedAsset info1 data");
		fixedAssetInfo2 = findElementInXLSheet(getDataFixedAssetSmoke, "fixed asset info2");
		AssetBookData = findElementInXLSheet(getDataFixedAssetSmoke, "AssetBook data");
		RevalueAmountData = findElementInXLSheet(getDataFixedAssetSmoke, "Revalue Amount Data");
		usefulLifeData = findElementInXLSheet(getDataFixedAssetSmoke, "useful Life Data");
		
		disposalModeDataWriteOff = findElementInXLSheet(getDataFixedAssetSmoke, "disposal Mode Data");
		disposalModeDataWrite = findElementInXLSheet(getDataFixedAssetSmoke, "disposal mode data");
		
		AssetBookDataSplit1 = findElementInXLSheet(getDataFixedAssetSmoke, "AssetBookSlit1 data");
		assetDescriptionData = findElementInXLSheet(getDataFixedAssetSmoke, "assetDescription data");
		usefulLifeDataSplit1 = findElementInXLSheet(getDataFixedAssetSmoke, "usefullifeSplit1 data");
		
		fixedAssetInfo3 = findElementInXLSheet(getDataFixedAssetSmoke, "fixedAssetInfo3 data");
		assetInfoLookupData = findElementInXLSheet(getDataFixedAssetSmoke, "assetInfoLookup data");
		
		customerData = findElementInXLSheet(getDataFixedAssetSmoke, "customer Data");
		salesUnitData = findElementInXLSheet(getDataFixedAssetSmoke, "salesUnit Data");
		amountDataText = findElementInXLSheet(getDataFixedAssetSmoke, "amountText data");
		
		
		//SMOKE Test case data
		
		assetBookCodeData = findElementInXLSheet(getDataFixedAssetSmoke, "assetBookCode Data");
		assetBookCodeDataCreate = findElementInXLSheet(getDataFixedAssetSmoke, "assetBookCode Data");
		descData = findElementInXLSheet(getDataFixedAssetSmoke, "desc Data");
		usefulLifeData1 = findElementInXLSheet(getDataFixedAssetSmoke, "usefulLife Data1");
		scheduleCodeData = findElementInXLSheet(getDataFixedAssetSmoke, "scheduleCode Data");
		responsiblePersonData = findElementInXLSheet(getDataFixedAssetSmoke, "responsiblePerson Data");
		
		//test case 02
		groupData = findElementInXLSheet(getDataFixedAssetSmoke, "group Data");
		fixedAssetBooksData = findElementInXLSheet(getDataFixedAssetSmoke, "fixedAssetBooks Data");
		quisitionData = findElementInXLSheet(getDataFixedAssetSmoke, "quisition Data");
		
		//test case 04
		InfoDescriptionData = findElementInXLSheet(getDataFixedAssetSmoke, "InfoDescription Data");
		quantityData = findElementInXLSheet(getDataFixedAssetSmoke, "quantity Data");
		perUnitCostData = findElementInXLSheet(getDataFixedAssetSmoke, "perUnitCost Data");
		
		//action verification data
		fxGroupData = findElementInXLSheet(getDataFixedAssetSmoke, "fxGroupData");
		assetBookCodeData1 = findElementInXLSheet(getDataFixedAssetSmoke, "assetBookCodeData1");
		fxcopyData = findElementInXLSheet(getDataFixedAssetSmoke, "fxcopyData");
		fixedAssetInfoDescData = findElementInXLSheet(getDataFixedAssetSmoke, "fixedAssetInfoDescData");
		resourceCodeData = findElementInXLSheet(getDataFixedAssetSmoke, "resourceCodeData");
		resourceDescData = findElementInXLSheet(getDataFixedAssetSmoke, "resourceDescData");
		
		//master test cases data
		vendorName1Data = findElementInXLSheet(getDataFixedAssetSmoke, "vendorName1Data");
		warehouseCodeData = findElementInXLSheet(getDataFixedAssetSmoke, "warehouseCodeData");
		warehousenameData = findElementInXLSheet(getDataFixedAssetSmoke, "warehousenameData");
	}
	
	
	//action verification properties
	protected static String txt_draft;
	protected static String btn_Delete;
	protected static String txt_delete;
	protected static String btn_duplicate;
	protected static String txt_duplicate;
	protected static String txt_edit;
	protected static String btn_updateNNew;
	protected static String txt_updateNNewBOM;
	protected static String txt_groupName;
	protected static String btn_Update;
	protected static String txt_update;
	protected static String btn_draftNNew;
	protected static String txt_draftNNew;
	protected static String btn_copyFrom;
	protected static String txt_copyFrom;
	protected static String txt_fxGroup;
	protected static String doublfxGroup;
	protected static String txt_fxBook;
	protected static String btn_fxGroup;
	protected static String label_fixedAssetBookdraft;
	protected static String txt_fxcopy;
	protected static String doublfxcopy;
	protected static String txt_copyFromBook;
	protected static String btn_newfixedAssetInfo;
	protected static String txt_fixedAssetInfoDesc;
	protected static String btn_plusMark;
	protected static String doubleClickschedulecode;
	protected static String txt_resourceCode;
	protected static String txt_resourceDesc;
	protected static String txt_resourceGroup;
	protected static String btn_resource;
	protected static String txt_fxInfoNo;
	protected static String btn_barCode;
	protected static String txt_barcode;
	protected static String btn_transfer;
	protected static String txt_transfer;
	protected static String txt_book;
	
	
	//action verification data

	protected static String fxGroupData;
	protected static String assetBookCodeData1;
	protected static String fxcopyData;
	protected static String fixedAssetInfoDescData;
	protected static String resourceCodeData;
	protected static String resourceDescData;
	
	
	//master test cases properties
	protected static String btn_vendorInfo;
	protected static String newVendor;
	protected static String txt_vendorType;
	protected static String txt_vendorName;
	protected static String txt_vendorName1;
	protected static String txt_vendorGroup;
	protected static String btn_warehouseInfo;
	protected static String btn_newWarehouse;
	protected static String txt_warehouseCode;
	protected static String txt_wraehousename;
	
	
	//master test cases data
	  
	protected static String vendorName1Data;
	protected static String warehouseCodeData;
	protected static String warehousenameData;
}
