package bileeta.BATF.Pages;

import bileeta.BTAF.Utilities.TestBase;

public class FinanceModuleDataSmoke extends TestBase {


	/* Reading the element locators to variables */
	protected static String site_logo;
	protected static String txt_username;
	protected static String txt_password;
	protected static String btn_login;
	protected static String link_home;

	/* Com_TC_002 */
	protected static String navigation_pane;
	protected static String btn_financeModule;
	protected static String btn_bulkChecqu;

	/* Fin_TC_001 */
	protected static String btn_inboundPaymentAdvice;
	protected static String btn_newInboundPaymentAdvice;
	protected static String btn_CusAdvanceJourneyIPA;
	protected static String lnl_postBusinessUnitIPA;
	protected static String drop_postBusinessUnit;
	protected static String btn_documentDataIPA;
	protected static String btn_docDateIPA;
	protected static String btn_postDateIPA;
	protected static String btn_dueDateIPA;
	protected static String btn_searchCustomerIPA;
	protected static String txt_cusAccountSearchIPA;
	protected static String lnk_resultCustomerAccountIPA;
	protected static String txt_refNoIPA;
	protected static String txt_descIPA;
	protected static String drop_cusrencyIPA;
	protected static String drop_taxIPA;
	protected static String txt_amountIPA;
	protected static String btn_calenderBackIAP;
	protected static String btn_calenderForwardIAP;
	protected static String tab_subbaryIPA;
	protected static String btn_checkout;
	public static String btn_draft;
	public static String btn_reelese;
	protected static String btn_print;
	protected static String lbl_docNoIPA;
	protected static String btn_relesedStatusOfDock;

	/* Fin_TC_002 */
	protected static String btn_searchGlCRV;
	protected static String txt_amountCustomerDebirMemoCRV;
	protected static String txt_secrhGlCRV;
	protected static String lnk_resultGL;
	protected static String btn_actionCRV;
	protected static String btn_converToInboundPaymentCRV;
	protected static String drop_paidCurrencyCRV;
	protected static String drop_paymentMethodCRV;
	protected static String drop_bankNAmeCRV;
	protected static String txt_checkNoCRV;
	protected static String plicker_dateCheckDateCRV;
	protected static String btn_detailTabCRV;
	protected static String drop_filterCurrencyCRV;
	protected static String txt_payingAmountCRV;
	protected static String btn_viewAllPendings;
	protected static String chk_adviceCRV;
	protected static String txt_paidAmountCRV;
	protected static String btn_journel;
	protected static String btn_cusDebitMemo;
	protected static String btn_summaryTab;
	protected static String btn_documentDataCRV;
	protected static String header_journelEntry;
	protected static String btn_viewPaymentFinTC002;
	protected static String lnk_paymentNoInboundPayemnt;

	/* Fin_TC_003 */
	protected static String btn_sallesMarketingModule;
	protected static String btn_salesInvoice;
	protected static String btn_newSalesInvoice;
	protected static String btn_slaesInvoiceServiceJourney;
	protected static String btn_customerSearchSalesInvoice;
	protected static String drop_currencySlaesInvoise;
	protected static String drop_salesUnitSalesIvoice;
	protected static String btn_productLockupSalesInvoice;
	protected static String txt_productSearch;
	protected static String lnk_resultProduct;
	protected static String txt_unitPriceSalesInvoice;
	protected static String btn_docFlowInboundPaymentAdvice;
	protected static String header_inboundPaymentAdviceSalesInvoice;

	/* Fin_TC_004 */
	protected static String btn_bankDeposit;
	protected static String lbl_InboundPaymentDocNo;
	protected static String btn_newBankDeposit;
	protected static String drop_paymentMethod;
	protected static String drop_bankNameBD;
	protected static String drop_accountNo;
	protected static String btn_viewPaymentBD;
	protected static String btn_rowPaymentBD;
	protected static String chk_paymentBD;
	
	/* Fin_TC_005 */
	protected static String txt_searchSalesInvoice;
	protected static String lnk_resultDocument;
	protected static String btn_inboundPaymentAdviceDocFlow;
	protected static String btn_setOffActionmenu;
	protected static String chk_setOffInvoiceDocNumber;
	protected static String btn_setOffWindowSetOffButton;
	protected static String txt_adviceDueAmountAfterSetOffSAlesInvoiceInboundPaymentAdvice;
	protected static String chk_inboundPaymentAdviceInboundPayment;

	/* Fin_TC_006 */
	protected static String btn_customerDebitMemoJourney;
	protected static String btn_nosCostAllocationDebitMemo;
	protected static String drop_costCenterCustomerDebitMemo;
	protected static String btn_addRowCostCenterCustomerDebitMemo;
	protected static String btn_updateCostCenterCustomerDebitMemo;
	protected static String txt_amountCustomerDebirMemo6;
	protected static String txt_costAllocationPresentageCustomerDebitMemo;

	/* Fin_TC_007 */
	protected static String btn_customerReciptVoucherJourneyboundPAyment;
	protected static String btn_customerAccountSearchCustomerReciptVoucher;
	protected static String btn_setOffNosCRV;
	protected static String chk_InboundPaymentCRV7;
	protected static String chk_InboundPaymentCRV78;
	protected static String lbl_baseAmountCRVSetOff;
	protected static String lbl_dueAmountCRVSetOff;
	protected static String lbl_dueAccordingAdvice7;
	protected static String txt_painAmountSetOffCRV;
	protected static String btn_closeSetOffPopUp;
	protected static String txt_paidAmountCRV7;
	protected static String txt_paidAmountCRVAfterHundredRecords;
	protected static String btn_inboundPayment;
	protected static String btn_newInboundPayment;
	protected static String header_inboundPaymentPage;
	protected static String tab_summaryCRV7;
	protected static String txt_billingAddressCRV;
	protected static String drop_contactPersonCRV7;
	protected static String btn_setOffUodateInsideButtonCRV;
	protected static String btn_scrollRight007;
	protected static String tbl_gridSectionRowCount_Fin_TC_007;
	protected static String btn_setOffAfter100Records;

	/* Fin_TC_008 */
	protected static String btn_action08;
	protected static String btn_converToInboundPayment08;
	protected static String btn_docDate08;
	protected static String btn_postDate08;
	protected static String btn_dueDate08;
	protected static String drop_paidCurrency08;
	protected static String drop_paymentMethod08;
	protected static String btn_detailsPage08;
	protected static String txt_payingAmount08;
	protected static String chk_IPA08;
	protected static String txt_paidAmount08;
	protected static String txt_searchInboundPayment;
	protected static String lnk_searchResultInboundPayment;
	protected static String tab_summary8;
	protected static String drom_customer8;
	protected static String drop_bankCheque8;
	protected static String txt_chequNo8;

	/* Fin_TC_009 */
	protected static String drop_payMethodBankDeposi;
	protected static String drop_bank;
	protected static String drop_bankAccountNo;
	protected static String btn_viewBankDeposit;
	protected static String chk_voucherBankDeposit;

	/* Fin_TC_010 */
	protected static String btn_outboundPaymentAdvice;
	protected static String btn_newOutboundPaymentAdvice;
	protected static String btn_vendorAdvanceJourney;
	protected static String btn_postBusinessUnit;
	protected static String drop_posBusinessUnit;
	protected static String btn_docDate10;
	protected static String btn_postDate10;
	protected static String btn_dueDate10;
	protected static String btn_searchVendorOPA;
	protected static String txt_serchVendorOPA;
	protected static String lnk_resultVendorOPA;
	protected static String txt_referenceCodeOPA;
	protected static String txt_descriptionOPA;
	protected static String drop_currencyOPA;
	protected static String drop_taxOPA;
	protected static String txt_amountOPA;
	protected static String drop_contactPerson;
	protected static String txt_billingAddress;

	/* Fin_TC_011 */
	protected static String txt_searchOutboundPaymentAdvice;
	protected static String lnk_outboundPaymentAdvice;
	protected static String btn_action;
	protected static String btn_convertToOutboundPaymentAdvice;
	protected static String btn_docDate;
	protected static String btn_calenderBack;
	protected static String btn_calenderForward;
	protected static String btn_postDate;
	protected static String btn_dueDate;
	protected static String drop_currency;
	protected static String drop_payBook;
	protected static String tab_detailOutboundPayment;
	protected static String drop_filterCurrencyOutboundPaymnet;
	protected static String chk_choseOPA;
	protected static String lbl_docNo;
	protected static String btn_viewButonOutboundPayment;

	/* Fin_TC_012 */
	protected static String btn_procument;
	protected static String btn_purchaseInvoice;
	protected static String btn_newPurchaseInvoice;
	protected static String btn_purchaseInvoiceJourney;
	protected static String btn_serchVendor;
	protected static String txt_vendorSearch;
	protected static String lnk_resultVendorSearch;
	protected static String btn_pdPostDate;
	protected static String btn_productLoockup;
	protected static String txt_unitPricePurchaseInvoice;
	protected static String btn_docFlow;
	protected static String header_page;
	public static String lbl_docStatus;
	protected static String btn_dateApply;
	protected static String btn_documentValueTotal;

	/* Fin_TC_014 */
	protected static String btn_vendorCreditMemoJourney;
	protected static String btn_vendorLookupCreditMemo;
	protected static String txt_description;
	protected static String txt_referenceCode;
	protected static String drop_tax;
	protected static String btn_glLookupCreditMemory;
	protected static String txt_GL;
	protected static String txt_amountCreditMemo;
	protected static String ico_noCostAlloacation;
	protected static String drop_costCenter;
	protected static String txt_apportionPresentage;
	protected static String btn_addRecordApportion;
	protected static String btn_update;
	
	/* Fin_TC_015 */
	protected static String btn_vendorPaymentVoucherJourney;
	protected static String btn_vendorSearchLookupVPV;
	protected static String tab_payment;
	protected static String chk_outboundPaymentAdvice;
	protected static String btn_setOffNos;
	protected static String btn_outboundPayment;
	protected static String btn_addNew;
	protected static String drop_filterCurrency;
	protected static String header_outboundPaymentSetOff;
	protected static String txt_paidAMountOutboundPaymentVendor;
	protected static String lbl_dueAmountOutboundPaymentVendor;
	protected static String btn_viewPendingAdvices;
	
	/* Fin_TC_016 */
	protected static String btn_letterOfGurentee;
	protected static String btn_new;
	protected static String txt_originalDocNo;
	protected static String drop_requesterType;
	protected static String btn_vendorLookupLOG;
	protected static String txt_vendor;
	protected static String lnk_resultVendor;
	protected static String btn_documentSearchNosLOG;
	protected static String txt_serchPurchaseOrder;
	protected static String lnk_serchResultPurchaseOrder;
	protected static String btn_applyPurchaseOrder;
	protected static String drop_gurenteeGroup;
	protected static String calende_openDate;
	protected static String calende_endDate;
	protected static String txt_amount;
	protected static String lbl_docStatus1;
	protected static String btn_documentSearchLookup16;
	protected static String btn_creditExtentionLetterOFGurentee;
	
	/* Fin_TC_017 */
	protected static String txt_letterPfGurenteeSearch;
	protected static String lnk_letterOfGurenteeResult;
	protected static String btn_convertToBankFacilityAgreement;
	protected static String txt_originalDocNoLetterOfGurentee;
	protected static String drop_bankBFA;
	protected static String txt_amountBFA;
	protected static String txt_startDate;
	protected static String txt_endDate;
	protected static String btn_date01;
	public static String lbl_journelEntryFirstRowDebit;
	public static String lbl_journelEntrySecondRowCredit;
	protected static String txt_referenceDocumenrLOG;
	protected static String header_page2;
	protected static String drop_bankFacilityGroup;
	
	/* Fin_TC_018 */
	protected static String lbl_OutboundPaymentNoLOG;
	protected static String btn_releeseLOGOutboundPayment;
	protected static String lnk_resultOutboundPayment;
	protected static String txt_searchOutboundPayment;
	
	/* Fin_TC_019 */
	protected static String btn_bankAdjustment;
	protected static String chk_facilitySettlement;
	protected static String btn_bankFacilityLookup;
	protected static String txt_bankFacilityAgreementSearch;
	protected static String lnl_bankFacilityAgreementSearchResult;
	protected static String drop_settlementTypeBankAdjustment;
	protected static String btn_costAllocationBankAdjustment;
	protected static String btn_glLookupBAnkAdjustment;
	protected static String txt_description1;
	protected static String lbl_minusValueCheck;
	protected static String header_taxBreackdown;
	protected static String close_headerPopup;
	protected static String btn_btnTaxBrackDownBankAdjustment;
	protected static String td_taxNameCell;
	
	/* Fin_TC_020 */
	protected static String btn_bankReconcillation;
	protected static String btn_newBankReconcilation;
	protected static String btn_statementDateBankReconcilation;
	protected static String lbl_lastBalaceBankReconcilation;
	protected static String txt_statementBalance;
	protected static String lbl_unReconciledAmount;
	protected static String btn_adjustentBankReconcilation;
	protected static String btn_descriptionLookupBAnkReconcilationAdjustment;
	protected static String txt_descriptionAdjustmenrBankReconcilation;
	protected static String btn_updateAdjustmentBankReconcilation;
	protected static String gl_nosAdjustmentBankReconcilation;
	protected static String txt_amountUluBankReconsillAdjustment;
	protected static String chk_advicePathBankReconcilation;
	protected static String lbl_amountAdviceBankReconcilation;
	protected static String lbl_reconcilAmount;
	protected static String btn_applyDescriptionBankeconcilation;
	protected static String btn_costAllocationAdjustmentBankReconcilation;
	protected static String btn_updateCostCenter;
	protected static String btn_scrollDownArrowBankReconsilGrid;
	protected static String div_reconcilAdvice;
	
	/* Fin_TC_021 */
	protected static String btn_pettyCash;
	protected static String btn_newPettyCash;
	protected static String drop_pettyCashAccount;
	protected static String btn_lookupPaidTo;
	protected static String txt_searchEmployee;
	protected static String lnk_resultEmployye;
	protected static String txt_descPettyCash;
	protected static String drop_pettyCashType;
	protected static String txt_amountPettyCash;
	protected static String nos_costApportionPettyCash;
	protected static String drop_costCenterPettyCash;
	protected static String txt_costPresentagePettyCash;
	protected static String btn_journelEntryPettyCashHeaderTExt;
	
	/* Fin_TC_023 */
	protected static String btn_glLookupPettyCash;
	protected static String nos_narrationPettyCash;
	protected static String txt_descriptionNarationPettyCash;
	protected static String btn_applyDescriptionPettyCash;
	protected static String btn_iouLookup;
	protected static String lnk_iouResult;
	
	/* Smoke_Finance_001 */
	protected static String btn_bankaAdjustment;
	protected static String btn_newBankAdjustment;
	protected static String drop_bankBankAdjustmentSmoke;
	protected static String drop_acoountNoBankAdjustmentSmoke;
	protected static String btn_glAccountLookupBankAdjustmentSmoke;
	protected static String txt_glInFrontPageBankAdjustmentSmoke;
	protected static String txt_amountBankAdjustmentSmoke;
	protected static String btn_summaryBankAdjustmentSmoke;
	protected static String header_releasedBankAdjustment;

	/* Smoke_Finance_011 */
	protected static String btn_journelEntryPage;
	protected static String btn_newJournelEntryPage;
	protected static String btn_description;
	protected static String txt_descriptionJournelEntry;
	protected static String btn_applyDescriptionJournelEntry;
	protected static String btn_addRowJournelEntry;
	protected static String btn_addRow2JournelEntry;
	protected static String btn_serchLoockupGLJournelEntry;
	protected static String btn_serchLoockupGLJournelEntry2;
	protected static String btn_serchLoockupGLJournelEntry3;
	protected static String txt_value1JjornelEntry;
	protected static String txt_value2JjornelEntry;
	protected static String txt_value3JjornelEntry;
	protected static String txt_setDescriptionJournelEntry;
	protected static String lbl_value1JournelAccount;
	protected static String lbl_value2JournelAccount;
	protected static String lbl_value3JournelAccount;
	protected static String header_releasedJE;
	
	/* Smoke_Finance_02 */
	protected static String btn_bankFacilityAgreement;
	protected static String btn_newBankFacilityAgreement;
	protected static String txt_originalDocNoBFA;
	protected static String drop_bankFacilityGroupBFA;
	protected static String txt_startDateBFA;
	protected static String txt_endDateBFA;
	protected static String lbl_loanCreationConfirmation1;
	protected static String lbl_loanCreationConfirmation2;
	
	/* Common customer creation */
	protected static String btn_accountInformation;
	protected static String btn_newAccountCustomerAccount;
	protected static String txt_accountNameCustomerAccount;
	protected static String drop_accountGtopuCustomerAccount;

	/* Reading the Data to variables */
	/* Login */
	protected static String site_Url;
	protected static String userNameData;
	protected static String passwordData;

	/* Fin_TC_001 */
	protected static String customerAccountIPA;
	protected static String testAllCharcterWordIPA;
	protected static String curenceIPA;
	protected static String amountIPA;
	protected static String postBusinessUnitIPA;

	/* Fin_TC_002 */
	protected static String amountCRV;
	protected static String glAccount;
	protected static String paidCurrencyCRV;
	protected static String paymentMethodCRV;
	protected static String filterCurrencyCRV;
	protected static String payingAmountCRV;
	protected static String customerAccountCRV;
	protected static String paidAmountCRV;

	/* Fin_TC_003 */
	protected static String currencySalesInvoise;
	protected static String slaesUnitSlaesInvoice;
	protected static String serviceProductSalesInvooice;
	protected static String unitPriceSalesInvice;

	/* Fin_TC_004 */
	protected static String paymentMethodBD;
	protected static String bankBD;

	/* Fin_TC_006 */
	protected static String customerCustomerDebitMemo;
	protected static String departmentCostAllocationCustomerDebitMemo;
	protected static String oresentageCostAllocationCustomerDebitMEmo;

	/* Fin_TC_007 */
	protected static String postBusinessUnitCustomerReciptVoucher;
	protected static String curencyCustomerReciptVoucher;
	protected static String paymentMethodCustomerReciptVoucher;
	protected static String filterCurrencyCustomerReciptVoucher;
	protected static String paidAmountCRV7;
	protected static String paymentMethodCRV7;
	protected static String payingAmountCRV7;

	/* Fin_TC_008 */
	protected static String paidCurrency08;
	protected static String paymentMethod08;
	protected static String payingAmount08;
	protected static String paidAmpunt8;
	protected static String bankCheque8;

	/* Fin_TC_009 */
	protected static String payMethodBankDeposit09;
	protected static String bank09;
	protected static String bankAccount09;

	/* Fin_TC_010 */
	protected static String vendorOutboundPaymentAdvice;
	protected static String referenceCodeOPA;
	protected static String descriptionOPA;
	protected static String currencyOPA;
	protected static String amountOPA;
	protected static String postBusinessUnitOPA;

	/* Fin_TC_011 */
	protected static String curencyTypeOutboundPayment;
	protected static String filterCurencyTypeOutboundPayment;
	protected static String payBookOutboundPayment;

	/* Fin_TC_011 */
	protected static String vendorPurchaseInvoice;
	protected static String serviceProductPurchaseInvoice;
	protected static String unitPricePurchaseInvice;
	protected static String currencyPurchaseInvoice;
	
	/* Fin_TC_014 */
	protected static String vendorVendorCreditMemo;
	protected static String description;
	protected static String referenceCode;
	protected static String curencyCreditMemo;
	protected static String glAccountCreditMemo;
	protected static String amountCreditMemo;
	protected static String postBusinessUnit;
	protected static String costUnit;
	protected static String costCenterAlloacatePresentage;
	protected static String postBusinessUnitOutboundPaymentVendorPaymentVoucher;

	
	/* Fin_TC_015 */
	protected static String vendorVRP;
	protected static String curencyVRP;
	protected static String filterCurrency;
	protected static String payBookVendorPaymentVoucher;

	/* Fin_TC_016 */
	protected static String payBookLetterOFGurentee;
	protected static String amountLOG;
	protected static String currencyLOG;
	public static String vebdorLetterOfGurentee;
	
	/* Fin_TC_017 */
	protected static String originalDocNoLetterOfGurentee;
	
	/* Fin_TC_019 */
	protected static String bankAccountBankAdjustment;
	protected static String glAccountBankAdjustment;
	protected static String bankBankAdjustment;
	protected static String costAllocatioPresentageNankAdjustment;
	protected static String postBusinessUnitBankAdjustment;
	protected static String amountBankAdjustment;
	protected static String costCenterBankAdjustment;
	
	/* Fin_TC_020 */
	protected static String bankStatementREconcilationStatementBalance;
	protected static String glAccountAdjustmetnBankReconcillation;
	protected static String bankBankReconcillation;
	protected static String bankAccountBankReconcilation;
	protected static String adjustmentDescriptionBankReconcilation;
	protected static String departmentCostAllocationBankReconcilationAdjustment;
	protected static String presentageCostAllocationBankReconcilationAdjustment;
	
	/* Fin_TC_021 */
	protected static String employeePettyCash;
	protected static String pettyCshType021;
	protected static String amountPettyCash;
	protected static String costPresentagePettyCash;
	protected static String costCenterPettyCsh;
	protected static String descriptionPettyCashIOU;
	
	/* Fin_TC_023 */
	protected static String narrationDescription;
	
	/* Smoke_Finance_001 */
	protected static String bankBankAdjustmentSmoke;
	protected static String accountBankAdjustmentSmoke;
	protected static String glAccountBankAdjustmentSmoke;
	protected static String amountBankAdjustmentSmoke;
	
	/* Smoke_Finance_011 */
	protected static String account1JournelEntry;
	protected static String account2JournelEntry;
	protected static String account3JournelEntry;
	protected static String value1JournelEntry;
	protected static String value2JournelEntry;
	protected static String value3JournelEntry;
	protected static String descriptionJournelEntry;
	
	/* Smoke_Finance_02 */
	protected static String originalDocNoBFA;
	protected static String bankNameBFA;
	protected static String accountNoBFA;
	protected static String currencyBFA;
	protected static String amountBFA;
	protected static String bankFacilityGroup;
	protected static String acoounLoanConfiramationBFA;
	
	/*Common customer account creation*/
	protected static String accountGroup;
	
	// Calling the constructor
	public static void readElementlocators() throws Exception {
		/* Login */
		site_logo = findElementInXLSheet(getParameterFinanceSmoke, "site_logo");
		txt_username = findElementInXLSheet(getParameterFinanceSmoke, "txt_username");
		txt_password = findElementInXLSheet(getParameterFinanceSmoke, "txt_password");
		btn_login = findElementInXLSheet(getParameterFinanceSmoke, "btn_login");
		link_home = findElementInXLSheet(getParameterFinanceSmoke, "link_home");

		/* Com_TC_002 */
		navigation_pane = findElementInXLSheet(getParameterFinanceSmoke, "navigation_pane");
		btn_financeModule = findElementInXLSheet(getParameterFinanceSmoke, "btn_financeModule");
		btn_bulkChecqu = findElementInXLSheet(getParameterFinanceSmoke, "btn_bulkChecqu");

		/* Fin_TC_001 */
		btn_inboundPaymentAdvice = findElementInXLSheet(getParameterFinanceSmoke, "btn_inboundPaymentAdvice");
		btn_newInboundPaymentAdvice = findElementInXLSheet(getParameterFinanceSmoke, "btn_newInboundPaymentAdvice");
		btn_CusAdvanceJourneyIPA = findElementInXLSheet(getParameterFinanceSmoke, "btn_CusAdvanceJourneyIPA");
		lnl_postBusinessUnitIPA = findElementInXLSheet(getParameterFinanceSmoke, "lnl_postBusinessUnitIPA");
		drop_postBusinessUnit = findElementInXLSheet(getParameterFinanceSmoke, "drop_postBusinessUnit");
		btn_documentDataIPA = findElementInXLSheet(getParameterFinanceSmoke, "btn_documentDataIPA");
		btn_docDateIPA = findElementInXLSheet(getParameterFinanceSmoke, "btn_docDateIPA");
		btn_postDateIPA = findElementInXLSheet(getParameterFinanceSmoke, "btn_postDateIPA");
		btn_dueDateIPA = findElementInXLSheet(getParameterFinanceSmoke, "btn_dueDateIPA");
		btn_searchCustomerIPA = findElementInXLSheet(getParameterFinanceSmoke, "btn_searchCustomerIPA");
		txt_cusAccountSearchIPA = findElementInXLSheet(getParameterFinanceSmoke, "txt_cusAccountSearchIPA");
		lnk_resultCustomerAccountIPA = findElementInXLSheet(getParameterFinanceSmoke, "lnk_resultCustomerAccountIPA");
		txt_refNoIPA = findElementInXLSheet(getParameterFinanceSmoke, "txt_refNoIPA");
		txt_descIPA = findElementInXLSheet(getParameterFinanceSmoke, "txt_descIPA");
		drop_cusrencyIPA = findElementInXLSheet(getParameterFinanceSmoke, "drop_cusrencyIPA");
		drop_taxIPA = findElementInXLSheet(getParameterFinanceSmoke, "drop_taxIPA");
		txt_amountIPA = findElementInXLSheet(getParameterFinanceSmoke, "txt_amountIPA");
		btn_calenderBackIAP = findElementInXLSheet(getParameterFinanceSmoke, "btn_calenderBackIAP");
		btn_calenderForwardIAP = findElementInXLSheet(getParameterFinanceSmoke, "btn_calenderForwardIAP");
		tab_subbaryIPA = findElementInXLSheet(getParameterFinanceSmoke, "tab_subbaryIPA");
		btn_checkout = findElementInXLSheet(getParameterFinanceSmoke, "btn_checkout");
		btn_draft = findElementInXLSheet(getParameterFinanceSmoke, "btn_draft");
		btn_reelese = findElementInXLSheet(getParameterFinanceSmoke, "btn_reelese");
		btn_print = findElementInXLSheet(getParameterFinanceSmoke, "btn_print");
		lbl_docNoIPA = findElementInXLSheet(getParameterFinanceSmoke, "lbl_docNoIPA");
		btn_relesedStatusOfDock = findElementInXLSheet(getParameterFinanceSmoke, "btn_relesedStatusOfDock");

		/* Fin_TC_002 */
		btn_searchGlCRV = findElementInXLSheet(getParameterFinanceSmoke, "btn_searchGlCRV");
		txt_amountCustomerDebirMemoCRV = findElementInXLSheet(getParameterFinanceSmoke, "txt_amountCustomerDebirMemoCRV");
		txt_secrhGlCRV = findElementInXLSheet(getParameterFinanceSmoke, "txt_secrhGlCRV");
		lnk_resultGL = findElementInXLSheet(getParameterFinanceSmoke, "lnk_resultGL");
		btn_actionCRV = findElementInXLSheet(getParameterFinanceSmoke, "btn_actionCRV");
		btn_converToInboundPaymentCRV = findElementInXLSheet(getParameterFinanceSmoke, "btn_converToInboundPaymentCRV");
		drop_paidCurrencyCRV = findElementInXLSheet(getParameterFinanceSmoke, "drop_paidCurrencyCRV");
		drop_paymentMethodCRV = findElementInXLSheet(getParameterFinanceSmoke, "drop_paymentMethodCRV");
		drop_bankNAmeCRV = findElementInXLSheet(getParameterFinanceSmoke, "drop_bankNAmeCRV");
		txt_checkNoCRV = findElementInXLSheet(getParameterFinanceSmoke, "txt_checkNoCRV");
		plicker_dateCheckDateCRV = findElementInXLSheet(getParameterFinanceSmoke, "plicker_dateCheckDateCRV");
		btn_detailTabCRV = findElementInXLSheet(getParameterFinanceSmoke, "btn_detailTabCRV");
		drop_filterCurrencyCRV = findElementInXLSheet(getParameterFinanceSmoke, "drop_filterCurrencyCRV");
		txt_payingAmountCRV = findElementInXLSheet(getParameterFinanceSmoke, "txt_payingAmountCRV");
		btn_viewAllPendings = findElementInXLSheet(getParameterFinanceSmoke, "btn_viewAllPendings");
		chk_adviceCRV = findElementInXLSheet(getParameterFinanceSmoke, "chk_adviceCRV");
		txt_paidAmountCRV = findElementInXLSheet(getParameterFinanceSmoke, "txt_paidAmountCRV");
		btn_journel = findElementInXLSheet(getParameterFinanceSmoke, "btn_journel");
		btn_cusDebitMemo = findElementInXLSheet(getParameterFinanceSmoke, "btn_cusDebitMemo");
		btn_summaryTab = findElementInXLSheet(getParameterFinanceSmoke, "btn_summaryTab");
		btn_documentDataCRV = findElementInXLSheet(getParameterFinanceSmoke, "btn_documentDataCRV");
		header_journelEntry = findElementInXLSheet(getParameterFinanceSmoke, "header_journelEntry");
		btn_viewPaymentFinTC002 = findElementInXLSheet(getParameterFinanceSmoke, "btn_viewPaymentFinTC002");
		lnk_paymentNoInboundPayemnt = findElementInXLSheet(getParameterFinanceSmoke, "lnk_paymentNoInboundPayemnt");

		/* Fin_TC_003 */
		btn_sallesMarketingModule = findElementInXLSheet(getParameterFinanceSmoke, "btn_sallesMarketingModule");
		btn_salesInvoice = findElementInXLSheet(getParameterFinanceSmoke, "btn_salesInvoice");
		btn_newSalesInvoice = findElementInXLSheet(getParameterFinanceSmoke, "btn_newSalesInvoice");
		btn_slaesInvoiceServiceJourney = findElementInXLSheet(getParameterFinanceSmoke, "btn_slaesInvoiceServiceJourney");
		btn_customerSearchSalesInvoice = findElementInXLSheet(getParameterFinanceSmoke, "btn_customerSearchSalesInvoice");
		drop_currencySlaesInvoise = findElementInXLSheet(getParameterFinanceSmoke, "drop_currencySlaesInvoise");
		drop_salesUnitSalesIvoice = findElementInXLSheet(getParameterFinanceSmoke, "drop_salesUnitSalesIvoice");
		btn_productLockupSalesInvoice = findElementInXLSheet(getParameterFinanceSmoke, "btn_productLockupSalesInvoice");
		txt_productSearch = findElementInXLSheet(getParameterFinanceSmoke, "txt_productSearch");
		lnk_resultProduct = findElementInXLSheet(getParameterFinanceSmoke, "lnk_resultProduct");
		txt_unitPriceSalesInvoice = findElementInXLSheet(getParameterFinanceSmoke, "txt_unitPriceSalesInvoice");
		btn_docFlowInboundPaymentAdvice = findElementInXLSheet(getParameterFinanceSmoke, "btn_docFlowInboundPaymentAdvice");
		header_inboundPaymentAdviceSalesInvoice = findElementInXLSheet(getParameterFinanceSmoke,
				"header_inboundPaymentAdviceSalesInvoice");

		/* Fin_TC_004 */
		btn_bankDeposit = findElementInXLSheet(getParameterFinanceSmoke, "btn_bankDeposit");
		lbl_InboundPaymentDocNo = findElementInXLSheet(getParameterFinanceSmoke, "lbl_InboundPaymentDocNo");
		btn_newBankDeposit = findElementInXLSheet(getParameterFinanceSmoke, "btn_newBankDeposit");
		drop_paymentMethod = findElementInXLSheet(getParameterFinanceSmoke, "drop_paymentMethod");
		drop_bankNameBD = findElementInXLSheet(getParameterFinanceSmoke, "drop_bankNameBD");
		drop_accountNo = findElementInXLSheet(getParameterFinanceSmoke, "drop_accountNo");
		btn_viewPaymentBD = findElementInXLSheet(getParameterFinanceSmoke, "btn_viewPaymentBD");
		btn_rowPaymentBD = findElementInXLSheet(getParameterFinanceSmoke, "btn_rowPaymentBD");
		chk_paymentBD = findElementInXLSheet(getParameterFinanceSmoke, "chk_paymentBD");
		
		/* Fin_TC_005 */
		txt_searchSalesInvoice = findElementInXLSheet(getParameterFinanceSmoke, "txt_searchSalesInvoice");
		lnk_resultDocument = findElementInXLSheet(getParameterFinanceSmoke, "lnk_resultDocument");
		btn_inboundPaymentAdviceDocFlow = findElementInXLSheet(getParameterFinanceSmoke, "btn_inboundPaymentAdviceDocFlow");
		btn_setOffActionmenu = findElementInXLSheet(getParameterFinanceSmoke, "btn_setOffActionmenu");
		chk_setOffInvoiceDocNumber = findElementInXLSheet(getParameterFinanceSmoke, "chk_setOffInvoiceDocNumber");
		btn_setOffWindowSetOffButton = findElementInXLSheet(getParameterFinanceSmoke, "btn_setOffWindowSetOffButton");
		txt_adviceDueAmountAfterSetOffSAlesInvoiceInboundPaymentAdvice = findElementInXLSheet(getParameterFinanceSmoke, "txt_adviceDueAmountAfterSetOffSAlesInvoiceInboundPaymentAdvice");
		chk_inboundPaymentAdviceInboundPayment = findElementInXLSheet(getParameterFinanceSmoke, "chk_inboundPaymentAdviceInboundPayment");

		/* Fin_TC_006 */
		btn_customerDebitMemoJourney = findElementInXLSheet(getParameterFinanceSmoke, "btn_customerDebitMemoJourney");
		btn_nosCostAllocationDebitMemo = findElementInXLSheet(getParameterFinanceSmoke, "btn_nosCostAllocationDebitMemo");
		drop_costCenterCustomerDebitMemo = findElementInXLSheet(getParameterFinanceSmoke,
				"drop_costCenterCustomerDebitMemo");
		btn_addRowCostCenterCustomerDebitMemo = findElementInXLSheet(getParameterFinanceSmoke,
				"btn_addRowCostCenterCustomerDebitMemo");
		btn_updateCostCenterCustomerDebitMemo = findElementInXLSheet(getParameterFinanceSmoke,
				"btn_updateCostCenterCustomerDebitMemo");
		txt_amountCustomerDebirMemo6 = findElementInXLSheet(getParameterFinanceSmoke, "txt_amountCustomerDebirMemo6");
		txt_costAllocationPresentageCustomerDebitMemo = findElementInXLSheet(getParameterFinanceSmoke,
				"txt_costAllocationPresentageCustomerDebitMemo");

		/* Fin_TC_007 */
		btn_customerReciptVoucherJourneyboundPAyment = findElementInXLSheet(getParameterFinanceSmoke,
				"btn_customerReciptVoucherJourneyboundPAyment");
		btn_customerAccountSearchCustomerReciptVoucher = findElementInXLSheet(getParameterFinanceSmoke,
				"btn_customerAccountSearchCustomerReciptVoucher");
		btn_setOffNosCRV = findElementInXLSheet(getParameterFinanceSmoke, "btn_setOffNosCRV");
		chk_InboundPaymentCRV7 = findElementInXLSheet(getParameterFinanceSmoke, "chk_InboundPaymentCRV7");
		chk_InboundPaymentCRV78 = findElementInXLSheet(getParameterFinanceSmoke, "chk_InboundPaymentCRV78");
		lbl_baseAmountCRVSetOff = findElementInXLSheet(getParameterFinanceSmoke, "lbl_baseAmountCRVSetOff");
		lbl_dueAmountCRVSetOff = findElementInXLSheet(getParameterFinanceSmoke, "lbl_dueAmountCRVSetOff");
		lbl_dueAccordingAdvice7 = findElementInXLSheet(getParameterFinanceSmoke, "lbl_dueAccordingAdvice7");
		txt_painAmountSetOffCRV = findElementInXLSheet(getParameterFinanceSmoke, "txt_painAmountSetOffCRV");
		btn_closeSetOffPopUp = findElementInXLSheet(getParameterFinanceSmoke, "btn_closeSetOffPopUp");
		txt_paidAmountCRV7 = findElementInXLSheet(getParameterFinanceSmoke, "txt_paidAmountCRV7");
		txt_paidAmountCRVAfterHundredRecords = findElementInXLSheet(getParameterFinanceSmoke, "txt_paidAmountCRVAfterHundredRecords");
		btn_inboundPayment = findElementInXLSheet(getParameterFinanceSmoke, "btn_inboundPayment");
		btn_newInboundPayment = findElementInXLSheet(getParameterFinanceSmoke, "btn_newInboundPayment");
		header_inboundPaymentPage = findElementInXLSheet(getParameterFinanceSmoke, "header_inboundPaymentPage");
		tab_summaryCRV7 = findElementInXLSheet(getParameterFinanceSmoke, "tab_summaryCRV7");
		txt_billingAddressCRV = findElementInXLSheet(getParameterFinanceSmoke, "txt_billingAddressCRV");
		drop_contactPersonCRV7 = findElementInXLSheet(getParameterFinanceSmoke, "drop_contactPersonCRV7");
		btn_setOffUodateInsideButtonCRV = findElementInXLSheet(getParameterFinanceSmoke, "btn_setOffUodateInsideButtonCRV");
		btn_scrollRight007 = findElementInXLSheet(getParameterFinanceSmoke, "btn_scrollRight007");
		tbl_gridSectionRowCount_Fin_TC_007 = findElementInXLSheet(getParameterFinanceSmoke, "tbl_gridSectionRowCount_Fin_TC_007");
		btn_setOffAfter100Records = findElementInXLSheet(getParameterFinanceSmoke, "btn_setOffAfter100Records");

		/* Fin_TC_008 */
		btn_action08 = findElementInXLSheet(getParameterFinanceSmoke, "btn_action08");
		btn_converToInboundPayment08 = findElementInXLSheet(getParameterFinanceSmoke, "btn_converToInboundPayment08");
		btn_docDate08 = findElementInXLSheet(getParameterFinanceSmoke, "btn_docDate08");
		btn_postDate08 = findElementInXLSheet(getParameterFinanceSmoke, "btn_postDate08");
		btn_dueDate08 = findElementInXLSheet(getParameterFinanceSmoke, "btn_dueDate08");
		drop_paidCurrency08 = findElementInXLSheet(getParameterFinanceSmoke, "drop_paidCurrency08");
		drop_paymentMethod08 = findElementInXLSheet(getParameterFinanceSmoke, "drop_paymentMethod08");
		btn_detailsPage08 = findElementInXLSheet(getParameterFinanceSmoke, "btn_detailsPage08");
		txt_payingAmount08 = findElementInXLSheet(getParameterFinanceSmoke, "txt_payingAmount08");
		chk_IPA08 = findElementInXLSheet(getParameterFinanceSmoke, "chk_IPA08");
		txt_paidAmount08 = findElementInXLSheet(getParameterFinanceSmoke, "txt_paidAmount08");
		txt_searchInboundPayment = findElementInXLSheet(getParameterFinanceSmoke, "txt_searchInboundPayment");
		lnk_searchResultInboundPayment = findElementInXLSheet(getParameterFinanceSmoke, "lnk_searchResultInboundPayment");
		tab_summary8 = findElementInXLSheet(getParameterFinanceSmoke, "tab_summary8");
		drom_customer8 = findElementInXLSheet(getParameterFinanceSmoke, "drom_customer8");
		drop_bankCheque8 = findElementInXLSheet(getParameterFinanceSmoke, "drop_bankCheque8");
		txt_chequNo8 = findElementInXLSheet(getParameterFinanceSmoke, "txt_chequNo8");
		drop_payMethodBankDeposi = findElementInXLSheet(getParameterFinanceSmoke, "drop_payMethodBankDeposi");
		drop_bank = findElementInXLSheet(getParameterFinanceSmoke, "drop_bank");
		drop_bankAccountNo = findElementInXLSheet(getParameterFinanceSmoke, "drop_bankAccountNo");
		btn_viewBankDeposit = findElementInXLSheet(getParameterFinanceSmoke, "btn_viewBankDeposit");
		chk_voucherBankDeposit = findElementInXLSheet(getParameterFinanceSmoke, "chk_voucherBankDeposit");

		/* Fin_TC_010 */
		btn_outboundPaymentAdvice = findElementInXLSheet(getParameterFinanceSmoke, "btn_outboundPaymentAdvice");
		btn_newOutboundPaymentAdvice = findElementInXLSheet(getParameterFinanceSmoke, "btn_newOutboundPaymentAdvice");
		btn_vendorAdvanceJourney = findElementInXLSheet(getParameterFinanceSmoke, "btn_vendorAdvanceJourney");
		btn_postBusinessUnit = findElementInXLSheet(getParameterFinanceSmoke, "btn_postBusinessUnit");
		drop_posBusinessUnit = findElementInXLSheet(getParameterFinanceSmoke, "drop_posBusinessUnit");
		btn_docDate10 = findElementInXLSheet(getParameterFinanceSmoke, "btn_docDate10");
		btn_postDate10 = findElementInXLSheet(getParameterFinanceSmoke, "btn_postDate10");
		btn_dueDate10 = findElementInXLSheet(getParameterFinanceSmoke, "btn_dueDate10");
		btn_searchVendorOPA = findElementInXLSheet(getParameterFinanceSmoke, "btn_searchVendorOPA");
		txt_serchVendorOPA = findElementInXLSheet(getParameterFinanceSmoke, "txt_serchVendorOPA");
		lnk_resultVendorOPA = findElementInXLSheet(getParameterFinanceSmoke, "lnk_resultVendorOPA");
		txt_referenceCodeOPA = findElementInXLSheet(getParameterFinanceSmoke, "txt_referenceCodeOPA");
		txt_descriptionOPA = findElementInXLSheet(getParameterFinanceSmoke, "txt_descriptionOPA");
		drop_currencyOPA = findElementInXLSheet(getParameterFinanceSmoke, "drop_currencyOPA");
		drop_taxOPA = findElementInXLSheet(getParameterFinanceSmoke, "drop_taxOPA");
		txt_amountOPA = findElementInXLSheet(getParameterFinanceSmoke, "txt_amountOPA");
		drop_contactPerson = findElementInXLSheet(getParameterFinanceSmoke, "drop_contactPerson");
		txt_billingAddress = findElementInXLSheet(getParameterFinanceSmoke, "txt_billingAddress");

		/* Fin_TC_011 */
		txt_searchOutboundPaymentAdvice = findElementInXLSheet(getParameterFinanceSmoke, "txt_searchOutboundPaymentAdvice");
		lnk_outboundPaymentAdvice = findElementInXLSheet(getParameterFinanceSmoke, "lnk_outboundPaymentAdvice");
		btn_action = findElementInXLSheet(getParameterFinanceSmoke, "btn_action");
		btn_convertToOutboundPaymentAdvice = findElementInXLSheet(getParameterFinanceSmoke,
				"btn_convertToOutboundPaymentAdvice");
		btn_docDate = findElementInXLSheet(getParameterFinanceSmoke, "btn_docDate");
		btn_calenderBack = findElementInXLSheet(getParameterFinanceSmoke, "btn_calenderBack");
		btn_calenderForward = findElementInXLSheet(getParameterFinanceSmoke, "btn_calenderForward");
		btn_postDate = findElementInXLSheet(getParameterFinanceSmoke, "btn_postDate");
		btn_dueDate = findElementInXLSheet(getParameterFinanceSmoke, "btn_dueDate");
		drop_currency = findElementInXLSheet(getParameterFinanceSmoke, "drop_currency");
		drop_payBook = findElementInXLSheet(getParameterFinanceSmoke, "drop_payBook");
		tab_detailOutboundPayment = findElementInXLSheet(getParameterFinanceSmoke, "tab_detailOutboundPayment");
		drop_filterCurrencyOutboundPaymnet = findElementInXLSheet(getParameterFinanceSmoke,
				"drop_filterCurrencyOutboundPaymnet");
		chk_choseOPA = findElementInXLSheet(getParameterFinanceSmoke, "chk_choseOPA");
		lbl_docNo = findElementInXLSheet(getParameterFinanceSmoke, "lbl_docNo");
		btn_viewButonOutboundPayment = findElementInXLSheet(getParameterFinanceSmoke, "btn_viewButonOutboundPayment");
		txt_setDescriptionJournelEntry = findElementInXLSheet(getParameterFinanceSmoke, "txt_setDescriptionJournelEntry");

		/* Fin_TC_012 */
		btn_procument = findElementInXLSheet(getParameterFinanceSmoke, "btn_procument");
		btn_purchaseInvoice = findElementInXLSheet(getParameterFinanceSmoke, "btn_purchaseInvoice");
		btn_newPurchaseInvoice = findElementInXLSheet(getParameterFinanceSmoke, "btn_newPurchaseInvoice");
		btn_purchaseInvoiceJourney = findElementInXLSheet(getParameterFinanceSmoke, "btn_purchaseInvoiceJourney");
		btn_serchVendor = findElementInXLSheet(getParameterFinanceSmoke, "btn_serchVendor");
		txt_vendorSearch = findElementInXLSheet(getParameterFinanceSmoke, "txt_vendorSearch");
		lnk_resultVendorSearch = findElementInXLSheet(getParameterFinanceSmoke, "lnk_resultVendorSearch");
		btn_pdPostDate = findElementInXLSheet(getParameterFinanceSmoke, "btn_pdPostDate");
		btn_productLoockup = findElementInXLSheet(getParameterFinanceSmoke, "btn_productLoockup");
		txt_unitPricePurchaseInvoice = findElementInXLSheet(getParameterFinanceSmoke, "txt_unitPricePurchaseInvoice");
		btn_docFlow = findElementInXLSheet(getParameterFinanceSmoke, "btn_docFlow");
		header_page = findElementInXLSheet(getParameterFinanceSmoke, "header_page");
		lbl_docStatus = findElementInXLSheet(getParameterFinanceSmoke, "lbl_docStatus");
		btn_dateApply = findElementInXLSheet(getParameterFinanceSmoke, "btn_dateApply");
		btn_documentValueTotal = findElementInXLSheet(getParameterFinanceSmoke, "btn_documentValueTotal");
		

		/* Fin_TC_014 */
		btn_vendorCreditMemoJourney = findElementInXLSheet(getParameterFinanceSmoke, "btn_vendorCreditMemoJourney");
		btn_vendorLookupCreditMemo = findElementInXLSheet(getParameterFinanceSmoke, "btn_vendorLookupCreditMemo");
		txt_referenceCode = findElementInXLSheet(getParameterFinanceSmoke, "txt_referenceCode");
		drop_tax = findElementInXLSheet(getParameterFinanceSmoke, "drop_tax");
		btn_glLookupCreditMemory = findElementInXLSheet(getParameterFinanceSmoke, "btn_glLookupCreditMemory");
		txt_GL = findElementInXLSheet(getParameterFinanceSmoke, "txt_GL");
		txt_amountCreditMemo = findElementInXLSheet(getParameterFinanceSmoke, "txt_amountCreditMemo");
		ico_noCostAlloacation = findElementInXLSheet(getParameterFinanceSmoke, "ico_noCostAlloacation");
		drop_costCenter = findElementInXLSheet(getParameterFinanceSmoke, "drop_costCenter");
		txt_apportionPresentage = findElementInXLSheet(getParameterFinanceSmoke, "txt_apportionPresentage");
		btn_addRecordApportion = findElementInXLSheet(getParameterFinanceSmoke, "btn_addRecordApportion");
		btn_update = findElementInXLSheet(getParameterFinanceSmoke, "btn_update");
		txt_description = findElementInXLSheet(getParameterFinanceSmoke, "txt_description");
		
		/* Fin_TC_015 */
		btn_vendorPaymentVoucherJourney = findElementInXLSheet(getParameterFinanceSmoke, "btn_vendorPaymentVoucherJourney");
		btn_vendorSearchLookupVPV = findElementInXLSheet(getParameterFinanceSmoke, "btn_vendorSearchLookupVPV");
		tab_payment = findElementInXLSheet(getParameterFinanceSmoke, "tab_payment");
		chk_outboundPaymentAdvice = findElementInXLSheet(getParameterFinanceSmoke, "chk_outboundPaymentAdvice");
		btn_setOffNos = findElementInXLSheet(getParameterFinanceSmoke, "btn_setOffNos");
		btn_outboundPayment = findElementInXLSheet(getParameterFinanceSmoke, "btn_outboundPayment");
		btn_addNew = findElementInXLSheet(getParameterFinanceSmoke, "btn_addNew");
		drop_filterCurrency = findElementInXLSheet(getParameterFinanceSmoke, "drop_filterCurrency");
		header_outboundPaymentSetOff = findElementInXLSheet(getParameterFinanceSmoke, "header_outboundPaymentSetOff");
		txt_paidAMountOutboundPaymentVendor = findElementInXLSheet(getParameterFinanceSmoke, "txt_paidAMountOutboundPaymentVendor");
		lbl_dueAmountOutboundPaymentVendor = findElementInXLSheet(getParameterFinanceSmoke, "lbl_dueAmountOutboundPaymentVendor");
		btn_viewPendingAdvices = findElementInXLSheet(getParameterFinanceSmoke, "btn_viewPendingAdvices");

		/* Fin_TC_016 */
		btn_letterOfGurentee = findElementInXLSheet(getParameterFinanceSmoke, "btn_letterOfGurentee");
		btn_new = findElementInXLSheet(getParameterFinanceSmoke, "btn_new");
		txt_originalDocNo = findElementInXLSheet(getParameterFinanceSmoke, "txt_originalDocNo");
		drop_requesterType = findElementInXLSheet(getParameterFinanceSmoke, "drop_requesterType");
		btn_vendorLookupLOG = findElementInXLSheet(getParameterFinanceSmoke, "btn_vendorLookupLOG");
		txt_vendor = findElementInXLSheet(getParameterFinanceSmoke, "txt_vendor");
		lnk_resultVendor = findElementInXLSheet(getParameterFinanceSmoke, "lnk_resultVendor");
		btn_documentSearchNosLOG = findElementInXLSheet(getParameterFinanceSmoke, "btn_documentSearchNosLOG");
		txt_serchPurchaseOrder = findElementInXLSheet(getParameterFinanceSmoke, "txt_serchPurchaseOrder");
		lnk_serchResultPurchaseOrder = findElementInXLSheet(getParameterFinanceSmoke, "lnk_serchResultPurchaseOrder");
		btn_applyPurchaseOrder = findElementInXLSheet(getParameterFinanceSmoke, "btn_applyPurchaseOrder");
		drop_gurenteeGroup = findElementInXLSheet(getParameterFinanceSmoke, "drop_gurenteeGroup");
		calende_openDate = findElementInXLSheet(getParameterFinanceSmoke, "calende_openDate");
		calende_endDate = findElementInXLSheet(getParameterFinanceSmoke, "calende_endDate");
		txt_amount = findElementInXLSheet(getParameterFinanceSmoke, "txt_amount");
		lbl_docStatus1 = findElementInXLSheet(getParameterFinanceSmoke, "lbl_docStatus1");
		btn_documentSearchLookup16 = findElementInXLSheet(getParameterFinanceSmoke, "btn_documentSearchLookup16");
		btn_creditExtentionLetterOFGurentee = findElementInXLSheet(getParameterFinanceSmoke, "btn_creditExtentionLetterOFGurentee");
		
		/* Fin_TC_017 */
		txt_letterPfGurenteeSearch = findElementInXLSheet(getParameterFinanceSmoke, "txt_letterPfGurenteeSearch");
		lnk_letterOfGurenteeResult = findElementInXLSheet(getParameterFinanceSmoke, "lnk_letterOfGurenteeResult");
		btn_convertToBankFacilityAgreement = findElementInXLSheet(getParameterFinanceSmoke, "btn_convertToBankFacilityAgreement");
		txt_originalDocNoLetterOfGurentee = findElementInXLSheet(getParameterFinanceSmoke, "txt_originalDocNoLetterOfGurentee");
		drop_bankBFA = findElementInXLSheet(getParameterFinanceSmoke, "drop_bankBFA");
		txt_amountBFA = findElementInXLSheet(getParameterFinanceSmoke, "txt_amountBFA");
		txt_startDate = findElementInXLSheet(getParameterFinanceSmoke, "txt_startDate");
		txt_endDate = findElementInXLSheet(getParameterFinanceSmoke, "txt_endDate");
		btn_date01 = findElementInXLSheet(getParameterFinanceSmoke, "btn_date01");
		lbl_journelEntryFirstRowDebit = findElementInXLSheet(getParameterFinanceSmoke, "lbl_journelEntryFirstRowDebit");
		lbl_journelEntrySecondRowCredit = findElementInXLSheet(getParameterFinanceSmoke, "lbl_journelEntrySecondRowCredit");
		lbl_journelEntrySecondRowCredit = findElementInXLSheet(getParameterFinanceSmoke, "lbl_journelEntrySecondRowCredit");
		txt_referenceDocumenrLOG = findElementInXLSheet(getParameterFinanceSmoke, "txt_referenceDocumenrLOG");
		header_page2 = findElementInXLSheet(getParameterFinanceSmoke, "header_page2");
		drop_bankFacilityGroup = findElementInXLSheet(getParameterFinanceSmoke, "drop_bankFacilityGroup");
		
		/* Fin_TC_018 */
		lbl_OutboundPaymentNoLOG = findElementInXLSheet(getParameterFinanceSmoke, "lbl_OutboundPaymentNoLOG");
		btn_releeseLOGOutboundPayment = findElementInXLSheet(getParameterFinanceSmoke, "btn_releeseLOGOutboundPayment");
		lnk_resultOutboundPayment = findElementInXLSheet(getParameterFinanceSmoke, "lnk_resultOutboundPayment");
		txt_searchOutboundPayment = findElementInXLSheet(getParameterFinanceSmoke, "txt_searchOutboundPayment");
		
		/* Fin_TC_019 */
		btn_bankAdjustment = findElementInXLSheet(getParameterFinanceSmoke, "btn_bankAdjustment");
		chk_facilitySettlement = findElementInXLSheet(getParameterFinanceSmoke, "chk_facilitySettlement");
		btn_bankFacilityLookup = findElementInXLSheet(getParameterFinanceSmoke, "btn_bankFacilityLookup");
		txt_bankFacilityAgreementSearch = findElementInXLSheet(getParameterFinanceSmoke, "txt_bankFacilityAgreementSearch");
		lnl_bankFacilityAgreementSearchResult = findElementInXLSheet(getParameterFinanceSmoke, "lnl_bankFacilityAgreementSearchResult");
		drop_settlementTypeBankAdjustment = findElementInXLSheet(getParameterFinanceSmoke, "drop_settlementTypeBankAdjustment");
		btn_costAllocationBankAdjustment = findElementInXLSheet(getParameterFinanceSmoke, "btn_costAllocationBankAdjustment");
		btn_glLookupBAnkAdjustment = findElementInXLSheet(getParameterFinanceSmoke, "btn_glLookupBAnkAdjustment");
		txt_description1 = findElementInXLSheet(getParameterFinanceSmoke, "txt_description1");
		lbl_minusValueCheck = findElementInXLSheet(getParameterFinanceSmoke, "lbl_minusValueCheck");
		header_taxBreackdown = findElementInXLSheet(getParameterFinanceSmoke, "header_taxBreackdown");
		close_headerPopup = findElementInXLSheet(getParameterFinanceSmoke, "close_headerPopup");
		btn_btnTaxBrackDownBankAdjustment = findElementInXLSheet(getParameterFinanceSmoke, "btn_btnTaxBrackDownBankAdjustment");
		td_taxNameCell = findElementInXLSheet(getParameterFinanceSmoke, "td_taxNameCell");
		
		/* Fin_TC_020 */
		btn_bankReconcillation = findElementInXLSheet(getParameterFinanceSmoke, "btn_bankReconcillation");
		btn_newBankReconcilation = findElementInXLSheet(getParameterFinanceSmoke, "btn_newBankReconcilation");
		btn_statementDateBankReconcilation = findElementInXLSheet(getParameterFinanceSmoke, "btn_statementDateBankReconcilation");
		lbl_lastBalaceBankReconcilation = findElementInXLSheet(getParameterFinanceSmoke, "lbl_lastBalaceBankReconcilation");
		txt_statementBalance = findElementInXLSheet(getParameterFinanceSmoke, "txt_statementBalance");
		lbl_unReconciledAmount = findElementInXLSheet(getParameterFinanceSmoke, "lbl_unReconciledAmount");
		btn_adjustentBankReconcilation = findElementInXLSheet(getParameterFinanceSmoke, "btn_adjustentBankReconcilation");
		btn_descriptionLookupBAnkReconcilationAdjustment = findElementInXLSheet(getParameterFinanceSmoke, "btn_descriptionLookupBAnkReconcilationAdjustment");
		txt_descriptionAdjustmenrBankReconcilation = findElementInXLSheet(getParameterFinanceSmoke, "txt_descriptionAdjustmenrBankReconcilation");
		btn_updateAdjustmentBankReconcilation = findElementInXLSheet(getParameterFinanceSmoke, "btn_updateAdjustmentBankReconcilation");
		gl_nosAdjustmentBankReconcilation = findElementInXLSheet(getParameterFinanceSmoke, "gl_nosAdjustmentBankReconcilation");
		txt_amountUluBankReconsillAdjustment = findElementInXLSheet(getParameterFinanceSmoke, "txt_amountUluBankReconsillAdjustment");
		chk_advicePathBankReconcilation = findElementInXLSheet(getParameterFinanceSmoke, "chk_advicePathBankReconcilation");
		lbl_amountAdviceBankReconcilation = findElementInXLSheet(getParameterFinanceSmoke, "lbl_amountAdviceBankReconcilation");
		lbl_reconcilAmount = findElementInXLSheet(getParameterFinanceSmoke, "lbl_reconcilAmount");
		btn_applyDescriptionBankeconcilation = findElementInXLSheet(getParameterFinanceSmoke, "btn_applyDescriptionBankeconcilation");
		btn_costAllocationAdjustmentBankReconcilation = findElementInXLSheet(getParameterFinanceSmoke, "btn_costAllocationAdjustmentBankReconcilation");
		btn_updateCostCenter = findElementInXLSheet(getParameterFinanceSmoke, "btn_updateCostCenter");
		btn_scrollDownArrowBankReconsilGrid = findElementInXLSheet(getParameterFinanceSmoke, "btn_scrollDownArrowBankReconsilGrid");
		div_reconcilAdvice = findElementInXLSheet(getParameterFinanceSmoke, "div_reconcilAdvice");

		/* Fin_TC_021 */
		btn_pettyCash = findElementInXLSheet(getParameterFinanceSmoke, "btn_pettyCash");
		btn_newPettyCash = findElementInXLSheet(getParameterFinanceSmoke, "btn_newPettyCash");
		drop_pettyCashAccount = findElementInXLSheet(getParameterFinanceSmoke, "drop_pettyCashAccount");
		btn_lookupPaidTo = findElementInXLSheet(getParameterFinanceSmoke, "btn_lookupPaidTo");
		txt_searchEmployee = findElementInXLSheet(getParameterFinanceSmoke, "txt_searchEmployee");
		lnk_resultEmployye = findElementInXLSheet(getParameterFinanceSmoke, "lnk_resultEmployye");
		txt_descPettyCash = findElementInXLSheet(getParameterFinanceSmoke, "txt_descPettyCash");
		drop_pettyCashType = findElementInXLSheet(getParameterFinanceSmoke, "drop_pettyCashType");
		txt_amountPettyCash = findElementInXLSheet(getParameterFinanceSmoke, "txt_amountPettyCash");
		nos_costApportionPettyCash = findElementInXLSheet(getParameterFinanceSmoke, "nos_costApportionPettyCash");
		drop_costCenterPettyCash = findElementInXLSheet(getParameterFinanceSmoke, "drop_costCenterPettyCash");
		txt_costPresentagePettyCash = findElementInXLSheet(getParameterFinanceSmoke, "txt_costPresentagePettyCash");
		btn_journelEntryPettyCashHeaderTExt = findElementInXLSheet(getParameterFinanceSmoke, "btn_journelEntryPettyCashHeaderTExt");
		
		/* Fin_TC_023 */
		btn_glLookupPettyCash = findElementInXLSheet(getParameterFinanceSmoke, "btn_glLookupPettyCash");
		nos_narrationPettyCash = findElementInXLSheet(getParameterFinanceSmoke, "nos_narrationPettyCash");
		txt_descriptionNarationPettyCash = findElementInXLSheet(getParameterFinanceSmoke, "txt_descriptionNarationPettyCash");
		btn_applyDescriptionPettyCash = findElementInXLSheet(getParameterFinanceSmoke, "btn_applyDescriptionPettyCash");
		btn_iouLookup = findElementInXLSheet(getParameterFinanceSmoke, "btn_iouLookup");
		lnk_iouResult = findElementInXLSheet(getParameterFinanceSmoke, "lnk_iouResult");
		
		/* Smoke_Finance_001 */
		btn_bankaAdjustment = findElementInXLSheet(getParameterFinanceSmoke, "btn_bankaAdjustment");
		btn_newBankAdjustment = findElementInXLSheet(getParameterFinanceSmoke, "btn_newBankAdjustment");
		drop_bankBankAdjustmentSmoke = findElementInXLSheet(getParameterFinanceSmoke, "drop_bankBankAdjustmentSmoke");
		drop_acoountNoBankAdjustmentSmoke = findElementInXLSheet(getParameterFinanceSmoke, "drop_acoountNoBankAdjustmentSmoke");
		btn_glAccountLookupBankAdjustmentSmoke = findElementInXLSheet(getParameterFinanceSmoke, "btn_glAccountLookupBankAdjustmentSmoke");
		txt_glInFrontPageBankAdjustmentSmoke = findElementInXLSheet(getParameterFinanceSmoke, "txt_glInFrontPageBankAdjustmentSmoke");
		txt_amountBankAdjustmentSmoke = findElementInXLSheet(getParameterFinanceSmoke, "txt_amountBankAdjustmentSmoke");
		btn_summaryBankAdjustmentSmoke = findElementInXLSheet(getParameterFinanceSmoke, "btn_summaryBankAdjustmentSmoke");
		header_releasedBankAdjustment = findElementInXLSheet(getParameterFinanceSmoke, "header_releasedBankAdjustment");
		
		/* Smoke_Finance_011 */
		btn_journelEntryPage = findElementInXLSheet(getParameterFinanceSmoke, "btn_journelEntryPage");
		btn_newJournelEntryPage = findElementInXLSheet(getParameterFinanceSmoke, "btn_newJournelEntryPage");
		btn_description = findElementInXLSheet(getParameterFinanceSmoke, "btn_description");
		txt_descriptionJournelEntry = findElementInXLSheet(getParameterFinanceSmoke, "txt_descriptionJournelEntry");
		btn_applyDescriptionJournelEntry = findElementInXLSheet(getParameterFinanceSmoke, "btn_applyDescriptionJournelEntry");
		btn_addRowJournelEntry = findElementInXLSheet(getParameterFinanceSmoke, "btn_addRowJournelEntry");
		btn_addRow2JournelEntry = findElementInXLSheet(getParameterFinanceSmoke, "btn_addRow2JournelEntry");
		btn_serchLoockupGLJournelEntry = findElementInXLSheet(getParameterFinanceSmoke, "btn_serchLoockupGLJournelEntry");
		btn_serchLoockupGLJournelEntry2 = findElementInXLSheet(getParameterFinanceSmoke, "btn_serchLoockupGLJournelEntry2");
		btn_serchLoockupGLJournelEntry3 = findElementInXLSheet(getParameterFinanceSmoke, "btn_serchLoockupGLJournelEntry3");
		txt_value1JjornelEntry = findElementInXLSheet(getParameterFinanceSmoke, "txt_value1JjornelEntry");
		txt_value2JjornelEntry = findElementInXLSheet(getParameterFinanceSmoke, "txt_value2JjornelEntry");
		txt_value3JjornelEntry = findElementInXLSheet(getParameterFinanceSmoke, "txt_value3JjornelEntry");
		lbl_value1JournelAccount = findElementInXLSheet(getParameterFinanceSmoke, "lbl_value1JournelAccount");
		lbl_value2JournelAccount = findElementInXLSheet(getParameterFinanceSmoke, "lbl_value2JournelAccount");
		lbl_value3JournelAccount = findElementInXLSheet(getParameterFinanceSmoke, "lbl_value3JournelAccount");
		header_releasedJE = findElementInXLSheet(getParameterFinanceSmoke, "header_releasedJE");
		
		/* Smoke_Finance_02 */
		btn_bankFacilityAgreement = findElementInXLSheet(getParameterFinanceSmoke, "btn_bankFacilityAgreement");
		btn_newBankFacilityAgreement = findElementInXLSheet(getParameterFinanceSmoke, "btn_newBankFacilityAgreement");
		txt_originalDocNoBFA = findElementInXLSheet(getParameterFinanceSmoke, "txt_originalDocNoBFA");
		drop_bankFacilityGroupBFA = findElementInXLSheet(getParameterFinanceSmoke, "drop_bankFacilityGroupBFA");
		txt_startDateBFA = findElementInXLSheet(getParameterFinanceSmoke, "txt_startDateBFA");
		txt_endDateBFA = findElementInXLSheet(getParameterFinanceSmoke, "txt_endDateBFA");
		lbl_loanCreationConfirmation1 = findElementInXLSheet(getParameterFinanceSmoke, "lbl_loanCreationConfirmation1");
		lbl_loanCreationConfirmation2 = findElementInXLSheet(getParameterFinanceSmoke, "lbl_loanCreationConfirmation2");
		
		/* Common customer creation */
		btn_accountInformation = findElementInXLSheet(getParameterFinanceSmoke, "btn_accountInformation");
		btn_newAccountCustomerAccount = findElementInXLSheet(getParameterFinanceSmoke, "btn_newAccountCustomerAccount");
		txt_accountNameCustomerAccount = findElementInXLSheet(getParameterFinanceSmoke, "txt_accountNameCustomerAccount");
		drop_accountGtopuCustomerAccount = findElementInXLSheet(getParameterFinanceSmoke, "drop_accountGtopuCustomerAccount");
		
	}

	public static void readData() throws Exception {
		/* Fin_TC_001 */
		String computerName = System.getenv().get("COMPUTERNAME");
		if (computerName.equals("DESKTOP-C6E8CJU") || computerName.equals("MADHUSHAN-LAPfg")) {
			site_Url = findElementInXLSheet(getDataFinanceSmoke, "siteUrlExternal");
		} else {
			site_Url = findElementInXLSheet(getDataFinanceSmoke, "site_Url");
		}
		userNameData = findElementInXLSheet(getDataFinanceSmoke, "userNameData");
		passwordData = findElementInXLSheet(getDataFinanceSmoke, "passwordData");
		customerAccountIPA = findElementInXLSheet(getDataFinanceSmoke, "customerAccountIPA");
		testAllCharcterWordIPA = findElementInXLSheet(getDataFinanceSmoke, "testAllCharcterWordIPA");
		curenceIPA = findElementInXLSheet(getDataFinanceSmoke, "curenceIPA");
		amountIPA = findElementInXLSheet(getDataFinanceSmoke, "amountIPA");
		postBusinessUnitIPA = findElementInXLSheet(getDataFinanceSmoke, "postBusinessUnitIPA");

		/* Fin_TC_002 */
		amountCRV = findElementInXLSheet(getDataFinanceSmoke, "amountCRV");
		glAccount = findElementInXLSheet(getDataFinanceSmoke, "glAccount");
		paidCurrencyCRV = findElementInXLSheet(getDataFinanceSmoke, "paidCurrencyCRV");
		paymentMethodCRV = findElementInXLSheet(getDataFinanceSmoke, "paymentMethodCRV");
		filterCurrencyCRV = findElementInXLSheet(getDataFinanceSmoke, "filterCurrencyCRV");
		payingAmountCRV = findElementInXLSheet(getDataFinanceSmoke, "payingAmountCRV");
		customerAccountCRV = findElementInXLSheet(getDataFinanceSmoke, "customerAccountCRV");
		paidAmountCRV = findElementInXLSheet(getDataFinanceSmoke, "paidAmountCRV");

		/* Fin_TC_003 */
		currencySalesInvoise = findElementInXLSheet(getDataFinanceSmoke, "currencySalesInvoise");
		slaesUnitSlaesInvoice = findElementInXLSheet(getDataFinanceSmoke, "slaesUnitSlaesInvoice");
		serviceProductSalesInvooice = findElementInXLSheet(getDataFinanceSmoke, "serviceProductSalesInvooice");
		unitPriceSalesInvice = findElementInXLSheet(getDataFinanceSmoke, "unitPriceSalesInvice");

		/* Fin_TC_004 */
		paymentMethodBD = findElementInXLSheet(getDataFinanceSmoke, "paymentMethodBD");
		bankBD = findElementInXLSheet(getDataFinanceSmoke, "bankBD");

		/* Fin_TC_006 */
		customerCustomerDebitMemo = findElementInXLSheet(getDataFinanceSmoke, "customerCustomerDebitMemo");
		departmentCostAllocationCustomerDebitMemo = findElementInXLSheet(getDataFinanceSmoke,
				"departmentCostAllocationCustomerDebitMemo");
		oresentageCostAllocationCustomerDebitMEmo = findElementInXLSheet(getDataFinanceSmoke,
				"oresentageCostAllocationCustomerDebitMEmo");
		paymentMethodCRV7 = findElementInXLSheet(getDataFinanceSmoke, "paymentMethodCRV7");
		postBusinessUnitCustomerReciptVoucher = findElementInXLSheet(getDataFinanceSmoke,
				"postBusinessUnitCustomerReciptVoucher");
		paymentMethodCustomerReciptVoucher = findElementInXLSheet(getDataFinanceSmoke, "paymentMethodCustomerReciptVoucher");
		payingAmountCRV7 = findElementInXLSheet(getDataFinanceSmoke, "payingAmountCRV7");
		paidAmountCRV7 = findElementInXLSheet(getDataFinanceSmoke, "paidAmountCRV7");
		filterCurrencyCustomerReciptVoucher = findElementInXLSheet(getDataFinanceSmoke,
				"filterCurrencyCustomerReciptVoucher");

		/* Fin_TC_007 */
		paidCurrency08 = findElementInXLSheet(getDataFinanceSmoke, "paidCurrency08");
		paymentMethod08 = findElementInXLSheet(getDataFinanceSmoke, "paymentMethod08");
		payingAmount08 = findElementInXLSheet(getDataFinanceSmoke, "payingAmount08");
		paidAmpunt8 = findElementInXLSheet(getDataFinanceSmoke, "paidAmpunt8");
		bankCheque8 = findElementInXLSheet(getDataFinanceSmoke, "bankCheque8");

		/* Fin_TC_009 */
		payMethodBankDeposit09 = findElementInXLSheet(getDataFinanceSmoke, "payMethodBankDeposit09");
		bank09 = findElementInXLSheet(getDataFinanceSmoke, "bank09");
		bankAccount09 = findElementInXLSheet(getDataFinanceSmoke, "bankAccount09");

		/* Fin_TC_010 */
		vendorOutboundPaymentAdvice = findElementInXLSheet(getDataFinanceSmoke, "vendorOutboundPaymentAdvice");
		referenceCodeOPA = findElementInXLSheet(getDataFinanceSmoke, "referenceCodeOPA");
		descriptionOPA = findElementInXLSheet(getDataFinanceSmoke, "descriptionOPA");
		currencyOPA = findElementInXLSheet(getDataFinanceSmoke, "currencyOPA");
		amountOPA = findElementInXLSheet(getDataFinanceSmoke, "amountOPA");
		postBusinessUnitOPA = findElementInXLSheet(getDataFinanceSmoke, "postBusinessUnitOPA");

		/* Fin_TC_011 */
		curencyTypeOutboundPayment = findElementInXLSheet(getDataFinanceSmoke, "curencyTypeOutboundPayment");
		filterCurencyTypeOutboundPayment = findElementInXLSheet(getDataFinanceSmoke, "filterCurencyTypeOutboundPayment");
		payBookOutboundPayment = findElementInXLSheet(getDataFinanceSmoke, "payBookOutboundPayment");

		/* Fin_TC_012 */
		vendorPurchaseInvoice = findElementInXLSheet(getDataFinanceSmoke, "vendorPurchaseInvoice");
		serviceProductPurchaseInvoice = findElementInXLSheet(getDataFinanceSmoke, "serviceProductPurchaseInvoice");
		unitPricePurchaseInvice = findElementInXLSheet(getDataFinanceSmoke, "unitPricePurchaseInvice");
		currencyPurchaseInvoice = findElementInXLSheet(getDataFinanceSmoke, "currencyPurchaseInvoice");
		
		/* Fin_TC_014 */
		vendorVendorCreditMemo = findElementInXLSheet(getDataFinanceSmoke, "vendorVendorCreditMemo");
		description = findElementInXLSheet(getDataFinanceSmoke, "description");
		referenceCode = findElementInXLSheet(getDataFinanceSmoke, "referenceCode");
		curencyCreditMemo = findElementInXLSheet(getDataFinanceSmoke, "curencyCreditMemo");
		glAccountCreditMemo = findElementInXLSheet(getDataFinanceSmoke, "glAccountCreditMemo");
		amountCreditMemo = findElementInXLSheet(getDataFinanceSmoke, "amountCreditMemo");
		postBusinessUnit = findElementInXLSheet(getDataFinanceSmoke, "postBusinessUnit");
		costUnit = findElementInXLSheet(getDataFinanceSmoke, "costUnit");
		costCenterAlloacatePresentage = findElementInXLSheet(getDataFinanceSmoke, "costCenterAlloacatePresentage");
		
		/* Fin_TC_015 */
		vendorVRP = findElementInXLSheet(getDataFinanceSmoke, "vendorVRP");
		curencyVRP = findElementInXLSheet(getDataFinanceSmoke, "curencyVRP");
		filterCurrency = findElementInXLSheet(getDataFinanceSmoke, "filterCurrency");
		postBusinessUnitOutboundPaymentVendorPaymentVoucher = findElementInXLSheet(getDataFinanceSmoke, "postBusinessUnitOutboundPaymentVendorPaymentVoucher");
		payBookVendorPaymentVoucher = findElementInXLSheet(getDataFinanceSmoke, "payBookVendorPaymentVoucher");
		
		/* Fin_TC_016 */
		payBookLetterOFGurentee = findElementInXLSheet(getDataFinanceSmoke, "payBookLetterOFGurentee");
		amountLOG = findElementInXLSheet(getDataFinanceSmoke, "amountLOG");
		currencyLOG = findElementInXLSheet(getDataFinanceSmoke, "currencyLOG");
		vebdorLetterOfGurentee = findElementInXLSheet(getDataFinanceSmoke, "vebdorLetterOfGurentee");
		
		/* Fin_TC_017 */
		originalDocNoLetterOfGurentee = findElementInXLSheet(getDataFinanceSmoke, "originalDocNoLetterOfGurentee");
		
		/*Fin_TC_019*/
		bankAccountBankAdjustment = findElementInXLSheet(getDataFinanceSmoke, "bankAccountBankAdjustment");
		glAccountBankAdjustment = findElementInXLSheet(getDataFinanceSmoke, "glAccountBankAdjustment");
		bankBankAdjustment = findElementInXLSheet(getDataFinanceSmoke, "bankBankAdjustment");
		costAllocatioPresentageNankAdjustment = findElementInXLSheet(getDataFinanceSmoke, "costAllocatioPresentageNankAdjustment");
		postBusinessUnitBankAdjustment = findElementInXLSheet(getDataFinanceSmoke, "postBusinessUnitBankAdjustment");
		amountBankAdjustment = findElementInXLSheet(getDataFinanceSmoke, "amountBankAdjustment");
		costCenterBankAdjustment = findElementInXLSheet(getDataFinanceSmoke, "costCenterBankAdjustment");
		
		/* Fin_TC_020 */
		bankStatementREconcilationStatementBalance = findElementInXLSheet(getDataFinanceSmoke, "bankStatementREconcilationStatementBalance");
		glAccountAdjustmetnBankReconcillation = findElementInXLSheet(getDataFinanceSmoke, "glAccountAdjustmetnBankReconcillation");
		bankBankReconcillation = findElementInXLSheet(getDataFinanceSmoke, "bankBankReconcillation");
		bankAccountBankReconcilation = findElementInXLSheet(getDataFinanceSmoke, "bankAccountBankReconcilation");
		adjustmentDescriptionBankReconcilation = findElementInXLSheet(getDataFinanceSmoke, "adjustmentDescriptionBankReconcilation");
		departmentCostAllocationBankReconcilationAdjustment = findElementInXLSheet(getDataFinanceSmoke, "departmentCostAllocationBankReconcilationAdjustment");
		presentageCostAllocationBankReconcilationAdjustment = findElementInXLSheet(getDataFinanceSmoke, "presentageCostAllocationBankReconcilationAdjustment");
		
		/* Fn_TC_021 */
		employeePettyCash = findElementInXLSheet(getDataFinanceSmoke, "employeePettyCash");
		pettyCshType021 = findElementInXLSheet(getDataFinanceSmoke, "pettyCshType021");
		amountPettyCash = findElementInXLSheet(getDataFinanceSmoke, "amountPettyCash");
		costPresentagePettyCash = findElementInXLSheet(getDataFinanceSmoke, "costPresentagePettyCash");
		costCenterPettyCsh = findElementInXLSheet(getDataFinanceSmoke, "costCenterPettyCsh");
		descriptionPettyCashIOU = findElementInXLSheet(getDataFinanceSmoke, "descriptionPettyCashIOU");
		
		/* Fin_TC_023 */
		narrationDescription = findElementInXLSheet(getDataFinanceSmoke, "narrationDescription");
		
		/* Smoke_Finance_001 */
		bankBankAdjustmentSmoke = findElementInXLSheet(getDataFinanceSmoke, "bankBankAdjustmentSmoke");
		accountBankAdjustmentSmoke = findElementInXLSheet(getDataFinanceSmoke, "accountBankAdjustmentSmoke");
		glAccountBankAdjustmentSmoke = findElementInXLSheet(getDataFinanceSmoke, "glAccountBankAdjustmentSmoke");
		amountBankAdjustmentSmoke = findElementInXLSheet(getDataFinanceSmoke, "amountBankAdjustmentSmoke");
		
		/* Smoke_Finance_011 */
		account1JournelEntry = findElementInXLSheet(getDataFinanceSmoke, "account1JournelEntry");
		account2JournelEntry = findElementInXLSheet(getDataFinanceSmoke, "account2JournelEntry");
		account3JournelEntry = findElementInXLSheet(getDataFinanceSmoke, "account3JournelEntry");
		value1JournelEntry = findElementInXLSheet(getDataFinanceSmoke, "value1JournelEntry");
		value2JournelEntry = findElementInXLSheet(getDataFinanceSmoke, "value2JournelEntry");
		value3JournelEntry = findElementInXLSheet(getDataFinanceSmoke, "value3JournelEntry");
		descriptionJournelEntry = findElementInXLSheet(getDataFinanceSmoke, "descriptionJournelEntry");
		
		/* Smoke_Finance_02 */
		originalDocNoBFA = findElementInXLSheet(getDataFinanceSmoke, "originalDocNoBFA");
		bankNameBFA = findElementInXLSheet(getDataFinanceSmoke, "bankNameBFA");
		accountNoBFA = findElementInXLSheet(getDataFinanceSmoke, "accountNoBFA");
		currencyBFA = findElementInXLSheet(getDataFinanceSmoke, "currencyBFA");
		amountBFA = findElementInXLSheet(getDataFinanceSmoke, "amountBFA");
		bankFacilityGroup = findElementInXLSheet(getDataFinanceSmoke, "bankFacilityGroup");
		acoounLoanConfiramationBFA = findElementInXLSheet(getDataFinanceSmoke, "acoounLoanConfiramationBFA");
		
		/*Common customer account creation*/
		accountGroup = findElementInXLSheet(getDataFinanceSmoke, "accountGroup");

	}
}
