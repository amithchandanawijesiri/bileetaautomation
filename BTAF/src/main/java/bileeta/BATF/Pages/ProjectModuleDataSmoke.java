package bileeta.BATF.Pages;

import bileeta.BTAF.Utilities.TestBase;

public class ProjectModuleDataSmoke extends TestBase {

	/* Reading the element locators to variables */
	/* Login */
	protected static String siteLogo;
	protected static String txt_username;
	protected static String txt_password;
	protected static String btn_login;
	protected static String lnk_home;
	protected static String btn_navigationPane;
	protected static String div_loginVerification;
	protected static String btn_projectModule;
	protected static String btn_projectQuatation;
	protected static String btn_newProjectQuatation;
	protected static String header_newProjectQuatation;
	protected static String btn_customerLookup;
	protected static String txt_customerAccountFrontPageProjectQuatation;
	protected static String txt_searchCustomer;
	protected static String lnk_inforOnLookupInformationReplace;
	protected static String drop_salesUnitProjectQuatation;
	protected static String btn_productLookupOnGridRowReplace;
	protected static String txt_searchProduct;
	protected static String drop_typeOnGridProjectQuatationRowReplace;
	protected static String lbl_productOnGridProductQuatationRowReplace;
	protected static String lbl_OHOnGridProductQuatation;
	protected static String txt_quantityOnGridProductQuatationRowReplace;
	protected static String btn_addNewRecordGrid;
	protected static String drop_referenceGrouopOnGridRowReplce;
	protected static String btn_labourLookupProductQuatation;
	protected static String txt_unitPriceProductQuatationRowReplace;
	protected static String txt_labourSearch;
	protected static String btn_resourceLookupProductQuatation;
	protected static String txt_searchResource;
	protected static String drop_referenceCodeExpence;
	protected static String btn_checkout;
	protected static String lbl_gridLineTotalParts;
	protected static String lbl_gridLineTotalLabour;
	protected static String lbl_labourTotalCheckoutBar;
	protected static String lbl_partsTotalCheckoutBar;
	protected static String btn_draft;
	protected static String btn_release;
	protected static String header_draftedProjectQuatation;
	protected static String header_releasedProjectQuatation;
	protected static String header_confirmedProjectQuatation;
	protected static String btn_versionProductQuatation;
	protected static String div_versionPopup;
	protected static String btn_confirmVersion;
	protected static String btn_confirmQuatation;
	protected static String btn_action;
	protected static String btn_convertToProject;
	protected static String header_newProject;
	protected static String txt_projectCode;
	protected static String txt_projectTitle;
	protected static String drop_projectGroup;
	protected static String txt_customerAccountFrontPageProject;
	protected static String btn_lookupResponsiblePerson;
	protected static String txt_responsiblePersonFrontPageProject;
	protected static String txt_searchPricingProfile;
	protected static String txt_frontPagePricingProfileProject;
	protected static String txt_requestDateProject;
	protected static String txt_projectLocationFrontPageProject;
	protected static String btn_locationInformationLookup;
	protected static String txt_searchLocation;
	protected static String txt_projectValue;
	protected static String lbl_totalCheckoutBarProjectQuatation;
	protected static String header_draftedProject;
	protected static String header_releasedProject;
	protected static String btn_edit;
	protected static String btn_update;
	protected static String tab_workBreackdown;
	protected static String lbl_draftedTaskWorkBreackdownProject;
	protected static String btn_releaseTaskWorkBreackdown;
	protected static String para_taskReleaseValidationWorkBreackdown;
	protected static String header_projectQuatationByPage;
	protected static String txt_employeeOnGridProductQuatation;
	protected static String txt_resourseOnGridProductQuatation;
	protected static String lbl_docNumber;
	protected static String btn_pricingProfileLookup;
	protected static String btn_expandProjectTask;
	protected static String btn_project;
	protected static String btn_newProject;
	protected static String btn_customerLookupProject;
	protected static String header_projectByPage;
	protected static String drop_businessUnitProject;
	protected static String btn_addTask;
	protected static String txt_taskNameTaskWindow;
	protected static String btn_inputProductsTabTaskWindow;
	protected static String btn_inputProductsLookupTaskWindow;
	protected static String div_selectedInputProductTaskWindow;
	protected static String txt_quantityInputProductsTaskWindow;
	protected static String chk_billableTaskWindowInputProducts;
	protected static String tab_labourTaskWindow;
	protected static String btn_lookupProductTaskWindowLabourTab;
	protected static String div_selectedServiceLabourTabTaskWindow;
	protected static String txt_hoursLabourTabTaskWindow;
	protected static String btn_fiveLabourHoursTaskWindow;
	protected static String btn_okLabourHoursTaskWindow;
	protected static String chk_billableLabourHoursTaskWindow;
	protected static String tab_resourseTaskWindow;
	protected static String drop_resourseGroupTaskWindow;
	protected static String btn_lookupResourseTaskWindow;
	protected static String div_selectedResourseTaskWindow;
	protected static String btn_lookupServiceTaskWindowResorseTab;
	protected static String div_serviceProductResourseTabTaskWindow;
	protected static String txt_hoursResourseTabTaskWindow;
	protected static String btn_fiveResourseHoursTaskWindow;
	protected static String btn_okResourseHoursTaskWindow;
	protected static String chk_billableResourseTabTaskWindow;
	protected static String tab_expenceTaskWindow;
	protected static String drop_analysisCodeExpenceTabTaskWindow;
	protected static String btn_nraationExpenceTabTaskWindow;
	protected static String txt_narrationTaskWindowExpenceTab;
	protected static String btn_applyNarrationExpence;
	protected static String txt_selectedNarrationTaskWindowExpenceTab;
	protected static String btn_serviceLoockupExpenceTabTaskWindow;
	protected static String div_selectedServiceExpenceTabTaskWindow;
	protected static String txt_estimatedExpenceTaskWindowExpenceTab;
	protected static String chk_billableExpenceTabTaskWindow;
	protected static String tab_overheadTaskWindow;
	protected static String div_autoLoadedOverhead;
	protected static String chk_billableOverheadTabTaskWindow;
	protected static String btn_updateTaskWindow;
	protected static String header_taskWindow;
	protected static String drop_contractTypeTaskWindow;
	protected static String tab_subContractServices;
	protected static String btn_vendorLookup;
	protected static String txt_vendorSearch;
	protected static String txt_vendorSubContractTabTaskWindow;
	protected static String btn_serviceProductLookupRowReplaceSubContractTab;
	protected static String div_selectedServiceSubContractTabTaskWindowRowReplace;
	protected static String txt_estimatedAmountServiceOneSubContractTypeTaskRowReplace;
	protected static String chk_billableServicesSubContractTypeTaskRowReplace;
	protected static String btn_addNewRowSubContractTaskTaskWindow;
	protected static String tab_interdepartmentTaskWindow;
	protected static String txt_loadedEmployeeNameSummarTabTaskWindowInterdepartmentalTask;
	protected static String btn_ownerLookupTaskWindow;
	protected static String txt_selectedOwnerInterdepartmentTabTaskWindow;
	protected static String txt_estimatedCostTaskWindowInterdepartmentalTask;
	protected static String txt_estimatedDateInterdepartmentTaskWindow;
	protected static String btn_dayClickTaskWindow;
	protected static String btn_releaseTaskWorkBreackdownPendingFirst;
	protected static String para_validatorTaskReleaseSuccessfully;
	protected static String lnk_employeeTaskWindowInterdepartmentalTabEmpReplace;
	protected static String lbl_allocationExpenceCostingTab;
	protected static String tab_costingSummary;
	protected static String lbl_allocationResourseCostCostingTab;
	protected static String lbl_allocationHourseCostCostingTab;
	protected static String lbl_allocationMaterialsCostCostingTab;
	protected static String lbl_allocationSubContractCostCostingTab;
	protected static String lbl_allocationOverheadCostCostingTab;
	protected static String lbl_allocationInterdepartmentalCostCostingTab;
	protected static String lbl_releasedTaskWorkBreackdowntaskNoReplace;
	protected static String btn_lookupPerformer;
	protected static String txt_performerInterDepartmentTabWorkBreackdown;

	/* Smoke_Project_004 */
	protected static String btn_createInternalOrder;
	protected static String header_genarateInternalOrder;
	protected static String drop_taskInternalOrder;
	protected static String div_productsGenarateInternalOrderWindowProductReplace;
	protected static String chk_productGenarateInternalOrder;
	protected static String btn_applyGenarateInternalOrder;
	protected static String header_draftedInternalOrder;
	protected static String header_releasedInternalOrder;
	protected static String header_draftedInternalDispatchOrder;
	protected static String header_releasedInternalDispatchOrder;
	protected static String tab_overviewProject;
	protected static String tab_inputProductsOverviewTab;
	protected static String lbl_internalOrderOverviewTabDocNoReplace;
	protected static String lnk_goToPage;
	protected static String header_newInternalOrder;
	protected static String header_newInternalDispatchOrder;

	/* Smoke_Project_005 */
	protected static String btn_service;
	protected static String btn_dailyWorkSheet;
	protected static String btn_newDailyWorkSheet;
	protected static String btn_dailyWorksheetProjectJourney;
	protected static String header_newDailyWorkSheet;
	protected static String btn_addNewDailyWorsheet;
	protected static String header_AddNewJobWindow;
	protected static String lnl_releventTaskProjectNoReplace;
	protected static String btn_lookupEmployyeAddNewJobWindow;
	protected static String txt_inTimeAddNewJob;
	protected static String btn_eightIntimeHoursAddNEwJob;
	protected static String btn_okAddNewwJobHoursInTime;
	protected static String txt_outTimeAddNewJob;
	protected static String btn_fiveOuttimeHoursAddNEwJob;
	protected static String btn_okAddNewwJobHoursOutTime;
	protected static String btn_updateAddnewJob;
	protected static String header_draftedDailyWorkSheet;
	protected static String header_releasedDailyWorkSheet;
	protected static String lbl_actualHourseCostCostingTab;
	protected static String btn_addProjectTaskLookup;
	protected static String txt_serachProjectTask;
	protected static String lnk_employeeProjectEmpReplace;

	/* Smoke_Project_006 */
	protected static String btn_subContractOrderActions;
	protected static String header_subContractWindow;
	protected static String drop_task;
	protected static String chk_selectAllWindowComeFromProjectAction;
	protected static String btn_applySucContractOrders;
	protected static String header_draftedPurchaseOrder;
	protected static String header_releasedPurchaseOrder;
	protected static String header_newPurchaseOrder;
	protected static String header_draftedPurchaseInvoice;
	protected static String header_releasedPurchaseInvoice;
	protected static String header_newPurchaseInvoice;
	protected static String tab_subContractServicesOverviewTab;
	protected static String lbl_purchaseOrderOverviewTabDocNoReplace;
	protected static String lbl_actualSubContractCostCostingTab;

	/* Smoke_Project_007 */
	protected static String btn_actualUpdate;
	protected static String txt_actualUpdateQuantity;
	protected static String btn_updateActualUpdate;
	protected static String tab_resourseActualUpdateWindow;
	protected static String txt_actualResourseHours;
	protected static String btn_fiveActualResourseHours;
	protected static String btn_okActualResourseHours;
	protected static String lbl_actualMaterialCostCostingTab;
	protected static String lbl_actualResourseCostCostingTab;
	protected static String lbl_actualOverheadCostCostingTab;

	/* Smoke_Project_008 */
	protected static String btn_inventoryAndWarehouseModule;
	protected static String btn_internalReturnOrder;
	protected static String btn_newInternalReturnOrder;
	protected static String btn_journeyWIPReturns;
	protected static String drop_warehouseInternalReturnOrder;
	protected static String btn_docIconInternalReturnOrder;
	protected static String txt_docSearchInternalReturnOrder;
	protected static String chk_documentFirstInternalReturnOrder;
	protected static String btn_applyInternalReturnOrder;
	protected static String header_newInternalReturnOrder;
	protected static String header_draftedIRO;
	protected static String header_releasedIRO;
	protected static String header_draftedInternalRecipt;
	protected static String header_releasedInternalReceipt;
	protected static String btn_serialBatchCapture;
	protected static String btn_itemNumberOneSerielBatchCapture;
	protected static String btn_updateSerielBatchCapture;
	protected static String btn_serialBatchCaptureBackButton;
	protected static String btn_searchInternalReturnOrder;
	protected static String div_capturedCountOnGrdSerialBatchCaptureInternalRecipt;
	protected static String txt_captureQuantityOnGridSerilBatchCaptureInternalRecipt;

	/* Smoke_Project_009 */
	protected static String btn_finance;
	protected static String btn_outboundPaymentAdvice;
	protected static String btn_newOutboundPaymentAdvice;
	protected static String btn_accruelVoucherJourney;
	protected static String btn_vendorLookupOutboundPaymentAdvice;
	protected static String drop_anlyzeCodeAccruelVoucher;
	protected static String txt_rowDescriptionAccruelVoucher;
	protected static String txt_amountAccruelVoucher;
	protected static String btn_costAllocationWidgetAccruelVoucher;
	protected static String txt_costinfAllocationPresentageAccruelVoucher;
	protected static String drop_costAssignmentType;
	protected static String btn_updateCostAllocationAccruelVoucher;
	protected static String header_newOutboundPaymentAdvice;
	protected static String header_draftedOutboundPaymentAdvice;
	protected static String header_releasedOutboundPaymentAdvice;
	protected static String lbl_actualCostExpence;
	protected static String btn_addRecordCostAllocationOPA;

	/* Smoke_Project_010 */
	protected static String btn_createInvoiceProposals;
	protected static String header_invoiceProposalWindow;
	protected static String drop_invoiceProposalFilter;
	protected static String chk_selectAllInvoiceProposalWindow;
	protected static String btn_productLoockupInvoiceProposalWindowRowReplace;
	protected static String txt_quantitySecondRowInvoiceProposalTask;
	protected static String btn_applyInvoiceProposalWindow;
	protected static String header_newInvoiceProposal;
	protected static String header_draftedInvoiceProposal;
	protected static String header_releasedInvoiceProposal;
	protected static String header_newServiceInvoice;
	protected static String header_draftedServiceInvoice;
	protected static String header_releasedServiceInvoice;
	protected static String tab_invoiceDetailsOverviewTab;
	protected static String div_salesInvoiceInviceDetailsTabOverviewDocReplace;
	protected static String tab_serviceDeatailsServiceInvoice;

	/* Smoke_Project_011 */
	protected static String btn_serviceCalender;
	protected static String btn_interdepartmntalTaskOnWorkingCalender;
	protected static String header_newServiceJobOrder;
	protected static String header_draftedServiceJobOrder;
	protected static String header_releasedServiceJobOrder;
	protected static String drop_serviceGroupServiceJobOrder;
	protected static String drop_priorityServiceJobOrder;
	protected static String btn_lookupServiceLocation;
	protected static String lnk_firstRecordPricingProfile;

	/* Smoke_Project_012 */
	protected static String btn_addProductTagNoActions;
	protected static String heaser_addProductTagNoPopup;
	protected static String btn_addProductWidgetAddProductPopup;
	protected static String chk_firstRegSerielAddProductPopup;
	protected static String btn_applyAssProductTagNoPopup;
	protected static String btn_stopActionsSJO;
	protected static String header_popupCompleteProcessSJO;
	protected static String btn_completeCompleteProcessSJP;
	protected static String labl_pocInterdepartmentTaskWorkBreackdown;
	protected static String labl_sjoNumberInterdepartmentTaskWorkBreackdown;
	protected static String btn_completeSJO;
	protected static String btn_yesCompleteStatusSJO;
	protected static String txt_serviceJobOrderCompletionPostDate;
	protected static String btn_dayReplaceServiceJobOrderCompleteDate;
	protected static String btn_applySJOCompletionDate;
	protected static String header_completedServiceJobOrder;

	/* Smoke_Project_013 */
	protected static String btn_updateTaskPocActtions;
	protected static String drop_taskPOCUpdate;
	protected static String txt_presentagePOC;
	protected static String btn_applyPOC;
	protected static String lbl_pocPresenatgeTaskOneWorkBreackdown;
	protected static String lbl_pocPresenatgeTaskSubContractWorkBreackdown;

	/* Smoke_Project_014 */
	protected static String btn_projectCostEstimation;
	protected static String btn_newProjectCostEstimation;
	protected static String header_newProjectCostEstimation;
	protected static String header_draftedProjectCostEstimation;
	protected static String header_releasedProjectCostEstimation;
	protected static String txt_descriptionCostEstimation;
	protected static String tab_estimationDetails;
	protected static String btn_productLookupRowReplaceCostEstimation;
	protected static String drop_warehouseCostEstimation;
	protected static String txt_quantityInputProdyctsPCE;
	protected static String txt_unitPricePCERowReplace;
	protected static String chk_billablePCERowReplace;
	protected static String drop_typePCE;
	protected static String drop_refrenceCodePCE;
	protected static String btn_addNewRecordGridCommon;
	protected static String btn_pricingProfileLookupCostEstimation;
	protected static String btn_tabSummaryCostEstimation;

	/* Smoke_Project_015 */
	protected static String btn_projectModel;
	protected static String btn_newProjectModel;
	protected static String header_newProjectModel;
	protected static String txt_projectCodeProjectModel;
	protected static String txt_projectModelName;
	protected static String drop_inputWarehouseProjectModel;
	protected static String drop_outputWarehouseProjectModel;
	protected static String header_draftedProjectModel;
	protected static String header_releasedProjectModel;
	protected static String tab_workBreackDownProjectModel;
	protected static String txt_daysCountSummaryTabProductionModel;

	/* Reading the Data to variables */
	protected static String entutionURL;
	protected static String automationTenantProjectSmokeUsername;
	protected static String automationTenantProjectSmokePassword;
	protected static String customer;
	protected static String salesUnit;
	protected static String typeInputProducts;
	protected static String typeExpence;
	protected static String typeServices;
	protected static String batchFifoProduct;
	protected static String employee;
	protected static String employeeName;
	protected static String typeLabour;
	protected static String referenceGroupLabour;
	protected static String serviceProduct;
	protected static String serviceProduct2;
	protected static String serviceProduct3;
	protected static String serviceProduct4;
	protected static String quantityFive;
	protected static String inputProductUnitPrice;
	protected static String labourUnitPrice;
	protected static String resourceUnitPrice;
	protected static String expenceUnitPrice;
	protected static String servicesUnitPrice;
	protected static String typeResource;
	protected static String referenceGroupMotorVehicle;
	protected static String resource;
	protected static String projectTitle;
	protected static String pricingProfile;
	protected static String location;
	protected static String businessUnit;
	protected static String task;
	protected static String naraation;
	protected static String expenceEstimatedAmount;
	protected static String overhead;
	protected static String contractTypeSubContract;
	protected static String taskSubContract;
	protected static String serviceOneEstimatedAmountSubContractTypeTask;
	protected static String serviceProduct5;
	protected static String serviceTwoEstimatedAmountSubContractTypeTask;
	protected static String taskNameInterdepartment;
	protected static String contractTypeInterDepartment;
	protected static String estimatedCostInterdepartmentalTask;

	/* Smoke_Project_004 */
	protected static String taskAll;

	/* Smoke_Project_007 */
	protected static String actualUpdateQuantity;

	/* Smoke_Project_008 */
	protected static String outptuWarehouse;
	protected static String remainingQuntityForReturn;

	/* Smoke_Project_009 */
	protected static String acruelVoucherRowDescription;
	protected static String acruelVoucherAmount;
	protected static String costAllocationPresentageAccruelVoucher;
	protected static String costAssignmentTypeProjectTask;

	/* Smoke_Project_010 */
	protected static String inoiceProposalFilterTask;
	protected static String serviceProduct6;
	protected static String serviceProduct7;
	protected static String invoiceProposalSecondRowQuantiry;

	/* Smoke_Project_013 */
	protected static String pocPresentage;

	/* Smoke_Project_014 */
	protected static String inputProductQuantityProjectCostEstimation;
	protected static String unitCostServicseProductsPCE;
	protected static String description;

	/* Smoke_Project_015 */
	protected static String projectName;
	protected static String durationOfDaysProdjectModel;

	// Calling the constructor
	public static void readElementlocators() throws Exception
	{
		/* Login */
		siteLogo = findElementInXLSheet(getParameterProjectSmoke, "site logo");
		txt_username = findElementInXLSheet(getParameterProjectSmoke, "userName");
		txt_password = findElementInXLSheet(getParameterProjectSmoke, "password");
		btn_login = findElementInXLSheet(getParameterProjectSmoke, "login");
		lnk_home = findElementInXLSheet(getParameterProjectSmoke, "headerlink");
		btn_navigationPane = findElementInXLSheet(getParameterProjectSmoke, "btn_navigationPane");
		div_loginVerification = findElementInXLSheet(getParameterProjectSmoke, "div_loginVerification");
		btn_projectModule = findElementInXLSheet(getParameterProjectSmoke, "btn_projectModule");
		btn_projectQuatation = findElementInXLSheet(getParameterProjectSmoke, "btn_projectQuatation");
		btn_newProjectQuatation = findElementInXLSheet(getParameterProjectSmoke, "btn_newProjectQuatation");
		header_newProjectQuatation = findElementInXLSheet(getParameterProjectSmoke, "header_newProjectQuatation");
		btn_customerLookup = findElementInXLSheet(getParameterProjectSmoke, "btn_customerLookup");
		txt_customerAccountFrontPageProjectQuatation = findElementInXLSheet(getParameterProjectSmoke,
				"txt_customerAccountFrontPageProjectQuatation");
		txt_searchCustomer = findElementInXLSheet(getParameterProjectSmoke, "txt_searchCustomer");
		lnk_inforOnLookupInformationReplace = findElementInXLSheet(getParameterProjectSmoke,
				"lnk_inforOnLookupInformationReplace");
		drop_salesUnitProjectQuatation = findElementInXLSheet(getParameterProjectSmoke,
				"drop_salesUnitProjectQuatation");
		btn_productLookupOnGridRowReplace = findElementInXLSheet(getParameterProjectSmoke,
				"btn_productLookupOnGridRowReplace");
		txt_searchProduct = findElementInXLSheet(getParameterProjectSmoke, "txt_searchProduct");
		drop_typeOnGridProjectQuatationRowReplace = findElementInXLSheet(getParameterProjectSmoke,
				"drop_typeOnGridProjectQuatationRowReplace");
		lbl_productOnGridProductQuatationRowReplace = findElementInXLSheet(getParameterProjectSmoke,
				"lbl_productOnGridProductQuatationRowReplace");
		lbl_OHOnGridProductQuatation = findElementInXLSheet(getParameterProjectSmoke, "lbl_OHOnGridProductQuatation");
		txt_quantityOnGridProductQuatationRowReplace = findElementInXLSheet(getParameterProjectSmoke,
				"txt_quantityOnGridProductQuatationRowReplace");
		btn_addNewRecordGrid = findElementInXLSheet(getParameterProjectSmoke, "btn_addNewRecordGrid");
		drop_referenceGrouopOnGridRowReplce = findElementInXLSheet(getParameterProjectSmoke,
				"drop_referenceGrouopOnGridRowReplce");
		btn_labourLookupProductQuatation = findElementInXLSheet(getParameterProjectSmoke,
				"btn_labourLookupProductQuatation");
		txt_unitPriceProductQuatationRowReplace = findElementInXLSheet(getParameterProjectSmoke,
				"txt_unitPriceProductQuatationRowReplace");
		txt_labourSearch = findElementInXLSheet(getParameterProjectSmoke, "txt_labourSearch");
		btn_resourceLookupProductQuatation = findElementInXLSheet(getParameterProjectSmoke,
				"btn_resourceLookupProductQuatation");
		txt_searchResource = findElementInXLSheet(getParameterProjectSmoke, "txt_searchResource");
		drop_referenceCodeExpence = findElementInXLSheet(getParameterProjectSmoke, "drop_referenceCodeExpence");
		btn_checkout = findElementInXLSheet(getParameterProjectSmoke, "btn_checkout");
		lbl_gridLineTotalParts = findElementInXLSheet(getParameterProjectSmoke, "lbl_gridLineTotalParts");
		lbl_gridLineTotalLabour = findElementInXLSheet(getParameterProjectSmoke, "lbl_gridLineTotalLabour");
		lbl_labourTotalCheckoutBar = findElementInXLSheet(getParameterProjectSmoke, "lbl_labourTotalCheckoutBar");
		lbl_partsTotalCheckoutBar = findElementInXLSheet(getParameterProjectSmoke, "lbl_partsTotalCheckoutBar");
		btn_draft = findElementInXLSheet(getParameterProjectSmoke, "btn_draft");
		btn_release = findElementInXLSheet(getParameterProjectSmoke, "btn_release");
		header_draftedProjectQuatation = findElementInXLSheet(getParameterProjectSmoke,
				"header_draftedProjectQuatation");
		header_releasedProjectQuatation = findElementInXLSheet(getParameterProjectSmoke,
				"header_releasedProjectQuatation");
		header_confirmedProjectQuatation = findElementInXLSheet(getParameterProjectSmoke,
				"header_confirmedProjectQuatation");
		btn_versionProductQuatation = findElementInXLSheet(getParameterProjectSmoke, "btn_versionProductQuatation");
		div_versionPopup = findElementInXLSheet(getParameterProjectSmoke, "div_versionPopup");
		btn_confirmVersion = findElementInXLSheet(getParameterProjectSmoke, "btn_confirmVersion");
		btn_confirmQuatation = findElementInXLSheet(getParameterProjectSmoke, "btn_confirmQuatation");
		btn_action = findElementInXLSheet(getParameterProjectSmoke, "btn_action");
		btn_convertToProject = findElementInXLSheet(getParameterProjectSmoke, "btn_convertToProject");
		header_newProject = findElementInXLSheet(getParameterProjectSmoke, "header_newProject");
		txt_projectCode = findElementInXLSheet(getParameterProjectSmoke, "txt_projectCode");
		txt_projectTitle = findElementInXLSheet(getParameterProjectSmoke, "txt_projectTitle");
		drop_projectGroup = findElementInXLSheet(getParameterProjectSmoke, "drop_projectGroup");
		txt_customerAccountFrontPageProject = findElementInXLSheet(getParameterProjectSmoke,
				"txt_customerAccountFrontPageProject");
		btn_lookupResponsiblePerson = findElementInXLSheet(getParameterProjectSmoke, "btn_lookupResponsiblePerson");
		txt_responsiblePersonFrontPageProject = findElementInXLSheet(getParameterProjectSmoke,
				"txt_responsiblePersonFrontPageProject");
		txt_searchPricingProfile = findElementInXLSheet(getParameterProjectSmoke, "txt_searchPricingProfile");
		txt_frontPagePricingProfileProject = findElementInXLSheet(getParameterProjectSmoke,
				"txt_frontPagePricingProfileProject");
		txt_requestDateProject = findElementInXLSheet(getParameterProjectSmoke, "txt_requestDateProject");
		txt_projectLocationFrontPageProject = findElementInXLSheet(getParameterProjectSmoke,
				"txt_projectLocationFrontPageProject");
		btn_locationInformationLookup = findElementInXLSheet(getParameterProjectSmoke, "btn_locationInformationLookup");
		txt_searchLocation = findElementInXLSheet(getParameterProjectSmoke, "txt_searchLocation");
		txt_projectValue = findElementInXLSheet(getParameterProjectSmoke, "txt_projectValue");
		lbl_totalCheckoutBarProjectQuatation = findElementInXLSheet(getParameterProjectSmoke,
				"lbl_totalCheckoutBarProjectQuatation");
		header_draftedProject = findElementInXLSheet(getParameterProjectSmoke, "header_draftedProject");
		header_releasedProject = findElementInXLSheet(getParameterProjectSmoke, "header_releasedProject");
		btn_edit = findElementInXLSheet(getParameterProjectSmoke, "btn_edit");
		btn_update = findElementInXLSheet(getParameterProjectSmoke, "btn_update");
		tab_workBreackdown = findElementInXLSheet(getParameterProjectSmoke, "tab_workBreackdown");
		lbl_draftedTaskWorkBreackdownProject = findElementInXLSheet(getParameterProjectSmoke,
				"lbl_draftedTaskWorkBreackdownProject");
		btn_releaseTaskWorkBreackdown = findElementInXLSheet(getParameterProjectSmoke, "btn_releaseTaskWorkBreackdown");
		para_taskReleaseValidationWorkBreackdown = findElementInXLSheet(getParameterProjectSmoke,
				"para_taskReleaseValidationWorkBreackdown");
		header_projectQuatationByPage = findElementInXLSheet(getParameterProjectSmoke, "header_projectQuatationByPage");
		txt_employeeOnGridProductQuatation = findElementInXLSheet(getParameterProjectSmoke,
				"txt_employeeOnGridProductQuatation");
		txt_resourseOnGridProductQuatation = findElementInXLSheet(getParameterProjectSmoke,
				"txt_resourseOnGridProductQuatation");
		lbl_docNumber = findElementInXLSheet(getParameterProjectSmoke, "lbl_docNumber");
		btn_pricingProfileLookup = findElementInXLSheet(getParameterProjectSmoke, "btn_pricingProfileLookup");
		btn_expandProjectTask = findElementInXLSheet(getParameterProjectSmoke, "btn_expandProjectTask");
		btn_project = findElementInXLSheet(getParameterProjectSmoke, "btn_project");
		btn_newProject = findElementInXLSheet(getParameterProjectSmoke, "btn_newProject");
		btn_customerLookupProject = findElementInXLSheet(getParameterProjectSmoke, "btn_customerLookupProject");
		header_projectByPage = findElementInXLSheet(getParameterProjectSmoke, "header_projectByPage");
		drop_businessUnitProject = findElementInXLSheet(getParameterProjectSmoke, "drop_businessUnitProject");
		btn_addTask = findElementInXLSheet(getParameterProjectSmoke, "btn_addTask");
		txt_taskNameTaskWindow = findElementInXLSheet(getParameterProjectSmoke, "txt_taskNameTaskWindow");
		btn_inputProductsTabTaskWindow = findElementInXLSheet(getParameterProjectSmoke,
				"btn_inputProductsTabTaskWindow");
		btn_inputProductsLookupTaskWindow = findElementInXLSheet(getParameterProjectSmoke,
				"btn_inputProductsLookupTaskWindow");
		div_selectedInputProductTaskWindow = findElementInXLSheet(getParameterProjectSmoke,
				"div_selectedInputProductTaskWindow");
		txt_quantityInputProductsTaskWindow = findElementInXLSheet(getParameterProjectSmoke,
				"txt_quantityInputProductsTaskWindow");
		chk_billableTaskWindowInputProducts = findElementInXLSheet(getParameterProjectSmoke,
				"chk_billableTaskWindowInputProducts");
		tab_labourTaskWindow = findElementInXLSheet(getParameterProjectSmoke, "tab_labourTaskWindow");
		btn_lookupProductTaskWindowLabourTab = findElementInXLSheet(getParameterProjectSmoke,
				"btn_lookupProductTaskWindowLabourTab");
		div_selectedServiceLabourTabTaskWindow = findElementInXLSheet(getParameterProjectSmoke,
				"div_selectedServiceLabourTabTaskWindow");
		txt_hoursLabourTabTaskWindow = findElementInXLSheet(getParameterProjectSmoke, "txt_hoursLabourTabTaskWindow");
		btn_fiveLabourHoursTaskWindow = findElementInXLSheet(getParameterProjectSmoke, "btn_fiveLabourHoursTaskWindow");
		btn_okLabourHoursTaskWindow = findElementInXLSheet(getParameterProjectSmoke, "btn_okLabourHoursTaskWindow");
		chk_billableLabourHoursTaskWindow = findElementInXLSheet(getParameterProjectSmoke,
				"chk_billableLabourHoursTaskWindow");
		tab_resourseTaskWindow = findElementInXLSheet(getParameterProjectSmoke, "tab_resourseTaskWindow");
		drop_resourseGroupTaskWindow = findElementInXLSheet(getParameterProjectSmoke, "drop_resourseGroupTaskWindow");
		btn_lookupResourseTaskWindow = findElementInXLSheet(getParameterProjectSmoke, "btn_lookupResourseTaskWindow");
		div_selectedResourseTaskWindow = findElementInXLSheet(getParameterProjectSmoke,
				"div_selectedResourseTaskWindow");
		btn_lookupServiceTaskWindowResorseTab = findElementInXLSheet(getParameterProjectSmoke,
				"btn_lookupServiceTaskWindowResorseTab");
		div_serviceProductResourseTabTaskWindow = findElementInXLSheet(getParameterProjectSmoke,
				"div_serviceProductResourseTabTaskWindow");
		txt_hoursResourseTabTaskWindow = findElementInXLSheet(getParameterProjectSmoke,
				"txt_hoursResourseTabTaskWindow");
		btn_fiveResourseHoursTaskWindow = findElementInXLSheet(getParameterProjectSmoke,
				"btn_fiveResourseHoursTaskWindow");
		btn_okResourseHoursTaskWindow = findElementInXLSheet(getParameterProjectSmoke, "btn_okResourseHoursTaskWindow");
		chk_billableResourseTabTaskWindow = findElementInXLSheet(getParameterProjectSmoke,
				"chk_billableResourseTabTaskWindow");
		tab_expenceTaskWindow = findElementInXLSheet(getParameterProjectSmoke, "tab_expenceTaskWindow");
		drop_analysisCodeExpenceTabTaskWindow = findElementInXLSheet(getParameterProjectSmoke,
				"drop_analysisCodeExpenceTabTaskWindow");
		btn_nraationExpenceTabTaskWindow = findElementInXLSheet(getParameterProjectSmoke,
				"btn_nraationExpenceTabTaskWindow");
		txt_narrationTaskWindowExpenceTab = findElementInXLSheet(getParameterProjectSmoke,
				"txt_narrationTaskWindowExpenceTab");
		btn_applyNarrationExpence = findElementInXLSheet(getParameterProjectSmoke, "btn_applyNarrationExpence");
		txt_selectedNarrationTaskWindowExpenceTab = findElementInXLSheet(getParameterProjectSmoke,
				"txt_selectedNarrationTaskWindowExpenceTab");
		btn_serviceLoockupExpenceTabTaskWindow = findElementInXLSheet(getParameterProjectSmoke,
				"btn_serviceLoockupExpenceTabTaskWindow");
		div_selectedServiceExpenceTabTaskWindow = findElementInXLSheet(getParameterProjectSmoke,
				"div_selectedServiceExpenceTabTaskWindow");
		txt_estimatedExpenceTaskWindowExpenceTab = findElementInXLSheet(getParameterProjectSmoke,
				"txt_estimatedExpenceTaskWindowExpenceTab");
		chk_billableExpenceTabTaskWindow = findElementInXLSheet(getParameterProjectSmoke,
				"chk_billableExpenceTabTaskWindow");
		tab_overheadTaskWindow = findElementInXLSheet(getParameterProjectSmoke, "tab_overheadTaskWindow");
		div_autoLoadedOverhead = findElementInXLSheet(getParameterProjectSmoke, "div_autoLoadedOverhead");
		chk_billableOverheadTabTaskWindow = findElementInXLSheet(getParameterProjectSmoke,
				"chk_billableOverheadTabTaskWindow");
		btn_updateTaskWindow = findElementInXLSheet(getParameterProjectSmoke, "btn_updateTaskWindow");
		header_taskWindow = findElementInXLSheet(getParameterProjectSmoke, "header_taskWindow");
		drop_contractTypeTaskWindow = findElementInXLSheet(getParameterProjectSmoke, "drop_contractTypeTaskWindow");
		tab_subContractServices = findElementInXLSheet(getParameterProjectSmoke, "tab_subContractServices");
		btn_vendorLookup = findElementInXLSheet(getParameterProjectSmoke, "btn_vendorLookup");
		txt_vendorSearch = findElementInXLSheet(getParameterProjectSmoke, "txt_vendorSearch");
		txt_vendorSubContractTabTaskWindow = findElementInXLSheet(getParameterProjectSmoke,
				"txt_vendorSubContractTabTaskWindow");
		btn_serviceProductLookupRowReplaceSubContractTab = findElementInXLSheet(getParameterProjectSmoke,
				"btn_serviceProductLookupRowReplaceSubContractTab");
		div_selectedServiceSubContractTabTaskWindowRowReplace = findElementInXLSheet(getParameterProjectSmoke,
				"div_selectedServiceSubContractTabTaskWindowRowReplace");
		txt_estimatedAmountServiceOneSubContractTypeTaskRowReplace = findElementInXLSheet(getParameterProjectSmoke,
				"txt_estimatedAmountServiceOneSubContractTypeTaskRowReplace");
		chk_billableServicesSubContractTypeTaskRowReplace = findElementInXLSheet(getParameterProjectSmoke,
				"chk_billableServicesSubContractTypeTaskRowReplace");
		btn_addNewRowSubContractTaskTaskWindow = findElementInXLSheet(getParameterProjectSmoke,
				"btn_addNewRowSubContractTaskTaskWindow");
		tab_interdepartmentTaskWindow = findElementInXLSheet(getParameterProjectSmoke, "tab_interdepartmentTaskWindow");
		txt_loadedEmployeeNameSummarTabTaskWindowInterdepartmentalTask = findElementInXLSheet(getParameterProjectSmoke,
				"txt_loadedEmployeeNameSummarTabTaskWindowInterdepartmentalTask");
		btn_ownerLookupTaskWindow = findElementInXLSheet(getParameterProjectSmoke, "btn_ownerLookupTaskWindow");
		txt_selectedOwnerInterdepartmentTabTaskWindow = findElementInXLSheet(getParameterProjectSmoke,
				"txt_selectedOwnerInterdepartmentTabTaskWindow");
		txt_estimatedCostTaskWindowInterdepartmentalTask = findElementInXLSheet(getParameterProjectSmoke,
				"txt_estimatedCostTaskWindowInterdepartmentalTask");
		txt_estimatedDateInterdepartmentTaskWindow = findElementInXLSheet(getParameterProjectSmoke,
				"txt_estimatedDateInterdepartmentTaskWindow");
		btn_dayClickTaskWindow = findElementInXLSheet(getParameterProjectSmoke, "btn_dayClickTaskWindow");
		btn_releaseTaskWorkBreackdownPendingFirst = findElementInXLSheet(getParameterProjectSmoke,
				"btn_releaseTaskWorkBreackdownPendingFirst");
		para_validatorTaskReleaseSuccessfully = findElementInXLSheet(getParameterProjectSmoke,
				"para_validatorTaskReleaseSuccessfully");
		lnk_employeeTaskWindowInterdepartmentalTabEmpReplace = findElementInXLSheet(getParameterProjectSmoke,
				"lnk_employeeTaskWindowInterdepartmentalTabEmpReplace");
		lbl_allocationExpenceCostingTab = findElementInXLSheet(getParameterProjectSmoke,
				"lbl_allocationExpenceCostingTab");
		tab_costingSummary = findElementInXLSheet(getParameterProjectSmoke, "tab_costingSummary");
		lbl_allocationResourseCostCostingTab = findElementInXLSheet(getParameterProjectSmoke,
				"lbl_allocationResourseCostCostingTab");
		lbl_allocationHourseCostCostingTab = findElementInXLSheet(getParameterProjectSmoke,
				"lbl_allocationHourseCostCostingTab");
		lbl_allocationMaterialsCostCostingTab = findElementInXLSheet(getParameterProjectSmoke,
				"lbl_allocationMaterialsCostCostingTab");
		lbl_allocationSubContractCostCostingTab = findElementInXLSheet(getParameterProjectSmoke,
				"lbl_allocationSubContractCostCostingTab");
		lbl_allocationOverheadCostCostingTab = findElementInXLSheet(getParameterProjectSmoke,
				"lbl_allocationOverheadCostCostingTab");
		lbl_allocationInterdepartmentalCostCostingTab = findElementInXLSheet(getParameterProjectSmoke,
				"lbl_allocationInterdepartmentalCostCostingTab");
		lbl_releasedTaskWorkBreackdowntaskNoReplace = findElementInXLSheet(getParameterProjectSmoke,
				"lbl_releasedTaskWorkBreackdowntaskNoReplace");
		btn_lookupPerformer = findElementInXLSheet(getParameterProjectSmoke,
				"btn_lookupPerformer");
		txt_performerInterDepartmentTabWorkBreackdown = findElementInXLSheet(getParameterProjectSmoke,
				"txt_performerInterDepartmentTabWorkBreackdown");

		/* Smoke_Project_004 */
		btn_createInternalOrder = findElementInXLSheet(getParameterProjectSmoke, "btn_createInternalOrder");
		header_genarateInternalOrder = findElementInXLSheet(getParameterProjectSmoke, "header_genarateInternalOrder");
		drop_taskInternalOrder = findElementInXLSheet(getParameterProjectSmoke, "drop_taskInternalOrder");
		div_productsGenarateInternalOrderWindowProductReplace = findElementInXLSheet(getParameterProjectSmoke,
				"div_productsGenarateInternalOrderWindowProductReplace");
		chk_productGenarateInternalOrder = findElementInXLSheet(getParameterProjectSmoke,
				"chk_productGenarateInternalOrder");
		btn_applyGenarateInternalOrder = findElementInXLSheet(getParameterProjectSmoke,
				"btn_applyGenarateInternalOrder");
		header_draftedInternalOrder = findElementInXLSheet(getParameterProjectSmoke, "header_draftedInternalOrder");
		header_releasedInternalOrder = findElementInXLSheet(getParameterProjectSmoke, "header_releasedInternalOrder");
		header_draftedInternalDispatchOrder = findElementInXLSheet(getParameterProjectSmoke,
				"header_draftedInternalDispatchOrder");
		header_releasedInternalDispatchOrder = findElementInXLSheet(getParameterProjectSmoke,
				"header_releasedInternalDispatchOrder");
		tab_overviewProject = findElementInXLSheet(getParameterProjectSmoke, "tab_overviewProject");
		tab_inputProductsOverviewTab = findElementInXLSheet(getParameterProjectSmoke, "tab_inputProductsOverviewTab");
		lbl_internalOrderOverviewTabDocNoReplace = findElementInXLSheet(getParameterProjectSmoke,
				"lbl_internalOrderOverviewTabDocNoReplace");
		lnk_goToPage = findElementInXLSheet(getParameterProjectSmoke, "lnk_goToPage");
		header_newInternalOrder = findElementInXLSheet(getParameterProjectSmoke, "header_newInternalOrder");
		header_newInternalDispatchOrder = findElementInXLSheet(getParameterProjectSmoke,
				"header_newInternalDispatchOrder");

		/* Project_Smoke_005 */
		btn_service = findElementInXLSheet(getParameterProjectSmoke, "btn_service");
		btn_dailyWorkSheet = findElementInXLSheet(getParameterProjectSmoke, "btn_dailyWorkSheet");
		btn_newDailyWorkSheet = findElementInXLSheet(getParameterProjectSmoke, "btn_newDailyWorkSheet");
		btn_dailyWorksheetProjectJourney = findElementInXLSheet(getParameterProjectSmoke,
				"btn_dailyWorksheetProjectJourney");
		header_newDailyWorkSheet = findElementInXLSheet(getParameterProjectSmoke, "header_newDailyWorkSheet");
		btn_addNewDailyWorsheet = findElementInXLSheet(getParameterProjectSmoke, "btn_addNewDailyWorsheet");
		header_AddNewJobWindow = findElementInXLSheet(getParameterProjectSmoke, "header_AddNewJobWindow");
		lnl_releventTaskProjectNoReplace = findElementInXLSheet(getParameterProjectSmoke,
				"lnl_releventTaskProjectNoReplace");
		btn_lookupEmployyeAddNewJobWindow = findElementInXLSheet(getParameterProjectSmoke,
				"btn_lookupEmployyeAddNewJobWindow");
		txt_inTimeAddNewJob = findElementInXLSheet(getParameterProjectSmoke, "txt_inTimeAddNewJob");
		btn_eightIntimeHoursAddNEwJob = findElementInXLSheet(getParameterProjectSmoke, "btn_eightIntimeHoursAddNEwJob");
		btn_okAddNewwJobHoursInTime = findElementInXLSheet(getParameterProjectSmoke, "btn_okAddNewwJobHoursInTime");
		txt_outTimeAddNewJob = findElementInXLSheet(getParameterProjectSmoke, "txt_outTimeAddNewJob");
		btn_fiveOuttimeHoursAddNEwJob = findElementInXLSheet(getParameterProjectSmoke, "btn_fiveOuttimeHoursAddNEwJob");
		btn_okAddNewwJobHoursOutTime = findElementInXLSheet(getParameterProjectSmoke, "btn_okAddNewwJobHoursOutTime");
		btn_updateAddnewJob = findElementInXLSheet(getParameterProjectSmoke, "btn_updateAddnewJob");
		header_draftedDailyWorkSheet = findElementInXLSheet(getParameterProjectSmoke, "header_draftedDailyWorkSheet");
		header_releasedDailyWorkSheet = findElementInXLSheet(getParameterProjectSmoke, "header_releasedDailyWorkSheet");
		header_releasedDailyWorkSheet = findElementInXLSheet(getParameterProjectSmoke, "header_releasedDailyWorkSheet");
		btn_addProjectTaskLookup = findElementInXLSheet(getParameterProjectSmoke, "btn_addProjectTaskLookup");
		lbl_actualHourseCostCostingTab = findElementInXLSheet(getParameterProjectSmoke,
				"lbl_actualHourseCostCostingTab");
		txt_serachProjectTask = findElementInXLSheet(getParameterProjectSmoke, "txt_serachProjectTask");
		lnk_employeeProjectEmpReplace = findElementInXLSheet(getParameterProjectSmoke, "lnk_employeeProjectEmpReplace");

		/* Smoke_Project_006 */
		btn_subContractOrderActions = findElementInXLSheet(getParameterProjectSmoke, "btn_subContractOrderActions");
		header_subContractWindow = findElementInXLSheet(getParameterProjectSmoke, "header_subContractWindow");
		drop_task = findElementInXLSheet(getParameterProjectSmoke, "drop_task");
		chk_selectAllWindowComeFromProjectAction = findElementInXLSheet(getParameterProjectSmoke,
				"chk_selectAllWindowComeFromProjectAction");
		btn_applySucContractOrders = findElementInXLSheet(getParameterProjectSmoke, "btn_applySucContractOrders");
		header_draftedPurchaseOrder = findElementInXLSheet(getParameterProjectSmoke, "header_draftedPurchaseOrder");
		header_releasedPurchaseOrder = findElementInXLSheet(getParameterProjectSmoke, "header_releasedPurchaseOrder");
		header_newPurchaseOrder = findElementInXLSheet(getParameterProjectSmoke, "header_newPurchaseOrder");
		header_draftedPurchaseInvoice = findElementInXLSheet(getParameterProjectSmoke, "header_draftedPurchaseInvoice");
		header_releasedPurchaseInvoice = findElementInXLSheet(getParameterProjectSmoke,
				"header_releasedPurchaseInvoice");
		header_newPurchaseInvoice = findElementInXLSheet(getParameterProjectSmoke, "header_newPurchaseInvoice");
		tab_subContractServicesOverviewTab = findElementInXLSheet(getParameterProjectSmoke,
				"tab_subContractServicesOverviewTab");
		lbl_purchaseOrderOverviewTabDocNoReplace = findElementInXLSheet(getParameterProjectSmoke,
				"lbl_purchaseOrderOverviewTabDocNoReplace");
		lbl_actualSubContractCostCostingTab = findElementInXLSheet(getParameterProjectSmoke,
				"lbl_actualSubContractCostCostingTab");

		/* Smoke_Project_007 */
		btn_actualUpdate = findElementInXLSheet(getParameterProjectSmoke, "btn_actualUpdate");
		txt_actualUpdateQuantity = findElementInXLSheet(getParameterProjectSmoke, "txt_actualUpdateQuantity");
		btn_updateActualUpdate = findElementInXLSheet(getParameterProjectSmoke, "btn_updateActualUpdate");
		tab_resourseActualUpdateWindow = findElementInXLSheet(getParameterProjectSmoke,
				"tab_resourseActualUpdateWindow");
		txt_actualResourseHours = findElementInXLSheet(getParameterProjectSmoke, "txt_actualResourseHours");
		btn_fiveActualResourseHours = findElementInXLSheet(getParameterProjectSmoke, "btn_fiveActualResourseHours");
		btn_okActualResourseHours = findElementInXLSheet(getParameterProjectSmoke, "btn_okActualResourseHours");
		lbl_actualMaterialCostCostingTab = findElementInXLSheet(getParameterProjectSmoke,
				"lbl_actualMaterialCostCostingTab");
		lbl_actualResourseCostCostingTab = findElementInXLSheet(getParameterProjectSmoke,
				"lbl_actualResourseCostCostingTab");
		lbl_actualOverheadCostCostingTab = findElementInXLSheet(getParameterProjectSmoke,
				"lbl_actualOverheadCostCostingTab");

		/* Smoke_Project_008 */
		btn_inventoryAndWarehouseModule = findElementInXLSheet(getParameterProjectSmoke,
				"btn_inventoryAndWarehouseModule");
		btn_internalReturnOrder = findElementInXLSheet(getParameterProjectSmoke, "btn_internalReturnOrder");
		btn_newInternalReturnOrder = findElementInXLSheet(getParameterProjectSmoke, "btn_newInternalReturnOrder");
		btn_journeyWIPReturns = findElementInXLSheet(getParameterProjectSmoke, "btn_journeyWIPReturns");
		drop_warehouseInternalReturnOrder = findElementInXLSheet(getParameterProjectSmoke,
				"drop_warehouseInternalReturnOrder");
		btn_docIconInternalReturnOrder = findElementInXLSheet(getParameterProjectSmoke,
				"btn_docIconInternalReturnOrder");
		txt_docSearchInternalReturnOrder = findElementInXLSheet(getParameterProjectSmoke,
				"txt_docSearchInternalReturnOrder");
		chk_documentFirstInternalReturnOrder = findElementInXLSheet(getParameterProjectSmoke,
				"chk_documentFirstInternalReturnOrder");
		btn_applyInternalReturnOrder = findElementInXLSheet(getParameterProjectSmoke, "btn_applyInternalReturnOrder");
		header_newInternalReturnOrder = findElementInXLSheet(getParameterProjectSmoke, "header_newInternalReturnOrder");
		header_draftedIRO = findElementInXLSheet(getParameterProjectSmoke, "header_draftedIRO");
		header_releasedIRO = findElementInXLSheet(getParameterProjectSmoke, "header_releasedIRO");
		header_draftedInternalRecipt = findElementInXLSheet(getParameterProjectSmoke, "header_draftedInternalRecipt");
		header_releasedInternalReceipt = findElementInXLSheet(getParameterProjectSmoke,
				"header_releasedInternalReceipt");
		btn_serialBatchCapture = findElementInXLSheet(getParameterProjectSmoke, "btn_serialBatchCapture");
		btn_itemNumberOneSerielBatchCapture = findElementInXLSheet(getParameterProjectSmoke,
				"btn_itemNumberOneSerielBatchCapture");
		btn_updateSerielBatchCapture = findElementInXLSheet(getParameterProjectSmoke, "btn_updateSerielBatchCapture");
		btn_serialBatchCaptureBackButton = findElementInXLSheet(getParameterProjectSmoke,
				"btn_serialBatchCaptureBackButton");
		btn_searchInternalReturnOrder = findElementInXLSheet(getParameterProjectSmoke, "btn_searchInternalReturnOrder");
		div_capturedCountOnGrdSerialBatchCaptureInternalRecipt = findElementInXLSheet(getParameterProjectSmoke,
				"div_capturedCountOnGrdSerialBatchCaptureInternalRecipt");
		txt_captureQuantityOnGridSerilBatchCaptureInternalRecipt = findElementInXLSheet(getParameterProjectSmoke,
				"txt_captureQuantityOnGridSerilBatchCaptureInternalRecipt");

		/* Smoke_Project_009 */
		btn_finance = findElementInXLSheet(getParameterProjectSmoke, "btn_finance");
		btn_outboundPaymentAdvice = findElementInXLSheet(getParameterProjectSmoke, "btn_outboundPaymentAdvice");
		btn_newOutboundPaymentAdvice = findElementInXLSheet(getParameterProjectSmoke, "btn_newOutboundPaymentAdvice");
		btn_accruelVoucherJourney = findElementInXLSheet(getParameterProjectSmoke, "btn_accruelVoucherJourney");
		btn_vendorLookupOutboundPaymentAdvice = findElementInXLSheet(getParameterProjectSmoke,
				"btn_vendorLookupOutboundPaymentAdvice");
		drop_anlyzeCodeAccruelVoucher = findElementInXLSheet(getParameterProjectSmoke, "drop_anlyzeCodeAccruelVoucher");
		txt_rowDescriptionAccruelVoucher = findElementInXLSheet(getParameterProjectSmoke,
				"txt_rowDescriptionAccruelVoucher");
		txt_amountAccruelVoucher = findElementInXLSheet(getParameterProjectSmoke, "txt_amountAccruelVoucher");
		btn_costAllocationWidgetAccruelVoucher = findElementInXLSheet(getParameterProjectSmoke,
				"btn_costAllocationWidgetAccruelVoucher");
		txt_costinfAllocationPresentageAccruelVoucher = findElementInXLSheet(getParameterProjectSmoke,
				"txt_costinfAllocationPresentageAccruelVoucher");
		drop_costAssignmentType = findElementInXLSheet(getParameterProjectSmoke, "drop_costAssignmentType");
		btn_updateCostAllocationAccruelVoucher = findElementInXLSheet(getParameterProjectSmoke,
				"btn_updateCostAllocationAccruelVoucher");
		header_newOutboundPaymentAdvice = findElementInXLSheet(getParameterProjectSmoke,
				"header_newOutboundPaymentAdvice");
		header_draftedOutboundPaymentAdvice = findElementInXLSheet(getParameterProjectSmoke,
				"header_draftedOutboundPaymentAdvice");
		header_releasedOutboundPaymentAdvice = findElementInXLSheet(getParameterProjectSmoke,
				"header_releasedOutboundPaymentAdvice");
		lbl_actualCostExpence = findElementInXLSheet(getParameterProjectSmoke, "lbl_actualCostExpence");
		btn_addRecordCostAllocationOPA = findElementInXLSheet(getParameterProjectSmoke, "btn_addRecordCostAllocationOPA");

		/* Smoke_Project_010 */
		btn_createInvoiceProposals = findElementInXLSheet(getParameterProjectSmoke, "btn_createInvoiceProposals");
		header_invoiceProposalWindow = findElementInXLSheet(getParameterProjectSmoke, "header_invoiceProposalWindow");
		drop_invoiceProposalFilter = findElementInXLSheet(getParameterProjectSmoke, "drop_invoiceProposalFilter");
		chk_selectAllInvoiceProposalWindow = findElementInXLSheet(getParameterProjectSmoke,
				"chk_selectAllInvoiceProposalWindow");
		btn_productLoockupInvoiceProposalWindowRowReplace = findElementInXLSheet(getParameterProjectSmoke,
				"btn_productLoockupInvoiceProposalWindowRowReplace");
		txt_quantitySecondRowInvoiceProposalTask = findElementInXLSheet(getParameterProjectSmoke,
				"txt_quantitySecondRowInvoiceProposalTask");
		btn_applyInvoiceProposalWindow = findElementInXLSheet(getParameterProjectSmoke,
				"btn_applyInvoiceProposalWindow");
		header_newInvoiceProposal = findElementInXLSheet(getParameterProjectSmoke, "header_newInvoiceProposal");
		header_draftedInvoiceProposal = findElementInXLSheet(getParameterProjectSmoke, "header_draftedInvoiceProposal");
		header_releasedInvoiceProposal = findElementInXLSheet(getParameterProjectSmoke,
				"header_releasedInvoiceProposal");
		header_newServiceInvoice = findElementInXLSheet(getParameterProjectSmoke, "header_newServiceInvoice");
		header_draftedServiceInvoice = findElementInXLSheet(getParameterProjectSmoke, "header_draftedServiceInvoice");
		header_releasedServiceInvoice = findElementInXLSheet(getParameterProjectSmoke, "header_releasedServiceInvoice");
		tab_invoiceDetailsOverviewTab = findElementInXLSheet(getParameterProjectSmoke, "tab_invoiceDetailsOverviewTab");
		div_salesInvoiceInviceDetailsTabOverviewDocReplace = findElementInXLSheet(getParameterProjectSmoke,
				"div_salesInvoiceInviceDetailsTabOverviewDocReplace");
		tab_serviceDeatailsServiceInvoice = findElementInXLSheet(getParameterProjectSmoke,
				"tab_serviceDeatailsServiceInvoice");

		/* Smoke_Project_011 */
		btn_serviceCalender = findElementInXLSheet(getParameterProjectSmoke, "btn_serviceCalender");
		btn_interdepartmntalTaskOnWorkingCalender = findElementInXLSheet(getParameterProjectSmoke,
				"btn_interdepartmntalTaskOnWorkingCalender");
		header_newServiceJobOrder = findElementInXLSheet(getParameterProjectSmoke, "header_newServiceJobOrder");
		header_draftedServiceJobOrder = findElementInXLSheet(getParameterProjectSmoke, "header_draftedServiceJobOrder");
		header_releasedServiceJobOrder = findElementInXLSheet(getParameterProjectSmoke,
				"header_releasedServiceJobOrder");
		drop_serviceGroupServiceJobOrder = findElementInXLSheet(getParameterProjectSmoke,
				"drop_serviceGroupServiceJobOrder");
		drop_priorityServiceJobOrder = findElementInXLSheet(getParameterProjectSmoke, "drop_priorityServiceJobOrder");
		btn_lookupServiceLocation = findElementInXLSheet(getParameterProjectSmoke, "btn_lookupServiceLocation");
		lnk_firstRecordPricingProfile = findElementInXLSheet(getParameterProjectSmoke, "lnk_firstRecordPricingProfile");

		/* Smoke_Project_012 */
		btn_addProductTagNoActions = findElementInXLSheet(getParameterProjectSmoke, "btn_addProductTagNoActions");
		heaser_addProductTagNoPopup = findElementInXLSheet(getParameterProjectSmoke, "heaser_addProductTagNoPopup");
		btn_addProductWidgetAddProductPopup = findElementInXLSheet(getParameterProjectSmoke,
				"btn_addProductWidgetAddProductPopup");
		chk_firstRegSerielAddProductPopup = findElementInXLSheet(getParameterProjectSmoke,
				"chk_firstRegSerielAddProductPopup");
		btn_applyAssProductTagNoPopup = findElementInXLSheet(getParameterProjectSmoke, "btn_applyAssProductTagNoPopup");
		btn_stopActionsSJO = findElementInXLSheet(getParameterProjectSmoke, "btn_stopActionsSJO");
		header_popupCompleteProcessSJO = findElementInXLSheet(getParameterProjectSmoke,
				"header_popupCompleteProcessSJO");
		btn_completeCompleteProcessSJP = findElementInXLSheet(getParameterProjectSmoke,
				"btn_completeCompleteProcessSJP");
		labl_pocInterdepartmentTaskWorkBreackdown = findElementInXLSheet(getParameterProjectSmoke,
				"labl_pocInterdepartmentTaskWorkBreackdown");
		labl_sjoNumberInterdepartmentTaskWorkBreackdown = findElementInXLSheet(getParameterProjectSmoke,
				"labl_sjoNumberInterdepartmentTaskWorkBreackdown");
		btn_completeSJO = findElementInXLSheet(getParameterProjectSmoke, "btn_completeSJO");
		btn_yesCompleteStatusSJO = findElementInXLSheet(getParameterProjectSmoke, "btn_yesCompleteStatusSJO");
		txt_serviceJobOrderCompletionPostDate = findElementInXLSheet(getParameterProjectSmoke,
				"txt_serviceJobOrderCompletionPostDate");
		btn_dayReplaceServiceJobOrderCompleteDate = findElementInXLSheet(getParameterProjectSmoke,
				"btn_dayReplaceServiceJobOrderCompleteDate");
		btn_applySJOCompletionDate = findElementInXLSheet(getParameterProjectSmoke, "btn_applySJOCompletionDate");
		header_completedServiceJobOrder = findElementInXLSheet(getParameterProjectSmoke,
				"header_completedServiceJobOrder");

		/* Smoke_Project_013 */
		btn_updateTaskPocActtions = findElementInXLSheet(getParameterProjectSmoke, "btn_updateTaskPocActtions");
		drop_taskPOCUpdate = findElementInXLSheet(getParameterProjectSmoke, "drop_taskPOCUpdate");
		txt_presentagePOC = findElementInXLSheet(getParameterProjectSmoke, "txt_presentagePOC");
		btn_applyPOC = findElementInXLSheet(getParameterProjectSmoke, "btn_applyPOC");
		lbl_pocPresenatgeTaskOneWorkBreackdown = findElementInXLSheet(getParameterProjectSmoke,
				"lbl_pocPresenatgeTaskOneWorkBreackdown");
		lbl_pocPresenatgeTaskSubContractWorkBreackdown = findElementInXLSheet(getParameterProjectSmoke,
				"lbl_pocPresenatgeTaskSubContractWorkBreackdown");

		/* Smoke_Project_014 */
		btn_projectCostEstimation = findElementInXLSheet(getParameterProjectSmoke, "btn_projectCostEstimation");
		btn_newProjectCostEstimation = findElementInXLSheet(getParameterProjectSmoke, "btn_newProjectCostEstimation");
		header_newProjectCostEstimation = findElementInXLSheet(getParameterProjectSmoke,
				"header_newProjectCostEstimation");
		header_draftedProjectCostEstimation = findElementInXLSheet(getParameterProjectSmoke,
				"header_draftedProjectCostEstimation");
		header_releasedProjectCostEstimation = findElementInXLSheet(getParameterProjectSmoke,
				"header_releasedProjectCostEstimation");
		txt_descriptionCostEstimation = findElementInXLSheet(getParameterProjectSmoke, "txt_descriptionCostEstimation");
		tab_estimationDetails = findElementInXLSheet(getParameterProjectSmoke, "tab_estimationDetails");
		btn_productLookupRowReplaceCostEstimation = findElementInXLSheet(getParameterProjectSmoke,
				"btn_productLookupRowReplaceCostEstimation");
		drop_warehouseCostEstimation = findElementInXLSheet(getParameterProjectSmoke, "drop_warehouseCostEstimation");
		txt_quantityInputProdyctsPCE = findElementInXLSheet(getParameterProjectSmoke, "txt_quantityInputProdyctsPCE");
		txt_unitPricePCERowReplace = findElementInXLSheet(getParameterProjectSmoke, "txt_unitPricePCERowReplace");
		chk_billablePCERowReplace = findElementInXLSheet(getParameterProjectSmoke, "chk_billablePCERowReplace");
		drop_typePCE = findElementInXLSheet(getParameterProjectSmoke, "drop_typePCE");
		drop_refrenceCodePCE = findElementInXLSheet(getParameterProjectSmoke, "drop_refrenceCodePCE");
		btn_addNewRecordGridCommon = findElementInXLSheet(getParameterProjectSmoke, "btn_addNewRecordGridCommon");
		btn_pricingProfileLookupCostEstimation = findElementInXLSheet(getParameterProjectSmoke, "btn_pricingProfileLookupCostEstimation");
		btn_tabSummaryCostEstimation = findElementInXLSheet(getParameterProjectSmoke, "btn_tabSummaryCostEstimation");
		
		/* Smoke_Project_015 */
		btn_projectModel = findElementInXLSheet(getParameterProjectSmoke, "btn_projectModel");
		btn_newProjectModel = findElementInXLSheet(getParameterProjectSmoke, "btn_newProjectModel");
		header_newProjectModel = findElementInXLSheet(getParameterProjectSmoke, "header_newProjectModel");
		txt_projectCodeProjectModel = findElementInXLSheet(getParameterProjectSmoke, "txt_projectCodeProjectModel");
		txt_projectModelName = findElementInXLSheet(getParameterProjectSmoke, "txt_projectModelName");
		drop_inputWarehouseProjectModel = findElementInXLSheet(getParameterProjectSmoke,
				"drop_inputWarehouseProjectModel");
		drop_outputWarehouseProjectModel = findElementInXLSheet(getParameterProjectSmoke,
				"drop_outputWarehouseProjectModel");
		header_draftedProjectModel = findElementInXLSheet(getParameterProjectSmoke, "header_draftedProjectModel");
		header_releasedProjectModel = findElementInXLSheet(getParameterProjectSmoke, "header_releasedProjectModel");
		tab_workBreackDownProjectModel = findElementInXLSheet(getParameterProjectSmoke,
				"tab_workBreackDownProjectModel");
		txt_daysCountSummaryTabProductionModel = findElementInXLSheet(getParameterProjectSmoke,
				"txt_daysCountSummaryTabProductionModel");
	}

	public static void readData() throws Exception {
		String computerName = System.getenv().get("COMPUTERNAME");
		if (computerName.equals("DESKTOP-C6E8CJU") || computerName.equals("MADHUSHAN-LAP")) {
			entutionURL = findElementInXLSheet(getDataProjectSmoke, "entutionUrlExternal");
		} else {
			entutionURL = findElementInXLSheet(getDataProjectSmoke, "entutionUrl");
		}
		automationTenantProjectSmokeUsername = findElementInXLSheet(getDataProjectSmoke,
				"automationTenantProjectSmokeUsername");
		automationTenantProjectSmokePassword = findElementInXLSheet(getDataProjectSmoke,
				"automationTenantProjectSmokePassword");
		customer = findElementInXLSheet(getDataProjectSmoke, "customer");
		salesUnit = findElementInXLSheet(getDataProjectSmoke, "salesUnit");
		typeInputProducts = findElementInXLSheet(getDataProjectSmoke, "typeInputProducts");
		typeExpence = findElementInXLSheet(getDataProjectSmoke, "typeExpence");
		typeServices = findElementInXLSheet(getDataProjectSmoke, "typeServices");
		batchFifoProduct = findElementInXLSheet(getDataProjectSmoke, "batchFifoProduct");
		employee = findElementInXLSheet(getDataProjectSmoke, "employee");
		employeeName = findElementInXLSheet(getDataProjectSmoke, "employeeName");
		typeLabour = findElementInXLSheet(getDataProjectSmoke, "typeLabour");
		referenceGroupLabour = findElementInXLSheet(getDataProjectSmoke, "referenceGroupLabour");
		serviceProduct = findElementInXLSheet(getDataProjectSmoke, "serviceProduct");
		serviceProduct2 = findElementInXLSheet(getDataProjectSmoke, "serviceProduct2");
		serviceProduct3 = findElementInXLSheet(getDataProjectSmoke, "serviceProduct3");
		serviceProduct4 = findElementInXLSheet(getDataProjectSmoke, "serviceProduct4");
		quantityFive = findElementInXLSheet(getDataProjectSmoke, "quantityFive");
		inputProductUnitPrice = findElementInXLSheet(getDataProjectSmoke, "inputProductUnitPrice");
		labourUnitPrice = findElementInXLSheet(getDataProjectSmoke, "labourUnitPrice");
		resourceUnitPrice = findElementInXLSheet(getDataProjectSmoke, "resourceUnitPrice");
		expenceUnitPrice = findElementInXLSheet(getDataProjectSmoke, "expenceUnitPrice");
		servicesUnitPrice = findElementInXLSheet(getDataProjectSmoke, "servicesUnitPrice");
		typeResource = findElementInXLSheet(getDataProjectSmoke, "typeResource");
		referenceGroupMotorVehicle = findElementInXLSheet(getDataProjectSmoke, "referenceGroupMotorVehicle");
		resource = findElementInXLSheet(getDataProjectSmoke, "resource");
		projectTitle = findElementInXLSheet(getDataProjectSmoke, "projectTitle");
		pricingProfile = findElementInXLSheet(getDataProjectSmoke, "pricingProfile");
		location = findElementInXLSheet(getDataProjectSmoke, "location");
		businessUnit = findElementInXLSheet(getDataProjectSmoke, "businessUnit");
		task = findElementInXLSheet(getDataProjectSmoke, "task");
		naraation = findElementInXLSheet(getDataProjectSmoke, "naraation");
		expenceEstimatedAmount = findElementInXLSheet(getDataProjectSmoke, "expenceEstimatedAmount");
		overhead = findElementInXLSheet(getDataProjectSmoke, "overhead");
		contractTypeSubContract = findElementInXLSheet(getDataProjectSmoke, "contractTypeSubContract");
		taskSubContract = findElementInXLSheet(getDataProjectSmoke, "taskSubContract");
		serviceOneEstimatedAmountSubContractTypeTask = findElementInXLSheet(getDataProjectSmoke,
				"serviceOneEstimatedAmountSubContractTypeTask");
		serviceProduct5 = findElementInXLSheet(getDataProjectSmoke, "serviceProduct5");
		serviceTwoEstimatedAmountSubContractTypeTask = findElementInXLSheet(getDataProjectSmoke,
				"serviceTwoEstimatedAmountSubContractTypeTask");
		taskNameInterdepartment = findElementInXLSheet(getDataProjectSmoke, "taskNameInterdepartment");
		contractTypeInterDepartment = findElementInXLSheet(getDataProjectSmoke, "contractTypeInterDepartment");
		estimatedCostInterdepartmentalTask = findElementInXLSheet(getDataProjectSmoke,
				"estimatedCostInterdepartmentalTask");

		/* Smoke_Project_004 */
		taskAll = findElementInXLSheet(getDataProjectSmoke, "taskAll");

		/* Smoke_Project_007 */
		actualUpdateQuantity = findElementInXLSheet(getDataProjectSmoke, "actualUpdateQuantity");

		/* Smoke_Project_008 */
		outptuWarehouse = findElementInXLSheet(getDataProjectSmoke, "outptuWarehouse");
		remainingQuntityForReturn = findElementInXLSheet(getDataProjectSmoke, "remainingQuntityForReturn");

		/* Smoke_Project_009 */
		acruelVoucherRowDescription = findElementInXLSheet(getDataProjectSmoke, "acruelVoucherRowDescription");
		acruelVoucherAmount = findElementInXLSheet(getDataProjectSmoke, "acruelVoucherAmount");
		costAllocationPresentageAccruelVoucher = findElementInXLSheet(getDataProjectSmoke,
				"costAllocationPresentageAccruelVoucher");
		costAssignmentTypeProjectTask = findElementInXLSheet(getDataProjectSmoke, "costAssignmentTypeProjectTask");

		/* Smoke_Project_010 */
		inoiceProposalFilterTask = findElementInXLSheet(getDataProjectSmoke, "inoiceProposalFilterTask");
		serviceProduct6 = findElementInXLSheet(getDataProjectSmoke, "serviceProduct6");
		serviceProduct7 = findElementInXLSheet(getDataProjectSmoke, "serviceProduct7");
		invoiceProposalSecondRowQuantiry = findElementInXLSheet(getDataProjectSmoke,
				"invoiceProposalSecondRowQuantiry");

		/* Smoke_Project_013 */
		pocPresentage = findElementInXLSheet(getDataProjectSmoke, "pocPresentage");

		/* Smoke_Project_014 */
		inputProductQuantityProjectCostEstimation = findElementInXLSheet(getDataProjectSmoke,
				"inputProductQuantityProjectCostEstimation");
		unitCostServicseProductsPCE = findElementInXLSheet(getDataProjectSmoke, "unitCostServicseProductsPCE");
		description = findElementInXLSheet(getDataProjectSmoke, "description");

		/* Smoke_Project_015 */
		projectName = findElementInXLSheet(getDataProjectSmoke, "projectName");
		durationOfDaysProdjectModel = findElementInXLSheet(getDataProjectSmoke, "durationOfDaysProdjectModel");

	}

}
