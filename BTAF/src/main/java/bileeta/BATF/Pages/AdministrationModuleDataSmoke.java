package bileeta.BATF.Pages;

import bileeta.BTAF.Utilities.TestBase;

public class AdministrationModuleDataSmoke extends TestBase

{
	/* Reading the element locators to variables */
	
	protected static String siteURL;
	protected static String siteLogo;
	protected static String txt_username;
	protected static String txt_password;
	protected static String btn_login;
	protected static String lnk_home;
	protected static String btn_navigationmenu;
    protected static String sidemenu;
	protected static String btn_administration;
	protected static String subsidemenu;
	protected static String btn_userinformation;
    protected static String userinformationpage;
    //create new user
	protected static String btn_newuser;
	protected static String newuserpage;
	protected static String cbox;
	protected static String fullname;
	protected static String displaynamefield;
	protected static String emailnamefield;
	protected static String userlevelfield;
	protected static String usertypefield;
	//draft
	protected static String btn_draft;
	protected static String draftpage;
	//edit
    protected static String btn_edit;
	protected static String editpage;
	protected static String UserLevelField2;
    protected static String btn_update;
	protected static String updatepage;
	//user permission
	protected static String grantedbalancinglevel;
	protected static String btn_addpermission;
	protected static String userpermissionpage1;
	protected static String balancinglevel;
	protected static String btn_form;
	protected static String formtab;
    protected static String modulename;
    protected static String btn_AllowTemplatetoall;
    protected static String btn_AllowPrinttoall;
	protected static String btn_AllowReversetoall;
    protected static String btn_AllowAllReports;
    protected static String btn_AllowReleasetoall;
	protected static String btn_update2;
	protected static String btn_update3;
	//protected static String userpermissionpage2;
	//release
	protected static String btn_release;
	protected static String releasedpage;
	//change password
	protected static String btn_configuration;
	protected static String btn_changepassword;
	protected static String changepasswordpopup;
	protected static String currentpassword;
	protected static String newpassword;
	protected static String confirmnewpassword;
	protected static String btn_change;
	protected static String newhomepage;
    //search a user
	protected static String searchuser;
	protected static String template;
	//protected static String btn_search;
	protected static String username;
	
	//forgot password
	protected static String btn_forgotpassword;
	protected static String resetpasswordpage;
	protected static String emailaddress;
	protected static String btn_next;
	protected static String mailURL;
	protected static String maillogo;
	protected static String mailid;
	protected static String btn_next1;
	protected static String mailpassword;
	protected static String mailpasswordpopup;
	protected static String btn_next2;
	protected static String mailpage;
	protected static String btn_refresh;
	
	//terminate a user,inactive user
	protected static String btn_user;
	protected static String btn_edit1;
	protected static String btn_active;
	protected static String status;
	protected static String btn_apply;
	protected static String btn_apply2;
	protected static String btn_update4;
	protected static String btn_changeperiod;
	protected static String btn_from;
	protected static String btn_fromdate;
	protected static String btn_to;
	protected static String btn_todate;
	protected static String calander;
	protected static String Assignuserpopup;
	protected static String btn_editblock;
	protected static String activeuserpage;
	protected static String btn_changeperiod1;
	protected static String btn_editactive;
	
	//Print tem
	protected static String btn_printtemplatesetup;
	protected static String printtemplatesetuppage;
	protected static String btn_print;
	protected static String selectbalancinglevel;
	protected static String btn_new;
	protected static String addnewtemplatepage;
	protected static String templatename;
	protected static String description;
	protected static String form;
	protected static String datasourse;
	protected static String emailtemplate;
	protected static String btn_update5;
	protected static String  Printtemplate;
	
	//report
	protected static String btn_report;
	protected static String datasourse1;
	protected static String btn_user1;
	protected static String btn_username2; 
	//work flow
	protected static String btn_workflowconfiguration;
	protected static String workflowconfigurationpage;
	protected static String btn_newworkflow;
	protected static String newworkflowpage;
	protected static String workflowname;
	protected static String formname;
	protected static String journeyname1;
	protected static String action;
	protected static String fromaction;
	protected static String btn_create;
	protected static String workflowconfignewpage;
	protected static String btn_sendmail;
	protected static String btn_udatetosendmail;
	protected static String  workflowupdatepage;
	protected static String  btn_activeflow;
	protected static String  confirmationpopup;
	protected static String searchuser01;
	
	
		
	
	//schedule tem
	protected static String btn_scheduletemplate;
	protected static String scheduletemplatepage;
	protected static String btn_newscheduletemplate;
	protected static String newscheduletemplatepage;
	protected static String templatecode;
	protected static String templatedescription;
	protected static String btn_Recurringseriesoftask;
	protected static String btn_daily;
	protected static String draftscheduletemplatepage;
	protected static String btn_releasest;
	protected static String releasesecheduletemplatepage;
	// scduletem task
	protected static String btn_entution;
	protected static String btn_rolecentre;
	protected static String rolecentrepage;
	protected static String btn_ScheduledJobs;
	protected static String ScheduledJobspage;
	protected static String btn_expand;
	protected static String btn_selectall;
	protected static String btn_run;
	protected static String message;
	
	//journey comfofiguration
	protected static String btn_journeyconfiguration;
	protected static String journeyconfigurationpage;
	protected static String btn_WIPrequest;
	protected static String WIPrequestpage;
	protected static String stockreservation;
	protected static String btn_update6;
	protected static String journeyupdatepage;
	
	//superuser can send resetpassword link
	protected static String btn_action;
	protected static String btn_resetpassword;
	protected static String resetpasswordpopup;
	protected static String btn_yes;
	
	//user can activate substitute rule
	protected static String btn_maintainsubstitue;
	protected static String vactionsubstituepopup;
	protected static String btn_isvaction;
	protected static String btn_search;
	protected static String assignuserpopup;
	protected static String username3;
	protected static String btn_startdate;
	protected static String btn_selectastartdate;
	protected static String btn_enddate;
	protected static String btn_selectenddate;
	protected static String btnapply;
	protected static String btn_usernow;
	protected static String btn_applynow;
	
	//search user workflow
	protected static String searchuserworkflow;
	protected static String btn_workflownameapper;
	protected static String workflowinformationpage;
	protected static String btn_editworkflow;
	protected static String btn_start;
	protected static String btn_newapprovalprocess;
	protected static String addeditapprovalprocesspage;
	protected static String displayname;
	protected static String noofapprovals;
	protected static String btn_sendmail2;  
	protected static String btn_lookup;
	protected static String btn_activeyes;
	
	
	protected static String btn_service;
	protected static String btn_case;
	protected static String  casepage;
	
	protected static String  UserNameDatatask;
	protected static String PasswordDatatask;
	protected static String btn_task1;
	protected static String taskpage;
	protected static String btn_Approval;
	protected static String approvalpage;
	protected static String  lnk_home2;
	protected static String  btn_arrow;
	protected static String approvalrequestpage;
	protected static String remark;
	protected static String btn_ok;
	protected static String btn_activeblock;
	
	protected static String tasktemplate;
	protected static String txt_taskTemplate;
	protected static String task;
	protected static String searchtask;
	protected static String updatedUserinformationPage;
	protected static String  TerminateStatusValue;
	
	//ActionCurrencyInformation
	protected static String  btn_CurrencyInformation;
	protected static String  CurrencyInformationPage;
	protected static String ticktoactivate;
	protected static String  btn_updateCurrencyInformationpage;
	protected static String btn_closevactionsubstitute;
	protected static String btn_UserPermission;
	protected static String  UserPermissionPage;
	protected static String btn_searchuser;
	protected static String user;
	protected static String btn_Report;
	protected static String  UserReportPage;
	protected static String btn_Addresstype;
	protected static String  AddressTypePage;
	protected static String  btn_IntegrationServiceSetup;
	protected static String IntegrationServiceSetupPage;
	protected static String  btn_editpencil;
	protected static String  btn_IntegrationServiceSetupupdate;
	protected static String  btn_PermissionGroup;
	protected static String  btn_NewPermissionGroup;
	protected static String PermissionGroupPage;
	protected static String GroupCode;
	protected static String GroupName;
	protected static String btn_searchuserPermissionGroup;
	protected static String  DraftedPermissionGroupPage;
	protected static String  btn_updateandnew;
	protected static String  btn_copyfrom;
	protected static String btn_draftandnew;
	protected static String Newscheduletemplatepage;
	protected static String  btn_SystemPolicyConfiguration;
	protected static String  SystemPolicyConfigurationSetuppage;
	protected static String  btn_viewpost;
	protected static String UpdatedSystemPolicyConfigurationSetuppage;
	protected static String btn_updatePolicyConfigurationSetuppage;
	protected static String ProductCodeName;
	protected static String btn_BalancingLevelSettings;
	protected static String BalancingLevelSettingPage;
	protected static String ProductCode;
	protected static String btn_productInformationupdate;
	protected static String  btn_PasswordPolicies;
	protected static String   PasswordPoliciesPage;
	protected static String btn_updatePasswordPolicies;
	protected static String btn_SharingGroup;
	protected static String SharingGroupPage;
	protected static String btn_TermsandConditions;
	protected static String TermsandConditionsPage;
	protected static String  btn_NewTermsandCondition;
	protected static String NewTermsandConditionsPage;
	protected static String BalancingLevel;
	protected static String code;
	protected static String DescriptionCode;
	protected static String  btn_UpdateandNew;


	
	//////////////////////
	/* Reading the Data to variables */
	
	protected static String UserNameData;
	protected static String PasswordData;
	protected static String txt_cbox;
	protected static String txt_FullName;
	protected static String txt_DisplayNameField;
	protected static String txt_EmailNameField;
	protected static String txt_UserLevelField;
	protected static String txt_UserTypeField;
	protected static String txt_BalancingLevel;
	protected static String txt_ModuleName;
	//edit a user
	protected static String txt_UserLevelField2;
	//change password
	protected static String txt_CurrentPassword;
	protected static String txt_NewPassword;
	protected static String txt_ConfirmNewPassword ;
	protected static String txt_SearchUser;
	protected static String txt_Template;
	//forgot password
	protected static String txt_EmailAddress;
	protected static String txt_mailpassword;
	//terminate user
	protected static String txt_status;
	//inactive user
	protected static String txt_status2;
	//block a user
	protected static String txt_status3;
	//active a blocked user
	protected static String txt_status4;
	
	//activate a inactive user
	protected static String txt_status5;
	
    //Terminate a inactive user
	protected static String txt_Templatestatus;
	protected static String txt_SearchUserstatus;
	protected static String txt_status6;
	//print template
	protected static String txt_selectbalancinglevel;
	protected static String txt_templatename;
	protected static String txt_description;
	protected static String txt_form;
	protected static String txt_datasourse;
	protected static String txt_emailtemplate;
	// report
	protected static String txt_datasourse1;
	
	//work flow
	protected static String txt_workflowname;
	protected static String txt_formname;
	protected static String txt_journeyname;
	protected static String txt_action;
	protected static String txt_fromaction;
	//schedule template
	protected static String txt_templatecode;
	protected static String txt_templatedescription;
	//journey config
	protected static String txt_stockreservation;
	//
	protected static String txt_searchuser01;
	//searchuser workflow
	protected static String txt_searchuserworkflow;
	protected static String txt_displayname;
	protected static String txt_noofapprovals;
	//search user for sustitute
	protected static String txt_SearchUser3;
	
	protected static String txt_EmailAddressvalidate;
	protected static String txt_journeyname1;
	protected static String txt_SearchUsercase;
	//aprove task
	protected static String txt_usernametask;
	protected static String txt_passwordtask;
	protected static String txt_remark;
	protected static String txt_searchtask;
	
	//
	protected static String txt_SearchUserstatusnew;
	protected static String txt_Groupcode;
	protected static String  txt_GroupName;
	protected static String  txt_GroupName2;
	protected static String  txt_code;
	protected static String  txt_DescriptionCode;
	//ActionCurrencyInformation

	
	 
	
	public static void readElementlocators() throws Exception
	{
		
		siteLogo= findElementInXLSheet(getParameterAdministrationSmoke,"siteLogo"); 
		txt_username= findElementInXLSheet(getParameterAdministrationSmoke,"user name"); 
		txt_password= findElementInXLSheet(getParameterAdministrationSmoke,"password"); 
		btn_login= findElementInXLSheet(getParameterAdministrationSmoke,"login button"); 
		lnk_home=findElementInXLSheet(getParameterAdministrationSmoke,"lnk_home");
		//admin page
		btn_navigationmenu= findElementInXLSheet(getParameterAdministrationSmoke, "btn_navigation");
		sidemenu= findElementInXLSheet(getParameterAdministrationSmoke, "sidemenu");
	    btn_administration= findElementInXLSheet(getParameterAdministrationSmoke, "btn_administration");
	    subsidemenu= findElementInXLSheet(getParameterAdministrationSmoke, "subsidemenu");
	    btn_userinformation=findElementInXLSheet(getParameterAdministrationSmoke, "btn_userinformation");
	    userinformationpage=findElementInXLSheet(getParameterAdministrationSmoke, "userinformationpage");
	    //create new user
	    btn_newuser=findElementInXLSheet(getParameterAdministrationSmoke, "btn_newuser");
	    newuserpage=findElementInXLSheet(getParameterAdministrationSmoke, "newuserpage");
		cbox=findElementInXLSheet(getParameterAdministrationSmoke, "cbox");
		fullname=findElementInXLSheet(getParameterAdministrationSmoke, "fullname");
		displaynamefield=findElementInXLSheet(getParameterAdministrationSmoke, "displaynamefield");
		emailnamefield=findElementInXLSheet(getParameterAdministrationSmoke, "emailnamefield");
		userlevelfield=findElementInXLSheet(getParameterAdministrationSmoke, "userlevelfield");
		usertypefield=findElementInXLSheet(getParameterAdministrationSmoke, "usertypefield");
		//draft
		btn_draft=findElementInXLSheet(getParameterAdministrationSmoke, "btn_draft");
		draftpage=findElementInXLSheet(getParameterAdministrationSmoke, "draftpage");
	   //edit
		
		btn_edit=findElementInXLSheet(getParameterAdministrationSmoke, "btn_edit");
	    editpage=findElementInXLSheet(getParameterAdministrationSmoke, "editpage");
	    UserLevelField2=findElementInXLSheet(getParameterAdministrationSmoke,"UserLevelField2");
	    //update
	    btn_update=findElementInXLSheet(getParameterAdministrationSmoke, "btn_update");
		updatepage=findElementInXLSheet(getParameterAdministrationSmoke, "updatepage");
		//permission
	    //grantedbalancinglevel=findElementInXLSheet(getParameterAdministrationSmoke, "grantedbalancinglevel");
	 	btn_addpermission=findElementInXLSheet(getParameterAdministrationSmoke, "btn_addpermission");
	 	userpermissionpage1=findElementInXLSheet(getParameterAdministrationSmoke, "userpermissionpage1");
		//fill form
	    balancinglevel=findElementInXLSheet(getParameterAdministrationSmoke, "balancinglevel");
		btn_form=findElementInXLSheet(getParameterAdministrationSmoke, "btn_form");
		formtab=findElementInXLSheet(getParameterAdministrationSmoke, "formtab");
		modulename=findElementInXLSheet(getParameterAdministrationSmoke, "modulename");
		// click btn
		btn_AllowTemplatetoall=findElementInXLSheet(getParameterAdministrationSmoke, "btn_AllowTemplatetoall");
		btn_AllowPrinttoall=findElementInXLSheet(getParameterAdministrationSmoke,"btn_AllowPrinttoall");
		btn_AllowReversetoall=findElementInXLSheet(getParameterAdministrationSmoke, "btn_AllowReversetoall");
		btn_AllowAllReports=findElementInXLSheet(getParameterAdministrationSmoke, "btn_AllowAllReports");
		btn_AllowReleasetoall=findElementInXLSheet(getParameterAdministrationSmoke, "btn_AllowReleasetoall");
		
		btn_update2=findElementInXLSheet(getParameterAdministrationSmoke, "btn_update2");
		btn_update3=findElementInXLSheet(getParameterAdministrationSmoke, "btn_update3");
//userpermissionpage=findElementInXLSheet(getParameterAdministrationSmoke, "userpermissionpage");
		btn_release=findElementInXLSheet(getParameterAdministrationSmoke, "btn_release");
		releasedpage=findElementInXLSheet(getParameterAdministrationSmoke, "releasedpage");
		//change password
		btn_configuration=findElementInXLSheet(getParameterAdministrationSmoke, "btn_configuration");
		btn_changepassword=findElementInXLSheet(getParameterAdministrationSmoke, "btn_changepassword"); 
		changepasswordpopup=findElementInXLSheet(getParameterAdministrationSmoke, "changepasswordpopup"); 
		currentpassword=findElementInXLSheet(getParameterAdministrationSmoke, "currentpassword"); 
		newpassword=findElementInXLSheet(getParameterAdministrationSmoke, "newpassword"); 
		confirmnewpassword=findElementInXLSheet(getParameterAdministrationSmoke, "confirmnewpassword"); 
		btn_change=findElementInXLSheet(getParameterAdministrationSmoke, "btn_change"); 
		newhomepage=findElementInXLSheet(getParameterAdministrationSmoke, "newhomepage"); 
		//search user
		searchuser=findElementInXLSheet(getParameterAdministrationSmoke, "searchuser"); 
		template=findElementInXLSheet(getParameterAdministrationSmoke, "template"); 
	    //btn_search=findElementInXLSheet(getParameterAdministrationSmoke, "btn_search"); 
	    username=findElementInXLSheet(getParameterAdministrationSmoke, "username"); 
	    
	    //forgot password
	    btn_forgotpassword=findElementInXLSheet(getParameterAdministrationSmoke, "btn_forgotpassword"); 
	    resetpasswordpage=findElementInXLSheet(getParameterAdministrationSmoke, "resetpasswordpage"); 
	    emailaddress=findElementInXLSheet(getParameterAdministrationSmoke, "emailaddress");
	    btn_next=findElementInXLSheet(getParameterAdministrationSmoke, "btn_next");
	    mailURL=findElementInXLSheet(getParameterAdministrationSmoke, "mailURL");
	    maillogo=findElementInXLSheet(getParameterAdministrationSmoke, "maillogo");
	    mailid=findElementInXLSheet(getParameterAdministrationSmoke, "mailid");
	    btn_next1=findElementInXLSheet(getParameterAdministrationSmoke, "btn_next1");
	    mailpassword=findElementInXLSheet(getParameterAdministrationSmoke,"mailpassword1");
	    mailpasswordpopup=findElementInXLSheet(getParameterAdministrationSmoke,"mailpasswordpopup");
	    btn_next2=findElementInXLSheet(getParameterAdministrationSmoke,"btn_next2");
	    mailpage=findElementInXLSheet(getParameterAdministrationSmoke,"mailpage");
	    btn_refresh=findElementInXLSheet(getParameterAdministrationSmoke,"btn_refresh");

	    
	    //terminate a user
	    
	    btn_user=findElementInXLSheet(getParameterAdministrationSmoke, "btn_user");
	    btn_edit1=findElementInXLSheet(getParameterAdministrationSmoke, "btn_edit1");
	    btn_active=findElementInXLSheet(getParameterAdministrationSmoke, "btn_active");
	    status=findElementInXLSheet(getParameterAdministrationSmoke, "status");
	    btn_apply=findElementInXLSheet(getParameterAdministrationSmoke, "btn_apply");
	    btn_apply2=findElementInXLSheet(getParameterAdministrationSmoke, "btn_apply2");
	    btn_update4=findElementInXLSheet(getParameterAdministrationSmoke, "btn_update4");
	    calander=findElementInXLSheet(getParameterAdministrationSmoke, "calander");
	    Assignuserpopup=findElementInXLSheet(getParameterAdministrationSmoke, "Assignuserpopup");
	    btn_editblock=findElementInXLSheet(getParameterAdministrationSmoke, "btn_editblock");
	    activeuserpage=findElementInXLSheet(getParameterAdministrationSmoke, "activeuserpage");
	    btn_activeblock=findElementInXLSheet(getParameterAdministrationSmoke, "btn_activeblock");
	    
	     btn_changeperiod=findElementInXLSheet(getParameterAdministrationSmoke, "btn_changeperiod");
		 btn_from=findElementInXLSheet(getParameterAdministrationSmoke, "btn_from");
	     btn_fromdate=findElementInXLSheet(getParameterAdministrationSmoke, "btn_fromdate");
		 btn_to=findElementInXLSheet(getParameterAdministrationSmoke,"btn_to");
		 btn_todate=findElementInXLSheet(getParameterAdministrationSmoke,"btn_todate");
		 btn_changeperiod1=findElementInXLSheet(getParameterAdministrationSmoke,"btn_changeperiod1");
	    
	    //print template
	    btn_printtemplatesetup=findElementInXLSheet(getParameterAdministrationSmoke,"btn_printtemplatesetup");
	    printtemplatesetuppage=findElementInXLSheet(getParameterAdministrationSmoke,"printtemplatesetuppage");
	    btn_print=findElementInXLSheet(getParameterAdministrationSmoke,"btn_print");
	    selectbalancinglevel=findElementInXLSheet(getParameterAdministrationSmoke,"selectbalancinglevel");
	    btn_new=findElementInXLSheet(getParameterAdministrationSmoke,"btn_new");
	    addnewtemplatepage=findElementInXLSheet(getParameterAdministrationSmoke,"addnewtemplatepage");
	    templatename=findElementInXLSheet(getParameterAdministrationSmoke,"templatename");
	    description=findElementInXLSheet(getParameterAdministrationSmoke,"description");
	    form=findElementInXLSheet(getParameterAdministrationSmoke,"form");
	    datasourse=findElementInXLSheet(getParameterAdministrationSmoke,"datasourse");
	    emailtemplate=findElementInXLSheet(getParameterAdministrationSmoke,"emailtemplate");
	    btn_update5=findElementInXLSheet(getParameterAdministrationSmoke,"btn_update5");
	    Printtemplate=findElementInXLSheet(getParameterAdministrationSmoke,"Printtemplate");
	    
	    //report
	    btn_report=findElementInXLSheet(getParameterAdministrationSmoke,"btn_report");
	    datasourse1=findElementInXLSheet(getParameterAdministrationSmoke,"datasourse1");
	    btn_user1=findElementInXLSheet(getParameterAdministrationSmoke,"btn_user1");
	    btn_username2=findElementInXLSheet(getParameterAdministrationSmoke,"btn_username2");
	    
	    //workflow
	    btn_workflowconfiguration=findElementInXLSheet(getParameterAdministrationSmoke,"btn_workflowconfiguration");
	    workflowconfigurationpage=findElementInXLSheet(getParameterAdministrationSmoke,"workflowconfigurationpage");
	    btn_newworkflow=findElementInXLSheet(getParameterAdministrationSmoke,"btn_newworkflow");
	    newworkflowpage=findElementInXLSheet(getParameterAdministrationSmoke,"newworkflowpage");
	    workflowname=findElementInXLSheet(getParameterAdministrationSmoke,"workflowname");
	    formname=findElementInXLSheet(getParameterAdministrationSmoke,"formname");
	   

//	   journeyname1=findElementInXLSheet(getParameterAdministrationSmoke,"journeyname1");
	    action=findElementInXLSheet(getParameterAdministrationSmoke,"action");
	    fromaction=findElementInXLSheet(getParameterAdministrationSmoke,"fromaction");
		btn_create=findElementInXLSheet(getParameterAdministrationSmoke,"btn_create");
		journeyupdatepage=findElementInXLSheet(getParameterAdministrationSmoke,"journeyupdatepage");
		
		//schedule template
		 btn_scheduletemplate=findElementInXLSheet(getParameterAdministrationSmoke,"btn_scheduletemplate"); 
		 scheduletemplatepage=findElementInXLSheet(getParameterAdministrationSmoke,"scheduletemplatepage");
         btn_newscheduletemplate=findElementInXLSheet(getParameterAdministrationSmoke,"btn_newscheduletemplate");
		 newscheduletemplatepage=findElementInXLSheet(getParameterAdministrationSmoke,"newscheduletemplatepage");
		 templatecode=findElementInXLSheet(getParameterAdministrationSmoke,"templatecode");
		 templatedescription=findElementInXLSheet(getParameterAdministrationSmoke,"templatedescription");
	     btn_Recurringseriesoftask=findElementInXLSheet(getParameterAdministrationSmoke,"btn_Recurringseriesoftask");
	     btn_daily=findElementInXLSheet(getParameterAdministrationSmoke,"btn_daily");
	     draftscheduletemplatepage=findElementInXLSheet(getParameterAdministrationSmoke,"draftscheduletemplatepage");
	     btn_releasest=findElementInXLSheet(getParameterAdministrationSmoke,"btn_releasest");
	     releasesecheduletemplatepage=findElementInXLSheet(getParameterAdministrationSmoke,"releasesecheduletemplatepage");
	     
	     // schedule template task
	      btn_entution=findElementInXLSheet(getParameterAdministrationSmoke,"btn_entution"); 
	 	 btn_rolecentre=findElementInXLSheet(getParameterAdministrationSmoke,"btn_rolecentre1"); 
	 	 rolecentrepage=findElementInXLSheet(getParameterAdministrationSmoke,"rolecentrepage"); 
	 	 btn_ScheduledJobs=findElementInXLSheet(getParameterAdministrationSmoke,"btn_ScheduledJobs"); 
	 	 ScheduledJobspage=findElementInXLSheet(getParameterAdministrationSmoke,"ScheduledJobspage"); 
	 	btn_expand=findElementInXLSheet(getParameterAdministrationSmoke,"btn_expand"); 
	 	btn_selectall=findElementInXLSheet(getParameterAdministrationSmoke,"btn_selectall"); 
	 	btn_run=findElementInXLSheet(getParameterAdministrationSmoke,"btn_run"); 
	 	message=findElementInXLSheet(getParameterAdministrationSmoke,"message"); 
	    
	     //journey config
	     btn_journeyconfiguration=findElementInXLSheet(getParameterAdministrationSmoke,"btn_journeyconfiguration");
		 journeyconfigurationpage=findElementInXLSheet(getParameterAdministrationSmoke,"journeyconfigurationpage");
		 btn_WIPrequest=findElementInXLSheet(getParameterAdministrationSmoke,"btn_WIPrequest1");
		 WIPrequestpage=findElementInXLSheet(getParameterAdministrationSmoke,"WIPrequestpage");
		 stockreservation=findElementInXLSheet(getParameterAdministrationSmoke,"stockreservation");
		 btn_update6=findElementInXLSheet(getParameterAdministrationSmoke,"btn_update6");
		 
		 //super user can send resetpassword link
		 btn_action=findElementInXLSheet(getParameterAdministrationSmoke,"btn_action"); 
	     btn_resetpassword=findElementInXLSheet(getParameterAdministrationSmoke,"btn_resetpassword");
	     resetpasswordpopup=findElementInXLSheet(getParameterAdministrationSmoke,"resetpasswordpopup");
		 btn_yes=findElementInXLSheet(getParameterAdministrationSmoke,"btn_yes");
			
		 //substitute
	    btn_maintainsubstitue=findElementInXLSheet(getParameterAdministrationSmoke,"btn_maintainsubstitue"); 
        vactionsubstituepopup=findElementInXLSheet(getParameterAdministrationSmoke,"vactionsubstituepopup"); 
	    btn_isvaction=findElementInXLSheet(getParameterAdministrationSmoke,"btn_isvaction"); 
	    btn_search=findElementInXLSheet(getParameterAdministrationSmoke,"btn_search"); 
	    assignuserpopup=findElementInXLSheet(getParameterAdministrationSmoke,"assignuserpopup"); 
	    username3=findElementInXLSheet(getParameterAdministrationSmoke,"username3");
	    btn_startdate=findElementInXLSheet(getParameterAdministrationSmoke,"btn_startdate"); 
	    btn_selectastartdate=findElementInXLSheet(getParameterAdministrationSmoke,"btn_selectastartdate"); 
	    btn_enddate=findElementInXLSheet(getParameterAdministrationSmoke,"btn_enddate"); 
	    btn_selectenddate=findElementInXLSheet(getParameterAdministrationSmoke,"btn_selectenddate"); 
		btnapply=findElementInXLSheet(getParameterAdministrationSmoke,"btnapply");  
		 btn_usernow=findElementInXLSheet(getParameterAdministrationSmoke,"btn_usernow");
		 btn_applynow=findElementInXLSheet(getParameterAdministrationSmoke,"btn_applynow");
		
		 //search user workflow
		 searchuserworkflow=findElementInXLSheet(getParameterAdministrationSmoke,"searchuserworkflow");
		 btn_workflownameapper=findElementInXLSheet(getParameterAdministrationSmoke,"btn_workflownameapper");
		 workflowinformationpage=findElementInXLSheet(getParameterAdministrationSmoke,"workflowinformationpage");
		 btn_editworkflow=findElementInXLSheet(getParameterAdministrationSmoke,"btn_editworkflow");
		 btn_start=findElementInXLSheet(getParameterAdministrationSmoke,"btn_start1");
		 btn_newapprovalprocess=findElementInXLSheet(getParameterAdministrationSmoke,"btn_newapprovalprocess");
		 addeditapprovalprocesspage=findElementInXLSheet(getParameterAdministrationSmoke,"addeditapprovalprocesspage");
		 displayname=findElementInXLSheet(getParameterAdministrationSmoke,"displayname");
		 noofapprovals=findElementInXLSheet(getParameterAdministrationSmoke,"noofapprovals");
		 btn_sendmail2=findElementInXLSheet(getParameterAdministrationSmoke,"btn_sendmail2");
		 btn_lookup=findElementInXLSheet(getParameterAdministrationSmoke,"btn_lookup");
		 btn_udatetosendmail=findElementInXLSheet(getParameterAdministrationSmoke,"btn_udatetosendmail");
		 workflowupdatepage=findElementInXLSheet(getParameterAdministrationSmoke,"workflowupdatepage");
		 btn_activeflow=findElementInXLSheet(getParameterAdministrationSmoke,"btn_activeflow");
		 confirmationpopup=findElementInXLSheet(getParameterAdministrationSmoke,"confirmationpopup");
		 btn_activeyes=findElementInXLSheet(getParameterAdministrationSmoke,"btn_activeyes");
		 
		 //
		 btn_service=findElementInXLSheet(getParameterAdministrationSmoke,"btn_service");
		 btn_case=findElementInXLSheet(getParameterAdministrationSmoke,"btn_case");
		 casepage=findElementInXLSheet(getParameterAdministrationSmoke,"casepage");
		 UserNameDatatask=findElementInXLSheet(getParameterAdministrationSmoke,"UserNameDatatask");
		 PasswordDatatask=findElementInXLSheet(getParameterAdministrationSmoke,"PasswordDatatask");
		 btn_task1=findElementInXLSheet(getParameterAdministrationSmoke,"btn_task1");
		 taskpage=findElementInXLSheet(getParameterAdministrationSmoke,"taskpage");
		 btn_Approval=findElementInXLSheet(getParameterAdministrationSmoke,"btn_Approval");
		 approvalpage=findElementInXLSheet(getParameterAdministrationSmoke,"approvalpage");
		 lnk_home2=findElementInXLSheet(getParameterAdministrationSmoke,"lnk_home2");
		 btn_arrow=findElementInXLSheet(getParameterAdministrationSmoke,"btn_arrow");
		 approvalrequestpage=findElementInXLSheet(getParameterAdministrationSmoke,"approvalrequestpage");
		 remark=findElementInXLSheet(getParameterAdministrationSmoke,"remark");
		 btn_ok=findElementInXLSheet(getParameterAdministrationSmoke,"btn_ok");
		 searchuser01=findElementInXLSheet(getParameterAdministrationSmoke,"searchuser01");
		 //
		  tasktemplate=findElementInXLSheet(getParameterAdministrationSmoke,"tasktemplate");
		  task=findElementInXLSheet(getParameterAdministrationSmoke,"task");
		  searchtask=findElementInXLSheet(getParameterAdministrationSmoke,"searchtask");
		  btn_editactive=findElementInXLSheet(getParameterAdministrationSmoke,"btn_editactive");
		  updatedUserinformationPage=findElementInXLSheet(getParameterAdministrationSmoke,"updatedUserinformationPage");
		  TerminateStatusValue=findElementInXLSheet(getParameterAdministrationSmoke,"TerminateStatusValue");
		//Action
		  btn_CurrencyInformation=findElementInXLSheet(getParameterAdministrationSmoke,"btn_CurrencyInformation");
		  CurrencyInformationPage=findElementInXLSheet(getParameterAdministrationSmoke,"CurrencyInformationPage");
		  ticktoactivate=findElementInXLSheet(getParameterAdministrationSmoke,"ticktoactivate");
		  btn_updateCurrencyInformationpage=findElementInXLSheet(getParameterAdministrationSmoke,"btn_updateCurrencyInformationpage");
		  btn_closevactionsubstitute=findElementInXLSheet(getParameterAdministrationSmoke,"btn_closevactionsubstitute");
		  btn_UserPermission=findElementInXLSheet(getParameterAdministrationSmoke,"btn_UserPermission");
		  UserPermissionPage=findElementInXLSheet(getParameterAdministrationSmoke,"UserPermissionPage");
		  user=findElementInXLSheet(getParameterAdministrationSmoke,"user");
		  btn_searchuser=findElementInXLSheet(getParameterAdministrationSmoke,"btn_searchuser");
		  btn_Report=findElementInXLSheet(getParameterAdministrationSmoke,"btn_Report");
		  UserReportPage=findElementInXLSheet(getParameterAdministrationSmoke,"UserReportPage");
		  btn_Addresstype=findElementInXLSheet(getParameterAdministrationSmoke,"btn_Addresstype");
		  AddressTypePage=findElementInXLSheet(getParameterAdministrationSmoke,"AddressTypePage");
		  btn_IntegrationServiceSetup=findElementInXLSheet(getParameterAdministrationSmoke,"btn_IntegrationServiceSetup");
		  IntegrationServiceSetupPage=findElementInXLSheet(getParameterAdministrationSmoke,"IntegrationServiceSetupPage");
		  btn_editpencil=findElementInXLSheet(getParameterAdministrationSmoke,"btn_editpencil");
		  btn_IntegrationServiceSetupupdate=findElementInXLSheet(getParameterAdministrationSmoke,"btn_IntegrationServiceSetupupdate");
		  btn_PermissionGroup=findElementInXLSheet(getParameterAdministrationSmoke,"btn_PermissionGroup");
		  btn_NewPermissionGroup=findElementInXLSheet(getParameterAdministrationSmoke,"btn_NewPermissionGroup");
		  PermissionGroupPage=findElementInXLSheet(getParameterAdministrationSmoke,"PermissionGroupPage");
		  GroupCode=findElementInXLSheet(getParameterAdministrationSmoke,"GroupCode");
		  GroupName=findElementInXLSheet(getParameterAdministrationSmoke,"GroupName");
		  btn_searchuserPermissionGroup=findElementInXLSheet(getParameterAdministrationSmoke,"btn_searchuserPermissionGroup");
		  DraftedPermissionGroupPage=findElementInXLSheet(getParameterAdministrationSmoke,"DraftedPermissionGroupPage");
		  btn_updateandnew=findElementInXLSheet(getParameterAdministrationSmoke,"btn_updateandnew");
		  btn_copyfrom=findElementInXLSheet(getParameterAdministrationSmoke,"btn_copyfrom");
		  btn_draftandnew=findElementInXLSheet(getParameterAdministrationSmoke,"btn_draftandnew");
		  Newscheduletemplatepage=findElementInXLSheet(getParameterAdministrationSmoke,"Newscheduletemplatepage");
		  btn_SystemPolicyConfiguration=findElementInXLSheet(getParameterAdministrationSmoke,"btn_SystemPolicyConfiguration");
		  SystemPolicyConfigurationSetuppage=findElementInXLSheet(getParameterAdministrationSmoke,"SystemPolicyConfigurationSetuppage");
		  btn_viewpost=findElementInXLSheet(getParameterAdministrationSmoke,"btn_viewpost");
		  UpdatedSystemPolicyConfigurationSetuppage=findElementInXLSheet(getParameterAdministrationSmoke,"UpdatedSystemPolicyConfigurationSetuppage");
		  btn_updatePolicyConfigurationSetuppage=findElementInXLSheet(getParameterAdministrationSmoke,"btn_updatePolicyConfigurationSetuppage");
		  ProductCodeName=findElementInXLSheet(getParameterAdministrationSmoke,"ProductCodeName");
		  btn_BalancingLevelSettings=findElementInXLSheet(getParameterAdministrationSmoke,"btn_BalancingLevelSettings");
		  BalancingLevelSettingPage=findElementInXLSheet(getParameterAdministrationSmoke,"BalancingLevelSettingPage");
		  ProductCode=findElementInXLSheet(getParameterAdministrationSmoke,"ProductCode");
		  btn_productInformationupdate=findElementInXLSheet(getParameterAdministrationSmoke,"btn_productInformationupdate");
		  btn_PasswordPolicies=findElementInXLSheet(getParameterAdministrationSmoke,"btn_PasswordPolicies");
		  PasswordPoliciesPage=findElementInXLSheet(getParameterAdministrationSmoke,"PasswordPoliciesPage");
		  btn_updatePasswordPolicies=findElementInXLSheet(getParameterAdministrationSmoke,"btn_updatePasswordPolicies");
		  btn_SharingGroup=findElementInXLSheet(getParameterAdministrationSmoke,"btn_SharingGroup");
		  SharingGroupPage=findElementInXLSheet(getParameterAdministrationSmoke,"SharingGroupPage");
		  btn_TermsandConditions=findElementInXLSheet(getParameterAdministrationSmoke,"btn_TermsandConditions");
		  TermsandConditionsPage=findElementInXLSheet(getParameterAdministrationSmoke,"TermsandConditionsPage");
		  btn_NewTermsandCondition=findElementInXLSheet(getParameterAdministrationSmoke,"btn_NewTermsandCondition");
		  NewTermsandConditionsPage=findElementInXLSheet(getParameterAdministrationSmoke,"NewTermsandConditionsPage");
		  BalancingLevel=findElementInXLSheet(getParameterAdministrationSmoke,"BalancingLevel");
		  code=findElementInXLSheet(getParameterAdministrationSmoke,"code");
		  DescriptionCode=findElementInXLSheet(getParameterAdministrationSmoke,"DescriptionCode");
		  btn_UpdateandNew=findElementInXLSheet(getParameterAdministrationSmoke,"btn_UpdateandNew");
	}
	
	
	
	public static void readData() throws Exception
	{
		
		siteURL = findElementInXLSheet(getDataAdministrationSmoke,"site url");
		UserNameData=findElementInXLSheet(getDataAdministrationSmoke,"user name data");
		PasswordData=findElementInXLSheet(getDataAdministrationSmoke,"password data");
		//new user
		txt_cbox=findElementInXLSheet(getDataAdministrationSmoke, "txt_Cbox");
		txt_FullName=findElementInXLSheet(getDataAdministrationSmoke, "txt_FullName");
		txt_DisplayNameField=findElementInXLSheet(getDataAdministrationSmoke, "txt_DisplayNameField");
		getRandomNumber();
		txt_EmailNameField=findElementInXLSheet(getDataAdministrationSmoke, "txt_EmailNameField");
		txt_EmailNameField=txt_EmailNameField.replace("ssssss", Integer.toString((int)randNumber));
		txt_UserLevelField=findElementInXLSheet(getDataAdministrationSmoke, "txt_UserLevelField");
		txt_UserTypeField=findElementInXLSheet(getDataAdministrationSmoke, "txt_UserTypeField");
      	txt_BalancingLevel=findElementInXLSheet(getDataAdministrationSmoke, "txt_BalancingLevel");
      	txt_ModuleName=findElementInXLSheet(getDataAdministrationSmoke, "txt_ModuleName");
      	//edit
      	txt_UserLevelField2=findElementInXLSheet(getDataAdministrationSmoke, "txt_UserLevelField2");
      	//change password
		txt_CurrentPassword=findElementInXLSheet(getDataAdministrationSmoke, "txt_CurrentPassword");
		txt_NewPassword=findElementInXLSheet(getDataAdministrationSmoke, "txt_NewPassword");
		txt_ConfirmNewPassword=findElementInXLSheet(getDataAdministrationSmoke, "txt_ConfirmNewPassword");
	    //search user
		txt_SearchUser=findElementInXLSheet(getDataAdministrationSmoke, "txt_SearchUser");
		txt_SearchUser=txt_EmailNameField;
	    txt_Template=findElementInXLSheet(getDataAdministrationSmoke, "txt_Template");
	    //Forgot password
	    txt_EmailAddress=findElementInXLSheet(getDataAdministrationSmoke,"txt_EmailAddress");
	    txt_mailpassword=findElementInXLSheet(getParameterAdministrationSmoke,"txt_mailpassword");
	    txt_EmailAddressvalidate=findElementInXLSheet(getParameterAdministrationSmoke," txt_EmailAddressvalidate");
	    //Terminate user
	    txt_status=findElementInXLSheet(getDataAdministrationSmoke, "txt_status");
	    //Inactive user
	    txt_status2=findElementInXLSheet(getDataAdministrationSmoke, "txt_status2");
	    //block a user
	    txt_status3=findElementInXLSheet(getDataAdministrationSmoke, "txt_status3");
	    //active a blocked user
	    txt_status4=findElementInXLSheet(getDataAdministrationSmoke, "txt_status4");
	    //activate a inactive user
	    txt_status5=findElementInXLSheet(getDataAdministrationSmoke, "txt_status5");
	    //terminate a inactive user
	    txt_Templatestatus=findElementInXLSheet(getDataAdministrationSmoke, "txt_Templatestatus");
	    txt_SearchUserstatus=findElementInXLSheet(getDataAdministrationSmoke, "txt_SearchUserstatus");
	    txt_status6=findElementInXLSheet(getDataAdministrationSmoke, "txt_status6");
	    //print template
	    txt_selectbalancinglevel=findElementInXLSheet(getDataAdministrationSmoke, "txt_selectbalancinglevel");
	    txt_templatename=findElementInXLSheet(getDataAdministrationSmoke,"txt_templatename");
	    txt_description=findElementInXLSheet(getDataAdministrationSmoke,"txt_description");
	    txt_form=findElementInXLSheet(getDataAdministrationSmoke,"txt_form");
	    txt_datasourse=findElementInXLSheet(getDataAdministrationSmoke,"txt_datasourse");
	    txt_emailtemplate=findElementInXLSheet(getDataAdministrationSmoke,"txt_emailtemplate");
	    // report
	    txt_datasourse1=findElementInXLSheet(getDataAdministrationSmoke,"txt_datasourse1");
	    //work flow
	    txt_workflowname=findElementInXLSheet(getDataAdministrationSmoke,"txt_workflowname");
	    txt_formname=findElementInXLSheet(getDataAdministrationSmoke,"txt_formname");
		txt_journeyname1=findElementInXLSheet(getDataAdministrationSmoke,"txt_journeyname1");
		txt_action=findElementInXLSheet(getDataAdministrationSmoke,"txt_action");
		txt_fromaction=findElementInXLSheet(getDataAdministrationSmoke,"txt_fromaction");
		
		//schedule template
		txt_templatecode=findElementInXLSheet(getDataAdministrationSmoke,"txt_templatecode");
		txt_templatedescription=findElementInXLSheet(getDataAdministrationSmoke,"txt_templatedescription");
		
		//journe config
		txt_stockreservation=findElementInXLSheet(getDataAdministrationSmoke,"txt_stockreservation");
	    //
		txt_searchuser01=findElementInXLSheet(getDataAdministrationSmoke,"txt_searchuser01");
		txt_SearchUserstatusnew=findElementInXLSheet(getDataAdministrationSmoke,"txt_SearchUserstatusnew");
		//
		txt_searchuserworkflow=findElementInXLSheet(getDataAdministrationSmoke,"txt_searchuserworkflow");
		txt_displayname=findElementInXLSheet(getDataAdministrationSmoke,"txt_displayname");
	    txt_noofapprovals=findElementInXLSheet(getDataAdministrationSmoke,"txt_noofapprovals");
	    txt_SearchUser3=findElementInXLSheet(getDataAdministrationSmoke,"txt_SearchUser3");
	    txt_SearchUsercase=findElementInXLSheet(getDataAdministrationSmoke,"txt_SearchUsercase");
	    
	    //approve task
	    txt_usernametask=findElementInXLSheet(getDataAdministrationSmoke,"txt_usernametask");
	    txt_passwordtask=findElementInXLSheet(getDataAdministrationSmoke,"txt_passwordtask");
	    txt_remark=findElementInXLSheet(getDataAdministrationSmoke,"txt_remark");
	    //
	    txt_taskTemplate=findElementInXLSheet(getDataAdministrationSmoke,"txt_taskTemplate");
	    txt_searchtask=findElementInXLSheet(getDataAdministrationSmoke,"txt_searchtask");
	    txt_Groupcode=findElementInXLSheet(getDataAdministrationSmoke,"txt_Groupcode");
	    txt_GroupName=findElementInXLSheet(getDataAdministrationSmoke,"txt_GroupName");
	    txt_GroupName2=findElementInXLSheet(getDataAdministrationSmoke,"txt_GroupName2");
	    txt_code=findElementInXLSheet(getDataAdministrationSmoke,"txt_code");
	    txt_DescriptionCode=findElementInXLSheet(getDataAdministrationSmoke,"txt_DescriptionCode");
	}

}
