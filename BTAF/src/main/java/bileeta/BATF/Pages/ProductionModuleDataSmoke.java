package bileeta.BATF.Pages;

import bileeta.BTAF.Utilities.TestBase;

public class ProductionModuleDataSmoke extends TestBase {


	protected static String siteURL;
	protected static String siteLogo;
	protected static String txt_username;
	protected static String txt_password;
	protected static String btn_login;
	protected static String lnk_home;

	/* Navigate to side menu */
	protected static String navigateMenu;
	protected static String sideNavBar;
	protected static String btn_Production;
	protected static String logo_Production;
	

	/* Reading the Data to variables */

	protected static String UserNameData;
	protected static String PasswordData;

	/*Navigate to inventory module*/
	protected static String btn_inventory;
	protected static String txt_inventory;
	protected static String btn_billOfMaterial;
	protected static String label_billOfMaterial;
	protected static String btn_newBOM;
	protected static String txt_newBOM;
	protected static String txt_desc;
	protected static String btn_productGrp;
	protected static String btn_outputProductSearch;
	protected static String txt_outputProductSearch;
	protected static String doubleClickoutputProduct;
	protected static String txt_BOMQuantity;
	protected static String btn_expiryDate;
	protected static String clickExpiryDate;
	protected static String btn_rawMaterial;
	protected static String txt_rawMaterial;
	protected static String doubleClickrawMaterial;
	protected static String txt_rawQuantity;
	protected static String btn_draft;
	protected static String btn_release;
	
	protected static String btn_resources;
	protected static String btn_resourceSearch;
	protected static String txt_additionalResources;
	protected static String doubleCLickAdditinalResources;
	protected static String txt_lotNo;
	protected static String bulkno;
	
	/*Navigate to Production module*/
	protected static String btn_production;
	protected static String txt_production;
	protected static String btn_BOO;
	protected static String txt_BOO;
	protected static String btn_newBOO;
	protected static String txt_newBOO;
	protected static String txt_BOOCode;
	protected static String btn_descriptionBOO;
	protected static String txt_descrBOO;
	protected static String btn_BOOApply;
	protected static String txt_BOOLogo;
	protected static String btn_plusBOO;
	protected static String txt_ElementCategory;
	protected static String txt_ElementDescription;
	protected static String btn_applyOperation;
	protected static String label_operationActivity;
	
	protected static String txt_ElementCategoryOperation;
	protected static String txt_ElementDescriptionOperation;
	protected static String GroupIdData;
	protected static String txt_activityType;
	protected static String txt_FinalInvestigation;
	protected static String btn_plusBOOSupply;
	protected static String btn_plusBOOOperation;
	protected static String btn_plusBOOQCBillOfOperation;
	
	protected static String btn_supplyBillOfOperationPlusMark;
	protected static String btn_productionModel;
	protected static String txt_productionModel; 
	protected static String btn_newProductionModelRebult;
	protected static String txt_modelCode;
	protected static String txt_productGroup;
	protected static String btn_yes;
	protected static String btn_ProductionUnit;
	
	protected static String txt_ProductionUnit;
	protected static String txt_ProductionUnitLookup;
	protected static String doubleClickProductionUnit;
	protected static String btn_MRPEnabled;
	protected static String btn_Infinite;
	protected static String txt_batchQty;
	protected static String txt_barcodeBook;
	protected static String txt_CostingPriority;
	protected static String txt_standardCost;
	protected static String btn_pricingProfile;
	protected static String txt_PricingProfile;
	protected static String doubleClickPricingProfile;
	protected static String txt_newproductionModel;
	
	protected static String btn_BOONoSearch;
	protected static String txt_BOOOperation;
	protected static String doubleClickBOOOperation;
	protected static String btn_informationOK;
	
	protected static String btn_BOOTab;
	protected static String btn_Expand;
	protected static String btn_supply;
	protected static String btn_inputProducts;
	protected static String btn_checkBox;
	protected static String btn_apply;
	
	
	protected static String btn_product;
	protected static String txt_product;
	protected static String doubleClickproduct;
	protected static String txt_processTime;
	protected static String btn_operation;
	protected static String btn_QC;
	
	protected static String btn_productionOrder;
	protected static String btn_newProductionOrder;
	protected static String txt_productionOrder;
	protected static String btn_mainUpdate;
	protected static String btn_internalReceiptControl;
	
	protected static String btn_outputProduct;
	protected static String txt_plannedqty;
	
	protected static String txt_productionOrderGroup;
	protected static String txt_productionOrderDesc;
	protected static String btn_productionOrderModel;
	protected static String txt_productionOrderModel;
	protected static String doubleClickOrderModel;
	protected static String txt_productionOrderNo;
	
	protected static String btn_ProductionControl;
	protected static String txt_searchProductionOrder;
	protected static String btn_searchProductionOrder;
	protected static String btn_checkBoxProductionOrderNo;
	protected static String btn_action;
	protected static String btn_RunMRP;
	protected static String txt_MRPNo;
	protected static String btn_run;
	protected static String btn_releaseProductionOrder;
	protected static String btn_close;
	
	protected static String btn_entutionLogo;
	protected static String btn_taskEvent;
	protected static String btn_InternalDispatchOrder;
	protected static String btn_arrow;
	protected static String txt_shippingAddress;
	
	protected static String txt_plannedQty;
	protected static String btn_serialCapture;
	protected static String btn_productList;
	protected static String txt_BatchNo;
	protected static String btn_back;
	
	
	protected static String btn_productionOrderNo;
	protected static String btn_shopFloorUpdate;
	protected static String btn_start;
	protected static String btn_startProcess;
	protected static String btn_actual;
	protected static String txt_producedQty;
	protected static String btn_update;
	protected static String btn_inputProduct;
	protected static String txt_actualQty;
	protected static String btn_complete;
	protected static String txt_prodOrderNo;
	protected static String btn_searchProdOrder;
	protected static String doubleClickprodNo;
	
	protected static String btn_labelPrint;
	protected static String btn_view;
	protected static String txt_batchNo;
	protected static String txt_machineNo;
	protected static String txt_color;
	protected static String btn_draftNPrint;
	
	protected static String btn_labelPrintHistory;
	protected static String btn_productOrder;
	protected static String btn_QcComplete;
	
	protected static String txt_productonOrder;
	protected static String btn_costingRefresh;
	protected static String btn_tick;
	protected static String btn_updateCosting;
	protected static String btn_inputProductsCostingTab;
	
	protected static String btn_organizationManagement;
	protected static String txt_organizationManagement;
	
	protected static String btn_locationInfo;
	protected static String txt_productionUnit;
	protected static String doubleClickProductUnit;
	protected static String txt_locationInformation;
	protected static String btn_productionUnit;
	
	protected static String btn_newLocation;
	protected static String txt_locationCode;
	protected static String txt_description;
	protected static String txt_businessUnit;
	protected static String txt_inputWare;
	protected static String txt_outWare;
	protected static String txt_WIPWare;
	protected static String txt_site;
	protected static String txt_newLocation;
	
	protected static String txt_BOONo;
	
	protected static String txt_overheadInformation;
	protected static String btn_searchNewLocation;
	protected static String txt_overhead;
	protected static String doubleClickOverhead;
	
	protected static String btn_draftLocation;
	protected static String txt_activate;
	
	protected static String btn_PricingProfile;
	protected static String btn_newPricingProfile;
	protected static String txt_pricingProfileCode;
	protected static String txt_descPricingProfile;
	protected static String txt_pricingGroup;
	
	protected static String txt_material;
	protected static String txt_labour;
	protected static String txt_Overhead;
	protected static String txt_service;
	protected static String txt_expense;
	protected static String btn_estimate;
	
	protected static String btn_actualCalculation;
	protected static String txt_materialAC;
	protected static String txt_labourAC;
	protected static String txt_OverheadAC;
	protected static String txt_serviceAC;
	protected static String txt_expenseAC;
	
	protected static String btn_closeShop; 
	protected static String btn_completeTick;
	protected static String txt_complete;
	
	protected static String btn_active;
	protected static String btn_delete;
	protected static String txt_rate;
	protected static String txt_Unit;
	
	protected static String txt_overheadInfo;
	protected static String btn_new;
	protected static String txt_overheadCode;
	protected static String txt_descOverhead;
	protected static String txt_overheadGroup;
	protected static String txt_rateType;
	protected static String btn_draftOverhead;
	protected static String txt_newOverheadCode;
	
	protected static String txt_BOM;
	protected static String btn_started;
	protected static String btn_open;
	
	protected static String btn_bulkProductionOrder;
	protected static String new_bulkProductionOrder;
	protected static String txt_descrProductionOrderGroup;
	protected static String txt_referenceType;
	
	protected static String txt_BulkProduction;
	protected static String txt_BulkProductionForm;
	
	protected static String btn_productionUnitBulk;
	protected static String txt_productionUnitBulk;
	protected static String doubleClickProductionUnitBulk;
	
	protected static String btn_productTab;
	protected static String btn_productSearch;
	protected static String txt_productSearch;
	protected static String doubleClickProductSearch;
	protected static String btn_productModel;
	protected static String txt_productModel;
	protected static String doubleClickProductModel;
	protected static String btn_checkOut;
	
	protected static String btn_createInternalOrder;
	protected static String btn_selectAll;
	
	protected static String txt_title;
	protected static String btn_requester;
	protected static String txt_requester;
	protected static String doubleClickRequester;
	
	protected static String btn_goTopage;
	protected static String txt_plannedQtyBulk;
	protected static String btn_draftProductionOrder;
	protected static String btn_releaseBulkProductionOrder;
	protected static String btn_actionBulk;
	protected static String btn_applyInternal;
	protected static String txt_shipping; 
	
	protected static String btn_productionCosting;
	protected static String btn_date;
	protected static String btn_input;
	protected static String btn_internalReceipt;
	protected static String btn_smallcheckBox;
	protected static String btn_generate;
	
	protected static String txt_bulkOrder;
	protected static String bulkOrder;
	
	protected static String txt_standardCost1;
	
	protected static String btn_inputProductsTab;
	protected static String btn_searchProduct;
	protected static String txt_searchProduct;
	protected static String doubleClickSearchProduct;
	protected static String txt_plannedqty1;
	
	protected static String btn_closeTab;
	protected static String btn_actualDate;
	
	protected static String txt_actualQty1;
	protected static String btn_releaseTab;
	protected static String txt_internalReceipt;
	
	protected static String txt_internalDispatchRelease;
	protected static String txt_internalOrderRelease;
	protected static String txt_BulkOrderRelease;
	protected static String txt_productionModelRelease;
	protected static String txt_POReleased;
	protected static String txt_internalDispatchOrderReleased;
	protected static String txt_Pricing;
	protected static String txt_activateStatus;
	protected static String txt_openStatus;
	
	protected static String btn_Capture;
	protected static String btn_productListClick;
	protected static String txt_batchNoTXT;
	protected static String btn_refresh;
	
	protected static String link_internalReceipt;
	protected static String btn_journalEntry;
	protected static String link_gernalEntryDetails;
	protected static String btn_closeCosting;
	
	protected static String btn_actionCosting;
	protected static String btn_generateReceipt;
	protected static String txt_journalEntryReleased;
	protected static String btn_actionJournal;
	
	protected static String txt_internalReceiptReleased;
	
	//creating new product
	protected static String btn_productInfo;
	protected static String btn_newProduct;
	protected static String txt_productCode;
	protected static String txt_productDescription;
	protected static String txt_prodGrop;
	protected static String txt_basePrice;
	protected static String btn_manufacturer;
	protected static String txt_manufacturer;
	protected static String doubleCLickManufacturer;
	protected static String txt_UOMGroup;
	protected static String txt_UOM;
	protected static String txt_length;
	protected static String txt_lengthVal;
	protected static String txt_width;
	protected static String txt_widthVal;
	protected static String txt_height;
	protected static String txt_heightVal;
	protected static String txt_volume;
	protected static String txt_VolumeVal;
	protected static String btn_tabDetails;
	protected static String btn_allowInventoryWithoutCosting;
	protected static String txt_manufacturingType;
	protected static String txt_batchBook;
	protected static String txt_productName;
	
	

	/* Reading the Data to variables */
	protected static String rawQuantityData;
	protected static String descData;
	protected static String productGrpData;
	protected static String BOMQuantityData;
	protected static String rawMaterialData;
	protected static String outputProductData;
	
	protected static String BOOCodeData;
	protected static String BOODescrData;
	protected static String ElementCategoryData;
	protected static String ElementDescData;
	
	protected static String ElementCategoryDataOperation;
	protected static String ElementDescDataOperation;
	protected static String txt_GroupId;
	protected static String GroupIdData2;
	
	protected static String GroupIdData1;
	protected static String GroupIdData4;
	protected static String GroupIdDataQC;
	protected static String modelCodeData;
	protected static String productGroupData;
	protected static String ProductionUnitData;
	
	protected static String batchQtyData;
	protected static String standardCostData;
	protected static String PricingProfileData;
	
	protected static String BOOOperationData;
	protected static String productData;
	protected static String plannedqtyData;
	
	protected static String productionOrderDescData;
	protected static String productionOrderModelData;
	
	protected static String shippingAddressData;
	
	protected static String plannedQtyData;
	protected static String BatchNoData;
	
	protected static String producedData;
	protected static String actualQtyData;
	
	protected static String productionUnitData;
	
	protected static String locationCodeData;
	protected static String descriptionData;
	protected static String inputwareData;
	protected static String outputwareData;
	protected static String WIPwareData;
	
	
	protected static String overheadData;
	
	protected static String pricingProfileCodeData;
	protected static String descPricingProfileData;
	protected static String rateData;
	
	protected static String overheadCodeData;
	protected static String overheadDescData;
	
	protected static String productionOrderGroupData;
	
	protected static String productionUnitBulkData;
	protected static String productSearchData;
	protected static String productModel;
	
	protected static String titleData;
	protected static String requesterData;
	
	protected static String additionalResourcesData;
	protected static String plannedqtyBulkData;
	protected static String shippingData;
	protected static String lotNo;
	
	protected static String bulkOrderData;
	
	protected static String standardCostData1;
	
	protected static String searchProductData;
	protected static String plannedqtyData1;
	
	protected static String actualQtyData1;
	protected static String modelData;
	
	protected static String batchData;
	protected static String GroupIdDatafinal;
	
	
	//creating new product
	protected static String productCodeData;
	protected static String basePriceData;
	protected static String manufacturerData;
	protected static String lengthData;
	protected static String widthData;
	protected static String heightData;
	protected static String volumeData;
	
	
	/*New Regression Test Cases*/
	protected static String	btn_productionorder;
	protected static String btn_edit;
	protected static String btn_Update;
	protected static String txt_searchProductionOrder1;
	protected static String btn_duplicate;
	protected static String txt_outputProduct1;
	protected static String txt_outputProduct2;
	protected static String txt_costingPriority1;
	protected static String txt_costingPriority2;
	protected static String txt_prodOrderGroup1;
	protected static String txt_prodOrderGroup2;
	protected static String txt_description1;
	protected static String txt_description2;
	protected static String txt_prodModel1;
	protected static String txt_prodModel2;

	
	
	
	/* QA Form level action verification */
	protected static String txt_draftBOM;
	protected static String txt_editBOM;
	protected static String txt_updateBOM;
	protected static String txt_duplicateBOM;
	protected static String btn_draftNNew;
	protected static String txt_draftNNewBOM;
	protected static String btn_copyFrom;
	protected static String txt_BOm;
	protected static String txt_previousBOM;
	protected static String doublePreviousBOM;
	protected static String txt_num;
	protected static String btn_hold;
	protected static String txt_changeStatus;
	protected static String txt_reason;
	protected static String btn_ok;
	protected static String btn_unHold;
	protected static String btn_newVersion;
	protected static String txt_validMsg;
	protected static String btn_reminder;
	protected static String txt_createReminder; 
	protected static String btn_history;
	protected static String txt_history;
	protected static String btn_reverse;
	protected static String txt_reverse;
	protected static String btn_updateReminder;
	protected static String txt_delete;
	protected static String btn_Delete;
	protected static String btn_updateNNew;
	protected static String txt_updateNNewBOM;
	protected static String txt_previousBOO;
	protected static String doublePreviousBOO;
	protected static String txt_duplicateBOO;
	protected static String txt_BOOAction;
	protected static String btn_activities;
	protected static String txt_activity;
	protected static String txt_subject;
	protected static String btn_assignTo;
	protected static String txt_AssignTo;
	protected static String doubleAssignTo;
	protected static String btn_UpdateTask;
	protected static String txt_Boo;
	protected static String txt_productionModelCode;
	protected static String txt_previousmodel;
	protected static String doublePreviousmodel;
	protected static String txt_productionModelTab;
	protected static String txt_BillOfMaterial;
	protected static String tab_summary;
	protected static String txt_plannedQty1;
	protected static String txt_priority;
	protected static String txt_previousproductionOrder;
	protected static String doublePreviousProductionOrder;
	protected static String txt_productionOrderTab;
	protected static String btn_okHold;
	protected static String btn_jobTechnicalDetail;
	protected static String btn_dockFlow;
	protected static String txt_dockFlow;
	protected static String btn_generatePackinOrder;
	protected static String txt_generatePackingOrder;
	protected static String btn_costEstimation;
	protected static String txt_costEstimation;
	protected static String txt_prodControl;
	protected static String txt_internalDispatchOrderdrafted;
	protected static String btn_editInternal;
	protected static String txt_release;
	protected static String txt_update;
	protected static String btn_datenew;
	protected static String btn_reverseClick;
	protected static String txt_date;
	protected static String txt_draftProductionOrder;
	protected static String txt_updateNNew;
	protected static String btn_products;
	protected static String txt_bulkproductionorderTab;
	protected static String txt_previousBulkOrder;
	protected static String doublePreviousbulkOrder;
	protected static String btn_InternalOrder;
	protected static String txt_InternalOrder;
	protected static String txt_title1;
	protected static String btn_datenew1;
	protected static String btn_report;
	protected static String btn_viewReport;
	protected static String txt_viewReport;
	protected static String txt_active;
	protected static String btn_inactive;
	protected static String txt_inactive;
	protected static String btn_yes1;
	protected static String btn_yes2;
	protected static String txt_new;
	protected static String txt_deleteAc;
	protected static String btn_deleteAc;
	protected static String btn_yes4;
	protected static String txt_updateOrganization;
	protected static String txt_edit;
	protected static String txt_search;
	protected static String btn_reset;
	protected static String txt_reset;
	protected static String txt_internalReceiptLabel;
	protected static String btn_productionParameter;
	protected static String btn_inactivePP;
	protected static String txt_inactivePP;
	protected static String btn_UPDATE;
	protected static String txt_UPDATE;
	protected static String btn_productcostEstimation;
	protected static String btn_newCostEstimation;
	protected static String txt_costEstDescription;
	protected static String btn_costProduct;
	protected static String txt_costProduct;
	protected static String costProductDoubleClick;
	protected static String txt_costEstQuantity;
	protected static String btn_pricingProfileCost;
	protected static String txt_pricingProfileCost;
	protected static String pricingProfileCostDoubleClick;
	protected static String btn_checkout;
	
	/* Smoke_Production Remade */
	/*Smoke_Production_0001_1*/
	protected static String btn_navigationPane;
	protected static String btn_inventoryAndWarehouse;
	protected static String btn_billOfMaterials;
	protected static String header_newBillOfMaterial;
	protected static String header_BillOfMaterailByPage;
	protected static String btn_newBillOfMaterial;
	protected static String drop_productGroup;
	protected static String btn_lookupOutputProduct;
	protected static String lnk_inforOnLookupInformationReplace;
	protected static String txt_productFrontPageBOM;
	protected static String txt_quantityBOM;
	protected static String btn_lookupOnLineBOMRowReplace;
	protected static String btn_addNewRecord;
	protected static String txt_qtyBOMInputProductLinesRowReplace;
	protected static String header_draftedBOM;
	protected static String btn_releaseCommon;
	protected static String header_releasedBillOfMaterial;
	protected static String lbl_docNumber;
	protected static String div_loginVerification;
	
	/* Smoke_Production_0001_2 */
	protected static String txt_booCodeBillOfOperation;
	protected static String drop_elementCategoryBOO;
	protected static String drop_activityTypeBOO;
	protected static String txt_elementDescriptionBOO;
	protected static String btn_applyOperationOneBOO;
	protected static String txt_groupIdBOO;
	protected static String drop_batchMethodBOO;
	protected static String chk_finalInvestigationBOO;
	protected static String header_draftedBOO;
	protected static String header_releasedBOO;
	protected static String btn_productionModule;
	protected static String btn_billOfOperation;
	protected static String header_newBillOfOperation;
	protected static String header_BillOfOperationByPage;
	protected static String btn_newBillOfOperation;
	protected static String btn_addBillOfOperationRowReplace;
	
	/* Smoke_Production_0001_3 */
	protected static String btn_productionModelRebuilt;
	protected static String header_newProductionModel;
	protected static String header_productionModelByPage;
	protected static String btn_newProductionModel;
	protected static String txt_modelCodePM;
	protected static String btn_productionUnitLookup;
	protected static String txt_prductionUnitSearch;
	protected static String txt_produxtProductionModelFrontPage;
	protected static String drop_sitePM;
	protected static String drop_bomPM;
	protected static String txt_batchQuantityPM;
	protected static String drop_barcodeBookPM;
	protected static String drop_costinPriority;
	protected static String txt_searchPricingProfile;
	protected static String btn_pricingProfileLookup;
	protected static String txt_pricingProfileProntPage;
	protected static String chk_mrpPM;
	protected static String btn_billOfOperationLookup;
	protected static String txt_searchBillOfOperation;
	protected static String txt_frontPageBOOPM;
	protected static String btn_expandBOOPM;
	protected static String btn_editBillOfOperationPMRowReplace;
	protected static String chk_inputProductsRowReplaceBOOPM;
	protected static String drop_processTimeTypeBOOPM;
	protected static String btn_inputProductsTabBOOPM;
	protected static String btn_applyBOOPM;
	protected static String header_draftedPM;
	protected static String header_releasedPM;
	protected static String txt_productionUnitFrontPagePM;
	protected static String btn_yesClearConfirmationPM;
	protected static String btn_tabBillOfOperationPM;
	protected static String btn_okClearMsgBOOPM;
	
	/* Smoke_Production_0002 */
	protected static String btn_productionOrder2;
	protected static String header_newProductionOrder;
	protected static String header_productionOrderByPage;
	protected static String btn_newProductionOrder2;
	protected static String drop_productionOrderGroupPO;
	protected static String txt_frontPageOutputProductPO;
	protected static String btn_lookupOutputProductSpan;
	protected static String txt_planedQuantityPO;
	protected static String header_draftedPO;
	protected static String header_releasedPO;
	protected static String txt_descriptionPO;
	protected static String btn_productionModuleLookup;
	protected static String txt_productionModuleFrontPagePO;
	protected static String txt_searchProductionModule;
	
	
	/* Smoke_Production_0003 */
	protected static String btn_productionControl;
	protected static String header_productionControlByPage;
	protected static String chk_mrpPODoRepalce;
	protected static String btn_actionMRP;
	protected static String btn_runMRP;
	protected static String btn_runMRPWindow;
	protected static String btn_releaseMRP;
	protected static String para_MRPReleaseValidation;
	protected static String btn_cloaseMRPWindow;
	protected static String lbl_genaratedInternalOrderMRP;
	protected static String btn_internalDispatchOrderTaskAndEvent;
	protected static String btn_pendingIDODocRepalce;
	protected static String header_draftedIDO;
	protected static String header_releasedIDO;
	protected static String header_newIDO;
	protected static String header_productionControlWindow;
	protected static String btn_homeLink;
	protected static String btn_taskEvent2;
	protected static String btn_logTabProductionControlWindow;
	protected static String txt_shippingAddressIDO;
	
	/* Smoke_Production_0004 */
	protected static String btn_action2;
	protected static String btn_shopFloorUpdate2;
	protected static String header_shopFloorUpdateWindow;
	protected static String btn_startFirstOperationSFU;
	protected static String btn_startSecondLeveleSFU;
	protected static String btn_actualUpdateSFU;
	protected static String txt_outoutQuantityACtualUpdateSFU;
	protected static String btn_tapInputProductsShopFloorUpdate;
	protected static String txt_actualUpdaInputProductQuantityFirstOperation;
	protected static String btn_upateActualSFU;
	protected static String para_successfullValidationActualUpdate;
	protected static String btn_closeFisrstOperationActualUpdateWindow;
	protected static String btn_completeFirstOperation;
	protected static String btn_completeSecondLeveleSFU;
	protected static String lbl_completedShopFloorUpdateBarFirstOperation;
	protected static String btn_startSecondOperationSFU;
	protected static String txt_actualUpdaInputProductQuantitySecondOperation;
	protected static String btn_closeSecondOperationActualUpdateWindow;
	protected static String btn_completeSecondOperation;
	protected static String lbl_completedShopFloorUpdateBarSecondOperation;
	protected static String para_successfullValidationOutProductActualUpdate;
	
	/* Smoke_Production_0005 */
	protected static String div_selectedQcDocProduction;
	protected static String chk_qcProductionAccordingToProductReplaceProduct;
	protected static String drop_documentTypeProductionQC;
	protected static String txt_passQuantityProductionQCOneRow;
	protected static String txt_failQuantityProductionQCOneRow;
	protected static String btn_releseProductionQC;
	protected static String para_successfullValidationProductionQC;
	protected static String txt_docNumberQC;
	protected static String btn_productionQCAndSorting;
	
	/* Smoke_Production_0006 */
	protected static String btn_productionCostingActions;
	protected static String header_productionCostingWindow;
	protected static String btn_actualUpdateProductionCosting;
	protected static String tab_inputProductsProductionCosting;
	protected static String txt_actualUpdateProductionCosting;
	protected static String btn_updateProductionCosting;
	protected static String para_successfullValidationActualUpdateProductionCosting;
	protected static String chk_pendingProductionCosting;
	protected static String chk_pendingProductionCostingSelected;
	protected static String btn_updateProductionCostingMainWindow;
	protected static String para_batchCostingUpdateValidation;
	protected static String header_completedPO;
	protected static String btn_closeSecondWindowCostingUpdate;
	protected static String btn_closeMainCostingUpdateWindowProductionOrder;
	
	/* Smoke_Production_0007 */
	protected static String btn_convertToInternalReceipt;
	protected static String header_genarateInternalReciptSmoke;
	protected static String chk_seletcOroductForInternalRecipt;
	protected static String btn_genarateInternalRecipt;
	protected static String para_validationInternalREciptGenarateSuccessfull;
	protected static String lnk_genaratedInternalREciptSmoke;
	protected static String header_releasedInternalRecipt;
	
	/* Smoke_Production_0008 */
	protected static String btn_journal;
	protected static String header_journalEntryPopup;
	protected static String lbl_debitValueJournelEntrySmoke;
	protected static String lbl_creditValueJournelEntrySmoke;
	protected static String lbl_debitTotalJournelEntrySingleEntry;
	protected static String lbl_creditTotalJournelEntrySingleEntry;

	/*Data*/
	protected static String subjectData;
	protected static String AssignToData;
	protected static String previousBOMData;
	protected static String reasonData;
	protected static String reasonData1;
	protected static String BOOCodeData1;
	protected static String modelCodeData1;
	protected static String modelCode;
	protected static String plannedqtyDataver;
	protected static String productionOrderData;
	protected static String prodNo;
	protected static String previousbulkOrderData;
	protected static String costEstDescription;
	protected static String costProductData;
	protected static String costEstQuantityData;
	protected static String pricingProfileCostData;
	
	/* Production Smoke Remade */
	/* Smoke_Production_0001_1 */
	protected static String description;
	protected static String productGroupSmokeProduction;
	protected static String hundred;
	protected static String five;
	protected static String ten;
	protected static String automationTenantUsername;
	protected static String automationTenantPassword;
	
	/* Smoke_Production_0001_2 */
	protected static String moveStockElementDescription;
	protected static String elementCategorySupply;
	protected static String activityTypeMoveStock;
	protected static String activityTypeProduce;
	protected static String produceElementDescription1;
	protected static String batchMethodTransferBOO;
	protected static String groupIdBOOOne;
	protected static String groupIdBOOTwo;
	protected static String groupIdBOOThree;
	protected static String produceElementDescription2;
	protected static String activityTypeQualityCheck;
	protected static String qcElementDescription;
	protected static String elementCategoryOperation;
	
	/* Smoke_Production_0001_3 */
	protected static String productionUnit;
	protected static String siteNegombo;
	protected static String barcodeBookBG;
	protected static String costingProrityNormal;
	protected static String pricingProfile;
	protected static String processTimeType;
	
	/* Smoke_Production_0002 */
	protected static String productionOrderGroup;
	protected static String productionOrderDescription;
	
	/* Smoke_Production_0003 */
	protected static String commonAddress;
	
	/* Smoke_Production_0004 */
	protected static String actualQuantityInputProductFirstOperation;
	protected static String actualQuantityOutputProductFirstOperation;
	protected static String actualQuantityInputProductSecondOperation;
	protected static String actualQuantityOutputProductSecondOperation;
	
	/* Smoke_Production_0005 */
	protected static String qcTypeProductionOrder;
	protected static String failQuantityProductionSmoke;
	protected static String passQuantityProductionSmoke;
	
	/* Smoke_Production_0006 */
	protected static String actualUpdateQuantityProductionQuntity;
	
	// Calling the constructor
	public static void readElementlocators() throws Exception {
		siteLogo = findElementInXLSheet(getParameterProductionSmoke, "site logo");
		txt_username = findElementInXLSheet(getParameterProductionSmoke, "user name");
		txt_password = findElementInXLSheet(getParameterProductionSmoke, "password");
		btn_login = findElementInXLSheet(getParameterProductionSmoke, "login button");
		lnk_home = findElementInXLSheet(getParameterProductionSmoke, "home_lnk");

		/* Navigate to side menu */
		navigateMenu = findElementInXLSheet(getParameterProductionSmoke, "nav_btn");
		sideNavBar = findElementInXLSheet(getParameterProductionSmoke, "nav_view");
		btn_Production = findElementInXLSheet(getParameterProductionSmoke, "production_btn");
		logo_Production = findElementInXLSheet(getParameterProductionSmoke, "production_logo");
		
		btn_inventory = findElementInXLSheet(getParameterProductionSmoke, "inventory_btn");
		txt_inventory = findElementInXLSheet(getParameterProductionSmoke, "inventory_txt");
		btn_billOfMaterial = findElementInXLSheet(getParameterProductionSmoke, "billOfMaterial_btn");
		label_billOfMaterial = findElementInXLSheet(getParameterProductionSmoke, "billOfMaterial_label");
		btn_newBOM = findElementInXLSheet(getParameterProductionSmoke, "newBOM_btn");
		txt_newBOM = findElementInXLSheet(getParameterProductionSmoke, "newBOM_txt");
		txt_desc = findElementInXLSheet(getParameterProductionSmoke, "txt_desc");
		btn_productGrp = findElementInXLSheet(getParameterProductionSmoke, "productGrp_btn");
		txt_BOMQuantity = findElementInXLSheet(getParameterProductionSmoke, "BOMQuantity_txt");
		btn_expiryDate = findElementInXLSheet(getParameterProductionSmoke, "expiryDate_btn");
		clickExpiryDate = findElementInXLSheet(getParameterProductionSmoke, "click_ExpiryDate");
		btn_rawMaterial = findElementInXLSheet(getParameterProductionSmoke, "rawMaterial_btn");
		txt_rawMaterial = findElementInXLSheet(getParameterProductionSmoke, "rawMaterial_txt");
		doubleClickrawMaterial = findElementInXLSheet(getParameterProductionSmoke, "rawMaterialDoubleClick");
		txt_rawQuantity = findElementInXLSheet(getParameterProductionSmoke, "rawQuantity_txt");
		btn_draft = findElementInXLSheet(getParameterProductionSmoke, "draft_btn");
		btn_release = findElementInXLSheet(getParameterProductionSmoke, "release_btn");
		btn_outputProductSearch = findElementInXLSheet(getParameterProductionSmoke, "outputProductSearch_btn");
		txt_outputProductSearch = findElementInXLSheet(getParameterProductionSmoke, "outputProductSearch_txt");
		doubleClickoutputProduct = findElementInXLSheet(getParameterProductionSmoke, "outputputProductDoubleclick");
		
		
		btn_production = findElementInXLSheet(getParameterProductionSmoke, "production_btn");
		txt_production = findElementInXLSheet(getParameterProductionSmoke, "production_txt");
		btn_BOO = findElementInXLSheet(getParameterProductionSmoke, "BOO_btn");
		txt_BOO = findElementInXLSheet(getParameterProductionSmoke, "BOO_txt");
		btn_newBOO = findElementInXLSheet(getParameterProductionSmoke, "newBOO_btn");
		txt_newBOO = findElementInXLSheet(getParameterProductionSmoke, "newBOO_txt");
		txt_BOOCode = findElementInXLSheet(getParameterProductionSmoke, "BOOCode_txt");
		btn_descriptionBOO = findElementInXLSheet(getParameterProductionSmoke, "descriptionBOO_btn");
		txt_descrBOO = findElementInXLSheet(getParameterProductionSmoke, "descrBOO_txt");
		btn_BOOApply = findElementInXLSheet(getParameterProductionSmoke, "BOOApply_btn");
		txt_BOOLogo = findElementInXLSheet(getParameterProductionSmoke, "BOOLogo_txt");
		btn_plusBOO = findElementInXLSheet(getParameterProductionSmoke, "plusBOO_btn");
		txt_ElementCategory = findElementInXLSheet(getParameterProductionSmoke, "ElementCategory_txt");
		txt_ElementDescription = findElementInXLSheet(getParameterProductionSmoke, "ElementDescription_txt");
		btn_applyOperation = findElementInXLSheet(getParameterProductionSmoke, "applyOperation_btn");
		label_operationActivity = findElementInXLSheet(getParameterProductionSmoke, "operationActivity_label");
		
		txt_ElementCategoryOperation = findElementInXLSheet(getParameterProductionSmoke, "ElementCategoryOperation_txt");
		txt_ElementDescriptionOperation = findElementInXLSheet(getParameterProductionSmoke, "ElementDescriptionOperation_txt");
		txt_GroupId = findElementInXLSheet(getParameterProductionSmoke, "GroupId_txt");
		txt_activityType = findElementInXLSheet(getParameterProductionSmoke, "activityType_txt");
		txt_FinalInvestigation = findElementInXLSheet(getParameterProductionSmoke, "finalInvestigation_txt");
		btn_plusBOOSupply = findElementInXLSheet(getParameterProductionSmoke, "plusBOOSupply_btn");
		btn_plusBOOOperation = findElementInXLSheet(getParameterProductionSmoke, "plusBOOOperation_btn");
		btn_plusBOOQCBillOfOperation = findElementInXLSheet(getParameterProductionSmoke, "plusBOOQCBillOfOperation_btn");
		
		
		btn_supplyBillOfOperationPlusMark = findElementInXLSheet(getParameterProductionSmoke, "supplyBillOfOperationPlusMark_btn");
		btn_productionModel = findElementInXLSheet(getParameterProductionSmoke, "productionModel_btn");
		txt_productionModel = findElementInXLSheet(getParameterProductionSmoke, "productionModel_txt");
		btn_newProductionModel = findElementInXLSheet(getParameterProductionSmoke, "newProductionModel_btn");
		txt_modelCode = findElementInXLSheet(getParameterProductionSmoke, "modelCode_txt");
		txt_productGroup = findElementInXLSheet(getParameterProductionSmoke, "productGroup_txt");
		btn_yes = findElementInXLSheet(getParameterProductionSmoke, "yes_btn");
		btn_ProductionUnit = findElementInXLSheet(getParameterProductionSmoke, "ProductionUnit_btn");
		txt_Unit = findElementInXLSheet(getParameterProductionSmoke, "Unit_txt");
		doubleClickProductionUnit = findElementInXLSheet(getParameterProductionSmoke, "ProductionUnitDoubleclick");
		
		btn_MRPEnabled = findElementInXLSheet(getParameterProductionSmoke, "MRPEnabled_btn");
		btn_Infinite = findElementInXLSheet(getParameterProductionSmoke, "Infinite_btn");
		txt_batchQty = findElementInXLSheet(getParameterProductionSmoke, "batchQty_txt");
		txt_barcodeBook = findElementInXLSheet(getParameterProductionSmoke, "barcodeBook_txt");
		txt_CostingPriority = findElementInXLSheet(getParameterProductionSmoke, "CostingPriority_txt");
		txt_standardCost = findElementInXLSheet(getParameterProductionSmoke, "standardCost_txt");
		btn_pricingProfile = findElementInXLSheet(getParameterProductionSmoke, "pricingProfile_btn");
		txt_PricingProfile = findElementInXLSheet(getParameterProductionSmoke, "PricingProfile_txt");
		doubleClickPricingProfile = findElementInXLSheet(getParameterProductionSmoke, "PricingProfileDoubleclick");
		txt_newproductionModel = findElementInXLSheet(getParameterProductionSmoke, "newProductionModel_txt");
		
		btn_BOONoSearch = findElementInXLSheet(getParameterProductionSmoke, "BOONoSearch_btn");
		txt_BOOOperation = findElementInXLSheet(getParameterProductionSmoke, "BOOOperation_txt");
		doubleClickBOOOperation = findElementInXLSheet(getParameterProductionSmoke, "doubleclick_BOOOperation");
		btn_informationOK = findElementInXLSheet(getParameterProductionSmoke, "informationOK_btn");
		
		btn_BOOTab = findElementInXLSheet(getParameterProductionSmoke, "BOOTab_btn");
		btn_Expand = findElementInXLSheet(getParameterProductionSmoke, "Expand_btn");
		btn_supply = findElementInXLSheet(getParameterProductionSmoke, "supply_btn");
		btn_inputProducts = findElementInXLSheet(getParameterProductionSmoke, "inputProducts_btn");
		btn_checkBox = findElementInXLSheet(getParameterProductionSmoke, "checkBox_btn");
		btn_apply = findElementInXLSheet(getParameterProductionSmoke, "apply_btn");
		
		btn_product = findElementInXLSheet(getParameterProductionSmoke, "product_btn");
		txt_product = findElementInXLSheet(getParameterProductionSmoke, "product_txt");
		btn_operation = findElementInXLSheet(getParameterProductionSmoke, "operation_btn");
		doubleClickproduct = findElementInXLSheet(getParameterProductionSmoke, "doubleClickProduct data");
		txt_processTime = findElementInXLSheet(getParameterProductionSmoke, "processTime_txt");
		btn_QC = findElementInXLSheet(getParameterProductionSmoke, "QC_btn");
		
		btn_productionOrder = findElementInXLSheet(getParameterProductionSmoke, "productionOrder_btn");
		btn_newProductionOrder = findElementInXLSheet(getParameterProductionSmoke, "newProductionOrder_btn");
		txt_productionOrder = findElementInXLSheet(getParameterProductionSmoke, "productionOrder_txt");
		btn_outputProduct = findElementInXLSheet(getParameterProductionSmoke, "outputProduct_btn");
		txt_plannedqty = findElementInXLSheet(getParameterProductionSmoke, "plannedqty_txt");
		
		txt_productionOrderGroup = findElementInXLSheet(getParameterProductionSmoke, "productionOrderGroup_txt");
		txt_productionOrderDesc = findElementInXLSheet(getParameterProductionSmoke, "productionOrderDesc_txt");
		btn_productionOrderModel = findElementInXLSheet(getParameterProductionSmoke, "productionOrderModel_btn");
		txt_productionOrderModel = findElementInXLSheet(getParameterProductionSmoke, "productionOrderModel_txt");
		doubleClickOrderModel = findElementInXLSheet(getParameterProductionSmoke, "doubleClick OrderModel");
		txt_productionOrderNo = findElementInXLSheet(getParameterProductionSmoke, "productionOrderNo_txt");
		
		btn_ProductionControl = findElementInXLSheet(getParameterProductionSmoke, "ProductionControl_btn");
		txt_searchProductionOrder = findElementInXLSheet(getParameterProductionSmoke, "searchProductionOrder_txt");
		btn_searchProductionOrder = findElementInXLSheet(getParameterProductionSmoke, "searchProductionOrder_btn");
		btn_checkBoxProductionOrderNo = findElementInXLSheet(getParameterProductionSmoke, "checkBoxProductionOrderNo_btn");
		btn_action = findElementInXLSheet(getParameterProductionSmoke, "action_btn");
		btn_RunMRP = findElementInXLSheet(getParameterProductionSmoke, "RunMRP_btn");
		txt_MRPNo = findElementInXLSheet(getParameterProductionSmoke, "MRPNo_txt");
		btn_run = findElementInXLSheet(getParameterProductionSmoke, "run_btn");
		btn_releaseProductionOrder = findElementInXLSheet(getParameterProductionSmoke, "releaseProductionOrder_btn");
		btn_close = findElementInXLSheet(getParameterProductionSmoke, "close_btn");
		
		btn_entutionLogo = findElementInXLSheet(getParameterProductionSmoke, "entutuionLogo_btn");
		btn_taskEvent = findElementInXLSheet(getParameterProductionSmoke, "taskEvent_btn");
		btn_InternalDispatchOrder = findElementInXLSheet(getParameterProductionSmoke, "InternalDispatchOrder_btn");
		btn_arrow = findElementInXLSheet(getParameterProductionSmoke, "arrow_btn");
		txt_shippingAddress = findElementInXLSheet(getParameterProductionSmoke, "shippingAddress_txt");
		
		
		txt_plannedQty = findElementInXLSheet(getParameterProductionSmoke, "plannedQty_txt");
		btn_serialCapture = findElementInXLSheet(getParameterProductionSmoke, "serialCapture_btn");
		btn_productList = findElementInXLSheet(getParameterProductionSmoke, "productList_btn");
		txt_BatchNo = findElementInXLSheet(getParameterProductionSmoke, "BatchNo_txt");
		btn_back = findElementInXLSheet(getParameterProductionSmoke, "back_btn");
		
		btn_productionOrderNo = findElementInXLSheet(getParameterProductionSmoke, "productionOrderNo_btn");
		btn_shopFloorUpdate = findElementInXLSheet(getParameterProductionSmoke, "shopFloorUpdate_btn");
		btn_start = findElementInXLSheet(getParameterProductionSmoke, "start_btn");
		btn_startProcess = findElementInXLSheet(getParameterProductionSmoke, "startProcess_btn");
		btn_actual = findElementInXLSheet(getParameterProductionSmoke, "actual_btn");
		txt_producedQty = findElementInXLSheet(getParameterProductionSmoke, "producedQty_txt");
		btn_update = findElementInXLSheet(getParameterProductionSmoke, "update_btn");
		btn_inputProduct = findElementInXLSheet(getParameterProductionSmoke, "inputProduct_btn");
		txt_actualQty = findElementInXLSheet(getParameterProductionSmoke, "actualQty_txt");
		btn_complete = findElementInXLSheet(getParameterProductionSmoke, "complete_btn");
		txt_prodOrderNo = findElementInXLSheet(getParameterProductionSmoke, "prodOrderNo_txt");
		btn_searchProdOrder = findElementInXLSheet(getParameterProductionSmoke, "searchProdOrder_btn");
		doubleClickprodNo = findElementInXLSheet(getParameterProductionSmoke, "doubleclick prodNo");
		
		
		
		btn_labelPrint = findElementInXLSheet(getParameterProductionSmoke, "labelPrint_btn");
		btn_view = findElementInXLSheet(getParameterProductionSmoke, "view_btn");
		txt_batchNo = findElementInXLSheet(getParameterProductionSmoke, "batchNo_txt");
		txt_machineNo = findElementInXLSheet(getParameterProductionSmoke, "machineNo_txt");
		txt_color = findElementInXLSheet(getParameterProductionSmoke, "color_txt");
		btn_draftNPrint = findElementInXLSheet(getParameterProductionSmoke, "draftNPrint_btn");
		
		btn_labelPrintHistory = findElementInXLSheet(getParameterProductionSmoke, "labelPrintHistory_btn");
		btn_productOrder = findElementInXLSheet(getParameterProductionSmoke, "productOrder_btn");
		btn_QcComplete = findElementInXLSheet(getParameterProductionSmoke, "QcComplete_btn");
		
		txt_productonOrder = findElementInXLSheet(getParameterProductionSmoke, "productionOrder_txt");
		btn_costingRefresh = findElementInXLSheet(getParameterProductionSmoke, "costingRefresh_btn");
		btn_tick = findElementInXLSheet(getParameterProductionSmoke, "tick_btn");
		btn_updateCosting = findElementInXLSheet(getParameterProductionSmoke, "updateCosting_btn");
		btn_inputProductsCostingTab = findElementInXLSheet(getParameterProductionSmoke, "inputProductsCostingTab_btn");
		btn_closeCosting = findElementInXLSheet(getParameterProductionSmoke, "closeCosting_btn");
		btn_mainUpdate= findElementInXLSheet(getParameterProductionSmoke, "mainUpdate_btn");
		
		btn_organizationManagement = findElementInXLSheet(getParameterProductionSmoke, "organizationManagement_btn");
		txt_organizationManagement = findElementInXLSheet(getParameterProductionSmoke, "organizationManagement_txt");
		
		
		btn_locationInfo = findElementInXLSheet(getParameterProductionSmoke, "locationInfo_btn");
		txt_productionUnit = findElementInXLSheet(getParameterProductionSmoke, "productionUnit_txt");
		txt_ProductionUnitLookup = findElementInXLSheet(getParameterProductionSmoke, "productionUnitLookup_txt");
		doubleClickProductUnit = findElementInXLSheet(getParameterProductionSmoke, "doubleClick ProductUnit");
		txt_locationInformation = findElementInXLSheet(getParameterProductionSmoke, "locationInformation_txt");
		
		
		btn_newLocation = findElementInXLSheet(getParameterProductionSmoke, "newLocation_btn");
		txt_locationCode = findElementInXLSheet(getParameterProductionSmoke, "locationCode_txt");
		txt_description = findElementInXLSheet(getParameterProductionSmoke, "description_txt");
		txt_businessUnit = findElementInXLSheet(getParameterProductionSmoke, "businessUnit_txt");
		txt_inputWare = findElementInXLSheet(getParameterProductionSmoke, "inputWare_txt");
		txt_outWare = findElementInXLSheet(getParameterProductionSmoke, "outWare_txt");
		txt_WIPWare = findElementInXLSheet(getParameterProductionSmoke, "WIPWare_txt");
		txt_site = findElementInXLSheet(getParameterProductionSmoke, "site_txt");
		txt_newLocation = findElementInXLSheet(getParameterProductionSmoke, "newLocation_txt");
		btn_productionUnit = findElementInXLSheet(getParameterProductionSmoke, "productionUnit_btn");
		
		txt_overheadInformation = findElementInXLSheet(getParameterProductionSmoke, "overheadInformation_txt");
		btn_searchNewLocation = findElementInXLSheet(getParameterProductionSmoke, "searchNewLocation_txt");
		txt_overhead = findElementInXLSheet(getParameterProductionSmoke, "overhead_txt");
		doubleClickOverhead = findElementInXLSheet(getParameterProductionSmoke, "doubleClick overhead");
		
		btn_draftLocation = findElementInXLSheet(getParameterProductionSmoke, "draftLocation_btn");
		txt_activate = findElementInXLSheet(getParameterProductionSmoke, "activate_txt");
		
		btn_PricingProfile = findElementInXLSheet(getParameterProductionSmoke, "PricingProfile_btn");
		btn_newPricingProfile = findElementInXLSheet(getParameterProductionSmoke, "newPricingProfile_btn");
		txt_pricingProfileCode = findElementInXLSheet(getParameterProductionSmoke, "pricingProfileCode_btn");
		txt_descPricingProfile = findElementInXLSheet(getParameterProductionSmoke, "descPricingProfile_txt");
		txt_pricingGroup = findElementInXLSheet(getParameterProductionSmoke, "pricingGroup_txt");
		
		txt_material = findElementInXLSheet(getParameterProductionSmoke, "material_txt");
		txt_labour = findElementInXLSheet(getParameterProductionSmoke, "labour_txt");
		txt_Overhead = findElementInXLSheet(getParameterProductionSmoke, "Overhead_txt");
		txt_service = findElementInXLSheet(getParameterProductionSmoke, "service_txt");
		txt_expense = findElementInXLSheet(getParameterProductionSmoke, "expense_txt");
		btn_estimate = findElementInXLSheet(getParameterProductionSmoke, "estimate_btn");
		
		btn_actualCalculation = findElementInXLSheet(getParameterProductionSmoke, "actualCalculation_btn");
		txt_materialAC = findElementInXLSheet(getParameterProductionSmoke, "materialAC_txt");
		txt_labourAC = findElementInXLSheet(getParameterProductionSmoke, "labourAC_txt");
		txt_OverheadAC = findElementInXLSheet(getParameterProductionSmoke, "OverheadAC_txt");
		txt_serviceAC = findElementInXLSheet(getParameterProductionSmoke, "serviceAC_txt");
		txt_expenseAC = findElementInXLSheet(getParameterProductionSmoke, "expenseAC_txt");
		
		btn_closeShop = findElementInXLSheet(getParameterProductionSmoke, "closeShop_btn");
		btn_completeTick = findElementInXLSheet(getParameterProductionSmoke, "completeTick_btn");
		txt_complete = findElementInXLSheet(getParameterProductionSmoke, "complete_txt");
		
		btn_active = findElementInXLSheet(getParameterProductionSmoke, "active_btn");
		btn_delete = findElementInXLSheet(getParameterProductionSmoke, "delete_btn");
		
		txt_rate = findElementInXLSheet(getParameterProductionSmoke, "rate_btn");
		
		txt_overheadInfo = findElementInXLSheet(getParameterProductionSmoke, "overheadInfo_btn");
		btn_new = findElementInXLSheet(getParameterProductionSmoke, "new_btn");
		txt_overheadCode = findElementInXLSheet(getParameterProductionSmoke, "overheadCode_btn");
		txt_descOverhead = findElementInXLSheet(getParameterProductionSmoke, "decOverhead_btn");
		txt_overheadGroup = findElementInXLSheet(getParameterProductionSmoke, "overheadGroup_btn");
		txt_rateType = findElementInXLSheet(getParameterProductionSmoke, "rateType_btn");
		btn_draftOverhead = findElementInXLSheet(getParameterProductionSmoke, "draftOverhead_btn");
		txt_newOverheadCode = findElementInXLSheet(getParameterProductionSmoke, "newOverheadCode_txt");
		
		txt_BOM = findElementInXLSheet(getParameterProductionSmoke, "BOM_txt");
		btn_started = findElementInXLSheet(getParameterProductionSmoke, "started_btn");
		btn_open = findElementInXLSheet(getParameterProductionSmoke, "open_btn");
		
		btn_bulkProductionOrder = findElementInXLSheet(getParameterProductionSmoke, "bulkProductionOrder_btn");
		new_bulkProductionOrder = findElementInXLSheet(getParameterProductionSmoke, "bulkProductionOrdernew_btn");
		txt_descrProductionOrderGroup = findElementInXLSheet(getParameterProductionSmoke, "descrProductionOrderGroup_txt");
		txt_referenceType = findElementInXLSheet(getParameterProductionSmoke, "referenceType_txt");
		
		txt_BulkProduction = findElementInXLSheet(getParameterProductionSmoke, "BulkProduction_txt");
		txt_BulkProductionForm = findElementInXLSheet(getParameterProductionSmoke, "BulkProductionForm_txt");
		
		btn_productionUnitBulk = findElementInXLSheet(getParameterProductionSmoke, "productionUnitBulk_btn");
		txt_productionUnitBulk = findElementInXLSheet(getParameterProductionSmoke, "productionUnitBulk_txt");
		doubleClickProductionUnitBulk = findElementInXLSheet(getParameterProductionSmoke, "doubleClick ProductionUnitBulk");
		
		btn_productTab = findElementInXLSheet(getParameterProductionSmoke, "productTab_btn");
		btn_productSearch = findElementInXLSheet(getParameterProductionSmoke, "productSearch_btn");
		txt_productSearch = findElementInXLSheet(getParameterProductionSmoke, "productSearch_txt");
		doubleClickProductSearch = findElementInXLSheet(getParameterProductionSmoke, "doubleClick ProductSearch");
		btn_productModel = findElementInXLSheet(getParameterProductionSmoke, "productModel_btn");
		txt_productModel = findElementInXLSheet(getParameterProductionSmoke, "productModel_txt");
		doubleClickProductModel = findElementInXLSheet(getParameterProductionSmoke, "doubleClick ProductModel");
		btn_checkOut = findElementInXLSheet(getParameterProductionSmoke, "checkOut_btn");
		
		btn_createInternalOrder = findElementInXLSheet(getParameterProductionSmoke, "createInternalOrder_btn");
		btn_selectAll = findElementInXLSheet(getParameterProductionSmoke, "selectAll_btn");
		
		txt_title = findElementInXLSheet(getParameterProductionSmoke, "title_txt");
		btn_requester = findElementInXLSheet(getParameterProductionSmoke, "requester_btn");
		txt_requester = findElementInXLSheet(getParameterProductionSmoke, "requester_txt");
		doubleClickRequester = findElementInXLSheet(getParameterProductionSmoke, "doubleClick Requester");
		
		
		btn_goTopage = findElementInXLSheet(getParameterProductionSmoke, "goToPage_btn");
		
		btn_resources = findElementInXLSheet(getParameterProductionSmoke, "resources_btn");
		btn_resourceSearch = findElementInXLSheet(getParameterProductionSmoke, "resourceSearch_btn");
		txt_additionalResources = findElementInXLSheet(getParameterProductionSmoke, "additionalResources_btn");
		doubleCLickAdditinalResources = findElementInXLSheet(getParameterProductionSmoke, "doubleCLickAdditinal Resources");
		
		txt_plannedQtyBulk = findElementInXLSheet(getParameterProductionSmoke, "plannedQtyBulk_txt");
		btn_draftProductionOrder = findElementInXLSheet(getParameterProductionSmoke, "draftProductionOrder_btn");
		btn_releaseBulkProductionOrder = findElementInXLSheet(getParameterProductionSmoke, "releaseBulkProductionOrder_btn");
		btn_actionBulk = findElementInXLSheet(getParameterProductionSmoke, "actionBulk_btn");
		
		btn_applyInternal = findElementInXLSheet(getParameterProductionSmoke, "applyInternal_btn");
		txt_shipping = findElementInXLSheet(getParameterProductionSmoke, "shipping_txt");
		btn_productionCosting = findElementInXLSheet(getParameterProductionSmoke, "productionCosting_btn");
		btn_date = findElementInXLSheet(getParameterProductionSmoke, "date_btn");
		btn_input = findElementInXLSheet(getParameterProductionSmoke, "input_btn");
		btn_internalReceipt = findElementInXLSheet(getParameterProductionSmoke, "internalReceipt_btn");
		btn_smallcheckBox = findElementInXLSheet(getParameterProductionSmoke, "smallcheckBox_btn");
		btn_generate = findElementInXLSheet(getParameterProductionSmoke, "generate_btn");
		txt_lotNo = findElementInXLSheet(getParameterProductionSmoke, "lotNo_txt");
		
		txt_bulkOrder = findElementInXLSheet(getParameterProductionSmoke, "bulkOrder_txt");
		bulkOrder = findElementInXLSheet(getParameterProductionSmoke, "bulk Order");
		bulkno = findElementInXLSheet(getParameterProductionSmoke, "bulk no");
		
		txt_standardCost1 = findElementInXLSheet(getParameterProductionSmoke, "standardCost1_txt");
		
		btn_inputProductsTab = findElementInXLSheet(getParameterProductionSmoke, "inputProductsTab_btn");
		btn_searchProduct = findElementInXLSheet(getParameterProductionSmoke, "searchProduct_btn");
		txt_searchProduct = findElementInXLSheet(getParameterProductionSmoke, "searchProduct_txt");
		doubleClickSearchProduct = findElementInXLSheet(getParameterProductionSmoke, "doubleClickSearch Product");
		txt_plannedqty1 = findElementInXLSheet(getParameterProductionSmoke, "plannedqty1_txt");
		
		btn_closeTab = findElementInXLSheet(getParameterProductionSmoke, "closeTab_btn");
		btn_actualDate = findElementInXLSheet(getParameterProductionSmoke, "actualDate_btn");
		
		txt_actualQty1 = findElementInXLSheet(getParameterProductionSmoke, "actualQty1_txt");
		btn_releaseTab = findElementInXLSheet(getParameterProductionSmoke, "releaseTab_btn");
		txt_internalReceipt = findElementInXLSheet(getParameterProductionSmoke, "internalReceipt_txt");
		txt_internalDispatchRelease = findElementInXLSheet(getParameterProductionSmoke, "internalDispatchRelease_txt");
		txt_internalOrderRelease = findElementInXLSheet(getParameterProductionSmoke, "internalOrderRelease_txt");
		txt_BulkOrderRelease = findElementInXLSheet(getParameterProductionSmoke, "BulkOrderRelease_txt");
		
		txt_productionModelRelease = findElementInXLSheet(getParameterProductionSmoke, "productionModelRelease_txt");
		txt_POReleased = findElementInXLSheet(getParameterProductionSmoke, "POReleased_txt");
		txt_internalDispatchOrderReleased = findElementInXLSheet(getParameterProductionSmoke, "internalDispatchOrderReleased_txt");
		txt_Pricing = findElementInXLSheet(getParameterProductionSmoke, "Pricing_txt");
		txt_activateStatus = findElementInXLSheet(getParameterProductionSmoke, "activateStatus_txt");
		txt_openStatus = findElementInXLSheet(getParameterProductionSmoke, "openStatus_txt");
		
		btn_Capture = findElementInXLSheet(getParameterProductionSmoke, "capture_btn");
		btn_productListClick = findElementInXLSheet(getParameterProductionSmoke, "productListClick_btn");
		txt_batchNoTXT = findElementInXLSheet(getParameterProductionSmoke, "batchNoTXT_txt");
		btn_refresh = findElementInXLSheet(getParameterProductionSmoke, "refresh_btn");
		
		btn_internalReceiptControl = findElementInXLSheet(getParameterProductionSmoke, "internalReceiptControl_btn");
		
		link_internalReceipt  = findElementInXLSheet(getParameterProductionSmoke, "internalReceipt_link");
		btn_journalEntry  = findElementInXLSheet(getParameterProductionSmoke, "journalEntry_btn");
		link_gernalEntryDetails = findElementInXLSheet(getParameterProductionSmoke, "gernalEntryDetails_link");
		btn_actionCosting =findElementInXLSheet(getParameterProductionSmoke, "actionCosting_btn");
		btn_generateReceipt = findElementInXLSheet(getParameterProductionSmoke, "generateReceipt_btn"); 
		txt_journalEntryReleased = findElementInXLSheet(getParameterProductionSmoke, "journalEntryReleased_txt");
		btn_actionJournal = findElementInXLSheet(getParameterProductionSmoke, "actionJournal_btn");
		
		txt_BOONo = findElementInXLSheet(getParameterProductionSmoke, "BOONO_txt");
		txt_internalReceiptReleased = findElementInXLSheet(getParameterProductionSmoke, "internalReceiptReleased_txt");
		
		
		
		btn_productInfo = findElementInXLSheet(getParameterProductionSmoke, "productInfo_btn");
		btn_newProduct = findElementInXLSheet(getParameterProductionSmoke, "newProduct_btn");
		txt_productCode = findElementInXLSheet(getParameterProductionSmoke, "productCode_txt");
		txt_productDescription = findElementInXLSheet(getParameterProductionSmoke, "productDescription_txt");
		txt_prodGrop = findElementInXLSheet(getParameterProductionSmoke, "prodGrop_txt");
		txt_basePrice = findElementInXLSheet(getParameterProductionSmoke, "basePrice_txt");
		btn_manufacturer = findElementInXLSheet(getParameterProductionSmoke, "manufacturer_btn");
		txt_manufacturer = findElementInXLSheet(getParameterProductionSmoke, "manufacturer_txt");
		doubleCLickManufacturer = findElementInXLSheet(getParameterProductionSmoke, "doubleCLick Manufacturer");
		txt_UOMGroup = findElementInXLSheet(getParameterProductionSmoke, "UOMGroup_txt");
		txt_UOM = findElementInXLSheet(getParameterProductionSmoke, "UOM_txt");
		txt_length = findElementInXLSheet(getParameterProductionSmoke, "length_txt");
		txt_lengthVal = findElementInXLSheet(getParameterProductionSmoke, "lengthVal_txt");
		txt_width = findElementInXLSheet(getParameterProductionSmoke, "width_txt");
		txt_widthVal = findElementInXLSheet(getParameterProductionSmoke, "widthVal_txt");
		txt_height = findElementInXLSheet(getParameterProductionSmoke, "height_txt");
		txt_heightVal = findElementInXLSheet(getParameterProductionSmoke, "heightVal_txt");
		txt_volume = findElementInXLSheet(getParameterProductionSmoke, "volume_txt");
		txt_VolumeVal = findElementInXLSheet(getParameterProductionSmoke, "volumeVal_txt");
		btn_tabDetails = findElementInXLSheet(getParameterProductionSmoke, "tabDetails_btn");
		btn_allowInventoryWithoutCosting = findElementInXLSheet(getParameterProductionSmoke, "allowInventoryWithoutCosting_btn");
		txt_manufacturingType = findElementInXLSheet(getParameterProductionSmoke, "manufacturingType_txt");
		txt_batchBook = findElementInXLSheet(getParameterProductionSmoke, "batchBook_txt");
		txt_productName = findElementInXLSheet(getParameterProductionSmoke, "productName_txt");
		txt_draftProductionOrder = findElementInXLSheet(getParameterProductionSmoke, "txt_draftProductionOrder");
		
		
		/*New Regression Test Cases*/
		btn_productionorder = findElementInXLSheet(getParameterProductionSmoke, "productionorder_btn");
		btn_edit = findElementInXLSheet(getParameterProductionSmoke, "edit_btn");
		btn_Update = findElementInXLSheet(getParameterProductionSmoke, "Update_btn");
		txt_searchProductionOrder1 = findElementInXLSheet(getParameterProductionSmoke, "searchProductionOrder1_txt");
		btn_duplicate = findElementInXLSheet(getParameterProductionSmoke, "duplicate_btn");
		txt_outputProduct1 = findElementInXLSheet(getParameterProductionSmoke, "outputProduct1_txt");
		txt_outputProduct2 = findElementInXLSheet(getParameterProductionSmoke, "outputProduct2_txt");
		txt_costingPriority1 = findElementInXLSheet(getParameterProductionSmoke, "costingPriority1_txt");
		txt_costingPriority2 = findElementInXLSheet(getParameterProductionSmoke, "costingPriority2_txt");
		txt_prodOrderGroup1 = findElementInXLSheet(getParameterProductionSmoke, "prodOrderGroup1_txt");
		txt_prodOrderGroup2 = findElementInXLSheet(getParameterProductionSmoke, "prodOrderGroup2_txt");
		txt_description1 = findElementInXLSheet(getParameterProductionSmoke, "description1_txt");
		txt_description2 = findElementInXLSheet(getParameterProductionSmoke, "description2_txt");
		txt_prodModel1 = findElementInXLSheet(getParameterProductionSmoke, "prodModel1_txt");
		txt_prodModel2 = findElementInXLSheet(getParameterProductionSmoke, "prodModel2_txt");
		
		
		
		/* QA Form level action verification */
		txt_draftBOM = findElementInXLSheet(getParameterProductionSmoke, "draftBOM_txt");
		txt_editBOM = findElementInXLSheet(getParameterProductionSmoke, "editBOM_txt");
		txt_updateBOM = findElementInXLSheet(getParameterProductionSmoke, "updateBOM_txt");
		txt_duplicateBOM = findElementInXLSheet(getParameterProductionSmoke, "duplicateBOM_txt");
		btn_draftNNew = findElementInXLSheet(getParameterProductionSmoke, "draftNNew_btn");
		txt_draftNNewBOM = findElementInXLSheet(getParameterProductionSmoke, "draftNNewBOM_txt");
		btn_copyFrom = findElementInXLSheet(getParameterProductionSmoke, "copyFrom_btn");
		txt_BOm = findElementInXLSheet(getParameterProductionSmoke, "Bom_txt");
		txt_previousBOM = findElementInXLSheet(getParameterProductionSmoke, "previousBOM_txt");
		doublePreviousBOM = findElementInXLSheet(getParameterProductionSmoke, "doublePrevious BOM");
		txt_num = findElementInXLSheet(getParameterProductionSmoke, "num_txt");
		btn_hold = findElementInXLSheet(getParameterProductionSmoke, "hold_btn");
		txt_changeStatus = findElementInXLSheet(getParameterProductionSmoke, "changeStatus_txt");
		txt_reason = findElementInXLSheet(getParameterProductionSmoke, "reason_txt");
		btn_ok = findElementInXLSheet(getParameterProductionSmoke, "ok_btn");
		btn_unHold = findElementInXLSheet(getParameterProductionSmoke, "unHold_btn");
		btn_newVersion = findElementInXLSheet(getParameterProductionSmoke, "newVersion_btn");
		txt_validMsg = findElementInXLSheet(getParameterProductionSmoke, "validMsg_txt");
		btn_reminder = findElementInXLSheet(getParameterProductionSmoke, "btn_reminder");
		txt_createReminder = findElementInXLSheet(getParameterProductionSmoke, "txt_createReminder");
		btn_history = findElementInXLSheet(getParameterProductionSmoke, "btn_history");
		txt_history = findElementInXLSheet(getParameterProductionSmoke, "txt_history");
		btn_reverse = findElementInXLSheet(getParameterProductionSmoke, "btn_reverse");
		txt_reverse = findElementInXLSheet(getParameterProductionSmoke, "txt_reverse");
		btn_updateReminder = findElementInXLSheet(getParameterProductionSmoke, "updateReminder_btn");
		txt_delete = findElementInXLSheet(getParameterProductionSmoke, "txt_delete");
		btn_Delete = findElementInXLSheet(getParameterProductionSmoke, "Delete_btn");
		btn_updateNNew = findElementInXLSheet(getParameterProductionSmoke, "updateNnew_btn");
		txt_updateNNewBOM = findElementInXLSheet(getParameterProductionSmoke, "updateNNew_txt"); 
		txt_previousBOO = findElementInXLSheet(getParameterProductionSmoke, "previousBOO_txt");
		doublePreviousBOO = findElementInXLSheet(getParameterProductionSmoke, "doublePrevious BOO");
		txt_duplicateBOO = findElementInXLSheet(getParameterProductionSmoke, "duplicateBOO_txt");
		txt_BOOAction = findElementInXLSheet(getParameterProductionSmoke, "BOOAction_txt");
		btn_activities = findElementInXLSheet(getParameterProductionSmoke, "btn_activities");
		txt_activity = findElementInXLSheet(getParameterProductionSmoke, "txt_activity");
		txt_subject = findElementInXLSheet(getParameterProductionSmoke, "txt_subject");
		btn_assignTo = findElementInXLSheet(getParameterProductionSmoke, "btn_assignTo");
		txt_AssignTo = findElementInXLSheet(getParameterProductionSmoke, "txt_AssignTo");
		doubleAssignTo = findElementInXLSheet(getParameterProductionSmoke, "doubleAssignTo");
		btn_UpdateTask = findElementInXLSheet(getParameterProductionSmoke, "btn_UpdateTask");
		txt_Boo = findElementInXLSheet(getParameterProductionSmoke, "txt_Boo");
		txt_productionModelCode = findElementInXLSheet(getParameterProductionSmoke, "productionModelCode_txt");
		txt_previousmodel = findElementInXLSheet(getParameterProductionSmoke, "txt_previousmodel");
		doublePreviousmodel = findElementInXLSheet(getParameterProductionSmoke, "doublePreviousmodel");
		txt_productionModelTab = findElementInXLSheet(getParameterProductionSmoke, "txt_productionModelTab");
		txt_BillOfMaterial = findElementInXLSheet(getParameterProductionSmoke, "txt_BillOfMaterial");
		tab_summary = findElementInXLSheet(getParameterProductionSmoke, "tab_summary");
		txt_plannedQty1 = findElementInXLSheet(getParameterProductionSmoke, "txt_plannedQty1");
		txt_priority = findElementInXLSheet(getParameterProductionSmoke, "txt_priority");
		txt_previousproductionOrder = findElementInXLSheet(getParameterProductionSmoke, "txt_previousproductionOrder");
		doublePreviousProductionOrder = findElementInXLSheet(getParameterProductionSmoke, "doublePreviousProductionOrder");
		txt_productionOrderTab = findElementInXLSheet(getParameterProductionSmoke, "txt_productionOrderTab");
		btn_okHold = findElementInXLSheet(getParameterProductionSmoke, "btn_okHold"); 
		btn_jobTechnicalDetail = findElementInXLSheet(getParameterProductionSmoke, "btn_jobTechnicalDetail");
		btn_dockFlow = findElementInXLSheet(getParameterProductionSmoke, "btn_dockFlow");
		txt_dockFlow = findElementInXLSheet(getParameterProductionSmoke, "txt_dockFlow");
		btn_generatePackinOrder = findElementInXLSheet(getParameterProductionSmoke, "btn_generatePackinOrder");
		txt_generatePackingOrder = findElementInXLSheet(getParameterProductionSmoke, "txt_generatePackingOrder");
		btn_costEstimation = findElementInXLSheet(getParameterProductionSmoke, "btn_costEstimation");
		txt_costEstimation = findElementInXLSheet(getParameterProductionSmoke, "txt_costEstimation");
		txt_prodControl = findElementInXLSheet(getParameterProductionSmoke, "txt_prodControl");
		txt_internalDispatchOrderdrafted = findElementInXLSheet(getParameterProductionSmoke, "txt_internalDispatchOrderdrafted");
		btn_editInternal = findElementInXLSheet(getParameterProductionSmoke, "btn_editInternal");
		txt_release = findElementInXLSheet(getParameterProductionSmoke, "txt_release");
		txt_update = findElementInXLSheet(getParameterProductionSmoke, "txt_update");
		btn_datenew = findElementInXLSheet(getParameterProductionSmoke, "btn_datenew");
		btn_reverseClick = findElementInXLSheet(getParameterProductionSmoke, "btn_reverseClick");
		txt_date = findElementInXLSheet(getParameterProductionSmoke, "txt_date");
		txt_updateNNew = findElementInXLSheet(getParameterProductionSmoke, "txt_updateNNew");
		btn_products = findElementInXLSheet(getParameterProductionSmoke, "btn_products");
		txt_previousBulkOrder = findElementInXLSheet(getParameterProductionSmoke, "txt_previousBulkOrder");
		doublePreviousbulkOrder = findElementInXLSheet(getParameterProductionSmoke, "doublePreviousbulkOrder");
		txt_bulkproductionorderTab = findElementInXLSheet(getParameterProductionSmoke, "txt_bulkproductionorderTab");
		btn_InternalOrder = findElementInXLSheet(getParameterProductionSmoke, "btn_InternalOrder");
		txt_InternalOrder = findElementInXLSheet(getParameterProductionSmoke, "txt_InternalOrder");
		txt_title1 = findElementInXLSheet(getParameterProductionSmoke, "txt_title1");
		btn_datenew1 = findElementInXLSheet(getParameterProductionSmoke, "btn_datenew1");
		btn_report = findElementInXLSheet(getParameterProductionSmoke, "btn_report");
		btn_viewReport = findElementInXLSheet(getParameterProductionSmoke, "btn_viewReport"); 
		txt_viewReport = findElementInXLSheet(getParameterProductionSmoke, "txt_viewReport");
		txt_active = findElementInXLSheet(getParameterProductionSmoke, "txt_active");
		btn_inactive = findElementInXLSheet(getParameterProductionSmoke, "btn_inactive");
		txt_inactive = findElementInXLSheet(getParameterProductionSmoke, "txt_inactive");
		btn_yes1 = findElementInXLSheet(getParameterProductionSmoke, "btn_yes1");
		btn_yes2 = findElementInXLSheet(getParameterProductionSmoke, "btn_yes2");
		txt_new = findElementInXLSheet(getParameterProductionSmoke, "txt_new");
		txt_deleteAc = findElementInXLSheet(getParameterProductionSmoke, "txt_deleteAc");
		btn_deleteAc = findElementInXLSheet(getParameterProductionSmoke, "btn_deleteAc");
		btn_yes4 = findElementInXLSheet(getParameterProductionSmoke, "btn_yes4");
		txt_updateOrganization = findElementInXLSheet(getParameterProductionSmoke, "txt_updateOrganization");
		txt_edit = findElementInXLSheet(getParameterProductionSmoke, "txt_edit");
		txt_search = findElementInXLSheet(getParameterProductionSmoke, "txt_search");
		txt_reset = findElementInXLSheet(getParameterProductionSmoke, "txt_reset");
		btn_reset = findElementInXLSheet(getParameterProductionSmoke, "btn_reset");
		txt_internalReceiptLabel = findElementInXLSheet(getParameterProductionSmoke, "txt_internalReceiptLabel");
		btn_productionParameter = findElementInXLSheet(getParameterProductionSmoke, "btn_productionParameter");
		btn_inactivePP = findElementInXLSheet(getParameterProductionSmoke, "btn_inactivePP");
		btn_UPDATE = findElementInXLSheet(getParameterProductionSmoke, "btn_UPDATE");
		txt_UPDATE = findElementInXLSheet(getParameterProductionSmoke, "txt_UPDATE");
		btn_productcostEstimation = findElementInXLSheet(getParameterProductionSmoke, "btn_productcostEstimation");
		btn_newCostEstimation = findElementInXLSheet(getParameterProductionSmoke, "btn_newCostEstimation");
		txt_costEstDescription = findElementInXLSheet(getParameterProductionSmoke, "txt_costEstDescription");
		btn_costProduct = findElementInXLSheet(getParameterProductionSmoke, "btn_costProduct");
		txt_costProduct = findElementInXLSheet(getParameterProductionSmoke, "txt_costProduct");
		costProductDoubleClick = findElementInXLSheet(getParameterProductionSmoke, "costProductDoubleClick");
		txt_costEstQuantity = findElementInXLSheet(getParameterProductionSmoke, "txt_costEstQuantity");
		btn_pricingProfileCost = findElementInXLSheet(getParameterProductionSmoke, "btn_pricingProfileCost");
		txt_pricingProfileCost = findElementInXLSheet(getParameterProductionSmoke, "txt_pricingProfileCost");
		pricingProfileCostDoubleClick = findElementInXLSheet(getParameterProductionSmoke, "pricingProfileCostDoubleClick");
		btn_checkout = findElementInXLSheet(getParameterProductionSmoke, "btn_checkout");
		
		/* Smoke_Production Remade */
		/*Smoke_Production_0001_1*/
		btn_navigationPane = findElementInXLSheet(getParameterProductionSmoke, "btn_navigationPane");
		btn_inventoryAndWarehouse = findElementInXLSheet(getParameterProductionSmoke, "btn_inventoryAndWarehouse");
		btn_billOfMaterials = findElementInXLSheet(getParameterProductionSmoke, "btn_billOfMaterials");
		header_newBillOfMaterial = findElementInXLSheet(getParameterProductionSmoke, "header_newBillOfMaterial");
		header_BillOfMaterailByPage = findElementInXLSheet(getParameterProductionSmoke, "header_BillOfMaterailByPage");
		btn_newBillOfMaterial = findElementInXLSheet(getParameterProductionSmoke, "btn_newBillOfMaterial");
		drop_productGroup = findElementInXLSheet(getParameterProductionSmoke, "drop_productGroup");
		btn_lookupOutputProduct = findElementInXLSheet(getParameterProductionSmoke, "btn_lookupOutputProduct");
		lnk_inforOnLookupInformationReplace = findElementInXLSheet(getParameterProductionSmoke, "lnk_inforOnLookupInformationReplace");
		txt_productFrontPageBOM = findElementInXLSheet(getParameterProductionSmoke, "txt_productFrontPageBOM");
		txt_quantityBOM = findElementInXLSheet(getParameterProductionSmoke, "txt_quantityBOM");
		btn_lookupOnLineBOMRowReplace = findElementInXLSheet(getParameterProductionSmoke, "btn_lookupOnLineBOMRowReplace");
		btn_addNewRecord = findElementInXLSheet(getParameterProductionSmoke, "btn_addNewRecord");
		txt_qtyBOMInputProductLinesRowReplace = findElementInXLSheet(getParameterProductionSmoke, "txt_qtyBOMInputProductLinesRowReplace");
		header_draftedBOM = findElementInXLSheet(getParameterProductionSmoke, "header_draftedBOM");
		btn_releaseCommon = findElementInXLSheet(getParameterProductionSmoke, "btn_releaseCommon");
		header_releasedBillOfMaterial = findElementInXLSheet(getParameterProductionSmoke, "header_releasedBillOfMaterial");
		lbl_docNumber = findElementInXLSheet(getParameterProductionSmoke, "lbl_docNumber");
		div_loginVerification = findElementInXLSheet(getParameterProductionSmoke, "div_loginVerification");
		
		/* Smoke_Production_0001_2 */
		txt_booCodeBillOfOperation = findElementInXLSheet(getParameterProductionSmoke, "txt_booCodeBillOfOperation");
		drop_elementCategoryBOO = findElementInXLSheet(getParameterProductionSmoke, "drop_elementCategoryBOO");
		drop_activityTypeBOO = findElementInXLSheet(getParameterProductionSmoke, "drop_activityTypeBOO");
		txt_elementDescriptionBOO = findElementInXLSheet(getParameterProductionSmoke, "txt_elementDescriptionBOO");
		btn_applyOperationOneBOO = findElementInXLSheet(getParameterProductionSmoke, "btn_applyOperationOneBOO");
		txt_groupIdBOO = findElementInXLSheet(getParameterProductionSmoke, "txt_groupIdBOO");
		drop_batchMethodBOO = findElementInXLSheet(getParameterProductionSmoke, "drop_batchMethodBOO");
		chk_finalInvestigationBOO = findElementInXLSheet(getParameterProductionSmoke, "chk_finalInvestigationBOO");
		header_draftedBOO = findElementInXLSheet(getParameterProductionSmoke, "header_draftedBOO");
		header_releasedBOO = findElementInXLSheet(getParameterProductionSmoke, "header_releasedBOO");
		btn_productionModule = findElementInXLSheet(getParameterProductionSmoke, "btn_productionModule");
		btn_billOfOperation = findElementInXLSheet(getParameterProductionSmoke, "btn_billOfOperation");
		header_newBillOfOperation = findElementInXLSheet(getParameterProductionSmoke, "header_newBillOfOperation");
		header_BillOfOperationByPage = findElementInXLSheet(getParameterProductionSmoke, "header_BillOfOperationByPage");
		btn_newBillOfOperation = findElementInXLSheet(getParameterProductionSmoke, "btn_newBillOfOperation");
		btn_addBillOfOperationRowReplace = findElementInXLSheet(getParameterProductionSmoke, "btn_addBillOfOperationRowReplace");
		
		/* Smoke_Production_0001_3 */
		btn_productionModelRebuilt = findElementInXLSheet(getParameterProductionSmoke, "btn_productionModelRebuilt");
		header_newProductionModel = findElementInXLSheet(getParameterProductionSmoke, "header_newProductionModel");
		header_productionModelByPage = findElementInXLSheet(getParameterProductionSmoke, "header_productionModelByPage");
		btn_newProductionModelRebult = findElementInXLSheet(getParameterProductionSmoke, "btn_newProductionModelRebult");
		txt_modelCodePM = findElementInXLSheet(getParameterProductionSmoke, "txt_modelCodePM");
		btn_productionUnitLookup = findElementInXLSheet(getParameterProductionSmoke, "btn_productionUnitLookup");
		txt_prductionUnitSearch = findElementInXLSheet(getParameterProductionSmoke, "txt_prductionUnitSearch");
		txt_produxtProductionModelFrontPage = findElementInXLSheet(getParameterProductionSmoke, "txt_produxtProductionModelFrontPage");
		drop_sitePM = findElementInXLSheet(getParameterProductionSmoke, "drop_sitePM");
		drop_bomPM = findElementInXLSheet(getParameterProductionSmoke, "drop_bomPM");
		txt_batchQuantityPM = findElementInXLSheet(getParameterProductionSmoke, "txt_batchQuantityPM");
		drop_barcodeBookPM = findElementInXLSheet(getParameterProductionSmoke, "drop_barcodeBookPM");
		drop_costinPriority = findElementInXLSheet(getParameterProductionSmoke, "drop_costinPriority");
		txt_searchPricingProfile = findElementInXLSheet(getParameterProductionSmoke, "txt_searchPricingProfile");
		btn_pricingProfileLookup = findElementInXLSheet(getParameterProductionSmoke, "btn_pricingProfileLookup");
		txt_pricingProfileProntPage = findElementInXLSheet(getParameterProductionSmoke, "txt_pricingProfileProntPage");
		chk_mrpPM = findElementInXLSheet(getParameterProductionSmoke, "chk_mrpPM");
		btn_billOfOperationLookup = findElementInXLSheet(getParameterProductionSmoke, "btn_billOfOperationLookup");
		txt_searchBillOfOperation = findElementInXLSheet(getParameterProductionSmoke, "txt_searchBillOfOperation");
		txt_frontPageBOOPM = findElementInXLSheet(getParameterProductionSmoke, "txt_frontPageBOOPM");
		btn_expandBOOPM = findElementInXLSheet(getParameterProductionSmoke, "btn_expandBOOPM");
		btn_editBillOfOperationPMRowReplace = findElementInXLSheet(getParameterProductionSmoke, "btn_editBillOfOperationPMRowReplace");
		chk_inputProductsRowReplaceBOOPM = findElementInXLSheet(getParameterProductionSmoke, "chk_inputProductsRowReplaceBOOPM");
		drop_processTimeTypeBOOPM = findElementInXLSheet(getParameterProductionSmoke, "drop_processTimeTypeBOOPM");
		btn_inputProductsTabBOOPM = findElementInXLSheet(getParameterProductionSmoke, "btn_inputProductsTabBOOPM");
		btn_applyBOOPM = findElementInXLSheet(getParameterProductionSmoke, "btn_applyBOOPM");
		header_draftedPM = findElementInXLSheet(getParameterProductionSmoke, "header_draftedPM");
		header_releasedPM = findElementInXLSheet(getParameterProductionSmoke, "header_releasedPM");
		txt_productionUnitFrontPagePM = findElementInXLSheet(getParameterProductionSmoke, "txt_productionUnitFrontPagePM");
		btn_yesClearConfirmationPM = findElementInXLSheet(getParameterProductionSmoke, "btn_yesClearConfirmationPM");
		btn_tabBillOfOperationPM = findElementInXLSheet(getParameterProductionSmoke, "btn_tabBillOfOperationPM");
		btn_okClearMsgBOOPM = findElementInXLSheet(getParameterProductionSmoke, "btn_okClearMsgBOOPM");
		
		/* Smoke_Production_0002 */
		btn_productionOrder2 = findElementInXLSheet(getParameterProductionSmoke, "btn_productionOrder2");
		header_newProductionOrder = findElementInXLSheet(getParameterProductionSmoke, "header_newProductionOrder");
		header_productionOrderByPage = findElementInXLSheet(getParameterProductionSmoke, "header_productionOrderByPage");
		btn_newProductionOrder2 = findElementInXLSheet(getParameterProductionSmoke, "btn_newProductionOrder2");
		drop_productionOrderGroupPO = findElementInXLSheet(getParameterProductionSmoke, "drop_productionOrderGroupPO");
		txt_frontPageOutputProductPO = findElementInXLSheet(getParameterProductionSmoke, "txt_frontPageOutputProductPO");
		btn_lookupOutputProductSpan = findElementInXLSheet(getParameterProductionSmoke, "btn_lookupOutputProductSpan");
		txt_planedQuantityPO = findElementInXLSheet(getParameterProductionSmoke, "txt_planedQuantityPO");
		header_draftedPO = findElementInXLSheet(getParameterProductionSmoke, "header_draftedPO");
		header_releasedPO = findElementInXLSheet(getParameterProductionSmoke, "header_releasedPO");
		txt_descriptionPO = findElementInXLSheet(getParameterProductionSmoke, "txt_descriptionPO");
		btn_productionModuleLookup = findElementInXLSheet(getParameterProductionSmoke, "btn_productionModuleLookup");
		txt_productionModuleFrontPagePO = findElementInXLSheet(getParameterProductionSmoke, "txt_productionModuleFrontPagePO");
		txt_searchProductionModule = findElementInXLSheet(getParameterProductionSmoke, "txt_searchProductionModule");
		
		/* Smoke_Production_0003 */
		btn_productionControl = findElementInXLSheet(getParameterProductionSmoke, "btn_productionControl");
		header_productionControlByPage = findElementInXLSheet(getParameterProductionSmoke, "header_productionControlByPage");
		chk_mrpPODoRepalce = findElementInXLSheet(getParameterProductionSmoke, "chk_mrpPODoRepalce");
		btn_actionMRP = findElementInXLSheet(getParameterProductionSmoke, "btn_actionMRP");
		btn_runMRP = findElementInXLSheet(getParameterProductionSmoke, "btn_runMRP");
		btn_runMRPWindow = findElementInXLSheet(getParameterProductionSmoke, "btn_runMRPWindow");
		btn_releaseMRP = findElementInXLSheet(getParameterProductionSmoke, "btn_releaseMRP");
		para_MRPReleaseValidation = findElementInXLSheet(getParameterProductionSmoke, "para_MRPReleaseValidation");
		btn_cloaseMRPWindow = findElementInXLSheet(getParameterProductionSmoke, "btn_cloaseMRPWindow");
		lbl_genaratedInternalOrderMRP = findElementInXLSheet(getParameterProductionSmoke, "lbl_genaratedInternalOrderMRP");
		btn_internalDispatchOrderTaskAndEvent = findElementInXLSheet(getParameterProductionSmoke, "btn_internalDispatchOrderTaskAndEvent");
		btn_pendingIDODocRepalce = findElementInXLSheet(getParameterProductionSmoke, "btn_pendingIDODocRepalce");
		header_draftedIDO = findElementInXLSheet(getParameterProductionSmoke, "header_draftedIDO");
		header_releasedIDO = findElementInXLSheet(getParameterProductionSmoke, "header_releasedIDO");
		header_newIDO = findElementInXLSheet(getParameterProductionSmoke, "header_newIDO");
		header_productionControlWindow = findElementInXLSheet(getParameterProductionSmoke, "header_productionControlWindow");
		btn_homeLink = findElementInXLSheet(getParameterProductionSmoke, "btn_homeLink");
		btn_taskEvent2 = findElementInXLSheet(getParameterProductionSmoke, "btn_taskEvent2");
		btn_logTabProductionControlWindow = findElementInXLSheet(getParameterProductionSmoke, "btn_logTabProductionControlWindow");
		txt_shippingAddressIDO = findElementInXLSheet(getParameterProductionSmoke, "txt_shippingAddressIDO");
		
		/* Smoke_Production_0004 */
		btn_action2 = findElementInXLSheet(getParameterProductionSmoke, "btn_action2");
		btn_shopFloorUpdate2 = findElementInXLSheet(getParameterProductionSmoke, "btn_shopFloorUpdate2");
		header_shopFloorUpdateWindow = findElementInXLSheet(getParameterProductionSmoke, "header_shopFloorUpdateWindow");
		btn_startFirstOperationSFU = findElementInXLSheet(getParameterProductionSmoke, "btn_startFirstOperationSFU");
		btn_startSecondLeveleSFU = findElementInXLSheet(getParameterProductionSmoke, "btn_startSecondLeveleSFU");
		btn_actualUpdateSFU = findElementInXLSheet(getParameterProductionSmoke, "btn_actualUpdateSFU");
		txt_outoutQuantityACtualUpdateSFU = findElementInXLSheet(getParameterProductionSmoke, "txt_outoutQuantityACtualUpdateSFU");
		btn_tapInputProductsShopFloorUpdate = findElementInXLSheet(getParameterProductionSmoke, "btn_tapInputProductsShopFloorUpdate");
		txt_actualUpdaInputProductQuantityFirstOperation = findElementInXLSheet(getParameterProductionSmoke, "txt_actualUpdaInputProductQuantityFirstOperation");
		btn_upateActualSFU = findElementInXLSheet(getParameterProductionSmoke, "btn_upateActualSFU");
		para_successfullValidationActualUpdate = findElementInXLSheet(getParameterProductionSmoke, "para_successfullValidationActualUpdate");
		btn_closeFisrstOperationActualUpdateWindow = findElementInXLSheet(getParameterProductionSmoke, "btn_closeFisrstOperationActualUpdateWindow");
		btn_completeFirstOperation = findElementInXLSheet(getParameterProductionSmoke, "btn_completeFirstOperation");
		btn_completeSecondLeveleSFU = findElementInXLSheet(getParameterProductionSmoke, "btn_completeSecondLeveleSFU");
		lbl_completedShopFloorUpdateBarFirstOperation = findElementInXLSheet(getParameterProductionSmoke, "lbl_completedShopFloorUpdateBarFirstOperation");
		btn_startSecondOperationSFU = findElementInXLSheet(getParameterProductionSmoke, "btn_startSecondOperationSFU");
		txt_actualUpdaInputProductQuantitySecondOperation = findElementInXLSheet(getParameterProductionSmoke, "txt_actualUpdaInputProductQuantitySecondOperation");
		btn_closeSecondOperationActualUpdateWindow = findElementInXLSheet(getParameterProductionSmoke, "btn_closeSecondOperationActualUpdateWindow");
		btn_completeSecondOperation = findElementInXLSheet(getParameterProductionSmoke, "btn_completeSecondOperation");
		lbl_completedShopFloorUpdateBarSecondOperation = findElementInXLSheet(getParameterProductionSmoke, "lbl_completedShopFloorUpdateBarSecondOperation");
		para_successfullValidationOutProductActualUpdate = findElementInXLSheet(getParameterProductionSmoke, "para_successfullValidationOutProductActualUpdate");
		
		/* Smoke_Production_0005 */
		div_selectedQcDocProduction = findElementInXLSheet(getParameterProductionSmoke, "div_selectedQcDocProduction");
		chk_qcProductionAccordingToProductReplaceProduct = findElementInXLSheet(getParameterProductionSmoke, "chk_qcProductionAccordingToProductReplaceProduct");
		drop_documentTypeProductionQC = findElementInXLSheet(getParameterProductionSmoke, "drop_documentTypeProductionQC");
		txt_passQuantityProductionQCOneRow = findElementInXLSheet(getParameterProductionSmoke, "txt_passQuantityProductionQCOneRow");
		txt_failQuantityProductionQCOneRow = findElementInXLSheet(getParameterProductionSmoke, "txt_failQuantityProductionQCOneRow");
		btn_releseProductionQC = findElementInXLSheet(getParameterProductionSmoke, "btn_releseProductionQC");
		para_successfullValidationProductionQC = findElementInXLSheet(getParameterProductionSmoke, "para_successfullValidationProductionQC");
		txt_docNumberQC = findElementInXLSheet(getParameterProductionSmoke, "txt_docNumberQC");
		btn_productionQCAndSorting = findElementInXLSheet(getParameterProductionSmoke, "btn_productionQCAndSorting");
		
		/* Smoke_Production_0006 */
		btn_productionCostingActions = findElementInXLSheet(getParameterProductionSmoke, "btn_productionCostingActions");
		header_productionCostingWindow = findElementInXLSheet(getParameterProductionSmoke, "header_productionCostingWindow");
		btn_actualUpdateProductionCosting = findElementInXLSheet(getParameterProductionSmoke, "btn_actualUpdateProductionCosting");
		tab_inputProductsProductionCosting = findElementInXLSheet(getParameterProductionSmoke, "tab_inputProductsProductionCosting");
		txt_actualUpdateProductionCosting = findElementInXLSheet(getParameterProductionSmoke, "txt_actualUpdateProductionCosting");
		btn_updateProductionCosting = findElementInXLSheet(getParameterProductionSmoke, "btn_updateProductionCosting");
		para_successfullValidationActualUpdateProductionCosting = findElementInXLSheet(getParameterProductionSmoke, "para_successfullValidationActualUpdateProductionCosting");
		chk_pendingProductionCosting = findElementInXLSheet(getParameterProductionSmoke, "chk_pendingProductionCosting");
		chk_pendingProductionCostingSelected = findElementInXLSheet(getParameterProductionSmoke, "chk_pendingProductionCostingSelected");
		btn_updateProductionCostingMainWindow = findElementInXLSheet(getParameterProductionSmoke, "btn_updateProductionCostingMainWindow");
		para_batchCostingUpdateValidation = findElementInXLSheet(getParameterProductionSmoke, "para_batchCostingUpdateValidation");
		header_completedPO = findElementInXLSheet(getParameterProductionSmoke, "header_completedPO");
		btn_closeSecondWindowCostingUpdate = findElementInXLSheet(getParameterProductionSmoke, "btn_closeSecondWindowCostingUpdate");
		btn_closeMainCostingUpdateWindowProductionOrder = findElementInXLSheet(getParameterProductionSmoke, "btn_closeMainCostingUpdateWindowProductionOrder");
		
		/* Smoke_Production_0007 */
		btn_convertToInternalReceipt = findElementInXLSheet(getParameterProductionSmoke, "btn_convertToInternalReceipt");
		header_genarateInternalReciptSmoke = findElementInXLSheet(getParameterProductionSmoke, "header_genarateInternalReciptSmoke");
		chk_seletcOroductForInternalRecipt = findElementInXLSheet(getParameterProductionSmoke, "chk_seletcOroductForInternalRecipt");
		btn_genarateInternalRecipt = findElementInXLSheet(getParameterProductionSmoke, "btn_genarateInternalRecipt");
		para_validationInternalREciptGenarateSuccessfull = findElementInXLSheet(getParameterProductionSmoke, "para_validationInternalREciptGenarateSuccessfull");
		lnk_genaratedInternalREciptSmoke = findElementInXLSheet(getParameterProductionSmoke, "lnk_genaratedInternalREciptSmoke");
		header_releasedInternalRecipt = findElementInXLSheet(getParameterProductionSmoke, "header_releasedInternalRecipt");
		
		/* Smoke_Production_0008 */
		btn_journal = findElementInXLSheet(getParameterProductionSmoke, "btn_journal");
		header_journalEntryPopup = findElementInXLSheet(getParameterProductionSmoke, "header_journalEntryPopup");
		lbl_debitValueJournelEntrySmoke = findElementInXLSheet(getParameterProductionSmoke, "lbl_debitValueJournelEntrySmoke");
		lbl_creditValueJournelEntrySmoke = findElementInXLSheet(getParameterProductionSmoke, "lbl_creditValueJournelEntrySmoke");
		lbl_debitTotalJournelEntrySingleEntry = findElementInXLSheet(getParameterProductionSmoke, "lbl_debitTotalJournelEntrySingleEntry");
		lbl_creditTotalJournelEntrySingleEntry = findElementInXLSheet(getParameterProductionSmoke, "lbl_creditTotalJournelEntrySingleEntry");
		
	}

	public static void readData() throws Exception {
		String computerName = System.getenv().get("COMPUTERNAME");
		if (computerName.equals("DESKTOP-C6E8CJU") || computerName.equals("MADHUSHAN-LAPdf")) {
			siteURL = findElementInXLSheet(getDataProductionSmoke, "site url external");
		} else {
			siteURL = findElementInXLSheet(getDataProductionSmoke, "site url");
		}
		
		UserNameData = findElementInXLSheet(getDataProductionSmoke, "user name data");
		PasswordData = findElementInXLSheet(getDataProductionSmoke, "password data");
		
		descData = findElementInXLSheet(getDataProductionSmoke, "desc data");
		productGrpData = findElementInXLSheet(getDataProductionSmoke, "productGrp data");
		BOMQuantityData = findElementInXLSheet(getDataProductionSmoke, "BOMQuantity data");
		rawQuantityData = findElementInXLSheet(getDataProductionSmoke, "rawQuantity data");
		rawMaterialData = findElementInXLSheet(getDataProductionSmoke, "rawMaterial data");
		outputProductData = findElementInXLSheet(getDataProductionSmoke, "outputProduct data");
		BOOCodeData = findElementInXLSheet(getDataProductionSmoke, "BOOCode data");
		BOODescrData = findElementInXLSheet(getDataProductionSmoke, "BOODescr data");
		outputProductData = findElementInXLSheet(getDataProductionSmoke, "outputProduct data");
		ElementCategoryData = findElementInXLSheet(getDataProductionSmoke, "ElementCategory data");
		ElementDescData = findElementInXLSheet(getDataProductionSmoke, "ElementDesc data");
		
		ElementCategoryDataOperation = findElementInXLSheet(getDataProductionSmoke, "ElementCategoryOperation data");
		ElementDescDataOperation = findElementInXLSheet(getDataProductionSmoke, "ElementDescOperation data");
		GroupIdData = findElementInXLSheet(getDataProductionSmoke, "GroupId data");
		GroupIdData2 = findElementInXLSheet(getDataProductionSmoke, "GroupId2 data");
		GroupIdDataQC = findElementInXLSheet(getDataProductionSmoke, "GroupIdData QC");
		GroupIdData1 = findElementInXLSheet(getDataProductionSmoke, "GroupId1 data");
		GroupIdData4 = findElementInXLSheet(getDataProductionSmoke, "GroupId4 data");
		
		modelCodeData = findElementInXLSheet(getDataProductionSmoke, "modelCode data");
		productGroupData = findElementInXLSheet(getDataProductionSmoke, "productGroup data");
		ProductionUnitData = findElementInXLSheet(getDataProductionSmoke, "productionUnit data");
		
		batchQtyData = findElementInXLSheet(getDataProductionSmoke, "batchQty data");
		standardCostData = findElementInXLSheet(getDataProductionSmoke, "standardCost data");
		PricingProfileData = findElementInXLSheet(getDataProductionSmoke, "PricingProfile data");
		
		BOOOperationData = findElementInXLSheet(getDataProductionSmoke, "BOOOperation data");
		productData = findElementInXLSheet(getDataProductionSmoke, "product data");
		plannedqtyData = findElementInXLSheet(getDataProductionSmoke, "plannedQty data");
		
		productionOrderDescData = findElementInXLSheet(getDataProductionSmoke, "productionOrderDesc data");
		productionOrderModelData = findElementInXLSheet(getDataProductionSmoke, "productionOrderModel data");
		
		shippingAddressData = findElementInXLSheet(getDataProductionSmoke, "shippingAddress data");
		
		plannedQtyData = findElementInXLSheet(getDataProductionSmoke, "plannedQty data1");
		BatchNoData = findElementInXLSheet(getDataProductionSmoke, "BatchNo data");
		
		producedData = findElementInXLSheet(getDataProductionSmoke, "produced data");
		actualQtyData = findElementInXLSheet(getDataProductionSmoke, "actualQty data");
		
		productionUnitData = findElementInXLSheet(getDataProductionSmoke, "productionUnit data");
		
		locationCodeData = findElementInXLSheet(getDataProductionSmoke, "locationCode data");
		descriptionData = findElementInXLSheet(getDataProductionSmoke, "description data");
		inputwareData = findElementInXLSheet(getDataProductionSmoke, "inputware data");
		outputwareData = findElementInXLSheet(getDataProductionSmoke, "outputware data");
		WIPwareData = findElementInXLSheet(getDataProductionSmoke, "WIPware data");
		
		overheadData = findElementInXLSheet(getDataProductionSmoke, "overhead data");
		
		pricingProfileCodeData = findElementInXLSheet(getDataProductionSmoke, "pricingProfileCode data");
		descPricingProfileData = findElementInXLSheet(getDataProductionSmoke, "descPricingProfile data");
		
		rateData = findElementInXLSheet(getDataProductionSmoke, "rate data");
		
		overheadCodeData = findElementInXLSheet(getDataProductionSmoke, "overheadCode data");
		overheadDescData = findElementInXLSheet(getDataProductionSmoke, "overheadDesc data");
		
		productionOrderGroupData = findElementInXLSheet(getDataProductionSmoke, "productionOrderGroup data");
		
		productionUnitBulkData = findElementInXLSheet(getDataProductionSmoke, "productionUnitBulk data");
		productSearchData = findElementInXLSheet(getDataProductionSmoke, "productSearch Data");
		productModel = findElementInXLSheet(getDataProductionSmoke, "productModel data");
		
		titleData = findElementInXLSheet(getDataProductionSmoke, "title data");
		requesterData = findElementInXLSheet(getDataProductionSmoke, "requester data");
		
		additionalResourcesData = findElementInXLSheet(getDataProductionSmoke, "additionalResources Data");
		
		plannedqtyBulkData = findElementInXLSheet(getDataProductionSmoke, "plannedqtyBulk Data");
		shippingData = findElementInXLSheet(getDataProductionSmoke, "shipping data");
		lotNo = findElementInXLSheet(getDataProductionSmoke, "lotNo data");
		
		bulkOrderData = findElementInXLSheet(getDataProductionSmoke, "bulkOrder data");
		standardCostData1 = findElementInXLSheet(getDataProductionSmoke, "standardCost Data1");
		searchProductData = findElementInXLSheet(getDataProductionSmoke, "searchProduct Data");
		plannedqtyData1 = findElementInXLSheet(getDataProductionSmoke, "plannedqty Data1");
		
		actualQtyData1 = findElementInXLSheet(getDataProductionSmoke, "actualQty Data1");
		modelData = findElementInXLSheet(getDataProductionSmoke, "model data");
		
		batchData = findElementInXLSheet(getDataProductionSmoke, "batch data");
		GroupIdDatafinal = findElementInXLSheet(getDataProductionSmoke, "GroupId Datafinal");
		
		productCodeData = findElementInXLSheet(getDataProductionSmoke, "productCode Data");
		basePriceData = findElementInXLSheet(getDataProductionSmoke, "basePrice Data");
		manufacturerData = findElementInXLSheet(getDataProductionSmoke, "manufacturer Data");
		lengthData = findElementInXLSheet(getDataProductionSmoke, "length Data");
		widthData = findElementInXLSheet(getDataProductionSmoke, "width Data");
		heightData = findElementInXLSheet(getDataProductionSmoke, "height Data");
		volumeData = findElementInXLSheet(getDataProductionSmoke, "volume Data");
		

		/* QA Form level action verification */
		
		previousBOMData = findElementInXLSheet(getDataProductionSmoke, "previousBOM Data");
		reasonData = findElementInXLSheet(getDataProductionSmoke, "reason Data");
		reasonData1 = findElementInXLSheet(getDataProductionSmoke, "reason Data1");
		BOOCodeData1 = findElementInXLSheet(getDataProductionSmoke, "BOO Code1");
		subjectData = findElementInXLSheet(getDataProductionSmoke, "subject Data");
		AssignToData = findElementInXLSheet(getDataProductionSmoke, "Assign ToData");
		modelCodeData1 = findElementInXLSheet(getDataProductionSmoke, "modelCode Data1");
		modelCode = findElementInXLSheet(getDataProductionSmoke, "modelCode");
		plannedqtyDataver = findElementInXLSheet(getDataProductionSmoke, "plannedqtyDataver");
		productionOrderData = findElementInXLSheet(getDataProductionSmoke, "productionOrderData");
		prodNo = findElementInXLSheet(getDataProductionSmoke, "prodNo");
		previousbulkOrderData = findElementInXLSheet(getDataProductionSmoke, "previousbulkOrderData");
		costEstDescription = findElementInXLSheet(getDataProductionSmoke, "costEstDescription");
		costProductData = findElementInXLSheet(getDataProductionSmoke, "costProductData");
		costEstQuantityData = findElementInXLSheet(getDataProductionSmoke, "costEstQuantityData");
		pricingProfileCostData = findElementInXLSheet(getDataProductionSmoke, "pricingProfileCostData");
		
		/* Production Smoke Re-Coding */
		/* Smoke_Production_0001_1 */
		description = findElementInXLSheet(getDataProductionSmoke, "description");
		productGroupSmokeProduction = findElementInXLSheet(getDataProductionSmoke, "productGroupSmokeProduction");
		hundred = findElementInXLSheet(getDataProductionSmoke, "hundred");
		five = findElementInXLSheet(getDataProductionSmoke, "five");
		ten = findElementInXLSheet(getDataProductionSmoke, "ten");
		automationTenantUsername = findElementInXLSheet(getDataProductionSmoke, "automationTenantUsername");
		automationTenantPassword = findElementInXLSheet(getDataProductionSmoke, "automationTenantPassword");
		
		/* Smoke_Production_0001_2 */
		moveStockElementDescription = findElementInXLSheet(getDataProductionSmoke, "moveStockElementDescription");
		elementCategorySupply = findElementInXLSheet(getDataProductionSmoke, "elementCategorySupply");
		activityTypeMoveStock = findElementInXLSheet(getDataProductionSmoke, "activityTypeMoveStock");
		activityTypeProduce = findElementInXLSheet(getDataProductionSmoke, "activityTypeProduce");
		produceElementDescription1 = findElementInXLSheet(getDataProductionSmoke, "produceElementDescription1");
		batchMethodTransferBOO = findElementInXLSheet(getDataProductionSmoke, "batchMethodTransferBOO");
		groupIdBOOOne = findElementInXLSheet(getDataProductionSmoke, "groupIdBOOOne");
		groupIdBOOTwo = findElementInXLSheet(getDataProductionSmoke, "groupIdBOOTwo");
		groupIdBOOThree = findElementInXLSheet(getDataProductionSmoke, "groupIdBOOThree");
		produceElementDescription2 = findElementInXLSheet(getDataProductionSmoke, "produceElementDescription2");
		activityTypeQualityCheck = findElementInXLSheet(getDataProductionSmoke, "activityTypeQualityCheck");
		qcElementDescription = findElementInXLSheet(getDataProductionSmoke, "qcElementDescription");
		elementCategoryOperation = findElementInXLSheet(getDataProductionSmoke, "elementCategoryOperation");
		
		/* Smoke_Production_0001_3 */
		productionUnit = findElementInXLSheet(getDataProductionSmoke, "productionUnit");
		siteNegombo = findElementInXLSheet(getDataProductionSmoke, "siteNegombo");
		barcodeBookBG = findElementInXLSheet(getDataProductionSmoke, "barcodeBookBG");
		costingProrityNormal = findElementInXLSheet(getDataProductionSmoke, "costingProrityNormal");
		pricingProfile = findElementInXLSheet(getDataProductionSmoke, "pricingProfile");
		processTimeType = findElementInXLSheet(getDataProductionSmoke, "processTimeType");
		
		/* Smoke_Production_0002 */
		productionOrderGroup = findElementInXLSheet(getDataProductionSmoke, "productionOrderGroup");
		productionOrderDescription = findElementInXLSheet(getDataProductionSmoke, "productionOrderDescription");
		
		/* Smoke_Production_0003 */
		commonAddress = findElementInXLSheet(getDataProductionSmoke, "commonAddress");
		
		/* Smoke_Production_0004 */
		actualQuantityInputProductFirstOperation = findElementInXLSheet(getDataProductionSmoke, "actualQuantityInputProductFirstOperation");
		actualQuantityOutputProductFirstOperation = findElementInXLSheet(getDataProductionSmoke, "actualQuantityOutputProductFirstOperation");
		actualQuantityInputProductSecondOperation = findElementInXLSheet(getDataProductionSmoke, "actualQuantityInputProductSecondOperation");
		actualQuantityOutputProductSecondOperation = findElementInXLSheet(getDataProductionSmoke, "actualQuantityOutputProductSecondOperation");

		/* Smoke_Production_0005 */
		qcTypeProductionOrder = findElementInXLSheet(getDataProductionSmoke, "qcTypeProductionOrder");
		failQuantityProductionSmoke = findElementInXLSheet(getDataProductionSmoke, "failQuantityProductionSmoke");
		passQuantityProductionSmoke = findElementInXLSheet(getDataProductionSmoke, "passQuantityProductionSmoke");

		/* Smoke_Production_0006 */
		actualUpdateQuantityProductionQuntity = findElementInXLSheet(getDataProductionSmoke, "actualUpdateQuantityProductionQuntity");

	}
	
	
	
	
}
