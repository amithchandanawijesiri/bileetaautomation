package bileeta.BATF.Pages;

import bileeta.BTAF.Utilities.TestBase;

public class FixedAssetData extends TestBase{
	
/* Reading the element locators to variables */
	
	
	protected static String siteURL;
	protected static String siteLogo;
	protected static String txt_username;
	protected static String txt_password;
	protected static String btn_login;
	protected static String lnk_home;
	
	/*Navigate to side menu*/
	protected static String navigateMenu;
	protected static String sideNavBar;
	
	/*Navigate to fixed asset module*/
	protected static String fixedAsset_btn; 
	protected static String fixedAssetLogo;
	
	/*Navigate to procurement and PO*/
	protected static String btn_procurement;
	protected static String label_procurement;
	protected static String btn_purchaseOrder;
	protected static String label_PurchaseOrder;
	protected static String btn_newPurchaseOrder;
	protected static String tab_journey;
	protected static String btn_newServiceJourney;
	protected static String label_PurchaseOrder1;
	
	/*Filling mandatory fields*/
	protected static String btn_vendorSearch;
	protected static String txt_vendorSearch;
	protected static String sel_vendorSearch;
	protected static String doubleClickVendor;
	protected static String txt_venderRefNo;
	protected static String txt_Product;
	protected static String txt_ProductQty;
	protected static String btn_productSearch;
	protected static String txt_productSearch;
	protected static String doubleClickProduct;
	protected static String txt_qty;
	protected static String txt_unitPrice;
	protected static String btn_advance;
	protected static String btn_fixedAss;
	protected static String label_advance;
	protected static String label_assetGrp;
	protected static String btn_assetGrp;
	protected static String btn_apply;
	protected static String btn_checkout;
	protected static String btn_sendReview;
	protected static String btn_entution;
	protected static String btn_taskEvent;
	protected static String btn_approvals;
	protected static String btn_PO;
	protected static String btn_draft;
	protected static String btn_release;
	protected static String txt_PONumber;
	protected static String txt_approveNo;
	protected static String btn_approval;
	protected static String btn_releaseProc;
	protected static String btn_checkoutPro;
	protected static String btn_draftPro;
	protected static String btn_releaseButton;
	protected static String btn_actionPro;
	protected static String btn_journal;
	protected static String label_purchaseOrderPage;
	protected static String label_purchaseInvoicePage;
	protected static String label_falabel;
	protected static String label_fixedAssetInformation;
	protected static String label_purchaseInvoicePageRelease;
	protected static String label_purchaseOrderPageNew;
	protected static String label_purchaseOrderPageRelease;
	protected static String label_gotopage;
	protected static String label_journalEntryDetails;
	protected static String label_fixedAssetInformationRelease;
	protected static String label_fixedAssetInformationlabel;

	
	protected static String btn_inventory;
	protected static String label_inventory;
	protected static String btn_internalOrder;
	protected static String label_InernalOrder;
	protected static String btn_newInternalOrder;
	protected static String tab_internalOrderjourney;
	protected static String btn_CapitalWIPRequestJourney;
	protected static String label_InternalOrder1;
	protected static String txt_title;
	protected static String btn_requester;
	protected static String txt_requesterSearch;
	protected static String doubleClickRequester;
	protected static String btn_date;
	protected static String btn_internalProductSearch;
	protected static String txt_internalProductSearch;
	protected static String sel_requesterSel;
	protected static String doubleClickinternalProduct;
	protected static String txt_internalqty;
	protected static String btn_advanceInventory;
	protected static String label_advanceInventory;
	protected static String btn_fixedAssInternal;
	protected static String label_assetGrpInternal;
	protected static String btn_assetGrpInternal;
	protected static String btn_applyInternal;
	protected static String btn_goToPageInternal;
	protected static String txt_shipping;
	protected static String btn_draftdispatch;
	protected static String btn_releasedispatch;
	protected static String btn_action;
	protected static String btn_journalEntry;
	protected static String txt_FA;
	protected static String btn_fixedAssSegment;
	protected static String txt_segmentNo;
	protected static String btn_fixedAssetInfo;
	protected static String txt_fixedAssetInfo;
	protected static String btn_ok;
	protected static String btn_close;
	protected static String doubleClickSegmentNo;
	protected static String btn_endRelease;
	protected static String label_InternalOrder;
	protected static String label_InternalDispatchOrder;
	protected static String label_journalEntry;
	protected static String label_InternalOrderRelease;
	

	protected static String btn_project;
	protected static String label_project;
	protected static String btn_subProject;
	protected static String label_subProject;
	protected static String btn_newProject;
	protected static String txt_project;
	protected static String txt_projectCode;
	protected static String txt_projectTitle;
	protected static String txt_projectGroup;
	protected static String txt_postingMethod;
	protected static String txt_projectType;
	protected static String txt_assetGroup;
	protected static String btn_responsiblePersonSearch;
	protected static String txt_responsiblePersonSearch;
	protected static String doubleClickresponsiblePerson;
	protected static String txt_bussinessUnit;
	protected static String btn_pricingProfile;
	protected static String txt_pricingProfile;
	protected static String txt_startDate;
	protected static String txt_endDate;
	protected static String doubleClick_startDate;
	protected static String doubleClick_endDate;
	protected static String btn_projectLocation;
	protected static String txt_projectLocation;
	protected static String doubleClickProjectLocation;
	protected static String txt_inputWarehouse;
	protected static String txt_WIPWarehouse;
	protected static String btn_edit;
	protected static String btn_workBreakdown;
	protected static String btn_plus;
	protected static String txt_projectCodeNo;
	protected static String txt_taskName;
	protected static String btn_update;
	protected static String btn_expand;
	protected static String btn_releaseTask;
	protected static String btn_updateMain;
	protected static String btn_finance;
	protected static String txt_financeInfo;
	protected static String btn_outboundPaymentAdvice;
	protected static String txt_OutBoundPaymentAdvice;
	protected static String btn_newOutboundPaymentAdvice;
	protected static String btn_accrualVoucher;
	protected static String btn_vendorDetails;
	protected static String txt_vendorDetails;
	protected static String doubleClickvendorDetails;
	protected static String btn_GLAcc;
	protected static String txt_GLAcc;
	protected static String doubleClickGLAcc;
	protected static String txt_amount;
	protected static String btn_costAllocation;
	protected static String txt_vendorRefNo;
	protected static String txt_costCenter;
	protected static String txt_percentage;
	protected static String txt_costAssignmentType;
	protected static String btn_costObject;
	protected static String txt_costObject;
	protected static String doubleClickCostObject;
	protected static String btn_addRecord;
	protected static String btn_updateCostAllocations;
	protected static String btn_checkoutCost;
	protected static String btn_draftCost;
	protected static String btn_releaseCost;
	protected static String txt_projecttxt;
	protected static String btn_submenuProject;
	protected static String clickProjectCode;
	protected static String btn_actionProject;
	protected static String btn_updateTaskPOC;
	protected static String txt_task;
	protected static String txt_POC;
	protected static String btn_applyTask;
	protected static String btn_complete;
	protected static String btn_yes;
	protected static String doubleClick_postDate;
	protected static String btn_SelectAll;
	protected static String btn_postDate;
	protected static String btn_applytaskCompletion;
	protected static String btn_actionCompletedProject;
	protected static String btn_journalCompletedProject;
	protected static String txt_FABookProject;
	protected static String btn_SelectCheck;
	protected static String btn_fixedAssSegmentNo;
	protected static String label_responsiblePerson;
	protected static String label_pricingProfile;
	protected static String label_projectLocation;
	protected static String label_projectAgain;
	protected static String label_projecttask;
	protected static String label_outboundPaymentAdvice;
	protected static String label_newJourney;
	protected static String label_outboundPayment;
	protected static String label_vendorDetails;
	protected static String label_GLAcc;
	protected static String label_costAllocation;
	protected static String label_taskCompletion;
	protected static String label_projectJournal;
	protected static String label_Project;
	

	protected static String btn_fixedAssetBook;
	protected static String txt_fixedAssetBook;
	protected static String btn_template;
	protected static String txt_fixedAssetBookNo;
	protected static String doubleClickFixedAssetBookNo;
	protected static String btn_depreciation;
	protected static String btn_releaseBook;
	protected static String btn_selectAll;
	protected static String txt_fixedAssetInfoNo;
	protected static String doubleClickFixedAssetInfoNo;
	protected static String btn_Revaluation;
	protected static String label_fixedAssetInformationMain;
	protected static String label_fixedAssetRevaluation;
	protected static String click_close;
	
	protected static String btn_fixedAssetInformation;
	protected static String txt_fixedAssetInformation;
	protected static String btn_AssetBook;
	protected static String txt_revalueAmount;
	protected static String txt_usefulLife;
	protected static String btn_applyAssetInfo;
	protected static String txt_assetDescription;
	protected static String btn_assetInfoSearch;
	protected static String txt_assetInfoLookup;
	protected static String doubleClickAssetInfoLookup;
	protected static String txt_disposalDate;
	protected static String txt_disposalMode;
	protected static String btn_applyDisposal;
	
	protected static String btn_split;
	protected static String doubleClickFixedAssetInfoNo1;
	protected static String btn_disposal;
	protected static String doubleClick_disposalDate;
	protected static String btn_createNewwAsset;
	protected static String btn_AssetBookSplit1;
	protected static String txt_usefulLifeSplit1;
	protected static String label_fixedAssetSplit;
	protected static String label_fixedAssetInformationSplit;
	
	protected static String btn_existingBook;
	protected static String txt_fixedAssetInfoNo3;
	protected static String doubleClickFixedAssetInfoNo3;
	protected static String label_fixedAssetDisposal;
	
	protected static String doubleClickFixedAssetInfoNoDisposal;
	protected static String btn_applyAssetInfoDisposal;
	
	protected static String txt_disposalDateSelling;
	protected static String doubleClick_disposalDateSelling;
	protected static String txt_disposalModeSelling;
	protected static String btn_applyAssetInfoSelling;
	
	protected static String txt_freeTextInvoice;
	protected static String btn_customer;
	protected static String txt_customer;
	protected static String doubleClickCustomer;
	protected static String txt_salesUnit;
	protected static String label_freeTextInvoice;
	protected static String txt_amountText;
	
	protected static String txt_dispatchOrderNo;
	protected static String btn_reports;
	protected static String btn_templatebtn;
	protected static String btn_reportTmpWiz;
	protected static String txt_productCode;
	protected static String btn_quickPreview;
	
	protected static String btn_AssetBooks;
	protected static String btn_FABook;
	protected static String txt_netBookNo1;
	protected static String txt_netBookNo2;
	
	protected static String txt_credit;
	protected static String txt_debit;
	protected static String txt_docValue;
	protected static String txt_stockValue;
	protected static String goToPageInternal;
	
	
	//SMOKE Test case properties
	protected static String btn_fixedAssetBookCreation;
	protected static String btn_newfixedAssetBook;
	protected static String txt_assetBookCode;
	protected static String btn_desc;
	protected static String txt_desc;
	protected static String txt_usefulLife1;
	protected static String btn_dateA;
	protected static String btn_aquisitionDate;
	protected static String btn_dateB;
	protected static String btn_depreciationClick;
	protected static String btn_depreciationStartDate;
	protected static String btn_scheduleCode;
	protected static String txt_scheduleCode;
	protected static String doubleClickscheduleCode;
	protected static String btn_responsiblePerson;
	protected static String txt_responsiblePerson;
	protected static String doubleClickresponsiblePerson1;
	protected static String btn_applyBook;
	protected static String label_fixedAssetBook;
	protected static String label_outboundPaymentnew;
	protected static String label_purchaseInvoicePageReleasebtn;
	
	
	protected static String btn_plus1;
	protected static String btn_plus2;
	protected static String txt_group;
	protected static String txt_GroupName;
	protected static String txt_Book;
	protected static String btn_fixedAssetBooks;
	protected static String txt_fixedAssetBooks;
	protected static String doubleClickfixedAssetBook;
	protected static String btn_post;
	protected static String btn_bookView;
	protected static String txt_aquisitionValue;
	protected static String label_GroupConfig;
	protected static String btn_fixedAssetGroupConfig;
	protected static String btn_newFixedAssetGroupConfig;
	protected static String btn_applyGroup;
	protected static String assetGroup_tbl;
	protected static String label_outboundPaymentlabel;
	
	protected static String btn_newFixedAssetInformation;
	protected static String txt_InfoDescription;
	protected static String txt_quantity;
	protected static String txt_perUnitCost;
	protected static String txt_barcodeBook;
	protected static String btn_assetBookTab;
	protected static String btn_fixedAssetBooksearch;
	protected static String btn_disposalAndDepreciation;
	protected static String btn_scheduleCodeSearch;
	protected static String btn_fixedAssetInformationCreate;
	protected static String txt_AssetGroupName;
	protected static String doubleClickscheduleCodeInfo;
	protected static String btn_applyInfo;
	protected static String label_fixedAssetInfoRelease;
	
	protected static String btn_CheckoutBtn;
	protected static String btn_DraftBtn;
	protected static String btn_ReleaseBtn;
	protected static String btn_GoToPageBtn;
	protected static String txt_pageDraft;
	protected static String txt_pageRelease;
	protected static String txt_DocHeader;
	protected static String btn_ActionBtn;
	protected static String txt_pageDelete;
	protected static String btn_ReverseBtn;
	protected static String btn_ReversePopupBtn;
	protected static String txt_pageReverse;
	
	/* Reading the Data to variables */
	
	protected static String UserNameData;
	protected static String PasswordData;
	
	protected static String vendorData;
	protected static String vendorRefData;
	protected static String Product1;
	protected static String Product2;
	protected static String productsData;
	protected static String qtyData;
	protected static String unitPriceData;
	protected static String assetGrpData;
	
	
	protected static String titleData;
	protected static String requesterData;
	protected static String dateData;
	protected static String productData1;
	protected static String internalqtyData;
	protected static String assetGrpInternalData;
	protected static String shippingData;

	
	
	protected static String projectCodeData;
	protected static String projectTitleData;
	protected static String projectGroupData;
	protected static String postingMethodData;
	protected static String projectTypeData;
	protected static String assetGroupData;
	protected static String responsiblePerson;
	protected static String bussinessUnitData;
	protected static String pricingProfileData;
	protected static String doubleClickPricingProfile;
	protected static String projectLocationData;
	protected static String inputwareData;
	protected static String WIPWareData;
	protected static String taskNameData;
	protected static String vendorDetailsData;
	protected static String GLAccData;
	protected static String amountData;
	protected static String vendorRefNoData;
	protected static String costCenterData;
	protected static String percentageData;
	protected static String costAssignmentData;
	protected static String costObjectData;
	protected static String taskData;
	protected static String POCData;
	
	protected static String fixedAssetBookNoData;
	protected static String templateData;
	
	protected static String fixedAssetInfo;
	protected static String AssetBookData;
	protected static String RevalueAmountData;
	protected static String usefulLifeData;
	protected static String fixedAssetInfo1;
	
	protected static String disposalModeDataWriteOff;
	protected static String disposalModeDataWrite;
	protected static String fixedAssetInfo2;
	
	protected static String AssetBookDataSplit1;
	protected static String assetDescriptionData;
	protected static String usefulLifeDataSplit1;
	
	protected static String fixedAssetInfo3;
	
	protected static String assetInfoLookupData;
	
	protected static String customerData;
	protected static String salesUnitData;
	protected static String amountDataText;
	//Calling the constructor

	//SMOKE Test case data
	protected static String assetBookCodeData;
	protected static String assetBookCodeDataCreate;
	protected static String descData;
	protected static String usefulLifeData1;
	protected static String scheduleCodeData;
	protected static String responsiblePersonData;
	
	//test case 02
	protected static String groupData;
	protected static String fixedAssetBooksData;
	protected static String quisitionData;
	
	//test case 04
	protected static String InfoDescriptionData;
	protected static String quantityData;
	protected static String perUnitCostData;

	
	
	public static void readElementlocators() throws Exception
	{
	
		siteLogo= findElementInXLSheet(getParameterFixedAsset,"site logo"); 
		txt_username= findElementInXLSheet(getParameterFixedAsset,"user name"); 
		txt_password= findElementInXLSheet(getParameterFixedAsset,"password"); 
		btn_login= findElementInXLSheet(getParameterFixedAsset,"login button"); 
		lnk_home=findElementInXLSheet(getParameterFixedAsset,"home_lnk");
		
		/*Navigate to side menu*/
		navigateMenu = findElementInXLSheet(getParameterFixedAsset, "nav_btn");
		sideNavBar = findElementInXLSheet(getParameterFixedAsset, "nav_view");
		fixedAsset_btn = findElementInXLSheet(getParameterFixedAsset, "fixedAssetbtn");
		fixedAssetLogo = findElementInXLSheet(getParameterFixedAsset, "fixedAsset_logo");
		
		/*Navigate to fixed asset module*/
		btn_procurement =findElementInXLSheet(getParameterFixedAsset, "procurement_btn");
		label_procurement=findElementInXLSheet(getParameterFixedAsset, "procurement_label");
		btn_purchaseOrder = findElementInXLSheet(getParameterFixedAsset, "purchaseOrder_btn");
		label_PurchaseOrder = findElementInXLSheet(getParameterFixedAsset, "purchaseOrderPage_logo");
		btn_newPurchaseOrder = findElementInXLSheet(getParameterFixedAsset, "newPurchase_btn");
		tab_journey = findElementInXLSheet(getParameterFixedAsset, "journey_label");
		btn_newServiceJourney = findElementInXLSheet(getParameterFixedAsset, "serviceJourney");
		label_PurchaseOrder1 = findElementInXLSheet(getParameterFixedAsset, "purchaseOrderPage_logo1");
		
		/*Filling mandatory fields Form*/
		btn_vendorSearch = findElementInXLSheet(getParameterFixedAsset, "vendorSearch_btn");
		txt_vendorSearch = findElementInXLSheet(getParameterFixedAsset, "vendorSearch_txt");
		sel_vendorSearch = findElementInXLSheet(getParameterFixedAsset, "vendorSearch_sel");
		doubleClickVendor = findElementInXLSheet(getParameterFixedAsset, "doubleClick");
		txt_venderRefNo = findElementInXLSheet(getParameterFixedAsset, "venderRefNo_txt");
		txt_Product = findElementInXLSheet(getParameterFixedAsset, "Product");
		txt_ProductQty = findElementInXLSheet(getParameterFixedAsset, "ProductQty");

		
		btn_productSearch = findElementInXLSheet(getParameterFixedAsset, "product_Search");
		txt_productSearch = findElementInXLSheet(getParameterFixedAsset, "productsearch_txt");
		doubleClickProduct = findElementInXLSheet(getParameterFixedAsset, "product double click");
		txt_qty = findElementInXLSheet(getParameterFixedAsset, "qty_txt");
		txt_unitPrice = findElementInXLSheet(getParameterFixedAsset, "unitPrice_txt");
		btn_advance = findElementInXLSheet(getParameterFixedAsset, "advance_btn");
		label_advance = findElementInXLSheet(getParameterFixedAsset, "advance_label");
		btn_fixedAss = findElementInXLSheet(getParameterFixedAsset, "asset_btn");
		label_assetGrp = findElementInXLSheet(getParameterFixedAsset, "asset Group_label");
		btn_assetGrp = findElementInXLSheet(getParameterFixedAsset, "assetgrp_btn");
		btn_apply = findElementInXLSheet(getParameterFixedAsset, "app_btn");
		btn_checkout = findElementInXLSheet(getParameterFixedAsset, "checkout_btn");
		btn_sendReview = findElementInXLSheet(getParameterFixedAsset, "sendReview_btn");
		btn_entution = findElementInXLSheet(getParameterFixedAsset, "entution_btn");
		btn_taskEvent = findElementInXLSheet(getParameterFixedAsset, "taskEvent_btn"); 
		btn_approvals = findElementInXLSheet(getParameterFixedAsset, "approval_btn");
		btn_PO = findElementInXLSheet(getParameterFixedAsset, "PO_btn");
		btn_draft = findElementInXLSheet(getParameterFixedAsset, "draft_btn");
		txt_PONumber = findElementInXLSheet(getParameterFixedAsset, "PONumber_txt");
		txt_approveNo = findElementInXLSheet(getParameterFixedAsset, "approveNo_txt");
		btn_approval = findElementInXLSheet(getParameterFixedAsset, "approval_btn");
		btn_releaseProc = findElementInXLSheet(getParameterFixedAsset, "releaseProc_btn");
		btn_checkoutPro = findElementInXLSheet(getParameterFixedAsset, "checkoutPro_btn");
		label_purchaseOrderPageRelease = findElementInXLSheet(getParameterFixedAsset, "label_purchaseOrderPageRelease");
		btn_draftPro = findElementInXLSheet(getParameterFixedAsset, "draftPro_btn");
		btn_releaseButton = findElementInXLSheet(getParameterFixedAsset, "releaseButton_btn");
		btn_actionPro = findElementInXLSheet(getParameterFixedAsset, "actionPro_btn");
		btn_journal = findElementInXLSheet(getParameterFixedAsset, "journal_btn");
		label_purchaseOrderPageNew = findElementInXLSheet(getParameterFixedAsset, "label_purchaseOrderPageNew");
		goToPageInternal = findElementInXLSheet(getParameterFixedAsset, "goToPageInternal");
		
		label_purchaseOrderPage = findElementInXLSheet(getParameterFixedAsset, "purchaseOrderPage_label");
		label_purchaseInvoicePage = findElementInXLSheet(getParameterFixedAsset, "purchaseInvoicePage_label");
		label_purchaseInvoicePageRelease = findElementInXLSheet(getParameterFixedAsset, "label_purchaseInvoicePageRelease");
		label_journalEntryDetails = findElementInXLSheet(getParameterFixedAsset, "label_journalEntryDetails");
		label_falabel = findElementInXLSheet(getParameterFixedAsset, "falabel_label");
		label_fixedAssetInformation = findElementInXLSheet(getParameterFixedAsset, "fixedAssetInformation_label");
		label_journalEntry = findElementInXLSheet(getParameterFixedAsset, "journalEntry_label");
		label_fixedAssetInformationRelease = findElementInXLSheet(getParameterFixedAsset, "label_fixedAssetInformationRelease");
		label_purchaseInvoicePageReleasebtn = findElementInXLSheet(getParameterFixedAsset, "label_purchaseInvoicePageReleasebtn");
		
		/*Navigate to inventory and warehousing*/
		btn_inventory = findElementInXLSheet(getParameterFixedAsset, "inventory_btn");
		label_inventory = findElementInXLSheet(getParameterFixedAsset, "inventory_label");
		btn_internalOrder = findElementInXLSheet(getParameterFixedAsset, "internalOrder_btn");
		label_InernalOrder = findElementInXLSheet(getParameterFixedAsset, "internalOrder_label");
		btn_newInternalOrder = findElementInXLSheet(getParameterFixedAsset, "newInternalOrder_btn");
		tab_internalOrderjourney = findElementInXLSheet(getParameterFixedAsset, "internalOrderjourney_tab");
		btn_CapitalWIPRequestJourney = findElementInXLSheet(getParameterFixedAsset, "CapitalWIPRequestJourney");
		label_InternalOrder1 = findElementInXLSheet(getParameterFixedAsset, "InternalOrder1_label");
		txt_title = findElementInXLSheet(getParameterFixedAsset, "title_txt");
		btn_requester = findElementInXLSheet(getParameterFixedAsset, "requester_btn");
		txt_requesterSearch = findElementInXLSheet(getParameterFixedAsset, "requester search");
		doubleClickRequester = findElementInXLSheet(getParameterFixedAsset, "doubleclick requester");
		btn_date = findElementInXLSheet(getParameterFixedAsset, "date button");
		btn_internalProductSearch = findElementInXLSheet(getParameterFixedAsset, "internal_product_Search");
		txt_internalProductSearch = findElementInXLSheet(getParameterFixedAsset, "internal_productsearch_txt");
		sel_requesterSel = findElementInXLSheet(getParameterFixedAsset, "requesterSel");
		doubleClickinternalProduct = findElementInXLSheet(getParameterFixedAsset, "internal_product double click");
		txt_internalqty = findElementInXLSheet(getParameterFixedAsset, "internal qty");
		btn_advanceInventory = findElementInXLSheet(getParameterFixedAsset, "advanceInventory_btn");
		label_advanceInventory = findElementInXLSheet(getParameterFixedAsset, "advanceInventory_label");
		btn_fixedAssInternal = findElementInXLSheet(getParameterFixedAsset, "fixedAssetInternal_btn");
		label_assetGrpInternal = findElementInXLSheet(getParameterFixedAsset, "assetGrpInternal_label");
		btn_assetGrpInternal = findElementInXLSheet(getParameterFixedAsset, "assetGrpInternal_btn");
		btn_applyInternal = findElementInXLSheet(getParameterFixedAsset, "applyInternal_btn");
		btn_release = findElementInXLSheet(getParameterFixedAsset, "release_btn");
		btn_goToPageInternal = findElementInXLSheet(getParameterFixedAsset, "goToPageInternal_btn");
		label_gotopage = findElementInXLSheet(getParameterFixedAsset, "label_gotopage");
		txt_shipping = findElementInXLSheet(getParameterFixedAsset, "shipping_txt");
		btn_draftdispatch = findElementInXLSheet(getParameterFixedAsset, "draftDispatch_btn");
		btn_releasedispatch = findElementInXLSheet(getParameterFixedAsset, "releasedispatch_btn");
		btn_action = findElementInXLSheet(getParameterFixedAsset, "action_btn");
		btn_journalEntry = findElementInXLSheet(getParameterFixedAsset, "journalEntry_btn");
		txt_FA = findElementInXLSheet(getParameterFixedAsset, "journalEntryNo_btn");
		btn_fixedAssSegment = findElementInXLSheet(getParameterFixedAsset, "fixedAssSegment_btn");
		txt_segmentNo = findElementInXLSheet(getParameterFixedAsset, "segmentNo_txt");
		btn_ok = findElementInXLSheet(getParameterFixedAsset, "ok_btn");
		btn_fixedAssetInfo = findElementInXLSheet(getParameterFixedAsset, "fixedAssetInfo_btn");
		txt_fixedAssetInfo = findElementInXLSheet(getParameterFixedAsset, "fixedAssetInfo_txt");
		btn_close = findElementInXLSheet(getParameterFixedAsset, "close_btn");
		doubleClickSegmentNo = findElementInXLSheet(getParameterFixedAsset, "doubleClick segmentNo");
		btn_endRelease = findElementInXLSheet(getParameterFixedAsset, "endRelease_btn");
		
		label_InternalOrder = findElementInXLSheet(getParameterFixedAsset, "InternalOrder_label");
		label_InternalDispatchOrder = findElementInXLSheet(getParameterFixedAsset, "internalDispatchOrder_label");
		label_InternalOrderRelease = findElementInXLSheet(getParameterFixedAsset, "label_InternalOrderRelease");
		
		/*Navigate to Project*/
		btn_project = findElementInXLSheet(getParameterFixedAsset, "project_btn");
		label_project = findElementInXLSheet(getParameterFixedAsset, "project_label");
		btn_subProject = findElementInXLSheet(getParameterFixedAsset, "subProject_btn");
		label_subProject = findElementInXLSheet(getParameterFixedAsset, "subProject_label");
		btn_newProject = findElementInXLSheet(getParameterFixedAsset, "newProject_btn");
		txt_project = findElementInXLSheet(getParameterFixedAsset, "project_txt");
		txt_projectCode =findElementInXLSheet(getParameterFixedAsset, "projectCode_txt");
		txt_projectTitle = findElementInXLSheet(getParameterFixedAsset, "projectTitle_txt");
		txt_projectGroup = findElementInXLSheet(getParameterFixedAsset, "projectGroup_txt");
		txt_postingMethod = findElementInXLSheet(getParameterFixedAsset, "postingMethod_txt");
		txt_projectType = findElementInXLSheet(getParameterFixedAsset, "projectType_txt");
		txt_assetGroup = findElementInXLSheet(getParameterFixedAsset, "asset group");
		btn_responsiblePersonSearch = findElementInXLSheet(getParameterFixedAsset, "responsible person_btn");
		txt_responsiblePersonSearch = findElementInXLSheet(getParameterFixedAsset, "responsible person_txt");
		doubleClickresponsiblePerson = findElementInXLSheet(getParameterFixedAsset, "doubleClick_responsiblePerson");
		txt_bussinessUnit = findElementInXLSheet(getParameterFixedAsset, "bussinessUnit_txt");
		btn_pricingProfile = findElementInXLSheet(getParameterFixedAsset, "pricing profile_btn");
		txt_pricingProfile = findElementInXLSheet(getParameterFixedAsset, "pricing profile_txt");
		doubleClickPricingProfile = findElementInXLSheet(getParameterFixedAsset, "doubleClick pricingProfile");
		txt_startDate = findElementInXLSheet(getParameterFixedAsset, "startdate_txt");
		txt_endDate = findElementInXLSheet(getParameterFixedAsset, "enddate_txt");
		doubleClick_startDate =findElementInXLSheet(getParameterFixedAsset, "startdate doublelick");
		doubleClick_endDate = findElementInXLSheet(getParameterFixedAsset, "enddate doubleclick");
		btn_projectLocation = findElementInXLSheet(getParameterFixedAsset, "projectLocation_btn");
		txt_projectLocation = findElementInXLSheet(getParameterFixedAsset, "projectLocation_txt");
		doubleClickProjectLocation = findElementInXLSheet(getParameterFixedAsset, "doubleClick projectLocation");
		txt_inputWarehouse = findElementInXLSheet(getParameterFixedAsset, "inputWare_txt");
		txt_WIPWarehouse = findElementInXLSheet(getParameterFixedAsset, "WIPWare_txt");
		btn_edit = findElementInXLSheet(getParameterFixedAsset, "edit_btn");
		btn_workBreakdown = findElementInXLSheet(getParameterFixedAsset, "workBreakdown_btn");
		btn_plus = findElementInXLSheet(getParameterFixedAsset, "plus_btn");
		txt_projectCodeNo = findElementInXLSheet(getParameterFixedAsset, "projectCodeNo_txt");
		txt_taskName = findElementInXLSheet(getParameterFixedAsset, "taskName_txt");
		btn_update = findElementInXLSheet(getParameterFixedAsset, "update_btn");
		btn_expand = findElementInXLSheet(getParameterFixedAsset, "expand_btn");
		btn_releaseTask = findElementInXLSheet(getParameterFixedAsset, "releaseTask_btn");
		btn_updateMain = findElementInXLSheet(getParameterFixedAsset, "updateMain_btn");
		btn_finance = findElementInXLSheet(getParameterFixedAsset, "finance_btn");
		txt_financeInfo = findElementInXLSheet(getParameterFixedAsset, "financeInfo_txt");
		btn_outboundPaymentAdvice = findElementInXLSheet(getParameterFixedAsset, "outboundPaymentAdvice_btn");
		txt_OutBoundPaymentAdvice = findElementInXLSheet(getParameterFixedAsset, "outboundPaymentAdvice_txt");
		btn_newOutboundPaymentAdvice = findElementInXLSheet(getParameterFixedAsset, "newOutboundPaymentAdvice_btn");
		btn_accrualVoucher = findElementInXLSheet(getParameterFixedAsset, "accrualVoucher_btn");
		btn_vendorDetails = findElementInXLSheet(getParameterFixedAsset, "vendorDetails_btn");
		txt_vendorDetails = findElementInXLSheet(getParameterFixedAsset, "vendorDetails_txt");
		doubleClickvendorDetails = findElementInXLSheet(getParameterFixedAsset, "doubleClick vendorDetails");
		btn_GLAcc = findElementInXLSheet(getParameterFixedAsset, "GLAcc_btn");
		txt_GLAcc = findElementInXLSheet(getParameterFixedAsset, "GLAcc_txt");
		doubleClickGLAcc = findElementInXLSheet(getParameterFixedAsset, "doubleClick GLAcc");
		txt_amount = findElementInXLSheet(getParameterFixedAsset, "amount_txt");
		btn_costAllocation = findElementInXLSheet(getParameterFixedAsset, "costAllocation_btn");
		txt_vendorRefNo = findElementInXLSheet(getParameterFixedAsset, "vendorRefNo_txt");
		txt_costCenter = findElementInXLSheet(getParameterFixedAsset, "costCenter_txt");
		txt_percentage = findElementInXLSheet(getParameterFixedAsset, "percentage_txt");
		txt_costAssignmentType = findElementInXLSheet(getParameterFixedAsset, "costAssignmentType_txt");
		btn_costObject = findElementInXLSheet(getParameterFixedAsset, "costObject_btn");
		txt_costObject = findElementInXLSheet(getParameterFixedAsset, "costObject_txt");
		doubleClickCostObject = findElementInXLSheet(getParameterFixedAsset, "doubleClick costObject");
		btn_addRecord = findElementInXLSheet(getParameterFixedAsset, "addRecord_btn");
		btn_updateCostAllocations = findElementInXLSheet(getParameterFixedAsset, "updateCostAllocation_btn");
		btn_checkoutCost = findElementInXLSheet(getParameterFixedAsset, "checkoutCost_btn");
		btn_draftCost = findElementInXLSheet(getParameterFixedAsset, "draftCost_btn");
		btn_releaseCost = findElementInXLSheet(getParameterFixedAsset, "releaseCost_btn");
		txt_projecttxt = findElementInXLSheet(getParameterFixedAsset, "projecttxt_txt");
		btn_submenuProject = findElementInXLSheet(getParameterFixedAsset, "submenuProject_btn");
		clickProjectCode = findElementInXLSheet(getParameterFixedAsset, "click ProjectCode");
		doubleClick_postDate = findElementInXLSheet(getParameterFixedAsset, "postDate_doubleClick");
		btn_actionProject = findElementInXLSheet(getParameterFixedAsset, "actionProject_btn");
		btn_updateTaskPOC = findElementInXLSheet(getParameterFixedAsset, "updateTaskPOC_btn");
		txt_task = findElementInXLSheet(getParameterFixedAsset, "task_txt");
		txt_POC = findElementInXLSheet(getParameterFixedAsset, "POC_txt");
		btn_applyTask = findElementInXLSheet(getParameterFixedAsset, "applyTask_btn");
		btn_complete = findElementInXLSheet(getParameterFixedAsset, "btn_complete");
		btn_yes = findElementInXLSheet(getParameterFixedAsset, "yes_btn");
		btn_SelectAll = findElementInXLSheet(getParameterFixedAsset, "selectAll_btn");
		btn_SelectCheck = findElementInXLSheet(getParameterFixedAsset, "SelectCheck_btn");
		btn_postDate = findElementInXLSheet(getParameterFixedAsset, "postDate_btn");
		btn_applytaskCompletion = findElementInXLSheet(getParameterFixedAsset, "applytaskCompletion_btn");
		btn_actionCompletedProject = findElementInXLSheet(getParameterFixedAsset, "actionCompletedProject_btn");
		btn_journalCompletedProject = findElementInXLSheet(getParameterFixedAsset, "journalCompletedProject_btn");
		txt_FABookProject = findElementInXLSheet(getParameterFixedAsset, "FABookProject_txt");
		btn_fixedAssSegmentNo = findElementInXLSheet(getParameterFixedAsset, "fixedAssSegmentNo_btn");
		label_responsiblePerson = findElementInXLSheet(getParameterFixedAsset, "responsiblePerson_label");
		label_pricingProfile = findElementInXLSheet(getParameterFixedAsset, "pricingProfile_label");
		label_projectLocation = findElementInXLSheet(getParameterFixedAsset, "projectLocation_label");
		label_projectAgain = findElementInXLSheet(getParameterFixedAsset, "projectAgain_label");
		label_projecttask = findElementInXLSheet(getParameterFixedAsset, "projecttask_label");
		label_outboundPaymentAdvice = findElementInXLSheet(getParameterFixedAsset, "outboundPaymentAdvice_label");
		label_newJourney = findElementInXLSheet(getParameterFixedAsset, "newJourney_label");
		label_outboundPayment = findElementInXLSheet(getParameterFixedAsset, "outboundPayment_label");
		label_vendorDetails = findElementInXLSheet(getParameterFixedAsset, "vendorDetails_label");
		label_GLAcc = findElementInXLSheet(getParameterFixedAsset, "GLAcc_label");
		label_costAllocation = findElementInXLSheet(getParameterFixedAsset, "costAllocation_label");
		label_taskCompletion = findElementInXLSheet(getParameterFixedAsset, "taskCompletion_label");
		label_projectJournal = findElementInXLSheet(getParameterFixedAsset, "projectJournal_label");
		label_Project = findElementInXLSheet(getParameterFixedAsset, "Project_label");
		label_outboundPaymentnew = findElementInXLSheet(getParameterFixedAsset, "label_outboundPaymentnew");
		label_outboundPaymentlabel = findElementInXLSheet(getParameterFixedAsset, "label_outboundPaymentlabel");
		label_fixedAssetInformationlabel = findElementInXLSheet(getParameterFixedAsset, "label_fixedAssetInformationlabel");
		
		
		btn_fixedAssetBook = findElementInXLSheet(getParameterFixedAsset, "fixedAssetBook_btn");
		txt_fixedAssetBook = findElementInXLSheet(getParameterFixedAsset, "fixedAssetBook_txt");
		btn_template = findElementInXLSheet(getParameterFixedAsset, "template_btn");
		txt_fixedAssetBookNo = findElementInXLSheet(getParameterFixedAsset, "fixedAssetBookNo_txt");
		doubleClickFixedAssetBookNo = findElementInXLSheet(getParameterFixedAsset, "doubleClick fixedAssetBook No");
		btn_depreciation = findElementInXLSheet(getParameterFixedAsset, "depreciation_btn");
		btn_releaseBook = findElementInXLSheet(getParameterFixedAsset, "releaseBook_btn");
		btn_selectAll = findElementInXLSheet(getParameterFixedAsset, "selectAll_btn");
		btn_Revaluation = findElementInXLSheet(getParameterFixedAsset, "Revaluation_btn");
		label_fixedAssetInformationMain = findElementInXLSheet(getParameterFixedAsset, "fixedAssetInformationMain_label");
		
		btn_fixedAssetInformation = findElementInXLSheet(getParameterFixedAsset, "fixedAssetInformation_btn");
		txt_fixedAssetInformation = findElementInXLSheet(getParameterFixedAsset, "fixedAssetInformation_txt");
		txt_fixedAssetInfoNo = findElementInXLSheet(getParameterFixedAsset, "fixedAssetInfoNo_txt");
		doubleClickFixedAssetInfoNo = findElementInXLSheet(getParameterFixedAsset, "fixedAssetInfoNo double");
		btn_AssetBook = findElementInXLSheet(getParameterFixedAsset, "AssetBook_btn");
		txt_revalueAmount = findElementInXLSheet(getParameterFixedAsset, "revalueAmount_txt");
		txt_usefulLife = findElementInXLSheet(getParameterFixedAsset, "usefulLife_txt");
		btn_applyAssetInfo = findElementInXLSheet(getParameterFixedAsset, "applyAssetInfo_btn");
		txt_assetDescription = findElementInXLSheet(getParameterFixedAsset, "assetDescription_txt");
		btn_assetInfoSearch = findElementInXLSheet(getParameterFixedAsset, "assetInfoSearch_btn");
		txt_assetInfoLookup = findElementInXLSheet(getParameterFixedAsset, "assetInfoLookup_txt");
		doubleClickAssetInfoLookup = findElementInXLSheet(getParameterFixedAsset, "AssetInfoLookupDoubleClick");
		txt_disposalDate = findElementInXLSheet(getParameterFixedAsset, "disposaldate_txt");
		txt_disposalMode = findElementInXLSheet(getParameterFixedAsset, "disposalMode_txt");
		btn_applyDisposal = findElementInXLSheet(getParameterFixedAsset, "applyDisposal_btn");
		label_fixedAssetRevaluation = findElementInXLSheet(getParameterFixedAsset, "fixedAssetRevaluation_label");
		click_close = findElementInXLSheet(getParameterFixedAsset, "close_click");
		
		btn_split = findElementInXLSheet(getParameterFixedAsset, "split_btn");
		label_fixedAssetSplit = findElementInXLSheet(getParameterFixedAsset, "fixedAssetSplit_label");
		label_fixedAssetInformationSplit = findElementInXLSheet(getParameterFixedAsset, "fixedAssetInformationSplit_label");
		
		btn_disposal = findElementInXLSheet(getParameterFixedAsset, "disposal_btn");
		doubleClick_disposalDate = findElementInXLSheet(getParameterFixedAsset, "disposaldate doubleClick");
		doubleClickFixedAssetInfoNo1 = findElementInXLSheet(getParameterFixedAsset, "FixedAssetInfoNo1 doubleClick");
		btn_createNewwAsset = findElementInXLSheet(getParameterFixedAsset, "createNewAsset btn");
		btn_AssetBookSplit1 = findElementInXLSheet(getParameterFixedAsset, "AssetBookSplit1_btn");
		txt_usefulLifeSplit1 = findElementInXLSheet(getParameterFixedAsset, "usefullifeSplit1_txt");
		label_fixedAssetDisposal = findElementInXLSheet(getParameterFixedAsset, "fixedAssetDisposal_label");
		
		btn_existingBook = findElementInXLSheet(getParameterFixedAsset, "existingBook_btn");
		txt_fixedAssetInfoNo3 = findElementInXLSheet(getParameterFixedAsset, "fixedAssetInfoNo3_txt");
		doubleClickFixedAssetInfoNo3 = findElementInXLSheet(getParameterFixedAsset, "doubleClick FixedAsset3");
		
		doubleClickFixedAssetInfoNoDisposal = findElementInXLSheet(getParameterFixedAsset, "doubleClick noDisposal");
		
		txt_disposalDateSelling = findElementInXLSheet(getParameterFixedAsset, "disposalDateSelling_txt");
		doubleClick_disposalDateSelling = findElementInXLSheet(getParameterFixedAsset, "disposalDateSelling_doubleclick");
		txt_disposalModeSelling = findElementInXLSheet(getParameterFixedAsset, "disposalModeSelling_txt");
		btn_applyAssetInfoSelling = findElementInXLSheet(getParameterFixedAsset, "applyAssetInfoSelling_btn");
		
		btn_applyAssetInfoDisposal = findElementInXLSheet(getParameterFixedAsset, "applyAssetInfoDisposal_btn");
		
		
		txt_freeTextInvoice = findElementInXLSheet(getParameterFixedAsset, "freeTextInvoice_txt");
		btn_customer = findElementInXLSheet(getParameterFixedAsset, "customer_btn");
		txt_customer = findElementInXLSheet(getParameterFixedAsset, "customer_txt");
		doubleClickCustomer = findElementInXLSheet(getParameterFixedAsset, "doubleclick customer");
		txt_salesUnit = findElementInXLSheet(getParameterFixedAsset, "salesunit_txt");
		label_freeTextInvoice = findElementInXLSheet(getParameterFixedAsset, "freeTextInvoice_label");
		txt_amountText = findElementInXLSheet(getParameterFixedAsset, "amountText_txt");
		
		txt_dispatchOrderNo = findElementInXLSheet(getParameterFixedAsset, "dispatchOrderNo_txt");
		btn_reports = findElementInXLSheet(getParameterFixedAsset, "reports_btn");
		btn_templatebtn = findElementInXLSheet(getParameterFixedAsset, "templatebtn_btn");
		btn_reportTmpWiz = findElementInXLSheet(getParameterFixedAsset, "reportTmpWiz_btn");
		txt_productCode = findElementInXLSheet(getParameterFixedAsset, "productCode_txt");
		btn_quickPreview = findElementInXLSheet(getParameterFixedAsset, "quickPreview_btn");
		
		btn_AssetBooks = findElementInXLSheet(getParameterFixedAsset, "AssetBooks_btn");
		btn_FABook = findElementInXLSheet(getParameterFixedAsset, "FABook_btn");
		txt_netBookNo1 = findElementInXLSheet(getParameterFixedAsset, "netBookNo1_txt");
		txt_netBookNo2 = findElementInXLSheet(getParameterFixedAsset, "netBookNo2_txt");
		
		txt_credit = findElementInXLSheet(getParameterFixedAsset, "credit_txt");
		txt_debit = findElementInXLSheet(getParameterFixedAsset, "debit_txt");
		
		txt_docValue = findElementInXLSheet(getParameterFixedAsset, "docValue_txt");
		txt_stockValue = findElementInXLSheet(getParameterFixedAsset, "stockValue_txt");
		
		
		//SMOKE Test case properties
		btn_fixedAssetBookCreation = findElementInXLSheet(getParameterFixedAsset, "fixedAssetBookCreation_btn");
		btn_newfixedAssetBook = findElementInXLSheet(getParameterFixedAsset, "newfixedAssetBook_btn");
		txt_assetBookCode = findElementInXLSheet(getParameterFixedAsset, "assetBookCode_txt");
		btn_desc = findElementInXLSheet(getParameterFixedAsset, "desc_btn");
		txt_desc = findElementInXLSheet(getParameterFixedAsset, "desc_txt");
		txt_usefulLife1 = findElementInXLSheet(getParameterFixedAsset, "usefulLife1_txt");
		btn_dateA = findElementInXLSheet(getParameterFixedAsset, "dateA_btn");
		btn_aquisitionDate = findElementInXLSheet(getParameterFixedAsset, "aquisitionDate_btn");
		btn_dateB = findElementInXLSheet(getParameterFixedAsset, "dateB_btn");
		btn_depreciationClick = findElementInXLSheet(getParameterFixedAsset, "depreciationClick_btn");
		btn_depreciationStartDate = findElementInXLSheet(getParameterFixedAsset, "depreciationStartDate_btn");
		btn_scheduleCode = findElementInXLSheet(getParameterFixedAsset, "scheduleCode_btn");
		txt_scheduleCode = findElementInXLSheet(getParameterFixedAsset, "scheduleCode_txt");
		doubleClickscheduleCode = findElementInXLSheet(getParameterFixedAsset, "doubleClickschedule Code");
		btn_responsiblePerson = findElementInXLSheet(getParameterFixedAsset, "responsiblePerson_btn");
		txt_responsiblePerson = findElementInXLSheet(getParameterFixedAsset, "responsiblePerson_txt");
		doubleClickresponsiblePerson1 = findElementInXLSheet(getParameterFixedAsset, "doubleClickresponsible Person1");
		btn_applyBook = findElementInXLSheet(getParameterFixedAsset, "applyBook_btn");
		label_fixedAssetBook = findElementInXLSheet(getParameterFixedAsset, "fixedAssetBook_label");
		
		
		//smoke test case 02
		btn_plus1 = findElementInXLSheet(getParameterFixedAsset, "plus1_btn");
		btn_plus2 = findElementInXLSheet(getParameterFixedAsset, "plus2_btn");
		txt_group = findElementInXLSheet(getParameterFixedAsset, "group_txt");
		txt_GroupName = findElementInXLSheet(getParameterFixedAsset, "GroupName_txt");
		txt_Book = findElementInXLSheet(getParameterFixedAsset, "Book_txt");
		btn_fixedAssetBooks = findElementInXLSheet(getParameterFixedAsset, "fixedAssetBooks_btn");
		txt_fixedAssetBooks = findElementInXLSheet(getParameterFixedAsset, "fixedAssetBooks_txt");
		doubleClickfixedAssetBook = findElementInXLSheet(getParameterFixedAsset, "doubleClick fixedAssetBook");
		btn_post = findElementInXLSheet(getParameterFixedAsset, "post_btn");
		btn_bookView = findElementInXLSheet(getParameterFixedAsset, "bookView_btn");
		txt_aquisitionValue = findElementInXLSheet(getParameterFixedAsset, "aquisitionValue_txt");
		label_GroupConfig = findElementInXLSheet(getParameterFixedAsset, "GroupConfig_label");
		btn_fixedAssetGroupConfig = findElementInXLSheet(getParameterFixedAsset, "fixedAssetGroupConfig_btn");
		btn_newFixedAssetGroupConfig = findElementInXLSheet(getParameterFixedAsset, "newFixedAssetGroupConfig_btn");
		btn_applyGroup = findElementInXLSheet(getParameterFixedAsset, "applyGroup_btn");
		assetGroup_tbl = findElementInXLSheet(getParameterFixedAsset, "tbl_assetGroup");
		
		//smoke test case 04
		btn_newFixedAssetInformation = findElementInXLSheet(getParameterFixedAsset, "newFixedAssetInformation_btn");
		txt_InfoDescription = findElementInXLSheet(getParameterFixedAsset, "InfoDescription_txt");
		btn_fixedAssetBooksearch = findElementInXLSheet(getParameterFixedAsset, "fixedAssetBooksearch_btn");
		txt_quantity = findElementInXLSheet(getParameterFixedAsset, "quantity_txt");
		txt_perUnitCost = findElementInXLSheet(getParameterFixedAsset, "perUnitCost_txt");
		txt_barcodeBook = findElementInXLSheet(getParameterFixedAsset, "barcodeBook_txt");
		btn_assetBookTab = findElementInXLSheet(getParameterFixedAsset, "assetBookTab_btn");
		btn_disposalAndDepreciation = findElementInXLSheet(getParameterFixedAsset, "disposalAndDepreciation_btn");
		btn_scheduleCodeSearch = findElementInXLSheet(getParameterFixedAsset, "scheduleCodeSearch_btn");
		btn_fixedAssetInformationCreate = findElementInXLSheet(getParameterFixedAsset, "fixedAssetInformationCreate_btn");
		txt_AssetGroupName = findElementInXLSheet(getParameterFixedAsset, "AssetGroupName_txt");
		doubleClickscheduleCodeInfo = findElementInXLSheet(getParameterFixedAsset, "doubleClickschedule CodeInfo");
		btn_applyInfo = findElementInXLSheet(getParameterFixedAsset, "applyInfo_btn");
		label_fixedAssetInfoRelease = findElementInXLSheet(getParameterFixedAsset, "fixedAssetInfoRelease_label");
		
		
		//action verification
		txt_draft = findElementInXLSheet(getParameterFixedAsset, "txt_draft");
		btn_Delete = findElementInXLSheet(getParameterFixedAsset, "btn_Delete");
		txt_delete = findElementInXLSheet(getParameterFixedAsset, "txt_delete");
		btn_duplicate = findElementInXLSheet(getParameterFixedAsset, "btn_duplicate");
		txt_duplicate = findElementInXLSheet(getParameterFixedAsset, "txt_duplicate");
		txt_edit = findElementInXLSheet(getParameterFixedAsset, "txt_edit");
		btn_updateNNew = findElementInXLSheet(getParameterFixedAsset, "btn_updateNNew");
		txt_updateNNewBOM = findElementInXLSheet(getParameterFixedAsset, "txt_updateNNewBOM");
		txt_groupName = findElementInXLSheet(getParameterFixedAsset, "txt_groupName");
		txt_update = findElementInXLSheet(getParameterFixedAsset, "txt_update");
		btn_Update = findElementInXLSheet(getParameterFixedAsset, "btn_Update");
		btn_draftNNew = findElementInXLSheet(getParameterFixedAsset, "btn_draftNNew");
		txt_draftNNew = findElementInXLSheet(getParameterFixedAsset, "txt_draftNNew");
		btn_copyFrom = findElementInXLSheet(getParameterFixedAsset, "btn_copyFrom");
		txt_copyFrom = findElementInXLSheet(getParameterFixedAsset, "txt_copyFrom");
		txt_fxGroup = findElementInXLSheet(getParameterFixedAsset, "txt_fxGroup");
		doublfxGroup = findElementInXLSheet(getParameterFixedAsset, "doublfxGroup");
		txt_fxBook = findElementInXLSheet(getParameterFixedAsset, "txt_fxBook");
		btn_fxGroup = findElementInXLSheet(getParameterFixedAsset, "btn_fxGroup");
		label_fixedAssetBookdraft = findElementInXLSheet(getParameterFixedAsset, "label_fixedAssetBookdraft");
		doublfxcopy = findElementInXLSheet(getParameterFixedAsset, "doublfxcopy");
		txt_fxcopy = findElementInXLSheet(getParameterFixedAsset, "txt_fxcopy");
		txt_copyFromBook = findElementInXLSheet(getParameterFixedAsset, "txt_copyFromBook");
		btn_newfixedAssetInfo = findElementInXLSheet(getParameterFixedAsset, "btn_newfixedAssetInfo");
		txt_fixedAssetInfoDesc = findElementInXLSheet(getParameterFixedAsset, "txt_fixedAssetInfoDesc");
		btn_plusMark = findElementInXLSheet(getParameterFixedAsset, "btn_plusMark");
		doubleClickschedulecode = findElementInXLSheet(getParameterFixedAsset, "doubleClickschedulecode");
		txt_resourceCode = findElementInXLSheet(getParameterFixedAsset, "txt_resourceCode");
		txt_resourceDesc = findElementInXLSheet(getParameterFixedAsset, "txt_resourceDesc");
		txt_resourceGroup = findElementInXLSheet(getParameterFixedAsset, "txt_resourceGroup");
		btn_resource = findElementInXLSheet(getParameterFixedAsset, "btn_resource");
		txt_fxInfoNo = findElementInXLSheet(getParameterFixedAsset, "txt_fxInfoNo");
		btn_barCode = findElementInXLSheet(getParameterFixedAsset, "btn_barCode");
		txt_barcode = findElementInXLSheet(getParameterFixedAsset, "txt_barcode");
		btn_transfer = findElementInXLSheet(getParameterFixedAsset, "btn_transfer");
		txt_transfer = findElementInXLSheet(getParameterFixedAsset, "txt_transfer");
		txt_book = findElementInXLSheet(getParameterFixedAsset, "txt_book");
		
		//master test cases properties
		btn_vendorInfo = findElementInXLSheet(getParameterFixedAsset, "btn_vendorInfo");
		newVendor = findElementInXLSheet(getParameterFixedAsset, "newVendor");
		txt_vendorType = findElementInXLSheet(getParameterFixedAsset, "txt_vendorType");
		txt_vendorName = findElementInXLSheet(getParameterFixedAsset, "txt_vendorName");
		txt_vendorName1 = findElementInXLSheet(getParameterFixedAsset, "txt_vendorName1");
		txt_vendorGroup  = findElementInXLSheet(getParameterFixedAsset, "txt_vendorGroup");
		btn_warehouseInfo = findElementInXLSheet(getParameterFixedAsset, "btn_warehouseInfo");
		btn_newWarehouse = findElementInXLSheet(getParameterFixedAsset, "btn_newWarehouse");
		txt_warehouseCode = findElementInXLSheet(getParameterFixedAsset, "txt_warehouseCode");
		txt_wraehousename = findElementInXLSheet(getParameterFixedAsset, "txt_wraehousename");
		
		btn_CheckoutBtn = findElementInXLSheet(getParameterFixedAsset, "CheckoutBtn");
		btn_DraftBtn = findElementInXLSheet(getParameterFixedAsset, "DraftBtn");
		btn_ReleaseBtn = findElementInXLSheet(getParameterFixedAsset, "ReleaseBtn");
		btn_GoToPageBtn = findElementInXLSheet(getParameterFixedAsset, "GoToPageBtn");
		txt_pageDraft = findElementInXLSheet(getParameterFixedAsset, "pageDraft");
		txt_pageRelease = findElementInXLSheet(getParameterFixedAsset, "pageRelease");
		txt_DocHeader = findElementInXLSheet(getParameterFixedAsset, "DocHeader");
		btn_ActionBtn = findElementInXLSheet(getParameterFixedAsset, "ActionBtn");
		txt_pageDelete = findElementInXLSheet(getParameterFixedAsset, "pageDelete");
		btn_ReverseBtn = findElementInXLSheet(getParameterFixedAsset, "ReverseBtn");
		btn_ReversePopupBtn = findElementInXLSheet(getParameterFixedAsset, "ReversePopupBtn");
		txt_pageReverse = findElementInXLSheet(getParameterFixedAsset, "pageReverse");

	}
	
	
	public static void readData() throws Exception
	{
		String computerName = System.getenv().get("COMPUTERNAME");
		if (computerName.equals("DESKTOP-C6E8CJU") || computerName.equals("MADHUSHAN-LAP")) {
			siteURL = findElementInXLSheet(getDataInvertoryAndWarehouse, "site url external");
		} else {
			siteURL = findElementInXLSheet(getDataInvertoryAndWarehouse, "site url");
		}
		UserNameData=findElementInXLSheet(getDataFixedAsset,"user name data");
		PasswordData=findElementInXLSheet(getDataFixedAsset,"password data");
		
		vendorData = findElementInXLSheet(getDataFixedAsset, "vendor data");
		vendorRefData = findElementInXLSheet(getDataFixedAsset, "vendor ref no");
		Product1 = findElementInXLSheet(getDataFixedAsset, "Product1");
		Product2 = findElementInXLSheet(getDataFixedAsset, "Product2");

		productsData = findElementInXLSheet(getDataFixedAsset, "product data");
		qtyData = findElementInXLSheet(getDataFixedAsset, "qty data");
		unitPriceData = findElementInXLSheet(getDataFixedAsset, "unit price data");
		assetGrpData = findElementInXLSheet(getDataFixedAsset, "asset Grp Data");
		titleData = findElementInXLSheet(getDataFixedAsset, "title data");
		requesterData = findElementInXLSheet(getDataFixedAsset, "requester data");
		dateData = findElementInXLSheet(getDataFixedAsset, "date data");
		productData1 = findElementInXLSheet(getDataFixedAsset, "product data1");
		internalqtyData = findElementInXLSheet(getDataFixedAsset, "internal_qty_data");
		shippingData = findElementInXLSheet(getDataFixedAsset, "shippingdata");
		projectCodeData = findElementInXLSheet(getDataFixedAsset, "project code");
		projectTitleData = findElementInXLSheet(getDataFixedAsset, "project title");
		projectGroupData = findElementInXLSheet(getDataFixedAsset, "project group");
		postingMethodData = findElementInXLSheet(getDataFixedAsset, "posting method");
		projectTypeData = findElementInXLSheet(getDataFixedAsset, "project type");
		assetGroupData = findElementInXLSheet(getDataFixedAsset, "assetGroup data");
		responsiblePerson = findElementInXLSheet(getDataFixedAsset, "responsiblePerson data");
		bussinessUnitData = findElementInXLSheet(getDataFixedAsset, "businessUnit data");
		pricingProfileData = findElementInXLSheet(getDataFixedAsset, "pricingProfile data");
		projectLocationData = findElementInXLSheet(getDataFixedAsset, "projectLocation data");
		inputwareData = findElementInXLSheet(getDataFixedAsset, "inputWare data");
		WIPWareData = findElementInXLSheet(getDataFixedAsset, "WIPWare data");
		taskNameData = findElementInXLSheet(getDataFixedAsset, "taskName data");
		vendorDetailsData = findElementInXLSheet(getDataFixedAsset, "vendorDetails data");
		GLAccData = findElementInXLSheet(getDataFixedAsset, "GLAcc data");
		amountData = findElementInXLSheet(getDataFixedAsset, "amount data");
		vendorRefNoData = findElementInXLSheet(getDataFixedAsset, "vendorRefNo data");
		costCenterData = findElementInXLSheet(getDataFixedAsset, "costCenter data");
		percentageData = findElementInXLSheet(getDataFixedAsset, "percentage data");
		costAssignmentData = findElementInXLSheet(getDataFixedAsset, "costAssignment data");
		costObjectData = findElementInXLSheet(getDataFixedAsset, "costObject data");
		taskData = findElementInXLSheet(getDataFixedAsset, "task data");
		POCData = findElementInXLSheet(getDataFixedAsset, "POC data");
		
		
		fixedAssetBookNoData = findElementInXLSheet(getDataFixedAsset, "fixedAssetBookNo data");
		templateData = findElementInXLSheet(getDataFixedAsset, "template data");
		fixedAssetInfo = findElementInXLSheet(getDataFixedAsset, "fixedAsset Info data");
		fixedAssetInfo1 = findElementInXLSheet(getDataFixedAsset, "fixedAsset info1 data");
		fixedAssetInfo2 = findElementInXLSheet(getDataFixedAsset, "fixed asset info2");
		AssetBookData = findElementInXLSheet(getDataFixedAsset, "AssetBook data");
		RevalueAmountData = findElementInXLSheet(getDataFixedAsset, "Revalue Amount Data");
		usefulLifeData = findElementInXLSheet(getDataFixedAsset, "useful Life Data");
		
		disposalModeDataWriteOff = findElementInXLSheet(getDataFixedAsset, "disposal Mode Data");
		disposalModeDataWrite = findElementInXLSheet(getDataFixedAsset, "disposal mode data");
		
		AssetBookDataSplit1 = findElementInXLSheet(getDataFixedAsset, "AssetBookSlit1 data");
		assetDescriptionData = findElementInXLSheet(getDataFixedAsset, "assetDescription data");
		usefulLifeDataSplit1 = findElementInXLSheet(getDataFixedAsset, "usefullifeSplit1 data");
		
		fixedAssetInfo3 = findElementInXLSheet(getDataFixedAsset, "fixedAssetInfo3 data");
		assetInfoLookupData = findElementInXLSheet(getDataFixedAsset, "assetInfoLookup data");
		
		customerData = findElementInXLSheet(getDataFixedAsset, "customer Data");
		salesUnitData = findElementInXLSheet(getDataFixedAsset, "salesUnit Data");
		amountDataText = findElementInXLSheet(getDataFixedAsset, "amountText data");
		
		
		//SMOKE Test case data
		
		assetBookCodeData = findElementInXLSheet(getDataFixedAsset, "assetBookCode Data");
		assetBookCodeDataCreate = findElementInXLSheet(getDataFixedAsset, "assetBookCode Data");
		descData = findElementInXLSheet(getDataFixedAsset, "desc Data");
		usefulLifeData1 = findElementInXLSheet(getDataFixedAsset, "usefulLife Data1");
		scheduleCodeData = findElementInXLSheet(getDataFixedAsset, "scheduleCode Data");
		responsiblePersonData = findElementInXLSheet(getDataFixedAsset, "responsiblePerson Data");
		
		//test case 02
		groupData = findElementInXLSheet(getDataFixedAsset, "group Data");
		fixedAssetBooksData = findElementInXLSheet(getDataFixedAsset, "fixedAssetBooks Data");
		quisitionData = findElementInXLSheet(getDataFixedAsset, "quisition Data");
		
		//test case 04
		InfoDescriptionData = findElementInXLSheet(getDataFixedAsset, "InfoDescription Data");
		quantityData = findElementInXLSheet(getDataFixedAsset, "quantity Data");
		perUnitCostData = findElementInXLSheet(getDataFixedAsset, "perUnitCost Data");
		
		//action verification data
		fxGroupData = findElementInXLSheet(getDataFixedAsset, "fxGroupData");
		assetBookCodeData1 = findElementInXLSheet(getDataFixedAsset, "assetBookCodeData1");
		fxcopyData = findElementInXLSheet(getDataFixedAsset, "fxcopyData");
		fixedAssetInfoDescData = findElementInXLSheet(getDataFixedAsset, "fixedAssetInfoDescData");
		resourceCodeData = findElementInXLSheet(getDataFixedAsset, "resourceCodeData");
		resourceDescData = findElementInXLSheet(getDataFixedAsset, "resourceDescData");
		
		//master test cases data
		vendorName1Data = findElementInXLSheet(getDataFixedAsset, "vendorName1Data");
		warehouseCodeData = findElementInXLSheet(getDataFixedAsset, "warehouseCodeData");
		warehousenameData = findElementInXLSheet(getDataFixedAsset, "warehousenameData");
	}
	
	
	//action verification properties
	protected static String txt_draft;
	protected static String btn_Delete;
	protected static String txt_delete;
	protected static String btn_duplicate;
	protected static String txt_duplicate;
	protected static String txt_edit;
	protected static String btn_updateNNew;
	protected static String txt_updateNNewBOM;
	protected static String txt_groupName;
	protected static String btn_Update;
	protected static String txt_update;
	protected static String btn_draftNNew;
	protected static String txt_draftNNew;
	protected static String btn_copyFrom;
	protected static String txt_copyFrom;
	protected static String txt_fxGroup;
	protected static String doublfxGroup;
	protected static String txt_fxBook;
	protected static String btn_fxGroup;
	protected static String label_fixedAssetBookdraft;
	protected static String txt_fxcopy;
	protected static String doublfxcopy;
	protected static String txt_copyFromBook;
	protected static String btn_newfixedAssetInfo;
	protected static String txt_fixedAssetInfoDesc;
	protected static String btn_plusMark;
	protected static String doubleClickschedulecode;
	protected static String txt_resourceCode;
	protected static String txt_resourceDesc;
	protected static String txt_resourceGroup;
	protected static String btn_resource;
	protected static String txt_fxInfoNo;
	protected static String btn_barCode;
	protected static String txt_barcode;
	protected static String btn_transfer;
	protected static String txt_transfer;
	protected static String txt_book;
	
	
	//action verification data

	protected static String fxGroupData;
	protected static String assetBookCodeData1;
	protected static String fxcopyData;
	protected static String fixedAssetInfoDescData;
	protected static String resourceCodeData;
	protected static String resourceDescData;
	
	
	//master test cases properties
	protected static String btn_vendorInfo;
	protected static String newVendor;
	protected static String txt_vendorType;
	protected static String txt_vendorName;
	protected static String txt_vendorName1;
	protected static String txt_vendorGroup;
	protected static String btn_warehouseInfo;
	protected static String btn_newWarehouse;
	protected static String txt_warehouseCode;
	protected static String txt_wraehousename;
	
	
	//master test cases data
	  
	protected static String vendorName1Data;
	protected static String warehouseCodeData;
	protected static String warehousenameData;
}
