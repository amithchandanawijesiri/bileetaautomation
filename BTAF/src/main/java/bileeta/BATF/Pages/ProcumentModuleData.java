package bileeta.BATF.Pages;

import bileeta.BTAF.Utilities.TestBase;

public class ProcumentModuleData extends TestBase {

	/* Reading the element locators to variables */
	// Com_TC_001
	protected static String siteURL;
	protected static String siteLogo;
	protected static String txt_username;
	protected static String txt_password;
	protected static String btn_login;
	protected static String lnk_home;
	// Com_TC_002
	protected static String btn_nav;
	protected static String sideMenu;
	protected static String subsideMenu;
	protected static String btn_probutton;
	// PR_VGC_001
	protected static String btn_vengroupbutton;
	protected static String btn_newvengroupbutton;
	protected static String txt_VendorGroupConfiguration;
	protected static String txt_newvendorgrouppage;
	// PR_VGC_002
	protected static String txt_vendorgroup;
	protected static String txt_currency;
	protected static String btn_btndetails;
	protected static String txt_PurchaseTaxGroup;
	protected static String txt_VendorClassification;
	protected static String txt_StandardDeliveryMode;
	protected static String txt_StandardDeliveryTerm;
	protected static String txt_Warehouse;
	protected static String btn_PartialDelivery;
	protected static String btn_ConsolidatedDelivery;
	protected static String btn_btnpaymentinfo;
	protected static String txt_PaymentAgreement;
	protected static String txt_PaymentTerm;
	protected static String txt_Tolerance;
	protected static String txt_RegulationTaxGroup;
	protected static String txt_CreditDays;
	protected static String txt_CreditLimit;
	protected static String btn_btndraft;
	protected static String btn_VendorGroupConfiguration;
	protected static String txt_VendorGroupConfigurationNumber;
	protected static String txt_VendorGroupConfigurationSerch;
	protected static String sel_selVendorGroupConfiguration;

	// PR_VGC_003
	protected static String btn_vendorinfo;
	protected static String btn_release;
	// PR_VI_001
	protected static String btn_veninfobutton;
	protected static String btn_newvenbutton;
	protected static String txt_VendorInformationpage;
	protected static String txt_newVendorInformationpage;
	// PR_VI_002
	protected static String txt_VendorType;
	protected static String txt_VendorNameTitle;
	protected static String txt_VendorName;
	protected static String btn_ReceivableAccount;
	protected static String txt_ReceivableAccount;
	protected static String sel_ReceivableAccount;
	protected static String txt_ContactTitle;
	protected static String txt_ContactName;
	protected static String txt_MobileNumber;
	protected static String btn_DetailsTab;
	protected static String txt_SelectIndustry;
	protected static String txt_TaxRegistrationNumber;
	protected static String txt_ReferenceAccoutNo;
	protected static String btn_3rdPartyAccount;
	protected static String txt_3rdPartyAccount;
	protected static String sel_3rdPartyAccount;
	protected static String txt_PassportNo;
	protected static String txt_newVendorName;
	protected static String btn_VendorInformation;
	protected static String txt_VendorInformationserch;
	protected static String sel_selVendorInformation;
	protected static String btn_newReceivableAccount;
	protected static String txt_newReceivableAccountname;
	protected static String txt_AccountGroup;
	protected static String btn_btnUpdate;
	protected static String btn_btnReceivableAccountclose;

	// PR_VI_003
	protected static String btn_vendor;

	// PM_VA_001
	protected static String VendorAgreementbutton;
	protected static String VendorAgreementpage;
	protected static String newVendorAgreementbutton;
	protected static String newVendorAgreementpage;
	protected static String txt_AgreementNo;
	protected static String txt_AgreementTitle;
	protected static String btn_btnAgreementDate;
	protected static String btn_selAgreementDate;
	protected static String txt_AgreementGroup;
	protected static String txt_ACurrency;
	protected static String txt_ValidityPeriodFor;
	protected static String txt_Description;
	protected static String btn_btnVendorAccount;
	protected static String txt_txtVendorAccount;
	protected static String sel_selVendorAccount;
	protected static String btn_btnproductdetails;
	protected static String txt_ProductRelation;
	protected static String btn_btnproduct;
	protected static String txt_txtproduct;
	protected static String sel_selproduct;
	protected static String txt_minOrderQty;
	protected static String txt_maxOrderQty;
	protected static String btn_btnupdatetask;
	protected static String btn_btnAssignTo;
	protected static String txt_txtAssignTo;
	protected static String sel_selAssignTo;
	protected static String txt_txtsubject;

	// PM_PR_001
	protected static String btn_PurchaseRequisitionbutton;
	protected static String txt_PurchaseRequisitionbuttonpage;
	protected static String btn_newPurchaseRequisitionbutton;
	protected static String txt_newPurchaseRequisitionbuttonpage;
	// PM_PR_002
	protected static String btn_RequestDate;
	protected static String txt_RCurrency;
	protected static String btn_Rbtnproduct;
	protected static String txt_Rtxtproduct;
	protected static String sel_Rselproduct;
	protected static String btn_btnCheckout;
	protected static String btn_selRequestDate;
	protected static String txt_PurchaseRequisitionNumber;
	protected static String sel_selPurchaseRequisitionNumber;
	protected static String lnkInforOnLookupInformationReplace;

	// PM_PR_003
	protected static String btn_btnPurchaseRequisition;

	// PM_PR_004
	protected static String btn_RbtnGridMasterInfo;
	protected static String btn_RbtnProductPurchasingHistory;
	protected static String txt_RPHUnitPrice;
	protected static String btn_RPHclose;
	protected static String txt_RUnitPrice;

	// PM_PR_005
	protected static String btn_btnRelPurchaseRequisition;
	protected static String btn_btnAction;
	protected static String btn_btnConverttoPurchaseOrder;
	protected static String btn_btnGeneratePO;
	protected static String btn_btnPOtoShipmentCosting;
	protected static String btn_RRDclose;
	protected static String btn_btnVendor;
	protected static String txt_txtVendor;
	protected static String sel_selVendor;
	protected static String btn_btnBillingAddress;
	protected static String txt_txtBillingAddress;
	protected static String btn_btnApply;
	protected static String btn_btnShippingAddress;
	protected static String txt_txtShippingAddress;

	// PM_PO_001
	protected static String btn_PurchaseOrderbutton;
	protected static String txt_PurchaseOrderpage;
	protected static String btn_newPurchaseOrderbutton;
	protected static String txt_newPurchaseOrderpage;
	protected static String txt_InboundShipmentpage;
	protected static String txt_PurchaseInvoicepage;
	protected static String txt_ShipmentCostingInformationpage;
	protected static String btn_PbtnVendor;
	protected static String btn_Pbtndetails;
	protected static String btn_btnPInvoiceAccount;
	protected static String btn_btnsummary;
	protected static String btn_pbtnproduct;
	protected static String txt_ptxtproduct;
	protected static String sel_pselproduct;
	protected static String btn_btnGotopage;
	protected static String btn_SerialRange;

	protected static String txt_pro7POqty1;
	protected static String txt_pro7POqty2;
	protected static String txt_pro7POqty3;
	protected static String txt_pro7POqty4;
	protected static String txt_pro7POqty5;
	protected static String txt_pro7POqty6;
	protected static String txt_pro7POqty7;

	protected static String btn_pro7POLook1;
	protected static String btn_pro7POLook2;
	protected static String btn_pro7POLook3;
	protected static String btn_pro7POLook4;
	protected static String btn_pro7POLook5;
	protected static String btn_pro7POLook6;
	protected static String btn_pro7POLook7;
	protected static String btn_pro7PILook1;
	protected static String btn_pro7PILook2;
	protected static String btn_pro7PILook3;
	protected static String btn_pro7PILook4;
	protected static String btn_pro7PILook5;
	protected static String btn_pro7PILook6;
	protected static String btn_pro7PILook7;
	protected static String btn_pro7ISLook1;
	protected static String btn_pro7ISLook2;
	protected static String btn_pro7ISLook3;
	protected static String btn_pro7ISLook4;
	protected static String btn_pro7ISLook5;
	protected static String btn_pro7ISLook6;
	protected static String btn_pro7ISLook7;

	protected static String btn_POProductAvailability;
	protected static String txt_POProductAvailabilityValue;
	protected static String btn_pro7SCILook1;

	// PM_PO_002
	protected static String btn_btnPOtoPIService;
	protected static String txt_txtVendorReferenceNo;

	// PM_PO_003
	protected static String btn_btnLPOtoShipmentCosting;
	protected static String btn_btndropdown;

	// PM_PO_005
	protected static String btn_btnConsolidatedPO;
	protected static String btn_btnDocumentList;
	protected static String btn_btnproductline1;
	protected static String btn_btnproductline2;
	protected static String btn_btnPApply;

	// PM_PO_006
	protected static String btn_btnaddProduct;
	protected static String btn_pbtnNxtproduct;
	protected static String txt_ptxtNxtproduct;
	protected static String btn_btnCostAllocation;
	protected static String btn_ImportCostEstimationShipmentbutton;
	protected static String txt_txtCEstDescription;
	protected static String txt_txtCostingType;
	protected static String txt_EstimateCost;
	protected static String btn_Apportionmentbutton;
	protected static String txt_ApportionmentBasis;
	protected static String btn_AppoCheckout;
	protected static String btn_AppoApply;
	protected static String btn_btnGenerateEstimation;
	protected static String btn_btnApplytomaingrid;
	protected static String btn_ApponxtCheckout;
	protected static String txt_ptxtBproduct;
	protected static String btn_btnSerialBatch;
	protected static String btn_btnselProductList;
	protected static String txt_BatchNo;
	protected static String txt_SerialNo;
	protected static String btn_updateSerial;
	protected static String btn_closeProductList;
	protected static String btn_ExpiryDate;
	protected static String btn_selExpiryDate;

	// PM_PO_007
	protected static String btn_ImportEstimationbutton;
	protected static String btn_addNewEstimationbutton;
	protected static String txt_txtQESTDescription;
	protected static String txt_EstimatedQty;
	protected static String btn_EstimationDetailsbutton;
	protected static String txt_CostingType;
	protected static String txt_EstimatenewCost;
	protected static String btn_Summary;
	protected static String btn_Checkout;
	protected static String btn_goPurchaseRequisitionpage;
	protected static String txt_serchPurchaseRequisition;
	protected static String btn_btnnxtGenerateEstimation;

	// PM_PI_001
	protected static String btn_PurchaseInvoicebutton;
	protected static String txt_PurchaseInvoicebuttonpage;
	protected static String btn_newPurchaseInvoicebutton;
	protected static String txt_newPurchaseInvoicebuttonpage;
	protected static String btn_btnPIToShipmentCosting;
	protected static String txt_PIcurrency;
	protected static String btn_btnPIVendor;
	protected static String txt_txtPIVendor;
	protected static String sel_selPIVendor;
	protected static String btn_btnPIBillingAddress;
	protected static String txt_txtPIBillingAddress;
	protected static String btn_btnPIShippingAddress;
	protected static String txt_txtPIShippingAddress;
	protected static String btn_PIbtnproduct;
	protected static String txt_PItxtproduct;
	protected static String sel_PIselproduct;
	protected static String btn_PIbtnNxtproduct;
	protected static String txt_PItxtNxtproduct;
	protected static String btn_PIbtnaddProduct;
	// PM_PI_002
	protected static String btn_btnPIToInboundShipment;
	// PM_PI_003
	protected static String btn_btnPIService;
	protected static String btn_btnPIsummary;
	protected static String txt_PItxtSproduct;

	// PM_PRO_001
	protected static String btn_PurchaseReturnOrderbutton;
	protected static String btn_newPurchaseReturnOrderbutton;
	protected static String btn_btnPROVendor;
	protected static String txt_txtPROVendor;
	protected static String sel_selPROVendor;
	protected static String btn_documentlistbutton;
	protected static String btn_PROsearch;
	protected static String btn_PROcheckbox;
	protected static String btn_PROapply;
	protected static String txt_PROqty;
	protected static String btn_PROdraft;
	protected static String btn_btnPOBillingAddress;
	protected static String txt_txtPOBillingAddress;
	protected static String btn_btnPOShippingAddress;
	protected static String txt_txtPOShippingAddress;
	protected static String btn_btnPOApply;
	protected static String btn_btnPOSerialNo;

	// PM_PRO_002
	protected static String btn_PObtnproduct;
	protected static String txt_POtxtproduct;
	protected static String sel_POselproduct;
	protected static String btn_POwarehouse;
	protected static String txt_POSerialNo;
	protected static String btn_POMasterInfo;
	protected static String btn_POProductBS;
	protected static String txt_POSNo;
	protected static String btn_POMasterInfoclose;
	protected static String btn_PROLook1;
	protected static String btn_PRILook1;
	protected static String txt_POProductAvailabilityValue1;

	protected static String txt_pageDraft;
	protected static String txt_pageRelease;

	// PM_RFQ_001
	protected static String btn_RequestForQuotation;
	protected static String btn_NewRequestForQuotation;
	protected static String txt_RequestForQuotationpage;
	protected static String txt_NewRequestForQuotationpage;
	protected static String btn_btncontactperson;
	protected static String txt_txtcontactperson;
	protected static String sel_selcontactperson;
	protected static String btn_btnRFQVendor;
	protected static String txt_txtRFQVendor;
	protected static String sel_selRFQVendor;
	protected static String btn_btnRFQproduct;
	protected static String txt_txtRFQproduct;
	protected static String sel_selRFQproduct;
	protected static String txt_RFQqty;
	protected static String btn_RFQdeadline;
	protected static String sel_selRFQdeadline;
	protected static String btn_QuotationTab;
	protected static String btn_RFQadd;
	protected static String txt_RFQaddVendor;
	protected static String txt_RFQQuotationNo;
	protected static String txt_RFQShippingTerm;
	protected static String txt_RFQUnitPrice;
	protected static String txt_RFQMinQty;
	protected static String txt_RFQMaxQty;
	protected static String btn_RFQcheckout;
	protected static String btn_RFQapply;
	protected static String btn_RFQsummarytab;
	protected static String btn_qualifiedvendorbutton;
	protected static String btn_Qualifiedtik;
	protected static String btn_RFQQualifiedapply;
	protected static String btn_Update;
	protected static String btn_btnbackground;
	protected static String btn_Edit;
	protected static String btn_btnRFQapply;

	// SMOKE_PRO_03
	protected static String btn_ImportCostEstimation;
	protected static String ImportCostEstimationpage;
	protected static String btn_newImportCostEstimation;
	protected static String newImportCostEstimationpage;
	protected static String txt_ICEDescription;
	protected static String btn_ICEProductbutoon;
	protected static String txt_ICEProducttxt;
	protected static String sel_ICEProductsel;
	protected static String btn_ICEEstimationDetails;
	protected static String txt_ICECostingType;
	protected static String txt_ICEEstimateCost;
	protected static String btn_ICESummary;
	protected static String btn_ICECheckout;
	protected static String btn_ICEDraft;
	protected static String btn_ICERelease;

	// SMOKE_PRO_05
	protected static String btn_PurchaseDesk;
	protected static String PurchaseDeskpage;
	protected static String sel_Requisitionsel;
	protected static String txt_SearchPRN;
	protected static String btn_SearchbuttonPRN;

	// SMOKE_PRO_10
	protected static String btn_DocFlow;

	// product7

	protected static String PRPlus1;
	protected static String PRPlus2;
	protected static String PRPlus3;
	protected static String PRPlus4;
	protected static String PRPlus5;
	protected static String PRPlus6;
	protected static String PRPro2;
	protected static String PRPro3;
	protected static String PRPro4;
	protected static String PRPro5;
	protected static String PRPro6;
	protected static String PRPro7;

	protected static String POPlus1;
	protected static String POPlus2;
	protected static String POPlus3;
	protected static String POPlus4;
	protected static String POPlus5;
	protected static String POPlus6;
	protected static String POPro2;
	protected static String POPro3;
	protected static String POPro4;
	protected static String POPro5;
	protected static String POPro6;
	protected static String POPro7;

	protected static String Cap1;
	protected static String Cap2;
	protected static String Cap4;
	protected static String Cap5;
	protected static String Cap6;
	protected static String Cap7;

	protected static String BatchNu;
	protected static String BatchLotNu;
	protected static String BatchExpiryDate;
	protected static String SerialExpiryDate;
	protected static String BatchExpiryDateSel;
	protected static String SerialFromNu;
	protected static String SerialLotNu;
	protected static String SerialBatchNu;

	protected static String PIPlus1;
	protected static String PIPro1;

	protected static String RFQPro1;
	protected static String RFQPlus1;
	protected static String RFQPlus2;
	protected static String RFQPlus3;
	protected static String RFQPlus4;
	protected static String RFQPlus5;
	protected static String RFQPlus6;

	protected static String RFQUnitPrice2;
	protected static String RFQUnitPrice3;
	protected static String RFQUnitPrice4;
	protected static String RFQUnitPrice5;
	protected static String RFQUnitPrice6;
	protected static String RFQUnitPrice7;

	protected static String RFQMinQty2;
	protected static String RFQMinQty3;
	protected static String RFQMinQty4;
	protected static String RFQMinQty5;
	protected static String RFQMinQty6;
	protected static String RFQMinQty7;

	protected static String RFQMaxQty2;
	protected static String RFQMaxQty3;
	protected static String RFQMaxQty4;
	protected static String RFQMaxQty5;
	protected static String RFQMaxQty6;
	protected static String RFQMaxQty7;

	protected static String Qualifiedtik2;
	protected static String Qualifiedtik3;
	protected static String Qualifiedtik4;
	protected static String Qualifiedtik5;
	protected static String Qualifiedtik6;
	protected static String Qualifiedtik7;

	protected static String nextdate;
	protected static String btn_btnPOtoPurchaseInvoice;

	protected static String pro7qty1;
	protected static String pro7qty2;
	protected static String pro7qty3;
	protected static String pro7qty4;
	protected static String pro7qty5;
	protected static String pro7qty6;
	protected static String pro7qty7;
	protected static String pro7price1;
	protected static String pro7price2;
	protected static String pro7price3;
	protected static String pro7price4;
	protected static String pro7price5;
	protected static String pro7price6;
	protected static String pro7price7;

	protected static String Warehouse1;
	protected static String Warehouse2;
	protected static String Warehouse3;
	protected static String Warehouse4;
	protected static String Warehouse5;
	protected static String Warehouse6;
	protected static String Warehouse7;

	protected static String WarehousePI1;
	protected static String WarehousePI2;
	protected static String WarehousePI3;
	protected static String WarehousePI4;
	protected static String WarehousePI5;
	protected static String WarehousePI6;
	protected static String WarehousePI7;

	protected static String pageDraft;
	protected static String pageRelease;
	protected static String Status;
	protected static String Header;

	protected static String Product;
	protected static String ProductQty;

	// Master_1
	protected static String btn_Delete;
	protected static String txt_DeleteText;
	protected static String btn_Yes;
	protected static String btn_History;
	protected static String txt_HistoryText;
	protected static String btn_Activities;
	protected static String txt_ActivitiesText;
	protected static String btn_headerclose;
	protected static String btn_Duplicate;
	protected static String txt_lblVendorGroup;
	protected static String txt_dupVendorGroup;
	protected static String btn_btnEdit;
	protected static String txt_ReleseText;
	protected static String btn_DraftAndNew;
	protected static String btn_UpdateAndNew;

	// Master_2
	protected static String btn_VendorGroupbtn;
	protected static String btn_VendorGroupAdd;
	protected static String txt_newVendorGroupTxt;
	protected static String btn_VendorGroupUpdate;
	protected static String btn_Reminders;
	protected static String txt_RemindersText;
	protected static String btn_ActivitiesHeaderclose;

	// Master_3
	protected static String btn_ConverttoPurchaseOrder;
	protected static String btn_Reverse;
	protected static String txt_ReverseText;
	protected static String btn_CopyFrom;
	protected static String txt_CopyFromText;

	// Master_4
	protected static String btn_CustomerDemand;
	protected static String btn_CustomerDemandsel;
	protected static String btn_GenerateProductionOrder;
	protected static String btn_GenerateAssemblyProposal;
	protected static String txt_GenerateProductionOrdertxt;
	protected static String txt_GenerateAssemblyProposaltxt;

	// Transaction_3
	protected static String btn_Complete;
	protected static String txt_CompleteText;

	// Transaction_4
	protected static String btn_AddNewDocument;
	protected static String ZoomBar;
	protected static String btn_No;
	protected static String btn_ReverseButton;
	protected static String btn_Print;
	protected static String txt_PrintoutTemplates;
	protected static String btn_Back;
	protected static String btn_RackwiseAvailable;

	// Transaction_5
	protected static String btn_Acknowledgement;
	protected static String txt_Acknowledgementtxt;
	protected static String btn_ChangeCostAllocations;
	protected static String txt_ChangeCostAllocationstxt;
	protected static String txt_PurchaseInvoicetxt;

	// Transaction_6
	protected static String btn_CostEstimation;
	protected static String txt_CostEstimationtxt;
	protected static String btn_Close;
	protected static String txt_Closetxt;
	protected static String btn_Hold;
	protected static String btn_DotheOutboundAdvance;
	protected static String txt_DotheOutboundAdvancetxt;
	protected static String btn_ExtendExpiryDate;
	protected static String txt_ExtendExpiryDatetxt;
	protected static String btn_UpdateLogisticDates;
	protected static String txt_UpdateLogisticDatestxt;
	protected static String btn_UnHold;
	protected static String txt_Reason;
	protected static String btn_OK;
	protected static String txt_PurchaseOrdertxt;
	protected static String btn_ImportShipment;
	protected static String txt_ImportShipmenttxt;
	protected static String btn_CostEstimationClose;

	// Transaction_8
	protected static String btn_GeneratePurchaseOrder;

	// Master_001
	protected static String btn_SalesAndMarketing;
	protected static String btn_AccountGroupConfiguration;
	protected static String btn_NewAccountGroupConfiguration;
	protected static String btn_addAccountGroup;
	protected static String btn_addNewAccountGroup;
	protected static String txt_NewAccountGrouptxt;
	protected static String btn_AccountGroupUpdate;
	protected static String sel_AccountGroupsel;
	protected static String btn_InventoryAndWarehousing;
	protected static String btn_WarehouseInformation;
	protected static String btn_NewWarehouse;
	protected static String txt_WarehouseCode;
	protected static String txt_WarehouseName;
	protected static String btn_AutoLot;
	protected static String txt_LotBookNo;

	protected static String btn_AgreementGroupAdd;
	protected static String btn_AgreementGroupParameters;
	protected static String txt_AgreementGroupParameterstxt;
	protected static String btn_AgreementGroupParametersUpdate;
	protected static String txt_QuaranWarehouse;

	// PM_PI_001to006Reg
	protected static String txt_PurchaseInvoicePageHeaderA;
	protected static String txt_NewPageHeaderTextA;
	protected static String txt_VendorFieldValidatorA;
	protected static String txt_CurrencyFieldValidatorA;
	protected static String btn_ProductGridErrorBtnA;
	protected static String txt_ProductIsRequiredErrorA;
	protected static String txt_QtyIsRequiredErrorA;
	protected static String txt_InvoiceAccountValidatorA;
	protected static String btn_DetailsTabBtnA;
	protected static String btn_SummaryTabBtnA;
	protected static String btn_PricingAndChargesTabBtnA;
	protected static String txt_VendorTextFieldA;
	protected static String txt_CurrencyTextFieldA;
	protected static String txt_ContactPersonNameTextFieldA;
	protected static String txt_VendorReferenceNoTextFieldA;
	protected static String txt_BillingAddressTextFieldA;
	protected static String txt_TitleTextFieldA;
	protected static String txt_OrderGroupTextFieldA;
	protected static String txt_CustomerAccountTextFieldA;
	protected static String txt_WarehouseTextFieldA;
	protected static String txt_ShippingAddressTextFieldA;
	protected static String txt_ProductCodeTextFieldA;
	protected static String txt_ProductCodeQtyTextFieldA;

	// PM_PI_007to010Reg
	protected static String btn_VendorSearchBtnA;
	protected static String txt_VendorSearchTxtA;
	protected static String sel_VendorSearchSelA;
	protected static String btn_NewVendorbtnA;
	protected static String txt_NewVendorTxtA;
	protected static String txt_NewVendorGroupTxtA;
	protected static String btn_NewVendorUpdatebtnA;
	protected static String btn_VendorDetailBtnA;
	protected static String txt_VendorDetailWidgetA;

	// PM_PI_011to014Reg
	protected static String btn_CurrencyDetailBtnA;
	protected static String txt_CurrencyDetailWidgetA;

	// PM_PI_015to025Reg
	protected static String btn_BillingAddressSearchBtnA;
	protected static String txt_BillingAddressTxtA;
	protected static String btn_ShippingAddressSearchBtnA;
	protected static String txt_ShippingAddressTxtA;
	protected static String btn_NewOrderGroupPlusBtnA;
	protected static String btn_NewOrderGroupAddBtnA;
	protected static String txt_NewOrderGroupAddTxtA;
	protected static String btn_NewOrderGroupUpdateBtnA;
	protected static String sel_OrderGroupSelA;
	protected static String btn_NewOrderGroupAddErrorBtnA;
	protected static String txt_NewOrderGroupAddErrorMsgA;
	protected static String btn_OrderGroupInactiveBtnA;
	protected static String btn_CustomerAccountBtnA;
	protected static String txt_CustomerAccountTxtA;
	protected static String sel_CustomerAccountSelA;
	protected static String btn_CustomerAccountDeleteBtnA;
	protected static String btn_NewCustomerAccountBtnA;
	protected static String txt_NewCustomerAccountNameA;
	protected static String txt_NewCustomerAccountGroupA;
	protected static String sel_NewCustomerAccountSelA;

	// PM_PI_026to031Reg
	protected static String txt_ProductTableNameA;
	protected static String txt_ProductTableQtyA;
	protected static String txt_ProductTableUnitPriceA;
	protected static String btn_ProductTableSearchBtnA;
	protected static String txt_ProductTableSearchTxtA;
	protected static String sel_ProductTableSearchSelA;
	protected static String sel_pro1A;
	protected static String sel_pro2A;
	protected static String sel_pro3A;
	protected static String txt_tval1A;
	protected static String txt_tval2A;
	protected static String txt_tval3A;
	protected static String btn_NewProductBtnA;
	protected static String txt_NewProductCodeA;
	protected static String txt_NewProductDescriptionA;
	protected static String txt_NewProductGroupA;
	protected static String btn_NewProductManufacturerSearchBtnA;
	protected static String txt_NewProductManufacturerSearchTxtA;
	protected static String sel_NewProductManufacturerSearchSelA;
	protected static String btn_ProductAddPlusBtnA;

	// PM_PI_032to043Reg
	protected static String btn_ProductAddDeleteBtnA;
	protected static String btn_ProductWidgetPIA;
	protected static String btn_ProductExDescriptionBtnA;
	protected static String txt_ProductExDescriptionTxtA;
	protected static String btn_ProductAdvanceBtnA;
	protected static String txt_ProductAdvanceHeaderA;
	protected static String txt_ProductAdvanceUnitPriceA;
	protected static String txt_ProductAdvanceDiscountA;
	protected static String txt_ProductAdvanceTaxGroupA;
	protected static String txt_ProductAdvanceRemarksA;
	protected static String txt_ProductAdvanceOnsiteWarrantyA;
	protected static String txt_ProductAdvanceLabourWarrantyA;
	protected static String txt_ProductTableDiscountA;
	protected static String txt_ProductTableTaxGroup1A;
	protected static String txt_ProductTableRemarksA;
	protected static String txt_ProductTableDiscountPresentage1A;
	protected static String btn_ProductTableErrorBtnA;
	protected static String txt_ProductTableErrorTxt1A;

	// PM_PI_044to058Reg
	protected static String txt_ProductTableErrorTxt2A;
	protected static String txt_TotalValueHeaderA;
	protected static String btn_SelectAllBtnA;
	protected static String sel_TaxGroupOnlyCboxA;
	protected static String txt_SelectAllTaxGroupA;
	protected static String txt_ProductTableTaxGroup2A;
	protected static String sel_DiscountOnlyCboxA;
	protected static String txt_SelectAllDiscountA;
	protected static String txt_ProductTableDiscountPresentage2A;
	protected static String txt_DraftValidatorPIA;
	protected static String txt_ProductTableLineTotalA;
	protected static String btn_TaskBreakDownBtnA;
	protected static String txt_TaskBreakDownTaxTxtA;
	protected static String txt_BottomTableUnitTotalA;
	protected static String txt_BottomTableDiscountTotalA;;
	protected static String txt_BottomTableSubTotalA;
	protected static String txt_BottomTableTaxTotalA;
	protected static String txt_BottomTableTotalA;
	protected static String txt_TotalUnitsHeaderA;

	// PM_PI_059to070Reg
	protected static String txt_ShippingModeA;
	protected static String txt_DeliveryTermA;
	protected static String txt_DestinationA;
	protected static String txt_TolleranceA;
	protected static String txt_UnderDeliveryA;
	protected static String txt_PartialDeliveryA;
	protected static String txt_ReqSlipDateA;
	protected static String txt_ConfirmedSlipDateA;
	protected static String txt_ReqReceiptDateA;
	protected static String txt_ConfirmedReceiptDateA;
	protected static String txt_VendorAgreementA;
	protected static String txt_InvoiceAccountA;
	protected static String txt_PaymentAgreementA;
	protected static String txt_PaymentTermA;
	protected static String btn_ShippingModeAddBtnA;
	protected static String btn_ShippingModeAddPlusA;
	protected static String txt_ShippingModeAddTxtA;
	protected static String btn_ShippingModeAddErrorBtnA;
	protected static String txt_ShippingModeAddErrorTxtA;
	protected static String btn_ShippingModeActiveInactiveBtnA;
	protected static String btn_ShippingTermAddBtnA;
	protected static String btn_ShippingTermAddPlusA;
	protected static String txt_ShippingTermAddTxtA;
	protected static String btn_ShippingTermAddErrorBtnA;
	protected static String txt_ShippingTermAddErrorTxtA;
	protected static String btn_ShippingTermActiveInactiveBtnA;


	// PM_PI_071to074Reg
	protected static String btn_PurchaseInvoiceByPageBtnA;
	protected static String txt_PurchaseInvoiceByPageTxtA;
	protected static String sel_PurchaseInvoiceByPageSelA;
	protected static String btn_JournalPIBtnA;
	protected static String txt_JournalEntryDebitValueA;
	protected static String txt_JournalEntryCreditValueA;
	protected static String btn_ReversePIBtnA;
	protected static String txt_pageReverseA;

	// PM_PI_075Reg
	protected static String btn_DeletePIBtnA;
	protected static String txt_pageDeleteA;

	// PM_PI_078Reg
	protected static String btn_DocFlowPIBtnA;

	// PM_PI_079to080Reg
	protected static String txt_ProductTableUnitPricePOtoPIA;
	protected static String btn_NewPOPlusBtnA;
	protected static String txt_NewPOUnitPriceTxtA;

	// PM_PI_082to083Reg
	protected static String btn_ProductAdvancePOtoPIBtnA;
	protected static String txt_HSCodeTxtA;

	// PM_PI_084Reg
	protected static String txt_UnitPriceValidatorA;
	protected static String btn_UnitPriceValidatorActiveInactiveBtnA;
	protected static String btn_UpdateBtnA;

	// PM_PI_085Reg
	protected static String btn_ActivitiesPIBtnA;
	protected static String txt_ActivitiesSubjectPIA;
	protected static String txt_ActivitiesAssignToTxtPIA;
	protected static String btn_ActivitiesUpdateBtnPIA;
	protected static String btn_TaskAndEventBtnA;
	protected static String btn_MyTaskBtnA;
	protected static String btn_PurchaseInvoiceTileA;
	protected static String btn_PurchaseInvoiceActivityArrowA;
	protected static String txt_PurchaseInvoiceNoActivityTabA;

	// PM_PI_086Reg
	protected static String txt_DeleteDocTxtPIA;

	// PM_PI_087Reg
	protected static String btn_ApprovalsBtnA;
	protected static String sel_ApprovalsDicisionA;
	protected static String sel_RejectReasonA;
	protected static String btn_OKBtnA;

	// PM_PI_088Reg
	protected static String btn_AdministrationBtnA;
	protected static String btn_WorkflowConfigurationBtnA;
	protected static String btn_PIDeleteBtnA;
	protected static String txt_WorkflowStatusA;
	protected static String txt_DeleteApprovalPageA;

	// PM_PI_098Reg
	protected static String txt_ProductTableUnitPriceAfterValuePOtoPIA;

	// PM_PI_101Reg
	protected static String btn_btnPIToServiceA;

	// PM_PI_102to103Reg
	protected static String btn_UICustomizerBtnA;
	protected static String sel_UICustomizerBalancingLevelA;
	protected static String sel_UICustomizerPermissionLevelA;
	protected static String btn_UICustomizerPurchaseInvoiceBtnA;
	protected static String btn_UICustomizerNextBtnA;
	protected static String sel_UICustomizerApplicableJourneyA;
	protected static String txt_UICustomizerBillingAddressTextFieldA;

	// PM_PI_104Reg
	protected static String sel_TitleDisableBtnA;

	// PM_PI_106to109Reg
	protected static String txt_ProductTableWarehouseA;
	protected static String btn_CostAllocationGridBtnA;
	protected static String txt_CostCenterTxtA;
	protected static String txt_ApportionPercentageTxtA;
	protected static String txt_CostAssignmentTypeTxtA;
	protected static String btn_CostObjectSearchBtnA;
	protected static String txt_CostObjectSearchTxtA;
	protected static String sel_CostObjectSearchSelA;
	protected static String btn_ApportionAddRecordBtnA;
	protected static String btn_CostAllocationGridDeleteFirstA;
	protected static String btn_CostAllocationUpdateBtnA;
	protected static String btn_ChangeCostAllocationsBtnA;
	protected static String txt_CostObjectTxtA;

	// PM_PI_110Reg
	protected static String btn_SaveAsTemplateBtnA;
	protected static String txt_ApportionTemplateNameTxtA;
	protected static String txt_TemplateCboxTxtA;
	
	// PM_PI_115Reg
	protected static String txt_InvoicePercentageA;
	protected static String txt_NoBalanceQtyToCompleteThisTransactionErrorA;
	
	//PM_PI_116Reg
	protected static String btn_ProductDeleteServiceA;
	
	//PM_PI_117to119Reg
	protected static String txt_PlannedQtyPI1A;
	protected static String txt_PlannedQtyPI2A;
	protected static String txt_ProductTableName2A;
	
	//PM_PI_123Reg
	protected static String txt_PurchaseOrderByPageSearchTxtA;
	protected static String sel_PurchaseOrderByPageSearchSelA;
	protected static String txt_ChangeStatusA;
	protected static String txt_pageCloseA;
	
	//PM_PI_124Reg
	protected static String btn_ProductDeleteService2A;
	
	//PM_PI_125to127Reg
	protected static String btn_StatusArrowBtnA;
	protected static String txt_StatusInboundShipmentLevelA;
	protected static String txt_StatusShipmentCostingLevelA;
	protected static String txt_StatusOutboundPaymentLevelA;
	
	//PM_PI_128Reg
	protected static String txt_ProductAfterTableWarehouseA;
	
	//PM_PI_129Reg
	protected static String txt_ProductTableWarehouse2A;
	
	//PM_PI_131Reg
	protected static String txt_ChargeCodeA;
	protected static String txt_ChargePresentageA;
	
	//PM_PI_132Reg
	protected static String sel_UnderDeliveryCboxA;
	protected static String sel_PartialDeliveryCboxA;
	protected static String btn_POPIISSCJourneyBtnA;
	
	//PM_PI_133Reg
	protected static String btn_POISPISCJourneyBtnA;
	
	//PM_PI_138Reg
	protected static String txt_ProductTableDiscountPOA;
	protected static String txt_ProductTableTaxGroupPOA;
	protected static String txt_OrderDiscountTxtA;
	protected static String btn_GridMasterBtnPIA;
	protected static String btn_ProductRelatedPriceBtnA;
	protected static String txt_LastPurchasePriceTxtA;
	
	//PM_PI_139Reg
	protected static String txt_ProductTableQtyPOtoPIA;
	
	//PM_PO_001to010Reg
	protected static String txt_VendorTxtValidatorPOA;
	protected static String txt_CurrencyValidatorPOA;
	protected static String txt_ProductGridValidatorPOBtnA;
	protected static String txt_ProductGridValidatorPOA;
	protected static String txt_InvoiceAccountValidatorPOA;
	protected static String txt_VendorTextFieldPOA;
	protected static String txt_CurrencyTextFieldPOA;
	protected static String txt_ContactPersonNameTextFieldPOA;
	protected static String txt_VendorReferenceNoTextFieldPOA;
	protected static String txt_BillingAddressTextFieldPOA;
	protected static String txt_TitleTextFieldPOA;
	protected static String txt_OrderGroupTextFieldPOA;
	protected static String txt_CustomerAccountTextFieldPOA;
	protected static String txt_WarehouseTextFieldPOA;
	protected static String txt_ShippingAddressTextFieldPOA;
	protected static String txt_ConsolidatedTextFieldPOA;
	protected static String btn_VendordetailsBtnPOA;
	protected static String txt_VendorCreditDetailsBtn1POA;
	protected static String txt_VendorCreditProfileBtn2POA;
	protected static String btn_UserPermissionBtnA;
	protected static String btn_UserSearchBtnA;
	protected static String txt_UserSearchTxtA;
	protected static String sel_UserSearchSelA;
	protected static String btn_WidgetsBtnA;
	protected static String txt_VendorCreditDetailsTxtA;
	protected static String txt_VendorCreditProfileTxtA;
	protected static String sel_VendorCreditDetailsCboxA;
	protected static String sel_VendorCreditProfileCboxA;
	
	//PM_PO_011to014Reg
	protected static String btn_CurrencyInformationBtnA;
	protected static String txt_CurrencyInformationTxtA;
	protected static String btn_CurrencydetailsBtnPOA;
	protected static String txt_CurrentExRatePOA;
	protected static String btn_FinanceBtnA;
	protected static String btn_CurrencyExchangeRateBtnA;
	protected static String txt_CurrencyExchangeSellingRateTxtA;
	
	//PM_PO_015to025Reg
	protected static String btn_NewOrderGroupPlusBtnPOA;
	protected static String btn_CustomerAccountPOBtnA;
	protected static String btn_CustomerAccountDeletePOBtnA;
	
	//PM_PO_026to032Reg
	protected static String txt_ProductTableNamePOA;
	protected static String txt_ProductTableQtyPOA;
	protected static String txt_ProductTableUnitPricePOA;
	protected static String txt_tval1POA;
	protected static String txt_tval2POA;
	protected static String txt_tval3POA;
	protected static String btn_ProductTableDeleteBtn1POA;
	protected static String btn_ProductTableDeleteBtn2POA;
	
	//PM_PO_033to049Reg
	protected static String btn_ProductWidgetPOA;
	protected static String btn_ProductAlternativesWidgetPOA;
	protected static String btn_ProductRelatedPriceWidgetPOA;
	protected static String btn_ProductPurchasingHistoryWidgetPOA;
	protected static String btn_ProductAvailabilityWidgetPOA;
	protected static String btn_RackWiseAvailableQtyWidgetPOA;
	protected static String btn_ProductBatchSerialWiseCostWidgetPOA;
	protected static String btn_ProductMovementHistoryWidgetPOA;
	protected static String btn_ProductSalesHistoryWidgetPOA;
	protected static String btn_ProductWiseGrossProfitWidgetPOA;
	protected static String btn_ViewProductImageWidgetPOA;
	protected static String btn_ProductROLWidgetPOA;
	protected static String btn_StockMovementHistoryWidgetPOA;
	protected static String btn_ProductAvailabilityInWIPWidgetPOA;
	protected static String btn_ProductAvailabilityOnPostDateWidgetPOA;
	protected static String btn_ProductBatchSerialWiseWithoutCostWidgetPOA;
	protected static String btn_ProductExDescriptionBtnPOA;
	protected static String btn_ProductAdvanceBtnPOA;
	protected static String txt_AdvanceProductNamePOA;
	protected static String txt_AdvanceExDescriptionPOA;
	protected static String txt_AdvanceUnitPricePOA;
	protected static String txt_AdvanceOnhandQtyPOA;
	protected static String txt_ProductTableRemarksPOA;
	protected static String txt_ProductTableDiscountPresentagePOA;
	protected static String btn_ProductTableErrorBtnPOA;
	protected static String txt_ProductTableErrorTxt3A;
	
	//PM_PO_050to058Reg
	protected static String txt_ProductTableTaxGroup1POA;
	protected static String txt_ProductTableTaxGroup2POA;
	protected static String txt_ProductTableDiscountPresentage1POA;
	protected static String txt_ProductTableDiscountPresentage2POA;
	protected static String txt_DraftValidatorPOA;
	protected static String txt_ProductTableLineTotalPOA;
	protected static String btn_TaskBreakDownBtnPOA;
	
	//PM_PO_059to070Reg
	protected static String txt_ShippingModePOA;
	protected static String txt_ShippingTermPOA;
	protected static String txt_DestinationPOA;
	protected static String txt_OverShipTolerancePOA;
	protected static String txt_UnderDeliveryPOA;
	protected static String txt_PartialDeliveryPOA;
	protected static String txt_RequestedShipDatePOA;
	protected static String txt_ConfirmedShipDatePOA;
	protected static String txt_RequestedReceiptDatePOA;
	protected static String txt_ConfirmedReceiptDatePOA;
	protected static String txt_VendorAgreementPOA;
	protected static String txt_InvoiceAccountPOA;
	protected static String txt_PaymentAgreementPOA;
	protected static String txt_PaymentTermPOA;
	protected static String txt_ChargeAmountTxtPOA;
	protected static String txt_PricingAndChargesDeleteBtnA;
	protected static String txt_PricingAndChargesOrderDiscountTxtA;
	
	//PM_PO_071to073Reg
	protected static String btn_PurchaseOrderByPageBtnA;
	protected static String txt_PurchaseOrderByPageTxtA;
	protected static String sel_PurchaseOrderByPageSelA;
	protected static String btn_YesBtnPOA;
	
	//PM_PO_083Reg
	protected static String txt_TotalPriceValidatorA;
	
	//PM_PO_084Reg
	protected static String btn_ActivitiesPOBtnA;
	protected static String txt_ActivitiesSubjectPOA;
	protected static String txt_ActivitiesAssignToTxtPOA;
	protected static String btn_ActivitiesUpdateBtnPOA;
	protected static String btn_PurchaseOrderTileA;
	protected static String btn_PurchaseOrderActivityArrowA;
	protected static String txt_PurchaseOrderNoActivityTabA;
	
	//PM_PO_085Reg
	protected static String txt_DeleteDocTxtPOA;
	
	//PM_PO_086Reg
	protected static String btn_PODeleteBtnA;
	
	//PM_PR_001to005Reg
	protected static String txt_TitlePRA;
	protected static String btn_ProductSearchGridBtnPRA;
	protected static String txt_ProductSearchGridTxtPRA;
	protected static String sel_ProductSearchGridSelPRA;
	protected static String btn_PurchaseRequisitionByPageBtnA;
	protected static String txt_PurchaseRequisitionByPageTxtA;
	protected static String sel_PurchaseRequisitionByPageSelA;
	protected static String btn_GridMasterInfoBtnPRA;
	protected static String txt_ProductTableUnitPricePRA;
	
	//PM_PR_006to008Reg
	protected static String txt_PreparerPRA;
	protected static String txt_PreparerAfterReleasedPRA;
	
	//PM_PR_009to010Reg
	protected static String txt_TitleAfterDraftPRA;
	
	//PM_PR_011to013Reg
	protected static String txt_RequestGroupPRA;
	protected static String txt_ProductGroupPRA;
	protected static String txt_ProductTableQtyPRA;
	protected static String btn_UpdateProcurementDetailsBtnPRA;
	protected static String txt_UpdateProcurementDetailsInvoiceNumberPRA;
	protected static String btn_ProcurementInformationTabBtnA;
	protected static String txt_ProcurementInformationTabInvoiceNumberPRA;
	
	//PM_PR_016Reg
	protected static String txt_DraftValidatorPRA;
	
	//PM_PR_018Reg
	protected static String txt_ProductTableExpectedDatePRA;
	protected static String sel_ProductTableExpectedDateSelPRA;
	protected static String btn_ProductTableAdvanceBtnPRA;
	protected static String txt_ExpectedDateAdvanceTablePRA;
	
	//PM_PR_019Reg
	protected static String txt_ProductTableWarehousePRA;
	
	//PM_PR_021to022Reg
	protected static String btn_InventoryAndWarehousingBtnPRA;
	protected static String btn_InternalOrderBtnPRA;
	protected static String btn_NewInternalOrderBtnPRA;
	protected static String btn_EmployeeRequestsJourneyBtnPRA;
	protected static String txt_InternalOrderTitlePRA;
	protected static String btn_InternalOrderRequesterBtnPRA;
	protected static String txt_InternalOrderRequesterTxtPRA;
	protected static String sel_InternalOrderRequesterSelPRA;
	protected static String btn_InternalOrderGeneratePurchaseRequisitionBtnPRA;
	protected static String txt_UnitPricePRA;
	
	//PM_PR_024Reg
	protected static String txt_CopyFromSearchTxtPRA;
	protected static String sel_CopyFromSearchSelPRA;
	protected static String txt_ProductTableNamePRA;
	
	//PM_PR_025Reg
	protected static String btn_ProductTableProductCatalogPRA;
	protected static String txt_ProductHierarchyPopupHeaderPRA;

	
	//PM_PR_026to028Reg
	protected static String txt_ConfirmationMessageValidationA;
	protected static String btn_ConfirmationMessageValidationCloseA;
	
	//PM_PR_029Reg
	protected static String txt_InternalOrderProductaNamePRA;
	
	//PM_PR_030to031Reg
	protected static String btn_RequesterSearchBtnPRA;
	protected static String txt_RequesterSearchTxtPRA;
	protected static String sel_RequesterSearchSelPRA;
	protected static String btn_CurrencyDetailsWidgetBtnPRA;
	protected static String txt_CurrencyCurrentExRatePRA;
	protected static String txt_CurrencyLastUpdatedPRA;
	protected static String txt_CurrencyExchangeLastUpdateDateTxtA;
	
	//PM_PR_032Reg
	protected static String icn_DraftDocIconPRA;
	protected static String icn_ReleaseDocIconPRA;
	protected static String icn_ReverseDocIconPRA;
	protected static String icn_DeleteDocIconPRA;
	
	//PM_PR_033to034Reg
	protected static String txt_RequestDatePRA;
	protected static String txt_RequisitionNoByPagePRA;
	protected static String txt_TitleByPagePRA;
	protected static String txt_PreparerByPagePRA;
	protected static String txt_RequestDateByPagePRA;
	
	//PM_PR_035to036Reg
	protected static String btn_ProductAdvanceBtnPRA;
	protected static String btn_ProductAdvanceProcurementInformationTabPRA;
	protected static String txt_ProductAdvanceSupplierTxtPRA;
	protected static String btn_ConvertToPurchaseOrderA;
	protected static String btn_ConvertToRequestForQuotationA;
	protected static String btn_UpdateProcurementDetailsA;
	
	//PM_PR_039to040Reg
	protected static String txt_ProductListHeaderPRA;
	protected static String btn_GenaratePOBtnPRA;
	protected static String txt_StartNewProcessHeaderPRA;
	
	//PM_PR_043Reg
	protected static String btn_VendorLookupBtnRFQA;
	protected static String txt_VendorLookupTxtRFQA;
	protected static String sel_VendorLookupSelRFQA;
	protected static String txt_TableProductNameRFQA;
	protected static String txt_pageReleasedForQuotationRFQA;
	
	//PM_PR_047Reg
	protected static String btn_ProductTableAddPlusBtnPRA;
	protected static String btn_ProductSearchGridBtn2PRA;
	protected static String txt_ProductTableName2POA;
	protected static String txt_ProductTableName1ISA;
	protected static String txt_ProductTableName2ISA;
	
	//PM_PR_048Reg
	protected static String txt_ReversedValidatorPRA;
	
	//PM_PR_054Reg
	protected static String txt_HistortTabDraftHistoryTxtA;
	protected static String txt_HistortTabUpdateHistoryTxtA;
	protected static String txt_HistortTabReleaseHistoryTxtA;
	protected static String txt_HistortTabReversedHistoryTxtA;
	
	//PM_PR_055to057Reg
	protected static String txt_PageHoldTxtA;
	protected static String txt_ProductTableQtyAfterReleasedPRA;
	
	//PM_PR_058to066Reg
	protected static String btn_PRDraftBtnA;
	protected static String btn_SendReviewBtnA;
	protected static String txt_SendReviewDraftPageA;
	protected static String icn_DraftReviewingDocIconPRA;
	protected static String txt_NextReviewUserA;
	protected static String btn_PurchaseRequisitionTileA;
	protected static String txt_PurchaseRequisitionSearchTxtA;
	protected static String btn_PurchaseRequisitionActivityArrowA;
	protected static String txt_ApprovedDraftPageA;
	protected static String icn_DraftApprovedDocIconPRA;
	protected static String btn_NotificationBtnA;
	protected static String txt_PRApprovedNotificationA;
	protected static String txt_ActivitiesStatusA;
	protected static String btn_UpdateTaskA;
	protected static String txt_ApprovalRequestLookUpHeaderA;
	
	//PM_PR_067to070Reg
	protected static String txt_RejectedDraftPageA;
	protected static String txt_DraftRejectedDocIconPRA;
	
	//PM_PR_071Reg
	protected static String txt_PurchaseRequisitionActivityDocNoA;
	protected static String btn_ApproveBtnA;
	protected static String sel_ApprovalsDicisionFormFieldA;
	protected static String btn_OKFormBtnA;
	
	//PM_PR_072Reg
	protected static String sel_RejectReasonFormFieldA;
	
	//PM_PR_073to074Reg
	protected static String btn_PRDraftNewBtnA;
	protected static String txt_PostBusinessUnitA;
	protected static String sel_PostBusinessUnitSelA;
	protected static String btn_LogOutBtnA;
	protected static String btn_SignOutBtnA;
	
	//PM_PR_078to079Reg
	protected static String btn_PRDraftLevelWiseBtnA;
	protected static String txt_ApprovalRequestLookUpHeaderLevel1A;
	protected static String txt_ApprovalRequestLookUpHeaderLevel2A;
	
	//PM_PR_083to084Reg
	protected static String txt_HistortTabWFDraftHistoryTxtA;
	protected static String txt_HistortTabWFReviewHistoryTxtA;
	protected static String txt_HistortTabWFDraft1HistoryTxtA;
	protected static String txt_HistortTabWFDraft2HistoryTxtA;
	protected static String txt_HistortTabWFReleaseHistoryTxtA;
	
	//PM_PR_085to093Reg
	protected static String btn_PRReleaseBtnA;
	protected static String txt_PendingApprovalPageA;
	protected static String icn_PendingApprovalDocIconPRA;
	protected static String txt_ApprovalRequestLookUpHeaderReleaseA;
	protected static String txt_PRReleasedApprovedNotificationA;
	
	//PM_PR_094to097Reg
	protected static String txt_RejectedPageA;
	protected static String icn_ReleasedRejectedDocIconPRA;
	
	//PM_PR_100to101Reg
	protected static String btn_PRReleaseNewBtnA;
	
	//PM_PR_105to106Reg
	protected static String btn_PRReleaseLevelWiseBtnA;
	protected static String txt_ApprovalRequestLookUpHeaderReleaseLevel1A;
	protected static String txt_ApprovalRequestLookUpHeaderReleaseLevel2A;
	
	//PM_PR_110Reg
	protected static String txt_HistortTabWFApprovedHistoryTxtA;
	protected static String txt_HistortTabWFApproval1HistoryTxtA;
	protected static String txt_HistortTabWFApproval2HistoryTxtA;
	
	//PM_PR_111to112Reg
	protected static String btn_PRDraftOnlyOneBtnA;
	
	//PM_PR_114to115Reg
	protected static String btn_PRReleaseOnlyOneBtnA;
	
	//PM_PR_117to125Reg
	protected static String btn_PRDeleteBtnA;
	protected static String txt_ApprovalRequestLookUpDeleteHeaderA;
	protected static String icn_DeleteApprovalDocIconPRA;
	protected static String txt_PRDeleteApprovedNotificationA;
	
	//PM_PR_132to133Reg
	protected static String btn_PRDeleteNewBtnA;
	
	//PM_PR_137to138Reg
	protected static String btn_PRDeleteLevelWiseBtnA;
	protected static String txt_ApprovalRequestLookUpHeaderDeleteLevel1A;
	protected static String txt_ApprovalRequestLookUpHeaderDeleteLevel2A;
	
	//PM_PR_142Reg
	protected static String txt_HistortTabWFDeleteAprHistoryTxtA;
	protected static String txt_HistortTabWFDelete1HistoryTxtA;
	protected static String txt_HistortTabWFDelete2HistoryTxtA;
	protected static String txt_HistortTabWFDeleteHistoryTxtA;
	
	//PM_PR_146to154Reg
	protected static String btn_PRDeleteOnlyOneBtnA;
	
	//PM_PR_146to148Reg
	protected static String btn_PRReverseBtnA;
	protected static String txt_ReverseApprovalPageA;
	protected static String icn_ReverseApprovalDocIconPRA;
	protected static String txt_ApprovalRequestLookUpReverseHeaderA;
	protected static String txt_PRReverseApprovedNotificationA;
	
	//PM_PR_161to162Reg
	protected static String btn_PRReverseNewBtnA;
	
	//PM_PR_166to167Reg
	protected static String btn_PRReverseLevelWiseBtnA;
	protected static String txt_ApprovalRequestLookUpHeaderReverseLevel1A;
	protected static String txt_ApprovalRequestLookUpHeaderReverseLevel2A;
	
	//PM_PR_171Reg
	protected static String txt_HistortTabWFDraftedReverseHistoryTxtA;
	protected static String txt_HistortTabWFReleasedReverseHistoryTxtA;
	protected static String txt_HistortTabWFSentReverseHistoryTxtA;
	protected static String txt_HistortTabWFApproveReverseHistory1TxtA;
	protected static String txt_HistortTabWFApproveReverseHistory2TxtA;
	protected static String txt_HistortTabWFReversedHistoryTxtA;
	
	//PM_PR_172to173Reg
	protected static String btn_PRReverseOnlyOneBtnA;
	
	//PM_PR_175to176Reg
	protected static String btn_JourneyConfigurationBtnA;
	protected static String btn_JourneyConfigurationPurchaseRequisitionBtnA;
	protected static String btn_JourneyConfigurationProcessStatusBtnA;
	protected static String txt_PurchaseRequisitionJourneyValidatorA;
	
	//PM_PR_177Reg
	protected static String btn_BalancingLevelSettingsBtnA;
	protected static String btn_BalancingLevelInventorySegBtnA;
	protected static String txt_BalancingLevelInventoryProductCodeNameTxtA;
	protected static String btn_BalancingLevelInventorySegUpdateBtnA;
	protected static String txt_PRProductFieldA;
	
	//PM_PR_180Reg
	protected static String btn_BalancingLevelGenaralSegBtnA;
	protected static String txt_BalancingLevelGenaralWarehouseNameTxtA;
	protected static String btn_BalancingLevelGenaralSegUpdateBtnA;
	protected static String txt_PRWarehouseField1A;
	protected static String txt_PRWarehouseField2A;
	
	//PM_PR_183Reg
	protected static String txt_UICustomizerPurchaseRequisitionBtnA;
	protected static String btn_UIPRNewCustomFieldA;
	protected static String txt_UIPRFieldLabelA;
	protected static String txt_UIPRFieldTypeA;
	protected static String btn_UIPRReportableA;
	protected static String btn_UIAddFieldDeleteBtnA;
	protected static String txt_NewFieldTxtA;
	protected static String txt_NewFieldAfterTxtA;
	protected static String btn_CreateBtnA;
	
	//PM_PR_184Reg
	protected static String btn_AddListValueBtnA;
	protected static String btn_AddNewListValueBtnA;
	protected static String txt_AddNewListValueTxtA;
	protected static String btn_AddNewListValueUpdateBtnA;
	protected static String txt_NewListFieldTxtA;
	
	//PM_PR_185Reg
	protected static String txt_NewTextAreaFieldTxtA;
	
	//PM_PR_190Reg
	protected static String sel_DateSelA;
	
	//PM_PRO_001to005Reg
	protected static String btn_ReturnGroupAddBtn;
	protected static String txt_ReturnGroupTextField;
	protected static String btn_NewReturnGroupAddBtn;
	protected static String txt_NewReturnGroupAddTxt;
	protected static String btn_NewReturnGroupUpdateBtn;
	protected static String sel_ReturnGroupInactiveSel;
	
	//PM_PRO_006to010Reg
	protected static String txt_ContactPersonTitleVI;
	protected static String txt_ContactPersonNameVI;
	protected static String txt_ContactPersonNamePRO;
	protected static String txt_CheckoutValidatorPRO;
	protected static String txt_VendorValidationPRO;
	protected static String txt_CurrencyValidationPRO;
	protected static String txt_BillingAddressValidationPRO;
	protected static String txt_ShippingAddressValidationPRO;
	protected static String txt_ProductGridErrorBtnPRO;
	
	//PM_PRO_011to013Reg
	protected static String txt_ProductGridQtyFieldPRO;
	protected static String txt_UnitPriceFieldPRO;
	protected static String txt_DiscountPresentageFieldPRO;
	protected static String txt_DiscountFieldPRO;
	protected static String txt_TaxGroupFieldPRO;
	
	//PM_PRO_014to016Reg
	protected static String txt_LineTotalPRO;
	protected static String btn_CheckoutBtnPRO;
	
	//PM_PRO_017Reg
	protected static String txt_DiscountFieldDraftedPRO;
	
	//PM_PRO_024Reg
	protected static String txt_TaxBreakdownTaxCode;
	protected static String btn_TaxBreakdownBtn;
	
	//PM_PRO_030Reg
	protected static String txt_BannerCurrency;
	
	//PM_PRO_031to032Reg
	protected static String btn_PurchaseReturnOrderByPageBtn;
	protected static String txt_PurchaseReturnOrderByPageSearchTxt;
	protected static String sel_PurchaseReturnOrderByPageSearchSel;
	protected static String icn_HoldIconPROByPage;
	protected static String icn_UnHoldIconPROByPage;
	
	//PM_PRO_033Reg
	protected static String txt_HistortTabDraftHistoryPROTxtA;
	protected static String txt_HistortTabUpdateHistoryPROTxtA;
	protected static String txt_HistortTabReleaseHistoryPROTxtA;
	protected static String txt_HistortTabReversedHistoryPROTxtA;
	
	//PM_PRO_034to035Reg
	protected static String txt_WarehouseFieldPRO;
	protected static String btn_ProductMasterInfoOS;
	protected static String btn_ProductBatchSerialWiseCostOS;
	protected static String txt_ProductBatchNoOS;
	
	//PM_PRO_036Reg
	protected static String txt_DocStatusPresentage;
	
	//Vendor Group Configuration
	
	//PM_VENGRP_001to005Reg
	protected static String txt_VendorGroupBypageVGC;
	protected static String btn_SummaryTabVGC;
	protected static String btn_DetailsTabVGC;
	protected static String btn_PaymentInformationVGC;
	protected static String txt_VendorGroupVGC;
	protected static String txt_CurrencyVGC;
	protected static String txt_ChargeGroupVGC;
	protected static String txt_VendorGroupValidatorVGC;

	/* Reading the Data to variables */
	// Com_TC_001
	protected static String userNameData;
	protected static String passwordData;

	// PR_VGC_002
	protected static String vendorgroup;
	protected static String currency;
	protected static String PurchaseTaxGroup;
	protected static String VendorClassification;
	protected static String StandardDeliveryMode;
	protected static String StandardDeliveryTerm;
	protected static String Warehouse;
	protected static String PaymentTerm;
	protected static String Tolerance;
	protected static String CreditDays;
	protected static String CreditLimit;
	// PR_VI_002
	protected static String VendorType;
	protected static String VendorNameTitle;
	protected static String VendorName;
	protected static String txtReceivableAccount;
	protected static String ContactTitle;
	protected static String ContactName;
	protected static String MobileNumber;
	protected static String SelectIndustry;
	protected static String TaxRegistrationNumber;
	protected static String ReferenceAccoutNo;
	protected static String txt3rdPartyAccount;
	protected static String PassportNo;
	protected static String newReceivableAccountname;
	protected static String vendorgroupforven;

	// PM_VA_001
	protected static String AgreementNo;
	protected static String AgreementTitle;
	protected static String AgreementDate;
	protected static String AgreementGroup;
	protected static String Acurrency;
	protected static String ValidityPeriodFor;
	protected static String Description;
	protected static String txtVendorAccount;
	protected static String ProductRelation;
	protected static String txtproduct;
	protected static String minOrderQty;
	protected static String maxOrderQty;
	protected static String txtAssignTo;
	protected static String txtsubject;

	// PM_PR_002

	protected static String RequestDate;
	protected static String RCurrency;
	protected static String Rtxtproduct;

	// PM_PR_005

	protected static String txtVendor;
	protected static String txtBillingAddress;
	protected static String txtShippingAddress;

	// PM_PO_001
	protected static String ptxtproduct;
	protected static String Count;
	protected static String TestQuantity;

	// PM_PO_002
	protected static String ptxtSproduct;
	protected static String txtVendorReferenceNo;

	// PM_PO_006
	protected static String ptxtNxtproduct;
	protected static String txtCEstDescription;
	protected static String EstimateCost;
	protected static String ApportionmentBasis;
	protected static String ptxtBproduct;
	protected static String SerialNo;
	protected static String BatchNo;
	protected static String ExpiryDate;

	// PM_PO_007
	protected static String txtQESTDescription;
	protected static String EstimatedQty;
	protected static String EstimatenewCost;

	// PM_PI_001

	protected static String PIcurrency;
	protected static String txtPIVendor;
	protected static String txtPIBillingAddress;
	protected static String txtPIShippingAddress;
	protected static String PItxtproduct;
	protected static String PItxtNxtproduct;

	// PM_PI_003
	protected static String PItxtSproduct;

	// PM_PRO_001
	protected static String txtPROVendor;
	protected static String PROqty;
	protected static String txtPOBillingAddress;
	protected static String txtPOShippingAddress;

	// PM_PRO_002
	protected static String POtxtproduct;
	protected static String POwarehouse;
	protected static String POSerialNo;
	// PM_RFQ_001
	protected static String txtcontactperson;
	protected static String txtRFQVendor;
	protected static String txtRFQproduct;
	protected static String RFQqty;
	protected static String RFQQuotationNo;
	protected static String RFQUnitPrice;
	protected static String RFQMinQty;
	protected static String RFQMaxQty;

	// SMOKE_PRO_03
	protected static String ICEDescription;
	protected static String ICEProducttxt;
	protected static String ICEEstimateCost;

	// Master_001
	protected static String WarehouseCode;
	protected static String WarehouseName;
	protected static String AgreementGroupParameterstxt;

	// PM_PI_001to006Reg
	protected static String userNameDataReg;
	protected static String passwordDataReg;

	// PM_PI_007to010Reg
	protected static String VendorTextFieldA;

	// PM_PI_015to025Reg
	protected static String BillingAddressA;
	protected static String ShippingAddressA;
	protected static String ContactPersonNameA;
	protected static String CustomerAccountTxtA;
	protected static String NewCustomerAccountGroupA;

	// PM_PI_026to031Reg
	protected static String ProductTxtA;
	protected static String ProductTableTxtA;
	protected static String ProductTableUnitPriceA;
	protected static String InactiveProductTxtA;
	protected static String InactiveProductTableTxtA;
	protected static String NewProductGroupA;
	protected static String NewProductManufacturerSearchTxtA;

	// PM_PI_032to043Reg
	protected static String TestProductTxtA;
	protected static String ProductAdvanceTaxGroupA;
	protected static String ProductAdvanceTaxGroupNewA;

	// PM_PI_044to058Reg
	protected static String TaxA;

	// PM_PI_079to080Reg
	protected static String TestProductToPOtoPITxtA;
	
	//PM_PI_084Reg
	protected static String UICustomizerPermissionLevel2A;


	// PM_PI_094Reg
	protected static String TestProductServiceTxtA;

	// PM_PI_101Reg
	protected static String NewUserNameA;
	protected static String NewPasswordA;

	// PM_PI_102to103Reg
	protected static String UICustomizerBalancingLevelA;
	protected static String UICustomizerPermissionLevelA;
	protected static String UICustomizerApplicableJourneyA;

	// PM_PI_106to109Reg
	protected static String CostCenterTxtA;
	protected static String CostAssignmentTypeTxtA;
	protected static String CostObjectSearchTxtA;

	// PM_PI_116Reg
	protected static String TestProductServiceTxtNewA;
	protected static String TestProductServiceTxtNewTxtA;

	// PM_PI_124Reg
	protected static String TestProductServiceTxtNew2A;
	protected static String TestProductServiceTxtNewTxt2A;

	// PM_PI_131Reg
	protected static String ChargeCodeA;

	// PM_PI_132Reg
	protected static String TestPOTxt1A;
	protected static String TestPOTxt2A;
	protected static String TestPOTxt3A;

	// PM_PO_001to010Reg
	protected static String UserSearchTxtA;

	// PM_PO_033to047Reg
	protected static String TestProPOA;

	// PM_PO_069
	protected static String ChargeCodePOA;

	// PM_PR_001to005Reg
	protected static String ProductPRA;
	protected static String ProductTxtPRA;

	// PM_PR_011to013Reg
	protected static String RequestGroupPR1A;
	protected static String RequestGroupPR2A;
	protected static String ProductGroupPR1A;
	protected static String ProductGroupPR2A;
	
	//PM_PR_021to022Reg
	protected static String InternalOrderRequesterTxtPRA;
	
	//PM_PR_030to031Reg
	protected static String Requester1;
	protected static String Requester2;
	protected static String Requester3;

	//PM_PR_058to061Reg
	protected static String NextReviewUserA;
	
	//PM_PR_073to074Reg
	protected static String PostBusinessUnit1A;
	protected static String NextReviewUserNewA;
	
	//PM_PR_078to079Reg
	protected static String PostBusinessUnit2A;
	
	//PM_PR_111to112Reg
	protected static String PostBusinessUnit3A;
	
	//PM_PR_177Reg
	protected static String PRProductField1A;
	protected static String PRProductField2A;
	protected static String PRProductField3A;
	
	//PM_PR_180Reg
	protected static String PRWarehouseField1A;
	protected static String PRWarehouseField2A;
	protected static String PRWarehouseField3A;
	
	//Purchase Return Order
	
	//PM_PRO_006to010Reg
	protected static String ProPRO1;
	protected static String ProPRO2;
	protected static String ProPRO3;
	
	//PM_PRO_024Reg
	protected static String TaxBreakdownTaxCode1;
	protected static String TaxBreakdownTaxCode2;
	
	//PM_PRO_029Reg
	protected static String VenNamePRO;
	
	// Calling the constructor

	public static void readElementlocators() throws Exception {
		// Com_TC_001
		siteLogo = findElementInXLSheet(getParameterProcurement, "site logo");
		txt_username = findElementInXLSheet(getParameterProcurement, "userName");
		txt_password = findElementInXLSheet(getParameterProcurement, "password");
		btn_login = findElementInXLSheet(getParameterProcurement, "login");
		lnk_home = findElementInXLSheet(getParameterProcurement, "headerlink");

		// Com_TC_002
		btn_nav = findElementInXLSheet(getParameterProcurement, "navbutton");
		sideMenu = findElementInXLSheet(getParameterProcurement, "sidemenu");
		btn_probutton = findElementInXLSheet(getParameterProcurement, "probutton");
		subsideMenu = findElementInXLSheet(getParameterProcurement, "subsidemenu");

		// PR_VGC_001

		btn_vengroupbutton = findElementInXLSheet(getParameterProcurement, "vendorgroupbutton");
		btn_newvengroupbutton = findElementInXLSheet(getParameterProcurement, "newvendorgroupbutton");
		txt_VendorGroupConfiguration = findElementInXLSheet(getParameterProcurement, "VendorGroupConfiguration");
		txt_newvendorgrouppage = findElementInXLSheet(getParameterProcurement, "newvendorgrouppage");
		// PR_VGC_002
		txt_vendorgroup = findElementInXLSheet(getParameterProcurement, "vendorGroup");
		txt_currency = findElementInXLSheet(getParameterProcurement, "currency");
		btn_btndetails = findElementInXLSheet(getParameterProcurement, "btndetails");
		txt_PurchaseTaxGroup = findElementInXLSheet(getParameterProcurement, "PurchaseTaxGroup");
		txt_VendorClassification = findElementInXLSheet(getParameterProcurement, "VendorClassification");
		txt_StandardDeliveryMode = findElementInXLSheet(getParameterProcurement, "StandardDeliveryMode");
		txt_StandardDeliveryTerm = findElementInXLSheet(getParameterProcurement, "StandardDeliveryTerm");
		txt_Warehouse = findElementInXLSheet(getParameterProcurement, "Warehouse");
		btn_PartialDelivery = findElementInXLSheet(getParameterProcurement, "PartialDelivery");
		btn_ConsolidatedDelivery = findElementInXLSheet(getParameterProcurement, "ConsolidatedDelivery");
		btn_btnpaymentinfo = findElementInXLSheet(getParameterProcurement, "btnpaymentinfo");
		txt_PaymentAgreement = findElementInXLSheet(getParameterProcurement, "PaymentAgreement");
		txt_PaymentTerm = findElementInXLSheet(getParameterProcurement, "PaymentTerm");
		txt_Tolerance = findElementInXLSheet(getParameterProcurement, "Tolerance");
		txt_RegulationTaxGroup = findElementInXLSheet(getParameterProcurement, "RegulationTaxGroup");
		txt_CreditDays = findElementInXLSheet(getParameterProcurement, "CreditDays");
		txt_CreditLimit = findElementInXLSheet(getParameterProcurement, "CreditLimit");
		btn_btndraft = findElementInXLSheet(getParameterProcurement, "btndraft");
		btn_VendorGroupConfiguration = findElementInXLSheet(getParameterProcurement, "VendorGroupConfiguration");
		txt_VendorGroupConfigurationNumber = findElementInXLSheet(getParameterProcurement,
				"VendorGroupConfigurationNumber");
		txt_VendorGroupConfigurationSerch = findElementInXLSheet(getParameterProcurement,
				"VendorGroupConfigurationSerch");
		sel_selVendorGroupConfiguration = findElementInXLSheet(getParameterProcurement, "selVendorGroupConfiguration");
		// PR_VGC_003
		btn_vendorinfo = findElementInXLSheet(getParameterProcurement, "btnvendorinfo");
		btn_release = findElementInXLSheet(getParameterProcurement, "btnrelease");
		// PR_VI_001
		btn_veninfobutton = findElementInXLSheet(getParameterProcurement, "vendorinfobutton");
		btn_newvenbutton = findElementInXLSheet(getParameterProcurement, "newvendorbutton");
		txt_VendorInformationpage = findElementInXLSheet(getParameterProcurement, "VendorInformationpage");
		txt_newVendorInformationpage = findElementInXLSheet(getParameterProcurement, "newVendorInformationpage");
		// PR_VI_002
		txt_VendorType = findElementInXLSheet(getParameterProcurement, "VendorType");
		txt_VendorNameTitle = findElementInXLSheet(getParameterProcurement, "VendorNameTitle");
		txt_VendorName = findElementInXLSheet(getParameterProcurement, "VendorName");
		btn_ReceivableAccount = findElementInXLSheet(getParameterProcurement, "btnReceivableAccount");
		txt_ReceivableAccount = findElementInXLSheet(getParameterProcurement, "txtReceivableAccount");
		sel_ReceivableAccount = findElementInXLSheet(getParameterProcurement, "selReceivableAccount");
		txt_ContactTitle = findElementInXLSheet(getParameterProcurement, "ContactTitle");
		txt_ContactName = findElementInXLSheet(getParameterProcurement, "ContactName");
		txt_MobileNumber = findElementInXLSheet(getParameterProcurement, "MobileNumber");
		txt_SelectIndustry = findElementInXLSheet(getParameterProcurement, "SelectIndustry");
		txt_TaxRegistrationNumber = findElementInXLSheet(getParameterProcurement, "TaxRegistrationNumber");
		txt_ReferenceAccoutNo = findElementInXLSheet(getParameterProcurement, "ReferenceAccoutNo");
		btn_3rdPartyAccount = findElementInXLSheet(getParameterProcurement, "btn3rdPartyAccount");
		txt_3rdPartyAccount = findElementInXLSheet(getParameterProcurement, "txt3rdPartyAccount");
		sel_3rdPartyAccount = findElementInXLSheet(getParameterProcurement, "sel3rdPartyAccount");
		txt_PassportNo = findElementInXLSheet(getParameterProcurement, "PassportNo");
		txt_newVendorName = findElementInXLSheet(getParameterProcurement, "newVendorName");
		btn_VendorInformation = findElementInXLSheet(getParameterProcurement, "VendorInformation");
		txt_VendorInformationserch = findElementInXLSheet(getParameterProcurement, "VendorInformationserch");
		sel_selVendorInformation = findElementInXLSheet(getParameterProcurement, "selVendorInformation");
		btn_newReceivableAccount = findElementInXLSheet(getParameterProcurement, "newReceivableAccount");
		txt_newReceivableAccountname = findElementInXLSheet(getParameterProcurement, "newReceivableAccountname");
		txt_AccountGroup = findElementInXLSheet(getParameterProcurement, "AccountGroup");
		btn_btnUpdate = findElementInXLSheet(getParameterProcurement, "btnUpdateven");
		btn_btnReceivableAccountclose = findElementInXLSheet(getParameterProcurement, "btnReceivableAccountclose");

		// PR_VI_003
		btn_vendor = findElementInXLSheet(getParameterProcurement, "btnvendor");
		// PM_VA_001
		VendorAgreementbutton = findElementInXLSheet(getParameterProcurement, "VendorAgreementbutton");
		VendorAgreementpage = findElementInXLSheet(getParameterProcurement, "VendorAgreementpage");
		newVendorAgreementbutton = findElementInXLSheet(getParameterProcurement, "newVendorAgreementbutton");
		newVendorAgreementpage = findElementInXLSheet(getParameterProcurement, "newVendorAgreementpage");
		txt_AgreementNo = findElementInXLSheet(getParameterProcurement, "AgreementNo");
		txt_AgreementTitle = findElementInXLSheet(getParameterProcurement, "AgreementTitle");
		btn_btnAgreementDate = findElementInXLSheet(getParameterProcurement, "btnAgreementDate");
		btn_selAgreementDate = findElementInXLSheet(getParameterProcurement, "btnselAgreementDate");
		txt_AgreementGroup = findElementInXLSheet(getParameterProcurement, "AgreementGroup");
		txt_ACurrency = findElementInXLSheet(getParameterProcurement, "ACurrency");
		txt_ValidityPeriodFor = findElementInXLSheet(getParameterProcurement, "ValidityPeriodFor");
		txt_Description = findElementInXLSheet(getParameterProcurement, "Description");
		btn_btnVendorAccount = findElementInXLSheet(getParameterProcurement, "btnVendorAccount");
		txt_txtVendorAccount = findElementInXLSheet(getParameterProcurement, "txtVendorAccount");
		sel_selVendorAccount = findElementInXLSheet(getParameterProcurement, "selVendorAccount");
		btn_btnproductdetails = findElementInXLSheet(getParameterProcurement, "btnproductdetails");
		txt_ProductRelation = findElementInXLSheet(getParameterProcurement, "ProductRelation");
		btn_btnproduct = findElementInXLSheet(getParameterProcurement, "btnproduct");
		txt_txtproduct = findElementInXLSheet(getParameterProcurement, "txtproduct");
		sel_selproduct = findElementInXLSheet(getParameterProcurement, "selproduct");
		txt_minOrderQty = findElementInXLSheet(getParameterProcurement, "minOrderQty");
		txt_maxOrderQty = findElementInXLSheet(getParameterProcurement, "maxOrderQty");
		btn_btnupdatetask = findElementInXLSheet(getParameterProcurement, "btnupdatetask");
		btn_btnAssignTo = findElementInXLSheet(getParameterProcurement, "btnAssignTo");
		txt_txtAssignTo = findElementInXLSheet(getParameterProcurement, "txtAssignTo");
		sel_selAssignTo = findElementInXLSheet(getParameterProcurement, "selAssignTo");
		txt_txtsubject = findElementInXLSheet(getParameterProcurement, "txtsubject");

		// PM_PR_001
		btn_PurchaseRequisitionbutton = findElementInXLSheet(getParameterProcurement, "PurchaseRequisitionbutton");
		txt_PurchaseRequisitionbuttonpage = findElementInXLSheet(getParameterProcurement,
				"PurchaseRequisitionbuttonpage");
		btn_newPurchaseRequisitionbutton = findElementInXLSheet(getParameterProcurement,
				"newPurchaseRequisitionbutton");
		txt_newPurchaseRequisitionbuttonpage = findElementInXLSheet(getParameterProcurement,
				"newPurchaseRequisitionbuttonpage");
		// PM_PR_002
		btn_RequestDate = findElementInXLSheet(getParameterProcurement, "btnRequestDate");
		btn_selRequestDate = findElementInXLSheet(getParameterProcurement, "btnselRequestDate");
		txt_RCurrency = findElementInXLSheet(getParameterProcurement, "RCurrency");
		btn_Rbtnproduct = findElementInXLSheet(getParameterProcurement, "Rbtnproduct");
		txt_Rtxtproduct = findElementInXLSheet(getParameterProcurement, "Rtxtproduct");
		sel_Rselproduct = findElementInXLSheet(getParameterProcurement, "Rselproduct");
		btn_btnCheckout = findElementInXLSheet(getParameterProcurement, "btnCheckout");
		txt_PurchaseRequisitionNumber = findElementInXLSheet(getParameterProcurement, "PurchaseRequisitionNumber");
		btn_goPurchaseRequisitionpage = findElementInXLSheet(getParameterProcurement, "goPurchaseRequisitionpage");
		txt_serchPurchaseRequisition = findElementInXLSheet(getParameterProcurement, "serchPurchaseRequisition");
		sel_selPurchaseRequisitionNumber = findElementInXLSheet(getParameterProcurement,
				"selPurchaseRequisitionNumber");
		lnkInforOnLookupInformationReplace = findElementInXLSheet(getParameterProcurement,
				"lnkInforOnLookupInformationReplace");

		// PM_PR_003
		btn_btnPurchaseRequisition = findElementInXLSheet(getParameterProcurement, "btnPurchaseRequisition");

		// PM_PR_004
		btn_RbtnGridMasterInfo = findElementInXLSheet(getParameterProcurement, "RbtnGridMasterInfo");
		btn_RbtnProductPurchasingHistory = findElementInXLSheet(getParameterProcurement,
				"RbtnProductPurchasingHistory");
		txt_RPHUnitPrice = findElementInXLSheet(getParameterProcurement, "RPHUnitPrice");
		btn_RPHclose = findElementInXLSheet(getParameterProcurement, "RPHclose");
		txt_RUnitPrice = findElementInXLSheet(getParameterProcurement, "RUnitPrice");

		// PM_PR_005
		btn_btnRelPurchaseRequisition = findElementInXLSheet(getParameterProcurement, "btnRelPurchaseRequisition");
		btn_btnAction = findElementInXLSheet(getParameterProcurement, "btnAction");
		btn_btnConverttoPurchaseOrder = findElementInXLSheet(getParameterProcurement, "btnConverttoPurchaseOrder");
		btn_btnGeneratePO = findElementInXLSheet(getParameterProcurement, "btnGeneratePO");
		btn_btnPOtoShipmentCosting = findElementInXLSheet(getParameterProcurement, "btnPOtoShipmentCosting");
		btn_RRDclose = findElementInXLSheet(getParameterProcurement, "RRDclose");
		btn_btnVendor = findElementInXLSheet(getParameterProcurement, "btnVendor");
		txt_txtVendor = findElementInXLSheet(getParameterProcurement, "txtVendor");
		sel_selVendor = findElementInXLSheet(getParameterProcurement, "selVendor");
		btn_btnBillingAddress = findElementInXLSheet(getParameterProcurement, "btnBillingAddress");
		txt_txtBillingAddress = findElementInXLSheet(getParameterProcurement, "txtBillingAddress");
		btn_btnApply = findElementInXLSheet(getParameterProcurement, "btnApply");
		btn_btnShippingAddress = findElementInXLSheet(getParameterProcurement, "btnShippingAddress");
		txt_txtShippingAddress = findElementInXLSheet(getParameterProcurement, "txtShippingAddress");

		// PM_PO_001
		btn_PurchaseOrderbutton = findElementInXLSheet(getParameterProcurement, "PurchaseOrderbutton");
		txt_PurchaseOrderpage = findElementInXLSheet(getParameterProcurement, "PurchaseOrderpage");
		btn_newPurchaseOrderbutton = findElementInXLSheet(getParameterProcurement, "newPurchaseOrderbutton");
		txt_newPurchaseOrderpage = findElementInXLSheet(getParameterProcurement, "newPurchaseOrderpage");
		txt_InboundShipmentpage = findElementInXLSheet(getParameterProcurement, "InboundShipmentpage");
		txt_PurchaseInvoicepage = findElementInXLSheet(getParameterProcurement, "PurchaseInvoicepage");
		txt_ShipmentCostingInformationpage = findElementInXLSheet(getParameterProcurement,
				"ShipmentCostingInformationpage");
		btn_PbtnVendor = findElementInXLSheet(getParameterProcurement, "PbtnVendor");
		btn_Pbtndetails = findElementInXLSheet(getParameterProcurement, "Pbtndetails");
		btn_btnPInvoiceAccount = findElementInXLSheet(getParameterProcurement, "btnPInvoiceAccount");
		btn_btnsummary = findElementInXLSheet(getParameterProcurement, "btnsummary");
		btn_pbtnproduct = findElementInXLSheet(getParameterProcurement, "pbtnproduct");
		txt_ptxtproduct = findElementInXLSheet(getParameterProcurement, "ptxtproduct");
		sel_pselproduct = findElementInXLSheet(getParameterProcurement, "pselproduct");
		btn_btnGotopage = findElementInXLSheet(getParameterProcurement, "btnGotopage");
		btn_SerialRange = findElementInXLSheet(getParameterProcurement, "SerialRange");

		txt_pro7POqty1 = findElementInXLSheet(getParameterProcurement, "pro7POqty1");
		txt_pro7POqty2 = findElementInXLSheet(getParameterProcurement, "pro7POqty2");
		txt_pro7POqty3 = findElementInXLSheet(getParameterProcurement, "pro7POqty3");
		txt_pro7POqty4 = findElementInXLSheet(getParameterProcurement, "pro7POqty4");
		txt_pro7POqty5 = findElementInXLSheet(getParameterProcurement, "pro7POqty5");
		txt_pro7POqty6 = findElementInXLSheet(getParameterProcurement, "pro7POqty6");
		txt_pro7POqty7 = findElementInXLSheet(getParameterProcurement, "pro7POqty7");

		btn_pro7POLook1 = findElementInXLSheet(getParameterProcurement, "pro7POLook1");
		btn_pro7POLook2 = findElementInXLSheet(getParameterProcurement, "pro7POLook2");
		btn_pro7POLook3 = findElementInXLSheet(getParameterProcurement, "pro7POLook3");
		btn_pro7POLook4 = findElementInXLSheet(getParameterProcurement, "pro7POLook4");
		btn_pro7POLook5 = findElementInXLSheet(getParameterProcurement, "pro7POLook5");
		btn_pro7POLook6 = findElementInXLSheet(getParameterProcurement, "pro7POLook6");
		btn_pro7POLook7 = findElementInXLSheet(getParameterProcurement, "pro7POLook7");

		btn_pro7PILook1 = findElementInXLSheet(getParameterProcurement, "pro7PILook1");
		btn_pro7PILook2 = findElementInXLSheet(getParameterProcurement, "pro7PILook2");
		btn_pro7PILook3 = findElementInXLSheet(getParameterProcurement, "pro7PILook3");
		btn_pro7PILook4 = findElementInXLSheet(getParameterProcurement, "pro7PILook4");
		btn_pro7PILook5 = findElementInXLSheet(getParameterProcurement, "pro7PILook5");
		btn_pro7PILook6 = findElementInXLSheet(getParameterProcurement, "pro7PILook6");
		btn_pro7PILook7 = findElementInXLSheet(getParameterProcurement, "pro7PILook7");

		btn_pro7ISLook1 = findElementInXLSheet(getParameterProcurement, "pro7ISLook1");
		btn_pro7ISLook2 = findElementInXLSheet(getParameterProcurement, "pro7ISLook2");
		btn_pro7ISLook3 = findElementInXLSheet(getParameterProcurement, "pro7ISLook3");
		btn_pro7ISLook4 = findElementInXLSheet(getParameterProcurement, "pro7ISLook4");
		btn_pro7ISLook5 = findElementInXLSheet(getParameterProcurement, "pro7ISLook5");
		btn_pro7ISLook6 = findElementInXLSheet(getParameterProcurement, "pro7ISLook6");
		btn_pro7ISLook7 = findElementInXLSheet(getParameterProcurement, "pro7ISLook7");

		btn_POProductAvailability = findElementInXLSheet(getParameterProcurement, "POProductAvailability");
		txt_POProductAvailabilityValue = findElementInXLSheet(getParameterProcurement, "POProductAvailabilityValue");
		btn_pro7SCILook1 = findElementInXLSheet(getParameterProcurement, "pro7SCILook1");

		// PM_PO_002
		btn_btnPOtoPIService = findElementInXLSheet(getParameterProcurement, "btnPOtoPIService");
		txt_txtVendorReferenceNo = findElementInXLSheet(getParameterProcurement, "txtVendorReferenceNo");

		// PM_PO_003
		btn_btnLPOtoShipmentCosting = findElementInXLSheet(getParameterProcurement, "btnLPOtoShipmentCosting");
		btn_btndropdown = findElementInXLSheet(getParameterProcurement, "btndropdown");

		// PM_PO_005
		btn_btnConsolidatedPO = findElementInXLSheet(getParameterProcurement, "btnConsolidatedPO");
		btn_btnDocumentList = findElementInXLSheet(getParameterProcurement, "btnDocumentList");
		btn_btnproductline1 = findElementInXLSheet(getParameterProcurement, "btnproductline1");
		btn_btnproductline2 = findElementInXLSheet(getParameterProcurement, "btnproductline2");
		btn_btnPApply = findElementInXLSheet(getParameterProcurement, "btnPApply");

		// PM_PO_006
		btn_btnaddProduct = findElementInXLSheet(getParameterProcurement, "btnaddProduct");
		btn_pbtnNxtproduct = findElementInXLSheet(getParameterProcurement, "pbtnNxtproduct");
		txt_ptxtNxtproduct = findElementInXLSheet(getParameterProcurement, "ptxtNxtproduct");
		btn_btnCostAllocation = findElementInXLSheet(getParameterProcurement, "btnCostAllocation");
		btn_ImportCostEstimationShipmentbutton = findElementInXLSheet(getParameterProcurement,
				"ImportCostEstimationShipmentbutton");
		txt_txtCEstDescription = findElementInXLSheet(getParameterProcurement, "txtCEstDescription");
		txt_txtCostingType = findElementInXLSheet(getParameterProcurement, "txtCostingType");
		txt_EstimateCost = findElementInXLSheet(getParameterProcurement, "EstimateCost");
		btn_Apportionmentbutton = findElementInXLSheet(getParameterProcurement, "Apportionmentbutton");
		txt_ApportionmentBasis = findElementInXLSheet(getParameterProcurement, "ApportionmentBasis");
		btn_AppoCheckout = findElementInXLSheet(getParameterProcurement, "AppoCheckout");
		btn_AppoApply = findElementInXLSheet(getParameterProcurement, "AppoApply");
		btn_btnGenerateEstimation = findElementInXLSheet(getParameterProcurement, "btnGenerateEstimation");
		btn_btnApplytomaingrid = findElementInXLSheet(getParameterProcurement, "btnApplytomaingrid");
		btn_ApponxtCheckout = findElementInXLSheet(getParameterProcurement, "ApponxtCheckout");
		txt_ptxtBproduct = findElementInXLSheet(getParameterProcurement, "ptxtBproduct");
		btn_btnSerialBatch = findElementInXLSheet(getParameterProcurement, "btnSerialBatch");
		btn_btnselProductList = findElementInXLSheet(getParameterProcurement, "btnselProductList");
		txt_BatchNo = findElementInXLSheet(getParameterProcurement, "BatchNo");
		txt_SerialNo = findElementInXLSheet(getParameterProcurement, "SerialNo");
		btn_updateSerial = findElementInXLSheet(getParameterProcurement, "updateSerial");
		btn_closeProductList = findElementInXLSheet(getParameterProcurement, "closeProductList");
		btn_ExpiryDate = findElementInXLSheet(getParameterProcurement, "ExpiryDate");
		btn_selExpiryDate = findElementInXLSheet(getParameterProcurement, "selExpiryDate");

		// PM_PO_007
		btn_ImportEstimationbutton = findElementInXLSheet(getParameterProcurement, "ImportEstimationbutton");
		btn_addNewEstimationbutton = findElementInXLSheet(getParameterProcurement, "addNewEstimationbutton");
		txt_txtQESTDescription = findElementInXLSheet(getParameterProcurement, "txtQESTDescription");
		txt_EstimatedQty = findElementInXLSheet(getParameterProcurement, "EstimatedQty");
		btn_EstimationDetailsbutton = findElementInXLSheet(getParameterProcurement, "EstimationDetailsbutton");
		txt_CostingType = findElementInXLSheet(getParameterProcurement, "CostingType");
		txt_EstimatenewCost = findElementInXLSheet(getParameterProcurement, "EstimatenewCost");
		btn_Summary = findElementInXLSheet(getParameterProcurement, "Summary");
		btn_Checkout = findElementInXLSheet(getParameterProcurement, "Checkout");
		btn_btnnxtGenerateEstimation = findElementInXLSheet(getParameterProcurement, "btnnxtGenerateEstimation");

		// PM_PI_001
		btn_PurchaseInvoicebutton = findElementInXLSheet(getParameterProcurement, "PurchaseInvoicebutton");
		txt_PurchaseInvoicebuttonpage = findElementInXLSheet(getParameterProcurement, "PurchaseInvoicebuttonpage");
		btn_newPurchaseInvoicebutton = findElementInXLSheet(getParameterProcurement, "newPurchaseInvoicebutton");
		txt_newPurchaseInvoicebuttonpage = findElementInXLSheet(getParameterProcurement,
				"newPurchaseInvoicebuttonpage");
		btn_btnPIToShipmentCosting = findElementInXLSheet(getParameterProcurement, "btnPIToShipmentCosting");
		txt_PIcurrency = findElementInXLSheet(getParameterProcurement, "PIcurrency");
		btn_btnPIVendor = findElementInXLSheet(getParameterProcurement, "btnPIVendor");
		txt_txtPIVendor = findElementInXLSheet(getParameterProcurement, "txtPIVendor");
		sel_selPIVendor = findElementInXLSheet(getParameterProcurement, "selPIVendor");
		btn_btnPIBillingAddress = findElementInXLSheet(getParameterProcurement, "btnPIBillingAddress");
		txt_txtPIBillingAddress = findElementInXLSheet(getParameterProcurement, "txtPIBillingAddress");
		btn_btnPIShippingAddress = findElementInXLSheet(getParameterProcurement, "btnPIShippingAddress");
		txt_txtPIShippingAddress = findElementInXLSheet(getParameterProcurement, "txtPIShippingAddress");
		btn_PIbtnproduct = findElementInXLSheet(getParameterProcurement, "PIbtnproduct");
		txt_PItxtproduct = findElementInXLSheet(getParameterProcurement, "PItxtproduct");
		sel_PIselproduct = findElementInXLSheet(getParameterProcurement, "PIselproduct");
		btn_PIbtnNxtproduct = findElementInXLSheet(getParameterProcurement, "PIbtnNxtproduct");
		txt_PItxtNxtproduct = findElementInXLSheet(getParameterProcurement, "PItxtNxtproduct");
		btn_PIbtnaddProduct = findElementInXLSheet(getParameterProcurement, "PIbtnaddProduct");

		// PM_PI_002
		btn_btnPIToInboundShipment = findElementInXLSheet(getParameterProcurement, "btnPIToInboundShipment");

		// PM_PI_003
		btn_btnPIService = findElementInXLSheet(getParameterProcurement, "btnPIService");
		btn_btnPIsummary = findElementInXLSheet(getParameterProcurement, "btnPIsummary");
		txt_PItxtSproduct = findElementInXLSheet(getParameterProcurement, "PItxtSproduct");

		// PM_PRO_001
		btn_PurchaseReturnOrderbutton = findElementInXLSheet(getParameterProcurement, "PurchaseReturnOrderbutton");
		btn_newPurchaseReturnOrderbutton = findElementInXLSheet(getParameterProcurement,
				"newPurchaseReturnOrderbutton");
		btn_btnPROVendor = findElementInXLSheet(getParameterProcurement, "btnPROVendor");
		txt_txtPROVendor = findElementInXLSheet(getParameterProcurement, "txtPROVendor");
		sel_selPROVendor = findElementInXLSheet(getParameterProcurement, "selPROVendor");
		btn_documentlistbutton = findElementInXLSheet(getParameterProcurement, "documentlistbutton");
		btn_PROsearch = findElementInXLSheet(getParameterProcurement, "PROsearch");
		btn_PROcheckbox = findElementInXLSheet(getParameterProcurement, "PROcheckbox");
		btn_PROapply = findElementInXLSheet(getParameterProcurement, "PROapply");
		txt_PROqty = findElementInXLSheet(getParameterProcurement, "PROqty");
		btn_PROdraft = findElementInXLSheet(getParameterProcurement, "PROdraft");
		btn_btnPOBillingAddress = findElementInXLSheet(getParameterProcurement, "btnPOBillingAddress");
		txt_txtPOBillingAddress = findElementInXLSheet(getParameterProcurement, "txtPOBillingAddress");
		btn_btnPOShippingAddress = findElementInXLSheet(getParameterProcurement, "btnPOShippingAddress");
		txt_txtPOShippingAddress = findElementInXLSheet(getParameterProcurement, "txtPOShippingAddress");
		btn_btnPOApply = findElementInXLSheet(getParameterProcurement, "btnPOApply");
		btn_btnPOSerialNo = findElementInXLSheet(getParameterProcurement, "btnPOSerialNo");

		// PM_PRO_002
		btn_PObtnproduct = findElementInXLSheet(getParameterProcurement, "PObtnproduct");
		txt_POtxtproduct = findElementInXLSheet(getParameterProcurement, "POtxtproduct");
		sel_POselproduct = findElementInXLSheet(getParameterProcurement, "POselproduct");
		btn_POwarehouse = findElementInXLSheet(getParameterProcurement, "POwarehouse");
		txt_POSerialNo = findElementInXLSheet(getParameterProcurement, "POSerialNo");
		btn_POMasterInfo = findElementInXLSheet(getParameterProcurement, "POMasterInfo");
		btn_POProductBS = findElementInXLSheet(getParameterProcurement, "POProductB/S");
		txt_POSNo = findElementInXLSheet(getParameterProcurement, "POSNo");
		btn_POMasterInfoclose = findElementInXLSheet(getParameterProcurement, "POMasterInfoclose");
		btn_PROLook1 = findElementInXLSheet(getParameterProcurement, "PROLook1");
		btn_PRILook1 = findElementInXLSheet(getParameterProcurement, "PRILook1");
		txt_POProductAvailabilityValue1 = findElementInXLSheet(getParameterProcurement, "POProductAvailabilityValue1");

		txt_pageDraft = findElementInXLSheet(getParameterProcurement, "pageDraft");
		txt_pageRelease = findElementInXLSheet(getParameterProcurement, "pageRelease");

		// PM_RFQ_001
		btn_RequestForQuotation = findElementInXLSheet(getParameterProcurement, "RequestForQuotation");
		btn_NewRequestForQuotation = findElementInXLSheet(getParameterProcurement, "NewRequestForQuotation");
		txt_RequestForQuotationpage = findElementInXLSheet(getParameterProcurement, "RequestForQuotationpage");
		txt_NewRequestForQuotationpage = findElementInXLSheet(getParameterProcurement, "NewRequestForQuotationpage");
		btn_btncontactperson = findElementInXLSheet(getParameterProcurement, "btncontactperson");
		txt_txtcontactperson = findElementInXLSheet(getParameterProcurement, "txtcontactperson");
		sel_selcontactperson = findElementInXLSheet(getParameterProcurement, "selcontactperson");
		btn_btnRFQVendor = findElementInXLSheet(getParameterProcurement, "btnRFQVendor");
		txt_txtRFQVendor = findElementInXLSheet(getParameterProcurement, "txtRFQVendor");
		sel_selRFQVendor = findElementInXLSheet(getParameterProcurement, "selRFQVendor");
		btn_btnRFQproduct = findElementInXLSheet(getParameterProcurement, "btnRFQproduct");
		txt_txtRFQproduct = findElementInXLSheet(getParameterProcurement, "txtRFQproduct");
		sel_selRFQproduct = findElementInXLSheet(getParameterProcurement, "selRFQproduct");
		txt_RFQqty = findElementInXLSheet(getParameterProcurement, "RFQqty");
		btn_RFQdeadline = findElementInXLSheet(getParameterProcurement, "RFQdeadline");
		sel_selRFQdeadline = findElementInXLSheet(getParameterProcurement, "selRFQdeadline");
		btn_QuotationTab = findElementInXLSheet(getParameterProcurement, "QuotationTab");
		btn_RFQadd = findElementInXLSheet(getParameterProcurement, "RFQadd");
		txt_RFQaddVendor = findElementInXLSheet(getParameterProcurement, "RFQaddVendor");
		txt_RFQQuotationNo = findElementInXLSheet(getParameterProcurement, "RFQQuotationNo");
		txt_RFQShippingTerm = findElementInXLSheet(getParameterProcurement, "RFQShippingTerm");
		txt_RFQUnitPrice = findElementInXLSheet(getParameterProcurement, "RFQUnitPrice");
		txt_RFQMinQty = findElementInXLSheet(getParameterProcurement, "RFQMinQty");
		txt_RFQMaxQty = findElementInXLSheet(getParameterProcurement, "RFQMaxQty");
		btn_RFQcheckout = findElementInXLSheet(getParameterProcurement, "RFQcheckout");
		btn_RFQapply = findElementInXLSheet(getParameterProcurement, "RFQapply");
		btn_RFQsummarytab = findElementInXLSheet(getParameterProcurement, "RFQsummarytab");
		btn_qualifiedvendorbutton = findElementInXLSheet(getParameterProcurement, "qualifiedvendorbutton");
		btn_Qualifiedtik = findElementInXLSheet(getParameterProcurement, "Qualifiedtik");
		btn_RFQQualifiedapply = findElementInXLSheet(getParameterProcurement, "RFQQualifiedapply");
		btn_Update = findElementInXLSheet(getParameterProcurement, "btnUpdate");
		btn_btnRFQapply = findElementInXLSheet(getParameterProcurement, "btnRFQapply");
		btn_btnbackground = findElementInXLSheet(getParameterProcurement, "bbtnbackground");
		btn_Edit = findElementInXLSheet(getParameterProcurement, "btnEdit");

		// SMOKE_PRO_03
		btn_ImportCostEstimation = findElementInXLSheet(getParameterProcurement, "ImportCostEstimation");
		ImportCostEstimationpage = findElementInXLSheet(getParameterProcurement, "ImportCostEstimationpage");
		btn_newImportCostEstimation = findElementInXLSheet(getParameterProcurement, "newImportCostEstimation");
		newImportCostEstimationpage = findElementInXLSheet(getParameterProcurement, "newImportCostEstimationpage");
		txt_ICEDescription = findElementInXLSheet(getParameterProcurement, "ICEDescription");
		btn_ICEProductbutoon = findElementInXLSheet(getParameterProcurement, "ICEProductbutoon");
		txt_ICEProducttxt = findElementInXLSheet(getParameterProcurement, "ICEProducttxt");
		sel_ICEProductsel = findElementInXLSheet(getParameterProcurement, "ICEProductsel");
		btn_ICEEstimationDetails = findElementInXLSheet(getParameterProcurement, "ICEEstimationDetails");
		txt_ICECostingType = findElementInXLSheet(getParameterProcurement, "ICECostingType");
		txt_ICEEstimateCost = findElementInXLSheet(getParameterProcurement, "ICEEstimateCost");
		btn_ICESummary = findElementInXLSheet(getParameterProcurement, "ICESummary");
		btn_ICECheckout = findElementInXLSheet(getParameterProcurement, "ICECheckout");
		btn_ICEDraft = findElementInXLSheet(getParameterProcurement, "ICEDraft");
		btn_ICERelease = findElementInXLSheet(getParameterProcurement, "ICERelease");

		// SMOKE_PRO_05
		btn_PurchaseDesk = findElementInXLSheet(getParameterProcurement, "PurchaseDesk");
		PurchaseDeskpage = findElementInXLSheet(getParameterProcurement, "PurchaseDeskpage");
		sel_Requisitionsel = findElementInXLSheet(getParameterProcurement, "Requisitionsel");
		txt_SearchPRN = findElementInXLSheet(getParameterProcurement, "SearchPRN");
		btn_SearchbuttonPRN = findElementInXLSheet(getParameterProcurement, "SearchbuttonPRN");

		// SMOKE_PRO_10
		btn_DocFlow = findElementInXLSheet(getParameterProcurement, "DocFlow");

		// product7

		PRPlus1 = findElementInXLSheet(getParameterProcurement, "PRPlus1");
		PRPlus2 = findElementInXLSheet(getParameterProcurement, "PRPlus2");
		PRPlus3 = findElementInXLSheet(getParameterProcurement, "PRPlus3");
		PRPlus4 = findElementInXLSheet(getParameterProcurement, "PRPlus4");
		PRPlus5 = findElementInXLSheet(getParameterProcurement, "PRPlus5");
		PRPlus6 = findElementInXLSheet(getParameterProcurement, "PRPlus6");
		PRPro2 = findElementInXLSheet(getParameterProcurement, "PRPro2");
		PRPro3 = findElementInXLSheet(getParameterProcurement, "PRPro3");
		PRPro4 = findElementInXLSheet(getParameterProcurement, "PRPro4");
		PRPro5 = findElementInXLSheet(getParameterProcurement, "PRPro5");
		PRPro6 = findElementInXLSheet(getParameterProcurement, "PRPro6");
		PRPro7 = findElementInXLSheet(getParameterProcurement, "PRPro7");

		POPlus1 = findElementInXLSheet(getParameterProcurement, "POPlus1");
		POPlus2 = findElementInXLSheet(getParameterProcurement, "POPlus2");
		POPlus3 = findElementInXLSheet(getParameterProcurement, "POPlus3");
		POPlus4 = findElementInXLSheet(getParameterProcurement, "POPlus4");
		POPlus5 = findElementInXLSheet(getParameterProcurement, "POPlus5");
		POPlus6 = findElementInXLSheet(getParameterProcurement, "POPlus6");
		POPro2 = findElementInXLSheet(getParameterProcurement, "POPro2");
		POPro3 = findElementInXLSheet(getParameterProcurement, "POPro3");
		POPro4 = findElementInXLSheet(getParameterProcurement, "POPro4");
		POPro5 = findElementInXLSheet(getParameterProcurement, "POPro5");
		POPro6 = findElementInXLSheet(getParameterProcurement, "POPro6");
		POPro7 = findElementInXLSheet(getParameterProcurement, "POPro7");

		Cap1 = findElementInXLSheet(getParameterProcurement, "Cap1");
		Cap2 = findElementInXLSheet(getParameterProcurement, "Cap2");
		Cap4 = findElementInXLSheet(getParameterProcurement, "Cap4");
		Cap5 = findElementInXLSheet(getParameterProcurement, "Cap5");
		Cap6 = findElementInXLSheet(getParameterProcurement, "Cap6");
		Cap7 = findElementInXLSheet(getParameterProcurement, "Cap7");

		BatchNu = findElementInXLSheet(getParameterProcurement, "BatchNu");
		BatchLotNu = findElementInXLSheet(getParameterProcurement, "BatchLotNu");
		BatchExpiryDate = findElementInXLSheet(getParameterProcurement, "BatchExpiryDate");
		SerialExpiryDate = findElementInXLSheet(getParameterProcurement, "SerialExpiryDate");
		BatchExpiryDateSel = findElementInXLSheet(getParameterProcurement, "BatchExpiryDateSel");
		SerialFromNu = findElementInXLSheet(getParameterProcurement, "SerialFromNu");
		SerialLotNu = findElementInXLSheet(getParameterProcurement, "SerialLotNu");
		SerialBatchNu = findElementInXLSheet(getParameterProcurement, "SerialBatchNu");

		PIPlus1 = findElementInXLSheet(getParameterProcurement, "PIPlus1");
		PIPro1 = findElementInXLSheet(getParameterProcurement, "PIPro1");

		RFQPro1 = findElementInXLSheet(getParameterProcurement, "RFQPro1");
		RFQPlus1 = findElementInXLSheet(getParameterProcurement, "RFQPlus1");
		RFQPlus2 = findElementInXLSheet(getParameterProcurement, "RFQPlus2");
		RFQPlus3 = findElementInXLSheet(getParameterProcurement, "RFQPlus3");
		RFQPlus4 = findElementInXLSheet(getParameterProcurement, "RFQPlus4");
		RFQPlus5 = findElementInXLSheet(getParameterProcurement, "RFQPlus5");
		RFQPlus6 = findElementInXLSheet(getParameterProcurement, "RFQPlus6");

		RFQUnitPrice2 = findElementInXLSheet(getParameterProcurement, "RFQUnitPrice2");
		RFQUnitPrice3 = findElementInXLSheet(getParameterProcurement, "RFQUnitPrice3");
		RFQUnitPrice4 = findElementInXLSheet(getParameterProcurement, "RFQUnitPrice4");
		RFQUnitPrice5 = findElementInXLSheet(getParameterProcurement, "RFQUnitPrice5");
		RFQUnitPrice6 = findElementInXLSheet(getParameterProcurement, "RFQUnitPrice6");
		RFQUnitPrice7 = findElementInXLSheet(getParameterProcurement, "RFQUnitPrice7");

		RFQMinQty2 = findElementInXLSheet(getParameterProcurement, "RFQMinQty2");
		RFQMinQty3 = findElementInXLSheet(getParameterProcurement, "RFQMinQty3");
		RFQMinQty4 = findElementInXLSheet(getParameterProcurement, "RFQMinQty4");
		RFQMinQty5 = findElementInXLSheet(getParameterProcurement, "RFQMinQty5");
		RFQMinQty6 = findElementInXLSheet(getParameterProcurement, "RFQMinQty6");
		RFQMinQty7 = findElementInXLSheet(getParameterProcurement, "RFQMinQty7");

		RFQMaxQty2 = findElementInXLSheet(getParameterProcurement, "RFQMaxQty2");
		RFQMaxQty3 = findElementInXLSheet(getParameterProcurement, "RFQMaxQty3");
		RFQMaxQty4 = findElementInXLSheet(getParameterProcurement, "RFQMaxQty4");
		RFQMaxQty5 = findElementInXLSheet(getParameterProcurement, "RFQMaxQty5");
		RFQMaxQty6 = findElementInXLSheet(getParameterProcurement, "RFQMaxQty6");
		RFQMaxQty7 = findElementInXLSheet(getParameterProcurement, "RFQMaxQty7");

		Qualifiedtik2 = findElementInXLSheet(getParameterProcurement, "Qualifiedtik2");
		Qualifiedtik3 = findElementInXLSheet(getParameterProcurement, "Qualifiedtik3");
		Qualifiedtik4 = findElementInXLSheet(getParameterProcurement, "Qualifiedtik4");
		Qualifiedtik5 = findElementInXLSheet(getParameterProcurement, "Qualifiedtik5");
		Qualifiedtik6 = findElementInXLSheet(getParameterProcurement, "Qualifiedtik6");
		Qualifiedtik7 = findElementInXLSheet(getParameterProcurement, "Qualifiedtik7");

		nextdate = findElementInXLSheet(getParameterProcurement, "nextdate");
		btn_btnPOtoPurchaseInvoice = findElementInXLSheet(getParameterProcurement, "btnPOtoPurchaseInvoice");

		pro7qty1 = findElementInXLSheet(getParameterProcurement, "pro7qty1");
		pro7qty2 = findElementInXLSheet(getParameterProcurement, "pro7qty2");
		pro7qty3 = findElementInXLSheet(getParameterProcurement, "pro7qty3");
		pro7qty4 = findElementInXLSheet(getParameterProcurement, "pro7qty4");
		pro7qty5 = findElementInXLSheet(getParameterProcurement, "pro7qty5");
		pro7qty6 = findElementInXLSheet(getParameterProcurement, "pro7qty6");
		pro7qty7 = findElementInXLSheet(getParameterProcurement, "pro7qty7");
		pro7price1 = findElementInXLSheet(getParameterProcurement, "pro7price1");
		pro7price2 = findElementInXLSheet(getParameterProcurement, "pro7price2");
		pro7price3 = findElementInXLSheet(getParameterProcurement, "pro7price3");
		pro7price4 = findElementInXLSheet(getParameterProcurement, "pro7price4");
		pro7price5 = findElementInXLSheet(getParameterProcurement, "pro7price5");
		pro7price6 = findElementInXLSheet(getParameterProcurement, "pro7price6");
		pro7price7 = findElementInXLSheet(getParameterProcurement, "pro7price7");

		Warehouse1 = findElementInXLSheet(getParameterProcurement, "Warehouse1");
		Warehouse2 = findElementInXLSheet(getParameterProcurement, "Warehouse2");
		Warehouse3 = findElementInXLSheet(getParameterProcurement, "Warehouse3");
		Warehouse4 = findElementInXLSheet(getParameterProcurement, "Warehouse4");
		Warehouse5 = findElementInXLSheet(getParameterProcurement, "Warehouse5");
		Warehouse6 = findElementInXLSheet(getParameterProcurement, "Warehouse6");
		Warehouse7 = findElementInXLSheet(getParameterProcurement, "Warehouse7");

		WarehousePI1 = findElementInXLSheet(getParameterProcurement, "WarehousePI1");
		WarehousePI2 = findElementInXLSheet(getParameterProcurement, "WarehousePI2");
		WarehousePI3 = findElementInXLSheet(getParameterProcurement, "WarehousePI3");
		WarehousePI4 = findElementInXLSheet(getParameterProcurement, "WarehousePI4");
		WarehousePI5 = findElementInXLSheet(getParameterProcurement, "WarehousePI5");
		WarehousePI6 = findElementInXLSheet(getParameterProcurement, "WarehousePI6");
		WarehousePI7 = findElementInXLSheet(getParameterProcurement, "WarehousePI7");

		pageDraft = findElementInXLSheet(getParameterProcurement, "pageDraft");
		pageRelease = findElementInXLSheet(getParameterProcurement, "pageRelease");
		Status = findElementInXLSheet(getParameterProcurement, "Status");
		Header = findElementInXLSheet(getParameterProcurement, "Header");

		Product = findElementInXLSheet(getParameterProcurement, "Product");
		ProductQty = findElementInXLSheet(getParameterProcurement, "ProductQty");

		// Master_1

		btn_Delete = findElementInXLSheet(getParameterProcurement, "Delete");
		txt_DeleteText = findElementInXLSheet(getParameterProcurement, "DeleteText");
		btn_Yes = findElementInXLSheet(getParameterProcurement, "Yes");
		btn_History = findElementInXLSheet(getParameterProcurement, "History");
		txt_HistoryText = findElementInXLSheet(getParameterProcurement, "HistoryText");
		btn_Activities = findElementInXLSheet(getParameterProcurement, "Activities");
		txt_ActivitiesText = findElementInXLSheet(getParameterProcurement, "ActivitiesText");
		btn_headerclose = findElementInXLSheet(getParameterProcurement, "headerclose");
		btn_Duplicate = findElementInXLSheet(getParameterProcurement, "Duplicate");
		txt_lblVendorGroup = findElementInXLSheet(getParameterProcurement, "lblVendorGroup");
		txt_dupVendorGroup = findElementInXLSheet(getParameterProcurement, "dupVendorGroup");
		btn_btnEdit = findElementInXLSheet(getParameterProcurement, "btnEdit");
		txt_ReleseText = findElementInXLSheet(getParameterProcurement, "ReleseText");
		btn_DraftAndNew = findElementInXLSheet(getParameterProcurement, "Draft&New");
		btn_UpdateAndNew = findElementInXLSheet(getParameterProcurement, "Update&New");

		// Master_2
		btn_VendorGroupbtn = findElementInXLSheet(getParameterProcurement, "VendorGroupbtn");
		btn_VendorGroupAdd = findElementInXLSheet(getParameterProcurement, "VendorGroupAdd");
		txt_newVendorGroupTxt = findElementInXLSheet(getParameterProcurement, "newVendorGroupTxt");
		btn_VendorGroupUpdate = findElementInXLSheet(getParameterProcurement, "VendorGroupUpdate");
		btn_Reminders = findElementInXLSheet(getParameterProcurement, "Reminders");
		txt_RemindersText = findElementInXLSheet(getParameterProcurement, "RemindersText");
		btn_ActivitiesHeaderclose = findElementInXLSheet(getParameterProcurement, "ActivitiesHeaderclose");

		// Master_3
		btn_ConverttoPurchaseOrder = findElementInXLSheet(getParameterProcurement, "ConverttoPurchaseOrder");
		btn_Reverse = findElementInXLSheet(getParameterProcurement, "Reverse");
		txt_ReverseText = findElementInXLSheet(getParameterProcurement, "ReverseText");
		btn_CopyFrom = findElementInXLSheet(getParameterProcurement, "CopyFrom");
		txt_CopyFromText = findElementInXLSheet(getParameterProcurement, "CopyFromText");

		// Master_4
		btn_CustomerDemand = findElementInXLSheet(getParameterProcurement, "CustomerDemand");
		btn_CustomerDemandsel = findElementInXLSheet(getParameterProcurement, "CustomerDemandsel");
		btn_GenerateProductionOrder = findElementInXLSheet(getParameterProcurement, "GenerateProductionOrder");
		btn_GenerateAssemblyProposal = findElementInXLSheet(getParameterProcurement, "GenerateAssemblyProposal");
		txt_GenerateProductionOrdertxt = findElementInXLSheet(getParameterProcurement, "GenerateProductionOrdertxt");
		txt_GenerateAssemblyProposaltxt = findElementInXLSheet(getParameterProcurement, "GenerateAssemblyProposaltxt");

		// Transaction_3
		btn_Complete = findElementInXLSheet(getParameterProcurement, "Complete");
		txt_CompleteText = findElementInXLSheet(getParameterProcurement, "CompleteText");

		// Transaction_4
		btn_AddNewDocument = findElementInXLSheet(getParameterProcurement, "AddNewDocument");
		ZoomBar = findElementInXLSheet(getParameterProcurement, "ZoomBar");
		btn_No = findElementInXLSheet(getParameterProcurement, "No");
		btn_ReverseButton = findElementInXLSheet(getParameterProcurement, "ReverseButton");
		btn_Print = findElementInXLSheet(getParameterProcurement, "Print");
		txt_PrintoutTemplates = findElementInXLSheet(getParameterProcurement, "PrintoutTemplates");
		btn_Back = findElementInXLSheet(getParameterProcurement, "Back");
		btn_RackwiseAvailable = findElementInXLSheet(getParameterProcurement, "RackwiseAvailable");

		// Transaction_5
		btn_Acknowledgement = findElementInXLSheet(getParameterProcurement, "Acknowledgement");
		txt_Acknowledgementtxt = findElementInXLSheet(getParameterProcurement, "Acknowledgementtxt");
		btn_ChangeCostAllocations = findElementInXLSheet(getParameterProcurement, "ChangeCostAllocations");
		txt_ChangeCostAllocationstxt = findElementInXLSheet(getParameterProcurement, "ChangeCostAllocationstxt");
		txt_PurchaseInvoicetxt = findElementInXLSheet(getParameterProcurement, "PurchaseInvoicetxt");

		// Transaction_6
		btn_CostEstimation = findElementInXLSheet(getParameterProcurement, "CostEstimation");
		txt_CostEstimationtxt = findElementInXLSheet(getParameterProcurement, "CostEstimationtxt");
		btn_Close = findElementInXLSheet(getParameterProcurement, "Close");
		txt_Closetxt = findElementInXLSheet(getParameterProcurement, "Closetxt");
		btn_Hold = findElementInXLSheet(getParameterProcurement, "Hold");
		btn_DotheOutboundAdvance = findElementInXLSheet(getParameterProcurement, "DotheOutboundAdvance");
		txt_DotheOutboundAdvancetxt = findElementInXLSheet(getParameterProcurement, "DotheOutboundAdvancetxt");
		btn_ExtendExpiryDate = findElementInXLSheet(getParameterProcurement, "ExtendExpiryDate");
		txt_ExtendExpiryDatetxt = findElementInXLSheet(getParameterProcurement, "ExtendExpiryDatetxt");
		btn_UpdateLogisticDates = findElementInXLSheet(getParameterProcurement, "UpdateLogisticDates");
		txt_UpdateLogisticDatestxt = findElementInXLSheet(getParameterProcurement, "UpdateLogisticDatestxt");
		btn_UnHold = findElementInXLSheet(getParameterProcurement, "UnHold");
		txt_Reason = findElementInXLSheet(getParameterProcurement, "Reason");
		btn_OK = findElementInXLSheet(getParameterProcurement, "OK");
		txt_PurchaseOrdertxt = findElementInXLSheet(getParameterProcurement, "PurchaseOrdertxt");
		btn_ImportShipment = findElementInXLSheet(getParameterProcurement, "ImportShipment");
		txt_ImportShipmenttxt = findElementInXLSheet(getParameterProcurement, "ImportShipmenttxt");
		btn_CostEstimationClose = findElementInXLSheet(getParameterProcurement, "CostEstimationClose");

		// Transaction_8
		btn_GeneratePurchaseOrder = findElementInXLSheet(getParameterProcurement, "GeneratePurchaseOrder");

		// Master_001
		btn_SalesAndMarketing = findElementInXLSheet(getParameterProcurement, "SalesAndMarketing");
		btn_AccountGroupConfiguration = findElementInXLSheet(getParameterProcurement, "AccountGroupConfiguration");
		btn_NewAccountGroupConfiguration = findElementInXLSheet(getParameterProcurement,
				"NewAccountGroupConfiguration");
		btn_addAccountGroup = findElementInXLSheet(getParameterProcurement, "addAccountGroup");
		btn_addNewAccountGroup = findElementInXLSheet(getParameterProcurement, "addNewAccountGroup");
		txt_NewAccountGrouptxt = findElementInXLSheet(getParameterProcurement, "NewAccountGrouptxt");
		btn_AccountGroupUpdate = findElementInXLSheet(getParameterProcurement, "AccountGroupUpdate");
		sel_AccountGroupsel = findElementInXLSheet(getParameterProcurement, "AccountGroupsel");

		btn_InventoryAndWarehousing = findElementInXLSheet(getParameterProcurement, "InventoryAndWarehousing");
		btn_WarehouseInformation = findElementInXLSheet(getParameterProcurement, "WarehouseInformation");
		btn_NewWarehouse = findElementInXLSheet(getParameterProcurement, "NewWarehouse");
		txt_WarehouseCode = findElementInXLSheet(getParameterProcurement, "WarehouseCode");
		txt_WarehouseName = findElementInXLSheet(getParameterProcurement, "WarehouseName");
		txt_QuaranWarehouse = findElementInXLSheet(getParameterProcurement, "QuaranWarehouse");
		btn_AutoLot = findElementInXLSheet(getParameterProcurement, "AutoLot");
		txt_LotBookNo = findElementInXLSheet(getParameterProcurement, "LotBookNo");
		btn_AgreementGroupAdd = findElementInXLSheet(getParameterProcurement, "AgreementGroupAdd");
		btn_AgreementGroupParameters = findElementInXLSheet(getParameterProcurement, "AgreementGroupParameters");
		txt_AgreementGroupParameterstxt = findElementInXLSheet(getParameterProcurement, "AgreementGroupParameterstxt");
		btn_AgreementGroupParametersUpdate = findElementInXLSheet(getParameterProcurement,
				"AgreementGroupParametersUpdate");

		// PM_PI_001to006Reg
		txt_PurchaseInvoicePageHeaderA = findElementInXLSheet(getParameterProcurement, "PurchaseInvoicePageHeaderA");
		txt_NewPageHeaderTextA = findElementInXLSheet(getParameterProcurement, "NewPageHeaderTextA");
		txt_VendorFieldValidatorA = findElementInXLSheet(getParameterProcurement, "VendorFieldValidatorA");
		txt_CurrencyFieldValidatorA = findElementInXLSheet(getParameterProcurement, "CurrencyFieldValidatorA");
		btn_ProductGridErrorBtnA = findElementInXLSheet(getParameterProcurement, "ProductGridErrorBtnA");
		txt_ProductIsRequiredErrorA = findElementInXLSheet(getParameterProcurement, "ProductIsRequiredErrorA");
		txt_QtyIsRequiredErrorA = findElementInXLSheet(getParameterProcurement, "QtyIsRequiredErrorA");
		txt_InvoiceAccountValidatorA = findElementInXLSheet(getParameterProcurement, "InvoiceAccountValidatorA");
		btn_DetailsTabBtnA = findElementInXLSheet(getParameterProcurement, "DetailsTabBtnA");
		btn_SummaryTabBtnA = findElementInXLSheet(getParameterProcurement, "SummaryTabBtnA");
		btn_PricingAndChargesTabBtnA = findElementInXLSheet(getParameterProcurement, "PricingAndChargesTabBtnA");
		txt_VendorTextFieldA = findElementInXLSheet(getParameterProcurement, "VendorTextFieldA");
		txt_CurrencyTextFieldA = findElementInXLSheet(getParameterProcurement, "CurrencyTextFieldA");
		txt_ContactPersonNameTextFieldA = findElementInXLSheet(getParameterProcurement, "ContactPersonNameTextFieldA");
		txt_VendorReferenceNoTextFieldA = findElementInXLSheet(getParameterProcurement, "VendorReferenceNoTextFieldA");
		txt_BillingAddressTextFieldA = findElementInXLSheet(getParameterProcurement, "BillingAddressTextFieldA");
		txt_TitleTextFieldA = findElementInXLSheet(getParameterProcurement, "TitleTextFieldA");
		txt_OrderGroupTextFieldA = findElementInXLSheet(getParameterProcurement, "OrderGroupTextFieldA");
		txt_CustomerAccountTextFieldA = findElementInXLSheet(getParameterProcurement, "CustomerAccountTextFieldA");
		txt_WarehouseTextFieldA = findElementInXLSheet(getParameterProcurement, "WarehouseTextFieldA");
		txt_ShippingAddressTextFieldA = findElementInXLSheet(getParameterProcurement, "ShippingAddressTextFieldA");
		txt_ProductCodeTextFieldA = findElementInXLSheet(getParameterProcurement, "ProductCodeTextFieldA");
		txt_ProductCodeQtyTextFieldA = findElementInXLSheet(getParameterProcurement, "ProductCodeQtyTextFieldA");

		// PM_PI_007to010Reg
		btn_VendorSearchBtnA = findElementInXLSheet(getParameterProcurement, "VendorSearchBtnA");
		txt_VendorSearchTxtA = findElementInXLSheet(getParameterProcurement, "VendorSearchTxtA");
		sel_VendorSearchSelA = findElementInXLSheet(getParameterProcurement, "VendorSearchSelA");
		btn_NewVendorbtnA = findElementInXLSheet(getParameterProcurement, "NewVendorbtnA");
		txt_NewVendorTxtA = findElementInXLSheet(getParameterProcurement, "NewVendorTxtA");
		txt_NewVendorGroupTxtA = findElementInXLSheet(getParameterProcurement, "NewVendorGroupTxtA");
		btn_NewVendorUpdatebtnA = findElementInXLSheet(getParameterProcurement, "NewVendorUpdatebtnA");
		btn_VendorDetailBtnA = findElementInXLSheet(getParameterProcurement, "VendorDetailBtnA");
		txt_VendorDetailWidgetA = findElementInXLSheet(getParameterProcurement, "VendorDetailWidgetA");

		// PM_PI_011to014Reg
		btn_CurrencyDetailBtnA = findElementInXLSheet(getParameterProcurement, "CurrencyDetailBtnA");
		txt_CurrencyDetailWidgetA = findElementInXLSheet(getParameterProcurement, "CurrencyDetailWidgetA");

		// PM_PI_015to025Reg
		btn_BillingAddressSearchBtnA = findElementInXLSheet(getParameterProcurement, "BillingAddressSearchBtnA");
		txt_BillingAddressTxtA = findElementInXLSheet(getParameterProcurement, "BillingAddressTxtA");
		btn_ShippingAddressSearchBtnA = findElementInXLSheet(getParameterProcurement, "ShippingAddressSearchBtnA");
		txt_ShippingAddressTxtA = findElementInXLSheet(getParameterProcurement, "ShippingAddressTxtA");
		btn_NewOrderGroupPlusBtnA = findElementInXLSheet(getParameterProcurement, "NewOrderGroupPlusBtnA");
		btn_NewOrderGroupAddBtnA = findElementInXLSheet(getParameterProcurement, "NewOrderGroupAddBtnA");
		txt_NewOrderGroupAddTxtA = findElementInXLSheet(getParameterProcurement, "NewOrderGroupAddTxtA");
		btn_NewOrderGroupUpdateBtnA = findElementInXLSheet(getParameterProcurement, "NewOrderGroupUpdateBtnA");
		sel_OrderGroupSelA = findElementInXLSheet(getParameterProcurement, "OrderGroupSelA");
		btn_NewOrderGroupAddErrorBtnA = findElementInXLSheet(getParameterProcurement, "NewOrderGroupAddErrorBtnA");
		txt_NewOrderGroupAddErrorMsgA = findElementInXLSheet(getParameterProcurement, "NewOrderGroupAddErrorMsgA");
		btn_OrderGroupInactiveBtnA = findElementInXLSheet(getParameterProcurement, "OrderGroupInactiveBtnA");
		btn_CustomerAccountBtnA = findElementInXLSheet(getParameterProcurement, "CustomerAccountBtnA");
		txt_CustomerAccountTxtA = findElementInXLSheet(getParameterProcurement, "CustomerAccountTxtA");
		sel_CustomerAccountSelA = findElementInXLSheet(getParameterProcurement, "CustomerAccountSelA");
		btn_CustomerAccountDeleteBtnA = findElementInXLSheet(getParameterProcurement, "CustomerAccountDeleteBtnA");
		btn_NewCustomerAccountBtnA = findElementInXLSheet(getParameterProcurement, "NewCustomerAccountBtnA");
		txt_NewCustomerAccountNameA = findElementInXLSheet(getParameterProcurement, "NewCustomerAccountNameA");
		txt_NewCustomerAccountGroupA = findElementInXLSheet(getParameterProcurement, "NewCustomerAccountGroupA");
		sel_NewCustomerAccountSelA = findElementInXLSheet(getParameterProcurement, "NewCustomerAccountSelA");

		// PM_PI_026to031Reg
		txt_ProductTableNameA = findElementInXLSheet(getParameterProcurement, "ProductTableNameA");
		txt_ProductTableQtyA = findElementInXLSheet(getParameterProcurement, "ProductTableQtyA");
		txt_ProductTableUnitPriceA = findElementInXLSheet(getParameterProcurement, "ProductTableUnitPriceA");
		btn_ProductTableSearchBtnA = findElementInXLSheet(getParameterProcurement, "ProductTableSearchBtnA");
		txt_ProductTableSearchTxtA = findElementInXLSheet(getParameterProcurement, "ProductTableSearchTxtA");
		sel_ProductTableSearchSelA = findElementInXLSheet(getParameterProcurement, "ProductTableSearchSelA");
		sel_pro1A = findElementInXLSheet(getParameterProcurement, "pro1A");
		sel_pro2A = findElementInXLSheet(getParameterProcurement, "pro2A");
		sel_pro3A = findElementInXLSheet(getParameterProcurement, "pro3A");
		txt_tval1A = findElementInXLSheet(getParameterProcurement, "tval1A");
		txt_tval2A = findElementInXLSheet(getParameterProcurement, "tval2A");
		txt_tval3A = findElementInXLSheet(getParameterProcurement, "tval3A");
		btn_NewProductBtnA = findElementInXLSheet(getParameterProcurement, "NewProductBtnA");
		txt_NewProductCodeA = findElementInXLSheet(getParameterProcurement, "NewProductCodeA");
		txt_NewProductDescriptionA = findElementInXLSheet(getParameterProcurement, "NewProductDescriptionA");
		txt_NewProductGroupA = findElementInXLSheet(getParameterProcurement, "NewProductGroupA");
		btn_NewProductManufacturerSearchBtnA = findElementInXLSheet(getParameterProcurement,"NewProductManufacturerSearchBtnA");
		txt_NewProductManufacturerSearchTxtA = findElementInXLSheet(getParameterProcurement,"NewProductManufacturerSearchTxtA");
		sel_NewProductManufacturerSearchSelA = findElementInXLSheet(getParameterProcurement,"NewProductManufacturerSearchSelA");
		btn_ProductAddPlusBtnA = findElementInXLSheet(getParameterProcurement, "ProductAddPlusBtnA");

		// PM_PI_032to043Reg
		btn_ProductAddDeleteBtnA = findElementInXLSheet(getParameterProcurement, "ProductAddDeleteBtnA");
		btn_ProductWidgetPIA = findElementInXLSheet(getParameterProcurement, "ProductWidgetPIA");
		btn_ProductExDescriptionBtnA = findElementInXLSheet(getParameterProcurement, "ProductExDescriptionBtnA");
		txt_ProductExDescriptionTxtA = findElementInXLSheet(getParameterProcurement, "ProductExDescriptionTxtA");
		btn_ProductAdvanceBtnA = findElementInXLSheet(getParameterProcurement, "ProductAdvanceBtnA");
		txt_ProductAdvanceHeaderA = findElementInXLSheet(getParameterProcurement, "ProductAdvanceHeaderA");
		txt_ProductAdvanceUnitPriceA = findElementInXLSheet(getParameterProcurement, "ProductAdvanceUnitPriceA");
		txt_ProductAdvanceDiscountA = findElementInXLSheet(getParameterProcurement, "ProductAdvanceDiscountA");
		txt_ProductAdvanceTaxGroupA = findElementInXLSheet(getParameterProcurement, "ProductAdvanceTaxGroupA");
		txt_ProductAdvanceRemarksA = findElementInXLSheet(getParameterProcurement, "ProductAdvanceRemarksA");
		txt_ProductAdvanceOnsiteWarrantyA = findElementInXLSheet(getParameterProcurement,"ProductAdvanceOnsiteWarrantyA");
		txt_ProductAdvanceLabourWarrantyA = findElementInXLSheet(getParameterProcurement,"ProductAdvanceLabourWarrantyA");
		txt_ProductTableDiscountA = findElementInXLSheet(getParameterProcurement, "ProductTableDiscountA");
		txt_ProductTableTaxGroup1A = findElementInXLSheet(getParameterProcurement, "ProductTableTaxGroup1A");
		txt_ProductTableRemarksA = findElementInXLSheet(getParameterProcurement, "ProductTableRemarksA");
		txt_ProductTableDiscountPresentage1A = findElementInXLSheet(getParameterProcurement,"ProductTableDiscountPresentage1A");
		btn_ProductTableErrorBtnA = findElementInXLSheet(getParameterProcurement, "ProductTableErrorBtnA");
		txt_ProductTableErrorTxt1A = findElementInXLSheet(getParameterProcurement, "ProductTableErrorTxt1A");

		// PM_PI_044to058Reg
		txt_ProductTableErrorTxt2A = findElementInXLSheet(getParameterProcurement, "ProductTableErrorTxt2A");
		txt_TotalValueHeaderA = findElementInXLSheet(getParameterProcurement, "TotalValueHeaderA");
		btn_SelectAllBtnA = findElementInXLSheet(getParameterProcurement, "SelectAllBtnA");
		sel_TaxGroupOnlyCboxA = findElementInXLSheet(getParameterProcurement, "TaxGroupOnlyCboxA");
		txt_SelectAllTaxGroupA = findElementInXLSheet(getParameterProcurement, "SelectAllTaxGroupA");
		txt_ProductTableTaxGroup2A = findElementInXLSheet(getParameterProcurement, "ProductTableTaxGroup2A");
		sel_DiscountOnlyCboxA = findElementInXLSheet(getParameterProcurement, "DiscountOnlyCboxA");
		txt_SelectAllDiscountA = findElementInXLSheet(getParameterProcurement, "SelectAllDiscountA");
		txt_ProductTableDiscountPresentage2A = findElementInXLSheet(getParameterProcurement,"ProductTableDiscountPresentage2A");
		txt_DraftValidatorPIA = findElementInXLSheet(getParameterProcurement, "DraftValidatorPIA");
		txt_ProductTableLineTotalA = findElementInXLSheet(getParameterProcurement, "ProductTableLineTotalA");
		btn_TaskBreakDownBtnA = findElementInXLSheet(getParameterProcurement, "TaskBreakDownBtnA");
		txt_TaskBreakDownTaxTxtA = findElementInXLSheet(getParameterProcurement, "TaskBreakDownTaxTxtA");
		txt_BottomTableUnitTotalA = findElementInXLSheet(getParameterProcurement, "BottomTableUnitTotalA");
		txt_BottomTableDiscountTotalA = findElementInXLSheet(getParameterProcurement, "BottomTableDiscountTotalA");
		txt_BottomTableSubTotalA = findElementInXLSheet(getParameterProcurement, "BottomTableSubTotalA");
		txt_BottomTableTaxTotalA = findElementInXLSheet(getParameterProcurement, "BottomTableTaxTotalA");
		txt_BottomTableTotalA = findElementInXLSheet(getParameterProcurement, "BottomTableTotalA");
		txt_TotalUnitsHeaderA = findElementInXLSheet(getParameterProcurement, "TotalUnitsHeaderA");

		// PM_PI_059to070Reg
		txt_ShippingModeA = findElementInXLSheet(getParameterProcurement, "ShippingModeA");
		txt_DeliveryTermA = findElementInXLSheet(getParameterProcurement, "DeliveryTermA");
		txt_DestinationA = findElementInXLSheet(getParameterProcurement, "DestinationA");
		txt_TolleranceA = findElementInXLSheet(getParameterProcurement, "TolleranceA");
		txt_UnderDeliveryA = findElementInXLSheet(getParameterProcurement, "UnderDeliveryA");
		txt_PartialDeliveryA = findElementInXLSheet(getParameterProcurement, "PartialDeliveryA");
		txt_ReqSlipDateA = findElementInXLSheet(getParameterProcurement, "ReqSlipDateA");
		txt_ConfirmedSlipDateA = findElementInXLSheet(getParameterProcurement, "ConfirmedSlipDateA");
		txt_ReqReceiptDateA = findElementInXLSheet(getParameterProcurement, "ReqReceiptDateA");
		txt_ConfirmedReceiptDateA = findElementInXLSheet(getParameterProcurement, "ConfirmedReceiptDateA");
		txt_VendorAgreementA = findElementInXLSheet(getParameterProcurement, "VendorAgreementA");
		txt_InvoiceAccountA = findElementInXLSheet(getParameterProcurement, "InvoiceAccountA");
		txt_PaymentAgreementA = findElementInXLSheet(getParameterProcurement, "PaymentAgreementA");
		txt_PaymentTermA = findElementInXLSheet(getParameterProcurement, "PaymentTermA");
		btn_ShippingModeAddBtnA = findElementInXLSheet(getParameterProcurement, "ShippingModeAddBtnA");
		btn_ShippingModeAddPlusA = findElementInXLSheet(getParameterProcurement, "ShippingModeAddPlusA");
		txt_ShippingModeAddTxtA = findElementInXLSheet(getParameterProcurement, "ShippingModeAddTxtA");
		btn_ShippingModeAddErrorBtnA = findElementInXLSheet(getParameterProcurement, "ShippingModeAddErrorBtnA");
		txt_ShippingModeAddErrorTxtA = findElementInXLSheet(getParameterProcurement, "ShippingModeAddErrorTxtA");
		btn_ShippingModeActiveInactiveBtnA = findElementInXLSheet(getParameterProcurement, "ShippingModeActiveInactiveBtnA");
		btn_ShippingTermAddBtnA = findElementInXLSheet(getParameterProcurement, "ShippingTermAddBtnA");
		btn_ShippingTermAddPlusA = findElementInXLSheet(getParameterProcurement, "ShippingTermAddPlusA");
		txt_ShippingTermAddTxtA = findElementInXLSheet(getParameterProcurement, "ShippingTermAddTxtA");
		btn_ShippingTermAddErrorBtnA = findElementInXLSheet(getParameterProcurement, "ShippingTermAddErrorBtnA");
		txt_ShippingTermAddErrorTxtA = findElementInXLSheet(getParameterProcurement, "ShippingTermAddErrorTxtA");
		btn_ShippingTermActiveInactiveBtnA = findElementInXLSheet(getParameterProcurement, "ShippingTermActiveInactiveBtnA");

		// PM_PI_071to074Reg
		btn_PurchaseInvoiceByPageBtnA = findElementInXLSheet(getParameterProcurement, "PurchaseInvoiceByPageBtnA");
		txt_PurchaseInvoiceByPageTxtA = findElementInXLSheet(getParameterProcurement, "PurchaseInvoiceByPageTxtA");
		sel_PurchaseInvoiceByPageSelA = findElementInXLSheet(getParameterProcurement, "PurchaseInvoiceByPageSelA");
		btn_JournalPIBtnA = findElementInXLSheet(getParameterProcurement, "JournalPIBtnA");
		txt_JournalEntryDebitValueA = findElementInXLSheet(getParameterProcurement, "JournalEntryDebitValueA");
		txt_JournalEntryCreditValueA = findElementInXLSheet(getParameterProcurement, "JournalEntryCreditValueA");
		btn_ReversePIBtnA = findElementInXLSheet(getParameterProcurement, "ReversePIBtnA");
		txt_pageReverseA = findElementInXLSheet(getParameterProcurement, "pageReverseA");

		// PM_PI_075Reg
		btn_DeletePIBtnA = findElementInXLSheet(getParameterProcurement, "DeletePIBtnA");
		txt_pageDeleteA = findElementInXLSheet(getParameterProcurement, "pageDeleteA");

		// PM_PI_078Reg
		btn_DocFlowPIBtnA = findElementInXLSheet(getParameterProcurement, "DocFlowPIBtnA");

		// PM_PI_079to080Reg
		txt_ProductTableUnitPricePOtoPIA = findElementInXLSheet(getParameterProcurement,
				"ProductTableUnitPricePOtoPIA");
		btn_NewPOPlusBtnA = findElementInXLSheet(getParameterProcurement, "NewPOPlusBtnA");
		txt_NewPOUnitPriceTxtA = findElementInXLSheet(getParameterProcurement, "NewPOUnitPriceTxtA");

		// PM_PI_082to083Reg
		btn_ProductAdvancePOtoPIBtnA = findElementInXLSheet(getParameterProcurement, "ProductAdvancePOtoPIBtnA");
		txt_HSCodeTxtA = findElementInXLSheet(getParameterProcurement, "HSCodeTxtA");

		// PM_PI_084Reg
		txt_UnitPriceValidatorA = findElementInXLSheet(getParameterProcurement, "UnitPriceValidatorA");
		btn_UnitPriceValidatorActiveInactiveBtnA = findElementInXLSheet(getParameterProcurement, "UnitPriceValidatorActiveInactiveBtnA");
		btn_UpdateBtnA = findElementInXLSheet(getParameterProcurement, "UpdateBtnA");
		
		// PM_PI_085Reg
		btn_ActivitiesPIBtnA = findElementInXLSheet(getParameterProcurement, "ActivitiesPIBtnA");
		txt_ActivitiesSubjectPIA = findElementInXLSheet(getParameterProcurement, "ActivitiesSubjectPIA");
		txt_ActivitiesAssignToTxtPIA = findElementInXLSheet(getParameterProcurement, "ActivitiesAssignToTxtPIA");
		btn_ActivitiesUpdateBtnPIA = findElementInXLSheet(getParameterProcurement, "ActivitiesUpdateBtnPIA");
		btn_TaskAndEventBtnA = findElementInXLSheet(getParameterProcurement, "TaskAndEventBtnA");
		btn_MyTaskBtnA = findElementInXLSheet(getParameterProcurement, "MyTaskBtnA");
		btn_PurchaseInvoiceTileA = findElementInXLSheet(getParameterProcurement, "PurchaseInvoiceTileA");
		btn_PurchaseInvoiceActivityArrowA = findElementInXLSheet(getParameterProcurement,"PurchaseInvoiceActivityArrowA");
		txt_PurchaseInvoiceNoActivityTabA = findElementInXLSheet(getParameterProcurement,"PurchaseInvoiceNoActivityTabA");

		// PM_PI_086Reg
		txt_DeleteDocTxtPIA = findElementInXLSheet(getParameterProcurement, "DeleteDocTxtPIA");

		// PM_PI_087Reg
		btn_ApprovalsBtnA = findElementInXLSheet(getParameterProcurement, "ApprovalsBtnA");
		sel_ApprovalsDicisionA = findElementInXLSheet(getParameterProcurement, "ApprovalsDicisionA");
		sel_RejectReasonA = findElementInXLSheet(getParameterProcurement, "RejectReasonA");
		btn_OKBtnA = findElementInXLSheet(getParameterProcurement, "OKBtnA");

		// PM_PI_088Reg
		btn_AdministrationBtnA = findElementInXLSheet(getParameterProcurement, "AdministrationBtnA");
		btn_WorkflowConfigurationBtnA = findElementInXLSheet(getParameterProcurement, "WorkflowConfigurationBtnA");
		btn_PIDeleteBtnA = findElementInXLSheet(getParameterProcurement, "PIDeleteBtnA");
		txt_WorkflowStatusA = findElementInXLSheet(getParameterProcurement, "WorkflowStatusA");
		txt_DeleteApprovalPageA = findElementInXLSheet(getParameterProcurement, "DeleteApprovalPageA");

		// PM_PI_098Reg
		txt_ProductTableUnitPriceAfterValuePOtoPIA = findElementInXLSheet(getParameterProcurement,
				"ProductTableUnitPriceAfterValuePOtoPIA");

		// PM_PI_101Reg
		btn_btnPIToServiceA = findElementInXLSheet(getParameterProcurement, "btnPIToServiceA");

		// PM_PI_102to103Reg
		btn_UICustomizerBtnA = findElementInXLSheet(getParameterProcurement, "UICustomizerBtnA");
		sel_UICustomizerBalancingLevelA = findElementInXLSheet(getParameterProcurement, "UICustomizerBalancingLevelA");
		sel_UICustomizerPermissionLevelA = findElementInXLSheet(getParameterProcurement,"UICustomizerPermissionLevelA");
		btn_UICustomizerPurchaseInvoiceBtnA = findElementInXLSheet(getParameterProcurement,"UICustomizerPurchaseInvoiceBtnA");
		btn_UICustomizerNextBtnA = findElementInXLSheet(getParameterProcurement, "UICustomizerNextBtnA");
		sel_UICustomizerApplicableJourneyA = findElementInXLSheet(getParameterProcurement,"UICustomizerApplicableJourneyA");
		txt_UICustomizerBillingAddressTextFieldA = findElementInXLSheet(getParameterProcurement,"UICustomizerBillingAddressTextFieldA");

		// PM_PI_104Reg
		sel_TitleDisableBtnA = findElementInXLSheet(getParameterProcurement, "TitleDisableBtnA");

		// PM_PI_106to109Reg
		txt_ProductTableWarehouseA = findElementInXLSheet(getParameterProcurement, "ProductTableWarehouseA");
		btn_CostAllocationGridBtnA = findElementInXLSheet(getParameterProcurement, "CostAllocationGridBtnA");
		txt_CostCenterTxtA = findElementInXLSheet(getParameterProcurement, "CostCenterTxtA");
		txt_ApportionPercentageTxtA = findElementInXLSheet(getParameterProcurement, "ApportionPercentageTxtA");
		txt_CostAssignmentTypeTxtA = findElementInXLSheet(getParameterProcurement, "CostAssignmentTypeTxtA");
		btn_CostObjectSearchBtnA = findElementInXLSheet(getParameterProcurement, "CostObjectSearchBtnA");
		txt_CostObjectSearchTxtA = findElementInXLSheet(getParameterProcurement, "CostObjectSearchTxtA");
		sel_CostObjectSearchSelA = findElementInXLSheet(getParameterProcurement, "CostObjectSearchSelA");
		btn_ApportionAddRecordBtnA = findElementInXLSheet(getParameterProcurement, "ApportionAddRecordBtnA");
		btn_CostAllocationGridDeleteFirstA = findElementInXLSheet(getParameterProcurement,"CostAllocationGridDeleteFirstA");
		btn_CostAllocationUpdateBtnA = findElementInXLSheet(getParameterProcurement, "CostAllocationUpdateBtnA");
		btn_ChangeCostAllocationsBtnA = findElementInXLSheet(getParameterProcurement, "ChangeCostAllocationsBtnA");
		txt_CostObjectTxtA = findElementInXLSheet(getParameterProcurement, "CostObjectTxtA");

		// PM_PI_110Reg
		btn_SaveAsTemplateBtnA = findElementInXLSheet(getParameterProcurement, "SaveAsTemplateBtnA");
		txt_ApportionTemplateNameTxtA = findElementInXLSheet(getParameterProcurement, "ApportionTemplateNameTxtA");
		txt_TemplateCboxTxtA = findElementInXLSheet(getParameterProcurement, "TemplateCboxTxtA");
		
		// PM_PI_115Reg
		txt_InvoicePercentageA = findElementInXLSheet(getParameterProcurement, "InvoicePercentageA");
		txt_NoBalanceQtyToCompleteThisTransactionErrorA = findElementInXLSheet(getParameterProcurement, "NoBalanceQtyToCompleteThisTransactionErrorA");
		
		//PM_PI_116Reg
		btn_ProductDeleteServiceA = findElementInXLSheet(getParameterProcurement, "ProductDeleteServiceA");

		//PM_PI_117to119Reg
		txt_PlannedQtyPI1A = findElementInXLSheet(getParameterProcurement, "PlannedQtyPI1A");
		txt_PlannedQtyPI2A = findElementInXLSheet(getParameterProcurement, "PlannedQtyPI2A");
		txt_ProductTableName2A = findElementInXLSheet(getParameterProcurement, "ProductTableName2A");
		
		//PM_PI_123Reg
		txt_PurchaseOrderByPageSearchTxtA = findElementInXLSheet(getParameterProcurement, "PurchaseOrderByPageSearchTxtA");
		sel_PurchaseOrderByPageSearchSelA = findElementInXLSheet(getParameterProcurement, "PurchaseOrderByPageSearchSelA");
		txt_ChangeStatusA = findElementInXLSheet(getParameterProcurement, "ChangeStatusA");
		txt_pageCloseA = findElementInXLSheet(getParameterProcurement, "pageCloseA");
		
		//PM_PI_124Reg
		btn_ProductDeleteService2A = findElementInXLSheet(getParameterProcurement, "ProductDeleteService2A");

		//PM_PI_125to127Reg
		btn_StatusArrowBtnA = findElementInXLSheet(getParameterProcurement, "StatusArrowBtnA");
		txt_StatusInboundShipmentLevelA = findElementInXLSheet(getParameterProcurement, "StatusInboundShipmentLevelA");
		txt_StatusShipmentCostingLevelA = findElementInXLSheet(getParameterProcurement, "StatusShipmentCostingLevelA");
		txt_StatusOutboundPaymentLevelA = findElementInXLSheet(getParameterProcurement, "StatusOutboundPaymentLevelA");

		//PM_PI_128Reg
		txt_ProductAfterTableWarehouseA = findElementInXLSheet(getParameterProcurement, "ProductAfterTableWarehouseA");

		//PM_PI_129Reg
		txt_ProductTableWarehouse2A = findElementInXLSheet(getParameterProcurement, "ProductTableWarehouse2A");
		
		//PM_PI_131Reg
		txt_ChargeCodeA = findElementInXLSheet(getParameterProcurement, "ChargeCodeA");
		txt_ChargePresentageA = findElementInXLSheet(getParameterProcurement, "ChargePresentageA");

		//PM_PI_132Reg
		sel_UnderDeliveryCboxA = findElementInXLSheet(getParameterProcurement, "UnderDeliveryCboxA");
		sel_PartialDeliveryCboxA = findElementInXLSheet(getParameterProcurement, "PartialDeliveryCboxA");
		btn_POPIISSCJourneyBtnA = findElementInXLSheet(getParameterProcurement, "POPIISSCJourneyBtnA");

		//PM_PI_133Reg
		btn_POISPISCJourneyBtnA = findElementInXLSheet(getParameterProcurement, "POISPISCJourneyBtnA");

		//PM_PI_138Reg
		txt_ProductTableDiscountPOA = findElementInXLSheet(getParameterProcurement, "ProductTableDiscountPOA");
		txt_ProductTableTaxGroupPOA = findElementInXLSheet(getParameterProcurement, "ProductTableTaxGroupPOA");
		txt_OrderDiscountTxtA = findElementInXLSheet(getParameterProcurement, "OrderDiscountTxtA");
		btn_GridMasterBtnPIA = findElementInXLSheet(getParameterProcurement, "GridMasterBtnPIA");
		btn_ProductRelatedPriceBtnA = findElementInXLSheet(getParameterProcurement, "ProductRelatedPriceBtnA");
		txt_LastPurchasePriceTxtA = findElementInXLSheet(getParameterProcurement, "LastPurchasePriceTxtA");

		//PM_PI_139Reg
		txt_ProductTableQtyPOtoPIA = findElementInXLSheet(getParameterProcurement, "ProductTableQtyPOtoPIA");
		
		//PM_PO_001to010Reg
		txt_VendorTxtValidatorPOA = findElementInXLSheet(getParameterProcurement, "VendorTxtValidatorPOA");
		txt_CurrencyValidatorPOA = findElementInXLSheet(getParameterProcurement, "CurrencyValidatorPOA");
		txt_ProductGridValidatorPOBtnA = findElementInXLSheet(getParameterProcurement, "ProductGridValidatorPOBtnA");
		txt_ProductGridValidatorPOA = findElementInXLSheet(getParameterProcurement, "ProductGridValidatorPOA");
		txt_InvoiceAccountValidatorPOA = findElementInXLSheet(getParameterProcurement, "InvoiceAccountValidatorPOA");
		txt_VendorTextFieldPOA = findElementInXLSheet(getParameterProcurement, "VendorTextFieldPOA");
		txt_CurrencyTextFieldPOA = findElementInXLSheet(getParameterProcurement, "CurrencyTextFieldPOA");
		txt_ContactPersonNameTextFieldPOA = findElementInXLSheet(getParameterProcurement, "ContactPersonNameTextFieldPOA");
		txt_VendorReferenceNoTextFieldPOA = findElementInXLSheet(getParameterProcurement, "VendorReferenceNoTextFieldPOA");
		txt_BillingAddressTextFieldPOA = findElementInXLSheet(getParameterProcurement, "BillingAddressTextFieldPOA");
		txt_TitleTextFieldPOA = findElementInXLSheet(getParameterProcurement, "TitleTextFieldPOA");
		txt_OrderGroupTextFieldPOA = findElementInXLSheet(getParameterProcurement, "OrderGroupTextFieldPOA");
		txt_CustomerAccountTextFieldPOA = findElementInXLSheet(getParameterProcurement, "CustomerAccountTextFieldPOA");
		txt_WarehouseTextFieldPOA = findElementInXLSheet(getParameterProcurement, "WarehouseTextFieldPOA");
		txt_ShippingAddressTextFieldPOA = findElementInXLSheet(getParameterProcurement, "ShippingAddressTextFieldPOA");
		txt_ConsolidatedTextFieldPOA = findElementInXLSheet(getParameterProcurement, "ConsolidatedTextFieldPOA");
		btn_VendordetailsBtnPOA = findElementInXLSheet(getParameterProcurement, "VendordetailsBtnPOA");
		txt_VendorCreditDetailsBtn1POA = findElementInXLSheet(getParameterProcurement, "VendorCreditDetailsBtn1POA");
		txt_VendorCreditProfileBtn2POA = findElementInXLSheet(getParameterProcurement, "VendorCreditProfileBtn2POA");
		btn_UserPermissionBtnA = findElementInXLSheet(getParameterProcurement, "UserPermissionBtnA");
		btn_UserSearchBtnA = findElementInXLSheet(getParameterProcurement, "UserSearchBtnA");
		txt_UserSearchTxtA = findElementInXLSheet(getParameterProcurement, "UserSearchTxtA");
		sel_UserSearchSelA = findElementInXLSheet(getParameterProcurement, "UserSearchSelA");
		btn_WidgetsBtnA = findElementInXLSheet(getParameterProcurement, "WidgetsBtnA");
		txt_VendorCreditDetailsTxtA = findElementInXLSheet(getParameterProcurement, "VendorCreditDetailsTxtA");
		txt_VendorCreditProfileTxtA = findElementInXLSheet(getParameterProcurement, "VendorCreditProfileTxtA");
		sel_VendorCreditDetailsCboxA = findElementInXLSheet(getParameterProcurement, "VendorCreditDetailsCboxA");
		sel_VendorCreditProfileCboxA = findElementInXLSheet(getParameterProcurement, "VendorCreditProfileCboxA");

		//PM_PO_011to014Reg
		btn_CurrencyInformationBtnA = findElementInXLSheet(getParameterProcurement, "CurrencyInformationBtnA");
		txt_CurrencyInformationTxtA = findElementInXLSheet(getParameterProcurement, "CurrencyInformationTxtA");
		btn_CurrencydetailsBtnPOA = findElementInXLSheet(getParameterProcurement, "CurrencydetailsBtnPOA");
		txt_CurrentExRatePOA = findElementInXLSheet(getParameterProcurement, "CurrentExRatePOA");
		btn_FinanceBtnA = findElementInXLSheet(getParameterProcurement, "FinanceBtnA");
		btn_CurrencyExchangeRateBtnA = findElementInXLSheet(getParameterProcurement, "CurrencyExchangeRateBtnA");
		txt_CurrencyExchangeSellingRateTxtA = findElementInXLSheet(getParameterProcurement, "CurrencyExchangeSellingRateTxtA");

		//PM_PO_015to025Reg
		btn_NewOrderGroupPlusBtnPOA = findElementInXLSheet(getParameterProcurement, "NewOrderGroupPlusBtnPOA");
		btn_CustomerAccountPOBtnA = findElementInXLSheet(getParameterProcurement, "CustomerAccountPOBtnA");
		btn_CustomerAccountDeletePOBtnA = findElementInXLSheet(getParameterProcurement, "CustomerAccountDeletePOBtnA");
		
		//PM_PO_026to032Reg
		txt_ProductTableNamePOA = findElementInXLSheet(getParameterProcurement, "ProductTableNamePOA");
		txt_ProductTableQtyPOA = findElementInXLSheet(getParameterProcurement, "ProductTableQtyPOA");
		txt_ProductTableUnitPricePOA = findElementInXLSheet(getParameterProcurement, "ProductTableUnitPricePOA");
		txt_tval1POA = findElementInXLSheet(getParameterProcurement, "tval1POA");
		txt_tval2POA = findElementInXLSheet(getParameterProcurement, "tval2POA");
		txt_tval3POA = findElementInXLSheet(getParameterProcurement, "tval3POA");
		btn_ProductTableDeleteBtn1POA = findElementInXLSheet(getParameterProcurement, "ProductTableDeleteBtn1POA");
		btn_ProductTableDeleteBtn2POA = findElementInXLSheet(getParameterProcurement, "ProductTableDeleteBtn2POA");
		
		//PM_PO_033to049Reg
		btn_ProductWidgetPOA = findElementInXLSheet(getParameterProcurement, "ProductWidgetPOA");
		btn_ProductAlternativesWidgetPOA = findElementInXLSheet(getParameterProcurement, "ProductAlternativesWidgetPOA");
		btn_ProductRelatedPriceWidgetPOA = findElementInXLSheet(getParameterProcurement, "ProductRelatedPriceWidgetPOA");
		btn_ProductPurchasingHistoryWidgetPOA = findElementInXLSheet(getParameterProcurement, "ProductPurchasingHistoryWidgetPOA");
		btn_ProductAvailabilityWidgetPOA = findElementInXLSheet(getParameterProcurement, "ProductAvailabilityWidgetPOA");
		btn_RackWiseAvailableQtyWidgetPOA = findElementInXLSheet(getParameterProcurement, "RackWiseAvailableQtyWidgetPOA");
		btn_ProductBatchSerialWiseCostWidgetPOA = findElementInXLSheet(getParameterProcurement, "ProductBatchSerialWiseCostWidgetPOA");
		btn_ProductMovementHistoryWidgetPOA = findElementInXLSheet(getParameterProcurement, "ProductMovementHistoryWidgetPOA");
		btn_ProductSalesHistoryWidgetPOA = findElementInXLSheet(getParameterProcurement, "ProductSalesHistoryWidgetPOA");
		btn_ProductWiseGrossProfitWidgetPOA = findElementInXLSheet(getParameterProcurement, "ProductWiseGrossProfitWidgetPOA");
		btn_ViewProductImageWidgetPOA = findElementInXLSheet(getParameterProcurement, "ViewProductImageWidgetPOA");
		btn_ProductROLWidgetPOA = findElementInXLSheet(getParameterProcurement, "ProductROLWidgetPOA");
		btn_StockMovementHistoryWidgetPOA = findElementInXLSheet(getParameterProcurement, "StockMovementHistoryWidgetPOA");
		btn_ProductAvailabilityInWIPWidgetPOA = findElementInXLSheet(getParameterProcurement, "ProductAvailabilityInWIPWidgetPOA");
		btn_ProductAvailabilityOnPostDateWidgetPOA = findElementInXLSheet(getParameterProcurement, "ProductAvailabilityOnPostDateWidgetPOA");
		btn_ProductBatchSerialWiseWithoutCostWidgetPOA = findElementInXLSheet(getParameterProcurement, "ProductBatchSerialWiseWithoutCostWidgetPOA");
		btn_ProductExDescriptionBtnPOA = findElementInXLSheet(getParameterProcurement, "ProductExDescriptionBtnPOA");
		btn_ProductAdvanceBtnPOA = findElementInXLSheet(getParameterProcurement, "ProductAdvanceBtnPOA");
		txt_AdvanceProductNamePOA = findElementInXLSheet(getParameterProcurement, "AdvanceProductNamePOA");
		txt_AdvanceExDescriptionPOA = findElementInXLSheet(getParameterProcurement, "AdvanceExDescriptionPOA");
		txt_AdvanceUnitPricePOA = findElementInXLSheet(getParameterProcurement, "AdvanceUnitPricePOA");
		txt_AdvanceOnhandQtyPOA = findElementInXLSheet(getParameterProcurement, "AdvanceOnhandQtyPOA");
		txt_ProductTableRemarksPOA = findElementInXLSheet(getParameterProcurement, "ProductTableRemarksPOA");
		txt_ProductTableDiscountPresentagePOA = findElementInXLSheet(getParameterProcurement, "ProductTableDiscountPresentagePOA");
		btn_ProductTableErrorBtnPOA = findElementInXLSheet(getParameterProcurement, "ProductTableErrorBtnPOA");
		txt_ProductTableErrorTxt3A = findElementInXLSheet(getParameterProcurement, "ProductTableErrorTxt3A");
		
		//PM_PO_050to058Reg
		txt_ProductTableTaxGroup1POA = findElementInXLSheet(getParameterProcurement, "ProductTableTaxGroup1POA");
		txt_ProductTableTaxGroup2POA = findElementInXLSheet(getParameterProcurement, "ProductTableTaxGroup2POA");
		txt_ProductTableDiscountPresentage1POA = findElementInXLSheet(getParameterProcurement, "ProductTableDiscountPresentage1POA");
		txt_ProductTableDiscountPresentage2POA = findElementInXLSheet(getParameterProcurement, "ProductTableDiscountPresentage2POA");
		txt_DraftValidatorPOA = findElementInXLSheet(getParameterProcurement, "DraftValidatorPOA");
		txt_ProductTableLineTotalPOA = findElementInXLSheet(getParameterProcurement, "ProductTableLineTotalPOA");
		btn_TaskBreakDownBtnPOA = findElementInXLSheet(getParameterProcurement, "TaskBreakDownBtnPOA");
		
		//PM_PO_059to070Reg
		txt_ShippingModePOA = findElementInXLSheet(getParameterProcurement, "ShippingModePOA");
		txt_ShippingTermPOA = findElementInXLSheet(getParameterProcurement, "ShippingTermPOA");
		txt_DestinationPOA = findElementInXLSheet(getParameterProcurement, "DestinationPOA");
		txt_OverShipTolerancePOA = findElementInXLSheet(getParameterProcurement, "OverShipTolerancePOA");
		txt_UnderDeliveryPOA = findElementInXLSheet(getParameterProcurement, "UnderDeliveryPOA");
		txt_PartialDeliveryPOA = findElementInXLSheet(getParameterProcurement, "PartialDeliveryPOA");
		txt_RequestedShipDatePOA = findElementInXLSheet(getParameterProcurement, "RequestedShipDatePOA");
		txt_ConfirmedShipDatePOA = findElementInXLSheet(getParameterProcurement, "ConfirmedShipDatePOA");
		txt_RequestedReceiptDatePOA = findElementInXLSheet(getParameterProcurement, "RequestedReceiptDatePOA");
		txt_ConfirmedReceiptDatePOA = findElementInXLSheet(getParameterProcurement, "ConfirmedReceiptDatePOA");
		txt_VendorAgreementPOA = findElementInXLSheet(getParameterProcurement, "VendorAgreementPOA");
		txt_InvoiceAccountPOA = findElementInXLSheet(getParameterProcurement, "InvoiceAccountPOA");
		txt_PaymentAgreementPOA = findElementInXLSheet(getParameterProcurement, "PaymentAgreementPOA");
		txt_PaymentTermPOA = findElementInXLSheet(getParameterProcurement, "PaymentTermPOA");
		txt_ChargeAmountTxtPOA = findElementInXLSheet(getParameterProcurement, "ChargeAmountTxtPOA");
		txt_PricingAndChargesDeleteBtnA = findElementInXLSheet(getParameterProcurement, "PricingAndChargesDeleteBtnA");
		txt_PricingAndChargesOrderDiscountTxtA = findElementInXLSheet(getParameterProcurement, "PricingAndChargesOrderDiscountTxtA");
		
		//PM_PO_071to073Reg
		btn_PurchaseOrderByPageBtnA = findElementInXLSheet(getParameterProcurement, "PurchaseOrderByPageBtnA");
		txt_PurchaseOrderByPageTxtA = findElementInXLSheet(getParameterProcurement, "PurchaseOrderByPageTxtA");
		sel_PurchaseOrderByPageSelA = findElementInXLSheet(getParameterProcurement, "PurchaseOrderByPageSelA");
		btn_YesBtnPOA = findElementInXLSheet(getParameterProcurement, "YesBtnPOA");
		
		//PM_PO_083Reg
		txt_TotalPriceValidatorA = findElementInXLSheet(getParameterProcurement, "TotalPriceValidatorA");
		
		//PM_PO_084Reg
		btn_ActivitiesPOBtnA = findElementInXLSheet(getParameterProcurement, "ActivitiesPOBtnA");
		txt_ActivitiesSubjectPOA = findElementInXLSheet(getParameterProcurement, "ActivitiesSubjectPOA");
		txt_ActivitiesAssignToTxtPOA = findElementInXLSheet(getParameterProcurement, "ActivitiesAssignToTxtPOA");
		btn_ActivitiesUpdateBtnPOA = findElementInXLSheet(getParameterProcurement, "ActivitiesUpdateBtnPOA");
		btn_PurchaseOrderTileA = findElementInXLSheet(getParameterProcurement, "PurchaseOrderTileA");
		btn_PurchaseOrderActivityArrowA = findElementInXLSheet(getParameterProcurement, "PurchaseOrderActivityArrowA");
		txt_PurchaseOrderNoActivityTabA = findElementInXLSheet(getParameterProcurement, "PurchaseOrderNoActivityTabA");

		//PM_PO_085Reg
		txt_DeleteDocTxtPOA = findElementInXLSheet(getParameterProcurement, "DeleteDocTxtPOA");

		//PM_PO_086Reg
		btn_PODeleteBtnA = findElementInXLSheet(getParameterProcurement, "PODeleteBtnA");
		
		//PM_PR_001to005Reg
		txt_TitlePRA = findElementInXLSheet(getParameterProcurement, "TitlePRA");
		btn_ProductSearchGridBtnPRA = findElementInXLSheet(getParameterProcurement, "ProductSearchGridBtnPRA");
		txt_ProductSearchGridTxtPRA = findElementInXLSheet(getParameterProcurement, "ProductSearchGridTxtPRA");
		sel_ProductSearchGridSelPRA = findElementInXLSheet(getParameterProcurement, "ProductSearchGridSelPRA");
		btn_PurchaseRequisitionByPageBtnA = findElementInXLSheet(getParameterProcurement, "PurchaseRequisitionByPageBtnA");
		txt_PurchaseRequisitionByPageTxtA = findElementInXLSheet(getParameterProcurement, "PurchaseRequisitionByPageTxtA");
		sel_PurchaseRequisitionByPageSelA = findElementInXLSheet(getParameterProcurement, "PurchaseRequisitionByPageSelA");
		btn_GridMasterInfoBtnPRA = findElementInXLSheet(getParameterProcurement, "GridMasterInfoBtnPRA");
		txt_ProductTableUnitPricePRA = findElementInXLSheet(getParameterProcurement, "ProductTableUnitPricePRA");
		
		//PM_PR_006to008Reg
		txt_PreparerPRA = findElementInXLSheet(getParameterProcurement, "PreparerPRA");
		txt_PreparerAfterReleasedPRA = findElementInXLSheet(getParameterProcurement, "PreparerAfterReleasedPRA");
		
		//PM_PR_009to010Reg
		txt_TitleAfterDraftPRA = findElementInXLSheet(getParameterProcurement, "TitleAfterDraftPRA");
		
		//PM_PR_011to013Reg
		txt_RequestGroupPRA = findElementInXLSheet(getParameterProcurement, "RequestGroupPRA");
		txt_ProductGroupPRA = findElementInXLSheet(getParameterProcurement, "ProductGroupPRA");
		txt_ProductTableQtyPRA = findElementInXLSheet(getParameterProcurement, "ProductTableQtyPRA");
		btn_UpdateProcurementDetailsBtnPRA = findElementInXLSheet(getParameterProcurement, "UpdateProcurementDetailsBtnPRA");
		txt_UpdateProcurementDetailsInvoiceNumberPRA = findElementInXLSheet(getParameterProcurement, "UpdateProcurementDetailsInvoiceNumberPRA");
		btn_ProcurementInformationTabBtnA = findElementInXLSheet(getParameterProcurement, "ProcurementInformationTabBtnA");
		txt_ProcurementInformationTabInvoiceNumberPRA = findElementInXLSheet(getParameterProcurement, "ProcurementInformationTabInvoiceNumberPRA");

		//PM_PR_016Reg
		txt_DraftValidatorPRA = findElementInXLSheet(getParameterProcurement, "DraftValidatorPRA");

		//PM_PR_018Reg
		txt_ProductTableExpectedDatePRA = findElementInXLSheet(getParameterProcurement, "ProductTableExpectedDatePRA");
		sel_ProductTableExpectedDateSelPRA = findElementInXLSheet(getParameterProcurement, "ProductTableExpectedDateSelPRA");
		btn_ProductTableAdvanceBtnPRA= findElementInXLSheet(getParameterProcurement, "ProductTableAdvanceBtnPRA");
		txt_ExpectedDateAdvanceTablePRA= findElementInXLSheet(getParameterProcurement, "ExpectedDateAdvanceTablePRA");
		
		//PM_PR_019Reg
		txt_ProductTableWarehousePRA= findElementInXLSheet(getParameterProcurement, "ProductTableWarehousePRA");

		//PM_PR_021to022Reg
		btn_InventoryAndWarehousingBtnPRA= findElementInXLSheet(getParameterProcurement, "InventoryAndWarehousingBtnPRA");
		btn_InternalOrderBtnPRA= findElementInXLSheet(getParameterProcurement, "InternalOrderBtnPRA");
		btn_NewInternalOrderBtnPRA= findElementInXLSheet(getParameterProcurement, "NewInternalOrderBtnPRA");
		btn_EmployeeRequestsJourneyBtnPRA= findElementInXLSheet(getParameterProcurement, "EmployeeRequestsJourneyBtnPRA");
		txt_InternalOrderTitlePRA= findElementInXLSheet(getParameterProcurement, "InternalOrderTitlePRA");
		btn_InternalOrderRequesterBtnPRA= findElementInXLSheet(getParameterProcurement, "InternalOrderRequesterBtnPRA");
		txt_InternalOrderRequesterTxtPRA= findElementInXLSheet(getParameterProcurement, "InternalOrderRequesterTxtPRA");
		sel_InternalOrderRequesterSelPRA= findElementInXLSheet(getParameterProcurement, "InternalOrderRequesterSelPRA");
		btn_InternalOrderGeneratePurchaseRequisitionBtnPRA= findElementInXLSheet(getParameterProcurement, "InternalOrderGeneratePurchaseRequisitionBtnPRA");
		txt_UnitPricePRA= findElementInXLSheet(getParameterProcurement, "UnitPricePRA");
		
		//PM_PR_024Reg
		txt_CopyFromSearchTxtPRA= findElementInXLSheet(getParameterProcurement, "CopyFromSearchTxtPRA");
		sel_CopyFromSearchSelPRA= findElementInXLSheet(getParameterProcurement, "CopyFromSearchSelPRA");
		txt_ProductTableNamePRA= findElementInXLSheet(getParameterProcurement, "ProductTableNamePRA");

		//PM_PR_025Reg
		btn_ProductTableProductCatalogPRA= findElementInXLSheet(getParameterProcurement, "ProductTableProductCatalogPRA");
		txt_ProductHierarchyPopupHeaderPRA= findElementInXLSheet(getParameterProcurement, "ProductHierarchyPopupHeaderPRA");
		
		//PM_PR_026to028Reg
		txt_ConfirmationMessageValidationA= findElementInXLSheet(getParameterProcurement, "ConfirmationMessageValidationA");
		btn_ConfirmationMessageValidationCloseA= findElementInXLSheet(getParameterProcurement, "ConfirmationMessageValidationCloseA");
		
		//PM_PR_029Reg
		txt_InternalOrderProductaNamePRA= findElementInXLSheet(getParameterProcurement, "InternalOrderProductaNamePRA");
		
		//PM_PR_030to031Reg
		btn_RequesterSearchBtnPRA= findElementInXLSheet(getParameterProcurement, "RequesterSearchBtnPRA");
		txt_RequesterSearchTxtPRA= findElementInXLSheet(getParameterProcurement, "RequesterSearchTxtPRA");
		sel_RequesterSearchSelPRA= findElementInXLSheet(getParameterProcurement, "RequesterSearchSelPRA");
		btn_CurrencyDetailsWidgetBtnPRA= findElementInXLSheet(getParameterProcurement, "CurrencyDetailsWidgetBtnPRA");
		txt_CurrencyCurrentExRatePRA= findElementInXLSheet(getParameterProcurement, "CurrencyCurrentExRatePRA");
		txt_CurrencyLastUpdatedPRA= findElementInXLSheet(getParameterProcurement, "CurrencyLastUpdatedPRA");
		txt_CurrencyExchangeLastUpdateDateTxtA= findElementInXLSheet(getParameterProcurement, "CurrencyExchangeLastUpdateDateTxtA");

		//PM_PR_032Reg
		icn_DraftDocIconPRA= findElementInXLSheet(getParameterProcurement, "DraftDocIconPRA");
		icn_ReleaseDocIconPRA= findElementInXLSheet(getParameterProcurement, "ReleaseDocIconPRA");
		icn_ReverseDocIconPRA= findElementInXLSheet(getParameterProcurement, "ReverseDocIconPRA");
		icn_DeleteDocIconPRA= findElementInXLSheet(getParameterProcurement, "DeleteDocIconPRA");

		//PM_PR_033to034Reg
		txt_RequestDatePRA= findElementInXLSheet(getParameterProcurement, "RequestDatePRA");
		txt_RequisitionNoByPagePRA= findElementInXLSheet(getParameterProcurement, "RequisitionNoByPagePRA");
		txt_TitleByPagePRA= findElementInXLSheet(getParameterProcurement, "TitleByPagePRA");
		txt_PreparerByPagePRA= findElementInXLSheet(getParameterProcurement, "PreparerByPagePRA");
		txt_RequestDateByPagePRA= findElementInXLSheet(getParameterProcurement, "RequestDateByPagePRA");
		
		//PM_PR_035to036Reg
		btn_ProductAdvanceBtnPRA= findElementInXLSheet(getParameterProcurement, "ProductAdvanceBtnPRA");
		btn_ProductAdvanceProcurementInformationTabPRA= findElementInXLSheet(getParameterProcurement, "ProductAdvanceProcurementInformationTabPRA");
		txt_ProductAdvanceSupplierTxtPRA= findElementInXLSheet(getParameterProcurement, "ProductAdvanceSupplierTxtPRA");
		btn_ConvertToPurchaseOrderA= findElementInXLSheet(getParameterProcurement, "ConvertToPurchaseOrderA");
		btn_ConvertToRequestForQuotationA= findElementInXLSheet(getParameterProcurement, "ConvertToRequestForQuotationA");
		btn_UpdateProcurementDetailsA= findElementInXLSheet(getParameterProcurement, "UpdateProcurementDetailsA");

		//PM_PR_039to040Reg
		txt_ProductListHeaderPRA= findElementInXLSheet(getParameterProcurement, "ProductListHeaderPRA");
		btn_GenaratePOBtnPRA= findElementInXLSheet(getParameterProcurement, "GenaratePOBtnPRA");
		txt_StartNewProcessHeaderPRA= findElementInXLSheet(getParameterProcurement, "StartNewProcessHeaderPRA");
		
		//PM_PR_043Reg
		btn_VendorLookupBtnRFQA= findElementInXLSheet(getParameterProcurement, "VendorLookupBtnRFQA");
		txt_VendorLookupTxtRFQA= findElementInXLSheet(getParameterProcurement, "VendorLookupTxtRFQA");
		sel_VendorLookupSelRFQA= findElementInXLSheet(getParameterProcurement, "VendorLookupSelRFQA");
		txt_TableProductNameRFQA= findElementInXLSheet(getParameterProcurement, "TableProductNameRFQA");
		txt_pageReleasedForQuotationRFQA= findElementInXLSheet(getParameterProcurement, "pageReleasedForQuotationRFQA");

		//PM_PR_047Reg
		btn_ProductTableAddPlusBtnPRA= findElementInXLSheet(getParameterProcurement, "ProductTableAddPlusBtnPRA");
		btn_ProductSearchGridBtn2PRA= findElementInXLSheet(getParameterProcurement, "ProductSearchGridBtn2PRA");
		txt_ProductTableName2POA= findElementInXLSheet(getParameterProcurement, "ProductTableName2POA");
		txt_ProductTableName1ISA= findElementInXLSheet(getParameterProcurement, "ProductTableName1ISA");
		txt_ProductTableName2ISA= findElementInXLSheet(getParameterProcurement, "ProductTableName2ISA");

		//PM_PR_048Reg
		txt_ReversedValidatorPRA= findElementInXLSheet(getParameterProcurement, "ReversedValidatorPRA");

		//PM_PR_054Reg
		txt_HistortTabDraftHistoryTxtA= findElementInXLSheet(getParameterProcurement, "HistortTabDraftHistoryTxtA");
		txt_HistortTabUpdateHistoryTxtA= findElementInXLSheet(getParameterProcurement, "HistortTabUpdateHistoryTxtA");
		txt_HistortTabReleaseHistoryTxtA= findElementInXLSheet(getParameterProcurement, "HistortTabReleaseHistoryTxtA");
		txt_HistortTabReversedHistoryTxtA= findElementInXLSheet(getParameterProcurement, "HistortTabReversedHistoryTxtA");

		//PM_PR_055to057Reg
		txt_PageHoldTxtA= findElementInXLSheet(getParameterProcurement, "PageHoldTxtA");
		txt_ProductTableQtyAfterReleasedPRA= findElementInXLSheet(getParameterProcurement, "ProductTableQtyAfterReleasedPRA");

		//PM_PR_058to066Reg
		btn_PRDraftBtnA= findElementInXLSheet(getParameterProcurement, "PRDraftBtnA");
		btn_SendReviewBtnA= findElementInXLSheet(getParameterProcurement, "SendReviewBtnA");
		txt_SendReviewDraftPageA= findElementInXLSheet(getParameterProcurement, "SendReviewDraftPageA");
		icn_DraftReviewingDocIconPRA= findElementInXLSheet(getParameterProcurement, "DraftReviewingDocIconPRA");
		txt_NextReviewUserA= findElementInXLSheet(getParameterProcurement, "NextReviewUserA");
		btn_PurchaseRequisitionTileA= findElementInXLSheet(getParameterProcurement, "PurchaseRequisitionTileA");
		txt_PurchaseRequisitionSearchTxtA= findElementInXLSheet(getParameterProcurement, "PurchaseRequisitionSearchTxtA");
		btn_PurchaseRequisitionActivityArrowA= findElementInXLSheet(getParameterProcurement, "PurchaseRequisitionActivityArrowA");
		txt_ApprovedDraftPageA= findElementInXLSheet(getParameterProcurement, "ApprovedDraftPageA");
		icn_DraftApprovedDocIconPRA= findElementInXLSheet(getParameterProcurement, "DraftApprovedDocIconPRA");
		btn_NotificationBtnA= findElementInXLSheet(getParameterProcurement, "NotificationBtnA");
		txt_PRApprovedNotificationA= findElementInXLSheet(getParameterProcurement, "PRApprovedNotificationA");
		txt_ActivitiesStatusA= findElementInXLSheet(getParameterProcurement, "ActivitiesStatusA");
		btn_UpdateTaskA= findElementInXLSheet(getParameterProcurement, "UpdateTaskA");
		txt_ApprovalRequestLookUpHeaderA= findElementInXLSheet(getParameterProcurement, "ApprovalRequestLookUpHeaderA");
		
		//PM_PR_067to070Reg
		txt_RejectedDraftPageA= findElementInXLSheet(getParameterProcurement, "RejectedDraftPageA");
		txt_DraftRejectedDocIconPRA= findElementInXLSheet(getParameterProcurement, "DraftRejectedDocIconPRA");
		
		//PM_PR_071Reg
		txt_PurchaseRequisitionActivityDocNoA= findElementInXLSheet(getParameterProcurement, "PurchaseRequisitionActivityDocNoA");
		btn_ApproveBtnA= findElementInXLSheet(getParameterProcurement, "ApproveBtnA");
		sel_ApprovalsDicisionFormFieldA= findElementInXLSheet(getParameterProcurement, "ApprovalsDicisionFormFieldA");
		btn_OKFormBtnA= findElementInXLSheet(getParameterProcurement, "OKFormBtnA");
		
		//PM_PR_072Reg
		sel_RejectReasonFormFieldA= findElementInXLSheet(getParameterProcurement, "RejectReasonFormFieldA");

		//PM_PR_073to074Reg
		btn_PRDraftNewBtnA= findElementInXLSheet(getParameterProcurement, "PRDraftNewBtnA");
		txt_PostBusinessUnitA= findElementInXLSheet(getParameterProcurement, "PostBusinessUnitA");
		sel_PostBusinessUnitSelA= findElementInXLSheet(getParameterProcurement, "PostBusinessUnitSelA");
		btn_LogOutBtnA= findElementInXLSheet(getParameterProcurement, "LogOutBtnA");
		btn_SignOutBtnA= findElementInXLSheet(getParameterProcurement, "SignOutBtnA");
		
		//PM_PR_078to079Reg
		btn_PRDraftLevelWiseBtnA= findElementInXLSheet(getParameterProcurement, "PRDraftLevelWiseBtnA");
		txt_ApprovalRequestLookUpHeaderLevel1A= findElementInXLSheet(getParameterProcurement, "ApprovalRequestLookUpHeaderLevel1A");
		txt_ApprovalRequestLookUpHeaderLevel2A= findElementInXLSheet(getParameterProcurement, "ApprovalRequestLookUpHeaderLevel2A");


		//PM_PR_083to084Reg
		txt_HistortTabWFDraftHistoryTxtA= findElementInXLSheet(getParameterProcurement, "HistortTabWFDraftHistoryTxtA");
		txt_HistortTabWFReviewHistoryTxtA= findElementInXLSheet(getParameterProcurement, "HistortTabWFReviewHistoryTxtA");
		txt_HistortTabWFDraft1HistoryTxtA= findElementInXLSheet(getParameterProcurement, "HistortTabWFDraft1HistoryTxtA");
		txt_HistortTabWFDraft2HistoryTxtA= findElementInXLSheet(getParameterProcurement, "HistortTabWFDraft2HistoryTxtA");
		txt_HistortTabWFReleaseHistoryTxtA= findElementInXLSheet(getParameterProcurement, "HistortTabWFReleaseHistoryTxtA");

		//PM_PR_085to093Reg
		btn_PRReleaseBtnA= findElementInXLSheet(getParameterProcurement, "PRReleaseBtnA");
		txt_PendingApprovalPageA= findElementInXLSheet(getParameterProcurement, "PendingApprovalPageA");
		icn_PendingApprovalDocIconPRA= findElementInXLSheet(getParameterProcurement, "PendingApprovalDocIconPRA");
		txt_ApprovalRequestLookUpHeaderReleaseA= findElementInXLSheet(getParameterProcurement, "ApprovalRequestLookUpHeaderReleaseA");
		txt_PRReleasedApprovedNotificationA= findElementInXLSheet(getParameterProcurement, "PRReleasedApprovedNotificationA");
		
		//PM_PR_094to097Reg
		txt_RejectedPageA= findElementInXLSheet(getParameterProcurement, "RejectedPageA");
		icn_ReleasedRejectedDocIconPRA= findElementInXLSheet(getParameterProcurement, "ReleasedRejectedDocIconPRA");
		
		//PM_PR_100to101Reg
		btn_PRReleaseNewBtnA= findElementInXLSheet(getParameterProcurement, "PRReleaseNewBtnA");

		//PM_PR_105to106Reg
		btn_PRReleaseLevelWiseBtnA= findElementInXLSheet(getParameterProcurement, "PRReleaseLevelWiseBtnA");
		txt_ApprovalRequestLookUpHeaderReleaseLevel1A= findElementInXLSheet(getParameterProcurement, "ApprovalRequestLookUpHeaderReleaseLevel1A");
		txt_ApprovalRequestLookUpHeaderReleaseLevel2A= findElementInXLSheet(getParameterProcurement, "ApprovalRequestLookUpHeaderReleaseLevel2A");

		//PM_PR_110Reg
		txt_HistortTabWFApprovedHistoryTxtA= findElementInXLSheet(getParameterProcurement, "HistortTabWFApprovedHistoryTxtA");
		txt_HistortTabWFApproval1HistoryTxtA= findElementInXLSheet(getParameterProcurement, "HistortTabWFApproval1HistoryTxtA");
		txt_HistortTabWFApproval2HistoryTxtA= findElementInXLSheet(getParameterProcurement, "HistortTabWFApproval2HistoryTxtA");

		//PM_PR_111to112Reg
		btn_PRDraftOnlyOneBtnA= findElementInXLSheet(getParameterProcurement, "PRDraftOnlyOneBtnA");

		//PM_PR_114to115Reg
		btn_PRReleaseOnlyOneBtnA= findElementInXLSheet(getParameterProcurement, "PRReleaseOnlyOneBtnA");

		//PM_PR_117to125Reg
		btn_PRDeleteBtnA= findElementInXLSheet(getParameterProcurement, "PRDeleteBtnA");
		txt_ApprovalRequestLookUpDeleteHeaderA= findElementInXLSheet(getParameterProcurement, "ApprovalRequestLookUpDeleteHeaderA");
		icn_DeleteApprovalDocIconPRA= findElementInXLSheet(getParameterProcurement, "DeleteApprovalDocIconPRA");
		txt_PRDeleteApprovedNotificationA= findElementInXLSheet(getParameterProcurement, "PRDeleteApprovedNotificationA");
		
		//PM_PR_132to133Reg
		btn_PRDeleteNewBtnA= findElementInXLSheet(getParameterProcurement, "PRDeleteNewBtnA");
		
		//PM_PR_137to138Reg
		btn_PRDeleteLevelWiseBtnA= findElementInXLSheet(getParameterProcurement, "PRDeleteLevelWiseBtnA");
		txt_ApprovalRequestLookUpHeaderDeleteLevel1A= findElementInXLSheet(getParameterProcurement, "ApprovalRequestLookUpHeaderDeleteLevel1A");
		txt_ApprovalRequestLookUpHeaderDeleteLevel2A= findElementInXLSheet(getParameterProcurement, "ApprovalRequestLookUpHeaderDeleteLevel2A");

		//PM_PR_142Reg
		txt_HistortTabWFDeleteAprHistoryTxtA= findElementInXLSheet(getParameterProcurement, "HistortTabWFDeleteAprHistoryTxtA");
		txt_HistortTabWFDelete1HistoryTxtA= findElementInXLSheet(getParameterProcurement, "HistortTabWFDelete1HistoryTxtA");
		txt_HistortTabWFDelete2HistoryTxtA= findElementInXLSheet(getParameterProcurement, "HistortTabWFDelete2HistoryTxtA");
		txt_HistortTabWFDeleteHistoryTxtA= findElementInXLSheet(getParameterProcurement, "HistortTabWFDeleteHistoryTxtA");

		//PM_PR_143to144Reg
		btn_PRDeleteOnlyOneBtnA= findElementInXLSheet(getParameterProcurement, "PRDeleteOnlyOneBtnA");
		
		//PM_PR_146to154Reg
		btn_PRReverseBtnA= findElementInXLSheet(getParameterProcurement, "PRReverseBtnA");
		txt_ReverseApprovalPageA= findElementInXLSheet(getParameterProcurement, "ReverseApprovalPageA");
		icn_ReverseApprovalDocIconPRA= findElementInXLSheet(getParameterProcurement, "ReverseApprovalDocIconPRA");
		txt_ApprovalRequestLookUpReverseHeaderA= findElementInXLSheet(getParameterProcurement, "ApprovalRequestLookUpReverseHeaderA");
		txt_PRReverseApprovedNotificationA= findElementInXLSheet(getParameterProcurement, "PRReverseApprovedNotificationA");

		//PM_PR_161to162Reg
		btn_PRReverseNewBtnA= findElementInXLSheet(getParameterProcurement, "PRReverseNewBtnA");
		
		//PM_PR_166to167Reg
		btn_PRReverseLevelWiseBtnA= findElementInXLSheet(getParameterProcurement, "PRReverseLevelWiseBtnA");
		txt_ApprovalRequestLookUpHeaderReverseLevel1A= findElementInXLSheet(getParameterProcurement, "ApprovalRequestLookUpHeaderReverseLevel1A");
		txt_ApprovalRequestLookUpHeaderReverseLevel2A= findElementInXLSheet(getParameterProcurement, "ApprovalRequestLookUpHeaderReverseLevel2A");

		//PM_PR_171Reg
		txt_HistortTabWFDraftedReverseHistoryTxtA= findElementInXLSheet(getParameterProcurement, "HistortTabWFDraftedReverseHistoryTxtA");
		txt_HistortTabWFReleasedReverseHistoryTxtA= findElementInXLSheet(getParameterProcurement, "HistortTabWFReleasedReverseHistoryTxtA");
		txt_HistortTabWFSentReverseHistoryTxtA= findElementInXLSheet(getParameterProcurement, "HistortTabWFSentReverseHistoryTxtA");
		txt_HistortTabWFApproveReverseHistory1TxtA= findElementInXLSheet(getParameterProcurement, "HistortTabWFApproveReverseHistory1TxtA");
		txt_HistortTabWFApproveReverseHistory2TxtA= findElementInXLSheet(getParameterProcurement, "HistortTabWFApproveReverseHistory2TxtA");
		txt_HistortTabWFReversedHistoryTxtA= findElementInXLSheet(getParameterProcurement, "HistortTabWFReversedHistoryTxtA");
		
		//PM_PR_172to173Reg
		btn_PRReverseOnlyOneBtnA= findElementInXLSheet(getParameterProcurement, "PRReverseOnlyOneBtnA");

		//PM_PR_175to176Reg
		btn_JourneyConfigurationBtnA= findElementInXLSheet(getParameterProcurement, "JourneyConfigurationBtnA");
		btn_JourneyConfigurationPurchaseRequisitionBtnA= findElementInXLSheet(getParameterProcurement, "JourneyConfigurationPurchaseRequisitionBtnA");
		btn_JourneyConfigurationProcessStatusBtnA= findElementInXLSheet(getParameterProcurement, "JourneyConfigurationProcessStatusBtnA");
		txt_PurchaseRequisitionJourneyValidatorA= findElementInXLSheet(getParameterProcurement, "PurchaseRequisitionJourneyValidatorA");

		//PM_PR_177Reg
		btn_BalancingLevelSettingsBtnA= findElementInXLSheet(getParameterProcurement, "BalancingLevelSettingsBtnA");
		btn_BalancingLevelInventorySegBtnA= findElementInXLSheet(getParameterProcurement, "BalancingLevelInventorySegBtnA");
		txt_BalancingLevelInventoryProductCodeNameTxtA= findElementInXLSheet(getParameterProcurement, "BalancingLevelInventoryProductCodeNameTxtA");
		btn_BalancingLevelInventorySegUpdateBtnA= findElementInXLSheet(getParameterProcurement, "BalancingLevelInventorySegUpdateBtnA");
		txt_PRProductFieldA= findElementInXLSheet(getParameterProcurement, "PRProductFieldA");
		
		//PM_PR_180Reg
		btn_BalancingLevelGenaralSegBtnA= findElementInXLSheet(getParameterProcurement, "BalancingLevelGenaralSegBtnA");
		txt_BalancingLevelGenaralWarehouseNameTxtA= findElementInXLSheet(getParameterProcurement, "BalancingLevelGenaralWarehouseNameTxtA");
		btn_BalancingLevelGenaralSegUpdateBtnA= findElementInXLSheet(getParameterProcurement, "BalancingLevelGenaralSegUpdateBtnA");
		txt_PRWarehouseField1A= findElementInXLSheet(getParameterProcurement, "PRWarehouseField1A");
		txt_PRWarehouseField2A= findElementInXLSheet(getParameterProcurement, "PRWarehouseField2A");

		//PM_PR_183Reg
		txt_UICustomizerPurchaseRequisitionBtnA= findElementInXLSheet(getParameterProcurement, "UICustomizerPurchaseRequisitionBtnA");
		btn_UIPRNewCustomFieldA= findElementInXLSheet(getParameterProcurement, "UIPRNewCustomFieldA");
		txt_UIPRFieldLabelA= findElementInXLSheet(getParameterProcurement, "UIPRFieldLabelA");
		txt_UIPRFieldTypeA= findElementInXLSheet(getParameterProcurement, "UIPRFieldTypeA");
		btn_UIPRReportableA= findElementInXLSheet(getParameterProcurement, "UIPRReportableA");
		btn_UIAddFieldDeleteBtnA= findElementInXLSheet(getParameterProcurement, "UIAddFieldDeleteBtnA");
		txt_NewFieldTxtA= findElementInXLSheet(getParameterProcurement, "NewFieldTxtA");
		txt_NewFieldAfterTxtA= findElementInXLSheet(getParameterProcurement, "NewFieldAfterTxtA");
		btn_CreateBtnA= findElementInXLSheet(getParameterProcurement, "CreateBtnA");
		
		//PM_PR_184Reg
		btn_AddListValueBtnA= findElementInXLSheet(getParameterProcurement, "AddListValueBtnA");
		btn_AddNewListValueBtnA= findElementInXLSheet(getParameterProcurement, "AddNewListValueBtnA");
		txt_AddNewListValueTxtA= findElementInXLSheet(getParameterProcurement, "AddNewListValueTxtA");
		btn_AddNewListValueUpdateBtnA= findElementInXLSheet(getParameterProcurement, "AddNewListValueUpdateBtnA");
		txt_NewListFieldTxtA= findElementInXLSheet(getParameterProcurement, "NewListFieldTxtA");
		
		//PM_PR_185Reg
		txt_NewTextAreaFieldTxtA= findElementInXLSheet(getParameterProcurement, "NewTextAreaFieldTxtA");

		//PM_PR_190Reg
		sel_DateSelA= findElementInXLSheet(getParameterProcurement, "DateSelA");
		
		//Purchase Return Order
		
		//PM_PRO_001to005Reg
		btn_ReturnGroupAddBtn= findElementInXLSheet(getParameterProcurement, "ReturnGroupAddBtn");
		txt_ReturnGroupTextField= findElementInXLSheet(getParameterProcurement, "ReturnGroupTextField");
		btn_NewReturnGroupAddBtn= findElementInXLSheet(getParameterProcurement, "NewReturnGroupAddBtn");
		txt_NewReturnGroupAddTxt= findElementInXLSheet(getParameterProcurement, "NewReturnGroupAddTxt");
		btn_NewReturnGroupUpdateBtn= findElementInXLSheet(getParameterProcurement, "NewReturnGroupUpdateBtn");
		sel_ReturnGroupInactiveSel= findElementInXLSheet(getParameterProcurement, "ReturnGroupInactiveSel");
		
		//PM_PRO_006to010Reg
		txt_ContactPersonTitleVI= findElementInXLSheet(getParameterProcurement, "ContactPersonTitleVI");
		txt_ContactPersonNameVI= findElementInXLSheet(getParameterProcurement, "ContactPersonNameVI");
		txt_ContactPersonNamePRO= findElementInXLSheet(getParameterProcurement, "ContactPersonNamePRO");
		txt_CheckoutValidatorPRO= findElementInXLSheet(getParameterProcurement, "CheckoutValidatorPRO");
		txt_VendorValidationPRO= findElementInXLSheet(getParameterProcurement, "VendorValidationPRO");
		txt_CurrencyValidationPRO= findElementInXLSheet(getParameterProcurement, "CurrencyValidationPRO");
		txt_BillingAddressValidationPRO= findElementInXLSheet(getParameterProcurement, "BillingAddressValidationPRO");
		txt_ShippingAddressValidationPRO= findElementInXLSheet(getParameterProcurement, "ShippingAddressValidationPRO");
		txt_ProductGridErrorBtnPRO= findElementInXLSheet(getParameterProcurement, "ProductGridErrorBtnPRO");

		//PM_PRO_011to013Reg
		txt_ProductGridQtyFieldPRO= findElementInXLSheet(getParameterProcurement, "ProductGridQtyFieldPRO");
		txt_UnitPriceFieldPRO= findElementInXLSheet(getParameterProcurement, "UnitPriceFieldPRO");
		txt_DiscountPresentageFieldPRO= findElementInXLSheet(getParameterProcurement, "DiscountPresentageFieldPRO");
		txt_DiscountFieldPRO= findElementInXLSheet(getParameterProcurement, "DiscountFieldPRO");
		txt_TaxGroupFieldPRO= findElementInXLSheet(getParameterProcurement, "TaxGroupFieldPRO");
		
		//PM_PRO_014to016Reg
		txt_LineTotalPRO= findElementInXLSheet(getParameterProcurement, "LineTotalPRO");
		btn_CheckoutBtnPRO= findElementInXLSheet(getParameterProcurement, "CheckoutBtnPRO");
		
		//PM_PRO_017Reg
		txt_DiscountFieldDraftedPRO= findElementInXLSheet(getParameterProcurement, "DiscountFieldDraftedPRO");

		//PM_PRO_024Reg
		txt_TaxBreakdownTaxCode= findElementInXLSheet(getParameterProcurement, "TaxBreakdownTaxCode");
		btn_TaxBreakdownBtn= findElementInXLSheet(getParameterProcurement, "TaxBreakdownBtn");
		
		//PM_PRO_030Reg
		txt_BannerCurrency= findElementInXLSheet(getParameterProcurement, "BannerCurrency");
		
		//PM_PRO_031to032Reg
		btn_PurchaseReturnOrderByPageBtn= findElementInXLSheet(getParameterProcurement, "PurchaseReturnOrderByPageBtn");
		txt_PurchaseReturnOrderByPageSearchTxt= findElementInXLSheet(getParameterProcurement, "PurchaseReturnOrderByPageSearchTxt");
		sel_PurchaseReturnOrderByPageSearchSel= findElementInXLSheet(getParameterProcurement, "PurchaseReturnOrderByPageSearchSel");
		icn_HoldIconPROByPage= findElementInXLSheet(getParameterProcurement, "HoldIconPROByPage");
		icn_UnHoldIconPROByPage= findElementInXLSheet(getParameterProcurement, "UnHoldIconPROByPage");
		
		//PM_PRO_033Reg
		txt_HistortTabDraftHistoryPROTxtA= findElementInXLSheet(getParameterProcurement, "HistortTabDraftHistoryPROTxtA");
		txt_HistortTabUpdateHistoryPROTxtA= findElementInXLSheet(getParameterProcurement, "HistortTabUpdateHistoryPROTxtA");
		txt_HistortTabReleaseHistoryPROTxtA= findElementInXLSheet(getParameterProcurement, "HistortTabReleaseHistoryPROTxtA");
		txt_HistortTabReversedHistoryPROTxtA= findElementInXLSheet(getParameterProcurement, "HistortTabReversedHistoryPROTxtA");

		//PM_PRO_034to035Reg
		txt_WarehouseFieldPRO= findElementInXLSheet(getParameterProcurement, "WarehouseFieldPRO");
		btn_ProductMasterInfoOS= findElementInXLSheet(getParameterProcurement, "ProductMasterInfoOS");
		btn_ProductBatchSerialWiseCostOS= findElementInXLSheet(getParameterProcurement, "ProductBatchSerialWiseCostOS");
		txt_ProductBatchNoOS= findElementInXLSheet(getParameterProcurement, "ProductBatchNoOS");
		
		//PM_PRO_036Reg
		txt_DocStatusPresentage= findElementInXLSheet(getParameterProcurement, "DocStatusPresentage");

		//Vendor Group Configuration
		
		//PM_VENGRP_001to005Reg
		txt_VendorGroupBypageVGC= findElementInXLSheet(getParameterProcurement, "VendorGroupBypageVGC");
		btn_SummaryTabVGC= findElementInXLSheet(getParameterProcurement, "SummaryTabVGC");
		btn_DetailsTabVGC= findElementInXLSheet(getParameterProcurement, "DetailsTabVGC");
		btn_PaymentInformationVGC= findElementInXLSheet(getParameterProcurement, "PaymentInformationVGC");
		txt_VendorGroupVGC= findElementInXLSheet(getParameterProcurement, "VendorGroupVGC");
		txt_CurrencyVGC= findElementInXLSheet(getParameterProcurement, "CurrencyVGC");
		txt_ChargeGroupVGC= findElementInXLSheet(getParameterProcurement, "ChargeGroupVGC");
		txt_VendorGroupValidatorVGC= findElementInXLSheet(getParameterProcurement, "VendorGroupValidatorVGC");

	}

	public static void readData() throws Exception {
		// Com_TC_001
		String computerName = System.getenv().get("COMPUTERNAME");
		if (computerName.equals("DESKTOP-C6E8CJU") || computerName.equals("MADHUSHAN-LAPdfg")) {
			siteURL = findElementInXLSheet(getDataProcurement, "site url external");
		} else {
			siteURL = findElementInXLSheet(getDataProcurement, "site url");
		}
		userNameData = findElementInXLSheet(getDataProcurement, "user name data");
		passwordData = findElementInXLSheet(getDataProcurement, "password data");

		// PR_VGC_002
		vendorgroup = findElementInXLSheet(getDataProcurement, "vendorgroup");
		currency = findElementInXLSheet(getDataProcurement, "currency");
		PurchaseTaxGroup = findElementInXLSheet(getDataProcurement, "PurchaseTaxGroup");
		VendorClassification = findElementInXLSheet(getDataProcurement, "VendorClassification");
		StandardDeliveryMode = findElementInXLSheet(getDataProcurement, "StandardDeliveryMode");
		StandardDeliveryTerm = findElementInXLSheet(getDataProcurement, "StandardDeliveryTerm");
		Warehouse = findElementInXLSheet(getDataProcurement, "Warehouse");
		PaymentTerm = findElementInXLSheet(getDataProcurement, "PaymentTerm");
		Tolerance = findElementInXLSheet(getDataProcurement, "Tolerance ");
		CreditDays = findElementInXLSheet(getDataProcurement, "CreditDays");
		CreditLimit = findElementInXLSheet(getDataProcurement, "CreditLimit");
		// PR_VI_002
		VendorType = findElementInXLSheet(getDataProcurement, "VendorType");
		VendorNameTitle = findElementInXLSheet(getDataProcurement, "VendorNameTitle");
		VendorName = findElementInXLSheet(getDataProcurement, "VendorName");
		txtReceivableAccount = findElementInXLSheet(getDataProcurement, "txtReceivableAccount");
		ContactTitle = findElementInXLSheet(getDataProcurement, "ContactTitle");
		ContactName = findElementInXLSheet(getDataProcurement, "ContactName");
		MobileNumber = findElementInXLSheet(getDataProcurement, "MobileNumber");
		SelectIndustry = findElementInXLSheet(getDataProcurement, "SelectIndustry");
		TaxRegistrationNumber = findElementInXLSheet(getDataProcurement, "TaxRegistrationNumber");
		ReferenceAccoutNo = findElementInXLSheet(getDataProcurement, "ReferenceAccoutNo");
		txt3rdPartyAccount = findElementInXLSheet(getDataProcurement, "txt3rdPartyAccount");
		PassportNo = findElementInXLSheet(getDataProcurement, "PassportNo");
		newReceivableAccountname = findElementInXLSheet(getDataProcurement, "newReceivableAccountname");
		vendorgroupforven = findElementInXLSheet(getDataProcurement, "vendorgroupforven");
		// PM_VA_001
		AgreementNo = findElementInXLSheet(getDataProcurement, "AgreementNo");
		AgreementTitle = findElementInXLSheet(getDataProcurement, "AgreementTitle");
		AgreementDate = findElementInXLSheet(getDataProcurement, "AgreementDate");
		AgreementGroup = findElementInXLSheet(getDataProcurement, "AgreementGroup");
		Acurrency = findElementInXLSheet(getDataProcurement, "Acurrency");
		ValidityPeriodFor = findElementInXLSheet(getDataProcurement, "ValidityPeriodFor");
		Description = findElementInXLSheet(getDataProcurement, "Description");
		txtVendorAccount = findElementInXLSheet(getDataProcurement, "txtVendorAccount");
		ProductRelation = findElementInXLSheet(getDataProcurement, "ProductRelation");
		txtproduct = findElementInXLSheet(getDataProcurement, "txtproduct");
		minOrderQty = findElementInXLSheet(getDataProcurement, "minOrderQty");
		maxOrderQty = findElementInXLSheet(getDataProcurement, "maxOrderQty");
		txtAssignTo = findElementInXLSheet(getDataProcurement, "txtAssignTo");
		txtsubject = findElementInXLSheet(getDataProcurement, "txtsubject");

		// PM_PR_002

		RequestDate = findElementInXLSheet(getDataProcurement, "RequestDate");
		RCurrency = findElementInXLSheet(getDataProcurement, "RCurrency");
		Rtxtproduct = findElementInXLSheet(getDataProcurement, "Rtxtproduct");

		// PM_PR_005
		txtVendor = findElementInXLSheet(getDataProcurement, "txtVendor");
		txtBillingAddress = findElementInXLSheet(getDataProcurement, "txtBillingAddress");
		txtShippingAddress = findElementInXLSheet(getDataProcurement, "txtShippingAddress");

		// PM_PO_001
		ptxtproduct = findElementInXLSheet(getDataProcurement, "ptxtproduct");
		Count = findElementInXLSheet(getDataProcurement, "Count");
		TestQuantity = findElementInXLSheet(getDataProcurement, "TestQuantity");

		// PM_PO_002
		ptxtSproduct = findElementInXLSheet(getDataProcurement, "ptxtSproduct");
		txtVendorReferenceNo = findElementInXLSheet(getDataProcurement, "txtVendorReferenceNo");

		// PM_PO_006
		ptxtNxtproduct = findElementInXLSheet(getDataProcurement, "ptxtNxtproduct");
		txtCEstDescription = findElementInXLSheet(getDataProcurement, "txtCEstDescription");
		EstimateCost = findElementInXLSheet(getDataProcurement, "EstimateCost");
		ptxtBproduct = findElementInXLSheet(getDataProcurement, "ptxtBproduct");
		SerialNo = findElementInXLSheet(getDataProcurement, "SerialNo");
		BatchNo = findElementInXLSheet(getDataProcurement, "BatchNo");
		ExpiryDate = findElementInXLSheet(getDataProcurement, "ExpiryDate");

		// PM_PO_007
		txtQESTDescription = findElementInXLSheet(getDataProcurement, "txtQESTDescription");
		EstimatedQty = findElementInXLSheet(getDataProcurement, "EstimatedQty");

		// PM_PI_001
		PIcurrency = findElementInXLSheet(getDataProcurement, "PIcurrency");
		txtPIVendor = findElementInXLSheet(getDataProcurement, "txtPIVendor");
		txtPIBillingAddress = findElementInXLSheet(getDataProcurement, "txtPIBillingAddress");
		txtPIShippingAddress = findElementInXLSheet(getDataProcurement, "txtPIShippingAddress");
		PItxtproduct = findElementInXLSheet(getDataProcurement, "PItxtproduct");
		PItxtNxtproduct = findElementInXLSheet(getDataProcurement, "PItxtNxtproduct");

		// PM_PI_003
		PItxtSproduct = findElementInXLSheet(getDataProcurement, "PItxtSproduct");
		EstimatenewCost = findElementInXLSheet(getDataProcurement, "EstimatenewCost");

		// PM_PRO_001
		txtPROVendor = findElementInXLSheet(getDataProcurement, "txtPROVendor");
		PROqty = findElementInXLSheet(getDataProcurement, "PROqty");
		txtPOBillingAddress = findElementInXLSheet(getDataProcurement, "txtPOBillingAddress");
		txtPOShippingAddress = findElementInXLSheet(getDataProcurement, "txtPOShippingAddress");

		// PM_PRO_002
		POtxtproduct = findElementInXLSheet(getDataProcurement, "POtxtproduct");
		POwarehouse = findElementInXLSheet(getDataProcurement, "POwarehouse");
		POSerialNo = findElementInXLSheet(getDataProcurement, "POSerialNo");

		// PM_RFQ_001
		txtcontactperson = findElementInXLSheet(getDataProcurement, "txtcontactperson");
		txtRFQVendor = findElementInXLSheet(getDataProcurement, "txtRFQVendor");
		txtRFQproduct = findElementInXLSheet(getDataProcurement, "txtRFQproduct");
		RFQqty = findElementInXLSheet(getDataProcurement, "RFQqty");
		RFQQuotationNo = findElementInXLSheet(getDataProcurement, "RFQQuotationNo");
		RFQUnitPrice = findElementInXLSheet(getDataProcurement, "RFQUnitPrice");
		RFQMinQty = findElementInXLSheet(getDataProcurement, "RFQMinQty");
		RFQMaxQty = findElementInXLSheet(getDataProcurement, "RFQMaxQty");

		// SMOKE_PRO_03
		ICEDescription = findElementInXLSheet(getDataProcurement, "ICEDescription");
		ICEProducttxt = findElementInXLSheet(getDataProcurement, "ICEProducttxt");
		ICEEstimateCost = findElementInXLSheet(getDataProcurement, "ICEEstimateCost");

		// Master_001
		WarehouseCode = findElementInXLSheet(getDataProcurement, "WarehouseCode");
		WarehouseName = findElementInXLSheet(getDataProcurement, "WarehouseName");
		AgreementGroupParameterstxt = findElementInXLSheet(getDataProcurement, "AgreementGroupParameterstxt");

		// PM_PI_001to006Reg
		userNameDataReg = findElementInXLSheet(getDataProcurement, "userNameDataReg");
		passwordDataReg = findElementInXLSheet(getDataProcurement, "passwordDataReg");

		// PM_PI_007to010Reg
		VendorTextFieldA = findElementInXLSheet(getDataProcurement, "VendorTextFieldA");

		// PM_PI_015to025Reg
		BillingAddressA = findElementInXLSheet(getDataProcurement, "BillingAddressA");
		ShippingAddressA = findElementInXLSheet(getDataProcurement, "ShippingAddressA");
		ContactPersonNameA = findElementInXLSheet(getDataProcurement, "ContactPersonNameA");
		CustomerAccountTxtA = findElementInXLSheet(getDataProcurement, "CustomerAccountTxtA");
		NewCustomerAccountGroupA = findElementInXLSheet(getDataProcurement, "NewCustomerAccountGroupA");

		// PM_PI_026to031Reg
		ProductTxtA = findElementInXLSheet(getDataProcurement, "ProductTxtA");
		ProductTableTxtA = findElementInXLSheet(getDataProcurement, "ProductTableTxtA");
		ProductTableUnitPriceA = findElementInXLSheet(getDataProcurement, "ProductTableUnitPriceA");
		InactiveProductTxtA = findElementInXLSheet(getDataProcurement, "InactiveProductTxtA");
		InactiveProductTableTxtA = findElementInXLSheet(getDataProcurement, "InactiveProductTableTxtA");
		NewProductGroupA = findElementInXLSheet(getDataProcurement, "NewProductGroupA");
		NewProductManufacturerSearchTxtA = findElementInXLSheet(getDataProcurement, "NewProductManufacturerSearchTxtA");

		// PM_PI_032to043Reg
		TestProductTxtA = findElementInXLSheet(getDataProcurement, "TestProductTxtA");
		ProductAdvanceTaxGroupA = findElementInXLSheet(getDataProcurement, "ProductAdvanceTaxGroupA");
		ProductAdvanceTaxGroupNewA = findElementInXLSheet(getDataProcurement, "ProductAdvanceTaxGroupNewA");

		// PM_PI_044to058Reg
		TaxA = findElementInXLSheet(getDataProcurement, "TaxA");

		// PM_PI_079to080Reg
		TestProductToPOtoPITxtA = findElementInXLSheet(getDataProcurement, "TestProductToPOtoPITxtA");
		
		//PM_PI_084Reg
		UICustomizerPermissionLevel2A = findElementInXLSheet(getDataProcurement, "UICustomizerPermissionLevel2A");


		// PM_PI_094Reg
		TestProductServiceTxtA = findElementInXLSheet(getDataProcurement, "TestProductServiceTxtA");

		// PM_PI_101Reg
		NewUserNameA = findElementInXLSheet(getDataProcurement, "NewUserNameA");
		NewPasswordA = findElementInXLSheet(getDataProcurement, "NewPasswordA");

		// PM_PI_102to103Reg
		UICustomizerBalancingLevelA = findElementInXLSheet(getDataProcurement, "UICustomizerBalancingLevelA");
		UICustomizerPermissionLevelA = findElementInXLSheet(getDataProcurement, "UICustomizerPermissionLevelA");
		UICustomizerApplicableJourneyA = findElementInXLSheet(getDataProcurement, "UICustomizerApplicableJourneyA");

		// PM_PI_106to109Reg
		CostCenterTxtA = findElementInXLSheet(getDataProcurement, "CostCenterTxtA");
		CostAssignmentTypeTxtA = findElementInXLSheet(getDataProcurement, "CostAssignmentTypeTxtA");
		CostObjectSearchTxtA = findElementInXLSheet(getDataProcurement, "CostObjectSearchTxtA");

		// PM_PI_116Reg
		TestProductServiceTxtNewA = findElementInXLSheet(getDataProcurement, "TestProductServiceTxtNewA");
		TestProductServiceTxtNewTxtA = findElementInXLSheet(getDataProcurement, "TestProductServiceTxtNewTxtA");

		// PM_PI_124Reg
		TestProductServiceTxtNew2A = findElementInXLSheet(getDataProcurement, "TestProductServiceTxtNew2A");
		TestProductServiceTxtNewTxt2A = findElementInXLSheet(getDataProcurement, "TestProductServiceTxtNewTxt2A");

		// PM_PI_131Reg
		ChargeCodeA = findElementInXLSheet(getDataProcurement, "ChargeCodeA");

		// PM_PI_132Reg
		TestPOTxt1A = findElementInXLSheet(getDataProcurement, "TestPOTxt1A");
		TestPOTxt2A = findElementInXLSheet(getDataProcurement, "TestPOTxt2A");
		TestPOTxt3A = findElementInXLSheet(getDataProcurement, "TestPOTxt3A");

		// PM_PO_001to010Reg
		UserSearchTxtA = findElementInXLSheet(getDataProcurement, "UserSearchTxtA");

		// PM_PO_033to047Reg
		TestProPOA = findElementInXLSheet(getDataProcurement, "TestProPOA");

		// PM_PO_069
		ChargeCodePOA = findElementInXLSheet(getDataProcurement, "ChargeCodePOA");

		// PM_PR_001to005Reg
		ProductPRA = findElementInXLSheet(getDataProcurement, "ProductPRA");
		ProductTxtPRA = findElementInXLSheet(getDataProcurement, "ProductTxtPRA");
		
		// PM_PR_011to013Reg
		RequestGroupPR1A = findElementInXLSheet(getDataProcurement, "RequestGroupPR1A");
		RequestGroupPR2A = findElementInXLSheet(getDataProcurement, "RequestGroupPR2A");
		ProductGroupPR1A = findElementInXLSheet(getDataProcurement, "ProductGroupPR1A");
		ProductGroupPR2A = findElementInXLSheet(getDataProcurement, "ProductGroupPR2A");

		//PM_PR_021to022Reg
		InternalOrderRequesterTxtPRA = findElementInXLSheet(getDataProcurement, "InternalOrderRequesterTxtPRA");

		//PM_PR_030to031Reg
		Requester1 = findElementInXLSheet(getDataProcurement, "Requester1");
		Requester2 = findElementInXLSheet(getDataProcurement, "Requester2");
		Requester3 = findElementInXLSheet(getDataProcurement, "Requester3");

		//PM_PR_058to061Reg
		NextReviewUserA = findElementInXLSheet(getDataProcurement, "NextReviewUserA");

		//PM_PR_073to074Reg
		PostBusinessUnit1A = findElementInXLSheet(getDataProcurement, "PostBusinessUnit1A");
		NextReviewUserNewA = findElementInXLSheet(getDataProcurement, "NextReviewUserNewA");
		
		//PM_PR_078to079Reg
		PostBusinessUnit2A = findElementInXLSheet(getDataProcurement, "PostBusinessUnit2A");
		
		//PM_PR_111to112Reg
		PostBusinessUnit3A = findElementInXLSheet(getDataProcurement, "PostBusinessUnit3A");
		
		//PM_PR_177Reg
		PRProductField1A = findElementInXLSheet(getDataProcurement, "PRProductField1A");
		PRProductField2A = findElementInXLSheet(getDataProcurement, "PRProductField2A");
		PRProductField3A = findElementInXLSheet(getDataProcurement, "PRProductField3A");

		//PM_PR_180Reg
		PRWarehouseField1A = findElementInXLSheet(getDataProcurement, "PRWarehouseField1A");
		PRWarehouseField2A = findElementInXLSheet(getDataProcurement, "PRWarehouseField2A");
		PRWarehouseField3A = findElementInXLSheet(getDataProcurement, "PRWarehouseField3A");
		
		//Purchase Return Order
		
		//PM_PRO_006to010Reg
		ProPRO1 = findElementInXLSheet(getDataProcurement, "ProPRO1");
		ProPRO2 = findElementInXLSheet(getDataProcurement, "ProPRO2");
		ProPRO3 = findElementInXLSheet(getDataProcurement, "ProPRO3");

		//PM_PRO_024Reg
		TaxBreakdownTaxCode1 = findElementInXLSheet(getDataProcurement, "TaxBreakdownTaxCode1");
		TaxBreakdownTaxCode2 = findElementInXLSheet(getDataProcurement, "TaxBreakdownTaxCode2");

		//PM_PRO_029Reg
		VenNamePRO = findElementInXLSheet(getDataProcurement, "VenNamePRO");

	}

}
